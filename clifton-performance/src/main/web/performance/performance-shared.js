Ext.ns('Clifton.performance', 'Clifton.performance.account', 'Clifton.performance.account.securitytarget', 'Clifton.performance.composite', 'Clifton.performance.composite.account', 'Clifton.performance.composite.assignment', 'Clifton.performance.composite.performance', 'Clifton.performance.composite.setup', 'Clifton.performance.composite.upload', 'Clifton.performance.composite.account.performance', 'Clifton.performance.report', 'Clifton.performance.account.benchmark');

Clifton.performance.PerformanceReportingFrequencies = [
	['NONE', 'NONE', 'No deliverable reporting'],
	['MONTHLY', 'MONTHLY', 'Monthly reporting'],
	['QUARTERLY', 'QUARTERLY', 'Quarterly reporting']
];


Clifton.performance.account.PerformanceAccountWindowOverrides = [];
Clifton.performance.account.PerformanceAccountWindowOverrides['SECURITY_TARGET'] = 'Clifton.performance.account.securitytarget.PerformanceAccountSecurityTargetWindow';


Clifton.performance.composite.CompositePerformanceBaseWorkflowGrid = Ext.extend(TCG.grid.GridPanel, {
	xtype: 'gridpanel',
	wikiPage: 'IT/Performance+Calculation+Requirements',
	queryExportTagName: 'Performance',
	viewNames: ['Default', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'],
	rowSelectionModel: 'multiple',
	workflowStateNames: undefined,
	workflowStatusNames: undefined,
	drillDownOnly: true,
	showPerformanceReportButton: false,
	showPerformanceReportPostButton: false,
	showPerformanceReportPreliminaryPostButton: false,
	showDragAndDropAttachment: false,
	initComponent: function() {
		const currentItems = [];
		Ext.each(this.firstColumns, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.accountingPeriodColumns, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.periodValueColumns, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.returnColumns, function(f) {
			currentItems.push(f);
		});
		this.columns = currentItems;
		this.editor.drillDownOnly = this.drillDownOnly;
		if (this.transitionWorkflowStateList) {
			this.editor.transitionWorkflowStateList = this.transitionWorkflowStateLsit;
		}
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);
	},
	editor: {
		allowToDeleteMultiple: true,
		getDetailPageClass: function(grid, row) {
			return this.getGridPanel().getDetailClass();
		},
		ptype: 'workflowAwareEntity-grideditor',
		transitionWorkflowStateList: [
			{stateName: 'Approve', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approve Selected Performance - Moves either to Pending Approval, Preliminary, or Approved based on Current State'},
			{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance - Return to Draft State'}
		],
		getCorrectToStateName: function(fromState, toState, record) {
			if (fromState === toState) {
				TCG.showError('Selected performance is already in state \'' + toState + '\'.');
				return;
			}
			else if (toState === 'Approve') {
				if (fromState === 'Approved') {
					TCG.showError('Cannot Approve \'Approved\' performance.');
					return;
				}
				if (fromState === 'Pending Approval') {
					return 'Preliminary';
				}
				if (fromState === 'Draft') {
					return 'Validated (Pre-Process Rules)';
				}
				return 'Validated (Post-Process Rules)';
			}
			return toState;
		}
	},
	plugins: {ptype: 'gridsummary'},
	firstColumns: [],
	accountingPeriodColumns: [
		{header: 'AccountingPeriodId', width: 50, dataIndex: 'accountingPeriod.id', hidden: true},
		{header: 'Start Date', width: 50, dataIndex: 'accountingPeriod.startDate', filter: {searchFieldName: 'periodStartDate'}, hidden: true},
		{header: 'End Date', width: 50, dataIndex: 'accountingPeriod.endDate', filter: {searchFieldName: 'periodEndDate'}, viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Date'}, {name: 'CAPS Export - Model Net', label: 'Date'}], defaultHeader: 'End Date'}
	],
	periodValueColumns: [
		{header: 'Market Value', width: 60, dataIndex: 'periodEndMarketValue', type: 'currency', useNull: true, viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Mkt Value'}, {name: 'CAPS Export - Model Net', label: 'Total Mkt Value'}], defaultHeader: 'Market Value'}
	],
	returnColumns: [
		{header: 'Gross Return', width: 60, dataIndex: 'periodGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net', 'Monthly Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Retn (Gross)', exportDataType: 'DECIMAL_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}, {name: 'CAPS Export - Model Net', label: 'Total Retn (Gross)', exportDataType: 'DECIMAL_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}], defaultHeader: {label: 'Gross Return', exportDataType: 'PERCENT_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}},
		{header: 'Actual Net Return', width: 60, dataIndex: 'periodNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, viewNames: ['Default', 'CAPS Export - Actual Net', 'Monthly Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Retn (Net)', exportDataType: 'DECIMAL_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}], defaultHeader: {label: 'Actual Net Return', exportDataType: 'DECIMAL_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}, tooltip: 'Actual Net'},
		{header: 'Model Net Return', width: 60, dataIndex: 'periodModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, viewNames: ['Default', 'CAPS Export - Model Net', 'Monthly Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Model Net', label: 'Total Retn (Net)', exportDataType: 'DECIMAL_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}], defaultHeader: {label: 'Model Net Return', exportDataType: 'DECIMAL_PRECISE', exportColumnValueConverter: 'percentColumnReversableConverter'}, tooltip: 'Model Net'},
		{header: 'Benchmark', width: 100, hidden: true, dataIndex: 'benchmarkSecurity.label', filter: {searchFieldName: 'benchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
		{header: 'Benchmark Return', width: 60, dataIndex: 'periodBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, viewNames: ['Default', 'Monthly Returns', 'Period Ending Returns']},
		{header: 'Secondary Benchmark', width: 100, hidden: true, dataIndex: 'secondaryBenchmarkSecurity.label', filter: {searchFieldName: 'secondaryBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
		{header: 'Secondary Benchmark Return', width: 60, dataIndex: 'periodSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, viewNames: ['Default', 'Monthly Returns', 'Period Ending Returns']},
		{header: 'Third Benchmark', width: 100, hidden: true, dataIndex: 'thirdBenchmarkSecurity.label', filter: {searchFieldName: 'thirdBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
		{header: 'Third Benchmark Return', width: 60, hidden: true, dataIndex: 'periodThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, viewNames: ['Default', 'Monthly Returns', 'Period Ending Returns']},
		{header: 'Risk Free Security', width: 100, hidden: true, dataIndex: 'riskFreeSecurity.label', filter: {searchFieldName: 'riskFreeSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
		{header: 'Risk Free Return', width: 60, dataIndex: 'periodRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: 'QTD Gross', width: 60, dataIndex: 'quarterToDateGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Quarterly Returns', 'Period Ending Returns']},
		{header: 'QTD Actual Net', width: 60, dataIndex: 'quarterToDateNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Quarterly Returns', 'Period Ending Returns']},
		{header: 'QTD Model Net', width: 60, dataIndex: 'quarterToDateModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Quarterly Returns', 'Period Ending Returns']},
		{header: 'QTD Benchmark', width: 60, dataIndex: 'quarterToDateBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Quarterly Returns', 'Period Ending Returns']},
		{header: 'QTD Secondary Benchmark', width: 60, dataIndex: 'quarterToDateSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Quarterly Returns', 'Period Ending Returns']},
		{header: 'QTD Third Benchmark', width: 60, dataIndex: 'quarterToDateThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Quarterly Returns', 'Period Ending Returns']},
		{header: 'QTD Risk Free Return', width: 60, dataIndex: 'quarterToDateRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: 'YTD Gross', width: 60, dataIndex: 'yearToDateGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'YTD Actual Net', width: 60, dataIndex: 'yearToDateNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'YTD Model Net', width: 60, dataIndex: 'yearToDateModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'YTD Benchmark', width: 60, dataIndex: 'yearToDateBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'YTD Secondary Benchmark', width: 60, dataIndex: 'yearToDateSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'YTD Third Benchmark', width: 60, dataIndex: 'yearToDateThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'YTD Risk Free Return', width: 60, dataIndex: 'yearToDateRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: '1Y Gross', width: 60, dataIndex: 'oneYearGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Annual Returns', 'Period Ending Returns']},
		{header: '1Y Actual Net', width: 60, dataIndex: 'oneYearNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Annual Returns', 'Period Ending Returns']},
		{header: '1Y Model Net', width: 60, dataIndex: 'oneYearModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Annual Returns', 'Period Ending Returns']},
		{header: '1Y Benchmark', width: 60, dataIndex: 'oneYearBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Annual Returns', 'Period Ending Returns']},
		{header: '1Y Secondary Benchmark', width: 60, dataIndex: 'oneYearSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Annual Returns', 'Period Ending Returns']},
		{header: '1Y Third Benchmark', width: 60, dataIndex: 'oneYearThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Annual Returns', 'Period Ending Returns']},
		{header: '1Y Risk Free Return', width: 60, dataIndex: 'oneYearRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: '1Y Gross STD', width: 60, dataIndex: 'oneYearGrossStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Actual Net STD', width: 60, dataIndex: 'oneYearNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Model Net STD', width: 60, dataIndex: 'oneYearModelNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Benchmark STD', width: 60, dataIndex: 'oneYearBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Secondary Benchmark STD', width: 60, dataIndex: 'oneYearSecondaryBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Third Benchmark STD', width: 60, dataIndex: 'oneYearThirdBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},

		{header: '1Y Gross Sharpe', width: 60, dataIndex: 'oneYearGrossSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Net Sharpe', width: 60, dataIndex: 'oneYearNetSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Benchmark Sharpe', width: 60, dataIndex: 'oneYearBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Secondary Benchmark Sharpe', width: 60, dataIndex: 'oneYearSecondaryBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '1Y Third Benchmark Sharpe', width: 60, dataIndex: 'oneYearThirdBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},

		{header: '3Y Gross', width: 60, dataIndex: 'threeYearGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Actual Net', width: 60, dataIndex: 'threeYearNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Model Net', width: 60, dataIndex: 'threeYearModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Benchmark', width: 60, dataIndex: 'threeYearBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Secondary Benchmark', width: 60, dataIndex: 'threeYearSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Third Benchmark', width: 60, dataIndex: 'threeYearThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Risk Free Return', width: 60, dataIndex: 'threeYearRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: '3Y Gross STD', width: 60, dataIndex: 'threeYearGrossStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Actual Net STD', width: 60, dataIndex: 'threeYearNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Model Net STD', width: 60, dataIndex: 'threeYearModelNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Benchmark STD', width: 60, dataIndex: 'threeYearBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Secondary Benchmark STD', width: 60, dataIndex: 'threeYearSecondaryBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Third Benchmark STD', width: 60, dataIndex: 'threeYearThirdBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},

		{header: '3Y Gross Sharpe', width: 60, dataIndex: 'threeYearGrossSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Net Sharpe', width: 60, dataIndex: 'threeYearNetSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Benchmark Sharpe', width: 60, dataIndex: 'threeYearBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Secondary Benchmark Sharpe', width: 60, dataIndex: 'threeYearSecondaryBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '3Y Third Benchmark Sharpe', width: 60, dataIndex: 'threeYearThirdBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true, viewNames: ['Period Ending Returns']},

		{header: '5Y Gross', width: 60, dataIndex: 'fiveYearGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '5Y Actual Net', width: 60, dataIndex: 'fiveYearNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '5Y Model Net', width: 60, dataIndex: 'fiveYearModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '5Y Benchmark', width: 60, dataIndex: 'fiveYearBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '5Y Secondary Benchmark', width: 60, dataIndex: 'fiveYearSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '5Y Third Benchmark', width: 60, dataIndex: 'fiveYearThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '5Y Risk Free Return', width: 60, dataIndex: 'fiveYearRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: '5Y Gross STD', width: 60, dataIndex: 'fiveYearGrossStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Actual Net STD', width: 60, dataIndex: 'fiveYearNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Model Net STD', width: 60, dataIndex: 'fiveYearModelNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Benchmark STD', width: 60, dataIndex: 'fiveYearBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Secondary Benchmark STD', width: 60, dataIndex: 'fiveYearSecondaryBenchmarkStandardDeviation', type: 'float', renderer: TCG.renderPercent, useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Third Benchmark STD', width: 60, dataIndex: 'fiveYearThirdBenchmarkStandardDeviation', type: 'float', renderer: TCG.renderPercent, useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},

		{header: '5Y Gross Sharpe', width: 60, dataIndex: 'fiveYearGrossSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Net Sharpe', width: 60, dataIndex: 'fiveYearNetSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Benchmark Sharpe', width: 60, dataIndex: 'fiveYearBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Secondary Benchmark Sharpe', width: 60, dataIndex: 'fiveYearSecondaryBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '5Y Third Benchmark Sharpe', width: 60, dataIndex: 'fiveYearThirdBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},

		{header: '10Y Gross', width: 60, dataIndex: 'tenYearGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '10Y Actual Net', width: 60, dataIndex: 'tenYearNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '10Y Model Net', width: 60, dataIndex: 'tenYearModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '10Y Benchmark', width: 60, dataIndex: 'tenYearBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '10Y Secondary Benchmark', width: 60, dataIndex: 'tenYearSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '10Y Third Benchmark', width: 60, dataIndex: 'tenYearThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: '10Y Risk Free Return', width: 60, dataIndex: 'tenYearRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: '10Y Gross STD', width: 60, dataIndex: 'tenYearGrossStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Actual Net STD', width: 60, dataIndex: 'tenYearNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Model Net STD', width: 60, dataIndex: 'tenYearModelNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Benchmark STD', width: 60, dataIndex: 'tenYearBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Secondary Benchmark STD', width: 60, dataIndex: 'tenYearSecondaryBenchmarkStandardDeviation', type: 'float', renderer: TCG.renderPercent, useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Third Benchmark STD', width: 60, dataIndex: 'tenYearThirdBenchmarkStandardDeviation', type: 'float', renderer: TCG.renderPercent, useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},

		{header: '10Y Gross Sharpe', width: 60, dataIndex: 'tenYearGrossSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Net Sharpe', width: 60, dataIndex: 'tenYearNetSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Benchmark Sharpe', width: 60, dataIndex: 'tenYearBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Secondary Benchmark Sharpe', width: 60, dataIndex: 'tenYearSecondaryBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},
		{header: '10Y Third Benchmark Sharpe', width: 60, dataIndex: 'tenYearThirdBenchmarkSharpeRatio', type: 'float', useNull: true, negativeInRed: true, positiveInGreen: true, hidden: true},

		{header: 'ITD Gross', width: 60, dataIndex: 'inceptionToDateGrossReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Actual Net', width: 60, dataIndex: 'inceptionToDateNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Model Net', width: 60, dataIndex: 'inceptionToDateModelNetReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Benchmark', width: 60, dataIndex: 'inceptionToDateBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Secondary Benchmark', width: 60, dataIndex: 'inceptionToDateSecondaryBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Third Benchmark', width: 60, dataIndex: 'inceptionToDateThirdBenchmarkReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Risk Free Return', width: 60, dataIndex: 'inceptionToDateRiskFreeSecurityReturn', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true},

		{header: 'ITD Gross STD', width: 60, dataIndex: 'inceptionToDateGrossStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Actual Net STD', width: 60, dataIndex: 'inceptionToDateNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Model Net STD', width: 60, dataIndex: 'inceptionToDateModelNetStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Benchmark STD', width: 60, dataIndex: 'inceptionToDateBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Secondary Benchmark STD', width: 60, dataIndex: 'inceptionToDateSecondaryBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Third Benchmark STD', width: 60, dataIndex: 'inceptionToDateThirdBenchmarkStandardDeviation', type: 'float', useNull: true, renderer: TCG.renderPercent, hidden: true, viewNames: ['Period Ending Returns']},

		{header: 'ITD Gross Sharpe', width: 60, dataIndex: 'inceptionToDateGrossSharpeRatio', type: 'float', useNull: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Actual Net Sharpe', width: 60, dataIndex: 'inceptionToDateNetSharpeRatio', type: 'float', useNull: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Model Net Sharpe', width: 60, dataIndex: 'inceptionToDateModelNetSharpeRatio', type: 'float', useNull: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Benchmark Sharpe', width: 60, dataIndex: 'inceptionToDateBenchmarkSharpeRatio', type: 'float', useNull: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Secondary Benchmark Sharpe', width: 60, dataIndex: 'inceptionToDateSecondaryBenchmarkSharpeRatio', type: 'float', useNull: true, hidden: true, viewNames: ['Period Ending Returns']},
		{header: 'ITD Third Benchmark Sharpe', width: 60, dataIndex: 'inceptionToDateThirdBenchmarkSharpeRatio', type: 'float', useNull: true, hidden: true, viewNames: ['Period Ending Returns']},

		{header: 'Workflow Status', dataIndex: 'workflowStatus.name', width: 75, filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
		{
			header: 'Workflow State', dataIndex: 'workflowState.name', width: 100, filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'],
			renderer: function(v, metaData, r) {
				const sts = r.data['workflowStatus.name'];
				if (sts === 'Draft') {
					metaData.css = 'amountNegative';
				}
				else if (sts === 'Approved') {
					if (v === 'Pending Reporting') {
						metaData.css = 'amountAdjusted';
					}
					else {
						metaData.css = 'amountPositive';
					}
				}
				else {
					metaData.css = 'amountAdjusted';
				}
				return v;
			}
		},
		{
			header: 'Violation Status', width: 80, dataIndex: 'violationStatus.name', filter: {type: 'list', options: Clifton.rule.violation.StatusOptions, searchFieldName: 'violationStatusNames'}, viewNames: ['Default', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'],
			renderer: function(v, p, r) {
				return (Clifton.rule.violation.renderViolationStatus(v));
			}
		},
		{header: '<div class="www" style="WIDTH:16px">&nbsp;</div>', width: 12, tooltip: 'If checked, there is at least one APPROVED file associated with this summary on the portal.', exportHeader: 'Posted', dataIndex: 'postedToPortal', type: 'boolean', viewNames: ['Default', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns']}
	],
	getWorkflowStateNames: function() {
		return this.workflowStateNames;
	},
	applyDefaultFilters: function(firstLoad) {
		// DO NOTHING OVERRIDDEN BY MARKETING REPORTING TAB TO DEFAULT ACCOUNTING PERIOD END DATE TO PREVIOUS QUARTER END
	},
	getLoadParams: function(firstLoad) {
		this.applyDefaultFilters(firstLoad);
		let params = this.getAdditionalLoadParams(firstLoad);
		if (params === undefined) {
			params = {};
		}
		// Only users with Full Control can see Draft Performance
		params.requireFullControlForDraftStatus = true;
		const workflowStateNames = this.getWorkflowStateNames();
		if (TCG.isNotNull(workflowStateNames)) {
			if (typeof workflowStateNames == 'string') {
				params['workflowStateNameEquals'] = workflowStateNames;
			}
			else {
				params['workflowStateNames'] = workflowStateNames;
			}
		}
		if (TCG.isNotNull(this.workflowStatusNames)) {
			if (typeof this.workflowStatusNames == 'string') {
				params['workflowStatusNameEquals'] = this.workflowStatusNames;
			}
			else {
				params['workflowStatusNames'] = this.workflowStatusNames;
			}
		}

		return params;
	},
	getTopToolbarFilters: function(toolbar) {
		let topToolbarFilters = this.getAdditionalTopToolbarFilters(toolbar).concat([
			{
				fieldLabel: 'Month', xtype: 'datefield-monthYear', name: 'measureMonthDate', width: 120,
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			},
			{name: 'performanceFrequencyCombo', xtype: 'performance-frequency-combo'}
		]);
		if (this.showDragAndDropAttachment) {
			// Not an actual filter, but would like the paperclip to show up on the right
			topToolbarFilters = topToolbarFilters.concat(
				{
					xtype: 'drag-drop-container',
					layout: 'fit',
					allowMultiple: true,
					cls: undefined,
					popupComponentName: 'Clifton.document.FileImportBulkSelectExistHandlerWindow',
					popupComponentConfig: {
						title: 'Account Performance Bulk Upload',
						instructions: 'The file name must contain the account name and date.  Example:  The University of Arkansas Foundation Inc._20170731__.pdf maps to Investment Account name \'The University of Arkansas Foundation Inc.\' with Report Date of \'7/31/2017\'.',
						definitionName: 'PerformanceCompositeAccountPerformanceAttachments'
					},
					maxFiles: 100,
					message: '&nbsp;',
					tooltipMessage: 'Drag and drop files to this grid to import for Account Performance.',
					getParams: function(formPanel) {
						const definitionName = 'PerformanceCompositeAccountPerformanceAttachments';
						const definition = TCG.data.getData('documentDefinitionByName.json?name=' + definitionName, formPanel, 'document.definitionName.' + definitionName);
						return {
							'definitionId': definition.id
						};
					}
				}
			);
		}
		return topToolbarFilters;
	},
	switchToViewBeforeReload: function(viewName) {
		//Bind the Views to the Frequency Combo. QTD View = QTD Frequency
		const combo = TCG.getChildByName(this.grid.ownerGridPanel.topToolbar, 'performanceFrequencyCombo');
		const monthCombo = TCG.getChildByName(this.grid.ownerGridPanel.topToolbar, 'measureMonthDate');
		if (combo && monthCombo) {
			const store = combo.getStore(),
				index = store.find('name', viewName),
				record = store.getAt(index);
			if (record === undefined) {
				combo.clearValue();
				monthCombo.setValue((new Date()).getFirstDateOfMonth().add(Date.MONTH, -1));
			}
			else {
				monthCombo.setValue();
				combo.setValue(record.data.value);
				combo.fireEvent('select', combo, record, index);
			}
		}
	},
	getAdditionalTopToolbarFilters: function(toolbar) {
		return [];//override by implementing objects
	},
	getDetailClass: function() {
		//place holder for overriding what the detail page class is at the grid level
	},
	executeReport: function(format) {
		//place holder for grid specific report execution logic
	},
	getAdditionalLoadParams: function(firstLoad) {
		//place holder for grid specific load param logic
		return undefined;
	},
	addToolbarButtons: function(t, gridPanel) {
		if (this.showPerformanceReportButton) {
			t.add({
				text: 'Report',
				tooltip: 'Download PDF version of report for selected performance record(s). Report selections are defined by the composite or composite assignment.',
				iconCls: 'pdf',
				xtype: 'splitbutton',
				scope: gridPanel,
				handler: function() {
					this.executeReport('PDF');
				},
				menu: {
					items: [
						{
							text: 'Report (Word)',
							iconCls: 'word',
							tooltip: 'Download Word version of report for selected performance record. Only one record can be selected.  Report selections are defined by the composite or composite assignment.',
							scope: gridPanel,
							handler: function() {
								this.executeReport('WORD');
							}
						},
						{
							text: 'Select a Report', iconCls: 'pdf',
							menu: new Ext.menu.Menu({
								layout: 'fit',
								style: {overflow: 'visible'},
								width: 200,
								items: [
									{
										xtype: 'combo', name: 'reportId', url: 'reportListFind.json?templateName=Performance Strategy Report',
										listeners: {
											'select': function(combo) {
												const reportId = combo.getValue();
												gridPanel.executeReport('PDF', reportId);
											}
										}
									}
								]
							})
						}
						,
						{
							text: 'Select a Report', iconCls: 'word',
							menu: new Ext.menu.Menu({
								layout: 'fit',
								style: {overflow: 'visible'},
								width: 200,
								items: [
									{
										xtype: 'combo', name: 'reportId', url: 'reportListFind.json?templateName=Performance Strategy Report',
										listeners: {
											'select': function(combo) {
												const reportId = combo.getValue();
												gridPanel.executeReport('WORD', reportId);
											}
										}
									}
								]
							})
						}
						,
						{
							text: 'Zip Reports (PDF)',
							iconCls: 'zip',
							tooltip: 'Download a zip version of report for selected performance records. One or more records can be selected.  PDF Attachments, if any, are included with each report.',
							scope: gridPanel,
							handler: function() {
								this.executeReport('PDF', null, true, true);
							}
						}
					]
				}
			});
			t.add('-');

			if (this.showPerformanceReportPostButton) {
				t.add({
					text: 'Post',
					tooltip: 'Manually Post PDF of Strategy Performance report for selected accounting period.  Warning this should only be done for historical reports or when necessary.  Otherwise, please use workflow transitions to handle posting.',
					iconCls: 'www',
					scope: gridPanel,
					handler: function() {
						this.postReport();
					}
				});
				t.add('-');
			}

			if (this.showPerformanceReportPreliminaryPostButton) {
				t.add({
					text: 'Post Prelim',
					tooltip: 'Manually Post Preliminary PDF of Strategy Performance report for selected accounting period.  Report and posting options can be customized.',
					iconCls: 'www',
					scope: gridPanel,
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select one row to post preliminary report for.', 'No Row(s) Selected');
						}
						else if (sm.getCount() > 1) {
							TCG.showError('Please select only one row to post preliminary report for.', 'Multiple Rows Selected');
							return;
						}
						let params = '';
						if (TCG.isEquals('Clifton.performance.composite.account.performance.CompositeAccountPerformanceWindow', gridPanel.getDetailClass())) {
							params += 'performanceCompositeInvestmentAccountPerformanceId=';
						}
						else {
							params += 'performanceCompositePerformanceId=';
						}
						params += sm.getSelected().id;
						TCG.data.getDataPromise('performanceReportPostingFileConfigurationPreliminary.json?' + params, this).then(function(defaultData) {
								TCG.createComponent('Clifton.performance.report.PerformanceReportPostingWindow', {
									defaultData: defaultData
								});
							}
						);
					}
				});
				t.add('-');
			}
		}
	}
});
Ext.reg('composite-performance-base-workflow-grid', Clifton.performance.composite.CompositePerformanceBaseWorkflowGrid);


Clifton.performance.composite.CompositePerformanceWorkflowGrid = Ext.extend(Clifton.performance.composite.CompositePerformanceBaseWorkflowGrid, {
	name: 'performanceCompositePerformanceListFind',
	importTableName: 'PerformanceCompositePerformance',
	tableName: 'PerformanceCompositePerformance',
	periodValueColumns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Composite Name', width: 200, dataIndex: 'performanceComposite.name', filter: {searchFieldName: 'performanceCompositeName'}, viewNames: ['Default', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns']},
		{header: 'Composite Short Name', width: 200, dataIndex: 'performanceComposite.compositeShortName', hidden: true},
		{header: 'Market Value', width: 60, dataIndex: 'periodEndMarketValue', type: 'currency', useNull: true, viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Mkt Value'}, {name: 'CAPS Export - Model Net', label: 'Total Mkt Value'}], defaultHeader: 'Market Value'}
	],
	getAdditionalTopToolbarFilters: function(toolbar) {
		return [
			{
				fieldLabel: 'Composite Status', name: 'compositeStatus', hiddenName: 'compositeStatus', xtype: 'toolbar-combo', displayField: 'name', valueField: 'value', mode: 'local', value: 'Active',
				store: {
					xtype: 'arraystore',
					fields: ['name', 'value', 'description'],
					data: Clifton.performance.composite.PerformanceCompositeStatus
				}
			},
			{
				fieldLabel: 'Composite', xtype: 'toolbar-combo', name: 'performanceCompositeId', width: 180, url: 'performanceCompositeListFind.json', displayField: 'label', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					combo.resetStore(); // Requery each time to get status filters applied
					const compositeStatus = TCG.getChildByName(toolbar, 'compositeStatus');
					queryEvent.combo.store.setBaseParam('compositeStatus', compositeStatus.getValue());
				}
			}
		];
	},
	getDetailClass: function() {
		return 'Clifton.performance.composite.performance.CompositePerformanceWindow';
	},
	getAdditionalLoadParams: function(firstLoad) {
		const t = this.getTopToolbar();
		const dateField = TCG.getChildByName(t, 'measureMonthDate');
		const performanceCompositeId = TCG.getChildByName(t, 'performanceCompositeId').getValue();
		if (firstLoad && dateField && dateField.getValue() === '') {
			// Default to Last Month
			const dt = (new Date()).getFirstDateOfMonth().add(Date.MONTH, -1);
			dateField.setValue(dt);
		}
		if (dateField) {
			const measureMonthDate = TCG.parseDate(dateField.getValue(), 'm/d/Y');
			return {
				periodStartDate: (measureMonthDate === '') ? '' : (measureMonthDate.getFirstDateOfMonth()).format('m/d/Y'),
				performanceCompositeId: performanceCompositeId
			};
		}
		return {
			performanceCompositeId: performanceCompositeId
		};
	},

	getReportParameters: function(format, reportId, singleSelectionOnly, zipFiles) {
		const sm = this.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one row to export report for.', 'No Rows Selected');
		}
		else {
			if (TCG.isBlank(reportId)) {
				reportId = '';
			}
			if (TCG.isBlank(zipFiles)) {
				zipFiles = false;
			}
			const params = {
				reportId: reportId,
				exportFormat: format,
				keepSubReportPageNumbers: true,
				zipFiles: zipFiles
			};
			if (sm.getCount() === 1) {
				params.performanceCompositePerformanceId = sm.getSelected().id;
				return params;
			}
			else {
				if (TCG.isTrue(singleSelectionOnly)) {
					TCG.showError('Please select only one row');
					return;
				}
				if (format !== 'PDF' && !zipFiles) {
					TCG.showError('Multiple reports can only be concatenated when rendering to pdf or zipped.');
					return;
				}
				const ids = [];
				const ut = sm.getSelections();

				for (let i = 0; i < ut.length; i++) {
					ids.push(ut[i].json.id);
				}
				params.performanceCompositePerformanceIds = ids;
				return params;
			}
		}
	},
	executeReport: function(format, reportId, zipFiles) {
		const params = this.getReportParameters(format, reportId, false, zipFiles);
		if (TCG.isNotBlank(params.performanceCompositePerformanceId)) {
			TCG.downloadFile('performanceCompositePerformanceReportDownload.json', params, this);
		}
		else {
			TCG.downloadFile('performanceCompositePerformanceReportListDownload.json', params, this);
		}
	},
	postReport: function() {
		const params = this.getReportParameters('PDF', '', true);
		const loader = new TCG.data.JsonLoader({
			waitTarget: this.grid,
			waitMsg: 'Posting...',
			params: params
		});
		loader.load('performanceCompositePerformanceReportPost.json');
	}
});
Ext.reg('composite-performance-workflow-grid', Clifton.performance.composite.CompositePerformanceWorkflowGrid);

Clifton.performance.composite.CompositeAccountPerformanceWorkflowGrid = Ext.extend(Clifton.performance.composite.CompositePerformanceBaseWorkflowGrid, {
	name: 'performanceCompositeInvestmentAccountPerformanceListFind',
	importTableName: 'PerformanceCompositeInvestmentAccountPerformance',
	importComponentName: 'Clifton.performance.composite.upload.CompositeAccountPerformanceUploadWindow',
	tableName: 'PerformanceCompositeInvestmentAccountPerformance',
	viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'],
	// NOTE: CAPS EXPORT VIEW NEEDS COLUMNS IN A SPECIFIC ORDER
	firstColumns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Account Number', width: 50, dataIndex: 'clientAccount.number', hidden: true, viewNames: ['CAPS Export - Actual Net', 'CAPS Export - Model Net'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Account'}, {name: 'CAPS Export - Model Net', label: 'Account'}], defaultHeader: 'Account Number', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'CAPS Identifier', width: 60, dataIndex: 'performanceCompositeInvestmentAccount.shortLabel', hidden: true, viewNames: ['CAPS Export - Actual Net', 'CAPS Export - Model Net'], filter: false, sortable: false},
		{header: '<div class="attach" style="WIDTH:16px">&nbsp;</div>', exportHeader: 'File Attachment(s)', width: 20, tooltip: 'File Attachment(s)', dataIndex: 'documentFileCount', filter: {searchFieldName: 'documentFileCount'}, type: 'int', useNull: true, align: 'center', viewNames: ['Default', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns']}
	],
	periodValueColumns: [
		{
			header: 'Positions On Date', dataIndex: 'coalescePositionsOnDate', width: 100, hidden: true,
			tooltip: 'The positions on date from the performance record.  If not defined, will display the account\'s performance inception date or the account\'s position date.'
		},
		{header: 'Investment Account', width: 150, dataIndex: 'clientAccount.label', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['Default']},
		{header: 'Account Name', width: 150, dataIndex: 'clientAccount.name', hidden: true, filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
		{header: 'Client Relationship', width: 150, dataIndex: 'clientAccount.businessClient.clientRelationship.name', filter: {searchFieldName: 'clientRelationshipName'}, hidden: true},
		{header: 'Service', width: 150, dataIndex: 'clientAccount.coalesceBusinessServiceGroupName', filter: {type: 'combo', searchFieldName: 'businessServiceOrParentId', displayField: 'nameExpanded', queryParam: 'nameExpanded', url: 'businessServiceListFind.json?excludeServiceLevelType=SERVICE_COMPONENT', listWidth: 500}, hidden: true},
		{header: 'Composite', width: 150, dataIndex: 'performanceCompositeInvestmentAccount.performanceComposite.label', filter: {type: 'combo', searchFieldName: 'performanceCompositeId', url: 'performanceCompositeListFind.json?rollupComposite=false', displayField: 'label'}},
		{header: 'Market Value', width: 60, dataIndex: 'periodEndMarketValue', type: 'currency', useNull: true, viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net', 'Monthly Returns', 'Quarterly Returns', 'Annual Returns', 'Period Ending Returns'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Mkt Value'}, {name: 'CAPS Export - Model Net', label: 'Total Mkt Value'}], defaultHeader: 'Market Value'},
		{header: 'Total Net Cash Flow', width: 60, dataIndex: 'periodTotalNetCashflow', type: 'currency', useNull: true, positiveInGreen: true, negativeInRed: true, summaryType: 'sum', viewNames: ['Default']},
		{header: 'Weighted Net Cash Flow', width: 60, dataIndex: 'periodWeightedNetCashflow', type: 'currency', useNull: true, positiveInGreen: true, negativeInRed: true, summaryType: 'sum', hidden: true, viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Net Flow'}, {name: 'CAPS Export - Model Net', label: 'Total Net Flow'}], defaultHeader: 'Weighted Net Cash Flow'},
		{header: 'Gain/Loss', width: 60, dataIndex: 'periodGainLoss', type: 'currency', useNull: true, positiveInGreen: true, negativeInRed: true, summaryType: 'sum', viewNames: ['Default', 'CAPS Export - Actual Net', 'CAPS Export - Model Net'], viewNameHeaders: [{name: 'CAPS Export - Actual Net', label: 'Total Net Flow'}, {name: 'CAPS Export - Model Net', label: 'Total Net Flow'}], defaultHeader: 'Gain/Loss'}
	],
	getAdditionalTopToolbarFilters: function(toolbar) {
		return [
			{
				fieldLabel: 'Composite Status', name: 'compositeStatus', hiddenName: 'compositeStatus', xtype: 'toolbar-combo', displayField: 'name', valueField: 'value', mode: 'local', value: 'Active',
				store: {
					xtype: 'arraystore',
					fields: ['name', 'value', 'description'],
					data: Clifton.performance.composite.PerformanceCompositeStatus
				}
			},
			{
				fieldLabel: 'Composite', xtype: 'toolbar-combo', name: 'performanceCompositeId', width: 180, url: 'performanceCompositeListFind.json', displayField: 'label', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					combo.resetStore(); // Requery each time to get status filters applied
					const compositeStatus = TCG.getChildByName(toolbar, 'compositeStatus');
					queryEvent.combo.store.setBaseParam('compositeStatus', compositeStatus.getValue());
				}
			},
			{fieldLabel: 'Client Acct', xtype: 'combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', linkedFilter: 'clientAccount.label', detailPageClass: 'Clifton.investment.account.AccountWindow'}
		];
	},
	getDetailClass: function() {
		return 'Clifton.performance.composite.account.performance.CompositeAccountPerformanceWindow';
	},
	getAdditionalLoadParams: function(firstLoad) {
		const toolbar = this.getTopToolbar();
		const dateField = TCG.getChildByName(toolbar, 'measureMonthDate');

		if (firstLoad && dateField && dateField.getValue() === '') {
			// Default to Last Month
			dateField.setValue((new Date()).getFirstDateOfMonth().add(Date.MONTH, -1));
		}

		if (dateField) {
			const measureMonthDate = TCG.parseDate(dateField.getValue(), 'm/d/Y');
			return {
				periodStartDate: (measureMonthDate === '') ? '' : (measureMonthDate.getFirstDateOfMonth()).format('m/d/Y'),
				performanceCompositeOrRollupId: TCG.getChildByName(toolbar, 'performanceCompositeId').getValue()
			};
		}
		return {
			performanceCompositeOrRollupId: TCG.getChildByName(toolbar, 'performanceCompositeId').getValue()
		};
	},
	getReportParams: function(format, reportId, singleSelectionOnly, zipFiles) {
		const sm = this.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one row to export report for.', 'No Rows Selected');
		}
		else {
			if (TCG.isBlank(reportId)) {
				reportId = '';
			}
			if (TCG.isBlank(zipFiles)) {
				zipFiles = false;
			}
			const params = {
				reportId: reportId,
				exportFormat: format,
				keepSubReportPageNumbers: true,
				zipFiles: zipFiles
			};
			if (sm.getCount() === 1) {
				params.performanceCompositeInvestmentAccountPerformanceId = sm.getSelected().id;
				return params;
			}
			else {
				if (TCG.isTrue(singleSelectionOnly)) {
					TCG.showError('Please select only one row');
					return;
				}
				if (format !== 'PDF' && !zipFiles) {
					TCG.showError('Multiple reports can only be concatenated when rendering to pdf or zipped.');
					return;
				}

				const ids = [];
				const ut = sm.getSelections();
				for (let i = 0; i < ut.length; i++) {
					ids.push(ut[i].json.id);
				}
				params.performanceCompositeInvestmentAccountPerformanceIds = ids;
				return params;
			}
		}
	},
	executeReport: function(format, reportId, zipFiles) {
		const params = this.getReportParams(format, reportId, false, zipFiles);
		let url = 'performanceCompositeInvestmentAccountPerformanceReportListDownload.json';
		if (TCG.isNotBlank(params.performanceCompositeInvestmentAccountPerformanceId)) {
			url = 'performanceCompositeInvestmentAccountPerformanceReportDownload.json';
		}
		TCG.downloadFile(url, params, this);
	},
	postReport: function() {
		const params = this.getReportParams('PDF', '', true);
		const loader = new TCG.data.JsonLoader({
			waitTarget: this.grid,
			waitMsg: 'Posting...',
			params: params
		});
		loader.load('performanceCompositeInvestmentAccountPerformanceReportPost.json');
	}
});
Ext.reg('composite-account-performance-workflow-grid', Clifton.performance.composite.CompositeAccountPerformanceWorkflowGrid);

Clifton.performance.composite.PerformanceCompositeStatus = [
	['Active', 'Active', 'Only active Performance Composites'],
	['Inactive', 'Inactive', 'Only inactive Performance Composites'],
	['Both', 'Both', 'Both active and inactive Performance Composites']
];

Clifton.performance.composite.registerBeforeTabChangeListener = function(tabPanel, newTab, currentTab) {
	if (currentTab) {
		const toolBar = currentTab.items.get(0).getTopToolbar();
		tabPanel.saveToolbarFilter(toolBar, 'measureMonthDate');
		tabPanel.saveToolbarFilter(toolBar, 'performanceCompositeId');
		tabPanel.saveToolbarFilter(toolBar, 'performanceFrequencyCombo');
		tabPanel.saveToolbarFilter(toolBar, 'investmentAccountId');
	}
};
Clifton.performance.composite.registerTabChangeListener = function(tabPanel, tab) {
	const toolBar = tab.items.get(0).getTopToolbar();
	// Restore applies to combos - this date field needs to be applied a little differently
	if (toolBar && this.savedToolbarFilters) {
		const o = TCG.getChildByName(toolBar, 'measureMonthDate');
		const s = this.savedToolbarFilters['measureMonthDate'];

		if (o) {
			if (s) {
				if (o.getValue() !== s.value) {
					o.setValue(s.value);
				}
			}
		}
	}
	tabPanel.restoreToolbarFilter(toolBar, 'performanceCompositeId');
	tabPanel.restoreToolbarFilter(toolBar, 'performanceFrequencyCombo');
	tabPanel.restoreToolbarFilter(toolBar, 'investmentAccountId');
};

Clifton.performance.composite.PerformanceFrequencyCombo = Ext.extend(TCG.form.ComboBox, {
	fieldLabel: 'Frequency', xtype: 'combo', name: 'displayPerformanceFrequency', width: 120, minListWidth: 150, mode: 'local', emptyText: 'Frequency', displayField: 'name', valueField: 'value',
	store: new Ext.data.ArrayStore({
		fields: ['value', 'name', 'description'],
		data: [
			['MONTHLY_RETURN', 'Monthly Returns', 'Display monthly returns.'],
			['QUARTERLY_RETURN', 'Quarterly Returns', 'Display quarterly returns.'],
			['ANNUAL_RETURN', 'Annual Returns', 'Display annual returns.']
		]
	}),
	listeners: {
		select: function(field) {
			const v = field.getValue();
			const gp = TCG.getParentByClass(field, Ext.Panel);

			if (TCG.isNull(gp.grid.filters.getFilter('performanceFrequency'))) {
				gp.grid.filters.addFilter({
					active: true,
					type: 'String',
					dataIndex: 'performanceFrequency'
				});
			}

			gp.grid.filters.getFilter('performanceFrequency').active = (v === 'QUARTERLY_RETURN' || v === 'ANNUAL_RETURN');
			gp.grid.filters.getFilter('performanceFrequency').setValue(v);
			gp.reload();
		}
	}
});
Ext.reg('performance-frequency-combo', Clifton.performance.composite.PerformanceFrequencyCombo);

Clifton.performance.composite.account.AccountPerformancePreview = Ext.extend(Ext.SplitButton, {
	text: 'Rebuild Performance',
	xtype: 'splitbutton',
	dailyPerformance: false,
	iconCls: 'run',
	tooltip: 'Rebuild this performance with <i>Calculate Account Daily and Monthly Base Performance</i> option.',
	handler: function(btn) {
		btn.executeRebuildHandler(btn, false, true, 'ALL');
	},

	menu: {
		items: [
			{
				text: 'Rebuild Performance Without Daily/Monthly Base',
				tooltip: 'Rebuild this performance without the <i>Calculate Account Daily and Monthly Base Performance</i> option. This recalculates period model net, period benchmarks, and all linked returns.',
				iconCls: 'run',
				scope: this,
				handler: function(btn) {
					const parent = TCG.getParentByClass(btn, Ext.SplitButton);
					parent.executeRebuildHandler(parent, false, false, 'ALL');
				}
			},
			{
				text: 'Rebuild Benchmark Returns Only',
				tooltip: 'Recalculates all daily, monthly and linked benchmark returns only..',
				iconCls: 'run',
				scope: this,
				handler: function(btn) {
					const parent = TCG.getParentByClass(btn, Ext.SplitButton);
					parent.executeRebuildHandler(parent, false, false, 'BENCHMARKS_ONLY');
				}
			},
			'-',
			{
				text: 'Preview Rebuild Performance',
				tooltip: 'Rebuild this performance into new window for preview and comparison.',
				iconCls: 'run',
				scope: this,
				handler: function(btn) {
					const parent = TCG.getParentByClass(btn, Ext.SplitButton);
					parent.executeRebuildHandler(parent, true, true, 'ALL');
				}
			}]
	},
	executeRebuildHandler: function(btn, preview, calculateAccountDailyMonthlyBase, metricProcessingType) {
		const fp = btn.scope;
		const f = fp.getForm();
		const formValues = f.formValues;

		if (TCG.isNull(formValues)) {
			return;
		}
		const performanceValues = (btn.dailyPerformance) ? formValues.performanceCompositeInvestmentAccountPerformance : formValues;
		const assignment = performanceValues.performanceCompositeInvestmentAccount;
		const accountingPeriod = performanceValues.accountingPeriod;
		if (TCG.isNull(assignment) || TCG.isNull(accountingPeriod)) {
			return;
		}
		const assignmentId = assignment.id;

		let measureDate;
		if (btn.dailyPerformance) {
			measureDate = TCG.parseDate(formValues.measureDate).format('m/d/Y');
		}

		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Loading Performance<marquee scrollamount="5" direction="right" width="15" scrolldelay="150">...</marquee>',
			success: function(response, opts) {
				if (this.isUseWaitMsg()) {
					this.getMsgTarget().unmask();
				}
				const result = Ext.decode(response.responseText);
				if (result.success) {
					try {
						const data = result.data;

						if (preview) {
							data['preview'] = true;
							let clazz = 'Clifton.performance.composite.account.performance.CompositeAccountPerformanceWindow';
							if (btn.dailyPerformance) {
								clazz = 'Clifton.performance.composite.account.performance.CompositeAccountDailyPerformanceWindow';
							}
							TCG.createComponent(clazz, {
								defaultData: data,
								openerCt: this
							});
						}
						else if (fp.reload) {
							fp.reload();
						}
					}
					finally {
						if (TCG.isNull(preview) || !preview) {
							fp.getForm().trackResetOnLoad = true;
						}
					}
				}
				else {
					this.onFailure();
					TCG.data.ErrorHandler.handleFailure(this, result);
				}
			}
		});
		let loadUrl = 'performanceCompositeAccountPerformanceRebuild.json?synchronous=true&';
		if (preview) {
			loadUrl = 'performanceCompositeAccountPerformancePreview.json?preview=true&';
			if (btn.dailyPerformance) {
				loadUrl = 'performanceCompositeAccountDailyPerformanceRunForMeasureDatePreview.json?preview=true&measureDate=' + measureDate + '&';
			}
		}
		loader.load(loadUrl + 'performanceCompositeInvestmentAccountId=' + assignmentId + '&toAccountingPeriod.id=' + accountingPeriod.id + '&calculateAccountDailyMonthlyBasePerformance=' + calculateAccountDailyMonthlyBase + '&metricProcessingType=' + metricProcessingType);
	}
});
Ext.reg('performance-account-performance-preview', Clifton.performance.composite.account.AccountPerformancePreview);

Clifton.performance.account.benchmark.BenchmarkGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'performanceSummaryBenchmarkListFind',
	instructions: 'Benchmarks associated with this client account had the following returns during selected performance summary period.',
	wikiPage: 'IT/Performance Benchmarks',
	getLoadParams: function() {
		return {summaryId: this.getWindow().getMainFormId()};
	},
	columns: [
		{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
		{header: 'Benchmark Name', width: 200, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'benchmarkName'}},
		{header: 'Benchmark Security', width: 200, hidden: true, dataIndex: 'referenceTwo.benchmarkSecurity.label', filter: {type: 'combo', searchFieldName: 'benchmarkSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Benchmark Interest Rate Index', hidden: true, width: 200, dataIndex: 'referenceTwo.benchmarkInterestRateIndex.label', filter: {type: 'combo', searchFieldName: 'benchmarkInterestRateIndexId', displayField: 'label', url: 'investmentInterestRateIndexListFind.json'}},
		{header: 'Order', width: 50, hidden: true, dataIndex: 'referenceTwo.benchmarkOrder', type: 'int'},

		{header: 'MTD (Calculated)', dataIndex: 'monthToDateReturn', type: 'currency', hidden: true, useNull: true, renderer: TCG.renderPercent},
		{header: 'MTD (Override)', dataIndex: 'monthToDateReturnOverride', type: 'currency', hidden: true, useNull: true, renderer: TCG.renderPercent},
		{
			header: 'MTD', width: 100, dataIndex: 'coalesceMonthToDateReturnOverride', type: 'currency', useNull: true, numberFormat: '0,000.0000 %',
			renderer: function(v, p, r) {
				return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['monthToDateReturnOverride']), r.data['monthToDateReturn'], '0,000.0000 %', 'Adjustment', true);
			}
		},
		{header: 'QTD', dataIndex: 'quarterToDateReturn', type: 'currency', useNull: true, renderer: TCG.renderPercent},
		{header: 'YTD', dataIndex: 'yearToDateReturn', type: 'currency', useNull: true, renderer: TCG.renderPercent},
		{header: 'ITD', dataIndex: 'inceptionToDateReturn', type: 'currency', useNull: true, renderer: TCG.renderPercent}
	],
	editor: {
		detailPageClass: 'Clifton.performance.account.benchmark.SummaryBenchmarkWindow',
		drillDownOnly: true,
		addEditButtons: function(t, grid) {
			const params = grid.getLoadParams();
			t.add({
				text: 'Load',
				tooltip: 'Load all missing benchmarks from client account benchmark setup.',
				iconCls: 'run',
				handler: function() {
					const loader = new TCG.data.JsonLoader({
						waitTarget: grid,
						timeout: 40000,
						params: params,
						scope: grid,
						conf: params,
						onLoad: function(record, conf) {
							grid.reload();
						}
					});
					loader.load('performanceSummaryBenchmarkListForSummaryProcess.json');
				}
			});
			t.add('-');
		}
	}
});
Ext.reg('performance-account-benchmark-grid', Clifton.performance.account.benchmark.BenchmarkGrid);
