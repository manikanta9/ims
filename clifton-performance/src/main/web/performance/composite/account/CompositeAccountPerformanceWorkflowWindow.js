Clifton.performance.composite.account.CompositeAccountPerformanceWorkflowWindow = Ext.extend(TCG.app.Window, {
	id: 'compositeAccountPerformanceWorkflowWindow',
	title: 'Account Performance Workflow',
	iconCls: 'chart-bar',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		listeners: {
			// keep selected month
			beforetabchange: Clifton.performance.composite.registerBeforeTabChangeListener,
			tabchange: Clifton.performance.composite.registerTabChangeListener
		},
		items: [
			{
				title: 'History',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					instructions: 'The following displays performance history across all workflow statuses.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.',
					showPerformanceReportButton: true,
					showDragAndDropAttachment: true
				}]
			},


			{
				title: 'Draft',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					workflowStateNames: 'Draft',
					drillDownOnly: false,
					showDragAndDropAttachment: true,

					transitionWorkflowStateList: [
						{stateName: 'Validated (Pre-Process Rules)', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Performance'}
					],

					instructions: 'The following performance are in Draft status and have not been approved yet. <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.'
				}]
			},


			{
				title: 'Pending',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					workflowStateNames: 'Pending Approval',
					showPerformanceReportButton: true,
					showDragAndDropAttachment: true,

					transitionWorkflowStateList: [
						{stateName: 'Preliminary', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Performance'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance'}
					],

					instructions: 'The following performance are in Pending status and have not had Preliminary approval by Accounting yet.  If you are a member of the Accounting team, you may select performance from the list below and approve them using the Approve button'
				}]
			},


			{
				title: 'Preliminary',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					workflowStateNames: 'Preliminary',
					showPerformanceReportButton: true,
					showPerformanceReportPreliminaryPostButton: true,
					showDragAndDropAttachment: true,

					transitionWorkflowStateList: [
						{stateName: 'Validated (Post-Process Rules)', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Performance'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance'}
					],

					instructions: 'The following performance are in Preliminary status and have not had final approval by Accounting yet.  If you are a member of the Accounting team, you may select performance from the list below and approve them using the Approve button'
				}]
			},

			{
				title: 'Approved',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					workflowStatusNames: 'Approved',
					showPerformanceReportButton: true,
					showPerformanceReportPostButton: true,
					showDragAndDropAttachment: true,

					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance'}
					],

					instructions: 'The following performance are in Approved status and have had final approval by Accounting.'
				}]
			},

			{
				title: 'Positions',
				items: [{
					xtype: 'accounting-liveHoldingsGrid',
					includeUnderlyingPrice: true,
					limitToPositions: true,
					reloadOnRender: false,
					columnOverrides: [
						{dataIndex: 'investmentSecurity.instrument.underlyingInstrument.label', hidden: false},
						{dataIndex: 'investmentSecurity.underlyingSecurity.label', hidden: false},
						{dataIndex: 'price', hidden: true},
						{dataIndex: 'exchangeRateToBase', hidden: true},
						{dataIndex: 'baseNotional', hidden: true},
						{dataIndex: 'baseMarketValue', hidden: true},
						{dataIndex: 'underlyingPrice', hidden: false, filter: false},
						{dataIndex: 'investmentSecurity.priceMultiplier', hidden: false}
					]
				}]
			},

			{
				title: 'Reporting',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					workflowStateNames: ['Pending Reporting', 'Reporting Complete'],
					showPerformanceReportButton: true,
					showPerformanceReportPostButton: false,
					showDragAndDropAttachment: true,

					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance - Returns back to Draft for Edits'},
						{stateName: 'Pending Reporting', iconCls: 'undo', buttonText: 'Pending Report', buttonTooltip: 'Undo selected performance as reporting completed'},
						{stateName: 'Reporting Complete', iconCls: 'www', buttonText: 'Post/Mark Complete', buttonTooltip: 'Post Report and Mark selected performance as reporting completed.  If the account is in the group GIPS Performance Posting: Private Funds Statement the report will be posted to Private Funds Statement.  Otherwise the report will be posted to Performance Reports.'}
					],
					instructions: 'The following performance have been fully approved and are eligible for quarter end reporting.',
					getTopToolbarFilters: function(toolbar) {
						return this.getAdditionalTopToolbarFilters(toolbar).concat([
							{name: 'performanceFrequencyCombo', xtype: 'performance-frequency-combo'},
							{fieldLabel: 'Include Reporting Complete', xtype: 'toolbar-checkbox', name: 'includeReportingComplete', checked: true}
						]);
					},
					applyDefaultFilters: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('accountingPeriod.endDate', {'after': TCG.getLastDateOfPreviousQuarter().add(Date.DAY, -1)});
						}
					},
					getWorkflowStateNames: function() {
						const toolbar = this.getTopToolbar();
						const includeCompleted = TCG.getChildByName(toolbar, 'includeReportingComplete');
						if (TCG.isTrue(includeCompleted.checked)) {
							return this.workflowStateNames;
						}
						return 'Pending Reporting';
					}
				}]
			},


			{
				title: 'Rule Violations',
				items: [{
					xtype: 'base-rule-violation-grid',
					instructions: 'The following rule violations have been recorded for "Performance Composite Account Rules" rule category. Defaults to last 7 days.',
					staticLoadParams: {categoryNameEquals: 'Performance Composite Account Rules'},
					includeRuleCategoryFilter: false
				}]
			},


			{
				title: 'Calculators',
				items: [{
					xtype: 'gridpanel',
					name: 'systemBeanListFind',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Name', width: 200, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'}},
						{header: 'Description', width: 300, dataIndex: 'description', hidden: true},
						{header: 'Type', width: 200, dataIndex: 'type.name', filter: {searchFieldName: 'typeId', type: 'combo', url: 'systemBeanTypeListFind.json?groupName=Performance Calculator'}},
						{header: 'Group', width: 150, dataIndex: 'type.group.name', hidden: true, filter: {searchFieldName: 'groupId', type: 'combo', url: 'systemBeanGroupListFind.json'}}
					],
					editor: {
						detailPageClass: 'Clifton.system.bean.BeanWindow'
					},
					getLoadParams: function(firstLoad) {
						return {groupName: 'Performance Calculator'};
					}

				}]
			},

			{
				title: 'Account Rollups',
				items: [{
					xtype: 'investment-account-relationship-grid',
					instructions: 'The following GIPS Performance Account Rollup relationships exist in the system.  These can be used for account performance reports to view the report as a rollup of the main and its related accounts',
					showDrillDowns: true,
					showClient: false,
					wikiPage: 'IT/GIPS+Performance+Account+Rollups',
					showHideAdditionalColumns: function() {
						this.showHideColumn('purpose.name', true);
						this.showHideColumn('accountAssetClass.label', true);
						this.showHideColumn('referenceTwo.issuingCompany.name', true);
						this.showHideColumn('referenceOne.workflowState.name', false);
					},
					getLoadParams: function() {
						return {purposeName: 'GIPS Performance Account Rollup'};
					},
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountRelationshipWindow',
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'mainAccountId', url: 'investmentAccountListFind.json?ourAccount=true', width: 200, listWidth: 400, emptyText: '< Select Main Account>', displayField: 'label'}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new relationship for selected account',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const mainAccount = TCG.getChildByName(toolBar, 'mainAccountId');
									const mainAccountId = mainAccount.getValue();
									if (TCG.isNotBlank(mainAccountId)) {
										TCG.showError('You must first select desired main account from the list.');
									}
									else {
										const clzName = this.detailPageClass;
										TCG.data.getDataPromiseUsingCaching('investmentAccountRelationshipPurposeByName.json?name=GIPS Performance Account Rollup', this, 'investment.account.relationship.purpose.GIPS Performance Account Rollup')
											.then(function(purpose) {
												const cmpId = TCG.getComponentId(clzName);
												TCG.createComponent(clzName, {
													id: cmpId,
													defaultData: {referenceOne: {'id': mainAccountId, 'label': mainAccount.getRawValue()}, purpose: {'id': purpose.id, 'name': purpose.name}},
													openerCt: gridPanel,
													defaultIconCls: gridPanel.getWindow().iconCls
												});
											});
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},

			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {
					align: 'stretch'
				},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 160,
						loadValidation: false, // using the form only to get background color/padding
						height: 280,
						buttonAlign: 'right',
						instructions: 'Use this section to (re)process multiple performance composites at one time. Select criteria below to filter which composites to (re)process.',
						items: [
							{
								xtype: 'columnpanel',
								columns: [
									{
										rows: [
											{
												fieldLabel: 'Composite Status', name: 'compositeStatus', hiddenName: 'compositeStatus', xtype: 'combo', displayField: 'name', valueField: 'value', mode: 'local', value: 'Active',
												store: {
													xtype: 'arraystore',
													fields: ['name', 'value', 'description'],
													data: Clifton.performance.composite.PerformanceCompositeStatus
												},
												qtip: 'Filter the Composite dropdown by status.'
											},
											{
												fieldLabel: 'Composite', name: 'performanceCompositeName', hiddenName: 'performanceCompositeId', xtype: 'combo', url: 'performanceCompositeListFind.json', mutuallyExclusiveFields: ['investmentAccountId', 'investmentAccountGroupId', 'performanceCompositeInvestmentAccountId'], detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow',
												requiredFields: ['compositeStatus'],
												listeners: {
													beforequery: function(queryEvent) {
														const combo = queryEvent.combo;
														const f = combo.getParentForm().getForm();
														combo.store.baseParams = {compositeStatus: f.findField('compositeStatus').getValue()};
													}
												}
											},
											{fieldLabel: 'Client Account Group', name: 'groupName', hiddenName: 'investmentAccountGroupId', xtype: 'combo', url: 'investmentAccountGroupListFind.json', mutuallyExclusiveFields: ['investmentAccountId', 'performanceCompositeId', 'performanceCompositeInvestmentAccountId'], detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
											{fieldLabel: 'Client Account', name: 'accountLabel', hiddenName: 'investmentAccountId', xtype: 'combo', url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', mutuallyExclusiveFields: ['investmentAccountGroupId', 'performanceCompositeId', 'performanceCompositeInvestmentAccountId'], detailPageClass: 'Clifton.investment.account.AccountWindow'},
											{fieldLabel: 'Assignment', name: 'performanceCompositeInvestmentAccountLabel', hiddenName: 'performanceCompositeInvestmentAccountId', xtype: 'combo', url: 'performanceCompositeInvestmentAccountListFind.json', displayField: 'label', mutuallyExclusiveFields: ['investmentAccountGroupId', 'performanceCompositeId'], detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow'},
											{fieldLabel: 'Process Period', name: 'toAccountingPeriod.label', hiddenName: 'toAccountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label', allowBlank: false},
											{
												fieldLabel: 'Start Period', name: 'fromAccountingPeriod.label', hiddenName: 'fromAccountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label', requiredFields: ['toAccountingPeriod.id'],
												qtip: 'Optional start date to re-process performance from instead of just the one process period.'
											}
										]
									},
									{
										rows: [
											{xtype: 'hidden', name: 'metricProcessingType', value: 'ALL'},
											{boxLabel: 'Calculate Account Daily and Monthly Base Performance', name: 'calculateAccountDailyMonthlyBasePerformance', xtype: 'checkbox', mutuallyExclusiveFields: ['metricProcessingType']},
											{
												boxLabel: 'Process Benchmarks Only (Existing Performance Only)', name: 'benchmarksOnly', submitValue: false, xtype: 'checkbox', mutuallyExclusiveFields: ['calculateAccountDailyMonthlyBasePerformance'],
												qtip: 'Will re-process benchmarks only for existing performance records.  Missing performance records will be skipped.',
												listeners: {
													check: function(obj, checked) {
														const metricProcessingType = (TCG.isTrue(checked) ? 'BENCHMARKS_ONLY' : 'ALL');
														const fp = TCG.getParentFormPanel(obj);
														fp.setFormValue('metricProcessingType', metricProcessingType);
													}
												}
											},
											{boxLabel: 'Process Composite Benchmarks', name: 'processCompositeBenchmarks', xtype: 'checkbox', requiredFields: ['performanceCompositeId', 'metricProcessingType'], qtip: 'If re-processing benchmarks only for all accounts in a composite, can optionally also recalculate the benchmarks on the composite itself.'},
											{
												boxLabel: 'Transition Account Performance To Draft Prior to Processing', name: 'transitionToDraftPriorToProcessing', xtype: 'checkbox',
												qtip: 'Prior to processing account performance for selected period(s) will first transition account performance to Draft.<br/><b>This should only be used if you are sure you want to re-process previously approved account performance for your selections.</b>'
											},
											{boxLabel: 'Restrict processing to require Composite, Account Group, or Account (uncheck to allow all)', name: 'restrictionRequired', xtype: 'checkbox', checked: true, qtip: '<b>Warning:</b> Should be unchecked ONLY when you want to create and process account performance across all Accounts.'}
										]
									}
								]
							}
						],

						validatePreview: function() {
							// Assignment is Required
							const assignment = this.getFormValue('performanceCompositeInvestmentAccountId', true);
							if (TCG.isBlank(assignment)) {
								TCG.showError('Assignment is required for previews');
								return false;
							}
							const processPeriod = this.getFormValue('toAccountingPeriod.id', true);
							if (TCG.isBlank(processPeriod)) {
								TCG.showError('Process Period is required for previews');
								return false;
							}
							return true;
						},

						validateLoadAll: function() {
							const restrictionRequired = this.getFormValue('restrictionRequired', true);
							if (TCG.isTrue(restrictionRequired)) {
								// All is Not Allowed - Must have Composite, Account Group, or Account selected
								const composite = this.getFormValue('performanceCompositeId', true);
								const accountGroup = this.getFormValue('investmentAccountGroupId', true);
								const account = this.getFormValue('investmentAccountId', true);
								const assignment = this.getFormValue('performanceCompositeInvestmentAccountId', true);
								if ((TCG.isNotBlank(composite)) || TCG.isNotBlank(accountGroup) || (TCG.isNotBlank(account)) || TCG.isNotBlank(assignment)) {
									return true;
								}
								TCG.showError('You have selected to restrict processing to a Composite, Account Group, Account, or Assignment.  Either uncheck the restriction checkbox or select at least one of the filters.');
								return false;
							}
							return true;
						},


						buttons: [
							{
								text: 'Preview Account Performance',
								iconCls: 'preview',
								width: 150,
								tooltip: 'Preview account performance metrics only',
								handler: function() {
									const owner = this.findParentByType('formpanel');

									if (TCG.isTrue(owner.validatePreview())) {
										const form = owner.getForm();
										form.submit(Ext.applyIf({
											url: 'performanceCompositeAccountPerformancePreview.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												const data = action.result.data;
												TCG.createComponent('Clifton.performance.composite.account.performance.CompositeAccountPerformanceWindow', {
													defaultData: data,
													openerCt: this
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								}
							},
							{
								text: 'Process Performance',
								iconCls: 'run',
								width: 150,
								tooltip: 'Calculate performance metrics ',
								handler: function() {
									const owner = this.findParentByType('formpanel');

									if (TCG.isTrue(owner.validateLoadAll())) {
										const form = owner.getForm();
										form.submit(Ext.applyIf({
											url: 'performanceCompositeAccountPerformanceRebuild.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Process Success', action.result.data, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								}
							}]
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'ACCOUNT-PERFORMANCE-RUN',
						instantRunner: true,
						instructions: 'The following Performance Composites are being (re)processed right now.',
						title: 'Current Performance Composites',
						flex: 1
					}
				]
			}
		]
	}]
});
