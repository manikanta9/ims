Clifton.performance.composite.account.performance.CompositeAccountDailyPerformanceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Composite Account Performance Daily Run',
	iconCls: 'chart-bar',
	width: 1100,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Daily Performance',
				items: [{
					xtype: 'formpanel',
					url: 'performanceCompositeInvestmentAccountDailyPerformance.json',
					labelWidth: 175,
					defaults: {anchor: '-15'},
					addToolbarButtons: function(t, formPanel) {
						if (TCG.getValue('preview', formPanel.getForm().formValues)) {
							return;
						}
						t.add('->');
						t.add('-');
						t.add({xtype: 'performance-account-performance-preview', scope: formPanel, dailyPerformance: true});
					},
					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('performanceCompositeInvestmentAccountDailyPerformance.json?id=' + w.getMainFormId(), this), true);
					},

					items: [
						{fieldLabel: 'Account Performance', name: 'performanceCompositeInvestmentAccountPerformance.label', xtype: 'linkfield', detailIdField: 'performanceCompositeInvestmentAccountPerformance.id', detailPageClass: 'Clifton.performance.composite.account.performance.CompositeAccountPerformanceWindow'},
						{fieldLabel: 'Measure Date', name: 'measureDate', xtype: 'datefield'},
						{
							fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
							defaults: {
								xtype: 'displayfield',
								flex: 1
							},
							items: [
								{value: 'Calculated Value'},
								{value: 'Override Value'}
							]
						},
						{
							fieldLabel: 'Gain / Loss', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'gainLoss'},
								{name: 'gainLossOverride', readOnly: false, submitValue: true}
							]
						},
						{
							fieldLabel: 'Position Value', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'positionValue'},
								{name: 'positionValueOverride', readOnly: false, submitValue: true}
							]
						},
						{
							fieldLabel: 'Net Cash Flow', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'netCashFlow'},
								{name: 'netCashFlowOverride', readOnly: false, submitValue: true}
							]
						},
						{
							fieldLabel: 'MOC Net Cash Flow', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: false,
								submitValue: true,
								flex: 1
							},
							items: [
								{name: 'marketOnCloseNetCashFlow', qtip: 'Can be used to move cash flow to end of day, which will be excluded from the return calculations, but included as a cash flow for the day/month.'},
								{xtype: 'label', html: '&nbsp;'}
							]
						},
						{
							fieldLabel: 'Weighted Net Cash Flow', xtype: 'compositefield',
							defaults: {
								xtype: 'currencyfield',
								readOnly: true,
								submitValue: false,
								flex: 1
							},
							items: [
								{name: 'weightedNetCashFlow'},
								{name: 'weightedNetCashFlowOverride', readOnly: false, submitValue: true}
							]
						},
						{fieldLabel: 'Gross Return', xtype: 'currencyfield', decimalPrecision: 4, name: 'grossReturn'},
						{fieldLabel: 'Benchmark Return', xtype: 'currencyfield', decimalPrecision: 4, name: 'benchmarkReturn'},
						{fieldLabel: 'Secondary Benchmark Return', xtype: 'currencyfield', decimalPrecision: 4, name: 'secondaryBenchmarkReturn'},
						{fieldLabel: 'Third Benchmark Return', xtype: 'currencyfield', decimalPrecision: 4, name: 'thirdBenchmarkReturn'}
					]
				}]
			},

			{
				title: 'Client Positions',
				xtype: 'accounting-positionAnalysisGrid',
				pageSize: 1000,
				reloadOnRender: true,
				viewNames: undefined,
				columnOverrides: [
					{dataIndex: 'accountingTransaction.clientInvestmentAccount.number', hidden: true},
					{dataIndex: 'accountingTransaction.clientInvestmentAccount.name', hidden: true},
					{dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', hidden: true},
					{dataIndex: 'dailyGainLossBase', hidden: false, width: 140}
				],

				getLoadParams: function(firstLoad) {
					const params = {};
					if (firstLoad) {
						const measureDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('measureDate'));
						if (measureDate) {
							this.setFilterValue('positionDate', {'on': measureDate});
						}
						else {
							return false;
						}
					}

					const toolbar = this.getTopToolbar();
					const instrumentGroupId = TCG.getChildByName(toolbar, 'investmentGroupId').getValue();
					if (TCG.isNotBlank(instrumentGroupId)) {
						params.investmentGroupId = instrumentGroupId;
					}
					params.clientInvestmentAccountId = this.getWindow().getMainFormPanel().getFormValue('performanceCompositeInvestmentAccountPerformance.clientAccount.id');
					return params;
				},
				getTopToolbarFilters: function(toolbar) {
					return [{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'}];
				}
			},

			{
				title: 'Transactions',
				items: [{
					xtype: 'accounting-transactionGrid',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'GL Account Group', xtype: 'toolbar-combo', name: 'accountingAccountGroupId', width: 180, url: 'accountingAccountGroupListFind.json'},
							{fieldLabel: 'Journal Type', xtype: 'toolbar-combo', name: 'accountingJournalTypeId', width: 180, url: 'accountingJournalTypeList.json', loadAll: true}
						];
					},
					columnOverrides: [
						{dataIndex: 'id', hidden: true},
						{dataIndex: 'parentTransaction.id', hidden: true},
						{dataIndex: 'clientInvestmentAccount.label', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const measureDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('measureDate'));
							if (measureDate) {
								this.setFilterValue('transactionDate', {'on': measureDate});
							}
							else {
								return false;
							}
						}
						const toolbar = this.getTopToolbar();
						const group = TCG.getChildByName(toolbar, 'accountingAccountGroupId');
						const params = {readUncommittedRequested: true};
						if (TCG.isNotBlank(group.getValue())) {
							params.accountingAccountGroupId = group.getValue();
						}

						const journalType = TCG.getChildByName(toolbar, 'accountingJournalTypeId');
						if (TCG.isNotBlank(journalType.getValue())) {
							params.journalTypeId = journalType.getValue();
						}
						params.clientInvestmentAccountId = this.getWindow().getMainFormPanel().getFormValue('performanceCompositeInvestmentAccountPerformance.clientAccount.id');
						return params;
					}

				}]
			}
		]
	}]
});
