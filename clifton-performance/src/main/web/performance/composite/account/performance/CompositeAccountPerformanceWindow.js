Clifton.performance.composite.account.performance.CompositeAccountPerformanceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Composite Account Performance',
	width: 1300,
	height: 660,

	gridTabWithCount: 'Portal Files', // show portal file count in  the tab

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: undefined,
		reloadOnChange: true,
		items: [
			{
				title: 'Performance',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'PerformanceCompositeInvestmentAccountPerformance',
					reloadFormPanelAfterTransition: true,
					addAdditionalButtons: function(t) {
						const formPanel = TCG.getChildrenByClass(TCG.getParentTabPanel(t), TCG.form.FormPanel)[0];
						t.add({
							text: 'Rebuild Positions',
							xtype: 'button',
							iconCls: 'run',
							tooltip: 'Rebuild Client Positions (Analytics -> Client Positions -> Administration Tab)',
							width: 120,
							scope: formPanel,
							handler: function(btn) {
								const fp = this;
								let startSnapshotDate = TCG.getChildByName(fp, 'accountingPeriod.startDate').getValue();
								//Adjust start snapshot to include previous months weekday since this day is used in daily runs
								startSnapshotDate = TCG.getPreviousWeekday(TCG.parseDate(startSnapshotDate, 'm/d/Y')).format('m/d/Y');
								const endSnapshotDate = TCG.getChildByName(fp, 'accountingPeriod.endDate').getValue().format('m/d/Y');
								const params = {
									'clientAccountId': fp.getForm().formValues.clientAccount.id,
									'startSnapshotDate': startSnapshotDate,
									'endSnapshotDate': endSnapshotDate,
									'doNotThrowExceptionForFutureSnapshots': true,
									'synchronous': true
								};
								if (TCG.isNull(params)) {
									TCG.showError('Performance entity must be saved before rebuilding positions.');
									return;
								}
								const loader = new TCG.data.JsonLoader({
									waitTarget: fp,
									timeout: 40000,
									waitMsg: 'Processing...',
									params: params,
									conf: params,
									onLoad: function(record, conf) {
										TCG.createComponent('Clifton.core.StatusWindow', {
											defaultData: {status: record}
										});
									}
								});
								loader.load('accountingPositionDailyRebuild.json?UI_SOURCE=CompositeAccountPerformance');
							}
						});
						t.add('-');

						t.add({xtype: 'performance-account-performance-preview', scope: formPanel});
						t.add('-');

						const menu = new Ext.menu.Menu();
						menu.add({
							text: '2 Decimals (Rounded)',
							xtype: 'menucheckitem',
							group: 'accountPerformanceView',
							checked: true,
							handler: function() {
								formPanel.formatNumberFields(2);
							}
						});
						menu.add({
							text: '4 Decimals (Rounded)',
							xtype: 'menucheckitem',
							group: 'accountPerformanceView',
							handler: function() {
								formPanel.formatNumberFields(4);
							}
						});
						menu.add({
							text: 'All Decimals',
							xtype: 'menucheckitem',
							group: 'accountPerformanceView',
							handler: function() {
								formPanel.formatNumberFields(15);
							}
						});

						t.add({
							text: 'Views',
							iconCls: 'views',
							menu: menu
						});
						t.add('-');
					}
				},
				items: [{
					xtype: 'formpanel',
					url: 'performanceCompositeInvestmentAccountPerformance.json',
					labelWidth: 175,
					reload: function() {
						const w = this.getWindow();
						// Don't use for Preview Window
						if (TCG.isNumber(w.getMainFormId())) {
							this.getForm().setValues(TCG.data.getData('performanceCompositeInvestmentAccountPerformance.json?id=' + w.getMainFormId(), this), true);
						}
					},
					getWarningMessage: function(form) {
						return Clifton.rule.violation.getRuleViolationWarningMessage('performance', form.formValues);
					},
					getLoadParams: function(firstLoad) {
						const win = this.getWindow();
						return {id: win.params.id, requestedMaxDepth: 5};
					},
					listeners: {
						afterrender: function(fp) {
							if (fp.getWindow().defaultData && fp.getWindow().defaultData.preview) {
								fp.setBaseFields(fp);
							}
						},
						afterload: function(fp) {
							fp.setBaseFields(fp);

							// Don't allow edits to the account / assignment / period
							if (TCG.isNumber(fp.getWindow().getMainFormId())) {
								fp.setReadOnlyField('accountingPeriod.label', true);
								fp.setReadOnlyField('clientAccount.label', true);
								fp.setReadOnlyField('performanceCompositeInvestmentAccount.labelWithoutAccount', true);
							}
						}
					},
					formatNumberFields: function(precision) {
						TCG.each(TCG.getChildrenByClass(TCG.getParentTabPanel(this), TCG.form.CurrencyField), function(e) {
							if (TCG.isTrue(e.readOnly)) {
								e.decimalPrecision = precision;
								e.setValue(this.getFormValue(e.name));
								e.originalValue = e.getValue();
							}
						}, this);
					},
					setBaseFields: function(fp) {
						const investmentAccountId = fp.getFormValue('clientAccount.id');
						const accountingPeriodId = fp.getFormValue('accountingPeriod.id');
						// Don't use for Preview Window
						if (TCG.isNumber(investmentAccountId) && TCG.isNumber(accountingPeriodId)) {
							TCG.data.getDataPromise('accountingPeriodClosingByPeriodAndAccount.json?accountingPeriodId=' + accountingPeriodId + '&investmentAccountId=' + investmentAccountId, this)
									.then(function(accountingPeriodClosing) {
										if (accountingPeriodClosing) {
											fp.setFormValue('managementFee', accountingPeriodClosing.periodProjectedRevenue, true);
										}
									});
						}

						const itd = TCG.getChildByName(fp, 'itd');

						const dayDifference = TCG.dayDifference(TCG.getChildByName(fp, 'accountingPeriod.endDate').getValue(), new Date(TCG.getChildByName(fp, 'startDate').getValue()));
						if (dayDifference && dayDifference > 365) {
							itd.setText('Annualized ' + itd.initialConfig.html);
						}
						else {
							itd.setText('Cumulative ' + itd.initialConfig.html);
						}
					},
					items: [
						{fieldLabel: 'Accounting Period', name: 'accountingPeriod.label', hiddenName: 'accountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label', detailPageClass: 'Clifton.accounting.period.PeriodWindow'},
						{name: 'accountingPeriod.startDate', xtype: 'datefield', hidden: true, submitValue: false},
						{name: 'accountingPeriod.endDate', xtype: 'datefield', hidden: true, submitValue: false},
						{fieldLabel: 'Investment Account', name: 'clientAccount.label', hiddenName: 'clientAccount.id', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow', submitValue: false},
						{
							fieldLabel: 'Composite Assignment', name: 'performanceCompositeInvestmentAccount.labelWithoutAccount', hiddenName: 'performanceCompositeInvestmentAccount.id', xtype: 'combo', displayField: 'labelWithoutAccount', requiredFields: ['clientAccount.id', 'accountingPeriod.id'], url: 'performanceCompositeInvestmentAccountListFind.json', detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow',
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const form = combo.getParentForm().getForm();

								const accountId = form.findField('clientAccount.id').getValue();
								combo.store.setBaseParam('investmentAccountId', accountId);

								const accountingPeriodId = form.findField('accountingPeriod.id').getValue();
								combo.store.setBaseParam('accountingPeriodId', accountingPeriodId);
							}
						},

						{
							xtype: 'columnpanel',
							columns: [
								{
									config: {columnWidth: 0.33},
									rows: [
										{fieldLabel: 'Management Fee', name: 'managementFee', xtype: 'currencyfield', submitValue: false, readOnly: true, qtip: 'Used for calculating actual net of fee. Management fee is treated as gain/loss on the last day of month for actual net calculation'},
										{fieldLabel: 'Management Fee (Override)', name: 'managementFeeOverride', xtype: 'currencyfield', qtip: 'Used for calculating actual net of fee. Management fee is treated as gain/loss on the last day of month for actual net calculation'}
									]
								},
								{
									config: {columnWidth: 0.33},
									rows: [
										{fieldLabel: 'Positions On Date', xtype: 'datefield', name: 'positionsOnDate', qtip: 'Date when we started recording performance for this period.  Most cases this is empty and inferred through the accounting period'},
										{fieldLabel: 'Market Value', name: 'periodEndMarketValue', xtype: 'currencyfield'}
									]
								},
								{
									config: {columnWidth: 0.33},
									rows: [
										{fieldLabel: 'Total Net Cash Flow', name: 'periodTotalNetCashflow', xtype: 'currencyfield'},
										{fieldLabel: 'Weighted Net Cash Flow', name: 'periodWeightedNetCashflow', xtype: 'currencyfield'}
									]
								}
							]
						},

						{html: '<hr/>', xtype: 'label'},
						{
							xtype: 'formtable',
							columns: 10,
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 2,
								readOnly: true,
								submitValue: false,
								style: {marginLeft: '5px', textAlign: 'right'},
								width: '88%'
							},
							items: [
								{cellWidth: '100px', xtype: 'label'},
								{html: 'Gross Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Actual Net Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Model Net Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Gross STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Actual Net STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Model Net STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Gross Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Actual Net Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Model Net Sharpe', xtype: 'label', style: 'text-align: right'},

								{html: 'Current Period:', xtype: 'label'},
								{name: 'periodGrossReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodNetReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodModelNetReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{xtype: 'label', colspan: 6},

								{html: 'QTD:', xtype: 'label'},
								{name: 'quarterToDateGrossReturn'},
								{name: 'quarterToDateNetReturn'},
								{name: 'quarterToDateModelNetReturn'},
								{xtype: 'label', colspan: 6},

								{html: 'YTD:', xtype: 'label'},
								{name: 'yearToDateGrossReturn'},
								{name: 'yearToDateNetReturn'},
								{name: 'yearToDateModelNetReturn'},
								{xtype: 'label', colspan: 6},

								{html: '1 Year:', xtype: 'label'},
								{name: 'oneYearGrossReturn'},
								{name: 'oneYearNetReturn'},
								{name: 'oneYearModelNetReturn'},
								{name: 'oneYearGrossStandardDeviation'},
								{name: 'oneYearNetStandardDeviation'},
								{name: 'oneYearModelNetStandardDeviation'},
								{name: 'oneYearGrossSharpeRatio'},
								{name: 'oneYearNetSharpeRatio'},
								{name: 'oneYearModelNetSharpeRatio'},

								{html: '3 Year:', xtype: 'label'},
								{name: 'threeYearGrossReturn'},
								{name: 'threeYearNetReturn'},
								{name: 'threeYearModelNetReturn'},
								{name: 'threeYearGrossStandardDeviation'},
								{name: 'threeYearNetStandardDeviation'},
								{name: 'threeYearModelNetStandardDeviation'},
								{name: 'threeYearGrossSharpeRatio'},
								{name: 'threeYearNetSharpeRatio'},
								{name: 'threeYearModelNetSharpeRatio'},

								{html: '5 Year:', xtype: 'label'},
								{name: 'fiveYearGrossReturn'},
								{name: 'fiveYearNetReturn'},
								{name: 'fiveYearModelNetReturn'},
								{name: 'fiveYearGrossStandardDeviation'},
								{name: 'fiveYearNetStandardDeviation'},
								{name: 'fiveYearModelNetStandardDeviation'},
								{name: 'fiveYearGrossSharpeRatio'},
								{name: 'fiveYearNetSharpeRatio'},
								{name: 'fiveYearModelNetSharpeRatio'},

								{html: '10 Year:', xtype: 'label'},
								{name: 'tenYearGrossReturn'},
								{name: 'tenYearNetReturn'},
								{name: 'tenYearModelNetReturn'},
								{name: 'tenYearGrossStandardDeviation'},
								{name: 'tenYearNetStandardDeviation'},
								{name: 'tenYearModelNetStandardDeviation'},
								{name: 'tenYearGrossSharpeRatio'},
								{name: 'tenYearNetSharpeRatio'},
								{name: 'tenYearModelNetSharpeRatio'},

								{html: 'ITD:', name: 'itd', xtype: 'label'},
								{name: 'inceptionToDateGrossReturn'},
								{name: 'inceptionToDateNetReturn'},
								{name: 'inceptionToDateModelNetReturn'},
								{name: 'inceptionToDateGrossStandardDeviation'},
								{name: 'inceptionToDateNetStandardDeviation'},
								{name: 'inceptionToDateModelNetStandardDeviation'},
								{name: 'inceptionToDateGrossSharpeRatio'},
								{name: 'inceptionToDateNetSharpeRatio'},
								{name: 'inceptionToDateModelNetSharpeRatio'},

								{html: 'Inception Date:', xtype: 'label'},
								{name: 'startDate', xtype: 'displayfield', type: 'date', style: 'marginLeft: 5px; text-align: left;'},
								{xtype: 'label', colspan: 7}
							]
						},
						{
							title: 'File Attachments',
							xtype: 'documentFileGrid-simple',
							definitionName: 'PerformanceCompositeAccountPerformanceAttachments'
						}
					]
				}]
			},


			{
				title: 'Benchmarks',
				items: [{
					xtype: 'formpanel',
					url: 'performanceCompositeInvestmentAccountPerformance.json',
					labelWidth: 140,
					getLoadURL: function() {
						const w = this.getWindow();
						if (w.defaultData && w.defaultData.preview) {
							w.defaultDataIsReal = true;
							return false;
						}

						if (this.url && w && w.params) {
							return this.url;
						}
						return false;
					},
					getLoadParams: function(firstLoad) {
						const win = this.getWindow();
						return {id: win.params.id, requestedMaxDepth: 5};
					},
					listeners: {
						afterload: function(fp, isClosing) {
							const itd = TCG.getChildByName(fp, 'itd');
							const oneYearBenchmarkReturn = TCG.getChildByName(fp, 'oneYearBenchmarkReturn');
							if (oneYearBenchmarkReturn && TCG.isNotBlank(oneYearBenchmarkReturn.getValue()) && oneYearBenchmarkReturn.getValue() !== 0) {
								itd.setText('Annualized ' + itd.initialConfig.html);
							}
							else {
								itd.setText('Cumulative ' + itd.initialConfig.html);
							}
						}
					},

					items: [
						{fieldLabel: 'Risk Free Security', name: 'performanceCompositeInvestmentAccount.performanceComposite.riskFreeSecurity.label', detailIdField: 'performanceCompositeInvestmentAccount.performanceComposite.riskFreeSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitDetailField: false},
						{fieldLabel: 'Benchmark', name: 'performanceCompositeInvestmentAccount.coalesceBenchmarkSecurity.label', detailIdField: 'performanceCompositeInvestmentAccount.coalesceBenchmarkSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitDetailField: false},
						{fieldLabel: 'Secondary Benchmark', name: 'performanceCompositeInvestmentAccount.coalesceSecondaryBenchmarkSecurity.label', detailIdField: 'performanceCompositeInvestmentAccount.coalesceSecondaryBenchmarkSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitDetailField: false},
						{fieldLabel: 'Third Benchmark', name: 'performanceCompositeInvestmentAccount.coalesceThirdBenchmarkSecurity.label', detailIdField: 'performanceCompositeInvestmentAccount.coalesceThirdBenchmarkSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', submitDetailField: false},
						{xtype: 'label', html: '<hr/>'},

						{
							xtype: 'formtable',
							columns: 11,
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 2,
								submitValue: false,
								readOnly: true,
								style: {marginLeft: '0px', textAlign: 'right'},
								width: '88%'
							},
							items: [
								{cellWidth: '100px', xtype: 'label'},
								{html: 'Risk Free Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Benchmark', xtype: 'label', style: 'text-align: right'},
								{html: 'Secondary Benchmark', xtype: 'label', style: 'text-align: right'},
								{html: 'Third Benchmark', xtype: 'label', style: 'text-align: right'},
								{html: 'Benchmark STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Secondary Benchmark STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Third Benchmark STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Benchmark Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Secondary Benchmark Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Third Benchmark Sharpe', xtype: 'label', style: 'text-align: right'},

								{html: 'Current Period:', xtype: 'label'},
								{name: 'periodRiskFreeSecurityReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodBenchmarkReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodSecondaryBenchmarkReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodThirdBenchmarkReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{xtype: 'label', colspan: 7},

								{html: 'QTD:', xtype: 'label'},
								{name: 'quarterToDateRiskFreeSecurityReturn'},
								{name: 'quarterToDateBenchmarkReturn'},
								{name: 'quarterToDateSecondaryBenchmarkReturn'},
								{name: 'quarterToDateThirdBenchmarkReturn'},
								{xtype: 'label', colspan: 7},

								{html: 'YTD:', xtype: 'label'},
								{name: 'yearToDateRiskFreeSecurityReturn'},
								{name: 'yearToDateBenchmarkReturn'},
								{name: 'yearToDateSecondaryBenchmarkReturn'},
								{name: 'yearToDateThirdBenchmarkReturn'},
								{xtype: 'label', colspan: 7},

								{html: '1 Year:', xtype: 'label'},
								{name: 'oneYearRiskFreeSecurityReturn'},
								{name: 'oneYearBenchmarkReturn'},
								{name: 'oneYearSecondaryBenchmarkReturn'},
								{name: 'oneYearThirdBenchmarkReturn'},

								{name: 'oneYearBenchmarkStandardDeviation'},
								{name: 'oneYearSecondaryBenchmarkStandardDeviation'},
								{name: 'oneYearThirdBenchmarkStandardDeviation'},
								{name: 'oneYearBenchmarkSharpeRatio'},
								{name: 'oneYearSecondaryBenchmarkSharpeRatio'},
								{name: 'oneYearThirdBenchmarkSharpeRatio'},

								{html: '3 Year:', xtype: 'label'},
								{name: 'threeYearRiskFreeSecurityReturn'},
								{name: 'threeYearBenchmarkReturn'},
								{name: 'threeYearSecondaryBenchmarkReturn'},
								{name: 'threeYearThirdBenchmarkReturn'},
								{name: 'threeYearBenchmarkStandardDeviation'},
								{name: 'threeYearSecondaryBenchmarkStandardDeviation'},
								{name: 'threeYearThirdBenchmarkStandardDeviation'},
								{name: 'threeYearBenchmarkSharpeRatio'},
								{name: 'threeYearSecondaryBenchmarkSharpeRatio'},
								{name: 'threeYearThirdBenchmarkSharpeRatio'},

								{html: '5 Year:', xtype: 'label'},
								{name: 'fiveYearRiskFreeSecurityReturn'},
								{name: 'fiveYearBenchmarkReturn'},
								{name: 'fiveYearSecondaryBenchmarkReturn'},
								{name: 'fiveYearThirdBenchmarkReturn'},
								{name: 'fiveYearBenchmarkStandardDeviation'},
								{name: 'fiveYearSecondaryBenchmarkStandardDeviation'},
								{name: 'fiveYearThirdBenchmarkStandardDeviation'},
								{name: 'fiveYearBenchmarkSharpeRatio'},
								{name: 'fiveYearSecondaryBenchmarkSharpeRatio'},
								{name: 'fiveYearThirdBenchmarkSharpeRatio'},

								{html: '10 Year:', xtype: 'label'},
								{name: 'tenYearRiskFreeSecurityReturn'},
								{name: 'tenYearBenchmarkReturn'},
								{name: 'tenYearSecondaryBenchmarkReturn'},
								{name: 'tenYearThirdBenchmarkReturn'},
								{name: 'tenYearBenchmarkStandardDeviation'},
								{name: 'tenYearSecondaryBenchmarkStandardDeviation'},
								{name: 'tenYearThirdBenchmarkStandardDeviation'},
								{name: 'tenYearBenchmarkSharpeRatio'},
								{name: 'tenYearSecondaryBenchmarkSharpeRatio'},
								{name: 'tenYearThirdBenchmarkSharpeRatio'},

								{html: 'ITD:', name: 'itd', xtype: 'label'},
								{name: 'inceptionToDateRiskFreeSecurityReturn'},
								{name: 'inceptionToDateBenchmarkReturn'},
								{name: 'inceptionToDateSecondaryBenchmarkReturn'},
								{name: 'inceptionToDateThirdBenchmarkReturn'},
								{name: 'inceptionToDateBenchmarkStandardDeviation'},
								{name: 'inceptionToDateSecondaryBenchmarkStandardDeviation'},
								{name: 'inceptionToDateThirdBenchmarkStandardDeviation'},
								{name: 'inceptionToDateBenchmarkSharpeRatio'},
								{name: 'inceptionToDateSecondaryBenchmarkSharpeRatio'},
								{name: 'inceptionToDateThirdBenchmarkSharpeRatio'},

								{html: 'Inception Date:', xtype: 'label'},
								{name: 'startDate', xtype: 'displayfield', type: 'date', style: 'marginLeft: 5px; text-align: left;'},
								{xtype: 'label', colspan: 11}
							]
						}
					]
				}]
			},


			{
				title: 'Daily Returns',
				items: [{
					name: 'performanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance',
					importTableName: 'PerformanceCompositeInvestmentAccountDailyPerformance',
					tableName: 'PerformanceCompositeInvestmentAccountDailyPerformance',
					xtype: 'gridpanel',
					instructions: 'Monthly performance is calculated by compounding daily performances of individual runs.',
					viewNames: ['Default', 'Proprietary Funds', 'Options (unfunded)'],
					appendStandardColumns: false,
					getLoadURL: function() {
						const w = this.getWindow();
						if (w.defaultData && w.defaultData.preview) {
							return 'performanceCompositeAccountDailyPerformanceRunPreview.json?preview=true&performanceCompositeInvestmentAccountId=' + w.defaultData.performanceCompositeInvestmentAccount.id + '&toAccountingPeriod.id=' +
									w.defaultData.accountingPeriod.id;
						}
						return 'performanceCompositeInvestmentAccountDailyPerformanceByMonthlyPerformance.json';
					},
					getLoadParams: function() {
						return {
							performanceCompositeInvestmentAccountPerformanceId: this.getWindow().getMainFormId()
						};
					},
					isPagingEnabled: function() {
						return false;
					},
					pageSize: 50,
					columns: [
						{header: 'ID', width: 30, dataIndex: 'id', hidden: true},
						{header: 'PerformanceCompositeInvestmentAccountPerformanceId', width: 30, dataIndex: 'performanceCompositeInvestmentAccountPerformance.id', hidden: true},
						{header: 'Measure Date', width: 75, dataIndex: 'measureDate', allViews: true},

						{header: 'Position Value (Calculated)', width: 90, dataIndex: 'positionValue', type: 'currency', hidden: true},
						{
							header: 'Position Value', width: 90, dataIndex: 'coalescePositionValuePositionValueOverride', type: 'currency', allViews: true,
							renderer: function(v, metaData, r) {
								return TCG.renderAdjustedAmount(v, v !== r.data.positionValue, r.data.positionValue, '0,000.00', 'Adjustment', true);
							}
						},

						{header: 'Net Cash Flow (Calculated)', width: 90, dataIndex: 'netCashFlow', type: 'currency', hidden: true, useNull: true},
						{header: 'Net Cash Flow (Override)', width: 90, dataIndex: 'netCashFlowOverride', type: 'currency', hidden: true, useNull: true},
						{header: 'MOC Net Cash Flow', width: 90, dataIndex: 'marketOnCloseNetCashFlow', type: 'currency', hidden: true, useNull: true},
						{
							header: 'Net Cash Flow', width: 90, dataIndex: 'totalNetCashFlow', type: 'currency', summaryType: 'sum', useNull: true, viewNames: ['Default', 'Proprietary Funds'],
							renderer: function(v, metaData, r) {
								let showAdjusted = false;
								const format = '0,000.00';
								let qtip = '<table><tr><td>Calculated Value: </td><td align="right">' + Ext.util.Format.number(r.data.netCashFlow, format) + '</td></tr>';
								if (TCG.isNotBlank(r.data.netCashFlowOverride) && TCG.isNotEquals(r.data.netCashFlow, r.data.netCashFlowOverride)) {
									showAdjusted = true;
									qtip += '<tr><td>Adjustment: </td><td align="right">' + TCG.renderAmount(r.data.netCashFlowOverride - r.data.netCashFlow, true, format) + '</td></tr>';
								}
								if (TCG.isNotBlank(r.data.marketOnCloseNetCashFlow)) {
									showAdjusted = true;
									qtip += '<tr><td>MOC Value: </td><td align="right">' + TCG.renderAmount(r.data.marketOnCloseNetCashFlow, true, format) + '</td></tr>';
								}
								qtip += '</table>';
								if (showAdjusted) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip(qtip);
								}
								return TCG.numberFormat(v, format);
							},
							summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return TCG.renderAmount(v, true, '0,000.00');
								}
							}
						},
						{header: 'Weighted Net Cash Flow (Calculated)', width: 90, dataIndex: 'weightedNetCashFlow', type: 'currency', hidden: true},
						{
							header: 'Weighted Net Cash Flow', width: 90, dataIndex: 'coalesceWeightedNetCashFlowWeightedNetCashFlowOverride', type: 'currency', summaryType: 'sum', viewNames: ['Default', 'Proprietary Funds'],
							renderer: function(v, metaData, r) {
								return TCG.renderAdjustedAmount(v, v !== r.data.weightedNetCashFlow, r.data.weightedNetCashFlow, '0,000.00', 'Adjustment', true);
							},
							summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return TCG.renderAmount(v, true, '0,000.00');
								}
							}
						},
						{header: 'Gain Loss (Calculated)', width: 90, dataIndex: 'gainLoss', type: 'currency', hidden: true, useNull: true},
						{
							header: 'Gain Loss', width: 90, dataIndex: 'coalesceGainLossGainLossOverride', type: 'currency', summaryType: 'sum', viewNames: ['Default', 'Options (unfunded)'], useNull: true,
							renderer: function(v, metaData, r) {
								return TCG.renderAdjustedAmount(v, v !== r.data.gainLoss, r.data.gainLoss, '0,000.00', 'Adjustment', true);
							},
							summaryRenderer: function(v) {
								if (TCG.isNotBlank(v)) {
									return TCG.renderAmount(v, true, '0,000.00');
								}
							}
						},

						{header: 'Gross Return', width: 85, dataIndex: 'grossReturn', type: 'currency', renderer: TCG.renderPercent, viewNames: ['Default', 'Options (unfunded)'], useNull: true},
						{header: 'Benchmark Return', width: 85, dataIndex: 'benchmarkReturn', type: 'currency', renderer: TCG.renderPercent, viewNames: ['Default', 'Options (unfunded)'], useNull: true},
						{header: 'Secondary Benchmark Return', width: 85, dataIndex: 'secondaryBenchmarkReturn', type: 'currency', renderer: TCG.renderPercent, viewNames: ['Default', 'Options (unfunded)'], useNull: true},
						{header: 'Third Benchmark Return', width: 85, dataIndex: 'thirdBenchmarkReturn', type: 'currency', renderer: TCG.renderPercent, viewNames: ['Default', 'Options (unfunded)'], useNull: true}
					],
					plugins: {
						ptype: 'gridsummary'
					},
					listeners: {
						afterRender: function() {
							const defaultPerformanceView = this.getWindow().getMainFormPanel().getFormValue('performanceCompositeInvestmentAccount.coalesceAccountPerformanceCalculatorBean');
							this.setDefaultView(defaultPerformanceView && this.viewNames.indexOf(defaultPerformanceView) > -1 ? defaultPerformanceView : 'Default');
						}
					},
					editor: {
						allowToDeleteMultiple: true,
						detailPageClass: 'Clifton.performance.composite.account.performance.CompositeAccountDailyPerformanceWindow',
						getDetailPageId: function(gridPanel, row) {
							if (gridPanel.getWindow().defaultData && gridPanel.getWindow().defaultData.preview) {
								return undefined;
							}
							return row.id;
						},
						getDeleteURL: function() {
							return 'performanceCompositeInvestmentAccountDailyPerformanceDelete.json';
						},
						getDefaultData: function(gridPanel, row) {
							const defaults = {};
							if (gridPanel.getWindow().getMainForm().formValues !== undefined) {
								const values = gridPanel.getWindow().getMainForm().formValues;
								defaults.performanceCompositeInvestmentAccountPerformance = {};
								defaults.performanceCompositeInvestmentAccountPerformance.id = values.id;
								defaults.performanceCompositeInvestmentAccountPerformance.name = values.name;
								defaults.performanceCompositeInvestmentAccountPerformance.label = values.label;
							}
							return defaults;
						}
					}
				}]
			},


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'PerformanceCompositeInvestmentAccountPerformance'
				}]
			},


			{
				title: 'Composite Assignments',
				items: [{
					name: 'performanceCompositeInvestmentAccountListFind',
					xtype: 'gridpanel',
					instructions: 'This account is assigned to the following composites for this period.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'performanceComposite.id', hidden: true},
						{header: 'Composite Name', width: 200, dataIndex: 'performanceComposite.name'},
						{header: 'Description', width: 250, dataIndex: 'performanceComposite.description', hidden: true},
						{header: 'Composite Code', width: 80, dataIndex: 'performanceComposite.code'},
						{header: 'Currency', width: 60, dataIndex: 'performanceComposite.currency.name'},
						{header: 'Benchmark', width: 120, dataIndex: 'performanceComposite.benchmarkSecurity.label'},
						{header: 'Rollup', width: 60, dataIndex: 'performanceComposite.rollupComposite', type: 'boolean', hidden: true},
						{header: 'Actual', width: 60, dataIndex: 'performanceComposite.actualNetReturnUsed', hidden: true, type: 'boolean', title: 'Specifies whether performance for this composite uses Actual (true) or Model (false) Net Returns.'},
						{header: 'Start Date', width: 60, dataIndex: 'performanceComposite.startDate', hidden: true},
						{header: 'End Date', width: 60, dataIndex: 'performanceComposite.endDate', hidden: true},
						{header: 'Active', width: 50, dataIndex: 'performanceComposite.active', type: 'boolean', hidden: true}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('performanceComposite.active', true);
						}
						return {investmentAccountId: TCG.getChildByName(this.getWindow().getMainFormPanel(), 'clientAccount.label').getValue()};
					},
					editor: {
						detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow'
					}
				}]
			},


			{
				title: 'Client Positions',
				xtype: 'accounting-positionAnalysisGrid',
				pageSize: 1000,
				reloadOnRender: true,
				viewNames: undefined,
				columnOverrides: [
					{dataIndex: 'accountingTransaction.clientInvestmentAccount.number', hidden: true},
					{dataIndex: 'accountingTransaction.clientInvestmentAccount.name', hidden: true},
					{dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', hidden: true},
					{dataIndex: 'positionDate', width: 80, hidden: false},
					{dataIndex: 'dailyGainLossBase', width: 130, hidden: false},
					{dataIndex: 'accrualBase', hidden: false, width: 90},
					{header: 'Underlying Price', dataIndex: 'underlyingPrice', width: 110, type: 'float', useNull: true, hidden: false, sortable: false, filter: false},
					{header: 'Multiplier', dataIndex: 'accountingTransaction.investmentSecurity.priceMultiplier', width: 70, type: 'float', useNull: true, hidden: false, filter: {searchFieldName: 'priceMultiplier'}}
				],

				getLoadParams: function(firstLoad) {
					const params = {};
					if (firstLoad) {
						const measureDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('accountingPeriod.endDate'));
						if (measureDate) {
							this.setFilterValue('positionDate', {'on': measureDate});
						}
						else {
							return false;
						}
					}

					const toolbar = this.getTopToolbar();
					const instrumentGroupId = TCG.getChildByName(toolbar, 'investmentGroupId').getValue();
					if (TCG.isNotBlank(instrumentGroupId)) {
						params.investmentGroupId = instrumentGroupId;
					}
					params.clientInvestmentAccountId = this.getWindow().getMainFormPanel().getFormValue('clientAccount.id');
					params.includeUnderlyingPrice = true;
					return params;
				},
				getTopToolbarFilters: function(toolbar) {
					return [{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'}];
				}
			},


			{
				title: 'Transactions',
				items: [{
					xtype: 'accounting-transactionGrid',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'GL Account Group', xtype: 'toolbar-combo', name: 'accountingAccountGroupId', width: 180, url: 'accountingAccountGroupListFind.json'},
							{fieldLabel: 'Journal Type', xtype: 'toolbar-combo', name: 'accountingJournalTypeId', width: 180, url: 'accountingJournalTypeList.json', loadAll: true}
						];
					},
					columnOverrides: [
						{dataIndex: 'id', hidden: true},
						{dataIndex: 'parentTransaction.id', hidden: true},
						{dataIndex: 'clientInvestmentAccount.label', hidden: true}
					],

					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							const startDate = TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('accountingPeriod.startDate'));
							if (startDate) {
								this.setFilterValue('transactionDate', {'after': startDate.add(Date.DAY, -1), 'before': TCG.parseDate(this.getWindow().getMainFormPanel().getFormValue('accountingPeriod.endDate')).add(Date.DAY, 1)});
							}
							else {
								return false;
							}
						}
						const toolbar = this.getTopToolbar();
						const group = TCG.getChildByName(toolbar, 'accountingAccountGroupId');
						const params = {readUncommittedRequested: true};
						if (TCG.isNotBlank(group.getValue())) {
							params.accountingAccountGroupId = group.getValue();
						}

						const journalType = TCG.getChildByName(toolbar, 'accountingJournalTypeId');
						if (TCG.isNotBlank(journalType.getValue())) {
							params.journalTypeId = journalType.getValue();
						}
						params.clientInvestmentAccountId = this.getWindow().getMainFormPanel().getFormValue('clientAccount.id');
						return params;
					}

				}]
			},


			{
				title: 'Portal Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-grid-for-source-entity',
						sourceTableName: 'PerformanceCompositeInvestmentAccountPerformance'
					}
				]
			}
		]
	}]
});
