Clifton.performance.composite.assignment.CompositeAccountWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Composite Account',
	width: 950,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Assignment',
				items: [{
					xtype: 'formpanel',
					url: 'performanceCompositeInvestmentAccount.json',
					labelWidth: 170,
					instructions: 'Assigns an investment account to a composite for a specified date range. Leave End Date blank for no expiration assignment.',
					items: [
						{fieldLabel: 'Performance Composite', name: 'performanceComposite.name', hiddenName: 'performanceComposite.id', xtype: 'combo', url: 'performanceCompositeListFind.json?rollupComposite=false', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow'},
						{fieldLabel: 'Investment Account', name: 'investmentAccount.label', hiddenName: 'investmentAccount.id', xtype: 'combo', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true', detailPageClass: 'Clifton.investment.account.AccountWindow'},
						{fieldLabel: 'Account Calculator', beanName: 'coalesceAccountPerformanceCalculatorBean', xtype: 'system-bean-combo', groupName: 'Performance Calculator'},
						{xtype: 'sectionheaderfield', header: 'Benchmark Overrides', fieldLabel: ''},
						{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Secondary Benchmark', name: 'secondaryBenchmarkSecurity.label', hiddenName: 'secondaryBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Third Benchmark', name: 'thirdBenchmarkSecurity.label', hiddenName: 'thirdBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{xtype: 'sectionheaderfield', header: 'Date Range', fieldLabel: ''},
						{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
						{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},

						{xtype: 'sectionheaderfield', header: 'Account Reporting Options'},
						{xtype: 'label', html: 'All account reports are posted to Performance Reports file category.  If the account is in GIPS Performance Posting: Private Funds Statement account group, the report will be posted to Private Funds Statement file group.'},
						{
							fieldLabel: 'Report', name: 'coalesceAccountReport.name', hiddenName: 'coalesceAccountReport.id', xtype: 'combo', url: 'reportListFind.json?templateName=Performance Strategy Report',
							qtip: 'The report to use when viewing/posting this account\'s performance reports.'
						},
						{
							fieldLabel: 'Reporting Frequency', name: 'coalesceAccountReportFrequency', hiddenName: 'coalesceAccountReportFrequency', xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore',
								data: Clifton.performance.PerformanceReportingFrequencies
							},
							qtip: 'The frequency this account performance\'s reports are delivered.  Used to determine when the performance for the account should move to reporting workflow states.'
						},
						{
							xtype: 'checkbox', name: 'coalesceAccountReportAppendAttachments', fieldLabel: '', boxLabel: 'Append File Attachment(s) (if any) to the Account Performance Report',
							qtip: 'If checked, then any attachments that exist on the Performance Account Performance will be appending to the system generated report prior to viewing and posting.  Primarily used for Fund Investors.'
						}
					]
				}]
			},


			{
				title: 'Performance',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					instructions: 'The following displays performance history for the account.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.',
					columnOverrides: [{dataIndex: 'clientAccount.label', hidden: true}, {dataIndex: 'performanceCompositeInvestmentAccount.performanceComposite.label', hidden: true}, {dataIndex: 'periodSecondaryBenchmarkReturn', hidden: true}, {dataIndex: 'periodThirdBenchmarkReturn', hidden: true}, {dataIndex: 'periodTotalNetCashflow', hidden: true}, {dataIndex: 'periodGainLoss', hidden: true}],
					getTopToolbarFilters: function(toolbar) {
						//no room for toolbar filters on this screen
					},
					getAdditionalLoadParams: function(firstLoad) {
						return {investmentAccountId: this.getWindow().getMainFormPanel().getFormValue('investmentAccount.id')};
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					tableName: 'PerformanceCompositeInvestmentAccount',
					columnOverrides: [{dataIndex: 'endDate', hidden: false}],
					defaultActiveFilter: false,
					applyAdditionalLoadParams: function(firstLoad, loadParams) {
						if (firstLoad) {
							// Default to endDate = null (active) or endDate >= 2 quarter ends back (i.e. ended during previous quarter)
							this.setFilterValue('endDate', {'after': TCG.getFirstDateOfPreviousQuarter().add(Date.DAY, -1)});
						}
						return loadParams;
					}
				}]
			}
		]
	}]
});
