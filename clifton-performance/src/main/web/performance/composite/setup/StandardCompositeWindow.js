Clifton.performance.composite.setup.StandardCompositeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Composite',
	width: 1100,
	height: 750,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				tbar: [
					{
						text: 'Update Model Fee',
						iconCls: 'numbers',
						tooltip: 'Allows fixing incorrectly entered current model fee on this page, or starting a new model fee as of given date (keeping existing fee in history). See history tab for additional information.',
						handler: function() {
							const f = this.ownerCt.ownerCt.items.get(0);
							const w = f.getWindow();
							const id = w.getMainFormId();
							const cmpId = TCG.getComponentId('Clifton.performance.composite.setup.CompositeHistoryEntryWindow', 'Composite History ' + id);
							TCG.createComponent('Clifton.performance.composite.setup.CompositeHistoryEntryWindow', {
								id: cmpId,
								openerCt: f,
								defaultData: {performanceComposite: w.getMainForm().formValues}
							});
						}
					}
				],

				items: [{
					xtype: 'formpanel',
					url: 'performanceComposite.json',
					labelWidth: 170,
					instructions: 'A Composite is a group of discretionary portfolios that collectively represent a particular investment strategy or objective.',
					listeners: {
						afterload: function(panel) {
							// Once the composite is created, in order to update the model fee must use specific model fee entry windows
							panel.disableField('modelFee');
						}
					},

					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('performanceComposite.json?id=' + w.getMainFormId(), this), true);
						this.fireEvent('afterload', this);
					},

					items: [
						{fieldLabel: 'Composite Name', name: 'name'},
						{fieldLabel: 'Composite Short Name', name: 'compositeShortName'},

						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{fieldLabel: 'Composite Code', name: 'code', xtype: 'textfield'},
									{fieldLabel: 'Currency', name: 'currency.name', hiddenName: 'currency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
								]
							}, {
								rows: [
									{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', allowBlank: false},
									{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'}
								]
							}]
						},
						{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Secondary Benchmark', name: 'secondaryBenchmarkSecurity.label', hiddenName: 'secondaryBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Third Benchmark', name: 'thirdBenchmarkSecurity.label', hiddenName: 'thirdBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Risk Free Security', name: 'riskFreeSecurity.label', hiddenName: 'riskFreeSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{
							fieldLabel: 'Model Fee', xtype: 'currencyfield', decimalPrecision: 4, name: 'modelFee', allowBlank: false,
							qtip: 'Please use the Update Model Fee button at the top of the form to edit model fee.'
						},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							boxLabel: 'Composite uses Actual Net Returns (uncheck to use Model Net Returns)', name: 'actualNetReturnUsed', xtype: 'checkbox',
							qtip: 'Note: Both values are calculated and available, however this option will drive what return is displayed by default for reporting.'
						},
						{
							boxLabel: 'Include Accounts with Partial Periods', name: 'includePartialPeriodAccounts', xtype: 'checkbox',
							qtip: 'If checked, accounts that had positions on for only a portion of the period are still included in the composite performance.  Leave unchecked to only include accounts will a full period of positions on.'
						},

						{xtype: 'sectionheaderfield', header: 'Composite Reporting Options'},
						{
							fieldLabel: 'Composite Report', name: 'compositeReport.name', hiddenName: 'compositeReport.id', xtype: 'combo', url: 'reportListFind.json?templateName=Performance Strategy Report',
							qtip: 'The report to use when viewing/posting the report for this composite.'
						},
						{
							fieldLabel: 'Reporting Frequency', name: 'compositeReportFrequency', hiddenName: 'compositeReportFrequency', xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore',
								data: Clifton.performance.PerformanceReportingFrequencies
							},
							qtip: 'The frequency this composite performance report is delivered.  Used to determine when the performance for the composite should move to reporting workflow states.'
						},

						{xtype: 'sectionheaderfield', header: 'Default Account Options'},
						{
							fieldLabel: 'Account Performance Calculator', beanName: 'accountPerformanceCalculatorBean', xtype: 'system-bean-combo', groupName: 'Performance Calculator', allowBlank: false,
							qtip: 'The default calculator all accounts assigned to this composite will use.  This can be overridden for an individual account by going to the account assignment for this composite..'
						},
						{
							fieldLabel: 'Account Report', name: 'accountReport.name', hiddenName: 'accountReport.id', allowBlank: false, xtype: 'combo', url: 'reportListFind.json?templateName=Performance Strategy Report',
							qtip: 'The default report to use when viewing/posting the account performance reports for this composite.  This can be overridden for an individual account by going to the account assignment for this composite.'
						},
						{
							fieldLabel: 'Account Reporting Frequency', name: 'accountReportFrequency', hiddenName: 'accountReportFrequency', allowBlank: false, xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore',
								data: Clifton.performance.PerformanceReportingFrequencies
							},
							qtip: 'The default frequency account performance reports under this composite are delivered.  Used to determine when the performance for the account should move to reporting workflow states.'
						},
						{
							xtype: 'checkbox', name: 'accountReportAppendAttachments', fieldLabel: '', boxLabel: 'Append File Attachment(s) (if any) to the Account Performance Report',
							qtip: 'If checked, then any attachments that exist on the Performance Account Performance will be appending to the system generated report prior to viewing and posting.  Primarily used for Fund Investors.'
						}
					]
				}]
			},

			{
				title: 'History',
				items: [{
					xtype: 'gridpanel',
					name: 'performanceCompositeHistoryListForComposite',
					instructions: 'The following composite model fee history exists for this composite.  If a model fee has no changes over time, then no history will be available.  If there is at least one change, full history will be available here.  You can add new model fees with a new start date from here. Existing model fee will apply until new Start Date - 1.  If additional historical edits are necessary, you need to remove history from the more recent first and then can enter new fees.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Start Date', width: 50, dataIndex: 'startDate'},
						{header: 'End Date', width: 50, dataIndex: 'endDate'},
						{header: 'Model Fee', width: 75, dataIndex: 'modelFee', type: 'percent', useNull: true, numberFormat: '0,000.0000'}
					],
					getLoadParams: function(firstLoad) {
						return {performanceCompositeId: this.getWindow().getMainFormId()};
					},
					editor: {
						deleteURL: 'performanceCompositeHistoryDelete.json',
						reloadGridAfterDelete: true,
						addToolbarAddButton: function(toolBar) {
							toolBar.add({
								text: 'Update Model Fee',
								iconCls: 'numbers',
								tooltip: 'Update Model Fee',
								handler: () => {
									const gridPanel = this.getGridPanel();
									const win = gridPanel.getWindow();
									const id = win.getMainFormId();
									const cmpId = TCG.getComponentId('Clifton.performance.composite.setup.CompositeHistoryEntryWindow', 'Composite History ' + id);
									TCG.createComponent('Clifton.performance.composite.setup.CompositeHistoryEntryWindow', {
										id: cmpId,
										defaultData: {performanceComposite: win.getMainForm().formValues}
									});
								}
							});
							toolBar.add('-');
						}

					}

				}]
			},

			{
				title: 'Performance',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					columnOverrides: [{dataIndex: 'performanceComposite.name', hidden: true}, {dataIndex: 'periodSecondaryBenchmarkReturn', hidden: true}, {dataIndex: 'periodThirdBenchmarkReturn', hidden: true}],
					instructions: 'The following displays performance history for selected composite.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.',
					viewNames: undefined, // No Views
					getTopToolbarFilters: function(toolbar) {
						//no room for toolbar filters on this screen
					},
					getAdditionalLoadParams: function(firstLoad) {
						return {performanceCompositeId: this.getWindow().getMainFormId()};
					}
				}]
			},

			{
				title: 'Account Assignments',
				items: [{
					name: 'performanceCompositeInvestmentAccountListFind',
					xtype: 'gridpanel',
					importTableName: 'PerformanceCompositeInvestmentAccount',
					instructions: 'The following investment accounts are assigned to this performance composite.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Account', width: 250, dataIndex: 'investmentAccount.label', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}},

						{header: 'Benchmark', width: 120, dataIndex: 'benchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'benchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
						{header: 'Secondary Benchmark', width: 120, dataIndex: 'secondaryBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'secondaryBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
						{header: 'Third Benchmark', width: 120, dataIndex: 'thirdBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'thirdBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},

						{header: 'Start Date', width: 60, dataIndex: 'startDate'},
						{header: 'End Date', width: 60, dataIndex: 'endDate'},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
						{
							header: 'Account Report', width: 100, hidden: true, dataIndex: 'coalesceAccountReport.name', filter: {type: 'combo', searchFieldName: 'coalesceAccountReportId', url: 'reportListFind.json?templateName=Performance Strategy Report'},
							tooltip: 'The default report to use when viewing/posting the account performance reports for this composite.  This can be overridden for an individual account by going to the account assignment for this composite.'
						},
						{
							header: 'Account Report Frequency', hidden: true, width: 80, dataIndex: 'coalesceAccountReportFrequency', filter: {type: 'list', options: Clifton.performance.PerformanceReportingFrequencies},
							tooltip: 'The default frequency account performance reports under this composite are delivered.  Used to determine when the performance for the account should move to reporting workflow states.'
						},
						{
							header: 'Account Report Append Attachment(s)', hidden: true, width: 80, dataIndex: 'coalesceAccountReportAppendAttachments', type: 'boolean',
							tooltip: 'If checked, then any attachments that exist on the Performance Account Performance will be appending to the system generated report prior to viewing and posting.  Primarily used for Fund Investors.'
						}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						return {performanceCompositeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow',
						getDefaultData: function(gridPanel, row) {
							return {performanceComposite: gridPanel.getWindow().getMainForm().formValues};
						}
					}
				}]
			},

			{
				title: 'Account Performance',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					groupField: 'clientAccount.label',
					columnOverrides: [{dataIndex: 'clientAccount.label', hidden: true}, {dataIndex: 'performanceCompositeInvestmentAccount.performanceComposite.name', hidden: true}, {dataIndex: 'periodSecondaryBenchmarkReturn', hidden: true}, {dataIndex: 'periodThirdBenchmarkReturn', hidden: true}, {dataIndex: 'periodTotalNetCashflow', hidden: true}, {dataIndex: 'periodGainLoss', hidden: true}],
					instructions: 'The following displays performance history for all accounts in the selected composite.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.',
					viewNames: ['Default', 'CAPS Export'],
					// One view of CAPS Export would update to CAPS Export - Actual Net or CAPS Export - Model Net based on composite option
					switchToView: function(viewName, cancelReload) {
						let adjustedViewName = viewName;
						if (viewName === 'CAPS Export') {
							const useActualNet = this.getWindow().getMainFormPanel().getFormValue('actualNetReturnUsed');
							if (TCG.isTrue(useActualNet)) {
								adjustedViewName = 'CAPS Export - Actual Net';
							}
							else {
								adjustedViewName = 'CAPS Export - Model Net';
							}
						}
						TCG.grid.GridPanel.prototype.switchToView.call(this, adjustedViewName, cancelReload);
					},
					getTopToolbarFilters: function(toolbar) {
						//no room for toolbar filters on this screen
					},
					getAdditionalLoadParams: function(firstLoad) {
						return {performanceCompositeId: this.getWindow().getMainFormId()};
					}
				}]
			},

			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					columnOverrides: [{dataIndex: 'endDate', hidden: false}],
					tableName: 'PerformanceComposite',
					defaultActiveFilter: false,
					applyAdditionalLoadParams: function(firstLoad, loadParams) {
						if (firstLoad) {
							// Default to endDate = null (active) or endDate >= 2 quarter ends back (i.e. ended during previous quarter)
							this.setFilterValue('endDate', {'after': TCG.getFirstDateOfPreviousQuarter().add(Date.DAY, -1)});
						}
						return loadParams;
					}
				}]
			},

			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'PerformanceComposite'
				}]
			}
		]
	}]
});
