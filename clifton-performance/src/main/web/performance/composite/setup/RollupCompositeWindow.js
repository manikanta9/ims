Clifton.performance.composite.setup.RollupCompositeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Rollup Performance Composite',
	width: 950,
	height: 750,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'performanceComposite.json',
					labelWidth: 150,
					instructions: 'A Rollup Composite is a grouping of one or more performance composites.',
					items: [
						{xtype: 'hidden', name: 'rollupComposite', value: 'true'},
						{fieldLabel: 'Composite Name', name: 'name'},
						{fieldLabel: 'Composite Short Name', name: 'compositeShortName'},
						{
							xtype: 'columnpanel',
							columns: [{
								rows: [
									{fieldLabel: 'Composite Code', name: 'code', xtype: 'textfield'},
									{fieldLabel: 'Currency', name: 'currency.name', hiddenName: 'currency.id', xtype: 'combo', url: 'investmentSecurityListFind.json?currency=true', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
								]
							}, {
								rows: [
									{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield', readOnly: true},
									{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield', readOnly: true}
								]
							}]
						},
						{fieldLabel: 'Benchmark', name: 'benchmarkSecurity.label', hiddenName: 'benchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Secondary Benchmark', name: 'secondaryBenchmarkSecurity.label', hiddenName: 'secondaryBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Third Benchmark', name: 'thirdBenchmarkSecurity.label', hiddenName: 'thirdBenchmarkSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Risk Free Security', name: 'riskFreeSecurity.label', hiddenName: 'riskFreeSecurity.id', xtype: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true&active=true', displayField: 'label', disableAddNewItem: true, detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							boxLabel: 'Composite uses Actual Net Returns (uncheck to use Model Net Returns)', name: 'actualNetReturnUsed', xtype: 'checkbox',
							qtip: 'Rollup Composites use the underlying composite model fee. Note: Both values are calculated and available, however this option will drive what return is displayed by default for reporting.'
						},
						{
							boxLabel: 'Include Accounts with Partial Periods', name: 'includePartialPeriodAccounts', xtype: 'checkbox',
							qtip: 'If checked, accounts that had positions on for only a portion of the period are still included in the composite performance.  Leave unchecked to only include accounts will a full period of positions on.'
						},
						{xtype: 'sectionheaderfield', header: 'Composite Reporting Options'},
						{
							fieldLabel: 'Composite Report', name: 'compositeReport.name', hiddenName: 'compositeReport.id', xtype: 'combo', url: 'reportListFind.json?templateName=Performance Strategy Report',
							qtip: 'The report to use when viewing/posting the report for this composite.'
						},
						{
							fieldLabel: 'Reporting Frequency', name: 'compositeReportFrequency', hiddenName: 'compositeReportFrequency', xtype: 'combo', mode: 'local',
							store: {
								xtype: 'arraystore',
								data: Clifton.performance.PerformanceReportingFrequencies
							},
							qtip: 'The frequency this composite performance report is delivered.  Used to determine when the performance for the composite should move to reporting workflow states.'
						},
						{
							xtype: 'formgrid-scroll',
							title: 'Child Performance Composites',
							storeRoot: 'childCompositeList',
							dtoClass: 'com.clifton.performance.composite.setup.PerformanceComposite',
							collapsible: false,
							viewConfig: {forceFit: true, emptyText: 'This Rollup Composite currently has no child composites', deferEmptyText: false},
							columnsConfig: [
								{header: 'Performance Composite', dataIndex: 'label', idDataIndex: 'id', editor: {xtype: 'combo', url: 'performanceCompositeListFind.json?rollupComposite=false', displayField: 'label', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow'}}
							]
						}
					]
				}]
			},


			{
				title: 'Performance',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					instructions: 'The following displays performance history for the selected composite.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.',
					columnOverrides: [{dataIndex: 'performanceComposite.name', hidden: true}, {dataIndex: 'periodSecondaryBenchmarkReturn', hidden: true}, {dataIndex: 'periodThirdBenchmarkReturn', hidden: true}],
					viewNames: undefined, // No Views
					getTopToolbarFilters: function(toolbar) {
						//no room for toolbar filters on this screen
					},
					getAdditionalLoadParams: function(firstLoad) {
						return {performanceCompositeId: this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Account Assignments',
				items: [{
					name: 'performanceCompositeInvestmentAccountListFind',
					xtype: 'gridpanel',
					importTableName: 'PerformanceCompositeInvestmentAccount',
					instructions: 'The following investment accounts are assigned children of this rollup performance composite.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Account', width: 250, dataIndex: 'investmentAccount.label', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}},
						{header: 'Start Date', width: 60, dataIndex: 'startDate'},
						{header: 'End Date', width: 60, dataIndex: 'endDate'},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('active', true);
						}
						return {rollupPerformanceCompositeId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Notes',
				items: [{
					xtype: 'document-system-note-grid',
					columnOverrides: [{dataIndex: 'endDate', hidden: false}],
					tableName: 'PerformanceComposite',
					defaultActiveFilter: false,
					applyAdditionalLoadParams: function(firstLoad, loadParams) {
						if (firstLoad) {
							// Default to endDate = null (active) or endDate >= 2 quarter ends back (i.e. ended during previous quarter)
							this.setFilterValue('endDate', {'after': TCG.getFirstDateOfPreviousQuarter().add(Date.DAY, -1)});
						}
						return loadParams;
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'PerformanceComposite'
				}]
			}
		]
	}]
});
