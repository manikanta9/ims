Clifton.performance.composite.setup.CompositeHistoryEntryWindow = Ext.extend(TCG.app.OKCancelWindow, {
	titlePrefix: 'Performance Composite History',

	items: [{
		xtype: 'formpanel',
		labelWidth: 140,
		instructions: 'This screen allows you to fix an incorrectly entered fee for the current model fee, or start a new model fee as of a given date (where current fee will be stored in history).  See the History tab on the composite for full history details.',
		getSaveURL: function() {
			return 'performanceCompositeHistorySave.json';
		},

		items: [
			{fieldLabel: 'Performance Composite', name: 'performanceComposite.name', detailIdField: 'performanceComposite.id', xtype: 'linkfield', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow'},
			{fieldLabel: 'Current Model Fee', xtype: 'currencyfield', decimalPrecision: 4, name: 'performanceComposite.modelFee', readOnly: true},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'enterNewModelFee', allowBlank: false,
				items: [
					{boxLabel: 'Enter New Model Fee', xtype: 'radio', name: 'enterNewModelFee', inputValue: true},
					{boxLabel: 'Fix Incorrectly Entered Model Fee', xtype: 'radio', name: 'enterNewModelFee', inputValue: false}
				],
				listeners: {
					change: function(rg, r) {
						const p = TCG.getParentFormPanel(rg);
						const startDateField = p.getForm().findField('startDate');
						startDateField.setDisabled(r.inputValue !== true);
						startDateField.allowBlank = (r.inputValue !== true);
					}
				}
			},
			{fieldLabel: 'New Model Fee', name: 'modelFee', xtype: 'currencyfield', decimalPrecision: 4, allowBlank: false},
			{
				fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield',
				qtip: 'Current Model Fee will be ended on Start Date - 1'
			}
		]
	}]
});
