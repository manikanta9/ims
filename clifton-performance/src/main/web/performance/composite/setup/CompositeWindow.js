Clifton.performance.composite.setup.CompositeWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'performanceComposite.json',

	getClassName: function(config, entity) {
		let className = 'Clifton.performance.composite.setup.StandardCompositeWindow';
		if (entity && entity.rollupComposite) {
			className = 'Clifton.performance.composite.setup.RollupCompositeWindow';
		}
		return className;
	}
});
