Clifton.performance.composite.setup.CompositeSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'compositeSetupWindow',
	title: 'Performance Composites',
	iconCls: 'hierarchy',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Composites',
				items: [{
					name: 'performanceCompositeListFind',
					xtype: 'gridpanel',
					wikiPage: 'IT/Performance+Calculation+Requirements',
					importTableName: 'PerformanceComposite',
					queryExportTagName: 'Performance',
					instructions: 'A Composite is a group of discretionary portfolios that collectively represent a particular investment strategy or objective.',
					topToolbarSearchParameter: 'searchPattern',
					rowSelectionModel: 'multiple',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Composite Name', width: 200, dataIndex: 'name'},
						{header: 'Composite Short Name', width: 200, dataIndex: 'compositeShortName', hidden: true},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Composite Code', width: 80, dataIndex: 'code'},
						{header: 'Currency', width: 60, dataIndex: 'currency.name', filter: {searchFieldName: 'currencyId', type: 'combo', url: 'investmentSecurityListFind.json?currency=true', displayField: 'label'}},
						{header: 'Benchmark', width: 120, dataIndex: 'benchmarkSecurity.label', filter: {searchFieldName: 'benchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Secondary Benchmark', width: 120, dataIndex: 'secondaryBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'secondaryBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Third Benchmark', width: 120, dataIndex: 'thirdBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'thirdBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Rollup', width: 60, dataIndex: 'rollupComposite', type: 'boolean'},
						{header: 'Actual', width: 60, dataIndex: 'actualNetReturnUsed', type: 'boolean', title: 'Specifies whether performance for this composite uses Actual (true) or Model (false) Net Returns.'},
						{header: 'Includes Partial Period Accounts', width: 60, hidden: true, dataIndex: 'includePartialPeriodAccounts', type: 'boolean', tooltip: 'If checked, accounts that had positions on for only a portion of the period are still included in the composite performance.  Leave unchecked to only include accounts will a full period of positions on.'},
						{header: 'Model Fee', width: 60, dataIndex: 'modelFee', type: 'percent', useNull: true, numberFormat: '0,000.0000'},
						{header: 'Start Date', width: 60, dataIndex: 'startDate'},
						{header: 'End Date', width: 60, dataIndex: 'endDate'},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},
						{
							header: 'Composite Report', hidden: true, width: 100, dataIndex: 'compositeReport.name', filter: {type: 'combo', searchFieldName: 'compositeReportId', url: 'reportListFind.json?templateName=Performance Strategy Report'},
							tooltip: 'The report to use when viewing/posting the report for this composite.'
						},
						{
							header: 'Composite Report Frequency', hidden: true, width: 80, dataIndex: 'compositeReportFrequency', filter: {type: 'list', options: Clifton.performance.PerformanceReportingFrequencies},
							tooltip: 'The frequency this composite performance report is delivered.  Used to determine when the performance for the composite should move to reporting workflow states.'
						},
						{header: 'Account Calculator', width: 80, dataIndex: 'accountPerformanceCalculatorBean.name', filter: {searchFieldName: 'accountPerformanceCalculatorBeanName'}, tooltip: 'Default calculator that each account assigned to this composite uses to calculate performance. Can be overridden on an individual assignment.'},
						{
							header: 'Account Report', width: 100, hidden: true, dataIndex: 'accountReport.name', filter: {type: 'combo', searchFieldName: 'accountReportId', url: 'reportListFind.json?templateName=Performance Strategy Report'},
							tooltip: 'The default report to use when viewing/posting the account performance reports for this composite.  This can be overridden for an individual account by going to the account assignment for this composite.'
						},
						{
							header: 'Account Report Frequency', hidden: true, width: 80, dataIndex: 'accountReportFrequency', filter: {type: 'list', options: Clifton.performance.PerformanceReportingFrequencies},
							tooltip: 'The default frequency account performance reports under this composite are delivered.  Used to determine when the performance for the account should move to reporting workflow states.'
						},
						{
							header: 'Account Report Append Attachment(s)', hidden: true, width: 80, dataIndex: 'accountReportAppendAttachments', type: 'boolean',
							tooltip: 'If checked, then any attachments that exist on the Performance Account Performance will be appending to the system generated report prior to viewing and posting.  Primarily used for Fund Investors.'
						}
					],
					listeners: {
						afterRender: function() {
							// default to active
							this.setFilterValue('active', true);
						}
					},
					editor: {
						addEditButtons: function(toolBar, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
							toolBar.add({
								text: 'Add Note',
								tooltip: 'Add a note linked to selected composites (NOTE: Maximum of 50 composites can be selected)',
								iconCls: 'pencil',
								scope: this,
								handler: function() {
									const grid = gridPanel.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one composite to add note to.', 'No Row(s) Selected');
									}
									else if (sm.getCount() > 50) {
										TCG.showError('You have selected ' + rows.length + ' composites, which is greater than the maximum allowed of 50.  Please limit your selection.');
									}
									else {
										const tabPanel = TCG.getParentTabPanel(gridPanel);
										tabPanel.addNote(sm.getSelections(), gridPanel, 'PerformanceComposite', 'system.table.PerformanceComposite');
									}
								}
							});
							toolBar.add('-');
						},
						detailPageClass: {
							getItemText: function(rowItem) {
								const t = rowItem.get('rollupComposite');
								return (t) ? 'Rollup Composite' : 'Standard Composite';
							},
							items: [
								{text: 'Standard Composite', iconCls: 'verify', className: 'Clifton.performance.composite.setup.StandardCompositeWindow'},
								{text: 'Rollup Composite', iconCls: 'hierarchy', className: 'Clifton.performance.composite.setup.RollupCompositeWindow'}
							]
						}
					}
				}]
			},


			{
				title: 'Assignments',
				items: [{
					name: 'performanceCompositeInvestmentAccountListFind',
					xtype: 'gridpanel',
					importTableName: 'PerformanceCompositeInvestmentAccount',
					instructions: 'The following investment accounts are assigned to this performance composite.',
					additionalPropertiesToRequest: 'performanceComposite.accountPerformanceCalculatorBean.name',
					rowSelectionModel: 'multiple',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Composite Name', width: 200, dataIndex: 'performanceComposite.name', filter: {type: 'combo', searchFieldName: 'performanceCompositeId', url: 'performanceCompositeListFind.json'}},
						{header: 'Composite Short Name', width: 200, dataIndex: 'performanceComposite.compositeShortName', hidden: true, filter: false, sortable: false},
						{header: 'Investment Account', width: 200, dataIndex: 'investmentAccount.label', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}},

						{
							header: 'Account Calculator', width: 100, dataIndex: 'coalesceAccountPerformanceCalculatorBean.name', filter: {searchFieldName: 'coalesceAccountPerformanceCalculatorBeanName'},
							renderer: function(v, metaData, r) {
								if (TCG.isNotBlank(v) && TCG.isNotEquals(TCG.getValue('performanceComposite.accountPerformanceCalculatorBean.name', r.json), v)) {
									metaData.css = 'amountAdjusted';
									metaData.attr = TCG.renderQtip('Assignment Override.  Composite Account Performance Calculator is [' + TCG.getValue('performanceComposite.accountPerformanceCalculatorBean.name', r.json) + ']');
								}
								return v;
							}
						},

						{header: 'Benchmark', width: 120, dataIndex: 'benchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'benchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Secondary Benchmark', width: 120, dataIndex: 'secondaryBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'secondaryBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},
						{header: 'Third Benchmark', width: 120, dataIndex: 'thirdBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'thirdBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json', displayField: 'label'}},

						{header: 'Start Date', width: 60, dataIndex: 'startDate'},
						{header: 'End Date', width: 60, dataIndex: 'endDate'},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'},

						{
							header: 'Account Report', width: 100, hidden: true, dataIndex: 'coalesceAccountReport.name', filter: {type: 'combo', searchFieldName: 'coalesceAccountReportId', url: 'reportListFind.json?templateName=Performance Strategy Report'},
							tooltip: 'The default report to use when viewing/posting the account performance reports for this composite.  This can be overridden for an individual account by going to the account assignment for this composite.'
						},
						{
							header: 'Account Report Frequency', hidden: true, width: 80, dataIndex: 'coalesceAccountReportFrequency', filter: {type: 'list', options: Clifton.performance.PerformanceReportingFrequencies},
							tooltip: 'The default frequency account performance reports under this composite are delivered.  Used to determine when the performance for the account should move to reporting workflow states.'
						},
						{
							header: 'Account Report Append Attachment(s)', hidden: true, width: 80, dataIndex: 'coalesceAccountReportAppendAttachments', type: 'boolean',
							tooltip: 'If checked, then any attachments that exist on the Performance Account Performance will be appending to the system generated report prior to viewing and posting.  Primarily used for Fund Investors.'
						}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Composite', xtype: 'toolbar-combo', name: 'performanceCompositeId', width: 180, url: 'performanceCompositeListFind.json', displayField: 'label', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow', linkedFilter: 'performanceComposite.name'},
							{fieldLabel: 'Client Acct', xtype: 'toolbar-combo', name: 'investmentAccountId', width: 180, url: 'investmentAccountListFind.json?ourAccount=true', displayField: 'label', detailPageClass: 'Clifton.investment.account.AccountWindow', linkedFilter: 'investmentAccount.label'},
							{
								fieldLabel: 'Active On', xtype: 'datefield-monthYear', name: 'activeOnDate', width: 110,
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						const dateField = TCG.getChildByName(t, 'activeOnDate');

						if (firstLoad) {
							// Default to Last Month
							const dt = (new Date()).getFirstDateOfMonth().add(Date.MONTH, -1);
							dateField.setValue(dt);
						}

						const activeOnDate = TCG.parseDate(dateField.getValue(), 'm/d/Y');

						return {
							activeOnDate: TCG.isBlank(activeOnDate) ? '' : (activeOnDate.getFirstDateOfMonth()).format('m/d/Y')
						};
					},
					editor: {
						addEditButtons: function(toolBar, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);
							toolBar.add({
								text: 'Add Note',
								tooltip: 'Add a note linked to selected assignment(s) (NOTE: Maximum of 50 assignments can be selected)',
								iconCls: 'pencil',
								scope: this,
								handler: function() {
									const grid = gridPanel.grid;
									const sm = grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one assignment to add note to.', 'No Row(s) Selected');
									}
									else if (sm.getCount() > 50) {
										TCG.showError('You have selected ' + rows.length + ' assignments, which is greater than the maximum allowed of 50.  Please limit your selection.');
									}
									else {
										const tabPanel = TCG.getParentTabPanel(gridPanel);
										tabPanel.addNote(sm.getSelections(), gridPanel, 'PerformanceCompositeInvestmentAccount', 'system.table.PerformanceCompositeInvestmentAccount');
									}
								}
							});
							toolBar.add('-');
						},
						detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow'
					}
				}]
			},


			{
				title: 'Missing Assignments',
				items: [{
					xtype: 'investment-accountsGrid_byClient',
					instructions: 'Shows accounts without active assignments for the given period.  By default, selecting the active assignments on date that will also limits to accounts with Positions On Date prior to the end of the selected month.',
					editor: {
						detailPageClass: 'Clifton.investment.account.AccountWindow',
						getDefaultData: function(gridPanel) { // defaults client and our account for the detail page
							return {
								ourAccount: true,
								baseCurrency: TCG.data.getData('investmentSecurityBySymbol.json?symbol=USD&currency=true', gridPanel, 'investment.security.usd'),
								type: TCG.data.getData('investmentAccountTypeByOurAccount.json', gridPanel, 'investment.getOurAccountType')
							};
						}
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{
								fieldLabel: 'Discretion Type', xtype: 'toolbar-combo', name: 'discretionTypeFilter', width: 150, minListWidth: 150, mode: 'local', value: 'Discretion', displayField: 'name', valueField: 'value',
								store: new Ext.data.ArrayStore({
									fields: ['value', 'name', 'description'],
									data: [['Discretion', 'Discretion', 'Accounts with discretion'], ['Non-Discretion', 'Non-Discretion', 'Accounts without discretion'], [null, 'Not Specified', 'Discretion has not been specified']]
								})
							},
							{
								fieldLabel: 'Active Assignments On', xtype: 'datefield-monthYear', name: 'activeOnDate', width: 110, value: (new Date()).getLastDateOfMonth(),
								listeners: {
									select: function(field) {
										const gridPanel = TCG.getParentByClass(field, Ext.Panel);
										const accountsMissingCompositeAssignmentActiveOnDate = TCG.parseDate(field.getValue(), 'm/d/Y');
										if (TCG.isNotBlank(accountsMissingCompositeAssignmentActiveOnDate)) {
											gridPanel.setFilterValue('inceptionDate', {'before': (accountsMissingCompositeAssignmentActiveOnDate.getLastDateOfMonth())});
											// Make Sure Filter is Active
											gridPanel.grid.filters.getFilter('inceptionDate').setActive(true);
										}
										gridPanel.reload();
									}
								}
							}
						];
					},
					onBeforeReturnFilterData: function(filters) {
						const t = this.gridPanel.topToolbar;
						const dateField = TCG.getChildByName(t, 'activeOnDate');
						let accountsMissingCompositeAssignmentActiveOnDate = TCG.parseDate(dateField.getValue(), 'm/d/Y');
						if (TCG.isNotBlank(accountsMissingCompositeAssignmentActiveOnDate)) {
							// Move the date to the end of the month
							accountsMissingCompositeAssignmentActiveOnDate = (accountsMissingCompositeAssignmentActiveOnDate.getLastDateOfMonth()).format('m/d/Y');

							filters.push({
								field: 'accountsMissingCompositeAssignmentActiveOnDate',
								data: {comparison: 'EQUALS', value: accountsMissingCompositeAssignmentActiveOnDate}
							});
						}
					},
					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						const dateField = TCG.getChildByName(t, 'activeOnDate');
						const discretionType = TCG.getChildByName(t, 'discretionTypeFilter');

						if (firstLoad) {
							this.setFilterValue('workflowState.label', 'Active');
							this.grid.filters.getFilter('workflowState.label').setActive(true);
							this.setFilterValue('inceptionDate', {'before': (new Date().getLastDateOfMonth())});
						}

						if (TCG.isBlank(dateField.getValue())) {
							TCG.showError('Active Assignments On Date is Required.', 'Missing Required Value');
							return false;
						}

						return {
							ourAccount: true,
							discretionTypeEqual: discretionType.getValue(),
							nullDiscretionType: TCG.isNull(discretionType.getValue())
						};
					}
				}]
			}
		],
		addNote: function(rows, gridPanel, tableName, dataKey) {
			let className = 'Clifton.system.note.SingleNoteWindow';
			const tbl = TCG.data.getData('systemTableByName.json?tableName=' + tableName, gridPanel, dataKey);
			let dd = {
				noteType: {table: tbl}
			};

			const selectionIds = [];
			for (let i = 0; i < rows.length; i++) {
				const rec = rows[i].json;
				const selectionId = TCG.getValue('id', rec);
				if (selectionId) {
					selectionIds.push(selectionId);
				}
			}
			if (selectionIds.length === 1) {
				dd = Ext.apply(dd, {linkedToMultiple: false, fkFieldId: selectionIds[0]});
			}
			else {
				className = 'Clifton.system.note.LinkedNoteWindow';
				dd = Ext.apply(dd, {linkedToMultiple: true, fkFieldIds: selectionIds});
			}
			const cmpId = TCG.getComponentId(className);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: dd,
				openerCt: gridPanel
			});
		}
	}]
});
