Clifton.performance.composite.performance.CompositePerformanceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Composite Performance',
	width: 1300,
	height: 510,

	gridTabWithCount: 'Portal Files', // show portal file count in  the tab

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		reloadOnChange: true,
		items: [
			{
				title: 'Performance',
				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'PerformanceCompositePerformance',
					reloadFormPanelAfterTransition: true,
					addAdditionalButtons: function(t) {
						const formPanel = TCG.getChildrenByClass(TCG.getParentTabPanel(t), TCG.form.FormPanel)[0];
						const menu = new Ext.menu.Menu();
						menu.add({
							text: '2 Decimals (Rounded)',
							xtype: 'menucheckitem',
							checked: true,
							group: 'compositePerformanceView',
							handler: function() {
								formPanel.formatNumberFields(2);
							}
						});
						menu.add({
							text: '4 Decimals (Rounded)',
							xtype: 'menucheckitem',
							group: 'compositePerformanceView',
							handler: function() {
								formPanel.formatNumberFields(4);
							}
						});
						menu.add({
							text: 'All Decimals',
							xtype: 'menucheckitem',
							group: 'compositePerformanceView',
							handler: function() {
								formPanel.formatNumberFields(15);
							}
						});

						t.add({
							text: 'Views',
							iconCls: 'views',
							menu: menu
						});
						t.add('-');

						t.add({
							text: 'Rebuild Performance',
							xtype: 'splitbutton',
							iconCls: 'run',
							tooltip: 'Rebuild composite performance',
							handler: function(btn) {
								formPanel.executeRebuildHandler('ALL');
							},

							menu: {
								items: [
									{
										text: 'Rebuild Benchmark Returns Only',
										tooltip: 'Recalculates all monthly and linked benchmark returns only.',
										iconCls: 'run',
										scope: this,
										handler: function(btn) {
											formPanel.executeRebuildHandler('BENCHMARKS_ONLY');
										}
									}
								]
							}
						});
					}
				},
				items: [{
					xtype: 'formpanel',
					url: 'performanceCompositePerformance.json',
					labelWidth: 140,
					getWarningMessage: function(form) {
						return Clifton.rule.violation.getRuleViolationWarningMessage('performance', form.formValues);
					},
					reload: function() {
						const w = this.getWindow();
						this.getForm().setValues(TCG.data.getData('performanceCompositePerformance.json?id=' + w.getMainFormId(), this), true);
					},
					getLoadParams: function(firstLoad) {
						const win = this.getWindow();
						return {id: win.params.id, requestedMaxDepth: 3};
					},
					listeners: {
						afterload: function(form, isClosing) {
							const itd = TCG.getChildByName(form, 'itd');
							const oneYearGrossReturn = TCG.getChildByName(form, 'oneYearGrossReturn');

							if (oneYearGrossReturn && TCG.isNotBlank(oneYearGrossReturn.getValue()) && oneYearGrossReturn.getValue() !== 0) {
								itd.setText('Annualized ' + itd.initialConfig.html);
							}
							else {
								itd.setText('Cumulative ' + itd.initialConfig.html);
							}
						}
					},
					executeRebuildHandler: function(metricProcessingType) {
						const fp = this;
						const performanceComposite = fp.getFormValue('performanceComposite.id');
						const accountingPeriod = fp.getFormValue('accountingPeriod.id');
						if (TCG.isNull(performanceComposite) || TCG.isNull(accountingPeriod)) {
							return;
						}
						const loader = new TCG.data.JsonLoader({
							waitTarget: fp,
							waitMsg: 'Loading Performance<marquee scrollamount="5" direction="right" width="15" scrolldelay="150">...</marquee>',
							success: function(response, opts) {
								if (this.isUseWaitMsg()) {
									this.getMsgTarget().unmask();
								}
								if (fp.reload) {
									fp.reload();
								}
								fp.getForm().trackResetOnLoad = true;
							}
						});
						const loadUrl = 'performanceCompositePerformanceRebuild.json?synchronous=true&';
						loader.load(loadUrl + 'performanceCompositeId=' + performanceComposite + '&toAccountingPeriod.id=' + accountingPeriod + '&metricProcessingType=' + metricProcessingType);
					},
					formatNumberFields: function(precision) {
						TCG.each(TCG.getChildrenByClass(TCG.getParentTabPanel(this), TCG.form.CurrencyField), function(e) {
							if (TCG.isTrue(e.readOnly)) {
								e.decimalPrecision = precision;
								e.setValue(this.getFormValue(e.name));
								e.originalValue = e.getValue();
							}
						}, this);
					},
					items: [
						{fieldLabel: 'Performance Composite', name: 'performanceComposite.name', hiddenName: 'performanceComposite.id', detailIdField: 'performanceComposite.id', xtype: 'combo', displayField: 'label', url: 'performanceCompositeListFind.json', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow'},
						{fieldLabel: 'Period', name: 'accountingPeriod.label', hiddenName: 'accountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label', detailPageClass: 'Clifton.accounting.period.PeriodWindow', detailIdField: 'accountingPeriod.id'},
						{fieldLabel: 'Start Date', name: 'accountingPeriod.startDate', hidden: true, submitValue: false},
						{fieldLabel: 'Market Value', name: 'periodEndMarketValue', xtype: 'currencyfield'},
						{html: '<hr/>', xtype: 'label'},
						{
							xtype: 'formtable',
							columns: 10,
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 2,
								readOnly: true,
								submitValue: false,
								style: {marginLeft: '5px', textAlign: 'right'},
								width: '88%'
							},
							items: [
								{cellWidth: '100px', xtype: 'label'},
								{html: 'Gross Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Actual Net Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Model Net Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Gross STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Actual Net STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Model Net STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Gross Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Actual Net Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Model Net Sharpe', xtype: 'label', style: 'text-align: right'},

								{html: 'Current Period:', xtype: 'label'},
								{name: 'periodGrossReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodNetReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodModelNetReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{xtype: 'label', colspan: 6},

								{html: 'QTD:', xtype: 'label'},
								{name: 'quarterToDateGrossReturn'},
								{name: 'quarterToDateNetReturn'},
								{name: 'quarterToDateModelNetReturn'},
								{xtype: 'label', colspan: 6},

								{html: 'YTD:', xtype: 'label'},
								{name: 'yearToDateGrossReturn'},
								{name: 'yearToDateNetReturn'},
								{name: 'yearToDateModelNetReturn'},
								{xtype: 'label', colspan: 6},

								{html: '1 Year:', xtype: 'label'},
								{name: 'oneYearGrossReturn'},
								{name: 'oneYearNetReturn'},
								{name: 'oneYearModelNetReturn'},
								{name: 'oneYearGrossStandardDeviation'},
								{name: 'oneYearNetStandardDeviation'},
								{name: 'oneYearModelNetStandardDeviation'},
								{name: 'oneYearGrossSharpeRatio'},
								{name: 'oneYearNetSharpeRatio'},
								{name: 'oneYearModelNetSharpeRatio'},

								{html: '3 Year:', xtype: 'label'},
								{name: 'threeYearGrossReturn'},
								{name: 'threeYearNetReturn'},
								{name: 'threeYearModelNetReturn'},
								{name: 'threeYearGrossStandardDeviation'},
								{name: 'threeYearNetStandardDeviation'},
								{name: 'threeYearModelNetStandardDeviation'},
								{name: 'threeYearGrossSharpeRatio'},
								{name: 'threeYearNetSharpeRatio'},
								{name: 'threeYearModelNetSharpeRatio'},

								{html: '5 Year:', xtype: 'label'},
								{name: 'fiveYearGrossReturn'},
								{name: 'fiveYearNetReturn'},
								{name: 'fiveYearModelNetReturn'},
								{name: 'fiveYearGrossStandardDeviation'},
								{name: 'fiveYearNetStandardDeviation'},
								{name: 'fiveYearModelNetStandardDeviation'},
								{name: 'fiveYearGrossSharpeRatio'},
								{name: 'fiveYearNetSharpeRatio'},
								{name: 'fiveYearModelNetSharpeRatio'},

								{html: '10 Year:', xtype: 'label'},
								{name: 'tenYearGrossReturn'},
								{name: 'tenYearNetReturn'},
								{name: 'tenYearModelNetReturn'},
								{name: 'tenYearGrossStandardDeviation'},
								{name: 'tenYearNetStandardDeviation'},
								{name: 'tenYearModelNetStandardDeviation'},
								{name: 'tenYearGrossSharpeRatio'},
								{name: 'tenYearNetSharpeRatio'},
								{name: 'tenYearModelNetSharpeRatio'},

								{html: 'ITD:', name: 'itd', xtype: 'label'},
								{name: 'inceptionToDateGrossReturn'},
								{name: 'inceptionToDateNetReturn'},
								{name: 'inceptionToDateModelNetReturn'},
								{name: 'inceptionToDateGrossStandardDeviation'},
								{name: 'inceptionToDateNetStandardDeviation'},
								{name: 'inceptionToDateModelNetStandardDeviation'},
								{name: 'inceptionToDateGrossSharpeRatio'},
								{name: 'inceptionToDateNetSharpeRatio'},
								{name: 'inceptionToDateModelNetSharpeRatio'},

								{html: 'Inception Date:', xtype: 'label'},
								{name: 'performanceComposite.startDate', xtype: 'displayfield', type: 'date', style: 'marginLeft: 5px; text-align: left;'},
								{xtype: 'label', colspan: 7}
							]
						}
					]
				}]
			},


			{
				title: 'Benchmarks',
				items: [{
					xtype: 'formpanel',
					url: 'performanceCompositePerformance.json',
					labelWidth: 140,
					labelFieldName: 'performanceComposite.name',
					getLoadParams: function(firstLoad) {
						const win = this.getWindow();
						return {id: win.params.id, requestedMaxDepth: 4};
					},
					listeners: {
						afterload: function(form, isClosing) {
							const itd = TCG.getChildByName(form, 'itd');
							const oneYearBenchmarkReturn = TCG.getChildByName(form, 'oneYearBenchmarkReturn');

							if (oneYearBenchmarkReturn && TCG.isNotBlank(oneYearBenchmarkReturn.getValue()) && oneYearBenchmarkReturn.getValue() !== 0) {
								itd.setText('Annualized ' + itd.initialConfig.html);
							}
							else {
								itd.setText('Cumulative ' + itd.initialConfig.html);
							}
						}
					},
					items: [
						{fieldLabel: 'Risk Free Security', name: 'performanceComposite.riskFreeSecurity.label', detailIdField: 'performanceComposite.riskFreeSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Benchmark', name: 'performanceComposite.benchmarkSecurity.label', detailIdField: 'performanceComposite.benchmarkSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Secondary Benchmark', name: 'performanceComposite.secondaryBenchmarkSecurity.label', detailIdField: 'performanceComposite.secondaryBenchmarkSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{fieldLabel: 'Third Benchmark', name: 'performanceComposite.thirdBenchmarkSecurity.label', detailIdField: 'performanceComposite.thirdBenchmarkSecurity.id', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'formtable',
							columns: 11,
							defaults: {
								xtype: 'currencyfield',
								decimalPrecision: 2,
								submitValue: false,
								readOnly: true,
								style: {marginLeft: '5px', textAlign: 'right'},
								width: '88%'
							},
							items: [
								{cellWidth: '100px', xtype: 'label'},
								{html: 'Risk Free Return', xtype: 'label', style: 'text-align: right'},
								{html: 'Benchmark', xtype: 'label', style: 'text-align: right'},
								{html: 'Secondary Benchmark', xtype: 'label', style: 'text-align: right'},
								{html: 'Third Benchmark', xtype: 'label', style: 'text-align: right'},
								{html: 'Benchmark STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Secondary Benchmark STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Third Benchmark STD', xtype: 'label', style: 'text-align: right'},
								{html: 'Benchmark Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Secondary Benchmark Sharpe', xtype: 'label', style: 'text-align: right'},
								{html: 'Third Benchmark Sharpe', xtype: 'label', style: 'text-align: right'},

								{html: 'Current Period:', xtype: 'label'},
								{name: 'periodRiskFreeSecurityReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodBenchmarkReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodSecondaryBenchmarkReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{name: 'periodThirdBenchmarkReturn', readOnly: false, decimalPrecision: 15, submitValue: true},
								{xtype: 'label', colspan: 7},

								{html: 'QTD:', xtype: 'label'},
								{name: 'quarterToDateRiskFreeSecurityReturn'},
								{name: 'quarterToDateBenchmarkReturn'},
								{name: 'quarterToDateSecondaryBenchmarkReturn'},
								{name: 'quarterToDateThirdBenchmarkReturn'},
								{xtype: 'label', colspan: 7},

								{html: 'YTD:', xtype: 'label'},
								{name: 'yearToDateRiskFreeSecurityReturn'},
								{name: 'yearToDateBenchmarkReturn'},
								{name: 'yearToDateSecondaryBenchmarkReturn'},
								{name: 'yearToDateThirdBenchmarkReturn'},
								{xtype: 'label', colspan: 7},

								{html: '1 Year:', xtype: 'label'},
								{name: 'oneYearRiskFreeSecurityReturn'},
								{name: 'oneYearBenchmarkReturn'},
								{name: 'oneYearSecondaryBenchmarkReturn'},
								{name: 'oneYearThirdBenchmarkReturn'},
								{name: 'oneYearBenchmarkStandardDeviation'},
								{name: 'oneYearSecondaryBenchmarkStandardDeviation'},
								{name: 'oneYearThirdBenchmarkStandardDeviation'},
								{name: 'oneYearBenchmarkSharpeRatio'},
								{name: 'oneYearSecondaryBenchmarkSharpeRatio'},
								{name: 'oneYearThirdBenchmarkSharpeRatio'},

								{html: '3 Year:', xtype: 'label'},
								{name: 'threeYearRiskFreeSecurityReturn'},
								{name: 'threeYearBenchmarkReturn'},
								{name: 'threeYearSecondaryBenchmarkReturn'},
								{name: 'threeYearThirdBenchmarkReturn'},
								{name: 'threeYearBenchmarkStandardDeviation'},
								{name: 'threeYearSecondaryBenchmarkStandardDeviation'},
								{name: 'threeYearThirdBenchmarkStandardDeviation'},
								{name: 'threeYearBenchmarkSharpeRatio'},
								{name: 'threeYearSecondaryBenchmarkSharpeRatio'},
								{name: 'threeYearThirdBenchmarkSharpeRatio'},

								{html: '5 Year:', xtype: 'label'},
								{name: 'fiveYearRiskFreeSecurityReturn'},
								{name: 'fiveYearBenchmarkReturn'},
								{name: 'fiveYearSecondaryBenchmarkReturn'},
								{name: 'fiveYearThirdBenchmarkReturn'},
								{name: 'fiveYearBenchmarkStandardDeviation'},
								{name: 'fiveYearSecondaryBenchmarkStandardDeviation'},
								{name: 'fiveYearThirdBenchmarkStandardDeviation'},
								{name: 'fiveYearBenchmarkSharpeRatio'},
								{name: 'fiveYearSecondaryBenchmarkSharpeRatio'},
								{name: 'fiveYearThirdBenchmarkSharpeRatio'},

								{html: '10 Year:', xtype: 'label'},
								{name: 'tenYearRiskFreeSecurityReturn'},
								{name: 'tenYearBenchmarkReturn'},
								{name: 'tenYearSecondaryBenchmarkReturn'},
								{name: 'tenYearThirdBenchmarkReturn'},
								{name: 'tenYearBenchmarkStandardDeviation'},
								{name: 'tenYearSecondaryBenchmarkStandardDeviation'},
								{name: 'tenYearThirdBenchmarkStandardDeviation'},
								{name: 'tenYearBenchmarkSharpeRatio'},
								{name: 'tenYearSecondaryBenchmarkSharpeRatio'},
								{name: 'tenYearThirdBenchmarkSharpeRatio'},

								{html: 'ITD:', name: 'itd', xtype: 'label'},
								{name: 'inceptionToDateRiskFreeSecurityReturn'},
								{name: 'inceptionToDateBenchmarkReturn'},
								{name: 'inceptionToDateSecondaryBenchmarkReturn'},
								{name: 'inceptionToDateThirdBenchmarkReturn'},
								{name: 'inceptionToDateBenchmarkStandardDeviation'},
								{name: 'inceptionToDateSecondaryBenchmarkStandardDeviation'},
								{name: 'inceptionToDateThirdBenchmarkStandardDeviation'},
								{name: 'inceptionToDateBenchmarkSharpeRatio'},
								{name: 'inceptionToDateSecondaryBenchmarkSharpeRatio'},
								{name: 'inceptionToDateThirdBenchmarkSharpeRatio'},

								{html: 'Inception Date:', xtype: 'label'},
								{name: 'performanceComposite.startDate', xtype: 'displayfield', type: 'date', style: 'marginLeft: 5px; text-align: left;'},
								{xtype: 'label', colspan: 11}
							]
						}
					]
				}]
			},


			{
				title: 'Violations',
				items: [{
					xtype: 'rule-violation-grid',
					tableName: 'PerformanceCompositePerformance'
				}]
			},


			{
				title: 'Account Performance',
				items: [{
					xtype: 'composite-account-performance-workflow-grid',
					instructions: 'The following displays performance history for the accounts under the selected composite performance.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.',
					columnOverrides: [
						{dataIndex: 'periodSecondaryBenchmarkReturn', hidden: true},
						{dataIndex: 'periodThirdBenchmarkReturn', hidden: true},
						{dataIndex: 'periodTotalNetCashflow', hidden: true},
						{dataIndex: 'periodGainLoss', hidden: true}
					],
					viewNames: ['Default', 'CAPS Export'],
					// One view of CAPS Export would update to CAPS Export - Actual Net or CAPS Export - Model Net based on composite option
					switchToView: function(viewName, cancelReload) {
						if (viewName === 'CAPS Export') {
							const useActualNet = this.getWindow().getMainFormPanel().getFormValue('performanceComposite.actualNetReturnUsed');
							if (TCG.isTrue(useActualNet)) {
								viewName = 'CAPS Export - Actual Net';
							}
							else {
								viewName = 'CAPS Export - Model Net';
							}
						}
						TCG.grid.GridPanel.prototype.switchToView.call(this, viewName, cancelReload);
					},
					getTopToolbarFilters: function(toolbar) {
						//no room for toolbar filters on this screen
					},
					getAdditionalLoadParams: function(firstLoad) {
						return {
							accountingPeriodId: TCG.getChildByName(this.getWindow().getMainFormPanel(), 'accountingPeriod.label').getValue(),
							performanceCompositeId: TCG.getChildByName(this.getWindow().getMainFormPanel(), 'performanceComposite.name').getValue()
						};
					}
				}]
			},


			{
				title: 'Account Assignments',
				items: [{
					name: 'performanceCompositeInvestmentAccountListFind',
					xtype: 'gridpanel',
					importTableName: 'PerformanceCompositeInvestmentAccount',
					instructions: 'The following investment accounts are assigned to this performance composite.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Investment Account', width: 250, dataIndex: 'investmentAccount.label', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: {type: 'combo', searchFieldName: 'investmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json'}},

						{header: 'Benchmark', width: 120, dataIndex: 'benchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'benchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
						{header: 'Secondary Benchmark', width: 120, dataIndex: 'secondaryBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'secondaryBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},
						{header: 'Third Benchmark', width: 120, dataIndex: 'thirdBenchmarkSecurity.label', hidden: true, filter: {searchFieldName: 'thirdBenchmarkSecurityId', type: 'combo', url: 'investmentSecurityListFind.json?tradingDisallowed=true', displayField: 'label'}},

						{header: 'Start Date', width: 60, dataIndex: 'startDate'},
						{header: 'End Date', width: 60, dataIndex: 'endDate'},
						{header: 'Active', width: 50, dataIndex: 'active', type: 'boolean'}
					],
					getLoadParams: function(firstLoad) {
						return {
							activeOnDate: TCG.parseDate(this.getWindow().getMainForm().formValues.accountingPeriod.startDate).format('m/d/Y'),
							performanceCompositeId: TCG.getChildByName(this.getWindow().getMainFormPanel(), 'performanceComposite.name').getValue()
						};
					},
					editor: {
						detailPageClass: 'Clifton.performance.composite.assignment.CompositeAccountWindow',
						getDefaultData: function(gridPanel, row) {
							return {performanceComposite: gridPanel.getWindow().getMainForm().formValues.performanceComposite.id};
						}
					}
				}]
			},


			{
				title: 'Portal Files',
				reloadOnTabChange: true,
				items: [
					{
						xtype: 'portal-files-grid-for-source-entity',
						sourceTableName: 'PerformanceCompositePerformance'
					}
				]
			}
		]
	}]
});
