Clifton.performance.composite.performance.CompositePerformanceWorkflowWindow = Ext.extend(TCG.app.Window, {
	id: 'compositePerformanceWorkflowWindow',
	title: 'Composite Performance Workflow',
	iconCls: 'chart-bar',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		listeners: {
			// keep selected month
			beforetabchange: Clifton.performance.composite.registerBeforeTabChangeListener,
			tabchange: Clifton.performance.composite.registerTabChangeListener
		},
		items: [
			{
				title: 'History',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					instructions: 'The following displays performance history across all workflow statuses.  <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.'
				}]
			},


			{
				title: 'Draft',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					workflowStateNames: 'Draft',
					drillDownOnly: false,

					transitionWorkflowStateList: [
						{stateName: 'Validated (Pre-Process Rules)', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Performance'}
					],

					instructions: 'The following performance are in Draft status and have not been approved yet. <b>ONLY</b> users will Full Control access can view performance in Draft status, if you do not have appropriate access those records will not be displayed.'
				}]
			},


			{
				title: 'Pending',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					workflowStateNames: 'Pending Approval',
					showPerformanceReportButton: true,

					transitionWorkflowStateList: [
						{stateName: 'Preliminary', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Performance'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance'}
					],

					instructions: 'The following performance are in Pending status and have not had Preliminary approval by Accounting yet.  If you are a member of the Accounting team, you may select performance from the list below and approve them using the Approve button'
				}]
			},


			{
				title: 'Preliminary',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					workflowStateNames: 'Preliminary',
					showPerformanceReportButton: true,
					showPerformanceReportPreliminaryPostButton: true,

					transitionWorkflowStateList: [
						{stateName: 'Validated (Post-Process Rules)', iconCls: 'row_reconciled', buttonText: 'Approve', buttonTooltip: 'Approved Selected Performance'},
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance'}
					],

					instructions: 'The following performance are in Preliminary status and have not had  approval by Accounting yet.  If you are a member of the Accounting team, you may select performance from the list below and approve them using the Approve button'
				}]
			},


			{
				title: 'Approved',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					workflowStatusNames: 'Approved',
					showPerformanceReportButton: true,
					showPerformanceReportPostButton: true,

					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance'}
					],

					instructions: 'The following performance are in Approved status and have had final approval by Accounting.'
				}]
			},

			{
				title: 'Reporting',
				items: [{
					xtype: 'composite-performance-workflow-grid',
					workflowStateNames: ['Pending Reporting', 'Reporting Complete'],
					showPerformanceReportButton: true,
					showPerformanceReportPostButton: false,

					transitionWorkflowStateList: [
						{stateName: 'Draft', iconCls: 'cancel', buttonText: 'Reject', buttonTooltip: 'Reject Selected Performance - Returns back to Draft for Edits'},
						{stateName: 'Pending Reporting', iconCls: 'undo', buttonText: 'Pending Report', buttonTooltip: 'Undo selected performance as reporting completed'},
						{stateName: 'Reporting Complete', iconCls: 'www', buttonText: 'Post/Mark Complete', buttonTooltip: 'Post Report and Mark selected performance as reporting completed'}
					],
					instructions: 'The following performance have been fully approved and are eligible for quarter end reporting.',
					getTopToolbarFilters: function(toolbar) {
						return this.getAdditionalTopToolbarFilters(toolbar).concat([
							{name: 'performanceFrequencyCombo', xtype: 'performance-frequency-combo'},
							{fieldLabel: 'Include Reporting Complete', xtype: 'toolbar-checkbox', name: 'includeReportingComplete', checked: true}
						]);
					},
					applyDefaultFilters: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('accountingPeriod.endDate', {'after': TCG.getLastDateOfPreviousQuarter().add(Date.DAY, -1)});
						}
					},
					getWorkflowStateNames: function() {
						const toolbar = this.getTopToolbar();
						const includeCompleted = TCG.getChildByName(toolbar, 'includeReportingComplete');
						if (TCG.isTrue(includeCompleted.checked)) {
							return this.workflowStateNames;
						}
						return 'Pending Reporting';
					}
				}]
			},


			{
				title: 'Rule Violations',
				items: [{
					xtype: 'base-rule-violation-grid',
					instructions: 'The following rule violations have been recorded for "Performance Composite Rules" rule category. Defaults to last 7 days.',
					staticLoadParams: {categoryNameEquals: 'Performance Composite Rules'},
					includeRuleCategoryFilter: false
				}]
			},


			{
				title: 'Administration',
				layout: 'vbox',
				layoutConfig: {
					align: 'stretch'
				},
				items: [
					{
						xtype: 'formpanel',
						labelWidth: 180,
						loadValidation: false, // using the form only to get background color/padding
						height: 290,
						buttonAlign: 'right',
						instructions: 'Use this section to (re)process multiple performance composites at one time. Select criteria below to filter which composites to (re)process.',
						items: [
							{
								xtype: 'columnpanel',
								columns: [
									{
										rows: [
											{
												fieldLabel: 'Composite Status', name: 'compositeStatus', hiddenName: 'compositeStatus', xtype: 'combo', displayField: 'name', valueField: 'value', mode: 'local', value: 'Active',
												store: {
													xtype: 'arraystore',
													fields: ['name', 'value', 'description'],
													data: Clifton.performance.composite.PerformanceCompositeStatus
												},
												qtip: 'Filter the Composite dropdown by status.'
											},
											{
												fieldLabel: 'Composite', name: 'performanceCompositeName', hiddenName: 'performanceCompositeId', xtype: 'combo', url: 'performanceCompositeListFind.json', detailPageClass: 'Clifton.performance.composite.setup.CompositeWindow',
												requiredFields: ['compositeStatus'],
												listeners: {
													beforequery: function(queryEvent) {
														const combo = queryEvent.combo;
														const f = combo.getParentForm().getForm();
														combo.store.baseParams = {compositeStatus: f.findField('compositeStatus').getValue()};
													}
												}
											},
											{fieldLabel: 'Process Period', name: 'toAccountingPeriod.label', hiddenName: 'toAccountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label', allowBlank: false},
											{
												fieldLabel: 'Start Period', name: 'fromAccountingPeriod.label', hiddenName: 'fromAccountingPeriod.id', xtype: 'combo', url: 'accountingPeriodListFind.json?orderBy=startDate:desc', displayField: 'label', requiredFields: ['toAccountingPeriod.id'],
												qtip: 'Optional start date to re-process performance from instead of just the one process period.'
											}
										]
									},
									{
										rows: [
											{xtype: 'hidden', name: 'metricProcessingType', value: 'ALL'},
											{
												boxLabel: 'Process Benchmarks Only (Existing Performance Only)', name: 'metricProcessingType', xtype: 'checkbox', submitValue: false,
												qtip: 'Will re-process benchmarks only for existing performance records.  Missing performance records will be skipped.',
												listeners: {
													check: function(obj, checked) {
														const metricProcessingType = (TCG.isTrue(checked) ? 'BENCHMARKS_ONLY' : 'ALL');
														const fp = TCG.getParentFormPanel(obj);
														fp.setFormValue('metricProcessingType', metricProcessingType);
													}
												}
											},
											{
												boxLabel: 'Transition Composite Performance To Draft Prior to Processing', name: 'transitionToDraftPriorToProcessing', xtype: 'checkbox',
												qtip: 'Prior to processing composite performance for selected period(s) will first transition composite performance to Draft.<br/><b>This should only be used if you are sure you want to re-process previously approved composite performance for your selections.</b>'
											},
											{boxLabel: 'Restrict processing to require Composite (uncheck to allow all)', name: 'restrictionRequired', xtype: 'checkbox', checked: true, qtip: '<b>Warning:</b> Should be unchecked ONLY when you want to create and process composite performance across all Composites.'}
										]
									}
								]
							}
						],


						validateLoadAll: function() {
							const restrictionRequired = this.getFormValue('restrictionRequired', true);
							if (TCG.isTrue(restrictionRequired)) {
								// All is Not Allowed - Must have Composite selected
								const composite = this.getFormValue('performanceCompositeId', true);
								if ((TCG.isNotBlank(composite))) {
									return true;
								}
								TCG.showError('You have selected to restrict processing to a Composite.  Either uncheck the restriction checkbox or select a composite.');
								return false;
							}
							return true;
						},


						buttons: [
							{
								text: 'Process Performance',
								iconCls: 'run',
								width: 150,
								tooltip: 'Calculate performance metrics ',
								handler: function() {
									const owner = this.findParentByType('formpanel');
									if (TCG.isTrue(owner.validateLoadAll())) {
										const form = owner.getForm();
										form.submit(Ext.applyIf({
											url: 'performanceCompositePerformanceRebuild.json',
											waitMsg: 'Processing...',
											success: function(form, action) {
												Ext.Msg.alert('Process Success', action.result.data, function() {
													const grid = owner.ownerCt.items.get(1);
													grid.reload.defer(300, grid);
												});
											}
										}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
									}
								}
							}]
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'COMPOSITE-PERFORMANCE-RUN',
						instantRunner: true,
						instructions: 'The following Performance Composites are being (re)processed right now.',
						title: 'Current Performance Composites',
						flex: 1
					}
				]
			}
		]
	}]
});
