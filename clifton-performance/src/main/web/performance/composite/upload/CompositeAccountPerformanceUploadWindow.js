Clifton.performance.composite.upload.CompositeAccountPerformanceUploadWindow = Ext.extend(TCG.app.DetailWindow, {
	id: 'performanceCompositeAccountPerformanceUploadWindow',
	title: 'Performance Composite Account Performance Import',
	iconCls: 'import',
	height: 600,
	width: 700,
	hideOKButton: true,


	tbar: [{
		text: 'Sample File',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File For Selected Table (Max 10 Rows of Data)',
		handler: function() {
			const f = this.ownerCt.ownerCt.items.get(0);
			f.getWindow().getMainFormPanel().downloadSampleFile(false);
		}
	}, '-', {
		text: 'Sample File (All Data)',
		iconCls: 'excel',
		tooltip: 'Download Excel Sample File For Selected Table (Contains All (up to 500 rows) Existing Data)',
		handler: function() {
			const f = this.ownerCt.ownerCt.items.get(0);
			f.getWindow().getMainFormPanel().downloadSampleFile(true);
		}
	}
	],

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		instructions: 'Use the following to upload account monthly return information into IMS.  You can optionally delete any existing daily performance data for each month uploaded (useful for cases where IMS is used to generate prelim values, however we need to upload external values for final returns). Also, you can automatically re-process the account/periods from the upload to automatically fill in benchmarks returns, model net return, and all historical data (QTD, YTD, etc.).',

		getDefaultData: function(win) {
			if (win.defaultData && win.defaultData.tableName) {
				this.loadNaturalKeys(win.defaultData.tableName);
			}
			return win.defaultData;
		},
		loadNaturalKeys: function(tableName) {
			let naturalKey = '';
			if (TCG.isNotBlank(tableName)) {
				naturalKey = TCG.getResponseText('systemUploadTableDefaultNaturalKey.json?tableName=' + tableName, this);
			}
			this.getForm().findField('systemUploadNaturalKey').setValue(naturalKey);
		},
		items: [
			{xtype: 'hidden', name: 'enableOpenSessionInView', value: 'true'},
			{fieldLabel: 'Table', name: 'tableName', xtype: 'hidden', value: 'PerformanceCompositeInvestmentAccountPerformance'},
			{fieldLabel: 'File', name: 'file', xtype: 'fileuploadfield', allowBlank: false},
			{xtype: 'sectionheaderfield', header: 'Performance Account Upload Options'},
			{
				fieldLabel: '', boxLabel: 'Delete any daily performance for each period included in the upload file', name: 'deleteDailyPerformance', xtype: 'checkbox',
				qtip: 'Useful for accounts were IMS was used to generate preliminary results, which are then removed for externally uploaded daily values.'
			},
			{fieldLabel: '', boxLabel: 'Process account level performance for each month uploaded (Rebuilds Benchmarks, and Historical Returns (QTD, YTD, etc).', name: 'processPerformance', xtype: 'checkbox'},
			{xtype: 'sectionheaderfield', header: 'If uploaded row already exists in the system'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'existingBeans',
				items: [
					{fieldLabel: '', boxLabel: 'Skip upload for rows that already exist.', name: 'existingBeans', inputValue: 'SKIP', checked: true},
					{fieldLabel: '', boxLabel: 'Overwrite existing data with uploaded information. (Columns not included in the file will NOT overwrite existing data in the database)', name: 'existingBeans', inputValue: 'UPDATE'},
					{fieldLabel: '', boxLabel: 'Insert all data to improve upload performance (Fails if a constraint is violated).', name: 'existingBeans', inputValue: 'INSERT'}
				]
			},
			{
				xtype: 'fieldset', title: 'Natural Key Overrides', collapsible: true, collapsed: true,
				items: [

					{xtype: 'label', html: 'For the uploaded table, you can chose to override the natural key used to find unique rows. Note: This is not common and should be used rarely.'},
					{fieldLabel: 'Default Natural Key', name: 'systemUploadNaturalKey', submitValue: false, readOnly: true},
					{
						xtype: 'listfield',
						allowBlank: true,
						name: 'overrideTableNaturalKeyBeanProperties',
						requiredFields: ['tableName'],
						controlConfig: {
							fieldLabel: 'Column', name: 'naturalKeyColumn.label', hiddenName: 'naturalKeyColumn.beanPropertyName', displayField: 'label', valueField: 'beanPropertyName', xtype: 'combo',
							url: 'systemColumnStandardListFind.json?excludeRecordStampColumns=true',
							beforequery: function(queryEvent) {
								const combo = queryEvent.combo;
								const f = combo.getParentForm().getForm();
								if (TCG.isBlank(f.findField('tableName').getValue())) {
									return false;
								}
								combo.store.baseParams = {tableName: f.findField('tableName').getValue()};
							}
						}
					}
				]
			},


			{xtype: 'sectionheaderfield', header: 'Upload Results'},
			{name: 'uploadResultsString', xtype: 'textarea', readOnly: true, submitField: false}
		],

		getSaveURL: function() {
			return 'performanceCompositeInvestmentAccountPerformanceUploadFileUpload.json';
		},

		downloadSampleFile: function(allRows) {
			const f = this.getForm();
			const tableName = f.findField('tableName').value;
			if (TCG.isBlank(tableName)) {
				TCG.showError('A table is required in order to generate a sample upload file.');
				return;
			}
			const params = this.getFormValuesFormatted(true);
			// Note: Value not used for sample downloads and returns component not value because its a radio group
			if (params.existingBeans) {
				params.existingBeans = '';
			}
			Clifton.system.downloadSampleUploadFile(tableName, allRows, this, params);
		}
	}]
});
