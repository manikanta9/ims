Clifton.performance.report.PerformanceReportPostingWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Post Preliminary GIPS Performance Report to Client Website',
	iconCls: 'pdf',
	hideOKButton: true,
	hideApplyButton: true,
	hideCancelButton: true,
	doNotWarnOnCloseModified: true,

	height: 550,
	width: 750,

	defaultDataIsReal: true,
	loadValidation: false,


	items: [{
		xtype: 'formpanel',

		listeners: {
			afterload: function(form, isClosing) {
				this.setResetPostEntityFields();
			}
		},

		items: [
			{xtype: 'hidden', name: 'performanceCompositePerformanceId'},
			{xtype: 'hidden', name: 'performanceCompositeInvestmentAccountPerformanceId'},
			{fieldLabel: 'Performance Entity', name: 'label', xtype: 'displayfield'},
			{fieldLabel: 'Report', name: 'reportName', hiddenName: 'exportConfiguration.reportId', xtype: 'combo', url: 'reportListFind.json?templateName=Performance Strategy Report'},
			{
				fieldLabel: 'Report Date', name: 'reportDate', xtype: 'datefield',
				listeners: {
					select: function(field) {
						const f = TCG.getParentFormPanel(this);
						f.updateDateFields(field.getValue());
					},
					change: function(field) {
						const f = TCG.getParentFormPanel(this);
						f.updateDateFields(field.getValue());
					},
					specialkey: function(field, e) {
						if (e.getKey() === e.ENTER) {
							const f = TCG.getParentFormPanel(this);
							f.updateDateFields(field.getValue());
						}
					}
				}
			},
			{fieldLabel: 'Publish Date', name: 'publishDate', xtype: 'datefield', value: (new Date())},

			{xtype: 'hidden', name: 'postEntitySourceTableName'},
			{xtype: 'hidden', name: 'postEntitySourceFkFieldId'},
			{xtype: 'hidden', name: 'sourceEntitySectionName'},
			{xtype: 'hidden', name: 'sourceEntityFkFieldId'},
			{
				fieldLabel: 'File Group', name: 'portalFileCategoryName', hiddenName: 'portalFileCategoryId', xtype: 'combo', url: 'portalFileCategoryForUploadListFind.json', allowBlank: false,
				requestedProps: 'portalFileFrequency',
				listeners: {
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						if (record.json) {
							if (!TCG.isBlank(record.json.portalFileFrequency)) {
								fp.setFormValue('portalFileFrequency', record.json.portalFileFrequency);
							}
						}
					}
				}
			},
			{
				fieldLabel: 'File Frequency', name: 'portalFileFrequency', hiddenName: 'portalFileFrequency', xtype: 'combo', mode: 'local', requiredFields: ['portalFileCategoryName'],
				store: {
					xtype: 'arraystore',
					data: Clifton.portal.file.PortalFileFrequencies
				}
			},
			{xtype: 'hidden', name: 'postEntityType.id', submitValue: false},
			{xtype: 'sectionheaderfield', header: 'Post Information'},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: '', name: 'postAssignmentOptions',
				items: [
					{
						fieldLabel: '', boxLabel: 'Default Based on Selected Performance Entity', name: 'postAssignmentOptions', inputValue: '', checked: true,
						qtip: 'For account performance, this would by default post to the client account.  Composite Performance would post to the composite.  You can optionally change this, i.e. post Fund Account performance to the Commingled Vehicle, which will assign the file to all investors in the Fund.'
					},
					{
						fieldLabel: '', boxLabel: 'One File Assignment', name: 'postAssignmentOptions', inputValue: 'ONE', checked: false,
						qtip: 'Assign this file to a single relationship, client, or account (based on file group selection).'
					},
					{
						fieldLabel: '', boxLabel: 'Rollup File Assignment', name: 'postAssignmentOptions', inputValue: 'ROLLUP',
						qtip: 'Assign this file to a a roll up (client group, commingled vehicle) which will assign it to all clients or client accounts for that rollup.'
					}
				],
				listeners: {
					change: function(f) {
						const p = TCG.getParentFormPanel(f);
						p.setResetPostEntityFields();
					}
				}
			},
			{xtype: 'hidden', name: 'postPortalEntityId'},

			// One File Assignment Fields
			{fieldLabel: 'Post Section', name: 'onePostEntitySourceSectionName', hiddenName: 'onePostEntitySourceSectionId', xtype: 'combo', url: 'portalEntitySourceSectionListFind.json?securableEntity=true&postableEntity=true', doNotSubmitValue: true, requiredFields: ['portalFileCategoryName'], displayField: 'nameWithSourceSystem'},
			{
				fieldLabel: 'Post Entity', name: 'onePostPortalEntityLabel', hiddenName: 'onePostPortalEntityId', requiredFields: ['portalFileCategoryName'], xtype: 'combo', url: 'portalEntityListFind.json?active=true', detailPageClass: 'Clifton.portal.entity.EntityWindow', displayField: 'label', allowBlank: false, doNotSubmitValue: true,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						combo.store.setBaseParam('entityTypeId', fp.getForm().findField('postEntityType.id').getValue());
						combo.store.setBaseParam('entitySourceSectionId', fp.getForm().findField('onePostEntitySourceSectionName').getValue());
					},
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.setFormValue('postPortalEntityId', record.json.id);
					}

				}
			},

			// Rollup File Assignment Fields
			{
				fieldLabel: 'Rollup Section', hidden: true, name: 'rollupPostEntitySourceSectionName', hiddenName: 'rollupPostEntitySourceSectionId', xtype: 'combo', url: 'portalEntitySourceSectionListFind.json?postableEntity=true', doNotSubmitValue: true, requiredFields: ['portalFileCategoryName'], displayField: 'nameWithSourceSystem',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						combo.store.setBaseParam('childEntityTypeId', fp.getForm().findField('postEntityType.id').getValue());
					}
				}
			},
			{
				fieldLabel: 'Rollup Entity', hidden: true, name: 'rollupPostPortalEntityLabel', hiddenName: 'rollupPostPortalEntityId', requiredFields: ['rollupPostEntitySourceSectionName'], xtype: 'combo', url: 'portalEntityListFind.json?active=true', detailPageClass: 'Clifton.portal.entity.EntityWindow', displayField: 'label', allowBlank: false, doNotSubmitValue: true,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						combo.store.setBaseParam('entitySourceSectionId', fp.getForm().findField('rollupPostEntitySourceSectionName').getValue());
					},
					select: function(combo, record, index) {
						const fp = combo.getParentForm();
						fp.setFormValue('postPortalEntityId', record.json.id);
					}
				}
			},
			{
				xtype: 'radiogroup', columns: 1, fieldLabel: 'If File Exists',
				items: [
					{boxLabel: 'Error', name: 'fileDuplicateAction', inputValue: 'ERROR', checked: true, qtip: 'Returns an error if file appears to be a duplicate on the new portal'},
					{boxLabel: 'Replace (If Existing file is Unapproved Only)', name: 'fileDuplicateAction', inputValue: 'REPLACE', qtip: 'Replacing the file is only allowed if the file that is being replaced is not currently approved.'},
					{boxLabel: 'Skip', name: 'fileDuplicateAction', inputValue: 'SKIP', qtip: 'Do Nothing - Leave the existing file as is and do not post this one.'}
				]
			}
		],

		setResetPostEntityFields: function() {
			const fp = this;
			const postAssignmentOption = fp.getForm().findField('postAssignmentOptions');

			// Clear All Values When Changed
			fp.setFormValue('postPortalEntityId', ''); // Clear Post Entity
			fp.getForm().findField('onePostPortalEntityLabel').clearAndReset();
			fp.getForm().findField('rollupPostPortalEntityLabel').clearAndReset();

			if (postAssignmentOption.getValue().getGroupValue() === 'ONE') {
				if (TCG.isBlank(fp.getFormValue('postEntityType.id'))) {
					fp.showField('onePostEntitySourceSectionName');
				}
				else {
					fp.hideField('onePostEntitySourceSectionName');
				}
				fp.showField('onePostPortalEntityLabel');
				fp.getForm().findField('onePostPortalEntityLabel').allowBlank = false;
				fp.hideField('rollupPostEntitySourceSectionName');
				fp.hideField('rollupPostPortalEntityLabel');
				fp.getForm().findField('rollupPostPortalEntityLabel').allowBlank = true;
			}
			else if (postAssignmentOption.getValue().getGroupValue() === 'ROLLUP') {
				fp.showField('rollupPostEntitySourceSectionName');
				fp.showField('rollupPostPortalEntityLabel');
				fp.getForm().findField('rollupPostPortalEntityLabel').allowBlank = false;

				fp.hideField('onePostEntitySourceSectionName');
				fp.hideField('onePostPortalEntityLabel');
				fp.getForm().findField('onePostPortalEntityLabel').allowBlank = true;
			}
			else {
				fp.hideField('rollupPostEntitySourceSectionName');
				fp.hideField('rollupPostPortalEntityLabel');
				fp.getForm().findField('rollupPostPortalEntityLabel').allowBlank = true;
				fp.hideField('onePostEntitySourceSectionName');
				fp.hideField('onePostPortalEntityLabel');
				fp.getForm().findField('onePostPortalEntityLabel').allowBlank = true;
			}
		},

		buttons: [{
			text: 'Post Report',
			handler: function() {
				const f = this.findParentByType('formpanel');
				const win = f.getWindow();

				f.getForm().submit(Ext.applyIf({
					url: 'performanceReportPostingFileConfigurationPreliminaryPost.json?disableValidatingBindingValidation=true',
					waitMsg: 'Posting...',
					success: function(form, action) {
						Ext.Msg.alert('Report Posted Successfully', 'Posted Successfully.');
						win.closeWindow();
					},
					error: function(form, action) {
						Ext.Msg.alert('Failed to post document', action.result.data.fileWrapper.fileName + ' posted failure');
					}
				}, Ext.applyIf({timeout: 16000}, TCG.form.submitDefaults)));
			}
		}]
	}]
});
