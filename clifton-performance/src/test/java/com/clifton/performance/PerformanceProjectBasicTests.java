package com.clifton.performance;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * The <code>PerformanceProjectBasicTests</code> ...
 *
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PerformanceProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "performance";
	}


	@Override
	protected void configureApprovedClassImports(@SuppressWarnings("unused") List<String> imports) {
		imports.add("org.springframework.util.StringUtils");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("type");
		approvedList.add("metric");
		approvedList.add("fee");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		String methodName = method.getName();

		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("savePerformanceCompositeHistory");
		skipMethods.add("deletePerformanceCompositeHistory");
		return skipMethods.contains(methodName);
	}
}
