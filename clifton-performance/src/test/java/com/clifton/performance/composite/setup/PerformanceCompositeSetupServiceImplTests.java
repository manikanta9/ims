package com.clifton.performance.composite.setup;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.dataaccess.dao.xml.XmlUpdatableDAO;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.performance.composite.setup.search.PerformanceCompositeSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PerformanceCompositeSetupServiceImplTests {

	@Resource
	private PerformanceCompositeSetupService performanceCompositeSetupService;

	@Resource
	private XmlUpdatableDAO<PerformanceCompositeHistory> performanceCompositeHistoryDAO;

	@Resource
	private XmlReadOnlyDAO<PerformanceCompositeRollup> performanceCompositeRollupDAO;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	private void resetSetupService() {
		PerformanceCompositeSearchForm searchForm = new PerformanceCompositeSearchForm();
		List<PerformanceComposite> compositeList = this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm);
		for (PerformanceComposite composite : compositeList) {
			this.performanceCompositeSetupService.deletePerformanceComposite(composite.getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCreateCompositeMissingStartDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			PerformanceComposite composite = createPerformanceCompositeBean("Test Composite 1", null, null, false);
			this.performanceCompositeSetupService.savePerformanceComposite(composite);
		}, "Start Date is required for a composite.");
	}


	@Test
	public void testCreateRollupMissingChildren() {
		TestUtils.expectException(ValidationException.class, () -> {
			PerformanceComposite composite = createPerformanceCompositeBean("Test Composite 1", null, null, true);
			this.performanceCompositeSetupService.savePerformanceComposite(composite);
		}, "A rollup composite must have at least one child composite.");
	}


	@Test
	public void testRollupStartEndDates() {
		this.performanceCompositeRollupDAO.setLogicalEvaluatesToTrue(true);

		PerformanceComposite child1 = createPerformanceCompositeBean("Test Composite 1", DateUtils.toDate("01/01/2015"), null, false);
		this.performanceCompositeSetupService.savePerformanceComposite(child1);

		PerformanceComposite child2 = createPerformanceCompositeBean("Test Composite 2", DateUtils.toDate("02/01/2015"), null, false);
		this.performanceCompositeSetupService.savePerformanceComposite(child2);

		PerformanceComposite rollup12 = createPerformanceCompositeBean("Rollup Composite 1-2", null, null, true);
		rollup12.setChildCompositeList(CollectionUtils.createList(child1, child2));
		this.performanceCompositeSetupService.savePerformanceComposite(rollup12);

		// Validate Lists Return Correctly
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeParentList(child1.getId()).size());
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeParentList(child2.getId()).size());

		rollup12 = this.performanceCompositeSetupService.getPerformanceComposite(rollup12.getId());
		Assertions.assertEquals("01/01/2015", DateUtils.fromDateShort(rollup12.getStartDate()));
		Assertions.assertNull(rollup12.getEndDate());

		child2.setStartDate(DateUtils.toDate("12/31/2014"));
		this.performanceCompositeSetupService.savePerformanceComposite(child2);

		rollup12 = this.performanceCompositeSetupService.getPerformanceComposite(rollup12.getId());
		Assertions.assertEquals("12/31/2014", DateUtils.fromDateShort(rollup12.getStartDate()));
		Assertions.assertNull(rollup12.getEndDate());

		child1.setEndDate(DateUtils.toDate("12/31/2015"));
		this.performanceCompositeSetupService.savePerformanceComposite(child1);

		rollup12 = this.performanceCompositeSetupService.getPerformanceComposite(rollup12.getId());
		Assertions.assertEquals("12/31/2014", DateUtils.fromDateShort(rollup12.getStartDate()));
		Assertions.assertNull(rollup12.getEndDate());

		child2.setEndDate(DateUtils.toDate("01/31/2016"));
		this.performanceCompositeSetupService.savePerformanceComposite(child2);

		rollup12 = this.performanceCompositeSetupService.getPerformanceComposite(rollup12.getId());
		Assertions.assertEquals("12/31/2014", DateUtils.fromDateShort(rollup12.getStartDate()));
		Assertions.assertEquals("01/31/2016", DateUtils.fromDateShort(rollup12.getEndDate()));

		child2.setEndDate(null);
		this.performanceCompositeSetupService.savePerformanceComposite(child2);

		rollup12 = this.performanceCompositeSetupService.getPerformanceComposite(rollup12.getId());
		Assertions.assertEquals("12/31/2014", DateUtils.fromDateShort(rollup12.getStartDate()));
		Assertions.assertNull(rollup12.getEndDate());
	}


	@Test
	public void testRollupOfRollupInvalid() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.performanceCompositeRollupDAO.setLogicalEvaluatesToTrue(true);

			PerformanceComposite child1 = createPerformanceCompositeBean("Test Composite 1", DateUtils.toDate("01/01/2015"), null, false);
			this.performanceCompositeSetupService.savePerformanceComposite(child1);

			PerformanceComposite child2 = createPerformanceCompositeBean("Test Composite 2", DateUtils.toDate("02/01/2015"), null, false);
			this.performanceCompositeSetupService.savePerformanceComposite(child2);

			PerformanceComposite rollup12 = createPerformanceCompositeBean("Rollup Composite 1-2", null, null, true);
			rollup12.setChildCompositeList(CollectionUtils.createList(child1, child2));
			this.performanceCompositeSetupService.savePerformanceComposite(rollup12);

			// Create another rollup of the current rollup - Not Allowed
			PerformanceComposite rollupOfRollup = createPerformanceCompositeBean("Rollup Composite of Rollup", null, null, true);
			rollupOfRollup.setChildCompositeList(CollectionUtils.createList(rollup12));
			this.performanceCompositeSetupService.savePerformanceComposite(rollupOfRollup);
		}, "Child composite [Rollup Composite 1-2] is invalid.  You cannot have children composites that are also rollup composites.");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	////////////////////////////////////////////////////////////////////////////////
	// NOTE: THE FOLLOWING TESTS CALL THE DAO METHOD DIRECTLY TO TEST THE VALIDATOR LOGIC
	// BELOW ARE SPECIFIC TESTS TO VALIDATE THE CUSTOMIZED SAVE/DELETE LOGIC
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidCompositeHistory_MissingCompositeField() {
		TestUtils.expectException(ValidationException.class, () -> {
			PerformanceCompositeHistory historyBean = new PerformanceCompositeHistory();
			this.performanceCompositeHistoryDAO.save(historyBean);
		}, "Performance Composite is Required");
	}


	@Test
	public void testInvalidCompositeHistory_MissingStartDateField() {
		TestUtils.expectException(ValidationException.class, () -> {
			PerformanceComposite composite = createPerformanceCompositeBean("Test Composite A", DateUtils.toDate("01/01/2015"), null, false);
			this.performanceCompositeSetupService.savePerformanceComposite(composite);
			PerformanceCompositeHistory historyBean = new PerformanceCompositeHistory();
			historyBean.setPerformanceComposite(composite);
			this.performanceCompositeHistoryDAO.save(historyBean);
		}, "Start Date is Required");
	}


	@Test
	public void testInvalidCompositeHistory_Rollup() {
		TestUtils.expectException(ValidationException.class, () -> {
			PerformanceComposite composite = createPerformanceCompositeBean("Test Composite C", DateUtils.toDate("01/01/2015"), null, true);
			// For the test - not actually saving the rollup composite to avoid validation on the rollup itself, but for this test it checks the flag on the object during validation
			PerformanceCompositeHistory historyBean = new PerformanceCompositeHistory();
			historyBean.setPerformanceComposite(composite);
			historyBean.setStartDate(DateUtils.toDate("01/01/2015"));
			historyBean.setEndDate(DateUtils.toDate("03/31/2015"));
			this.performanceCompositeHistoryDAO.save(historyBean);
		}, "Model fee on rollup composites is not allowed.");
	}


	@Test
	public void testInvalidCompositeHistory_OverlapDates() {
		TestUtils.expectException(ValidationException.class, () -> {
			PerformanceComposite composite = createPerformanceCompositeBean("Test Composite D", DateUtils.toDate("01/01/2015"), null, false);
			this.performanceCompositeSetupService.savePerformanceComposite(composite);

			PerformanceCompositeHistory historyBean = new PerformanceCompositeHistory();
			historyBean.setPerformanceComposite(composite);
			historyBean.setStartDate(DateUtils.toDate("01/01/2015"));
			historyBean.setEndDate(DateUtils.toDate("03/31/2015"));
			this.performanceCompositeHistoryDAO.save(historyBean);

			PerformanceCompositeHistory history2Bean = new PerformanceCompositeHistory();
			history2Bean.setPerformanceComposite(composite);
			history2Bean.setStartDate(DateUtils.toDate("04/01/2015"));
			history2Bean.setEndDate(DateUtils.toDate("06/30/2015"));
			this.performanceCompositeHistoryDAO.save(history2Bean);

			PerformanceCompositeHistory history3Bean = new PerformanceCompositeHistory();
			history3Bean.setPerformanceComposite(composite);
			history3Bean.setStartDate(DateUtils.toDate("06/01/2015"));
			history3Bean.setEndDate(DateUtils.toDate("09/30/2015"));
			this.performanceCompositeHistoryDAO.save(history3Bean);
		}, "Performance Composite History record already exists for overlapping date range (04/01/2015 - 06/30/2015)");
	}

	///  SERVICE METHOD LOGIC TESTS


	@Test
	public void testValidCompositeHistory() {
		PerformanceComposite composite = createPerformanceCompositeBean("Test Composite E", DateUtils.toDate("01/01/2015"), null, false);
		this.performanceCompositeSetupService.savePerformanceComposite(composite);

		// No start Date Implies changing the existing model fee
		PerformanceCompositeHistory historyBean = new PerformanceCompositeHistory();
		historyBean.setPerformanceComposite(composite);
		historyBean.setModelFee(new BigDecimal("0.6"));
		this.performanceCompositeSetupService.savePerformanceCompositeHistory(historyBean);

		// Changed existing fee only - no history should have been saved
		composite = this.performanceCompositeSetupService.getPerformanceComposite(composite.getId());
		Assertions.assertEquals(new BigDecimal("0.6"), composite.getModelFee());

		List<PerformanceCompositeHistory> historyList = this.performanceCompositeSetupService.getPerformanceCompositeHistoryListForComposite(composite.getId());
		Assertions.assertTrue(CollectionUtils.isEmpty(historyList));

		// Now add a new model fee
		historyBean = new PerformanceCompositeHistory();
		historyBean.setPerformanceComposite(composite);
		historyBean.setModelFee(new BigDecimal("0.5"));
		historyBean.setStartDate(DateUtils.toDate("01/01/2016"));
		this.performanceCompositeSetupService.savePerformanceCompositeHistory(historyBean);

		// Check Fee on the Composite
		composite = this.performanceCompositeSetupService.getPerformanceComposite(composite.getId());
		Assertions.assertEquals(new BigDecimal("0.5"), composite.getModelFee());

		// Validate History
		historyList = this.performanceCompositeSetupService.getPerformanceCompositeHistoryListForComposite(composite.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));
		historyList = BeanUtils.sortWithFunction(historyList, PerformanceCompositeHistory::getStartDate, true);

		PerformanceCompositeHistory firstHistory = historyList.get(0);
		Assertions.assertEquals(DateUtils.toDate("01/01/2015"), firstHistory.getStartDate());
		Assertions.assertEquals(DateUtils.toDate("12/31/2015"), firstHistory.getEndDate());
		Assertions.assertEquals(new BigDecimal("0.6"), firstHistory.getModelFee());

		PerformanceCompositeHistory secondHistory = historyList.get(1);
		Assertions.assertEquals(DateUtils.toDate("01/01/2016"), secondHistory.getStartDate());
		Assertions.assertNull(secondHistory.getEndDate());
		Assertions.assertEquals(new BigDecimal("0.5"), secondHistory.getModelFee());

		// Try to Delete first history - should get an error
		String errorMessage = null;
		try {
			this.performanceCompositeSetupService.deletePerformanceCompositeHistory(firstHistory.getId());
		}
		catch (Exception e) {
			errorMessage = e.getMessage();
		}
		Assertions.assertEquals("You can only delete the latest history record for Performance Composites.", errorMessage);

		// Add another fee
		historyBean = new PerformanceCompositeHistory();
		historyBean.setPerformanceComposite(composite);
		historyBean.setModelFee(new BigDecimal("0.75"));
		historyBean.setStartDate(DateUtils.toDate("04/01/2016"));
		this.performanceCompositeSetupService.savePerformanceCompositeHistory(historyBean);

		// Check Fee on the Composite
		composite = this.performanceCompositeSetupService.getPerformanceComposite(composite.getId());
		Assertions.assertEquals(new BigDecimal("0.75"), composite.getModelFee());

		// Validate History
		historyList = this.performanceCompositeSetupService.getPerformanceCompositeHistoryListForComposite(composite.getId());
		Assertions.assertEquals(3, CollectionUtils.getSize(historyList));

		// Delete the latest one
		this.performanceCompositeSetupService.deletePerformanceCompositeHistory(historyBean.getId());

		// Check Fee on the Composite
		composite = this.performanceCompositeSetupService.getPerformanceComposite(composite.getId());
		Assertions.assertEquals(new BigDecimal("0.5"), composite.getModelFee());

		// Validate History
		historyList = this.performanceCompositeSetupService.getPerformanceCompositeHistoryListForComposite(composite.getId());
		Assertions.assertEquals(2, CollectionUtils.getSize(historyList));
		historyList = BeanUtils.sortWithFunction(historyList, PerformanceCompositeHistory::getStartDate, true);

		firstHistory = historyList.get(0);
		Assertions.assertEquals(DateUtils.toDate("01/01/2015"), firstHistory.getStartDate());
		Assertions.assertEquals(DateUtils.toDate("12/31/2015"), firstHistory.getEndDate());
		Assertions.assertEquals(new BigDecimal("0.6"), firstHistory.getModelFee());

		secondHistory = historyList.get(1);
		Assertions.assertEquals(DateUtils.toDate("01/01/2016"), secondHistory.getStartDate());
		Assertions.assertNull(secondHistory.getEndDate());
		Assertions.assertEquals(new BigDecimal("0.5"), secondHistory.getModelFee());
	}

	////////////////////////////////////////////////////////////////////////////////
	// ACTIVE/INACTIVE COMPOSITE TESTS                                            //
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOneActiveComposite() {
		PerformanceComposite composite = createPerformanceCompositeBean("Test Composite 1", DateUtils.toDate("01/01/2015"), null, false);
		this.performanceCompositeSetupService.savePerformanceComposite(composite);
		PerformanceCompositeSearchForm searchForm = new PerformanceCompositeSearchForm();
		searchForm.setCompositeStatus("Active");
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //One active composite
		searchForm.setCompositeStatus("Inactive");
		Assertions.assertEquals(0, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //No inactive composites
		searchForm.setCompositeStatus("Both");
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //One total composite
	}


	@Test
	public void testOneInactiveComposite() {
		PerformanceComposite composite = createPerformanceCompositeBean("Test Composite 1", DateUtils.toDate("01/01/2015"), DateUtils.toDate("01/01/2016"), false);
		this.performanceCompositeSetupService.savePerformanceComposite(composite);
		PerformanceCompositeSearchForm searchForm = new PerformanceCompositeSearchForm();
		searchForm.setCompositeStatus("Active");
		Assertions.assertEquals(0, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //No active composites
		searchForm.setCompositeStatus("Inactive");
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //One inactive composite
		searchForm.setCompositeStatus("Both");
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //One total composite
	}


	@Test
	public void testActiveAndInactiveComposites() {
		PerformanceComposite composite = createPerformanceCompositeBean("Test Composite 1", DateUtils.toDate("01/01/2015"), null, false);
		this.performanceCompositeSetupService.savePerformanceComposite(composite);
		PerformanceComposite composite2 = createPerformanceCompositeBean("Test Composite 2", DateUtils.toDate("01/01/2015"), DateUtils.toDate("01/01/2016"), false);
		this.performanceCompositeSetupService.savePerformanceComposite(composite2);
		PerformanceCompositeSearchForm searchForm = new PerformanceCompositeSearchForm();
		searchForm.setCompositeStatus("Active");
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //One active composite
		searchForm.setCompositeStatus("Inactive");
		Assertions.assertEquals(1, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //One inactive composite
		searchForm.setCompositeStatus("Both");
		Assertions.assertEquals(2, this.performanceCompositeSetupService.getPerformanceCompositeList(searchForm).size()); //Two total composites
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private PerformanceComposite createPerformanceCompositeBean(String name, Date startDate, Date endDate, boolean rollup) {
		PerformanceComposite composite = new PerformanceComposite();
		composite.setName(name);
		composite.setStartDate(startDate);
		composite.setEndDate(endDate);
		composite.setRollupComposite(rollup);
		if (!rollup) {
			composite.setModelFee(new BigDecimal(0.5));
		}
		return composite;
	}
}
