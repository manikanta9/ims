package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.accounting.period.AccountingPeriodService;
import com.clifton.accounting.period.AccountingPeriodServiceImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceService;
import com.clifton.performance.composite.performance.PerformanceCompositePerformanceServiceImpl;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import com.clifton.performance.composite.setup.PerformanceCompositeInvestmentAccount;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupService;
import com.clifton.performance.composite.setup.PerformanceCompositeSetupServiceImpl;
import com.clifton.system.bean.SystemBean;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>BasePerformanceAccountCalculatorTests</code> base methods for assisting in the testing of various performance calculators.
 *
 * @author apopp
 */
@ContextConfiguration(locations = "../../../../PerformanceProjectBasicTests-context.xml")
@ExtendWith(SpringExtension.class)
public abstract class BasePerformanceAccountCalculatorTests implements ApplicationContextAware {

	@Resource
	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////////


	protected abstract BasePerformanceCalculator getPerformanceCalculatorImpl();


	protected BasePerformanceCalculator getPerformanceCalculator() {
		BasePerformanceCalculator performanceCalculator = getPerformanceCalculatorImpl();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(performanceCalculator, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return performanceCalculator;
	}

	////////////////////////////////////////////////////////////////////////////////


	protected void validateCalculation(String previousMonthEndDateString, String startDateString, String endDateString, String managementFee, String previousMonthEndMarketValue, String endMonthMarketValue, String firstOfMonthNetCashFlow, String firstOfMonthWeightedNetCashFlow, String lastOfMonthGainLoss, String expectedGrossReturn, String expectedNetReturn) {
		BasePerformanceCalculator calculator = getPerformanceCalculator();

		Date previousMonthEndDate = DateUtils.toDate(previousMonthEndDateString);
		Date startDate = DateUtils.toDate(startDateString);
		Date endDate = DateUtils.toDate(endDateString);

		AccountingPeriod accountingPeriod = new AccountingPeriod();
		accountingPeriod.setStartDate(DateUtils.getFirstDayOfMonth(startDate));
		accountingPeriod.setEndDate(DateUtils.getLastDayOfMonth(endDate));
		accountingPeriod.setId(1);

		InvestmentAccount investmentAccount = new InvestmentAccount();
		investmentAccount.setId(1);

		PerformanceCompositeInvestmentAccount assignment = new PerformanceCompositeInvestmentAccount();
		assignment.setInvestmentAccount(investmentAccount);
		assignment.setAccountPerformanceCalculatorBean(new SystemBean());
		assignment.getAccountPerformanceCalculatorBean().setId(1);

		PerformanceCompositeInvestmentAccountPerformance monthlyPerformance = PerformanceCompositeInvestmentAccountPerformance.ofAccountAssignmentAndPeriod(assignment, accountingPeriod);
		monthlyPerformance.setManagementFeeOverride(new BigDecimal(managementFee));
		monthlyPerformance.setAccountingPeriod(accountingPeriod);

		PerformanceCompositeInvestmentAccountDailyPerformance previousMonthEndDailyPerformance = new PerformanceCompositeInvestmentAccountDailyPerformance(monthlyPerformance, previousMonthEndDate);
		PerformanceCompositeInvestmentAccountDailyPerformance startDailyPerformance = new PerformanceCompositeInvestmentAccountDailyPerformance(monthlyPerformance, startDate);
		PerformanceCompositeInvestmentAccountDailyPerformance endDailyPerformance = new PerformanceCompositeInvestmentAccountDailyPerformance(monthlyPerformance, endDate);

		PerformanceCompositeAccountRuleEvaluatorContext config = new PerformanceCompositeAccountRuleEvaluatorContext();
		PerformanceCompositePerformanceService performanceCompositePerformanceService = Mockito.mock(PerformanceCompositePerformanceServiceImpl.class);
		PerformanceCompositeSetupService performanceCompositeSetupService = Mockito.mock(PerformanceCompositeSetupServiceImpl.class);
		AccountingPeriodService accountingPeriodService = Mockito.mock(AccountingPeriodServiceImpl.class);
		config.setPerformanceCompositePerformanceService(performanceCompositePerformanceService);
		config.setAccountingPeriodService(accountingPeriodService);
		config.setPerformanceCompositeSetupService(performanceCompositeSetupService);

		Mockito.when(performanceCompositeSetupService.getPerformanceCompositeInvestmentAccountListByAccountAndPeriod(Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(performanceCompositePerformanceService.getPerformanceCompositeInvestmentAccountPerformanceByAssignmentAndPeriod(Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);
		Mockito.when(accountingPeriodService.getAccountingPeriodForDate(Mockito.any(Date.class))).thenReturn(new AccountingPeriod());
		Mockito.when(accountingPeriodService.getAccountingPeriodClosingByPeriodAndAccount(Mockito.anyInt(), Mockito.anyInt())).thenReturn(null);

		config.setPerformanceCompositeInvestmentAccountPerformance(monthlyPerformance);
		config.setPerformanceCompositeInvestmentAccountDailyPerformanceList(CollectionUtils.createList(previousMonthEndDailyPerformance, startDailyPerformance, endDailyPerformance), true, false);

		previousMonthEndDailyPerformance.setPositionValue(new BigDecimal(previousMonthEndMarketValue));

		startDailyPerformance.setNetCashFlow(new BigDecimal(firstOfMonthNetCashFlow));
		startDailyPerformance.setWeightedNetCashFlow(new BigDecimal(firstOfMonthWeightedNetCashFlow));
		startDailyPerformance.setPositionValue(BigDecimal.ZERO);

		endDailyPerformance.setPositionValue(new BigDecimal(endMonthMarketValue));
		endDailyPerformance.setGainLoss(new BigDecimal(lastOfMonthGainLoss));

		calculator.calculate(config, monthlyPerformance);

		BigDecimal grossReturn = MathUtils.round(monthlyPerformance.getPeriodGrossReturn(), 4);
		BigDecimal netReturn = MathUtils.round(monthlyPerformance.getPeriodNetReturn(), 4);

		Assertions.assertTrue(MathUtils.isEqual(grossReturn, new BigDecimal(expectedGrossReturn)), "Expected gross return of [" + expectedGrossReturn + "] but calculated [" + grossReturn + "]");
		Assertions.assertTrue(MathUtils.isEqual(netReturn, new BigDecimal(expectedNetReturn)), "Expected net return of [" + expectedNetReturn + "] but calculated [" + netReturn + "]");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
