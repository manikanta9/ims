package com.clifton.performance.composite.performance.process.calculators;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountDailyPerformance;
import com.clifton.performance.composite.performance.PerformanceCompositeInvestmentAccountPerformance;
import com.clifton.performance.composite.performance.rule.context.PerformanceCompositeAccountRuleEvaluatorContext;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>PerformanceAccountModifiedDietzSubPeriodCalculatorTests</code> allows for testing the modified dietz monthly algorithm
 * including externally applied data.
 *
 * @author apopp
 */
public class PerformanceAccountModifiedDietzSubPeriodCalculatorTests extends BasePerformanceAccountCalculatorTests {


	@Override
	protected BasePerformanceCalculator getPerformanceCalculatorImpl() {
		return new PerformanceCompositeAccountSubPeriodModifiedDietzExternalCalculator();
	}


	@Test
	public void testBasicModifiedDietzSubPeriodCalculation() {
		//Data to be uploaded by accounting: previousMonthEndDate, startDate, endDate, managementFee, previousMonthEndMarketValue, endMonthMarketValue, firstOfMonthNetCashFlow, firstOfMonthWeightedNetCashFlow, lastOfMonthGainLoss, expectedGrossReturn, expectedNetReturn
		validateCalculation("10/31/2015", "11/2/2015", "11/30/2015", "-20090.34", "64350269.39", "61388731.42", "900000.00", "900000.00", "-10946231.41", "-5.9208", "-5.8900");
	}


	@Test
	public void testCalculateDailyWeightedNetCashFlowValueJune() {
		calculateDailyWeightedNetCashFlow("06/1/2015", new BigDecimal(10000), new BigDecimal(10000));
		calculateDailyWeightedNetCashFlow("06/1/2015", new BigDecimal(30), new BigDecimal(30));
		calculateDailyWeightedNetCashFlow("06/1/2015", new BigDecimal(500), new BigDecimal(500));
		calculateDailyWeightedNetCashFlow("06/1/2015", new BigDecimal(5000000), new BigDecimal(5000000));

		calculateDailyWeightedNetCashFlow("06/10/2015", new BigDecimal(10000), new BigDecimal(7000));
		calculateDailyWeightedNetCashFlow("06/10/2015", new BigDecimal(30), new BigDecimal(21));
		calculateDailyWeightedNetCashFlow("06/10/2015", new BigDecimal(500), new BigDecimal(350));
		calculateDailyWeightedNetCashFlow("06/10/2015", new BigDecimal(5000000), new BigDecimal(3500000));

		calculateDailyWeightedNetCashFlow("06/25/2015", new BigDecimal(10000), new BigDecimal(2000));
		calculateDailyWeightedNetCashFlow("06/25/2015", new BigDecimal(30), new BigDecimal(6));
		calculateDailyWeightedNetCashFlow("06/25/2015", new BigDecimal(500), new BigDecimal(100));
		calculateDailyWeightedNetCashFlow("06/25/2015", new BigDecimal(5000000), new BigDecimal(1000000));

		calculateDailyWeightedNetCashFlow("06/30/2015", new BigDecimal(10000), new BigDecimal(333.3333333333333333));
		calculateDailyWeightedNetCashFlow("06/30/2015", new BigDecimal(30), new BigDecimal(0.9999999999999999999));
		calculateDailyWeightedNetCashFlow("06/30/2015", new BigDecimal(500), new BigDecimal(16.666666666666666665));
		calculateDailyWeightedNetCashFlow("06/30/2015", new BigDecimal(5000000), new BigDecimal(166666.66666666666665));
	}


	@Test
	public void testCalculateDailyWeightedNetCashFlowValueJuly() {
		calculateDailyWeightedNetCashFlow("07/1/2015", new BigDecimal(10000), new BigDecimal(10000));
		calculateDailyWeightedNetCashFlow("07/1/2015", new BigDecimal(30), new BigDecimal(30));
		calculateDailyWeightedNetCashFlow("07/1/2015", new BigDecimal(500), new BigDecimal(500));
		calculateDailyWeightedNetCashFlow("07/1/2015", new BigDecimal(5000000), new BigDecimal(5000000));

		calculateDailyWeightedNetCashFlow("07/10/2015", new BigDecimal(10000), new BigDecimal(7096.7741935483870968));
		calculateDailyWeightedNetCashFlow("07/10/2015", new BigDecimal(30), new BigDecimal(21.2903225806451612904));
		calculateDailyWeightedNetCashFlow("07/10/2015", new BigDecimal(500), new BigDecimal(354.83870967741935484));
		calculateDailyWeightedNetCashFlow("07/10/2015", new BigDecimal(5000000), new BigDecimal(3548387.0967741935484));

		calculateDailyWeightedNetCashFlow("07/25/2015", new BigDecimal(10000), new BigDecimal(2258.0645161290322581));
		calculateDailyWeightedNetCashFlow("07/25/2015", new BigDecimal(30), new BigDecimal(6.7741935483870967743));
		calculateDailyWeightedNetCashFlow("07/25/2015", new BigDecimal(500), new BigDecimal(112.903225806451612905));
		calculateDailyWeightedNetCashFlow("07/25/2015", new BigDecimal(5000000), new BigDecimal(1129032.25806451612905));

		calculateDailyWeightedNetCashFlow("07/30/2015", new BigDecimal(10000), new BigDecimal(645.1612903225806452));
		calculateDailyWeightedNetCashFlow("07/30/2015", new BigDecimal(30), new BigDecimal(1.9354838709677419356));
		calculateDailyWeightedNetCashFlow("07/30/2015", new BigDecimal(500), new BigDecimal(32.25806451612903226));
		calculateDailyWeightedNetCashFlow("07/30/2015", new BigDecimal(5000000), new BigDecimal(322580.6451612903226));
	}


	@Test
	public void testCalculateDailyWeightedNetCashFlowValueVsOverride() {
		BasePerformanceDailyCalculator calculator = (BasePerformanceDailyCalculator) getPerformanceCalculator();

		Date measureDate = DateUtils.toDate("07/30/2015");
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = new PerformanceCompositeInvestmentAccountDailyPerformance(null, measureDate);
		dailyPerformance.setNetCashFlowOverride(new BigDecimal(30));

		//Ensure weighted cash flow and weighted cash flow override are calculated independently from underlying
		PerformanceCompositeAccountRuleEvaluatorContext config = new PerformanceCompositeAccountRuleEvaluatorContext();
		config.setPerformanceCompositeInvestmentAccountDailyPerformanceList(CollectionUtils.createList(dailyPerformance), true, false);
		dailyPerformance.setNetCashFlow(new BigDecimal(10000));

		ValidationUtils.assertNull(dailyPerformance.getWeightedNetCashFlow(), "Weighted cash flow should be empty");
		ValidationUtils.assertNull(dailyPerformance.getWeightedNetCashFlowOverride(), "Weighted cash flow override should be empty");

		dailyPerformance.setWeightedNetCashFlow(calculator.calculateDailyWeightedNetCashFlowValue(dailyPerformance.getMeasureDate(), dailyPerformance.getNetCashFlow()));
		dailyPerformance.setWeightedNetCashFlowOverride(calculator.calculateDailyWeightedNetCashFlowValue(dailyPerformance.getMeasureDate(), dailyPerformance.getNetCashFlowOverride()));

		ValidationUtils.assertEquals(MathUtils.compare(dailyPerformance.getWeightedNetCashFlow(), new BigDecimal(645.1612903225806452)), 0, "Expected weighted cash flow of [" + new BigDecimal(645.1612903225806452) + "] but found [" + dailyPerformance.getWeightedNetCashFlow() + "]");
		ValidationUtils.assertEquals(MathUtils.compare(dailyPerformance.getWeightedNetCashFlowOverride(), new BigDecimal(1.9354838709677419356)), 0, "Expected weighted cash flow of [" + new BigDecimal(1.9354838709677419356) + "] but found [" + dailyPerformance.getWeightedNetCashFlowOverride() + "]");
	}


	@Test
	public void testCalculateActualNetReturn() {
		List<String> dateGrossReturnList = CollectionUtils.createList("07/1/2015,.0029417082", "07/5/2015,.0059417082", "07/10/2015,.00272", "07/25/2015,.0097865");
		calculateActualNetReturn(dateGrossReturnList, new BigDecimal(".0029417082"), new BigDecimal("0.01454588237229926600"));

		dateGrossReturnList = CollectionUtils.createList("07/2/2015,.00123", "07/7/2015,.0051223", "07/14/2015,.00654", "07/29/2015,.00978666");
		calculateActualNetReturn(dateGrossReturnList, new BigDecimal(".0021239417082"), new BigDecimal("0.01501699399212930200"));
	}


	private void calculateDailyWeightedNetCashFlow(String processDate, BigDecimal netCashFlow, BigDecimal expectedWeightedCashFlow) {
		Date measureDate = DateUtils.toDate(processDate);
		PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = new PerformanceCompositeInvestmentAccountDailyPerformance(null, measureDate);
		PerformanceCompositeAccountRuleEvaluatorContext config = new PerformanceCompositeAccountRuleEvaluatorContext();
		config.setPerformanceCompositeInvestmentAccountDailyPerformanceList(CollectionUtils.createList(dailyPerformance), true, false);
		dailyPerformance.setNetCashFlow(netCashFlow);
		ValidationUtils.assertNull(dailyPerformance.getWeightedNetCashFlow(), "Weighted cash flow should be empty");
		dailyPerformance.setWeightedNetCashFlow(((BasePerformanceDailyCalculator) getPerformanceCalculator()).calculateDailyWeightedNetCashFlowValue(dailyPerformance.getMeasureDate(), dailyPerformance.getNetCashFlow()));
		ValidationUtils.assertEquals(MathUtils.compare(dailyPerformance.getWeightedNetCashFlow(), expectedWeightedCashFlow), 0, "Expected weighted cash flow of [" + expectedWeightedCashFlow + "] but found [" + dailyPerformance.getWeightedNetCashFlow() + "]");
	}


	private void calculateActualNetReturn(List<String> dateGrossReturnList, BigDecimal lastDayNetReturnWithManagementFee, BigDecimal expectedNetReturn) {
		PerformanceCompositeAccountRuleEvaluatorContext config = new PerformanceCompositeAccountRuleEvaluatorContext();
		List<PerformanceCompositeInvestmentAccountDailyPerformance> dailyPerformanceList = new ArrayList<>();
		PerformanceCompositeInvestmentAccountPerformance accountPerformance = new PerformanceCompositeInvestmentAccountPerformance();
		config.setPerformanceCompositeInvestmentAccountPerformance(accountPerformance);

		for (String dateGrossReturn : CollectionUtils.getIterable(dateGrossReturnList)) {
			String[] dateGrossReturns = dateGrossReturn.split(",");
			PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance = new PerformanceCompositeInvestmentAccountDailyPerformance(null, DateUtils.toDate(dateGrossReturns[0]));
			dailyPerformance.setGrossReturn(new BigDecimal(dateGrossReturns[1]));
			dailyPerformanceList.add(dailyPerformance);
		}

		for (PerformanceCompositeInvestmentAccountDailyPerformance dailyPerformance : CollectionUtils.getIterable(dailyPerformanceList)) {
			config.getPerformanceCompositeInvestmentAccountDailyPerformanceMap().put(dailyPerformance.getMeasureDate(), dailyPerformance);
		}

		((BasePerformanceDailyCalculator) getPerformanceCalculator()).calculateActualNetReturn(config, lastDayNetReturnWithManagementFee);
		ValidationUtils.assertEquals(MathUtils.compare(expectedNetReturn, config.getPerformanceCompositeInvestmentAccountPerformance().getPeriodNetReturn()), 0, "Expected net return [" + expectedNetReturn + "] but found [" + config.getPerformanceCompositeInvestmentAccountPerformance().getPeriodNetReturn() + "]");
	}
}
