Clifton.performance.account.portfolio.target.RunWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Performance Run',
	iconCls: 'chart-bar',
	width: 1100,
	height: 550,

	items: [{
		xtype: 'formpanel',
		url: 'performancePortfolioTargetRun.json?requestedMaxDepth=5',
		loadValidation: false, // entity extends a persisted entity and there is no direct table
		labelWidth: 140,
		defaults: {anchor: '-20'},
		labelFieldName: 'portfolioRun.id',

		items: [
			{fieldLabel: 'Performance Summary', name: 'performanceSummary.label', xtype: 'linkfield', detailIdField: 'performanceSummary.id', detailPageClass: 'Clifton.performance.account.PerformanceAccountWindow'},
			{
				fieldLabel: 'Portfolio Run', name: 'portfolioRun.label', hiddenName: 'portfolioRun.id', xtype: 'combo', detailPageClass: 'Clifton.portfolio.run.RunWindow', url: 'portfolioRunListFind.json', displayField: 'label', tooltipField: 'statusLabel',
				beforequery: function(queryEvent) {
					const f = queryEvent.combo.getParentForm().getForm();
					const balanceDate = f.findField('portfolioRunDate').value;
					const clientAccountId = TCG.getValue('performanceSummary.clientAccount.id', f.formValues);
					const parentClientAccountId = TCG.getValue('performanceSummary.parentClientAccount.id', f.formValues);
					// If there is a parent account - can use one of those runs OR those ran individually for the account
					if (TCG.isNotBlank(parentClientAccountId)) {
						queryEvent.combo.store.baseParams = {
							clientInvestmentAccountIds: [parentClientAccountId, clientAccountId],
							marketOnCloseAdjustmentsIncluded: false,
							balanceDate: balanceDate
						};
					}
					else {
						queryEvent.combo.store.baseParams = {
							clientInvestmentAccountId: clientAccountId,
							marketOnCloseAdjustmentsIncluded: false,
							balanceDate: balanceDate
						};
					}
				}
			},
			{fieldLabel: 'Balance Date', name: 'portfolioRunDate', xtype: 'datefield', readOnly: true},

			{
				fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
				defaults: {
					xtype: 'displayfield',
					flex: 1
				},
				items: [
					{value: 'Calculated Value'},
					{value: 'Override Value'},
					{value: 'Change'}
				]
			},
			{
				fieldLabel: 'Gain / Loss', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					readOnly: true,
					submitValue: false,
					flex: 1
				},
				items: [
					{name: 'gainLoss'},
					{name: 'gainLossOverride'},
					{name: 'gainLossDifference'}
				]
			},
			{
				fieldLabel: 'Overlay Target', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					readOnly: true,
					submitValue: false,
					flex: 1
				},
				items: [
					{name: 'target'},
					{name: 'targetOverride'},
					{name: 'targetDifference'}
				]
			},
			{
				fieldLabel: 'Gain / Loss (Other)', xtype: 'compositefield',
				defaults: {
					xtype: 'currencyfield',
					readOnly: true,
					submitValue: false,
					flex: 1
				},
				items: [
					{name: 'gainLossOther'},
					{name: 'gainLossOtherOverride', readOnly: false, submitValue: true},
					{name: 'gainLossOtherDifference'}
				]
			},
			{fieldLabel: 'Financing Amount', xtype: 'currencyfield', name: 'financingAmountOverride'},
			{fieldLabel: 'Account Return', xtype: 'currencyfield', decimalPrecision: 4, readOnly: true, submitValue: false, name: 'accountReturn'},
			{fieldLabel: 'Benchmark Return', xtype: 'currencyfield', decimalPrecision: 4, readOnly: true, submitValue: false, name: 'benchmarkReturn'},
			{fieldLabel: 'Return Difference', xtype: 'currencyfield', decimalPrecision: 4, readOnly: true, submitValue: false, name: 'returnDifference'},
			{
				xtype: 'formgrid',
				storeRoot: 'performancePortfolioTargetSummaryList',
				dtoClass: 'com.clifton.performance.account.portfolio.target.process.PerformancePortfolioTargetSummary',
				readOnly: true,
				detailPageClass: 'Clifton.portfolio.target.run.RunTargetSummaryWindow',
				getDetailPageId: function(grid, row) {
					return row.json.id;
				},
				getDefaultData: function(grid, row) {
					return {
						runId: TCG.getValue('performancePortfolioRun.portfolioRun.id', row.json),
						targetId: TCG.getValue('portfolioTarget.id', row.json)
					};
				},
				doNotSubmitFields: ['coalesceGainLossOverride', 'coalesceTargetOverride'],
				columnsConfig: [
					{header: 'ID', width: 10, dataIndex: 'id', hidden: true},
					{header: 'Run ID', width: 10, dataIndex: 'performancePortfolioRun.portfolioRun.id', hidden: true},
					{header: 'Target ID', width: 10, dataIndex: 'portfolioTarget.id', hidden: true},
					{header: 'Portfolio Target', width: 175, dataIndex: 'portfolioTarget.label'},
					{header: 'Benchmark', width: 175, dataIndex: 'benchmarkSecurity.label'},
					{header: 'Target Return', width: 85, dataIndex: 'targetReturn', type: 'currency', numberFormat: '0,000.0000 %', hidden: true},
					{header: 'Benchmark Return', width: 85, dataIndex: 'benchmarkReturn', type: 'currency', numberFormat: '0,000.0000 %', hidden: true},
					{
						header: 'Return Difference', width: 85, dataIndex: 'returnDifference', type: 'currency', numberFormat: '0,000.0000 %',
						renderer: function(v, metaData, r) {
							const format = '0,000.0000 %';
							const value = TCG.renderAmount(v, true, format);
							let qtip = '<table><tr><td>Asset Class Return:</td><td align="right">' + Ext.util.Format.number(r.data.assetClassReturn, format) + '</td></tr>';
							qtip += '<tr><td>Benchmark Return:</td><td align="right">' + Ext.util.Format.number(r.data.benchmarkReturn, format) + '</td></tr>';
							qtip += '<tr><td>Difference:</td><td align="right">' + value + '</td></tr>';
							qtip += '</table>';
							metaData.attr = 'qtip=\'' + qtip + '\'';
							return value;
						}
					},
					{header: 'Financing Rate', width: 85, dataIndex: 'financingRate', type: 'currency', numberFormat: '0,000.0000 %'},
					{header: 'Gain Loss (Calculated)', hidden: true, width: 100, dataIndex: 'gainLoss', type: 'currency', negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
					{header: 'Gain Loss (Override)', hidden: true, width: 100, dataIndex: 'gainLossOverride', type: 'currency', useNull: true, negativeInRed: true, positiveInGreen: true, summaryType: 'sum'},
					{
						header: 'Gain Loss', width: 100, dataIndex: 'coalesceGainLossOverride', type: 'currency', summaryType: 'sum', editor: {xtype: 'currencyfield'},
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['gainLossOverride']), r.data['gainLoss'], '0,000.00', 'Adjustment', true);
						},
						summaryRenderer: function(v) {
							if (TCG.isNotBlank(v)) {
								return TCG.renderAmount(v, false, '0,000.00');
							}
						}
					},

					{header: 'Target (Calculated)', hidden: true, width: 100, dataIndex: 'target', type: 'currency', summaryType: 'sum'},
					{header: 'Target (Override)', hidden: true, width: 100, dataIndex: 'targetOverride', type: 'currency', useNull: true, summaryType: 'sum'},
					{
						header: 'Target', width: 100, dataIndex: 'coalesceTargetOverride', type: 'currency', summaryType: 'sum', editor: {xtype: 'currencyfield'},
						renderer: function(v, p, r) {
							return TCG.renderAdjustedAmount(v, TCG.isNotBlank(r.data['targetOverride']), r.data['target'], '0,000.00');
						},
						summaryRenderer: function(v) {
							if (TCG.isNotBlank(v)) {
								return TCG.renderAmount(v, false, '0,000.00');
							}
						}
					},
					{header: 'Override Note', width: 400, dataIndex: 'overrideNote', editor: {xtype: 'textarea'}}
				],
				plugins: {ptype: 'gridsummary'},
				// submit the override only if different from calculated
				onAfterUpdateFieldValue: function(editor, field) {
					const f = editor.field;
					if (f === 'coalesceGainLossOverride') {
						const orig = editor.record.get('gainLoss');
						let val = editor.value;
						if (!Ext.isNumber(val)) {
							val = orig;
							editor.value = orig;
							editor.record.set(f, orig);
						}
						if (orig - val !== 0) {
							editor.record.set('gainLossOverride', val);
						}
						else {
							editor.record.set('gainLossOverride', '');
						}
					}
					if (f === 'coalesceTargetOverride') {
						const orig = editor.record.get('target');
						let val = editor.value;
						if (!Ext.isNumber(val)) {
							val = orig;
							editor.value = orig;
							editor.record.set(f, orig);
						}
						if (orig - val !== 0) {
							editor.record.set('targetOverride', val);
						}
						else {
							editor.record.set('targetOverride', '');
						}
					}
				}
			}
		]
	}]
});
