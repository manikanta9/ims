Ext.ns(
	'Clifton.performance.account.portfolio.target'
);

// UI Overrides for performance windows
Clifton.performance.account.portfolio.PerformanceRunWindowOverrides['PORTFOLIO_TARGET'] = 'Clifton.performance.account.portfolio.target.RunWindow';
