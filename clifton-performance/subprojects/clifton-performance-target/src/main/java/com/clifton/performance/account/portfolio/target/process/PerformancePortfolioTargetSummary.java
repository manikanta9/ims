package com.clifton.performance.account.portfolio.target.process;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.MathUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.run.PortfolioTargetRunTargetSummary;

import java.math.BigDecimal;


/**
 * <code>PerformancePortfolioTargetSummary</code> represents a gain/loss summary for each portfolio run's portfolio target
 * for PORTFOLIO_TARGET processing. Override values are allowed.
 *
 * @author nickk
 */
public class PerformancePortfolioTargetSummary extends BaseEntity<Integer> {

	private PerformancePortfolioRun performancePortfolioRun;
	private PortfolioTarget portfolioTarget;
	@NonPersistentField
	private PortfolioTargetRunTargetSummary portfolioTargetSummary;

	private BigDecimal gainLoss; // since previous performance detail (business day); mark to market for futures; includes commission; NOT synthetically adjusted
	private BigDecimal gainLossOverride;

	private BigDecimal target;
	private BigDecimal targetOverride;

	private String overrideNote;

	// ((1+(Previous Month End Cost of Capital (3 month LIBOR, T-BILL)))^(1/360)-1)*Days from Previous Performance Detail
	private BigDecimal financingRate;

	private InvestmentSecurity benchmarkSecurity; // Benchmark used on this date to calculate benchmark return
	private BigDecimal benchmarkReturn; // % increase since last business day (run)

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public BigDecimal getCoalesceGainLossOverride() {
		if (getGainLossOverride() == null) {
			return getGainLoss();
		}
		return getGainLossOverride();
	}


	public BigDecimal getCoalesceTargetOverride() {
		if (getTargetOverride() == null) {
			return getTarget();
		}
		return getTargetOverride();
	}


	public BigDecimal getTargetTimesFinancingRate() {
		if (MathUtils.isNullOrZero(getFinancingRate())) {
			return BigDecimal.ZERO;
		}
		return MathUtils.getPercentageOf(getFinancingRate(), getCoalesceTargetOverride(), true);
	}


	public BigDecimal getAbsCoalesceOverlayTargetOverride() {
		return MathUtils.abs(getCoalesceTargetOverride());
	}


	public BigDecimal getTargetReturn() {
		BigDecimal result = BigDecimal.ZERO;
		if (!MathUtils.isNullOrZero(getCoalesceTargetOverride())) {
			// (Gain/Loss + (Target * Financing Rate) / ABS(Target)
			result = MathUtils.add(getCoalesceGainLossOverride(), getTargetTimesFinancingRate());
			result = MathUtils.divide(result, MathUtils.abs(getTarget()));
			result = MathUtils.multiply(result, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}
		return result;
	}


	public BigDecimal getReturnDifference() {
		return MathUtils.subtract(getTargetReturn(), getBenchmarkReturn());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioRun getPerformancePortfolioRun() {
		return this.performancePortfolioRun;
	}


	public void setPerformancePortfolioRun(PerformancePortfolioRun performancePortfolioRun) {
		this.performancePortfolioRun = performancePortfolioRun;
	}


	public PortfolioTarget getPortfolioTarget() {
		return this.portfolioTarget;
	}


	public void setPortfolioTarget(PortfolioTarget portfolioTarget) {
		this.portfolioTarget = portfolioTarget;
	}


	public PortfolioTargetRunTargetSummary getPortfolioTargetSummary() {
		return this.portfolioTargetSummary;
	}


	public void setPortfolioTargetSummary(PortfolioTargetRunTargetSummary portfolioTargetSummary) {
		if (portfolioTargetSummary != null) {
			this.portfolioTarget = portfolioTargetSummary.getTarget();
		}
		this.portfolioTargetSummary = portfolioTargetSummary;
	}


	public BigDecimal getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(BigDecimal gainLoss) {
		this.gainLoss = gainLoss;
	}


	public BigDecimal getGainLossOverride() {
		return this.gainLossOverride;
	}


	public void setGainLossOverride(BigDecimal gainLossOverride) {
		this.gainLossOverride = gainLossOverride;
	}


	public BigDecimal getTarget() {
		return this.target;
	}


	public void setTarget(BigDecimal target) {
		this.target = target;
	}


	public BigDecimal getTargetOverride() {
		return this.targetOverride;
	}


	public void setTargetOverride(BigDecimal targetOverride) {
		this.targetOverride = targetOverride;
	}


	public String getOverrideNote() {
		return this.overrideNote;
	}


	public void setOverrideNote(String overrideNote) {
		this.overrideNote = overrideNote;
	}


	public BigDecimal getFinancingRate() {
		return this.financingRate;
	}


	public void setFinancingRate(BigDecimal financingRate) {
		this.financingRate = financingRate;
	}


	public InvestmentSecurity getBenchmarkSecurity() {
		return this.benchmarkSecurity;
	}


	public void setBenchmarkSecurity(InvestmentSecurity benchmarkSecurity) {
		this.benchmarkSecurity = benchmarkSecurity;
	}


	public BigDecimal getBenchmarkReturn() {
		return this.benchmarkReturn;
	}


	public void setBenchmarkReturn(BigDecimal benchmarkReturn) {
		this.benchmarkReturn = benchmarkReturn;
	}
}
