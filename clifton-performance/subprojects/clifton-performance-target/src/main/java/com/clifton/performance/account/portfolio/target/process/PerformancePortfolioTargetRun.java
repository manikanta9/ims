package com.clifton.performance.account.portfolio.target.process;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;

import java.util.List;


/**
 * <code>PerformancePortfolioTargetRun</code> is a {@link PerformancePortfolioRun}
 * decorated with portfolio target run processing performance details.
 *
 * @author nickk
 */
@NonPersistentObject(populatePropertiesBeforeBinding = true)
public class PerformancePortfolioTargetRun extends PerformancePortfolioRun {

	private List<PerformancePortfolioTargetSummary> performancePortfolioTargetSummaryList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static PerformancePortfolioTargetRun fromPerformancePortfolioRun(PerformancePortfolioRun performancePortfolioRun) {
		PerformancePortfolioTargetRun productRun = new PerformancePortfolioTargetRun();
		BeanUtils.copyProperties(performancePortfolioRun, productRun);
		return productRun;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<PerformancePortfolioTargetSummary> getPerformancePortfolioTargetSummaryList() {
		return this.performancePortfolioTargetSummaryList;
	}


	public void setPerformancePortfolioTargetSummaryList(List<PerformancePortfolioTargetSummary> performancePortfolioTargetSummaryList) {
		this.performancePortfolioTargetSummaryList = performancePortfolioTargetSummaryList;
	}
}
