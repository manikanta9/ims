package com.clifton.performance.account.portfolio.target.process;

import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunProcessConfig;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * <code>PerformancePortfolioTargetProcessConfig</code> object is used for processing {@link PerformancePortfolioRun}s
 * associated with PORTFOLIO_TARGET processing. It pre-loads necessary data which can be passed around and reused by calculators.
 *
 * @author nickk
 */
public class PerformancePortfolioTargetProcessConfig extends PerformancePortfolioRunProcessConfig {

	/**
	 * PortfolioRun ID to PerformancePortfolioTargetSummary. As the summaries are constructed and processed, they are mapped to
	 * the persisted run rather than the PerformancePortfolioRun, which may not be saved yet.
	 */
	private final Map<Integer, List<PerformancePortfolioTargetSummary>> runPortfolioTargetSummaryListMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioTargetProcessConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		super(performanceAccount, processingType);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<PerformancePortfolioTargetSummary> getPerformanceRunTargetSummaryListForRun(PerformancePortfolioRun performanceRun) {
		if (performanceRun != null && performanceRun.getPortfolioRun() != null) {
			return this.runPortfolioTargetSummaryListMap.get(performanceRun.getPortfolioRun().getId());
		}
		return null;
	}


	public void setPerformanceRunTargetSummaryListForRun(PerformancePortfolioRun performanceRun, List<PerformancePortfolioTargetSummary> performanceRunTargetSummaryList) {
		this.runPortfolioTargetSummaryListMap.put(performanceRun.getPortfolioRun().getId(), performanceRunTargetSummaryList);
	}
}
