package com.clifton.performance.account.portfolio.target.process;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;

import java.util.List;


/**
 * <code>PerformancePortfolioTargetService</code> provides services needed for PORTFOLIO_TARGET processing performance.
 *
 * @author nickk
 */
public interface PerformancePortfolioTargetService {

	////////////////////////////////////////////////////////////////////////////
	/////     Performance Portfolio Target Business Methods      //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Convenience service for getting a {@link com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun} by
	 * handling the conversion from and to {@link PerformancePortfolioTargetRun}.
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public PerformancePortfolioTargetRun getPerformancePortfolioTargetRun(int id);


	/**
	 * Convenience service for saving a {@link com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun} by
	 * handling the conversion from and to {@link PerformancePortfolioTargetRun}.
	 */
	@SecureMethod(dtoClass = PerformancePortfolioRun.class)
	public PerformancePortfolioTargetRun savePerformancePortfolioTargetRun(PerformancePortfolioTargetRun performancePortfolioTargetRun);

	////////////////////////////////////////////////////////////////////////////
	/////     Performance Portfolio Target Summary Business Methods      //////
	////////////////////////////////////////////////////////////////////////////


	public List<PerformancePortfolioTargetSummary> getPerformancePortfolioTargetSummaryListForRun(int performanceRunId);


	public void savePerformancePortfolioTargetSummaryListForRun(int performanceRunId, List<PerformancePortfolioTargetSummary> performancePortfolioTargetSummaryList);


	public void deletePerformancePortfolioTargetSummaryListForRun(int performanceRunId);
}
