package com.clifton.performance.account.portfolio.target.process;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.PerformanceInvestmentAccountProcessService;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * @author nickk
 */
@Service
public class PerformancePortfolioTargetServiceImpl implements PerformancePortfolioTargetService {

	private AdvancedUpdatableDAO<PerformancePortfolioTargetSummary, Criteria> performancePortfolioTargetSummaryDAO;

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;
	private PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService;
	private PerformancePortfolioRunService performancePortfolioRunService;

	private PortfolioRunService portfolioRunService;

	////////////////////////////////////////////////////////////////////////////
	/////     Performance Portfolio Target Business Methods      //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public PerformancePortfolioTargetRun getPerformancePortfolioTargetRun(int id) {
		PerformancePortfolioTargetRun targetRun = convertPerformanceRunToTargetRun(getPerformancePortfolioRunService().getPerformancePortfolioRun(id));
		if (targetRun != null) {
			targetRun.setPerformancePortfolioTargetSummaryList(getPerformancePortfolioTargetSummaryListForRun(id));
		}
		return targetRun;
	}


	@Override
	public PerformancePortfolioTargetRun savePerformancePortfolioTargetRun(PerformancePortfolioTargetRun performancePortfolioTargetRun) {
		PerformanceInvestmentAccount summary = performancePortfolioTargetRun.getPerformanceSummary();
		// Validate Editing Is Allowed
		getPerformanceInvestmentAccountService().validatePerformanceInvestmentAccountModify(summary.getId());

		// If the Portfolio Run isn't the Main Run, then it was changed.  Let's set it to be the main run so used when re-processed
		PortfolioRun run = performancePortfolioTargetRun.getPortfolioRun();
		if (run != null && !run.isMainRun()) {
			run.setMainRun(true);
			getPortfolioRunService().savePortfolioRun(run);
		}

		List<PerformancePortfolioTargetSummary> performancePortfolioTargetSummaryList = performancePortfolioTargetRun.getPerformancePortfolioTargetSummaryList();
		/*
		Since ProductPerformancePortfolioRun is a non-persistent DTO, we need to ensure the saved instance is
		hydrated and any updated properties are saved.
		 */
		PerformancePortfolioRun existingRun = performancePortfolioTargetRun.isNewBean() ? null : getPerformancePortfolioRunService().getPerformancePortfolioRun(performancePortfolioTargetRun.getId());
		PerformancePortfolioRun savedRun;
		if (existingRun != null) {
			existingRun.copyProperties(performancePortfolioTargetRun);
			savedRun = getPerformancePortfolioRunService().savePerformancePortfolioRun(existingRun);
		}
		else {
			savedRun = getPerformancePortfolioRunService().savePerformancePortfolioRun(performancePortfolioTargetRun.toPerformancePortfolioRun());
		}

		performancePortfolioTargetRun = convertPerformanceRunToTargetRun(savedRun);
		// save target summaries for the run
		savePerformancePortfolioTargetSummaryListForRun(performancePortfolioTargetRun.getId(), performancePortfolioTargetSummaryList);
		performancePortfolioTargetRun.setPerformancePortfolioTargetSummaryList(performancePortfolioTargetSummaryList);

		// Re-process this run and Re-calculate the summary
		getPerformanceInvestmentAccountProcessService().processPerformanceInvestmentAccountForDate(summary, performancePortfolioTargetRun.getMeasureDate());
		// Return saved and reprocessed instance
		return getPerformancePortfolioTargetRun(performancePortfolioTargetRun.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	/////     Performance Portfolio Target Summary Business Methods      //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<PerformancePortfolioTargetSummary> getPerformancePortfolioTargetSummaryListForRun(int performanceRunId) {
		return getPerformancePortfolioTargetSummaryDAO().findByField("performancePortfolioRun.id", performanceRunId);
	}


	@Override
	public void savePerformancePortfolioTargetSummaryListForRun(int performanceRunId, List<PerformancePortfolioTargetSummary> performancePortfolioTargetSummaryList) {
		getPerformancePortfolioTargetSummaryDAO().saveList(performancePortfolioTargetSummaryList, getPerformancePortfolioTargetSummaryListForRun((performanceRunId)));
	}


	@Override
	public void deletePerformancePortfolioTargetSummaryListForRun(int performanceRunId) {
		getPerformancePortfolioTargetSummaryDAO().deleteList(getPerformancePortfolioTargetSummaryListForRun(performanceRunId));
	}

	////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////


	private PerformancePortfolioTargetRun convertPerformanceRunToTargetRun(PerformancePortfolioRun performancePortfolioRun) {
		return performancePortfolioRun == null ? null : PerformancePortfolioTargetRun.fromPerformancePortfolioRun(performancePortfolioRun);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformancePortfolioTargetSummary, Criteria> getPerformancePortfolioTargetSummaryDAO() {
		return this.performancePortfolioTargetSummaryDAO;
	}


	public void setPerformancePortfolioTargetSummaryDAO(AdvancedUpdatableDAO<PerformancePortfolioTargetSummary, Criteria> performancePortfolioTargetSummaryDAO) {
		this.performancePortfolioTargetSummaryDAO = performancePortfolioTargetSummaryDAO;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}


	public PerformanceInvestmentAccountProcessService getPerformanceInvestmentAccountProcessService() {
		return this.performanceInvestmentAccountProcessService;
	}


	public void setPerformanceInvestmentAccountProcessService(PerformanceInvestmentAccountProcessService performanceInvestmentAccountProcessService) {
		this.performanceInvestmentAccountProcessService = performanceInvestmentAccountProcessService;
	}


	public PerformancePortfolioRunService getPerformancePortfolioRunService() {
		return this.performancePortfolioRunService;
	}


	public void setPerformancePortfolioRunService(PerformancePortfolioRunService performancePortfolioRunService) {
		this.performancePortfolioRunService = performancePortfolioRunService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
