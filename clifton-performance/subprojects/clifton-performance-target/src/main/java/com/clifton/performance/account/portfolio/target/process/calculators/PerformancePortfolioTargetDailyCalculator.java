package com.clifton.performance.account.portfolio.target.process.calculators;

import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.portfolio.target.process.PerformancePortfolioTargetProcessConfig;
import com.clifton.performance.account.portfolio.target.process.PerformancePortfolioTargetService;
import com.clifton.performance.account.portfolio.target.process.PerformancePortfolioTargetSummary;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.calculators.PerformancePortfolioRunCalculator;
import com.clifton.portfolio.run.PortfolioCurrencyOther;
import com.clifton.portfolio.target.PortfolioTarget;
import com.clifton.portfolio.target.run.PortfolioTargetRunReplication;
import com.clifton.portfolio.target.run.PortfolioTargetRunService;
import com.clifton.portfolio.target.run.PortfolioTargetRunTargetSummary;
import com.clifton.portfolio.target.run.search.PortfolioTargetRunTargetSummaryCommand;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <code>PerformancePortfolioTargetDailyCalculator</code> calculates returns for each {@link PortfolioTarget} of
 * a run and rolls up the totals to the {@link PerformancePortfolioRun}.
 *
 * @author nickk
 */
@Component
public class PerformancePortfolioTargetDailyCalculator extends PerformancePortfolioRunCalculator<PerformancePortfolioTargetProcessConfig> {

	private PerformancePortfolioTargetService performancePortfolioTargetService;

	private PortfolioTargetRunService portfolioTargetRunService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(PerformancePortfolioTargetProcessConfig config, Date date) {
		setupProcessing(config);
		for (PerformancePortfolioRun performanceRun : CollectionUtils.getIterable(config.getPerformanceRunList())) {
			if (date == null || DateUtils.compare(date, performanceRun.getMeasureDate(), false) == 0) {
				processPerformancePortfolioTargetRun(config, performanceRun);
			}
		}
	}


	@Override
	@Transactional
	public void saveResults(PerformancePortfolioTargetProcessConfig config, Date date) {
		if (!CollectionUtils.isEmpty(config.getPerformanceRunList())) {
			List<PerformancePortfolioRun> savedList = new ArrayList<>();
			for (PerformancePortfolioRun performancePortfolioRun : config.getPerformanceRunList()) {
				if (date == null || DateUtils.compare(date, performancePortfolioRun.getMeasureDate(), false) == 0) {
					savedList.add(savePerformanceRun(config, performancePortfolioRun));
				}
			}
			config.setPerformanceRunList(savedList);
		}
	}


	protected PerformancePortfolioRun savePerformanceRun(PerformancePortfolioTargetProcessConfig config, PerformancePortfolioRun performanceRun) {
		PerformancePortfolioRun saved = getPerformancePortfolioRunService().savePerformancePortfolioRun(performanceRun);
		getPerformancePortfolioTargetService().savePerformancePortfolioTargetSummaryListForRun(saved.getId(), config.getPerformanceRunTargetSummaryListForRun(performanceRun));
		return saved;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Transactional(readOnly = true)
	protected void processPerformancePortfolioTargetRun(PerformancePortfolioTargetProcessConfig config, PerformancePortfolioRun performanceRun) {
		// If no run - nothing to calculate - set all gain loss under "other"
		if (performanceRun.getPortfolioRun() == null) {
			// Don't throw an exception, because accounts that start late or end early it is OK if they are missing runs
			// There are non-ignorable warnings to catch all other cases
			// Put Account Gain/Loss Total Into Other Gain/Loss so we have the amounts
			List<AccountingPositionDaily> positionList = config.getPositionListForDate(performanceRun.getMeasureDate());
			performanceRun.setGainLoss(CoreMathUtils.sumProperty(positionList, AccountingPositionDaily::getDailyGainLossBase));
			return;
		}
		if (performanceRun.getPortfolioRun().getServiceProcessingType() == ServiceProcessingTypes.PORTFOLIO_TARGET) {
			processPerformancePortfolioTargetRunSummaryList(config, performanceRun, DateUtils.getPreviousWeekday(performanceRun.getMeasureDate()));
		}
	}


	private void processPerformancePortfolioTargetRunSummaryList(PerformancePortfolioTargetProcessConfig config, PerformancePortfolioRun performanceRun, Date previousWeekday) {
		boolean beforeInception = (config.getPerformanceInceptionDate() == null || DateUtils.compare(performanceRun.getMeasureDate(), config.getPerformanceInceptionDate(), false) < 0);

		PerformancePortfolioTargetRunConfig dailyConfig = setupPerformancePortfolioTargetDailyConfig(config, performanceRun);

		List<PerformancePortfolioTargetSummary> existingPerformanceTargetSummaryList = performanceRun.isNewBean() ? null
				: getPerformancePortfolioTargetService().getPerformancePortfolioTargetSummaryListForRun(performanceRun.getId());
		Map<PortfolioTarget, PerformancePortfolioTargetSummary> portfolioTargetToSummaryMap = BeanUtils.getBeanMap(existingPerformanceTargetSummaryList, PerformancePortfolioTargetSummary::getPortfolioTarget);

		List<PerformancePortfolioTargetSummary> newPerformanceTargetSummaryList = new ArrayList<>();
		for (PortfolioTargetRunTargetSummary runTargetSummary : CollectionUtils.getIterable(dailyConfig.getRunTargetSummaryList())) {
			if (runTargetSummary.getTarget().getReplication() == null) {
				continue;
			}

			PerformancePortfolioTargetSummary targetSummary = portfolioTargetToSummaryMap.get(runTargetSummary.getTarget());
			if (targetSummary == null) {
				targetSummary = new PerformancePortfolioTargetSummary();
				targetSummary.setPerformancePortfolioRun(performanceRun);
				targetSummary.setPortfolioTargetSummary(runTargetSummary);
			}

			BigDecimal portfolioTarget = BigDecimal.ZERO;
			// Gain/Loss - Contract Splitting
			BigDecimal gainLoss = BigDecimal.ZERO;

			List<PortfolioTargetRunReplication> targetReplicationList = runTargetSummary.getReplicationList();
			for (PortfolioTargetRunReplication rep : CollectionUtils.getIterable(targetReplicationList)) {
				// Skip "Cash Exposure" replications - if it's the only one
				if (rep.isCashExposure() && targetReplicationList.size() == 1) {
					continue;
				}
				portfolioTarget = MathUtils.add(portfolioTarget, rep.getOverlayExposureTotal());

				// If we are still looking at a cash exposure rep - continue on - all we needed to add was overlay target
				if (rep.isCashExposure()) {
					continue;
				}

				// replication gain/loss
				gainLoss = MathUtils.add(gainLoss, getReplicationGainLoss(dailyConfig, rep));

				// Currency
				gainLoss = MathUtils.add(gainLoss, getReplicationCurrencyGainLoss(dailyConfig, rep));
			}

			// Apply percentage of Currency Other total gain/loss
			if (!MathUtils.isNullOrZero(dailyConfig.getCurrencyOtherTotal())) {
				BigDecimal percentage = CoreMathUtils.getPercentValue(dailyConfig.getCurrencyOtherTotal(), dailyConfig.getTotalBaseCurrencyOther(), true);
				gainLoss = MathUtils.add(gainLoss, MathUtils.getPercentageOf(percentage, dailyConfig.getCurrencyOtherTotalGainLoss(), true));
			}

			// If Target is within -1 and 1 - consider it to be zero
			if (MathUtils.isGreaterThan(portfolioTarget, -1) && MathUtils.isLessThan(portfolioTarget, 1)) {
				targetSummary.setTarget(BigDecimal.ZERO);
			}
			else {
				targetSummary.setTarget(portfolioTarget);
			}

			targetSummary.setGainLoss(gainLoss);

			// If the map is null, then this client has opted not to include financing rates - or if before inception
			if (config.getInterestRateDailyChangeIndexMap() != null && !beforeInception) {
				InvestmentInterestRateIndex index = runTargetSummary.getTarget().getReplication().getType().getFinancingRateIndex();
				// Generate daily change if it does not exist - for reuse
				addInterestRateIndexDailyChangeToMap(config.getInterestRateDailyChangeIndexMap(), index, DateUtils.getLastWeekdayOfPreviousMonth(performanceRun.getMeasureDate()));

				// ((1+(Previous Month End Cost of Capital (3 month LIBOR, T-BILL)))^(1/360)-1)*Days from Previous Performance Detail
				BigDecimal dailyRate = config.getInterestRateDailyChangeIndexMap().get(index.getId());
				// Interest Rate Performance Calculation: Multiply # of days by the deannualized rate
				targetSummary.setFinancingRate(MathUtils.multiply(dailyRate, DateUtils.getDaysDifference(performanceRun.getMeasureDate(), previousWeekday)));
			}
			else {
				targetSummary.setFinancingRate(null);
			}

			newPerformanceTargetSummaryList.add(targetSummary);
		}

		performanceRun.setGainLoss(CoreMathUtils.sumProperty(newPerformanceTargetSummaryList, PerformancePortfolioTargetSummary::getGainLoss));
		performanceRun.setGainLossOther(MathUtils.subtract(dailyConfig.getTotalGainLoss(), performanceRun.getGainLoss()));
		performanceRun.setTarget(CoreMathUtils.sumProperty(newPerformanceTargetSummaryList, PerformancePortfolioTargetSummary::getTarget));

		config.setPerformanceRunTargetSummaryListForRun(performanceRun, newPerformanceTargetSummaryList);
		calculateAccountReturn(performanceRun, newPerformanceTargetSummaryList);
	}


	private void calculateAccountReturn(PerformancePortfolioRun performanceRun, List<PerformancePortfolioTargetSummary> performancePortfolioTargetSummaryList) {
		performanceRun.setGainLoss(CoreMathUtils.sumProperty(performancePortfolioTargetSummaryList, PerformancePortfolioTargetSummary::getGainLoss));
		performanceRun.setTarget(CoreMathUtils.sumProperty(performancePortfolioTargetSummaryList, PerformancePortfolioTargetSummary::getTarget));

		BigDecimal gainLossCoalesceSum = CoreMathUtils.sumProperty(performancePortfolioTargetSummaryList, PerformancePortfolioTargetSummary::getCoalesceGainLossOverride);
		if (!MathUtils.isEqual(performanceRun.getGainLoss(), gainLossCoalesceSum)) {
			performanceRun.setGainLossOverride(gainLossCoalesceSum);
		}
		else {
			performanceRun.setGainLossOverride(null);
		}
		BigDecimal targetCoalesceSum = CoreMathUtils.sumProperty(performancePortfolioTargetSummaryList, PerformancePortfolioTargetSummary::getCoalesceTargetOverride);
		if (!MathUtils.isEqual(performanceRun.getTarget(), targetCoalesceSum)) {
			performanceRun.setTargetOverride(targetCoalesceSum);
		}
		else {
			performanceRun.setTargetOverride(null);
		}

		BigDecimal absTargetSummaryTarget = MathUtils.abs(CoreMathUtils.sumProperty(performancePortfolioTargetSummaryList, PerformancePortfolioTargetSummary::getAbsCoalesceOverlayTargetOverride));
		BigDecimal benchmarkReturn = BigDecimal.ZERO;
		if (MathUtils.isNullOrZero(absTargetSummaryTarget)) {
			performanceRun.setBenchmarkReturn(benchmarkReturn);
			performanceRun.setAccountReturn(BigDecimal.ZERO);
		}
		else {
			for (PerformancePortfolioTargetSummary performancePortfolioTargetSummary : CollectionUtils.getIterable(performancePortfolioTargetSummaryList)) {
				BigDecimal target = performancePortfolioTargetSummary.getTarget();
				BigDecimal acBenchmarkReturn = MathUtils.divide(MathUtils.multiply(performancePortfolioTargetSummary.getBenchmarkReturn(), target), target);
				benchmarkReturn = MathUtils.add(benchmarkReturn, acBenchmarkReturn);
			}
			performanceRun.setBenchmarkReturn(benchmarkReturn);

			// Daily Return = (gainLoss + target*financingRate) / SUM(ABS(assetClassOverlayTarget))
			BigDecimal accountReturn = BigDecimal.ZERO;
			for (PerformancePortfolioTargetSummary performancePortfolioTargetSummary : CollectionUtils.getIterable(performancePortfolioTargetSummaryList)) {
				accountReturn = MathUtils.add(accountReturn, performancePortfolioTargetSummary.getTargetTimesFinancingRate());
			}
			accountReturn = MathUtils.add(accountReturn, performanceRun.getCoalesceGainLossOverride());

			Boolean includeGLOther = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(
					performanceRun.getPerformanceSummary().getClientAccount(), PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_INCLUDE_GL_OTHER_IN_DAILY_ACCOUNT_RETURN_CALCULATIONS);
			if (includeGLOther == null) {
				includeGLOther = false;
			}

			if (includeGLOther) {
				accountReturn = MathUtils.add(accountReturn, performanceRun.getCoalesceGainLossOtherOverride());
			}

			// Multiply By 100 to set value as a percentage
			performanceRun.setAccountReturn(MathUtils.multiply(MathUtils.divide(accountReturn, absTargetSummaryTarget), MathUtils.BIG_DECIMAL_ONE_HUNDRED));
		}
	}


	private BigDecimal getReplicationGainLoss(PerformancePortfolioTargetRunConfig dailyConfig, PortfolioTargetRunReplication replication) {
		// If there is only one target that uses this replication - give it all
		List<AccountingPositionDaily> instrumentPositionList = dailyConfig.getInstrumentPositionList(replication.getSecurity().getInstrument());
		BigDecimal instrumentGainLoss = CoreMathUtils.sumProperty(instrumentPositionList, AccountingPositionDaily::getDailyGainLossBase);

		// need to get percentage of gain loss for multiple targets using the replication
		List<PortfolioTargetRunReplication> instrumentRepList = dailyConfig.getInstrumentReplicationList(replication.getSecurity().getInstrument());
		if (!CollectionUtils.isEmpty(instrumentPositionList) && instrumentRepList.size() > 1) {
			BigDecimal totalQuantity = CoreMathUtils.sumProperty(instrumentRepList, PortfolioTargetRunReplication::getActualContractsAdjusted);
			// If actual total is zero, use targets for proportional allocation
			// shouldn't happen very often, only when positions are first put on and split across asset classes
			// If the analyst doesn't like the split, he can re-allocate to the asset classes he wants using the overrides.
			boolean useTarget = false;
			if (MathUtils.isNullOrZero(totalQuantity)) {
				useTarget = true;
				totalQuantity = CoreMathUtils.sumProperty(instrumentRepList, PortfolioTargetRunReplication::getTargetContractsAdjusted);
			}
			BigDecimal percentage = CoreMathUtils.getPercentValue(useTarget ? replication.getTargetContractsAdjusted() : replication.getActualContractsAdjusted(), totalQuantity, true);
			instrumentGainLoss = MathUtils.getPercentageOf(percentage, instrumentGainLoss, true);
		}

		return instrumentGainLoss;
	}


	private BigDecimal getReplicationCurrencyGainLoss(PerformancePortfolioTargetRunConfig dailyConfig, PortfolioTargetRunReplication replication) {
		BigDecimal currencyGainLoss = BigDecimal.ZERO;
		if (replication.isCurrencyReplication() && replication.getUnderlyingSecurity() != null) {
			// if one use of replication - give it all
			InvestmentSecurity currencySecurity = replication.getUnderlyingSecurity();
			List<AccountingPositionDaily> currencyPositionList = dailyConfig.getCurrencyPositionList(currencySecurity);
			if (!CollectionUtils.isEmpty(currencyPositionList)) {
				currencyGainLoss = CoreMathUtils.sumProperty(currencyPositionList, AccountingPositionDaily::getDailyGainLossBase);

				List<PortfolioTargetRunReplication> currencyRepList = dailyConfig.getCurrencyReplicationList(currencySecurity);
				if (CollectionUtils.getSize(currencyRepList) > 1) {
					// need to split it
					BigDecimal totalAmount = CoreMathUtils.sumProperty(currencyRepList, PortfolioTargetRunReplication::getCurrencyActualLocalAmount);
					BigDecimal percentage = CoreMathUtils.getPercentValue(replication.getCurrencyActualLocalAmount(), totalAmount, true);
					currencyGainLoss = MathUtils.getPercentageOf(percentage, currencyGainLoss, true);
				}
			}
			// Currency Other - Track total currency other applied to this asset class - likely 100%
			if (!CollectionUtils.isEmpty(dailyConfig.getCurrencyOtherList()) && !MathUtils.isNullOrZero(replication.getCurrencyOtherBaseAmount())) {
				dailyConfig.currencyOtherTotal = MathUtils.add(dailyConfig.currencyOtherTotal, replication.getCurrencyOtherBaseAmount());
			}
		}
		return currencyGainLoss;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private PerformancePortfolioTargetRunConfig setupPerformancePortfolioTargetDailyConfig(PerformancePortfolioTargetProcessConfig config, PerformancePortfolioRun performanceRun) {
		PerformancePortfolioTargetRunConfig dailyConfig = new PerformancePortfolioTargetRunConfig();
		// Total Gain/Loss for the day - Once replications are processed, anything left over is put into the Gain Loss Other field - some accounts include it in reporting
		List<AccountingPositionDaily> positionList = config.getPositionListForDate(performanceRun.getMeasureDate());
		setupPerformancePortfolioTargetDailyPositionDetails(dailyConfig, positionList);
		setupPerformancePortfolioTargetDailyReplicationDetails(dailyConfig, performanceRun);
		setupPerformancePortfolioTargetDailyCurrencyDetails(dailyConfig, performanceRun, positionList);

		return dailyConfig;
	}


	private void setupPerformancePortfolioTargetDailyPositionDetails(PerformancePortfolioTargetRunConfig dailyConfig, List<AccountingPositionDaily> positionList) {
		// Total Gain/Loss for the day - Once replications are processed, anything left over is put into the Gain Loss Other field - some accounts include it in reporting
		for (AccountingPositionDaily positionDaily : positionList) {
			dailyConfig.totalGainLoss = MathUtils.add(dailyConfig.totalGainLoss, positionDaily.getDailyGainLossBase());
			if (positionDaily.getAccountingTransaction() != null) {
				dailyConfig.instrumentPositionMap.computeIfAbsent(positionDaily.getAccountingTransaction().getInvestmentSecurity().getInstrument().getId(), key -> new ArrayList<>())
						.add(positionDaily);
				dailyConfig.currencyPositionMap.computeIfAbsent(positionDaily.getAccountingTransaction().getInvestmentSecurity().getId(), key -> new ArrayList<>())
						.add(positionDaily);
			}
		}
	}


	private void setupPerformancePortfolioTargetDailyReplicationDetails(PerformancePortfolioTargetRunConfig dailyConfig, PerformancePortfolioRun performanceRun) {
		dailyConfig.runTargetSummaryList = getPortfolioTargetSummaryListForRun(performanceRun);
		CollectionUtils.getStream(dailyConfig.runTargetSummaryList)
				.map(PortfolioTargetRunTargetSummary::getReplicationList)
				.flatMap(CollectionUtils::getStream)
				.filter(replication -> !replication.isCashExposure())
				.forEach(replication -> {
					dailyConfig.instrumentReplicationMap.computeIfAbsent(replication.getSecurity().getInstrument().getId(), key -> new ArrayList<>())
							.add(replication);
					// Currency Gain/Loss is applied to the security gain/loss at the proportion the actual currency and actual currency other amount is allocated to it
					if (replication.isCurrencyReplication()) {
						dailyConfig.currencyReplicationMap.computeIfAbsent(replication.getUnderlyingSecurity().getId(), key -> new ArrayList<>())
								.add(replication);
					}
				});
	}


	private List<PortfolioTargetRunTargetSummary> getPortfolioTargetSummaryListForRun(PerformancePortfolioRun performanceRun) {
		if (performanceRun.getPortfolioRun() == null) {
			return null;
		}

		PortfolioTargetRunTargetSummaryCommand command = new PortfolioTargetRunTargetSummaryCommand();
		command.setRunId(performanceRun.getPortfolioRun().getId());
		return getPortfolioTargetRunService().getPortfolioTargetRunTargetSummaryList(command);
	}


	private void setupPerformancePortfolioTargetDailyCurrencyDetails(PerformancePortfolioTargetRunConfig dailyConfig, PerformancePortfolioRun performanceRun, List<AccountingPositionDaily> positionList) {
		dailyConfig.setCurrencyOtherList(getPortfolioRunService().getPortfolioCurrencyOtherListByRun(performanceRun.getPortfolioRun().getId()));
		if (!CollectionUtils.isEmpty(dailyConfig.getCurrencyOtherList())) {
			// Currency Other Positions
			Set<InvestmentSecurity> currencySecuritySet = dailyConfig.getCurrencyOtherList().stream()
					.map(PortfolioCurrencyOther::getCurrencySecurity)
					.collect(Collectors.toSet());
			List<AccountingPositionDaily> currencyOtherPositionList = BeanUtils.filter(positionList, accountingPositionDaily -> currencySecuritySet.contains(accountingPositionDaily.getInvestmentSecurity()));
			dailyConfig.currencyOtherTotalGainLoss = CoreMathUtils.sumProperty(currencyOtherPositionList, AccountingPositionDaily::getDailyGainLossBase);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	private static final class PerformancePortfolioTargetRunConfig {

		private final Map<Integer, List<AccountingPositionDaily>> instrumentPositionMap = new HashMap<>();
		private final Map<Integer, List<AccountingPositionDaily>> currencyPositionMap = new HashMap<>();
		private BigDecimal totalGainLoss = BigDecimal.ZERO;

		private final Map<Integer, List<PortfolioTargetRunReplication>> instrumentReplicationMap = new HashMap<>();
		private final Map<Integer, List<PortfolioTargetRunReplication>> currencyReplicationMap = new HashMap<>();

		private List<PortfolioTargetRunTargetSummary> runTargetSummaryList = null;

		private List<PortfolioCurrencyOther> currencyOtherList = null;
		private BigDecimal totalBaseCurrencyOther = BigDecimal.ZERO;
		private BigDecimal currencyOtherTotalGainLoss = BigDecimal.ZERO;
		private BigDecimal currencyOtherTotal = BigDecimal.ZERO;

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		public List<AccountingPositionDaily> getInstrumentPositionList(InvestmentInstrument instrument) {
			return this.instrumentPositionMap.get(instrument.getId());
		}


		public List<AccountingPositionDaily> getCurrencyPositionList(InvestmentSecurity currency) {
			return this.currencyPositionMap.get(currency.getId());
		}


		public BigDecimal getTotalGainLoss() {
			return this.totalGainLoss;
		}


		public List<PortfolioTargetRunReplication> getInstrumentReplicationList(InvestmentInstrument instrument) {
			return this.instrumentReplicationMap.get(instrument.getId());
		}


		public List<PortfolioTargetRunReplication> getCurrencyReplicationList(InvestmentSecurity currency) {
			return this.currencyReplicationMap.get(currency.getId());
		}


		public List<PortfolioTargetRunTargetSummary> getRunTargetSummaryList() {
			return this.runTargetSummaryList;
		}


		public List<PortfolioCurrencyOther> getCurrencyOtherList() {
			return this.currencyOtherList;
		}


		public void setCurrencyOtherList(List<PortfolioCurrencyOther> currencyOtherList) {
			this.currencyOtherList = currencyOtherList;
			this.totalBaseCurrencyOther = CoreMathUtils.sumProperty(currencyOtherList, PortfolioCurrencyOther::getTotalBaseAmount);
		}


		public BigDecimal getTotalBaseCurrencyOther() {
			return this.totalBaseCurrencyOther;
		}


		public BigDecimal getCurrencyOtherTotalGainLoss() {
			return this.currencyOtherTotalGainLoss;
		}


		public BigDecimal getCurrencyOtherTotal() {
			return this.currencyOtherTotal;
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioTargetService getPerformancePortfolioTargetService() {
		return this.performancePortfolioTargetService;
	}


	public void setPerformancePortfolioTargetService(PerformancePortfolioTargetService performancePortfolioTargetService) {
		this.performancePortfolioTargetService = performancePortfolioTargetService;
	}


	public PortfolioTargetRunService getPortfolioTargetRunService() {
		return this.portfolioTargetRunService;
	}


	public void setPortfolioTargetRunService(PortfolioTargetRunService portfolioTargetRunService) {
		this.portfolioTargetRunService = portfolioTargetRunService;
	}
}
