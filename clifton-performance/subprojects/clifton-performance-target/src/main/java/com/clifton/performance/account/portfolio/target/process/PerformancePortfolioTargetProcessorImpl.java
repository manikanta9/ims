package com.clifton.performance.account.portfolio.target.process;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.BasePerformanceInvestmentAccountPortfolioRunProcessor;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * <code>ProductPerformanceOverlayProcessorImpl</code> provides processing logic for PORTFOLIO_TARGET processing.
 *
 * @author nickk
 */
public class PerformancePortfolioTargetProcessorImpl extends BasePerformanceInvestmentAccountPortfolioRunProcessor<PerformancePortfolioTargetProcessConfig> {

	private PerformancePortfolioTargetService performancePortfolioTargetService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformancePortfolioTargetProcessConfig createPerformanceInvestmentAccountConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		return new PerformancePortfolioTargetProcessConfig(performanceAccount, processingType);
	}


	@Override
	@Transactional
	public void deletePerformanceInvestmentAccountProcessing(int performanceAccountId) {
		getPerformanceBenchmarkService().deletePerformanceSummaryBenchmarkListBySummary(performanceAccountId);
		// Delete performance run and associated entities
		List<PerformancePortfolioRun> performancePortfolioRunList = getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(performanceAccountId);
		for (PerformancePortfolioRun performancePortfolioRun : CollectionUtils.getIterable(performancePortfolioRunList)) {
			getPerformancePortfolioTargetService().deletePerformancePortfolioTargetSummaryListForRun(performancePortfolioRun.getId());
			if (performancePortfolioRun.getPortfolioRun() == null || performancePortfolioRun.getPortfolioRun().getServiceProcessingType() == ServiceProcessingTypes.PORTFOLIO_TARGET) {
				getPerformancePortfolioRunService().deletePerformancePortfolioRun(performancePortfolioRun.getId());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioTargetService getPerformancePortfolioTargetService() {
		return this.performancePortfolioTargetService;
	}


	public void setPerformancePortfolioTargetService(PerformancePortfolioTargetService performancePortfolioTargetService) {
		this.performancePortfolioTargetService = performancePortfolioTargetService;
	}
}
