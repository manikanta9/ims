package com.clifton.performance.account.portfolio.target.process;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PerformancePortfolioTargetProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "performance-target";
	}
}
