package com.clifton.performance.account.process.portfolio.run.comparisons;


import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PerformanceSummaryForPortfolioRunExistsComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private PortfolioRunService portfolioRunService;

	private static final int OVERLAY_RUN_USED = 10;
	private static final int OVERLAY_RUN_NOT_USED = 20;


	@Test
	public void testSummaryExists() {
		PerformanceSummaryForPortfolioRunExistsComparison existsComparison = new PerformanceSummaryForPortfolioRunExistsComparison();
		PerformanceSummaryForPortfolioRunNotExistsComparison notExistsComparison = new PerformanceSummaryForPortfolioRunNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(existsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(notExistsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		PortfolioRun run = this.portfolioRunService.getPortfolioRun(OVERLAY_RUN_USED);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(existsComparison.evaluate(run, context));
		Assertions.assertEquals("Found 1 performance summary using this portfolio run [000001: Test Account: 05/2014]", context.getTrueMessage());

		context = new SimpleComparisonContext();
		Assertions.assertFalse(notExistsComparison.evaluate(run, context));
		Assertions.assertEquals("Found 1 performance summary using this portfolio run [000001: Test Account: 05/2014]", context.getFalseMessage());
	}


	@Test
	public void testSummaryNotExists() {
		PerformanceSummaryForPortfolioRunExistsComparison existsComparison = new PerformanceSummaryForPortfolioRunExistsComparison();
		PerformanceSummaryForPortfolioRunNotExistsComparison notExistsComparison = new PerformanceSummaryForPortfolioRunNotExistsComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(existsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(notExistsComparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);

		PortfolioRun run = this.portfolioRunService.getPortfolioRun(OVERLAY_RUN_NOT_USED);

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(existsComparison.evaluate(run, context));
		Assertions.assertEquals("Found No performance summary using this portfolio run", context.getFalseMessage());

		context = new SimpleComparisonContext();
		Assertions.assertTrue(notExistsComparison.evaluate(run, context));
		Assertions.assertEquals("Found No performance summary using this portfolio run", context.getTrueMessage());
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
