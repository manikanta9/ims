package com.clifton.performance.account.process.portfolio.run;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class PerformancePortfolioProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "performance-portfolio";
	}
}
