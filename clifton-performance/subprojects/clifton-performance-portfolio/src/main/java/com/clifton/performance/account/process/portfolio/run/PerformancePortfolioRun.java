package com.clifton.performance.account.process.portfolio.run;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.portfolio.run.PortfolioRun;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformancePortfolioRun</code> class represents daily details used to calculate ProductPerformanceSummary information.
 * It rolls up asset class level data and performs additional daily calculations.
 * All amounts are in client's base currency.
 *
 * @author vgomelsky
 */
public class PerformancePortfolioRun extends BaseEntity<Integer> {

	private PerformanceInvestmentAccount performanceSummary;
	private Date measureDate;

	/**
	 * Summary Information for PIOS on a specific date uses the PIOS Run information from the previous business day
	 */
	private PortfolioRun portfolioRun;
	private Date portfolioRunDate; // In cases where a run is not available, store the date so someone can come in and set these values manually.

	private BigDecimal gainLoss; // since previous performance detail (business day) - sum of asset classes
	private BigDecimal target; // SUM(assetClassTarget) -

	private BigDecimal gainLossOverride; // roll up of asset class levels: null if no overrides, otherwise SUM(COALESCE(gainLossOverride, gainLoss))
	private BigDecimal targetOverride; // -//-

	/**
	 * Gain/Loss for other positions that aren't included in PIOS.
	 * Example: Continental has ETFs which aren't included in PIOS, but the Gain/Loss for the Chart they'd like to include this
	 * data
	 */
	private BigDecimal gainLossOther;

	/**
	 * Explicit override of Gain/Loss other that was calculated by the system.
	 * This override can allow moving the gain/loss other to a specific asset class (case would be a position wasn't on a run on a day, but was there the next day,
	 * if we don't have that position on yet, then the system doesn't know what asset class to put the gain/loss in for that contract and puts it in other)
	 * This allows the analyst to override the "other" value and add it to a specific asset class.
	 */
	private BigDecimal gainLossOtherOverride;

	/**
	 * If set, uses this value in calculation for the account return instead of Target * financing rate
	 */
	private BigDecimal financingAmountOverride;

	// Daily Return = (gainLoss + Target*financingRate) / SUM(ABS(assetClassTarget))
	private BigDecimal accountReturn;

	// benchmark return for each asset class
	// asset class benchmark return * assetClassTarget / SUM(ABS(assetClassTarget))
	private BigDecimal benchmarkReturn;

	private BigDecimal trackingErrorBenchmarkReturn;
	private BigDecimal trackingErrorAdjustedTargetAllocationPercent;
	private BigDecimal trackingErrorPhysicalExposurePercent;
	private BigDecimal trackingErrorTotalExposurePercent;

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	/**
	 * Returns an instance of {@link PerformancePortfolioRun} that can be saved.
	 * Classes that extend this class must call this before saving to ensure ORM mappings.
	 */
	public PerformancePortfolioRun toPerformancePortfolioRun() {
		if (PerformancePortfolioRun.class.equals(this.getClass())) {
			return this;
		}
		PerformancePortfolioRun performanceRun = new PerformancePortfolioRun();
		BeanUtils.copyProperties(this, performanceRun);
		return performanceRun;
	}


	/**
	 * Copy properties from the source into this object.
	 * <p>
	 * When classes extend this class, binding the DTOs does not always occur. Thus,
	 * the binding has to occur manually with the updated properties being copied to an existing entity.
	 * This method copies necessary details from the source to the destination for saving.
	 */
	public <T extends PerformancePortfolioRun> void copyProperties(T source) {
		// Copy values where this has null values from processing of performance summaries
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getGainLoss, PerformancePortfolioRun::setGainLoss, false);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getGainLossOther, PerformancePortfolioRun::setGainLossOther, false);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getTarget, PerformancePortfolioRun::setTarget, false);

		// copy overridable properties, often populated from the source via the UI.
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getGainLossOverride, PerformancePortfolioRun::setGainLossOverride, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getGainLossOtherOverride, PerformancePortfolioRun::setGainLossOtherOverride, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getTargetOverride, PerformancePortfolioRun::setTargetOverride, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getFinancingAmountOverride, PerformancePortfolioRun::setFinancingAmountOverride, true);

		// Values calculated from the above values
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getAccountReturn, PerformancePortfolioRun::setAccountReturn, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getBenchmarkReturn, PerformancePortfolioRun::setBenchmarkReturn, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getTrackingErrorBenchmarkReturn, PerformancePortfolioRun::setTrackingErrorBenchmarkReturn, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getTrackingErrorAdjustedTargetAllocationPercent, PerformancePortfolioRun::setTrackingErrorAdjustedTargetAllocationPercent, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getTrackingErrorPhysicalExposurePercent, PerformancePortfolioRun::setTrackingErrorPhysicalExposurePercent, true);
		BeanUtils.copyProperty(source, this, PerformancePortfolioRun::getTrackingErrorTotalExposurePercent, PerformancePortfolioRun::setTrackingErrorTotalExposurePercent, true);
	}

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public BigDecimal getCoalesceGainLossOverride() {
		if (getGainLossOverride() == null) {
			return getGainLoss();
		}
		return getGainLossOverride();
	}


	public BigDecimal getCoalesceGainLossOtherOverride() {
		if (getGainLossOtherOverride() == null) {
			return getGainLossOther();
		}
		return getGainLossOtherOverride();
	}


	public BigDecimal getCoalesceGainLossTotalOverride() {
		return MathUtils.add(getCoalesceGainLossOverride(), getCoalesceGainLossOtherOverride());
	}


	public BigDecimal getCoalesceTargetOverride() {
		if (getTargetOverride() == null) {
			return getTarget();
		}
		return getTargetOverride();
	}


	public String getMeasureDateLabel() {
		if (getMeasureDate() != null) {
			return DateUtils.fromDateShort(getMeasureDate());
		}
		return null;
	}


	public BigDecimal getReturnDifference() {
		return MathUtils.subtract(getAccountReturn(), getBenchmarkReturn());
	}


	public BigDecimal getTargetDifference() {
		if (getTargetOverride() == null) {
			return BigDecimal.ZERO;
		}
		return MathUtils.subtract(getTargetOverride(), getTarget());
	}


	public BigDecimal getGainLossDifference() {
		if (getGainLossOverride() == null) {
			return BigDecimal.ZERO;
		}
		return MathUtils.subtract(getGainLossOverride(), getGainLoss());
	}


	public BigDecimal getGainLossOtherDifference() {
		if (getGainLossOtherOverride() == null) {
			return BigDecimal.ZERO;
		}
		return MathUtils.subtract(getGainLossOtherOverride(), getGainLossOther());
	}

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public PortfolioRun getPortfolioRun() {
		return this.portfolioRun;
	}


	public void setPortfolioRun(PortfolioRun portfolioRun) {
		this.portfolioRun = portfolioRun;
	}


	public BigDecimal getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(BigDecimal gainLoss) {
		this.gainLoss = gainLoss;
	}


	public BigDecimal getTarget() {
		return this.target;
	}


	public void setTarget(BigDecimal target) {
		this.target = target;
	}


	public BigDecimal getGainLossOverride() {
		return this.gainLossOverride;
	}


	public void setGainLossOverride(BigDecimal gainLossOverride) {
		this.gainLossOverride = gainLossOverride;
	}


	public BigDecimal getTargetOverride() {
		return this.targetOverride;
	}


	public void setTargetOverride(BigDecimal targetOverride) {
		this.targetOverride = targetOverride;
	}


	public BigDecimal getAccountReturn() {
		return this.accountReturn;
	}


	public void setAccountReturn(BigDecimal accountReturn) {
		this.accountReturn = accountReturn;
	}


	public BigDecimal getBenchmarkReturn() {
		return this.benchmarkReturn;
	}


	public void setBenchmarkReturn(BigDecimal benchmarkReturn) {
		this.benchmarkReturn = benchmarkReturn;
	}


	public PerformanceInvestmentAccount getPerformanceSummary() {
		return this.performanceSummary;
	}


	public void setPerformanceSummary(PerformanceInvestmentAccount performanceSummary) {
		this.performanceSummary = performanceSummary;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Date getPortfolioRunDate() {
		return this.portfolioRunDate;
	}


	public void setPortfolioRunDate(Date portfolioRunDate) {
		this.portfolioRunDate = portfolioRunDate;
	}


	public BigDecimal getFinancingAmountOverride() {
		return this.financingAmountOverride;
	}


	public void setFinancingAmountOverride(BigDecimal financingAmountOverride) {
		this.financingAmountOverride = financingAmountOverride;
	}


	public BigDecimal getGainLossOther() {
		return this.gainLossOther;
	}


	public void setGainLossOther(BigDecimal gainLossOther) {
		this.gainLossOther = gainLossOther;
	}


	public BigDecimal getGainLossOtherOverride() {
		return this.gainLossOtherOverride;
	}


	public void setGainLossOtherOverride(BigDecimal gainLossOtherOverride) {
		this.gainLossOtherOverride = gainLossOtherOverride;
	}


	public BigDecimal getTrackingErrorBenchmarkReturn() {
		return this.trackingErrorBenchmarkReturn;
	}


	public void setTrackingErrorBenchmarkReturn(BigDecimal trackingErrorBenchmarkReturn) {
		this.trackingErrorBenchmarkReturn = trackingErrorBenchmarkReturn;
	}


	public BigDecimal getTrackingErrorAdjustedTargetAllocationPercent() {
		return this.trackingErrorAdjustedTargetAllocationPercent;
	}


	public void setTrackingErrorAdjustedTargetAllocationPercent(BigDecimal trackingErrorAdjustedTargetAllocationPercent) {
		this.trackingErrorAdjustedTargetAllocationPercent = trackingErrorAdjustedTargetAllocationPercent;
	}


	public BigDecimal getTrackingErrorPhysicalExposurePercent() {
		return this.trackingErrorPhysicalExposurePercent;
	}


	public void setTrackingErrorPhysicalExposurePercent(BigDecimal trackingErrorPhysicalExposurePercent) {
		this.trackingErrorPhysicalExposurePercent = trackingErrorPhysicalExposurePercent;
	}


	public BigDecimal getTrackingErrorTotalExposurePercent() {
		return this.trackingErrorTotalExposurePercent;
	}


	public void setTrackingErrorTotalExposurePercent(BigDecimal trackingErrorTotalExposurePercent) {
		this.trackingErrorTotalExposurePercent = trackingErrorTotalExposurePercent;
	}
}
