package com.clifton.performance.account.process.portfolio.run.rule;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService;
import com.clifton.performance.account.rule.PerformanceInvestmentAccountRuleEvaluatorContext;
import com.clifton.portfolio.run.PortfolioRun;

import java.util.ArrayList;
import java.util.List;


/**
 * @author nickk
 */
public class PerformanceInvestmentAccountPortfolioRunRuleEvaluatorContext extends PerformanceInvestmentAccountRuleEvaluatorContext {

	private static final String PERFORMANCE_DAILY_RUN_LIST = "PERFORMANCE_DAILY_RUN_LIST";

	/////////////////////////////////////////////////////////////////////////////

	private PerformancePortfolioRunService performancePortfolioRunService;

	/////////////////////////////////////////////////////////////////////////////


	public List<PortfolioRun> getPerformancePortfolioRunPortfolioRunList(PerformanceInvestmentAccount performanceAccount) {
		return CollectionUtils.createList(BeanUtils.getPropertyValuesExcludeNull(getPerformancePortfolioRunList(performanceAccount), PerformancePortfolioRun::getPortfolioRun, PortfolioRun.class));
	}


	public List<PerformancePortfolioRun> getPerformancePortfolioRunList(PerformanceInvestmentAccount performanceAccount) {
		@SuppressWarnings("unchecked")
		List<PerformancePortfolioRun> performanceRunList = (List<PerformancePortfolioRun>) getContext().getBean(PERFORMANCE_DAILY_RUN_LIST);

		if (performanceRunList == null) {
			performanceRunList = getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(performanceAccount.getId());
			if (performanceRunList == null) {
				performanceRunList = new ArrayList<>();
			}
			getContext().setBean(PERFORMANCE_DAILY_RUN_LIST, performanceRunList);
		}
		return performanceRunList;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods            //////////////
	/////////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioRunService getPerformancePortfolioRunService() {
		return this.performancePortfolioRunService;
	}


	public void setPerformancePortfolioRunService(PerformancePortfolioRunService performancePortfolioRunService) {
		this.performancePortfolioRunService = performancePortfolioRunService;
	}
}
