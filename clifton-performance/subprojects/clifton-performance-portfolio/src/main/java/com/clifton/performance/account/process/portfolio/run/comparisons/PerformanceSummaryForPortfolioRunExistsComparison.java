package com.clifton.performance.account.process.portfolio.run.comparisons;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService;
import com.clifton.portfolio.run.PortfolioRun;

import java.util.List;


/**
 * The <code>PerformanceSummaryForPortfolioRunExistsComparison</code> is used to determine if a summary exists
 * that is associated with a specific run.
 *
 * @author manderson
 */
public class PerformanceSummaryForPortfolioRunExistsComparison implements Comparison<PortfolioRun> {

	private PerformancePortfolioRunService performancePortfolioRunService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(PortfolioRun run, ComparisonContext context) {
		AssertUtils.assertNotNull(run, "Portfolio Run is required to evaluate Performance Summary Exists comparison for it.");

		List<PerformancePortfolioRun> performanceRunList = getPerformancePortfolioRunService().getPerformancePortfolioRunListByPortfolioRun(run.getId());

		int size = CollectionUtils.getSize(performanceRunList);
		boolean result = (size != 0);
		if (isReverse()) {
			result = !result;
		}

		if (context != null) {
			String message = "Found " + (size == 0 ? "No" : size) + " performance " + (size > 1 ? "summaries" : "summary") + " using this portfolio run";
			if (size > 0) {
				message = message + " [" + BeanUtils.getPropertyValues(performanceRunList, "performanceSummary.label", ",") + "]";
			}
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioRunService getPerformancePortfolioRunService() {
		return this.performancePortfolioRunService;
	}


	public void setPerformancePortfolioRunService(PerformancePortfolioRunService performancePortfolioRunService) {
		this.performancePortfolioRunService = performancePortfolioRunService;
	}
}
