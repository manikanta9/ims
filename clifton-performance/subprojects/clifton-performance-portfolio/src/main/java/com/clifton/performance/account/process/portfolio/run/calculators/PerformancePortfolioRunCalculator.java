package com.clifton.performance.account.process.portfolio.run.calculators;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.replication.InvestmentReplicationService;
import com.clifton.investment.replication.InvestmentReplicationType;
import com.clifton.marketdata.rates.MarketDataRatesRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.process.calculators.PerformanceInvestmentAccountCalculator;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunProcessConfig;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunService;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * The <code>PerformancePortfolioRunCalculator</code> populates the {@link PerformancePortfolioRun} for each day
 * for the {@link PerformanceInvestmentAccount}
 * <p/>
 * NOTE: This calculator just populates the run information - gain/loss, overlay target, tracking error, etc
 * values are to be populated in separate calculators ordered after it.
 *
 * @author nickk
 */
@Component
public class PerformancePortfolioRunCalculator<T extends PerformancePortfolioRunProcessConfig> implements PerformanceInvestmentAccountCalculator<T> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentReplicationService investmentReplicationService;
	private MarketDataRatesRetriever marketDataRatesRetriever;
	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;
	private PerformancePortfolioRunService performancePortfolioRunService;
	private PortfolioRunService portfolioRunService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public void calculate(T config, Date date) {
		setupProcessing(config);
		if (date == null) {
			config.setPerformanceRunList(generatePerformancePortfolioRunList(config));
		}
		else {
			// If only editing one date - then nothing to actually process at this level
			// pull existing list and set it - any changes will following in subsequent calculators
			config.setPerformanceRunList(getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(config.getPerformanceAccount().getId()));
		}
	}


	@Override
	public boolean calculateHistoricalReturns(T config) {
		// DO NOTHING - NO HISTORICAL RETURNS AT DAILY DETAIL LEVEL
		return false;
	}


	@Override
	public void saveResults(T config, Date date) {
		if (date == null && !CollectionUtils.isEmpty(config.getPerformanceRunList())) {
			List<PerformancePortfolioRun> savableList = CollectionUtils.getConverted(config.getPerformanceRunList(), PerformancePortfolioRun::toPerformancePortfolioRun);
			getPerformancePortfolioRunService().savePerformancePortfolioRunListForSummary(config.getPerformanceAccount().getId(), savableList);
			config.setPerformanceRunList(savableList);
		}
		// If only editing one date - then nothing was actually processed at this point so nothing to save
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Transactional(readOnly = true)
	protected void setupProcessing(T config) {
		Boolean doNotUseFinancingRate = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(config.getPerformanceAccount().getClientAccount(),
				PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_DO_NOT_USE_FINANCING_RATE);
		if (doNotUseFinancingRate == null) {
			doNotUseFinancingRate = false;
		}

		if (!doNotUseFinancingRate && CollectionUtils.isEmpty(config.getInterestRateDailyChangeIndexMap())) {
			config.setInterestRateDailyChangeIndexMap(generateInterestRateDailyChangeIndexMap(config.getPerformanceAccount()));
		}
		if (CollectionUtils.isEmpty(config.getRepoAccruedInterestMap())) {
			config.setRepoAccruedInterestMap(getPerformanceInvestmentAccountDataRetriever().getLendingRepoAccruedInterestMapForPerformanceAccount(config.getPerformanceAccount()));
		}
		if (CollectionUtils.isEmpty(config.getDailyPositionMap())) {
			config.setDailyPositionMap(getPerformanceInvestmentAccountDataRetriever().getAccountingPositionDailyMapForPerformanceAccount(config.getPerformanceAccount().getClientAccount(), config.getPerformanceAccount().getAccountingPeriod(), false, true, true));
		}
	}

	///////////////////////////////////////////////////////////////////////////
	////////////                Market Data Methods                ////////////
	///////////////////////////////////////////////////////////////////////////


	protected Map<Integer, BigDecimal> generateInterestRateDailyChangeIndexMap(PerformanceInvestmentAccount performanceAccount) {
		// Originally was using the first day of the month, but per Greg L should be using the last day of the previous month
		// This has been their convention in the past.  Likely wasn't noticed because most times
		// this really doesn't make a different because the rates are low and de-annualized that they evaluate to just about the same (especially when rounding to 4 decimal places on screen)
		Date date = DateUtils.getLastWeekdayOfPreviousMonth(performanceAccount.getAccountingPeriod().getStartDate());

		// Get a list of all replication types and set the interest rate for the given date (should be the first business day of the month) for each index that is used
		return CollectionUtils.getStream(getInvestmentReplicationService().getInvestmentReplicationTypeList())
				.map(InvestmentReplicationType::getFinancingRateIndex)
				.filter(Objects::nonNull)
				.distinct()
				.collect(Collectors.toMap(BaseSimpleEntity::getId, index -> getInterestRateIndexDailyChange(index, date)));
	}


	protected void addInterestRateIndexDailyChangeToMap(Map<Integer, BigDecimal> map, InvestmentInterestRateIndex index, Date date) {
		if (index != null) {
			map.computeIfAbsent(index.getId(), key -> getInterestRateIndexDailyChange(index, date));
		}
	}


	private BigDecimal getInterestRateIndexDailyChange(InvestmentInterestRateIndex index, Date date) {
		BigDecimal rate = getMarketDataRatesRetriever().getMarketDataInterestRateForDate(index, date, true);
		BigDecimal dailyChange = MathUtils.getDeAnnualizedRate(rate);
		return MathUtils.multiply(dailyChange, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
	}

	///////////////////////////////////////////////////////////////////////////
	////////////              Portfolio Run Methods                ////////////
	///////////////////////////////////////////////////////////////////////////


	protected <P extends PerformancePortfolioRun> List<P> generatePerformancePortfolioRunList(T config) {
		PerformanceInvestmentAccount performanceInvestmentAccount = config.getPerformanceAccount();
		List<P> runList = new ArrayList<>();
		List<P> existingRunList = getPerformancePortfolioRunListForPerformanceSummary(performanceInvestmentAccount);
		Map<String, P> dateToPerformancePortfolioRunMap = BeanUtils.getBeanMap(existingRunList, PerformancePortfolioRun::getMeasureDateLabel);

		Date date = config.getStartDate();
		if (!DateUtils.isWeekday(date)) {
			date = DateUtils.getNextWeekday(date);
		}
		Date endDate = config.getEndDate();
		Date previousDate = DateUtils.getPreviousWeekday(date);

		while (DateUtils.compare(date, endDate, false) <= 0) {
			P performanceRun = dateToPerformancePortfolioRunMap.get(DateUtils.fromDateShort(date));
			if (performanceRun == null) {
				performanceRun = createNewPerformancePortfolioRun();
				performanceRun.setPerformanceSummary(performanceInvestmentAccount);
				performanceRun.setMeasureDate(date);
			}

			// If re-processing - need to support updating the "main run"
			// If summary parent account is set - use it, otherwise use the run for the account
			PortfolioRun run = getPortfolioRunForSummaryAndDate(performanceInvestmentAccount, previousDate);
			// If No Run and Is Not A Business Day - OK - check previous
			// Otherwise Use It
			while (run == null && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(previousDate))) {
				previousDate = DateUtils.getPreviousWeekday(previousDate);
				run = getPortfolioRunForSummaryAndDate(performanceInvestmentAccount, previousDate);
			}

			// Once we have the run - set the Previous Date Correctly based on the Run that we are using:
			// Overlay Run for the Measure Date is actually from the previous day.  (used for overlay targets)
			// Reset for existing in case calendar has changed
			performanceRun.setPortfolioRunDate(previousDate);
			performanceRun.setPortfolioRun(run);
			runList.add(performanceRun);

			// Move to Next Weekday
			previousDate = date;
			date = DateUtils.getNextWeekday(date);
		}
		return runList;
	}


	@SuppressWarnings("unchecked")
	protected <P extends PerformancePortfolioRun> List<P> getPerformancePortfolioRunListForPerformanceSummary(PerformanceInvestmentAccount performanceInvestmentAccount) {
		return (List<P>) getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(performanceInvestmentAccount.getId());
	}


	@SuppressWarnings("unchecked")
	protected <P extends PerformancePortfolioRun> P createNewPerformancePortfolioRun() {
		return (P) new PerformancePortfolioRun();
	}


	protected PortfolioRun getPortfolioRunForSummaryAndDate(PerformanceInvestmentAccount summary, Date date) {
		PortfolioRun run = null;
		if (summary.getParentClientAccount() != null) {
			run = getPortfolioRunService().getPortfolioRunByMainRun(summary.getParentClientAccount().getId(), date);
		}
		// If no parent account - or none for the parent account - check the account specifically
		// helpful when sub-account relationships are added mid-month
		if (run == null) {
			run = getPortfolioRunService().getPortfolioRunByMainRun(summary.getClientAccount().getId(), date);
		}
		return run;
	}
	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentReplicationService getInvestmentReplicationService() {
		return this.investmentReplicationService;
	}


	public void setInvestmentReplicationService(InvestmentReplicationService investmentReplicationService) {
		this.investmentReplicationService = investmentReplicationService;
	}


	public MarketDataRatesRetriever getMarketDataRatesRetriever() {
		return this.marketDataRatesRetriever;
	}


	public void setMarketDataRatesRetriever(MarketDataRatesRetriever marketDataRatesRetriever) {
		this.marketDataRatesRetriever = marketDataRatesRetriever;
	}


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformancePortfolioRunService getPerformancePortfolioRunService() {
		return this.performancePortfolioRunService;
	}


	public void setPerformancePortfolioRunService(PerformancePortfolioRunService performancePortfolioRunService) {
		this.performancePortfolioRunService = performancePortfolioRunService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
