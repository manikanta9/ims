package com.clifton.performance.account.process.portfolio.run.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>PerformancePortfolioRunSearchForm</code>
 *
 * @author manderson
 */
public class PerformancePortfolioRunSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchFieldPath = "performanceSummary", searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "performanceSummary.id")
	private Integer performanceSummaryId;

	@SearchField(searchFieldPath = "performanceSummary", searchField = "accountingPeriod.id")
	private Integer accountingPeriodId;

	@SearchField(searchField = "performanceSummary.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] performanceSummaryIds;

	@SearchField
	private Date measureDate;

	@SearchField(searchField = "measureDate", comparisonConditions = ComparisonConditions.LESS_THAN_OR_EQUALS)
	private Date maxMeasureDate;

	@SearchField
	private Date portfolioRunDate;

	@SearchField(searchField = "portfolioRun.id")
	private Integer portfolioRunId;

	@SearchField
	private BigDecimal gainLoss;

	@SearchField
	private BigDecimal target;

	@SearchField
	private BigDecimal gainLossOverride;

	@SearchField
	private BigDecimal targetOverride;

	@SearchField(searchField = "gainLoss,gainLossOverride", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalesceGainLossOverride;

	@SearchField(searchField = "target,targetOverride", searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private BigDecimal coalesceTargetOverride;

	@SearchField
	private BigDecimal gainLossOther;

	@SearchField
	private BigDecimal gainLossOtherOverride;

	@SearchField
	private BigDecimal financingAmountOverride;

	@SearchField(searchField = "gainLossOverride,gainLossOtherOverride", comparisonConditions = ComparisonConditions.IS_NOT_NULL, searchFieldCustomType = SearchFieldCustomTypes.OR)
	private Boolean gainLossOrGainLossOtherOverridden;

	////////////////////////////////////////////////////////////
	////////        Getter and Setter Methods          /////////
	////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getPerformanceSummaryId() {
		return this.performanceSummaryId;
	}


	public void setPerformanceSummaryId(Integer performanceSummaryId) {
		this.performanceSummaryId = performanceSummaryId;
	}


	public Integer[] getPerformanceSummaryIds() {
		return this.performanceSummaryIds;
	}


	public void setPerformanceSummaryIds(Integer[] performanceSummaryIds) {
		this.performanceSummaryIds = performanceSummaryIds;
	}


	public Date getMeasureDate() {
		return this.measureDate;
	}


	public void setMeasureDate(Date measureDate) {
		this.measureDate = measureDate;
	}


	public Date getPortfolioRunDate() {
		return this.portfolioRunDate;
	}


	public void setPortfolioRunDate(Date portfolioRunDate) {
		this.portfolioRunDate = portfolioRunDate;
	}


	public Integer getPortfolioRunId() {
		return this.portfolioRunId;
	}


	public void setPortfolioRunId(Integer portfolioRunId) {
		this.portfolioRunId = portfolioRunId;
	}


	public BigDecimal getGainLoss() {
		return this.gainLoss;
	}


	public void setGainLoss(BigDecimal gainLoss) {
		this.gainLoss = gainLoss;
	}


	public BigDecimal getTarget() {
		return this.target;
	}


	public void setTarget(BigDecimal target) {
		this.target = target;
	}


	public BigDecimal getGainLossOverride() {
		return this.gainLossOverride;
	}


	public void setGainLossOverride(BigDecimal gainLossOverride) {
		this.gainLossOverride = gainLossOverride;
	}


	public BigDecimal getTargetOverride() {
		return this.targetOverride;
	}


	public void setTargetOverride(BigDecimal targetOverride) {
		this.targetOverride = targetOverride;
	}


	public BigDecimal getGainLossOther() {
		return this.gainLossOther;
	}


	public void setGainLossOther(BigDecimal gainLossOther) {
		this.gainLossOther = gainLossOther;
	}


	public BigDecimal getGainLossOtherOverride() {
		return this.gainLossOtherOverride;
	}


	public void setGainLossOtherOverride(BigDecimal gainLossOtherOverride) {
		this.gainLossOtherOverride = gainLossOtherOverride;
	}


	public BigDecimal getFinancingAmountOverride() {
		return this.financingAmountOverride;
	}


	public void setFinancingAmountOverride(BigDecimal financingAmountOverride) {
		this.financingAmountOverride = financingAmountOverride;
	}


	public Boolean getGainLossOrGainLossOtherOverridden() {
		return this.gainLossOrGainLossOtherOverridden;
	}


	public void setGainLossOrGainLossOtherOverridden(Boolean gainLossOrGainLossOtherOverridden) {
		this.gainLossOrGainLossOtherOverridden = gainLossOrGainLossOtherOverridden;
	}


	public Date getMaxMeasureDate() {
		return this.maxMeasureDate;
	}


	public void setMaxMeasureDate(Date maxMeasureDate) {
		this.maxMeasureDate = maxMeasureDate;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public BigDecimal getCoalesceGainLossOverride() {
		return this.coalesceGainLossOverride;
	}


	public void setCoalesceGainLossOverride(BigDecimal coalesceGainLossOverride) {
		this.coalesceGainLossOverride = coalesceGainLossOverride;
	}


	public BigDecimal getCoalesceTargetOverride() {
		return this.coalesceTargetOverride;
	}


	public void setCoalesceTargetOverride(BigDecimal coalesceTargetOverride) {
		this.coalesceTargetOverride = coalesceTargetOverride;
	}


	public Integer getAccountingPeriodId() {
		return this.accountingPeriodId;
	}


	public void setAccountingPeriodId(Integer accountingPeriodId) {
		this.accountingPeriodId = accountingPeriodId;
	}
}
