package com.clifton.performance.account.process.portfolio.run;

import com.clifton.accounting.period.AccountingPeriod;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetriever;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.benchmark.PerformanceBenchmarkService;
import com.clifton.performance.account.process.processor.BasePerformanceInvestmentAccountProcessorImpl;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;

import java.util.Date;


/**
 * <code>BasePerformanceInvestmentAccountPortfolioRunProcessor</code> is an abstract class that contains common methods,
 * components, and services for use with performance processing for client accounts utilizing portfolio runs.
 *
 * @author nickk
 */
public abstract class BasePerformanceInvestmentAccountPortfolioRunProcessor<T extends PerformancePortfolioRunProcessConfig> extends BasePerformanceInvestmentAccountProcessorImpl<T> {

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	private PerformanceBenchmarkService performanceBenchmarkService;
	private PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever;
	private PerformancePortfolioRunService performancePortfolioRunService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected T setupPerformanceInvestmentAccountConfig(T config, Date date) {
		config.setPerformanceInceptionDate(getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountInceptionDate(config.getPerformanceAccount().getClientAccount().getId()));
		// Full processing - not for a specific date lookup/set parent account if applies
		if (config.getProcessingType().isCalculate() && date == null) {
			populateParentAccount(config.getPerformanceAccount());
		}
		return config;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected void populateParentAccount(PerformanceInvestmentAccount performanceAccount) {
		InvestmentAccount clientAccount = performanceAccount.getClientAccount();
		AccountingPeriod accountingPeriod = performanceAccount.getAccountingPeriod();

		// Set the Parent Account if Necessary
		Boolean ignoreParent = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(clientAccount,
				PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_IGNORE_CLIFTON_SUB_ACCOUNT_RELATIONSHIPS);
		if (ignoreParent == null) {
			ignoreParent = false;
		}

		if (ignoreParent) {
			performanceAccount.setParentClientAccount(null);
		}
		else {
			// For Cases where parent/child relationships start/end mid month - check first for the active one as of the summary measure date
			// If none, then check for the first day of the month.  We only support one parent for the summary and we don't want to check for each day
			InvestmentAccount parentAccount = getPortfolioAccountDataRetriever().getMainAccountForCliftonSubAccount(clientAccount, false, accountingPeriod.getEndDate());
			if (parentAccount == null) {
				parentAccount = getPortfolioAccountDataRetriever().getMainAccountForCliftonSubAccount(clientAccount, false, accountingPeriod.getStartDate());
			}
			performanceAccount.setParentClientAccount(parentAccount);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PerformanceBenchmarkService getPerformanceBenchmarkService() {
		return this.performanceBenchmarkService;
	}


	public void setPerformanceBenchmarkService(PerformanceBenchmarkService performanceBenchmarkService) {
		this.performanceBenchmarkService = performanceBenchmarkService;
	}


	public PerformanceInvestmentAccountDataRetriever getPerformanceInvestmentAccountDataRetriever() {
		return this.performanceInvestmentAccountDataRetriever;
	}


	public void setPerformanceInvestmentAccountDataRetriever(PerformanceInvestmentAccountDataRetriever performanceInvestmentAccountDataRetriever) {
		this.performanceInvestmentAccountDataRetriever = performanceInvestmentAccountDataRetriever;
	}


	public PerformancePortfolioRunService getPerformancePortfolioRunService() {
		return this.performancePortfolioRunService;
	}


	public void setPerformancePortfolioRunService(PerformancePortfolioRunService performancePortfolioRunService) {
		this.performancePortfolioRunService = performancePortfolioRunService;
	}
}
