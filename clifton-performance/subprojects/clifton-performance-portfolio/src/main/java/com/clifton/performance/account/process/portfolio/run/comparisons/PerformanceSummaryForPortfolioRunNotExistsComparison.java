package com.clifton.performance.account.process.portfolio.run.comparisons;


/**
 * The <code>PerformanceSummaryForPortfolioRunNotExistsComparison</code> is used to determine if a summary does not exist
 * that is associated with a specific run.  Currently used to determine if the run can be re-processed
 *
 * @author manderson
 */
public class PerformanceSummaryForPortfolioRunNotExistsComparison extends PerformanceSummaryForPortfolioRunExistsComparison {


	@Override
	protected boolean isReverse() {
		return true;
	}
}
