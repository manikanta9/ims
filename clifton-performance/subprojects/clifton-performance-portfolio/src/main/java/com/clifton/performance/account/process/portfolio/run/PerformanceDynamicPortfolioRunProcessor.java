package com.clifton.performance.account.process.portfolio.run;

import com.clifton.business.service.ServiceProcessingTypes;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig;
import com.clifton.performance.account.process.portfolio.run.calculators.PerformancePortfolioRunCalculator;
import com.clifton.performance.account.process.processor.PerformanceInvestmentAccountProcessor;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * <code>PerformanceDynamicPortfolioRunProcessor</code> is a {@link PerformanceInvestmentAccountProcessor} for processing
 * performance summaries for client accounts utilizing {@link com.clifton.portfolio.run.PortfolioRun}s. This processor
 * processes each applicable {@link ServiceProcessingTypes} for the runs of the performance summary period.
 *
 * @author nickk
 */
@Component
public class PerformanceDynamicPortfolioRunProcessor<T extends PerformanceInvestmentAccountConfig> extends BasePerformanceInvestmentAccountPortfolioRunProcessor<PerformancePortfolioRunProcessConfig> {

	private PerformancePortfolioRunCalculator<PerformancePortfolioRunProcessConfig> performancePortfolioRunCalculator;

	private Map<String, PerformanceInvestmentAccountProcessor<T>> performanceServiceProcessingTypesProcessorMap;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public PerformancePortfolioRunProcessConfig createPerformanceInvestmentAccountConfig(PerformanceInvestmentAccount performanceInvestmentAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		return new PerformancePortfolioRunProcessConfig(performanceInvestmentAccount, processingType);
	}


	@Override
	public void processPerformanceInvestmentAccount(PerformancePortfolioRunProcessConfig config, Date date) {
		config = setupPerformanceInvestmentAccountConfig(config, date);
		// Load and save performance portfolio runs to get a list of runs for child processor processing
		processPerformancePortfolioRunCalculator(config, date);

		LinkedHashSet<PerformanceInvestmentAccountProcessor<T>> processorSet = getPerformanceInvestmentAccountProcessorLinkedSet(config.getPerformanceAccount(), config.getPerformanceRunList());
		for (PerformanceInvestmentAccountProcessor<T> processor : CollectionUtils.getIterable(processorSet)) {
			T childConfig = processor.createPerformanceInvestmentAccountConfig(config.getPerformanceAccount(), config.getProcessingType());
			// copy common properties from the parent to child configurations to avoid double lookups (e.g., positions, repo data, etc.)
			BeanUtils.copyProperties(config, childConfig);
			processor.processPerformanceInvestmentAccount(childConfig, date);
		}
	}


	@Override
	@Transactional
	public void deletePerformanceInvestmentAccountProcessing(int performanceAccountId) {
		// Delete Benchmarks
		getPerformanceBenchmarkService().deletePerformanceSummaryBenchmarkListBySummary(performanceAccountId);

		// Delete each run's performance detail
		Set<PerformanceInvestmentAccountProcessor<T>> processorSet = getPerformanceInvestmentAccountProcessorLinkedSet(performanceAccountId);
		if (CollectionUtils.isEmpty(processorSet)) {
			// PerformancePortfolioRun may exist with no linked PortfoliRun; ensure they are deleted
			getPerformancePortfolioRunService().deletePerformancePortfolioRunListBySummary(performanceAccountId);
		}
		else {
			for (PerformanceInvestmentAccountProcessor<T> processor : processorSet) {
				processor.deletePerformanceInvestmentAccountProcessing(performanceAccountId);
			}
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private LinkedHashSet<PerformanceInvestmentAccountProcessor<T>> getPerformanceInvestmentAccountProcessorLinkedSet(int performanceAccountId) {
		PerformanceInvestmentAccount performanceInvestmentAccount = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(performanceAccountId);
		List<PerformancePortfolioRun> performancePortfolioRunList = getPerformancePortfolioRunService().getPerformancePortfolioRunListByPerformanceSummary(performanceAccountId);
		return getPerformanceInvestmentAccountProcessorLinkedSet(performanceInvestmentAccount, performancePortfolioRunList);
	}


	private Set<ServiceProcessingTypes> getServiceProcessingTypeSetForPerformancePortfolioRunList(List<? extends PerformancePortfolioRun> performancePortfolioRunList) {
		return CollectionUtils.getStream(performancePortfolioRunList)
				.filter(performancePortfolioRun -> performancePortfolioRun.getPortfolioRun() != null)
				.map(performancePortfolioRun -> performancePortfolioRun.getPortfolioRun().getServiceProcessingType())
				.collect(Collectors.toSet());
	}


	private LinkedHashSet<PerformanceInvestmentAccountProcessor<T>> getPerformanceInvestmentAccountProcessorLinkedSet(PerformanceInvestmentAccount performanceInvestmentAccount, List<? extends PerformancePortfolioRun> performancePortfolioRunList) {
		Set<ServiceProcessingTypes> runProcessingTypes = getServiceProcessingTypeSetForPerformancePortfolioRunList(performancePortfolioRunList);
		return CollectionUtils.getStream(runProcessingTypes)
				.map(serviceProcessingType -> getPortfolioAccountDataRetriever().getClientAccountObjectFromContextMap(performanceInvestmentAccount.getClientAccount(), serviceProcessingType, getPerformanceServiceProcessingTypesProcessorMap(), "Performance Dynamic Investment Account Run Processor"))
				.sorted(Comparator.comparingInt(PerformanceInvestmentAccountProcessor::getOrder))
				.collect(Collectors.toCollection(LinkedHashSet::new));
	}


	private void processPerformancePortfolioRunCalculator(PerformancePortfolioRunProcessConfig config, Date date) {
		PerformancePortfolioRunCalculator<PerformancePortfolioRunProcessConfig> runCalculator = getPerformancePortfolioRunCalculator();
		processPerformanceInvestmentAccountCalculator(config, runCalculator, date);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioRunCalculator<PerformancePortfolioRunProcessConfig> getPerformancePortfolioRunCalculator() {
		return this.performancePortfolioRunCalculator;
	}


	public void setPerformancePortfolioRunCalculator(PerformancePortfolioRunCalculator<PerformancePortfolioRunProcessConfig> performancePortfolioRunCalculator) {
		this.performancePortfolioRunCalculator = performancePortfolioRunCalculator;
	}


	public Map<String, PerformanceInvestmentAccountProcessor<T>> getPerformanceServiceProcessingTypesProcessorMap() {
		return this.performanceServiceProcessingTypesProcessorMap;
	}


	public void setPerformanceServiceProcessingTypesProcessorMap(Map<String, PerformanceInvestmentAccountProcessor<T>> performanceServiceProcessingTypesProcessorMap) {
		this.performanceServiceProcessingTypesProcessorMap = performanceServiceProcessingTypesProcessorMap;
	}
}
