package com.clifton.performance.account.process.portfolio.run;

import com.clifton.core.util.date.DateUtils;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.benchmark.process.PerformanceInvestmentAccountBenchmarkConfig;
import com.clifton.performance.account.process.type.PerformanceInvestmentAccountProcessingTypes;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * <code>PerformancePortfolioRunProcessConfig</code> is a {@link com.clifton.performance.account.process.config.PerformanceInvestmentAccountConfig}
 * that holds details for benchmarks and portfolio runs for applicable days.
 *
 * @author nickk
 */
public class PerformancePortfolioRunProcessConfig extends PerformanceInvestmentAccountBenchmarkConfig {

	private List<? extends PerformancePortfolioRun> performanceRunList;

	// Contains Daily % Change based on the rate for the first measure date in the month
	private Map<Integer, BigDecimal> interestRateDailyChangeIndexMap = null;
	private Map<String, BigDecimal> cashValueDailyMap = null;
	private Map<String, BigDecimal> repoAccruedInterestMap = null;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioRunProcessConfig(PerformanceInvestmentAccount performanceAccount, PerformanceInvestmentAccountProcessingTypes processingType) {
		super(performanceAccount, processingType);
	}


	public BigDecimal getCashValueForDate(Date date) {
		return this.cashValueDailyMap != null ? this.cashValueDailyMap.get(DateUtils.fromDateShort(date)) : null;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<? extends PerformancePortfolioRun> getPerformanceRunList() {
		return this.performanceRunList;
	}


	public void setPerformanceRunList(List<? extends PerformancePortfolioRun> performanceRunList) {
		this.performanceRunList = performanceRunList;
	}


	public Map<Integer, BigDecimal> getInterestRateDailyChangeIndexMap() {
		return this.interestRateDailyChangeIndexMap;
	}


	public void setInterestRateDailyChangeIndexMap(Map<Integer, BigDecimal> interestRateDailyChangeIndexMap) {
		this.interestRateDailyChangeIndexMap = interestRateDailyChangeIndexMap;
	}


	public Map<String, BigDecimal> getCashValueDailyMap() {
		return this.cashValueDailyMap;
	}


	public void setCashValueDailyMap(Map<String, BigDecimal> cashValueDailyMap) {
		this.cashValueDailyMap = cashValueDailyMap;
	}


	public Map<String, BigDecimal> getRepoAccruedInterestMap() {
		return this.repoAccruedInterestMap;
	}


	public void setRepoAccruedInterestMap(Map<String, BigDecimal> repoAccruedInterestMap) {
		this.repoAccruedInterestMap = repoAccruedInterestMap;
	}
}
