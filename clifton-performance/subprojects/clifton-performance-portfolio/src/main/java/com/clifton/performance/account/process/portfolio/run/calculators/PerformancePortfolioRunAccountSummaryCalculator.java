package com.clifton.performance.account.process.portfolio.run.calculators;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountDataRetrieverImpl;
import com.clifton.performance.account.process.calculators.BasePerformanceInvestmentAccountCalculator;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRun;
import com.clifton.performance.account.process.portfolio.run.PerformancePortfolioRunProcessConfig;
import com.clifton.portfolio.run.PortfolioRun;
import com.clifton.portfolio.run.PortfolioRunService;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;


/**
 * <code>PerformancePortfolioRunAccountSummaryCalculator</code> takes the results from the processing and applies the MTD values to the {@link PerformanceInvestmentAccount}
 *
 * @author nickk
 */
@Component
public class PerformancePortfolioRunAccountSummaryCalculator<T extends PerformancePortfolioRunProcessConfig> extends BasePerformanceInvestmentAccountCalculator<T> {

	private CalendarBusinessDayService calendarBusinessDayService;
	private InvestmentAccountService investmentAccountService;
	private PortfolioRunService portfolioRunService;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	protected void recalculateAccountReturns(T config) {
		PerformanceInvestmentAccount performanceInvestmentAccount = config.getPerformanceAccount();

		performanceInvestmentAccount.setMonthToDateReturn(CoreMathUtils.getTotalPercentChange(config.getPerformanceRunList(), PerformancePortfolioRun::getAccountReturn, true));
		performanceInvestmentAccount.setMonthToDateBenchmarkReturn(CoreMathUtils.getTotalPercentChange(config.getPerformanceRunList(), PerformancePortfolioRun::getBenchmarkReturn, true));

		Boolean includeGLTotal = (Boolean) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(performanceInvestmentAccount.getClientAccount(),
				PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_INCLUDE_GL_OTHER_IN_PORTFOLIO_RETURN_CALCULATIONS);

		BigDecimal totalPortfolioValue = getTotalPortfolioValueForPortfolioReturnCalculation(config);
		if (!MathUtils.isNullOrZero(totalPortfolioValue)) {
			performanceInvestmentAccount.setMonthToDatePortfolioReturn(CoreMathUtils.getPercentValue(CoreMathUtils.sumProperty(config.getPerformanceRunList(), BooleanUtils.isTrue(includeGLTotal) ? PerformancePortfolioRun::getCoalesceGainLossTotalOverride : PerformancePortfolioRun::getCoalesceGainLossOverride), totalPortfolioValue, true));
		}
		else {
			performanceInvestmentAccount.setMonthToDatePortfolioReturn(BigDecimal.ZERO);
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private BigDecimal getTotalPortfolioValueForPortfolioReturnCalculation(T config) {
		// First Check if a Static Value should be used
		BigDecimal value = (BigDecimal) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(config.getPerformanceAccount().getClientAccount(),
				PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_USE_VALUE_FOR_PORTFOLIO_RETURN);
		if (!MathUtils.isNullOrZero(value)) {
			return value;
		}

		// Otherwise need to check account specific PIOS Runs
		// Check if should use Specific Account's Run
		Integer specificAccountId = (Integer) getPerformanceInvestmentAccountDataRetriever().getPerformanceInvestmentAccountCustomFieldValue(config.getPerformanceAccount().getClientAccount(),
				PerformanceInvestmentAccountDataRetrieverImpl.FIELD_PERFORMANCE_USE_ACCOUNT_FOR_PORTFOLIO_RETURN);
		if (specificAccountId != null) {
			InvestmentAccount specificAccount = getInvestmentAccountService().getInvestmentAccount(specificAccountId);
			if (specificAccount != null) {
				// Use last weekday of previous month as the date for the TPV
				Date runDate = DateUtils.getLastWeekdayOfPreviousMonth(config.getEndDate());
				// Unless the account started mid-month, then use the previous weekday from the positions on date
				if (DateUtils.isDateBetween(config.getPerformanceInceptionDate(), config.getStartDate(), config.getEndDate(), false)) {
					runDate = DateUtils.getPreviousWeekday(config.getPerformanceInceptionDate());
				}
				PortfolioRun run = getPortfolioRunService().getPortfolioRunByMainRun(specificAccountId, runDate);
				while (run == null && !getCalendarBusinessDayService().isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(runDate))) {
					runDate = DateUtils.getPreviousWeekday(runDate);
					run = getPortfolioRunService().getPortfolioRunByMainRun(specificAccountId, runDate);
				}
				if (run == null) {
					throw new ValidationException("Unable to use specified account for portfolio return calculations, because cannot find a main run for account [" + specificAccount.getNumber()
							+ "] on [" + DateUtils.fromDateShort(runDate));
				}
				return run.getPortfolioTotalValue();
			}
		}
		else {
			PerformancePortfolioRun firstRun = null;
			if (DateUtils.isDateBetween(config.getPerformanceInceptionDate(), config.getStartDate(), config.getEndDate(), false)) {
				firstRun = CollectionUtils.getFirstElement(BeanUtils.filter(config.getPerformanceRunList(), PerformancePortfolioRun::getMeasureDate, config.getPerformanceInceptionDate()));
			}
			if (firstRun == null) {
				firstRun = CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(config.getPerformanceRunList(), PerformancePortfolioRun::getMeasureDate, true));
			}
			if (firstRun != null && firstRun.getPortfolioRun() != null) {
				return firstRun.getPortfolioRun().getPortfolioTotalValue();
			}
		}
		return BigDecimal.ZERO;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public PortfolioRunService getPortfolioRunService() {
		return this.portfolioRunService;
	}


	public void setPortfolioRunService(PortfolioRunService portfolioRunService) {
		this.portfolioRunService = portfolioRunService;
	}
}
