package com.clifton.performance.account.process.portfolio.run;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.performance.account.process.portfolio.run.search.PerformancePortfolioRunSearchForm;

import java.util.Date;
import java.util.List;


public interface PerformancePortfolioRunService {

	////////////////////////////////////////////////////////////////////////////////
	///////            Performance Portfolio Run Business Methods            ///////
	////////////////////////////////////////////////////////////////////////////////


	public PerformancePortfolioRun getPerformancePortfolioRun(int id);


	public PerformancePortfolioRun getPerformancePortfolioRunBySummaryAndDate(int summaryId, Date measureDate);


	/**
	 * Returns the list of {@link PerformancePortfolioRun} (likely only one) records associated with a specific Portfolio Run
	 * Used to determine if the run is used or not so it can be re-processed
	 */
	@DoNotAddRequestMapping
	public List<PerformancePortfolioRun> getPerformancePortfolioRunListByPortfolioRun(int runId);


	public List<PerformancePortfolioRun> getPerformancePortfolioRunListByPerformanceSummary(int summaryId);


	public List<PerformancePortfolioRun> getPerformancePortfolioRunList(PerformancePortfolioRunSearchForm searchForm);


	/**
	 * This method will return back all product performance runs for the provided performance summary.  If portfolio wide is specified it will take the
	 * account from the provided performance summary and find it's related main account. A list of all sub-accounts will be created from
	 * this main account.  This whole list is considered the portfolio wide list.  We then iterate over it building a complete list of all
	 * product performance runs for each account on the given measure date.
	 */
	public List<PerformancePortfolioRun> getPerformancePortfolioRunListByPerformanceSummaryPortfolioWide(int summaryId, boolean portfolioWide, String startMeasureDate, String endMeasureDate);


	@DoNotAddRequestMapping
	public PerformancePortfolioRun savePerformancePortfolioRun(PerformancePortfolioRun bean);


	/**
	 * Save given list as the {@link PerformancePortfolioRun} for the given summary.
	 * Will get the original list to properly perform inserts/updates/deletes
	 */
	@DoNotAddRequestMapping
	public void savePerformancePortfolioRunListForSummary(int summaryId, List<PerformancePortfolioRun> beanList);


	public void deletePerformancePortfolioRun(int id);


	public void deletePerformancePortfolioRunListBySummary(int summaryId);
}
