package com.clifton.performance.account.process.portfolio.run;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccount;
import com.clifton.performance.account.PerformanceInvestmentAccountService;
import com.clifton.performance.account.process.portfolio.run.search.PerformancePortfolioRunSearchForm;
import com.clifton.performance.account.search.PerformanceInvestmentAccountSearchForm;
import com.clifton.portfolio.account.PortfolioAccountDataRetriever;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


/**
 * The <code>PerformancePortfolioRunServiceImpl</code> ...
 *
 * @author Mary Anderson
 */
@Service
public class PerformancePortfolioRunServiceImpl implements PerformancePortfolioRunService {

	private AdvancedUpdatableDAO<PerformancePortfolioRun, Criteria> performancePortfolioRunDAO;

	private PerformanceInvestmentAccountService performanceInvestmentAccountService;

	private PortfolioAccountDataRetriever portfolioAccountDataRetriever;

	////////////////////////////////////////////////////////////////////////////
	///////          Performance Portfolio Run Business Methods          ///////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public PerformancePortfolioRun getPerformancePortfolioRun(int id) {
		return getPerformancePortfolioRunDAO().findByPrimaryKey(id);
	}


	@Override
	public PerformancePortfolioRun getPerformancePortfolioRunBySummaryAndDate(int summaryId, Date measureDate) {
		return getPerformancePortfolioRunDAO().findOneByFields(new String[]{"performanceSummary.id", "measureDate",}, new Object[]{summaryId, measureDate});
	}


	@Override
	public List<PerformancePortfolioRun> getPerformancePortfolioRunListByPortfolioRun(int runId) {
		return getPerformancePortfolioRunDAO().findByField("portfolioRun.id", runId);
	}


	@Override
	public List<PerformancePortfolioRun> getPerformancePortfolioRunListByPerformanceSummary(int summaryId) {
		return getPerformancePortfolioRunDAO().findByField("performanceSummary.id", summaryId);
	}


	@Override
	public List<PerformancePortfolioRun> getPerformancePortfolioRunList(PerformancePortfolioRunSearchForm searchForm) {
		return getPerformancePortfolioRunDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	private List<PerformancePortfolioRun> getPerformancePortfolioRunListByPerformanceSummaryList(List<PerformanceInvestmentAccount> summaryList) {
		PerformancePortfolioRunSearchForm searchForm = new PerformancePortfolioRunSearchForm();
		searchForm.setPerformanceSummaryIds(BeanUtils.getPropertyValues(summaryList, PerformanceInvestmentAccount::getId, Integer.class));
		return getPerformancePortfolioRunList(searchForm);
	}


	@Override
	public List<PerformancePortfolioRun> getPerformancePortfolioRunListByPerformanceSummaryPortfolioWide(int summaryId, boolean portfolioWide, String startMeasureDate, String endMeasureDate) {

		PerformanceInvestmentAccount summary = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccount(summaryId);
		Integer mainAccountId = summary.getClientAccount().getId();

		//Get summary list for all accounts
		PerformanceInvestmentAccountSearchForm searchForm = new PerformanceInvestmentAccountSearchForm();

		if (portfolioWide) {
			InvestmentAccount mainAccount = getPortfolioAccountDataRetriever().getMainAccountForCliftonSubAccount(summary.getClientAccount(), false, summary.getMeasureDate());

			if (mainAccount != null) {
				mainAccountId = mainAccount.getId();
			}

			searchForm.setClientInvestmentAccountIdOrSubAccount(mainAccountId);
		}
		else {
			searchForm.setClientAccountId(mainAccountId);
		}

		searchForm.setStartMeasureDate(DateUtils.toDate(startMeasureDate));
		searchForm.setEndMeasureDate(DateUtils.toDate(endMeasureDate));
		List<PerformanceInvestmentAccount> summaryList = getPerformanceInvestmentAccountService().getPerformanceInvestmentAccountList(searchForm);

		//From the list of summaries return the list of all performance runs

		//Combine the tracking error results by day
		return getPerformancePortfolioRunListByPerformanceSummaryList(summaryList);
	}


	@Override
	public PerformancePortfolioRun savePerformancePortfolioRun(PerformancePortfolioRun bean) {
		return getPerformancePortfolioRunDAO().save(bean);
	}


	@Override
	@Transactional
	public void savePerformancePortfolioRunListForSummary(int summaryId, List<PerformancePortfolioRun> beanList) {
		List<PerformancePortfolioRun> originalList = getPerformancePortfolioRunListByPerformanceSummary(summaryId);
		getPerformancePortfolioRunDAO().saveList(beanList);
		for (PerformancePortfolioRun old : CollectionUtils.getIterable(originalList)) {
			if (beanList == null || !beanList.contains(old)) {
				deletePerformancePortfolioRun(old.getId());
			}
		}
	}


	@Override
	@Transactional
	public void deletePerformancePortfolioRun(int id) {
		getPerformancePortfolioRunDAO().delete(id);
	}


	@Override
	@Transactional
	public void deletePerformancePortfolioRunListBySummary(int summaryId) {
		for (PerformancePortfolioRun r : CollectionUtils.getIterable(getPerformancePortfolioRunListByPerformanceSummary(summaryId))) {
			deletePerformancePortfolioRun(r.getId());
		}
	}

	////////////////////////////////////////////////////////////////////
	/////////           Getter and Setter Methods             //////////
	////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<PerformancePortfolioRun, Criteria> getPerformancePortfolioRunDAO() {
		return this.performancePortfolioRunDAO;
	}


	public void setPerformancePortfolioRunDAO(AdvancedUpdatableDAO<PerformancePortfolioRun, Criteria> performancePortfolioRunDAO) {
		this.performancePortfolioRunDAO = performancePortfolioRunDAO;
	}


	public PortfolioAccountDataRetriever getPortfolioAccountDataRetriever() {
		return this.portfolioAccountDataRetriever;
	}


	public void setPortfolioAccountDataRetriever(PortfolioAccountDataRetriever portfolioAccountDataRetriever) {
		this.portfolioAccountDataRetriever = portfolioAccountDataRetriever;
	}


	public PerformanceInvestmentAccountService getPerformanceInvestmentAccountService() {
		return this.performanceInvestmentAccountService;
	}


	public void setPerformanceInvestmentAccountService(PerformanceInvestmentAccountService performanceInvestmentAccountService) {
		this.performanceInvestmentAccountService = performanceInvestmentAccountService;
	}
}
