Clifton.performance.account.portfolio.RunGainLossDetailsWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Performance Portfolio Run Gain/Loss Details',
	iconCls: 'chart-pie',
	width: 1100,
	height: 600,
	layout: 'border',

	setTitle: function(title, iconCls) {
		const newTitle = this.titlePrefix + ' - ' + this.params.measureDate;
		TCG.Window.superclass.setTitle.call(this, newTitle, iconCls);
	},

	items: [{
		title: 'Client Positions',
		xtype: 'accounting-positionAnalysisGrid',
		region: 'north',
		height: 280,
		pageSize: 1000,
		reloadOnRender: true,
		viewNames: undefined,
		columnOverrides: [
			{dataIndex: 'positionDate', hidden: false}, // For measure dates that include previous day holidays
			{dataIndex: 'accountingTransaction.clientInvestmentAccount.number', hidden: true},
			{dataIndex: 'accountingTransaction.clientInvestmentAccount.name', hidden: true},
			{dataIndex: 'accountingTransaction.holdingInvestmentAccount.issuingCompany.name', hidden: true},
			{dataIndex: 'dailyGainLossBase', hidden: false, width: 140}
		],

		getLoadParams: function() {
			const params = this.getWindow().params;
			return {
				clientInvestmentAccountId: params.clientInvestmentAccountId,
				startPositionDate: new Date(params.portfolioRunDate).add(Date.DAY, 1).format('m/d/Y'), // Start the Day after Overlay Run Date and Run until Measure Date
				endPositionDate: params.measureDate
			};
		},
		getTopToolbarFilters: function(toolbar) {
			return [];
		}
	}, {
		title: 'REPO Accrued Interest',
		region: 'center',
		height: 250,
		xtype: 'lending-repoAccruedInterestGrid',
		instructions: 'Lists REPO details for all REPOs that apply to the specified date.  Note: When applied as gain loss to performance summaries, Repo Accrued interest is negated for Performance Summary Gain Loss as it is accruing liability.',
		columnOverrides: [
			{dataIndex: 'repo.maturityDate', hidden: true},
			{dataIndex: 'repo.repoDefinition.clientInvestmentAccount.label', hidden: true},
			{dataIndex: 'repo.workflowStatus.name', hidden: true},
			{dataIndex: 'repo.workflowState.name', hidden: true},
			{dataIndex: 'repo.traderUser.label', hidden: true},
			{dataIndex: 'repo.interestAmount', hidden: true},
			{dataIndex: 'repo.netCash', hidden: true}
		],
		getTopToolbarFilters: function(toolbar) {
			return [];
		},
		getLoadParams: function() {
			const params = this.getWindow().params;
			return {
				clientInvestmentAccountId: params.clientInvestmentAccountId,
				measureDate: params.measureDate,
				excludeWorkflowStateName: 'Cancelled'
			};
		}
	}]
});
