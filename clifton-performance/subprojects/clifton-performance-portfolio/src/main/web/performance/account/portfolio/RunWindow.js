// not the actual window but a window selector based on processing type of the run or client account
Clifton.performance.account.portfolio.RunWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'performancePortfolioRun.json',

	getClassName: function(config, entity) {
		let processingType = entity ? TCG.getValue('portfolioRun.serviceProcessingType', entity)
			: TCG.getValue('portfolioRun.serviceProcessingType', config.defaultData);

		// If not on the run, look on the client account
		if (!processingType) {
			processingType = entity ? TCG.getValue('performanceSummary.clientAccount.serviceProcessingType.processingType', entity)
				: TCG.getValue('performanceSummary.clientAccount.serviceProcessingType.processingType', config.defaultData);
		}

		// Still not found use DEFAULT;
		if (TCG.isBlank(processingType)) {
			processingType = 'DEFAULT';
		}

		return Clifton.performance.account.portfolio.PerformanceRunWindowOverrides[processingType];
	},

	useEntityAsDefaultData: function(className, config, entity) {
		// force reload with the correct window
		return false;
	}
});
