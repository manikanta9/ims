package com.clifton.replication;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ReplicationProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "replication";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.reflections.Reflections");
		imports.add("org.reflections.scanners.SubTypesScanner");
		imports.add("org.apache.commons.collections4.BidiMap");
		imports.add("org.apache.commons.collections4.bidimap.DualHashBidiMap");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		if ("saveReplicationTask".equals(method.getName())) {
			return true;
		}
		else if ("saveReplicationKeyMapping".equals(method.getName())) {
			return true;
		}
		return false;
	}
}
