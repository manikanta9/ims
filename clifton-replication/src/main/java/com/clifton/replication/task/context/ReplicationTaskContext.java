package com.clifton.replication.task.context;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.status.StatusLogger;
import com.clifton.replication.BaseReplicationObjectWrapper;
import com.clifton.replication.key.ReplicationKeyMapping;
import com.clifton.replication.task.ReplicationTaskColumn;
import com.clifton.replication.task.ReplicationTaskTable;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemTable;
import org.apache.commons.collections4.BidiMap;
import org.apache.commons.collections4.bidimap.DualHashBidiMap;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReplicationTaskContext<O extends IdentityObject, S> {

	private Date replicationDate;
	private ReplicationTaskTable taskTable;
	private List<ReplicationTaskColumn> taskColumnList;
	private List<ReplicationKeyMapping> replicationKeyMappings;

	private S sourceService;
	private S targetService;
	private SystemDataSource sourceDataSource;
	private SystemDataSource targetDataSource;
	private SystemTable firstDataSourceSystemTable;

	private Map<String, Long> sourceNaturalKeyMap = new HashMap<>();
	private Map<String, Long> targetNaturalKeyMap = new HashMap<>();
	private Map<Long, BaseReplicationObjectWrapper<O>> sourceObjectMap = new HashMap<>();
	private Map<Long, BaseReplicationObjectWrapper<O>> targetObjectMap = new HashMap<>();
	private BidiMap<Long, Long> biDirectionalIdMap = new DualHashBidiMap<>();
	private StatusLogger status;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public boolean isPreviewOnly() {
		return getTaskTable().getTask().getCommand().isPreviewOnly();
	}


	public SystemDataSource getFirstDataSource() {
		return this.sourceDataSource.getId() < this.targetDataSource.getId() ? this.sourceDataSource : this.targetDataSource;
	}


	public SystemDataSource getSecondDataSource() {
		return this.sourceDataSource.getId() > this.targetDataSource.getId() ? this.sourceDataSource : this.targetDataSource;
	}


	/**
	 * @param keyMappings
	 * @<code>populateBidirectionalIdMap</code> is populated placing the source object id as key and target id as value regardless of weather the id value
	 * corresponds to the id returned by {@link ReplicationKeyMapping#getFirstDataSourceFkFieldId()} or {@link ReplicationKeyMapping#getSecondDataSourceFkFieldId()}
	 */
	public void populateBidirectionalIdMap(List<ReplicationKeyMapping> keyMappings) {
		setReplicationKeyMappings(keyMappings);
		for (ReplicationKeyMapping keyMapping : CollectionUtils.getIterable(keyMappings)) {
			if (isReplicateFirstToSecond()) {
				this.biDirectionalIdMap.put(keyMapping.getFirstDataSourceFkFieldId(), keyMapping.getSecondDataSourceFkFieldId());
			}
			else {
				this.biDirectionalIdMap.put(keyMapping.getSecondDataSourceFkFieldId(), keyMapping.getFirstDataSourceFkFieldId());
			}
		}
	}


	public void addNewKeyMappingToContext(ReplicationKeyMapping keyMapping) {
		getReplicationKeyMappings().add(keyMapping);
		if (isReplicateFirstToSecond()) {
			this.biDirectionalIdMap.put(keyMapping.getFirstDataSourceFkFieldId(), keyMapping.getSecondDataSourceFkFieldId());
		}
		else {
			this.biDirectionalIdMap.put(keyMapping.getSecondDataSourceFkFieldId(), keyMapping.getFirstDataSourceFkFieldId());
		}
	}


	public ReplicationKeyMapping removeKeyMappingFromContext(ReplicationKeyMapping keyMapping) {
		if (isReplicateFirstToSecond()) {
			this.biDirectionalIdMap.remove(keyMapping.getFirstDataSourceFkFieldId(), keyMapping.getSecondDataSourceFkFieldId());
		}
		else {
			this.biDirectionalIdMap.remove(keyMapping.getSecondDataSourceFkFieldId(), keyMapping.getFirstDataSourceFkFieldId());
		}
		return getReplicationKeyMappings().remove(getReplicationKeyMappings().indexOf(keyMapping));
	}


	public ReplicationKeyMapping generateNewKeyMappingFor(BaseReplicationObjectWrapper<O> sourceObject, BaseReplicationObjectWrapper<O> targetObject) {
		ReplicationKeyMapping mapping = new ReplicationKeyMapping();
		mapping.setFirstDataSourceSystemTable(getFirstDataSourceSystemTable());
		mapping.setSecondDataSource(getSecondDataSource());
		mapping.setFirstDataSourceFkFieldId(isReplicateFirstToSecond() ? sourceObject.getId() : targetObject.getId());
		mapping.setSecondDataSourceFkFieldId(isReplicateFirstToSecond() ? targetObject.getId() : sourceObject.getId());
		mapping.setReplicationDate(new Date());
		return mapping;
	}


	public ReplicationKeyMapping getReplicationKeyMappingBySourceAndTargetID(Long sourceID, Long targetID) {
		//If isfirstToSecond then source is first and target is second;
		Long firstFkID = isReplicateFirstToSecond() ? sourceID : targetID;
		Long secondFkID = isReplicateFirstToSecond() ? targetID : sourceID;
		for (ReplicationKeyMapping keyMapping : CollectionUtils.getIterable(getReplicationKeyMappings())) {
			if (keyMapping.getFirstDataSourceFkFieldId() == firstFkID && keyMapping.getSecondDataSourceFkFieldId() == secondFkID) {
				return keyMapping;
			}
		}
		return null;
	}


	public boolean isReplicateFirstToSecond() {
		return getFirstDataSource() == this.sourceDataSource;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              Delegate Methods                          //////////
	////////////////////////////////////////////////////////////////////////////
	public void put(String key, String value) {
		getStatus().getStatusMap().put(key, value);
	}


	public String get(String key) {
		return (String) getStatus().getStatusMap().get(key);
	}


	public boolean containsKey(String key) {
		return getStatus().getStatusMap().containsKey(key);
	}


	public void incrementStatusCount(String statusCountType) {
		getStatus().incrementStatusCount(statusCountType);
	}

/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Date getReplicationDate() {
		return this.replicationDate;
	}


	public void setReplicationDate(Date replicationDate) {
		this.replicationDate = replicationDate;
	}


	public ReplicationTaskTable getTaskTable() {
		return this.taskTable;
	}


	public void setTaskTable(ReplicationTaskTable taskTable) {
		this.taskTable = taskTable;
	}


	public S getSourceService() {
		return this.sourceService;
	}


	public void setSourceService(S sourceService) {
		this.sourceService = sourceService;
	}


	public S getTargetService() {
		return this.targetService;
	}


	public void setTargetService(S targetService) {
		this.targetService = targetService;
	}


	public SystemDataSource getSourceDataSource() {
		return this.sourceDataSource;
	}


	public void setSourceDataSource(SystemDataSource sourceDataSource) {
		this.sourceDataSource = sourceDataSource;
	}


	public SystemDataSource getTargetDataSource() {
		return this.targetDataSource;
	}


	public void setTargetDataSource(SystemDataSource targetDataSource) {
		this.targetDataSource = targetDataSource;
	}


	public List<ReplicationTaskColumn> getTaskColumnList() {
		return this.taskColumnList;
	}


	public void setTaskColumnList(List<ReplicationTaskColumn> taskColumnList) {
		this.taskColumnList = taskColumnList;
	}


	public StatusLogger getStatus() {
		return this.status;
	}


	public void setStatus(StatusLogger status) {
		this.status = status;
	}


	public Map<String, Long> getSourceNaturalKeyMap() {
		return this.sourceNaturalKeyMap;
	}


	public void setSourceNaturalKeyMap(Map<String, Long> sourceNaturalKeyMap) {
		this.sourceNaturalKeyMap = sourceNaturalKeyMap;
	}


	public Map<String, Long> getTargetNaturalKeyMap() {
		return this.targetNaturalKeyMap;
	}


	public void setTargetNaturalKeyMap(Map<String, Long> targetNaturalKeyMap) {
		this.targetNaturalKeyMap = targetNaturalKeyMap;
	}


	public Map<Long, BaseReplicationObjectWrapper<O>> getSourceObjectMap() {
		return this.sourceObjectMap;
	}


	public void setSourceObjectMap(Map<Long, BaseReplicationObjectWrapper<O>> sourceObjectMap) {
		this.sourceObjectMap = sourceObjectMap;
	}


	public Map<Long, BaseReplicationObjectWrapper<O>> getTargetObjectMap() {
		return this.targetObjectMap;
	}


	public void setTargetObjectMap(Map<Long, BaseReplicationObjectWrapper<O>> targetObjectMap) {
		this.targetObjectMap = targetObjectMap;
	}


	public BidiMap<Long, Long> getBiDirectionalIdMap() {
		return this.biDirectionalIdMap;
	}


	public void setBiDirectionalIdMap(BidiMap<Long, Long> biDirectionalIdMap) {
		this.biDirectionalIdMap = biDirectionalIdMap;
	}


	public SystemTable getFirstDataSourceSystemTable() {
		return this.firstDataSourceSystemTable;
	}


	public void setFirstDataSourceSystemTable(SystemTable firstDataSourceSystemTable) {
		this.firstDataSourceSystemTable = firstDataSourceSystemTable;
	}


	public List<ReplicationKeyMapping> getReplicationKeyMappings() {
		return this.replicationKeyMappings;
	}


	public void setReplicationKeyMappings(List<ReplicationKeyMapping> replicationKeyMappings) {
		this.replicationKeyMappings = replicationKeyMappings;
	}
}
