package com.clifton.replication.task.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseUpdatableEntitySearchForm;
import com.clifton.replication.task.ReplicationTaskTableFilter;


/**
 * <code>ReplicationTaskTableFilterSearchForm</code> defines search configuration for {@link ReplicationTaskTableFilter} objects.
 *
 * @author StevenF
 */
public class ReplicationTaskTableFilterSearchForm extends BaseUpdatableEntitySearchForm {

	@SearchField(searchField = "taskTable.id")
	private Integer taskTableId;

	@SearchField
	private String beanFieldName;

	@SearchField
	private String beanFieldValue;

	@SearchField
	private String beanFieldValueQuery;

	@SearchField
	private Boolean preFilter;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getTaskTableId() {
		return this.taskTableId;
	}


	public void setTaskTableId(Integer taskTableId) {
		this.taskTableId = taskTableId;
	}


	public String getBeanFieldName() {
		return this.beanFieldName;
	}


	public void setBeanFieldName(String beanFieldName) {
		this.beanFieldName = beanFieldName;
	}


	public String getBeanFieldValue() {
		return this.beanFieldValue;
	}


	public void setBeanFieldValue(String beanFieldValue) {
		this.beanFieldValue = beanFieldValue;
	}


	public String getBeanFieldValueQuery() {
		return this.beanFieldValueQuery;
	}


	public void setBeanFieldValueQuery(String beanFieldValueQuery) {
		this.beanFieldValueQuery = beanFieldValueQuery;
	}


	public Boolean getPreFilter() {
		return this.preFilter;
	}


	public void setPreFilter(Boolean preFilter) {
		this.preFilter = preFilter;
	}
}
