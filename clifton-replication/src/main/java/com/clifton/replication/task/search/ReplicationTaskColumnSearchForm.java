package com.clifton.replication.task.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseUpdatableEntitySearchForm;
import com.clifton.replication.task.ReplicationTaskColumn;


/**
 * <code>ReplicationTaskColumnSearchForm</code> defines search configuration for {@link ReplicationTaskColumn} objects.
 *
 * @author StevenF
 */
public class ReplicationTaskColumnSearchForm extends BaseUpdatableEntitySearchForm {

	@SearchField(searchField = "taskTable.id")
	private Integer taskTableId;

	@SearchField(searchFieldPath = "taskTable", searchField = "sourceSystemTable.id")
	private Short sourceSystemTableId;

	@SearchField(searchFieldPath = "taskTable.sourceSystemTable", searchField = "name")
	private String sourceSystemTableName;

	@SearchField(searchField = "sourceSystemColumn.id")
	private Integer sourceSystemColumnId;

	@SearchField(searchFieldPath = "sourceSystemColumn", searchField = "name")
	private String sourceSystemColumnName;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getTaskTableId() {
		return this.taskTableId;
	}


	public void setTaskTableId(Integer taskTableId) {
		this.taskTableId = taskTableId;
	}


	public Short getSourceSystemTableId() {
		return this.sourceSystemTableId;
	}


	public void setSourceSystemTableId(Short sourceSystemTableId) {
		this.sourceSystemTableId = sourceSystemTableId;
	}


	public String getSourceSystemTableName() {
		return this.sourceSystemTableName;
	}


	public void setSourceSystemTableName(String sourceSystemTableName) {
		this.sourceSystemTableName = sourceSystemTableName;
	}


	public Integer getSourceSystemColumnId() {
		return this.sourceSystemColumnId;
	}


	public void setSourceSystemColumnId(Integer sourceSystemColumnId) {
		this.sourceSystemColumnId = sourceSystemColumnId;
	}


	public String getSourceSystemColumnName() {
		return this.sourceSystemColumnName;
	}


	public void setSourceSystemColumnName(String sourceSystemColumnName) {
		this.sourceSystemColumnName = sourceSystemColumnName;
	}
}
