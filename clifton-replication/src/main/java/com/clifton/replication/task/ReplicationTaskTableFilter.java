package com.clifton.replication.task;

import com.clifton.core.beans.BaseEntity;


/**
 * The <code>ReplicationTaskTableFilter</code> allows for table replication to specify any number
 * of search form field parameters and values that will be used when retrieving the initial object list to replicate.
 *
 * @author StevenF
 */
public class ReplicationTaskTableFilter extends BaseEntity<Integer> {

	/**
	 * Specifies the property to be set by either the 'beanFieldValue' or
	 * the result of the 'beanFieldValueQuery'
	 */
	private String beanFieldName;

	/**
	 * Mutually exclusive with 'beanFieldValueQuery'
	 * Specifies the value to be set for the given 'beanFieldName'
	 */
	private String beanFieldValue;

	/**
	 * Mutually exclusive with 'beanFieldValue'
	 * Stores a query to be executed in order to retrieve the value to be set
	 * for the given 'beanFieldName'; the query must return 'Value' as the column
	 * name for the value being retrieved.
	 */
	private String beanFieldValueQuery;

	private ReplicationTaskTable taskTable;

	private boolean targetDataSourceFilter;

	private boolean preFilter;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReplicationTaskTable getTaskTable() {
		return this.taskTable;
	}


	public void setTaskTable(ReplicationTaskTable taskTable) {
		this.taskTable = taskTable;
	}


	public String getBeanFieldName() {
		return this.beanFieldName;
	}


	public void setBeanFieldName(String beanFieldName) {
		this.beanFieldName = beanFieldName;
	}


	public String getBeanFieldValue() {
		return this.beanFieldValue;
	}


	public void setBeanFieldValue(String beanFieldValue) {
		this.beanFieldValue = beanFieldValue;
	}


	public String getBeanFieldValueQuery() {
		return this.beanFieldValueQuery;
	}


	public void setBeanFieldValueQuery(String beanFieldValueQuery) {
		this.beanFieldValueQuery = beanFieldValueQuery;
	}


	public boolean isTargetDataSourceFilter() {
		return this.targetDataSourceFilter;
	}


	public void setTargetDataSourceFilter(boolean targetDataSourceFilter) {
		this.targetDataSourceFilter = targetDataSourceFilter;
	}


	public boolean isPreFilter() {
		return this.preFilter;
	}


	public void setPreFilter(boolean preFilter) {
		this.preFilter = preFilter;
	}
}
