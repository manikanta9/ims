package com.clifton.replication.task.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseUpdatableEntitySearchForm;
import com.clifton.replication.task.ReplicationTaskTable;


/**
 * <code>ReplicationTaskTableSearchForm</code> defines search configuration for {@link ReplicationTaskTable} objects.
 *
 * @author StevenF
 */
public class ReplicationTaskTableSearchForm extends BaseUpdatableEntitySearchForm {

	@SearchField(searchField = "task.name,sourceSystemTable.name")
	private String searchPattern;

	@SearchField(searchField = "task.id")
	private Integer taskId;

	@SearchField(searchField = "sourceSystemTable.id")
	private Short sourceSystemTableId;

	@SearchField(searchFieldPath = "sourceSystemTable", searchField = "name")
	private String sourceSystemTableName;

	@SearchField
	private Boolean disabled;

	@SearchField
	private Boolean replicateInserts;

	@SearchField
	private Boolean replicateUpdates;

	@SearchField
	private Boolean replicateDeletes;

	@SearchField
	private Integer processingOrder;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getTaskId() {
		return this.taskId;
	}


	public void setTaskId(Integer taskId) {
		this.taskId = taskId;
	}


	public Short getSourceSystemTableId() {
		return this.sourceSystemTableId;
	}


	public void setSourceSystemTableId(Short sourceSystemTableId) {
		this.sourceSystemTableId = sourceSystemTableId;
	}


	public String getSourceSystemTableName() {
		return this.sourceSystemTableName;
	}


	public void setSourceSystemTableName(String sourceSystemTableName) {
		this.sourceSystemTableName = sourceSystemTableName;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Boolean getReplicateInserts() {
		return this.replicateInserts;
	}


	public void setReplicateInserts(Boolean replicateInserts) {
		this.replicateInserts = replicateInserts;
	}


	public Boolean getReplicateUpdates() {
		return this.replicateUpdates;
	}


	public void setReplicateUpdates(Boolean replicateUpdates) {
		this.replicateUpdates = replicateUpdates;
	}


	public Boolean getReplicateDeletes() {
		return this.replicateDeletes;
	}


	public void setReplicateDeletes(Boolean replicateDeletes) {
		this.replicateDeletes = replicateDeletes;
	}


	public Integer getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(Integer processingOrder) {
		this.processingOrder = processingOrder;
	}
}
