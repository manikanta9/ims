package com.clifton.replication.task;

import com.clifton.replication.task.search.ReplicationTaskColumnSearchForm;
import com.clifton.replication.task.search.ReplicationTaskSearchForm;
import com.clifton.replication.task.search.ReplicationTaskTableFilterSearchForm;
import com.clifton.replication.task.search.ReplicationTaskTableSearchForm;

import java.util.List;


/**
 * The <code>ReplicationKeyMappingService</code> defines all CRUD methods for replication configuration and processing objects:
 * <p>
 * -ReplicationTask (Top level contained for replication configuration information)
 * -ReplicationTaskTable (Table level configuration (inserts/updates/deletes and object filtering)
 * -ReplicationTaskColumn (Optional column level configuration for column inclusion)
 * -ReplicationKeyMapping (Used for tracking replication source/target ids (table, sourceFkFieldId, targetFkFieldId, date)
 * -ReplicationTaskTableFilter (Allows for applying search form filters to table replication - limits source data to replicate)
 *
 * @author StevenF
 */
public interface ReplicationTaskService {

	///////////////////////////////////////////////////////////
	////////    ReplicationTaskTable Business Methods     /////////
	///////////////////////////////////////////////////////////


	public ReplicationTask getReplicationTask(int id);


	public List<ReplicationTask> getReplicationTaskList(ReplicationTaskSearchForm searchForm);


	public ReplicationTask saveReplicationTask(ReplicationTask task);


	public void deleteReplicationTask(int id);

	///////////////////////////////////////////////////////////////
	////////    ReplicationTaskTable Business Methods     /////////
	///////////////////////////////////////////////////////////////


	public ReplicationTaskTable getReplicationTaskTable(int id);


	public List<ReplicationTaskTable> getReplicationTaskTableList(ReplicationTaskTableSearchForm searchForm);


	public ReplicationTaskTable saveReplicationTaskTable(ReplicationTaskTable table);


	public void deleteReplicationTaskTable(int id);

	////////////////////////////////////////////////////////////////
	////////    ReplicationTaskColumn Business Methods     /////////
	////////////////////////////////////////////////////////////////


	public ReplicationTaskColumn getReplicationTaskColumn(int id);


	public List<ReplicationTaskColumn> getReplicationTaskColumnList(ReplicationTaskColumnSearchForm searchForm);


	public ReplicationTaskColumn saveReplicationTaskColumn(ReplicationTaskColumn column);


	public void deleteReplicationTaskColumn(int id);

	///////////////////////////////////////////////////////////////////
	////// ReplicationTaskTableFilter Business Methods  /////
	///////////////////////////////////////////////////////////////////


	public ReplicationTaskTableFilter getReplicationTaskTableFilter(int id);


	public List<ReplicationTaskTableFilter> getReplicationTaskTableFilterList(ReplicationTaskTableFilterSearchForm searchForm);


	public ReplicationTaskTableFilter saveReplicationTaskTableFilter(ReplicationTaskTableFilter searchFormFilter);


	public void deleteReplicationTaskTableFilter(int id);
}
