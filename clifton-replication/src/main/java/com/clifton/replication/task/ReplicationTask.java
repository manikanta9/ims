package com.clifton.replication.task;

import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.replication.ReplicationCommand;
import com.clifton.system.schema.SystemDataSource;

import java.util.List;


/**
 * The <code>ReplicationTask</code> is the wrapper object for all configuration
 * necessary for object replication between systems of table and column data.
 *
 * @author StevenF
 */
public class ReplicationTask extends NamedEntityWithoutLabel<Integer> {

	private SystemDataSource sourceDataSource;
	private SystemDataSource targetDataSource;

	@NonPersistentField
	private ReplicationCommand command;

	private List<ReplicationTaskTable> taskTableList;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public SystemDataSource getSourceDataSource() {
		return this.sourceDataSource;
	}


	public void setSourceDataSource(SystemDataSource sourceDataSource) {
		this.sourceDataSource = sourceDataSource;
	}


	public SystemDataSource getTargetDataSource() {
		return this.targetDataSource;
	}


	public void setTargetDataSource(SystemDataSource targetDataSource) {
		this.targetDataSource = targetDataSource;
	}


	public List<ReplicationTaskTable> getTaskTableList() {
		return this.taskTableList;
	}


	public void setTaskTableList(List<ReplicationTaskTable> taskTableList) {
		this.taskTableList = taskTableList;
	}


	public ReplicationCommand getCommand() {
		return this.command;
	}


	public void setCommand(ReplicationCommand command) {
		this.command = command;
	}
}
