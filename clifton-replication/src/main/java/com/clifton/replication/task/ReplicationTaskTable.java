package com.clifton.replication.task;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.schema.SystemTable;

import java.util.List;


/**
 * The <code>ReplicationTaskTable</code> contains the configuration flags used when
 * executing a replication for the specified table and defines what actions should be
 * replicated (e.g. inserts/updates/deletes)
 * <p>
 * Optionally a columnList can be defined to limit replication to only those columns.
 * If no columnList is defined, then all <code>SystemColumnStandard</code> columns are
 * retrieved prior to executing the replication
 */
public class ReplicationTaskTable extends BaseEntity<Integer> {

	private ReplicationTask task;

	private SystemTable sourceSystemTable;

	private SystemCondition exclusionCondition;

	private int processingOrder;

	private boolean disabled;
	private boolean replicateInserts;
	private boolean replicateUpdates;
	private boolean replicateDeletes;


	private List<ReplicationTaskColumn> taskColumnList;
	private List<ReplicationTaskTableFilter> filterList;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReplicationTask getTask() {
		return this.task;
	}


	public void setTask(ReplicationTask task) {
		this.task = task;
	}


	public SystemTable getSourceSystemTable() {
		return this.sourceSystemTable;
	}


	public void setSourceSystemTable(SystemTable sourceSystemTable) {
		this.sourceSystemTable = sourceSystemTable;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public boolean isReplicateInserts() {
		return this.replicateInserts;
	}


	public void setReplicateInserts(boolean replicateInserts) {
		this.replicateInserts = replicateInserts;
	}


	public boolean isReplicateUpdates() {
		return this.replicateUpdates;
	}


	public void setReplicateUpdates(boolean replicateUpdates) {
		this.replicateUpdates = replicateUpdates;
	}


	public boolean isReplicateDeletes() {
		return this.replicateDeletes;
	}


	public void setReplicateDeletes(boolean replicateDeletes) {
		this.replicateDeletes = replicateDeletes;
	}


	public int getProcessingOrder() {
		return this.processingOrder;
	}


	public void setProcessingOrder(int processingOrder) {
		this.processingOrder = processingOrder;
	}


	public List<ReplicationTaskColumn> getTaskColumnList() {
		return this.taskColumnList;
	}


	public void setTaskColumnList(List<ReplicationTaskColumn> taskColumnList) {
		this.taskColumnList = taskColumnList;
	}


	public List<ReplicationTaskTableFilter> getFilterList() {
		return this.filterList;
	}


	public void setFilterList(List<ReplicationTaskTableFilter> filterList) {
		this.filterList = filterList;
	}


	public SystemCondition getExclusionCondition() {
		return this.exclusionCondition;
	}


	public void setExclusionCondition(SystemCondition exclusionCondition) {
		this.exclusionCondition = exclusionCondition;
	}
}
