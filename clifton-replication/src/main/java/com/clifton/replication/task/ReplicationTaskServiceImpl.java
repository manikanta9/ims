package com.clifton.replication.task;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.replication.task.search.ReplicationTaskColumnSearchForm;
import com.clifton.replication.task.search.ReplicationTaskSearchForm;
import com.clifton.replication.task.search.ReplicationTaskTableFilterSearchForm;
import com.clifton.replication.task.search.ReplicationTaskTableSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
 * <code>ReplicationKeyMappingServiceImpl</code> is the implementation class for the {@link ReplicationTaskService} interface.
 * It provides all CRUD operations for:
 * <p>
 * - ReplicationTask
 * - ReplicationTaskTable
 * - ReplicationTaskColumn
 * - ReplicationKeyMapping
 * - ReplicationTaskTableFilter
 */
@Service
public class ReplicationTaskServiceImpl implements ReplicationTaskService {

	private AdvancedUpdatableDAO<ReplicationTask, Criteria> replicationTaskDAO;
	private AdvancedUpdatableDAO<ReplicationTaskTable, Criteria> replicationTaskTableDAO;
	private AdvancedUpdatableDAO<ReplicationTaskColumn, Criteria> replicationTaskColumnDAO;
	private AdvancedUpdatableDAO<ReplicationTaskTableFilter, Criteria> replicationTaskTableFilterDAO;

	///////////////////////////////////////////////////////////
	////////  ReplicationTaskTable Business Methods   /////////
	///////////////////////////////////////////////////////////


	@Override
	@Transactional
	public ReplicationTask getReplicationTask(int id) {
		return getReplicationTaskPopulated(getReplicationTaskDAO().findByPrimaryKey(id));
	}


	private ReplicationTask getReplicationTaskPopulated(ReplicationTask task) {
		if (task != null) {
			ReplicationTaskTableSearchForm tableSearchForm = new ReplicationTaskTableSearchForm();
			tableSearchForm.setTaskId(task.getId());
			tableSearchForm.setOrderBy("processingOrder:ASC");
			task.setTaskTableList(getReplicationTaskTableList(tableSearchForm));

			for (ReplicationTaskTable table : CollectionUtils.getIterable(task.getTaskTableList())) {
				ReplicationTaskColumnSearchForm fieldSearchForm = new ReplicationTaskColumnSearchForm();
				fieldSearchForm.setTaskTableId(table.getId());
				table.setTaskColumnList(getReplicationTaskColumnList(fieldSearchForm));

				ReplicationTaskTableFilterSearchForm filterSearchForm = new ReplicationTaskTableFilterSearchForm();
				filterSearchForm.setTaskTableId(table.getId());
				table.setFilterList(getReplicationTaskTableFilterList(filterSearchForm));
			}
		}
		return task;
	}


	@Override
	public List<ReplicationTask> getReplicationTaskList(ReplicationTaskSearchForm searchForm) {
		return getReplicationTaskDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReplicationTask saveReplicationTask(ReplicationTask task) {
		ValidationUtils.assertNotNull(task, "Task may not be null!");
		ValidationUtils.assertNotNull(task.getSourceDataSource(), "You must define a source data source!");
		ValidationUtils.assertNotNull(task.getTargetDataSource(), "You must define a target data source!");
		ValidationUtils.assertNotEquals(task.getSourceDataSource().getId(), task.getTargetDataSource().getId(), "Source and target data sources may not be the same!");
		return getReplicationTaskDAO().save(task);
	}


	@Override
	public void deleteReplicationTask(int id) {
		getReplicationTaskDAO().delete(id);
	}

	///////////////////////////////////////////////////////////
	////////    ReplicationTaskTable Business Methods     /////////
	///////////////////////////////////////////////////////////


	@Override
	public ReplicationTaskTable getReplicationTaskTable(int id) {
		return getReplicationTaskTableDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReplicationTaskTable> getReplicationTaskTableList(ReplicationTaskTableSearchForm searchForm) {
		return getReplicationTaskTableDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReplicationTaskTable saveReplicationTaskTable(ReplicationTaskTable table) {
		return getReplicationTaskTableDAO().save(table);
	}


	@Override
	public void deleteReplicationTaskTable(int id) {
		getReplicationTaskTableDAO().delete(id);
	}

	////////////////////////////////////////////////////////////
	////////    ReplicationTaskColumn Business Methods     /////////
	////////////////////////////////////////////////////////////


	@Override
	public ReplicationTaskColumn getReplicationTaskColumn(int id) {
		return getReplicationTaskColumnDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReplicationTaskColumn> getReplicationTaskColumnList(ReplicationTaskColumnSearchForm searchForm) {
		return getReplicationTaskColumnDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReplicationTaskColumn saveReplicationTaskColumn(ReplicationTaskColumn column) {
		return getReplicationTaskColumnDAO().save(column);
	}


	@Override
	public void deleteReplicationTaskColumn(int id) {
		getReplicationTaskColumnDAO().delete(id);
	}

	///////////////////////////////////////////////////////////////////
	////// ReplicationTaskTableFilter Business Methods  /////
	///////////////////////////////////////////////////////////////////


	@Override
	public ReplicationTaskTableFilter getReplicationTaskTableFilter(int id) {
		return getReplicationTaskTableFilterDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReplicationTaskTableFilter> getReplicationTaskTableFilterList(ReplicationTaskTableFilterSearchForm searchForm) {
		return getReplicationTaskTableFilterDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReplicationTaskTableFilter saveReplicationTaskTableFilter(ReplicationTaskTableFilter searchFormFilter) {
		if (searchFormFilter.getBeanFieldValue() != null || searchFormFilter.getBeanFieldValueQuery() != null) {
			ValidationUtils.assertMutuallyExclusive("Only one of 'beanFieldValue' or 'beanFieldValueQuery' may be set!", searchFormFilter.getBeanFieldValue(), searchFormFilter.getBeanFieldValueQuery());
		}
		return getReplicationTaskTableFilterDAO().save(searchFormFilter);
	}


	@Override
	public void deleteReplicationTaskTableFilter(int id) {
		getReplicationTaskTableFilterDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReplicationTask, Criteria> getReplicationTaskDAO() {
		return this.replicationTaskDAO;
	}


	public void setReplicationTaskDAO(AdvancedUpdatableDAO<ReplicationTask, Criteria> replicationTaskDAO) {
		this.replicationTaskDAO = replicationTaskDAO;
	}


	public AdvancedUpdatableDAO<ReplicationTaskTable, Criteria> getReplicationTaskTableDAO() {
		return this.replicationTaskTableDAO;
	}


	public void setReplicationTaskTableDAO(AdvancedUpdatableDAO<ReplicationTaskTable, Criteria> replicationTaskTableDAO) {
		this.replicationTaskTableDAO = replicationTaskTableDAO;
	}


	public AdvancedUpdatableDAO<ReplicationTaskColumn, Criteria> getReplicationTaskColumnDAO() {
		return this.replicationTaskColumnDAO;
	}


	public void setReplicationTaskColumnDAO(AdvancedUpdatableDAO<ReplicationTaskColumn, Criteria> replicationTaskColumnDAO) {
		this.replicationTaskColumnDAO = replicationTaskColumnDAO;
	}


	public AdvancedUpdatableDAO<ReplicationTaskTableFilter, Criteria> getReplicationTaskTableFilterDAO() {
		return this.replicationTaskTableFilterDAO;
	}


	public void setReplicationTaskTableFilterDAO(AdvancedUpdatableDAO<ReplicationTaskTableFilter, Criteria> replicationTaskTableFilterDAO) {
		this.replicationTaskTableFilterDAO = replicationTaskTableFilterDAO;
	}
}
