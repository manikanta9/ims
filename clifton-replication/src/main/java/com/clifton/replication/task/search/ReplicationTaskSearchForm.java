package com.clifton.replication.task.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseUpdatableEntitySearchForm;
import com.clifton.replication.task.ReplicationTask;


/**
 * <code>ReplicationTaskSearchForm</code> defines search configuration for {@link ReplicationTask} objects.
 *
 * @author StevenF
 */
public class ReplicationTaskSearchForm extends BaseUpdatableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "sourceDataSource.id")
	private Short sourceDataSourceId;

	@SearchField(searchFieldPath = "sourceDataSource", searchField = "name")
	private String sourceDataSourceName;

	@SearchField(searchField = "targetDataSource.id")
	private Short targetDataSourceId;

	@SearchField(searchFieldPath = "targetDataSource", searchField = "name")
	private String targetDataSourceName;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getSourceDataSourceId() {
		return this.sourceDataSourceId;
	}


	public void setSourceDataSourceId(Short sourceDataSourceId) {
		this.sourceDataSourceId = sourceDataSourceId;
	}


	public String getSourceDataSourceName() {
		return this.sourceDataSourceName;
	}


	public void setSourceDataSourceName(String sourceDataSourceName) {
		this.sourceDataSourceName = sourceDataSourceName;
	}


	public Short getTargetDataSourceId() {
		return this.targetDataSourceId;
	}


	public void setTargetDataSourceId(Short targetDataSourceId) {
		this.targetDataSourceId = targetDataSourceId;
	}


	public String getTargetDataSourceName() {
		return this.targetDataSourceName;
	}


	public void setTargetDataSourceName(String targetDataSourceName) {
		this.targetDataSourceName = targetDataSourceName;
	}
}
