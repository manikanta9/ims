package com.clifton.replication.task;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.schema.column.SystemColumn;


/**
 * The <code>ReplicationTaskColumn</code> allows for table replication to optionally choose which columns should be replicated
 *
 * @author StevenF
 */
public class ReplicationTaskColumn extends BaseEntity<Integer> {

	private ReplicationTaskTable taskTable;
	private SystemColumn sourceSystemColumn;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReplicationTaskTable getTaskTable() {
		return this.taskTable;
	}


	public void setTaskTable(ReplicationTaskTable taskTable) {
		this.taskTable = taskTable;
	}


	public SystemColumn getSourceSystemColumn() {
		return this.sourceSystemColumn;
	}


	public void setSourceSystemColumn(SystemColumn sourceSystemColumn) {
		this.sourceSystemColumn = sourceSystemColumn;
	}
}
