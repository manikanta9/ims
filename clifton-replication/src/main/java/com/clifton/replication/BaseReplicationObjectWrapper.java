package com.clifton.replication;

import com.clifton.core.beans.IdentityObject;


/**
 * The <code>BaseReplicationObjectWrapper</code> class is a simple wrapper class implemented to increase performance during the replication process by eliminating the need to repeatedly
 * calculate naturalKey and ID for a given object.
 *
 * @author jonathanr
 */
public class BaseReplicationObjectWrapper<O extends IdentityObject> {

	private O object;
	private Long id;
	private String naturalKey;
	private BaseReplicationObjectWrapper<O> referenceObject;


	public BaseReplicationObjectWrapper(O object, Long id, String naturalKey) {
		this.object = object;
		this.id = id;
		this.naturalKey = naturalKey;
	}


	public O getObject() {
		return this.object;
	}


	public void setObject(O object) {
		this.object = object;
	}


	public Long getId() {
		return this.id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getNaturalKey() {
		return this.naturalKey;
	}


	public void setNaturalKey(String naturalKey) {
		this.naturalKey = naturalKey;
	}


	public BaseReplicationObjectWrapper<O> getReferenceObject() {
		return this.referenceObject;
	}


	public void setReferenceObject(BaseReplicationObjectWrapper<O> referenceObject) {
		this.referenceObject = referenceObject;
	}
}
