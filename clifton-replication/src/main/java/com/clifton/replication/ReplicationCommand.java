package com.clifton.replication;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;

import java.util.Date;


/**
 * @author stevenf
 */
public class ReplicationCommand {

	private Status status;

	private Integer replicationTaskId;

	private boolean synchronous;

	private boolean previewOnly;

	private boolean showTableRowStatus;

	/////////////////////////////////////////////////////////////////////////////


	public String getRunId() {
		StringBuilder runId = new StringBuilder();
		if (getReplicationTaskId() != null) {
			runId.append("Task").append(getReplicationTaskId());
		}
		else {
			runId.append("ALL");
		}
		runId.append("-").append(DateUtils.fromDateShort(new Date()));
		return runId.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isSynchronous() {
		return this.synchronous;
	}


	public void setSynchronous(boolean synchronous) {
		this.synchronous = synchronous;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public Integer getReplicationTaskId() {
		return this.replicationTaskId;
	}


	public void setReplicationTaskId(Integer replicationTaskId) {
		this.replicationTaskId = replicationTaskId;
	}


	public boolean isPreviewOnly() {
		return this.previewOnly;
	}


	public void setPreviewOnly(boolean previewOnly) {
		this.previewOnly = previewOnly;
	}


	public boolean isShowTableRowStatus() {
		return this.showTableRowStatus;
	}


	public void setShowTableRowStatus(boolean showTableRowStatus) {
		this.showTableRowStatus = showTableRowStatus;
	}
}
