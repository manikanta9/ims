package com.clifton.replication;

import com.clifton.core.util.status.Status;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author stevenf
 */
public interface ReplicationService {

	public static final String INSERTED = "Inserted";
	public static final String DELETED = "Deleted";
	public static final String UPDATED = "Updated";
	public static final String SKIPPED = "Skipped";
	public static final String ERROR = "Error";
	public static final String REPLICATING = "Replicating";


	@RequestMapping("replicationTaskExecute")
	public Status executeReplicationTask(final ReplicationCommand command);
}
