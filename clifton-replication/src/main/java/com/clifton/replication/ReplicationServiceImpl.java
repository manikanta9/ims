package com.clifton.replication;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.form.BaseSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.replication.jobs.ReplicationJob;
import com.clifton.replication.replicator.Replicator;
import com.clifton.replication.task.ReplicationTask;
import com.clifton.replication.task.ReplicationTaskService;
import com.clifton.replication.task.ReplicationTaskTable;
import org.springframework.stereotype.Service;

import java.lang.reflect.Modifier;
import java.util.Date;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.clifton.core.util.date.DateUtils.fromDate;
import static com.clifton.core.util.status.Status.ofMessage;


/**
 * The <code>ReplicationServiceImpl</code> class provides basic implementation of ReplicationService interface.
 *
 * @author StevenF
 */
@Service
public class ReplicationServiceImpl<O extends IdentityObject, S> implements ReplicationService {

	private DaoLocator daoLocator;
	private ReplicatorLocator<O, S> replicatorLocator;
	private ReplicationTaskService replicationTaskService;

	private RunnerHandler runnerHandler;


	@Override
	public Status executeReplicationTask(final ReplicationCommand command) {
		if (command.isSynchronous()) {
			Status status = (command.getStatus() != null) ? command.getStatus() : ofMessage("Synchronously running for " + command);
			command.setStatus(status);
			doExecuteReplicationTask(command);
			return status;
		}

		// asynchronous run support
		final String runId = command.getRunId();
		final Date now = new Date();
		final StatusHolder statusHolder = (command.getStatus() != null) ? new StatusHolder(command.getStatus()) : new StatusHolder("Scheduled for " + fromDate(now));
		Runner runner = new AbstractStatusAwareRunner("REPLICATION-TASK", runId, now, statusHolder) {

			@Override
			public void run() {
				try {
					command.setStatus(statusHolder.getStatus());
					doExecuteReplicationTask(command);
				} catch (Throwable e) {
					getStatus().setMessage("Error executing replication task " + runId + ": " + ExceptionUtils.getDetailedMessage(e));
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error executing replication task for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);

		return Status.ofMessage("Started replication: " + runId + ". Processing will be completed shortly.");
	}


	private Status doExecuteReplicationTask(ReplicationCommand command) {
		Status status = command.getStatus();
		Integer replicationTaskId = command.getReplicationTaskId();

		ValidationUtils.assertNotNull(replicationTaskId, "Replication task id is required!");

		Map<String, Class<?>> searchFormClassesMap = CoreClassUtils.getProjectClassFiles(BaseSearchForm.class).stream()
				.filter(clazz -> AnnotationUtils.getAnnotation(clazz, SearchForm.class).hasOrmDtoClass())
				.filter(clazz -> !(Modifier.isAbstract(clazz.getModifiers())))
				.collect(Collectors.toMap(Class::getSimpleName, Function.identity()));


		ReplicationTask task = getReplicationTaskService().getReplicationTask(replicationTaskId);
		task.setCommand(command);
		clearContextForReplicators(task, searchFormClassesMap);
		Map<String, Status> statusList = new HashMap<>();

		for (ReplicationTaskTable taskTable : CollectionUtils.getIterable(task.getTaskTableList())) {
			if (!taskTable.isDisabled()) {

				Status tableStatus = replicateTable(taskTable, false);

				//Now add the individual tableStatus details to the overall status object.
				String msg = "Finished update/insert replication of table [" + taskTable.getSourceSystemTable().getName() + "]:" + getTableStatusMessage(tableStatus);
				LogUtils.info(LogCommand.ofMessage(this.getClass(), msg));
				status.addMessage(msg);
				status.addErrors(tableStatus.getDetailListForCategory(StatusDetail.CATEGORY_ERROR).stream().map(StatusDetail::getNote).collect(Collectors.toList()));
				status.addWarnings(tableStatus.getDetailListForCategory(StatusDetail.CATEGORY_WARNING).stream().map(StatusDetail::getNote).collect(Collectors.toList()));
				statusList.put(taskTable.getSourceSystemTable().getName(), tableStatus);
			}
		}
		//Delete operations need to be executed in the opposite order of inserts.
		ListIterator<ReplicationTaskTable> li = task.getTaskTableList().listIterator(task.getTaskTableList().size());
		while (li.hasPrevious()) {
			ReplicationTaskTable taskTable = li.previous();
			if (!taskTable.isDisabled() && taskTable.isReplicateDeletes()) {

				Status tableStatus = replicateTable(taskTable, true);

				//Now add the individual tableStatus details to the overall status object.
				String msg = "Finished delete replication of table [" + taskTable.getSourceSystemTable().getName() + "]:" + getTableStatusMessage(tableStatus);
				LogUtils.info(LogCommand.ofMessage(this.getClass(), msg));
				status.addMessage(msg);
				status.addErrors(tableStatus.getDetailListForCategory(StatusDetail.CATEGORY_ERROR).stream().map(StatusDetail::getNote).collect(Collectors.toList()));
				status.addWarnings(tableStatus.getDetailListForCategory(StatusDetail.CATEGORY_WARNING).stream().map(StatusDetail::getNote).collect(Collectors.toList()));
				statusList.put(taskTable.getSourceSystemTable().getName(), tableStatus);
			}
		}
		int totalInsertedCount = 0;
		int totalUpdatedCount = 0;
		int totalSkippedCount = 0;
		int totalDeletedCount = 0;

		for (Status tableStatus : CollectionUtils.getIterable(statusList.values())) {
			if (command.isShowTableRowStatus()) {
				for (Map.Entry<String, Object> entry : CollectionUtils.getIterable(tableStatus.getStatusMap().entrySet())) {
					if (entry.getValue() instanceof String) {
						status.addDetail((String) entry.getValue(), entry.getKey());
					}
				}
			}

			totalInsertedCount += tableStatus.getStatusCount(ReplicationService.INSERTED);
			totalUpdatedCount += tableStatus.getStatusCount(ReplicationService.UPDATED);
			totalSkippedCount += tableStatus.getStatusCount(ReplicationService.SKIPPED);
			totalDeletedCount += tableStatus.getStatusCount(ReplicationService.DELETED);
		}
		int totalCount = totalInsertedCount + totalUpdatedCount + totalSkippedCount + totalDeletedCount;

		if (command.isPreviewOnly()) {
			status.setMessage("Preview completed: Total " + totalCount + "; Inserts " + totalInsertedCount + "; Updates " + totalUpdatedCount + "; Deletes " + totalDeletedCount + "; Skips " + totalSkippedCount + "; Errors " + status.getErrorCount() + "; Warnings " + status.getWarningCount());
			LogUtils.info(LogCommand.ofMessage(this.getClass(), "Preview completed: Total ", totalCount, "; Inserts ", totalInsertedCount, "; Updates ", totalUpdatedCount, "; Deletes " + totalDeletedCount, "; Skips ", totalSkippedCount, "; Errors " + status.getErrorCount(), "; Warnings ", status.getWarningCount()));
			status.setActionPerformed(false);
		} else {
			status.setMessage("Replication completed: Total " + totalCount + "; Inserted " + totalInsertedCount + "; Updated " + totalUpdatedCount + "; Deleted " + totalDeletedCount + "; Skipped " + totalSkippedCount + "; Errors " + status.getErrorCount() + "; Warnings " + status.getWarningCount());
			LogUtils.info(LogCommand.ofMessage(this.getClass(), "Replication completed: Total ", totalCount, "; Inserted ", totalInsertedCount, "; Updated ", totalUpdatedCount, "; Deleted " + totalDeletedCount, "; Skipped ", totalSkippedCount, "; Errors " + status.getErrorCount(), "; Warnings ", status.getWarningCount()));
		}

		return status;
	}


	private String getTableStatusMessage(Status tableStatus) {

		int totalTableInsertedCount = tableStatus.getStatusCount(ReplicationService.INSERTED);
		int totalTableUpdatedCount = tableStatus.getStatusCount(ReplicationService.UPDATED);
		int totalTableSkippedCount = tableStatus.getStatusCount(ReplicationService.SKIPPED);
		int totalTableDeletedCount = tableStatus.getStatusCount(ReplicationService.DELETED);
		int totalTableCount = totalTableInsertedCount + totalTableUpdatedCount + totalTableSkippedCount + totalTableDeletedCount;
		StringBuilder msg = new StringBuilder();

		msg.append("Total ").append(totalTableCount).append("; Inserted ").append(totalTableInsertedCount).append("; Updated ").append(totalTableUpdatedCount);
		msg.append("; Skipped ").append(totalTableSkippedCount).append("; Deleted ").append(totalTableDeletedCount);
		msg.append("; Errors ").append(tableStatus.getErrorCount()).append("; Warnings ").append(tableStatus.getWarningCount());
		msg.append("; Key Mappings Created ").append(tableStatus.getStatusCount("KeyMappingsCreated"));

		return msg.toString();
	}


	@SuppressWarnings("unchecked")
	private void clearContextForReplicators(ReplicationTask task, Map<String, Class<?>> searchFormClassesMap) {
		for (ReplicationTaskTable taskTable : CollectionUtils.getIterable(task.getTaskTableList())) {
			ReadOnlyDAO<?> dao = getDaoLocator().locate(taskTable.getSourceSystemTable().getName());
			Replicator<O, S> replicator = getReplicatorLocator().locate((Class<O>) dao.getConfiguration().getBeanClass());
			replicator.clearContext(taskTable, searchFormClassesMap);
		}
	}


	@SuppressWarnings("unchecked")
	private Status replicateTable(ReplicationTaskTable taskTable, boolean delete) {

		Status tableStatus = new Status();
		try {
			ReadOnlyDAO<?> dao = getDaoLocator().locate(taskTable.getSourceSystemTable().getName());
			Replicator<O, S> replicator = getReplicatorLocator().locate((Class<O>) dao.getConfiguration().getBeanClass());
			if (delete) {
				LogUtils.debug(this.getClass(), "Replicating deletes for table [" + taskTable.getSourceSystemTable().getName() + "]");
				tableStatus = replicator.doDelete();
			} else {
				LogUtils.debug(this.getClass(), "Replicating inserts/updates for table [" + taskTable.getSourceSystemTable().getName() + "]");
				tableStatus = replicator.doReplication();
			}
		} catch (Exception e) {
			String errorMessage = ExceptionUtils.getDetailedMessage(e);
			tableStatus.addError("Failed " + (delete ? "deletes" : "inserts/updates") + " replication for table [" + taskTable.getSourceSystemTable().getName() + "]: " + errorMessage);
			LogUtils.error(ReplicationJob.class, errorMessage, e);
		}
		return tableStatus;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ReplicatorLocator<O, S> getReplicatorLocator() {
		return this.replicatorLocator;
	}


	public void setReplicatorLocator(ReplicatorLocator<O, S> replicatorLocator) {
		this.replicatorLocator = replicatorLocator;
	}


	public ReplicationTaskService getReplicationTaskService() {
		return this.replicationTaskService;
	}


	public void setReplicationTaskService(ReplicationTaskService replicationTaskService) {
		this.replicationTaskService = replicationTaskService;
	}
}
