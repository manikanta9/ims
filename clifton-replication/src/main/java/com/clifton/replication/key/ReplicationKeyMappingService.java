package com.clifton.replication.key;

import com.clifton.replication.key.search.ReplicationKeyMappingSearchForm;

import java.util.List;


/**
 * The <code>ReplicationKeyMappingService</code> defines all CRUD methods for replication key mapping.
 *
 * @author StevenF
 */
public interface ReplicationKeyMappingService {

	///////////////////////////////////////////////////////////////////
	////////    ReplicationKeyMapping Business Methods     ////////
	///////////////////////////////////////////////////////////////////


	public ReplicationKeyMapping getReplicationKeyMapping(long id);


	public List<ReplicationKeyMapping> getReplicationKeyMappingList(ReplicationKeyMappingSearchForm searchForm);


	public ReplicationKeyMapping saveReplicationKeyMapping(ReplicationKeyMapping keyMapping);


	public void deleteReplicationKeyMapping(long id);
}
