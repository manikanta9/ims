package com.clifton.replication.key.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.replication.key.ReplicationKeyMapping;


/**
 * <code>ReplicationKeyMappingSearchForm</code> defines search configuration for {@link ReplicationKeyMapping} objects.
 *
 * @author StevenF
 */
public class ReplicationKeyMappingSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "firstDataSourceSystemTable.name,firstDataSourceSystemTable.dataSource.name,secondDataSource.name")
	private String searchPattern;

	@SearchField(searchFieldPath = "firstDataSourceSystemTable", searchField = "name")
	private String firstSystemTableName;

	@SearchField(searchFieldPath = "firstDataSourceSystemTable", searchField = "name", comparisonConditions = ComparisonConditions.EQUALS)
	private String firstSystemTableNameEquals;

	@SearchField(searchFieldPath = "firstDataSourceSystemTable", searchField = "dataSource.id")
	private Short firstDataSourceId;

	@SearchField(searchFieldPath = "firstDataSourceSystemTable.dataSource", searchField = "databaseName")
	private String firstDataSourceDatabaseName;

	@SearchField(searchFieldPath = "firstDataSourceSystemTable.dataSource", searchField = "databaseName", comparisonConditions = ComparisonConditions.EQUALS)
	private String firstDataSourceDatabaseNameEquals;

	@SearchField(searchField = "secondDataSource.id")
	private Short secondDataSourceId;

	@SearchField(searchFieldPath = "secondDataSource", searchField = "databaseName")
	private String secondDataSourceDatabaseName;

	@SearchField(searchFieldPath = "secondDataSource", searchField = "databaseName", comparisonConditions = ComparisonConditions.EQUALS)
	private String secondDataSourceDatabaseNameEquals;

	@SearchField
	private Long firstDataSourceFkFieldId;

	@SearchField
	private Long secondDataSourceFkFieldId;


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getFirstSystemTableName() {
		return this.firstSystemTableName;
	}


	public void setFirstSystemTableName(String firstSystemTableName) {
		this.firstSystemTableName = firstSystemTableName;
	}


	public String getFirstSystemTableNameEquals() {
		return this.firstSystemTableNameEquals;
	}


	public void setFirstSystemTableNameEquals(String firstSystemTableNameEquals) {
		this.firstSystemTableNameEquals = firstSystemTableNameEquals;
	}


	public Short getFirstDataSourceId() {
		return this.firstDataSourceId;
	}


	public void setFirstDataSourceId(Short firstDataSourceId) {
		this.firstDataSourceId = firstDataSourceId;
	}


	public String getFirstDataSourceDatabaseName() {
		return this.firstDataSourceDatabaseName;
	}


	public void setFirstDataSourceDatabaseName(String firstDataSourceDatabaseName) {
		this.firstDataSourceDatabaseName = firstDataSourceDatabaseName;
	}


	public String getFirstDataSourceDatabaseNameEquals() {
		return this.firstDataSourceDatabaseNameEquals;
	}


	public void setFirstDataSourceDatabaseNameEquals(String firstDataSourceDatabaseNameEquals) {
		this.firstDataSourceDatabaseNameEquals = firstDataSourceDatabaseNameEquals;
	}


	public Short getSecondDataSourceId() {
		return this.secondDataSourceId;
	}


	public void setSecondDataSourceId(Short secondDataSourceId) {
		this.secondDataSourceId = secondDataSourceId;
	}


	public String getSecondDataSourceDatabaseName() {
		return this.secondDataSourceDatabaseName;
	}


	public void setSecondDataSourceDatabaseName(String secondDataSourceDatabaseName) {
		this.secondDataSourceDatabaseName = secondDataSourceDatabaseName;
	}


	public String getSecondDataSourceDatabaseNameEquals() {
		return this.secondDataSourceDatabaseNameEquals;
	}


	public void setSecondDataSourceDatabaseNameEquals(String secondDataSourceDatabaseNameEquals) {
		this.secondDataSourceDatabaseNameEquals = secondDataSourceDatabaseNameEquals;
	}


	public Long getFirstDataSourceFkFieldId() {
		return this.firstDataSourceFkFieldId;
	}


	public void setFirstDataSourceFkFieldId(Long firstDataSourceFkFieldId) {
		this.firstDataSourceFkFieldId = firstDataSourceFkFieldId;
	}


	public Long getSecondDataSourceFkFieldId() {
		return this.secondDataSourceFkFieldId;
	}


	public void setSecondDataSourceFkFieldId(Long secondDataSourceFkFieldId) {
		this.secondDataSourceFkFieldId = secondDataSourceFkFieldId;
	}
}
