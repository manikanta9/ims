package com.clifton.replication.key;

import com.clifton.core.beans.BaseEntity;
import com.clifton.system.schema.SystemDataSource;
import com.clifton.system.schema.SystemTable;

import java.util.Date;


/**
 * The <code>ReplicationKeyMapping</code> tracks the individual replication fkFieldIds for a given
 * source and target object.  This allows the sourceId and targetIds to be tracked so when objects
 * are updated in the source, the proper target item can be updated as well.
 *
 * @author StevenF
 */
public class ReplicationKeyMapping extends BaseEntity<Long> {

	private long firstDataSourceFkFieldId;
	private long secondDataSourceFkFieldId;

	private Date replicationDate;

	private SystemTable firstDataSourceSystemTable;
	private SystemDataSource secondDataSource;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public long getFirstDataSourceFkFieldId() {
		return this.firstDataSourceFkFieldId;
	}


	public void setFirstDataSourceFkFieldId(long firstDataSourceFkFieldId) {
		this.firstDataSourceFkFieldId = firstDataSourceFkFieldId;
	}


	public long getSecondDataSourceFkFieldId() {
		return this.secondDataSourceFkFieldId;
	}


	public void setSecondDataSourceFkFieldId(long secondDataSourceFkFieldId) {
		this.secondDataSourceFkFieldId = secondDataSourceFkFieldId;
	}


	public SystemTable getFirstDataSourceSystemTable() {
		return this.firstDataSourceSystemTable;
	}


	public void setFirstDataSourceSystemTable(SystemTable firstDataSourceSystemTable) {
		this.firstDataSourceSystemTable = firstDataSourceSystemTable;
	}


	public SystemDataSource getSecondDataSource() {
		return this.secondDataSource;
	}


	public void setSecondDataSource(SystemDataSource secondDataSource) {
		this.secondDataSource = secondDataSource;
	}


	public Date getReplicationDate() {
		return this.replicationDate;
	}


	public void setReplicationDate(Date replicationDate) {
		this.replicationDate = replicationDate;
	}
}
