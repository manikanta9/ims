package com.clifton.replication.key;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.AssertUtils;
import com.clifton.replication.key.search.ReplicationKeyMappingSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * <code>ReplicationKeyMappingServiceImpl</code> is the implementation class for the {@link ReplicationKeyMappingService} interface.
 */
@Service
public class ReplicationKeyMappingServiceImpl implements ReplicationKeyMappingService {

	private AdvancedUpdatableDAO<ReplicationKeyMapping, Criteria> replicationKeyMappingDAO;

	////////////////////////////////////////////////////////////
	////////    ReplicationKeyMapping Business Methods     ////////
	////////////////////////////////////////////////////////////


	@Override
	public ReplicationKeyMapping getReplicationKeyMapping(long id) {
		return getReplicationKeyMappingDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReplicationKeyMapping> getReplicationKeyMappingList(ReplicationKeyMappingSearchForm searchForm) {
		return getReplicationKeyMappingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ReplicationKeyMapping saveReplicationKeyMapping(ReplicationKeyMapping keyMapping) {
		AssertUtils.assertTrue(keyMapping.getFirstDataSourceSystemTable().getDataSource().getId() < keyMapping.getSecondDataSource().getId(),
				"By Convention the FirstDataSource ID %s must have a lower value than the secondDataSource ID %s",
				keyMapping.getFirstDataSourceSystemTable().getDataSource().getId(),
				keyMapping.getSecondDataSource().getId());
		return getReplicationKeyMappingDAO().save(keyMapping);
	}


	@Override
	public void deleteReplicationKeyMapping(long id) {
		getReplicationKeyMappingDAO().delete(id);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReplicationKeyMapping, Criteria> getReplicationKeyMappingDAO() {
		return this.replicationKeyMappingDAO;
	}


	public void setReplicationKeyMappingDAO(AdvancedUpdatableDAO<ReplicationKeyMapping, Criteria> replicationKeyMappingDAO) {
		this.replicationKeyMappingDAO = replicationKeyMappingDAO;
	}
}
