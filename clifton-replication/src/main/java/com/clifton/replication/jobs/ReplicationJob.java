package com.clifton.replication.jobs;

import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.replication.ReplicationCommand;
import com.clifton.replication.ReplicationService;
import com.clifton.replication.task.ReplicationTask;
import com.clifton.replication.task.ReplicationTaskService;

import java.util.Map;


/**
 * @author StevenF
 */
public class ReplicationJob implements Task, StatusHolderObjectAware<Status>, ValidationAware {

	private Integer replicationTaskId;

	private ReplicationService replicationService;
	private ReplicationTaskService replicationTaskService;

	/**
	 * Object used to return the overall status of the replication process
	 */
	private StatusHolderObject<Status> statusHolder;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(getReplicationTaskId(), "Replication Task ID may not be null!");
		ReplicationTask task = getReplicationTaskService().getReplicationTask(getReplicationTaskId());
		ValidationUtils.assertNotNull(task, "Replication Task may not be null!");
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	@Override
	public Status run(Map<String, Object> context) {
		ReplicationCommand replicationCommand = new ReplicationCommand();
		replicationCommand.setSynchronous(true);
		replicationCommand.setReplicationTaskId(getReplicationTaskId());
		replicationCommand.setStatus(this.statusHolder.getStatus());
		return getReplicationService().executeReplicationTask(replicationCommand);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public ReplicationService getReplicationService() {
		return this.replicationService;
	}


	public void setReplicationService(ReplicationService replicationService) {
		this.replicationService = replicationService;
	}


	public ReplicationTaskService getReplicationTaskService() {
		return this.replicationTaskService;
	}


	public void setReplicationTaskService(ReplicationTaskService replicationTaskService) {
		this.replicationTaskService = replicationTaskService;
	}


	public Integer getReplicationTaskId() {
		return this.replicationTaskId;
	}


	public void setReplicationTaskId(Integer replicationTaskId) {
		this.replicationTaskId = replicationTaskId;
	}
}
