package com.clifton.replication;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.AssertUtils;
import com.clifton.replication.replicator.IdentityObjectReplicator;
import com.clifton.replication.replicator.Replicator;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <code>ReplicatorLocatorImpl</code> provides the implementation for {@link ReplicatorLocator}
 * It registers {@link Replicator} implementations for retrieval/execution during replications.
 *
 * @author StevenF
 */
@Component
public class ReplicatorLocatorImpl<O extends IdentityObject, S> implements ReplicatorLocator<O, S>, InitializingBean, ApplicationContextAware {

	private ApplicationContextService applicationContextService;
	private Map<Class<O>, Replicator<O, S>> replicationTableMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public Replicator<O, S> locate(Class<O> entityClass) {
		AssertUtils.assertNotNull(entityClass, "Required entity class cannot be null.");
		Replicator<O, S> result = getReplicationTableMap().get(entityClass);
		if (result == null) {
			Replicator<O, S> identityObjectReplicator = (Replicator<O, S>) new IdentityObjectReplicator();
			getApplicationContextService().autowireBean(identityObjectReplicator);
			result = identityObjectReplicator;
			getReplicationTableMap().put(entityClass, result);
		}
		return result;
	}


	/**
	 * Attempts to locate any replicators that were created specifically for DTOs; if none are found,
	 * then when a request is made to locate a new generic {@link IdentityObjectReplicator} will be created and returned.
	 */
	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void afterPropertiesSet() {
		Map<String, Replicator> beanMap = getApplicationContext().getBeansOfType(Replicator.class);
		for (Map.Entry<String, Replicator> beanMapEntry : beanMap.entrySet()) {
			Replicator replicator = beanMapEntry.getValue();
			if (getReplicationTableMap().containsKey(replicator.getDtoClass())) {
				throw new RuntimeException("Cannot register '" + beanMapEntry.getKey() + "' as a replicator '" + replicator.getDtoClass()
						+ "' because this class already has a registered replicator.");
			}
			getReplicationTableMap().put(replicator.getDtoClass(), replicator);
		}
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public Map<Class<O>, Replicator<O, S>> getReplicationTableMap() {
		return this.replicationTableMap;
	}


	public void setReplicationTableMap(Map<Class<O>, Replicator<O, S>> replicationTableMap) {
		this.replicationTableMap = replicationTableMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
