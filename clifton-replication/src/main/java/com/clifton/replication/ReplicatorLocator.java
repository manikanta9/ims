package com.clifton.replication;

import com.clifton.core.beans.IdentityObject;
import com.clifton.replication.replicator.Replicator;


/**
 * The <code>ReplicatorLocator</code> is used to identify all beans that implement the {@link Replicator} interface.
 * The beans are stored in a map and retrieved/executed when replication for a given DTO object is triggered.
 *
 * @author StevenF
 */
public interface ReplicatorLocator<O extends IdentityObject, S> {

	public Replicator<O, S> locate(Class<O> entityClass);
}
