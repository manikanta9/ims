package com.clifton.replication.replicator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusLogger;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.api.client.locator.ExternalServiceLocator;
import com.clifton.replication.BaseReplicationObjectWrapper;
import com.clifton.replication.ReplicationService;
import com.clifton.replication.ReplicatorLocator;
import com.clifton.replication.key.ReplicationKeyMapping;
import com.clifton.replication.key.ReplicationKeyMappingService;
import com.clifton.replication.key.search.ReplicationKeyMappingSearchForm;
import com.clifton.replication.task.ReplicationTask;
import com.clifton.replication.task.ReplicationTaskColumn;
import com.clifton.replication.task.ReplicationTaskService;
import com.clifton.replication.task.ReplicationTaskTable;
import com.clifton.replication.task.context.ReplicationTaskContext;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;


/**
 * <code>BaseReplicator</code> is an abstract implementation of the {@link Replicator} interface
 * and provides a default implementation that initializes objects used during replication.
 * <p>
 * It adds a number of abstract methods that must be implemented by all subclasses to provide
 * proper configuration for their given DTOs replication to another system.
 *
 * @param <O> - DTO Object for the given replication
 * @param <S> - Service used for CRUD operations for the given DTO
 */
public abstract class BaseReplicator<O extends IdentityObject, S> implements Replicator<O, S> {

	private ReplicationTaskTable taskTable;
	private Map<String, Class<?>> searchFormClassesMap;
	private ReplicationTaskService replicationTaskService;
	private ReplicationKeyMappingService replicationKeyMappingService;
	private SystemColumnService systemColumnService;
	private SystemSchemaService systemSchemaService;
	private DaoLocator daoLocator;
	private ReplicatorLocator<O, S> replicatorLocator;
	private ExternalServiceLocator externalServiceLocator;
	private SqlHandler sqlHandler;
	private Class<O> dtoClass;
	private Class<S> dtoServiceClass;
	private Lazy<ReplicationTaskContext<O, S>> lazyContext = new Lazy<>(this::initializeContext);

	/////////////////////////////////////////////////////////////////////////////
	////////////             Initialization Methods              ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public void clearContext(ReplicationTaskTable taskTable, Map<String, Class<?>> searchFormClassesMap) {
		LogUtils.debug(this.getClass(), "Initializing Replicator: " + taskTable.getSourceSystemTable().getName());
		AssertUtils.assertNotNull(taskTable, "Replication task table may not be null!");
		AssertUtils.assertNotNull(taskTable.getTask(), "Replication task may not be null!");
		setTaskTable(taskTable);
		setSearchFormClassesMap(searchFormClassesMap);
		this.dtoClass = null;
		this.dtoServiceClass = null;
		this.lazyContext = new Lazy<>(this::initializeContext);
	}


	private ReplicationTaskContext<O, S> initializeContext() {
		ReplicationTaskContext<O, S> context = new ReplicationTaskContext<>();
		context.setTaskTable(getTaskTable());
		context.setReplicationDate(new Date());
		StatusLogger status = new StatusLogger();
		status.setDefaultMsgLogLevel(StatusLogger.INFO);
		status.setLoggingClass(this.getClass());
		context.setStatus(status);
		//Initialize column list
		if (CollectionUtils.isEmpty(getTaskTable().getTaskColumnList())) {
			List<ReplicationTaskColumn> taskColumnList = new ArrayList<>();
			for (SystemColumnStandard systemColumn : CollectionUtils.getIterable(getSystemColumnService().getSystemColumnStandardListByTable(getTaskTable().getSourceSystemTable().getId()))) {
				if (!"id".equals(systemColumn.getBeanPropertyName()) && !ArrayUtils.contains(BeanUtils.AUDIT_PROPERTIES, systemColumn.getBeanPropertyName())) {
					ReplicationTaskColumn replicationTaskColumn = new ReplicationTaskColumn();
					replicationTaskColumn.setTaskTable(getTaskTable());
					replicationTaskColumn.setSourceSystemColumn(systemColumn);
					taskColumnList.add(replicationTaskColumn);
				}
			}
			getTaskTable().setTaskColumnList(taskColumnList);
		}
		context.setTaskColumnList(getTaskTable().getTaskColumnList());

		//Initialize data sources
		context.setSourceDataSource(getTaskTable().getTask().getSourceDataSource());
		LogUtils.debug(LogCommand.ofMessage(this.getClass(), "Initialized data source ", getTaskTable().getTask().getSourceDataSource().getDatabaseName()));

		context.setTargetDataSource(getTaskTable().getTask().getTargetDataSource());
		LogUtils.debug(LogCommand.ofMessage(this.getClass(), "Initialized data source ", getTaskTable().getTask().getTargetDataSource().getDatabaseName()));

		//Initialize services
		AssertUtils.assertNotNull(getDtoServiceClass(), "Unable to initialize replication!  No DTO Service Class found for '%s'.", getTaskTable().getSourceSystemTable().getName());
		context.setSourceService(getExternalServiceLocator().getExternalService(getDtoServiceClass(), context.getSourceDataSource().getName()));
		LogUtils.trace(LogCommand.ofMessage(this.getClass(), "Source service: ", unwrap(context.getSourceService())));
		context.setTargetService(getExternalServiceLocator().getExternalService(getDtoServiceClass(), context.getTargetDataSource().getName()));
		LogUtils.trace(LogCommand.ofMessage(this.getClass(), "Target service: ", unwrap(context.getTargetService())));

		//Create data object maps for simplified lookups
		List<BaseReplicationObjectWrapper<O>> sourceObjects = wrapObjects(getObjectList(context, true));
		List<BaseReplicationObjectWrapper<O>> targetObjects = wrapObjects(getObjectList(context, false));

		context.setSourceObjectMap(BeanUtils.getBeanMapUnique(sourceObjects, BaseReplicationObjectWrapper::getId));
		context.setTargetObjectMap(BeanUtils.getBeanMapUnique(targetObjects, BaseReplicationObjectWrapper::getId));

		context.setSourceNaturalKeyMap(generateNaturalKeyToIdMap(sourceObjects));
		context.setTargetNaturalKeyMap(generateNaturalKeyToIdMap(targetObjects));

		if (!context.getFirstDataSource().getId().equals(getTaskTable().getSourceSystemTable().getDataSource().getId())) {
			context.setFirstDataSourceSystemTable(getSystemSchemaService().getSystemTableByNameAndDataSource(getTaskTable().getSourceSystemTable().getName(), context.getFirstDataSource().getName()));
		}
		else {
			context.setFirstDataSourceSystemTable(getTaskTable().getSourceSystemTable());
		}
		context.populateBidirectionalIdMap(getFullReplicationKeyMappingList(context));
		//We need to check that we have the correct SystemTable for the key mappings.

		LogUtils.info(LogCommand.ofMessage(this.getClass(), "Loaded ", context.getBiDirectionalIdMap().size(), " Key Maps for table [", context.getTaskTable().getSourceSystemTable().getName(), "]"));
		LogUtils.info(LogCommand.ofMessage(this.getClass(), "Loaded ", context.getSourceObjectMap().size(), " Source Objects and ", context.getTargetObjectMap().size(), " target objects for table [", context.getTaskTable().getSourceSystemTable().getName(), "]"));
		return context;
	}


	private Map<String, Long> generateNaturalKeyToIdMap(List<BaseReplicationObjectWrapper<O>> wrappedObjectList) {
		Map<String, Long> map = new HashMap<>();
		for (BaseReplicationObjectWrapper<O> wrappedObject : CollectionUtils.getIterable(wrappedObjectList)) {
			map.put(wrappedObject.getNaturalKey(), wrappedObject.getId());
		}
		return map;
	}


	private List<ReplicationKeyMapping> getFullReplicationKeyMappingList(ReplicationTaskContext<O, S> context) {
		ReplicationKeyMappingSearchForm searchForm = new ReplicationKeyMappingSearchForm();
		searchForm.setFirstDataSourceId(context.getFirstDataSource().getId());
		searchForm.setSecondDataSourceId(context.getSecondDataSource().getId());
		searchForm.setFirstSystemTableNameEquals(context.getFirstDataSourceSystemTable().getName());
		return getReplicationKeyMappingService().getReplicationKeyMappingList(searchForm);
	}


	private List<BaseReplicationObjectWrapper<O>> wrapObjects(List<O> objects) {
		ArrayList<BaseReplicationObjectWrapper<O>> wrappedList = new ArrayList<>();
		for (O object : CollectionUtils.getIterable(objects)) {
			wrappedList.add(wrapObject(object, null));
		}
		return wrappedList;
	}


	private BaseReplicationObjectWrapper<O> wrapObject(O object, BaseReplicationObjectWrapper<O> referenceObject) {
		BaseReplicationObjectWrapper<O> wrapObject = new BaseReplicationObjectWrapper<>(object, getObjectIdentity(object), getNaturalKey(object));
		if (referenceObject != null) {
			wrapObject.setReferenceObject(referenceObject);
		}
		return wrapObject;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////               Entry Point methods                      //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status doReplication() {
		ReplicationTaskContext<O, S> context = getContext();
		if (context.getTaskTable().isReplicateInserts() || context.getTaskTable().isReplicateUpdates()) {
			//Iterate the source object list so that any ordering from the retrieval function is retained
			for (BaseReplicationObjectWrapper<O> sourceObject : CollectionUtils.getIterable(context.getSourceObjectMap().values())) {
				replicateObject(sourceObject);
			}
		}
		return context.getStatus();
	}


	@Override
	public Status doDelete() {
		ReplicationTaskContext<O, S> context = getContext();

		if (context.getTaskTable().isReplicateDeletes()) {
			Iterator<BaseReplicationObjectWrapper<O>> iterator = CollectionUtils.getIterable(context.getTargetObjectMap().values()).iterator();
			//Iterate the target object list so that any ordering from the retrieval function is retained
			while (iterator.hasNext()) {
				BaseReplicationObjectWrapper<O> targetObject = iterator.next();
				if (replicateDeleteObject(targetObject)) {
					iterator.remove();
				}
			}
		}
		return context.getStatus();
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                 Decision Making Methods                //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public O replicateObject(BaseReplicationObjectWrapper<O> wrappedSourceObject) {
		String naturalKey = wrappedSourceObject.getNaturalKey();
		Long sourceID = wrappedSourceObject.getId();
		O sourceObject = wrappedSourceObject.getObject();
		ReplicationTaskContext<O, S> context = getContext();
		O targetObject = null;
		try {
			if (!context.getSourceObjectMap().containsKey(sourceID)) {
				//The use case here is we found a FK reference while updating a different object and now we are trying to update that fk object. The fk object may be outside of scope but we can still use it to update local fk id
				// check it should always have a reference object in this case but null check is just in case. If the reference Object is of the same type
				if (wrappedSourceObject.getReferenceObject() == null || (!wrappedSourceObject.getReferenceObject().getObject().getClass().isAssignableFrom(getDtoClass()))) {
					context.getStatus().addWarning(naturalKey + " :: an attempt to replicate source object [" + sourceObject + "] failed because object was out of scope.", this.getClass());
					Long targetID = context.getBiDirectionalIdMap().get(sourceID);
					if (targetID != null) {
						// found object in keyMap
						targetObject = getTargetObjectFromService(targetID);

						if (targetObject == null) {
							// the key mapping was bad because the target object doesn't exist.
							deleteKeyMap(sourceID, targetID);
						}
						else {
							context.put(naturalKey, ReplicationService.SKIPPED);
						}
						return targetObject;
					}
					return null;
				}
				LogUtils.debug(this.getClass(), naturalKey + ":: was dependent on " + wrappedSourceObject.getReferenceObject().getObject());
				// continue
			}
			LogUtils.debug(LogCommand.ofMessage(this.getClass(), naturalKey, ":: Replicating [", sourceObject, "];"));
			// Try to find the object using the key map context populate method has assured us source id is the key so we can use get method.
			BaseReplicationObjectWrapper<O> wrappedTargetObject = context.getTargetObjectMap().get(context.getBiDirectionalIdMap().get(sourceID));
			targetObject = wrappedTargetObject == null ? null : wrappedTargetObject.getObject();

			//To avoid an infinite recursion loop we check the status map
			//any object that has been replicated or is currently replicating should appear in the status map and should also be in the Keymap.
			if (context.containsKey(naturalKey) && targetObject != null) {
				//this targetObject has already been replicated in its entirety.
				LogUtils.debug(LogCommand.ofMessage(this.getClass(), naturalKey, ":: sourceObject [", sourceObject, "], already has replication status ", context.get(naturalKey)));
				return targetObject;
			}

			if (targetObject == null) {
				//There were not keyMaps for this sourceObject. This item may not have been replicated before. Try to find a target using natural key.
				wrappedTargetObject = context.getTargetObjectMap().get(context.getTargetNaturalKeyMap().get(naturalKey));
				targetObject = wrappedTargetObject == null ? null : wrappedTargetObject.getObject();
				if (targetObject != null) {
					//We found one but we need to add the relationship to the keymap in both the database and in the context bidiMap
					//We need to check the keyMap for target object entries to avoid duplicate key sql error.
					Long invalidSourceId = context.getBiDirectionalIdMap().getKey(wrappedTargetObject.getId());
					if (invalidSourceId != null) {
						//Could actually update the keymap but this will not be an issue if we pre-validate the keymappings.
						deleteKeyMap(invalidSourceId, wrappedTargetObject.getId());
					}
					insertNewKeyMap(wrappedSourceObject, wrappedTargetObject);
				}
			}
			if (targetObject == null) {
				//If we dont have a targetObject we need to create one and insert it. The insert method updates maps.
				targetObject = insertNewTargetObject(wrappedSourceObject);
				LogUtils.debug(this.getClass(), naturalKey + ":: sourceObject [" + sourceObject + "] was INSERT.");
			}
			else {
				//if we found a targetObject attempt to update it.
				boolean updated = updateTargetObject(wrappedSourceObject, wrappedTargetObject);
				context.put(naturalKey, updated ? ReplicationService.UPDATED : ReplicationService.SKIPPED);
				context.incrementStatusCount(updated ? ReplicationService.UPDATED : ReplicationService.SKIPPED);
			}
		}
		catch (UnsupportedOperationException e) {
			//Simply log a warning if a table was set to implement an action (INSERT/UPDATE/DELETE) that it doesn't support.
			//If a replicator is created for a specific DTO it may throw an unsupportedOperation exception (e.g. delete not supported)
			LogUtils.warn(this.getClass(), ExceptionUtils.getOriginalMessage(e), e);
		}
		catch (Exception e) {
			context.getStatus().addError("[" + getDtoClass().getSimpleName() + "]: " + ExceptionUtils.getDetailedMessage(e));
			context.put(naturalKey, ReplicationService.ERROR);
		}
		return targetObject;
	}


	@Override
	public boolean replicateDeleteObject(BaseReplicationObjectWrapper<O> wrappedTargetObject) {
		ReplicationTaskContext<O, S> context = getContext();
		String naturalKey = wrappedTargetObject.getNaturalKey();
		Long targetID = wrappedTargetObject.getId();
		O targetObject = wrappedTargetObject.getObject();

		// First check that the item we are trying to delete wasn't already deleted or is not in the process of being deleted. This is our recursion check.
		if (ReplicationService.DELETED.equals(context.get(naturalKey)) || ReplicationService.REPLICATING.equals(context.get(naturalKey))) {
			LogUtils.debug(this.getClass(), naturalKey + ":: targetObject [" + targetObject + "] already Deleted.");
			return true;
		}
		//Next make sure the object we are trying to delete is in scope
		if (!context.getTargetObjectMap().containsKey(targetID)) {
			LogUtils.warn(this.getClass(), naturalKey + " :: Attempted Delete of targetObject [" + targetObject + "] Deletion failed Object outside of scope");
			context.getStatus().addWarning(naturalKey + " :: Attempted Delete of targetObject [" + targetObject + "] Deletion failed Object outside of scope", this.getClass());
			return false;
		}
		//Finally begin decision tree for deletion
		//Check if there is a keyMapping
		BaseReplicationObjectWrapper<O> wrappedSourceObject;
		if (context.getBiDirectionalIdMap().containsValue(targetID)) {
			//It was in the keymap but check to make sure keyMapping is not invalid.
			Long sourceID = context.getBiDirectionalIdMap().getKey(targetID);
			wrappedSourceObject = context.getSourceObjectMap().get(sourceID);
			if (wrappedSourceObject == null) {
				//KeyMapping invalid. Remove bad keyMapping
				deleteKeyMap(sourceID, targetID);
				//Check for matching source object by natural key just in case.
				wrappedSourceObject = context.getSourceObjectMap().get(context.getSourceNaturalKeyMap().get(naturalKey));
				if (wrappedSourceObject != null) {
					//SourceObject found; Dont think this should ever happen but we check for it anyway.
					insertNewKeyMap(wrappedSourceObject, wrappedTargetObject);
				}
				else {
					// No source object its safe to try to delete
					return deleteTargetObject(wrappedTargetObject);
				}
			}
			//KeyMapping valid dont delete
		}
		else {
			// No keyMapping but check natural key map to be safe.
			wrappedSourceObject = context.getSourceObjectMap().get(context.getSourceNaturalKeyMap().get(naturalKey));
			if (wrappedSourceObject != null) {
				// In Natural Key map add to keyMapping
				insertNewKeyMap(wrappedSourceObject, wrappedTargetObject);
			}
			else {
				// No natural key map safe to delete.
				return deleteTargetObject(wrappedTargetObject);
			}
		}
		return false;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                  CRUD Methods                          //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * inserts a copy of the sourceObject into the target dataSource using the targetService. Only columns in the in the taskColumnList are copied.
	 * If foreign keys are found the method will locate the responsible <code>Replicator</code> object and call {@link Replicator#replicateObject(BaseReplicationObjectWrapper)}
	 *
	 * @return This object is returned from the targetService so any generated keys are returned as part of the insert process are also returned.
	 */
	@SuppressWarnings("unchecked")
	private O insertNewTargetObject(BaseReplicationObjectWrapper<O> wrappedSourceObject) {
		ReplicationTaskContext<O, S> context = getContext();
		O sourceObject = wrappedSourceObject.getObject();
		String naturalKey = wrappedSourceObject.getNaturalKey();

		// Create a new empty object so we dont replicate any columns not specified.
		O targetObject = (O) BeanUtils.newInstance(sourceObject.getClass());
		//loop through the columns to be replicated.
		for (ReplicationTaskColumn taskColumn : CollectionUtils.getIterable(getContext().getTaskColumnList())) {

			if (columnIsReplicable((SystemColumnStandard) taskColumn.getSourceSystemColumn())) {

				String propertyName = ((SystemColumnStandard) taskColumn.getSourceSystemColumn()).getBeanPropertyName();
				Object propertyValue = BeanUtils.getPropertyValue(sourceObject, propertyName);

				if (propertyValue instanceof IdentityObject) {
					//To avoid an infinite recursion loop we are going to set a 'Replicating' Status
					getContext().put(naturalKey, ReplicationService.REPLICATING);
					LogUtils.debug(LogCommand.ofMessageSupplier(this.getClass(), () -> naturalKey + ":: While Inserting [" + sourceObject + "] a foreign key [" + propertyName + "] object was found"));
					Replicator<O, S> replicator;
					if (getDtoClass().isAssignableFrom(propertyValue.getClass())) {
						replicator = this;
					}
					else {
						//Get a new Replicator for the different class.
						replicator = getReplicator(propertyValue);
					}
					O targetPropertyValue = replicator.replicateObject(wrapObject(replicator.getDtoClass().cast(propertyValue), wrappedSourceObject));
					AssertUtils.assertNotNull(targetPropertyValue, "Failed to find/create target object! [ " + propertyValue + " ] ");
					BeanUtils.setPropertyValue(targetObject, propertyName, targetPropertyValue, true);
				}
				else {
					BeanUtils.setPropertyValue(targetObject, propertyName, propertyValue, true);
				}
			}
		}
		targetObject = replicateInsert(context.getTargetService(), targetObject);
		AssertUtils.assertNotNull(targetObject, naturalKey + " :: Failed to find/create target object! For [ " + sourceObject + " ]");

		//Update our association maps in context
		insertNewKeyMap(wrappedSourceObject, wrapObject(targetObject, null));

		//Update the status maps
		context.put(naturalKey, ReplicationService.INSERTED);
		context.incrementStatusCount(ReplicationService.INSERTED);

		return targetObject;
	}


	/**
	 * Updates the targetObject using the sourceObject's data. Only field values belonging to the context taskColumnList are compared and updated.
	 * If no changes are required no updates are made.
	 */
	private boolean updateTargetObject(BaseReplicationObjectWrapper<O> wrappedSourceObject, BaseReplicationObjectWrapper<O> wrappedTargetObject) {
		boolean updated = false;
		O sourceObject = wrappedSourceObject.getObject();
		String sourceNaturalKey = wrappedSourceObject.getNaturalKey();
		O targetObject = wrappedTargetObject.getObject();
		String targetNaturalKey = wrappedTargetObject.getNaturalKey();

		for (ReplicationTaskColumn taskColumn : CollectionUtils.getIterable(getContext().getTaskColumnList())) {

			//Make sure this is a column that is replicable.
			if (columnIsReplicable((SystemColumnStandard) taskColumn.getSourceSystemColumn())) {

				String propertyName = ((SystemColumnStandard) taskColumn.getSourceSystemColumn()).getBeanPropertyName();
				Object sourcePropertyValue = BeanUtils.getPropertyValue(sourceObject, propertyName);
				Object targetPropertyValue = BeanUtils.getPropertyValue(targetObject, propertyName);

				if (sourcePropertyValue instanceof IdentityObject) {
					//oldTargetPropertyValue is used to for comparing id's to the new targetProperty value after the recursive call.  The recursive replicateObject call handles
					// everything for that object but the id's belong to this parent object and need to be updated here.
					Object oldTargetPropertyValue = BeanUtils.getPropertyValue(targetObject, propertyName);
					LogUtils.trace(this.getClass(), sourceNaturalKey + ":: While Updating with [" + sourceObject + "] a foreign key " + propertyName + " object was found");
					Replicator<O, S> replicator;
					if (getDtoClass().isAssignableFrom(sourcePropertyValue.getClass())) {
						replicator = this;
					}
					else {
						replicator = getReplicator(sourcePropertyValue);
					}
					getContext().put(sourceNaturalKey, ReplicationService.REPLICATING);
					AssertUtils.assertNotNull(replicator.getDtoClass(), "Replicator DTO class is null. Ensure task table has been added for [%s].", sourcePropertyValue.getClass().getSimpleName());
					targetPropertyValue = replicator.replicateObject(wrapObject(replicator.getDtoClass().cast(sourcePropertyValue), wrappedSourceObject));
					AssertUtils.assertNotNull(targetPropertyValue, "Failed to find/create target object! [ " + sourcePropertyValue + " ]");
					//this check is for the case where current parent object used to have fk link to one object but now has a fk link to a different object.
					if (!replicator.getObjectIdentity(replicator.getDtoClass().cast(targetPropertyValue)).equals(replicator.getObjectIdentity(replicator.getDtoClass().cast(oldTargetPropertyValue)))) {
						LogUtils.trace(LogCommand.ofMessage(this.getClass(), "Updating [", targetObject, "]; column [", propertyName, "] was [", oldTargetPropertyValue, "] setting to [", targetPropertyValue, "]"));
						BeanUtils.setPropertyValue(targetObject, propertyName, targetPropertyValue, true);
						updated = true;
					}
					// The id are paired in the KeyMapping and no update is needed
				}
				else {
					if (!Objects.equals(sourcePropertyValue, targetPropertyValue)) {
						LogUtils.trace(LogCommand.ofMessage(this.getClass(), "Updating [", targetObject, "]; column [", propertyName, "] was [", targetPropertyValue, "] setting to [", sourcePropertyValue, "]"));
						BeanUtils.setPropertyValue(targetObject, propertyName, sourcePropertyValue, true);
						updated = true;
					}
				}
			}
		}
		if (updated) {
			targetObject = replicateUpdate(getContext().getTargetService(), targetObject);
			AssertUtils.assertNotNull(targetObject, "Failed to find/create target object! [ " + sourceObject + " ]");
			LogUtils.debug(this.getClass(), targetNaturalKey + " :: targetObject [" + targetObject + "] required UPDATE to sourceObject [" + sourceObject + "]");
		}
		else {
			LogUtils.debug(this.getClass(), targetNaturalKey + ":: targetObject [" + targetObject + "] SKIPPED");
		}
		return updated;
	}


	/**
	 * Deletes the target object using the target service. This method does not make recursive calls for foreign keys. Rather attempts to delete the item and forwards the database
	 * error if it fails. This means the preview only logic cannot detect any potential delete issues.
	 */
	private boolean deleteTargetObject(BaseReplicationObjectWrapper<O> wrappedTargetObject) {
		ReplicationTaskContext<O, S> context = getContext();
		O targetObject = wrappedTargetObject.getObject();
		String naturalKey = wrappedTargetObject.getNaturalKey();
		boolean delete = true;
		try {
			//TODO attempt to find objects that reference this object. Select delete to false if condition is found
			if (delete) {

				replicateDelete(context.getTargetService(), targetObject);
				context.getTargetNaturalKeyMap().remove(naturalKey);

				LogUtils.debug(this.getClass(), naturalKey + ":: targetObject [" + targetObject + "] was DELETE.");
				context.put(naturalKey, ReplicationService.DELETED);
				context.incrementStatusCount(ReplicationService.DELETED);
			}
		}
		catch (UnsupportedOperationException e) {
			//Simply log a warning if a table was set to implement an action (INSERT/UPDATE/DELETE) that it doesn't support.
			//If a replicator is created for a specific DTO it may throw an unsupportedOperation exception (e.g. delete not supported)
			LogUtils.warn(this.getClass(), ExceptionUtils.getOriginalMessage(e), e);
			context.put(naturalKey, ReplicationService.ERROR);
			delete = false;
		}
		catch (Exception e) {
			context.getStatus().addError("[" + getDtoClass().getSimpleName() + "]: " + ExceptionUtils.getDetailedMessage(e));
			context.put(naturalKey, ReplicationService.ERROR);
			delete = false;
		}
		return delete;
	}


	private void insertNewKeyMap(BaseReplicationObjectWrapper<O> wrappedSourceObject, BaseReplicationObjectWrapper<O> wrappedTargetObject) {
		ReplicationTaskContext<O, S> context = getContext();
		if (!context.getTargetObjectMap().containsKey(wrappedTargetObject.getId())) {
			context.getTargetObjectMap().put(wrappedTargetObject.getId(), wrappedTargetObject);
		}
		if (!context.getTargetNaturalKeyMap().containsKey(wrappedTargetObject.getNaturalKey())) {
			context.getTargetNaturalKeyMap().put(wrappedTargetObject.getNaturalKey(), wrappedTargetObject.getId());
		}
		if (context.getBiDirectionalIdMap().get(wrappedSourceObject.getId()) == null && context.getBiDirectionalIdMap().getKey(wrappedTargetObject.getId()) == null) {
			context.getBiDirectionalIdMap().put(wrappedSourceObject.getId(), wrappedTargetObject.getId());
		}
		ReplicationKeyMapping newKeyMapping = context.generateNewKeyMappingFor(wrappedSourceObject, wrappedTargetObject);
		if (!context.isPreviewOnly()) {
			newKeyMapping = getReplicationKeyMappingService().saveReplicationKeyMapping(newKeyMapping);
		}
		context.addNewKeyMappingToContext(newKeyMapping);
		context.getStatus().incrementStatusCount("KeyMappingsCreated");
	}


	private void deleteKeyMap(Long sourceID, Long targetID) {
		ReplicationTaskContext<O, S> context = getContext();
		ReplicationKeyMapping badKeyMapping = context.getReplicationKeyMappingBySourceAndTargetID(sourceID, targetID);
		AssertUtils.assertNotNull(badKeyMapping, "Expected keymapping for " + sourceID + ", " + targetID + " but found none. ");
		if (!context.isPreviewOnly()) {
			getReplicationKeyMappingService().deleteReplicationKeyMapping(badKeyMapping.getId());
		}
		context.removeKeyMappingFromContext(badKeyMapping);
		context.getStatus().incrementStatusCount("KeyMappingsDeleted");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @return false if the column is an id column or is one of the system columns defined by {@link BeanUtils#AUDIT_PROPERTIES}
	 */
	private boolean columnIsReplicable(SystemColumnStandard column) {
		return !("id").equals(column.getBeanPropertyName()) && !ArrayUtils.contains(BeanUtils.AUDIT_PROPERTIES, column.getBeanPropertyName());
	}


	@Override
	public O getTargetObjectFromService(Serializable id) {
		return getObject(getContext().getTargetService(), id);
	}


	public Object executeFieldValueQuery(String sql) {
		return getSqlHandler().executeSelect(sql, rs -> {
			List<Object> result = new ArrayList<>();
			while (rs.next()) {
				result.add(rs.getObject("Value"));
			}
			return result;
		});
	}


	@SuppressWarnings("unchecked")
	private Replicator<O, S> getReplicator(Object sourceObject) {
		Replicator<O, S> replicator = getReplicatorLocator().locate((Class<O>) sourceObject.getClass());
		return replicator;
	}


	@Override
	public Long getObjectIdentity(IdentityObject object) {
		if (object != null) {
			return ((Number) object.getIdentity()).longValue();
		}
		return null;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////     Default Abstract Method Implementations     ////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public Class<O> getDtoClass() {
		if (this.dtoClass == null) {
			this.dtoClass = (Class<O>) getDaoLocator().locate(getTaskTable().getSourceSystemTable().getName()).getConfiguration().getBeanClass();
		}
		return this.dtoClass;
	}


	@Override
	@SuppressWarnings("unchecked")
	public Class<S> getDtoServiceClass() {
		if (this.dtoServiceClass == null) {
			this.dtoServiceClass = (Class<S>) getDaoLocator().locate(getTaskTable().getSourceSystemTable().getName()).getConfiguration().getBeanServiceClass();
		}
		return this.dtoServiceClass;
	}


	@Override
	public String getNaturalKey(O object) {
		return getNaturalKey(object, false);
	}


	@SuppressWarnings("unchecked")
	private String getNaturalKey(O object, boolean recursive) {

		StringBuilder key = new StringBuilder();
		if (object != null) {
			ReadOnlyDAO<O> dao = getDaoLocator().locate(object);
			Set<String> naturalKeys = dao.getConfiguration().getNaturalKeys(getDaoLocator());
			if (naturalKeys == null) {
				throw new ValidationException("Unable to replicate; object [" + object.getClass() + "] has no natural keys defined!");
			}
			else {
				for (String naturalKey : naturalKeys) {
					if (key.length() > 0) {
						key.append(MapUtils.KEY_SEPARATOR);
					}
					if (naturalKey.startsWith("parent.")) {
						Method getParentMethod = MethodUtils.getMethod(object.getClass(), "getParent");
						key.append(getNaturalKey((O) MethodUtils.invoke(getParentMethod, object), true));
					}
					else {
						key.append(BeanUtils.getPropertyValue(object, naturalKey));
					}
				}
			}
			return recursive ? key.toString().toUpperCase() : object.getClass().getSimpleName().toUpperCase() + MapUtils.KEY_SEPARATOR + key.toString().toUpperCase();
		}
		return "";
	}


	protected String unwrap(Object object) {
		if (Proxy.class.isAssignableFrom(object.getClass())) {
			return ((Proxy) object).getClass().getInterfaces()[0].getSimpleName();
		}
		return object.getClass().getSimpleName();
	}


	public ReplicationTaskContext<O, S> getContext() {
		return this.lazyContext.get();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ReplicationTask getTask() {
		return getTaskTable().getTask();
	}


	public ReplicationTaskTable getTaskTable() {
		return this.taskTable;
	}


	public void setTaskTable(ReplicationTaskTable taskTable) {
		this.taskTable = taskTable;
	}


	public Map<String, Class<?>> getSearchFormClassesMap() {
		return this.searchFormClassesMap;
	}


	public void setSearchFormClassesMap(Map<String, Class<?>> searchFormClassesMap) {
		this.searchFormClassesMap = searchFormClassesMap;
	}


	public ReplicationTaskService getReplicationTaskService() {
		return this.replicationTaskService;
	}


	public void setReplicationTaskService(ReplicationTaskService replicationTaskService) {
		this.replicationTaskService = replicationTaskService;
	}


	public SystemColumnService getSystemColumnService() {
		return this.systemColumnService;
	}


	public void setSystemColumnService(SystemColumnService systemColumnService) {
		this.systemColumnService = systemColumnService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ReplicatorLocator<O, S> getReplicatorLocator() {
		return this.replicatorLocator;
	}


	public void setReplicatorLocator(ReplicatorLocator<O, S> replicatorLocator) {
		this.replicatorLocator = replicatorLocator;
	}


	public ExternalServiceLocator getExternalServiceLocator() {
		return this.externalServiceLocator;
	}


	public void setExternalServiceLocator(ExternalServiceLocator externalServiceLocator) {
		this.externalServiceLocator = externalServiceLocator;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public ReplicationKeyMappingService getReplicationKeyMappingService() {
		return this.replicationKeyMappingService;
	}


	public void setReplicationKeyMappingService(ReplicationKeyMappingService replicationKeyMappingService) {
		this.replicationKeyMappingService = replicationKeyMappingService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}
}
