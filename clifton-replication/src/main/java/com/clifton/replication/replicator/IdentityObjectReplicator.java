package com.clifton.replication.replicator;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.replication.ReplicationService;
import com.clifton.replication.task.ReplicationTaskTableFilter;
import com.clifton.replication.task.context.ReplicationTaskContext;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.schema.SystemDataSource;

import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <code>IdentityObjectReplicator</code> is the base concrete implementation of a replicator that should
 * handle the majority of replications on its own.  If a DTO has methods that don't conform to our standards
 * or are in some way unique due to child objects, or many to many objects, then the service classes for those
 * objects should either be updated to conform; or a new Replicator can be created that extends this one and
 * overrides methods as needed.
 *
 * @author stevenf
 */
public class IdentityObjectReplicator extends BaseReplicator<IdentityObject, Object> {

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	@Override
	public IdentityObject getObject(Object service, Serializable id) {
		Method method = getMethod(getDtoServiceClass(), "get" + getDtoClass().getSimpleName());
		AssertUtils.assertTrue(method.getParameterTypes().length == 1, "Replication supports 'get' methods with 1 parameter only!");
		return (IdentityObject) MethodUtils.invoke(method, service, convertIdentityToParameterType(id, method.getParameterTypes()[0]));
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<IdentityObject> getObjectList(ReplicationTaskContext<IdentityObject, Object> context, boolean source) {
		Class<?> searchFormClass = getSearchFormClassesMap().get(getDtoClass().getSimpleName() + "SearchForm");
		AssertUtils.assertNotNull(searchFormClass, "Unable to locate searchForm with name '" + getDtoClass().getSimpleName() + "SearchForm'");
		//Since in this case we can infer the parameterType we pass it so we get the exact method we need;
		// i.e. if we don't pass, then it is possible to get multiple methods returned, e.g. 'getSecurityUserList' vs. 'getSecurityUserList(SecurityUserSearchForm searchForm)'
		Method method = getMethod(getDtoServiceClass(), "get" + getDtoClass().getSimpleName() + "List", searchFormClass);
		SystemDataSource systemDataSource = source ? context.getSourceDataSource() : context.getTargetDataSource();
		Object service = source ? context.getSourceService() : context.getTargetService();
		boolean isTargetDataSource = getTask().getTargetDataSource().equals(systemDataSource);
		Object searchForm = ClassUtils.newInstance(searchFormClass);

		for (ReplicationTaskTableFilter searchFormFilter : CollectionUtils.getIterable(getTaskTable().getFilterList())) {
			if (searchFormFilter.isPreFilter()) {
				if (searchFormFilter.isTargetDataSourceFilter() == isTargetDataSource) {
					Object fieldValue = searchFormFilter.getBeanFieldValueQuery() != null ? executeFieldValueQuery(searchFormFilter.getBeanFieldValueQuery()) : searchFormFilter.getBeanFieldValue();
					AssertUtils.assertNotNull(fieldValue, "While trying to retrieve " + (source ? "source objects from " : "target objects from ") + method.getDeclaringClass() + "." + method.getName() + " : " + searchFormFilter.getBeanFieldName() + " Cannot be null");
					if (fieldValue instanceof String) {
						fieldValue = ((String) fieldValue).split(",");
					}
					if (fieldValue instanceof Collection<?>) {
						AssertUtils.assertNotEmpty((Collection<?>) fieldValue, "While trying to retrieve " + (source ? "source objects from " : "target objects from ") + method.getDeclaringClass() + "." + method.getName() + " : " + searchFormFilter.getBeanFieldName() + " collection can not be empty!");
					}
					BeanUtils.setPropertyValue(searchForm, searchFormFilter.getBeanFieldName(), fieldValue);
				}
			}
		}
		List<IdentityObject> objectList = (List<IdentityObject>) MethodUtils.invoke(method, service, searchForm);

		for (ReplicationTaskTableFilter searchFormFilter : CollectionUtils.getIterable(getTaskTable().getFilterList())) {
			if (!searchFormFilter.isPreFilter()) {
				if (searchFormFilter.isTargetDataSourceFilter() == isTargetDataSource) {
					Object fieldValue = searchFormFilter.getBeanFieldValueQuery() != null ? executeFieldValueQuery(searchFormFilter.getBeanFieldValueQuery()) : searchFormFilter.getBeanFieldValue();
					if (fieldValue instanceof String) {
						fieldValue = ((String) fieldValue).split(",");
					}
					if (fieldValue instanceof ArrayList<?>) {
						String beanFieldName = searchFormFilter.getBeanFieldName().substring(0, searchFormFilter.getBeanFieldName().lastIndexOf("s"));
						objectList = BeanUtils.filterByPropertyName(objectList, beanFieldName, ((ArrayList<String>) fieldValue).toArray(new Object[0]), false, true);
					}
					else {
						objectList = BeanUtils.filter(objectList, o -> BeanUtils.getPropertyValue(o, searchFormFilter.getBeanFieldName()), fieldValue);
					}
				}
			}
		}
		if (!CollectionUtils.isEmpty(objectList) && getTaskTable().getExclusionCondition() != null) {
			objectList.removeIf(identityObject -> getSystemConditionEvaluationHandler().evaluateCondition(getTaskTable().getExclusionCondition(), identityObject).isResult());
		}

		if (CollectionUtils.isEmpty(objectList)) {
			context.getStatus().addWarning("After applying filters to " + context.getTaskTable().getSourceSystemTable().getName() + " on the " + (source ? "source" : "target") + " system, no replicable objects were found", this.getClass());
		}
		return objectList;
	}


	@Override
	public IdentityObject replicateInsert(Object targetService, IdentityObject targetObject) {
		Method method = getMethod(getDtoServiceClass(), "save" + getDtoClass().getSimpleName());
		if (!getContext().isPreviewOnly()) {
			LogUtils.trace(getClass(), "Invoking method [" + method.getName() + "] on target service [" + unwrap(targetService) + "] for object [" + targetObject.getClass() + ":" + targetObject.getIdentity() + "]");
			return (IdentityObject) MethodUtils.invoke(method, targetService, targetObject);
		}
		LogUtils.trace(getClass(), "METHOD NOT INVOKED! To update delete or insert simulate must be disabled");
		Number fakeID = (getContext().getStatus().getStatusCount(ReplicationService.INSERTED) + 1) * -1;
		BeanUtils.setPropertyValue(targetObject, "id", fakeID);
		return targetObject;
	}


	@Override
	public IdentityObject replicateUpdate(Object targetService, IdentityObject targetObject) {
		return replicateInsert(targetService, targetObject);
	}


	@Override
	public void replicateDelete(Object targetService, IdentityObject targetObject) {
		Method method = getMethod(getDtoServiceClass(), "delete" + getDtoClass().getSimpleName());
		AssertUtils.assertTrue(method.getParameterTypes().length == 1, "Replication supports 'delete' methods with 1 parameter only!");
		if (!getContext().isPreviewOnly()) {
			LogUtils.trace(getClass(), "Invoking method [" + method.getName() + "] on target service [" + unwrap(targetService) + "] for object [" + targetObject.getClass() + ":" + targetObject.getIdentity() + "]");
			MethodUtils.invoke(method, targetService, convertIdentityToParameterType(targetObject.getIdentity(), method.getParameterTypes()[0]));
		}
		else {
			LogUtils.trace(getClass(), "METHOD NOT INVOKED! To update delete or insert simulate must be disabled");
		}
	}


	private Method getMethod(Class<?> serviceClass, String methodName, Class<?>... parameterTypes) {
		Method method;
		if (ArrayUtils.isEmpty(parameterTypes)) {
			method = MethodUtils.getMethod(serviceClass, methodName, true);
		}
		else {
			method = MethodUtils.getMethod(serviceClass, methodName, parameterTypes);
		}
		AssertUtils.assertNotNull(method, "Unable to retrieve method [" + methodName + "] from class [" + serviceClass + "]");
		return method;
	}


	private Object convertIdentityToParameterType(Serializable id, Class<?> parameterTypeClass) {
		if (id == null) {
			return id;
		}
		try {
			// Short Values
			if (Short.class.equals(parameterTypeClass) || short.class.equals(parameterTypeClass)) {
				return ((Number) id).shortValue();
			}
			// Integer Values
			if (Integer.class.equals(parameterTypeClass) || int.class.equals(parameterTypeClass)) {
				return ((Number) id).intValue();
			}
			// Double Values
			if (Double.class.equals(parameterTypeClass) || double.class.equals(parameterTypeClass)) {
				return ((Number) id).doubleValue();
			}
			// Long Values
			if (Long.class.equals(parameterTypeClass) || long.class.equals(parameterTypeClass)) {
				return ((Number) id).longValue();
			}
			if (String.class.equals(parameterTypeClass)) {
				return id;
			}
			throw new RuntimeException("Unsupported parameter type [" + parameterTypeClass.getName() + "].");
		}
		catch (Exception e) {
			throw new RuntimeException("Error converting value [" + id + "] to type [" + parameterTypeClass.getName() + "]", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                Getters And Setters                     //////////
	////////////////////////////////////////////////////////////////////////////


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
