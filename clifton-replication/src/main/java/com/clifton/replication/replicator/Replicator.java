package com.clifton.replication.replicator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.status.Status;
import com.clifton.replication.BaseReplicationObjectWrapper;
import com.clifton.replication.jobs.ReplicationJob;
import com.clifton.replication.task.ReplicationTaskTable;
import com.clifton.replication.task.context.ReplicationTaskContext;

import java.io.Serializable;
import java.util.List;
import java.util.Map;


/**
 * The <code>Replicator</code> interface is implemented by all classes the provide the implementation
 * details of how their given DTO object should be replicated.
 *
 * @param <O> - The DTO Object that the given Replicator provides implementation details for
 * @param <S> - The Service used for CRUD operations for the specified DTO Object
 * @author StevenF
 */
public interface Replicator<O extends IdentityObject, S> {

	/**
	 * Lazily initializes the context for this replicator.
	 * This will retrieve the source and object lists based on the task and task table configuration, locates
	 * dto services for source and target systems, and populates all relevant maps used in replication
	 */
	public void clearContext(ReplicationTaskTable taskTable, Map<String, Class<?>> searchFormClassesMap);


	/**
	 * This method is the initial entry point of a replication task; it is called from the {@link ReplicationJob}
	 * Once replication has begun, additional replicators can be used if a foreign key is encountered in the current table.
	 * At that point the replicator associated with that foreign key's table will be lazily initialized and executed.
	 */
	public Status doReplication();


	/**
	 * This method is the initial entry point of a replication delete operation.
	 */
	public Status doDelete();


	/**
	 * Replicates the given object from the source to target
	 */
	public O replicateObject(BaseReplicationObjectWrapper<O> wrappedSourceObject);


	/**
	 * Decides weather or not to delete the targetObject
	 */
	public boolean replicateDeleteObject(BaseReplicationObjectWrapper<O> wrappedTargetObject);


	/**
	 * Convenience method that allows an id only to be sent, the method then gets the target service for the given DTO
	 * and calls 'getObject(S service, Serializable id)'
	 */
	public O getTargetObjectFromService(Serializable id);


	/**
	 * Implementation for retrieving a DTO object, typically from the target system.
	 * It infers the get method by applying our naming conventions: 'get<DTOClass_SimpleName>(id)'
	 */
	public O getObject(S service, Serializable id);


	/**
	 * Retrieves the list of objects to be replicated from the specified dataSource;
	 * For the source system, search for
	 */
	public List<O> getObjectList(ReplicationTaskContext<O, S> context, boolean source);


	/**
	 * Executes an insert for replication
	 */
	public O replicateInsert(S targetService, O targetObject);


	/**
	 * Executes an update for replication - in most cases this simply calls the insert method with an already fully populated DTO
	 */
	public O replicateUpdate(S targetService, O targetObject);


	/**
	 * Executes a delete for replication - this will traverse foreign keys and delete referencing entities when the foreign key is required;
	 * or will null the values when the key is not.
	 */
	public void replicateDelete(S targetService, O targetObject);


	/**
	 * Returns the concrete class for the given DTO being replicated.
	 */
	public Class<O> getDtoClass();


	/**
	 * Returns the concrete service class user for CRUD operations for the given DTO
	 */
	public Class<S> getDtoServiceClass();


	/**
	 * Returns the natural key for the given DTO
	 */
	public String getNaturalKey(O object);


	/**
	 * Returns a Long version of the objects identity using the {@link IdentityObject#getIdentity()} method and parsing accordingly
	 */
	public Long getObjectIdentity(O object);
}
