Clifton.replication.task.ReplicationTaskWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Replication Task',
	iconCls: 'list',
	width: 1000,
	height: 550,

	items: [{
		xtype: 'formpanel',
		instructions: 'A Replication Task defines the source and target systems of a replication with associated table and field configs.',
		url: 'replicationTask.json?requestedMaxDepth=4',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Task Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 60},
			{fieldLabel: 'Source Data Source', name: 'sourceDataSource.databaseName', hiddenName: 'sourceDataSource.id', displayField: 'databaseName', xtype: 'combo', url: 'systemDataSourceList.json', loadAll: true, allowBlank: false},
			{fieldLabel: 'Target Data Source', name: 'targetDataSource.databaseName', hiddenName: 'targetDataSource.id', displayField: 'databaseName', xtype: 'combo', url: 'systemDataSourceList.json', loadAll: true, allowBlank: false},
			{
				xtype: 'gridpanel',
				//Disable by default - enable once ReplicationTask has been applied/saved. (see 'afterLoad' method)
				disabled: true,
				loadValidation: false,
				title: 'Tables to Replicate',
				name: 'replicationTaskTableListFind',
				instructions: 'A Replication Task Table defines how a table should be replicated to an external application.',
				frame: true,
				height: 275,
				heightResized: true,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Table Name', width: 70, dataIndex: 'sourceSystemTable.name'},
					{header: 'Exclusion Condition', width: 100, dataIndex: 'exclusionCondition.name'},
					{header: 'Inserts', width: 25, dataIndex: 'replicateInserts', type: 'boolean'},
					{header: 'Updates', width: 25, dataIndex: 'replicateUpdates', type: 'boolean'},
					{header: 'Deletes', width: 25, dataIndex: 'replicateDeletes', type: 'boolean'},
					{header: 'Disabled', width: 25, dataIndex: 'disabled', type: 'boolean'},
					{header: 'Order', width: 25, dataIndex: 'processingOrder', type: 'int'}
				],
				getLoadParams: function(firstLoad) {
					if (TCG.isNotBlank(this.getWindow().getMainFormId())) {
						return {
							taskId: this.getWindow().getMainFormId(),
							requestedMaxDepth: 3
						};
					}
					return false;
				},
				editor: {
					detailPageClass: 'Clifton.replication.task.ReplicationTaskTableWindow',
					getDefaultData: function(gridPanel) {
						return {
							task: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			}
		],
		listeners: {
			afterrender: function() {
				const formPanel = this;
				formPanel.enableFields();
			},
			afterload: function(formPanel, closeOnSuccess) {
				// Skip UI modifications if window is about to be closed
				if (!closeOnSuccess) {
					formPanel.enableFields();
				}
			}
		},
		enableFields: function() {
			const formPanel = this;
			const hasName = !!formPanel.getFormValue('name');
			const hasSourceDataSource = !!formPanel.getFormValue('sourceDataSource.id');
			const hasTargetDataSource = !!formPanel.getFormValue('targetDataSource.id');
			if (!!formPanel.getIdFieldValue() || (hasName && hasSourceDataSource && hasTargetDataSource)) {
				formPanel.findByType('gridpanel')[0].enable();
			}
		}
	}]
});
