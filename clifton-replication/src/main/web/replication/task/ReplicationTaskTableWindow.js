Clifton.replication.task.ReplicationTaskTableWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Replication Task Table',
	iconCls: 'grid',
	width: 1000,
	height: 730,

	items: [{
		xtype: 'formpanel',
		instructions: 'A Replication Task Table defines how a table should be replicated to an external application.<br /><b>Note: Replicating Deletes is not possible when pre-filtering the object list.</b>',
		url: 'replicationTaskTable.json?requestedMaxDepth=4',
		labelFieldName: 'sourceSystemTable.name',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Task Name', name: 'task.name', detailIdField: 'task.id', xtype: 'linkfield', detailPageClass: 'Clifton.replication.task.ReplicationTaskWindow'},
			{
				fieldLabel: 'Source Table Name', name: 'sourceSystemTable.name', hiddenName: 'sourceSystemTable.id', displayField: 'name', xtype: 'combo', url: 'systemTableListFind.json', allowBlank: false,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.baseParams = {
							dataSourceId: TCG.getParentFormPanel(combo).getFormValue('task.sourceDataSource.id')
						};
					}
				}
			},
			{
				fieldLabel: 'Exclusion Condition', name: 'exclusionCondition.name', hiddenName: 'exclusionCondition.id', allowBlank: true, xtype: 'system-condition-combo', url: 'DYNAMICALLY_REPLACE_ME',
				qtip: 'Exclusion Conditions exclude any data from replication that matches the condition. Exclusions are applied to both target and source dataset\'s and as such, system unique comparisons like ID fields should NOT be used.',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						const url = 'systemConditionListFind.json?optionalSystemTableName=' + fp.getFormValue('sourceSystemTable.name');
						if (url) {
							combo.setUrl(url);
						}

					}
				}
			},
			{fieldLabel: 'Processing Order', name: 'processingOrder', xtype: 'integerfield', minValue: 0, qtip: 'Enter a positive value. This determines the order in which the tables will be processed. If tables contain foreign key references, those items will be processed as needed.'},
			{
				fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 4, qtip: 'The configuration flags determine how this definition field is used; each configuration options has a more detailed description of its specific use.',
				items: [
					{boxLabel: 'Replicate Inserts', name: 'replicateInserts', xtype: 'checkbox', qtip: 'Specifies if this table should replicate inserts.'},
					{boxLabel: 'Replicate Updates', name: 'replicateUpdates', xtype: 'checkbox', qtip: 'Specifies if this table should replicate updates.'},
					{boxLabel: 'Replicate Deletes', name: 'replicateDeletes', xtype: 'checkbox', qtip: 'Specifies if this table should replicate deletes.'}
				]
			},
			{fieldLabel: 'Disabled', boxLabel: ' (check if you do not want to replicate the data for this table)', name: 'disabled', xtype: 'checkbox'},

			{
				xtype: 'gridpanel',
				//Disable by default - enable once ReplicationTaskTable has been applied/saved. (see 'afterLoad' method)
				disabled: true,
				loadValidation: false,
				title: 'Object List Filters',
				name: 'replicationTaskTableFilterListFind',
				instructions: 'Defines fields and values to use when retrieving the initial object list to be replicated. Multiple filters are applied using AND clause.',
				frame: true,
				height: 200,
				heightResized: true,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Bean Field Name', width: 50, dataIndex: 'beanFieldName'},
					{header: 'Bean Field Value', width: 100, dataIndex: 'beanFieldValue'},
					{header: 'Bean Field Value Query', width: 150, dataIndex: 'beanFieldValueQuery'},
					{header: 'Target Filter', width: 35, dataIndex: 'targetDataSourceFilter', type: 'boolean', tooltip: 'Determines if this filter should be applied to the source data or the target data. Cannot be used with Bean field query because it is only possible to query IMS.'},
					{header: 'Pre-Filter', width: 30, dataIndex: 'preFilter', type: 'boolean', tooltip: 'Determines if this replication filter should be applied before(adds value to SearchForm) or after(using BeanUtils) the object list is retrieved.'}
				],
				getLoadParams: function(firstLoad) {
					if (TCG.isNotBlank(this.getWindow().getMainFormId())) {
						return {
							taskTableId: this.getWindow().getMainFormId(),
							requestedMaxDepth: 3
						};
					}
					return false;
				},
				editor: {
					detailPageClass: 'Clifton.replication.task.ReplicationTaskTableFilterWindow',
					getDefaultData: function(gridPanel) {
						return {
							taskTable: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			},

			{
				xtype: 'gridpanel',
				//Disable by default - enable once ReplicationTaskTable has been applied/saved. (see 'afterLoad' method)
				disabled: true,
				loadValidation: false,
				title: 'Replication Task Column',
				name: 'replicationTaskColumnListFind',
				instructions: 'Defines what columns should be replicated from the table; if no columns are defined all columns with the exception of system managed columns are replicated (i.e. id, createDate, updateDate, etc).',
				frame: true,
				height: 250,
				heightResized: true,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Column Name', width: 100, dataIndex: 'sourceSystemColumn.name'},
					{header: 'Column Description', width: 300, dataIndex: 'sourceSystemColumn.description'}
				],
				getLoadParams: function(firstLoad) {
					if (TCG.isNotBlank(this.getWindow().getMainFormId())) {
						return {
							taskTableId: this.getWindow().getMainFormId(),
							requestedMaxDepth: 3
						};
					}
					return false;
				},
				editor: {
					detailPageClass: 'Clifton.replication.task.ReplicationTaskColumnWindow',
					getDefaultData: function(gridPanel) {
						return {
							taskTable: gridPanel.getWindow().getMainForm().formValues
						};
					}
				}
			}
		],
		listeners: {
			afterrender: function() {
				const formPanel = this;
				formPanel.enableFields();

			},
			afterload: function(formPanel, closeOnSuccess) {
				// Skip UI modifications if window is about to be closed
				if (!closeOnSuccess) {
					formPanel.enableFields();
				}

			}
		},
		enableFields: function() {
			const formPanel = this;
			const hasName = !!formPanel.getFormValue('sourceSystemTable.name');
			if (!!formPanel.getIdFieldValue() || (hasName)) {
				formPanel.findByType('gridpanel')[0].enable();
				formPanel.findByType('gridpanel')[1].enable();
			}
		}
	}]
});
