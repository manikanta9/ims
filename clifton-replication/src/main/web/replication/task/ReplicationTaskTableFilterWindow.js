Clifton.replication.task.ReplicationTaskTableFilterWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Replication Table Filter',
	iconCls: 'filter',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'formpanel',
		instructions: 'Defines a Search Form bean field and value to use in order to limit rows being replicated.  Either static value or SQL query can be used.',
		url: 'replicationTaskTableFilter.json?requestedMaxDepth=4',
		labelFieldName: 'taskTable.sourceSystemTable.name',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Task Name', name: 'taskTable.task.name', detailIdField: 'taskTable.task.id', xtype: 'linkfield', detailPageClass: 'Clifton.replication.task.ReplicationTaskWindow'},
			{fieldLabel: 'Table Name', name: 'taskTable.sourceSystemTable.name', detailIdField: 'taskTable.id', xtype: 'linkfield', detailPageClass: 'Clifton.replication.task.ReplicationTaskTableWindow'},
			{boxLabel: 'Is Target Data Source Filter', name: 'targetDataSourceFilter', xtype: 'checkbox', mutuallyExclusiveFields: ['beanFieldValueQuery'], qtip: 'Determines if this filter should be applied to the source data or the target data. Cannot be used with Bean field query because it is only possible to query IMS.'},
			{boxLabel: 'Pre-Filter', name: 'preFilter', xtype: 'checkbox', qtip: 'Determines if this replication filter should be applied before(adds value to SearchForm) or after(using BeanUtils) the object list is retrieved.'},
			{fieldLabel: 'Bean Field Name', name: 'beanFieldName'},
			{fieldLabel: 'Bean Field Value', name: 'beanFieldValue', mutuallyExclusiveFields: ['beanFieldValueQuery']},
			{fieldLabel: 'Bean Field Value Query', name: 'beanFieldValueQuery', xtype: 'textarea', anchor: '-35 -180', mutuallyExclusiveFields: ['beanFieldValue', 'targetDataSourceFilter'], qtip: 'It is only possible to query IMS using this field. Returned values are used as the "Bean Field Value" value.'}
		]
	}]
});
