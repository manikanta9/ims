Clifton.replication.task.ReplicationTaskColumnWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Data Replication Task Column',
	iconCls: 'grid',
	width: 750,
	height: 350,

	items: [{
		xtype: 'formpanel',
		instructions: 'Specifies the database column (field) that should be replicated from the source to the target data source.',
		url: 'replicationTaskColumn.json?requestedMaxDepth=4',
		labelFieldName: 'sourceSystemColumn.name',
		labelWidth: 140,
		items: [
			{fieldLabel: 'Task Name', name: 'taskTable.task.name', detailIdField: 'taskTable.task.id', xtype: 'linkfield', detailPageClass: 'Clifton.replication.task.ReplicationTaskWindow'},
			{fieldLabel: 'Table Name', name: 'taskTable.sourceSystemTable.name', detailIdField: 'taskTable.id', xtype: 'linkfield', detailPageClass: 'Clifton.replication.task.ReplicationTaskTableWindow'},
			{
				fieldLabel: 'Column Name', name: 'sourceSystemColumn.name', hiddenName: 'sourceSystemColumn.id', displayField: 'name', xtype: 'combo', url: 'systemColumnListFind.json', allowBlank: false,
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						combo.store.baseParams = {
							tableId: TCG.getParentFormPanel(combo).getFormValue('taskTable.sourceSystemTable.id')
						};
					}
				}
			}
		]
	}]
});
