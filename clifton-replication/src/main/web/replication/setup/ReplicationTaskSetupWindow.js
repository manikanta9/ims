Clifton.replication.setup.ReplicationTaskSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'replicationTaskSetupWindow',
	title: 'Data Replication Tasks Setup',
	iconCls: 'replicate',
	width: 1400,
	height: 600,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Tasks',
				items: [{
					name: 'replicationTaskListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all replication tasks in the system that are used to replicate data between systems.',
					wikiPage: 'IT/Replication+Documentation',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Name', width: 100, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Source Data Source', width: 50, dataIndex: 'sourceDataSource.databaseName'},
						{header: 'Target Data Source', width: 50, dataIndex: 'targetDataSource.databaseName'}
					],
					editor: {
						detailPageClass: 'Clifton.replication.task.ReplicationTaskWindow'
					}
				}]
			},


			{
				title: 'Task Tables',
				items: [{
					name: 'replicationTaskTableListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all tables across all tasks that are replicated between systems. See Replication Task details for task specific information.',
					wikiPage: 'IT/Replication+Documentation',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Name', width: 120, dataIndex: 'task.name'},
						{header: 'Table Name', width: 70, dataIndex: 'sourceSystemTable.name'},
						{header: 'Exclusion Condition', width: 120, dataIndex: 'exclusionCondition.name'},
						{header: 'Inserts', width: 25, dataIndex: 'replicateInserts', type: 'boolean'},
						{header: 'Updates', width: 25, dataIndex: 'replicateUpdates', type: 'boolean'},
						{header: 'Deletes', width: 25, dataIndex: 'replicateDeletes', type: 'boolean'},
						{header: 'PreFilter', width: 25, dataIndex: 'preFilter', type: 'boolean'},
						{header: 'Disabled', width: 25, dataIndex: 'disabled', type: 'boolean'},
						{header: 'Order', width: 25, dataIndex: 'processingOrder', type: 'int'}
					],
					editor: {
						detailPageClass: 'Clifton.replication.task.ReplicationTaskTableWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Task Columns',
				items: [{
					name: 'replicationTaskColumnListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all columns across all replication tasks that are replicated between systems.',
					wikiPage: 'IT/Replication+Documentation',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Task Name', width: 100, dataIndex: 'taskTable.task.name'},
						{header: 'Table Name', width: 80, dataIndex: 'taskTable.sourceSystemTable.name'},
						{header: 'Column Name', width: 80, dataIndex: 'sourceSystemColumn.name'},
						{header: 'Column Description', width: 150, dataIndex: 'sourceSystemColumn.description', hidden: true}
					],
					editor: {
						detailPageClass: 'Clifton.replication.task.ReplicationTaskColumnWindow',
						drillDownOnly: true
					}
				}]
			},


			{
				title: 'Key Mappings',
				items: [{
					xtype: 'gridpanel',
					name: 'replicationKeyMappingListFind',
					instructions: 'Replication key mapping store the source and target fkFieldIds for replication from source to target. They are used to replicate changes between systems.',
					wikiPage: 'IT/Replication+Documentation',
					topToolbarSearchParameter: 'searchPattern',
					reloadOnRender: false,
					columns: [
						{header: 'First Data Source', width: 80, displayField: 'firstDataSourceId', dataIndex: 'firstDataSourceSystemTable.dataSource.databaseName', filter: {type: 'combo', displayField: 'databaseName', searchFieldName: 'firstDataSourceId', url: 'systemDataSourceList.json', loadAll: true}},
						{header: 'Second Data Source', width: 80, displayField: 'secondDataSourceId', dataIndex: 'secondDataSource.databaseName', filter: {type: 'combo', displayField: 'databaseName', searchFieldName: 'secondDataSourceId', url: 'systemDataSourceList.json', loadAll: true}},
						{header: 'Table Name', width: 100, dataIndex: 'firstDataSourceSystemTable.name', filter: {searchFieldName: 'firstSystemTableName'}},
						{header: 'First FK ID', width: 30, type: 'int', dataIndex: 'firstDataSourceFkFieldId', doNotFormat: true},
						{header: 'Second FK ID', width: 30, type: 'int', dataIndex: 'secondDataSourceFkFieldId', doNotFormat: true},
						{header: 'Date', width: 40, dataIndex: 'replicationDate'}
					],
					editor: {
						drillDownOnly: true,
						allowToDeleteMultiple: true,
						addEditButtons: function(toolbar, gridPanel) {
							this.addToolbarDeleteButton(toolbar, gridPanel);
						}
					}
				}]
			},


			{
				title: 'Admin',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'formpanel',
						loadValidation: false, // using the form only to get background color/padding
						height: 125,
						labelWidth: 200,
						instructions: 'Executes the selected replication task.',
						items: [
							{fieldLabel: 'Replication Task', name: 'replicationTask', hiddenName: 'replicationTaskId', xtype: 'combo', url: 'replicationTaskListFind.json', displayField: 'name', detailPageClass: 'Clifton.replication.task.ReplicationTaskWindow'},
							{
								fieldLabel: 'Configuration Flags', xtype: 'checkboxgroup', columns: 2, qtip: 'Configuration flags',
								items: [
									{boxLabel: 'Preview Only', name: 'previewOnly', xtype: 'checkbox', qtip: 'Runs the replicate logic without calling any service methods that would alter the datasource'},
									{boxLabel: 'Capture Table Row Status', name: 'showTableRowStatus', xtype: 'checkbox', qtip: 'Stores table row status as status detail'}
								]
							}
						],
						buttons: [
							{
								text: 'Execute Replication Task',
								tooltip: 'Run the replication task/',
								iconCls: 'run',
								width: 170,
								handler: function() {
									const formPanel = TCG.getParentFormPanel(this);
									const loader = new TCG.data.JsonLoader({
										waitTarget: formPanel,
										params: {
											replicationTaskId: this.ownerCt.ownerCt.getFormValue('replicationTaskId'),
											previewOnly: formPanel.getForm().findField('previewOnly').checked,
											showTableRowStatus: formPanel.getForm().findField('showTableRowStatus').checked
										},
										onLoad: function(record, conf) {
											TCG.showInfo('Replication Task has been executed.', 'Replication Task');
										}
									});
									loader.load('replicationTaskExecute.json');
								}
							}
						]
					},

					{
						xtype: 'core-scheduled-runner-grid',
						typeName: 'REPLICATION-TASK',
						instantRunner: false,
						title: 'Current Processing',
						flex: 1
					}
				]
			}
		]
	}]
});
