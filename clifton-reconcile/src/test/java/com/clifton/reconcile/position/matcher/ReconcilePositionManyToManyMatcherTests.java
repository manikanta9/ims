package com.clifton.reconcile.position.matcher;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.reconcile.position.accounting.ReconcilePositionAccountingDailyGrouping;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDaily;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;


public class ReconcilePositionManyToManyMatcherTests {

	@Test
	public void testPartialMatch() {
		ReconcilePositionManyToManyMatcher<ReconcilePositionExternalDaily> matcher = new ReconcilePositionManyToManyMatcher<>();
		List<ReconcilePositionExternalDaily> externalList = new ArrayList<>();
		List<ReconcilePositionAccountingDailyGrouping> internalList = new ArrayList<>();

		externalList.add(createExternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		externalList.add(createExternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		externalList.add(createExternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		externalList.add(createExternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));

		internalList.add(createInternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		internalList.add(createInternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		internalList.add(createInternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		internalList.add(createInternalPosition(9, DateUtils.toDate("12/15/2012"), 5.4, 13));

		MatchingResult<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> match = matcher.match(internalList, externalList, 1, true);

		Assertions.assertEquals(3, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());
	}


	@Test
	public void testPartialMatchOneSideHasExtra() {
		ReconcilePositionManyToManyMatcher<ReconcilePositionExternalDaily> matcher = new ReconcilePositionManyToManyMatcher<>();
		List<ReconcilePositionExternalDaily> externalList = new ArrayList<>();
		List<ReconcilePositionAccountingDailyGrouping> internalList = new ArrayList<>();

		externalList.add(createExternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		externalList.add(createExternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		externalList.add(createExternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		externalList.add(createExternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 12));

		internalList.add(createInternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		internalList.add(createInternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		internalList.add(createInternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));

		MatchingResult<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> match = matcher.match(internalList, externalList, 1, true);

		Assertions.assertEquals(3, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());
	}


	@Test
	public void testPartialMatchTwoPossibleMatches() {
		ReconcilePositionManyToManyMatcher<ReconcilePositionExternalDaily> matcher = new ReconcilePositionManyToManyMatcher<>();
		List<ReconcilePositionExternalDaily> externalList = new ArrayList<>();
		List<ReconcilePositionAccountingDailyGrouping> internalList = new ArrayList<>();

		externalList.add(createExternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		externalList.add(createExternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		externalList.add(createExternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		externalList.add(createExternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));

		internalList.add(createInternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		internalList.add(createInternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		internalList.add(createInternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		internalList.add(createInternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));
		internalList.add(createInternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 14));

		MatchingResult<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> match = matcher.match(internalList, externalList, 1, true);

		Assertions.assertEquals(4, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(13, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(0).getId());
	}


	@Test
	public void testPartialMatchPricesInOrder() {
		ReconcilePositionManyToManyMatcher<ReconcilePositionExternalDaily> matcher = new ReconcilePositionManyToManyMatcher<>();
		List<ReconcilePositionExternalDaily> externalList = new ArrayList<>();
		List<ReconcilePositionAccountingDailyGrouping> internalList = new ArrayList<>();

		externalList.add(createExternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		externalList.add(createExternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		externalList.add(createExternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		externalList.add(createExternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));

		internalList.add(createInternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		internalList.add(createInternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		internalList.add(createInternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		internalList.add(createInternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));
		internalList.add(createInternalPosition(7, DateUtils.toDate("12/15/2012"), 5.5, 14));

		MatchingResult<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> match = matcher.match(internalList, externalList, 1, true);

		Assertions.assertEquals(4, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(13, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(0).getId());
	}


	@Test
	public void testPartialMatchPricesOutOfOrder() {
		ReconcilePositionManyToManyMatcher<ReconcilePositionExternalDaily> matcher = new ReconcilePositionManyToManyMatcher<>();
		List<ReconcilePositionExternalDaily> externalList = new ArrayList<>();
		List<ReconcilePositionAccountingDailyGrouping> internalList = new ArrayList<>();

		externalList.add(createExternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		externalList.add(createExternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		externalList.add(createExternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		externalList.add(createExternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));

		internalList.add(createInternalPosition(2, DateUtils.toDate("12/12/2012"), 5.1, 10));
		internalList.add(createInternalPosition(4, DateUtils.toDate("12/13/2012"), 5.2, 11));
		internalList.add(createInternalPosition(3, DateUtils.toDate("12/14/2012"), 5.3, 12));
		internalList.add(createInternalPosition(7, DateUtils.toDate("12/15/2012"), 5.5, 14));
		internalList.add(createInternalPosition(7, DateUtils.toDate("12/15/2012"), 5.4, 13));

		MatchingResult<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> match = matcher.match(internalList, externalList, 1, true);

		Assertions.assertEquals(4, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDaily> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		//Matches 13 - 14 because prices are out of order (best match should be 13 - 13)
		Assertions.assertEquals(14, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(0).getId());
	}


	private ReconcilePositionExternalDaily createExternalPosition(int quantity, Date tradeDate, double price, int id) {
		ReconcilePositionExternalDaily external = new ReconcilePositionExternalDaily();
		external.setQuantity(BigDecimal.valueOf(quantity));
		external.setTradeDate(tradeDate);
		external.setTradePrice(BigDecimal.valueOf(price));
		external.setId(id);

		return external;
	}


	private ReconcilePositionAccountingDailyGrouping createInternalPosition(int quantity, Date tradeDate, double price, int id) {
		ReconcilePositionAccountingDailyGrouping internal = new ReconcilePositionAccountingDailyGrouping();
		internal.setOriginalTransactionDate(tradeDate);
		internal.setPrice(BigDecimal.valueOf(price));
		internal.setRemainingQuantity(BigDecimal.valueOf(quantity));
		internal.setId(id);

		return internal;
	}
}
