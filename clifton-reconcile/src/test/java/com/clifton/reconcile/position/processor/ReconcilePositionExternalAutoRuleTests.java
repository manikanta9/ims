package com.clifton.reconcile.position.processor;


import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionAccount;
import com.clifton.reconcile.position.ReconcilePositionDefinition;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;


public class ReconcilePositionExternalAutoRuleTests {

	@Test
	public void testReconcilePositionExternaAutoRule() {

		// Instantiate main test object
		ReconcilePositionExternalAutoRule autoRule = new ReconcilePositionExternalAutoRule();

		// create null context
		HashMap<String, Object> context = null;

		ReconcilePosition position = buildPosition();
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurMarketValueBase(position.getOurMarketValueBase().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurMarketValueBase(position.getOurMarketValueBase().subtract(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurQuantity(position.getOurQuantity().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurQuantity(position.getOurQuantity().subtract(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurMarketValueLocal(position.getOurMarketValueLocal().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurMarketValueLocal(position.getOurMarketValueLocal().subtract(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurOpenTradeEquityLocal(position.getOurOpenTradeEquityLocal().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurOpenTradeEquityLocal(position.getOurOpenTradeEquityLocal().subtract(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.getPositionAccount().getDefinition().setLocalOnly(true);
		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().subtract(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurFxRate(position.getOurFxRate().add(new BigDecimal(1)));
		position.setOurMarketValueBase(position.getOurMarketValueBase().subtract(new BigDecimal(1)));
		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurMarketValueLocal(position.getOurMarketValueLocal().add(new BigDecimal(1)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		//////////////////////////////////////////////////
		//     Test ote and market value thresholds     //
		//////////////////////////////////////////////////

		context = new HashMap<>();
		context.put("oteMaxThreshold", BigDecimal.valueOf(0.01));
		context.put("marketValueMaxThreshold", BigDecimal.valueOf(0.01));

		position = buildPosition();
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		// test MarketValueBase plus/minus .01
		position.setOurMarketValueBase(position.getOurMarketValueBase().add(BigDecimal.valueOf(0.01)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurMarketValueBase(position.getOurMarketValueBase().subtract(BigDecimal.valueOf(0.02)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		// test MarketValueLocal plus/minus .01
		position.setOurMarketValueLocal(position.getOurMarketValueLocal().add(BigDecimal.valueOf(0.01)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurMarketValueLocal(position.getOurMarketValueLocal().subtract(BigDecimal.valueOf(0.02)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		// test OpenTradeEquityBase plus/minus .01
		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().add(BigDecimal.valueOf(0.01)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().subtract(BigDecimal.valueOf(0.02)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		// test OpenTradeEquityLocal plus/minus .01
		position.setOurOpenTradeEquityLocal(position.getOurOpenTradeEquityLocal().add(BigDecimal.valueOf(0.01)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		position.setOurOpenTradeEquityLocal(position.getOurOpenTradeEquityLocal().subtract(BigDecimal.valueOf(0.02)));
		autoRule.reconcile(position, context);
		Assertions.assertTrue(position.isReconciled());

		// test unreconciled
		position = buildPosition();

		position.setOurMarketValueBase(position.getOurMarketValueBase().add(BigDecimal.valueOf(0.02)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurMarketValueBase(position.getOurMarketValueBase().subtract(BigDecimal.valueOf(0.04)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().add(BigDecimal.valueOf(0.02)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());

		position.setOurOpenTradeEquityBase(position.getOurOpenTradeEquityBase().subtract(BigDecimal.valueOf(0.04)));
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());
	}


	private ReconcilePosition buildPosition() {
		ReconcilePositionDefinition definition = new ReconcilePositionDefinition();
		definition.setId(1);
		definition.setLocalOnly(false);
		ReconcilePositionAccount account = new ReconcilePositionAccount();
		account.setDefinition(definition);
		ReconcilePosition position = new ReconcilePosition();
		position.setPositionAccount(account);

		position.setOurFxRate(new BigDecimal(1));
		position.setOurQuantity(new BigDecimal(15));
		position.setOurMarketValueLocal(new BigDecimal(100546.54));
		position.setOurMarketValueBase(new BigDecimal(100546.54));
		position.setOurOpenTradeEquityLocal(new BigDecimal(52000.00));
		position.setOurOpenTradeEquityBase(new BigDecimal(52000.00));

		position.setExternalFxRate(new BigDecimal(1));
		position.setExternalQuantity(new BigDecimal(15));
		position.setExternalMarketValueLocal(new BigDecimal(100546.54));
		position.setExternalMarketValueBase(new BigDecimal(100546.54));
		position.setExternalOpenTradeEquityLocal(new BigDecimal(52000.00));
		position.setExternalOpenTradeEquityBase(new BigDecimal(52000.00));

		return position;
	}


	@Test
	public void testReconcilePositionExternaAutoRuleInternalZero() {

		// Instantiate main test object
		ReconcilePositionExternalAutoRule autoRule = new ReconcilePositionExternalAutoRule();

		HashMap<String, Object> context = null;
		ReconcilePosition position = buildPositionInternalZero();
		autoRule.reconcile(position, context);
		Assertions.assertFalse(position.isReconciled());
		Assertions.assertFalse(position.isQuantityReconciled());
	}


	private ReconcilePosition buildPositionInternalZero() {
		ReconcilePositionDefinition definition = new ReconcilePositionDefinition();
		definition.setId(1);
		definition.setLocalOnly(false);
		ReconcilePositionAccount account = new ReconcilePositionAccount();
		account.setDefinition(definition);
		ReconcilePosition position = new ReconcilePosition();
		position.setPositionAccount(account);

		position.setMatched(false);

		position.setOurQuantity(BigDecimal.ZERO);
		position.setOurMarketValueLocal(BigDecimal.ZERO);
		position.setOurMarketValueBase(BigDecimal.ZERO);
		position.setOurOpenTradeEquityLocal(BigDecimal.ZERO);
		position.setOurOpenTradeEquityBase(BigDecimal.ZERO);
		position.setOurTodayClosedQuantity(BigDecimal.ZERO);
		position.setOurTodayCommissionLocal(BigDecimal.ZERO);
		position.setOurTodayRealizedGainLossLocal(BigDecimal.ZERO);

		position.setExternalFxRate(new BigDecimal("1.5403570000000000"));
		position.setExternalPrice(new BigDecimal("6414.500000000000000"));
		position.setExternalQuantity(new BigDecimal("9"));
		position.setExternalMarketValueLocal(new BigDecimal("577305.00"));
		position.setExternalMarketValueBase(new BigDecimal("889255.80"));
		position.setExternalOpenTradeEquityLocal(new BigDecimal("-11385.00"));
		position.setExternalOpenTradeEquityBase(new BigDecimal("-17536.96"));
		position.setExternalTodayClosedQuantity(BigDecimal.ZERO);
		position.setExternalTodayCommissionLocal(BigDecimal.ZERO);
		position.setExternalTodayRealizedGainLossLocal(BigDecimal.ZERO);

		return position;
	}
}
