package com.clifton.reconcile.trade.intraday.matcher;

import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.trade.TradeFill;
import com.clifton.trade.builder.TradeBuilder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;


public class ReconcileTradeIntradayMatchingManyToManyTest {

	/**
	 * Changes need to be made to the matcher to account for duplicates in the list being matched.  For now,
	 * leaving this test here because all the setup needed to test these changes to duplicate matching is already done.
	 */
	// @Test
	public void testGroupManyToManyDuplicates() {
		// TODO: Finish duplicate matching testing which is potentially.
		//		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping>(false,
		//		        new BigDecimal("0.000001"));
		//		List<TradeFill> fillList = new ArrayList<TradeFill>();
		//		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<ReconcileTradeIntradayExternalGrouping>();
		//
		//		fillList.add(createFill(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9018.000000000000000), 336890));
		//		fillList.add(createFill(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9019.000000000000000), 336891));
		//		fillList.add(createFill(BigDecimal.valueOf(1.0000000000), BigDecimal.valueOf(9020.000000000000000), 336892));
		//		fillList.add(createFill(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9021.000000000000000), 336893));
		//		fillList.add(createFill(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9022.000000000000000), 336894));
		//		fillList.add(createFill(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9023.000000000000000), 336895));
		//		fillList.add(createFill(BigDecimal.valueOf(3.0000000000), BigDecimal.valueOf(9025.000000000000000), 336896));
		//		fillList.add(createFill(BigDecimal.valueOf(3.0000000000), BigDecimal.valueOf(9026.000000000000000), 336897));
		//		fillList.add(createFill(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9027.000000000000000), 336898));
		//		fillList.add(createFill(BigDecimal.valueOf(5.0000000000), BigDecimal.valueOf(9028.000000000000000), 336899));
		//		fillList.add(createFill(BigDecimal.valueOf(8.0000000000), BigDecimal.valueOf(9029.000000000000000), 336900));
		//		fillList.add(createFill(BigDecimal.valueOf(13.0000000000), BigDecimal.valueOf(9030.000000000000000), 336901));
		//		fillList.add(createFill(BigDecimal.valueOf(16.0000000000), BigDecimal.valueOf(9031.000000000000000), 336902));
		//		fillList.add(createFill(BigDecimal.valueOf(20.0000000000), BigDecimal.valueOf(9032.000000000000000), 336903));
		//		fillList.add(createFill(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(9033.000000000000000), 336904));
		//		fillList.add(createFill(BigDecimal.valueOf(13.0000000000), BigDecimal.valueOf(9034.000000000000000), 336905));
		//		fillList.add(createFill(BigDecimal.valueOf(5.0000000000), BigDecimal.valueOf(9035.000000000000000), 336906));
		//		fillList.add(createFill(BigDecimal.valueOf(5.0000000000), BigDecimal.valueOf(9036.000000000000000), 336907));
		//		fillList.add(createFill(BigDecimal.valueOf(21.0000000000), BigDecimal.valueOf(9037.000000000000000), 336908));
		//		fillList.add(createFill(BigDecimal.valueOf(11.0000000000), BigDecimal.valueOf(9038.000000000000000), 336909));
		//		fillList.add(createFill(BigDecimal.valueOf(12.0000000000), BigDecimal.valueOf(9039.000000000000000), 336910));
		//		fillList.add(createFill(BigDecimal.valueOf(8.0000000000), BigDecimal.valueOf(9040.000000000000000), 336911));
		//		fillList.add(createFill(BigDecimal.valueOf(1.0000000000), BigDecimal.valueOf(9041.000000000000000), 336912));
		//		fillList.add(createFill(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9042.000000000000000), 336913));
		//
		//		externalList.add(createExternal(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9020.000000000000000), 197224, Arrays.asList(1, 1), Arrays.asList(197224, 197223)));
		//		externalList.add(createExternal(BigDecimal.valueOf(2.0000000000), BigDecimal.valueOf(9041.000000000000000), 197256, Arrays.asList(1, 1), Arrays.asList(197256, 197255)));
		//		externalList.add(createExternal(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9018.000000000000000), 197236, Arrays.asList(2, 2), Arrays.asList(197236, 197235)));
		//		externalList.add(createExternal(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9019.000000000000000), 197220, Arrays.asList(2, 2), Arrays.asList(197220, 197219)));
		//		externalList.add(createExternal(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9021.000000000000000), 197240, Arrays.asList(2, 2), Arrays.asList(197240, 197239)));
		//		externalList.add(createExternal(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9027.000000000000000), 197214, Arrays.asList(2, 2), Arrays.asList(197214, 197213)));
		//		externalList.add(createExternal(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(9042.000000000000000), 197250, Arrays.asList(2, 2), Arrays.asList(197250, 197249)));
		//		externalList.add(createExternal(BigDecimal.valueOf(6.0000000000), BigDecimal.valueOf(9025.000000000000000), 197238, Arrays.asList(3, 3), Arrays.asList(197238, 197237)));
		//		externalList.add(createExternal(BigDecimal.valueOf(6.0000000000), BigDecimal.valueOf(9026.000000000000000), 197218, Arrays.asList(3, 3), Arrays.asList(197218, 197217)));
		//		externalList.add(createExternal(BigDecimal.valueOf(8.0000000000), BigDecimal.valueOf(9022.000000000000000), 197226, Arrays.asList(4, 4), Arrays.asList(197226, 197225)));
		//		externalList.add(createExternal(BigDecimal.valueOf(8.0000000000), BigDecimal.valueOf(9023.000000000000000), 197234, Arrays.asList(4, 4), Arrays.asList(197234, 197233)));
		//		externalList.add(createExternal(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(9028.000000000000000), 197222, Arrays.asList(5, 5), Arrays.asList(197222, 197221)));
		//		externalList.add(createExternal(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(9035.000000000000000), 197258, Arrays.asList(5, 5), Arrays.asList(197258, 197257)));
		//		externalList.add(createExternal(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(9036.000000000000000), 197242, Arrays.asList(5, 5), Arrays.asList(197242, 197241)));
		//		externalList.add(createExternal(BigDecimal.valueOf(16.0000000000), BigDecimal.valueOf(9029.000000000000000), 197232, Arrays.asList(8, 8), Arrays.asList(197232, 197231)));
		//		externalList.add(createExternal(BigDecimal.valueOf(16.0000000000), BigDecimal.valueOf(9040.000000000000000), 197254, Arrays.asList(8, 8), Arrays.asList(197254, 197253)));
		//		externalList.add(createExternal(BigDecimal.valueOf(20.0000000000), BigDecimal.valueOf(9033.000000000000000), 197244, Arrays.asList(10, 10), Arrays.asList(197244, 197243)));
		//		externalList.add(createExternal(BigDecimal.valueOf(22.0000000000), BigDecimal.valueOf(9038.000000000000000), 197252, Arrays.asList(11, 11), Arrays.asList(197252, 197251)));
		//		externalList.add(createExternal(BigDecimal.valueOf(24.0000000000), BigDecimal.valueOf(9039.000000000000000), 197216, Arrays.asList(12, 12), Arrays.asList(197216, 197215)));
		//		externalList.add(createExternal(BigDecimal.valueOf(26.0000000000), BigDecimal.valueOf(9030.000000000000000), 197230, Arrays.asList(13, 13), Arrays.asList(197230, 197229)));
		//		externalList.add(createExternal(BigDecimal.valueOf(26.0000000000), BigDecimal.valueOf(9034.000000000000000), 197260, Arrays.asList(13, 13), Arrays.asList(197260, 197259)));
		//		externalList.add(createExternal(BigDecimal.valueOf(32.0000000000), BigDecimal.valueOf(9031.000000000000000), 197246, Arrays.asList(16, 16), Arrays.asList(197246, 197245)));
		//		externalList.add(createExternal(BigDecimal.valueOf(40.0000000000), BigDecimal.valueOf(9032.000000000000000), 197228, Arrays.asList(20, 20), Arrays.asList(197228, 197227)));
		//		externalList.add(createExternal(BigDecimal.valueOf(42.0000000000), BigDecimal.valueOf(9037.000000000000000), 197248, Arrays.asList(21, 21), Arrays.asList(197248, 197247)));
		//
		//		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);
		//		Assertions.assertEquals(0, match.getItemList().size());
	}


	@Test
	public void testGroupManyToMany() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>(false,
				new BigDecimal("0.000001"));
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(1514.800000000000000), 276494));
		fillList.add(createFill(BigDecimal.valueOf(36.0000000000), BigDecimal.valueOf(1514.800000000000000), 276495));
		fillList.add(createFill(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(1514.400000000000000), 276496));
		fillList.add(createFill(BigDecimal.valueOf(8.0000000000), BigDecimal.valueOf(1514.600000000000000), 276498));
		fillList.add(createFill(BigDecimal.valueOf(13.0000000000), BigDecimal.valueOf(1514.900000000000000), 276501));
		fillList.add(createFill(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(1514.700000000000000), 276503));
		fillList.add(createFill(BigDecimal.valueOf(6.0000000000), BigDecimal.valueOf(1514.500000000000000), 276504));
		fillList.add(createFill(BigDecimal.valueOf(15.0000000000), BigDecimal.valueOf(1515.000000000000000), 276507));
		fillList.add(createFill(BigDecimal.valueOf(4.0000000000), BigDecimal.valueOf(1514.400000000000000), 276497));
		fillList.add(createFill(BigDecimal.valueOf(22.0000000000), BigDecimal.valueOf(1514.600000000000000), 276499));
		fillList.add(createFill(BigDecimal.valueOf(7.0000000000), BigDecimal.valueOf(1514.900000000000000), 276500));
		fillList.add(createFill(BigDecimal.valueOf(3.0000000000), BigDecimal.valueOf(1514.700000000000000), 276502));
		fillList.add(createFill(BigDecimal.valueOf(18.0000000000), BigDecimal.valueOf(1514.500000000000000), 276505));
		fillList.add(createFill(BigDecimal.valueOf(10.0000000000), BigDecimal.valueOf(1515.000000000000000), 276506));

		externalList.add(createExternal(BigDecimal.valueOf(8.0000000000), BigDecimal.valueOf(1514.400000000000000), 112145, Arrays.asList(1, 1, 1, 1, 2, 2),
				Arrays.asList(111965, 111966, 111967, 111968, 112144, 112145)));
		externalList.add(createExternal(BigDecimal.valueOf(24.0000000000), BigDecimal.valueOf(1514.500000000000000), 112160,
				Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1, 1),
				Arrays.asList(111969, 111970, 111971, 111972, 111973, 111974, 112146, 112147, 112148, 112149, 112150, 112151, 112152, 112153, 112154, 112155, 112156, 112157, 112158, 112159, 112160)));
		externalList.add(createExternal(BigDecimal.valueOf(30.0000000000), BigDecimal.valueOf(1514.600000000000000), 112176, Arrays.asList(2, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1), Arrays.asList(111975, 111976, 111977, 111978, 111979, 111980, 111981, 112161, 112162, 112163, 112164, 112165, 112166, 112167, 112168, 112169, 112170, 112171, 112172, 112173,
				112174, 112175, 112176)));
		externalList.add(createExternal(BigDecimal.valueOf(13.0000000000), BigDecimal.valueOf(1514.700000000000000), 112178, Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1),
				Arrays.asList(111722, 111723, 111724, 111725, 111982, 111983, 111984, 111985, 111986, 111987, 112177, 112178)));
		externalList.add(createExternal(BigDecimal.valueOf(46.0000000000), BigDecimal.valueOf(1514.800000000000000), 112205, Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1,
				1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2), Arrays.asList(111988, 111989, 111990, 111991, 111992, 111993, 111994, 111995, 111996, 111997, 112179, 112180, 112181, 112182,
				112183, 112184, 112185, 112186, 112187, 112188, 112189, 112190, 112191, 112192, 112193, 112194, 112195, 112196, 112197, 112198, 112199, 112200, 112201, 112202, 112203, 112204, 112205)));
		externalList.add(createExternal(BigDecimal.valueOf(20.0000000000), BigDecimal.valueOf(1514.900000000000000), 112211, Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1),
				Arrays.asList(111726, 111727, 111728, 111729, 111730, 111731, 111732, 111733, 111734, 111735, 111736, 111737, 111738, 112206, 112207, 112208, 112209, 112210, 112211)));
		externalList.add(createExternal(BigDecimal.valueOf(25.0000000000), BigDecimal.valueOf(1515.000000000000000), 112218,
				Arrays.asList(2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 1, 1, 1, 1, 2),
				Arrays.asList(111739, 111740, 111741, 111742, 111743, 111744, 111745, 111746, 111747, 111748, 111749, 111750, 111751, 111752, 112212, 112213, 112214, 112215, 112216, 112217, 112218)));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);
		Assertions.assertEquals(7, match.getItemList().size());

		//Print out for debugging
		StringBuilder sb = new StringBuilder();
		sb.append("\t").append(match.getScore()).append("\n");
		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
			sb.append("\t").append(i).append("\n");
		}

		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
			Assertions.assertEquals(0, CoreMathUtils.sumProperty(i.getExternalList(), ReconcileTradeIntradayExternalGrouping::getQuantity).compareTo(CoreMathUtils.sumProperty(i.getInternalList(), TradeFill::getQuantity)));

			for (ReconcileTradeIntradayExternal et : CollectionUtils.getIterable(i.getExternalList())) {
				Assertions.assertFalse(i.getInternalList().contains(et));
			}
			for (TradeFill t : CollectionUtils.getIterable(i.getInternalList())) {
				Assertions.assertFalse(i.getExternalList().contains(t));
			}

			List<ReconcileTradeIntradayExternal> newExternalList = new ArrayList<>();
			for (ReconcileTradeIntradayExternalGrouping g : CollectionUtils.getIterable(i.getExternalList())) {
				newExternalList.addAll(g.getExternalTradeList());
			}

			ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternal> newMatcher = new ReconcileTradeIntradayManyToManyMatcher<>(true, new BigDecimal(
					"0.000001"));
			MatchingResult<TradeFill, ReconcileTradeIntradayExternal> newMatch = newMatcher.match(i.getInternalList(), newExternalList);
			Assertions.assertEquals(2, newMatch.getItemList().size());

			//Print out for debugging
			sb.append("\t\t").append(newMatch.getScore()).append("\n");
			for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternal> e : CollectionUtils.getIterable(newMatch.getItemList())) {
				sb.append("\t\t").append(e).append("\n");
				Assertions.assertEquals(0, CoreMathUtils.sumProperty(e.getExternalList(), ReconcileTradeIntradayExternal::getQuantity).compareTo(CoreMathUtils.sumProperty(e.getInternalList(), TradeFill::getQuantity)));

				for (ReconcileTradeIntradayExternal et : CollectionUtils.getIterable(e.getExternalList())) {
					Assertions.assertFalse(e.getInternalList().contains(et));
				}
				for (TradeFill t : CollectionUtils.getIterable(e.getInternalList())) {
					Assertions.assertFalse(e.getExternalList().contains(t));
				}
			}
		}
		//Print out for debugging
		//		System.out.println(sb.toString());
	}


	@Test
	public void testManyToMany() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>(true,
				new BigDecimal("0.000001"));
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(10), BigDecimal.valueOf(144), 10));
		fillList.add(createFill(BigDecimal.valueOf(16), BigDecimal.valueOf(144), 11));
		fillList.add(createFill(BigDecimal.valueOf(20), BigDecimal.valueOf(144), 12));

		List<Integer> numbers = Arrays.asList(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2);
		for (int i = 0; i < numbers.size(); i++) {
			externalList.add(createExternal(BigDecimal.valueOf(numbers.get(i)), BigDecimal.valueOf(144), 50 + i));
		}

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(0, CoreMathUtils.sumProperty(r.getExternalList(), ReconcileTradeIntradayExternalGrouping::getQuantity).compareTo(CoreMathUtils.sumProperty(r.getInternalList(), TradeFill::getQuantity)));
		r = match.getItemList().get(1);
		Assertions.assertEquals(0, CoreMathUtils.sumProperty(r.getExternalList(), ReconcileTradeIntradayExternalGrouping::getQuantity).compareTo(CoreMathUtils.sumProperty(r.getInternalList(), TradeFill::getQuantity)));
		r = match.getItemList().get(2);
		Assertions.assertEquals(0, CoreMathUtils.sumProperty(r.getExternalList(), ReconcileTradeIntradayExternalGrouping::getQuantity).compareTo(CoreMathUtils.sumProperty(r.getInternalList(), TradeFill::getQuantity)));
	}


	@Test
	public void testAveragePriceMatch() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>(true,
				new BigDecimal("0.000001"));
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(5), BigDecimal.valueOf(149), 10));
		fillList.add(createFill(BigDecimal.valueOf(5), BigDecimal.valueOf(151), 11));

		ReconcileTradeIntradayExternalGrouping e = createExternal(BigDecimal.valueOf(10), BigDecimal.valueOf(150), 110, Arrays.asList(1, 1, 1, 1, 2, 2, 2),
				Arrays.asList(110, 109, 108, 107, 106, 105, 104));
		externalList.add(e);

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);
		Assertions.assertEquals(1, match.getItemList().size());

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(2, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getInternalList().get(1).getId());
		Assertions.assertEquals(110, (int) r.getExternalList().get(0).getId());

		List<TradeFill> tradeLevelFillList = new ArrayList<>(r.getInternalList());
		List<ReconcileTradeIntradayExternal> tradeLevelExternalList = new ArrayList<>(r.getExternalList().get(0).getExternalTradeList());

		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternal> newMatcher = new ReconcileTradeIntradayManyToManyMatcher<>(true, new BigDecimal(
				"0.000001"));
		MatchingResult<TradeFill, ReconcileTradeIntradayExternal> newMatch = newMatcher.match(tradeLevelFillList, tradeLevelExternalList);
		// should fail trying to do an average price across a group
		Assertions.assertNull(newMatch);

		e = createExternal(BigDecimal.valueOf(10), BigDecimal.valueOf(150), 110, Collections.singletonList(10), Collections.singletonList(110));

		newMatch = newMatcher.match(tradeLevelFillList, e.getExternalTradeList());
		// should fail trying to do an average price across a group
		Assertions.assertEquals(1, newMatch.getItemList().size());

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternal> rt = newMatch.getItemList().get(0);
		Assertions.assertEquals(2, rt.getInternalList().size());
		Assertions.assertEquals(1, rt.getExternalList().size());

		Assertions.assertEquals(10, (int) rt.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) rt.getInternalList().get(1).getId());
		Assertions.assertEquals(110, (int) rt.getExternalList().get(0).getId());
	}


	/**
	 * End matches should be:
	 * <p/>
	 * Internal->External
	 * 10->10,11
	 * 11->12,13
	 * 12->14,15,16
	 */
	@Test
	public void testManyToManyInternalSideHasMore() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));

		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 10));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 11));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 12));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 13));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 14));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 15));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 16));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		Assertions.assertEquals(3, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r1 = match.getItemList().get(0);
		Assertions.assertEquals(1, r1.getInternalList().size());
		Assertions.assertEquals(2, r1.getExternalList().size());

		Assertions.assertEquals(10, (int) r1.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r1.getExternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r1.getExternalList().get(1).getId());

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r2 = match.getItemList().get(1);
		Assertions.assertEquals(1, r2.getInternalList().size());
		Assertions.assertEquals(2, r2.getExternalList().size());

		Assertions.assertEquals(11, (int) r2.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r2.getExternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r2.getExternalList().get(1).getId());

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r3 = match.getItemList().get(2);
		Assertions.assertEquals(1, r3.getInternalList().size());
		Assertions.assertEquals(3, r3.getExternalList().size());

		Assertions.assertEquals(12, (int) r3.getInternalList().get(0).getId());
		Assertions.assertEquals(14, (int) r3.getExternalList().get(0).getId());
		Assertions.assertEquals(15, (int) r3.getExternalList().get(1).getId());
		Assertions.assertEquals(16, (int) r3.getExternalList().get(2).getId());
	}


	/**
	 * No match because the average price of all trades on both sides doesn't match.
	 */
	@Test
	public void testManyToManyNoAverage() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));
		fillList.add(createFill(BigDecimal.valueOf(7), BigDecimal.valueOf(175), 13));

		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 10));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 11));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 12));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 13));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 14));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 15));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 16));
		externalList.add(createExternal(BigDecimal.valueOf(7), BigDecimal.valueOf(172), 17));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);
		Assertions.assertNull(match);
	}


	/**
	 * End matches should be:
	 * 10,11->10
	 * 12,13->11
	 * 14,15,16->12
	 * 17->13,14
	 */
	@Test
	public void testManyToManyExternalSideHasMore() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		externalList.add(createExternal(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		externalList.add(createExternal(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));
		externalList.add(createExternal(BigDecimal.valueOf(2), BigDecimal.valueOf(175), 13));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(172), 14));

		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 10));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 11));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 12));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 13));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 14));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 15));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 16));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(174), 17));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		Assertions.assertEquals(4, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(2, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getInternalList().get(1).getId());

		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(2, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getInternalList().get(1).getId());

		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(3, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(14, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(15, (int) r.getInternalList().get(1).getId());
		Assertions.assertEquals(16, (int) r.getInternalList().get(2).getId());

		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(17, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(14, (int) r.getExternalList().get(1).getId());
	}


	/**
	 * End matches should be:
	 * <p/>
	 * Internal->External
	 * 10->10,11
	 * 11->12,13
	 * 12->14,15,16
	 * 13,14->17
	 */
	@Test
	public void testBothSidesHaveManyToMany() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));
		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(175), 13));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(172), 14));

		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 10));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 11));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 12));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 13));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 14));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 15));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 16));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(174), 17));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		Assertions.assertEquals(4, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(3, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(14, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(15, (int) r.getExternalList().get(1).getId());
		Assertions.assertEquals(16, (int) r.getExternalList().get(2).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(2, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(13, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(14, (int) r.getInternalList().get(1).getId());
		Assertions.assertEquals(17, (int) r.getExternalList().get(0).getId());
	}


	@Test
	public void testGrouping() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(50), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(50), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(1), BigDecimal.valueOf(175), 12));

		for (int i = 0; i < 50; i++) {
			externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(166), i + 1));
		}
		for (int i = 0; i < 25; i++) {
			externalList.add(createExternal(BigDecimal.valueOf(2), BigDecimal.valueOf(158), i + 51));
		}
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(175), 101));

		List<Integer> s = new ArrayList<>();
		s.add(50);
		s.add(25);
		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList, s);
		Assertions.assertEquals(3, match.getItemList().size());
	}


	/**
	 * End matches should be:
	 * 10,11->10
	 * 12,13->11
	 * 14,15,16->12
	 * 17->13,14
	 */
	@Test
	public void testBothSidesHaveOneToOne() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		externalList.add(createExternal(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		externalList.add(createExternal(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));

		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		Assertions.assertEquals(3, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(0).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(1, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());
		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());
	}


	@Test
	public void testManyToManyBig() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));
		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 13));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 14));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 15));

		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 10));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 11));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 12));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 13));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 14));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 15));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 16));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 17));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 18));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 19));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 20));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 21));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 22));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 23));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		match.getItemList().sort(Comparator.comparing(o2 -> o2.getInternalList().get(0).getId()));

		Assertions.assertEquals(6, match.getItemList().size());

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(3, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(14, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(15, (int) r.getExternalList().get(1).getId());
		Assertions.assertEquals(16, (int) r.getExternalList().get(2).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(13, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(17, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(18, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(4);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(14, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(19, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(20, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(5);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(3, r.getExternalList().size());

		Assertions.assertEquals(15, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(21, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(22, (int) r.getExternalList().get(1).getId());
		Assertions.assertEquals(23, (int) r.getExternalList().get(2).getId());
	}


	@Test
	public void testManyToManyBigger() {
		ReconcileTradeIntradayManyToManyMatcher<ReconcileTradeIntradayExternalGrouping> matcher = new ReconcileTradeIntradayManyToManyMatcher<>();
		List<TradeFill> fillList = new ArrayList<>();
		List<ReconcileTradeIntradayExternalGrouping> externalList = new ArrayList<>();

		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 10));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 11));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 12));
		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 13));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 14));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 15));
		fillList.add(createFill(BigDecimal.valueOf(2), BigDecimal.valueOf(166), 16));
		fillList.add(createFill(BigDecimal.valueOf(4), BigDecimal.valueOf(158), 17));
		fillList.add(createFill(BigDecimal.valueOf(3), BigDecimal.valueOf(148), 18));

		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 10));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 11));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 12));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 13));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 14));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 15));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 16));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 17));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 18));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 19));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 20));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 21));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 22));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 23));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(165), 24));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(167), 25));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(160), 26));
		externalList.add(createExternal(BigDecimal.valueOf(3), BigDecimal.valueOf(157.33333333333333333), 27));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(152), 28));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(142), 29));
		externalList.add(createExternal(BigDecimal.valueOf(1), BigDecimal.valueOf(150), 30));

		MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> match = matcher.match(fillList, externalList);

		match.getItemList().sort(Comparator.comparing(o -> o.getInternalList().get(0).getId()));

		//Print out for debugging
		//		System.out.println("{");
		//		System.out.println("\t" + match.getScore());
		//		for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> i : CollectionUtils.getIterable(match.getItemList())) {
		//			System.out.println("\t" + i);
		//		}
		//		System.out.println("}");

		MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> r = match.getItemList().get(0);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(10, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(10, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(11, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(1);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(11, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(12, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(13, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(2);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(3, r.getExternalList().size());

		Assertions.assertEquals(12, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(14, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(15, (int) r.getExternalList().get(1).getId());
		Assertions.assertEquals(16, (int) r.getExternalList().get(2).getId());

		r = match.getItemList().get(3);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(13, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(17, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(18, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(4);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(14, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(19, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(20, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(5);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(3, r.getExternalList().size());

		Assertions.assertEquals(15, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(21, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(22, (int) r.getExternalList().get(1).getId());
		Assertions.assertEquals(23, (int) r.getExternalList().get(2).getId());

		r = match.getItemList().get(6);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(16, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(24, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(25, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(7);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(2, r.getExternalList().size());

		Assertions.assertEquals(17, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(26, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(27, (int) r.getExternalList().get(1).getId());

		r = match.getItemList().get(8);
		Assertions.assertEquals(1, r.getInternalList().size());
		Assertions.assertEquals(3, r.getExternalList().size());

		Assertions.assertEquals(18, (int) r.getInternalList().get(0).getId());

		Assertions.assertEquals(28, (int) r.getExternalList().get(0).getId());
		Assertions.assertEquals(29, (int) r.getExternalList().get(1).getId());
		Assertions.assertEquals(30, (int) r.getExternalList().get(2).getId());
	}


	private TradeFill createFill(BigDecimal quantity, BigDecimal price, int id) {
		TradeFill fill = new TradeFill();
		fill.setQuantity(quantity);
		fill.setNotionalUnitPrice(price);
		fill.setId(id);
		fill.setTrade(TradeBuilder.createNewTrade().withId(42).build());
		return fill;
	}


	private ReconcileTradeIntradayExternalGrouping createExternal(BigDecimal quantity, BigDecimal price, int id) {
		return createExternal(quantity, price, id, null, null);
	}


	private ReconcileTradeIntradayExternalGrouping createExternal(BigDecimal quantity, BigDecimal price, int id, List<Integer> childQty, List<Integer> childIds) {
		ReconcileTradeIntradayExternalGrouping fill = new ReconcileTradeIntradayExternalGrouping();
		fill.setQuantity(quantity);
		fill.setPrice(price);
		fill.setId(id);
		fill.setExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany());

		if (childQty != null && childIds != null) {
			for (int i = 0; i < childQty.size(); i++) {
				ReconcileTradeIntradayExternal ext = new ReconcileTradeIntradayExternal();
				ext.setQuantity(BigDecimal.valueOf(childQty.get(i)));
				ext.setPrice(price);
				ext.setId(childIds.get(i));
				ext.setExecutingBrokerCompany(BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany());
				if (fill.getExternalTradeList() == null) {
					fill.setExternalTradeList(new ArrayList<>());
				}
				fill.getExternalTradeList().add(ext);
			}
		}
		return fill;
	}
}
