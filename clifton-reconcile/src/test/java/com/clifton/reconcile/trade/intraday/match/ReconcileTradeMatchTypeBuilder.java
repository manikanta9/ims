package com.clifton.reconcile.trade.intraday.match;

import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.workflow.definition.Workflow;

import java.util.Date;


/**
 * @author TerryS
 */
public final class ReconcileTradeMatchTypeBuilder {

	public Workflow workflow;
	private InvestmentGroup investmentGroup;
	private InvestmentAccountGroup clientInvestmentAccountGroup;
	private SystemCondition autoCreateManualMatchCondition;
	private ReconcileTradeMatchSources source;
	private int daysToConfirm; // usually 0: meaning same day
	private boolean systemDefined; // cannot be changed
	private boolean noteRequired;
	private boolean contactRequired;
	private boolean attachmentRequired;
	private boolean autoCreateManualMatch;
	private boolean autoConfirm;
	private String label;
	private String name;
	private String description;
	private short createUserId;
	private Date createDate;
	private short updateUserId;
	private Date updateDate;
	private Short id;


	private ReconcileTradeMatchTypeBuilder() {
	}


	public static ReconcileTradeMatchTypeBuilder aReconcileTradeMatchType() {
		return new ReconcileTradeMatchTypeBuilder();
	}


	public ReconcileTradeMatchTypeBuilder withInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withClientInvestmentAccountGroup(InvestmentAccountGroup clientInvestmentAccountGroup) {
		this.clientInvestmentAccountGroup = clientInvestmentAccountGroup;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withAutoCreateManualMatchCondition(SystemCondition autoCreateManualMatchCondition) {
		this.autoCreateManualMatchCondition = autoCreateManualMatchCondition;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withSource(ReconcileTradeMatchSources source) {
		this.source = source;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withDaysToConfirm(int daysToConfirm) {
		this.daysToConfirm = daysToConfirm;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withNoteRequired(boolean noteRequired) {
		this.noteRequired = noteRequired;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withContactRequired(boolean contactRequired) {
		this.contactRequired = contactRequired;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withAttachmentRequired(boolean attachmentRequired) {
		this.attachmentRequired = attachmentRequired;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withAutoCreateManualMatch(boolean autoCreateManualMatch) {
		this.autoCreateManualMatch = autoCreateManualMatch;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withAutoConfirm(boolean autoConfirm) {
		this.autoConfirm = autoConfirm;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withWorkflow(Workflow workflow) {
		this.workflow = workflow;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withLabel(String label) {
		this.label = label;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withName(String name) {
		this.name = name;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withDescription(String description) {
		this.description = description;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withCreateUserId(short createUserId) {
		this.createUserId = createUserId;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withCreateDate(Date createDate) {
		this.createDate = createDate;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withUpdateUserId(short updateUserId) {
		this.updateUserId = updateUserId;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
		return this;
	}


	public ReconcileTradeMatchTypeBuilder withId(Short id) {
		this.id = id;
		return this;
	}


	public ReconcileTradeMatchType build() {
		ReconcileTradeMatchType reconcileTradeMatchType = new ReconcileTradeMatchType();
		reconcileTradeMatchType.setInvestmentGroup(this.investmentGroup);
		reconcileTradeMatchType.setClientInvestmentAccountGroup(this.clientInvestmentAccountGroup);
		reconcileTradeMatchType.setAutoCreateManualMatchCondition(this.autoCreateManualMatchCondition);
		reconcileTradeMatchType.setSource(this.source);
		reconcileTradeMatchType.setDaysToConfirm(this.daysToConfirm);
		reconcileTradeMatchType.setSystemDefined(this.systemDefined);
		reconcileTradeMatchType.setNoteRequired(this.noteRequired);
		reconcileTradeMatchType.setContactRequired(this.contactRequired);
		reconcileTradeMatchType.setAttachmentRequired(this.attachmentRequired);
		reconcileTradeMatchType.setAutoCreateManualMatch(this.autoCreateManualMatch);
		reconcileTradeMatchType.setAutoConfirm(this.autoConfirm);
		reconcileTradeMatchType.setWorkflow(this.workflow);
		reconcileTradeMatchType.setLabel(this.label);
		reconcileTradeMatchType.setName(this.name);
		reconcileTradeMatchType.setDescription(this.description);
		reconcileTradeMatchType.setCreateUserId(this.createUserId);
		reconcileTradeMatchType.setCreateDate(this.createDate);
		reconcileTradeMatchType.setUpdateUserId(this.updateUserId);
		reconcileTradeMatchType.setUpdateDate(this.updateDate);
		reconcileTradeMatchType.setId(this.id);
		return reconcileTradeMatchType;
	}
}
