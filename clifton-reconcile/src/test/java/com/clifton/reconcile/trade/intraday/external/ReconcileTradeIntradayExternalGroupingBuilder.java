package com.clifton.reconcile.trade.intraday.external;

import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author TerryS
 */
public final class ReconcileTradeIntradayExternalGroupingBuilder {

	private Integer totalTrades;
	private List<ReconcileTradeIntradayExternal> externalTradeList;
	private boolean reconciled;
	private InvestmentAccount holdingInvestmentAccount;
	private String holdingInvestmentAccountNumber;
	private BusinessCompany executingBrokerCompany;
	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity payingSecurity;
	private boolean buy;
	private Date tradeDate;
	private BigDecimal quantity;
	private BigDecimal price;
	private BigDecimal originalPrice;
	private ReconcileTradeIntradayExternalStatuses status;
	private String externalUniqueTradeId;
	private BusinessCompany sourceCompany;
	private List<ReconcileTradeIntradayMatch> matchList;
	private Integer id;


	private ReconcileTradeIntradayExternalGroupingBuilder() {
	}


	public static ReconcileTradeIntradayExternalGroupingBuilder aReconcileTradeIntradayExternalGrouping() {
		return new ReconcileTradeIntradayExternalGroupingBuilder();
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withTotalTrades(Integer totalTrades) {
		this.totalTrades = totalTrades;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withExternalTradeList(List<ReconcileTradeIntradayExternal> externalTradeList) {
		this.externalTradeList = externalTradeList;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withReconciled(boolean reconciled) {
		this.reconciled = reconciled;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withPayingSecurity(InvestmentSecurity payingSecurity) {
		this.payingSecurity = payingSecurity;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withBuy(boolean buy) {
		this.buy = buy;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withQuantity(BigDecimal quantity) {
		this.quantity = quantity;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withPrice(BigDecimal price) {
		this.price = price;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withStatus(ReconcileTradeIntradayExternalStatuses status) {
		this.status = status;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withExternalUniqueTradeId(String externalUniqueTradeId) {
		this.externalUniqueTradeId = externalUniqueTradeId;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withSourceCompany(BusinessCompany sourceCompany) {
		this.sourceCompany = sourceCompany;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withMatchList(List<ReconcileTradeIntradayMatch> matchList) {
		this.matchList = matchList;
		return this;
	}


	public ReconcileTradeIntradayExternalGroupingBuilder withId(Integer id) {
		this.id = id;
		return this;
	}


	public ReconcileTradeIntradayExternalGrouping build() {
		ReconcileTradeIntradayExternalGrouping reconcileTradeIntradayExternalGrouping = new ReconcileTradeIntradayExternalGrouping();
		reconcileTradeIntradayExternalGrouping.setTotalTrades(this.totalTrades);
		reconcileTradeIntradayExternalGrouping.setExternalTradeList(this.externalTradeList);
		reconcileTradeIntradayExternalGrouping.setReconciled(this.reconciled);
		reconcileTradeIntradayExternalGrouping.setHoldingInvestmentAccount(this.holdingInvestmentAccount);
		reconcileTradeIntradayExternalGrouping.setHoldingInvestmentAccountNumber(this.holdingInvestmentAccountNumber);
		reconcileTradeIntradayExternalGrouping.setExecutingBrokerCompany(this.executingBrokerCompany);
		reconcileTradeIntradayExternalGrouping.setInvestmentSecurity(this.investmentSecurity);
		reconcileTradeIntradayExternalGrouping.setPayingSecurity(this.payingSecurity);
		reconcileTradeIntradayExternalGrouping.setBuy(this.buy);
		reconcileTradeIntradayExternalGrouping.setTradeDate(this.tradeDate);
		reconcileTradeIntradayExternalGrouping.setQuantity(this.quantity);
		reconcileTradeIntradayExternalGrouping.setPrice(this.price);
		reconcileTradeIntradayExternalGrouping.setOriginalPrice(this.originalPrice);
		reconcileTradeIntradayExternalGrouping.setStatus(this.status);
		reconcileTradeIntradayExternalGrouping.setExternalUniqueTradeId(this.externalUniqueTradeId);
		reconcileTradeIntradayExternalGrouping.setSourceCompany(this.sourceCompany);
		reconcileTradeIntradayExternalGrouping.setMatchList(this.matchList);
		reconcileTradeIntradayExternalGrouping.setId(this.id);
		return reconcileTradeIntradayExternalGrouping;
	}
}
