package com.clifton.reconcile.trade.intraday.matcher.util;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.trade.Trade;
import com.clifton.trade.builder.TradeBuilder;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


public class ReconcileTradeIntradayUtilsTests {

	@Test
	public void testFillTrades() {
		TradeBuilder tradeBuilder = TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FAU3").withPriceMultiplier(100).build());
		List<Trade> tradeList = new ArrayList<>();
		tradeList.add(tradeBuilder.withId(40)
				.withExpectedUnitPrice("101")
				.withQuantityIntended(10)
				.build());
		tradeList.add(tradeBuilder.withId(45)
				.withExpectedUnitPrice("102")
				.withQuantityIntended(20)
				.build());

		List<ExternalFillDetail> detailList = new ArrayList<>();
		detailList.add(new ExternalFillDetail("1", "H-200", new BigDecimal("103"), new BigDecimal("5")));
		detailList.add(new ExternalFillDetail("2", "H-200", new BigDecimal("102"), new BigDecimal("7")));
		detailList.add(new ExternalFillDetail("3", "H-200", new BigDecimal("105"), new BigDecimal("3")));
		detailList.add(new ExternalFillDetail("4", "H-200", new BigDecimal("110"), new BigDecimal("5")));
		detailList.add(new ExternalFillDetail("5", "H-200", new BigDecimal("108"), new BigDecimal("10")));

		List<TradeFillResult> result = ReconcileTradeIntradayUtils.fillTrades(tradeList, detailList);

		Assertions.assertEquals(6, result.size());

		Assertions.assertEquals("40	3	3	105", result.get(0).toStringFormatted(false));
		Assertions.assertEquals("40	1	5	103", result.get(1).toStringFormatted(false));
		Assertions.assertEquals("40	4	2	110", result.get(2).toStringFormatted(false));
		Assertions.assertEquals("45	4	3	110", result.get(3).toStringFormatted(false));
		Assertions.assertEquals("45	2	7	102", result.get(4).toStringFormatted(false));
		Assertions.assertEquals("45	5	10	108", result.get(5).toStringFormatted(false));
	}


	@Test
	public void testToManyExternalFills() {
		List<Trade> tradeList = new ArrayList<>();
		tradeList.add(TradeBuilder.newTradeForSecurity(InvestmentSecurityBuilder.newFuture("FAU3").withPriceMultiplier(100).build())
				.withId(40)
				.withExpectedUnitPrice("101")
				.withQuantityIntended(10)
				.build());

		List<ExternalFillDetail> detailList = new ArrayList<>();
		detailList.add(new ExternalFillDetail("1", "H-200", new BigDecimal("103"), new BigDecimal("5")));
		detailList.add(new ExternalFillDetail("2", "H-200", new BigDecimal("102"), new BigDecimal("7")));
		detailList.add(new ExternalFillDetail("3", "H-200", new BigDecimal("105"), new BigDecimal("3")));
		detailList.add(new ExternalFillDetail("4", "H-200", new BigDecimal("110"), new BigDecimal("5")));
		detailList.add(new ExternalFillDetail("5", "H-200", new BigDecimal("108"), new BigDecimal("10")));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			List<TradeFillResult> result = ReconcileTradeIntradayUtils.fillTrades(tradeList, detailList);

			Assertions.assertEquals(6, result.size());

			Assertions.assertEquals("40	3	3	105", result.get(0).toStringFormatted(false));
			Assertions.assertEquals("40	1	5	103", result.get(1).toStringFormatted(false));
			Assertions.assertEquals("40	4	2	110", result.get(2).toStringFormatted(false));
			Assertions.assertEquals("45	4	3	110", result.get(3).toStringFormatted(false));
			Assertions.assertEquals("45	2	7	102", result.get(4).toStringFormatted(false));
			Assertions.assertEquals("45	5	10	108", result.get(5).toStringFormatted(false));
		});
		MatcherAssert.assertThat(validationException.getMessage(), StringContains.containsString("Cannot fill trades with total quantity of [10] with fills of quantity [30]."));
	}
}
