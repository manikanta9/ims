package com.clifton.reconcile.trade.intraday.matcher;

import com.clifton.business.company.BusinessCompany;
import com.clifton.business.company.BusinessCompanyBuilder;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.converter.json.jackson.strategy.JacksonHibernateStrategy;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountBuilder;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalBuilder;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalStatuses;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchSources;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchTypeBuilder;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeMatchTypeSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.builder.TradeBuilder;
import com.clifton.trade.builder.TradeDestinationBuilder;
import com.clifton.trade.builder.TradeExecutionTypeBuilder;
import com.clifton.trade.builder.TradeFillBuilder;
import com.clifton.trade.builder.TradeOpenCloseTypeBuilder;
import com.clifton.trade.builder.TradeTypeBuilder;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStateBuilder;
import com.clifton.workflow.transition.WorkflowTransitionService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


@ExtendWith({SpringExtension.class, MockitoExtension.class})
@ContextConfiguration
class ReconcileTradeIntradayMatchHandlerImplTests {

	private final ObjectMapper mapper = new JacksonObjectMapper(new JacksonHibernateStrategy());

	@Resource
	private ReconcileTradeIntradayMatchHandlerImpl reconcileTradeIntradayMatchHandler;

	@Mock
	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;

	@Mock
	private WorkflowDefinitionService workflowDefinitionService;

	@Mock
	private WorkflowTransitionService workflowTransitionService;

	@Mock
	private TradeService tradeService;

	private BusinessCompany goldman;
	private InvestmentAccount holdingAccount;

	private Trade trade;
	private List<TradeFill> tradeFills;

	private final Date tradeDate = DateUtils.toDate("08/03/2020");
	private final InvestmentSecurity future = InvestmentSecurityBuilder.newFuture("MESU0").toInvestmentSecurity();

	private final WorkflowState bookedState = WorkflowStateBuilder.createBooked().toWorkflowState();
	private final WorkflowState unBookedState = WorkflowStateBuilder.createUnBooked().toWorkflowState();

	private List<ReconcileTradeIntradayExternalGrouping> groupingList;


	@BeforeEach
	public void setup() throws IOException {
		this.goldman = BusinessCompanyBuilder.createGoldmanSachs().toBusinessCompany();
		this.holdingAccount = InvestmentAccountBuilder.createTestHoldingAccount1().toInvestmentAccount();
		this.trade = TradeBuilder.createEmptyTrade()
				.withHoldingInvestmentAccount(this.holdingAccount)
				.withExecutingBroker(this.goldman)
				.withInvestmentSecurity(this.future)
				.build();

		setupExternalTrades();

		this.reconcileTradeIntradayMatchHandler.setReconcileTradeIntradayMatchService(this.reconcileTradeIntradayMatchService);
		this.reconcileTradeIntradayMatchHandler.setTradeService(this.tradeService);
		this.reconcileTradeIntradayMatchHandler.setWorkflowDefinitionService(this.workflowDefinitionService);
		this.reconcileTradeIntradayMatchHandler.setWorkflowTransitionService(this.workflowTransitionService);

		Mockito.when(this.reconcileTradeIntradayMatchService.getReconcileTradeMatchTypeList(Mockito.any(ReconcileTradeMatchTypeSearchForm.class))).thenAnswer(i -> {
			ReconcileTradeMatchTypeSearchForm searchForm = i.getArgument(0, ReconcileTradeMatchTypeSearchForm.class);
			Assertions.assertNotNull(searchForm.getSource());
			ReconcileTradeMatchSources sourceName = searchForm.getSource();
			if (sourceName == ReconcileTradeMatchSources.FILE && searchForm.getSystemDefined()) {
				return Stream.of(
						ReconcileTradeMatchTypeBuilder.aReconcileTradeMatchType()
								.withId((short) 1)
								.withName("External File")
								.withAutoConfirm(true)
								.withSource(ReconcileTradeMatchSources.FILE)
								.withSystemDefined(true)
								.build()
				).collect(Collectors.toList());
			}
			else {
				Assertions.fail();
			}
			return null;
		});
	}


	private void setupExternalTrades() throws IOException {
		ClassPathResource data = new ClassPathResource(getClass().getPackage().getName().replaceAll("\\.", "/") + "/data/" + this.getClass().getSimpleName() + ".json");
		ReconcileTradeIntradayExternal[] externals;
		try (InputStream inputStream = data.getInputStream()) {
			externals = this.mapper.readValue(inputStream, ReconcileTradeIntradayExternal[].class);
		}
		ReconcileTradeIntradayExternal template = ReconcileTradeIntradayExternalBuilder.aReconcileTradeIntradayExternal()
				.withHoldingInvestmentAccount(this.holdingAccount)
				.withHoldingInvestmentAccountNumber("003-11341")
				.withExecutingBrokerCompany(this.goldman)
				.withInvestmentSecurity(this.future)
				.withTradeDate(this.tradeDate)
				.withStatus(ReconcileTradeIntradayExternalStatuses.CONFIRMED)
				.withSourceCompany(BusinessCompanyBuilder.createTestingCompany3().toBusinessCompany())
				.build();
		String[] ignoreProperties = new String[]{
				"id",
				"quantity",
				"price",
				"originalPrice",
				"externalUniqueTradeId"
		};
		List<ReconcileTradeIntradayExternal> externalTradeList = Arrays.stream(externals).peek(e -> BeanUtils.copyProperties(template, e, ignoreProperties)).collect(Collectors.toList());
		this.groupingList = externalTradeList.stream()
				.collect(Collectors.groupingBy(ReconcileTradeIntradayExternal::getPrice))
				.values()
				.stream()
				.map(l -> {
					ReconcileTradeIntradayExternal first = l.iterator().next();
					ReconcileTradeIntradayExternalGrouping grouping = new ReconcileTradeIntradayExternalGrouping();
					BeanUtils.copyProperties(first, grouping);
					grouping.setQuantity(l.stream().map(ReconcileTradeIntradayExternal::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add));
					grouping.setExternalTradeList(l);
					return grouping;
				})
				.collect(Collectors.toList());
	}


	@Test
	public void testMatchWithBrokerPrice() {
		updateMatchWithBrokerPrice();

		Mockito.when(this.workflowDefinitionService.getWorkflowStateNextAllowedList("Trade", BeanUtils.getIdentityAsLong(this.trade)))
				.thenReturn(Arrays.asList(this.bookedState, this.unBookedState));

		List<TradeFill> tradeFillList = new ArrayList<>(this.trade.getTradeFillList());
		ReconcileTradeIntradayMatchObject beanList = new ReconcileTradeIntradayMatchObject();
		beanList.setTradeFillList(tradeFillList);
		Mockito.when(this.tradeService.getTradeFillListByTrade(Mockito.anyInt())).thenReturn(tradeFillList);
		beanList.setMatchBrokerPrice(true);
		beanList.setExternalGroupingList(this.groupingList);

		this.reconcileTradeIntradayMatchHandler.createTradeMatch(beanList);
		verifyUpdateMatchWithBrokerPrice();
	}


	private void updateMatchWithBrokerPrice() {
		this.trade.setId(2373187);
		this.trade.setTradeType(TradeTypeBuilder.createFutures().toTradeType());
		this.trade.setQuantityIntended(new BigDecimal("179.0000000000"));
		this.trade.setOriginalFace(new BigDecimal("179.00"));
		this.trade.setAccountingNotional(new BigDecimal("9620355.00"));
		this.trade.setExpectedUnitPrice(new BigDecimal("1069.300000000000000"));
		this.trade.setAverageUnitPrice(new BigDecimal("1074.900000000000000"));
		this.trade.setCommissionPerUnit(new BigDecimal("2.2100000000"));
		this.trade.setCommissionAmount(new BigDecimal("-395.59"));
		this.trade.setBuy(false);
		this.trade.setOpenCloseType(TradeOpenCloseTypeBuilder.sell().build());
		this.trade.setTradeExecutionType(TradeExecutionTypeBuilder.moc().build());
		this.trade.setTradeDestination(TradeDestinationBuilder.createRediTicket().toTradeDestination());
		this.trade.setDescription("Goldman Sachs REDI trade ticket via FIX.");
		this.trade.setInvestmentSecurity(this.future);
		this.trade.setPayingSecurity(InvestmentSecurityBuilder.newUSD());
		this.trade.setTradeDate(this.tradeDate);
		this.trade.setSettlementDate(this.tradeDate);
		this.trade.setWorkflowState(WorkflowStateBuilder.createBooked().toWorkflowState());

		this.tradeFills = Stream.of(
				TradeFillBuilder.aTradeFill()
						.withId(2621994)
						.withNotionalUnitPrice("1074.979888268156425")
						.withQuantity("113.0000000000")
						.withPaymentUnitPrice("0.000000000000000")
						.withPaymentTotalPrice(new BigDecimal("0.00"))
						.withNotionalTotalPrice(new BigDecimal("6073636.37"))
						.withTrade(this.trade)
						.withOpenCloseType(TradeOpenCloseTypeBuilder.sellToClose().build())
						.build(),
				TradeFillBuilder.aTradeFill()
						.withId(2621995)
						.withNotionalUnitPrice("1074.979888268156425")
						.withQuantity("66.0000000000")
						.withPaymentTotalPrice(new BigDecimal("0.00"))
						.withPaymentUnitPrice("0.000000000000000")
						.withNotionalTotalPrice(new BigDecimal("3547433.63"))
						.withTrade(this.trade)
						.withOpenCloseType(TradeOpenCloseTypeBuilder.sellToOpen().build())
						.build()
		).collect(Collectors.toList());

		this.trade.setTradeFillList(this.tradeFills);
	}


	@SuppressWarnings("unchecked")
	private void verifyUpdateMatchWithBrokerPrice() {
		ArgumentCaptor<List<TradeFill>> tradeFillDeleteCaptor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.tradeService).deleteTradeFillList(tradeFillDeleteCaptor.capture());
		Assertions.assertTrue(this.tradeFills.containsAll(tradeFillDeleteCaptor.getValue()));

		Mockito.verify(this.tradeService, Mockito.times(2)).getTradeFillListByTrade(this.trade.getId());
		Mockito.verify(this.workflowTransitionService).executeWorkflowTransitionSystemDefined("Trade", this.trade.getId(), this.unBookedState.getId());

		ArgumentCaptor<List<TradeFill>> tradeFillSaveCaptor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.tradeService).saveTradeFillList(tradeFillSaveCaptor.capture());
		Assertions.assertEquals(tradeFillSaveCaptor.getValue().size(), this.groupingList.size());

		ArgumentCaptor<List<ReconcileTradeIntradayMatch>> matchSaveCaptor = ArgumentCaptor.forClass(List.class);
		Mockito.verify(this.reconcileTradeIntradayMatchService).saveReconcileTradeIntradayMatchList(matchSaveCaptor.capture());
		List<ReconcileTradeIntradayMatch> savedMatches = matchSaveCaptor.getValue();
		Assertions.assertEquals(137, savedMatches.size());

		for (ReconcileTradeIntradayMatch savedMatch : savedMatches) {
			Assertions.assertEquals(0, savedMatch.getReconcileTradeIntradayExternal().getPrice().compareTo(savedMatch.getTradeFill().getNotionalUnitPrice()));
		}
	}
}
