package com.clifton.reconcile.trade.intraday.external;

import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author TerryS
 */
public final class ReconcileTradeIntradayExternalBuilder {

	private InvestmentAccount holdingInvestmentAccount;
	private String holdingInvestmentAccountNumber;
	private BusinessCompany executingBrokerCompany;
	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity payingSecurity;
	private boolean buy;
	private Date tradeDate;
	private BigDecimal quantity;
	private BigDecimal price;
	private BigDecimal originalPrice;
	private ReconcileTradeIntradayExternalStatuses status;
	private String externalUniqueTradeId;
	private BusinessCompany sourceCompany;
	private List<ReconcileTradeIntradayMatch> matchList;
	private Integer id;


	private ReconcileTradeIntradayExternalBuilder() {
	}


	public static ReconcileTradeIntradayExternalBuilder aReconcileTradeIntradayExternal() {
		return new ReconcileTradeIntradayExternalBuilder();
	}


	public ReconcileTradeIntradayExternalBuilder withHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withPayingSecurity(InvestmentSecurity payingSecurity) {
		this.payingSecurity = payingSecurity;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withBuy(boolean buy) {
		this.buy = buy;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withQuantity(BigDecimal quantity) {
		this.quantity = quantity;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withPrice(BigDecimal price) {
		this.price = price;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withStatus(ReconcileTradeIntradayExternalStatuses status) {
		this.status = status;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withExternalUniqueTradeId(String externalUniqueTradeId) {
		this.externalUniqueTradeId = externalUniqueTradeId;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withSourceCompany(BusinessCompany sourceCompany) {
		this.sourceCompany = sourceCompany;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withMatchList(List<ReconcileTradeIntradayMatch> matchList) {
		this.matchList = matchList;
		return this;
	}


	public ReconcileTradeIntradayExternalBuilder withId(Integer id) {
		this.id = id;
		return this;
	}


	public ReconcileTradeIntradayExternal build() {
		ReconcileTradeIntradayExternal reconcileTradeIntradayExternal = new ReconcileTradeIntradayExternal();
		reconcileTradeIntradayExternal.setHoldingInvestmentAccount(this.holdingInvestmentAccount);
		reconcileTradeIntradayExternal.setHoldingInvestmentAccountNumber(this.holdingInvestmentAccountNumber);
		reconcileTradeIntradayExternal.setExecutingBrokerCompany(this.executingBrokerCompany);
		reconcileTradeIntradayExternal.setInvestmentSecurity(this.investmentSecurity);
		reconcileTradeIntradayExternal.setPayingSecurity(this.payingSecurity);
		reconcileTradeIntradayExternal.setBuy(this.buy);
		reconcileTradeIntradayExternal.setTradeDate(this.tradeDate);
		reconcileTradeIntradayExternal.setQuantity(this.quantity);
		reconcileTradeIntradayExternal.setPrice(this.price);
		reconcileTradeIntradayExternal.setOriginalPrice(this.originalPrice);
		reconcileTradeIntradayExternal.setStatus(this.status);
		reconcileTradeIntradayExternal.setExternalUniqueTradeId(this.externalUniqueTradeId);
		reconcileTradeIntradayExternal.setSourceCompany(this.sourceCompany);
		reconcileTradeIntradayExternal.setMatchList(this.matchList);
		reconcileTradeIntradayExternal.setId(this.id);
		return reconcileTradeIntradayExternal;
	}
}
