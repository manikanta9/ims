package com.clifton.reconcile;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ReconcileProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "reconcile";
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> result = super.getAllowedContextManagedBeanSuffixNames();
		result.add("Matcher");
		return result;
	}


	@Override
	public boolean isMethodSkipped(Method method) {
		Set<String> methodsToSkip = new HashSet<>();
		methodsToSkip.add("getReconcilePositionAccountExtendedList");
		methodsToSkip.add("saveReconcilePositionExternalPriceOverride");

		methodsToSkip.add("getTradeExtendedList");
		methodsToSkip.add("getTradeFillExtendedList");
		methodsToSkip.add("getTradeSettlementList");
		methodsToSkip.add("getReconcileTradeIntradaySecurityList");
		methodsToSkip.add("saveReconcileTradeIntradayMatch");
		methodsToSkip.add("getReconcilePositionExternalDailyGroupingList");
		methodsToSkip.add("getReconcilePositionAccountingDailyGroupingList");

		return methodsToSkip.contains(method.getName());
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveReconcileTradeIntradayManualMatchForTrade");
		ignoredVoidSaveMethodSet.add("saveReconcileTradeIntradayMatchObject");
		return ignoredVoidSaveMethodSet;
	}
}
