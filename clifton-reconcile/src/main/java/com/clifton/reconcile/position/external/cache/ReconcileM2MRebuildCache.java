package com.clifton.reconcile.position.external.cache;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.reconcile.position.external.ReconcilePositionExternalPriceOverride;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Caches data for M2M rebuild.
 *
 * @author mwacker
 */
public class ReconcileM2MRebuildCache {

	private final Map<String, AccountingM2MDaily> m2MDailyCache = new HashMap<>();
	private final Map<Integer, Boolean> markTypeCache = new HashMap<>();
	private final Map<String, AccountingM2MDailyExpense> m2MDailyExpenseCache = new HashMap<>();
	private AccountingM2MDailyExpenseType expenseType;
	private AccountingM2MService accountingM2MService;

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public ReconcileM2MRebuildCache(AccountingM2MService accountingM2MService, AccountingM2MDailyExpenseType expenseType) {
		this.accountingM2MService = accountingM2MService;
		this.expenseType = expenseType;
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public AccountingM2MDaily getM2M(AccountingM2MDaily m2mDaily) {
		return getM2M(m2mDaily.getHoldingInvestmentAccount(), m2mDaily.getMarkDate(), m2mDaily.getMarkCurrency());
	}


	public AccountingM2MDaily getM2M(InvestmentAccount holdingAccount, Date markDate, InvestmentSecurity markCurrency) {
		String key = getM2MKey(holdingAccount, markDate, markCurrency);
		if (!this.m2MDailyCache.containsKey(key)) {
			AccountingM2MDaily m2mDaily = getAccountingM2MService().getAccountingM2MDailyByHoldingAccount(holdingAccount, markDate, isM2mInLocalCurrency(holdingAccount) ? markCurrency : null);
			this.m2MDailyCache.put(key, m2mDaily);
			return m2mDaily;
		}
		return this.m2MDailyCache.get(key);
	}


	/**
	 * Gets the list of M2M items that need to be saved.
	 */
	public List<AccountingM2MDaily> getM2MList() {
		return new ArrayList<>(this.m2MDailyCache.values());
	}


	public boolean isM2mInLocalCurrency(InvestmentAccount holdingAccount) {
		if (!this.markTypeCache.containsKey(holdingAccount.getId())) {
			boolean m2mInLocalCurrency = getAccountingM2MService().isM2MinLocalCurrency(holdingAccount);
			this.markTypeCache.put(holdingAccount.getId(), m2mInLocalCurrency);
		}
		return this.markTypeCache.get(holdingAccount.getId());
	}


	public void putM2MExpense(AccountingM2MDailyExpense expense) {
		this.m2MDailyExpenseCache.put(getM2MExpenseKey(expense), expense);
	}


	public AccountingM2MDailyExpense getM2MExpense(AccountingM2MDaily m2mDaily, ReconcilePositionExternalPriceOverride override) {
		String key = getM2MExpenseKey(m2mDaily, override.getId());
		if (!this.m2MDailyExpenseCache.containsKey(key)) {
			AccountingM2MDailyExpense expense = getAccountingM2MService().getAccountingM2MDailyExpenseForSpecificMark(m2mDaily, getExpenseType(), m2mDaily.getHoldingInvestmentAccount().getBaseCurrency(), override.getId());
			this.m2MDailyExpenseCache.put(key, expense);
			return expense;
		}
		return this.m2MDailyExpenseCache.get(key);
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	private String getM2MKey(InvestmentAccount holdingAccount, Date markDate, InvestmentSecurity markCurrency) {
		return BeanUtils.createKeyFromBeans(holdingAccount, markDate, isM2mInLocalCurrency(holdingAccount) ? markCurrency : "");
	}


	private String getM2MExpenseKey(AccountingM2MDailyExpense expense) {
		return getM2MExpenseKey(expense.getM2mDaily(), expense.getSourceFkFieldId());
	}


	private String getM2MExpenseKey(AccountingM2MDaily m2mDaily, Integer overrideId) {
		return overrideId + ":" + m2mDaily.getId();
	}


	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public AccountingM2MDailyExpenseType getExpenseType() {
		return this.expenseType;
	}


	public void setExpenseType(AccountingM2MDailyExpenseType expenseType) {
		this.expenseType = expenseType;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}
}
