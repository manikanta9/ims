package com.clifton.reconcile.position.processor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.processor.ReconcileRule;

import java.math.BigDecimal;
import java.util.Map;


public class ReconcilePositionQuantityRule implements ReconcileRule<ReconcilePosition> {

	@Override
	public void reconcile(ReconcilePosition bean, @SuppressWarnings("unused") Map<String, Object> context) {
		if (!bean.isQuantityReconciled() && !compareProperty(bean, "Quantity")) {
			bean.setQuantityReconciled(false);
		}
		else {
			bean.setQuantityReconciled(true);
		}
	}


	public boolean compareProperty(ReconcilePosition position, String name) {
		Object ours = BeanUtils.getPropertyValue(position, "our" + name);
		Object external = BeanUtils.getPropertyValue(position, "external" + name);
		ours = ours == null ? BigDecimal.ZERO : ours;
		external = external == null ? BigDecimal.ZERO : external;
		if ((ours instanceof BigDecimal) && (external instanceof BigDecimal)) {
			BigDecimal o = (BigDecimal) ours;
			BigDecimal e = (BigDecimal) external;
			return o.compareTo(e) == 0;
		}
		return ours.equals(external);
	}


	@Override
	public String getPropertyName() {
		return "quantityReconciled";
	}
}
