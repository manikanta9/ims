package com.clifton.reconcile.position;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.accounting.gl.AccountingTransactionService;
import com.clifton.accounting.gl.journal.AccountingJournalType;
import com.clifton.accounting.gl.journal.adjust.AccountingAdjustingJournalService;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyRebuildCommand;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.search.AccountingTransactionSearchForm;
import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.SubselectParameterExpression;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.marketdata.api.rates.FxRateLookupCommand;
import com.clifton.marketdata.api.rates.MarketDataExchangeRatesApiService;
import com.clifton.reconcile.position.cache.ReconcilePositionDefinitionListCache;
import com.clifton.reconcile.position.comparison.ReconcilePositionComparison;
import com.clifton.reconcile.position.processor.ReconcilePositionCommand;
import com.clifton.reconcile.position.processor.ReconcilePositionProcessor;
import com.clifton.reconcile.position.processor.ReconcilePositionRebuildCommand;
import com.clifton.reconcile.position.search.ReconcilePositionAccountExtendedSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionAccountSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionDefinitionSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.type.BooleanType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


@Service
public class ReconcilePositionServiceImpl implements ReconcilePositionService {

	private AdvancedUpdatableDAO<ReconcilePositionDefinition, Criteria> reconcilePositionDefinitionDAO;
	private AdvancedUpdatableDAO<ReconcilePositionAccount, Criteria> reconcilePositionAccountDAO;
	private AdvancedReadOnlyDAO<ReconcilePositionAccountExtended, Criteria> reconcilePositionAccountExtendedDAO;
	private AdvancedUpdatableDAO<ReconcilePosition, Criteria> reconcilePositionDAO;

	private ReconcilePositionDefinitionListCache reconcilePositionDefinitionListCache;

	private AccountingAdjustingJournalService accountingAdjustingJournalService;
	private AccountingPositionDailyService accountingPositionDailyService;
	private AccountingM2MService accountingM2MService;
	private AccountingTransactionService accountingTransactionService;
	private InvestmentAccountService investmentAccountService;
	private MarketDataExchangeRatesApiService marketDataExchangeRatesApiService;

	private RunnerHandler runnerHandler;

	/**
	 * Map of processors for reconciliation definitions.  Key is the name of reconciliation definition.
	 */
	private Map<String, ReconcilePositionProcessor> reconcileProcessorMap = new HashMap<>();
	/**
	 * If no processor is found in the map this one will be used.
	 */
	private ReconcilePositionProcessor defaultReconcileProcessor;


	////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionDefinition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcilePositionDefinition getReconcilePositionDefinition(int id) {
		return getReconcilePositionDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconcilePositionDefinition> getReconcilePositionDefinitionList() {
		return getReconcilePositionDefinitionListCache().getReconcilePositionDefinitionList();
	}


	@Override
	public List<ReconcilePositionDefinition> getReconcilePositionDefinitionList(final ReconcilePositionDefinitionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientInvestmentAccountGroupId() != null) {
					criteria.add(Restrictions.sqlRestriction(" EXISTS ("
							+ "SELECT aga.InvestmentAccountGroupAccountID FROM InvestmentAccountGroupAccount aga "
							+ "INNER JOIN InvestmentAccount ca ON aga.InvestmentAccountID = ca.InvestmentAccountID "
							+ "INNER JOIN InvestmentAccountRelationship r ON r.MainAccountID = ca.InvestmentAccountID "
							+ "INNER JOIN InvestmentAccountRelationshipPurpose p ON r.InvestmentAccountRelationshipPurposeID = p.InvestmentAccountRelationshipPurposeID AND p.RelationshipPurposeName = '"
							+ InvestmentAccountRelationshipPurpose.TRADING_FUTURES_PURPOSE_NAME + "' "
							+ "INNER JOIN InvestmentAccount ha ON r.RelatedAccountID = ha.InvestmentAccountID WHERE aga.InvestmentAccountGroupID = " + searchForm.getClientInvestmentAccountGroupId()
							+ " AND ha.IssuingCompanyID = {alias}.HoldingCompanyID)"));
				}
			}
		};
		return getReconcilePositionDefinitionDAO().findBySearchCriteria(config);
	}


	////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionAccountRun Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcilePositionAccount getReconcilePositionAccount(int id) {
		return getReconcilePositionAccountDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconcilePositionAccount> getReconcilePositionAccountList(final ReconcilePositionAccountSearchForm searchForm) {
		return getReconcilePositionAccountDAO().findBySearchCriteria(configureReconcilePositionAccountSearchForm(searchForm));
	}


	@Override
	public ReconcilePositionAccount saveReconcilePositionAccount(ReconcilePositionAccount bean) {
		return getReconcilePositionAccountDAO().save(bean);
	}


	////////////////////////////////////////////////////////////////////////////
	///////    ReconcilePositionAccountExtended Business Methods     ///////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcilePositionAccountExtended> getReconcilePositionAccountExtendedList(ReconcilePositionAccountExtendedSearchForm searchForm) {
		return getReconcilePositionAccountExtendedDAO().findBySearchCriteria(configureReconcilePositionAccountSearchForm(searchForm));
	}


	private HibernateSearchFormConfigurer configureReconcilePositionAccountSearchForm(final ReconcilePositionAccountSearchForm searchForm) {
		return new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				if (searchForm instanceof ReconcilePositionAccountExtendedSearchForm) {
					ReconcilePositionAccountExtendedSearchForm sf = (ReconcilePositionAccountExtendedSearchForm) searchForm;
					if (sf.getExcludeClosedPositions() != null) {
						criteria.add(new SubselectParameterExpression(new BooleanType(), sf.getExcludeClosedPositions()));
					}
					else {
						criteria.add(new SubselectParameterExpression(new BooleanType(), false));
					}
				}

				super.configureCriteria(criteria);

				if (searchForm.getExcludeInvestmentAccountGroupName() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "neLink");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "neGroup");
					sub.createAlias("referenceTwo", "neAcct");
					sub.add(Restrictions.eqProperty("neAcct.id", criteria.getAlias() + ".clientAccount.id"));
					sub.add(Restrictions.eq("neGroup.name", searchForm.getExcludeInvestmentAccountGroupName()));
					criteria.add(Subqueries.notExists(sub));
				}
				if (searchForm.getClientAccountGroupId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "agLink");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "group");
					sub.createAlias("referenceTwo", "acct");
					sub.add(Restrictions.eqProperty("acct.id", criteria.getAlias() + ".clientAccount.id"));
					sub.add(Restrictions.eq("group.id", searchForm.getClientAccountGroupId()));
					criteria.add(Subqueries.exists(sub));
				}
			}
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////          ReconcilePosition Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcilePosition getReconcilePosition(int id) {
		return getReconcilePositionDAO().findByPrimaryKey(id);
	}


	@Override
	@Transactional
	public void unreconcileReconcilePosition(Boolean clearNote, Boolean unreconcileActivity, Boolean unreconcilePosition, Integer[] ids) {
		if (unreconcilePosition) {
			doSetReconciled(null, null, ids, false, clearNote);
		}
		if (unreconcileActivity) {
			doSetReconciled(null, "Activity Reconciliation", ids, false, clearNote);
		}
	}


	@Override
	@Transactional
	public void forceReconcilePosition(String note, String viewName, Integer[] ids) {
		doSetReconciled(note, viewName, ids, true, false);
	}


	private void doSetReconciled(String note, String viewName, Integer[] ids, boolean reconciled, boolean clearNote) {
		if (reconciled) {
			ValidationUtils.assertNotNull(note, "Please enter a note for forced reconciliation.", "note");
		}
		ValidationUtils.assertTrue((ids != null && ids.length > 0), "Please choose positions to " + (reconciled ? "reconcile" : "unreconcile") + ".");

		ReconcilePositionSearchForm searchForm = new ReconcilePositionSearchForm();
		searchForm.setIds(ids);
		List<ReconcilePosition> positions = getReconcilePositionList(searchForm);

		for (ReconcilePosition bean : CollectionUtils.getIterable(positions)) {
			if (viewName != null && viewName.startsWith("Activity Rec")) {
				bean.setRealizedAndCommissionReconciled(reconciled);
				if (reconciled) {
					bean.setRealizedAndCommissionNote(note);
				}
				else if (clearNote) {
					bean.setRealizedAndCommissionNote(null);
				}
			}
			else {
				bean.setReconciled(reconciled);
				bean.setQuantityReconciled(reconciled);
				if (reconciled) {
					bean.setNote(note);
				}
				else if (clearNote) {
					bean.setNote(null);
				}
			}
			saveReconcilePosition(bean);
		}

		ReconcilePositionAccount positionAccount = CollectionUtils.getFirstElementStrict(positions).getPositionAccount();
		ReconcilePositionProcessor processor = getReconcileProcessor(positionAccount.getDefinition());
		processor.reconcile(new ReconcilePositionCommand(positionAccount.getDefinition(), positionAccount.getPositionDate(), true));
	}


	@Override
	public List<ReconcilePosition> getReconcilePositionListByAccount(int reconcilePositionAccountId) {
		return getReconcilePositionDAO().findByField("positionAccount.id", reconcilePositionAccountId);
	}


	@Override
	public List<ReconcilePosition> getReconcilePositionList(final ReconcilePositionSearchForm searchForm) {
		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm != null) {
					if (BooleanUtils.isTrue(searchForm.getExcludeClosedPositions())) {
						criteria.add(Restrictions.not(Restrictions.and(Restrictions.eq("ourQuantity", BigDecimal.ZERO), Restrictions.eq("externalQuantity", BigDecimal.ZERO))));
						//Restrictions.or(Restrictions.eq("externalQuantity", BigDecimal.ZERO), Restrictions.eq("matched", true)))));
					}
					if (BooleanUtils.isTrue(searchForm.getRealizedOrCommissionPresent())) {
						criteria.add(Restrictions.or(
								Restrictions.ne("ourTodayCommissionLocal", BigDecimal.ZERO),
								Restrictions.ne("ourTodayRealizedGainLossLocal", BigDecimal.ZERO),
								Restrictions.ne("externalTodayCommissionLocal", BigDecimal.ZERO),
								Restrictions.ne("externalTodayRealizedGainLossLocal", BigDecimal.ZERO)
						));
					}
					if (BooleanUtils.isTrue(searchForm.getReconciliationBreaksPresent())) {
						criteria.add(Restrictions.or(
								Restrictions.neProperty("ourFxRate", "externalFxRate"),
								Restrictions.neProperty("ourPrice", "externalPrice"),
								Restrictions.neProperty("ourQuantity", "externalQuantity"),
								Restrictions.neProperty("ourTodayClosedQuantity", "externalTodayClosedQuantity"),
								Restrictions.neProperty("ourMarketValueLocal", "externalMarketValueLocal"),
								Restrictions.neProperty("ourMarketValueBase", "externalMarketValueBase"),
								Restrictions.neProperty("ourOpenTradeEquityLocal", "externalOpenTradeEquityLocal"),
								Restrictions.neProperty("ourOpenTradeEquityBase", "externalOpenTradeEquityBase"),
								Restrictions.neProperty("ourOTEVariation", "externalOTEVariation"),
								Restrictions.neProperty("ourTodayCommissionLocal", "externalTodayCommissionLocal"),
								Restrictions.neProperty("ourTodayRealizedGainLossLocal", "externalTodayRealizedGainLossLocal")
						));
					}
					if (BooleanUtils.isTrue(searchForm.getActivityBreaksPresent())) {
						criteria.add(Restrictions.or(
								Restrictions.neProperty("ourTodayCommissionLocal", "externalTodayCommissionLocal"),
								Restrictions.neProperty("ourTodayRealizedGainLossLocal", "externalTodayRealizedGainLossLocal")
						));
					}
					if (BooleanUtils.isTrue(searchForm.getPositionBreaksPresent())) {
						criteria.add(Restrictions.or(
								Restrictions.neProperty("ourFxRate", "externalFxRate"),
								Restrictions.neProperty("ourPrice", "externalPrice"),
								Restrictions.neProperty("ourQuantity", "externalQuantity"),
								Restrictions.neProperty("ourTodayClosedQuantity", "externalTodayClosedQuantity"),
								Restrictions.neProperty("ourMarketValueLocal", "externalMarketValueLocal"),
								Restrictions.neProperty("ourMarketValueBase", "externalMarketValueBase"),
								Restrictions.neProperty("ourOpenTradeEquityLocal", "externalOpenTradeEquityLocal"),
								Restrictions.neProperty("ourOpenTradeEquityBase", "externalOpenTradeEquityBase"),
								Restrictions.neProperty("ourOTEVariation", "externalOTEVariation")
						));
					}
					if (BooleanUtils.isTrue(searchForm.getAnyNoteNotNull())) {
						criteria.add(Restrictions.or(Restrictions.isNotNull("note"), Restrictions.isNotNull("realizedAndCommissionNote")));
					}
				}
			}
		};
		return getReconcilePositionDAO().findBySearchCriteria(config);
	}


	@Override
	public List<? extends BaseSimpleEntity<?>> getOurReconcilePositionList(int reconcilePositionAccountId) {
		ReconcilePositionAccount account = getReconcilePositionAccount(reconcilePositionAccountId);
		ReconcilePositionDefinition definition = getReconcilePositionDefinition(account.getDefinition().getId());
		ReconcilePositionProcessor processor = getReconcileProcessor(definition);
		return processor.getOurReconcilePositionList(account);
	}


	@Override
	public List<? extends BaseSimpleEntity<?>> getExternalReconcilePositionList(int reconcilePositionAccountId) {
		ReconcilePositionAccount account = getReconcilePositionAccount(reconcilePositionAccountId);
		ReconcilePositionDefinition definition = getReconcilePositionDefinition(account.getDefinition().getId());
		ReconcilePositionProcessor processor = getReconcileProcessor(definition);
		return processor.getExternalReconcilePositionList(account);
	}


	@Override
	public List<? extends BaseSimpleEntity<?>> getExternalReconcilePNLList(int reconcilePositionAccountId) {
		ReconcilePositionAccount account = getReconcilePositionAccount(reconcilePositionAccountId);
		ReconcilePositionDefinition definition = getReconcilePositionDefinition(account.getDefinition().getId());
		ReconcilePositionProcessor processor = getReconcileProcessor(definition);
		return processor.getExternalReconcilePNLList(account);
	}


	@Override
	public ReconcilePositionComparison getReconcilePositionComparison(int id) {
		ReconcilePosition position = getReconcilePosition(id);
		ReconcilePositionDefinition definition = getReconcilePositionDefinition(position.getPositionAccount().getDefinition().getId());
		ReconcilePositionProcessor processor = getReconcileProcessor(definition);
		return processor.getReconcilePositionComparison(position);
	}


	@Override
	public String rebuildReconcilePosition(final ReconcilePositionRebuildCommand command, boolean synchronous) {
		ValidationUtils.assertNotNull(command.getPositionDate(), "Position Date is required for position reconciliation rebuild.", "positionDate");

		if (command.getOteMaxThreshold() != null) {
			ValidationUtils.assertTrue(command.getOteMaxThreshold().compareTo(BigDecimal.ZERO) >= 0, "OTE Max Threshold cannot be negative", "oteMaxThreshold");
		}

		if (command.getMarketValueMaxThreshold() != null) {
			ValidationUtils.assertTrue(command.getMarketValueMaxThreshold().compareTo(BigDecimal.ZERO) >= 0, "Market Value Max Threshold cannot be negative", "marketValueMaxThreshold");
		}

		if (command.getRealizedGainLossThreshold() != null) {
			ValidationUtils.assertTrue(command.getRealizedGainLossThreshold().compareTo(BigDecimal.ZERO) >= 0, "Realized Gain Loss Threshold cannot be negative", "oteMaxThreshold");
		}

		if (command.getCommissionThreshold() != null) {
			ValidationUtils.assertTrue(command.getCommissionThreshold().compareTo(BigDecimal.ZERO) >= 0, "Commission Max Threshold cannot be negative", "marketValueMaxThreshold");
		}

		if (synchronous) {
			return doRebuildReconcilePosition(command, null);
		}

		ValidationUtils.assertTrue((command.getHoldingCompanyId() != null || command.getHoldingAccountId() != null),
				"Please choose at least a holding company or account filter to perform the reconciliation.");

		final String runId = (command.getHoldingCompanyId() != null) ? "C" + command.getHoldingCompanyId() : "D" + command.getReconcilePositionDefinitionId();
		final Date now = new Date();

		Runner runner = new AbstractStatusAwareRunner("RECONCILE-POSITION-AUTO", runId, now) {

			@Override
			public void run() {
				try {
					getStatus().setMessage(doRebuildReconcilePosition(command, getStatus()));
				}
				catch (Throwable e) {
					getStatus().addError(ExceptionUtils.getDetailedMessage(e));
					LogUtils.errorOrInfo(getClass(), "Error reconciling positions for " + runId, e);
				}
			}
		};
		getRunnerHandler().runNow(runner);
		return "Processing chosen positions.";
	}


	private String doRebuildReconcilePosition(ReconcilePositionRebuildCommand command, Status status) {
		// map from a ReconcilePositionDefinition to a list of params - each definition may need to be called multiple times with different sets of parameters
		HashMap<ReconcilePositionDefinition, List<ReconcilePositionRebuildParameters>> definitionMap = new HashMap<>();

		// List of reconciliation parameters, as each definition may need to be called with a separate set of parameters
		ArrayList<ReconcilePositionRebuildParameters> paramList = new ArrayList<>();
		paramList.add(command);

		if (command.getReconcilePositionDefinitionId() != null) {
			ReconcilePositionDefinition definition = getReconcilePositionDefinition(command.getReconcilePositionDefinitionId());
			definitionMap.put(definition, paramList);
		}
		else {
			ReconcilePositionDefinitionSearchForm searchForm = new ReconcilePositionDefinitionSearchForm();
			if (command.getHoldingCompanyId() != null) {
				searchForm.setHoldingCompanyId(command.getHoldingCompanyId());
			}
			else if (command.getHoldingAccountId() != null) {
				InvestmentAccount holdingAccount = getInvestmentAccountService().getInvestmentAccount(command.getHoldingAccountId());
				ValidationUtils.assertNotNull(holdingAccount, "Cannot find Holding Account with id = " + command.getHoldingAccountId(), "holdingAccountId");
				searchForm.setHoldingCompanyId(holdingAccount.getIssuingCompany().getId());
			}

			for (ReconcilePositionDefinition definition : CollectionUtils.getIterable(getReconcilePositionDefinitionList(searchForm))) {
				definitionMap.put(definition, paramList);
			}
		}

		// Process each definition, with each set of params
		StringBuilder message = new StringBuilder();
		for (Entry<ReconcilePositionDefinition, List<ReconcilePositionRebuildParameters>> entry : definitionMap.entrySet()) {
			ReconcilePositionDefinition definition = entry.getKey();

			if (status != null) {
				status.setMessage("Processing position reconciliation for definition: [" + definition.getName() + "].");
			}

			for (ReconcilePositionRebuildParameters parameters : entry.getValue()) {
				try {
					ReconcilePositionProcessor processor = getReconcileProcessor(definition);
					processor.process(definition, parameters.getPositionDate(), parameters.getInvestmentSecurityId(), parameters.getHoldingAccountId(), parameters.getCleanRebuild());

					ReconcilePositionCommand reconcileCommand = new ReconcilePositionCommand(definition, parameters.getPositionDate(), parameters.getInvestmentSecurityId(),
							parameters.getHoldingAccountId(), command.getOteMaxThreshold(), command.getMarketValueMaxThreshold(), command.getRealizedGainLossThreshold(),
							command.getCommissionThreshold(), false);
					processor.reconcile(reconcileCommand);
				}
				catch (Throwable e) {
					LogUtils.error(getClass(), "Failed to run reconciliation definition [" + definition.getName() + "].", e);
					message.append(new RuntimeException("Failed to run reconciliation definition [" + definition.getName() + "].", e));
					if (status != null) {
						status.addError("Failed to run reconciliation definition [" + definition.getName() + "]: " + e.getMessage());
					}
				}
			}
		}
		if (message.length() > 0) {
			throw new RuntimeException(message.toString());
		}

		return "Processing Complete";
	}


	@Override
	public ReconcilePosition saveReconcilePosition(ReconcilePosition bean) {
		return getReconcilePositionDAO().save(bean);
	}


	@Override
	public void rebuildReconcilePositionForToday() {
		Date positionDate = DateUtils.getPreviousWeekday(DateUtils.clearTime(new Date()));
		ReconcilePositionRebuildCommand command = new ReconcilePositionRebuildCommand();
		command.setPositionDate(positionDate);
		command.setCleanRebuild(true);
		doRebuildReconcilePosition(command, null);
	}


	private ReconcilePositionProcessor getReconcileProcessor(ReconcilePositionDefinition definition) {
		String processorKey = definition.getOurSystemTable().getName() + "-" + definition.getExternalSystemTable().getName();
		return getReconcileProcessorMap().containsKey(processorKey) ? getReconcileProcessorMap().get(processorKey) : getDefaultReconcileProcessor();
	}


	@Override
	public void copyReconcilePositionNotesToM2M(Date positionDate) {
		ReconcilePositionSearchForm searchForm = new ReconcilePositionSearchForm();
		searchForm.setAnyNoteNotNull(true);
		searchForm.setPositionDate(positionDate);
		searchForm.setNoteNotStartWith("Auto");
		Map<Integer, AccountingM2MDaily> accountToNoteMap = new HashMap<>();
		List<ReconcilePosition> positionList = getReconcilePositionList(searchForm);
		for (ReconcilePosition position : CollectionUtils.getIterable(positionList)) {
			AccountingM2MDaily m2m = accountToNoteMap.get(position.getPositionAccount().getClientAccount().getId());
			if (m2m == null) {
				m2m = getAccountingM2MService().getAccountingM2MDailyByClientAccountAndDate(position.getPositionAccount().getClientAccount().getId(), positionDate);
				accountToNoteMap.put(position.getPositionAccount().getClientAccount().getId(), m2m);
			}
			String note = !StringUtils.isEmpty(position.getNote()) ? position.getNote() : position.getRealizedAndCommissionNote();
			if (StringUtils.isEmpty(m2m.getNote())) {
				m2m.setNote(note);
			}
			else if (!m2m.getNote().contains(position.getNote())) {
				m2m.setNote(m2m.getNote() + "\n" + note);
			}
		}
		for (AccountingM2MDaily m2m : accountToNoteMap.values()) {
			getAccountingM2MService().saveAccountingM2MDaily(m2m);
		}
	}


	@Override
	public void adjustReconcileRealizedGainLoss(int reconcilePositionId, BigDecimal threshold, String note) {
		ReconcilePosition reconcilePosition = getReconcilePosition(reconcilePositionId);
		InvestmentAccount clientAccount = doAdjustRealizedGainLoss(reconcilePosition, threshold, note);
		rebuildDailyPosition(reconcilePosition, clientAccount);
		rebuildReconcilePositionForAdjustment(reconcilePosition);
	}


	@Override
	public void adjustReconcileCommission(int reconcilePositionId, BigDecimal threshold, String note) {
		ReconcilePosition reconcilePosition = getReconcilePosition(reconcilePositionId);
		InvestmentAccount clientAccount = doAdjustCommission(reconcilePosition, threshold, note);
		rebuildDailyPosition(reconcilePosition, clientAccount);
		rebuildReconcilePositionForAdjustment(reconcilePosition);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates the adjusting realized gain/loss entry and returns the client account the transactions were applied to.
	 * <p>
	 * NOTE:  The client account is need for the daily position rebuild done after the entry.  This is because the
	 * client account on the ReconcilePositionAccount object is not necessarily the client account that the adjustment
	 * was applied to.
	 *
	 * @param reconcilePosition
	 * @param threshold
	 * @param note
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected InvestmentAccount doAdjustRealizedGainLoss(ReconcilePosition reconcilePosition, BigDecimal threshold, String note) {

		BigDecimal adjustmentAmount = reconcilePosition.getExternalTodayRealizedGainLossLocal().subtract(reconcilePosition.getOurTodayRealizedGainLossLocal());
		BigDecimal usdAdjustmentAmount = getUSDAmount(reconcilePosition, adjustmentAmount);

		ValidationUtils.assertTrue(adjustmentAmount.compareTo(BigDecimal.ZERO) != 0, "No adjustment is needed.");
		ValidationUtils.assertFalse(MathUtils.abs(usdAdjustmentAmount).compareTo(threshold) > 0, "Cannot adjustment Realized Gain/Loss by [" + adjustmentAmount + "] because it is larger than the ["
				+ threshold + "] threshold.");

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		searchForm.setHoldingInvestmentAccountId(reconcilePosition.getPositionAccount().getHoldingAccount().getId());
		searchForm.setAccountingAccountName(AccountingAccount.REVENUE_REALIZED);
		searchForm.setJournalTypeName(AccountingJournalType.TRADE_JOURNAL);
		searchForm.setTransactionDate(reconcilePosition.getPositionAccount().getPositionDate());
		List<AccountingTransaction> transactions = getAccountingTransactionService().getAccountingTransactionList(searchForm);

		ValidationUtils.assertTrue(transactions != null && !transactions.isEmpty(), "No Gain/Loss transactions found for [" + reconcilePosition.getInvestmentSecurity().getSymbol() + " on ["
				+ reconcilePosition.getPositionAccount().getPositionDate() + "] in account [" + reconcilePosition.getPositionAccount().getHoldingAccount().getNumber() + "].");

		AccountingTransaction transaction = CoreMathUtils.getBeanWithMaxProperty(transactions, "localDebitCredit", true);
		getAccountingAdjustingJournalService().adjustAccountingJournalRealized(transaction.getParentTransaction().getId(), adjustmentAmount,
				note != null && !note.isEmpty() ? note : "Reconciliation - Realized Gain/Loss Adjustment");

		return transaction.getParentTransaction().getClientInvestmentAccount();
	}


	/**
	 * Creates the adjusting commission entry and returns the client account the transactions were applied to.
	 * <p>
	 * NOTE:  The client account is need for the daily position rebuild done after the entry.  This is because the
	 * client account on the ReconcilePositionAccount object is not necessarily the client account that the adjustment
	 * was applied to.
	 */
	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected InvestmentAccount doAdjustCommission(ReconcilePosition reconcilePosition, BigDecimal threshold, String note) {
		BigDecimal adjustmentAmount = reconcilePosition.getExternalTodayCommissionLocal().subtract(reconcilePosition.getOurTodayCommissionLocal());
		BigDecimal usdAdjustmentAmount = getUSDAmount(reconcilePosition, adjustmentAmount);

		ValidationUtils.assertTrue(adjustmentAmount.compareTo(BigDecimal.ZERO) != 0, "No adjustment is needed.");
		ValidationUtils.assertFalse(MathUtils.abs(usdAdjustmentAmount).compareTo(threshold) > 0, "Cannot adjustment Realized Gain/Loss by [" + adjustmentAmount + "] because it is larger than the ["
				+ threshold + "] threshold.");

		AccountingTransactionSearchForm searchForm = new AccountingTransactionSearchForm();
		searchForm.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		searchForm.setHoldingInvestmentAccountId(reconcilePosition.getPositionAccount().getHoldingAccount().getId());
		searchForm.setAccountingAccountName(AccountingAccount.EXPENSE_COMMISSION);
		searchForm.setJournalTypeName(AccountingJournalType.TRADE_JOURNAL);
		searchForm.setTransactionDate(reconcilePosition.getPositionAccount().getPositionDate());
		List<AccountingTransaction> transactions = getAccountingTransactionService().getAccountingTransactionList(searchForm);

		ValidationUtils.assertTrue(transactions != null && !transactions.isEmpty(), "No Commission transactions found for [" + reconcilePosition.getInvestmentSecurity().getSymbol() + " on ["
				+ reconcilePosition.getPositionAccount().getPositionDate() + "] in account [" + reconcilePosition.getPositionAccount().getHoldingAccount().getNumber() + "].");

		AccountingTransaction transaction = CoreMathUtils.getBeanWithMaxProperty(transactions, "localDebitCredit", reconcilePosition.getOurTodayCommissionLocal().compareTo(BigDecimal.ZERO) >= 0);
		getAccountingAdjustingJournalService().adjustAccountingJournalCommission(transaction.getParentTransaction().getId(), adjustmentAmount.negate(),
				note != null && !note.isEmpty() ? note : "Reconciliation - Commission Adjustment");

		return transaction.getParentTransaction().getClientInvestmentAccount();
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void rebuildDailyPosition(ReconcilePosition reconcilePosition, InvestmentAccount clientAccount) {
		AccountingPositionDailyRebuildCommand command = new AccountingPositionDailyRebuildCommand();
		command.setClientAccountId(clientAccount.getId());
		command.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		command.setSnapshotDate(reconcilePosition.getPositionAccount().getPositionDate());
		command.setSynchronous(true);
		getAccountingPositionDailyService().rebuildAccountingPositionDaily(command);
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void rebuildReconcilePositionForAdjustment(ReconcilePosition reconcilePosition) {
		ReconcilePositionRebuildCommand command = new ReconcilePositionRebuildCommand();
		command.setHoldingAccountId(reconcilePosition.getPositionAccount().getHoldingAccount().getId());
		command.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		command.setPositionDate(reconcilePosition.getPositionAccount().getPositionDate());
		command.setReconcilePositionDefinitionId(reconcilePosition.getPositionAccount().getDefinition().getId());
		doRebuildReconcilePosition(command, null);
	}


	private BigDecimal getUSDAmount(ReconcilePosition reconcilePosition, BigDecimal adjustmentAmount) {
		BigDecimal usdAdjustmentAmount = adjustmentAmount;
		String fromCurrency = reconcilePosition.getInvestmentSecurity().getInstrument().getTradingCurrency().getSymbol();
		if (!"USD".equalsIgnoreCase(fromCurrency)) {
			BigDecimal fxRate;
			String toCurrency = reconcilePosition.getPositionAccount().getHoldingAccount().getBaseCurrency().getSymbol();
			if ((reconcilePosition.getExternalFxRate() != null) && "USD".equalsIgnoreCase(toCurrency)) {
				fxRate = reconcilePosition.getExternalFxRate();
			}
			else {
				fxRate = getMarketDataExchangeRatesApiService().getExchangeRate(FxRateLookupCommand.forHoldingAccount(reconcilePosition.getPositionAccount().getHoldingAccount().toHoldingAccount(),
						fromCurrency, toCurrency, reconcilePosition.getPositionAccount().getPositionDate()).flexibleLookup());
			}
			usdAdjustmentAmount = adjustmentAmount.multiply(fxRate);
		}

		return usdAdjustmentAmount;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconcilePositionDefinition, Criteria> getReconcilePositionDefinitionDAO() {
		return this.reconcilePositionDefinitionDAO;
	}


	public void setReconcilePositionDefinitionDAO(AdvancedUpdatableDAO<ReconcilePositionDefinition, Criteria> reconcilePositionDefinitionDAO) {
		this.reconcilePositionDefinitionDAO = reconcilePositionDefinitionDAO;
	}


	public AdvancedUpdatableDAO<ReconcilePositionAccount, Criteria> getReconcilePositionAccountDAO() {
		return this.reconcilePositionAccountDAO;
	}


	public void setReconcilePositionAccountDAO(AdvancedUpdatableDAO<ReconcilePositionAccount, Criteria> reconcilePositionAccountDAO) {
		this.reconcilePositionAccountDAO = reconcilePositionAccountDAO;
	}


	public Map<String, ReconcilePositionProcessor> getReconcileProcessorMap() {
		return this.reconcileProcessorMap;
	}


	public void setReconcileProcessorMap(Map<String, ReconcilePositionProcessor> reconcileProcessorMap) {
		this.reconcileProcessorMap = reconcileProcessorMap;
	}


	public ReconcilePositionProcessor getDefaultReconcileProcessor() {
		return this.defaultReconcileProcessor;
	}


	public void setDefaultReconcileProcessor(ReconcilePositionProcessor defaultReconcileProcessor) {
		this.defaultReconcileProcessor = defaultReconcileProcessor;
	}


	public AdvancedUpdatableDAO<ReconcilePosition, Criteria> getReconcilePositionDAO() {
		return this.reconcilePositionDAO;
	}


	public void setReconcilePositionDAO(AdvancedUpdatableDAO<ReconcilePosition, Criteria> reconcilePositionDAO) {
		this.reconcilePositionDAO = reconcilePositionDAO;
	}


	public AdvancedReadOnlyDAO<ReconcilePositionAccountExtended, Criteria> getReconcilePositionAccountExtendedDAO() {
		return this.reconcilePositionAccountExtendedDAO;
	}


	public void setReconcilePositionAccountExtendedDAO(AdvancedReadOnlyDAO<ReconcilePositionAccountExtended, Criteria> reconcilePositionAccountExtendedDAO) {
		this.reconcilePositionAccountExtendedDAO = reconcilePositionAccountExtendedDAO;
	}


	public AccountingAdjustingJournalService getAccountingAdjustingJournalService() {
		return this.accountingAdjustingJournalService;
	}


	public void setAccountingAdjustingJournalService(AccountingAdjustingJournalService accountingAdjustingJournalService) {
		this.accountingAdjustingJournalService = accountingAdjustingJournalService;
	}


	public ReconcilePositionDefinitionListCache getReconcilePositionDefinitionListCache() {
		return this.reconcilePositionDefinitionListCache;
	}


	public void setReconcilePositionDefinitionListCache(ReconcilePositionDefinitionListCache reconcilePositionDefinitionListCache) {
		this.reconcilePositionDefinitionListCache = reconcilePositionDefinitionListCache;
	}


	public AccountingTransactionService getAccountingTransactionService() {
		return this.accountingTransactionService;
	}


	public void setAccountingTransactionService(AccountingTransactionService accountingTransactionService) {
		this.accountingTransactionService = accountingTransactionService;
	}


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public MarketDataExchangeRatesApiService getMarketDataExchangeRatesApiService() {
		return this.marketDataExchangeRatesApiService;
	}


	public void setMarketDataExchangeRatesApiService(MarketDataExchangeRatesApiService marketDataExchangeRatesApiService) {
		this.marketDataExchangeRatesApiService = marketDataExchangeRatesApiService;
	}
}
