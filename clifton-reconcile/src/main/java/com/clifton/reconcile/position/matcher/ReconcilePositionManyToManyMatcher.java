package com.clifton.reconcile.position.matcher;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.matching.AbstractManyToManyMatcher;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.core.util.matching.PermutationScorer;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconcile.position.accounting.ReconcilePositionAccountingDailyGrouping;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDaily;
import org.springframework.stereotype.Component;


@Component
public class ReconcilePositionManyToManyMatcher<T> extends AbstractManyToManyMatcher<ReconcilePositionAccountingDailyGrouping, T> {

	@Override
	protected int getMatchResultItemScore(MatchingResultItem<ReconcilePositionAccountingDailyGrouping, T> matchItem) {
		ValidationUtils.assertNotNull(matchItem, "No MatchingResultItem was passed. Cannot produce score.");
		ValidationUtils.assertNotEmpty(matchItem.getExternalList(), "The external list to match is null or contains no elements");
		ValidationUtils.assertNotEmpty(matchItem.getInternalList(), "The internal list to match is null or contains no elements");

		if (matchItem.getInternalList().size() > 1 || matchItem.getExternalList().size() > 1) {
			return -1;
		}

		ReconcilePositionAccountingDailyGrouping internalPosition = CollectionUtils.getOnlyElementStrict(matchItem.getInternalList());
		ReconcilePositionExternalDaily externalPosition = (ReconcilePositionExternalDaily) CollectionUtils.getOnlyElementStrict(matchItem.getExternalList());

		if (internalPosition.getRemainingQuantity().compareTo(externalPosition.getQuantity()) != 0) {
			return -1;
		}

		if (!internalPosition.getOriginalTransactionDate().equals(externalPosition.getTradeDate())) {
			return -1;
		}

		return 1;
	}


	@Override
	protected int getMatchResultScore(MatchingResult<ReconcilePositionAccountingDailyGrouping, T> match) {
		for (MatchingResultItem<ReconcilePositionAccountingDailyGrouping, T> item : CollectionUtils.getIterable(match.getItemList())) {
			if (getMatchResultItemScore(item) == -1) {
				return -1;
			}
		}
		return 0;
	}


	@Override
	protected PermutationScorer<ReconcilePositionAccountingDailyGrouping> getInternalPermutationScorer() {
		return null; // not necessary for one-to-one
	}


	@Override
	protected PermutationScorer<T> getExternalPermutationScorer() {
		return null; // not necessary for one-to-one
	}
}
