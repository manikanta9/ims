package com.clifton.reconcile.position.cache;


import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.reconcile.position.ReconcilePositionDefinition;
import com.clifton.reconcile.position.ReconcilePositionService;
import com.clifton.reconcile.position.search.ReconcilePositionDefinitionSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>ReconcilePositionDefinitionListCacheImpl</code> class is a cache of the list of all ReconcilePositionDefinitions
 * <p>
 *
 * @author vgomelsky
 */
@Component
public class ReconcilePositionDefinitionListCacheImpl extends SelfRegisteringSimpleDaoCache<ReconcilePositionDefinition, String, List<ReconcilePositionDefinition>> implements ReconcilePositionDefinitionListCache {

	private ReconcilePositionService reconcilePositionService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcilePositionDefinition> getReconcilePositionDefinitionList() {
		List<ReconcilePositionDefinition> result = getCacheHandler().get(getCacheName(), "ReconcilePositionDefinitionList");
		if (result == null) {
			// not in cache: retrieve from service and store in cache for future calls
			result = getReconcilePositionService().getReconcilePositionDefinitionList(new ReconcilePositionDefinitionSearchForm());
			if (result != null) {
				getCacheHandler().put(getCacheName(), "ReconcilePositionDefinitionList", result);
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconcilePositionService getReconcilePositionService() {
		return this.reconcilePositionService;
	}


	public void setReconcilePositionService(ReconcilePositionService reconcilePositionService) {
		this.reconcilePositionService = reconcilePositionService;
	}
}
