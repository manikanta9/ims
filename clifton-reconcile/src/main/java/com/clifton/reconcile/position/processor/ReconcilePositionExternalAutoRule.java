package com.clifton.reconcile.position.processor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.processor.ReconcileRule;

import java.math.BigDecimal;
import java.util.Map;


/**
 * The <code>ReconcilePositionExternalAutoRule</code> compares internal and external positions on Quantity, MarketValue (local and base), OpenTradeEquity(local and base), and FX Rate. If the
 * difference between any two of these fields is within a given threshold (threshold defaults to zero), the ReconcilePosition object will be marked as reconciled.  Additionally, if a threshold was
 * used, a note will automatically be populated on the ReconcilePosition's note field.
 *
 * @author rbrooks
 */
public class ReconcilePositionExternalAutoRule implements ReconcileRule<ReconcilePosition> {

	@Override
	public void reconcile(ReconcilePosition bean, Map<String, Object> context) {

		// skip reconciliation if a user note has been entered
		if (!StringUtils.isEmpty(bean.getNote()) && !bean.getNote().contains(ReconcilePositionProcessor.AUTO_REC_INDICATOR)) {
			return;
		}

		// normalize thresholds
		BigDecimal oteThreshold = BigDecimal.ZERO;
		BigDecimal marketValueThreshold = BigDecimal.ZERO;
		if (context != null) {
			if (context.get(ReconcilePositionProcessor.OTE_MAX_THRESHOLD) != null) {
				oteThreshold = (BigDecimal) context.get(ReconcilePositionProcessor.OTE_MAX_THRESHOLD);
			}
			if (context.get(ReconcilePositionProcessor.MARKET_VALUE_MAX_THRESHOLD) != null) {
				marketValueThreshold = (BigDecimal) context.get(ReconcilePositionProcessor.MARKET_VALUE_MAX_THRESHOLD);
			}
		}

		// reconcile fields
		if ((bean.getOurQuantity().compareTo(BigDecimal.ZERO) == 0) && (bean.getExternalQuantity().compareTo(BigDecimal.ZERO) == 0)) {
			bean.setReconciled(true);
			return;
		}

		if (!compareProperty(bean, "Quantity", BigDecimal.ZERO)) {
			bean.setReconciled(false);
		}
		else if (!compareProperty(bean, "MarketValueLocal", marketValueThreshold)) {
			bean.setReconciled(false);
		}
		else if (!compareProperty(bean, "OpenTradeEquityLocal", oteThreshold)) {
			bean.setReconciled(false);
		}
		else if (!bean.getPositionAccount().getDefinition().isLocalOnly()) {
			if (!compareProperty(bean, "FxRate", BigDecimal.ZERO)) {
				bean.setReconciled(false);
			}
			else if (!compareProperty(bean, "MarketValueBase", marketValueThreshold)) {
				bean.setReconciled(false);
			}
			else if (!compareProperty(bean, "OpenTradeEquityBase", oteThreshold)) {
				bean.setReconciled(false);
			}
			else {
				bean.setReconciled(true);
			}
		}
		else {
			bean.setReconciled(true);
		}

		//set note if threshold was used (reconciled, but unequal values)
		if (bean.isReconciled() && (!bean.isMarketValueEqual() || !bean.isOTEEqual())) {
			bean.setNote(ReconcilePositionProcessor.AUTO_REC_INDICATOR + ": " + oteThreshold + " and " + marketValueThreshold);
		}
	}


	/**
	 * Compares two properties within a ReconcilePosition instance to determine if the values are
	 * equal or within a threshold (for properties of type BigDecimal).
	 *
	 * @return true if values are equal or their absolute difference is withing the provided threshold. Otherwise returns false.
	 */
	private boolean compareProperty(ReconcilePosition position, String name, BigDecimal threshold) {
		Object ours = BeanUtils.getPropertyValue(position, "our" + name);
		Object external = BeanUtils.getPropertyValue(position, "external" + name);
		ours = ours == null ? BigDecimal.ZERO : ours;
		external = external == null ? BigDecimal.ZERO : external;
		if ((ours instanceof BigDecimal) && (external instanceof BigDecimal)) {
			BigDecimal o = (BigDecimal) ours;
			BigDecimal e = (BigDecimal) external;
			return o.subtract(e).abs().compareTo(threshold) <= 0;
		}
		return ours.equals(external);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPropertyName() {
		return "reconciled";
	}
}
