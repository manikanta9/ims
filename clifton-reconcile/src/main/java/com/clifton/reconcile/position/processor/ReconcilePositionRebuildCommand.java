package com.clifton.reconcile.position.processor;


import com.clifton.reconcile.position.ReconcilePositionRebuildParameters;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionRebuildCommand implements ReconcilePositionRebuildParameters {

	//////////////////////////////////////////////////////////////////
	//////	Parameters for finding the right set of definitions //////
	//////////////////////////////////////////////////////////////////

	/**
	 * Used to find all the definitions for this holding company
	 */
	private Integer holdingCompanyId;

	/**
	 * Used to run a single ReconcilePositionDefinition
	 */
	private Integer reconcilePositionDefinitionId;

	////////////////////////////////////////////////////////
	//////  Parameters for filtering each definition  //////
	////////////////////////////////////////////////////////

	/**
	 * Used to filter beyond the definition level
	 */
	private Integer investmentSecurityId;

	/**
	 * Used to filter beyond the definition level
	 */
	private Integer holdingAccountId;

	/**
	 * Used to filter beyond the definition level
	 */
	private Date positionDate;

	/**
	 * Used to filter beyond the definition level
	 */
	private Boolean cleanRebuild;

	/////////////////////////////////////////////
	//////  Optional threshold parameters  //////
	/////////////////////////////////////////////

	private BigDecimal marketValueMaxThreshold;

	private BigDecimal oteMaxThreshold;

	private BigDecimal realizedGainLossThreshold;

	private BigDecimal commissionThreshold;


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Integer getReconcilePositionDefinitionId() {
		return this.reconcilePositionDefinitionId;
	}


	public void setReconcilePositionDefinitionId(Integer reconcilePositionDefinitionId) {
		this.reconcilePositionDefinitionId = reconcilePositionDefinitionId;
	}


	@Override
	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	@Override
	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	@Override
	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	@Override
	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	@Override
	public Date getPositionDate() {
		return this.positionDate;
	}


	@Override
	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	@Override
	public Boolean getCleanRebuild() {
		return this.cleanRebuild;
	}


	@Override
	public void setCleanRebuild(Boolean cleanRebuild) {
		this.cleanRebuild = cleanRebuild;
	}


	public BigDecimal getMarketValueMaxThreshold() {
		return this.marketValueMaxThreshold;
	}


	public void setMarketValueMaxThreshold(BigDecimal marketValueMaxThreshold) {
		this.marketValueMaxThreshold = marketValueMaxThreshold;
	}


	public BigDecimal getOteMaxThreshold() {
		return this.oteMaxThreshold;
	}


	public void setOteMaxThreshold(BigDecimal oteMaxThreshold) {
		this.oteMaxThreshold = oteMaxThreshold;
	}


	public BigDecimal getRealizedGainLossThreshold() {
		return this.realizedGainLossThreshold;
	}


	public void setRealizedGainLossThreshold(BigDecimal realizedGainLossThreshold) {
		this.realizedGainLossThreshold = realizedGainLossThreshold;
	}


	public BigDecimal getCommissionThreshold() {
		return this.commissionThreshold;
	}


	public void setCommissionThreshold(BigDecimal commissionThreshold) {
		this.commissionThreshold = commissionThreshold;
	}
}
