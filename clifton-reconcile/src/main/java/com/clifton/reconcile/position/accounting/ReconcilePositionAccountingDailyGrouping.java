package com.clifton.reconcile.position.accounting;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ReconcilePositionAccountingDailyGrouping</code> is an extended DTO that groups AccountingPositionDaily
 * objects by OpenDate and OpenPrice for easily reconciling lot-level positions.
 *
 * @author rbrooks
 */
public class ReconcilePositionAccountingDailyGrouping extends BaseSimpleEntity<Integer> {

	/*
	 * The following fields come from AccountingPositionDaily.accountingTransaction associated table
	 */

	private int accountingTransactionId;

	private int fkFieldId;

	private int accountingJournalId;

	private String detailScreenClass;

	private InvestmentSecurity investmentSecurity;

	private InvestmentAccount holdingInvestmentAccount;

	private InvestmentAccount clientInvestmentAccount;

	private Date originalTransactionDate;

	private Date settlementDate;

	private BigDecimal price;

	private Boolean closedPosition;

	/*
	 * The following fields come from AccountingPositionDaily table proper
	 */

	private Date positionDate;

	private BigDecimal marketPrice;

	private BigDecimal marketFxRate;

	private BigDecimal remainingQuantity;

	private BigDecimal remainingCostBasisLocal = BigDecimal.ZERO;

	private BigDecimal remainingCostBasisBase = BigDecimal.ZERO;

	private BigDecimal notionalValueLocal = BigDecimal.ZERO;

	private BigDecimal notionalValueBase = BigDecimal.ZERO;

	private BigDecimal openTradeEquityLocal = BigDecimal.ZERO;

	private BigDecimal openTradeEquityBase = BigDecimal.ZERO;

	private BigDecimal priorOpenTradeEquityLocal = BigDecimal.ZERO;

	private BigDecimal priorOpenTradeEquityBase = BigDecimal.ZERO;

	private int totalPositions;


	public int getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(int accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public int getFkFieldId() {
		return this.fkFieldId;
	}


	public void setFkFieldId(int fkFieldId) {
		this.fkFieldId = fkFieldId;
	}


	public int getAccountingJournalId() {
		return this.accountingJournalId;
	}


	public void setAccountingJournalId(int accountingJournalId) {
		this.accountingJournalId = accountingJournalId;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public InvestmentAccount getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(InvestmentAccount clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public Boolean getClosedPosition() {
		return this.closedPosition;
	}


	public void setClosedPosition(Boolean closedPosition) {
		this.closedPosition = closedPosition;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getMarketFxRate() {
		return this.marketFxRate;
	}


	public void setMarketFxRate(BigDecimal marketFxRate) {
		this.marketFxRate = marketFxRate;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getRemainingCostBasisLocal() {
		return this.remainingCostBasisLocal;
	}


	public void setRemainingCostBasisLocal(BigDecimal remainingCostBasisLocal) {
		this.remainingCostBasisLocal = remainingCostBasisLocal;
	}


	public BigDecimal getRemainingCostBasisBase() {
		return this.remainingCostBasisBase;
	}


	public void setRemainingCostBasisBase(BigDecimal remainingCostBasisBase) {
		this.remainingCostBasisBase = remainingCostBasisBase;
	}


	public BigDecimal getNotionalValueLocal() {
		return this.notionalValueLocal;
	}


	public void setNotionalValueLocal(BigDecimal notionalValueLocal) {
		this.notionalValueLocal = notionalValueLocal;
	}


	public BigDecimal getNotionalValueBase() {
		return this.notionalValueBase;
	}


	public void setNotionalValueBase(BigDecimal notionalValueBase) {
		this.notionalValueBase = notionalValueBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}


	public int getTotalPositions() {
		return this.totalPositions;
	}


	public void setTotalPositions(int totalPositions) {
		this.totalPositions = totalPositions;
	}
}
