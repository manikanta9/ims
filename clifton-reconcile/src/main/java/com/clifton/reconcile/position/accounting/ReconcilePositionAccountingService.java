package com.clifton.reconcile.position.accounting;


import com.clifton.core.context.DoNotAddRequestMapping;

import java.util.List;


public interface ReconcilePositionAccountingService {

	//////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionAccountingDailyGrouping Business Methods     /////////////
	//////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public List<ReconcilePositionAccountingDailyGrouping> getReconcilePositionAccountingDailyGroupingList(ReconcilePositionAccountingDailyGroupingSearchForm searchForm);
}
