package com.clifton.reconcile.position.cache;

import com.clifton.reconcile.position.ReconcilePositionDefinition;

import java.util.List;


/**
 * The <code>ReconcilePositionDefinitionListCacheImpl</code> class is a cache of the list of all ReconcilePositionDefinitions
 * <p>
 *
 * @author vgomelsky
 */
public interface ReconcilePositionDefinitionListCache {

	public List<ReconcilePositionDefinition> getReconcilePositionDefinitionList();
}
