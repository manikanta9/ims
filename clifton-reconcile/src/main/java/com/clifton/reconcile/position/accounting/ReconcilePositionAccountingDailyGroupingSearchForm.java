package com.clifton.reconcile.position.accounting;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionAccountingDailyGroupingSearchForm extends BaseEntitySearchForm {

	@SearchField
	private Integer accountingTransactionId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "clientInvestmentAccount.id")
	private Integer clientInvestmentAccountId;

	@SearchField
	private Date originalTransactionDate;

	@SearchField
	private Date settlementDate;

	@SearchField
	private BigDecimal price;

	@SearchField
	private Boolean closedPosition;

	@SearchField
	private Date positionDate;

	@SearchField
	private BigDecimal marketPrice;

	@SearchField
	private BigDecimal marketFxRate;

	@SearchField
	private BigDecimal remainingQuantity;

	@SearchField
	private BigDecimal remainingCostBasisLocal;

	@SearchField
	private BigDecimal remainingCostBasisBase;

	@SearchField
	private BigDecimal notionalValueLocal;

	@SearchField
	private BigDecimal notionalValueBase;

	@SearchField
	private BigDecimal openTradeEquityLocal;

	@SearchField
	private BigDecimal openTradeEquityBase;

	@SearchField
	private BigDecimal priorOpenTradeEquityLocal;

	@SearchField
	private BigDecimal priorOpenTradeEquityBase;

	@SearchField
	private Integer totalPositions;


	public Integer getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(Integer accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Date getOriginalTransactionDate() {
		return this.originalTransactionDate;
	}


	public void setOriginalTransactionDate(Date originalTransactionDate) {
		this.originalTransactionDate = originalTransactionDate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public Boolean getClosedPosition() {
		return this.closedPosition;
	}


	public void setClosedPosition(Boolean closedPosition) {
		this.closedPosition = closedPosition;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getMarketFxRate() {
		return this.marketFxRate;
	}


	public void setMarketFxRate(BigDecimal marketFxRate) {
		this.marketFxRate = marketFxRate;
	}


	public BigDecimal getRemainingQuantity() {
		return this.remainingQuantity;
	}


	public void setRemainingQuantity(BigDecimal remainingQuantity) {
		this.remainingQuantity = remainingQuantity;
	}


	public BigDecimal getRemainingCostBasisLocal() {
		return this.remainingCostBasisLocal;
	}


	public void setRemainingCostBasisLocal(BigDecimal remainingCostBasisLocal) {
		this.remainingCostBasisLocal = remainingCostBasisLocal;
	}


	public BigDecimal getRemainingCostBasisBase() {
		return this.remainingCostBasisBase;
	}


	public void setRemainingCostBasisBase(BigDecimal remainingCostBasisBase) {
		this.remainingCostBasisBase = remainingCostBasisBase;
	}


	public BigDecimal getNotionalValueLocal() {
		return this.notionalValueLocal;
	}


	public void setNotionalValueLocal(BigDecimal notionalValueLocal) {
		this.notionalValueLocal = notionalValueLocal;
	}


	public BigDecimal getNotionalValueBase() {
		return this.notionalValueBase;
	}


	public void setNotionalValueBase(BigDecimal notionalValueBase) {
		this.notionalValueBase = notionalValueBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}


	public Integer getTotalPositions() {
		return this.totalPositions;
	}


	public void setTotalPositions(Integer totalPositions) {
		this.totalPositions = totalPositions;
	}
}
