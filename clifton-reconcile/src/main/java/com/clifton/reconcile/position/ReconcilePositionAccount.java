package com.clifton.reconcile.position;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


public class ReconcilePositionAccount extends BaseEntity<Integer> implements LabeledObject {

	private ReconcilePositionDefinition definition;
	private InvestmentAccount holdingAccount;
	private InvestmentAccount clientAccount;

	private Date positionDate;

	private boolean reconciled; // specifies whether positions (qty, ote, market value) are the same
	private boolean realizedAndCommissionReconciled;
	private boolean quantityReconciled;


	@Override
	public String getLabel() {
		return getHoldingAccount().getLabel() + " for " + DateUtils.fromDate(getPositionDate(), DateUtils.DATE_FORMAT_INPUT);
	}


	public ReconcilePositionDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(ReconcilePositionDefinition definition) {
		this.definition = definition;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public boolean isQuantityReconciled() {
		return this.quantityReconciled;
	}


	public void setQuantityReconciled(boolean quantityReconciled) {
		this.quantityReconciled = quantityReconciled;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public boolean isRealizedAndCommissionReconciled() {
		return this.realizedAndCommissionReconciled;
	}


	public void setRealizedAndCommissionReconciled(boolean realizedAndCommissionReconciled) {
		this.realizedAndCommissionReconciled = realizedAndCommissionReconciled;
	}
}
