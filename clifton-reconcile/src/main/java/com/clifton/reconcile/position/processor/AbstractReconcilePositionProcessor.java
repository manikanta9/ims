package com.clifton.reconcile.position.processor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionAccount;
import com.clifton.reconcile.position.ReconcilePositionService;
import com.clifton.reconcile.position.search.ReconcilePositionAccountSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionSearchForm;
import com.clifton.reconcile.processor.ReconcileRule;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class AbstractReconcilePositionProcessor implements ReconcilePositionProcessor {

	ReconcilePositionService reconcilePositionService;

	List<ReconcileRule<ReconcilePosition>> reconcileRuleList;


	@Override
	@DoNotAddRequestMapping
	public void reconcile(ReconcilePositionCommand command) {
		HashMap<String, Object> reconcileContext = new HashMap<>();
		reconcileContext.put(ReconcilePositionProcessor.OTE_MAX_THRESHOLD, command.getOteMaxThreshold());
		reconcileContext.put(ReconcilePositionProcessor.MARKET_VALUE_MAX_THRESHOLD, command.getMarketValueMaxThreshold());
		reconcileContext.put(ReconcilePositionProcessor.REALIZED_MAX_THRESHOLD, command.getRealizedGainLossThreshold());
		reconcileContext.put(ReconcilePositionProcessor.COMMISSION_MAX_THRESHOLD, command.getCommissionThreshold());

		ReconcilePositionSearchForm positionSearchForm = new ReconcilePositionSearchForm();
		positionSearchForm.setReconcilePositionDefinitionId(command.getReconcilePositionDefinition().getId());
		positionSearchForm.setPositionDate(command.getPositionDate());
		if (command.getHoldingInvestmentAccountId() != null) {
			positionSearchForm.setHoldingAccountId(command.getHoldingInvestmentAccountId());
		}
		positionSearchForm.setOrderBy("positionAccount");
		List<ReconcilePosition> positionList = getReconcilePositionService().getReconcilePositionList(positionSearchForm);
		Map<ReconcilePositionAccount, Map<String, Boolean>> positionAccountsRuleResultsMap = new HashMap<>();
		for (ReconcilePosition position : CollectionUtils.getIterable(positionList)) {
			Map<String, Boolean> positionAccountRuleResultsMap = positionAccountsRuleResultsMap.computeIfAbsent(position.getPositionAccount(), k -> new HashMap<>());
			Map<String, Boolean> positionRuleResultsMap = new HashMap<>();
			for (ReconcileRule<ReconcilePosition> rule : CollectionUtils.getIterable(getReconcileRuleList())) {
				// update position according to rule evaluation
				if (!command.isAccountLevelOnly() && ((command.getInvestmentSecurityId() == null) || (command.getInvestmentSecurityId().equals(position.getInvestmentSecurity().getId())))) {
					rule.reconcile(position, reconcileContext);
					getReconcilePositionService().saveReconcilePosition(position);
				}
				// if account value map doesn't have the property, add it with the default value = true.
				if (!positionAccountRuleResultsMap.containsKey(rule.getPropertyName())) {
					positionAccountRuleResultsMap.put(rule.getPropertyName(), true);
				}
				// add position rule evaluation value to the position map.
				positionRuleResultsMap.put(rule.getPropertyName(), (Boolean) BeanUtils.getPropertyValue(position, rule.getPropertyName()));
			}
			// if the any of the position values are false, the account values becomes false
			for (Map.Entry<String, Boolean> entry : positionRuleResultsMap.entrySet()) {
				if (!entry.getValue()) {
					positionAccountRuleResultsMap.put(entry.getKey(), false);
				}
			}
		}
		// update accounts based on the cumulative position rule evaluation results.
		for (Map.Entry<ReconcilePositionAccount, Map<String, Boolean>> reconcilePositionAccountMapEntry : positionAccountsRuleResultsMap.entrySet()) {
			Map<String, Boolean> positionRuleResultsMap = reconcilePositionAccountMapEntry.getValue();
			for (Map.Entry<String, Boolean> entry : positionRuleResultsMap.entrySet()) {
				com.clifton.core.beans.BeanUtils.setPropertyValue(reconcilePositionAccountMapEntry.getKey(), entry.getKey(), entry.getValue());
			}
			getReconcilePositionService().saveReconcilePositionAccount(reconcilePositionAccountMapEntry.getKey());
		}
		// update those accounts with no associated position records.
		ReconcilePositionAccountSearchForm searchForm = new ReconcilePositionAccountSearchForm();
		searchForm.setDefinitionId(command.getReconcilePositionDefinition().getId());
		if (command.getHoldingInvestmentAccountId() != null) {
			searchForm.setHoldingAccountId(command.getHoldingInvestmentAccountId());
		}
		searchForm.setPositionDate(command.getPositionDate());
		List<ReconcilePositionAccount> accountList = getReconcilePositionService().getReconcilePositionAccountList(searchForm);
		for (ReconcilePositionAccount positionAccount : accountList) {
			if (!positionAccountsRuleResultsMap.containsKey(positionAccount)) {
				positionAccount.setReconciled(true);
				positionAccount.setQuantityReconciled(true);
				positionAccount.setRealizedAndCommissionReconciled(true);
				getReconcilePositionService().saveReconcilePositionAccount(positionAccount);
			}
		}
	}


	public ReconcilePositionService getReconcilePositionService() {
		return this.reconcilePositionService;
	}


	public void setReconcilePositionService(ReconcilePositionService reconcilePositionService) {
		this.reconcilePositionService = reconcilePositionService;
	}


	public List<ReconcileRule<ReconcilePosition>> getReconcileRuleList() {
		return this.reconcileRuleList;
	}


	@Override
	public void setReconcileRuleList(List<ReconcileRule<ReconcilePosition>> reconcileRuleList) {
		this.reconcileRuleList = reconcileRuleList;
	}
}
