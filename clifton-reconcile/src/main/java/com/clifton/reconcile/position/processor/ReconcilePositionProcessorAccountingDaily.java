package com.clifton.reconcile.position.processor;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.accounting.gl.position.daily.AccountingPositionDailyService;
import com.clifton.accounting.gl.position.daily.search.AccountingPositionDailySearchForm;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.matching.MatchingMatcher;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.reconcile.position.ReconcileDisplayPosition;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionAccount;
import com.clifton.reconcile.position.ReconcilePositionDefinition;
import com.clifton.reconcile.position.accounting.ReconcilePositionAccountingDailyGrouping;
import com.clifton.reconcile.position.accounting.ReconcilePositionAccountingDailyGroupingSearchForm;
import com.clifton.reconcile.position.accounting.ReconcilePositionAccountingService;
import com.clifton.reconcile.position.comparison.ReconcilePositionComparison;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDaily;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyGrouping;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyPNL;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailyGroupingSearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailyPNLSearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailySearchForm;
import com.clifton.security.user.SecurityUser;
import org.hibernate.Criteria;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;


/**
 * The <code>ExternalPositionReconcileProcessor</code> ...
 *
 * @author mwacker
 */
public class ReconcilePositionProcessorAccountingDaily extends AbstractReconcilePositionProcessor {

	private AdvancedUpdatableDAO<ReconcilePositionExternalDaily, Criteria> reconcilePositionExternalDailyDAO;

	private AccountingPositionDailyService accountingPositionDailyService;

	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;

	private ReconcilePositionAccountingService reconcilePositionAccountingService;

	private ContextHandler contextHandler;

	private MatchingMatcher<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDailyGrouping> reconcilePositionManyToManyMatcher;


	@Override
	@Transactional(timeout = 180, propagation = Propagation.REQUIRES_NEW)
	public void process(ReconcilePositionDefinition definition, Date positionDate, Integer investmentSecurityId, Integer holdingInvestmentAccountId, Boolean cleanRebuild) {
		getReconcilePositionExternalDailyDAO().executeNamedQuery("reconcile-loadReconcilePositionDefinitionData", positionDate, definition.getId(), investmentSecurityId, holdingInvestmentAccountId,
				((SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME)).getId(), cleanRebuild);
	}


	@Override
	public List<AccountingPositionDaily> getOurReconcilePositionList(ReconcilePositionAccount reconcilePositionAccount) {
		AccountingPositionDailySearchForm searchForm = new AccountingPositionDailySearchForm();
		searchForm.setHoldingInvestmentAccountId(reconcilePositionAccount.getHoldingAccount().getId());
		searchForm.setPositionDate(reconcilePositionAccount.getPositionDate());
		if (reconcilePositionAccount.getDefinition().getInvestmentGroup() != null) {
			searchForm.setInvestmentGroupId(reconcilePositionAccount.getDefinition().getInvestmentGroup().getId());
		}

		List<AccountingPositionDaily> result = getAccountingPositionDailyService().getAccountingPositionDailyList(searchForm);
		return result == null ? new ArrayList<>() : result;
	}


	@Override
	public List<ReconcilePositionExternalDaily> getExternalReconcilePositionList(ReconcilePositionAccount reconcilePositionAccount) {
		ReconcilePositionExternalDailySearchForm searchForm = new ReconcilePositionExternalDailySearchForm();
		searchForm.setHoldingAccountId(reconcilePositionAccount.getHoldingAccount().getId());
		searchForm.setPositionDate(reconcilePositionAccount.getPositionDate());
		if (reconcilePositionAccount.getDefinition().getInvestmentGroup() != null) {
			searchForm.setInvestmentGroupId(reconcilePositionAccount.getDefinition().getInvestmentGroup().getId());
		}

		List<ReconcilePositionExternalDaily> result = getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyList(searchForm);
		return result == null ? new ArrayList<>() : result;
	}


	@Override
	public List<ReconcilePositionExternalDailyPNL> getExternalReconcilePNLList(ReconcilePositionAccount reconcilePositionAccount) {
		ReconcilePositionExternalDailyPNLSearchForm searchForm = new ReconcilePositionExternalDailyPNLSearchForm();
		searchForm.setHoldingAccountId(reconcilePositionAccount.getHoldingAccount().getId());
		searchForm.setPositionDate(reconcilePositionAccount.getPositionDate());
		if (reconcilePositionAccount.getDefinition().getInvestmentGroup() != null) {
			searchForm.setInvestmentGroupId(reconcilePositionAccount.getDefinition().getInvestmentGroup().getId());
		}

		List<ReconcilePositionExternalDailyPNL> result = getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyPNLList(searchForm);
		return result == null ? new ArrayList<>() : result;
	}


	@Override
	public ReconcilePositionComparison getReconcilePositionComparison(ReconcilePosition reconcilePosition) {
		ReconcilePositionComparison comparison = new ReconcilePositionComparison();

		// set top-level fields
		comparison.setHoldingAccount(reconcilePosition.getPositionAccount().getHoldingAccount());
		comparison.setClientAccount(reconcilePosition.getPositionAccount().getClientAccount());
		comparison.setSecurity(reconcilePosition.getInvestmentSecurity());

		// compile list of AccountingDailyPositions and sort
		ReconcilePositionAccountingDailyGroupingSearchForm ourSearchForm = new ReconcilePositionAccountingDailyGroupingSearchForm();
		ourSearchForm.setHoldingInvestmentAccountId(reconcilePosition.getPositionAccount().getHoldingAccount().getId());
		ourSearchForm.setPositionDate(reconcilePosition.getPositionAccount().getPositionDate());
		ourSearchForm.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		// exclude closed positions for matching
		ourSearchForm.setClosedPosition(false);
		List<ReconcilePositionAccountingDailyGrouping> ourPositions = getReconcilePositionAccountingService().getReconcilePositionAccountingDailyGroupingList(ourSearchForm);

		if (!CollectionUtils.isEmpty(ourPositions)) {
			ReconcilePositionAccountingDailyGrouping firstPosition = CollectionUtils.getFirstElement(ourPositions);
			if (firstPosition != null) {
				comparison.setMarketPrice(firstPosition.getMarketPrice());
				comparison.setFxRate(firstPosition.getMarketFxRate());
			}

			Collections.sort(ourPositions, (p1, p2) -> {
				int priceComparison = p1.getPrice().compareTo(p2.getPrice());
				if (priceComparison != 0) {
					return priceComparison;
				}

				int dateComparison = p1.getOriginalTransactionDate().compareTo(p2.getOriginalTransactionDate());
				if (dateComparison != 0) {
					return dateComparison;
				}

				return p1.getRemainingQuantity().compareTo(p2.getRemainingQuantity());
			});
		}

		// compile list of ReconcilePositionExternals and sort
		ReconcilePositionExternalDailyGroupingSearchForm externalSearchForm = new ReconcilePositionExternalDailyGroupingSearchForm();
		externalSearchForm.setHoldingAccountId(reconcilePosition.getPositionAccount().getHoldingAccount().getId());
		externalSearchForm.setPositionDate(reconcilePosition.getPositionAccount().getPositionDate());
		externalSearchForm.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		List<ReconcilePositionExternalDailyGrouping> externalPositions = getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyGroupingList(externalSearchForm);

		if (!CollectionUtils.isEmpty(externalPositions)) {
			ReconcilePositionExternalDailyGrouping firstPosition = CollectionUtils.getFirstElement(externalPositions);
			if (firstPosition != null) {
				comparison.setExternalMarketPrice(firstPosition.getMarketPrice());
				comparison.setExternalFxRate(firstPosition.getFxRate());
			}

			Collections.sort(externalPositions, (p1, p2) -> {
				// Ensure Price is first priority for comparison, will help avoid tie-breaker scenarios
				int priceComparison = p1.getTradePrice().compareTo(p2.getTradePrice());
				if (priceComparison != 0) {
					return priceComparison;
				}

				int dateComparison = p1.getTradeDate().compareTo(p2.getTradeDate());
				if (dateComparison != 0) {
					return dateComparison;
				}

				return p1.getQuantity().compareTo(p2.getQuantity());
			});
		}

		// Find matches between internal/external positions, and create list of matched objects
		MatchingResult<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDailyGrouping> result = null;
		if ((!CollectionUtils.isEmpty(externalPositions)) && (!CollectionUtils.isEmpty(ourPositions))) {
			result = getReconcilePositionManyToManyMatcher().match(ourPositions, externalPositions, 1, true);
		}

		List<ReconcileDisplayPosition> mappedPositions = new ArrayList<>();

		if (result != null) {
			for (MatchingResultItem<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDailyGrouping> match : CollectionUtils.getIterable(result.getItemList())) {
				ReconcilePositionAccountingDailyGrouping internal = CollectionUtils.getOnlyElement(match.getInternalList());
				ReconcilePositionExternalDailyGrouping external = CollectionUtils.getOnlyElement(match.getExternalList());

				mappedPositions.add(createDisplayPosition(internal, external));

				// removed matched items from external and internal lists
				ourPositions.remove(internal);
				externalPositions.remove(external);
			}
		}

		// if there is one position left on both sides, match it by default
		if (ourPositions.size() == 1 && externalPositions.size() == 1) {
			ReconcilePositionAccountingDailyGrouping internal = CollectionUtils.getOnlyElement(ourPositions);
			ReconcilePositionExternalDailyGrouping external = CollectionUtils.getOnlyElement(externalPositions);

			mappedPositions.add(createDisplayPosition(internal, external));

			ourPositions.remove(internal);
			externalPositions.remove(external);
		}
		// Include closed positions for unmatched list (no closed positions should match the external side, as they are all open)
		ReconcilePositionAccountingDailyGroupingSearchForm closedPositionsSearchForm = new ReconcilePositionAccountingDailyGroupingSearchForm();

		closedPositionsSearchForm.setHoldingInvestmentAccountId(reconcilePosition.getPositionAccount().getHoldingAccount().getId());
		closedPositionsSearchForm.setPositionDate(reconcilePosition.getPositionAccount().getPositionDate());
		closedPositionsSearchForm.setInvestmentSecurityId(reconcilePosition.getInvestmentSecurity().getId());
		closedPositionsSearchForm.setClosedPosition(true);

		List<ReconcilePositionAccountingDailyGrouping> ourClosedPositions = getReconcilePositionAccountingService().getReconcilePositionAccountingDailyGroupingList(closedPositionsSearchForm);

		ourPositions.addAll(ourClosedPositions);

		comparison.setMappedPositions(mappedPositions);
		comparison.setUnmappedInternalPositions(ourPositions);
		comparison.setUnmappedExternalPositions(externalPositions);

		return comparison;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	private ReconcileDisplayPosition createDisplayPosition(ReconcilePositionAccountingDailyGrouping internal, ReconcilePositionExternalDailyGrouping external) {
		ReconcileDisplayPosition combinedPosition = new ReconcileDisplayPosition();

		// set internal position properties
		combinedPosition.setOurTotalPositions(internal.getTotalPositions());
		combinedPosition.setAccountingTransactionId(internal.getAccountingTransactionId());
		combinedPosition.setAccountingJournalId(internal.getAccountingJournalId());
		combinedPosition.setDetailScreenClass(internal.getDetailScreenClass());
		combinedPosition.setOurTradeDate(internal.getOriginalTransactionDate());
		combinedPosition.setOurSettleDate(internal.getSettlementDate());
		combinedPosition.setOurFxRate(internal.getMarketFxRate());
		combinedPosition.setOurTradePrice(internal.getPrice());
		combinedPosition.setOurPrice(internal.getMarketPrice());
		combinedPosition.setOurQuantity(internal.getRemainingQuantity());
		combinedPosition.setOurRemainingCostBasisLocal(internal.getRemainingCostBasisLocal());
		combinedPosition.setOurRemainingCostBasisBase(internal.getRemainingCostBasisBase());
		combinedPosition.setOurMarketValueLocal(internal.getNotionalValueLocal());
		combinedPosition.setOurMarketValueBase(internal.getNotionalValueBase());
		combinedPosition.setOurOpenTradeEquityLocal(internal.getOpenTradeEquityLocal());
		combinedPosition.setOurOpenTradeEquityBase(internal.getOpenTradeEquityBase());

		// set external position properties
		combinedPosition.setExternalTotalPositions(external.getTotalPositions());
		combinedPosition.setExternalTradeDate(external.getTradeDate());
		combinedPosition.setExternalSettleDate(external.getSettleDate());
		combinedPosition.setExternalFxRate(external.getFxRate());
		combinedPosition.setExternalTradePrice(external.getTradePrice());
		combinedPosition.setExternalPrice(external.getMarketPrice());
		combinedPosition.setExternalQuantity(external.getQuantity());
		combinedPosition.setExternalRemainingCostBasisLocal(external.getRemainingCostBasisLocal());
		combinedPosition.setExternalRemainingCostBasisBase(external.getRemainingCostBasisBase());
		combinedPosition.setExternalMarketValueLocal(external.getMarketValueLocal());
		combinedPosition.setExternalMarketValueBase(external.getMarketValueBase());
		combinedPosition.setExternalOpenTradeEquityLocal(external.getOpenTradeEquityLocal());
		combinedPosition.setExternalOpenTradeEquityBase(external.getOpenTradeEquityBase());

		return combinedPosition;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////
	public AdvancedUpdatableDAO<ReconcilePositionExternalDaily, Criteria> getReconcilePositionExternalDailyDAO() {
		return this.reconcilePositionExternalDailyDAO;
	}


	public void setReconcilePositionExternalDailyDAO(AdvancedUpdatableDAO<ReconcilePositionExternalDaily, Criteria> reconcilePositionExternalDailyDAO) {
		this.reconcilePositionExternalDailyDAO = reconcilePositionExternalDailyDAO;
	}


	public AccountingPositionDailyService getAccountingPositionDailyService() {
		return this.accountingPositionDailyService;
	}


	public void setAccountingPositionDailyService(AccountingPositionDailyService accountingPositionDailyService) {
		this.accountingPositionDailyService = accountingPositionDailyService;
	}


	public ReconcilePositionExternalDailyService getReconcilePositionExternalDailyService() {
		return this.reconcilePositionExternalDailyService;
	}


	public void setReconcilePositionExternalDailyService(ReconcilePositionExternalDailyService reconcilePositionExternalDailyService) {
		this.reconcilePositionExternalDailyService = reconcilePositionExternalDailyService;
	}


	public ReconcilePositionAccountingService getReconcilePositionAccountingService() {
		return this.reconcilePositionAccountingService;
	}


	public void setReconcilePositionAccountingService(ReconcilePositionAccountingService reconcilePositionAccountingService) {
		this.reconcilePositionAccountingService = reconcilePositionAccountingService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public MatchingMatcher<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDailyGrouping> getReconcilePositionManyToManyMatcher() {
		return this.reconcilePositionManyToManyMatcher;
	}


	public void setReconcilePositionManyToManyMatcher(MatchingMatcher<ReconcilePositionAccountingDailyGrouping, ReconcilePositionExternalDailyGrouping> reconcilePositionManyToManyMatcher) {
		this.reconcilePositionManyToManyMatcher = reconcilePositionManyToManyMatcher;
	}
}
