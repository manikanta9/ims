package com.clifton.reconcile.position.external.search;


import com.clifton.core.dataaccess.search.SearchField;


public class ReconcilePositionExternalDailyGroupingSearchForm extends ReconcilePositionExternalDailySearchForm {

	@SearchField
	private Integer totalPositions;


	public Integer getTotalPositions() {
		return this.totalPositions;
	}


	public void setTotalPositions(Integer totalPositions) {
		this.totalPositions = totalPositions;
	}
}
