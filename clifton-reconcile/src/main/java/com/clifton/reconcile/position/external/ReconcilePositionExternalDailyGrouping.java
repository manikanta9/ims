package com.clifton.reconcile.position.external;


/**
 * The <code>ReconcilePositionExternalDailyGrouping</code> class is an extended DTO that groups
 * ReconcilePositionExternalDaily objects for easily viewing positions at the fill-level.
 *
 * @author rbrooks
 */
public class ReconcilePositionExternalDailyGrouping extends ReconcilePositionExternalDaily {

	/**
	 * The total number of underlying position fills
	 */
	private int totalPositions;


	public int getTotalPositions() {
		return this.totalPositions;
	}


	public void setTotalPositions(int totalPositions) {
		this.totalPositions = totalPositions;
	}
}
