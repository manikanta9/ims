package com.clifton.reconcile.position.external;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailyGroupingSearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailyPNLSearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailySearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalPriceOverrideSearchForm;

import java.util.Date;
import java.util.List;


public interface ReconcilePositionExternalDailyService {

	////////////////////////////////////////////////////////////////////////////////////
	////////        ReconcilePositionExternalDaily Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////////////
	public List<ReconcilePositionExternalDaily> getReconcilePositionExternalDailyList(ReconcilePositionExternalDailySearchForm searchForm);


	public List<ReconcilePositionExternalDaily> getReconcilePositionExternalDailyListUnmapped(Date positionDate, int holdingAccountId);


	@DoNotAddRequestMapping
	public void deleteReconcilePositionExternalDaily(List<ReconcilePositionExternalDaily> externalDailyList);


	@DoNotAddRequestMapping
	public void saveReconcilePositionExternalDaily(List<ReconcilePositionExternalDaily> beanList);


	@DoNotAddRequestMapping
	public int getReconcilePositionExternalDailyCount(Date positionDate, int holdingAccountId);

	/////////////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionExternalDailyGrouping Business Methods     /////////////
	/////////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public List<ReconcilePositionExternalDailyGrouping> getReconcilePositionExternalDailyGroupingList(ReconcilePositionExternalDailyGroupingSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////////////
	////////       ReconcilePositionExternalDailyPNL Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////////////


	public List<ReconcilePositionExternalDailyPNL> getReconcilePositionExternalDailyPNLList(ReconcilePositionExternalDailyPNLSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////////////
	////////   ReconcilePositionExternalPriceOverride Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////////////
	public ReconcilePositionExternalPriceOverride getReconcilePositionExternalPriceOverride(int id);


	public List<ReconcilePositionExternalPriceOverride> getReconcilePositionExternalPriceOverrideList(ReconcilePositionExternalPriceOverrideSearchForm searchForm);


	public ReconcilePositionExternalPriceOverride saveReconcilePositionExternalPriceOverride(ReconcilePositionExternalPriceOverride bean);


	public void deleteReconcilePositionExternalPriceOverride(int id);


	@DoNotAddRequestMapping
	public void loadReconcilePriceOverrides(Date date);

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public AccountingM2MDaily createOrUpdateM2MPriceOverrideExpense(ReconcilePositionPriceOverrideM2MExpenseCommand command);
}
