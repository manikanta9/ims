package com.clifton.reconcile.position;


import java.util.Date;


/**
 * The <code>ReconcilePositionRebuildParameters</code> define parameters needed to run a ReconcilePosition Rebuild
 *
 * @author rbrooks
 */
public interface ReconcilePositionRebuildParameters {

	public Integer getInvestmentSecurityId();


	public void setInvestmentSecurityId(Integer investmentSecurityId);


	public Integer getHoldingAccountId();


	public void setHoldingAccountId(Integer holdingAccountId);


	public Date getPositionDate();


	public void setPositionDate(Date positionDate);


	public Boolean getCleanRebuild();


	public void setCleanRebuild(Boolean cleanRebuild);
}
