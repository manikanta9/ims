package com.clifton.reconcile.position.comparison;


import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.reconcile.position.ReconcileDisplayPosition;
import com.clifton.reconcile.position.accounting.ReconcilePositionAccountingDailyGrouping;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyGrouping;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ReconcilePositionComparison</code> object is a display object, with no DB entity.  It contains
 * lists and fields used for fill-level position reconciliation
 *
 * @author rbrooks
 */
public class ReconcilePositionComparison {

	InvestmentAccount holdingAccount;

	InvestmentAccount clientAccount;

	InvestmentSecurity security;

	BigDecimal fxRate;

	BigDecimal marketPrice;

	BigDecimal externalFxRate;

	BigDecimal externalMarketPrice;

	// A list of positions (ours and external combined) that were successfully mapped
	List<ReconcileDisplayPosition> mappedPositions;

	//  A list of our positions that could not be mapped to an external position
	List<ReconcilePositionAccountingDailyGrouping> unmappedInternalPositions;

	// A list of external positions that could not be mapped to one of our internal positions
	List<ReconcilePositionExternalDailyGrouping> unmappedExternalPositions;


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}


	public InvestmentSecurity getSecurity() {
		return this.security;
	}


	public void setSecurity(InvestmentSecurity security) {
		this.security = security;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getExternalFxRate() {
		return this.externalFxRate;
	}


	public void setExternalFxRate(BigDecimal externalFxRate) {
		this.externalFxRate = externalFxRate;
	}


	public BigDecimal getExternalMarketPrice() {
		return this.externalMarketPrice;
	}


	public void setExternalMarketPrice(BigDecimal externalMarketPrice) {
		this.externalMarketPrice = externalMarketPrice;
	}


	public List<ReconcileDisplayPosition> getMappedPositions() {
		return this.mappedPositions;
	}


	public void setMappedPositions(List<ReconcileDisplayPosition> mappedPositions) {
		this.mappedPositions = mappedPositions;
	}


	public List<ReconcilePositionAccountingDailyGrouping> getUnmappedInternalPositions() {
		return this.unmappedInternalPositions;
	}


	public void setUnmappedInternalPositions(List<ReconcilePositionAccountingDailyGrouping> unmappedInternalPositions) {
		this.unmappedInternalPositions = unmappedInternalPositions;
	}


	public List<ReconcilePositionExternalDailyGrouping> getUnmappedExternalPositions() {
		return this.unmappedExternalPositions;
	}


	public void setUnmappedExternalPositions(List<ReconcilePositionExternalDailyGrouping> unmappedExternalPositions) {
		this.unmappedExternalPositions = unmappedExternalPositions;
	}
}
