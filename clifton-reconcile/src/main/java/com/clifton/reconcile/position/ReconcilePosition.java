package com.clifton.reconcile.position;


import com.clifton.accounting.account.AccountingAccount;
import com.clifton.accounting.gl.AccountingTransaction;
import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.ObjectUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


public class ReconcilePosition extends BaseEntity<Integer> implements LabeledObject {

	private ReconcilePositionAccount positionAccount;
	private AccountingAccount account;
	private AccountingTransaction accountingTransaction;
	private InvestmentSecurity investmentSecurity;

	private boolean reconciled; // specifies whether positions (qty, ote, market value) are the same
	private boolean realizedAndCommissionReconciled;
	private boolean quantityReconciled;
	private boolean matched;
	private String note;
	private String realizedAndCommissionNote;

	private BigDecimal ourFxRate;
	private BigDecimal ourPrice;
	private BigDecimal ourQuantity;
	private BigDecimal ourTodayClosedQuantity;
	private BigDecimal ourMarketValueLocal;
	private BigDecimal ourMarketValueBase;
	private BigDecimal ourOpenTradeEquityLocal;
	private BigDecimal ourOpenTradeEquityBase;
	private BigDecimal ourOTEVariation;
	private BigDecimal ourTodayCommissionLocal; // includes all commissions and fees: accountingAccount.commission == true
	private BigDecimal ourTodayRealizedGainLossLocal;

	private BigDecimal externalFxRate;
	private BigDecimal externalPrice;
	private BigDecimal externalQuantity;
	private BigDecimal externalTodayClosedQuantity;
	private BigDecimal externalMarketValueLocal;
	private BigDecimal externalMarketValueBase;
	private BigDecimal externalOpenTradeEquityLocal;
	private BigDecimal externalOpenTradeEquityBase;
	private BigDecimal externalOTEVariation;
	private BigDecimal externalTodayCommissionLocal; // includes all commissions and fees: accountingAccount.commission == true
	private BigDecimal externalTodayRealizedGainLossLocal;


	//private BigDecimal externalTodayRealizedGainLossBase;


	@Override
	public String getLabel() {
		String label = "";
		if (getPositionAccount() != null && getInvestmentSecurity() != null) {
			label = getPositionAccount().getLabel() + " [" + getInvestmentSecurity().getSymbol() + "]";
		}
		return label;
	}


	///////////////////////////////////////////////////////////////////
	//////////////  	String Difference Methods		///////////////
	///////////////////////////////////////////////////////////////////


	public BigDecimal getFxRateDifference() {
		return getDifference(getOurFxRate(), getExternalFxRate());
	}


	public BigDecimal getPriceDifference() {
		return getDifference(getOurPrice(), getExternalPrice());
	}


	public BigDecimal getQuantityDifference() {
		return getDifference(getOurQuantity(), getExternalQuantity());
	}


	public BigDecimal getClosedQuantityDifference() {
		return getDifference(getOurTodayClosedQuantity(), getExternalTodayClosedQuantity());
	}


	public BigDecimal getMarketValueLocalDifference() {
		return getDifference(getOurMarketValueLocal(), getExternalMarketValueLocal());
	}


	public BigDecimal getMarketValueBaseDifference() {
		return getDifference(getOurMarketValueBase(), getExternalMarketValueBase());
	}


	public BigDecimal getOteLocalDifference() {
		return getDifference(getOurOpenTradeEquityLocal(), getExternalOpenTradeEquityLocal());
	}


	public BigDecimal getOteBaseDifference() {
		return getDifference(getOurOpenTradeEquityBase(), getExternalOpenTradeEquityBase());
	}


	public BigDecimal getCommissionLocalDifference() {
		return getDifference(getOurTodayCommissionLocal(), getExternalTodayCommissionLocal());
	}


	public BigDecimal getCommissionBaseDifference() {
		BigDecimal result = getCommissionLocalDifference();
		if (result != null) {
			result = MathUtils.multiply(result, ObjectUtils.coalesce(getOurFxRate(), getExternalFxRate()), result.scale());
		}
		return result;
	}


	public BigDecimal getRealizedGainLossLocalDifference() {
		return getDifference(getOurTodayRealizedGainLossLocal(), getExternalTodayRealizedGainLossLocal());
	}


	public BigDecimal getRealizedGainLossBaseDifference() {
		BigDecimal result = getRealizedGainLossLocalDifference();
		if (result != null) {
			result = MathUtils.multiply(result, ObjectUtils.coalesce(getOurFxRate(), getExternalFxRate()), result.scale());
		}
		return result;
	}


	///////////////////////////////////////////////////////////
	////////////	 Helper Methods		///////////////////////
	///////////////////////////////////////////////////////////


	protected BigDecimal getDifference(BigDecimal n1, BigDecimal n2) {
		BigDecimal result = MathUtils.subtract(n1, n2);
		if (result == null) {
			return BigDecimal.ZERO;
		}
		return result;
	}


	public boolean isMarketValueEqual() {
		return MathUtils.isEqual(getOurMarketValueLocal(), getExternalMarketValueLocal()) && MathUtils.isEqual(getOurMarketValueBase(), getExternalMarketValueBase());
	}


	public boolean isOTEEqual() {
		return MathUtils.isEqual(getOurOpenTradeEquityLocal(), getExternalOpenTradeEquityLocal()) && MathUtils.isEqual(getOurOpenTradeEquityBase(), getExternalOpenTradeEquityBase());
	}


	public boolean isRealizedGainLossEqual() {
		return MathUtils.isEqual(getOurTodayRealizedGainLossLocal(), getExternalTodayRealizedGainLossLocal());
	}


	public boolean isCommissionEqual() {
		return MathUtils.isEqual(getOurTodayCommissionLocal(), getExternalTodayCommissionLocal());
	}


	//////////////////////////////////////////////////////
	///////////		Getters and Setters		//////////////
	//////////////////////////////////////////////////////


	public ReconcilePositionAccount getPositionAccount() {
		return this.positionAccount;
	}


	public void setPositionAccount(ReconcilePositionAccount positionAccount) {
		this.positionAccount = positionAccount;
	}


	public AccountingAccount getAccount() {
		return this.account;
	}


	public void setAccount(AccountingAccount account) {
		this.account = account;
	}


	public AccountingTransaction getAccountingTransaction() {
		return this.accountingTransaction;
	}


	public void setAccountingTransaction(AccountingTransaction accountingTransaction) {
		this.accountingTransaction = accountingTransaction;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public BigDecimal getOurFxRate() {
		return this.ourFxRate;
	}


	public void setOurFxRate(BigDecimal ourFxRate) {
		this.ourFxRate = ourFxRate;
	}


	public BigDecimal getOurPrice() {
		return this.ourPrice;
	}


	public void setOurPrice(BigDecimal ourPrice) {
		this.ourPrice = ourPrice;
	}


	public BigDecimal getOurQuantity() {
		return this.ourQuantity;
	}


	public void setOurQuantity(BigDecimal ourQuantity) {
		this.ourQuantity = ourQuantity;
	}


	public BigDecimal getOurMarketValueLocal() {
		return this.ourMarketValueLocal;
	}


	public void setOurMarketValueLocal(BigDecimal ourMarketValueLocal) {
		this.ourMarketValueLocal = ourMarketValueLocal;
	}


	public BigDecimal getOurMarketValueBase() {
		return this.ourMarketValueBase;
	}


	public void setOurMarketValueBase(BigDecimal ourMarketValueBase) {
		this.ourMarketValueBase = ourMarketValueBase;
	}


	public BigDecimal getOurOTEVariation() {
		return this.ourOTEVariation;
	}


	public void setOurOTEVariation(BigDecimal ourOTEVariation) {
		this.ourOTEVariation = ourOTEVariation;
	}


	public BigDecimal getExternalFxRate() {
		return this.externalFxRate;
	}


	public void setExternalFxRate(BigDecimal externalFxRate) {
		this.externalFxRate = externalFxRate;
	}


	public BigDecimal getExternalPrice() {
		return this.externalPrice;
	}


	public void setExternalPrice(BigDecimal externalPrice) {
		this.externalPrice = externalPrice;
	}


	public BigDecimal getExternalQuantity() {
		return this.externalQuantity;
	}


	public void setExternalQuantity(BigDecimal externalQuantity) {
		this.externalQuantity = externalQuantity;
	}


	public BigDecimal getExternalMarketValueLocal() {
		return this.externalMarketValueLocal;
	}


	public void setExternalMarketValueLocal(BigDecimal externalMarketValueLocal) {
		this.externalMarketValueLocal = externalMarketValueLocal;
	}


	public BigDecimal getExternalMarketValueBase() {
		return this.externalMarketValueBase;
	}


	public void setExternalMarketValueBase(BigDecimal externalMarketValueBase) {
		this.externalMarketValueBase = externalMarketValueBase;
	}


	public BigDecimal getExternalOTEVariation() {
		return this.externalOTEVariation;
	}


	public void setExternalOTEVariation(BigDecimal externalOTEVariation) {
		this.externalOTEVariation = externalOTEVariation;
	}


	public boolean isMatched() {
		return this.matched;
	}


	public void setMatched(boolean matched) {
		this.matched = matched;
	}


	public boolean isQuantityReconciled() {
		return this.quantityReconciled;
	}


	public void setQuantityReconciled(boolean quantityReconciled) {
		this.quantityReconciled = quantityReconciled;
	}


	public BigDecimal getOurOpenTradeEquityLocal() {
		return this.ourOpenTradeEquityLocal;
	}


	public void setOurOpenTradeEquityLocal(BigDecimal ourOpenTradeEquityLocal) {
		this.ourOpenTradeEquityLocal = ourOpenTradeEquityLocal;
	}


	public BigDecimal getOurOpenTradeEquityBase() {
		return this.ourOpenTradeEquityBase;
	}


	public void setOurOpenTradeEquityBase(BigDecimal ourOpenTradeEquityBase) {
		this.ourOpenTradeEquityBase = ourOpenTradeEquityBase;
	}


	public BigDecimal getExternalOpenTradeEquityLocal() {
		return this.externalOpenTradeEquityLocal;
	}


	public void setExternalOpenTradeEquityLocal(BigDecimal externalOpenTradeEquityLocal) {
		this.externalOpenTradeEquityLocal = externalOpenTradeEquityLocal;
	}


	public BigDecimal getExternalOpenTradeEquityBase() {
		return this.externalOpenTradeEquityBase;
	}


	public void setExternalOpenTradeEquityBase(BigDecimal externalOpenTradeEquityBase) {
		this.externalOpenTradeEquityBase = externalOpenTradeEquityBase;
	}


	public BigDecimal getOurTodayClosedQuantity() {
		return this.ourTodayClosedQuantity;
	}


	public void setOurTodayClosedQuantity(BigDecimal ourTodayClosedQuantity) {
		this.ourTodayClosedQuantity = ourTodayClosedQuantity;
	}


	public BigDecimal getOurTodayCommissionLocal() {
		return this.ourTodayCommissionLocal;
	}


	public void setOurTodayCommissionLocal(BigDecimal ourTodayCommissionLocal) {
		this.ourTodayCommissionLocal = ourTodayCommissionLocal;
	}


	public BigDecimal getOurTodayRealizedGainLossLocal() {
		return this.ourTodayRealizedGainLossLocal;
	}


	public void setOurTodayRealizedGainLossLocal(BigDecimal ourTodayRealizedGainLossLocal) {
		this.ourTodayRealizedGainLossLocal = ourTodayRealizedGainLossLocal;
	}


	public BigDecimal getExternalTodayClosedQuantity() {
		return this.externalTodayClosedQuantity;
	}


	public void setExternalTodayClosedQuantity(BigDecimal externalTodayClosedQuantity) {
		this.externalTodayClosedQuantity = externalTodayClosedQuantity;
	}


	public BigDecimal getExternalTodayCommissionLocal() {
		return this.externalTodayCommissionLocal;
	}


	public void setExternalTodayCommissionLocal(BigDecimal externalTodayCommissionLocal) {
		this.externalTodayCommissionLocal = externalTodayCommissionLocal;
	}


	public BigDecimal getExternalTodayRealizedGainLossLocal() {
		return this.externalTodayRealizedGainLossLocal;
	}


	public void setExternalTodayRealizedGainLossLocal(BigDecimal externalTodayRealizedGainLossLocal) {
		this.externalTodayRealizedGainLossLocal = externalTodayRealizedGainLossLocal;
	}


	public boolean isRealizedAndCommissionReconciled() {
		return this.realizedAndCommissionReconciled;
	}


	public void setRealizedAndCommissionReconciled(boolean realizedAndCommissionReconciled) {
		this.realizedAndCommissionReconciled = realizedAndCommissionReconciled;
	}


	public String getRealizedAndCommissionNote() {
		return this.realizedAndCommissionNote;
	}


	public void setRealizedAndCommissionNote(String realizedAndCommissionNote) {
		this.realizedAndCommissionNote = realizedAndCommissionNote;
	}
}
