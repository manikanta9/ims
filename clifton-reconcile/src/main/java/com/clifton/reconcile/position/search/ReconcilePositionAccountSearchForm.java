package com.clifton.reconcile.position.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class ReconcilePositionAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "definition.id")
	private Integer definitionId;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "name,number", searchFieldPath = "holdingAccount", sortField = "number")
	private String holdingAccount;

	@SearchField(searchField = "name", searchFieldPath = "holdingAccount.issuingCompany")
	private String holdingAccountIssuerName;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingAccount")
	private Integer holdingAccountIssuerId;

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "name,number", searchFieldPath = "clientAccount", sortField = "number")
	private String clientAccount;

	@SearchField
	private Date positionDate;

	@SearchField
	private Boolean reconciled;

	@SearchField
	private Boolean realizedAndCommissionReconciled;

	@SearchField
	private Boolean quantityReconciled;

	// Custom Search Filter
	private String excludeInvestmentAccountGroupName;

	// Custom Search Filter
	private Integer clientAccountGroupId;


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public String getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(String holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public Boolean getQuantityReconciled() {
		return this.quantityReconciled;
	}


	public void setQuantityReconciled(Boolean quantityReconciled) {
		this.quantityReconciled = quantityReconciled;
	}


	public String getHoldingAccountIssuerName() {
		return this.holdingAccountIssuerName;
	}


	public void setHoldingAccountIssuerName(String holdingAccountIssuerName) {
		this.holdingAccountIssuerName = holdingAccountIssuerName;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public String getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(String clientAccount) {
		this.clientAccount = clientAccount;
	}


	public Boolean getRealizedAndCommissionReconciled() {
		return this.realizedAndCommissionReconciled;
	}


	public void setRealizedAndCommissionReconciled(Boolean realizedAndCommissionReconciled) {
		this.realizedAndCommissionReconciled = realizedAndCommissionReconciled;
	}


	public Integer getHoldingAccountIssuerId() {
		return this.holdingAccountIssuerId;
	}


	public void setHoldingAccountIssuerId(Integer holdingAccountIssuerId) {
		this.holdingAccountIssuerId = holdingAccountIssuerId;
	}


	public String getExcludeInvestmentAccountGroupName() {
		return this.excludeInvestmentAccountGroupName;
	}


	public void setExcludeInvestmentAccountGroupName(String excludeInvestmentAccountGroupName) {
		this.excludeInvestmentAccountGroupName = excludeInvestmentAccountGroupName;
	}


	public Integer getClientAccountGroupId() {
		return this.clientAccountGroupId;
	}


	public void setClientAccountGroupId(Integer clientAccountGroupId) {
		this.clientAccountGroupId = clientAccountGroupId;
	}
}
