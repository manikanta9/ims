package com.clifton.reconcile.position;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ReconcileDisplayPosition</code> class objects are not persisted.  They're used for display purposes or processing.
 *
 * @author vgomelsky
 */
public class ReconcileDisplayPosition extends ReconcilePosition {

	/*
	 * Our Fields
	 */
	private long accountingTransactionId;
	private int accountingJournalId;
	private String detailScreenClass;

	private int ourTotalPositions;
	private Date ourTradeDate; // openDate: the date this position was open: originalTransactionDate
	private Date ourSettleDate;
	private BigDecimal ourTradePrice;
	private BigDecimal ourRemainingCostBasisLocal;
	private BigDecimal ourRemainingCostBasisBase;

	/*
	 * External Fields
	 */
	private int externalTotalPositions;
	private Date externalTradeDate;
	private Date externalSettleDate;
	private BigDecimal externalTradePrice;
	private BigDecimal externalRemainingCostBasisLocal;
	private BigDecimal externalRemainingCostBasisBase;


	//////////////////////////////////////////////////
	//////////		Difference Methods		//////////
	//////////////////////////////////////////////////


	public BigDecimal getTradePriceDifference() {
		return getDifference(getOurTradePrice(), getExternalTradePrice());
	}


	public BigDecimal getCostBasisLocalDifference() {
		return getDifference(getOurRemainingCostBasisLocal(), getExternalRemainingCostBasisLocal());
	}


	public BigDecimal getCostBasisBaseDifference() {
		return getDifference(getOurRemainingCostBasisBase(), getExternalRemainingCostBasisBase());
	}


	//////////////////////////////////////////////////
	///////////		Getters and Setters		//////////
	//////////////////////////////////////////////////


	public long getAccountingTransactionId() {
		return this.accountingTransactionId;
	}


	public void setAccountingTransactionId(long accountingTransactionId) {
		this.accountingTransactionId = accountingTransactionId;
	}


	public int getAccountingJournalId() {
		return this.accountingJournalId;
	}


	public void setAccountingJournalId(int accountingJournalId) {
		this.accountingJournalId = accountingJournalId;
	}


	public String getDetailScreenClass() {
		return this.detailScreenClass;
	}


	public void setDetailScreenClass(String detailScreenClass) {
		this.detailScreenClass = detailScreenClass;
	}


	public int getOurTotalPositions() {
		return this.ourTotalPositions;
	}


	public void setOurTotalPositions(int ourTotalPositions) {
		this.ourTotalPositions = ourTotalPositions;
	}


	public Date getOurTradeDate() {
		return this.ourTradeDate;
	}


	public void setOurTradeDate(Date ourTradeDate) {
		this.ourTradeDate = ourTradeDate;
	}


	public Date getOurSettleDate() {
		return this.ourSettleDate;
	}


	public void setOurSettleDate(Date ourSettleDate) {
		this.ourSettleDate = ourSettleDate;
	}


	public BigDecimal getOurTradePrice() {
		return this.ourTradePrice;
	}


	public void setOurTradePrice(BigDecimal ourTradePrice) {
		this.ourTradePrice = ourTradePrice;
	}


	public BigDecimal getOurRemainingCostBasisLocal() {
		return this.ourRemainingCostBasisLocal;
	}


	public void setOurRemainingCostBasisLocal(BigDecimal ourRemainingCostBasisLocal) {
		this.ourRemainingCostBasisLocal = ourRemainingCostBasisLocal;
	}


	public BigDecimal getOurRemainingCostBasisBase() {
		return this.ourRemainingCostBasisBase;
	}


	public void setOurRemainingCostBasisBase(BigDecimal ourRemainingCostBasisBase) {
		this.ourRemainingCostBasisBase = ourRemainingCostBasisBase;
	}


	public int getExternalTotalPositions() {
		return this.externalTotalPositions;
	}


	public void setExternalTotalPositions(int externalTotalPositions) {
		this.externalTotalPositions = externalTotalPositions;
	}


	public Date getExternalTradeDate() {
		return this.externalTradeDate;
	}


	public void setExternalTradeDate(Date externalTradeDate) {
		this.externalTradeDate = externalTradeDate;
	}


	public Date getExternalSettleDate() {
		return this.externalSettleDate;
	}


	public void setExternalSettleDate(Date externalSettleDate) {
		this.externalSettleDate = externalSettleDate;
	}


	public BigDecimal getExternalTradePrice() {
		return this.externalTradePrice;
	}


	public void setExternalTradePrice(BigDecimal externalTradePrice) {
		this.externalTradePrice = externalTradePrice;
	}


	public BigDecimal getExternalRemainingCostBasisLocal() {
		return this.externalRemainingCostBasisLocal;
	}


	public void setExternalRemainingCostBasisLocal(BigDecimal externalRemainingCostBasisLocal) {
		this.externalRemainingCostBasisLocal = externalRemainingCostBasisLocal;
	}


	public BigDecimal getExternalRemainingCostBasisBase() {
		return this.externalRemainingCostBasisBase;
	}


	public void setExternalRemainingCostBasisBase(BigDecimal externalRemainingCostBasisBase) {
		this.externalRemainingCostBasisBase = externalRemainingCostBasisBase;
	}
}
