package com.clifton.reconcile.position.external;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionExternalDaily extends BaseSimpleEntity<Integer> {

	private InvestmentAccount holdingAccount;
	private InvestmentAccount clientAccount;
	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity investmentCurrency;
	private InvestmentSecurity baseCurrency;

	private Date positionDate;
	private Date tradeDate;
	private Date settleDate;

	private BigDecimal fxRate;
	private BigDecimal quantity = BigDecimal.ZERO;
	private boolean shortPosition;

	private BigDecimal tradePrice;
	private BigDecimal marketPrice;

	private BigDecimal remainingCostBasisLocal = BigDecimal.ZERO;
	private BigDecimal remainingCostBasisBase = BigDecimal.ZERO;
	private BigDecimal marketValueLocal = BigDecimal.ZERO;
	private BigDecimal marketValueBase = BigDecimal.ZERO;
	private BigDecimal openTradeEquityLocal = BigDecimal.ZERO;
	private BigDecimal openTradeEquityBase = BigDecimal.ZERO;

	private boolean overridden;


	public InvestmentAccount getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(InvestmentAccount holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentSecurity getInvestmentCurrency() {
		return this.investmentCurrency;
	}


	public void setInvestmentCurrency(InvestmentSecurity investmentCurrency) {
		this.investmentCurrency = investmentCurrency;
	}


	public InvestmentSecurity getBaseCurrency() {
		return this.baseCurrency;
	}


	public void setBaseCurrency(InvestmentSecurity baseCurrency) {
		this.baseCurrency = baseCurrency;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Date getSettleDate() {
		return this.settleDate;
	}


	public void setSettleDate(Date settleDate) {
		this.settleDate = settleDate;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public boolean isShortPosition() {
		return this.shortPosition;
	}


	public void setShortPosition(boolean shortPosition) {
		this.shortPosition = shortPosition;
	}


	public BigDecimal getTradePrice() {
		return this.tradePrice;
	}


	public void setTradePrice(BigDecimal tradePrice) {
		this.tradePrice = tradePrice;
	}


	public BigDecimal getMarketPrice() {
		return this.marketPrice;
	}


	public void setMarketPrice(BigDecimal marketPrice) {
		this.marketPrice = marketPrice;
	}


	public BigDecimal getMarketValueLocal() {
		return this.marketValueLocal;
	}


	public void setMarketValueLocal(BigDecimal marketValueLocal) {
		this.marketValueLocal = marketValueLocal;
	}


	public BigDecimal getMarketValueBase() {
		return this.marketValueBase;
	}


	public void setMarketValueBase(BigDecimal marketValueBase) {
		this.marketValueBase = marketValueBase;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getRemainingCostBasisLocal() {
		return this.remainingCostBasisLocal;
	}


	public void setRemainingCostBasisLocal(BigDecimal remainingCostBasisLocal) {
		this.remainingCostBasisLocal = remainingCostBasisLocal;
	}


	public BigDecimal getRemainingCostBasisBase() {
		return this.remainingCostBasisBase;
	}


	public void setRemainingCostBasisBase(BigDecimal remainingCostBasisBase) {
		this.remainingCostBasisBase = remainingCostBasisBase;
	}


	public boolean isOverridden() {
		return this.overridden;
	}


	public void setOverridden(boolean overridden) {
		this.overridden = overridden;
	}


	public InvestmentAccount getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(InvestmentAccount clientAccount) {
		this.clientAccount = clientAccount;
	}
}
