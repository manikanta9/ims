package com.clifton.reconcile.position;


import java.math.BigDecimal;


/**
 * The <code>ReconcilePositionAccountExtended</code> class adds a few calculated fields used in UI.
 *
 * @author Mary Anderson
 */
public class ReconcilePositionAccountExtended extends ReconcilePositionAccount {

	private int positionCount;

	private BigDecimal marketValueDifference;
	private BigDecimal oteValueDifference;
	private BigDecimal todayRealizedGainLossDifference;
	private BigDecimal todayCommissionDifference;


	public int getPositionCount() {
		return this.positionCount;
	}


	public void setPositionCount(int positionCount) {
		this.positionCount = positionCount;
	}


	public BigDecimal getMarketValueDifference() {
		return this.marketValueDifference;
	}


	public void setMarketValueDifference(BigDecimal marketValueDifference) {
		this.marketValueDifference = marketValueDifference;
	}


	public BigDecimal getOteValueDifference() {
		return this.oteValueDifference;
	}


	public void setOteValueDifference(BigDecimal oteValueDifference) {
		this.oteValueDifference = oteValueDifference;
	}


	public BigDecimal getTodayRealizedGainLossDifference() {
		return this.todayRealizedGainLossDifference;
	}


	public void setTodayRealizedGainLossDifference(BigDecimal todayRealizedGainLossDifference) {
		this.todayRealizedGainLossDifference = todayRealizedGainLossDifference;
	}


	public BigDecimal getTodayCommissionDifference() {
		return this.todayCommissionDifference;
	}


	public void setTodayCommissionDifference(BigDecimal todayCommissionDifference) {
		this.todayCommissionDifference = todayCommissionDifference;
	}
}
