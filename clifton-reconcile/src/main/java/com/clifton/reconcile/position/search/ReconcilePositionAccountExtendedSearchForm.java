package com.clifton.reconcile.position.search;


import com.clifton.core.dataaccess.search.SearchField;

import java.math.BigDecimal;


/**
 * The <code>ReconcilePositionAccountExtendedSearchForm</code> ...
 *
 * @author Mary Anderson
 */
public class ReconcilePositionAccountExtendedSearchForm extends ReconcilePositionAccountSearchForm {

	@SearchField
	private Integer positionCount;

	@SearchField
	private BigDecimal marketValueDifference;

	@SearchField
	private BigDecimal oteValueDifference;

	@SearchField
	private BigDecimal todayRealizedGainLossDifference;

	@SearchField
	private BigDecimal todayCommissionDifference;

	// Custom Search Field
	private Boolean excludeClosedPositions;


	public Integer getPositionCount() {
		return this.positionCount;
	}


	public void setPositionCount(Integer positionCount) {
		this.positionCount = positionCount;
	}


	public BigDecimal getMarketValueDifference() {
		return this.marketValueDifference;
	}


	public void setMarketValueDifference(BigDecimal marketValueDifference) {
		this.marketValueDifference = marketValueDifference;
	}


	public BigDecimal getOteValueDifference() {
		return this.oteValueDifference;
	}


	public void setOteValueDifference(BigDecimal oteValueDifference) {
		this.oteValueDifference = oteValueDifference;
	}


	public BigDecimal getTodayRealizedGainLossDifference() {
		return this.todayRealizedGainLossDifference;
	}


	public void setTodayRealizedGainLossDifference(BigDecimal todayRealizedGainLossDifference) {
		this.todayRealizedGainLossDifference = todayRealizedGainLossDifference;
	}


	public BigDecimal getTodayCommissionDifference() {
		return this.todayCommissionDifference;
	}


	public void setTodayCommissionDifference(BigDecimal todayCommissionDifference) {
		this.todayCommissionDifference = todayCommissionDifference;
	}


	public Boolean getExcludeClosedPositions() {
		return this.excludeClosedPositions;
	}


	public void setExcludeClosedPositions(Boolean excludeClosedPositions) {
		this.excludeClosedPositions = excludeClosedPositions;
	}
}
