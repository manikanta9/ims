package com.clifton.reconcile.position;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.account.relationship.InvestmentAccountRelationshipPurpose;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.system.schema.SystemTable;


public class ReconcilePositionDefinition extends NamedEntity<Integer> {

	private SystemTable ourSystemTable;
	private SystemTable externalSystemTable;
	private InvestmentGroup investmentGroup;
	private BusinessCompany holdingCompany;
	private boolean zeroRemainingQuantity;
	private InvestmentAccountRelationshipPurpose accountRelationshipPurpose;
	private boolean localOnly;


	public SystemTable getOurSystemTable() {
		return this.ourSystemTable;
	}


	public void setOurSystemTable(SystemTable ourSystemTable) {
		this.ourSystemTable = ourSystemTable;
	}


	public SystemTable getExternalSystemTable() {
		return this.externalSystemTable;
	}


	public void setExternalSystemTable(SystemTable externalSystemTable) {
		this.externalSystemTable = externalSystemTable;
	}


	public InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}


	public void setInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}


	public BusinessCompany getHoldingCompany() {
		return this.holdingCompany;
	}


	public void setHoldingCompany(BusinessCompany holdingCompany) {
		this.holdingCompany = holdingCompany;
	}


	public boolean isZeroRemainingQuantity() {
		return this.zeroRemainingQuantity;
	}


	public void setZeroRemainingQuantity(boolean zeroRemainingQuantity) {
		this.zeroRemainingQuantity = zeroRemainingQuantity;
	}


	public InvestmentAccountRelationshipPurpose getAccountRelationshipPurpose() {
		return this.accountRelationshipPurpose;
	}


	public void setAccountRelationshipPurpose(InvestmentAccountRelationshipPurpose accountRelationshipPurpose) {
		this.accountRelationshipPurpose = accountRelationshipPurpose;
	}


	public boolean isLocalOnly() {
		return this.localOnly;
	}


	public void setLocalOnly(boolean localOnly) {
		this.localOnly = localOnly;
	}
}
