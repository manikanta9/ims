package com.clifton.reconcile.position.external;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.accounting.m2m.search.AccountingM2MDailyExpenseSearchForm;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.reconcile.position.ReconcilePositionDefinition;
import com.clifton.reconcile.position.ReconcilePositionService;
import com.clifton.reconcile.position.external.cache.ReconcileM2MRebuildCache;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailyGroupingSearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailyPNLSearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailySearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalPriceOverrideSearchForm;
import com.clifton.reconcile.position.processor.ReconcilePositionRebuildCommand;
import com.clifton.reconcile.position.search.ReconcilePositionDefinitionSearchForm;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Service
public class ReconcilePositionExternalDailyServiceImpl implements ReconcilePositionExternalDailyService {

	private AdvancedUpdatableDAO<ReconcilePositionExternalDaily, Criteria> reconcilePositionExternalDailyDAO;
	private AdvancedReadOnlyDAO<ReconcilePositionExternalDailyGrouping, Criteria> reconcilePositionExternalDailyGroupingDAO;
	private AdvancedUpdatableDAO<ReconcilePositionExternalDailyPNL, Criteria> reconcilePositionExternalDailyPNLDAO;
	private AdvancedUpdatableDAO<ReconcilePositionExternalPriceOverride, Criteria> reconcilePositionExternalPriceOverrideDAO;

	private AccountingM2MService accountingM2MService;
	private InvestmentCalculator investmentCalculator;
	private ReconcilePositionService reconcilePositionService;
	private SystemSchemaService systemSchemaService;

	private static final String OUR_RECONCILE_TABLE_NAME = "AccountingPositionDaily";

	////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionExternalDaily Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcilePositionExternalDaily> getReconcilePositionExternalDailyList(ReconcilePositionExternalDailySearchForm searchForm) {
		return getReconcilePositionExternalDailyDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<ReconcilePositionExternalDaily> getReconcilePositionExternalDailyListUnmapped(final Date positionDate, final int holdingAccountId) {
		HibernateSearchConfigurer searchConfigurer = new HibernateSearchConfigurer() {

			@Override
			public boolean configureOrderBy(@SuppressWarnings("unused") Criteria criteria) {
				return true;
			}


			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(Restrictions.eq("positionDate", positionDate))
						.add(Restrictions.eq("holdingAccount.id", holdingAccountId))
						.createAlias("positionList", "pl", JoinType.LEFT_OUTER_JOIN)
						.add(Restrictions.isNull("pl.fkFieldId"));
			}
		};

		return getReconcilePositionExternalDailyDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public void deleteReconcilePositionExternalDaily(List<ReconcilePositionExternalDaily> externalDailyList) {
		getReconcilePositionExternalDailyDAO().deleteList(externalDailyList);
	}


	@Override
	public void saveReconcilePositionExternalDaily(List<ReconcilePositionExternalDaily> beanList) {
		getReconcilePositionExternalDailyDAO().saveList(beanList);
	}


	@Override
	public int getReconcilePositionExternalDailyCount(Date positionDate, int holdingAccountId) {
		ReconcilePositionExternalDailySearchForm searchForm = new ReconcilePositionExternalDailySearchForm();
		searchForm.setHoldingAccountId(holdingAccountId);
		searchForm.setPositionDate(positionDate);
		List<ReconcilePositionExternalDaily> result = getReconcilePositionExternalDailyList(searchForm);
		return result == null ? 0 : result.size();
	}

	/////////////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionExternalDailyGrouping Business Methods     /////////////
	/////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcilePositionExternalDailyGrouping> getReconcilePositionExternalDailyGroupingList(ReconcilePositionExternalDailyGroupingSearchForm searchForm) {
		return getReconcilePositionExternalDailyGroupingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionExternalDailyPNL Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcilePositionExternalDailyPNL> getReconcilePositionExternalDailyPNLList(ReconcilePositionExternalDailyPNLSearchForm searchForm) {
		return getReconcilePositionExternalDailyPNLDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////   ReconcilePositionExternalPriceOverride Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcilePositionExternalPriceOverride getReconcilePositionExternalPriceOverride(int id) {
		return getReconcilePositionExternalPriceOverrideDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ReconcilePositionExternalPriceOverride> getReconcilePositionExternalPriceOverrideList(ReconcilePositionExternalPriceOverrideSearchForm searchForm) {
		return getReconcilePositionExternalPriceOverrideDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public ReconcilePositionExternalPriceOverride saveReconcilePositionExternalPriceOverride(ReconcilePositionExternalPriceOverride bean) {
		if (!bean.isNewBean()) {
			throw new RuntimeException("Cannot re-apply an existing price override.");
		}
		bean = getReconcilePositionExternalPriceOverrideDAO().save(bean);
		rebuildExternalPositionAndCreateExpenses(bean, new ReconcileM2MRebuildCache(getAccountingM2MService(), getAccountingM2MService().getAccountingM2MDailyExpenseTypeByName(AccountingM2MDailyExpenseType.M2M_PRICE_ADJUSTMENT)));
		doRebuildReconcilePosition(bean);
		return bean;
	}


	@Override
	@Transactional
	public void deleteReconcilePositionExternalPriceOverride(int id) {
		ReconcilePositionExternalPriceOverride bean = getReconcilePositionExternalPriceOverride(id);
		getReconcilePositionExternalPriceOverrideDAO().delete(bean);
		// remove the adjustment expenses
		ReconcileM2MRebuildCache m2mRebuildCache = new ReconcileM2MRebuildCache(getAccountingM2MService(), getAccountingM2MService().getAccountingM2MDailyExpenseTypeByName(AccountingM2MDailyExpenseType.M2M_PRICE_ADJUSTMENT));
		removeM2MAdjustmentExpenses(bean, m2mRebuildCache);

		// rebuild with the original price
		bean.setOverridePrice(bean.getExternalPrice());
		rebuildExternalPositionWithoutExpenses(bean, m2mRebuildCache);
		doRebuildReconcilePosition(bean);
	}


	@Override
	public void loadReconcilePriceOverrides(Date date) {
		List<ReconcilePositionExternalPriceOverride> priceOverrideList = getReconcilePositionExternalPriceOverrideListByDate(date);
		ReconcileM2MRebuildCache m2mRebuildCache = new ReconcileM2MRebuildCache(getAccountingM2MService(), getAccountingM2MService().getAccountingM2MDailyExpenseTypeByName(AccountingM2MDailyExpenseType.M2M_PRICE_ADJUSTMENT));
		for (ReconcilePositionExternalPriceOverride priceOverride : CollectionUtils.getIterable(priceOverrideList)) {
			rebuildExternalPositionAndCreateExpenses(priceOverride, m2mRebuildCache);
			doRebuildReconcilePosition(priceOverride);
		}
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Will create the expense for the correct m2mDailyItem and return the m2mDaily entry that will need to be saved.
	 */
	@Override
	public AccountingM2MDaily createOrUpdateM2MPriceOverrideExpense(ReconcilePositionPriceOverrideM2MExpenseCommand command) {
		InvestmentAccount holdingInvestmentAccount = command.getHoldingInvestmentAccount();
		InvestmentSecurity expenseCurrency = holdingInvestmentAccount.getBaseCurrency();
		AccountingM2MDaily m2mDaily = command.getM2mRebuildCache().getM2M(holdingInvestmentAccount, command.getPositionDate(), expenseCurrency);
		if (m2mDaily != null && !m2mDaily.isBooked() && !m2mDaily.isReconciled()) {
			// Get an existing AccountingM2MDailyExpense to update or create a new one
			AccountingM2MDailyExpense accountingExpense = command.getM2mRebuildCache().getM2MExpense(m2mDaily, command.getPriceOverride());
			if (accountingExpense == null) {
				accountingExpense = new AccountingM2MDailyExpense();
				accountingExpense.setSourceFkFieldId(command.getPriceOverride().getId());
				accountingExpense.setType(command.getM2mRebuildCache().getExpenseType());
				accountingExpense.setExpenseCurrency(expenseCurrency);
				accountingExpense.setM2mDaily(m2mDaily);
				accountingExpense.setExpectedExpenseAmount(command.getExpenseAmount());
				accountingExpense.setExpenseAmount(BigDecimal.ZERO);
				accountingExpense.setOurExpenseAmount(BigDecimal.ZERO);
				accountingExpense.setExpenseFxRate(command.getFxRate());
				accountingExpense.setNote("Broker price override on security [" + command.getPriceOverride().getInvestmentSecurity().getSymbol() + "] from [" + CoreMathUtils.formatNumberDecimal(command.getPriceOverride().getExternalPrice()) + "] to [" + CoreMathUtils.formatNumberDecimal(command.getPriceOverride().getOverridePrice()) + "].");
				m2mDaily.getExpenseList().add(accountingExpense);
				command.getM2mRebuildCache().putM2MExpense(accountingExpense);
			}
			else {
				accountingExpense.setExpectedExpenseAmount(MathUtils.add(accountingExpense.getExpectedExpenseAmount(), command.getExpenseAmount()));
			}
			return m2mDaily;
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	private void doRebuildReconcilePosition(ReconcilePositionExternalPriceOverride bean) {
		SystemTable table = getSystemSchemaService().getSystemTableByName(OUR_RECONCILE_TABLE_NAME);

		ReconcilePositionDefinitionSearchForm searchForm = new ReconcilePositionDefinitionSearchForm();
		if (table != null) {
			searchForm.setOurSystemTableId(table.getId());
		}
		searchForm.setHoldingCompanyId(bean.getHoldingCompany().getId());
		List<ReconcilePositionDefinition> defList = getReconcilePositionService().getReconcilePositionDefinitionList(searchForm);

		if ((defList != null) && (!defList.isEmpty())) {
			for (ReconcilePositionDefinition def : defList) {
				doRebuildReconcilePosition(bean, def);
			}
		}
		else {
			doRebuildReconcilePosition(bean, null);
		}
	}


	private void doRebuildReconcilePosition(ReconcilePositionExternalPriceOverride bean, ReconcilePositionDefinition def) {
		ReconcilePositionRebuildCommand command = new ReconcilePositionRebuildCommand();
		if (def != null) {
			command.setReconcilePositionDefinitionId(def.getId());
		}
		command.setPositionDate(bean.getPositionDate());
		command.setInvestmentSecurityId(bean.getInvestmentSecurity().getId());
		getReconcilePositionService().rebuildReconcilePosition(command, true);
	}


	private void rebuildExternalPositionAndCreateExpenses(ReconcilePositionExternalPriceOverride override, ReconcileM2MRebuildCache m2mRebuildCache) {
		rebuildExternalPosition(override, true, m2mRebuildCache);
	}


	private void rebuildExternalPositionWithoutExpenses(ReconcilePositionExternalPriceOverride override, ReconcileM2MRebuildCache m2mRebuildCache) {
		rebuildExternalPosition(override, false, m2mRebuildCache);
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected void rebuildExternalPosition(ReconcilePositionExternalPriceOverride override, boolean createExpenses, ReconcileM2MRebuildCache m2mRebuildCache) {
		ReconcilePositionExternalDailySearchForm searchForm = new ReconcilePositionExternalDailySearchForm();
		searchForm.setHoldingCompanyId(override.getHoldingCompany().getId());
		searchForm.setInvestmentSecurityId(override.getInvestmentSecurity().getId());
		searchForm.setPositionDate(override.getPositionDate());
		List<ReconcilePositionExternalDaily> externalPositionList = getReconcilePositionExternalDailyList(searchForm);
		Set<AccountingM2MDaily> m2MDailyItemsToSave = new HashSet<>();

		for (ReconcilePositionExternalDaily externalPosition : externalPositionList) {
			BigDecimal currentBaseMarketValue = externalPosition.getMarketValueBase();
			externalPosition.setMarketPrice(override.getOverridePrice());
			externalPosition.setMarketValueLocal(getInvestmentCalculator().calculateNotional(override.getInvestmentSecurity(), override.getOverridePrice(), externalPosition.getQuantity(), override.getPositionDate()));
			externalPosition.setMarketValueBase(externalPosition.getMarketValueLocal().multiply(externalPosition.getFxRate()).setScale(2, RoundingMode.HALF_UP));
			externalPosition.setOpenTradeEquityLocal(externalPosition.getMarketValueLocal().subtract(externalPosition.getRemainingCostBasisLocal()));
			externalPosition.setOpenTradeEquityBase(externalPosition.getMarketValueBase().subtract(externalPosition.getRemainingCostBasisBase()));
			externalPosition.setOverridden(true);
			getReconcilePositionExternalDailyDAO().save(externalPosition);
			if (createExpenses) {
				AccountingM2MDaily m2mDaily = createOrUpdateM2MPriceOverrideExpense(ReconcilePositionPriceOverrideM2MExpenseCommand.ofExternalPosition(externalPosition, MathUtils.subtract(currentBaseMarketValue, externalPosition.getMarketValueBase()), override, m2mRebuildCache));
				if (m2mDaily != null) {
					m2MDailyItemsToSave.add(m2mDaily);
				}
			}
		}

		if (createExpenses) {
			for (AccountingM2MDaily m2mDaily : CollectionUtils.getIterable(m2MDailyItemsToSave)) {
				getAccountingM2MService().saveAccountingM2MDailyWithExpenses(m2mDaily, true);
			}
		}
	}


	private void removeM2MAdjustmentExpenses(ReconcilePositionExternalPriceOverride override, ReconcileM2MRebuildCache m2mRebuildCache) {
		AccountingM2MDailyExpenseSearchForm searchForm = new AccountingM2MDailyExpenseSearchForm();
		searchForm.setSourceFkFieldId(override.getId());
		searchForm.setTypeId(m2mRebuildCache.getExpenseType().getId());

		List<AccountingM2MDailyExpense> expenseList = getAccountingM2MService().getAccountingM2MDailyExpenseList(searchForm);
		for (AccountingM2MDailyExpense expense : CollectionUtils.getIterable(expenseList)) {
			// get the populated mark to market
			AccountingM2MDaily m2mDaily = m2mRebuildCache.getM2M(expense.getM2mDaily());
			if (m2mDaily != null) {
				m2mDaily.getExpenseList().remove(expense);
				getAccountingM2MService().saveAccountingM2MDailyWithExpenses(m2mDaily, true);
			}
		}
	}


	private List<ReconcilePositionExternalPriceOverride> getReconcilePositionExternalPriceOverrideListByDate(final Date positionDate) {
		HibernateSearchConfigurer configurer = new HibernateSearchConfigurer() {

			@Override
			public boolean configureOrderBy(Criteria criteria) {
				criteria.addOrder(Order.asc("id"));
				return true;
			}


			@Override
			public void configureCriteria(Criteria criteria) {
				criteria.add(Restrictions.eq("positionDate", positionDate));
			}
		};
		return getReconcilePositionExternalPriceOverrideDAO().findBySearchCriteria(configurer);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconcilePositionExternalDaily, Criteria> getReconcilePositionExternalDailyDAO() {
		return this.reconcilePositionExternalDailyDAO;
	}


	public AdvancedReadOnlyDAO<ReconcilePositionExternalDailyGrouping, Criteria> getReconcilePositionExternalDailyGroupingDAO() {
		return this.reconcilePositionExternalDailyGroupingDAO;
	}


	public void setReconcilePositionExternalDailyGroupingDAO(AdvancedReadOnlyDAO<ReconcilePositionExternalDailyGrouping, Criteria> reconcilePositionExternalDailyGroupingDAO) {
		this.reconcilePositionExternalDailyGroupingDAO = reconcilePositionExternalDailyGroupingDAO;
	}


	public AdvancedUpdatableDAO<ReconcilePositionExternalDailyPNL, Criteria> getReconcilePositionExternalDailyPNLDAO() {
		return this.reconcilePositionExternalDailyPNLDAO;
	}


	public void setReconcilePositionExternalDailyPNLDAO(AdvancedUpdatableDAO<ReconcilePositionExternalDailyPNL, Criteria> reconcilePositionExternalDailyPNLDAO) {
		this.reconcilePositionExternalDailyPNLDAO = reconcilePositionExternalDailyPNLDAO;
	}


	public void setReconcilePositionExternalDailyDAO(AdvancedUpdatableDAO<ReconcilePositionExternalDaily, Criteria> reconcilePositionExternalDailyDAO) {
		this.reconcilePositionExternalDailyDAO = reconcilePositionExternalDailyDAO;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AdvancedUpdatableDAO<ReconcilePositionExternalPriceOverride, Criteria> getReconcilePositionExternalPriceOverrideDAO() {
		return this.reconcilePositionExternalPriceOverrideDAO;
	}


	public void setReconcilePositionExternalPriceOverrideDAO(AdvancedUpdatableDAO<ReconcilePositionExternalPriceOverride, Criteria> reconcilePositionExternalPriceOverrideDAO) {
		this.reconcilePositionExternalPriceOverrideDAO = reconcilePositionExternalPriceOverrideDAO;
	}


	public ReconcilePositionService getReconcilePositionService() {
		return this.reconcilePositionService;
	}


	public void setReconcilePositionService(ReconcilePositionService reconcilePositionService) {
		this.reconcilePositionService = reconcilePositionService;
	}


	public SystemSchemaService getSystemSchemaService() {
		return this.systemSchemaService;
	}


	public void setSystemSchemaService(SystemSchemaService systemSchemaService) {
		this.systemSchemaService = systemSchemaService;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}
}
