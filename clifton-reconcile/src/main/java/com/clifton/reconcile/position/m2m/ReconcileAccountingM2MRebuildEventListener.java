package com.clifton.reconcile.position.m2m;

import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.accounting.m2m.AccountingM2MDailyExpenseType;
import com.clifton.accounting.m2m.AccountingM2MService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.event.BaseEventListener;
import com.clifton.core.util.event.Event;
import com.clifton.investment.instrument.calculator.InvestmentCalculator;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDaily;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyService;
import com.clifton.reconcile.position.external.ReconcilePositionExternalPriceOverride;
import com.clifton.reconcile.position.external.ReconcilePositionPriceOverrideM2MExpenseCommand;
import com.clifton.reconcile.position.external.cache.ReconcileM2MRebuildCache;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalDailySearchForm;
import com.clifton.reconcile.position.external.search.ReconcilePositionExternalPriceOverrideSearchForm;
import com.clifton.core.util.MathUtils;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * Will create any expenses needed because of price overrides for an M2M entry after it has been rebuilt.  This will
 * handle creating the M2M expense if the M2M is rebuilt or created after the price override is created.
 *
 * @author mwacker
 */
@Component
public class ReconcileAccountingM2MRebuildEventListener extends BaseEventListener<Event<AccountingM2MDaily, Object>> {

	private AccountingM2MService accountingM2MService;
	private InvestmentCalculator investmentCalculator;
	private ReconcilePositionExternalDailyService reconcilePositionExternalDailyService;


	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEventName() {
		return AccountingM2MService.M2M_ACCOUNT_REBUILD_COMPLETE_AFTER_SAVE_EVENT_NAME;
	}


	@Override
	public void onEvent(Event<AccountingM2MDaily, Object> event) {
		AccountingM2MDaily m2m = event.getTarget();
		ReconcileM2MRebuildCache m2mRebuildCache = new ReconcileM2MRebuildCache(getAccountingM2MService(), getAccountingM2MService().getAccountingM2MDailyExpenseTypeByName(AccountingM2MDailyExpenseType.M2M_PRICE_ADJUSTMENT));


		AccountingM2MDaily m2mToSave = null;
		List<ReconcilePositionExternalPriceOverride> overrides = getPriceOverrides(m2m.getMarkDate());
		for (ReconcilePositionExternalPriceOverride override : CollectionUtils.getIterable(overrides)) {
			AccountingM2MDailyExpense accountingExpense = m2mRebuildCache.getM2MExpense(m2m, override);
			// if the expense doesn't exist, create it
			if (accountingExpense == null) {
				List<ReconcilePositionExternalDaily> externalPositions = getExternalPositionList(override, m2m);
				for (ReconcilePositionExternalDaily externalPosition : CollectionUtils.getIterable(externalPositions)) {
					BigDecimal m2mExpenseAmount = getMarketValueDifference(externalPosition, override);
					ReconcilePositionPriceOverrideM2MExpenseCommand command = ReconcilePositionPriceOverrideM2MExpenseCommand.ofExternalPositionWithExisting(externalPosition, m2mExpenseAmount, override, m2mRebuildCache, accountingExpense);
					m2mToSave = getReconcilePositionExternalDailyService().createOrUpdateM2MPriceOverrideExpense(command);
				}
			}
		}

		// save the populated m2m object
		if (m2mToSave != null && !CollectionUtils.isEmpty(m2mToSave.getExpenseList())) {
			getAccountingM2MService().saveAccountingM2MDailyWithExpenses(m2mToSave, true);
		}
	}


	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////
	private BigDecimal getMarketValueDifference(ReconcilePositionExternalDaily externalPosition, ReconcilePositionExternalPriceOverride override) {
		// calculate the market value using the original price from the broker.
		BigDecimal newMarketValue = getInvestmentCalculator().calculateNotional(override.getInvestmentSecurity(), override.getExternalPrice(), externalPosition.getQuantity(), override.getPositionDate());
		return MathUtils.subtract(newMarketValue, externalPosition.getMarketValueBase());
	}


	private List<ReconcilePositionExternalDaily> getExternalPositionList(ReconcilePositionExternalPriceOverride override, AccountingM2MDaily m2m) {
		ReconcilePositionExternalDailySearchForm searchForm = new ReconcilePositionExternalDailySearchForm();
		searchForm.setHoldingAccountId(m2m.getHoldingInvestmentAccount().getId());
		searchForm.setHoldingCompanyId(override.getHoldingCompany().getId());
		searchForm.setInvestmentSecurityId(override.getInvestmentSecurity().getId());
		searchForm.setPositionDate(override.getPositionDate());
		return getReconcilePositionExternalDailyService().getReconcilePositionExternalDailyList(searchForm);
	}


	private List<ReconcilePositionExternalPriceOverride> getPriceOverrides(Date markDate) {
		ReconcilePositionExternalPriceOverrideSearchForm searchForm = new ReconcilePositionExternalPriceOverrideSearchForm();
		searchForm.setPositionDate(markDate);
		return getReconcilePositionExternalDailyService().getReconcilePositionExternalPriceOverrideList(searchForm);
	}

	////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////////


	public ReconcilePositionExternalDailyService getReconcilePositionExternalDailyService() {
		return this.reconcilePositionExternalDailyService;
	}


	public void setReconcilePositionExternalDailyService(ReconcilePositionExternalDailyService reconcilePositionExternalDailyService) {
		this.reconcilePositionExternalDailyService = reconcilePositionExternalDailyService;
	}


	public InvestmentCalculator getInvestmentCalculator() {
		return this.investmentCalculator;
	}


	public void setInvestmentCalculator(InvestmentCalculator investmentCalculator) {
		this.investmentCalculator = investmentCalculator;
	}


	public AccountingM2MService getAccountingM2MService() {
		return this.accountingM2MService;
	}


	public void setAccountingM2MService(AccountingM2MService accountingM2MService) {
		this.accountingM2MService = accountingM2MService;
	}
}
