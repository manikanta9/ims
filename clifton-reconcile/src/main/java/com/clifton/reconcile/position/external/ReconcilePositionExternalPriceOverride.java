package com.clifton.reconcile.position.external;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseEntity;
import com.clifton.investment.instrument.InvestmentSecurity;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionExternalPriceOverride extends BaseEntity<Integer> {

	private BusinessCompany holdingCompany;
	private InvestmentSecurity investmentSecurity;
	private Date positionDate;
	private BigDecimal externalPrice;
	private BigDecimal overridePrice;
	private String note;


	public BusinessCompany getHoldingCompany() {
		return this.holdingCompany;
	}


	public void setHoldingCompany(BusinessCompany holdingCompany) {
		this.holdingCompany = holdingCompany;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getExternalPrice() {
		return this.externalPrice;
	}


	public void setExternalPrice(BigDecimal externalPrice) {
		this.externalPrice = externalPrice;
	}


	public BigDecimal getOverridePrice() {
		return this.overridePrice;
	}


	public void setOverridePrice(BigDecimal overridePrice) {
		this.overridePrice = overridePrice;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}
}
