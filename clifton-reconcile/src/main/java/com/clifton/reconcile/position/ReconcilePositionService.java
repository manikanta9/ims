package com.clifton.reconcile.position;


import com.clifton.accounting.m2m.AccountingM2MDaily;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.reconcile.position.comparison.ReconcilePositionComparison;
import com.clifton.reconcile.position.processor.ReconcilePositionRebuildCommand;
import com.clifton.reconcile.position.search.ReconcilePositionAccountExtendedSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionAccountSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionDefinitionSearchForm;
import com.clifton.reconcile.position.search.ReconcilePositionSearchForm;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public interface ReconcilePositionService {

	////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionDefinition Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcilePositionDefinition getReconcilePositionDefinition(int id);


	public List<ReconcilePositionDefinition> getReconcilePositionDefinitionList();


	public List<ReconcilePositionDefinition> getReconcilePositionDefinitionList(ReconcilePositionDefinitionSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePositionAccountRun Business Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcilePositionAccount getReconcilePositionAccount(int id);


	public List<ReconcilePositionAccount> getReconcilePositionAccountList(ReconcilePositionAccountSearchForm searchForm);


	public ReconcilePositionAccount saveReconcilePositionAccount(ReconcilePositionAccount bean);


	////////////////////////////////////////////////////////////////////////////
	///////    ReconcilePositionAccountExtended Business Methods     ///////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ReconcilePositionAccount.class)
	public List<ReconcilePositionAccountExtended> getReconcilePositionAccountExtendedList(ReconcilePositionAccountExtendedSearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	////////    ReconcilePosition Processing Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////
	public ReconcilePosition getReconcilePosition(int id);


	@RequestMapping("reconcilePositionUnreconcile")
	public void unreconcileReconcilePosition(Boolean clearNote, Boolean unreconcileActivity, Boolean unreconcilePosition, Integer[] ids);


	@RequestMapping("reconcilePositionForce")
	public void forceReconcilePosition(String note, String viewName, Integer[] ids);


	public List<ReconcilePosition> getReconcilePositionListByAccount(int reconcilePositionAccountId);


	public List<ReconcilePosition> getReconcilePositionList(ReconcilePositionSearchForm searchForm);


	@RequestMapping("reconcilePositionOurList")
	@SecureMethod(dtoClass = ReconcilePosition.class)
	public List<? extends BaseSimpleEntity<?>> getOurReconcilePositionList(int reconcilePositionAccountId);


	@RequestMapping("reconcilePositionExternalList")
	@SecureMethod(dtoClass = ReconcilePosition.class)
	public List<? extends BaseSimpleEntity<?>> getExternalReconcilePositionList(int reconcilePositionAccountId);


	@SecureMethod(dtoClass = ReconcilePosition.class)
	@RequestMapping("reconcilePositionExternalPNLList")
	public List<? extends BaseSimpleEntity<?>> getExternalReconcilePNLList(int reconcilePositionAccountId);


	@SecureMethod(dtoClass = ReconcilePosition.class)
	public ReconcilePositionComparison getReconcilePositionComparison(int id);


	@ModelAttribute("result")
	public String rebuildReconcilePosition(ReconcilePositionRebuildCommand command, boolean synchronous);


	/**
	 * Rebuild for previous weekday.
	 */
	@DoNotAddRequestMapping
	public void rebuildReconcilePositionForToday();


	/**
	 * Copy the reconciliation notes to the M2M.
	 *
	 * @param positionDate
	 */
	@SecureMethod(dtoClass = AccountingM2MDaily.class)
	public void copyReconcilePositionNotesToM2M(Date positionDate);


	public ReconcilePosition saveReconcilePosition(ReconcilePosition bean);


	@RequestMapping("reconcileRealizedGainLossAdjust")
	@SecureMethod(dtoClass = ReconcilePosition.class)
	public void adjustReconcileRealizedGainLoss(int reconcilePositionId, BigDecimal threshold, String note);


	@RequestMapping("reconcileCommissionAdjust")
	@SecureMethod(dtoClass = ReconcilePosition.class)
	public void adjustReconcileCommission(int reconcilePositionId, BigDecimal threshold, String note);
}
