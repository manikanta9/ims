package com.clifton.reconcile.position.processor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.processor.ReconcileRule;

import java.math.BigDecimal;
import java.util.Map;


public class ReconcilePositionRealizedAndCommissionRule implements ReconcileRule<ReconcilePosition> {

	@Override
	public void reconcile(ReconcilePosition bean, Map<String, Object> context) {

		// skip reconciliation if a user note has been entered
		if (!StringUtils.isEmpty(bean.getRealizedAndCommissionNote()) && !bean.getRealizedAndCommissionNote().contains(ReconcilePositionProcessor.AUTO_REC_INDICATOR)) {
			return;
		}

		// normalize thresholds
		BigDecimal realizedThreshold = BigDecimal.ZERO;
		BigDecimal commissionThreshold = BigDecimal.ZERO;
		if (context != null) {
			if (context.get(ReconcilePositionProcessor.REALIZED_MAX_THRESHOLD) != null) {
				realizedThreshold = (BigDecimal) context.get(ReconcilePositionProcessor.REALIZED_MAX_THRESHOLD);
			}
			if (context.get(ReconcilePositionProcessor.COMMISSION_MAX_THRESHOLD) != null) {
				commissionThreshold = (BigDecimal) context.get(ReconcilePositionProcessor.COMMISSION_MAX_THRESHOLD);
			}
		}

		// reconcile fields
		if (!compareProperty(bean, "TodayClosedQuantity", BigDecimal.ZERO)) {
			bean.setRealizedAndCommissionReconciled(false);
		}
		else if (!compareProperty(bean, "TodayCommissionLocal", commissionThreshold)) {
			bean.setRealizedAndCommissionReconciled(false);
		}
		else if (!compareProperty(bean, "TodayRealizedGainLossLocal", realizedThreshold)) {
			bean.setRealizedAndCommissionReconciled(false);
		}
		else {
			bean.setRealizedAndCommissionReconciled(true);
		}

		// set note if threshold was used (reconciled, but unequal values)
		if (bean.isRealizedAndCommissionReconciled() && (!bean.isRealizedGainLossEqual() || !bean.isCommissionEqual())) {
			bean.setRealizedAndCommissionNote(ReconcilePositionProcessor.AUTO_REC_INDICATOR + ": " + realizedThreshold + " and " + commissionThreshold);
		}
	}


	/**
	 * Compares two properties within a ReconcilePosition instance to determine if the values are
	 * equal or within a threshold (for properties of type BigDecimal).
	 *
	 * @return true if values are equal or their absolute difference is withing the provided threshold. Otherwise returns false.
	 */
	public boolean compareProperty(ReconcilePosition position, String name, BigDecimal threshold) {
		Object ours = BeanUtils.getPropertyValue(position, "our" + name);
		Object external = BeanUtils.getPropertyValue(position, "external" + name);
		ours = ours == null ? BigDecimal.ZERO : ours;
		external = external == null ? BigDecimal.ZERO : external;
		if ((ours instanceof BigDecimal) && (external instanceof BigDecimal)) {
			BigDecimal o = (BigDecimal) ours;
			BigDecimal e = (BigDecimal) external;
			return o.subtract(e).abs().compareTo(threshold) <= 0;
		}
		return ours.equals(external);
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getPropertyName() {
		return "realizedAndCommissionReconciled";
	}
}
