package com.clifton.reconcile.position.external.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


public class ReconcilePositionExternalDailySearchForm extends BaseEntitySearchForm {

	@SearchField
	private Date positionDate;

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "holdingAccount.number", comparisonConditions = ComparisonConditions.IN)
	private String[] holdingAccountNumberList;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingAccount")
	private Integer holdingCompanyId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "holdingAccount", comparisonConditions = ComparisonConditions.IN)
	private Integer[] holdingCompanyIdList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcilePositionExternalDailySearchForm() {
		// Empty no-arg constructor
	}


	public ReconcilePositionExternalDailySearchForm(Date positionDate, int holdingAccountId, Integer investmentSecurityId) {
		this.positionDate = positionDate;
		this.holdingAccountId = holdingAccountId;
		this.investmentSecurityId = investmentSecurityId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public String[] getHoldingAccountNumberList() {
		return this.holdingAccountNumberList;
	}


	public void setHoldingAccountNumberList(String[] holdingAccountNumberList) {
		this.holdingAccountNumberList = holdingAccountNumberList;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Integer[] getHoldingCompanyIdList() {
		return this.holdingCompanyIdList;
	}


	public void setHoldingCompanyIdList(Integer[] holdingCompanyIdList) {
		this.holdingCompanyIdList = holdingCompanyIdList;
	}
}
