package com.clifton.reconcile.position.external;

import com.clifton.accounting.m2m.AccountingM2MDailyExpense;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.reconcile.position.external.cache.ReconcileM2MRebuildCache;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mwacker
 */
public class ReconcilePositionPriceOverrideM2MExpenseCommand {

	private final AccountingM2MDailyExpense existingAccountingExpense;
	private final InvestmentAccount holdingInvestmentAccount;
	private final Date positionDate;
	private final BigDecimal fxRate;
	private final BigDecimal expenseAmount;
	private final ReconcilePositionExternalPriceOverride priceOverride;
	private final ReconcileM2MRebuildCache m2mRebuildCache;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ReconcilePositionPriceOverrideM2MExpenseCommand ofExternalPosition(ReconcilePositionExternalDaily externalPosition, BigDecimal expenseAmount, ReconcilePositionExternalPriceOverride priceOverride, ReconcileM2MRebuildCache m2mRebuildCache) {
		return new ReconcilePositionPriceOverrideM2MExpenseCommand(externalPosition.getHoldingAccount(), externalPosition.getPositionDate(), externalPosition.getFxRate(), expenseAmount, priceOverride, m2mRebuildCache, null);
	}


	public static ReconcilePositionPriceOverrideM2MExpenseCommand ofExternalPositionWithExisting(ReconcilePositionExternalDaily externalPosition, BigDecimal expenseAmount, ReconcilePositionExternalPriceOverride priceOverride, ReconcileM2MRebuildCache m2mRebuildCache, AccountingM2MDailyExpense existingAccountingExpense) {
		return new ReconcilePositionPriceOverrideM2MExpenseCommand(externalPosition.getHoldingAccount(), externalPosition.getPositionDate(), externalPosition.getFxRate(), expenseAmount, priceOverride, m2mRebuildCache, existingAccountingExpense);
	}


	private ReconcilePositionPriceOverrideM2MExpenseCommand(InvestmentAccount holdingInvestmentAccount, Date positionDate, BigDecimal fxRate, BigDecimal expenseAmount, ReconcilePositionExternalPriceOverride priceOverride, ReconcileM2MRebuildCache m2mRebuildCache, AccountingM2MDailyExpense existingAccountingExpense) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
		this.positionDate = positionDate;
		this.fxRate = fxRate;
		this.expenseAmount = expenseAmount;
		this.priceOverride = priceOverride;
		this.m2mRebuildCache = m2mRebuildCache;
		this.existingAccountingExpense = existingAccountingExpense;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public BigDecimal getExpenseAmount() {
		return this.expenseAmount;
	}


	public ReconcilePositionExternalPriceOverride getPriceOverride() {
		return this.priceOverride;
	}


	public ReconcileM2MRebuildCache getM2mRebuildCache() {
		return this.m2mRebuildCache;
	}


	public AccountingM2MDailyExpense getExistingAccountingExpense() {
		return this.existingAccountingExpense;
	}
}
