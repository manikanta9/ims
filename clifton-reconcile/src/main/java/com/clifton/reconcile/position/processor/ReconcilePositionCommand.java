package com.clifton.reconcile.position.processor;


import com.clifton.reconcile.position.ReconcilePositionDefinition;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ReconcilePositionCommand</code> encapsulates parameters for a position reconciliation
 *
 * @author rbrooks
 */
public class ReconcilePositionCommand {

	private ReconcilePositionDefinition reconcilePositionDefinition;

	private Date positionDate;

	private Integer investmentSecurityId;

	private Integer holdingInvestmentAccountId;

	private BigDecimal oteMaxThreshold;

	private BigDecimal marketValueMaxThreshold;

	private BigDecimal realizedGainLossThreshold;

	private BigDecimal commissionThreshold;

	/**
	 * Indicates the reconciliation will be done at the account level, meaning that it is only rebuilding the account level summary.
	 * Thus no rules will be run against the ReconcilePosition objects.
	 */
	private boolean accountLevelOnly;


	public ReconcilePositionCommand() {
		// default
	}


	public ReconcilePositionCommand(ReconcilePositionDefinition reconcilePositionDefinition, Date positionDate, Integer investmentSecurityId, Integer holdingInvestmentAccountId,
	                                BigDecimal oteMaxThreshold, BigDecimal marketValueMaxThreshold, BigDecimal realizedGainLossThreshold, BigDecimal commissionThreshold, boolean accountLevelOnly) {
		this.reconcilePositionDefinition = reconcilePositionDefinition;
		this.positionDate = positionDate;
		this.investmentSecurityId = investmentSecurityId;
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
		this.oteMaxThreshold = oteMaxThreshold;
		this.marketValueMaxThreshold = marketValueMaxThreshold;
		this.realizedGainLossThreshold = realizedGainLossThreshold;
		this.commissionThreshold = commissionThreshold;
		this.accountLevelOnly = accountLevelOnly;
	}


	public ReconcilePositionCommand(ReconcilePositionDefinition definition, Date positionDate, boolean accountLevelOnly) {
		this.reconcilePositionDefinition = definition;
		this.positionDate = positionDate;
		this.accountLevelOnly = accountLevelOnly;
	}


	public ReconcilePositionDefinition getReconcilePositionDefinition() {
		return this.reconcilePositionDefinition;
	}


	public void setReconcilePositionDefinition(ReconcilePositionDefinition reconcilePositionDefinition) {
		this.reconcilePositionDefinition = reconcilePositionDefinition;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public BigDecimal getOteMaxThreshold() {
		return this.oteMaxThreshold;
	}


	public void setOteMaxThreshold(BigDecimal oteMaxThreshold) {
		this.oteMaxThreshold = oteMaxThreshold;
	}


	public BigDecimal getMarketValueMaxThreshold() {
		return this.marketValueMaxThreshold;
	}


	public void setMarketValueMaxThreshold(BigDecimal marketValueMaxThreshold) {
		this.marketValueMaxThreshold = marketValueMaxThreshold;
	}


	public boolean isAccountLevelOnly() {
		return this.accountLevelOnly;
	}


	public void setAccountLevelOnly(boolean accountLevelOnly) {
		this.accountLevelOnly = accountLevelOnly;
	}


	public BigDecimal getRealizedGainLossThreshold() {
		return this.realizedGainLossThreshold;
	}


	public void setRealizedGainLossThreshold(BigDecimal realizedGainLossThreshold) {
		this.realizedGainLossThreshold = realizedGainLossThreshold;
	}


	public BigDecimal getCommissionThreshold() {
		return this.commissionThreshold;
	}


	public void setCommissionThreshold(BigDecimal commissionThreshold) {
		this.commissionThreshold = commissionThreshold;
	}
}
