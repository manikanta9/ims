package com.clifton.reconcile.position.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ReconcilePositionDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField(searchField = "ourSystemTable.id")
	private Short ourSystemTableId;

	@SearchField(searchField = "holdingCompany.id")
	private Integer holdingCompanyId;

	/**
	 * Find definitions by: ClientAccount -> HoldingAccounts -> Distinct IssuingCompany -> ReconcilePositionDefinitions
	 */
	private Integer clientInvestmentAccountGroupId;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public Integer getClientInvestmentAccountGroupId() {
		return this.clientInvestmentAccountGroupId;
	}


	public void setClientInvestmentAccountGroupId(Integer clientInvestmentAccountGroupId) {
		this.clientInvestmentAccountGroupId = clientInvestmentAccountGroupId;
	}


	public Short getOurSystemTableId() {
		return this.ourSystemTableId;
	}


	public void setOurSystemTableId(Short ourSystemTableId) {
		this.ourSystemTableId = ourSystemTableId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
