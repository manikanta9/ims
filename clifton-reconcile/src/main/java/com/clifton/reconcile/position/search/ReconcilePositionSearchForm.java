package com.clifton.reconcile.position.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] ids;

	@SearchField(searchField = "holdingAccount.id", searchFieldPath = "positionAccount", comparisonConditions = ComparisonConditions.IN)
	private Integer[] holdingAccountIds;

	@SearchField(searchField = "holdingAccount.id", searchFieldPath = "positionAccount", sortField = "holdingAccount.number")
	private Integer holdingAccountId;

	@SearchField(searchField = "name,number", searchFieldPath = "positionAccount.holdingAccount", sortField = "number")
	private String holdingAccount;

	@SearchField(searchField = "name", searchFieldPath = "positionAccount.holdingAccount.issuingCompany")
	private String holdingAccountIssuerName;

	@SearchField(searchField = "clientAccount.id", searchFieldPath = "positionAccount", sortField = "clientAccount.number")
	private Integer clientAccountId;

	@SearchField(searchField = "name,number", searchFieldPath = "positionAccount.clientAccount", sortField = "number")
	private String clientAccount;

	@SearchField(searchField = "positionDate", searchFieldPath = "positionAccount")
	private Date positionDate;

	@SearchField(searchField = "definition.id", searchFieldPath = "positionAccount")
	private Integer reconcilePositionDefinitionId;

	@SearchField(searchField = "positionAccount.id")
	private Integer reconcilePositionAccountId;

	@SearchField(searchField = "investmentSecurity.id", sortField = "investmentSecurity.symbol")
	private Integer investmentSecurityId;

	@SearchField(searchField = "investmentType.id", searchFieldPath = "investmentSecurity.instrument.hierarchy")
	private Short investmentSecurityTypeId;


	@SearchField(searchField = "ourPrice,externalPrice", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal priceDifference;

	@SearchField(searchField = "ourFxRate,externalFxRate", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal fxRateDifference;

	@SearchField(searchField = "ourQuantity,externalQuantity", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal quantityDifference;

	@SearchField(searchField = "ourTodayClosedQuantity,externalTodayClosedQuantity", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal closedQuantityDifference;

	@SearchField(searchField = "ourMarketValueLocal,externalMarketValueLocal", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal marketValueLocalDifference;

	@SearchField(searchField = "ourMarketValueBase,externalMarketValueBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal marketValueBaseDifference;

	@SearchField(searchField = "ourOpenTradeEquityLocal,externalOpenTradeEquityLocal", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal oteLocalDifference;

	@SearchField(searchField = "ourOpenTradeEquityBase,externalOpenTradeEquityBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal oteBaseDifference;

	@SearchField(searchField = "ourTodayCommissionLocal,externalTodayCommissionLocal", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal commissionLocalDifference;

	@SearchField(searchField = "ourTodayRealizedGainLossLocal,externalTodayRealizedGainLossLocal", searchFieldCustomType = SearchFieldCustomTypes.MATH_SUBTRACT)
	private BigDecimal realizedGainLossLocalDifference;


	@SearchField
	private Boolean reconciled;

	@SearchField
	private Boolean matched;

	@SearchField
	private Boolean quantityReconciled;

	// Custom Search Field
	private Boolean excludeClosedPositions;

	// Custom Search Field: set to true to limit results to when our or external realized or commission field is populated
	private Boolean realizedOrCommissionPresent;

	// Custom Search Filed: set to true to limit results when reconciliation breaks are found: mismatches on quantity, price, ote, commission, etc.
	private Boolean reconciliationBreaksPresent;

	// Custom Search Filed: set to true to limit results when reconciliation breaks are found: mismatches on realized or commission.
	private Boolean activityBreaksPresent;

	// Custom Search Filed: set to true to limit results when reconciliation breaks are found: mismatches on quantity, price, ote, etc.
	private Boolean positionBreaksPresent;

	@SearchField(searchField = "note", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean noteNotNull;

	@SearchField(searchField = "note", comparisonConditions = ComparisonConditions.NOT_BEGINS_WITH)
	private String noteNotStartWith;

	@SearchField(searchField = "realizedAndCommissionNote", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean realizedAndCommissionNoteNotNull;

	@SearchField(searchField = "externalPrice", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean externalPriceNotNull;

	// Custom Search Field
	private Boolean anyNoteNotNull;

	/////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public Integer[] getHoldingAccountIds() {
		return this.holdingAccountIds;
	}


	public void setHoldingAccountIds(Integer[] holdingAccountIds) {
		this.holdingAccountIds = holdingAccountIds;
	}


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public String getHoldingAccount() {
		return this.holdingAccount;
	}


	public void setHoldingAccount(String holdingAccount) {
		this.holdingAccount = holdingAccount;
	}


	public String getHoldingAccountIssuerName() {
		return this.holdingAccountIssuerName;
	}


	public void setHoldingAccountIssuerName(String holdingAccountIssuerName) {
		this.holdingAccountIssuerName = holdingAccountIssuerName;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public String getClientAccount() {
		return this.clientAccount;
	}


	public void setClientAccount(String clientAccount) {
		this.clientAccount = clientAccount;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public Integer getReconcilePositionDefinitionId() {
		return this.reconcilePositionDefinitionId;
	}


	public void setReconcilePositionDefinitionId(Integer reconcilePositionDefinitionId) {
		this.reconcilePositionDefinitionId = reconcilePositionDefinitionId;
	}


	public Integer getReconcilePositionAccountId() {
		return this.reconcilePositionAccountId;
	}


	public void setReconcilePositionAccountId(Integer reconcilePositionAccountId) {
		this.reconcilePositionAccountId = reconcilePositionAccountId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getInvestmentSecurityTypeId() {
		return this.investmentSecurityTypeId;
	}


	public void setInvestmentSecurityTypeId(Short investmentSecurityTypeId) {
		this.investmentSecurityTypeId = investmentSecurityTypeId;
	}


	public BigDecimal getPriceDifference() {
		return this.priceDifference;
	}


	public void setPriceDifference(BigDecimal priceDifference) {
		this.priceDifference = priceDifference;
	}


	public BigDecimal getFxRateDifference() {
		return this.fxRateDifference;
	}


	public void setFxRateDifference(BigDecimal fxRateDifference) {
		this.fxRateDifference = fxRateDifference;
	}


	public BigDecimal getQuantityDifference() {
		return this.quantityDifference;
	}


	public void setQuantityDifference(BigDecimal quantityDifference) {
		this.quantityDifference = quantityDifference;
	}


	public BigDecimal getClosedQuantityDifference() {
		return this.closedQuantityDifference;
	}


	public void setClosedQuantityDifference(BigDecimal closedQuantityDifference) {
		this.closedQuantityDifference = closedQuantityDifference;
	}


	public BigDecimal getMarketValueLocalDifference() {
		return this.marketValueLocalDifference;
	}


	public void setMarketValueLocalDifference(BigDecimal marketValueLocalDifference) {
		this.marketValueLocalDifference = marketValueLocalDifference;
	}


	public BigDecimal getMarketValueBaseDifference() {
		return this.marketValueBaseDifference;
	}


	public void setMarketValueBaseDifference(BigDecimal marketValueBaseDifference) {
		this.marketValueBaseDifference = marketValueBaseDifference;
	}


	public BigDecimal getOteLocalDifference() {
		return this.oteLocalDifference;
	}


	public void setOteLocalDifference(BigDecimal oteLocalDifference) {
		this.oteLocalDifference = oteLocalDifference;
	}


	public BigDecimal getOteBaseDifference() {
		return this.oteBaseDifference;
	}


	public void setOteBaseDifference(BigDecimal oteBaseDifference) {
		this.oteBaseDifference = oteBaseDifference;
	}


	public BigDecimal getCommissionLocalDifference() {
		return this.commissionLocalDifference;
	}


	public void setCommissionLocalDifference(BigDecimal commissionLocalDifference) {
		this.commissionLocalDifference = commissionLocalDifference;
	}


	public BigDecimal getRealizedGainLossLocalDifference() {
		return this.realizedGainLossLocalDifference;
	}


	public void setRealizedGainLossLocalDifference(BigDecimal realizedGainLossLocalDifference) {
		this.realizedGainLossLocalDifference = realizedGainLossLocalDifference;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public Boolean getMatched() {
		return this.matched;
	}


	public void setMatched(Boolean matched) {
		this.matched = matched;
	}


	public Boolean getQuantityReconciled() {
		return this.quantityReconciled;
	}


	public void setQuantityReconciled(Boolean quantityReconciled) {
		this.quantityReconciled = quantityReconciled;
	}


	public Boolean getExcludeClosedPositions() {
		return this.excludeClosedPositions;
	}


	public void setExcludeClosedPositions(Boolean excludeClosedPositions) {
		this.excludeClosedPositions = excludeClosedPositions;
	}


	public Boolean getRealizedOrCommissionPresent() {
		return this.realizedOrCommissionPresent;
	}


	public void setRealizedOrCommissionPresent(Boolean realizedOrCommissionPresent) {
		this.realizedOrCommissionPresent = realizedOrCommissionPresent;
	}


	public Boolean getReconciliationBreaksPresent() {
		return this.reconciliationBreaksPresent;
	}


	public void setReconciliationBreaksPresent(Boolean reconciliationBreaksPresent) {
		this.reconciliationBreaksPresent = reconciliationBreaksPresent;
	}


	public Boolean getActivityBreaksPresent() {
		return this.activityBreaksPresent;
	}


	public void setActivityBreaksPresent(Boolean activityBreaksPresent) {
		this.activityBreaksPresent = activityBreaksPresent;
	}


	public Boolean getPositionBreaksPresent() {
		return this.positionBreaksPresent;
	}


	public void setPositionBreaksPresent(Boolean positionBreaksPresent) {
		this.positionBreaksPresent = positionBreaksPresent;
	}


	public Boolean getNoteNotNull() {
		return this.noteNotNull;
	}


	public void setNoteNotNull(Boolean noteNotNull) {
		this.noteNotNull = noteNotNull;
	}


	public String getNoteNotStartWith() {
		return this.noteNotStartWith;
	}


	public void setNoteNotStartWith(String noteNotStartWith) {
		this.noteNotStartWith = noteNotStartWith;
	}


	public Boolean getRealizedAndCommissionNoteNotNull() {
		return this.realizedAndCommissionNoteNotNull;
	}


	public void setRealizedAndCommissionNoteNotNull(Boolean realizedAndCommissionNoteNotNull) {
		this.realizedAndCommissionNoteNotNull = realizedAndCommissionNoteNotNull;
	}


	public Boolean getExternalPriceNotNull() {
		return this.externalPriceNotNull;
	}


	public void setExternalPriceNotNull(Boolean externalPriceNotNull) {
		this.externalPriceNotNull = externalPriceNotNull;
	}


	public Boolean getAnyNoteNotNull() {
		return this.anyNoteNotNull;
	}


	public void setAnyNoteNotNull(Boolean anyNoteNotNull) {
		this.anyNoteNotNull = anyNoteNotNull;
	}
}
