package com.clifton.reconcile.position.processor;


import com.clifton.accounting.gl.position.daily.AccountingPositionDaily;
import com.clifton.reconcile.position.ReconcilePosition;
import com.clifton.reconcile.position.ReconcilePositionAccount;
import com.clifton.reconcile.position.ReconcilePositionDefinition;
import com.clifton.reconcile.position.comparison.ReconcilePositionComparison;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDaily;
import com.clifton.reconcile.position.external.ReconcilePositionExternalDailyPNL;
import com.clifton.reconcile.processor.ReconcileRule;

import java.util.Date;
import java.util.List;


public interface ReconcilePositionProcessor {

	/**
	 * Indicates an open position was auto-reconciled
	 */
	public static final String AUTO_REC_INDICATOR = "Auto-Rec";

	/**
	 * A key for passing the OTE threshold to a ReconcileRule
	 */
	public static final String OTE_MAX_THRESHOLD = "oteMaxThreshold";

	/**
	 * A key for passing the Market Value threshold to a ReconcileRule
	 */
	public static final String MARKET_VALUE_MAX_THRESHOLD = "marketValueMaxThreshold";

	/**
	 * A key for passing the Realized Gain Loss threshold to a ReconcileRule
	 */
	public static final String REALIZED_MAX_THRESHOLD = "realizedMaxThreshold";

	/**
	 * A key for passing the Commission threshold to a ReconcileRule
	 */
	public static final String COMMISSION_MAX_THRESHOLD = "commissionMaxThreshold";


	/**
	 * Loads all position data into the ReconcilePosition table.
	 */
	public void process(ReconcilePositionDefinition definition, Date positionDate, Integer investmentSecurityId, Integer holdingInvestmentAccountId, Boolean cleanRebuild);


	/**
	 * Runs the auto reconciliation rules.
	 */
	public void reconcile(ReconcilePositionCommand command);


	/**
	 * Returns a list of our calculated open position values
	 */
	public List<AccountingPositionDaily> getOurReconcilePositionList(ReconcilePositionAccount reconcilePositionAccount);


	/**
	 * Returns a list of external open position values
	 */
	public List<ReconcilePositionExternalDaily> getExternalReconcilePositionList(ReconcilePositionAccount reconcilePositionAccount);


	/**
	 * Returns a list of external closed position values
	 */
	public List<ReconcilePositionExternalDailyPNL> getExternalReconcilePNLList(ReconcilePositionAccount account);


	/**
	 * Returns a comparison view of our positions vs. external positions
	 */
	public ReconcilePositionComparison getReconcilePositionComparison(ReconcilePosition reconcilePosition);


	public void setReconcileRuleList(List<ReconcileRule<ReconcilePosition>> reconcileRuleList);
}
