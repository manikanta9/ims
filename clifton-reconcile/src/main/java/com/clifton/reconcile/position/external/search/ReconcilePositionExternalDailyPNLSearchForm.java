package com.clifton.reconcile.position.external.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcilePositionExternalDailyPNLSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "holdingAccount.id")
	private Integer holdingAccountId;

	@SearchField(searchField = "clientAccount.id")
	private Integer clientAccountId;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;

	@SearchField(searchField = "groupItemList.referenceOne.group.id", searchFieldPath = "investmentSecurity.instrument", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentGroupId;

	@SearchField(searchField = "investmentCurrency.id")
	private Integer investmentCurrencyId;

	@SearchField(searchField = "baseCurrency.id")
	private Integer baseCurrencyId;

	@SearchField
	private Date positionDate;

	@SearchField
	private BigDecimal fxRate;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal priorQuantity;

	@SearchField
	private BigDecimal todayClosedQuantity;

	@SearchField
	private Boolean shortPosition;

	@SearchField
	private BigDecimal openTradeEquityLocal;

	@SearchField
	private BigDecimal openTradeEquityBase;

	@SearchField
	private BigDecimal priorOpenTradeEquityLocal;

	@SearchField
	private BigDecimal priorOpenTradeEquityBase;

	@SearchField
	private BigDecimal todayCommissionLocal;

	@SearchField
	private BigDecimal todayRealizedGainLossLocal;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getHoldingAccountId() {
		return this.holdingAccountId;
	}


	public void setHoldingAccountId(Integer holdingAccountId) {
		this.holdingAccountId = holdingAccountId;
	}


	public Integer getClientAccountId() {
		return this.clientAccountId;
	}


	public void setClientAccountId(Integer clientAccountId) {
		this.clientAccountId = clientAccountId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Short getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Short investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public Integer getInvestmentCurrencyId() {
		return this.investmentCurrencyId;
	}


	public void setInvestmentCurrencyId(Integer investmentCurrencyId) {
		this.investmentCurrencyId = investmentCurrencyId;
	}


	public Integer getBaseCurrencyId() {
		return this.baseCurrencyId;
	}


	public void setBaseCurrencyId(Integer baseCurrencyId) {
		this.baseCurrencyId = baseCurrencyId;
	}


	public Date getPositionDate() {
		return this.positionDate;
	}


	public void setPositionDate(Date positionDate) {
		this.positionDate = positionDate;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPriorQuantity() {
		return this.priorQuantity;
	}


	public void setPriorQuantity(BigDecimal priorQuantity) {
		this.priorQuantity = priorQuantity;
	}


	public BigDecimal getTodayClosedQuantity() {
		return this.todayClosedQuantity;
	}


	public void setTodayClosedQuantity(BigDecimal todayClosedQuantity) {
		this.todayClosedQuantity = todayClosedQuantity;
	}


	public Boolean getShortPosition() {
		return this.shortPosition;
	}


	public void setShortPosition(Boolean shortPosition) {
		this.shortPosition = shortPosition;
	}


	public BigDecimal getOpenTradeEquityLocal() {
		return this.openTradeEquityLocal;
	}


	public void setOpenTradeEquityLocal(BigDecimal openTradeEquityLocal) {
		this.openTradeEquityLocal = openTradeEquityLocal;
	}


	public BigDecimal getOpenTradeEquityBase() {
		return this.openTradeEquityBase;
	}


	public void setOpenTradeEquityBase(BigDecimal openTradeEquityBase) {
		this.openTradeEquityBase = openTradeEquityBase;
	}


	public BigDecimal getPriorOpenTradeEquityLocal() {
		return this.priorOpenTradeEquityLocal;
	}


	public void setPriorOpenTradeEquityLocal(BigDecimal priorOpenTradeEquityLocal) {
		this.priorOpenTradeEquityLocal = priorOpenTradeEquityLocal;
	}


	public BigDecimal getPriorOpenTradeEquityBase() {
		return this.priorOpenTradeEquityBase;
	}


	public void setPriorOpenTradeEquityBase(BigDecimal priorOpenTradeEquityBase) {
		this.priorOpenTradeEquityBase = priorOpenTradeEquityBase;
	}


	public BigDecimal getTodayCommissionLocal() {
		return this.todayCommissionLocal;
	}


	public void setTodayCommissionLocal(BigDecimal todayCommissionLocal) {
		this.todayCommissionLocal = todayCommissionLocal;
	}


	public BigDecimal getTodayRealizedGainLossLocal() {
		return this.todayRealizedGainLossLocal;
	}


	public void setTodayRealizedGainLossLocal(BigDecimal todayRealizedGainLossLocal) {
		this.todayRealizedGainLossLocal = todayRealizedGainLossLocal;
	}
}
