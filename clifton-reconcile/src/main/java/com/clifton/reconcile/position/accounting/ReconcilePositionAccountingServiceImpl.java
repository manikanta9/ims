package com.clifton.reconcile.position.accounting;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ReconcilePositionAccountingServiceImpl implements ReconcilePositionAccountingService {

	private AdvancedReadOnlyDAO<ReconcilePositionAccountingDailyGrouping, Criteria> reconcilePositionAccountingDailyGroupingDAO;


	//////////////////////////////////////////////////////////////////////////////
	////////   ReconcilePositionAccountingDailyGrouping Business Methods   ///////
	//////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcilePositionAccountingDailyGrouping> getReconcilePositionAccountingDailyGroupingList(ReconcilePositionAccountingDailyGroupingSearchForm searchForm) {
		return getReconcilePositionAccountingDailyGroupingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	public AdvancedReadOnlyDAO<ReconcilePositionAccountingDailyGrouping, Criteria> getReconcilePositionAccountingDailyGroupingDAO() {
		return this.reconcilePositionAccountingDailyGroupingDAO;
	}


	public void setReconcilePositionAccountingDailyGroupingDAO(AdvancedReadOnlyDAO<ReconcilePositionAccountingDailyGrouping, Criteria> reconcilePositionAccountingDailyGroupingDAO) {
		this.reconcilePositionAccountingDailyGroupingDAO = reconcilePositionAccountingDailyGroupingDAO;
	}
}
