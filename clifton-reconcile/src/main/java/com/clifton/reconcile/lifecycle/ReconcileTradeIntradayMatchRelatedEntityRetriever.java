package com.clifton.reconcile.lifecycle;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.system.lifecycle.retriever.SystemLifeCycleRelatedEntityRetriever;
import com.clifton.trade.Trade;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


/**
 * @author vgomelsky
 */
@Component
public class ReconcileTradeIntradayMatchRelatedEntityRetriever implements SystemLifeCycleRelatedEntityRetriever {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isApplyToEntity(IdentityObject entity) {
		return entity instanceof Trade;
	}


	@Override
	public List<IdentityObject> getRelatedEntityListForEntity(IdentityObject entity) {
		List<IdentityObject> relatedEntityList = new ArrayList<>();

		if (entity instanceof Trade) {
			List<ReconcileTradeIntradayMatch> matchList = getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByTrade(((Trade) entity).getId());
			if (!CollectionUtils.isEmpty(matchList)) {
				relatedEntityList.addAll(matchList);
			}
		}

		return relatedEntityList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
