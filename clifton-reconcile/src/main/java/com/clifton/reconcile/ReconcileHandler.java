package com.clifton.reconcile;


import com.clifton.business.company.BusinessCompany;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;


public interface ReconcileHandler {

	public abstract InvestmentSecurity lookupInvestmentSecurity(String symbol, Short dataSourceId, boolean currency);


	public abstract InvestmentAccount lookupInvestmentHoldingAccount(String accountNumber, Integer issuingCompanyId, String accountGroupTypeName);


	public abstract InvestmentAccount lookupInvestmentHoldingAccount(String accountNumber, Integer issuingCompanyId);


	public abstract InvestmentAccount lookupInvestmentHoldingAccount(String accountNumber, Integer issuingCompanyId, boolean validate);


	public abstract InvestmentAccount lookupClientInvestmentAccount(int relatedAccountId, BusinessCompany brokerCompany, String holdingAccountNumber, String[] relationshipPurposes);
}
