package com.clifton.reconcile;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.investment.account.group.InvestmentAccountGroupService;
import com.clifton.investment.account.group.search.InvestmentAccountGroupAccountSearchForm;
import com.clifton.investment.account.search.InvestmentAccountSearchForm;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceSecurity;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.datasource.search.MarketDataSourceSecuritySearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class ReconcileHandlerImpl implements ReconcileHandler {

	private InvestmentAccountService investmentAccountService;
	private InvestmentAccountGroupService InvestmentAccountGroupService;
	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataSourceService marketDataSourceService;


	@Override
	public InvestmentSecurity lookupInvestmentSecurity(String symbol, Short dataSourceId, boolean currency) {
		MarketDataSourceSecuritySearchForm searchForm = new MarketDataSourceSecuritySearchForm();
		searchForm.setDataSourceSymbol(symbol);
		searchForm.setDataSourceId(dataSourceId);
		MarketDataSourceSecurity sourceSecurity = CollectionUtils.getOnlyElement(getMarketDataSourceService().getMarketDataSourceSecurityList(searchForm));
		if (sourceSecurity != null) {
			return sourceSecurity.getSecurity();
		}
		return getInvestmentInstrumentService().getInvestmentSecurityBySymbol(symbol, currency);
	}


	@Override
	public InvestmentAccount lookupInvestmentHoldingAccount(String accountNumber, Integer issuingCompanyId, String accountGroupTypeName) {
		InvestmentAccount account = lookupInvestmentHoldingAccount(accountNumber, issuingCompanyId, false);
		if (account == null) {
			InvestmentAccountGroupAccountSearchForm searchForm = new InvestmentAccountGroupAccountSearchForm();
			searchForm.setGroupAlias(accountNumber);
			searchForm.setGroupTypeName(accountGroupTypeName);
			searchForm.setPrimary(true);
			List<InvestmentAccountGroupAccount> groupAccountList = getInvestmentAccountGroupService().getInvestmentAccountGroupAccountList(searchForm);
			ValidationUtils.assertFalse((groupAccountList != null) && (groupAccountList.size() > 1), "Multiple holding account groups found for [" + accountNumber + "].");
			ValidationUtils.assertFalse((groupAccountList == null) || (groupAccountList.isEmpty()), "Could not find a holding account or account group for [" + accountNumber + "].");
			return groupAccountList == null ? null : groupAccountList.get(0).getReferenceTwo();
		}
		return account;
	}


	@Override
	public InvestmentAccount lookupInvestmentHoldingAccount(String accountNumber, Integer issuingCompanyId) {
		return lookupInvestmentHoldingAccount(accountNumber, issuingCompanyId, true);
	}


	@Override
	public InvestmentAccount lookupInvestmentHoldingAccount(String accountNumber, Integer issuingCompanyId, boolean validate) {
		InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
		searchForm.setNumberEquals(accountNumber);
		searchForm.setOurAccount(false);
		if (issuingCompanyId != null) {
			searchForm.setIssuingCompanyId(issuingCompanyId);
		}
		//searchForm.setAccountType("Futures Broker");
		List<InvestmentAccount> result = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		// remove the '-' and try the lookup again
		if (((result == null) || (result.isEmpty())) && !accountNumber.isEmpty() && accountNumber.contains("-")) {
			searchForm = new InvestmentAccountSearchForm();
			searchForm.setNumber(accountNumber.replaceAll("-", ""));
			searchForm.setOurAccount(false);
			result = getInvestmentAccountService().getInvestmentAccountList(searchForm);
		}
		ValidationUtils.assertFalse((result != null) && (result.size() > 1), "Multiple holding accounts found for [" + accountNumber + "].");
		if (validate) {
			ValidationUtils.assertFalse((result == null) || (result.isEmpty()), "Could not find a holding account for [" + accountNumber + "].");
		}
		return (result == null) || (result.isEmpty()) ? null : result.get(0);
	}


	@Override
	public InvestmentAccount lookupClientInvestmentAccount(int relatedAccountId, BusinessCompany brokerCompany, String holdingAccountNumber, String[] relationshipPurposes) {
		try {
			InvestmentAccountSearchForm searchForm = new InvestmentAccountSearchForm();
			//Do we need to use this purpose?
			searchForm.setRelatedPurposes(relationshipPurposes);
			if (brokerCompany != null) {
				searchForm.setIssuingCompanyId(brokerCompany.getId());
			}
			searchForm.setOurAccount(true);
			searchForm.setRelatedAccountId(relatedAccountId);
			searchForm.setWorkflowStateNames(new String[]{"Active", "Open"});
			return CollectionUtils.getFirstElement(getInvestmentAccountService().getInvestmentAccountList(searchForm));
		}
		catch (Throwable e) {
			LogUtils.warn(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Could not find related client account for [" + holdingAccountNumber + " (" + relatedAccountId + ")] with [" + (brokerCompany != null ? brokerCompany.getLabel() : "") + "]"));
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public InvestmentAccountGroupService getInvestmentAccountGroupService() {
		return this.InvestmentAccountGroupService;
	}


	public void setInvestmentAccountGroupService(InvestmentAccountGroupService investmentAccountGroupService) {
		this.InvestmentAccountGroupService = investmentAccountGroupService;
	}


	public MarketDataSourceService getMarketDataSourceService() {
		return this.marketDataSourceService;
	}


	public void setMarketDataSourceService(MarketDataSourceService marketDataSourceService) {
		this.marketDataSourceService = marketDataSourceService;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}
}
