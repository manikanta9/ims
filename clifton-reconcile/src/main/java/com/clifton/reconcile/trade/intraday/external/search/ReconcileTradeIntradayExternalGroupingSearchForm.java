package com.clifton.reconcile.trade.intraday.external.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;

import java.util.Date;


public class ReconcileTradeIntradayExternalGroupingSearchForm extends ReconcileTradeIntradayExternalExtendedSearchForm {

	@SearchField
	private Integer totalTrades;

	@SearchField(searchField = "investmentSecurity.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] investmentSecurityIdList;

	@SearchField(searchField = "tradeDate", comparisonConditions = ComparisonConditions.IN)
	private Date[] tradeDateList;


	public Integer getTotalTrades() {
		return this.totalTrades;
	}


	public void setTotalTrades(Integer totalTrades) {
		this.totalTrades = totalTrades;
	}


	public Integer[] getInvestmentSecurityIdList() {
		return this.investmentSecurityIdList;
	}


	public void setInvestmentSecurityIdList(Integer[] investmentSecurityIdList) {
		this.investmentSecurityIdList = investmentSecurityIdList;
	}


	public Date[] getTradeDateList() {
		return this.tradeDateList;
	}


	public void setTradeDateList(Date[] tradeDateList) {
		this.tradeDateList = tradeDateList;
	}
}
