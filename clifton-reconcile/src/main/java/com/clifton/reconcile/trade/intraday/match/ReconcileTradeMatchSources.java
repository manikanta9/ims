package com.clifton.reconcile.trade.intraday.match;


/**
 * The <code>ReconcileTradeMatchSource</code> enum defines available sources of reconciliation/confirmation data.
 *
 * @author vgomelsky
 */
public enum ReconcileTradeMatchSources {

	FILE, EMAIL, PDF, PHONE, WEBSITE, AUTO
}
