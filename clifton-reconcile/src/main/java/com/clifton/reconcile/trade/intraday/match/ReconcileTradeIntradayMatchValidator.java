package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;


/**
 * The <code>ReconcileTradeIntradayMatchValidator</code> validates
 * when TradeFillID is not NULL that it is not a duplicate, i.e. No other records for the
 * same TradeFillID and ExternalID
 *
 * @author manderson
 */
@Component
public class ReconcileTradeIntradayMatchValidator extends SelfRegisteringDaoValidator<ReconcileTradeIntradayMatch> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.SAVE;
	}


	@Override
	public void validate(ReconcileTradeIntradayMatch bean, DaoEventTypes config, ReadOnlyDAO<ReconcileTradeIntradayMatch> dao) throws ValidationException {
		// Validate Only If Trade Fill is populated
		if (bean.getTradeFill() != null) {
			// And is an Insert or TradeFillID changed
			ReconcileTradeIntradayMatch originalBean = (config.isUpdate() ? getOriginalBean(bean) : null);
			if (originalBean == null || !bean.getTradeFill().equals(originalBean.getTradeFill())) {
				Integer externalId = (bean.getReconcileTradeIntradayExternal() == null ? null : bean.getReconcileTradeIntradayExternal().getId());
				ReconcileTradeIntradayMatch duplicate = dao.findOneByFields(new String[]{"tradeFill.id", "reconcileTradeIntradayExternal.id"}, new Object[]{bean.getTradeFill().getId(),
						externalId});
				if (duplicate != null && (config.isInsert() || !bean.equals(duplicate))) {
					throw new ValidationException("Duplicate match record.  There already exists a " + (externalId == null ? "manual " : "")
							+ "Reconcile Trade Intraday Match Record for Trade Fill ID [" + bean.getTradeFill().getId() + "]" + (externalId == null ? "." : " and external id [" + externalId + "]."));
				}
			}
		}
		// Else if External ID is populated without a Trade Fill, match sure not a duplicate match for external without a trade id
		else if (bean.getReconcileTradeIntradayExternal() != null) {
			// And is an Insert or External ID changed
			ReconcileTradeIntradayMatch originalBean = (config.isUpdate() ? getOriginalBean(bean) : null);
			if (originalBean == null || !bean.getTradeFill().equals(originalBean.getTradeFill())) {
				ReconcileTradeIntradayMatch duplicate = dao.findOneByFields(new String[]{"tradeFill.id", "reconcileTradeIntradayExternal.id"}, new Object[]{null,
						bean.getReconcileTradeIntradayExternal().getId()});
				if (duplicate != null && (config.isInsert() || !bean.equals(duplicate))) {
					throw new ValidationException("Duplicate match record.  There already exists a Reconcile Trade Intraday Match Record for no IMS Trade Fill ID and external id ["
							+ bean.getReconcileTradeIntradayExternal().getId() + "].");
				}
			}
		}
	}


	@SuppressWarnings("unused")
	@Override
	public void validate(ReconcileTradeIntradayMatch bean, DaoEventTypes config) throws ValidationException {
		// uses dao method
	}
}
