package com.clifton.reconcile.trade.intraday.match.note.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.system.note.SystemNote;
import com.clifton.system.note.SystemNoteEntity;
import com.clifton.system.note.SystemNoteLink;
import com.clifton.system.note.SystemNoteService;
import com.clifton.system.note.SystemNoteType;
import com.clifton.system.note.action.SystemNoteActionHandler;
import com.clifton.system.note.search.SystemNoteSearchForm;

import java.util.List;


/**
 * The <code>ReconcileTradeIntradayMatchNoteUpdateActionHandler</code> updates either the internal or external trade note
 * fields on the {@link com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch} object based on changes to the
 * trade notes.
 * <p>
 * This essentially copies the note text to the reconciliation match table so they can be easily exported and search on in the grids
 * We use the last note of the type, so if it's an insert or an update we just update the note text, but if it's a delete, then check the trades for the last note (if there is one)
 *
 * @author manderson
 */
public class ReconcileTradeIntradayMatchNoteUpdateActionHandler implements SystemNoteActionHandler {


	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;

	private SystemNoteService systemNoteService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The note field on the {@link com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch}
	 * that is synced with the system note.
	 */
	private String notePropertyName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void validateSystemNoteType(SystemNoteType noteType) {
		String msgPrefix = "In order to use action bean [" + noteType.getNoteActionBean().getName() + "]: ";
		ValidationUtils.assertEquals("Trade", noteType.getTable().getName(), msgPrefix + "The note type must be for the Trade table.");
		ValidationUtils.assertFalse(noteType.isGlobalNoteAllowed(), msgPrefix + "The note type cannot support global notes.");
	}


	@Override
	public void processSystemNoteAction(SystemNote originalBean, SystemNote bean, DaoEventTypes event) {
		String note = (event.isDelete() ? null : bean.getText());
		if (bean.getFkFieldId() != null) {
			if (event.isDelete()) {
				resetReconciliationTradeIntradayMatchNotePropertyValueForTrade(MathUtils.getNumberAsInteger(bean.getFkFieldId()), bean.getNoteType());
			}
			else {
				updateReconcileTradeIntradayMatchNotePropertyValueForTrade(MathUtils.getNumberAsInteger(bean.getFkFieldId()), note);
			}
		}
		else if (bean.isLinkedToMultiple()) {
			// Only need to apply on update and ONLY if note text changed - inserts and deletes can be handled by the link action processing
			if (event.isUpdate() && !StringUtils.isEqual(originalBean.getText(), note)) {
				List<SystemNoteEntity> systemNoteEntityList = getSystemNoteService().getSystemNoteLinkedEntityListByNote(bean.getId());
				for (SystemNoteEntity systemNoteEntity : CollectionUtils.getIterable(systemNoteEntityList)) {
					updateReconcileTradeIntradayMatchNotePropertyValueForTrade(BeanUtils.getIdentityAsInteger(systemNoteEntity.getEntity()), note);
				}
			}
		}
		else {
			throw new ValidationException("Unsupported action on note " + bean.getId() + ". Unable to determine what trade it belongs to.  It is not directly linked to any trades.");
		}
	}


	@Override
	public void processSystemNoteLinkAction(SystemNoteLink originalBean, SystemNoteLink bean, DaoEventTypes event) {
		// NO UPDATES ON LINKS - INSERT AND DELETE ONLY
		if (event.isInsert()) {
			String note = (event.isDelete() ? null : bean.getNote().getText());
			updateReconcileTradeIntradayMatchNotePropertyValueForTrade(bean.getFkFieldId(), note);
		}
		else if (event.isDelete()) {
			resetReconciliationTradeIntradayMatchNotePropertyValueForTrade(bean.getFkFieldId(), bean.getNote().getNoteType());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Used to update the note text to given value
	 */
	private void updateReconcileTradeIntradayMatchNotePropertyValueForTrade(long tradeId, String note) {
		List<ReconcileTradeIntradayMatch> matchList = getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByTrade(MathUtils.getNumberAsInteger(tradeId));
		for (ReconcileTradeIntradayMatch match : CollectionUtils.getIterable(matchList)) {
			if (!StringUtils.isEqual((String) BeanUtils.getPropertyValue(match, getNotePropertyName()), note)) {
				BeanUtils.setPropertyValue(match, getNotePropertyName(), note);
				getReconcileTradeIntradayMatchService().saveReconcileTradeIntradayMatch(match);
			}
		}
	}


	/**
	 * Used for deletes to find the last note updated for the type and use that to update the note text
	 */
	private void resetReconciliationTradeIntradayMatchNotePropertyValueForTrade(long tradeId, SystemNoteType noteType) {
		SystemNoteSearchForm searchForm = new SystemNoteSearchForm();
		searchForm.setEntityTableName("Trade");
		searchForm.setFkFieldId(tradeId);
		searchForm.setNoteTypeId(noteType.getId());
		searchForm.setOrderBy("lastUpdateDate:DESC");
		searchForm.setLimit(1);
		SystemNote lastNote = CollectionUtils.getFirstElement(getSystemNoteService().getSystemNoteListForEntity(searchForm));
		if (lastNote != null) {
			updateReconcileTradeIntradayMatchNotePropertyValueForTrade(tradeId, lastNote.getText());
		}
		else {
			updateReconcileTradeIntradayMatchNotePropertyValueForTrade(tradeId, null);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////////              Getter and Setter Methods              /////////////
	////////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}


	public SystemNoteService getSystemNoteService() {
		return this.systemNoteService;
	}


	public void setSystemNoteService(SystemNoteService systemNoteService) {
		this.systemNoteService = systemNoteService;
	}


	public String getNotePropertyName() {
		return this.notePropertyName;
	}


	public void setNotePropertyName(String notePropertyName) {
		this.notePropertyName = notePropertyName;
	}
}
