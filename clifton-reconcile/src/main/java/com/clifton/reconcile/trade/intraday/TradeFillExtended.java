package com.clifton.reconcile.trade.intraday;


import com.clifton.core.util.CollectionUtils;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.trade.TradeFill;

import java.util.List;


public class TradeFillExtended extends TradeFill {

	private List<ReconcileTradeIntradayMatch> matchList;
	private boolean reconciled;


	////////////////////////////////////////////


	public int getMatchListSize() {
		return (CollectionUtils.getSize(getMatchList()));
	}


	public ReconcileTradeIntradayMatch getMatch() {
		if (getMatchListSize() == 1) {
			return getMatchList().get(0);
		}
		return null;
	}


	////////////////////////////////////////////


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public List<ReconcileTradeIntradayMatch> getMatchList() {
		return this.matchList;
	}


	public void setMatchList(List<ReconcileTradeIntradayMatch> matchList) {
		this.matchList = matchList;
	}
}
