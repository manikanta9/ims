package com.clifton.reconcile.trade.intraday.workflow;


import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchTemp;
import com.clifton.trade.Trade;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>TradeBookingIntradayReconciliationWorkflowAction</code> class is a workflow transition action that re-attaches reconciliation information
 * to trades that were previously unbooked and there is a record in the {@link ReconcileTradeIntradayMatchTemp} table
 * <p/>
 * Comments: When Trade is unbooked, the reconciliation row will be deleted unless it has a Workflow attached to it (and also would be a single fill trade)
 * Under those cases, a {@link ReconcileTradeIntradayMatchTemp} record is created to store the link from the Trade to the Reconciliation row
 * and the TradeFillID is cleared.  On re-booking, the temp table is then checked to re-attach reconciliation information to the fill and is removed from the temp table
 *
 * @author manderson
 */
public class TradeBookingIntradayReconciliationWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		getReconcileTradeIntradayMatchService().processReconcileTradeIntradayMatchBookTrade(trade);
		return trade;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
