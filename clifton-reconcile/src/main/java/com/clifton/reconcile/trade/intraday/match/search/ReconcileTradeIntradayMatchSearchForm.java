package com.clifton.reconcile.trade.intraday.match.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.workflow.search.BaseWorkflowAwareSearchForm;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcileTradeIntradayMatchSearchForm extends BaseWorkflowAwareSearchForm {

	@SearchField(searchField = "trade.id", searchFieldPath = "tradeFill")
	private Integer tradeId;

	@SearchField(searchField = "tradeFill.id")
	private Integer tradeFillId;

	@SearchField(searchField = "reconcileTradeIntradayExternal.id")
	private Integer reconcileTradeIntradayExternalId;

	@SearchField(searchField = "tradeFill.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] tradeFillIdList;

	@SearchField(searchField = "reconcileTradeIntradayExternal.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] reconcileTradeIntradayExternalIdList;

	@SearchField(searchField = "investmentSecurity.id", searchFieldPath = "tradeFill.trade")
	private Integer investmentSecurityId;

	@SearchField(searchField = "endDate", searchFieldPath = "tradeFill.trade.investmentSecurity")
	private Date securityEndDate;

	@SearchField(searchField = "underlyingSecurity.id", searchFieldPath = "tradeFill.trade.investmentSecurity", sortField = "underlyingSecurity.symbol")
	private Integer underlyingInvestmentSecurityId;

	@SearchField(searchField = "clientInvestmentAccount.id", searchFieldPath = "tradeFill.trade")
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "businessClient.id", sortField = "businessClient.name", searchFieldPath = "tradeFill.trade.clientInvestmentAccount")
	private Integer clientId;

	@SearchField(searchField = "holdingInvestmentAccount.id", searchFieldPath = "tradeFill.trade")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "name", searchFieldPath = "tradeFill.trade.holdingInvestmentAccount.type")
	private String holdingInvestmentAccountTypeName;

	@SearchField(searchField = "type.id", searchFieldPath = "tradeFill.trade.holdingInvestmentAccount")
	private Short holdingInvestmentAccountTypeId;

	@SearchField(searchField = "executingBrokerCompany.id", searchFieldPath = "tradeFill.trade")
	private Integer executingBrokerCompanyId;

	@SearchField(searchField = "issuingCompany.id", searchFieldPath = "tradeFill.trade.holdingInvestmentAccount")
	private Integer holdingCompanyId;

	@SearchField(searchField = "name", searchFieldPath = "tradeFill.trade.tradeType")
	private String tradeTypeName;

	@SearchField(searchField = "tradeType.id", searchFieldPath = "tradeFill.trade")
	private Short tradeTypeId;

	@SearchField(searchField = "quantity", searchFieldPath = "tradeFill")
	private BigDecimal quantity;

	@SearchField(searchField = "notionalUnitPrice", searchFieldPath = "tradeFill")
	private BigDecimal notionalUnitPrice;

	@SearchField(searchField = "buy", searchFieldPath = "tradeFill.trade")
	private Boolean buy;

	@SearchField(searchField = "tradeDate", searchFieldPath = "tradeFill.trade")
	private Date tradeDate;

	@SearchField
	private Boolean confirmed;


	@SearchField
	private String note;

	@SearchField
	private String internalTradeNote;

	@SearchField
	private String externalTradeNote;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Integer getTradeId() {
		return this.tradeId;
	}


	public void setTradeId(Integer tradeId) {
		this.tradeId = tradeId;
	}


	public Integer getTradeFillId() {
		return this.tradeFillId;
	}


	public void setTradeFillId(Integer tradeFillId) {
		this.tradeFillId = tradeFillId;
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Boolean getConfirmed() {
		return this.confirmed;
	}


	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Integer getHoldingCompanyId() {
		return this.holdingCompanyId;
	}


	public void setHoldingCompanyId(Integer holdingCompanyId) {
		this.holdingCompanyId = holdingCompanyId;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getNotionalUnitPrice() {
		return this.notionalUnitPrice;
	}


	public void setNotionalUnitPrice(BigDecimal notionalUnitPrice) {
		this.notionalUnitPrice = notionalUnitPrice;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public Integer getReconcileTradeIntradayExternalId() {
		return this.reconcileTradeIntradayExternalId;
	}


	public void setReconcileTradeIntradayExternalId(Integer reconcileTradeIntradayExternalId) {
		this.reconcileTradeIntradayExternalId = reconcileTradeIntradayExternalId;
	}


	public String getHoldingInvestmentAccountTypeName() {
		return this.holdingInvestmentAccountTypeName;
	}


	public void setHoldingInvestmentAccountTypeName(String holdingInvestmentAccountTypeName) {
		this.holdingInvestmentAccountTypeName = holdingInvestmentAccountTypeName;
	}


	public Integer getExecutingBrokerCompanyId() {
		return this.executingBrokerCompanyId;
	}


	public void setExecutingBrokerCompanyId(Integer executingBrokerCompanyId) {
		this.executingBrokerCompanyId = executingBrokerCompanyId;
	}


	public String getTradeTypeName() {
		return this.tradeTypeName;
	}


	public void setTradeTypeName(String tradeTypeName) {
		this.tradeTypeName = tradeTypeName;
	}


	public Short getTradeTypeId() {
		return this.tradeTypeId;
	}


	public void setTradeTypeId(Short tradeTypeId) {
		this.tradeTypeId = tradeTypeId;
	}


	public Short getHoldingInvestmentAccountTypeId() {
		return this.holdingInvestmentAccountTypeId;
	}


	public void setHoldingInvestmentAccountTypeId(Short holdingInvestmentAccountTypeId) {
		this.holdingInvestmentAccountTypeId = holdingInvestmentAccountTypeId;
	}


	public Integer[] getTradeFillIdList() {
		return this.tradeFillIdList;
	}


	public void setTradeFillIdList(Integer[] tradeFillIdList) {
		this.tradeFillIdList = tradeFillIdList;
	}


	public Integer[] getReconcileTradeIntradayExternalIdList() {
		return this.reconcileTradeIntradayExternalIdList;
	}


	public void setReconcileTradeIntradayExternalIdList(Integer[] reconcileTradeIntradayExternalIdList) {
		this.reconcileTradeIntradayExternalIdList = reconcileTradeIntradayExternalIdList;
	}


	public Integer getUnderlyingInvestmentSecurityId() {
		return this.underlyingInvestmentSecurityId;
	}


	public void setUnderlyingInvestmentSecurityId(Integer underlyingInvestmentSecurityId) {
		this.underlyingInvestmentSecurityId = underlyingInvestmentSecurityId;
	}


	public Integer getClientId() {
		return this.clientId;
	}


	public void setClientId(Integer clientId) {
		this.clientId = clientId;
	}


	public Date getSecurityEndDate() {
		return this.securityEndDate;
	}


	public void setSecurityEndDate(Date securityEndDate) {
		this.securityEndDate = securityEndDate;
	}


	public String getInternalTradeNote() {
		return this.internalTradeNote;
	}


	public void setInternalTradeNote(String internalTradeNote) {
		this.internalTradeNote = internalTradeNote;
	}


	public String getExternalTradeNote() {
		return this.externalTradeNote;
	}


	public void setExternalTradeNote(String externalTradeNote) {
		this.externalTradeNote = externalTradeNote;
	}
}
