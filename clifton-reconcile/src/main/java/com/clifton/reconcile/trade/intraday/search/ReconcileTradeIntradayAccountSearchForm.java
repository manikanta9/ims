package com.clifton.reconcile.trade.intraday.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


@SearchForm(hasOrmDtoClass = false)
public class ReconcileTradeIntradayAccountSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;


	@SearchField(searchField = "name,number", searchFieldPath = "holdingInvestmentAccount", sortField = "number")
	private String holdingInvestmentAccount;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount.issuingCompany")
	private String holdingInvestmentAccountIssuerName;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount.type")
	private String holdingInvestmentAccountTypeName;

	@SearchField(searchField = "holdingInvestmentAccount.type.id", comparisonConditions = ComparisonConditions.IN)
	private Short[] holdingInvestmentAccountTypeIds;

	@SearchField
	private Date tradeDate;

	@SearchField
	private Boolean reconciled;

	// Custom filter showing records with a holding account linked to this client account
	private Integer clientInvestmentAccountId;


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public String getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(String holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public String getHoldingInvestmentAccountIssuerName() {
		return this.holdingInvestmentAccountIssuerName;
	}


	public void setHoldingInvestmentAccountIssuerName(String holdingInvestmentAccountIssuerName) {
		this.holdingInvestmentAccountIssuerName = holdingInvestmentAccountIssuerName;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public String getHoldingInvestmentAccountTypeName() {
		return this.holdingInvestmentAccountTypeName;
	}


	public void setHoldingInvestmentAccountTypeName(String holdingInvestmentAccountTypeName) {
		this.holdingInvestmentAccountTypeName = holdingInvestmentAccountTypeName;
	}


	public Short[] getHoldingInvestmentAccountTypeIds() {
		return this.holdingInvestmentAccountTypeIds;
	}


	public void setHoldingInvestmentAccountTypeIds(Short[] holdingInvestmentAccountTypeIds) {
		this.holdingInvestmentAccountTypeIds = holdingInvestmentAccountTypeIds;
	}
}
