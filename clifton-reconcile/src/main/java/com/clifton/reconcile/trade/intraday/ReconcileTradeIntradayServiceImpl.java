package com.clifton.reconcile.trade.intraday;


import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.account.relationship.InvestmentAccountRelationship;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.search.ReconcileTradeIntradayAccountSearchForm;
import com.clifton.reconcile.trade.intraday.search.ReconcileTradeIntradaySecuritySearchForm;
import com.clifton.reconcile.trade.intraday.search.TradeExtendedSearchForm;
import com.clifton.reconcile.trade.intraday.search.TradeFillExtendedSearchForm;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ReconcileTradeIntradayServiceImpl implements ReconcileTradeIntradayService {

	private AdvancedReadOnlyDAO<ReconcileTradeIntradaySecurity, Criteria> reconcileTradeIntradaySecurityDAO;
	private AdvancedReadOnlyDAO<TradeExtended, Criteria> tradeExtendedDAO;
	private AdvancedReadOnlyDAO<TradeFillExtended, Criteria> tradeFillExtendedDAO;

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;
	private WorkflowDefinitionService workflowDefinitionService;

	private List<String> intradayTradeInvestmentGroupNameList;


	//////////////////////////////////////////////////////////////////////////////////////
	////////     ReconcileTradeIntradayAccountExtended Business Methods      /////////////
	//////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcileTradeIntradayAccount> getReconcileTradeIntradayAccountList(ReconcileTradeIntradayAccountSearchForm searchForm) {
		int resultCount = 0;
		List<ReconcileTradeIntradayAccount> result = new ArrayList<>();
		Map<String, ReconcileTradeIntradayAccount> resultMap = new HashMap<>();

		// get security level reconciliation data and sum counts by client/holding accounts and date
		List<ReconcileTradeIntradaySecurity> securityRecList = getReconcileTradeIntradaySecurityList(new ReconcileTradeIntradaySecuritySearchForm(searchForm));
		for (ReconcileTradeIntradaySecurity securityRec : CollectionUtils.getIterable(securityRecList)) {
			String resultKey = securityRec.getHoldingInvestmentAccount().getId() + "-" + securityRec.getTradeDate().getTime();
			ReconcileTradeIntradayAccount accountRec = resultMap.get(resultKey);
			if (accountRec == null) {
				resultCount++;
				accountRec = new ReconcileTradeIntradayAccount();
				accountRec.setId(resultCount);
				accountRec.setTradeDate(securityRec.getTradeDate());
				accountRec.setHoldingInvestmentAccount(securityRec.getHoldingInvestmentAccount());
				accountRec.setReconciled(securityRec.isReconciled());
				accountRec.setTradeCount(securityRec.getTradeCount());
				accountRec.setExternalTradeCount(securityRec.getExternalTradeCount());
				accountRec.setUnreconciledTradeCount(securityRec.getUnreconciledTradeCount());
				accountRec.setExternalUnreconciledTradeCount(securityRec.getExternalUnreconciledTradeCount());

				result.add(accountRec);
				resultMap.put(resultKey, accountRec);
			}
			else { // add to account that was already processed
				accountRec.setTradeCount(accountRec.getTradeCount() + securityRec.getTradeCount());
				accountRec.setExternalTradeCount(accountRec.getExternalTradeCount() + securityRec.getExternalTradeCount());
				accountRec.setUnreconciledTradeCount(accountRec.getUnreconciledTradeCount() + securityRec.getUnreconciledTradeCount());
				accountRec.setExternalUnreconciledTradeCount(accountRec.getExternalUnreconciledTradeCount() + securityRec.getExternalUnreconciledTradeCount());
				if (!securityRec.isReconciled()) {
					accountRec.setReconciled(securityRec.isReconciled());
				}
			}
		}
		return result;
	}


	//////////////////////////////////////////////////////////////////////////////////////
	////////     ReconcileTradeIntradaySecurityExtended Business Methods     /////////////
	//////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcileTradeIntradaySecurity> getReconcileTradeIntradaySecurityList(final ReconcileTradeIntradaySecuritySearchForm searchForm) {
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (!CollectionUtils.isEmpty(getIntradayTradeInvestmentGroupNameList())) {
					criteria.createAlias("investmentSecurity", "s")
							.createAlias("s.instrument", "i")
							.createAlias("i.groupItemList", "gi")
							.createAlias("gi.referenceOne", "r")
							.createAlias("r.group", "g")
							.add(Restrictions.in("g.name", getIntradayTradeInvestmentGroupNameList()));
				}

				if (searchForm.getClientInvestmentAccountId() != null) {
					DetachedCriteria subSelect = DetachedCriteria.forClass(InvestmentAccountRelationship.class, "r");
					subSelect.setProjection(Projections.id())
							.add(Restrictions.eqProperty("referenceTwo.id", criteria.getAlias() + ".holdingInvestmentAccount.id"))
							.add(Restrictions.eq("referenceOne.id", searchForm.getClientInvestmentAccountId()))
							.add(Restrictions.or(Restrictions.isNull("startDate"), Restrictions.lt("startDate", new Date())))
							.add(Restrictions.or(Restrictions.isNull("endDate"), Restrictions.gt("endDate", new Date())));

					criteria.add(Subqueries.exists(subSelect));
				}
			}
		};
		return getReconcileTradeIntradaySecurityDAO().findBySearchCriteria(configurer);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          TradeFillExtended Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<TradeExtended> getTradeExtendedList(TradeExtendedSearchForm searchForm) {
		return getTradeExtendedDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	@Override
	public List<TradeFillExtended> getTradeFillExtendedList(TradeFillExtendedSearchForm searchForm) {
		List<TradeFillExtended> result = getTradeFillExtendedDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
		for (TradeFillExtended fill : CollectionUtils.getIterable(result)) {
			if (fill.isReconciled()) {
				fill.setMatchList(getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByFill(fill.getId()));
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  /////////// 
	////////////////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<ReconcileTradeIntradaySecurity, Criteria> getReconcileTradeIntradaySecurityDAO() {
		return this.reconcileTradeIntradaySecurityDAO;
	}


	public void setReconcileTradeIntradaySecurityDAO(AdvancedReadOnlyDAO<ReconcileTradeIntradaySecurity, Criteria> reconcileTradeIntradaySecurityDAO) {
		this.reconcileTradeIntradaySecurityDAO = reconcileTradeIntradaySecurityDAO;
	}


	public AdvancedReadOnlyDAO<TradeFillExtended, Criteria> getTradeFillExtendedDAO() {
		return this.tradeFillExtendedDAO;
	}


	public void setTradeFillExtendedDAO(AdvancedReadOnlyDAO<TradeFillExtended, Criteria> tradeFillExtendedDAO) {
		this.tradeFillExtendedDAO = tradeFillExtendedDAO;
	}


	public AdvancedReadOnlyDAO<TradeExtended, Criteria> getTradeExtendedDAO() {
		return this.tradeExtendedDAO;
	}


	public void setTradeExtendedDAO(AdvancedReadOnlyDAO<TradeExtended, Criteria> tradeExtendedDAO) {
		this.tradeExtendedDAO = tradeExtendedDAO;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}


	public List<String> getIntradayTradeInvestmentGroupNameList() {
		return this.intradayTradeInvestmentGroupNameList;
	}


	public void setIntradayTradeInvestmentGroupNameList(List<String> intradayTradeInvestmentGroupNameList) {
		this.intradayTradeInvestmentGroupNameList = intradayTradeInvestmentGroupNameList;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
