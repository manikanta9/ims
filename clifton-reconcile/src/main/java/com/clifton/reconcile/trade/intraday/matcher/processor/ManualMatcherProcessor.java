package com.clifton.reconcile.trade.intraday.matcher.processor;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatcherResult;
import com.clifton.trade.TradeFill;


/**
 * The <code>ManualMatcherProcessor</code> handles manual trade matches.  This will create a match entry
 * for each fill to each external trade or a one side match for either the internal or external trade.
 *
 * @author mwacker
 */
public class ManualMatcherProcessor implements ReconcileTradeIntradayMatcherProcessor {

	private int order;


	@Override
	public boolean isMatcherProcessorUsable(ReconcileTradeIntradayMatchObject beanList) {
		return beanList.isManualMatch() && (beanList.getTradeFillList().size() <= 1 || beanList.getIntradayExternalList().size() <= 1);
	}


	@Override
	public void processTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		// verify that either our trade fill list or the external trade list has 1 trade.
		ValidationUtils.assertTrue(beanList.getTradeFillList().size() <= 1 || beanList.getIntradayExternalList().size() <= 1, "Manual matching cannot be done on a many to many match.");

		if (CollectionUtils.getSize(beanList.getTradeFillList()) > 0) {
			for (TradeFill fill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
				if (!beanList.getIntradayExternalList().isEmpty()) {
					for (ReconcileTradeIntradayExternal external : CollectionUtils.getIterable(beanList.getIntradayExternalList())) {
						ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
						match.setMatchType(matchType);
						match.setReconcileTradeIntradayExternal(external);
						match.setTradeFill(fill);
						match.setContact(beanList.getContact());
						match.setNote(beanList.getNote());
						match.setConfirmed(matchType.isAutoConfirm());
						result.getNewMatchList().add(match);
					}
				}
				else {
					ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
					match.setMatchType(matchType);
					match.setTradeFill(fill);
					match.setContact(beanList.getContact());
					match.setNote(beanList.getNote());
					match.setConfirmed(matchType.isAutoConfirm());
					result.getNewMatchList().add(match);
				}
			}
		}
		else if (CollectionUtils.getSize(beanList.getIntradayExternalList()) > 0) {
			for (ReconcileTradeIntradayExternal external : CollectionUtils.getIterable(beanList.getIntradayExternalList())) {
				ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
				match.setMatchType(matchType);
				match.setReconcileTradeIntradayExternal(external);
				match.setContact(beanList.getContact());
				match.setNote(beanList.getNote());
				match.setConfirmed(matchType.isAutoConfirm());
				result.getNewMatchList().add(match);
			}
		}
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
