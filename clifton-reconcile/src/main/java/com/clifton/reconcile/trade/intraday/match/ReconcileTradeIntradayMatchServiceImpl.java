package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.InExpressionForNumbers;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.group.InvestmentAccountGroupAccount;
import com.clifton.reconcile.trade.intraday.match.cache.ReconcileTradeMatchTypeCache;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeIntradayMatchSearchForm;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeMatchTypeSearchForm;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import com.clifton.workflow.transition.WorkflowTransitionService;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class ReconcileTradeIntradayMatchServiceImpl implements ReconcileTradeIntradayMatchService {

	/**
	 * NOTE: EVERY WORKFLOW THAT IS USED FOR RECONCILIATION MUST HAVE THESE TWO STATES WITH SYSTEM DEFINED TRANSITIONS FROM ALL STATES
	 * TO Trade Update AND SYSTEM DEFINED TRANSITION FROM TRADE UPDATE TO PENDING RECONCILIATION.  THESE ARE THE STANDARD STATES
	 * THAT PROPERLY HANDLE TRADE UN-BOOKING/RE-BOOKING SO THAT MATCHES CAN ONLY BE RECONCILED/AFFIRMED/CONFIRMED ON BOOKED TRADES
	 */
	public static final String WORKFLOW_STATE_RECONCILE_MATCH_TRADE_UPDATE = "Trade Update";
	public static final String WORKFLOW_STATE_RECONCILE_MATCH_PENDING_REC = "Pending Reconciliation";
	public static final String TABLE_RECONCILE_MATCH = "ReconcileTradeIntradayMatch";


	private AdvancedUpdatableDAO<ReconcileTradeIntradayMatch, Criteria> reconcileTradeIntradayMatchDAO;
	private AdvancedUpdatableDAO<ReconcileTradeMatchType, Criteria> reconcileTradeMatchTypeDAO;

	private UpdatableDAO<ReconcileTradeIntradayMatchTemp> reconcileTradeIntradayMatchTempDAO;

	private TradeService tradeService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	private ReconcileTradeMatchTypeCache reconcileTradeMatchTypeCache;

	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////     ReconcileTradeIntradayMatch Business Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcileTradeIntradayMatch getReconcileTradeIntradayMatch(int id) {
		ReconcileTradeIntradayMatch match = getReconcileTradeIntradayMatchDAO().findByPrimaryKey(id);
		// Check Temp table for Trade
		if (match != null && match.getTradeFill() == null) {
			match.setMatchTemp(getReconcileTradeIntradayMatchTempDAO().findOneByField("referenceOne.id", id));
		}
		return match;
	}


	@Override
	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchByFill(int tradeFillId) {
		ReconcileTradeIntradayMatchSearchForm searchForm = new ReconcileTradeIntradayMatchSearchForm();
		searchForm.setTradeFillId(tradeFillId);
		return getReconcileTradeIntradayMatchList(searchForm);
	}


	@Override
	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchByExternalTrade(int reconcileTradeIntradayExternalId) {
		ReconcileTradeIntradayMatchSearchForm searchForm = new ReconcileTradeIntradayMatchSearchForm();
		searchForm.setReconcileTradeIntradayExternalId(reconcileTradeIntradayExternalId);
		return getReconcileTradeIntradayMatchList(searchForm);
	}


	@Override
	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchByTrade(int tradeId) {
		ReconcileTradeIntradayMatchSearchForm searchForm = new ReconcileTradeIntradayMatchSearchForm();
		searchForm.setTradeId(tradeId);
		return getReconcileTradeIntradayMatchList(searchForm);
	}


	@Override
	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchList(final ReconcileTradeIntradayMatchSearchForm searchForm) {
		// get the list of matches using the default lookup
		List<ReconcileTradeIntradayMatch> result = getReconcileTradeIntradayMatchDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));

		// look up the rest of the matches using the matches that we've already retrieved
		Boolean filterByExternalTrades = null;
		if (searchForm.getTradeId() != null || searchForm.getTradeFillId() != null) {
			filterByExternalTrades = true;
		}
		else if (searchForm.getReconcileTradeIntradayExternalId() != null) {
			// filter by trade fill or trades
			filterByExternalTrades = false;
		}
		if (filterByExternalTrades != null) {
			final List<Integer> matchIdList = new ArrayList<>();
			List<Integer> filterIdList = new ArrayList<>();
			for (ReconcileTradeIntradayMatch match : CollectionUtils.getIterable(result)) {
				matchIdList.add(match.getId());
				if (filterByExternalTrades) {
					if (match.getReconcileTradeIntradayExternal() != null) {
						filterIdList.add(match.getReconcileTradeIntradayExternal().getId());
					}
				}
				else {
					if (match.getTradeFill() != null) {
						filterIdList.add(match.getTradeFill().getId());
					}
				}
			}
			if (!CollectionUtils.isEmpty(filterIdList)) {
				final ReconcileTradeIntradayMatchSearchForm sf = new ReconcileTradeIntradayMatchSearchForm();
				BeanUtils.copyProperties(searchForm, sf, new String[]{"tradeId", "tradeFillId", "reconcileTradeIntradayExternalId", "restrictionList"});
				Integer[] filterIdArray = new Integer[filterIdList.size()];
				filterIdList.toArray(filterIdArray);
				if (filterByExternalTrades) {
					sf.setReconcileTradeIntradayExternalIdList(filterIdArray);
				}
				else {
					sf.setTradeFillIdList(filterIdArray);
				}

				HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(sf) {

					@Override
					public void configureCriteria(Criteria criteria) {
						if (!CollectionUtils.isEmpty(matchIdList)) {
							criteria.add(Restrictions.not(new InExpressionForNumbers("id", matchIdList.toArray())));
						}
						super.configureCriteria(criteria);
					}
				};
				result.addAll(getReconcileTradeIntradayMatchDAO().findBySearchCriteria(config));
			}
		}
		return result;
	}


	@Override
	public void saveReconcileTradeIntradayManualMatchForTrade(int tradeId) {
		// Pull The Trade
		Trade trade = getTradeService().getTrade(tradeId);
		ReconcileTradeMatchTypeSearchForm searchForm = new ReconcileTradeMatchTypeSearchForm();
		searchForm.setSecurityId(trade.getInvestmentSecurity().getId());
		searchForm.setSystemDefined(false);
		List<ReconcileTradeMatchType> typeList = getReconcileTradeMatchTypeList(searchForm);
		if (CollectionUtils.isEmpty(typeList)) {
			throw new ValidationException("There are no manual match types available for security [" + trade.getInvestmentSecurity().getLabel() + "].");
		}
		if (CollectionUtils.getSize(typeList) > 1) {
			throw new ValidationException("There are " + typeList.size() + " manual match types available for security [" + trade.getInvestmentSecurity().getLabel() + "]: "
					+ BeanUtils.getPropertyValues(typeList, "name", ","));
		}
		saveReconcileTradeIntradayManualMatchForTrade(trade, typeList.get(0));
	}


	protected void saveReconcileTradeIntradayManualMatchForTrade(Trade trade, ReconcileTradeMatchType tradeMatchType) {
		// Otherwise ONLY one - OK to Create One Per Fill
		for (TradeFill tf : CollectionUtils.getIterable(trade.getTradeFillList())) {
			ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
			match.setTradeFill(tf);
			match.setMatchType(tradeMatchType);
			saveReconcileTradeIntradayMatch(match);
		}
	}


	@Override
	public ReconcileTradeIntradayMatch saveReconcileTradeIntradayMatch(ReconcileTradeIntradayMatch bean) {
		// TODO: determine appropriate action and validation if fills do not match external trade
		if ((bean.getReconcileTradeIntradayExternal() != null) && (bean.getTradeFill() != null)) {
			ValidationUtils.assertTrue(
					bean.getTradeFill().getTrade().getHoldingInvestmentAccount().getIssuingCompany().equals(bean.getReconcileTradeIntradayExternal().getHoldingInvestmentAccount().getIssuingCompany()),
					"Cannot match trades with different clearing firms.  Trade cleared with [" + bean.getTradeFill().getTrade().getHoldingInvestmentAccount().getIssuingCompany().getLabel()
							+ "] and external trade cleared with [" + bean.getReconcileTradeIntradayExternal().getHoldingInvestmentAccount().getIssuingCompany().getLabel() + "].");
		}

		ValidationUtils.assertNotNull(bean.getMatchType(), "Trade reconciliation Match Type is required.", "matchType");
		if (bean.getMatchType().isContactRequired()) {
			ValidationUtils.assertNotNull(bean.getContact(), "Contact is required for selected match type: " + bean.getMatchType().getName(), "contact");
		}
		if (bean.getMatchType().isNoteRequired()) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(bean.getNote()), "Note is required for selected match type: " + bean.getMatchType().getName(), "note");
		}

		if (bean.getMatchType().isAutoConfirm()) {
			bean.setConfirmed(true);
		}

		return getReconcileTradeIntradayMatchDAO().save(bean);
	}


	@Override
	public void saveReconcileTradeIntradayMatchList(List<ReconcileTradeIntradayMatch> matchList) {
		if (!CollectionUtils.isEmpty(matchList)) {
			getReconcileTradeIntradayMatchDAO().saveList(matchList);
		}
	}


	@Override
	public void deleteReconcileTradeIntradayMatch(int id) {
		getReconcileTradeIntradayMatchDAO().delete(id);
	}


	@Override
	public void deleteReconcileTradeIntradayMatchList(List<ReconcileTradeIntradayMatch> deleteList) {
		getReconcileTradeIntradayMatchDAO().deleteList(deleteList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////     ReconcileTradeMatchType Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcileTradeMatchType getReconcileTradeMatchType(short id) {
		return getReconcileTradeMatchTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public ReconcileTradeMatchType getReconcileTradeMatchTypeByName(String name) {
		return getReconcileTradeMatchTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<ReconcileTradeMatchType> getReconcileTradeMatchTypeList(ReconcileTradeMatchTypeSearchForm searchForm) {
		return getReconcileTradeMatchTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getClientInvestmentAccountId() != null) {
					DetachedCriteria sub = DetachedCriteria.forClass(InvestmentAccountGroupAccount.class, "link");
					sub.setProjection(Projections.property("id"));
					sub.createAlias("referenceOne", "group");
					sub.createAlias("referenceTwo", "acct");
					sub.add(Restrictions.eqProperty("group.id", criteria.getAlias() + ".clientInvestmentAccountGroup.id"));
					sub.add(Restrictions.eq("acct.id", searchForm.getClientInvestmentAccountId()));
					criteria.add(Subqueries.exists(sub));
				}
			}
		});
	}


	/**
	 * Validation: {@link ReconcileTradeMatchTypeValidator} *
	 */
	@Override
	public ReconcileTradeMatchType saveReconcileTradeMatchType(ReconcileTradeMatchType bean) {
		ValidationUtils.assertTrue((!bean.isAutoCreateManualMatch() && bean.getClientInvestmentAccountGroup() == null) || (bean.isAutoCreateManualMatch() && bean.getClientInvestmentAccountGroup() != null), "An account group must be set if auto creation of matching is turned on.");

		return getReconcileTradeMatchTypeDAO().save(bean);
	}


	/**
	 * Validation: {@link ReconcileTradeMatchTypeValidator} *
	 */
	@Override
	public void deleteReconcileTradeMatchType(short id) {
		getReconcileTradeMatchTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	///////     ReconcileTradeIntradayMatchTemp Business Methods      //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void linkReconcileTradeIntradayMatchTemp(ReconcileTradeIntradayMatch match, Trade trade) {
		ReconcileTradeIntradayMatchTemp temp = new ReconcileTradeIntradayMatchTemp();
		temp.setReferenceOne(match);
		temp.setReferenceTwo(trade);
		getReconcileTradeIntradayMatchTempDAO().save(temp);
		match.setTradeFill(null);
		saveReconcileTradeIntradayMatch(match);
	}


	////////////////////////////////////////////////////////////////////////////
	//////  ReconcileTradeIntradayMatch Trade Booking/Unbooking Methods   //////
	//////     Inserts/Deletes Records in ReconcileTradeIntradayMatchTemp //////
	//////     Updates TradeFillID on ReconcileTradeIntradayMatch         //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade getTradeForReconcileTradeIntradayMatch(ReconcileTradeIntradayMatch match) {
		if (match != null) {
			if (match.getTradeFill() != null) {

				return match.getTradeFill().getTrade();
			}
			ReconcileTradeIntradayMatchTemp matchTemp = getReconcileTradeIntradayMatchTempDAO().findOneByField("referenceOne.id", match.getId());
			if (matchTemp != null) {
				return matchTemp.getReferenceTwo();
			}
		}
		return null;
	}


	/**
	 * UN-BOOKING a TRADE: Find All match records for the Trade...
	 * <p/>
	 * If Workflow is Not Used: the match record is automatically deleted.
	 * We do not need to keep the match for workflow history, since there isn't a workflow associated with it, and without workflow the
	 * existence of the match record itself implies that the Trade Fill is reconciled, which cannot be the case here because you can't
	 * reconcile an un-booked trade. Once the Trade is Re-Booked, the matches can be re-created/associated with the new fills.
	 * If Workflow is Used: the match record performs system defined transition to "Trade Update" state
	 *
	 * @param trade
	 */
	@Override
	public void processReconcileTradeIntradayMatchUnbookTrade(Trade trade) {
		List<ReconcileTradeIntradayMatch> matchList = getReconcileTradeIntradayMatchByTrade(trade.getId());
		if (!CollectionUtils.isEmpty(matchList)) {
			for (ReconcileTradeIntradayMatch match : matchList) {
				if (match.getWorkflowState() != null) {
					executeTransitionSystemDefinedForReconcileMatch(match, WORKFLOW_STATE_RECONCILE_MATCH_TRADE_UPDATE);
				}
				else {
					deleteReconcileTradeIntradayMatch(match.getId());
				}
			}
		}
	}


	/**
	 * RE-BOOKING a TRADE: Find All match records for the Trade (CHECK MATCH AND TEMP TABLES)
	 * NOTE: Linking in TEMP table is performed in {@link ReconcileTradeIntradayMatchRemoverForTradeFillObserver} when fills are deleted
	 * <p/>
	 * 1. If exists in match and uses workflow - move to Pending Reconciliation state
	 * 2. If exists in temp:
	 * a. Is single fill trade - re-attaches and moves to Pending Reconciliation state
	 * b. Is multiple fill eligible - single fill trade re-attaches to new fill and moves to Pending Reconciliation state
	 * c. Is multiple fill - re-attaches to the first fill
	 * 3. Only Resets workflow back to Pending Reconciliation if Reconciliation Match was in Trade Update state (i.e. was previously unbooked)
	 *
	 * @param trade
	 */
	@Override
	@Transactional
	public void processReconcileTradeIntradayMatchBookTrade(Trade trade) {
		List<Integer> fillIds = new ArrayList<>();
		List<ReconcileTradeIntradayMatch> matchList = getReconcileTradeIntradayMatchByTrade(trade.getId());
		if (!CollectionUtils.isEmpty(matchList)) {
			for (ReconcileTradeIntradayMatch match : matchList) {
				if (match.getWorkflowState() != null && WORKFLOW_STATE_RECONCILE_MATCH_TRADE_UPDATE.equals(match.getWorkflowState().getName())) {
					fillIds.add(match.getTradeFill().getId());
					executeTransitionSystemDefinedForReconcileMatch(match, WORKFLOW_STATE_RECONCILE_MATCH_PENDING_REC);
				}
			}
		}

		// If there is a temp record, then it was unbooked
		ReconcileTradeIntradayMatchTemp temp = getReconcileTradeIntradayMatchTempDAO().findOneByField("referenceTwo.id", trade.getId());
		if (temp != null) {
			// Sort in ascending order by create date so re-attaches to first one not already with a match
			List<TradeFill> fillList = BeanUtils.sortWithFunction(getTradeService().getTradeFillListByTrade(trade.getId()), TradeFill::getCreateDate, true);
			for (TradeFill fill : CollectionUtils.getIterable(fillList)) {
				if (!fillIds.contains(fill.getId())) {
					ReconcileTradeIntradayMatch match = getReconcileTradeIntradayMatch(temp.getReferenceOne().getId());
					if (match != null && match.getTradeFill() == null) {
						match.setTradeFill(fill);
						saveReconcileTradeIntradayMatch(match);
						getReconcileTradeIntradayMatchTempDAO().delete(temp.getId());
						executeTransitionSystemDefinedForReconcileMatch(match, WORKFLOW_STATE_RECONCILE_MATCH_PENDING_REC);
					}
					break;
				}
			}
		}
		else if (CollectionUtils.isEmpty(matchList)) {
			// require only one matching group
			ReconcileTradeMatchType type = getReconcileTradeMatchTypeCache().getReconcileTradeMatchTypeByClientAccount(getReconcileTradeMatchTypeDAO(), trade.getClientInvestmentAccount().getId());
			if (type != null && type.isAutoCreateManualMatch()) {
				if (type.getAutoCreateManualMatchCondition() == null || getSystemConditionEvaluationHandler().isConditionTrue(type.getAutoCreateManualMatchCondition(), trade)) {
					saveReconcileTradeIntradayManualMatchForTrade(trade, type);
				}
			}
		}
	}


	private void executeTransitionSystemDefinedForReconcileMatch(ReconcileTradeIntradayMatch match, String nextStateName) {
		if (match.getWorkflowState() != null) {
			WorkflowState nextState = getWorkflowDefinitionService().getWorkflowStateByName(match.getWorkflowState().getWorkflow().getId(), nextStateName);
			if (nextState == null) {
				throw new ValidationException("Workflow Setup Error: Workflow [" + match.getWorkflowState().getWorkflow().getName() + "] is missing required workflow state [" + nextStateName + "]");
			}
			getWorkflowTransitionService().executeWorkflowTransitionSystemDefined(TABLE_RECONCILE_MATCH, match.getId(), nextState.getId());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconcileTradeIntradayMatch, Criteria> getReconcileTradeIntradayMatchDAO() {
		return this.reconcileTradeIntradayMatchDAO;
	}


	public void setReconcileTradeIntradayMatchDAO(AdvancedUpdatableDAO<ReconcileTradeIntradayMatch, Criteria> reconcileTradeIntradayMatchDAO) {
		this.reconcileTradeIntradayMatchDAO = reconcileTradeIntradayMatchDAO;
	}


	public AdvancedUpdatableDAO<ReconcileTradeMatchType, Criteria> getReconcileTradeMatchTypeDAO() {
		return this.reconcileTradeMatchTypeDAO;
	}


	public void setReconcileTradeMatchTypeDAO(AdvancedUpdatableDAO<ReconcileTradeMatchType, Criteria> reconcileTradeMatchTypeDAO) {
		this.reconcileTradeMatchTypeDAO = reconcileTradeMatchTypeDAO;
	}


	public UpdatableDAO<ReconcileTradeIntradayMatchTemp> getReconcileTradeIntradayMatchTempDAO() {
		return this.reconcileTradeIntradayMatchTempDAO;
	}


	public void setReconcileTradeIntradayMatchTempDAO(UpdatableDAO<ReconcileTradeIntradayMatchTemp> reconcileTradeIntradayMatchTempDAO) {
		this.reconcileTradeIntradayMatchTempDAO = reconcileTradeIntradayMatchTempDAO;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public ReconcileTradeMatchTypeCache getReconcileTradeMatchTypeCache() {
		return this.reconcileTradeMatchTypeCache;
	}


	public void setReconcileTradeMatchTypeCache(ReconcileTradeMatchTypeCache reconcileTradeMatchTypeCache) {
		this.reconcileTradeMatchTypeCache = reconcileTradeMatchTypeCache;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
