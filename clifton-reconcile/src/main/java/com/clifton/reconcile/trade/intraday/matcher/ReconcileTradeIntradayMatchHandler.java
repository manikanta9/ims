package com.clifton.reconcile.trade.intraday.matcher;

import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;


/**
 * The <code>ReconcileTradeIntradayMatchHandler</code> defines an object used to create external to internal trade matches.
 *
 * @author mwacker
 */
public interface ReconcileTradeIntradayMatchHandler {

	public void createTradeMatch(ReconcileTradeIntradayMatchObject beanList);


	public ReconcileTradeMatchType getAutoReconcileTradeMatchType(ReconcileTradeMatchType defaultMatchType);
}
