package com.clifton.reconcile.trade.intraday.search;


import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.BooleanUtils;
import com.clifton.trade.search.TradeSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Component;


/**
 * The <code>ReconcileTradeSearchFormRestrictionConfigurer</code> class allows to define cross-project form restriction
 * for trade reconciliation status.
 *
 * @author vgomelsky
 */
@Component
public class ReconcileTradeSearchFormRestrictionConfigurer implements SearchFormRestrictionConfigurer {


	@Override
	public Class<?> getSearchFormClass() {
		return TradeSearchForm.class;
	}


	@Override
	public String getSearchFieldName() {
		return "reconciled";
	}


	@Override
	public void configureCriteria(Criteria criteria, SearchRestriction restriction) {
		if (restriction.getValue() != null) {
			// if there is a Match record for each fill, then the trade is considered to be reconciled
			// unless it supports workflow (swaps)
			// for cases with workflow, workflow status CANNOT be "Open" for a trade to be considered reconciled
			String sql = "EXISTS (SELECT * FROM TradeFill f " + //
					"LEFT JOIN ReconcileTradeIntradayMatch m ON f.TradeFillID = m.TradeFillID " + //
					"LEFT JOIN WorkflowStatus w ON m.WorkflowStatusID = w.WorkflowStatusID " + //
					"WHERE {alias}.TradeID = f.TradeID AND " + //
					"COALESCE(w.WorkflowStatusName, CASE WHEN m.ReconcileTradeIntradayMatchID IS NULL THEN 'Open' ELSE 'REC' END) = 'Open')";

			if (BooleanUtils.isTrueStrict(restriction.getValue())) {
				sql = "EXISTS (SELECT * FROM TradeFill f WHERE {alias}.TradeID = f.TradeID) AND NOT " + sql;
			}
			criteria.add(Restrictions.sqlRestriction(sql));
		}
	}
}
