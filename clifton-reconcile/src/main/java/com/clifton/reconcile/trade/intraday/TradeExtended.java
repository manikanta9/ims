package com.clifton.reconcile.trade.intraday;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchTemp;
import com.clifton.system.note.SystemNote;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.definition.WorkflowStatus;

import java.math.BigDecimal;


/**
 * The <code>TradeExtended</code> is a virtual object that holds
 * a {@link Trade}, {@link TradeFill}, {@link ReconcileTradeIntradayMatch} record for every Fill, or if there are no fills, then Trades that have been unbooked and there exists
 * a match in the {@link ReconcileTradeIntradayMatchTemp} table.
 *
 * @author manderson
 */
@NonPersistentObject
public class TradeExtended extends Trade {

	/**
	 * PK field: not used but needed by Hibernate
	 * Can't use TradeID as a Trade could have more than one fill, so multiple
	 * records would then have the same ID
	 */
	private String uuid;

	/**
	 * Don't need the full object, so just setting properties from the match that are needed:
	 */
	private Integer matchId;
	private WorkflowState matchWorkflowState;
	private WorkflowStatus matchWorkflowStatus;
	private boolean confirmed;

	/**
	 * Calculated field in sql select so can easily filter
	 */
	private boolean reconciled;

	/**
	 * Calculated field in sql = TradeFill.Quantity else Trade.QuantityIntended
	 */
	private BigDecimal quantity;

	/**
	 * Calculated field in sql = TradeFill.NotionalUnitPrice else Trade.ExpectedUnitPrice
	 */
	private BigDecimal price;

	/**
	 * Calculated field in sql select
	 */
	private boolean unbooked;

	/**
	 * Last updated note associated with the Trade of Note Type Trade Break or Reconciliation Note
	 */
	private SystemNote reconciliationNote;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getTradeId() {
		return getId();
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	@Override
	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public boolean isUnbooked() {
		return this.unbooked;
	}


	public void setUnbooked(boolean unbooked) {
		this.unbooked = unbooked;
	}


	public Integer getMatchId() {
		return this.matchId;
	}


	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}


	public WorkflowState getMatchWorkflowState() {
		return this.matchWorkflowState;
	}


	public void setMatchWorkflowState(WorkflowState matchWorkflowState) {
		this.matchWorkflowState = matchWorkflowState;
	}


	public WorkflowStatus getMatchWorkflowStatus() {
		return this.matchWorkflowStatus;
	}


	public void setMatchWorkflowStatus(WorkflowStatus matchWorkflowStatus) {
		this.matchWorkflowStatus = matchWorkflowStatus;
	}


	public boolean isConfirmed() {
		return this.confirmed;
	}


	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}


	public SystemNote getReconciliationNote() {
		return this.reconciliationNote;
	}


	public void setReconciliationNote(SystemNote reconciliationNote) {
		this.reconciliationNote = reconciliationNote;
	}
}
