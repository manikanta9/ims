package com.clifton.reconcile.trade.intraday.matcher.util;


import java.math.BigDecimal;


public class ExternalFillDetail {

	/**
	 * Manually assigned id used to distinguish between details when creating the trade fills.
	 */
	private String externalId;

	private String account;
	private BigDecimal price;
	private BigDecimal quantity;


	public ExternalFillDetail() {
		//
	}


	public ExternalFillDetail(String externalId, String account, BigDecimal price, BigDecimal quantity) {
		this.externalId = externalId;
		this.account = account;
		this.price = price;
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal shares) {
		this.quantity = shares;
	}


	public String getAccount() {
		return this.account;
	}


	public void setAccount(String account) {
		this.account = account;
	}


	public String getExternalId() {
		return this.externalId;
	}


	public void setExternalId(String externalId) {
		this.externalId = externalId;
	}
}
