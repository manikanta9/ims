package com.clifton.reconcile.trade.intraday.matcher.util;


import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.trade.Trade;

import java.math.BigDecimal;


public class TradeFillResult extends ExternalFillDetail {

	private Trade trade;


	public TradeFillResult(Trade trade, String externalId, BigDecimal shares, BigDecimal price) {
		this.trade = trade;
		setExternalId(externalId);
		setQuantity(shares);
		setPrice(price);
	}


	public Trade getTrade() {
		return this.trade;
	}


	public void setTrade(Trade trade) {
		this.trade = trade;
	}


	@Override
	public String toString() {
		return getTrade().getId() + "\t" + getQuantity() + "\t" + getPrice();
	}


	public String toStringFormatted(boolean includeHeader) {
		String result = "";
		if (includeHeader) {
			result = "TradeID\tShares\tPrice";
		}
		return result + String.format("%d\t%s\t%s\t%s", getTrade().getId(), getExternalId(), CoreMathUtils.formatNumberDecimal(getQuantity()), CoreMathUtils.formatNumberDecimal(getPrice()));
	}
}
