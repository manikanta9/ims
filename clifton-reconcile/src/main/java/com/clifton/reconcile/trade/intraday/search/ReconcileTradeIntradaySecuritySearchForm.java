package com.clifton.reconcile.trade.intraday.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.SearchField;


public class ReconcileTradeIntradaySecuritySearchForm extends ReconcileTradeIntradayAccountSearchForm {

	@SearchField(searchField = "investmentSecurity.id")
	private Integer investmentSecurityId;


	public ReconcileTradeIntradaySecuritySearchForm() {
		//
	}


	public ReconcileTradeIntradaySecuritySearchForm(ReconcileTradeIntradayAccountSearchForm searchForm) {
		BeanUtils.copyProperties(searchForm, this);
	}


	public Integer getInvestmentSecurityId() {
		return this.investmentSecurityId;
	}


	public void setInvestmentSecurityId(Integer investmentSecurityId) {
		this.investmentSecurityId = investmentSecurityId;
	}
}
