package com.clifton.reconcile.trade.intraday;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.search.ReconcileTradeIntradayAccountSearchForm;
import com.clifton.reconcile.trade.intraday.search.ReconcileTradeIntradaySecuritySearchForm;
import com.clifton.reconcile.trade.intraday.search.TradeExtendedSearchForm;
import com.clifton.reconcile.trade.intraday.search.TradeFillExtendedSearchForm;
import com.clifton.trade.TradeFill;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface ReconcileTradeIntradayService {

	//////////////////////////////////////////////////////////////////////////////////////
	////////         ReconcileTradeIntradayAccount Business Methods          /////////////
	//////////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class, namingConventionViolation = true)
	public List<ReconcileTradeIntradayAccount> getReconcileTradeIntradayAccountList(ReconcileTradeIntradayAccountSearchForm searchForm);


	//////////////////////////////////////////////////////////////////////////////////////
	////////         ReconcileTradeIntradaySecurity Business Methods         /////////////
	//////////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public List<ReconcileTradeIntradaySecurity> getReconcileTradeIntradaySecurityList(ReconcileTradeIntradaySecuritySearchForm searchForm);


	////////////////////////////////////////////////////////////////////////////
	///////    TradeExtended and TradeFillExtended Business Methods   //////////
	////////////////////////////////////////////////////////////////////////////


	@RequestMapping("reconcileTradeExtendedList")
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public List<TradeExtended> getTradeExtendedList(TradeExtendedSearchForm searchForm);


	@SecureMethod(dtoClass = TradeFill.class)
	@RequestMapping("reconcileTradeFillExtendedList")
	public List<TradeFillExtended> getTradeFillExtendedList(TradeFillExtendedSearchForm searchForm);
}
