package com.clifton.reconcile.trade.intraday.external;


/**
 * <code>ReconcileTradeIntradayExternalStatuses</code> enumerates statuses that external trades may have.
 * CONFIRMED indicates the trade has been confirmed by the exchange. PENDING_BROKER_ACCEPTANCE indicates
 * the trade has been acknowledged by the exchange, but is awaiting confirmation by the clearing broker.
 * UNCONFIRMED indicates the trade has not yet been acknowledged by the exchange.
 *
 * @author rbrooks
 */
public enum ReconcileTradeIntradayExternalStatuses {
	CONFIRMED, PENDING_BROKER_ACCEPTANCE, UNCONFIRMED
}
