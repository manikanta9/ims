package com.clifton.reconcile.trade.intraday.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.trade.search.TradeSearchForm;

import java.math.BigDecimal;


public class TradeExtendedSearchForm extends TradeSearchForm {

	@SearchField(searchField = "name", searchFieldPath = "matchWorkflowState")
	private String matchWorkflowStateName;

	@SearchField(searchField = "name", searchFieldPath = "matchWorkflowStatus")
	private String matchWorkflowStatusName;

	@SearchField
	private Integer matchId;

	@SearchField
	private Boolean reconciled;

	@SearchField
	private Boolean unbooked;

	@SearchField
	private BigDecimal price;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private Boolean confirmed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public String getMatchWorkflowStateName() {
		return this.matchWorkflowStateName;
	}


	public void setMatchWorkflowStateName(String matchWorkflowStateName) {
		this.matchWorkflowStateName = matchWorkflowStateName;
	}


	public String getMatchWorkflowStatusName() {
		return this.matchWorkflowStatusName;
	}


	public void setMatchWorkflowStatusName(String matchWorkflowStatusName) {
		this.matchWorkflowStatusName = matchWorkflowStatusName;
	}


	public Boolean getConfirmed() {
		return this.confirmed;
	}


	public void setConfirmed(Boolean confirmed) {
		this.confirmed = confirmed;
	}


	public Boolean getUnbooked() {
		return this.unbooked;
	}


	public void setUnbooked(Boolean unbooked) {
		this.unbooked = unbooked;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public Integer getMatchId() {
		return this.matchId;
	}


	public void setMatchId(Integer matchId) {
		this.matchId = matchId;
	}
}
