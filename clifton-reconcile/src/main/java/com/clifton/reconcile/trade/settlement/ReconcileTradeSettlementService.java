package com.clifton.reconcile.trade.settlement;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.trade.Trade;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


public interface ReconcileTradeSettlementService {

	@RequestMapping("reconcileTradeSettlementList")
	@SecureMethod(dtoClass = Trade.class)
	public List<TradeSettlement> getTradeSettlementList(TradeSettlementSearchForm searchForm);
}
