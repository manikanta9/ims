package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.CollectionUtils;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>ReconcileTradeIntradayMatchRemoverForTradeFillObserver</code> manages
 * matches that are linked to a Trade Fill that is deleted.
 * <p/>
 * When Workflow is Used:
 * <p/>
 * Option 1: For Single Fill Trades will create a temp link in Match Temp Table
 * Option 2: For Multiple Fill Eligible Trades, but single filled will create a temp link in Match Temp Table
 * Option 3: For Multiple Fills - will delete the match
 *
 * @author manderson
 */
@Component
public class ReconcileTradeIntradayMatchRemoverForTradeFillObserver extends SelfRegisteringDaoObserver<TradeFill> {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;
	private TradeService tradeService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.DELETE;
	}


	@Override
	protected void beforeMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<TradeFill> dao, DaoEventTypes event, TradeFill bean) {
		// Only Registered Event, but extra check
		if (event.isDelete()) {
			List<ReconcileTradeIntradayMatch> matchList = getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByFill(bean.getId());
			if (!CollectionUtils.isEmpty(matchList)) {
				if (CollectionUtils.getSize(matchList) == 1) {
					ReconcileTradeIntradayMatch match = matchList.get(0);
					// Single fill - can just link the trade
					if (bean.getTrade().getTradeType().isSingleFillTrade()) {
						getReconcileTradeIntradayMatchService().linkReconcileTradeIntradayMatchTemp(match, bean.getTrade());
					}
					else {
						List<TradeFill> fillList = getTradeService().getTradeFillListByTrade(bean.getTrade().getId());
						// Multiple Fill Eligible - but Single Fill - link the trade
						if (CollectionUtils.getSize(fillList) == 1) {
							getReconcileTradeIntradayMatchService().linkReconcileTradeIntradayMatchTemp(match, bean.getTrade());
						}
						// Multiple Fill - delete the match - can't track at Trade level
						else {
							getReconcileTradeIntradayMatchService().deleteReconcileTradeIntradayMatch(match.getId());
						}
					}
				}
				// Multiple match records for the fill (external trades) - delete the matches
				else {
					getReconcileTradeIntradayMatchService().deleteReconcileTradeIntradayMatchList(matchList);
				}
			}
		}
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
