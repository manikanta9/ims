package com.clifton.reconcile.trade.intraday;


import com.clifton.investment.instrument.InvestmentSecurity;


/**
 * The <code>ReconcileTradeIntradaySecurity</code> class represents security level summary of reconciliation data
 * (grouped by security, client account, holding account and date).
 * <p/>
 * NOTE: this object is built dynamically in code and doesn't have a corresponding database table
 *
 * @author mwacker
 */
public class ReconcileTradeIntradaySecurity extends ReconcileTradeIntradayAccount {

	private InvestmentSecurity investmentSecurity;

	private String uuid; // PK field: not used but needed by Hibernate


	@Override
	public String getLabel() {
		return super.getLabel() + (getInvestmentSecurity() != null ? " [" + getInvestmentSecurity().getLabel() + "]" : "");
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public String getUuid() {
		return this.uuid;
	}


	public void setUuid(String uuid) {
		this.uuid = uuid;
	}
}
