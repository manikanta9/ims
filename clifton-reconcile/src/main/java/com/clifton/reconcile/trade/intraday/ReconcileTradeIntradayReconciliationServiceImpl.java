package com.clifton.reconcile.trade.intraday;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.account.InvestmentAccountService;
import com.clifton.investment.account.InvestmentAccountType;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalService;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalStatuses;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalGroupingSearchForm;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatchHandler;
import com.clifton.reconcile.trade.intraday.search.ReconcileTradeIntradaySecuritySearchForm;
import com.clifton.reconcile.trade.intraday.search.TradeFillExtendedSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.trade.search.TradeFillSearchForm;
import com.clifton.workflow.definition.Workflow;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class ReconcileTradeIntradayReconciliationServiceImpl implements ReconcileTradeIntradayReconciliationService {

	private InvestmentAccountService investmentAccountService;
	private ReconcileTradeIntradayService reconcileTradeIntradayService;
	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;
	private ReconcileTradeIntradayExternalService reconcileTradeIntradayExternalService;
	private ReconcileTradeIntradayMatchHandler reconcileTradeIntradayMatchHandler;
	private SynchronizationHandler synchronizationHandler;
	private TradeService tradeService;
	private WorkflowDefinitionService workflowDefinitionService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void autoReconcileTradeIntraday(Date date, boolean reconcileUnconfirmed) {
		autoReconcileTradeIntradayByHoldingAccount(date, reconcileUnconfirmed, null);
	}


	@Override
	public void autoReconcileTradeIntradayByHoldingAccount(Date date, boolean reconcileUnconfirmed, Integer holdingInvestmentAccountId) {
		if (reconcileUnconfirmed && (DateUtils.getMillisecondsFromMidnight(new Date()) < 15 * 60 * 60 * 1000) && !date.before(DateUtils.clearTime(new Date()))) {
			reconcileUnconfirmed = false;
		}
		ReconcileTradeIntradayExternalGroupingSearchForm searchForm = new ReconcileTradeIntradayExternalGroupingSearchForm();
		searchForm.setTradeDate(DateUtils.clearTime(date));
		if (holdingInvestmentAccountId != null) {
			searchForm.setHoldingInvestmentAccountId(holdingInvestmentAccountId);
		}
		searchForm.setReconciled(false);
		List<ReconcileTradeIntradayExternalGrouping> externalTradeGroupingList = getReconcileTradeIntradayExternalService().getReconcileTradeExternalGroupingListInternal(searchForm, false);

		ReconcileTradeMatchType matchType = getReconcileTradeIntradayMatchHandler().getAutoReconcileTradeMatchType(null);
		Workflow workflow = getWorkflowDefinitionService().getWorkflowByName(TradeService.TRADE_WORKFLOW_NAME);
		WorkflowState bookedWorkflowState = getWorkflowDefinitionService().getWorkflowStateByName(workflow.getId(), TradeService.TRADE_BOOKED_STATE_NAME);
		WorkflowState executedValidWorkflowState = getWorkflowDefinitionService().getWorkflowStateByName(workflow.getId(), TradeService.TRADE_EXECUTED_VALID_STATE_NAME);

		Short[] workFlowStateIdList = new Short[2];
		workFlowStateIdList[0] = bookedWorkflowState.getId();
		workFlowStateIdList[1] = executedValidWorkflowState.getId();

		StringBuilder errors = null;
		Throwable firstCause = null;
		for (ReconcileTradeIntradayExternalGrouping externalTradeGrouping : CollectionUtils.getIterable(externalTradeGroupingList)) {
			if (reconcileUnconfirmed || (externalTradeGrouping.getStatus() == ReconcileTradeIntradayExternalStatuses.CONFIRMED)) {
				try {
					processReconcileTradeIntradayExternalGrouping(externalTradeGrouping, workFlowStateIdList, matchType);
				}
				catch (Throwable e) {
					LogUtils.error(getClass(), "Failed to auto reconcile.", e);

					// full log for first error and only causes for consecutive errors
					boolean firstError = false;
					if (errors == null) {
						firstCause = e;
						firstError = true;
						errors = new StringBuilder(1024);
					}
					else {
						errors.append("\n\n");
					}
					errors.append("Failed to processes external trade group with holding account [").append(externalTradeGrouping.getHoldingInvestmentAccountNumber()).append("], security [").append(externalTradeGrouping.getInvestmentSecurity().getSymbol()).append("], price [").append(externalTradeGrouping.getPrice()).append("], quantity [").append(externalTradeGrouping.getQuantity()).append("], and effective date [").append(DateUtils.fromDate(date)).append("]");
					if (!firstError) {
						errors.append("\nCAUSED BY ");
						errors.append(ExceptionUtils.getDetailedMessage(e));
					}
				}
			}
		}
		if (errors != null) {
			throw new RuntimeException(errors.toString(), firstCause);
		}
	}


	@Transactional(timeout = 120)
	@Override
	public Status reconcileMatchMultiFillTrades(String[] investmentAccountTypeNames, Date date) {
		String lockMessage = "Performing reconcile matching on Multi-Fill trades.";
		String lockKey = "RECONCILE_MATCH_MULTI_FILL_TRADES_KEY";
		return getSynchronizationHandler().call(
				SynchronizableBuilder.forLocking("RECONCILE_MATCH_MULTI_FILL_TRADES", lockMessage, lockKey)
						.buildWithCallableAction(() -> {
							Date tradeDate = DateUtils.clearTime(date != null ? date : new Date());
							int processedCount = 0;
							int reconciledCount = 0;
							int skippedCount = 0;
							Status status = new Status();

							ReconcileTradeIntradaySecuritySearchForm reconcileTradeIntradaySecuritySearchForm = new ReconcileTradeIntradaySecuritySearchForm();
							reconcileTradeIntradaySecuritySearchForm.setTradeDate(tradeDate);
							reconcileTradeIntradaySecuritySearchForm.setReconciled(false);
							if (investmentAccountTypeNames != null && investmentAccountTypeNames.length > 0) {
								Short[] investmentAccountTypeIds = ArrayUtils.getStream(investmentAccountTypeNames)
										.map(getInvestmentAccountService()::getInvestmentAccountTypeByName)
										.map(InvestmentAccountType::getId)
										.toArray(Short[]::new);
								reconcileTradeIntradaySecuritySearchForm.setHoldingInvestmentAccountTypeIds(investmentAccountTypeIds);
							}
							List<ReconcileTradeIntradaySecurity> reconcileTradeIntradaySecurityList = getReconcileTradeIntradayService().getReconcileTradeIntradaySecurityList(reconcileTradeIntradaySecuritySearchForm);

							List<Integer> holdingAccountIdList = new ArrayList<>();
							List<Integer> investmentSecurityIdList = new ArrayList<>();

							Map<String, List<ReconcileTradeIntradayExternalGrouping>> reconcileTradeIntradayExternalGroupingMap = new HashMap<>();
							Map<String, List<TradeFillExtended>> tradeFillExtendedListMap = new HashMap<>();

							// Set up lists of holding accounts and securities for bulk query
							for (ReconcileTradeIntradaySecurity reconcileTradeIntradaySecurity : CollectionUtils.getIterable(reconcileTradeIntradaySecurityList)) {
								// operate only on non-reconciled entries that have have multiple internal trade fills to match an single external trade or a  single internal trade fill with multiple external trades
								if (!reconcileTradeIntradaySecurity.isReconciled() && ((reconcileTradeIntradaySecurity.getUnreconciledTradeCount() > 1 && reconcileTradeIntradaySecurity.getExternalUnreconciledTradeCount() == 1) ||
										(reconcileTradeIntradaySecurity.getUnreconciledTradeCount() == 1 && reconcileTradeIntradaySecurity.getExternalUnreconciledTradeCount() > 1))) {
									holdingAccountIdList.add(reconcileTradeIntradaySecurity.getHoldingInvestmentAccount().getId());
									investmentSecurityIdList.add(reconcileTradeIntradaySecurity.getInvestmentSecurity().getId());
								}
							}

							if (!holdingAccountIdList.isEmpty()) {
								ReconcileTradeIntradayExternalGroupingSearchForm externalGroupSearchForm = new ReconcileTradeIntradayExternalGroupingSearchForm();
								externalGroupSearchForm.setHoldingInvestmentAccountIds(CollectionUtils.toArray(holdingAccountIdList, Integer.class));
								externalGroupSearchForm.setInvestmentSecurityIdList(CollectionUtils.toArray(investmentSecurityIdList, Integer.class));
								externalGroupSearchForm.setTradeDate(tradeDate);
								externalGroupSearchForm.setReconciled(false);
								List<ReconcileTradeIntradayExternalGrouping> externalTradeGroupingList = getReconcileTradeIntradayExternalService().getReconcileTradeExternalGroupingList(externalGroupSearchForm);

								for (ReconcileTradeIntradayExternalGrouping entry : CollectionUtils.getIterable(externalTradeGroupingList)) {
									String key = getReconcileMatchKey(entry.getHoldingInvestmentAccount().getId(), entry.getInvestmentSecurity().getId(), entry.getExecutingBrokerCompany().getId(), entry.isBuy());
									reconcileTradeIntradayExternalGroupingMap.computeIfAbsent(key, k -> new ArrayList<>()).add(entry);
								}

								final String[] workflowStateNames = {TradeService.TRADE_BOOKED_STATE_NAME, TradeService.TRADE_EXECUTED_VALID_STATE_NAME};
								TradeFillExtendedSearchForm tradeFillExtendedSearchForm = new TradeFillExtendedSearchForm();
								tradeFillExtendedSearchForm.setHoldingInvestmentAccountIds(CollectionUtils.toArray(holdingAccountIdList, Integer.class));
								tradeFillExtendedSearchForm.setSecurityIds(CollectionUtils.toArray(investmentSecurityIdList, Integer.class));
								tradeFillExtendedSearchForm.setTradeDate(tradeDate);
								tradeFillExtendedSearchForm.setReconciled(false);
								tradeFillExtendedSearchForm.setWorkflowStateNames(workflowStateNames);
								List<TradeFillExtended> tradeFillExtendedList = getReconcileTradeIntradayService().getTradeFillExtendedList(tradeFillExtendedSearchForm);

								for (TradeFillExtended entry : CollectionUtils.getIterable(tradeFillExtendedList)) {
									String key = getReconcileMatchKey(entry.getHoldingInvestmentAccount().getId(), entry.getInvestmentSecurity().getId(), entry.getTrade().getExecutingBrokerCompany().getId(), entry.isBuy());
									tradeFillExtendedListMap.computeIfAbsent(key, k -> new ArrayList<>()).add(entry);
								}

								for (Map.Entry<String, List<ReconcileTradeIntradayExternalGrouping>> mapEntry : CollectionUtils.getIterable(reconcileTradeIntradayExternalGroupingMap.entrySet())) {
									InvestmentAccount currentHoldingAccount = null;
									try {
										externalTradeGroupingList = mapEntry.getValue();
										List<TradeFillExtended> tradeFillListExtended = tradeFillExtendedListMap.get(mapEntry.getKey());

										++processedCount;
										if (!CollectionUtils.isEmpty(tradeFillListExtended) && !CollectionUtils.isEmpty(externalTradeGroupingList)) {
											currentHoldingAccount = externalTradeGroupingList.get(0).getHoldingInvestmentAccount();
											ReconcileTradeIntradayMatchObject matchObject = new ReconcileTradeIntradayMatchObject();
											matchObject.setAveragePriceMatch(true);
											matchObject.setTradeFillList(new ArrayList<>(tradeFillListExtended));
											matchObject.setExternalGroupingList(externalTradeGroupingList);
											validatedTradeMatch(matchObject);
											getReconcileTradeIntradayMatchHandler().createTradeMatch(matchObject);
											++reconciledCount;
										}
										else {
											++skippedCount;
										}
									}
									catch (Exception exc) {
										String currentHoldingAccountLabel = currentHoldingAccount != null ? currentHoldingAccount.getLabel() : "unknown";
										String augmentedMessage = "Error reconciling holding account: " + currentHoldingAccountLabel + ". " + ExceptionUtils.getOriginalException(exc);
										status.addError(augmentedMessage);
									}
								}
							}

							status.setMessage(processedCount + " entries processed with " + reconciledCount + " entries successfully reconciled. " + skippedCount + " entries were skipped.");
							return status;
						}));
	}


	@Override
	@Transactional(timeout = 180)
	public void saveReconcileTradeIntradayMatchObject(ReconcileTradeIntradayMatchObject beanList) {
		populateReconcileTradeIntradayMatchObject(beanList);
		validatedTradeMatch(beanList);
		getReconcileTradeIntradayMatchHandler().createTradeMatch(beanList);
	}


	@Override
	public void deleteReconcileTradeIntradayMatchObject(ReconcileTradeIntradayMatchObject beanList) {
		populateReconcileTradeIntradayMatchObject(beanList);

		List<ReconcileTradeIntradayMatch> matchList = new ArrayList<>();

		// get all the matches using the external trade list
		for (ReconcileTradeIntradayExternalGrouping grouping : CollectionUtils.getIterable(beanList.getExternalGroupingList())) {
			for (ReconcileTradeIntradayMatch match : CollectionUtils.getIterable(grouping.getMatchList())) {
				if (!matchList.contains(match)) {
					matchList.add(match);
				}
			}
		}

		// get all the matches using the internal trade list
		for (TradeFill fill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
			for (ReconcileTradeIntradayMatch match : CollectionUtils.getIterable(getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByFill(fill.getId()))) {
				if (!matchList.contains(match)) {
					matchList.add(match);
				}
			}
		}

		getReconcileTradeIntradayMatchService().deleteReconcileTradeIntradayMatchList(matchList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Function used to generate keys for use in Maps within the reconcileMatchMultiFillTrades method.
	 */
	private String getReconcileMatchKey(int investmentAccountId, int investmentSecurityId, int executingBrokerCompanyId, boolean isBuy) {
		return investmentAccountId + ":" + investmentSecurityId + ":" + executingBrokerCompanyId + ":" + (isBuy ? "BUY" : "SELL");
	}


	private void validatedTradeMatch(ReconcileTradeIntradayMatchObject beanList) {
		boolean fillListEmpty = CollectionUtils.isEmpty(beanList.getTradeFillList());
		boolean externalGroupingListEmpty = CollectionUtils.isEmpty(beanList.getExternalGroupingList());
		// must have at least 1 fill to match
		if (!beanList.isManualMatch()) {
			ValidationUtils.assertTrue(!fillListEmpty, "No internal trades were selected.");
		}
		else {
			ValidationUtils.assertTrue(!fillListEmpty || !externalGroupingListEmpty, "No internal or external trades were selected.");
			if (fillListEmpty) {
				ValidationUtils.assertNotNull(beanList.getNote(), "Note is required when matching without an internal trade fill.");
			}
		}
		// must use manual matching when match without an external trade.
		if (!beanList.isManualMatch()) {
			ValidationUtils.assertTrue(!externalGroupingListEmpty, "To match a trade without an external trade, you must use manual match.");
		}
		// if matching to an external trade, the quantities must match
		if (!beanList.isManualMatch() || (!externalGroupingListEmpty && !fillListEmpty)) {
			BigDecimal totalFill = CoreMathUtils.sumProperty(beanList.getTradeFillList(), TradeFill::getQuantity);
			BigDecimal totalExternalQuantity = CoreMathUtils.sumProperty(beanList.getExternalGroupingList(), ReconcileTradeIntradayExternalGrouping::getQuantity);
			ValidationUtils.assertTrue(totalFill.compareTo(totalExternalQuantity) == 0, "Fill quantity [" + totalFill + "] does not match external quantity [" + totalExternalQuantity + "].");
		}

		if (!beanList.isManualMatch() || (!fillListEmpty)) {
			Trade trade = beanList.getTradeFillList().get(0).getTrade();
			// check that the trade is booked.
			ValidationUtils.assertTrue(
					TradeService.TRADE_BOOKED_STATE_NAME.equals(trade.getWorkflowState().getName()) || TradeService.TRADE_EXECUTED_VALID_STATE_NAME.equals(trade.getWorkflowState().getName()),
					"Cannot match trades in the [" + trade.getWorkflowState().getName() + "] state.  Trade must be in the [" + TradeService.TRADE_BOOKED_STATE_NAME + "] state,");
			// check that the trade and the external trades are in the same direction.
			for (ReconcileTradeIntradayExternalGrouping externalTrade : CollectionUtils.getIterable(beanList.getExternalGroupingList())) {
				ValidationUtils.assertTrue(trade.isBuy() == externalTrade.isBuy(), "Cannot match trade fills that are in opposite directions.");
			}
		}
		if (beanList.isManualMatch()) {
			ValidationUtils.assertFalse(beanList.isMatchBrokerPrice(), "Cannot match to broker price when doing a manual match.");
		}
	}


	private void populateReconcileTradeIntradayMatchObject(ReconcileTradeIntradayMatchObject beanList) {
		if (beanList.getTradeFillList() == null) {
			beanList.setTradeFillList(new ArrayList<>());
		}

		// get id property for each external grouping that was passed in
		Integer[] idList = BeanUtils.getBeanIdentityArray(beanList.getExternalGroupingList(), Integer.class);
		if (idList.length > 0) {
			Integer[] externalIdList = new Integer[idList.length];
			System.arraycopy(idList, 0, externalIdList, 0, idList.length);

			// Initialize each grouping and add underlying external trades to tradeList
			ReconcileTradeIntradayExternalGroupingSearchForm searchForm = new ReconcileTradeIntradayExternalGroupingSearchForm();
			searchForm.setIdList(externalIdList);
			List<ReconcileTradeIntradayExternalGrouping> groupingList = getReconcileTradeIntradayExternalService().getReconcileTradeExternalGroupingList(searchForm);
			beanList.setExternalGroupingList(groupingList);
		}
		idList = BeanUtils.getBeanIdentityArray(beanList.getTradeFillList(), Integer.class);
		if (idList.length > 0) {
			Integer[] fillIdList = new Integer[idList.length];
			System.arraycopy(idList, 0, fillIdList, 0, idList.length);

			TradeFillSearchForm sf = new TradeFillSearchForm();
			sf.setIdList(fillIdList);
			List<TradeFill> newTradeFillList = getTradeService().getTradeFillList(sf);
			beanList.setTradeFillList(newTradeFillList);
		}

		// avoid attempt to create a contact if not set
		if (beanList.getContact() != null && beanList.getContact().getIdentity() == null) {
			beanList.setContact(null);
		}
	}


	private TradeFill processReconcileTradeIntradayExternalGrouping(ReconcileTradeIntradayExternalGrouping grouping, Short[] workflowStateIdList, ReconcileTradeMatchType matchType) {
		if (grouping.getHoldingInvestmentAccount() != null) {
			// find an unmatched trade for the grouped external trade
			TradeFill tradeFill = findTradeFill(grouping, workflowStateIdList);
			if (tradeFill != null) {
				// Get list of individual trades for this grouping
				List<ReconcileTradeIntradayExternal> externalTradeList = getReconcileTradeIntradayExternalService().getReconcileTradeExternalListForGroup(grouping);

				for (ReconcileTradeIntradayExternal trade : CollectionUtils.getIterable(externalTradeList)) {
					ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
					match.setTradeFill(tradeFill);
					match.setReconcileTradeIntradayExternal(trade);
					match.setMatchType(matchType);
					match.setConfirmed(matchType.isAutoConfirm());
					getReconcileTradeIntradayMatchService().saveReconcileTradeIntradayMatch(match);
				}
			}
			return tradeFill;
		}
		return null;
	}


	private TradeFillExtended findTradeFill(ReconcileTradeIntradayExternalGrouping tradeGroup, Short[] workflowStateIdList) {
		if (tradeGroup.getInvestmentSecurity() == null || tradeGroup.getPrice() == null || tradeGroup.getExecutingBrokerCompany() == null) {
			return null; // cannot rec with these fields missing
		}
		TradeFillExtendedSearchForm searchForm = new TradeFillExtendedSearchForm();
		searchForm.setHoldingInvestmentAccountId(tradeGroup.getHoldingInvestmentAccount().getId());
		searchForm.setSecurityId(tradeGroup.getInvestmentSecurity().getId());
		searchForm.setQuantity(tradeGroup.getQuantity());
		searchForm.setBuy(tradeGroup.isBuy());
		searchForm.setNotionalUnitPrice(tradeGroup.getPrice());
		searchForm.setTradeDate(tradeGroup.getTradeDate());
		searchForm.setExecutingBrokerCompanyId(tradeGroup.getExecutingBrokerCompany().getId());
		searchForm.setWorkflowStateIds(workflowStateIdList);
		searchForm.setReconciled(false);
		List<TradeFillExtended> result = getReconcileTradeIntradayService().getTradeFillExtendedList(searchForm);
		if (result.size() != 1) {
			return null;
		}
		return result.get(0);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccountService getInvestmentAccountService() {
		return this.investmentAccountService;
	}


	public void setInvestmentAccountService(InvestmentAccountService investmentAccountService) {
		this.investmentAccountService = investmentAccountService;
	}


	public ReconcileTradeIntradayExternalService getReconcileTradeIntradayExternalService() {
		return this.reconcileTradeIntradayExternalService;
	}


	public void setReconcileTradeIntradayExternalService(ReconcileTradeIntradayExternalService reconcileTradeIntradayExternalService) {
		this.reconcileTradeIntradayExternalService = reconcileTradeIntradayExternalService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public ReconcileTradeIntradayMatchHandler getReconcileTradeIntradayMatchHandler() {
		return this.reconcileTradeIntradayMatchHandler;
	}


	public void setReconcileTradeIntradayMatchHandler(ReconcileTradeIntradayMatchHandler reconcileTradeIntradayMatchHandler) {
		this.reconcileTradeIntradayMatchHandler = reconcileTradeIntradayMatchHandler;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}


	public ReconcileTradeIntradayService getReconcileTradeIntradayService() {
		return this.reconcileTradeIntradayService;
	}


	public void setReconcileTradeIntradayService(ReconcileTradeIntradayService reconcileTradeIntradayService) {
		this.reconcileTradeIntradayService = reconcileTradeIntradayService;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}
}
