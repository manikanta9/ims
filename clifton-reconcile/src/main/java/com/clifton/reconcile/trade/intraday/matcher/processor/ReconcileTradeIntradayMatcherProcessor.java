package com.clifton.reconcile.trade.intraday.matcher.processor;


import com.clifton.core.comparison.Ordered;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatcherResult;


/**
 * The <code>ReconcileTradeIntradayMatcherProcessor</code> processes the trade match.  Adds all the
 * trades to be booked/unbooked, fills to be deleted, fills to be created and matches to create to
 * the <code>ReconcileTradeIntradayMatcherResult</code> object.
 *
 * @author mwacker
 */
public interface ReconcileTradeIntradayMatcherProcessor extends Ordered {

	/**
	 * Determines if the match processor can be used.
	 *
	 * @param beanList
	 */
	public boolean isMatcherProcessorUsable(ReconcileTradeIntradayMatchObject beanList);


	public void processTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType);
}
