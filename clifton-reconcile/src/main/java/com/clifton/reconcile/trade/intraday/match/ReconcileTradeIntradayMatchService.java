package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeIntradayMatchSearchForm;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeMatchTypeSearchForm;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.workflow.definition.Workflow;

import java.util.List;


public interface ReconcileTradeIntradayMatchService {

	//////////////////////////////////////////////////////////////////////////////////
	////////        ReconcileTradeIntradayMatch Business Methods         /////////////
	//////////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeIntradayMatch getReconcileTradeIntradayMatch(int id);


	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchByFill(int tradeFillId);


	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchByExternalTrade(int reconcileTradeIntradayExternalId);


	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchByTrade(int tradeId);


	public List<ReconcileTradeIntradayMatch> getReconcileTradeIntradayMatchList(ReconcileTradeIntradayMatchSearchForm searchForm);


	/**
	 * Creates a new manual match record for the fill(s) on the trade as long as there is only one manual match type that fits
	 * for the security
	 *
	 * @param tradeId
	 */
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public void saveReconcileTradeIntradayManualMatchForTrade(int tradeId);


	public ReconcileTradeIntradayMatch saveReconcileTradeIntradayMatch(ReconcileTradeIntradayMatch bean);


	public void saveReconcileTradeIntradayMatchList(List<ReconcileTradeIntradayMatch> matchList);


	public void deleteReconcileTradeIntradayMatch(int id);


	@DoNotAddRequestMapping
	public void deleteReconcileTradeIntradayMatchList(List<ReconcileTradeIntradayMatch> deleteList);


	//////////////////////////////////////////////////////////////////////////////////
	////////        ReconcileTradeMatchType Business Methods             /////////////
	//////////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeMatchType getReconcileTradeMatchType(short id);


	public ReconcileTradeMatchType getReconcileTradeMatchTypeByName(String name);


	public List<ReconcileTradeMatchType> getReconcileTradeMatchTypeList(ReconcileTradeMatchTypeSearchForm searchForm);


	public ReconcileTradeMatchType saveReconcileTradeMatchType(ReconcileTradeMatchType bean);


	public void deleteReconcileTradeMatchType(short id);


	//////////////////////////////////////////////////////////////////////////////////
	///////        ReconcileTradeIntradayMatchTemp Business Methods         //////////
	//////////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public void linkReconcileTradeIntradayMatchTemp(ReconcileTradeIntradayMatch match, Trade trade);


	//////////////////////////////////////////////////////////////////////////////////
	//////     ReconcileTradeIntradayMatch Trade Booking/Unbooking Methods      //////
	//////        Inserts/Deletes Records in ReconcileTradeIntradayMatchTemp    //////
	//////        Updates TradeFillID on ReconcileTradeIntradayMatch            //////
	//////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Trade for the {@link ReconcileTradeIntradayMatch} object either from the
	 * {@link TradeFill} or {@link ReconcileTradeIntradayMatchTemp} related object
	 * <p/>
	 * Pulls the trade the database again to ensure using the most recent information
	 *
	 * @param match
	 */
	@DoNotAddRequestMapping
	public Trade getTradeForReconcileTradeIntradayMatch(ReconcileTradeIntradayMatch match);


	/**
	 * If Trade is a Single Fill Trade and there is a {@link ReconcileTradeIntradayMatch} record for the TradeFill
	 * that supports a {@link Workflow}:
	 * 1. Creates a {@link ReconcileTradeIntradayMatchTemp} row linking the Trade (Note: This links the Trade, not the TradeFill) to the {@link ReconcileTradeIntradayMatch}
	 * 2. Removes TradeFillID from the {@link ReconcileTradeIntradayMatch} object
	 *
	 * @param trade
	 */
	@DoNotAddRequestMapping
	public void processReconcileTradeIntradayMatchUnbookTrade(Trade trade);


	/**
	 * If Trade is a Single Fill Trade and there is a {@link ReconcileTradeIntradayMatchTemp} record for the Trade
	 * 1. Re-associates the TradeFill for the Trade to the previously used {@link ReconcileTradeIntradayMatch} record
	 * 2. Then deletes the {@link ReconcileTradeIntradayMatchTemp} row
	 *
	 * @param trade
	 */
	@DoNotAddRequestMapping
	public void processReconcileTradeIntradayMatchBookTrade(Trade trade);
}
