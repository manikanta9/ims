package com.clifton.reconcile.trade.intraday;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.account.InvestmentAccount;

import java.util.Date;


/**
 * The <code>ReconcileTradeIntradayAccount</code> class represents client/holding accounts level summary of reconciliation data
 * (sums all security level details).
 * <p/>
 * NOTE: this object is built dynamically in code and doesn't have a corresponding database table
 *
 * @author mwacker
 */
public class ReconcileTradeIntradayAccount extends BaseEntity<Integer> implements LabeledObject {

	private Date tradeDate;
	private InvestmentAccount holdingInvestmentAccount;

	private boolean reconciled;
	private int tradeCount;
	private int externalTradeCount;
	private int unreconciledTradeCount;
	private int externalUnreconciledTradeCount;


	@Override
	public String getLabel() {
		String lbl = "Unknown Holding Account";
		if (getHoldingInvestmentAccount() != null) {
			lbl = getHoldingInvestmentAccount().getLabel();
		}
		return lbl + " for " + DateUtils.fromDate(getTradeDate(), DateUtils.DATE_FORMAT_INPUT);
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}


	public int getTradeCount() {
		return this.tradeCount;
	}


	public void setTradeCount(int tradeCount) {
		this.tradeCount = tradeCount;
	}


	public int getExternalTradeCount() {
		return this.externalTradeCount;
	}


	public void setExternalTradeCount(int externalTradeCount) {
		this.externalTradeCount = externalTradeCount;
	}


	public int getUnreconciledTradeCount() {
		return this.unreconciledTradeCount;
	}


	public void setUnreconciledTradeCount(int unreconciledTradeCount) {
		this.unreconciledTradeCount = unreconciledTradeCount;
	}


	public int getExternalUnreconciledTradeCount() {
		return this.externalUnreconciledTradeCount;
	}


	public void setExternalUnreconciledTradeCount(int externalUnreconciledTradeCount) {
		this.externalUnreconciledTradeCount = externalUnreconciledTradeCount;
	}
}
