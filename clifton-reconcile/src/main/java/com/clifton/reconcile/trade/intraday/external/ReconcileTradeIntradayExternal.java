package com.clifton.reconcile.trade.intraday.external;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class ReconcileTradeIntradayExternal extends BaseSimpleEntity<Integer> {

	private InvestmentAccount holdingInvestmentAccount;
	private String holdingInvestmentAccountNumber;
	private BusinessCompany executingBrokerCompany;

	private InvestmentSecurity investmentSecurity;
	private InvestmentSecurity payingSecurity;

	private boolean buy;
	private Date tradeDate;
	private BigDecimal quantity;
	private BigDecimal price;
	private BigDecimal originalPrice;
	private ReconcileTradeIntradayExternalStatuses status;

	private String externalUniqueTradeId;

	private BusinessCompany sourceCompany;

	private List<ReconcileTradeIntradayMatch> matchList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InvestmentAccount getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(InvestmentAccount holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public BusinessCompany getExecutingBrokerCompany() {
		return this.executingBrokerCompany;
	}


	public void setExecutingBrokerCompany(BusinessCompany executingBrokerCompany) {
		this.executingBrokerCompany = executingBrokerCompany;
	}


	public InvestmentSecurity getInvestmentSecurity() {
		return this.investmentSecurity;
	}


	public void setInvestmentSecurity(InvestmentSecurity investmentSecurity) {
		this.investmentSecurity = investmentSecurity;
	}


	public InvestmentSecurity getPayingSecurity() {
		return this.payingSecurity;
	}


	public void setPayingSecurity(InvestmentSecurity payingSecurity) {
		this.payingSecurity = payingSecurity;
	}


	public boolean isBuy() {
		return this.buy;
	}


	public void setBuy(boolean buy) {
		this.buy = buy;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getOriginalPrice() {
		return this.originalPrice;
	}


	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}


	public ReconcileTradeIntradayExternalStatuses getStatus() {
		return this.status;
	}


	public void setStatus(ReconcileTradeIntradayExternalStatuses status) {
		this.status = status;
	}


	public String getExternalUniqueTradeId() {
		return this.externalUniqueTradeId;
	}


	public void setExternalUniqueTradeId(String externalUniqueTradeId) {
		this.externalUniqueTradeId = externalUniqueTradeId;
	}


	public String getHoldingInvestmentAccountNumber() {
		return this.holdingInvestmentAccountNumber;
	}


	public void setHoldingInvestmentAccountNumber(String holdingInvestmentAccountNumber) {
		this.holdingInvestmentAccountNumber = holdingInvestmentAccountNumber;
	}


	public BusinessCompany getSourceCompany() {
		return this.sourceCompany;
	}


	public void setSourceCompany(BusinessCompany sourceCompany) {
		this.sourceCompany = sourceCompany;
	}


	public List<ReconcileTradeIntradayMatch> getMatchList() {
		return this.matchList;
	}


	public void setMatchList(List<ReconcileTradeIntradayMatch> matchList) {
		this.matchList = matchList;
	}
}
