package com.clifton.reconcile.trade.intraday.workflow;


import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchTemp;
import com.clifton.trade.Trade;
import com.clifton.workflow.transition.WorkflowTransition;
import com.clifton.workflow.transition.action.WorkflowTransitionActionHandler;


/**
 * The <code>TradeUnbookingWorkflowAction</code> class is a workflow transition action that un-posts and un-books
 * passed trades from the accounting system.
 * <p/>
 * When Trade is unbooked, the reconciliation row will be deleted unless it has a Workflow attached to it (and also would be a single fill trade)
 * Under those cases, a {@link ReconcileTradeIntradayMatchTemp} record is created to store the link from the Trade to the Reconciliation row
 * and the TradeFillID is cleared.  On re-booking, the temp table is then checked to re-attach reconciliation information to the fill and is removed from the temp table
 *
 * @author vgomelsky
 */
public class TradeUnbookingIntradayReconcilationWorkflowAction implements WorkflowTransitionActionHandler<Trade> {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Trade processAction(Trade trade, WorkflowTransition transition) {
		getReconcileTradeIntradayMatchService().processReconcileTradeIntradayMatchUnbookTrade(trade);
		return trade;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
