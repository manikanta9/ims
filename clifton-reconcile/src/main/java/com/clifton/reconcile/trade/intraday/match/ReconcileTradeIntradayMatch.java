package com.clifton.reconcile.trade.intraday.match;


import com.clifton.business.contact.BusinessContact;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.workflow.BaseWorkflowAwareEntity;
import com.clifton.workflow.WorkflowConfig;


/**
 * The <code>ReconcileTradeIntradayMatch</code> class represents a single trade fill match.
 * For every trade fill that was reconciled, there must be a corresponding match record.
 * If the record doesn't exist, then the fill hasn't been matched.
 * <p/>
 * Workflow is optionally populated based on the matchType.workflow
 * and if it's for a single fill trade
 *
 * @author mwacker
 */
@WorkflowConfig(required = false, workflowBeanPropertyName = "matchType.workflow")
public class ReconcileTradeIntradayMatch extends BaseWorkflowAwareEntity<Integer> {

	public static final String UNRECONCILED_STATUS_NAME = "Open";

	private ReconcileTradeMatchType matchType;

	private TradeFill tradeFill;
	/**
	 * Optional external trade that this fill was reconciled to (we don't get external trades from all brokers for all security types)
	 */
	private ReconcileTradeIntradayExternal reconcileTradeIntradayExternal;

	/**
	 * Specifies whether this trade was confirmed. Some trade types (forwards, swaps, etc.) are initially reconciled
	 * against preliminary data and then are confirmed a day or few later against final confirmation.
	 * Once a trade is confirmed, no one should be able to change it.
	 */
	private boolean confirmed;

	// optional details
	private BusinessContact contact;
	private String note;

	// If no fill, but we keep match because of workflow - need access to the Trade
	@NonPersistentField
	private ReconcileTradeIntradayMatchTemp matchTemp;

	/**
	 * Auto populated and synchronized with the Trade Note linked for type "Internal Use Only - Ops"
	 */
	private String internalTradeNote;

	/**
	 * Auto populated and synchronized with the Trade Note linked for type "External Note - Ops"
	 */
	private String externalTradeNote;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getLabelShort() {
		StringBuilder result = new StringBuilder();
		if (getMatchType() != null) {
			result.append(getMatchType().getName());
			result.append(", ");
		}
		result.append(isReconciled() ? "Reconciled" : "Not Reconciled");
		result.append(isConfirmed() ? ", Confirmed" : ", Not Confirmed");
		if (getNote() != null) {
			result.append(", ").append(getNote());
		}
		return result.toString();
	}


	/**
	 * Calculated getter to determine if match has been reconciled
	 * If no workflow is used, then the existence of this match entry implies
	 * it has been reconciled - otherwise when workflow is used
	 * a status of anything except open is considered to be reconciled
	 */
	public boolean isReconciled() {
		if (getWorkflowStatus() != null) {
			return !UNRECONCILED_STATUS_NAME.equals(getWorkflowStatus().getName());
		}
		return true;
	}


	public Trade getTrade() {
		if (getTradeFill() != null) {
			return getTradeFill().getTrade();
		}
		if (getMatchTemp() != null) {
			return getMatchTemp().getReferenceTwo();
		}
		return null;
	}

	////////////////////////////////////////////////////


	public TradeFill getTradeFill() {
		return this.tradeFill;
	}


	public void setTradeFill(TradeFill tradeFill) {
		this.tradeFill = tradeFill;
	}


	public ReconcileTradeIntradayExternal getReconcileTradeIntradayExternal() {
		return this.reconcileTradeIntradayExternal;
	}


	public void setReconcileTradeIntradayExternal(ReconcileTradeIntradayExternal reconcileTradeIntradayExternal) {
		this.reconcileTradeIntradayExternal = reconcileTradeIntradayExternal;
	}


	public ReconcileTradeMatchType getMatchType() {
		return this.matchType;
	}


	public void setMatchType(ReconcileTradeMatchType matchType) {
		this.matchType = matchType;
	}


	public BusinessContact getContact() {
		return this.contact;
	}


	public void setContact(BusinessContact contact) {
		this.contact = contact;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public boolean isConfirmed() {
		return this.confirmed;
	}


	public void setConfirmed(boolean confirmed) {
		this.confirmed = confirmed;
	}


	public String getInternalTradeNote() {
		return this.internalTradeNote;
	}


	public void setInternalTradeNote(String internalTradeNote) {
		this.internalTradeNote = internalTradeNote;
	}


	public String getExternalTradeNote() {
		return this.externalTradeNote;
	}


	public void setExternalTradeNote(String externalTradeNote) {
		this.externalTradeNote = externalTradeNote;
	}


	public ReconcileTradeIntradayMatchTemp getMatchTemp() {
		return this.matchTemp;
	}


	public void setMatchTemp(ReconcileTradeIntradayMatchTemp matchTemp) {
		this.matchTemp = matchTemp;
	}
}
