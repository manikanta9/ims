package com.clifton.reconcile.trade.intraday.match.cache;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleDaoCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeMatchTypeSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>ReconcileTradeMatchTypeCacheImpl</code> class caches ReconcileTradeMatchType id by client account.  Use to speed up the lookup when
 * checking for auto reconciled accounts.
 */
@Component
public class ReconcileTradeMatchTypeCacheImpl extends SelfRegisteringSimpleDaoCache<ReconcileTradeMatchType, Integer, ObjectWrapper<Short>> implements ReconcileTradeMatchTypeCache {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcileTradeMatchType getReconcileTradeMatchTypeByClientAccount(ReadOnlyDAO<ReconcileTradeMatchType> dao, int clientInvestmentAccountId) {
		ObjectWrapper<Short> id = getCacheHandler().get(getCacheName(), clientInvestmentAccountId);
		if (id != null) {
			if (!id.isPresent()) {
				// id is not null, but not present then there is not
				return null;
			}
			ReconcileTradeMatchType bean = dao.findByPrimaryKey(id.getObject());
			// If bean is null, then transaction was rolled back, look it up again and reset the cache
			if (bean != null) {
				return bean;
			}
		}
		ReconcileTradeMatchType bean = lookupBean(dao, clientInvestmentAccountId);
		setBean(clientInvestmentAccountId, bean);
		return bean;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected ReconcileTradeMatchType lookupBean(ReadOnlyDAO<ReconcileTradeMatchType> dao, int clientInvestmentAccountId) {
		ReconcileTradeMatchTypeSearchForm searchForm = new ReconcileTradeMatchTypeSearchForm();
		searchForm.setClientInvestmentAccountId(clientInvestmentAccountId);
		List<ReconcileTradeMatchType> list = getReconcileTradeIntradayMatchService().getReconcileTradeMatchTypeList(searchForm);
		return CollectionUtils.getOnlyElement(list);
	}


	protected void setBean(int clientInvestmentAccountId, ReconcileTradeMatchType bean) {
		ObjectWrapper<Short> id = (bean != null ? new ObjectWrapper<>(bean.getId()) : new ObjectWrapper<>());
		getCacheHandler().put(getCacheName(), clientInvestmentAccountId, id);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
