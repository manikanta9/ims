package com.clifton.reconcile.trade.intraday.matcher;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.matching.AbstractManyToManyMatcher;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchSources;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeMatchTypeSearchForm;
import com.clifton.reconcile.trade.intraday.matcher.processor.ReconcileTradeIntradayMatcherProcessor;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.trade.TradeService;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.definition.WorkflowState;
import com.clifton.workflow.transition.WorkflowTransitionService;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>ReconcileTradeIntradayMatchHandlerImpl</code> create the trades, fills and matches for a match object.
 *
 * @author mwacker
 */
public class ReconcileTradeIntradayMatchHandlerImpl implements ReconcileTradeIntradayMatchHandler {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;
	private TradeService tradeService;
	private WorkflowDefinitionService workflowDefinitionService;
	private WorkflowTransitionService workflowTransitionService;

	private List<ReconcileTradeIntradayMatcherProcessor> matcherProcessorList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void createTradeMatch(ReconcileTradeIntradayMatchObject beanList) {
		// compile a list of trades that were unbooked
		ReconcileTradeIntradayMatcherResult result = new ReconcileTradeIntradayMatcherResult();
		// to the many to many matching - only necessary when not using broker prices.
		if (!beanList.isMatchBrokerPrice() && ((CollectionUtils.getSize(beanList.getTradeFillList()) > 1) && (CollectionUtils.getSize(beanList.getExternalGroupingList()) > 1))) {
			BeanUtils.sortWithFunctions(beanList.getTradeFillList(), CollectionUtils.createList(
					TradeFill::getNotionalUnitPrice,
					TradeFill::getQuantity
			), CollectionUtils.createList(true, true));
			BeanUtils.sortWithFunctions(beanList.getExternalGroupingList(), CollectionUtils.createList(
					ReconcileTradeIntradayExternalGrouping::getPrice,
					ReconcileTradeIntradayExternalGrouping::getQuantity
			), CollectionUtils.createList(true, true));
			MatchingResult<TradeFill, ReconcileTradeIntradayExternalGrouping> groupMatch = getMatchList(beanList.getTradeFillList(), beanList.getExternalGroupingList(), false,
					beanList.getAveragePriceThresholdPercent());
			ValidationUtils.assertNotNull(groupMatch, "No matches found.");

			// do the match at the group level
			for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternalGrouping> groupItem : CollectionUtils.getIterable(groupMatch.getItemList())) {
				ReconcileTradeIntradayMatchObject groupMatchObject = new ReconcileTradeIntradayMatchObject();
				BeanUtils.copyProperties(beanList, groupMatchObject);
				groupMatchObject.setTradeFillList(groupItem.getInternalList());
				groupMatchObject.setExternalGroupingList(groupItem.getExternalList());

				// create the matching for each returned group
				doCreateTradeMatch(groupMatchObject, result);
			}
		}
		else {
			doCreateTradeMatch(beanList, result);
		}
		saveMatchItems(beanList, result);
	}


	@Override
	public ReconcileTradeMatchType getAutoReconcileTradeMatchType(ReconcileTradeMatchType defaultMatchType) {
		if (defaultMatchType != null) {
			return getReconcileTradeIntradayMatchService().getReconcileTradeMatchType(defaultMatchType.getId());
		}
		ReconcileTradeMatchTypeSearchForm searchForm = new ReconcileTradeMatchTypeSearchForm();
		searchForm.setSource(ReconcileTradeMatchSources.FILE);
		searchForm.setSystemDefined(true);
		List<ReconcileTradeMatchType> list = getReconcileTradeIntradayMatchService().getReconcileTradeMatchTypeList(searchForm);
		ValidationUtils.assertNotEmpty(list, "Cannot find trade reconcile source type for FILE.");
		ValidationUtils.assertTrue(list.size() == 1, "Cannot have more than one system defined trade reconcile source type for FILE.");
		return list.get(0);
	}


	private void doCreateTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result) {
		// populate the list of ungroup external trades
		List<ReconcileTradeIntradayExternal> externalTrades = new ArrayList<>();
		for (ReconcileTradeIntradayExternalGrouping grouping : CollectionUtils.getIterable(beanList.getExternalGroupingList())) {
			externalTrades.addAll(grouping.getExternalTradeList());
		}
		beanList.setIntradayExternalList(externalTrades);

		// do the many to many match at the trade level if needed
		if ((CollectionUtils.getSize(beanList.getTradeFillList()) > 1) && (CollectionUtils.getSize(beanList.getIntradayExternalList()) > 1)) {
			// sort the trade fills in asc order to help with the matching
			BeanUtils.sortWithFunctions(beanList.getTradeFillList(), Arrays.asList(TradeFill::getNotionalUnitPrice, TradeFill::getQuantity), Arrays.asList(true, true));
			// sort the external trades in asc order to help with the matching
			BeanUtils.sortWithFunctions(beanList.getIntradayExternalList(), Arrays.asList(ReconcileTradeIntradayExternal::getPrice, ReconcileTradeIntradayExternal::getQuantity), Arrays.asList(true, true));

			MatchingResult<TradeFill, ReconcileTradeIntradayExternal> tradeMatches = null;
			if (!beanList.isMatchBrokerPrice()) {
				tradeMatches = getMatchList(beanList.getTradeFillList(), beanList.getIntradayExternalList(), true,
						beanList.getAveragePriceThresholdPercent());
			}

			// at the trade level if nothing matches, then it's a many to many match
			if (tradeMatches == null) {
				ReconcileTradeIntradayMatchObject tradeMatch = new ReconcileTradeIntradayMatchObject();
				BeanUtils.copyProperties(beanList, tradeMatch);
				tradeMatch.setTradeFillList(beanList.getTradeFillList());
				tradeMatch.setIntradayExternalList(beanList.getIntradayExternalList());
				regroupTradeMatch(tradeMatch);
				tradeMatch.setAveragePriceMatch(true);
				doReconcileTradeIntradayMatching(tradeMatch, result, false);
			}
			else {
				for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternal> tradeItem : CollectionUtils.getIterable(tradeMatches.getItemList())) {
					ReconcileTradeIntradayMatchObject tradeMatch = new ReconcileTradeIntradayMatchObject();
					BeanUtils.copyProperties(beanList, tradeMatch);
					tradeMatch.setTradeFillList(tradeItem.getInternalList());
					tradeMatch.setIntradayExternalList(tradeItem.getExternalList());
					regroupTradeMatch(tradeMatch);
					doReconcileTradeIntradayMatching(tradeMatch, result, true);
				}
			}
		}
		else {
			if ((beanList.getIntradayExternalList().size() == 1) && (beanList.getTradeFillList().size() > 1)) {
				regroupTradeMatch(beanList);
			}
			doReconcileTradeIntradayMatching(beanList, result, true);
		}
	}


	private void saveMatchItems(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result) {
		List<ReconcileTradeIntradayMatch> originalMatchList = new ArrayList<>();

		for (Trade trade : CollectionUtils.getIterable(result.getUnbookTradeList())) {
			// get the original matches
			if (TradeService.TRADE_BOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				for (ReconcileTradeIntradayMatch originalMatch : CollectionUtils.getIterable(getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByTrade(trade.getId()))) {
					if (!originalMatchList.contains(originalMatch)) {
						originalMatchList.add(originalMatch);
					}
				}
				unbookTrade(result, trade);
			}
		}

		// validations
		BigDecimal totalDeletedQuantity = CollectionUtils.getStream(result.getDeleteTradeFillList()).map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		BigDecimal totalNewQuantity = CollectionUtils.getStream(result.getNewTradeFillList()).map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
		AssertUtils.assertTrue(MathUtils.isEqual(totalDeletedQuantity, totalNewQuantity), String.format("Expected the total deleted quantity [%s] to be the same as new fill quantity [%s].", CoreMathUtils.formatNumber(totalDeletedQuantity, null), CoreMathUtils.formatNumber(totalNewQuantity, null)));

		// delete the replaced fills
		getTradeService().deleteTradeFillList(result.getDeleteTradeFillList());

		// save the new fills
		getTradeService().saveTradeFillList(result.getNewTradeFillList());

		// save the new matches
		getReconcileTradeIntradayMatchService().saveReconcileTradeIntradayMatchList(result.getNewMatchList());

		// recreate the original matches
		internalSaveReconcileTradeIntradayMatchList(originalMatchList);

		// rebook the trades
		for (Trade trade : CollectionUtils.getIterable(result.getUnbookTradeList())) {
			// 3. rebook the trade
			if (TradeService.TRADE_UNBOOKED_STATE_NAME.equals(trade.getWorkflowState().getName())) {
				reBookTrade(beanList, result.getNewMatchList(), trade);
			}
		}
	}


	/**
	 * Unbook the trade before recreating trade fills to match external.
	 * Don't delete any orphaned trade fills created after re-merging fills in {@link com.clifton.trade.workflow.splitting.TradeFillUnSplittingWorkflowAction}
	 */
	private void unbookTrade(ReconcileTradeIntradayMatcherResult result, Trade trade) {
		// unbook the trade
		List<TradeFill> beforeTradeFillList = getTradeService().getTradeFillListByTrade(trade.getId());
		transitionTradeToWorkflowState(trade, TradeService.TRADE_UNBOOKED_STATE_NAME);
		// reload fills - the unbook may merge the fills again.
		List<TradeFill> afterTradeFillList = getTradeService().getTradeFillListByTrade(trade.getId());
		beforeTradeFillList.removeAll(afterTradeFillList);
		// should be the list of deleted fills
		if (!beforeTradeFillList.isEmpty()) {
			result.getDeleteTradeFillList().removeAll(beforeTradeFillList);
		}
	}


	/**
	 * Create and save additional ReconcileTradeIntradayMatch records necessary after splitting
	 * trade fills in {@link com.clifton.trade.workflow.splitting.TradeFillSplittingWorkflowAction}.
	 */
	private void reBookTrade(ReconcileTradeIntradayMatchObject beanList, List<ReconcileTradeIntradayMatch> newMatchList, Trade trade) {
		TradeRebookingResults tradeRebookingResults = doRebookTrade(trade);

		// the fills changed as a result of the booking process.
		if (tradeRebookingResults.hasTradeFillChanges()) {
			for (TradeFill splitFill : tradeRebookingResults.getCreatedFillList()) {
				// split Fill + modified fill = fill before reposting
				TradeFill modifiedFill = tradeRebookingResults.getModifiedFill(splitFill);
				AssertUtils.assertNotNull(modifiedFill, "Could not identify the modified fill for " + splitFill.getIdentity());

				// get one of the original matches to use when created new matches for the split fill
				ReconcileTradeIntradayMatch matchTemplate = newMatchList.stream()
						.filter(m -> MathUtils.isEqual(m.getTradeFill().getId(), modifiedFill.getId()))
						.findFirst()
						.orElse(null);
				AssertUtils.assertNotNull(matchTemplate, "Existing match cannot be found for " + modifiedFill.getId());

				// the matches before the fill was split off, delete these
				List<ReconcileTradeIntradayMatch> oldMatchList = newMatchList.stream()
						.filter(m -> MathUtils.isEqual(m.getTradeFill().getId(), modifiedFill.getId()))
						.collect(Collectors.toList());
				// the external fills affected by the split fill
				List<ReconcileTradeIntradayExternal> externalList = oldMatchList.stream()
						.map(ReconcileTradeIntradayMatch::getReconcileTradeIntradayExternal)
						.collect(Collectors.toList());
				// group affected externals by price
				List<ReconcileTradeIntradayExternalGrouping> externalGroupingList = groupIntradayExternalList(externalList);

				List<TradeFill> fillList = Stream.of(splitFill, modifiedFill).collect(Collectors.toList());
				MatchingResult<TradeFill, ReconcileTradeIntradayExternal> tradeMatches = null;
				if (externalList.size() > 1 && externalGroupingList.size() > 1) {
					// match based on all permutations between the two fills and the external fill list
					tradeMatches = getMatchList(fillList, externalList, true,
							beanList.getAveragePriceThresholdPercent());
				}
				else {
					// there is only one external fill or grouping.
					BigDecimal fillQuantity = fillList.stream().map(TradeFill::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
					BigDecimal externalQuantity = externalList.stream().map(ReconcileTradeIntradayExternal::getQuantity).reduce(BigDecimal.ZERO, BigDecimal::add);
					if (MathUtils.isLessThanOrEqual(fillQuantity, externalQuantity)) {
						MatchingResultItem<TradeFill, ReconcileTradeIntradayExternal> matchingResultItem = new MatchingResultItem<>(fillList, externalList);
						tradeMatches = new MatchingResult<>(Collections.singletonList(matchingResultItem));
					}
				}

				AssertUtils.assertNotNull(tradeMatches, "Expected external matches for split fill " + splitFill.getId());

				// for the matches, create match database objects.
				List<ReconcileTradeIntradayMatch> matchList = new ArrayList<>();
				for (MatchingResultItem<TradeFill, ReconcileTradeIntradayExternal> tradeItem : CollectionUtils.getIterable(Objects.requireNonNull(tradeMatches).getItemList())) {
					for (TradeFill tradeFill : tradeItem.getInternalList()) {
						for (ReconcileTradeIntradayExternal external : tradeItem.getExternalList()) {
							ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
							BeanUtils.copyProperties(matchTemplate, match);
							match.setTradeFill(tradeFill);
							match.setReconcileTradeIntradayExternal(external);
							matchList.add(match);
						}
					}
				}

				// delete the old matches
				getReconcileTradeIntradayMatchService().deleteReconcileTradeIntradayMatchList(oldMatchList);
				// save the new matches.
				matchList.forEach(m -> getReconcileTradeIntradayMatchService().saveReconcileTradeIntradayMatch(m));
			}
		}
	}


	private TradeRebookingResults doRebookTrade(Trade trade) {
		// before quantities - do not store session managed entities for comparisons
		Map<Integer, BigDecimal> beforeTradeFillQtyMap = CollectionUtils.getStream(getTradeService().getTradeFillListByTrade(trade.getId()))
				.collect(Collectors.toMap(BaseSimpleEntity::getId, TradeFill::getQuantity));
		BigDecimal beforeNotionalTotalAmount = CollectionUtils.getStream(getTradeService().getTradeFillListByTrade(trade.getId()))
				.map(TradeFill::getNotionalTotalPrice).reduce(BigDecimal.ZERO, BigDecimal::add);

		// book trade
		transitionTradeToWorkflowState(trade, TradeService.TRADE_BOOKED_STATE_NAME);

		// after quantities
		List<TradeFill> afterFillsList = new ArrayList<>(getTradeService().getTradeFillListByTrade(trade.getId()));
		BigDecimal afterNotionalTotalAmount = CollectionUtils.getStream(getTradeService().getTradeFillListByTrade(trade.getId()))
				.map(TradeFill::getNotionalTotalPrice).reduce(BigDecimal.ZERO, BigDecimal::add);

		// validation of quantities and prices via notional total price
		AssertUtils.assertTrue(MathUtils.isLessThanOrEqual(MathUtils.absoluteDiff(beforeNotionalTotalAmount, afterNotionalTotalAmount), new BigDecimal(".02")), String.format("Expected the trade notional before reposting [%s] to be the same as after reposting [%s]", CoreMathUtils.formatNumberDecimal(beforeNotionalTotalAmount), CoreMathUtils.formatNumberDecimal(afterNotionalTotalAmount)));

		return new TradeRebookingResults(beforeTradeFillQtyMap, afterFillsList);
	}


	/**
	 * Create and execute the matcher.
	 */
	private <T extends ReconcileTradeIntradayExternal> MatchingResult<TradeFill, T> getMatchList(List<TradeFill> fillList, List<T> externalList, boolean useExternalPermutationScorer,
	                                                                                             BigDecimal averagePriceThresholdPercent) {
		AbstractManyToManyMatcher<TradeFill, T> matcher = new ReconcileTradeIntradayManyToManyMatcher<>(useExternalPermutationScorer, averagePriceThresholdPercent);
		return matcher.match(fillList, externalList);
	}


	/**
	 * Re-groups the trades by price to avoid creating fills with duplicate prices.
	 */
	private void regroupTradeMatch(ReconcileTradeIntradayMatchObject tradeMatch) {
		tradeMatch.setExternalGroupingList(groupIntradayExternalList(tradeMatch.getIntradayExternalList()));
	}


	/**
	 * Group the provided list of ReconcileTradeIntradayExternal based on price.
	 */
	private List<ReconcileTradeIntradayExternalGrouping> groupIntradayExternalList(List<ReconcileTradeIntradayExternal> intradayExternalList) {
		List<ReconcileTradeIntradayExternalGrouping> results = new ArrayList<>();

		for (ReconcileTradeIntradayExternal et : CollectionUtils.getIterable(intradayExternalList)) {
			ReconcileTradeIntradayExternalGrouping group = CollectionUtils.getOnlyElement(BeanUtils.filter(results, ReconcileTradeIntradayExternalGrouping::getPrice, et.getPrice()));
			if (group == null) {
				group = new ReconcileTradeIntradayExternalGrouping();
				group.setExternalTradeList(new ArrayList<>());
				BeanUtils.copyProperties(et, group);
				results.add(group);
			}
			else {
				group.setQuantity(MathUtils.add(et.getQuantity(), group.getQuantity()));
			}
			group.getExternalTradeList().add(et);
		}

		return results;
	}


	/**
	 * Create the lists  trades, fills and matches to be unbooked, add/deleted and created.
	 * <p/>
	 * If the external list size <= 0, then for each fill create a match.  If matching to the broker price, delete the old fill
	 * and create a new one with the same quantity at the broker price.
	 * <p/>
	 * If the internal (TradeFill) list size == 1, then delete the matched fill, create a new fill for each external group, and create the
	 * a match for each external trade in the group to the new match.
	 * <p/>
	 * Example for internal list size = 0:
	 * <p/>
	 * Fill				External Group
	 * Qty	Price		ID	Qty	Price
	 * 4	150			4	2	149 - group has 2 trades with qty 1 at price 149 and id's 5,6
	 * 5	2	151 - group has 1 trade with qty 2 at price 149 and id 7
	 * <p/>
	 * <p/>
	 * Fills Creates:
	 * ID	Qty	Price
	 * 1	2	149
	 * 2	2	151
	 * <p/>
	 * Matches Creates:
	 * FillID	ExternalID
	 * 1		5
	 * 1		6
	 * 2		7
	 *
	 * @param beanList
	 * @param result
	 */
	private void doReconcileTradeIntradayMatching(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, boolean setAveragePrice) {
		if (setAveragePrice) {
			beanList.setAveragePriceMatch(isAveragePriceMatch(beanList));
		}

		// do the final match validation
		validateFinalTradeMatchObject(beanList);

		BeanUtils.sortWithFunction(getMatcherProcessorList(), ReconcileTradeIntradayMatcherProcessor::getOrder, true);

		ReconcileTradeMatchType matchType = getAutoReconcileTradeMatchType(beanList.getMatchType());
		for (ReconcileTradeIntradayMatcherProcessor processor : CollectionUtils.getIterable(getMatcherProcessorList())) {
			if (processor.isMatcherProcessorUsable(beanList)) {
				processor.processTradeMatch(beanList, result, matchType);
				break;
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	private void internalSaveReconcileTradeIntradayMatchList(List<ReconcileTradeIntradayMatch> beanList) {
		for (ReconcileTradeIntradayMatch bean : CollectionUtils.getIterable(beanList)) {
			bean.setId(null);
			getReconcileTradeIntradayMatchService().saveReconcileTradeIntradayMatch(bean);
		}
	}


	private void validateFinalTradeMatchObject(ReconcileTradeIntradayMatchObject beanList) {
		// validate executing broker on trade fills
		BusinessCompany executingBroker = null;
		for (TradeFill tf : CollectionUtils.getIterable(beanList.getTradeFillList())) {
			if (executingBroker == null) {
				executingBroker = tf.getTrade().getExecutingBrokerCompany();
			}
			else {
				ValidationUtils.assertEquals(executingBroker, tf.getTrade().getExecutingBrokerCompany(), "Cannot match across executing brokers.");
			}
		}

		if (!beanList.isManualMatch()) {
			// check that all fills are for the same trade, and that the trade is booked.
			if (!CollectionUtils.isEmpty(beanList.getExternalGroupingList())) {
				TradeFill lastTradeFill = null;
				for (TradeFill tradeFill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
					if (lastTradeFill == null) {
						lastTradeFill = tradeFill;
					}
					else {
						//ValidationUtils.assertTrue(lastTradeFill.getTrade().equals(tradeFill.getTrade()), "Cannot match across trades.  All fills must be for the same trade.");
					}
				}
			}

			// validate executing broker on external trades
			for (ReconcileTradeIntradayExternal external : CollectionUtils.getIterable(beanList.getIntradayExternalList())) {
				ValidationUtils.assertEquals(executingBroker, external.getExecutingBrokerCompany(), "Cannot match across executing brokers.");
			}

			if (beanList.isAveragePriceMatch() || beanList.isMatchBrokerPrice()) {
				// validate the average price
				BigDecimal internalAveragePrice = getAveragePrice(beanList.getTradeFillList(), TradeFill::getQuantity, TradeFill::getNotionalUnitPrice);
				BigDecimal externalAveragePrice = getAveragePrice(beanList.getIntradayExternalList(), ReconcileTradeIntradayExternal::getQuantity, ReconcileTradeIntradayExternal::getPrice);
				BigDecimal threshold = MathUtils.multiply(internalAveragePrice, beanList.getAveragePriceThresholdPercent());
				BigDecimal priceDiff = MathUtils.abs(MathUtils.subtract(internalAveragePrice, externalAveragePrice));
				ValidationUtils.assertTrue(MathUtils.compare(priceDiff, threshold) < 1, "The difference between the fill average price of [" + internalAveragePrice
						+ "] and the external average price of [" + externalAveragePrice + "] exceeds max average price difference of [" + beanList.getAveragePriceThresholdPercent() + "%].");
			}
		}
	}


	private boolean isAveragePriceMatch(ReconcileTradeIntradayMatchObject beanList) {
		if ((beanList.getTradeFillList().size() > 1) && (beanList.getIntradayExternalList().size() == 1)) {
			ReconcileTradeIntradayExternal external = beanList.getIntradayExternalList().get(0);
			for (TradeFill tradeFill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
				if (MathUtils.compare(tradeFill.getNotionalUnitPrice(), external.getPrice()) != 0) {
					return true;
				}
			}
		}
		return false;
	}


	private <E> BigDecimal getAveragePrice(List<E> entityList, Function<E, BigDecimal> functionReturningQuantity, Function<E, BigDecimal> functionReturningPrice) {
		BigDecimal result = BigDecimal.ZERO;
		BigDecimal quantity = BigDecimal.ZERO;
		for (E entry : CollectionUtils.getIterable(entityList)) {
			result = MathUtils.add(result, MathUtils.multiply(functionReturningQuantity.apply(entry), functionReturningPrice.apply(entry)));
			quantity = MathUtils.add(quantity, functionReturningQuantity.apply(entry));
		}
		return MathUtils.divide(result, quantity);
	}


	private Trade transitionTradeToWorkflowState(Trade trade, String stateName) {
		WorkflowState workflowState = getNextTradeWorkflowState(trade, stateName);
		if (workflowState == null) {
			throw new RuntimeException("Workflow state [" + stateName + "] does not exist.");
		}
		return (Trade) getWorkflowTransitionService().executeWorkflowTransitionSystemDefined("Trade", trade.getId(), workflowState.getId());
	}


	private WorkflowState getNextTradeWorkflowState(Trade trade, String stateName) {
		List<WorkflowState> nextStates = getWorkflowDefinitionService().getWorkflowStateNextAllowedList("Trade", BeanUtils.getIdentityAsLong(trade));
		for (WorkflowState state : CollectionUtils.getIterable(nextStates)) {
			if (stateName.equals(state.getName())) {
				return state;
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}


	public WorkflowTransitionService getWorkflowTransitionService() {
		return this.workflowTransitionService;
	}


	public void setWorkflowTransitionService(WorkflowTransitionService workflowTransitionService) {
		this.workflowTransitionService = workflowTransitionService;
	}


	public List<ReconcileTradeIntradayMatcherProcessor> getMatcherProcessorList() {
		return this.matcherProcessorList;
	}


	public void setMatcherProcessorList(List<ReconcileTradeIntradayMatcherProcessor> matcherProcessorList) {
		this.matcherProcessorList = matcherProcessorList;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                  Public Inner Classes                 /////////////
	////////////////////////////////////////////////////////////////////////////

	private static class TradeRebookingResults {

		private final List<TradeFill> createdFillList;
		private final Map<TradeFill, BigDecimal> modifiedFillMap;


		private TradeRebookingResults(Map<Integer, BigDecimal> before, List<TradeFill> after) {
			// created
			this.createdFillList = Collections.unmodifiableList(after.stream()
					.filter(f -> !before.containsKey(f.getId()))
					.collect(Collectors.toList())
			);
			// modified / differences
			this.modifiedFillMap = Collections.unmodifiableMap(getModifiedMap(before, after));
		}


		private boolean hasTradeFillChanges() {
			return !getCreatedFillList().isEmpty() || !getModifiedFillMap().isEmpty();
		}


		/**
		 * Trade fill with a difference between before / after reposting
		 */
		private Map<TradeFill, BigDecimal> getModifiedMap(Map<Integer, BigDecimal> beforeTradeFillQtyMap, List<TradeFill> after) {
			Map<Integer, TradeFill> afterTradeFillQtyMap = after.stream()
					.collect(Collectors.toMap(TradeFill::getId, Function.identity()));
			Map<TradeFill, BigDecimal> results = new HashMap<>();
			for (Map.Entry<Integer, BigDecimal> before : beforeTradeFillQtyMap.entrySet()) {
				TradeFill afterFill = afterTradeFillQtyMap.get(before.getKey());
				// identify the trade fill with a quantity change
				BigDecimal difference = MathUtils.abs(MathUtils.subtract(before.getValue(), afterFill.getQuantity()));
				if (!MathUtils.isNullOrZero(difference)) {
					results.put(afterFill, difference);
				}
			}
			return results;
		}


		public TradeFill getModifiedFill(TradeFill createdFill) {
			for (Map.Entry<TradeFill, BigDecimal> entry : getModifiedFillMap().entrySet()) {
				if (MathUtils.isEqual(entry.getValue(), createdFill.getQuantity())) {
					return entry.getKey();
				}
			}
			return null;
		}


		private List<TradeFill> getCreatedFillList() {
			return this.createdFillList;
		}


		public Map<TradeFill, BigDecimal> getModifiedFillMap() {
			return this.modifiedFillMap;
		}
	}
}
