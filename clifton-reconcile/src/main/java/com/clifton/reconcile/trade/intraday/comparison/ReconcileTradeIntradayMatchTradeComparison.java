package com.clifton.reconcile.trade.intraday.comparison;


import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeService;


/**
 * The <code>ReconcileTradeIntradayMatchTradeComparison</code> evaluates the selected
 * condition on the Trade associated with the match entity.
 * <p>
 * Trade is either from the TradeFill on the Match object, or pulled from the Temp table
 * <p>
 * Most comparisons for Reconciliation checks/verifies data on the Trade, it's workflow, associated notes, etc.
 *
 * @author manderson
 */
public class ReconcileTradeIntradayMatchTradeComparison implements Comparison<ReconcileTradeIntradayMatch> {

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private TradeService tradeService;

	//////////////////////////////////////////////////

	private Integer tradeConditionId;


	//////////////////////////////////////////////////


	@Override
	public boolean evaluate(ReconcileTradeIntradayMatch match, ComparisonContext context) {
		Trade trade = getReconcileTradeIntradayMatchService().getTradeForReconcileTradeIntradayMatch(match);
		boolean result = false;
		String resultMessage;
		if (trade != null) {
			EvaluationResult evalResult = getSystemConditionEvaluationHandler().evaluateCondition(getTradeConditionId(), trade);
			result = evalResult.isResult();
			resultMessage = evalResult.getMessage();
		}
		else {
			resultMessage = "Unable to find Trade associated with Reconcile Trade Intraday Match entity with ID [" + match.getId() + "]";
		}

		if (context != null) {
			if (result) {
				context.recordTrueMessage(resultMessage);
			}
			else {
				context.recordFalseMessage(resultMessage);
			}
		}
		return result;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public Integer getTradeConditionId() {
		return this.tradeConditionId;
	}


	public void setTradeConditionId(Integer tradeConditionId) {
		this.tradeConditionId = tradeConditionId;
	}


	public TradeService getTradeService() {
		return this.tradeService;
	}


	public void setTradeService(TradeService tradeService) {
		this.tradeService = tradeService;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
