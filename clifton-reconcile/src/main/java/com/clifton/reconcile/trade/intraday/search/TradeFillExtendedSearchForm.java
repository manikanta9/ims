package com.clifton.reconcile.trade.intraday.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.trade.search.TradeFillSearchForm;


public class TradeFillExtendedSearchForm extends TradeFillSearchForm {

	@SearchField
	private Boolean reconciled;

	@SearchField(searchField = "name,number", searchFieldPath = "trade.clientInvestmentAccount", sortField = "number")
	private String clientInvestmentAccount;

	@SearchField(searchField = "name,number", searchFieldPath = "trade.holdingInvestmentAccount", sortField = "number")
	private String holdingInvestmentAccount;

	@SearchField(searchField = "name", searchFieldPath = "trade.holdingInvestmentAccount.issuingCompany")
	private String holdingAccountIssuerName;

	@SearchField(searchField = "symbol", searchFieldPath = "trade.investmentSecurity", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String symbol;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public String getClientInvestmentAccount() {
		return this.clientInvestmentAccount;
	}


	public void setClientInvestmentAccount(String clientInvestmentAccount) {
		this.clientInvestmentAccount = clientInvestmentAccount;
	}


	public String getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(String holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public String getHoldingAccountIssuerName() {
		return this.holdingAccountIssuerName;
	}


	public void setHoldingAccountIssuerName(String holdingAccountIssuerName) {
		this.holdingAccountIssuerName = holdingAccountIssuerName;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}
}
