package com.clifton.reconcile.trade.intraday.match.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchSources;


/**
 * The <code>ReconcileTradeMatchTypeSearchForm</code> class defines search configuration for ReconcileTradeMatchType objects.
 *
 * @author vgomelsky
 */
public class ReconcileTradeMatchTypeSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "itemList.instrumentList.referenceTwo.hierarchy.investmentType.id", searchFieldPath = "investmentGroup", comparisonConditions = ComparisonConditions.EXISTS)
	private Short investmentTypeId;

	@SearchField(searchField = "itemList.instrumentList.referenceTwo.hierarchy.id", searchFieldPath = "investmentGroup", comparisonConditions = ComparisonConditions.EXISTS)
	private Short instrumentHierarchyId;

	@SearchField(searchField = "itemList.instrumentList.referenceTwo.securityList.id", searchFieldPath = "investmentGroup", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer securityId;

	// Custom
	private Integer clientInvestmentAccountId;

	@SearchField(searchField = "autoCreateManualMatchCondition.id")
	private Integer autoCreateManualMatchConditionId;

	@SearchField
	private ReconcileTradeMatchSources source;

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField
	private Integer daysToConfirm;

	@SearchField
	private Boolean autoConfirm;

	@SearchField
	private Boolean systemDefined;

	@SearchField
	private Boolean noteRequired;

	@SearchField
	private Boolean contactRequired;

	@SearchField
	private Boolean attachmentRequired;

	@SearchField(searchField = "workflow.id", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean workflowExists;

	@SearchField(searchField = "workflow.id")
	private Short workflowId;

	@SearchField(searchFieldPath = "workflow", searchField = "name")
	private String workflowName;

	@SearchField(searchFieldPath = "investmentGroup", searchField = "name")
	private String investmentGroupName;

	@SearchField(searchFieldPath = "investmentGroup.id")
	private Integer investmentGroupId;

	@SearchField(searchFieldPath = "clientInvestmentAccountGroup", searchField = "name")
	private String clientInvestmentAccountGroupName;

	@SearchField(searchFieldPath = "clientInvestmentAccountGroup.id")
	private Integer clientInvestmentAccountGroupNameId;

	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Short getInvestmentTypeId() {
		return this.investmentTypeId;
	}


	public void setInvestmentTypeId(Short investmentTypeId) {
		this.investmentTypeId = investmentTypeId;
	}


	public Short getInstrumentHierarchyId() {
		return this.instrumentHierarchyId;
	}


	public void setInstrumentHierarchyId(Short instrumentHierarchyId) {
		this.instrumentHierarchyId = instrumentHierarchyId;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public ReconcileTradeMatchSources getSource() {
		return this.source;
	}


	public void setSource(ReconcileTradeMatchSources source) {
		this.source = source;
	}


	public Integer getDaysToConfirm() {
		return this.daysToConfirm;
	}


	public void setDaysToConfirm(Integer daysToConfirm) {
		this.daysToConfirm = daysToConfirm;
	}


	public Boolean getSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(Boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public Boolean getNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(Boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public Boolean getContactRequired() {
		return this.contactRequired;
	}


	public void setContactRequired(Boolean contactRequired) {
		this.contactRequired = contactRequired;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getAttachmentRequired() {
		return this.attachmentRequired;
	}


	public void setAttachmentRequired(Boolean attachmentRequired) {
		this.attachmentRequired = attachmentRequired;
	}


	public Boolean getAutoConfirm() {
		return this.autoConfirm;
	}


	public void setAutoConfirm(Boolean autoConfirm) {
		this.autoConfirm = autoConfirm;
	}


	public Boolean getWorkflowExists() {
		return this.workflowExists;
	}


	public void setWorkflowExists(Boolean workflowExists) {
		this.workflowExists = workflowExists;
	}


	public Short getWorkflowId() {
		return this.workflowId;
	}


	public void setWorkflowId(Short workflowId) {
		this.workflowId = workflowId;
	}


	public String getWorkflowName() {
		return this.workflowName;
	}


	public void setWorkflowName(String workflowName) {
		this.workflowName = workflowName;
	}


	public Integer getClientInvestmentAccountId() {
		return this.clientInvestmentAccountId;
	}


	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
		this.clientInvestmentAccountId = clientInvestmentAccountId;
	}


	public Integer getAutoCreateManualMatchConditionId() {
		return this.autoCreateManualMatchConditionId;
	}


	public void setAutoCreateManualMatchConditionId(Integer autoCreateManualMatchConditionId) {
		this.autoCreateManualMatchConditionId = autoCreateManualMatchConditionId;
	}


	public String getInvestmentGroupName() {
		return this.investmentGroupName;
	}


	public void setInvestmentGroupName(String investmentGroupName) {
		this.investmentGroupName = investmentGroupName;
	}


	public Integer getInvestmentGroupId() {
		return this.investmentGroupId;
	}


	public void setInvestmentGroupId(Integer investmentGroupId) {
		this.investmentGroupId = investmentGroupId;
	}


	public String getClientInvestmentAccountGroupName() {
		return this.clientInvestmentAccountGroupName;
	}


	public void setClientInvestmentAccountGroupName(String clientInvestmentAccountGroupName) {
		this.clientInvestmentAccountGroupName = clientInvestmentAccountGroupName;
	}


	public Integer getClientInvestmentAccountGroupNameId() {
		return this.clientInvestmentAccountGroupNameId;
	}


	public void setClientInvestmentAccountGroupNameId(Integer clientInvestmentAccountGroupNameId) {
		this.clientInvestmentAccountGroupNameId = clientInvestmentAccountGroupNameId;
	}
}
