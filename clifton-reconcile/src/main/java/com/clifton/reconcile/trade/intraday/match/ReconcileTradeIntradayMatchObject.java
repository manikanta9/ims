package com.clifton.reconcile.trade.intraday.match;


import com.clifton.business.contact.BusinessContact;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.trade.TradeFill;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ReconcileTradeIntradayMatchList</code> class allows to match multiple fills at the same time or
 * matching fill(s) to external trade(s).
 *
 * @author mwacker
 */
public class ReconcileTradeIntradayMatchObject {

	private boolean matchBrokerPrice;
	private boolean manualMatch;
	private boolean averagePriceMatch;

	private List<ReconcileTradeIntradayExternalGrouping> externalGroupingList;

	private List<TradeFill> tradeFillList;
	private List<ReconcileTradeIntradayExternal> intradayExternalList;

	private List<ReconcileTradeIntradayMatch> matchList;

	private ReconcileTradeMatchType matchType;
	private BusinessContact contact;
	private String note;

	private BigDecimal averagePriceThresholdPercent = new BigDecimal("0.000001");

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<ReconcileTradeIntradayExternalGrouping> getExternalGroupingList() {
		return this.externalGroupingList;
	}


	public void setExternalGroupingList(List<ReconcileTradeIntradayExternalGrouping> externalGroupingList) {
		this.externalGroupingList = externalGroupingList;
	}


	public List<TradeFill> getTradeFillList() {
		return this.tradeFillList;
	}


	public void setTradeFillList(List<TradeFill> tradeFillList) {
		this.tradeFillList = tradeFillList;
	}


	public List<ReconcileTradeIntradayExternal> getIntradayExternalList() {
		return this.intradayExternalList;
	}


	public void setIntradayExternalList(List<ReconcileTradeIntradayExternal> intradayExternalList) {
		this.intradayExternalList = intradayExternalList;
	}


	public List<ReconcileTradeIntradayMatch> getMatchList() {
		return this.matchList;
	}


	public void setMatchList(List<ReconcileTradeIntradayMatch> matchList) {
		this.matchList = matchList;
	}


	public boolean isMatchBrokerPrice() {
		return this.matchBrokerPrice;
	}


	public void setMatchBrokerPrice(boolean matchBrokerPrice) {
		this.matchBrokerPrice = matchBrokerPrice;
	}


	public boolean isManualMatch() {
		return this.manualMatch;
	}


	public void setManualMatch(boolean manualMatch) {
		this.manualMatch = manualMatch;
	}


	public BusinessContact getContact() {
		return this.contact;
	}


	public void setContact(BusinessContact contact) {
		this.contact = contact;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public ReconcileTradeMatchType getMatchType() {
		return this.matchType;
	}


	public void setMatchType(ReconcileTradeMatchType matchType) {
		this.matchType = matchType;
	}


	public BigDecimal getAveragePriceThresholdPercent() {
		return this.averagePriceThresholdPercent;
	}


	public void setAveragePriceThresholdPercent(BigDecimal averagePriceThresholdPercent) {
		this.averagePriceThresholdPercent = averagePriceThresholdPercent;
	}


	public boolean isAveragePriceMatch() {
		return this.averagePriceMatch;
	}


	public void setAveragePriceMatch(boolean averagePriceMatch) {
		this.averagePriceMatch = averagePriceMatch;
	}
}
