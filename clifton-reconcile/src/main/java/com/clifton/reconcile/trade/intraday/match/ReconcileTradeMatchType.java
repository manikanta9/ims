package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.beans.NamedEntity;
import com.clifton.investment.account.group.InvestmentAccountGroup;
import com.clifton.investment.setup.group.InvestmentGroup;
import com.clifton.system.condition.SystemCondition;
import com.clifton.workflow.definition.Workflow;


/**
 * The <code>ReconcileTradeMatchType</code> class defines the type of reconciliation that was used to
 * match/confirm a trade fill.
 *
 * @author vgomelsky
 */
public class ReconcileTradeMatchType extends NamedEntity<Short> {

	/**
	 * applies to instruments in this group only. If NULL, can apply to any instrument (global).
	 */
	private InvestmentGroup investmentGroup;

	/**
	 * Used when auto create manual match is set to true.
	 */
	private InvestmentAccountGroup clientInvestmentAccountGroup;

	/**
	 * Condition to use when creating matches for trade fills. Condition must evaluate to true in order to automatically create the matches.
	 */
	private SystemCondition autoCreateManualMatchCondition;

	private ReconcileTradeMatchSources source;

	private int daysToConfirm; // usually 0: meaning same day

	private boolean systemDefined; // cannot be changed
	private boolean noteRequired;
	private boolean contactRequired;
	private boolean attachmentRequired;

	/**
	 * Automatically create a manual match when a trade in the investment group and account group is booked.
	 */
	private boolean autoCreateManualMatch;

	/**
	 * Automatically mark trades as confirmed when this type is used.
	 */
	private boolean autoConfirm;

	/**
	 * Optional Workflow that can be used for reconciliation/affirmation/confirmation process
	 * Allowed to select workflow where there exists at least 1 security in the associated InvestmentGroup where there is a TradeType flagged as IsSingleFillTrade = 1 for its investment type.
	 */
	public Workflow workflow;


	////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public ReconcileTradeMatchSources getSource() {
		return this.source;
	}


	public void setSource(ReconcileTradeMatchSources source) {
		this.source = source;
	}


	public int getDaysToConfirm() {
		return this.daysToConfirm;
	}


	public void setDaysToConfirm(int daysToConfirm) {
		this.daysToConfirm = daysToConfirm;
	}


	public boolean isSystemDefined() {
		return this.systemDefined;
	}


	public void setSystemDefined(boolean systemDefined) {
		this.systemDefined = systemDefined;
	}


	public boolean isNoteRequired() {
		return this.noteRequired;
	}


	public void setNoteRequired(boolean noteRequired) {
		this.noteRequired = noteRequired;
	}


	public boolean isContactRequired() {
		return this.contactRequired;
	}


	public void setContactRequired(boolean contactRequired) {
		this.contactRequired = contactRequired;
	}


	public InvestmentGroup getInvestmentGroup() {
		return this.investmentGroup;
	}


	public void setInvestmentGroup(InvestmentGroup investmentGroup) {
		this.investmentGroup = investmentGroup;
	}


	public boolean isAttachmentRequired() {
		return this.attachmentRequired;
	}


	public void setAttachmentRequired(boolean attachmentRequired) {
		this.attachmentRequired = attachmentRequired;
	}


	public boolean isAutoConfirm() {
		return this.autoConfirm;
	}


	public void setAutoConfirm(boolean autoConfirm) {
		this.autoConfirm = autoConfirm;
	}


	public Workflow getWorkflow() {
		return this.workflow;
	}


	public void setWorkflow(Workflow workflow) {
		this.workflow = workflow;
	}


	public boolean isAutoCreateManualMatch() {
		return this.autoCreateManualMatch;
	}


	public void setAutoCreateManualMatch(boolean autoCreateManualMatch) {
		this.autoCreateManualMatch = autoCreateManualMatch;
	}


	public InvestmentAccountGroup getClientInvestmentAccountGroup() {
		return this.clientInvestmentAccountGroup;
	}


	public void setClientInvestmentAccountGroup(InvestmentAccountGroup clientInvestmentAccountGroup) {
		this.clientInvestmentAccountGroup = clientInvestmentAccountGroup;
	}

	// TODO: noteRequiredIfAttachment?


	public SystemCondition getAutoCreateManualMatchCondition() {
		return this.autoCreateManualMatchCondition;
	}


	public void setAutoCreateManualMatchCondition(SystemCondition autoCreateManualMatchCondition) {
		this.autoCreateManualMatchCondition = autoCreateManualMatchCondition;
	}
}
