package com.clifton.reconcile.trade.intraday.matcher;


import com.clifton.business.company.BusinessCompany;
import com.clifton.core.beans.BeanUtils.BeanPropertyRetriever;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.matching.AbstractManyToManyMatcher;
import com.clifton.core.util.matching.MatchingResult;
import com.clifton.core.util.matching.MatchingResultItem;
import com.clifton.core.util.matching.PermutationScorer;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.trade.TradeFill;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.List;


/**
 * The <code>ReconcileTradeIntradayMatchingManyToMany</code> many to many matcher for trade fills.
 *
 * @param <T>
 * @author mwacker
 */
public class ReconcileTradeIntradayManyToManyMatcher<T extends ReconcileTradeIntradayExternal> extends AbstractManyToManyMatcher<TradeFill, T> {

	/**
	 * Determines if a permutation scorer is passed to external permutation object.
	 */
	private boolean externalPermutationScorerUsed;

	private BigDecimal averagePriceThresholdPercent = new BigDecimal("0.000001");

	private static final BeanPropertyRetriever<TradeFill, BigDecimal> INTERNAL_QTY_RETRIEVER = TradeFill::getQuantity;
	private static final BeanPropertyRetriever<ReconcileTradeIntradayExternal, BigDecimal> EXTERNAL_QTY_RETRIEVER = ReconcileTradeIntradayExternal::getQuantity;
	private static final BeanPropertyRetriever<TradeFill, BigDecimal> INTERNAL_PRICE_RETRIEVER = TradeFill::getNotionalUnitPrice;
	private static final BeanPropertyRetriever<ReconcileTradeIntradayExternal, BigDecimal> EXTERNAL_PRICE_RETRIEVER = ReconcileTradeIntradayExternal::getPrice;


	public ReconcileTradeIntradayManyToManyMatcher() {
		//
	}


	public ReconcileTradeIntradayManyToManyMatcher(boolean externalPermutationScorerUsed, BigDecimal averagePriceThresholdPercent) {
		this.externalPermutationScorerUsed = externalPermutationScorerUsed;
		this.averagePriceThresholdPercent = averagePriceThresholdPercent;
	}


	@Override
	protected PermutationScorer<TradeFill> getInternalPermutationScorer() {
		return null;
	}


	@Override
	protected PermutationScorer<T> getExternalPermutationScorer() {
		if (this.externalPermutationScorerUsed) {
			return permutation -> getQuantity(permutation, ReconcileTradeIntradayExternal::getQuantity).intValue();
		}
		return null;
	}


	/**
	 * Get the score for the complete result, which is sum of item results.
	 * <p/>
	 * NOTE: -1 means this option is not viable.
	 */
	@Override
	protected int getMatchResultScore(MatchingResult<TradeFill, T> match) {
		int result = -1;
		boolean allHaveSideWithOne = true;
		for (MatchingResultItem<TradeFill, T> item : CollectionUtils.getIterable(match.getItemList())) {
			int itemScore = getMatchResultItemScore(item);
			if (itemScore == -1) {
				return -1;
			}
			if (result == -1) {
				result = 0;
			}
			result += itemScore;

			if (item.getExternalList().size() != 1 && item.getInternalList().size() != 1) {
				allHaveSideWithOne = false;
			}
		}
		return allHaveSideWithOne ? 0 : result;
	}


	/**
	 * Get the score to sub match, 1 indicates a good match and -1 indicates a bad match.
	 * <p/>
	 * NOTE: The the total if the quantities don't match or the average price difference is out side the threshold return -1 to
	 * indicate it's not a viable match.
	 */
	@Override
	@SuppressWarnings("unchecked")
	protected int getMatchResultItemScore(MatchingResultItem<TradeFill, T> matchItem) {
		BigDecimal iq = getQuantity(matchItem.getInternalList(), INTERNAL_QTY_RETRIEVER);
		BigDecimal eq = getQuantity((List<ReconcileTradeIntradayExternal>) matchItem.getExternalList(), EXTERNAL_QTY_RETRIEVER);
		if (MathUtils.compare(iq, eq) != 0) {
			return -1;
		}
		BigDecimal internalAveragePrice = getAveragePrice(iq, matchItem.getInternalList(), INTERNAL_PRICE_RETRIEVER, INTERNAL_QTY_RETRIEVER);
		BigDecimal externalAveragePrice = getAveragePrice(eq, (List<ReconcileTradeIntradayExternal>) matchItem.getExternalList(), EXTERNAL_PRICE_RETRIEVER, EXTERNAL_QTY_RETRIEVER);
		BigDecimal threshold = MathUtils.multiply(internalAveragePrice, getAveragePriceThresholdPercent());
		BigDecimal priceDiff = MathUtils.abs(MathUtils.subtract(internalAveragePrice, externalAveragePrice));
		if (MathUtils.compare(threshold, priceDiff) < 1) {
			return -1;
		}
		BusinessCompany executingBroker = null;
		for (TradeFill tf : CollectionUtils.getIterable(matchItem.getInternalList())) {
			if (executingBroker == null) {
				executingBroker = tf.getTrade().getExecutingBrokerCompany();
			}
			else {
				if (!executingBroker.equals(tf.getTrade().getExecutingBrokerCompany())) {
					return -1;
				}
			}
		}
		// validate executing broker on external trades
		for (T external : CollectionUtils.getIterable(matchItem.getExternalList())) {
			BusinessCompany broker = external.getExecutingBrokerCompany();
			if (!broker.equals(executingBroker)) {
				return -1;
			}
		}
		return 1;
	}


	/**
	 * Get the total quantity from the list.
	 *
	 * @param entityList
	 */
	private <R> BigDecimal getQuantity(List<R> entityList, BeanPropertyRetriever<R, BigDecimal> retriever) {
		BigDecimal quantity = BigDecimal.ZERO;
		for (R entry : CollectionUtils.getIterable(entityList)) {
			quantity = MathUtils.add(quantity, retriever.getPropertyValue(entry));
		}
		return quantity;
	}


	/**
	 * Average price methods broker into 2, because profiler revealed that BeanUtils.getPropertyValue was 75% of the processing time
	 * when getting the score.
	 *
	 * @param entityList
	 */
	private <R> BigDecimal getAveragePrice(BigDecimal quantity, List<R> entityList, BeanPropertyRetriever<R, BigDecimal> priceRetriever, BeanPropertyRetriever<R, BigDecimal> quantityRetriever) {
		BigDecimal result = BigDecimal.ZERO;
		for (R entry : CollectionUtils.getIterable(entityList)) {
			result = MathUtils.add(result, MathUtils.multiply(quantityRetriever.getPropertyValue(entry), priceRetriever.getPropertyValue(entry)));
		}
		return MathUtils.divide(result, quantity);
	}


	public BigDecimal getAveragePriceThresholdPercent() {
		return this.averagePriceThresholdPercent;
	}


	public void setAveragePriceThresholdPercent(BigDecimal averagePriceThresholdPercent) {
		this.averagePriceThresholdPercent = averagePriceThresholdPercent;
	}
}
