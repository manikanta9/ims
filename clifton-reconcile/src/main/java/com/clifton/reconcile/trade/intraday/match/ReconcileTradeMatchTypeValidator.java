package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.workflow.assignment.WorkflowAssignment;
import com.clifton.workflow.assignment.WorkflowAssignmentService;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The <code>ReconcileTradeMatchTypeValidator</code> validates:
 * <p/>
 * 1. Cannot make changes to System Defined Types
 * 2. "FILE match source can only be used for system defined types."
 * 3. If Workflow is Selected:
 * a. It is active and has an assignment to the ReconcileTradeMatchType table
 *
 * @author manderson
 */
@Component
public class ReconcileTradeMatchTypeValidator extends SelfRegisteringDaoValidator<ReconcileTradeMatchType> {

	private WorkflowAssignmentService workflowAssignmentService;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(ReconcileTradeMatchType bean, DaoEventTypes config) throws ValidationException {
		ReconcileTradeMatchType originalBean = null;
		if (config.isUpdate()) {
			originalBean = getOriginalBean(bean);
		}
		validateSystemDefined(bean, originalBean, config);

		// "Files" are only for system defined types
		ValidationUtils.assertFalse(ReconcileTradeMatchSources.FILE == bean.getSource(), "FILE match source can only be used for system defined types.");

		// New Type Or Updating Type to a New Workflow
		if (config.isInsert() || config.isUpdate()) {
			if (bean.getWorkflow() != null && (originalBean == null || originalBean.getWorkflow() == null || !originalBean.getWorkflow().equals(bean.getWorkflow()))) {
				// It is active and has an assignment to the ReconcileTradeIntradayMatch table
				validateActiveAndAssignedWorkflow(bean);
			}
		}
	}


	private void validateSystemDefined(ReconcileTradeMatchType bean, ReconcileTradeMatchType originalBean, DaoEventTypes config) {
		if (config.isUpdate()) {
			if (originalBean != null && originalBean.isSystemDefined()) {
				throw new ValidationException("Cannot update system defined ReconcileTradeMatchType: " + originalBean.getLabel());
			}
			else if (bean.isSystemDefined()) {
				throw new ValidationException("Cannot update system defined flag for ReconcileTradeMatchType: " + bean.getLabel());
			}
		}
		else if (bean.isSystemDefined()) {
			throw new ValidationException("Cannot " + (config.isInsert() ? "insert" : "delete") + " system defined Reconcile Trade Match Types.");
		}
	}


	/**
	 * It is active and has an assignment to the ReconcileTradeMatchType table *
	 */
	private void validateActiveAndAssignedWorkflow(ReconcileTradeMatchType bean) {
		// Should happen because of filters on the screen, but just in case
		ValidationUtils.assertTrue(bean.getWorkflow().isActive(), "Workflow [" + bean.getWorkflow().getName() + "] cannot be assigned because it is not currently active.");
		List<WorkflowAssignment> assignmentList = getWorkflowAssignmentService().getWorkflowAssignmentListByTableName("ReconcileTradeIntradayMatch", false);
		assignmentList = BeanUtils.filter(assignmentList, WorkflowAssignment::getWorkflow, bean.getWorkflow());
		if (CollectionUtils.isEmpty(assignmentList)) {
			throw new ValidationException("Cannot use selected Workflow [" + bean.getWorkflow().getName() + "].  It does not have any assignments to 'ReconcileTradeIntradayMatch' table.");
		}
	}


	public WorkflowAssignmentService getWorkflowAssignmentService() {
		return this.workflowAssignmentService;
	}


	public void setWorkflowAssignmentService(WorkflowAssignmentService workflowAssignmentService) {
		this.workflowAssignmentService = workflowAssignmentService;
	}
}
