package com.clifton.reconcile.trade.intraday.matcher.processor;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatcherResult;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AveragePriceMatcherProcessor</code> does an average price match when the number of external trades is == 1.  This will
 * remove any trade fills and create one trade fill that matches the external trade.
 * <p/>
 * NOTE: This method class assumes that the validation to check that this is a valid average price match is done prior to calling processTradeMatch.
 *
 * @author mwacker
 */
public class AveragePriceMatcherProcessor implements ReconcileTradeIntradayMatcherProcessor {

	private int order;


	@Override
	public boolean isMatcherProcessorUsable(ReconcileTradeIntradayMatchObject beanList) {
		return !beanList.isMatchBrokerPrice() && beanList.isAveragePriceMatch() && (CollectionUtils.getSize(beanList.getIntradayExternalList()) == 1);
	}


	@Override
	public void processTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		if (CollectionUtils.getSize(beanList.getIntradayExternalList()) == 1) {
			ReconcileTradeIntradayExternal external = CollectionUtils.getOnlyElement(beanList.getIntradayExternalList());

			List<String> oldPriceList = new ArrayList<>();
			// add the existing fills to the delete list
			for (TradeFill fill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
				oldPriceList.add(CoreMathUtils.formatNumberDecimal(fill.getNotionalUnitPrice()));
				if (!result.getDeleteTradeFillList().contains(fill)) {
					result.getDeleteTradeFillList().add(fill);
				}
				// get the trade and add to the unbook list
				Trade tradeToUnbook = fill.getTrade();
				if (!result.getUnbookTradeList().contains(tradeToUnbook)) {
					result.getUnbookTradeList().add(tradeToUnbook);
				}
			}

			// go through each trade and create a new
			for (Trade trade : CollectionUtils.getIterable(result.getUnbookTradeList())) {
				// create the new fill
				TradeFill fill = new TradeFill();
				fill.setTrade(trade);
				fill.setQuantity(trade.getQuantityIntended());
				// update the price of the fill - always use the broker price because the average prices is already in the threshold
				fill.setNotionalUnitPrice(external.getPrice());

				// add the new fill the new fill list
				result.getNewTradeFillList().add(fill);

				// create the match
				ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
				match.setMatchType(matchType);
				match.setReconcileTradeIntradayExternal(external);
				match.setContact(beanList.getContact());
				match.setNote(beanList.getNote());
				match.setConfirmed(matchType.isAutoConfirm());

				// set the new fill on the match
				match.setTradeFill(fill);

				// add the note
				StringBuilder note = new StringBuilder();
				if (StringUtils.isEmpty(match.getNote())) {
					note.append(match.getNote()).append("\n\n");
				}
				note.append("Removing average price fills of [").append(StringUtils.join(oldPriceList, ",")).append("] and match to broker price of [").append(external.getPrice()).append("].");
				match.setNote(note.toString());

				result.getNewMatchList().add(match);
			}
		}
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
