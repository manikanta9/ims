package com.clifton.reconcile.trade.intraday.matcher.processor;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatcherResult;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;


/**
 * The <code>OneTradeFillMatcherProcessor</code> handles matching one internal trade fill to one or many external trades.
 *
 * @author mwacker
 */
public class OneTradeFillMatcherProcessor implements ReconcileTradeIntradayMatcherProcessor {

	private int order;


	@Override
	public boolean isMatcherProcessorUsable(ReconcileTradeIntradayMatchObject beanList) {
		return CollectionUtils.getSize(beanList.getTradeFillList()) == 1;
	}


	@Override
	public void processTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		if (beanList.getTradeFillList().size() == 1) {
			TradeFill fill = beanList.getTradeFillList().get(0);
			// 1. get the trade and add to the unbook list
			Trade trade = fill.getTrade();
			if (!result.getUnbookTradeList().contains(trade)) {
				result.getUnbookTradeList().add(trade);
			}

			// 2. add the existing fill to the delete list
			if (!result.getDeleteTradeFillList().contains(fill)) {
				result.getDeleteTradeFillList().add(fill);
			}

			for (ReconcileTradeIntradayExternalGrouping external : CollectionUtils.getIterable(beanList.getExternalGroupingList())) {
				TradeFill nextFill = new TradeFill();
				ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
				match.setMatchType(matchType);
				match.setContact(beanList.getContact());
				match.setNote(beanList.getNote());
				match.setConfirmed(matchType.isAutoConfirm());
				nextFill.setTrade(trade);
				nextFill.setQuantity(external.getQuantity());
				if (beanList.isMatchBrokerPrice() || (beanList.getExternalGroupingList().size() > 1)) {
					nextFill.setNotionalUnitPrice(external.getPrice());
					if (StringUtils.isEmpty(match.getNote())) {
						match.setNote("Matched to broker price of " + CoreMathUtils.formatNumberDecimal(external.getPrice()) + " from " + CoreMathUtils.formatNumberDecimal(fill.getNotionalUnitPrice()));
					}
				}
				else {
					nextFill.setNotionalUnitPrice(fill.getNotionalUnitPrice());
				}
				result.getNewTradeFillList().add(nextFill);

				for (ReconcileTradeIntradayExternal extTrade : CollectionUtils.getIterable(external.getExternalTradeList())) {
					ReconcileTradeIntradayMatch newMatch = new ReconcileTradeIntradayMatch();
					BeanUtils.copyProperties(match, newMatch);
					newMatch.setTradeFill(nextFill);
					newMatch.setReconcileTradeIntradayExternal(extTrade);

					result.getNewMatchList().add(newMatch);
				}
			}
		}
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
