package com.clifton.reconcile.trade.settlement;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.system.note.SystemNote;
import com.clifton.trade.Trade;


@NonPersistentObject
public class TradeSettlement extends Trade {

	private boolean finalSettlement;

	/**
	 * Last updated note associated with the Trade of Note Type: 'Internal Use Only - Ops'
	 */
	private SystemNote systemNote;


	public boolean isFinalSettlement() {
		return this.finalSettlement;
	}


	public void setFinalSettlement(boolean finalSettlement) {
		this.finalSettlement = finalSettlement;
	}


	public SystemNote getSystemNote() {
		return this.systemNote;
	}


	public void setSystemNote(SystemNote systemNote) {
		this.systemNote = systemNote;
	}
}
