package com.clifton.reconcile.trade.settlement;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.workflow.definition.WorkflowDefinitionService;
import com.clifton.workflow.search.WorkflowAwareSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ReconcileTradeSettlementServiceImpl implements ReconcileTradeSettlementService {

	private AdvancedReadOnlyDAO<TradeSettlement, Criteria> tradeSettlementDAO;
	private WorkflowDefinitionService workflowDefinitionService;


	@Override
	public List<TradeSettlement> getTradeSettlementList(TradeSettlementSearchForm searchForm) {
		return getTradeSettlementDAO().findBySearchCriteria(new WorkflowAwareSearchFormConfigurer(searchForm, getWorkflowDefinitionService()));
	}


	public AdvancedReadOnlyDAO<TradeSettlement, Criteria> getTradeSettlementDAO() {
		return this.tradeSettlementDAO;
	}


	public void setTradeSettlementDAO(AdvancedReadOnlyDAO<TradeSettlement, Criteria> tradeSettlementDAO) {
		this.tradeSettlementDAO = tradeSettlementDAO;
	}


	public WorkflowDefinitionService getWorkflowDefinitionService() {
		return this.workflowDefinitionService;
	}


	public void setWorkflowDefinitionService(WorkflowDefinitionService workflowDefinitionService) {
		this.workflowDefinitionService = workflowDefinitionService;
	}
}
