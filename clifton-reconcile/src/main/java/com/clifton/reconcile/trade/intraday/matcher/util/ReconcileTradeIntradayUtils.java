package com.clifton.reconcile.trade.intraday.matcher.util;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.trade.Trade;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ReconcileTradeIntradayUtils {

	public static List<TradeFillResult> fillTrades(List<Trade> tradeList, List<ExternalFillDetail> detailList) {
		BigDecimal totalTrades = CoreMathUtils.sumProperty(tradeList, Trade::getQuantityIntended);
		BigDecimal totalExternalFills = CoreMathUtils.sumProperty(detailList, ExternalFillDetail::getQuantity);
		ValidationUtils.assertTrue(MathUtils.compare(totalTrades, totalExternalFills) == 0, "Cannot fill trades with total quantity of [" + CoreMathUtils.formatNumberDecimal(totalTrades)
				+ "] with fills of quantity [" + CoreMathUtils.formatNumberDecimal(totalExternalFills) + "].");

		List<TradeFillResult> result = new ArrayList<>();
		// if there is only one trade set the allocation and skip the rest of the processing
		if (tradeList.size() == 1) {

			for (ExternalFillDetail detail : CollectionUtils.getIterable(detailList)) {
				result.add(new TradeFillResult(tradeList.get(0), detail.getExternalId(), detail.getQuantity(), detail.getPrice()));
			}
			return result;
		}

		List<String> accountNumbers = new ArrayList<>();
		// loop through the accounts
		for (ExternalFillDetail detail : CollectionUtils.getIterable(detailList)) {
			if (accountNumbers.contains(detail.getAccount())) {
				// only process the account once
				continue;
			}
			accountNumbers.add(detail.getAccount());

			// get the list of trades and allocations for the account
			List<ExternalFillDetail> accountDetailList = BeanUtils.filter(detailList, ExternalFillDetail::getAccount, detail.getAccount());
			List<Trade> accountTradeList = BeanUtils.filter(tradeList, trade -> trade.getHoldingInvestmentAccount() == null ? null : trade.getHoldingInvestmentAccount().getNumber(), detail.getAccount());

			// sort the lists of trades by qty and then price
			accountDetailList = BeanUtils.sortWithFunctions(accountDetailList, CollectionUtils.createList(
					ExternalFillDetail::getQuantity,
					ExternalFillDetail::getPrice),
					CollectionUtils.createList(true, true));
			accountTradeList = BeanUtils.sortWithFunction(accountTradeList, Trade::getQuantityIntended, true);

			if (accountTradeList.size() == 1) {
				for (ExternalFillDetail accountDetail : accountDetailList) {
					result.add(new TradeFillResult(accountTradeList.get(0), accountDetail.getExternalId(), accountDetail.getQuantity(), accountDetail.getPrice()));
				}
			}
			else if ((accountTradeList.size() > 1) && (!accountDetailList.isEmpty())) {
				Map<String, BigDecimal> sharesLeft = new HashMap<>();
				for (Trade accountTrade : accountTradeList) {
					//Trade accountTrade = accountTrades[j];
					double totalTradeFill = 0.0;
					for (ExternalFillDetail accountDetail : accountDetailList) {
						//TradeFillDetail accountDetail = accountDetails[i];
						if (!sharesLeft.containsKey(accountDetail.getExternalId())) {
							sharesLeft.put(accountDetail.getExternalId(), accountDetail.getQuantity());
						}
						if (sharesLeft.get(accountDetail.getExternalId()).doubleValue() > 0) {
							BigDecimal sharesLeftToFill = sharesLeft.get(accountDetail.getExternalId());
							BigDecimal sharesToFill;

							if (accountTrade.getQuantityIntended().subtract(BigDecimal.valueOf(totalTradeFill)).doubleValue() <= sharesLeftToFill.doubleValue()) {
								sharesToFill = accountTrade.getQuantityIntended().subtract(BigDecimal.valueOf(totalTradeFill));
							}
							else {
								sharesToFill = sharesLeftToFill;
							}
							totalTradeFill += sharesToFill.doubleValue();
							result.add(new TradeFillResult(accountTrade, accountDetail.getExternalId(), sharesToFill, accountDetail.getPrice()));
							sharesLeft.put(accountDetail.getExternalId(), sharesLeftToFill.subtract(sharesToFill));
						}
						if (totalTradeFill >= accountTrade.getQuantityIntended().doubleValue()) {
							// the trade is filled move to the next
							break;
						}
					}
				}
			}
		}
		return result;
	}
}
