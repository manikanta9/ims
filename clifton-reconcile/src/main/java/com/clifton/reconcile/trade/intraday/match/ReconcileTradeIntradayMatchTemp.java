package com.clifton.reconcile.trade.intraday.match;


import com.clifton.core.beans.ManyToManySimpleEntity;
import com.clifton.trade.Trade;


/**
 * The <code>ReconcileTradeIntradayMatchTemp</code>
 * Temporary record of a link between a trade and reconciliation row when the trade is unbooked.
 * Used for single fill trades only with a workflow we don't lose reconciliation/affirmation/confirmation history -->
 * TradeFillID in ReconcileTradeIntradayMatch table is set to NULL instead of deleting the row
 * and the mapping is stored here in the temp table
 * When the Trade is Re-booked, the reconciliation is then re-attached to the fill and removed from this temp table -->
 *
 * @author manderson
 */
public class ReconcileTradeIntradayMatchTemp extends ManyToManySimpleEntity<ReconcileTradeIntradayMatch, Trade, Integer> {

	// NOTHING HERE
}
