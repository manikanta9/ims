package com.clifton.reconcile.trade.intraday.external.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalStatuses;

import java.math.BigDecimal;
import java.util.Date;


public class ReconcileTradeIntradayExternalSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] idList;

	@SearchField
	private String externalUniqueTradeId;

	@SearchField
	private Boolean buy;

	@SearchField
	private Date tradeDate;

	@SearchField
	private BigDecimal quantity;

	@SearchField
	private BigDecimal price;

	@SearchField
	private BigDecimal originalPrice;

	@SearchField(searchField = "number", searchFieldPath = "holdingInvestmentAccount")
	private String accountNumber;

	@SearchField(searchField = "holdingInvestmentAccount.id")
	private Integer holdingInvestmentAccountId;

	@SearchField(searchField = "holdingInvestmentAccount.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] holdingInvestmentAccountIds;

	@SearchField(searchField = "investmentSecurity.id")
	private Integer securityId;

	@SearchField(searchField = "investmentSecurity.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean securityNotExists;

	@SearchField(searchField = "symbol", searchFieldPath = "investmentSecurity", comparisonConditions = {ComparisonConditions.BEGINS_WITH})
	private String symbol;

	@SearchField(searchField = "name,number", searchFieldPath = "holdingInvestmentAccount", sortField = "number")
	private String holdingInvestmentAccount;

	@SearchField(searchField = "name", searchFieldPath = "holdingInvestmentAccount.issuingCompany")
	private String holdingAccountIssuerName;

	@SearchField(searchField = "name", searchFieldPath = "sourceCompany")
	private String sourceCompanyName;

	@SearchField(searchField = "sourceCompany.id")
	private Integer sourceCompanyId;

	@SearchField(searchField = "executingBrokerCompany.id")
	private Integer executingCompanyId;

	@SearchField(searchField = "executingBrokerCompany.id", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean executingCompanyNotExists;

	@SearchField
	private ReconcileTradeIntradayExternalStatuses status;

	// Custom filter showing records with a holding account linked to this client account
	//	private Integer clientInvestmentAccountId;

	// Custom filter
	private Boolean reconciled;


	public Integer[] getIdList() {
		return this.idList;
	}


	public void setIdList(Integer[] idList) {
		this.idList = idList;
	}


	public String getExternalUniqueTradeId() {
		return this.externalUniqueTradeId;
	}


	public void setExternalUniqueTradeId(String externalUniqueTradeId) {
		this.externalUniqueTradeId = externalUniqueTradeId;
	}


	public Boolean getBuy() {
		return this.buy;
	}


	public void setBuy(Boolean buy) {
		this.buy = buy;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public BigDecimal getQuantity() {
		return this.quantity;
	}


	public void setQuantity(BigDecimal quantity) {
		this.quantity = quantity;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public String getAccountNumber() {
		return this.accountNumber;
	}


	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}


	public Integer getHoldingInvestmentAccountId() {
		return this.holdingInvestmentAccountId;
	}


	public void setHoldingInvestmentAccountId(Integer holdingInvestmentAccountId) {
		this.holdingInvestmentAccountId = holdingInvestmentAccountId;
	}


	public Integer[] getHoldingInvestmentAccountIds() {
		return this.holdingInvestmentAccountIds;
	}


	public void setHoldingInvestmentAccountIds(Integer[] holdingInvestmentAccountIds) {
		this.holdingInvestmentAccountIds = holdingInvestmentAccountIds;
	}


	public Integer getSecurityId() {
		return this.securityId;
	}


	public void setSecurityId(Integer securityId) {
		this.securityId = securityId;
	}


	public String getSymbol() {
		return this.symbol;
	}


	public void setSymbol(String symbol) {
		this.symbol = symbol;
	}


	public String getHoldingInvestmentAccount() {
		return this.holdingInvestmentAccount;
	}


	public void setHoldingInvestmentAccount(String holdingInvestmentAccount) {
		this.holdingInvestmentAccount = holdingInvestmentAccount;
	}


	public String getHoldingAccountIssuerName() {
		return this.holdingAccountIssuerName;
	}


	public void setHoldingAccountIssuerName(String holdingAccountIssuerName) {
		this.holdingAccountIssuerName = holdingAccountIssuerName;
	}


	public String getSourceCompanyName() {
		return this.sourceCompanyName;
	}


	public void setSourceCompanyName(String sourceCompanyName) {
		this.sourceCompanyName = sourceCompanyName;
	}


	public Integer getExecutingCompanyId() {
		return this.executingCompanyId;
	}


	public void setExecutingCompanyId(Integer executingCompanyId) {
		this.executingCompanyId = executingCompanyId;
	}


	public ReconcileTradeIntradayExternalStatuses getStatus() {
		return this.status;
	}


	public void setStatus(ReconcileTradeIntradayExternalStatuses status) {
		this.status = status;
	}


	public Boolean getExecutingCompanyNotExists() {
		return this.executingCompanyNotExists;
	}


	public void setExecutingCompanyNotExists(Boolean notExistsExecutingCompanyId) {
		this.executingCompanyNotExists = notExistsExecutingCompanyId;
	}


	//	public Integer getClientInvestmentAccountId() {
	//		return this.clientInvestmentAccountId;
	//	}
	//
	//
	//	public void setClientInvestmentAccountId(Integer clientInvestmentAccountId) {
	//		this.clientInvestmentAccountId = clientInvestmentAccountId;
	//	}


	public Boolean getSecurityNotExists() {
		return this.securityNotExists;
	}


	public void setSecurityNotExists(Boolean securityNotExists) {
		this.securityNotExists = securityNotExists;
	}


	public Boolean getReconciled() {
		return this.reconciled;
	}


	public void setReconciled(Boolean reconciled) {
		this.reconciled = reconciled;
	}


	public BigDecimal getOriginalPrice() {
		return this.originalPrice;
	}


	public void setOriginalPrice(BigDecimal originalPrice) {
		this.originalPrice = originalPrice;
	}


	public Integer getSourceCompanyId() {
		return this.sourceCompanyId;
	}


	public void setSourceCompanyId(Integer sourceCompanyId) {
		this.sourceCompanyId = sourceCompanyId;
	}
}
