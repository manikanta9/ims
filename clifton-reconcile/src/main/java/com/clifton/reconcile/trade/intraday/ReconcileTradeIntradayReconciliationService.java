package com.clifton.reconcile.trade.intraday;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.status.Status;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;


/**
 * The <code>ReconcileTradeIntradayReconciliationService</code> defines methods used to reconcile and match trades.
 *
 * @author mwacker
 */
public interface ReconcileTradeIntradayReconciliationService {

	/**
	 * Auto reconcile trades for a given date.
	 *
	 * @param date
	 * @param reconcileUnconfirmed
	 */
	@RequestMapping("reconcileTradeIntraday")
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public void autoReconcileTradeIntraday(Date date, boolean reconcileUnconfirmed);


	/**
	 * Auto reconcile trades for a given date as holding account.
	 *
	 * @param date
	 * @param reconcileUnconfirmed
	 */
	@RequestMapping("reconcileTradeIntradayByHoldingAccount")
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public void autoReconcileTradeIntradayByHoldingAccount(Date date, boolean reconcileUnconfirmed, Integer holdingInvestmentAccountId);


	/**
	 * Automatically reconciles trades where there are multiple trade fills that need to be reconciled to a single external trade or multiple external trades that
	 * compose an internal trade.
	 *
	 * @param date
	 * @param investmentAccountTypeNames holding accounts' investment account types to query for trades
	 */
	@RequestMapping("reconcileMatchMultiFillTrades")
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public Status reconcileMatchMultiFillTrades(String[] investmentAccountTypeNames,  Date date);


	/**
	 * Create and save a list of matches from the provided fill and external grouping lists.
	 *
	 * @param beanList
	 */
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public void saveReconcileTradeIntradayMatchObject(ReconcileTradeIntradayMatchObject beanList);


	/**
	 * Delete all matches associated with fills and external trades.
	 *
	 * @param beanList
	 */
	@SecureMethod(dtoClass = ReconcileTradeIntradayMatch.class)
	public void deleteReconcileTradeIntradayMatchObject(ReconcileTradeIntradayMatchObject beanList);
}
