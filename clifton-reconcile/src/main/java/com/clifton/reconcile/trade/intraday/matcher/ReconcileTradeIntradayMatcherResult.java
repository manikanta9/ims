package com.clifton.reconcile.trade.intraday.matcher;


import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ReconcileTradeIntradayMatcherResult</code> holds the trades to be booked/unbooked, trade fills to be
 * created/deleted and matches created when the matching process is complete.
 *
 * @author mwacker
 */
public class ReconcileTradeIntradayMatcherResult {

	private List<Trade> unbookTradeList = new ArrayList<>();

	private List<TradeFill> deleteTradeFillList = new ArrayList<>();
	private List<TradeFill> newTradeFillList = new ArrayList<>();

	private List<ReconcileTradeIntradayMatch> newMatchList = new ArrayList<>();


	public List<Trade> getUnbookTradeList() {
		return this.unbookTradeList;
	}


	public void setUnbookTradeList(List<Trade> unbookedTradeList) {
		this.unbookTradeList = unbookedTradeList;
	}


	public List<TradeFill> getDeleteTradeFillList() {
		return this.deleteTradeFillList;
	}


	public void setDeleteTradeFillList(List<TradeFill> deleteTradeFillList) {
		this.deleteTradeFillList = deleteTradeFillList;
	}


	public List<TradeFill> getNewTradeFillList() {
		return this.newTradeFillList;
	}


	public void setNewTradeFillList(List<TradeFill> newTradeFillList) {
		this.newTradeFillList = newTradeFillList;
	}


	public List<ReconcileTradeIntradayMatch> getNewMatchList() {
		return this.newMatchList;
	}


	public void setNewMatchList(List<ReconcileTradeIntradayMatch> newMatchList) {
		this.newMatchList = newMatchList;
	}
}
