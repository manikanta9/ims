package com.clifton.reconcile.trade.intraday.external;


public class ReconcileTradeIntradayExternalExtended extends ReconcileTradeIntradayExternal {

	private boolean reconciled;


	public boolean isReconciled() {
		return this.reconciled;
	}


	public void setReconciled(boolean reconciled) {
		this.reconciled = reconciled;
	}
}
