package com.clifton.reconcile.trade.settlement;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.trade.search.TradeSearchForm;


public class TradeSettlementSearchForm extends TradeSearchForm {

	@SearchField
	private Boolean finalSettlement;


	public Boolean getFinalSettlement() {
		return this.finalSettlement;
	}


	public void setFinalSettlement(Boolean finalSettlement) {
		this.finalSettlement = finalSettlement;
	}
}
