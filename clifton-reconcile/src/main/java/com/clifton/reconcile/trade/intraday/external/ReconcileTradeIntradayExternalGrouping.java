package com.clifton.reconcile.trade.intraday.external;


import com.clifton.core.util.CollectionUtils;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ReconcileTradeIntradayExternalGrouping</code> aggregates ReconcileTradeIntradayExternal
 * objects, grouped by price (and other attributes) and summed on "Quantity."  The totalTrades field
 * displays how many underlying objects have been rolled up into the grouping.
 *
 * @author rbrooks
 */
public class ReconcileTradeIntradayExternalGrouping extends ReconcileTradeIntradayExternalExtended {

	private Integer totalTrades;
	private List<ReconcileTradeIntradayExternal> externalTradeList;


	public boolean isGrouped() {
		return getTotalTrades() != null && getTotalTrades() > 1;
	}


	@Override
	public List<ReconcileTradeIntradayMatch> getMatchList() {
		List<ReconcileTradeIntradayMatch> matchList = super.getMatchList();
		if (getExternalTradeList() != null && matchList == null) {
			matchList = new ArrayList<>();
			for (ReconcileTradeIntradayExternal et : CollectionUtils.getIterable(getExternalTradeList())) {
				matchList.addAll(et.getMatchList());
			}
		}
		return matchList;
	}


	public Integer getTotalTrades() {
		return this.totalTrades;
	}


	public void setTotalTrades(Integer totalTrades) {
		this.totalTrades = totalTrades;
	}


	public List<ReconcileTradeIntradayExternal> getExternalTradeList() {
		return this.externalTradeList;
	}


	public void setExternalTradeList(List<ReconcileTradeIntradayExternal> externalTradeList) {
		this.externalTradeList = externalTradeList;
	}
}
