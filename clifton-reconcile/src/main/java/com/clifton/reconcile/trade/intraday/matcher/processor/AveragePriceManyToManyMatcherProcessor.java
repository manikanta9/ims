package com.clifton.reconcile.trade.intraday.matcher.processor;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternalGrouping;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatcherResult;
import com.clifton.reconcile.trade.intraday.matcher.util.ExternalFillDetail;
import com.clifton.reconcile.trade.intraday.matcher.util.ReconcileTradeIntradayUtils;
import com.clifton.reconcile.trade.intraday.matcher.util.TradeFillResult;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>AveragePriceMatcherProcessor</code> does an average price match when the number of external trades is == 1.  This will
 * remove any trade fills and create one trade fill that matches the external trade.
 * <p/>
 * NOTE: This method class assumes that the validation to check that this is a valid average price match is done prior to calling processTradeMatch.
 *
 * @author mwacker
 */
public class AveragePriceManyToManyMatcherProcessor implements ReconcileTradeIntradayMatcherProcessor {

	private int order;


	@Override
	public boolean isMatcherProcessorUsable(ReconcileTradeIntradayMatchObject beanList) {
		return beanList.isAveragePriceMatch() && (CollectionUtils.getSize(beanList.getIntradayExternalList()) > 1) && (CollectionUtils.getSize(beanList.getTradeFillList()) > 1);
	}


	@Override
	public void processTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		for (TradeFill fill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
			if (!result.getUnbookTradeList().contains(fill.getTrade())) {
				result.getUnbookTradeList().add(fill.getTrade());
			}
			if (!result.getDeleteTradeFillList().contains(fill)) {
				result.getDeleteTradeFillList().add(fill);
			}
		}
		if (CollectionUtils.isEmpty(beanList.getExternalGroupingList())) {
			doProcessExternalList(beanList, result, matchType);
		}
		else {
			doProcessExternalGroupingList(beanList, result, matchType);
		}
	}


	private void doProcessExternalList(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		List<ExternalFillDetail> fillDetailList = new ArrayList<>();
		Map<String, ReconcileTradeIntradayExternal> externalTradeMap = new HashMap<>();
		for (ReconcileTradeIntradayExternal external : CollectionUtils.getIterable(beanList.getIntradayExternalList())) {
			externalTradeMap.put(external.getExternalUniqueTradeId(), external);
			fillDetailList.add(new ExternalFillDetail(
					external.getExternalUniqueTradeId(),
					external.getHoldingInvestmentAccount() != null ? external.getHoldingInvestmentAccount().getNumber() : external.getHoldingInvestmentAccountNumber(),
					external.getPrice(), external.getQuantity())
			);
		}

		List<TradeFillResult> fillResultList = ReconcileTradeIntradayUtils.fillTrades(getTradeList(beanList.getTradeFillList()), fillDetailList);
		for (TradeFillResult fillResult : CollectionUtils.getIterable(fillResultList)) {
			// create the new fill
			TradeFill fill = new TradeFill();
			fill.setTrade(fillResult.getTrade());
			fill.setQuantity(fillResult.getQuantity());
			// update the price of the fill - always use the broker price because the average prices is already in the threshold
			fill.setNotionalUnitPrice(fillResult.getPrice());

			// add the new fill the new fill list
			result.getNewTradeFillList().add(fill);

			// create the match
			ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
			match.setMatchType(matchType);
			match.setReconcileTradeIntradayExternal(externalTradeMap.get(fillResult.getExternalId()));
			match.setContact(beanList.getContact());
			match.setNote(beanList.getNote());
			match.setConfirmed(matchType.isAutoConfirm());

			// set the new fill on the match
			match.setTradeFill(fill);

			// add the note
			StringBuilder note = new StringBuilder();
			if (!StringUtils.isEmpty(match.getNote())) {
				note.append(match.getNote()).append("\n\n");
			}
			note.append("Removing average price fill and matching to broker price [").append(CoreMathUtils.formatNumber(fillResult.getPrice(), null)).append("].");
			match.setNote(note.toString());

			result.getNewMatchList().add(match);
		}
	}


	private void doProcessExternalGroupingList(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		List<ExternalFillDetail> fillDetailList = CollectionUtils.getStream(beanList.getExternalGroupingList())
				.map(g -> new ExternalFillDetail(
						g.getExternalUniqueTradeId(),
						g.getHoldingInvestmentAccount() != null ? g.getHoldingInvestmentAccount().getNumber() : g.getHoldingInvestmentAccountNumber(),
						g.getPrice(),
						g.getQuantity())
				)
				.collect(Collectors.toList());
		Map<String, List<ReconcileTradeIntradayExternal>> externalTradeMap = CollectionUtils.getStream(beanList.getExternalGroupingList())
				.collect(Collectors.toMap(ReconcileTradeIntradayExternal::getExternalUniqueTradeId, ReconcileTradeIntradayExternalGrouping::getExternalTradeList));

		List<TradeFillResult> fillResultList = ReconcileTradeIntradayUtils.fillTrades(getTradeList(beanList.getTradeFillList()), fillDetailList);
		for (TradeFillResult fillResult : CollectionUtils.getIterable(fillResultList)) {
			// create the new fill
			TradeFill fill = new TradeFill();
			fill.setTrade(fillResult.getTrade());
			fill.setQuantity(fillResult.getQuantity());
			// update the price of the fill - always use the broker price because the average prices is already in the threshold
			fill.setNotionalUnitPrice(fillResult.getPrice());

			// add the new fill the new fill list
			result.getNewTradeFillList().add(fill);

			// externalId is the holding investment account number
			for (ReconcileTradeIntradayExternal external : externalTradeMap.get(fillResult.getExternalId())) {
				ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
				match.setMatchType(matchType);
				match.setReconcileTradeIntradayExternal(external);
				match.setContact(beanList.getContact());
				match.setNote(beanList.getNote());
				match.setConfirmed(matchType.isAutoConfirm());

				// set the new fill on the match
				match.setTradeFill(fill);

				// add the note
				StringBuilder note = new StringBuilder();
				if (!StringUtils.isEmpty(match.getNote())) {
					note.append(match.getNote()).append("\n\n");
				}
				note.append("Removing average price fill and matching to broker price [").append(CoreMathUtils.formatNumber(fillResult.getPrice(), null)).append("].");
				match.setNote(note.toString());

				result.getNewMatchList().add(match);
			}
		}
	}


	private List<Trade> getTradeList(List<TradeFill> tradeFills) {
		return CollectionUtils.getStream(tradeFills)
				.map(TradeFill::getTrade)
				.distinct()
				.collect(Collectors.toList());
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
