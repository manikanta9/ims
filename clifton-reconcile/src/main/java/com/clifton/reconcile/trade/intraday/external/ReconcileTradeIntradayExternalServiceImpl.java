package com.clifton.reconcile.trade.intraday.external;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalExtendedSearchForm;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalGroupingSearchForm;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalSearchForm;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchService;
import com.clifton.reconcile.trade.intraday.match.search.ReconcileTradeIntradayMatchSearchForm;
import com.clifton.workflow.definition.WorkflowStatus;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;


@Service
public class ReconcileTradeIntradayExternalServiceImpl implements ReconcileTradeIntradayExternalService {

	private AdvancedUpdatableDAO<ReconcileTradeIntradayExternal, Criteria> reconcileTradeIntradayExternalDAO;
	private AdvancedReadOnlyDAO<ReconcileTradeIntradayExternalExtended, Criteria> reconcileTradeIntradayExternalExtendedDAO;
	private AdvancedReadOnlyDAO<ReconcileTradeIntradayExternalGrouping, Criteria> reconcileTradeIntradayExternalGroupingDAO;

	private ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService;


	////////////////////////////////////////////////////////////////////////////
	////////       ReconcileTradeExternal Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public ReconcileTradeIntradayExternal getReconcileTradeExternal(int id) {
		ReconcileTradeIntradayExternal result = getReconcileTradeIntradayExternalDAO().findByPrimaryKey(id);
		result.setMatchList(getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByExternalTrade(result.getId()));
		return result;
	}


	@Override
	public List<ReconcileTradeIntradayExternal> getReconcileTradeExternalList(final ReconcileTradeIntradayExternalSearchForm searchForm) {
		List<ReconcileTradeIntradayExternal> result = getReconcileTradeIntradayExternalDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getReconciled() != null) {

					DetachedCriteria ne = DetachedCriteria.forClass(ReconcileTradeIntradayMatch.class, "m");
					ne.setProjection(Projections.property("id"));
					ne.createAlias("workflowStatus", "ws", JoinType.LEFT_OUTER_JOIN);
					ne.add(Restrictions.eqProperty("m.reconcileTradeIntradayExternal.id", criteria.getAlias() + ".id"));
					ne.add(Restrictions.or(Restrictions.isNull("ws.name"), Restrictions.ne("ws.name", WorkflowStatus.STATUS_OPEN)));
					if (searchForm.getReconciled()) {
						criteria.add(Subqueries.exists(ne));
					}
					else {
						criteria.add(Subqueries.notExists(ne));
					}
				}
			}
		});

		if (!CollectionUtils.isEmpty(result)) {
			ReconcileTradeIntradayMatchSearchForm matchSearchForm = new ReconcileTradeIntradayMatchSearchForm();
			Integer[] ids = BeanUtils.getBeanIdentityArray(result, Integer.class);
			matchSearchForm.setReconcileTradeIntradayExternalIdList(ids);
			List<ReconcileTradeIntradayMatch> matches = getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchList(matchSearchForm);
			Map<ReconcileTradeIntradayExternal, List<ReconcileTradeIntradayMatch>> map = BeanUtils.getBeansMap(matches, ReconcileTradeIntradayMatch::getReconcileTradeIntradayExternal);

			for (ReconcileTradeIntradayExternal et : CollectionUtils.getIterable(result)) {
				List<ReconcileTradeIntradayMatch> etMatches = map.get(et);
				if (etMatches == null) {
					etMatches = new ArrayList<>();
				}

				et.setMatchList(etMatches);
			}
		}
		return result;
	}


	@Override
	public List<ReconcileTradeIntradayExternal> getReconcileTradeExternalListForGroup(ReconcileTradeIntradayExternalGrouping group) {
		if (group.getTotalTrades() == 1) {
			return CollectionUtils.createList(getReconcileTradeExternal(group.getId()));
		}
		// Get a list of external trades that match the attributes of the basis trade
		ReconcileTradeIntradayExternalSearchForm tradeSearchForm = new ReconcileTradeIntradayExternalSearchForm();
		tradeSearchForm.setHoldingInvestmentAccountId(group.getHoldingInvestmentAccount().getId());
		if (group.getInvestmentSecurity() != null) {
			tradeSearchForm.setSecurityId(group.getInvestmentSecurity().getId());
		}
		else {
			tradeSearchForm.setSecurityNotExists(true);
		}
		tradeSearchForm.setPrice(group.getPrice());
		tradeSearchForm.setBuy(group.isBuy());
		tradeSearchForm.setTradeDate(group.getTradeDate());

		if (group.getExecutingBrokerCompany() != null) {
			tradeSearchForm.setExecutingCompanyId(group.getExecutingBrokerCompany().getId());
		}
		else {
			tradeSearchForm.setExecutingCompanyNotExists(true);
		}
		tradeSearchForm.setReconciled(group.isReconciled());

		return getReconcileTradeExternalList(tradeSearchForm);
	}


	@Override
	public void saveReconcileTradeIntradayExternal(ReconcileTradeIntradayExternal bean) {
		getReconcileTradeIntradayExternalDAO().save(bean);
	}


	@Override
	public void deleteReconcileTradeIntradayExternal(ReconcileTradeIntradayExternal bean) {
		getReconcileTradeIntradayExternalDAO().delete(bean);
	}


	@Override
	public void deleteReconcileTradeIntradayExternalList(List<ReconcileTradeIntradayExternal> beanList) {
		getReconcileTradeIntradayExternalDAO().deleteList(beanList);
	}


	////////////////////////////////////////////////////////////////////////////////////////////
	////////       ReconcileTradeIntradayExternalExtended Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ReconcileTradeIntradayExternalExtended> getReconcileTradeExternalExtendedList(final ReconcileTradeIntradayExternalExtendedSearchForm searchForm) {
		List<ReconcileTradeIntradayExternalExtended> result = getReconcileTradeIntradayExternalExtendedDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getReconciled() != null) {
					criteria.add(Restrictions.eq("reconciled", searchForm.getReconciled()));
				}
			}
		});
		for (ReconcileTradeIntradayExternalExtended et : CollectionUtils.getIterable(result)) {
			et.setMatchList(getReconcileTradeIntradayMatchService().getReconcileTradeIntradayMatchByExternalTrade(et.getId()));
		}
		return result;
	}


	@Override
	public List<ReconcileTradeIntradayExternalExtended> getReconcileTradeExternalExtendedListByGroupingId(int id) {
		ReconcileTradeIntradayExternalGrouping group = getReconcileTradeIntradayExternalGroupingDAO().findByPrimaryKey(id);

		ValidationUtils.assertNotNull(group, "One or more of the grouped trades are missing, please refresh and try again.");

		// Get a list of external trades that match the attributes of the basis trade
		ReconcileTradeIntradayExternalExtendedSearchForm tradeSearchForm = new ReconcileTradeIntradayExternalExtendedSearchForm();
		tradeSearchForm.setHoldingInvestmentAccountId(group.getHoldingInvestmentAccount().getId());
		if (group.getInvestmentSecurity() != null) {
			tradeSearchForm.setSecurityId(group.getInvestmentSecurity().getId());
		}
		else {
			tradeSearchForm.setSecurityNotExists(true);
		}
		tradeSearchForm.setPrice(group.getPrice());
		tradeSearchForm.setBuy(group.isBuy());
		tradeSearchForm.setTradeDate(group.getTradeDate());

		if (group.getExecutingBrokerCompany() != null) {
			tradeSearchForm.setExecutingCompanyId(group.getExecutingBrokerCompany().getId());
		}
		else {
			tradeSearchForm.setExecutingCompanyNotExists(true);
		}
		tradeSearchForm.setReconciled(group.isReconciled());

		// Validation of listSize = group.totalTrades here?

		return getReconcileTradeExternalExtendedList(tradeSearchForm);
	}


	////////////////////////////////////////////////////////////////////////////////////////////
	////////       ReconcileTradeIntradayExternalGrouping Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public ReconcileTradeIntradayExternalGrouping getReconcileTradeExternalGrouping(int id) {
		ReconcileTradeIntradayExternalGrouping result = getReconcileTradeIntradayExternalGroupingDAO().findByPrimaryKey(id);

		result.setExternalTradeList(getReconcileTradeExternalListForGroup(result));

		//ValidationUtils.assertTrue(result.getTotalTrades().equals(result.getExternalTradeList().size()),
		//        "The totalTrades property of the external trade group does not match the number of underlying trades.  Please refresh and try again.");

		return result;
	}


	@Override
	public List<ReconcileTradeIntradayExternalGrouping> getReconcileTradeExternalGroupingList(ReconcileTradeIntradayExternalGroupingSearchForm searchForm) {
		return getReconcileTradeExternalGroupingListInternal(searchForm, true);
	}


	@Override
	public List<ReconcileTradeIntradayExternalGrouping> getReconcileTradeExternalGroupingListInternal(ReconcileTradeIntradayExternalGroupingSearchForm searchForm, boolean populateExternalList) {
		List<ReconcileTradeIntradayExternalGrouping> result = getReconcileTradeIntradayExternalGroupingDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			protected void configureSearchForm() {
				if (!ArrayUtils.isEmpty(searchForm.getIdList())) {
					// set additional search properties based on the base ReconcileTradeIntradayExternal entity.
					ReconcileTradeIntradayExternalSearchForm externalSearchForm = new ReconcileTradeIntradayExternalSearchForm();
					externalSearchForm.setIdList(searchForm.getIdList());
					List<ReconcileTradeIntradayExternal> reconcileTradeIntradayExternalList = getReconcileTradeExternalList(externalSearchForm);
					Integer[] securityIds = BeanUtils.getPropertyValues(CollectionUtils.getFiltered(reconcileTradeIntradayExternalList, rec -> Objects.nonNull(rec.getInvestmentSecurity())),
							rec -> rec.getInvestmentSecurity().getId(), Integer.class);
					if (!ArrayUtils.isEmpty(securityIds)) {
						searchForm.setInvestmentSecurityIdList(securityIds);
					}
					Date[] tradeDates = BeanUtils.getPropertyValues(CollectionUtils.getFiltered(reconcileTradeIntradayExternalList, rec -> Objects.nonNull(rec.getTradeDate())),
							ReconcileTradeIntradayExternal::getTradeDate, Date.class);
					if (!ArrayUtils.isEmpty(tradeDates)) {
						searchForm.setTradeDateList(tradeDates);
					}
				}
				super.configureSearchForm();
			}
		});
		if (populateExternalList) {
			for (ReconcileTradeIntradayExternalGrouping group : CollectionUtils.getIterable(result)) {
				group.setExternalTradeList(getReconcileTradeExternalListForGroup(group));
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ReconcileTradeIntradayExternal, Criteria> getReconcileTradeIntradayExternalDAO() {
		return this.reconcileTradeIntradayExternalDAO;
	}


	public void setReconcileTradeIntradayExternalDAO(AdvancedUpdatableDAO<ReconcileTradeIntradayExternal, Criteria> reconcileTradeIntradayExternalDAO) {
		this.reconcileTradeIntradayExternalDAO = reconcileTradeIntradayExternalDAO;
	}


	public AdvancedReadOnlyDAO<ReconcileTradeIntradayExternalExtended, Criteria> getReconcileTradeIntradayExternalExtendedDAO() {
		return this.reconcileTradeIntradayExternalExtendedDAO;
	}


	public void setReconcileTradeIntradayExternalExtendedDAO(AdvancedReadOnlyDAO<ReconcileTradeIntradayExternalExtended, Criteria> reconcileTradeIntradayExternalExtendedDAO) {
		this.reconcileTradeIntradayExternalExtendedDAO = reconcileTradeIntradayExternalExtendedDAO;
	}


	public AdvancedReadOnlyDAO<ReconcileTradeIntradayExternalGrouping, Criteria> getReconcileTradeIntradayExternalGroupingDAO() {
		return this.reconcileTradeIntradayExternalGroupingDAO;
	}


	public void setReconcileTradeIntradayExternalGroupingDAO(AdvancedReadOnlyDAO<ReconcileTradeIntradayExternalGrouping, Criteria> reconcileTradeIntradayExternalGroupingDAO) {
		this.reconcileTradeIntradayExternalGroupingDAO = reconcileTradeIntradayExternalGroupingDAO;
	}


	public ReconcileTradeIntradayMatchService getReconcileTradeIntradayMatchService() {
		return this.reconcileTradeIntradayMatchService;
	}


	public void setReconcileTradeIntradayMatchService(ReconcileTradeIntradayMatchService reconcileTradeIntradayMatchService) {
		this.reconcileTradeIntradayMatchService = reconcileTradeIntradayMatchService;
	}
}
