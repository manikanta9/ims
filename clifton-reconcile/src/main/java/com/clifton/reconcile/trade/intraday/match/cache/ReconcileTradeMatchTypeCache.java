package com.clifton.reconcile.trade.intraday.match.cache;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;


public interface ReconcileTradeMatchTypeCache {

	public ReconcileTradeMatchType getReconcileTradeMatchTypeByClientAccount(ReadOnlyDAO<ReconcileTradeMatchType> dao, int clientInvestmentAccountId);
}
