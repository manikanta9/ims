package com.clifton.reconcile.trade.intraday.matcher.processor;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.reconcile.trade.intraday.external.ReconcileTradeIntradayExternal;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatch;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeIntradayMatchObject;
import com.clifton.reconcile.trade.intraday.match.ReconcileTradeMatchType;
import com.clifton.reconcile.trade.intraday.matcher.ReconcileTradeIntradayMatcherResult;
import com.clifton.trade.Trade;
import com.clifton.trade.TradeFill;
import com.clifton.core.util.MathUtils;


/**
 * The <code>OneExternalTradeMatcherProcessor</code> handles matching one external trade to one or many internal trade fills.
 *
 * @author mwacker
 */
public class OneExternalTradeMatcherProcessor implements ReconcileTradeIntradayMatcherProcessor {

	private int order;


	@Override
	public boolean isMatcherProcessorUsable(ReconcileTradeIntradayMatchObject beanList) {
		return CollectionUtils.getSize(beanList.getIntradayExternalList()) <= 1;
	}


	@Override
	public void processTradeMatch(ReconcileTradeIntradayMatchObject beanList, ReconcileTradeIntradayMatcherResult result, ReconcileTradeMatchType matchType) {
		if (CollectionUtils.getSize(beanList.getIntradayExternalList()) <= 1) {
			ReconcileTradeIntradayExternal external = CollectionUtils.getOnlyElement(beanList.getIntradayExternalList());
			for (TradeFill tradeFill : CollectionUtils.getIterable(beanList.getTradeFillList())) {
				ReconcileTradeIntradayMatch match = new ReconcileTradeIntradayMatch();
				match.setMatchType(matchType);
				match.setReconcileTradeIntradayExternal(external);
				match.setContact(beanList.getContact());
				match.setNote(beanList.getNote());
				match.setConfirmed(matchType.isAutoConfirm());

				if (beanList.isMatchBrokerPrice() && !MathUtils.isEqual(external.getPrice(), tradeFill.getNotionalUnitPrice())) {
					// 1. get the trade and add to the unbook list
					Trade trade = tradeFill.getTrade();
					if (!result.getUnbookTradeList().contains(trade)) {
						result.getUnbookTradeList().add(trade);
					}
					// 2. add the existing fill to the delete list
					if (!result.getDeleteTradeFillList().contains(tradeFill)) {
						result.getDeleteTradeFillList().add(tradeFill);
					}

					// 3. create the new fill
					TradeFill fill = new TradeFill();
					fill.setTrade(trade);
					fill.setQuantity(tradeFill.getQuantity());
					// 4. update the price of the fill
					fill.setNotionalUnitPrice(external.getPrice());

					// 5. add the new fill the new fill list
					result.getNewTradeFillList().add(fill);

					// 6. set the new fill on the match
					match.setTradeFill(fill);

					if (StringUtils.isEmpty(match.getNote())) {
						match.setNote("Matched to broker price of " + CoreMathUtils.formatNumberDecimal(external.getPrice()) + " from " + CoreMathUtils.formatNumberDecimal(tradeFill.getNotionalUnitPrice()));
					}
				}
				else {
					match.setTradeFill(tradeFill);
				}
				result.getNewMatchList().add(match);
			}
		}
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
