package com.clifton.reconcile.trade.intraday.external;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalExtendedSearchForm;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalGroupingSearchForm;
import com.clifton.reconcile.trade.intraday.external.search.ReconcileTradeIntradayExternalSearchForm;

import java.util.List;


public interface ReconcileTradeIntradayExternalService {

	////////////////////////////////////////////////////////////////////////////
	////////       ReconcileTradeExternal Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////
	public ReconcileTradeIntradayExternal getReconcileTradeExternal(int id);


	public List<ReconcileTradeIntradayExternal> getReconcileTradeExternalList(ReconcileTradeIntradayExternalSearchForm searchForm);


	public List<ReconcileTradeIntradayExternal> getReconcileTradeExternalListForGroup(ReconcileTradeIntradayExternalGrouping group);


	@DoNotAddRequestMapping
	public void saveReconcileTradeIntradayExternal(ReconcileTradeIntradayExternal bean);


	@DoNotAddRequestMapping
	public void deleteReconcileTradeIntradayExternal(ReconcileTradeIntradayExternal bean);


	@DoNotAddRequestMapping
	public void deleteReconcileTradeIntradayExternalList(List<ReconcileTradeIntradayExternal> beanList);


	//////////////////////////////////////////////////////////////////////////////////////////////
	////////        ReconcileTradeIntradayExternalExtended Business Methods          /////////////
	//////////////////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ReconcileTradeIntradayExternal.class)
	public List<ReconcileTradeIntradayExternalExtended> getReconcileTradeExternalExtendedList(ReconcileTradeIntradayExternalExtendedSearchForm searchForm);


	@SecureMethod(dtoClass = ReconcileTradeIntradayExternal.class)
	public List<ReconcileTradeIntradayExternalExtended> getReconcileTradeExternalExtendedListByGroupingId(int id);


	////////////////////////////////////////////////////////////////////////////////////////////
	////////       ReconcileTradeIntradayExternalGrouping Business Methods         /////////////
	////////////////////////////////////////////////////////////////////////////////////////////


	@SecureMethod(dtoClass = ReconcileTradeIntradayExternal.class)
	public ReconcileTradeIntradayExternalGrouping getReconcileTradeExternalGrouping(int id);


	@SecureMethod(dtoClass = ReconcileTradeIntradayExternal.class)
	public List<ReconcileTradeIntradayExternalGrouping> getReconcileTradeExternalGroupingList(ReconcileTradeIntradayExternalGroupingSearchForm searchForm);


	@DoNotAddRequestMapping
	public List<ReconcileTradeIntradayExternalGrouping> getReconcileTradeExternalGroupingListInternal(ReconcileTradeIntradayExternalGroupingSearchForm searchForm, boolean populateExternalList);
}
