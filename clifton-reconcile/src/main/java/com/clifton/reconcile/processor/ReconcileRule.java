package com.clifton.reconcile.processor;


import java.util.Map;


public interface ReconcileRule<T> {

	public void reconcile(T bean, Map<String, Object> context);


	public String getPropertyName();
}
