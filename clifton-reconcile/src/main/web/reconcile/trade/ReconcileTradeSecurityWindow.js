Clifton.reconcile.trade.ReconcileTradeSecurityWindow = Ext.extend(TCG.app.Window, {
	id: 'reconcileTradeWindow',
	titlePrefix: 'Traded Securities',
	iconCls: 'reconcile',
	width: 1000,

	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		win.on('beforeclose', function() {
			if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
				win.openerCt.reload();
			}
		});

		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}
	},

	items: [{
		name: 'reconcileTradeIntradaySecurityListFind',
		xtype: 'gridpanel',
		instructions: 'Our and external trades for selected holding account on selected date.',
		additionalPropertiesToRequest: 'id|label|clientInvestmentAccount.id|holdingInvestmentAccount.id|tradeDate|investmentSecurity.id',
		getLoadParams: function() {
			const data = this.getWindow().defaultData;
			if (data) {
				return {
					tradeDate: data.tradeDate,
					holdingInvestmentAccountId: data.holdingInvestmentAccountId
				};
			}
			return false;
		},

		columns: [
			{header: 'Holding Account', width: 140, dataIndex: 'holdingInvestmentAccount.name', filter: {searchFieldName: 'holdingInvestmentAccount'}},
			{header: 'Account Number', width: 45, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccount'}},
			{header: 'Holding Company', width: 60, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'holdingInvestmentAccountIssuerName'}},
			{header: 'Security', width: 40, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', url: 'investmentSecurityListFind.json'}, defaultSortColumn: true},
			{
				header: 'Trade Count', width: 42, dataIndex: 'tradeCount', type: 'int', tooltip: 'The number of trade fills we have accounted for internally',
				renderer: function(v, metaData, r) {
					const ours = r.data.tradeCount;
					const external = r.data.externalTradeCount;
					if (!metaData.isGrandTotal && ours !== external && !r.data.reconciled) {
						metaData.css = 'ruleViolationBig';
						let qtip = '<table>';
						qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000') + '</td></tr>';
						qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(ours - external, true, '0,000') + '</td></tr>';
						qtip += '</table>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return TCG.numberFormat(v, '0,000', true);
				}, summaryType: 'sum'
			},
			{
				header: 'External Trade Count', width: 55, dataIndex: 'externalTradeCount', type: 'int', hidden: true,
				tooltip: 'The number of trades from the external file(s)', summaryType: 'sum'
			},
			{
				header: 'Unreconciled Count', width: 55, dataIndex: 'unreconciledTradeCount', type: 'int', tooltip: 'The number of our trade fills that are unreconciled',
				renderer: function(v, metaData, r) {
					const ours = r.data.unreconciledTradeCount;
					const external = r.data.externalUnreconciledTradeCount;
					if (!metaData.isGrandTotal && ours !== external) {
						metaData.css = 'ruleViolationBig';
						let qtip = '<table>';
						qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000') + '</td></tr>';
						qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(ours - external, true, '0,000') + '</td></tr>';
						qtip += '</table>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return TCG.numberFormat(v, '0,000', true);
				}, summaryType: 'sum'
			},
			{
				header: 'External Unreconciled Count', width: 65, dataIndex: 'externalUnreconciledTradeCount', type: 'int', hidden: true,
				tooltip: 'The number of external trades that are unreconciled', summaryType: 'sum'
			},
			{header: 'Reconciled', width: 35, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
		],
		plugins: {ptype: 'gridsummary'},
		editor: {
			detailPageClass: 'Clifton.reconcile.trade.ReconcileTradeMatchWindow',
			addEnabled: false,
			deleteEnabled: false,
			addEditButtons: function(t) {
				TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

				const gridPanel = this.getGridPanel();
				const win = gridPanel.getWindow();

				t.add({
					text: 'Reconcile Unconfirmed',
					iconCls: 'run',
					handler: function() {
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							params: {
								date: win.defaultData.tradeDate,
								reconcileUnconfirmed: true,
								holdingInvestmentAccountId: win.defaultData.holdingInvestmentAccountId
							},
							onLoad: function(record, conf) {
								gridPanel.reload();
							}
						});
						loader.load('reconcileTradeIntradayByHoldingAccount.json');
					}
				});
			},
			getDefaultData: function(gridPanel, row) {
				const win = gridPanel.getWindow();
				// use selected row parameters for drill down filtering
				const params = {
					label: row.json.label,
					tradeDate: win.defaultData.tradeDate,
					holdingInvestmentAccountId: row.json.holdingInvestmentAccount.id
				};

				if (row.json.investmentSecurity) {
					params.investmentSecurityId = row.json.investmentSecurity.id;
				}

				return params;
			}
		}
	}]
});
