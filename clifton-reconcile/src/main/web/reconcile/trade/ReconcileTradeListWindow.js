Clifton.reconcile.trade.ReconcileTradeListWindow = Ext.extend(TCG.app.Window, {
	id: 'reconcileTradeWindow',
	title: 'Trade Reconciliation',
	iconCls: 'reconcile',
	width: 1500,
	height: 750,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		listeners: {
			// keep selected security, client account, and holding account (if available filter on tab)
			beforetabchange: function(tabPanel, newTab, currentTab) {
				if (currentTab) {
					const tb = currentTab.items.get(0).getTopToolbar();
					if (tb) {
						let o = TCG.getChildByName(tb, 'tradeTypeId');
						if (o && o.getValue()) {
							tabPanel.savedTradeType = {
								value: o.getValue(),
								text: o.getRawValue()
							};
						}
						else {
							tabPanel.savedTradeType = null;
						}
						o = TCG.getChildByName(tb, 'holdingInvestmentAccountTypeId');
						if (o && o.getValue()) {
							tabPanel.savedHoldingAccountType = {
								value: o.getValue(),
								text: o.getRawValue()
							};
						}
						else {
							tabPanel.savedHoldingAccountType = null;
						}
					}
				}
			},
			tabchange: function(tabPanel, tab) {
				const tb = tab.items.get(0).getTopToolbar();
				if (tb) {
					let o = TCG.getChildByName(tb, 'tradeTypeId');
					let s = tabPanel.savedTradeType;
					if (o) {
						if (s) {
							if (o.getValue() !== s.value) {
								o.setValue(s);
								o.fireEvent('select', o, s);
							}
						}
						else {
							o.reset();
						}
					}
					o = TCG.getChildByName(tb, 'holdingInvestmentAccountTypeId');
					s = tabPanel.savedHoldingAccountType;
					if (o) {
						if (s) {
							if (o.getValue() !== s.value) {
								o.setValue(s);
								o.fireEvent('select', o, s);
							}
						}
						else {
							o.reset();
						}
					}
				}
			}
		},
		items: [
			{
				title: 'Account Rec',
				items: [{
					name: 'reconcileTradeIntradayAccountListFind',
					isPagingEnabled: function() {
						return false;
					},
					remoteSort: true,
					xtype: 'gridpanel',
					instructions: 'Holding account level summary on selected date range for trades that have been reconciled between us and brokers through external files. Broker trades will not show up in our system until they get to MATCHED status (matched by the exchange).',
					wikiPage: 'IT/Workflow+-+Reconciliation%3A+Affirm%2C+Confirm-Ops%2C+Confirm-Docs',
					additionalPropertiesToRequest: 'id|label|clientInvestmentAccount.id|holdingInvestmentAccount.id|tradeDate',
					getTopToolbarFilters: function(toolbar) {
						return ['-',
							{
								fieldLabel: 'Client Account', xtype: 'combo', name: 'clientInvestmentAccountId', displayField: 'label', width: 180, url: 'investmentAccountListFind.json?ourAccount=true',
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('reconciled', false);
							// default to last 7 days of trades
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -7)});
						}
						// get value from client account filter
						const t = this.getTopToolbar();
						let clientInvestmentAccountId = null;
						const clientCombo = TCG.getChildByName(t, 'clientInvestmentAccountId');
						if (TCG.isNotBlank(clientCombo.getValue())) {
							clientInvestmentAccountId = (clientCombo.getValue());
						}
						return {
							clientInvestmentAccountId: clientInvestmentAccountId,
							readUncommittedRequested: true
						};
					},
					groupField: 'tradeDate',
					groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Accounts" : "Account"]}',
					columns: [
						{header: 'Trade Date', width: 33, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Holding Account', width: 140, dataIndex: 'holdingInvestmentAccount.name', filter: {searchFieldName: 'holdingInvestmentAccount'}},
						{header: 'Account Type', width: 33, dataIndex: 'holdingInvestmentAccount.type.name', filter: {searchFieldName: 'holdingInvestmentAccountTypeName'}},
						{header: 'Account Number', width: 40, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccount'}},
						{header: 'Holding Company', width: 70, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'holdingInvestmentAccountIssuerName'}},
						{
							header: 'Trade Count', width: 33, dataIndex: 'tradeCount', type: 'int', tooltip: 'The number of trade fills we have accounted for internally',
							renderer: function(v, metaData, r) {
								const ours = r.data.tradeCount;
								const external = r.data.externalTradeCount;
								if (!metaData.isGrandTotal && ours !== external) {
									metaData.css = 'ruleViolationBig';
									let qtip = '<table>';
									qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000') + '</td></tr>';
									qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000') + '</td></tr>';
									qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(ours - external, true, '0,000') + '</td></tr>';
									qtip += '</table>';
									metaData.attr = 'qtip=\'' + qtip + '\'';
								}
								return TCG.numberFormat(v, '0,000', true);
							},
							summaryType: 'sum'
						},
						{
							header: 'External Trade Count', width: 55, dataIndex: 'externalTradeCount', type: 'int', hidden: true,
							tooltip: 'The number of trades from the external file(s)', summaryType: 'sum'
						},
						{
							header: 'Unreconciled Count', width: 45, dataIndex: 'unreconciledTradeCount', type: 'int', tooltip: 'The number of our trade fills that are unreconciled',
							renderer: function(v, metaData, r) {
								const ours = r.data.unreconciledTradeCount;
								const external = r.data.externalUnreconciledTradeCount;
								if (!metaData.isGrandTotal && ours !== external) {
									metaData.css = 'ruleViolationBig';
									let qtip = '<table>';
									qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000') + '</td></tr>';
									qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000') + '</td></tr>';
									qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(ours - external, true, '0,000') + '</td></tr>';
									qtip += '</table>';
									metaData.attr = 'qtip=\'' + qtip + '\'';
								}
								return TCG.numberFormat(v, '0,000', true);
							},
							summaryType: 'sum'
						},
						{
							header: 'External Unreconciled Count', width: 65, dataIndex: 'externalUnreconciledTradeCount', type: 'int', hidden: true,
							tooltip: 'The number of external trades that are unreconciled', summaryType: 'sum'
						},
						{header: 'Reconciled', width: 30, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
					],
					plugins: {ptype: 'gridsummary'},
					editor: {
						detailPageClass: 'Clifton.reconcile.trade.ReconcileTradeSecurityWindow',
						drillDownOnly: true,
						addEditButtons: function(t, gridPanel) {
							TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

							const today = new Date().format('m/d/Y');
							t.add({name: 'reconcileForTradeDate', xtype: 'datefield', width: 80, value: today});

							t.add({
								text: 'Auto Reconcile (Confirmed/Unconfirmed)',
								tooltip: 'Reconcile all trades for selected date that have identical attributes.  After 3pm the status will be ignored (some securities never get confirmed).',
								iconCls: 'run',
								scope: t,
								handler: function() {
									let dateValue = null;
									const bd = TCG.getChildByName(this, 'reconcileForTradeDate');
									if (TCG.isNotBlank(bd.getValue())) {
										dateValue = (bd.getValue()).format('m/d/Y');
									}

									const loader = new TCG.data.JsonLoader({
										params: {
											date: dateValue,
											reconcileUnconfirmed: true
										},
										waitTarget: gridPanel,
										timeout: 120000,
										onLoad: function(record, conf) {
											gridPanel.reload();
										}
									});
									loader.load('reconcileTradeIntraday.json');
								}
							}, '-');
						},
						getDefaultData: function(gridPanel, row) {
							// use selected row parameters for drill down filtering
							return {
								label: row.json.label,
								tradeDate: TCG.parseDate(row.json.tradeDate).format('m/d/Y'),
								holdingInvestmentAccountId: row.json.holdingInvestmentAccount.id
							};
						}
					},
					gridConfig: {
						listeners: {
							'rowclick': function(grid, rowIndex, evt) {
								const row = grid.store.data.items[rowIndex];
								const td = TCG.parseDate(row.json.tradeDate).format('m/d/Y');
								const gridPanel = grid.ownerGridPanel;
								const rtd = TCG.getChildByName(gridPanel.getTopToolbar(), 'reconcileForTradeDate');
								if (rtd.getValue().format('m/d/Y') !== td) {
									rtd.setValue(td);
								}
							}
						}
					}
				}]
			},


			{
				title: 'Trade Rec',
				layout: {
					type: 'border',
					align: 'stretch'
				},
				items: [
					{
						name: 'reconcileTradeExtendedList',
						xtype: 'gridpanel',
						reloadOnRender: false, // avoid double loading: reloads on tab switch
						remoteSort: false,
						wikiPage: 'IT/Trade Reconciliation',
						additionalPropertiesToRequest: 'tradeId|clientInvestmentAccount.id|holdingInvestmentAccount.id|investmentSecurity.id',
						instructions: 'The following <b>un-reconciled</b> trade/trade fills exist in our system with associated reconcile match records.  Includes all trades with at least one fill, or single fill trades that were un-reconciled in order to unbook and update the trade.<br><br><b>NOTE: Trade Reconciliation has a hard start of 6/1/2013.  This tab will show nothing before this date.',
						groupField: 'tradeDate',
						groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Trades" : "Trade"]}',
						rowSelectionModel: 'multiple',
						viewNames: ['Default View', 'ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)'],

						region: 'center',
						layout: 'fit',
						flex: 1,

						getTopToolbarFilters: function(toolbar) {
							return ['-',
								{
									fieldLabel: 'Security', xtype: 'combo', name: 'investmentSecurityId', displayField: 'label', width: 120, url: 'investmentSecurityListFind.json',
									linkedFilter: 'investmentSecurity.symbol',
									listeners: {
										select: function(field) {
											TCG.getParentByClass(field, Ext.Panel).reload();
										}
									}
								},
								{
									fieldLabel: 'Trade Type', xtype: 'combo', name: 'tradeTypeId', displayField: 'name', width: 120, url: 'tradeTypeListFind.json',
									linkedFilter: 'tradeType.name',
									listeners: {
										select: function(field) {
											TCG.getParentByClass(field, Ext.Panel).reload();
										}
									}
								},
								{
									fieldLabel: 'Account Type', xtype: 'combo', name: 'holdingInvestmentAccountTypeId', displayField: 'label', width: 120, url: 'investmentAccountTypeListFind.json',
									linkedFilter: 'holdingInvestmentAccount.type.name',
									listeners: {
										select: function(field) {
											TCG.getParentByClass(field, Ext.Panel).reload();
										}
									}
								}
							];
						},
						switchToViewBeforeReload: function(viewName) {
							const tt = TCG.getChildByName(this.getTopToolbar(), 'tradeTypeId');

							if (viewName === 'ALERT - Equities (Funds)') {
								const type = TCG.data.getData('tradeTypeByName.json?name=Funds', this, 'trade.type.Funds');
								tt.setValue({text: type.name, value: type.id});
								tt.fireEvent('select', tt);
							}
							else if (viewName === 'ALERT - Fixed Income (Bonds)') {
								const type = TCG.data.getData('tradeTypeByName.json?name=Bonds', this, 'trade.type.Bonds');
								tt.setValue({text: type.name, value: type.id});
								tt.fireEvent('select', tt);
							}
							else {
								tt.reload(null, true);
								tt.fireEvent('blur', tt);
							}
							return true; // cancel reload: select even will reload
						},

						columns: [
							{header: 'Trade ID', width: 5, dataIndex: 'tradeId', hidden: true, type: 'int', useNull: true, filter: {searchFieldName: 'id'}},
							{header: 'Match ID', width: 5, dataIndex: 'matchId', hidden: true, type: 'int', useNull: true},
							{header: 'Unbooked', width: 20, dataIndex: 'unbooked', type: 'boolean', hidden: true},
							{header: 'Trade Type', width: 20, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json', showNotEquals: true}},
							{header: 'Trade Date', width: 22, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
							{
								header: 'Settled On', width: 22, dataIndex: 'settlementDate', searchFieldName: 'settlementDate',
								allViews: true, viewNameHeaders: [{name: 'Default View', label: 'Settled On'}, {name: 'ALERT - Equities (Funds)', label: 'Settlement Date'}, {name: 'ALERT - Fixed Income (Bonds)', label: 'Settlement Date'}]
							},
							{header: 'Recon State', width: 20, dataIndex: 'matchWorkflowState.name', filter: {searchFieldName: 'matchWorkflowStateName'}, viewNames: ['Default View']},
							{header: 'Recon Status', width: 30, dataIndex: 'matchWorkflowStatus.name', filter: {searchFieldName: 'matchWorkflowStatusName'}, hidden: true},
							{
								header: 'Trade State', width: 20, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View'],
								renderer: function(v, metaData, r) {
									if (TCG.isTrue(r.data['unbooked'])) {
										if (v === 'Executed' || v === 'Executed Valid') {
											metaData.css = 'amountAdjusted';
										}
										else {
											metaData.css = 'amountNegative';
										}
									}
									return v;
								}
							},
							{header: 'Client Account', width: 55, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['Default View']},
							{
								header: 'Holding Account ID', width: 20, dataIndex: 'holdingInvestmentAccount.id', type: 'int', numberFormat: '000', hidden: true, filter: {searchFieldName: 'holdingInvestmentAccountId'}, tooltip: 'Used by ALERT platform to help locate account-specific Standard Settlement Instructions (SSIs)',
								viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)'],
								viewNameHeaders: [{name: 'ALERT - Equities (Funds)', label: 'Alert Code'}, {name: 'ALERT - Fixed Income (Bonds)', label: 'Alert Code'}]
							},
							{header: 'Holding Account', width: 32, dataIndex: 'holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
							{header: 'Holding Company', width: 35, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
							{header: 'Executing Broker', width: 40, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
							{
								header: 'Buy/Sell', width: 15, dataIndex: 'buy', type: 'boolean',
								exportColumnValueConverter: 'tradeBuyColumnReversableConverter',
								renderer: function(v, metaData) {
									metaData.css = v ? 'buy-light' : 'sell-light';
									return v ? 'BUY' : 'SELL';
								}
							},
							{header: 'Original Face', width: 22, dataIndex: 'originalFace', type: 'float', hidden: true, useNull: true, viewNames: ['ALERT - Fixed Income (Bonds)']},
							{
								header: 'Quantity', width: 22, dataIndex: 'quantity', type: 'float', useNull: true,
								viewNames: ['Default View', 'ALERT - Equities (Funds)'], viewNameHeaders: [{name: 'Default View', label: 'Quantity'}, {name: 'ALERT - Equities (Funds)', label: 'Shares'}, {name: 'ALERT - Fixed Income (Bonds)', label: 'Face'}],
								renderer: function(value, metaData, r) {
									if (TCG.isTrue(r.data['unbooked'])) {
										metaData.attr = 'qtip=\'Expected Quantity From Trade - Fills Not Currently Booked.\'';
										metaData.css = 'amountAdjusted';
									}
									return TCG.numberFormat(value, '0,000.00', true);
								}
							},
							{header: 'Security', width: 35, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
							{
								header: 'Price', width: 20, dataIndex: 'price', type: 'float', useNull: true,
								renderer: function(value, metaData, r) {
									if (TCG.isTrue(r.data['unbooked'])) {
										metaData.attr = 'qtip=\'Expected Unit Price From Trade - Fills Not Currently Booked.\'';
										metaData.css = 'amountAdjusted';
									}
									return TCG.numberFormat(value, '0,000.00', true);
								}
							},
							{header: 'Notional', width: 20, dataIndex: 'accountingNotional', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Fixed Income (Bonds)'], viewNameHeaders: [{name: 'ALERT - Fixed Income (Bonds)', label: 'Principal'}]},
							{header: 'Accrual 1', width: 20, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
							{header: 'Accrual 2', width: 20, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
							{header: 'Accrual', width: 20, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Fixed Income (Bonds)']},
							{header: 'Commission Per Unit', width: 20, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
							{header: 'Commission', width: 20, dataIndex: 'commissionAmount', type: 'currency', hidden: true, viewNames: ['ALERT - Equities (Funds)'], tooltip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
							{header: 'Fee', width: 20, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Equities (Funds)']},
							{header: 'Net Payment', width: 20, dataIndex: 'netPayment', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)'], negativeInRed: true, positiveInGreen: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
							{header: 'Net Payment (Base)', width: 20, dataIndex: 'netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
							{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, viewNames: ['Default View']},
							{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
							{header: 'Account Type', width: 30, dataIndex: 'holdingInvestmentAccount.type.name', filter: {type: 'combo', searchFieldName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json'}, hidden: true},
							{header: 'Underlying Security', width: 40, dataIndex: 'investmentSecurity.underlyingSecurity.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
							{header: 'Trader', width: 16, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, viewNames: ['Default View']},
							/* Removing from columns: When taking this filter off, can kill browser trying to load because there is no paging supported for this list
							 * {header: 'Rec', width: 15, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
							 */
							{header: 'Confirmed', width: 15, dataIndex: 'confirmed', type: 'boolean', renderer: TCG.renderCheck, align: 'center', hidden: true},
							{
								header: 'Last Reconciliation Note', width: 100, dataIndex: 'reconciliationNote.text', hidden: true,
								tooltip: 'Last Updated Trade note associated with the trade for type <i>Trade Break</i> or <i>Reconciliation Note</i>',
								renderer: function(v, metaData, r) {
									if (TCG.isNotBlank(v)) {
										return TCG.renderText(v);
									}
								}
							}
						],
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -90)});
							}
							return {
								reconciled: false,
								readUncommittedRequested: true,
								excludeWorkflowStateName: 'Cancelled'
							};
						},
						listeners: {
							afterrender: function(gridPanel) {
								// set the grid to reload
								gridPanel.reloadRepeated(300000, -1);
								// set the context menu
								const el = gridPanel.getEl();
								el.on('contextmenu', function(e, target) {
									const g = gridPanel.grid;
									g.contextRowIndex = g.view.findRowIndex(target);
									e.preventDefault();
									if (!g.drillDownMenu) {
										g.drillDownMenu = new Ext.menu.Menu({
											items: [
												{
													text: 'Open Match', iconCls: 'reconcile',
													handler: function() {
														gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'MATCH');
													}
												},
												{
													text: 'Open Trade Rec', iconCls: 'reconcile',
													handler: function() {
														gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'TRADE-REC');
													}
												},
												{
													text: 'Open Trade', iconCls: 'shopping-cart',
													handler: function() {
														gridPanel.editor.openWindowFromContextMenu(g, g.contextRowIndex, 'TRADE');
													}
												}
											]
										});
									}
									TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
								}, gridPanel);
							}
						},

						editor: {
							drillDownOnly: true,
							getDetailPageClass: function(grid, row) {
								if (row.json.matchId) {
									return 'Clifton.reconcile.trade.MatchWindow';
								}
								return 'Clifton.reconcile.trade.ReconcileTradeMatchWindow';
							},
							getDetailPageId: function(gridPanel, row) {
								if (row.json.matchId) {
									return row.json.matchId;
								}
								return undefined;
							},

							// Override because have undefined id, but drill down goes directly to trade rec window
							startEditing: function(grid, rowIndex, evt) {
								const gridPanel = this.getGridPanel();
								const row = grid.store.data.items[rowIndex];
								const clazz = this.getDetailPageClass(grid, row);
								const id = this.getDetailPageId(grid, row);
								this.openDetailPage(clazz, gridPanel, id, row);
							},
							openWindowFromContextMenu: function(grid, rowIndex, screen) {
								const gridPanel = this.getGridPanel();
								const row = grid.store.data.items[rowIndex];
								let clazz;
								let id = undefined;
								// Trade Window
								if (screen === 'TRADE') {
									id = row.json.tradeId;
									clazz = 'Clifton.trade.TradeWindow';
								}
								else if (screen === 'MATCH') {
									const matchId = row.json.matchId;
									if (matchId) {
										id = matchId;
										clazz = 'Clifton.reconcile.trade.MatchWindow';
									}
									else {
										TCG.showError('There is not an existing match record for that Trade.  To create one, open the Trade Rec window.');
										return;
									}
								}
								// Otherwise - opens Trade Rec window
								else {
									clazz = 'Clifton.reconcile.trade.ReconcileTradeMatchWindow';
								}
								if (clazz) {
									this.openDetailPage(clazz, gridPanel, id, row);
								}
							},
							getDefaultData: function(grid, row) {
								return {
									label: row.json.holdingInvestmentAccount.label + ' for ' + TCG.parseDate(row.json.tradeDate).format('m/d/Y') + ' [' + row.json.investmentSecurity.symbol + ']',
									investmentSecurityId: row.json.investmentSecurity.id,
									tradeDate: TCG.parseDate(row.json.tradeDate).format('m/d/Y'),
									holdingInvestmentAccountId: row.json.holdingInvestmentAccount.id

								};
							},
							addEditButtons: function(toolBar, gridPanel) {
								TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

								const gridEditor = this;

								toolBar.add({
									text: 'Add Note',
									tooltip: 'Add a note linked to selected trades (NOTE: All trades must be for the same Trade Type)',
									iconCls: 'pencil',
									scope: this,
									handler: function() {
										const grid = gridPanel.grid;
										const sm = grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select at least one trade to add note to.', 'No Row(s) Selected');
										}
										else {
											const ut = sm.getSelections();
											gridEditor.addTradeNote(ut, gridEditor, gridPanel);
										}
									}
								});
								toolBar.add('-');

								const today = new Date().format('m/d/Y');
								toolBar.add({name: 'reconcileForTradeDate', xtype: 'datefield', width: 80, value: today, qtip: 'The date for which the "Auto Match (Multiple Fills)" reconciliation will be run.'});

								toolBar.add({
									text: 'Auto Match (Multiple Fills)',
									tooltip: 'Automatically matches and confirms Futures trades where multiple internal trade fills exist that should match an external trade. Average Price is used to perform the matching. This should be run after initial reconciliation has been completed.',
									iconCls: 'run',
									scope: this,
									handler: function() {
										let reconcileDate = null;
										const reconcileForTradeDateField = TCG.getChildByName(toolBar, 'reconcileForTradeDate');
										if (TCG.isNotBlank(reconcileForTradeDateField)) {
											reconcileDate = (reconcileForTradeDateField.getValue()).format('m/d/Y');
										}
										TCG.data.getDataPromise('reconcileMatchMultiFillTrades.json', this, {params: {investmentAccountTypeNames: ['Futures Broker', 'Futures Broker (Hold)'], date: reconcileDate}, waitTarget: gridPanel, waitMsg: 'Processing...', timeout: 120000})
											.then(function(result, conf) {
												gridPanel.reload();
												TCG.createComponent('Clifton.core.StatusWindow', {title: 'Auto Match Multi-Fill Trade Reconciliation Completed', defaultData: {status: result}});
											});
									}
								}, '-');

								toolBar.add({
									text: 'Manual Match',
									tooltip: 'Create manual match records for selected trades (Will only create records if one manual match type fits).  If the match record already exists, will attempt to reconcile (workflow transition).',
									iconCls: 'reconcile',
									scope: this,
									handler: function() {
										const grid = gridPanel.grid;
										const sm = grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a row to manually match.', 'No Row(s) Selected');
										}
										else {
											const matchCount = 0;
											const ut = sm.getSelections();
											gridEditor.createManualMatch(ut, matchCount, gridEditor, gridPanel);
										}
									}
								});
								toolBar.add('-');

								toolBar.add({
									text: 'Re-Assign',
									tooltip: 'Reassign workflow for the selected entities if workflow did not previously apply or assignment has changed.',
									iconCls: 'workflow',
									scope: this,
									handler: function() {
										const grid = gridPanel.grid;
										const sm = grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a row to re-assign.', 'No Row(s) Selected');
										}
										else {
											const assignmentCount = 0;
											const ut = sm.getSelections();
											gridEditor.reassignRow(ut, assignmentCount, gridPanel, 'ReconcileTradeIntradayMatch');
										}
									}
								});
								toolBar.add('-');
							},
							addTradeNote: function(rows, editor, gridPanel) {
								Clifton.trade.addTradeNote(rows, editor, gridPanel, 'tradeId', 'tradeType.name', true);
							},
							createManualMatch: function(rows, matchCount, gridEditor, gridPanel) {
								const matchId = rows[matchCount].json.matchId;
								const tradeId = rows[matchCount].json.tradeId;
								// If a match already exists for the row - see if we can move forward "Reconcile Trade" transition
								// Otherwise attempt a Manual Match
								if (!tradeId) {
									matchCount++;
									if (matchCount === rows.length) { // refresh after all rows were transitioned
										gridPanel.reload();
									}
									else {
										// Move On to Next Row
										gridEditor.createManualMatch(rows, matchCount, gridEditor, gridPanel);
									}
								}
								else {
									let waitMessage = 'Creating Manual Match For Trade ' + tradeId;
									let params = {tradeId: tradeId};
									let url = 'reconcileTradeIntradayManualMatchForTradeSave.json';
									// If there is a match - then try to push it forward through the workflow (already "matched")
									if (matchId) {
										waitMessage = 'Attempting to Reconcile Trade for Match for Trade ' + tradeId;
										const transitions = TCG.data.getData('workflowTransitionNextListByEntity.json?tableName=ReconcileTradeIntradayMatch&id=' + matchId, gridPanel);
										let transition = null;
										// Note: Need to check Transition Name because different match records can go to different states, however they all use the same transition name "Reconcile Trade"
										for (let i = 0; i < transitions.length; i++) {
											if (transitions[i].name === 'Reconcile Trade') {
												transition = transitions[i];
												break;
											}
										}
										// If not a transition - just skip it
										if (transition) {
											params = {
												tableName: 'ReconcileTradeIntradayMatch',
												id: matchId,
												workflowTransitionId: transition.id,
												enableOpenSessionInView: true
											};
											url = 'workflowTransitionExecuteByTransition.json';
										}
										else {
											url = undefined;
										}
									}
									if (url) {
										const loader = new TCG.data.JsonLoader({
											waitMsg: waitMessage,
											waitTarget: gridPanel,
											params: params,
											onLoad: function(record, conf) {
												matchCount++;
												if (matchCount === rows.length) { // refresh after all rows were transitioned
													gridPanel.reload();
												}
												else {
													gridEditor.createManualMatch(rows, matchCount, gridEditor, gridPanel);
												}
											},
											onFailure: function() {
												// continue with the rest, even if one failed
												matchCount++;
												if (matchCount === rows.length) { // refresh after all rows were transitioned
													gridPanel.reload();
												}
												else {
													gridEditor.createManualMatch(rows, matchCount, gridEditor, gridPanel);
												}
											}
										});
										loader.load(url);
									}
									else {
										matchCount++;
										if (matchCount === rows.length) { // refresh after all rows were transitioned
											gridPanel.reload();
										}
										else {
											gridEditor.createManualMatch(rows, matchCount, gridEditor, gridPanel);
										}
									}
								}
							},
							reassignRow: function(rows, assignmentCount, gridPanel, tableName) {
								const gridEditor = this;
								const rowId = rows[assignmentCount].json.matchId;
								if (!rowId) {
									assignmentCount++;
									if (assignmentCount === rows.length) { // refresh after all rows were transitioned
										gridPanel.reload();
									}
									else {
										gridEditor.reassignRow(rows, assignmentCount, gridPanel, tableName);
									}
								}
								else {
									const loader = new TCG.data.JsonLoader({
										waitMsg: 'Executing Re-Assignment',
										waitTarget: gridPanel,
										params: {
											tableName: tableName,
											fkFieldId: rowId,

											enableOpenSessionInView: gridEditor.enableOpenSessionInView,
											requestedPropertiesToExclude: gridEditor.requestedPropertiesToExclude,
											requestedMaxDepth: gridEditor.requestedMaxDepth
										},
										onLoad: function(record, conf) {
											assignmentCount++;
											if (assignmentCount === rows.length) { // refresh after all rows were transitioned
												gridPanel.reload();
											}
											else {
												gridEditor.reassignRow(rows, assignmentCount, gridPanel, tableName);
											}
										}
									});
									const url = 'workflowReassign.json';
									loader.load(url);
								}
							}

						}
					},

					{
						xtype: 'tradeOrder-activeTradeOrderGrid',
						title: 'FIX Orders with Allocation Rejects',
						reloadOnRender: false, // avoid double loading: reloads on tab switch
						layout: 'fit',
						flex: 2,
						height: 175,
						region: 'south',
						split: true,
						collapsible: true,
						isPagingEnabled: function() {
							return false;
						},
						listeners: {
							afterrender: function(gridPanel) {
								gridPanel.reloadRepeated(300000, -1);
							}
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								this.setFilterValue('allocationStatus.allocationStatus', ['BLOCK_LEVEL_REJECT']);
								this.grid.filters.getFilter('allocationStatus.allocationStatus').setActive(true, true);
							}
							return this.getStandardLoadParams();
						}
					}
				]
			},


			{
				title: 'Confirmation (Ops)',
				items: [{
					xtype: 'reconcile-trade-match-grid',
					workflowStateNames: ['Pending Operations Approval', 'Pending Confirmation', 'Revised Confirm - Pending Approval'],
					workflowStatusName: 'Review',
					alwaysShowWorkflowStateColumn: true,
					includeNoWorkflowUnconfirmed: true,
					columnOverrides: [
						{dataIndex: 'workflowStatus.name', hidden: true},
						{dataIndex: 'workflowState.name', hidden: true, viewNames: undefined},
						{dataIndex: 'tradeFill.trade.holdingInvestmentAccount.number', hidden: true},
						{dataIndex: 'tradeFill.trade.settlementDate', hidden: false, viewNames: ['Default View', 'ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)']},
						{dataIndex: 'tradeFill.trade.investmentSecurity.underlyingSecurity.symbol', hidden: false, viewNames: ['Default View']},
						{dataIndex: 'tradeFill.trade.accountingNotional', hidden: false, viewNames: ['Default View', 'ALERT - Fixed Income (Bonds)']},
						{dataIndex: 'internalTradeNote', hidden: false, viewNames: ['Default View']},
						{dataIndex: 'externalTradeNote', hidden: false, viewNames: ['Default View']},
						{dataIndex: 'updateUserId', hidden: false, viewNames: ['Default View']},
						{dataIndex: 'updateDate', hidden: false, viewNames: ['Default View']}
					],
					instructions: 'The following trades have been reconciled and are waiting for operations confirmation/approval. Note: By default only shows those awaiting action from Operations, can also see those pending confirmation action by Documentation team by checking the <i>Include Documentation Team Confirmations</i> checkbox. These are trade matches in Review status, or those without a workflow that have been reconciled but not confirmed.  Not all trades require Ops confirmation or additional approval. This is determined based on the Match Type workflow (See Match Types tab for additional details).',
					appendAdditionalToolbarFilters: function(toolbar, filters) {
						filters.push('-');
						filters.push(
							{
								boxLabel: 'Include Documentation Team Confirmation(s)', xtype: 'checkbox', name: 'includeDocTeam', width: 90,
								//qtip: 'Leave uncheck to view only those waiting for action from Operations team.  Check this option to also see those pending action from Documentation team.',
								listeners: {
									check: function(field, checked) {
										const gp = TCG.getParentByClass(field, Ext.Panel);
										if (checked) {
											gp.workflowStateNames = undefined;
										}
										else {
											gp.workflowStateNames = ['Pending Operations Approval', 'Pending Confirmation'];
										}
										gp.reload();
									}
								}
							}
						);
						return filters;
					}
				}]
			},


			{
				title: 'Confirmation (Docs)',
				layout: {
					type: 'border',
					align: 'stretch'
				},
				items: [
					{
						region: 'north',
						split: true,
						layout: 'fit',
						flex: 1,
						height: 260,
						xtype: 'reconcile-trade-match-grid',
						workflowStatusName: 'Review',
						workflowStateNames: ['Pending Documentation Approval', 'Pending Document Signatures', 'Document Rejected'],
						instructions: 'The following trades have been reconciled and confirmed by Operations, but are awaiting final documentation approval.   Note: By default only shows those awaiting action from Documentation team. Not all trades require Documentation team approval. This is determined based on the Match Type workflow (See Match Types tab for additional details).',

						columnOverrides: [
							{dataIndex: 'note', hidden: false},
							{dataIndex: 'workflowStatus.name', hidden: true},
							{dataIndex: 'internalTradeNote', hidden: false, viewNames: ['Default View']},
							{dataIndex: 'externalTradeNote', hidden: false, viewNames: ['Default View']},
							{dataIndex: 'updateUserId', hidden: false, viewNames: ['Default View']},
							{dataIndex: 'updateDate', hidden: false, viewNames: ['Default View']}
						],

						appendAdditionalToolbarFilters: function(toolbar, filters) {
							filters.push('-');
							filters.push(
								{
									boxLabel: 'Include Operations Team Confirmation(s)', xtype: 'checkbox', name: 'includeOpsTeam', width: 90,
									listeners: {
										check: function(field, checked) {
											const gp = TCG.getParentByClass(field, Ext.Panel);
											if (checked) {
												// Still exclude Pending Confirmation since they never go to docs team
												gp.excludeWorkflowStateName = 'Pending Confirmation';
											}
											else {
												gp.workflowStateNames = ['Pending Documentation Approval', 'Pending Document Signatures', 'Document Rejected'];
											}
											gp.reload();
										}
									}
								}
							);
							return filters;
						}

					},

					{
						title: 'OTC Confirmations',
						region: 'center',
						layout: 'fit',
						flex: 2,
						xtype: 'gridpanel',
						instructions: 'The following lists the OTC Confirmation documents that are currently tracked/stored on the Client.',
						name: 'businessContractExtendedListFind',

						columns: [
							// Hidden Columns
							{header: 'ID', width: 25, dataIndex: 'id', hidden: true},
							{header: 'Client', width: 125, dataIndex: 'company.name', filter: {type: 'combo', searchFieldName: 'companyId', url: 'businessCompanyListFind.json?companyTypeName=Client'}},
							// Visible Columns
							{
								header: 'Format', width: 25, dataIndex: 'documentFileType', filter: false, align: 'center',
								renderer: function(ft, args, r) {
									return TCG.renderDocumentFileTypeIcon(ft);
								}
							},
							{
								header: 'Name', width: 175, dataIndex: 'name', filter: {searchFieldName: 'searchPattern'},
								renderer: function(v, metaData, r) {
									const note = r.data['description'];
									if (TCG.isNotBlank(note)) {
										const qtip = r.data['description'];
										metaData.css = 'amountAdjusted';
										metaData.attr = TCG.renderQtip(qtip);
									}
									return v;
								}
							},
							{header: 'Comments', hidden: true, width: 200, dataIndex: 'description', filter: false, sortable: false},
							{header: 'Workflow State', width: 85, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
							{header: 'Status', width: 50, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
							{header: 'Effective On', width: 48, dataIndex: 'effectiveDate'},
							{header: 'Doc Updated On', width: 65, dataIndex: 'documentUpdateDate'},
							{header: 'Doc Updated By', width: 55, dataIndex: 'documentUpdateUser'},
							// Hidden
							{header: 'Active', width: 50, dataIndex: 'active', hidden: true, type: 'boolean', filter: {searchFieldName: 'active'}, sortable: false},
							{header: 'Active Client', width: 50, dataIndex: 'client.active', hidden: true, type: 'boolean', filter: {searchFieldName: 'clientActive'}, sortable: false},
							{header: 'Signed On', width: 50, dataIndex: 'signedDate', hidden: true}
						],
						getTopToolbarFilters: function(toolbar) {
							return [
								{
									fieldLabel: 'Client', xtype: 'combo', name: 'companyId', width: 300, url: 'businessCompanyListFind.json?companyType=Client', linkedFilter: 'company.name',
									listeners: {
										select: function(field) {
											TCG.getParentByClass(field, Ext.Panel).reload();
										}
									}
								}
							];
						},
						getLoadParams: function(firstLoad) {
							if (firstLoad) {
								// default to active clients & contracts
								this.setFilterValue('workflowStatus.name', 'Pending');
								this.grid.filters.getFilter('workflowStatus.name').setActive(true, true);
							}
							return {contractTypeName: 'OTC Confirmations', requestedMaxDepth: 4};
						},
						editor: {
							ptype: 'documentAware-grideditor',
							tableName: 'BusinessContract',
							entityText: 'contract',

							detailPageClass: 'Clifton.business.contract.MainContractWindow',
							deleteURL: 'businessContractDelete.json',
							getDefaultData: function(gridPanel) { // defaults groupItem for the detail page
								return {
									contractType: TCG.data.getData('businessContractTypeByName.json?name=OTC Confirmations', gridPanel, 'business.contract.type.OTC Confirmation')
								};
							}
						}
					}
				]
			},


			{
				title: 'Settlement',
				items: [
					{
						name: 'reconcileTradeSettlementList',
						xtype: 'gridpanel',
						rowSelectionModel: 'multiple',
						columns: [
							{header: 'Trade ID', width: 30, dataIndex: 'id', hidden: true},
							{header: 'Trade Type', width: 20, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json', showNotEquals: true}},
							{header: 'Trade Date', width: 22, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
							{header: 'Settled On', width: 22, dataIndex: 'settlementDate', searchFieldName: 'settlementDate'},
							{header: 'Actual Settle', width: 22, dataIndex: 'actualSettlementDate', searchFieldName: 'actualSettlementDate'},
							{header: 'Final Settlement', width: 20, dataIndex: 'finalSettlement', type: 'boolean', filter: false},
							{header: 'Security', width: 35, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
							{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
							{header: 'Underlying Security', width: 40, dataIndex: 'investmentSecurity.underlyingSecurity.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'underlyingSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
							{header: 'Client Account', width: 55, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
							{header: 'Holding Account', width: 32, dataIndex: 'holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
							{header: 'Holding Company', width: 35, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
							{header: 'Executing Broker', width: 40, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}},
							{header: 'System Note', width: 50, dataIndex: 'systemNote.text', renderer: TCG.renderValueWithTooltip, tooltip: 'Populated from the Trade Note type "External Note - Ops"'},
							{header: 'Description', width: 50, dataIndex: 'description', hidden: true},
							{header: 'Account Type', width: 30, dataIndex: 'holdingInvestmentAccount.type.name', filter: {type: 'combo', searchFieldName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json'}, hidden: true},
							{header: 'Trader', width: 16, dataIndex: 'traderUser.label', hidden: true, filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}}
						],
						editor: {
							detailPageClass: 'Clifton.trade.TradeWindow',
							drillDownOnly: true,
							addEditButtons: function(t, gridPanel) {
								t.add(
									{
										text: 'Update Actual Settle ',
										tooltip: 'Update actual settle date on selected trades',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											const actualSettlementDateField = TCG.getChildByName(t, 'actualSettlementDate');
											if (actualSettlementDateField.getValue()) {
												const actualSettlementDate = (actualSettlementDateField.getValue()).format('m/d/Y');
												const grid = gridPanel.grid;
												const sm = grid.getSelectionModel();
												if (sm.getCount() === 0) {
													TCG.showError('Please select at least one trade.', 'No Row(s) Selected');
												}
												else {
													const trades = sm.getSelections();
													const tradeIdList = [];
													for (let i = 0; i < trades.length; i++) {
														tradeIdList[i] = trades[i].id;
													}
													const loader = new TCG.data.JsonLoader({
														root: 'data',
														waitMsg: 'Assigning actual settlement date to selected Trades',
														waitTarget: gridPanel,
														params: {
															tradeIds: tradeIdList,
															actualSettlementDate: actualSettlementDate
														},
														onLoad: function(record, conf) {
															TCG.createComponent('Clifton.core.StatusWindow', {
																defaultData: {status: record},
																savedSinceOpen: true,
																openerCt: gridPanel,
																reloadOpener: function() {
																	gridPanel.reload();
																}
															});
														}
													});
													loader.load('tradeActualSettlementDateSave.json');
												}
											}
											else {
												TCG.showError('Please enter an actual settlement date.', 'No Date Selected');
											}
										}
									},
									{
										name: 'actualSettlementDate', xtype: 'datefield', width: 80, qtip: 'The date for which the "Actual Settle Date" will be saved to the selected trades.'
									},
									'-',
									{
										text: 'Final Settlement',
										tooltip: 'For selected trades with actual settlement dates, book the reversals of accrued cash.',
										iconCls: 'run',
										scope: gridPanel,
										handler: function() {
											const sm = this.grid.getSelectionModel();
											if (sm.getCount() === 0) {
												TCG.showError('Please select at least one trade.', 'No Trade(s) Selected');
											}
											else {
												const trades = sm.getSelections();
												const tradeIdList = [];
												for (let i = 0; i < trades.length; i++) {
													tradeIdList[i] = trades[i].id;
												}
												TCG.createComponent('Clifton.reconcile.trade.FinalSettlementWindow', {
													defaultData: {tradeIds: tradeIdList},
													openerCt: this
												});
											}
										}
									},
									'-');
							}
						},
						getLoadParams: function(firstLoad) {
							return {
								excludeWorkflowStateName: 'Cancelled'
							};
						}
					}
				]
			},


			{
				title: 'History',
				items: [{
					xtype: 'reconcile-trade-match-grid',
					instructions: 'The following trade match history is in the system.  Anything marked as Confirmed has been confirmed by all parties.',
					columnOverrides: [{dataIndex: 'confirmed', hidden: false}, {dataIndex: 'note', hidden: false}, {dataIndex: 'workflowStatus.name', hidden: true}],
					setDefaultFilters: function(firstLoad) {
						if (firstLoad) {
							// Default to last 7 days
							this.setFilterValue('tradeFill.trade.tradeDate', {'after': new Date().add(Date.DAY, -7)});
						}
					}
				}]
			},


			{
				title: 'Active Trades',
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				items: [
					{
						xtype: 'trade-activeFixOrderGrid',
						border: 1,
						flex: 1,
						getLoadParams: function(firstLoad) {
							return this.getStandardLoadParams();
						}
					},
					{
						xtype: 'trade-nonFixActiveTradesGrid',
						getLoadParams: function(firstLoad) {
							return this.getStandardLoadParams();
						}
					}
				]
			},


			{
				title: 'Waiting Trades',
				items: [{
					name: 'reconcileTradeExtendedList',
					xtype: 'gridpanel',
					rowSelectionModel: 'checkbox',
					instructions: 'A list of all trades that have not been Booked to the General Ledger yet but have been completed and are ready to be Booked.  Includes trades in Executed or Unbooked state only.',
					columns: [
						{header: 'ID', width: 24, dataIndex: 'id', hidden: true},
						{header: 'Workflow Status', width: 28, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}, hidden: true},
						{header: 'Workflow State', width: 28, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Trade Type', width: 22, dataIndex: 'tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}},
						{header: 'Trade Group Type', width: 22, hidden: true, dataIndex: 'tradeGroup.tradeGroupType.name', filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}},
						{header: 'Destination', width: 22, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, hidden: true},
						{header: 'Client', width: 80, dataIndex: 'clientInvestmentAccount.businessClient.name', filter: {searchFieldName: 'businessClientName'}, hidden: true},
						{header: 'Client Account', width: 80, dataIndex: 'clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}},
						{header: 'Holding Account', width: 80, dataIndex: 'holdingInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Holding Company', width: 60, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Executing Broker', width: 60, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true},
						{header: 'Description', width: 100, dataIndex: 'description', hidden: true},
						{
							header: 'Buy/Sell', width: 16, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{header: 'Original Face', width: 20, dataIndex: 'originalFace', type: 'float', useNull: true, hidden: true},
						{header: 'Quantity', width: 20, dataIndex: 'quantityIntended', type: 'float', useNull: true},
						{header: 'Notional Multiplier', width: 20, dataIndex: 'notionalMultiplier', type: 'float', useNull: true, hidden: true},
						{header: 'Price Multiplier', width: 30, dataIndex: 'investmentSecurity.priceMultiplier', type: 'float', hidden: true, filter: {searchFieldName: 'priceMultiplier'}},
						{header: 'Security', width: 30, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Security Name', width: 80, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'Investment Hierarchy', width: 80, dataIndex: 'investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true},
						{
							header: 'CCY Denomination', width: 30, dataIndex: 'investmentSecurity.instrument.tradingCurrency.symbol', hidden: true,
							filter: {
								type: 'combo', searchFieldName: 'tradingCurrencyId', showNotEquals: true, displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'
							}
						},
						{
							header: 'Settlement CCY', width: 30, dataIndex: 'payingSecurity.symbol', hidden: true,
							filter: {
								type: 'combo', searchFieldName: 'payingSecurityId', showNotEquals: true, displayField: 'label', url: 'investmentSecurityListFind.json?currency=true'
							}
						},
						{header: 'Exchange Rate', width: 24, dataIndex: 'exchangeRateToBase', type: 'float', useNull: true, hidden: true},
						{header: 'Average Price', width: 28, dataIndex: 'averageUnitPrice', type: 'float', useNull: true},
						{header: 'Expected Price', width: 28, dataIndex: 'expectedUnitPrice', type: 'float', useNull: true, hidden: true, tooltip: 'Market price used in portfolio analytics and pre-trade compliance before the trade was submitted for execution.'},
						{header: 'Accounting Notional', width: 36, dataIndex: 'accountingNotional', type: 'currency', useNull: true},
						{header: 'Accrual 1', width: 20, dataIndex: 'accrualAmount1', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual 2', width: 20, dataIndex: 'accrualAmount2', type: 'currency', hidden: true, useNull: true},
						{header: 'Accrual', width: 20, dataIndex: 'accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Commission Per Unit', width: 20, dataIndex: 'commissionPerUnit', type: 'float', hidden: true},
						{header: 'Commission Amount', width: 20, dataIndex: 'commissionAmount', type: 'currency', hidden: true},
						{header: 'Fee', width: 20, dataIndex: 'feeAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Trader', width: 24, dataIndex: 'traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Team', width: 24, dataIndex: 'clientInvestmentAccount.teamSecurityGroup.name', filter: {type: 'combo', searchFieldName: 'teamSecurityGroupId', url: 'securityGroupTeamList.json', loadAll: true}, hidden: true},
						{header: 'Collateral', width: 20, dataIndex: 'collateralTrade', type: 'boolean', hidden: true},
						{header: 'FIX', width: 20, dataIndex: 'fixTrade', type: 'boolean', hidden: true},
						{header: 'Block', width: 20, dataIndex: 'blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Destination', width: 26, dataIndex: 'tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, hidden: true},
						{header: 'Traded On', width: 26, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Settled On', width: 26, dataIndex: 'settlementDate', hidden: true},
						{header: 'Rec', width: 10, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
					],
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Instrument Group', xtype: 'toolbar-combo', name: 'investmentGroupId', width: 150, url: 'investmentGroupListFind.json'},
							{fieldLabel: 'Client Acct Group', xtype: 'toolbar-combo', name: 'clientGroupName', hiddenName: 'clientInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'},
							{fieldLabel: 'Holding Acct Group', xtype: 'toolbar-combo', name: 'holdingGroupName', hiddenName: 'holdingInvestmentAccountGroupId', width: 150, url: 'investmentAccountGroupListFind.json'}
						];
					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 90 days of trades
							this.setFilterValue('tradeDate', {'after': new Date().add(Date.DAY, -90)});
						}

						const params = {
							workflowStateNames: ['Executed', 'Executed Valid', 'Unbooked'],
							readUncommittedRequested: true
						};
						const t = this.getTopToolbar();
						let grp = TCG.getChildByName(t, 'clientGroupName');
						if (TCG.isNotBlank(grp.getValue())) {
							params.clientInvestmentAccountGroupId = grp.getValue();
						}
						grp = TCG.getChildByName(t, 'holdingGroupName');
						if (TCG.isNotBlank(grp.getValue())) {
							params.holdingInvestmentAccountGroupId = grp.getValue();
						}
						grp = TCG.getChildByName(t, 'investmentGroupId');
						if (TCG.isNotBlank(grp.getValue())) {
							params.investmentGroupId = grp.getValue();
						}
						return params;
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true,
						addEditButtons: function(t, gridPanel) {
							t.add({
								text: 'Book and Post',
								tooltip: 'Book and Post to the General Ledger trades from selected list that are in Executed workflow state.',
								iconCls: 'run',
								scope: gridPanel,
								handler: function() {
									const sm = this.grid.getSelectionModel();
									if (sm.getCount() === 0) {
										TCG.showError('Please select at least one trade.', 'No Trade(s) Selected');
									}
									else {
										const trades = sm.getSelections();
										const tradeIdList = [];
										for (let i = 0; i < trades.length; i++) {
											tradeIdList[i] = trades[i].id;
										}
										TCG.createComponent('Clifton.reconcile.trade.BookAndPostWindow', {
											defaultData: {tradeIds: tradeIdList},
											openerCt: this
										});
									}
								}
							}, '-');
						}
					}
				}]
			},


			{
				title: 'Match Types',
				items: [{
					name: 'reconcileTradeMatchTypeListFind',
					xtype: 'gridpanel',
					instructions: 'When matching our trades to external trades, one of the following match types must be selected.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Match Type Name', width: 50, dataIndex: 'name'},
						{header: 'Source', width: 20, dataIndex: 'source'},
						{header: 'Instrument Group', width: 50, dataIndex: 'investmentGroup.name', filter: {searchField: 'investmentGroupName'}},
						{header: 'Client Account Group', width: 50, dataIndex: 'clientInvestmentAccountGroup.name', filter: {searchField: 'clientInvestmentAccountGroupName'}},
						{header: 'Auto-Create Condition', width: 50, dataIndex: 'autoCreateManualMatchCondition.name', idDataIndex: 'autoCreateManualMatchCondition.id', filter: {searchField: 'autoCreateManualMatchConditionId', type: 'combo', url: 'systemConditionListFind.json'}},
						{header: 'Workflow', width: 50, dataIndex: 'workflow.name', searchFieldName: 'workflowName'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Days to Confirm', width: 30, dataIndex: 'daysToConfirm', type: 'int'},
						{header: 'Contact Required', width: 29, dataIndex: 'contactRequired', type: 'boolean'},
						{header: 'Note Required', width: 25, dataIndex: 'noteRequired', type: 'boolean'},
						{header: 'Attachment Required', width: 30, dataIndex: 'attachmentRequired', type: 'boolean'},
						{header: 'Auto Confirm', width: 20, dataIndex: 'autoConfirm', type: 'boolean'},
						{header: 'Auto Create Match', width: 20, dataIndex: 'autoCreateManualMatch', type: 'boolean'},
						{header: 'System', width: 18, dataIndex: 'systemDefined', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.reconcile.trade.MatchTypeWindow'
					}
				}]
			},


			{
				title: 'External Trades',
				items: [{
					name: 'reconcileTradeExternalExtendedListFind',
					xtype: 'gridpanel',
					instructions: 'The following trades have been received by our system from the broker. Only MATCHED (by the exchange) trades are loaded into our system.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Trade Date', xtype: 'toolbar-datefield', name: 'tradeDate'}
						];
					},
					additionalPropertiesToRequest: 'matchList.trade.id|matchList.tradeFill.trade.id',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holding Account', width: 70, dataIndex: 'holdingInvestmentAccount.name', filter: {searchFieldName: 'holdingInvestmentAccount'}},
						{header: 'Account Number', width: 31, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccount'}},
						{header: 'Holding Company', width: 40, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'holdingAccountIssuerName'}},
						{header: 'Executing Broker', width: 55, dataIndex: 'executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{
							header: 'Buy/Sell', width: 15, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{header: 'Qty', width: 16, dataIndex: 'quantity', type: 'float', useNull: true},
						{header: 'Security', width: 22, dataIndex: 'investmentSecurity.symbol', filter: {searchFieldName: 'symbol', comparison: 'BEGINS_WITH'}, defaultSortColumn: true},
						{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'Price', width: 25, dataIndex: 'price', type: 'float', useNull: true},
						{
							header: 'Status', width: 22, dataIndex: 'status', tooltip: 'If CONFIRMED, the trade has been confirmed by the exchange',
							filter: {
								type: 'list',
								options: [
									['CONFIRMED', 'CONFIRMED'],
									['UNCONFIRMED', 'UNCONFIRMED'],
									['PENDING_BROKER_ACCEPTANCE', 'PENDING_BROKER_ACCEPTANCE']
								]
							}
						},
						{header: 'Unique Trade Id', width: 40, dataIndex: 'externalUniqueTradeId'},
						{header: 'Source', width: 28, dataIndex: 'sourceCompany.name', filter: {searchFieldName: 'sourceCompanyName'}},
						{header: 'Reconciled', width: 15, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to last 90 days of trades
							this.setFilterValue('reconciled', false);
						}

						const t = this.getTopToolbar();
						const bd = TCG.getChildByName(t, 'tradeDate');
						if (TCG.isBlank(bd.getValue())) {
							bd.setValue(new Date().format('m/d/Y'));
						}
						const dateValue = (bd.getValue()).format('m/d/Y');
						return {
							tradeDate: dateValue
						};
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							if (!row.json.matchList) {
								TCG.showError('Drill down is only allowed for reconciled trades.', 'Unreconciled Trade');
								return undefined;
							}
							return row.json.matchList;
						},
						startEditing: function(grid, rowIndex, evt) {
							if (evt && TCG.isActionColumn(evt.target)) {
								return; // skip because this is action column
							}
							const gridPanel = this.getGridPanel();
							const row = grid.store.data.items[rowIndex];
							const clazz = this.getDetailPageClass(grid, row);
							if (clazz) {
								const matchList = this.getDetailPageId(gridPanel, row);
								let openedTrades = '';
								for (let i = 0; i < matchList.length; i++) {
									if (matchList[i].tradeFill) {
										const id = matchList[i].trade.id;
										if (!openedTrades.includes(':' + id)) {
											openedTrades += (':' + id);
											this.openDetailPage(clazz, gridPanel, id, row);
										}
									}
								}
							}
						}
					}
				}]
			},


			{
				title: 'Integration Trades',
				items: [{
					name: 'integrationTradeIntradayListFind',
					xtype: 'gridpanel',
					instructions: 'A list of every trade received from all brokers.  ' +
						'It will include duplicates for trades that come in on both the clearing and executing broker\'s file.  ' +
						'It can be used for cross-checking or manual matching.  Drill-downs will open the Integration File\'s detail page.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Trade Date', xtype: 'toolbar-datefield', name: 'tradeDate'}
						];
					},
					columns: [
						{header: 'ID', width: 40, dataIndex: 'id', type: 'string', hidden: true},
						{header: 'File Id', width: 30, dataIndex: 'run.file.id', hidden: true},
						{header: 'Account', width: 60, dataIndex: 'accountNumber'},
						{header: 'Executing Broker', width: 70, dataIndex: 'executingBrokerCompanyName'},
						{header: 'Clearing Broker', width: 70, dataIndex: 'clearingBrokerCompanyName'},
						{header: 'Investment Security', width: 80, dataIndex: 'investmentSecuritySymbol'},
						{header: 'Paying Security', width: 80, dataIndex: 'payingSecuritySymbol', hidden: true},
						{header: 'Symbol Type', width: 80, dataIndex: 'securitySymbolType', hidden: true},
						{
							header: 'B/S', width: 30, dataIndex: 'buy', type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{header: 'Quantity', width: 50, dataIndex: 'quantity', type: 'float'},
						{header: 'Trade Price', width: 50, dataIndex: 'price', type: 'float'},
						{
							header: 'Status',
							width: 50,
							dataIndex: 'status',
							filter: {
								type: 'list', options: [
									['MATCHED', 'MATCHED'],
									['UNMATCHED', 'UNMATCHED']]
							},
							tooltip: 'If MATCHED, then the trade has been confirmed by the exchange'
						},
						{header: 'Source', width: 60, dataIndex: 'run.file.fileDefinition.source.name', filter: {searchField: 'fileSourceName'}},
						{
							header: 'Type',
							width: 50,
							dataIndex: 'type',
							filter: {
								type: 'list', options: [
									['ADDED', 'ADDED'],
									['CHANGED', 'CHANGED'],
									['DELETED', 'DELETED']]
							},
							tooltip: 'Most cumulative files do not provide a Type flag, and are mapped to CHANGED automatically.  ' +
								'For these files, deleted trades simply do not show up.',
							hidden: true
						},
						{header: 'Unique Trade Id', width: 80, dataIndex: 'uniqueTradeId'}
					],
					editor: {
						detailPageClass: 'Clifton.integration.file.FileWindow',
						drillDownOnly: true,
						getDetailPageId: function(grid, row) {
							return row.json.run.file.id;
						}
					},
					getLoadParams: function(firstLoad) {
						const t = this.getTopToolbar();
						const bd = TCG.getChildByName(t, 'tradeDate');
						if (TCG.isBlank(bd.getValue())) {
							bd.setValue(new Date().format('m/d/Y'));
						}
						const dateValue = (bd.getValue()).format('m/d/Y');
						return {tradeDate: dateValue};
					}
				}]
			},


			{
				title: 'External Loads',
				items: [{
					xtype: 'integration-importRunEventGrid',
					targetApplicationName: 'IMS',
					importEventName: 'Broker Intraday Trades'
				}]
			},


			{
				title: 'Trade Instructions',
				items: [{
					xtype: 'investment-instruction-grid',
					tableName: 'Trade',
					defaultCategoryName: 'Trade Counterparty',
					defaultViewName: 'Group by Recipient'
				}]
			}
		]
	}]
});


Clifton.reconcile.trade.BookAndPostWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Book and Post',
	iconCls: 'book-red',
	height: 200,
	width: 510,
	modal: true,
	saveTimeout: 180,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Book selected trades and immediately post them to the General Ledger.',
		labelWidth: 10,
		items: [
			{boxLabel: 'Uncheck to also book and post trades that have not been reconciled yet', xtype: 'checkbox', name: 'reconciled', checked: true}
		]
	}],
	saveWindow: function() {
		const win = this;
		const fp = win.getMainFormPanel();
		const loader = new TCG.data.JsonLoader({
			root: 'status',
			waitMsg: 'Booking and Posting selected Trades',
			waitTarget: fp,
			params: {
				tradeIds: fp.getFormValue('tradeIds'),
				reconciled: fp.getForm().findField('reconciled').checked
			},
			onLoad: function(record, conf) {
				TCG.createComponent('Clifton.core.StatusWindow', {
					defaultData: {status: record}
				});
			}
		});
		loader.load('tradeBookAndPost.json');
	}
});

Clifton.reconcile.trade.FinalSettlementWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Final Settlement',
	iconCls: 'book-red',
	height: 150,
	width: 300,
	modal: true,
	saveTimeout: 180,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Perform final settlement on selected trades.',
		labelWidth: 10
	}],
	saveWindow: function() {
		const win = this;
		const fp = win.getMainFormPanel();
		const loader = new TCG.data.JsonLoader({
			root: 'data',
			waitMsg: 'Performing final settlement on selected Trades',
			waitTarget: fp,
			params: {
				tradeIds: fp.getFormValue('tradeIds')
			},
			onLoad: function(record, conf) {
				TCG.createComponent('Clifton.core.StatusWindow', {
					defaultData: {status: record}
				});
			}
		});
		loader.load('tradeFinalSettlement.json');
	}
});
