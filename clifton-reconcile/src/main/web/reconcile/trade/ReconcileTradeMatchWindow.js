Clifton.reconcile.trade.ReconcileTradeMatchWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Reconcile Match',
	iconCls: 'reconcile',
	width: 1200,

	reload: function() {
		this.reloadGrids();
	},
	reloadGrids: function() {
		const win = this;
		const tradeGrid = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
		const externalGrid = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
		const tradeModel = tradeGrid.grid.getSelectionModel();
		const externalModel = externalGrid.grid.getSelectionModel();

		tradeModel.clearSelections(true);
		externalModel.clearSelections(true);

		tradeGrid.reconciled = undefined;
		externalGrid.reconciled = undefined;

		tradeGrid.reload();
		externalGrid.reload();
	},
	deselectGridItems: function(record, tradeGrid, externalGrid) {
		const tradeModel = tradeGrid.grid.getSelectionModel();
		const externalModel = externalGrid.grid.getSelectionModel();
		if (record.json.reconciled) {

			tradeModel.suspendEvents(false);
			externalModel.suspendEvents(false);

			for (let i = 0; i < tradeGrid.grid.store.data.items.length; i++) {
				tradeModel.deselectRow(i);
			}
			for (let i = 0; i < externalGrid.grid.store.data.items.length; i++) {
				externalModel.deselectRow(i);
			}

			tradeModel.resumeEvents(false);
			externalModel.resumeEvents(false);

		}
		else if ((tradeModel.getCount() < 1) && (externalModel.getCount() < 1)) {

			tradeModel.clearSelections(true);
			externalModel.clearSelections(true);

			tradeGrid.reconciled = undefined;
			tradeGrid.reload();

			externalGrid.reconciled = undefined;
			externalGrid.reload();
		}
	},
	selectGridItems: function(record, tradeGrid, externalGrid) {
		// TODO: handle multiple selections on trade fill side
		if (!record.json.reconciled) {
			if (TCG.isNull(tradeGrid.reconciled)) {
				tradeGrid.reconciled = false;
				tradeGrid.reload();

				externalGrid.reconciled = false;
				externalGrid.reload();
			}
		}
		else {
			const tradeModel = tradeGrid.grid.getSelectionModel();
			const externalModel = externalGrid.grid.getSelectionModel();

			if (record.json.matchList) {
				const matchList = record.json.matchList;

				const fillSelectionList = [];
				let selectedItems = '';
				for (let j = 0; j < tradeGrid.grid.store.data.items.length; j++) {
					const fill = tradeGrid.grid.store.data.items[j].json;
					for (let i = 0; i < matchList.length; i++) {
						const match = matchList[i];
						if (match.tradeFill && fill.id === match.tradeFill.id) {
							if (!selectedItems.contains(j)) {
								fillSelectionList.push(j);
								selectedItems += j;
							}
						}
					}
				}

				const externalSelectionList = [];
				selectedItems = '';
				for (let j = 0; j < externalGrid.grid.store.data.items.length; j++) {
					const externalItem = externalGrid.grid.store.data.items[j].json;
					for (let i = 0; i < matchList.length; i++) {
						const match = matchList[i];
						if (match.reconcileTradeIntradayExternal && externalItem.id === match.reconcileTradeIntradayExternal.id) {
							if (!selectedItems.contains(j)) {
								externalSelectionList.push(j);
								selectedItems += j;
							}
						}
					}
				}

				tradeModel.suspendEvents(false);
				externalModel.suspendEvents(false);

				tradeModel.selectRows(fillSelectionList, false);
				externalModel.selectRows(externalSelectionList, false);

				tradeModel.resumeEvents();
				externalModel.resumeEvents();
			}

			if (TCG.isFalse(tradeGrid.reconciled)) {
				tradeGrid.reconciled = undefined;
				tradeGrid.reload();
			}
			if (TCG.isFalse(externalGrid.reconciled)) {
				externalGrid.reconciled = undefined;
				externalGrid.reload();
			}
		}
	},
	unMatchSelectedTrades: function() {
		const win = this;

		const fillGridPanel = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
		const fillSm = fillGridPanel.grid.getSelectionModel();

		const externalGridPanel = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
		const externalSm = externalGridPanel.grid.getSelectionModel();

		const params = {};
		for (let i = 0; i < fillSm.getSelections().length; i++) {
			params['tradeFillList[' + i + '].id'] = fillSm.getSelections()[i].id;
		}
		for (let i = 0; i < externalSm.getSelections().length; i++) {
			params['externalGroupingList[' + i + '].id'] = externalSm.getSelections()[i].id;
		}
		const waitTarget = TCG.getParentByClass(fillGridPanel, Ext.Panel);
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Matching Trades',
			waitTarget: waitTarget,
			params: params,
			onLoad: function(record, conf) {
				win.reloadGrids();
				win.matchChanges = true;
			}
		});
		loader.load('reconcileTradeIntradayMatchObjectDelete.json?requestedMaxDepth=4&requestedPropertiesRoot=data&requestedProperties=');
	},
	matchSelectedTrades: function(matchBrokerPrice, manualMatch, averagePriceThresholdPercent) {
		const win = this;

		const fillGridPanel = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
		const fillSm = fillGridPanel.grid.getSelectionModel();

		const externalGridPanel = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
		const externalSm = externalGridPanel.grid.getSelectionModel();


		const params = {};
		for (let i = 0; i < fillSm.getSelections().length; i++) {
			params['tradeFillList[' + i + '].id'] = fillSm.getSelections()[i].id;
		}
		for (let i = 0; i < externalSm.getSelections().length; i++) {
			params['externalGroupingList[' + i + '].id'] = externalSm.getSelections()[i].id;
		}
		if (matchBrokerPrice) {
			params.matchBrokerPrice = matchBrokerPrice;
		}
		if (manualMatch) {
			params.manualMatch = manualMatch;
		}
		if (averagePriceThresholdPercent) {
			params.averagePriceThresholdPercent = averagePriceThresholdPercent;
		}
		const waitTarget = TCG.getParentByClass(fillGridPanel, Ext.Panel);
		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Matching Trades',
			waitTarget: waitTarget,
			params: params,
			timeout: 240000,
			onLoad: function(record, conf) {
				win.reloadGrids();
				win.matchChanges = true;
			}
		});
		loader.load('reconcileTradeIntradayMatchObjectSave.json?resultKeysToExclude=data&requestedMaxDepth=3');
	},
	openManualMatch: function() {
		const win = this;

		const fillGrid = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
		const fillSm = fillGrid.grid.getSelectionModel();

		const externalGridPanel = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
		const externalSm = externalGridPanel.grid.getSelectionModel();

		if ((fillSm.getSelections().length === 0) && (externalSm.getSelections().length === 0)) {
			TCG.showError('Please select a trade fill or external trade to be matched manually.', 'No Fill Selected');
		}
		else {
			win.matchChanges = true;
			const fills = {};
			let fillTotalPrice = 0;
			for (let i = 0; i < fillSm.getSelections().length; i++) {
				fills['tradeFillList[' + i + '].id'] = fillSm.getSelections()[i].id;
				fillTotalPrice += fillSm.getSelections()[i].json.notionalUnitPrice * fillSm.getSelections()[i].json.quantity;
			}
			let extTotalPrice = externalSm.getSelections().length ? 0 : null;
			for (let i = 0; i < externalSm.getSelections().length; i++) {
				fills['externalGroupingList[' + i + '].id'] = externalSm.getSelections()[i].id;
				extTotalPrice += externalSm.getSelections()[i].json.price * externalSm.getSelections()[i].json.quantity;
			}

			const fill = fillSm.getSelections()[0] ? fillSm.getSelections()[0].json : externalSm.getSelections()[0].json;
			const contactCompanies =
				fill.trade ? [fill.trade.executingBrokerCompany.id, fill.trade.holdingInvestmentAccount.issuingCompany.id] :
					(fill.executingBrokerCompany ? [fill.executingBrokerCompany.id, fill.holdingInvestmentAccount.issuingCompany.id] : [fill.holdingInvestmentAccount.issuingCompany.id]);

			TCG.createComponent('Clifton.reconcile.trade.ManualMatchWindow', {
				defaultData: {
					id: (fill.matchList && fill.matchList[0]) ? fill.matchList[0].id : undefined,
					tradeFills: fills,
					contactCompanies: contactCompanies,
					investmentSecurity: fill.trade ? fill.trade.investmentSecurity : fill.investmentSecurity,
					noteRequired: !fill.trade || (extTotalPrice !== fillTotalPrice && TCG.isNotNull(extTotalPrice))
				},
				openerCt: win
			});
		}
	},
	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		win.on('beforeclose', function() {
			if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload && TCG.isTrue(win.matchChanges)) {
				win.openerCt.reload();
			}
		});

		let keymap = new Ext.KeyMap(win.getEl(), {
			key: 's',
			ctrl: true,
			stopEvent: true,
			fn: function() {
				win.matchSelectedTrades(false, false);
			}
		});

		keymap.ctrl = true;

		keymap = new Ext.KeyMap(win.getEl(), {
			key: 'b',
			ctrl: true,
			stopEvent: true,
			fn: function() {
				win.matchSelectedTrades(true, false, '0.00001');
			}
		});
		keymap.ctrl = true;

		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}


		const t = this.getTopToolbar();
		t.add({
			text: 'Reload',
			tooltip: 'Reload latest grid data',
			iconCls: 'table-refresh',
			handler: function() {
				win.reloadGrids();
			}
		});
		t.add('-');
		t.add({
			text: 'Actions',
			iconCls: 'run',
			menu: new Ext.menu.Menu({
				items: [{
					text: 'Match',
					tooltip: 'Match selected trades',
					iconCls: 'reconcile',
					handler: function() {
						win.matchSelectedTrades(false, false);
					}
				},

					{
						text: 'Match with Broker Price',
						tooltip: 'Match selected trades update the trade with the broker price.',
						iconCls: 'reconcile',
						handler: function() {
							win.matchSelectedTrades(true, false, '0.00001');
						}
					},

					{
						text: 'Manual Match',
						tooltip: 'Confirm the trade when external information in not available in our system for matching',
						iconCls: 'reconcile',
						handler: function() {
							win.openManualMatch();
						}
					}]
			})
		});
		t.add('-');
		t.add({
			text: 'Unmatch',
			tooltip: 'unmatch selected trades',
			iconCls: 'cancel',
			handler: function() {
				win.unMatchSelectedTrades();
			}
		});
		t.add('-');
	},

	tbar: [],
	items: [{
		xtype: 'panel',
		layout: {
			type: 'border',
			align: 'stretch'
		},
		frame: true,
		items: [{
			title: 'Our Trade Fills',
			name: 'reconcileTradeFillExtendedList',
			xtype: 'gridpanel',
			instructions: 'Approved trades have been validated, approved and are waiting for execution.',
			additionalPropertiesToRequest: 'id|trade.investmentSecurity.id|trade.executingBrokerCompany.id|trade.holdingInvestmentAccount.issuingCompany.id|matchList',
			region: 'west',
			split: true,
			layout: 'fit',
			flex: 2,
			width: 600,
			hideStandardButtons: true,
			rowSelectionModel: 'checkbox',

			//groupField: 'trade.id',
			getRowSelectionModel: function() {
				const win = this.getWindow();
				if (typeof this.rowSelectionModel !== 'object') {
					this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel({
						singleSelect: false,
						selectAll: function() {
							if (this.isLocked()) {
								return;
							}
							this.suspendEvents(false);
							this.selections.clear();
							for (let i = 0, len = this.grid.store.getCount(); i < len - 1; i++) {
								this.selectRow(i, true);
							}
							this.resumeEvents();
							this.selectRow(len - 1, true);
						},
						listeners: {

							'rowselect': function(model, rowIndex, record) {
								const tradeGrid = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
								const externalGrid = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
								win.selectGridItems(record, tradeGrid, externalGrid);
							},
							'rowdeselect': function(model, rowIndex, record) {
								const tradeGrid = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
								const externalGrid = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
								win.deselectGridItems(record, tradeGrid, externalGrid);
							}
						}
					});
				}
				return this.rowSelectionModel;
			},
			listeners: {
				afterrender: function(gridPanel) {
					const el = gridPanel.getEl();
					el.on('contextmenu', function(e, target) {
						const g = gridPanel.grid;
						g.contextRowIndex = g.view.findRowIndex(target);
						e.preventDefault();
						if (!g.drillDownMenu) {
							g.drillDownMenu = new Ext.menu.Menu({
								items: [
									{
										text: 'Match', iconCls: 'reconcile', handler: function() {
											gridPanel.getWindow().matchSelectedTrades(false, false);
										}
									},
									{
										text: 'Match with Broker Price', iconCls: 'reconcile', handler: function() {
											gridPanel.getWindow().matchSelectedTrades(true, false, '0.00001');
										}
									},
									{
										text: 'Manual Match', iconCls: 'reconcile', handler: function() {
											gridPanel.getWindow().openManualMatch();
										}
									},
									{
										text: 'Unmatch', iconCls: 'cancel', handler: function() {
											gridPanel.getWindow().unMatchSelectedTrades();
										}
									}
								]
							});
						}
						TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
					}, gridPanel);
				}
			},
			plugins: {ptype: 'gridsummary'},
			columns: [
				{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
				{header: 'Trade ID', width: 14, dataIndex: 'trade.id', defaultSortColumn: true},
				{header: 'Description', width: 50, dataIndex: 'tradeAllocationDescription', hidden: true},
				{
					header: 'Buy/Sell', width: 15, dataIndex: 'trade.buy', type: 'boolean',
					renderer: function(v, metaData) {
						metaData.css = v ? 'buy-light' : 'sell-light';
						return v ? 'BUY' : 'SELL';
					}
				},
				{header: 'Qty', width: 15, dataIndex: 'quantity', type: 'float', useNull: true, defaultSortColumn: true, summaryType: 'sum'},
				{header: 'Security', width: 15, dataIndex: 'trade.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
				{header: 'Security Name', width: 40, dataIndex: 'trade.investmentSecurity.name', hidden: true},
				{header: 'Price', width: 23, dataIndex: 'notionalUnitPrice', type: 'float', useNull: true},
				{header: 'Total Price', width: 23, dataIndex: 'notionalTotalPrice', type: 'float', useNull: true, hidden: true},
				{header: 'Payment Price', width: 23, dataIndex: 'paymentUnitPrice', type: 'float', useNull: true, hidden: true},
				{header: 'Total Payment Price', width: 30, dataIndex: 'paymentTotalPrice', type: 'float', useNull: true, hidden: true},
				{header: 'Executing Broker', width: 30, dataIndex: 'trade.executingBrokerCompany.name'},
				{header: 'Block', width: 15, dataIndex: 'trade.blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
				{header: 'Traded On', width: 15, dataIndex: 'trade.tradeDate', hidden: true},
				{header: 'Settled On', width: 12, dataIndex: 'settlementDate', searchFieldName: 'settlementDate', hidden: true},
				{header: 'Trader', width: 12, dataIndex: 'trade.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}, hidden: true},
				{
					header: 'State', width: 15, dataIndex: 'trade.workflowState.name',
					renderer: function(v, metaData, r) {
						if (r.json.trade.workflowState.name !== 'Booked' && r.json.trade.workflowState.name !== 'Executed' && r.json.trade.workflowState.name !== 'Executed Valid') {
							metaData.css = 'ruleViolation';
						}
						return v;
					}
				},
				{header: '', width: 9, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
			],
			getLoadParams: function() {
				const data = this.getWindow().defaultData;
				if (data) {
					return {
						readUncommittedRequested: true,
						enableOpenSessionInView: true,
						reconciled: this.reconciled,
						tradeDate: data.tradeDate,
						holdingInvestmentAccountId: data.holdingInvestmentAccountId,
						securityId: data.investmentSecurityId,
						excludeWorkflowStateName: 'Cancelled',
						requestedPropertiesToExclude: 'order',
						requestedMaxDepth: 4

					};
				}
				return false;
			},
			editor: {
				detailPageClass: 'Clifton.trade.TradeWindow',
				drillDownOnly: true,
				getDetailPageId: function(gridPanel, row) {
					return row.json.trade.id;
				}
			}
		},


			{
				title: 'External Trades',
				name: 'reconcileTradeExternalGroupingListFind',
				xtype: 'gridpanel',
				instructions: 'TBD',
				additionalPropertiesToRequest: 'trade.id|matchList|executingBrokerCompany.id|holdingInvestmentAccount.issuingCompany.id|externalTradeList',
				layout: 'fit',
				flex: 1,
				width: 600,
				region: 'center',
				hideStandardButtons: true,
				rowSelectionModel: 'checkbox',
				isPagingEnabled: function() {
					return false;
				},
				listeners: {
					afterrender: function(gridPanel) {
						const el = gridPanel.getEl();
						el.on('contextmenu', function(e, target) {
							const g = gridPanel.grid;
							g.contextRowIndex = g.view.findRowIndex(target);
							e.preventDefault();
							if (!g.drillDownMenu) {
								g.drillDownMenu = new Ext.menu.Menu({
									items: [
										{
											text: 'Match', iconCls: 'reconcile', handler: function() {
												gridPanel.getWindow().matchSelectedTrades(false, false);
											}
										},
										{
											text: 'Match with Broker Price', iconCls: 'reconcile', handler: function() {
												gridPanel.getWindow().matchSelectedTrades(true, false, '0.00001');
											}
										},
										{
											text: 'Manual Match', iconCls: 'reconcile', handler: function() {
												gridPanel.getWindow().openManualMatch();
											}
										},
										{
											text: 'Unmatch', iconCls: 'cancel', handler: function() {
												gridPanel.getWindow().unMatchSelectedTrades();
											}
										}
									]
								});
							}
							TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, g.contextRowIndex, g.view.findCellIndex(target), e);
						}, gridPanel);
					}
				},
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{
						header: 'Buy/Sell', width: 15, dataIndex: 'buy', type: 'boolean',
						renderer: function(v, metaData) {
							metaData.css = v ? 'buy-light' : 'sell-light';
							return v ? 'BUY' : 'SELL';
						}
					},
					{header: 'Holding Account', width: 60, dataIndex: 'holdingInvestmentAccount.name', filter: {searchFieldName: 'holdingInvestmentAccount'}, hidden: true},
					{header: 'Account Number', width: 25, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccount'}, hidden: true},
					{header: 'Holding Company', width: 45, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'holdingInvestmentAccountIssuerName'}, hidden: true},
					{
						header: 'Qty', width: 15, dataIndex: 'quantity', type: 'float', useNull: true, defaultSortColumn: true, summaryType: 'sum',
						renderer: function(value, metaData, row) {
							const totalTrades = row.data['totalTrades'];
							if (totalTrades > 1) {
								metaData.css = 'amountAdjusted';
								const qtip = totalTrades + ' total trades';
								metaData.attr = 'qtip=\'' + qtip + '\'';
							}
							return value;
						}
					},
					{header: 'Security', width: 15, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
					{header: 'Price', width: 23, dataIndex: 'price', type: 'float'},
					{header: 'Original Price', width: 23, dataIndex: 'originalPrice', type: 'float', hidden: true, tooltip: 'The price before multipliers are applied'},
					{header: 'Traded On', width: 13, dataIndex: 'tradeDate', hidden: true},
					{header: 'Source', width: 20, dataIndex: 'sourceCompany.name', filter: {searchFieldName: 'sourceCompanyName'}},
					{
						header: 'Executing Broker', width: 30, dataIndex: 'executingBrokerCompany.name',
						renderer: function(v, metaData, r) {
							if (!v) {
								metaData.css = 'ruleViolationBig';
								const qtip = 'Please map an executing broker for this data source';
								metaData.attr = TCG.renderQtip(qtip);
								return 'UNMAPPED';
							}
							return v;
						}
					},
					{
						header: 'Status', width: 17, dataIndex: 'status', tooltip: 'Indicates whether the trade has been confirmed by the exchange',
						filter: {
							type: 'list', options: [
								['CONFIRMED', 'CONFIRMED'],
								['UNCONFIRMED', 'UNCONFIRMED']]
						}
					},
					{
						header: 'Total Trades', width: 20, dataIndex: 'totalTrades', tooltip: 'The total number of underlying trades in this trade group', type: 'int', hidden: true,
						renderer: function(v, metaData, r) {
							if (v > 1) {
								metaData.css = 'amountAdjusted';
								const qtip = 'This is a group of ' + v + ' external trades';
								metaData.attr = 'qtip=\'' + qtip + '\'';
							}
							return v;
						}
					},
					{header: '', width: 9, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'}
				],
				plugins: {ptype: 'gridsummary'},
				getRowSelectionModel: function() {
					const win = this.getWindow();
					if (typeof this.rowSelectionModel !== 'object') {
						this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel({
							singleSelect: false,
							selectAll: function() {
								if (this.isLocked()) {
									return;
								}
								this.suspendEvents(false);
								this.selections.clear();
								for (let i = 0, len = this.grid.store.getCount(); i < len - 1; i++) {
									this.selectRow(i, true);
								}
								this.resumeEvents();
								this.selectRow(len - 1, true);
							},
							listeners: {
								'rowselect': function(model, rowIndex, record) {
									const tradeGrid = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
									const externalGrid = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
									win.selectGridItems(record, tradeGrid, externalGrid);
								},
								'rowdeselect': function(model, rowIndex, record) {
									const tradeGrid = TCG.getChildByName(win, 'reconcileTradeFillExtendedList');
									const externalGrid = TCG.getChildByName(win, 'reconcileTradeExternalGroupingListFind');
									win.deselectGridItems(record, tradeGrid, externalGrid);
								}
							}
						});
					}
					return this.rowSelectionModel;
				},
				getLoadParams: function() {
					const data = this.getWindow().defaultData;
					if (data) {
						const params = {
							reconciled: this.reconciled,
							tradeDate: data.tradeDate,
							holdingInvestmentAccountId: data.holdingInvestmentAccountId,
							requestedMaxDepth: 3
						};

						if (data.investmentSecurityId) {
							params.securityId = data.investmentSecurityId;
						}
						else {
							params.securityNotExists = true;
						}
						return params;
					}
					return false;
				},
				editor: {
					detailPageClass: 'Clifton.reconcile.trade.TradeExternalListWindow',
					drillDownOnly: true,
					getDefaultData: function(grid, row, className) {
						return {
							groupingId: row.data['id'],
							accountNumber: row.data['holdingInvestmentAccount.number'],
							investmentSecuritySymbol: row.data['investmentSecurity.symbol'],
							originalPrice: row.data['originalPrice'],
							buy: row.data['buy'],
							tradeDate: row.data['tradeDate']
						};
					}
				}
			}]
	}]
});


Clifton.reconcile.trade.ManualMatchWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Reconcile: Manual Match',
	iconCls: 'reconcile',
	height: 300,
	width: 510,
	modal: true,
	allowOpenFromModal: true, // contact drill down
	saveTimeout: 240,

	items: [{ // TRADE: investmentSecurity.id, holdingAccount.issuingCompany.id, executingBroker.id
		xtype: 'formpanel',
		instructions: 'Manually confirm selected trade fill:',
		url: 'reconcileTradeIntradayMatch.json',
		getDefaultData: function(win) {
			const dd = win.defaultData || {};
			if (TCG.isTrue(dd.noteRequired)) {
				const f = this.getForm();

				f.findField('note').allowBlank = false;

				try {
					this.doLayout();
				}
				catch (e) {
					// strange error due to removal of elements with allowBlank = false
				}
			}
			return dd;
		},

		listeners: {
			// After Creating - If a workflow is assigned - open the real match window
			aftercreate: function(panel, closeOnSuccess) {
				// the entity was already retrieved: pass it to the window and instruct not to get it again
				const tfList = TCG.getValue('tradeFillList', panel.getForm().formValues);
				if (tfList.length === 1) {
					const tradeFillId = tfList[0].id;
					if (tradeFillId) {
						const matches = TCG.data.getData('reconcileTradeIntradayMatchByFill.json?enableOpenSessionInView=true&requestedMaxDepth=4&tradeFillId=' + tradeFillId, panel);
						if (matches && matches.length === 1) {
							const match = matches[0];
							if (match && match['workflowState']) {
								const config = {
									defaultDataIsReal: true,
									defaultData: match,
									openerCt: panel.ownerCt.openerCt
								};
								TCG.createComponent('Clifton.reconcile.trade.MatchWindow', config);
							}
						}
					}
				}
			}
		},
		resetMatchTypeFields: function(record) {
			if (record) {
				const f = this.getForm();
				const confirmedField = f.findField('confirmed');
				confirmedField.setDisabled(TCG.isNotBlank(record.workflow));
			}
		},
		items: [
			{xtype: 'hidden', name: 'manualMatch', value: 'true'},
			{
				fieldLabel: 'Match Type', xtype: 'combo', hiddenName: 'matchType.id', name: 'matchType.name', url: 'reconcileTradeMatchTypeListFind.json',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						combo.store.baseParams = {securityId: fp.getDefaultData(fp.getWindow()).investmentSecurity.id};
					},
					select: function(combo, record, index) {
						combo.ownerCt.resetMatchTypeFields(record.json);
						// update required? record.json.noteRequired

					}
				}
			},
			{
				fieldLabel: 'Contact', xtype: 'combo', hiddenName: 'contact.id', name: 'contact.label', displayField: 'label', valueField: 'referenceTwo.id', url: 'businessCompanyContactListFind.json', detailPageClass: 'Clifton.business.contact.ContactWindow',
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						combo.store.baseParams = {companyIds: fp.getDefaultData(fp.getWindow()).contactCompanies};
					}
				}
			},
			{boxLabel: 'Note', xtype: 'textarea', name: 'note'},
			{boxLabel: 'Confirmed', xtype: 'checkbox', name: 'confirmed'}
		],
		getSubmitParams: function() {
			return this.getDefaultData(this.getWindow()).tradeFills;
		},
		getSaveURL: function() {
			return 'reconcileTradeIntradayMatchObjectSave.json?enableOpenSessionInView=true&requestedMaxDepth=3';
		}
	}]
});
