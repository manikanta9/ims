Clifton.reconcile.trade.MatchWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Match',
	iconCls: 'reconcile',
	width: 1000,
	height: 600,

	items: [{
		xtype: 'tabpanel',

		items: [
			{
				title: 'Trade',
				layout: 'border',

				tbar: {
					xtype: 'workflow-toolbar',
					tableName: 'ReconcileTradeIntradayMatch',
					enableOpenSessionInView: true,
					optionalWorkflow: true // Hides Workflow Toolbar for Matches that don't use a workflow
				},

				items: [
					{
						xtype: 'formpanel',
						region: 'north',
						height: 200,
						url: 'reconcileTradeIntradayMatch.json?enableOpenSessionInView=true',
						labelFieldName: 'id',

						listeners: {
							afterload: function(panel) {
								// If workflow is used - disable confirmed check box - driven by workflow
								const f = panel.getForm();
								const st = TCG.getValue('workflowState.id', f.formValues);
								if (TCG.isNotBlank(st)) {
									const field = f.findField('confirmed');
									field.setDisabled(true);
								}
							}
						},

						getWarningMessage: function(form) {
							let msg = undefined;
							if (form.formValues.trade.workflowState.name !== 'Booked') {
								msg = '<b>Warning</b>: Trade is currently in <b>' + form.formValues.trade.workflowState.name + '</b> workflow state.';
							}
							return msg;
						},

						items: [{
							xtype: 'panel',
							layout: 'column',
							items: [
								{
									columnWidth: .45,
									items: [{
										xtype: 'formfragment',
										labelWidth: 90,
										frame: false,
										items: [
											{fieldLabel: 'Match Type', name: 'matchType.name', detailIdField: 'matchType.id', xtype: 'linkfield', detailPageClass: 'Clifton.reconcile.trade.MatchTypeWindow'},
											{fieldLabel: 'Match Contact', name: 'contact.label', detailIdField: 'contact.id', xtype: 'linkfield', detailPageClass: 'Clifton.business.contact.ContactWindow'},
											{fieldLabel: 'Note', name: 'note', xtype: 'textarea', height: 80},
											{boxLabel: 'Trade fill was reconciled and confirmed', name: 'confirmed', xtype: 'checkbox'}
										]
									}]
								},

								{columnWidth: .01, items: [{xtype: 'label', html: '&nbsp;'}]},

								{
									columnWidth: .54,
									items: [{
										xtype: 'formfragment',
										labelWidth: 100,
										frame: false,
										items: [
											{fieldLabel: 'Trade ID', name: 'trade.id', detailIdField: 'trade.id', xtype: 'linkfield', detailPageClass: 'Clifton.trade.TradeWindow', submitDetailField: false},
											{
												fieldLabel: 'Trade State', name: 'trade.workflowState.name', detailIdField: 'trade.id', xtype: 'linkfield', submitDetailField: false,
												qtip: 'Click to view workflow history of the Trade',
												createDetailClass: function() {
													const p = TCG.getParentFormPanel(this);
													const id = this.getDetailIdFieldValue(p);
													if (id === false) {
														// cancelled
													}
													else if (TCG.isNull(id)) {
														TCG.showError('Cannot find field "' + this.detailIdField + '"', 'Client Side Error');
													}
													else {
														new TCG.app.CloseWindow({
															modal: true,
															allowOpenFromModal: true,
															width: 1100,
															iconCls: 'workflow',
															title: 'Workflow History for Trade ' + id,
															openerCt: this,
															items: [{
																xtype: 'workflow-history-effective-grid',
																tableName: 'Trade',
																getEntityId: function() {
																	return id;
																}
															}]
														});
													}
												}

											},
											{fieldLabel: 'Security', name: 'trade.investmentSecurity.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', detailIdField: 'trade.investmentSecurity.id', submitDetailField: false},
											{fieldLabel: 'Client Account', name: 'trade.clientInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'trade.clientInvestmentAccount.id', submitDetailField: false},
											{fieldLabel: 'Holding Account', name: 'trade.holdingInvestmentAccount.label', xtype: 'linkfield', detailPageClass: 'Clifton.investment.account.AccountWindow', detailIdField: 'trade.holdingInvestmentAccount.id', submitDetailField: false}
										],
										buttons: [
											{
												text: 'Trade Summary',
												xtype: 'button',
												iconCls: 'pdf',
												width: 120,
												handler: function() {
													const f = TCG.getParentFormPanel(this);
													Clifton.trade.downloadTradeReport(TCG.getValue('trade.id', f.getWindow().getMainForm().formValues), this);
												}
											}
										]
									}]
								}
							]
						}]
					},


					{
						title: 'Trade Notes & Supporting Documents',
						xtype: 'document-system-note-grid',
						region: 'center',
						tableName: 'Trade',
						showInternalInfo: false,
						showPrivateInfo: false,
						defaultActiveFilter: false,
						// Set to true to see notes for related entities - i.e. on Trades also see security notes
						includeNotesForLinkedEntity: true,
						reloadOnRender: false,

						bindDragAndDropToFormLoad: true,

						listeners: {
							afterRender: function() {
								const grid = this;
								const fp = this.getWindow().getMainFormPanel();
								fp.on('afterload', function(record) {
									if (record) {
										grid.reload();
									}
								}, this);
							}
						},
						getEntity: function() {
							return TCG.getValue('trade', this.getWindow().getMainForm().formValues);
						},

						getEntityId: function() {
							return TCG.getValue('trade.id', this.getWindow().getMainForm().formValues);
						}
					}
				]
			},


			{
				title: 'Trade Instructions',
				items: [{
					xtype: 'investment-instruction-item-grid',
					tableName: 'Trade',
					defaultCategoryName: 'Trade Counterparty',
					getEntityId: function() {
						return TCG.getValue('trade.id', this.getWindow().getMainForm().formValues);
					},
					getInstructionDate: function() {
						return TCG.getValue('trade.tradeDate', this.getWindow().getMainForm().formValues);
					}
				}]
			}
		]
	}]
});
