Clifton.reconcile.trade.TradeExternalListWindow = Ext.extend(TCG.app.Window, {
	id: 'reconcileTradeExternalListWindow',
	title: 'Trade Grouping Detail',
	iconCls: 'reconcile',
	width: 1000,
	height: 550,

	items: [{
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},
		items: [
			{
				title: 'External Trades for Selected Group',
				name: 'reconcileTradeExternalExtendedListByGroupingId',
				xtype: 'gridpanel',
				instructions: 'The following trades have been received by our system from the broker. Only MATCHED (by the exchange) trades are loaded into our system.',
				additionalPropertiesToRequest: 'match.tradeFill.trade.id',
				border: 1,
				flex: 1,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
					{header: 'Holding Account', width: 30, dataIndex: 'holdingInvestmentAccount.name', filter: {searchFieldName: 'holdingInvestmentAccount'}, hidden: true},
					{header: 'Account Number', width: 30, dataIndex: 'holdingInvestmentAccount.number', filter: {searchFieldName: 'holdingInvestmentAccount'}},
					{header: 'Holding Company', width: 30, dataIndex: 'holdingInvestmentAccount.issuingCompany.name', filter: {searchFieldName: 'holdingAccountIssuerName'}, hidden: true},
					{
						header: 'Buy/Sell', width: 20, dataIndex: 'buy', type: 'boolean',
						renderer: function(v, metaData) {
							metaData.css = v ? 'buy-light' : 'sell-light';
							return v ? 'BUY' : 'SELL';
						}
					},
					{header: 'Quantity', width: 20, dataIndex: 'quantity', type: 'float', useNull: true},
					{header: 'Security', width: 25, dataIndex: 'investmentSecurity.symbol', filter: {searchFieldName: 'symbol', comparison: 'BEGINS_WITH'}, defaultSortColumn: true},
					{header: 'Security Name', width: 40, dataIndex: 'investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
					{header: 'Price', width: 25, dataIndex: 'price', type: 'float', useNull: true},
					{header: 'Trade Date', width: 25, dataIndex: 'tradeDate'},
					{
						header: 'Executing Broker', width: 40, dataIndex: 'executingBrokerCompany.name',
						renderer: function(v, metaData, r) {
							if (!v) {
								metaData.css = 'ruleViolationBig';
								const qtip = 'Please map an executing broker for this data source';
								metaData.attr = TCG.renderQtip(qtip);
								return 'UNMAPPED';
							}
							return v;
						}
					},
					{
						header: 'Status', width: 30, dataIndex: 'status', tooltip: 'If CONFIRMED, the trade has been confirmed by the exchange',
						filter: {
							type: 'list',
							options: [
								['CONFIRMED', 'CONFIRMED'],
								['UNCONFIRMED', 'UNCONFIRMED'],
								['PENDING_BROKER_ACCEPTANCE', 'PENDING_BROKER_ACCEPTANCE']
							]
						}
					},
					{header: 'Unique Trade Id', width: 50, dataIndex: 'externalUniqueTradeId', defaultSortColumn: true, hidden: true},
					{header: 'Source', width: 30, dataIndex: 'sourceCompany.name', filter: {searchFieldName: 'sourceCompanyName'}},
					{header: '', width: 10, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', tooltip: 'Reconciled'}
				],
				getLoadParams: function() {
					return {id: this.getWindow().defaultData.groupingId};
				},
				editor: {
					detailPageClass: 'Clifton.trade.TradeWindow',
					drillDownOnly: true,
					getDetailPageId: function(gridPanel, row) {
						if (row.json.matchList && row.json.matchList.length === 1) {
							return row.json.matchList[0].tradeFill.trade.id;
						}
						TCG.showError(row.json.matchList ?
							'Cannot drill down because external trade is reconciled to multiple trade fills.'
							: 'Drill down is only allowed for reconciled trades.', 'Unreconciled Trade');
						return undefined;
					}
				}
			},


			{
				title: 'Corresponding Integration Trades (from both clearing and executing sources)',
				name: 'integrationTradeIntradayListFind',
				xtype: 'gridpanel',
				instructions: 'A list of external trades with extra information from broker files.',
				border: 1,
				flex: 1,
				columns: [
					{header: 'ID', width: 15, dataIndex: 'id', type: 'string', hidden: true},
					{header: 'File Id', width: 15, dataIndex: 'run.file.id', hidden: true},
					{header: 'Clearing Broker', width: 30, dataIndex: 'clearingBrokerCompanyName', hidden: true},
					{header: 'Holding Account', width: 30, dataIndex: 'accountNumber'},
					{
						header: 'Buy/Sell', width: 20, dataIndex: 'buy', type: 'boolean',
						renderer: function(v, metaData) {
							metaData.css = v ? 'buy-light' : 'sell-light';
							return v ? 'BUY' : 'SELL';
						}
					},
					{header: 'Quantity', width: 20, dataIndex: 'quantity', type: 'float'},
					{header: 'Security', width: 25, dataIndex: 'investmentSecuritySymbol'},
					{header: 'Paying Security', width: 20, dataIndex: 'payingSecuritySymbol', hidden: true},
					{header: 'Symbol Type', width: 30, dataIndex: 'securitySymbolType', hidden: true},
					{header: 'Original Price', width: 25, dataIndex: 'price', type: 'float'},
					{header: 'Trade Date', width: 25, dataIndex: 'tradeDate'},
					{header: 'Executing Broker', width: 40, dataIndex: 'executingBrokerCompanyName'},
					{
						header: 'Status',
						width: 30,
						dataIndex: 'status',
						filter: {
							type: 'list', options: [
								['CONFIRMED', 'CONFIRMED'],
								['UNCONFIRMED', 'UNCONFIRMED'],
								['PENDING_BROKER_ACCEPTANCE', 'PENDING_BROKER_ACCEPTANCE']]
						},
						tooltip: 'If CONFIRMED, then the trade has been confirmed by the exchange'
					},
					{
						header: 'Type',
						width: 30,
						dataIndex: 'type',
						hidden: true,
						filter: {
							type: 'list', options: [
								['ADDED', 'ADDED'],
								['CHANGED', 'CHANGED'],
								['DELETED', 'DELETED']]
						},
						tooltip: 'Most cumulative files do not provide a Type flag, and are mapped to CHANGED automatically.  ' +
							'For these files, deleted trades simply do not show up.'
					},
					{header: 'Unique Trade Id', width: 40, dataIndex: 'uniqueTradeId', defaultSortColumn: true, hidden: true},
					{header: 'Source', width: 30, dataIndex: 'run.file.fileDefinition.source.name', filter: {searchField: 'fileSourceName'}},
					{header: '', width: 10, tooltip: 'Reconciled'}
				],
				isPagingEnabled: function() {
					return false;
				},
				editor: {
					detailPageClass: 'Clifton.integration.file.FileWindow',
					drillDownOnly: true,
					getDetailPageId: function(grid, row) {
						return row.json.run.file.id;
					}
				},
				getLoadParams: function(firstLoad) {
					const data = this.getWindow().defaultData;

					return {
						accountNumber: data.accountNumber,
						investmentSecuritySymbol: data.investmentSecuritySymbol,
						buy: data.buy,
						price: data.originalPrice,
						tradeDate: TCG.renderDate(data.tradeDate)
					};
				}
			}
		]
	}]
});
