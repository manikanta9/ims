Clifton.reconcile.trade.MatchTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Trade Match Type',
	iconCls: 'reconcile',
	height: 510,

	items: [{
		xtype: 'formpanel',
		instructions: 'When matching our trades to external trades, one of the following match types must be selected.',
		url: 'reconcileTradeMatchType.json',
		labelWidth: 125,

		listeners: {
			afterload: function(panel) {
				const f = this.getForm();
				const systemDefined = TCG.getValue('systemDefined', f.formValues);
				if (systemDefined === true) {
					this.setReadOnly(true);
				}
			}
		},

		getWarningMessage: function(form) {
			let msg = undefined;
			if (form.formValues.systemDefined === true) {
				msg = 'This match type is system defined and cannot be changed.';
			}
			return msg;
		},

		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 70},
			{
				fieldLabel: 'Source', name: 'source', hiddenName: 'source', displayField: 'name', valueField: 'value', mode: 'local', xtype: 'combo',
				store: new Ext.data.ArrayStore({
					fields: ['name', 'value', 'description'],
					data: [
						['AUTO', 'AUTO', 'Automatically confirmed from information in the system. Usually used when Auto Create Match is used with a specified Client Account Group'],
						['EMAIL', 'EMAIL', 'Confirmed via email'],
						['PDF', 'PDF', 'PDF confirmation is attached'],
						['PHONE', 'PHONE', 'Confirmed over the phone'],
						['WEBSITE', 'WEBSITE', 'Matched using clearing broker website resources']
					]
				})
			},
			{fieldLabel: 'Instrument Group', name: 'investmentGroup.name', hiddenName: 'investmentGroup.id', xtype: 'combo', url: 'investmentGroupListFind.json', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
			{fieldLabel: 'Client Account Group', name: 'clientInvestmentAccountGroup.name', hiddenName: 'clientInvestmentAccountGroup.id', xtype: 'combo', url: 'investmentAccountGroupListFind.json?accountTypeName=Client', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
			{fieldLabel: 'Auto-Create Condition', name: 'autoCreateManualMatchCondition.name', hiddenName: 'autoCreateManualMatchCondition.id', xtype: 'system-condition-combo', url: 'systemConditionListFind.json?optionalSystemTableName=Trade', qtip: 'Optional condition to evaluate when auto-creating manual matches for trade fills to limit automatically generated matches. If the condition is defined, it must evaluate to true in order to auto-create manual matches.'},
			{fieldLabel: 'Days to Confirm', name: 'daysToConfirm', xtype: 'spinnerfield', minValue: 0, maxValue: 100},
			{
				fieldLabel: 'Workflow', name: 'workflow.name', hiddenName: 'workflow.id',
				qtip: 'Optional Workflow, when selected, is applied to matches of this type where the Trade is a single fill trade.<br/><br/>Note: If workflow is changed, existing matches will keep the original workflow that was selected and all new matches will get the new workflow.',
				xtype: 'combo', url: 'workflowAssignmentListByTableName.json?tableName=ReconcileTradeIntradayMatch&includeInactive=false',
				root: 'data',
				displayField: 'workflow.name',
				valueField: 'workflow.id',
				tooltipField: 'workflow.description',
				detailPageClass: 'Clifton.workflow.definition.WorkflowWindow'
			},

			{boxLabel: 'Automatically mark trade fill matches of this type as confirmed', name: 'autoConfirm', xtype: 'checkbox'},
			{boxLabel: 'Automatically create manual matches for trade fills of this type', name: 'autoCreateManualMatch', xtype: 'checkbox'},
			{boxLabel: 'Require Note for matches of this type', name: 'noteRequired', xtype: 'checkbox'},
			{boxLabel: 'Require Contact selection for matches of this type', name: 'contactRequired', xtype: 'checkbox'},
			{boxLabel: 'Require a file Attachment for matches of this type', name: 'attachmentRequired', xtype: 'checkbox'},
			{boxLabel: 'This match type is System Defined and cannot be modified', name: 'systemDefined', xtype: 'checkbox', disabled: true}
		]
	}]
});
