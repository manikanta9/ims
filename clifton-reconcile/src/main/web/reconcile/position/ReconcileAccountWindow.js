Clifton.reconcile.position.ReconcileAccountWindow = Ext.extend(TCG.app.Window, {
	titlePrefix: 'Account Reconciliation',
	iconCls: 'reconcile',
	width: 1300,
	height: 650,

	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		win.on('beforeclose', function() {
			if (win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
				win.openerCt.reload();
			}
		});

		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}
	},
	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Account Positions',
				items: [{
					xtype: 'reconcile-position-match-grid'
				}]
			},


			{
				title: 'Our Positions',
				items: [{
					name: 'reconcilePositionOurList',
					xtype: 'gridpanel',
					instructions: 'Lot level positions for selected account.',
					getLoadParams: function() {
						// default position date to previous business day
						const win = this.getWindow();
						return {reconcilePositionAccountId: win.params.id, requestedMaxDepth: 5};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Security', width: 50, dataIndex: 'accountingTransaction.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', url: 'investmentSecurityListFind.json'}},
						{header: 'Position Date', width: 50, dataIndex: 'positionDate', type: 'date'},
						{header: 'Open Date', width: 50, dataIndex: 'accountingTransaction.originalTransactionDate', type: 'date'},

						{header: 'FX Rate', width: 50, dataIndex: 'marketFxRate', type: 'float'},
						{header: 'Open Price', width: 50, dataIndex: 'accountingTransaction.price', type: 'float'},
						{header: 'Settlement Price', width: 50, dataIndex: 'marketPrice', type: 'float'},
						{header: 'Open Quantity', width: 50, dataIndex: 'accountingTransaction.quantity', type: 'float', hidden: true},
						{header: 'Remaining Quantity', width: 50, dataIndex: 'remainingQuantity', type: 'float'},
						{header: 'Notional Value Local', width: 50, dataIndex: 'notionalValueLocal', type: 'currency'},
						{header: 'Notional Value Base', width: 50, dataIndex: 'notionalValueBase', type: 'currency'},
						{header: 'OTE Local', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency'},
						{header: 'OTE Base', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency'}
					]
				}]
			},


			{
				title: 'External Positions',
				items: [{
					name: 'reconcilePositionExternalList',
					xtype: 'gridpanel',
					instructions: 'External position details for selected account.',
					getLoadParams: function() {
						// default position date to previous business day
						const win = this.getWindow();
						return {reconcilePositionAccountId: win.params.id, requestedMaxDepth: 3};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.label', hidden: true},
						{header: 'Client Account', width: 100, dataIndex: 'clientAccount.label', hidden: true},
						{header: 'Security', width: 50, dataIndex: 'investmentSecurity.symbol'},
						{header: 'Currency', width: 50, dataIndex: 'investmentCurrency.symbol', hidden: true},
						{header: 'Position Date', width: 50, dataIndex: 'positionDate', type: 'date'},
						{header: 'Open Date', width: 50, dataIndex: 'tradeDate', type: 'date'},
						{header: 'Settle Date', width: 50, dataIndex: 'settleDate', type: 'date', hidden: true},

						{header: 'FX Rate', width: 50, dataIndex: 'fxRate', type: 'float'},
						{header: 'Open Price', width: 50, dataIndex: 'tradePrice', type: 'float'},
						{header: 'Settlement Price', width: 50, dataIndex: 'marketPrice', type: 'float'},
						{header: 'Quantity', width: 50, dataIndex: 'quantity', type: 'float'},
						{header: 'Market Value Local', width: 50, dataIndex: 'marketValueLocal', type: 'currency'},
						{header: 'Market Value Base', width: 50, dataIndex: 'marketValueBase', type: 'currency'},
						{header: 'Cost Basis Local', width: 50, dataIndex: 'remainingCostBasisLocal', type: 'currency', hidden: true},
						{header: 'Cost Basis Base', width: 50, dataIndex: 'remainingCostBasisBase', type: 'currency', hidden: true},
						{header: 'OTE Local', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency'},
						{header: 'OTE Base', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency'},
						{header: 'Short Position', dataIndex: 'shortPosition', type: 'boolean', width: 15, hidden: true}
					]
				}]
			},


			{
				title: 'External Activity',
				items: [{
					name: 'reconcilePositionExternalPNLList',
					xtype: 'gridpanel',
					instructions: 'External activity details for selected account.',
					getLoadParams: function() {
						const win = this.getWindow();
						return {reconcilePositionAccountId: win.params.id, requestedMaxDepth: 3};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount.name', hidden: true},
						{header: 'Client Account', width: 50, dataIndex: 'clientAccount.name', hidden: true},
						{header: 'Security', width: 35, dataIndex: 'investmentSecurity.symbol'},
						{header: 'Position Date', width: 40, dataIndex: 'positionDate'},
						{header: 'Short Position', width: 35, dataIndex: 'shortPosition', type: 'boolean', hidden: true},
						{header: 'Quantity', width: 35, dataIndex: 'quantity', type: 'float', hidden: true},
						{header: 'Prior Quantity', width: 35, dataIndex: 'priorQuantity', type: 'float', hidden: true},
						{header: 'Closed Quantity', width: 35, dataIndex: 'todayClosedQuantity', type: 'float'},
						{header: 'Investment Currency', width: 35, dataIndex: 'investmentCurrency.symbol', hidden: true},
						{header: 'Base Currency', width: 35, dataIndex: 'baseCurrency.symbol', hidden: true},
						{header: 'FX Rate', width: 50, dataIndex: 'fxRate', type: 'float', hidden: true},
						{header: 'Open Trade Equity Local', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency'},
						{header: 'Open Trade Equity Base', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency'},
						{header: 'Prior Open Trade Equity Local', width: 50, dataIndex: 'priorOpenTradeEquityLocal', type: 'currency', hidden: true},
						{header: 'Prior Open Trade Equity Base', width: 50, dataIndex: 'priorOpenTradeEquityBase', type: 'currency', hidden: true},
						{header: 'Commission Local', width: 40, dataIndex: 'todayCommissionLocal', type: 'currency'},
						{header: 'Realized Gain Loss', width: 50, dataIndex: 'todayRealizedGainLossLocal', type: 'currency'}
					]
				}]
			}
		]
	}]
});
