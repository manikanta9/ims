Clifton.reconcile.position.ReconcileBrokerPriceOverride = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Broker Price Override',

	items: [{
		xtype: 'formpanel',
		url: 'reconcilePositionExternalPriceOverride.json',
		labelWidth: 150,
		instructions: 'Use this screen to override external security end of day prices that have been confirmed to be inaccurate.',
		initComponent: function() {
			TCG.form.FormPanel.prototype.initComponent.call(this);

			const win = this.getWindow();
			if (win.params && win.params.id) {
				this.readOnly = true;
				return {id: win.params.id};
			}
		},
		items: [
			{fieldLabel: 'Account Type', xtype: 'combo', hiddenName: 'accountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter broker selection by issuers of active accounts of selected account type'},
			{
				fieldLabel: 'Broker', name: 'holdingCompany.name', width: 250, hiddenName: 'holdingCompany.id', xtype: 'combo', detailPageClass: 'Clifton.business.company.CompanyWindow',
				url: 'businessCompanyListFind.json?tradingAllowed=true',
				beforequery: function(queryEvent) {
					const combo = queryEvent.combo;
					const f = combo.getParentForm().getForm();
					const atv = f.findField('accountTypeId').getValue();
					combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
				}
			},
			{fieldLabel: 'Security', name: 'investmentSecurity.label', width: 250, allowBlank: false, displayField: 'label', hiddenName: 'investmentSecurity.id', xtype: 'combo', detailPageClass: 'Clifton.investment.instrument.SecurityWindow', url: 'investmentSecurityListFind.json'},
			{fieldLabel: 'Position Date', name: 'positionDate', xtype: 'datefield'},
			{fieldLabel: 'External Price', name: 'externalPrice', xtype: 'floatfield'},
			{fieldLabel: 'Override Price', name: 'overridePrice', xtype: 'floatfield'},
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea'}
		]
	}]
});
