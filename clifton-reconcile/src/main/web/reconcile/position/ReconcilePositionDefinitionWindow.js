Clifton.reconcile.position.ReconcilePositionDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Reconcile Position Definition',
	iconCls: 'reconcile',

	items: [{
		xtype: 'formpanel',
		url: 'reconcilePositionDefinition.json',
		instructions: 'Reconcile definition defines a single reconciliation process.',
		readOnly: true,
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textfield'},
			{fieldLabel: 'Our Table', name: 'ourSystemTable.name', xtype: 'linkfield', detailIdField: 'ourSystemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow'},
			{fieldLabel: 'External Table', name: 'externalSystemTable.name', xtype: 'linkfield', detailIdField: 'externalSystemTable.id', detailPageClass: 'Clifton.system.schema.TableWindow'},
			{fieldLabel: 'Instrument Group', name: 'investmentGroup.name', xtype: 'linkfield', detailIdField: 'investmentGroup.id', detailPageClass: 'Clifton.investment.setup.GroupWindow'},
			{fieldLabel: 'Holding Company', name: 'holdingCompany.name', xtype: 'linkfield', detailIdField: 'holdingCompany.id', detailPageClass: 'Clifton.business.company.CompanyWindow'},
			{fieldLabel: 'Purpose', name: 'accountRelationshipPurpose.name', xtype: 'displayfield'},
			{boxLabel: 'Reconcile local amounts only and ignore base currency amounts and exchange rates', name: 'localOnly', xtype: 'checkbox'},
			{boxLabel: 'Zero Remaining Quantity', name: 'zeroRemainingQuantity', xtype: 'checkbox'}
		]
	}]
});
