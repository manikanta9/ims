Clifton.reconcile.position.ReconcilePositionComparisonWindow = Ext.extend(TCG.app.Window, {
	id: 'reconcilePositionWindow',
	titlePrefix: 'Reconcile Position Details',
	iconCls: 'reconcile',
	width: 1300,
	height: 650,

	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		if (win.defaultData && win.defaultData.label) {
			TCG.Window.superclass.setTitle.call(this, this.titlePrefix + ' - ' + win.defaultData.label, this.iconCls);
		}
	},
	frame: true,
	items: [{
		xtype: 'tabpanel',
		items: [{
			title: 'Comparison',
			listeners: {
				render: function() {
					const t = this.getTopToolbar();
					t.add({
						text: 'Actions',
						iconCls: 'run',
						menu: new Ext.menu.Menu({
							items: [{
								text: 'Add Broker Price Override for Selection',
								tooltip: 'Override the broker\'s price.  This will rebuild the broker position.',
								iconCls: 'run',
								handler: function(btn) {
									const win = TCG.getParentByClass(btn, Ext.Window);

									const defaultData = {
										investmentSecurity: win.defaultData.investmentSecurity,
										holdingCompany: win.defaultData.holdingCompany,
										positionDate: win.defaultData.positionDate,
										externalPrice: win.defaultData.externalPrice
									};

									const className = 'Clifton.reconcile.position.ReconcileBrokerPriceOverride';
									const cmpId = TCG.getComponentId(className);
									TCG.createComponent(className, {
										id: cmpId,
										openerCt: win,
										defaultData: defaultData,
										defaultIconCls: win.iconCls
									});
								}
							}]
						})
					});
					t.add('-');
					t.add({
						text: 'View Security',
						iconCls: 'coins',
						tooltip: 'Open detail screen for security being reconciled',
						handler: function(btn) {
							const win = TCG.getParentByClass(btn, Ext.Window);
							const params = {id: win.defaultData.investmentSecurity.id};
							const className = 'Clifton.investment.instrument.SecurityWindow';
							const cmpId = TCG.getComponentId(className, params.id);
							TCG.createComponent(className, {
								id: cmpId,
								params: params,
								openerCt: win
							});
						}
					});
					t.add('-');
					t.add({
						text: 'View Currency',
						iconCls: 'money',
						tooltip: 'Open detail screen for base currency of selected client account',
						handler: function(btn) {
							const win = TCG.getParentByClass(btn, Ext.Window);
							const params = {id: win.defaultData.baseCurrencyId};
							const className = 'Clifton.investment.instrument.CurrencyWindow';
							const cmpId = TCG.getComponentId(className, params.id);
							TCG.createComponent(className, {
								id: cmpId,
								params: params,
								openerCt: win
							});
						}
					});
					t.add('-');
				}
			},
			tbar: [],

			items: [{
				xtype: 'formpanel',
				loadValidation: false,
				url: 'reconcilePositionComparison.json?enableOpenSessionInView=true&requestedMaxDepth=3',
				defaults: {
					anchor: '0'
				},
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						items: [{
							columnWidth: .7,
							layout: 'form',
							items: [
								{fieldLabel: 'Holding Account', name: 'holdingAccount.label', xtype: 'linkfield', detailIdField: 'holdingAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Client Account', name: 'clientAccount.label', xtype: 'linkfield', detailIdField: 'clientAccount.id', detailPageClass: 'Clifton.investment.account.AccountWindow'},
								{fieldLabel: 'Security', name: 'security.symbol', xtype: 'linkfield', detailIdField: 'security.id', detailPageClass: 'Clifton.investment.instrument.SecurityWindow'}
							]
						},
							{
								columnWidth: .3,
								layout: 'form',
								labelWidth: 80,
								items: [
									{
										fieldLabel: ' ', xtype: 'compositefield', labelSeparator: '',
										defaults: {
											xtype: 'displayfield',
											style: 'text-align: right',
											flex: 1
										},
										items: [
											{value: 'Our', submitValue: false},
											{value: 'External', submitValue: false}
										]
									},
									{
										fieldLabel: 'Market Price', xtype: 'compositefield',
										defaults: {
											xtype: 'floatfield',
											flex: 1
										},
										items: [
											{name: 'marketPrice', readOnly: true, submitValue: false},
											{name: 'externalMarketPrice', readOnly: true, submitValue: false}
										]
									},
									{
										fieldLabel: 'FX Rate', xtype: 'compositefield',
										defaults: {
											xtype: 'floatfield',
											flex: 1
										},
										items: [
											{name: 'fxRate', readOnly: true, submitValue: false},
											{name: 'externalFxRate', readOnly: true, submitValue: false}
										]
									}
								]
							}]
					},
					{
						xtype: 'panel',
						layout: 'border',
						anchor: '100%, -90',
						items: [{
							xtype: 'formgrid',
							title: 'Matched Positions',
							name: 'matchedPositionsGrid',
							storeRoot: 'mappedPositions',
							dtoClass: 'com.clifton.reconcile.position.ReconcileDisplayPosition',
							height: 250,
							autoHeight: false,
							collapsible: false,
							hideStandardButtons: true,
							region: 'north',
							split: true,
							flex: 1,
							viewConfig: {forceFit: true, emptyText: 'No matching positions', deferEmptyText: false},
							sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
							readOnly: true,
							useFilters: true,
							markModified: Ext.emptyFn,
							detailPageClass: 'Clifton.reconcile.trade.ReconcileTradeMatchWindow',
							getDefaultData: function(grid, row) {
								const form = TCG.getParentByClass(grid, TCG.form.FormPanel);
								const holdingAccount = form.getFormValue('holdingAccount');
								const tradeDate = TCG.parseDate(row.json.ourTradeDate).format('m/d/Y');
								const security = form.getFormValue('security');

								return {
									label: holdingAccount.label + ' for ' + tradeDate + ' [' + security.label + ']',
									investmentSecurityId: security.id,
									holdingInvestmentAccountId: holdingAccount.id,
									tradeDate: tradeDate
								};
							},
							contextMenuConfig: [
								{
									text: 'Open Trade Rec', iconCls: 'reconcile', handler: function(btn, e) {
										const grid = btn.parentMenu.openerCt;
										const row = btn.parentMenu.row;
										const className = 'Clifton.reconcile.trade.ReconcileTradeMatchWindow';
										const defaultData = grid.getDefaultData(grid, row);

										const cmpId = TCG.getComponentId(className);
										TCG.createComponent(className, {
											id: cmpId,
											defaultData: defaultData,
											openerCt: this,
											defaultIconCls: 'reconcile'
										});
									}
								},
								{
									text: 'Open Source Screen', iconCls: 'book-open', handler: function(btn, e) {
										const row = btn.parentMenu.row;
										if (row) {
											const grouped = row.json.ourTotalPositions > 1;
											if (!grouped) {
												const className = row.json.detailScreenClass;
												const fkFieldId = row.json.fkFieldId;
												TCG.createComponent(className, {
													id: TCG.getComponentId(className, fkFieldId),
													params: {id: fkFieldId},
													openerCt: this,
													defaultIconCls: 'book-open'
												});
											}
											else {
												TCG.showError('This is a grouped position.  Source details are not available at the grouped level.', 'Grouped Position');
											}
										}
									}
								},
								{
									text: 'Open Journal', iconCls: 'book-open-blue', handler: function(btn, e) {
										const row = btn.parentMenu.row;
										if (row) {
											const grouped = row.json.ourTotalPositions > 1;
											if (!grouped) {
												const className = 'Clifton.accounting.journal.JournalWindow';
												const accountingJournalId = row.json.accountingJournalId;
												TCG.createComponent(className, {
													id: TCG.getComponentId(className, accountingJournalId),
													params: {id: accountingJournalId},
													openerCt: this,
													defaultIconCls: 'book-open-blue'
												});
											}
											else {
												TCG.showError('This is a grouped position.  Journal details are not available at the grouped level.', 'Grouped Position');
											}
										}
									}
								},
								{
									text: 'Open Transaction', iconCls: 'book-red', handler: function(btn, e) {
										const row = btn.parentMenu.row;
										if (row) {
											const grouped = row.json.ourTotalPositions > 1;
											if (!grouped) {
												const className = 'Clifton.accounting.gl.TransactionWindow';
												const transactionId = row.json.accountingTransactionId;
												TCG.createComponent(className, {
													id: TCG.getComponentId(className, transactionId),
													params: {id: transactionId},
													openerCt: this,
													defaultIconCls: 'book-red'
												});
											}
											else {
												TCG.showError('This is a grouped position.  Transaction details are not available at the grouped level.', 'Grouped Position');
											}
										}
									}
								}
							],
							columnsConfig: [
								{
									header: 'Lot Ratio', width: 30, align: 'center', tooltip: 'If the position is grouped, shows ratio of our underlying lots to external',
									renderer: function(v, metaData, r) {
										const ourLots = r.data.ourTotalPositions;
										const externalLots = r.data.externalTotalPositions;
										if (ourLots > 1 || externalLots > 1) {
											metaData.css = 'amountAdjusted';
											const qtip = ourLots + ' of our lots to ' + externalLots + ' external lot' + (externalLots > 1 ? 's' : '');
											metaData.attr = 'qtip=\'' + qtip + '\'';

											return ourLots + ' to ' + externalLots;
										}
										return '';
									}
								},
								{header: 'Total Lots', type: 'int', dataIndex: 'ourTotalPositions', width: 20, hidden: true},
								{header: 'External Total Lots', type: 'int', dataIndex: 'externalTotalPositions', width: 20, hidden: true},
								{header: 'Accounting Transaction ID', dataIndex: 'accountingTransactionId', width: 30, hidden: true},
								{header: 'Accounting Journal ID', dataIndex: 'accountingJournalId', width: 30, hidden: true},
								{header: 'Transaction Source ID', dataIndex: 'fkFieldId', width: 30, hidden: true},
								{
									header: 'Open Date', width: 50, dataIndex: 'ourTradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC',
									renderer: function(v, metaData, r) {
										const ours = r.data.ourTradeDate;
										const external = r.data.externalTradeDate;
										const difference = TCG.dayDifference(ours, external);

										if (difference > 0 || difference < 0) {
											metaData.css = 'ruleViolationBig';
											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.renderDate(ours) + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.renderDate(external) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.renderDate(ours);

									}
								},
								{header: 'External Open Date', width: 50, dataIndex: 'externalTradeDate', hidden: true},
								{header: 'Settle Date', width: 50, dataIndex: 'ourSettleDate'},
								{header: 'External Settle Date', width: 50, dataIndex: 'externalSettleDate', hidden: true},

								{
									header: 'FX Rate', width: 40, dataIndex: 'ourFxRate', type: 'float', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourFxRate;
										const external = r.data.externalFxRate;

										if (ours !== external) {
											const difference = r.json.fxRateDifference;
											metaData.css = 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true, '0,000', true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}

										return TCG.numberFormat(v, '0,000', true);

									}
								},
								{header: 'External FX Rate', width: 40, dataIndex: 'externalFxRate', type: 'float', useNull: true, hidden: true},

								{
									header: 'Quantity', width: 40, dataIndex: 'ourQuantity', type: 'float', summaryType: 'sum', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourQuantity;
										const external = r.data.externalQuantity;

										if (ours !== external) {
											const difference = r.json.quantityDifference;
											metaData.css = 'ruleViolationBig';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true, '0,000', true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}

										return TCG.numberFormat(v, '0,000', true);

									}
								},
								{header: 'External Quantity', width: 40, dataIndex: 'externalQuantity', type: 'float', summaryType: 'sum', useNull: true, hidden: true},

								{
									header: 'Open Price', width: 50, dataIndex: 'ourTradePrice', type: 'float', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourTradePrice;
										const external = r.data.externalTradePrice;

										if (ours !== external) {
											const difference = r.json.tradePriceDifference;
											metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true, '0,000', true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.numberFormat(v, '0,000', true);

									}
								},
								{header: 'External Open Price', width: 50, dataIndex: 'externalTradePrice', type: 'float', useNull: true, hidden: true},

								{
									header: 'Market Price', width: 50, dataIndex: 'ourPrice', type: 'float', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourPrice;
										const external = r.data.externalPrice;

										if (ours !== external) {
											const difference = r.json.priceDifference;
											metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000', true) + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true, '0,000', true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.numberFormat(v, '0,000', true);

									}
								},
								{header: 'External Market Price', width: 50, dataIndex: 'externalPrice', type: 'float', useNull: true, hidden: true},

								{
									header: 'Market Value', width: 50, dataIndex: 'ourMarketValueLocal', type: 'currency', summaryType: 'sum', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourMarketValueLocal;
										const external = r.data.externalMarketValueLocal;

										if (ours !== external) {
											const difference = r.json.marketValueLocalDifference;
											metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.numberFormat(v, '0,000.00');

									}
								},
								{header: 'External Market Value', width: 50, dataIndex: 'externalMarketValueLocal', type: 'currency', summaryType: 'sum', useNull: true, hidden: true},

								{
									header: 'Base Market Value', width: 60, dataIndex: 'ourMarketValueBase', type: 'currency', summaryType: 'sum', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourMarketValueBase;
										const external = r.data.externalMarketValueBase;

										if (ours !== external) {
											const difference = r.json.marketValueBaseDifference;
											metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.numberFormat(v, '0,000.00');

									}
								},
								{header: 'External Base Market Value', width: 60, dataIndex: 'externalMarketValueBase', type: 'currency', summaryType: 'sum', useNull: true, hidden: true},

								{
									header: 'OTE', width: 45, dataIndex: 'ourOpenTradeEquityLocal', type: 'currency', summaryType: 'sum', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourOpenTradeEquityLocal;
										const external = r.data.externalOpenTradeEquityLocal;

										if (ours !== external) {
											const difference = r.json.oteLocalDifference;
											metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.numberFormat(v, '0,000.00');

									}
								},
								{header: 'External OTE', width: 45, dataIndex: 'externalOpenTradeEquityLocal', type: 'currency', summaryType: 'sum', useNull: true, hidden: true},

								{
									header: 'Base OTE', width: 45, dataIndex: 'ourOpenTradeEquityBase', type: 'currency', summaryType: 'sum', useNull: true,
									renderer: function(v, metaData, r) {
										const ours = r.data.ourOpenTradeEquityBase;
										const external = r.data.externalOpenTradeEquityBase;

										if (ours !== external) {
											const difference = r.json.oteBaseDifference;
											metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';

											let qtip = '<table>';
											qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000.00') + '</td></tr>';
											qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
											qtip += '</table>';
											metaData.attr = 'qtip=\'' + qtip + '\'';
										}
										return TCG.numberFormat(v, '0,000.00');

									}
								},
								{header: 'External Base OTE', width: 45, dataIndex: 'externalOpenTradeEquityBase', type: 'currency', summaryType: 'sum', useNull: true, hidden: true}
							]
						},


							{
								xtype: 'container',
								layoutConfig: {align: 'stretch'},
								layout: 'hbox',
								region: 'center',
								split: true,
								flex: 2,
								items: [{
									xtype: 'formgrid',
									title: 'Our Positions (Unmatched)',
									name: 'ourUnmappedPositionsGrid',
									storeRoot: 'unmappedInternalPositions',
									dtoClass: 'com.clifton.reconcile.position.accounting.ReconcilePositionAccountingDailyGrouping',
									collapsible: false,
									layout: 'fit',
									flex: 1,
									autoHeight: false,
									hideStandardButtons: true,
									plugins: {ptype: 'gridsummary'},
									viewConfig: {forceFit: true, emptyText: 'All positions have been mapped to an external position', deferEmptyText: false},
									sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
									readOnly: true,
									useFilters: true,
									detailPageClass: 'Clifton.reconcile.trade.ReconcileTradeMatchWindow',
									getDefaultData: function(grid, row) {
										const form = TCG.getParentByClass(grid, TCG.form.FormPanel);
										const holdingAccount = form.getFormValue('holdingAccount');
										const tradeDate = TCG.parseDate(row.json.originalTransactionDate).format('m/d/Y');
										const security = form.getFormValue('security');

										return {
											label: holdingAccount.label + ' for ' + tradeDate + ' [' + security.label + ']',
											investmentSecurityId: security.id,
											holdingInvestmentAccountId: holdingAccount.id,
											tradeDate: tradeDate
										};
									},
									contextMenuConfig: [
										{
											text: 'Open Trade Rec', iconCls: 'reconcile', handler: function(btn, e) {
												const grid = btn.parentMenu.openerCt;
												const row = btn.parentMenu.row;
												const className = 'Clifton.reconcile.trade.ReconcileTradeMatchWindow';
												const defaultData = grid.getDefaultData(grid, row);

												const cmpId = TCG.getComponentId(className);
												TCG.createComponent(className, {
													id: cmpId,
													defaultData: defaultData,
													openerCt: this,
													defaultIconCls: 'reconcile'
												});
											}
										},
										{
											text: 'Open Source Screen', iconCls: 'book-open', handler: function(btn, e) {
												const row = btn.parentMenu.row;
												if (row) {
													const grouped = row.json.totalPositions > 1;
													if (!grouped) {
														const className = row.json.detailScreenClass;
														const fkFieldId = row.json.fkFieldId;
														TCG.createComponent(className, {
															id: TCG.getComponentId(className, fkFieldId),
															params: {id: fkFieldId},
															openerCt: this,
															defaultIconCls: 'book-open'
														});
													}
													else {
														TCG.showError('This is a grouped position.  Source details are not available at the grouped level.', 'Grouped Position');
													}
												}
											}
										},
										{
											text: 'Open Journal', iconCls: 'book-open-blue', handler: function(btn, e) {
												const row = btn.parentMenu.row;
												if (row) {
													const grouped = row.json.totalPositions > 1;
													if (!grouped) {
														const className = 'Clifton.accounting.journal.JournalWindow';
														const accountingJournalId = row.json.accountingJournalId;
														TCG.createComponent(className, {
															id: TCG.getComponentId(className, accountingJournalId),
															params: {id: accountingJournalId},
															openerCt: this,
															defaultIconCls: 'book-open-blue'
														});
													}
													else {
														TCG.showError('This is a grouped position.  Journal details are not available at the grouped level.', 'Grouped Position');
													}
												}
											}
										},
										{
											text: 'Open Transaction', iconCls: 'book-red', handler: function(btn, e) {
												const row = btn.parentMenu.row;
												if (row) {
													const grouped = row.json.totalPositions > 1;
													if (!grouped) {
														const className = 'Clifton.accounting.gl.TransactionWindow';
														const transactionId = row.json.accountingTransactionId;
														TCG.createComponent(className, {
															id: TCG.getComponentId(className, transactionId),
															params: {id: transactionId},
															openerCt: this,
															defaultIconCls: 'book-red'
														});
													}
													else {
														TCG.showError('This is a grouped position.  Transaction details are not available at the grouped level.', 'Grouped Position');
													}
												}
											}
										}
									],
									columnsConfig: [
										{header: 'Total Lots', type: 'int', dataIndex: 'totalPositions', width: 20, hidden: true},
										{header: 'Accounting Transaction ID', dataIndex: 'accountingTransactionId', width: 30, hidden: true},
										{header: 'Accounting Journal ID', dataIndex: 'accountingJournalId', width: 30, hidden: true},
										{header: 'Transaction Source ID', dataIndex: 'fkFieldId', width: 30, hidden: true},
										{header: 'Holding Account', dataIndex: 'holdingInvestmentAccount.number', width: 100, hidden: true},
										{header: 'Client Account', dataIndex: 'clientInvestmentAccount.label', width: 50, hidden: true},
										{header: 'Security', width: 50, dataIndex: 'investmentSecurity.symbol', hidden: true},
										{header: 'Open Date', width: 45, dataIndex: 'originalTransactionDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
										{header: 'Settle Date', width: 45, dataIndex: 'settlementDate', hidden: true},
										{header: 'FX Rate', width: 40, dataIndex: 'marketFxRate', type: 'float', useNull: true, hidden: true},
										{header: 'Quantity', width: 40, dataIndex: 'remainingQuantity', type: 'float', summaryType: 'sum', useNull: true, negativeInRed: true},
										{header: 'Open Price', width: 50, dataIndex: 'price', type: 'float', useNull: true},
										{header: 'Market Price', width: 50, dataIndex: 'marketPrice', type: 'float', useNull: true, hidden: true},
										{header: 'Market Value', width: 50, dataIndex: 'notionalValueLocal', type: 'currency', summaryType: 'sum', useNull: true},
										{header: 'Base Market Value', width: 60, dataIndex: 'notionalValueBase', type: 'currency', summaryType: 'sum', useNull: true},
										{header: 'OTE', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency', summaryType: 'sum', useNull: true},
										{header: 'Base OTE', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency', summaryType: 'sum', useNull: true},
										{header: 'Closed Position', width: 40, dataIndex: 'closedPosition', type: 'boolean', hidden: true}
									]
								},


									{
										xtype: 'formgrid',
										title: 'External Positions (Unmatched)',
										name: 'externalUnmatchedPositionsGrid',
										storeRoot: 'unmappedExternalPositions',
										dtoClass: 'com.clifton.reconcile.position.external.ReconcilePositionExternalDailyGrouping',
										collapsible: false,
										layout: 'fit',
										flex: 1,
										readOnly: true,
										useFilters: true,
										autoHeight: false,
										hideStandardButtons: true,
										plugins: {ptype: 'gridsummary'},
										viewConfig: {forceFit: true, emptyText: 'All external positions have been mapped to one of our positions', deferEmptyText: false},
										sm: new Ext.grid.RowSelectionModel({singleSelect: true}),
										columnsConfig: [
											{header: 'Total Lots', type: 'int', dataIndex: 'totalPositions', width: 20, hidden: true},
											{header: 'Holding Account', dataIndex: 'holdingAccount.number', hidden: true},
											{header: 'Client Account', dataIndex: 'clientAccount.label', hidden: true},
											{header: 'Security', width: 50, dataIndex: 'investmentSecurity.symbol', hidden: true},
											{header: 'Investment Currency', dataIndex: 'investmentCurrency.symbol', hidden: true},
											{header: 'Base Currency', dataIndex: 'baseCurrency.symbol', hidden: true},
											{header: 'Position Date', width: 50, dataIndex: 'positionDate', hidden: true},
											{header: 'Open Date', width: 45, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
											{header: 'Settle Date', width: 45, dataIndex: 'settleDate', hidden: true},
											{header: 'FX Rate', width: 40, dataIndex: 'fxRate', type: 'float', useNull: true, hidden: true},
											{header: 'Quantity', width: 42, dataIndex: 'quantity', type: 'float', summaryType: 'sum', useNull: true, negativeInRed: true},
											{header: 'Open Price', width: 50, dataIndex: 'tradePrice', type: 'float', useNull: true},
											{header: 'Market Price', width: 50, dataIndex: 'marketPrice', type: 'float', useNull: true, hidden: true},
											{header: 'Market Value', width: 50, dataIndex: 'marketValueLocal', type: 'currency', summaryType: 'sum', useNull: true},
											{header: 'Base Market Value', width: 60, dataIndex: 'marketValueBase', type: 'currency', summaryType: 'sum', useNull: true},
											{header: 'OTE', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency', summaryType: 'sum', useNull: true},
											{header: 'Base OTE', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency', summaryType: 'sum', useNull: true}
										]
									}]
							}]

					}]
			}]
		},

			{
				title: 'Our Positions',
				items: [{
					name: 'accountingPositionDailyListFind',
					xtype: 'gridpanel',
					instructions: 'Lot level positions for selected account and security',
					getLoadParams: function() {
						const win = this.getWindow();
						return {
							holdingInvestmentAccountId: win.defaultData.holdingAccountId,
							positionDate: TCG.parseDate(win.defaultData.positionDate).format('m/d/Y'),
							investmentSecurityId: win.defaultData.investmentSecurity.id
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Security', width: 50, dataIndex: 'accountingTransaction.investmentSecurity.symbol'},
						{header: 'Position Date', width: 50, dataIndex: 'positionDate', type: 'date'},
						{header: 'Open Date', width: 50, dataIndex: 'accountingTransaction.originalTransactionDate', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},

						{header: 'FX Rate', width: 50, dataIndex: 'marketFxRate', type: 'float'},
						{header: 'Open Price', width: 50, dataIndex: 'accountingTransaction.price', type: 'float'},
						{header: 'Market Price', width: 50, dataIndex: 'marketPrice', type: 'float'},
						{header: 'Open Quantity', width: 50, dataIndex: 'accountingTransaction.quantity', type: 'float', hidden: true},
						{header: 'Remaining Quantity', width: 50, dataIndex: 'remainingQuantity', type: 'float'},
						{header: 'Notional Value Local', width: 50, dataIndex: 'notionalValueLocal', type: 'currency'},
						{header: 'Notional Value Base', width: 50, dataIndex: 'notionalValueBase', type: 'currency'},
						{header: 'Cost Basis Local', width: 50, dataIndex: 'remainingCostBasisLocal', type: 'currency'},
						{header: 'Cost Basis Base', width: 50, dataIndex: 'remainingCostBasisBase', type: 'currency'},
						{header: 'OTE Local', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency'},
						{header: 'OTE Base', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency'}
					]
				}]
			},


			{
				title: 'External Positions',
				items: [{
					name: 'reconcilePositionExternalDailyListFind',
					xtype: 'gridpanel',
					instructions: 'External position details for selected account and security',
					getLoadParams: function() {
						const win = this.getWindow();
						return {
							holdingAccountId: win.defaultData.holdingAccountId,
							positionDate: TCG.parseDate(win.defaultData.positionDate).format('m/d/Y'),
							investmentSecurityId: win.defaultData.investmentSecurity.id
						};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holding Account', width: 100, dataIndex: 'holdingAccount', hidden: true},
						{header: 'Client Account', width: 100, dataIndex: 'clientAccount', hidden: true},
						{header: 'Security', width: 50, dataIndex: 'investmentSecurity.symbol'},
						{header: 'Currency', width: 50, dataIndex: 'investmentCurrency.symbol', hidden: true},
						{header: 'Position Date', width: 50, dataIndex: 'positionDate', type: 'date'},
						{header: 'Open Date', width: 50, dataIndex: 'tradeDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Settle Date', width: 50, dataIndex: 'settleDate', type: 'date', hidden: true},

						{header: 'FX Rate', width: 50, dataIndex: 'fxRate', type: 'float'},
						{header: 'Open Price', width: 50, dataIndex: 'tradePrice', type: 'float'},
						{header: 'Market Price', width: 50, dataIndex: 'marketPrice', type: 'float'},
						{header: 'Quantity', width: 50, dataIndex: 'quantity', type: 'float'},
						{header: 'Market Value Local', width: 50, dataIndex: 'marketValueLocal', type: 'currency'},
						{header: 'Market Value Base', width: 50, dataIndex: 'marketValueBase', type: 'currency'},
						{header: 'Cost Basis Local', width: 50, dataIndex: 'remainingCostBasisLocal', type: 'currency'},
						{header: 'Cost Basis Base', width: 50, dataIndex: 'remainingCostBasisBase', type: 'currency'},
						{header: 'OTE Local', width: 50, dataIndex: 'openTradeEquityLocal', type: 'currency'},
						{header: 'OTE Base', width: 50, dataIndex: 'openTradeEquityBase', type: 'currency'},
						{header: 'Short Position', dataIndex: 'shortPosition', type: 'boolean', width: 15, hidden: true}
					]
				}]
			},


			{
				title: 'GL Details',
				items: [{
					xtype: 'accounting-transactionGrid',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.ds.setDefaultSort('originalTransactionDate', 'DESC');
						}

						const data = this.getWindow().defaultData;
						if (data) {
							return {
								requestedMaxDepth: 3,
								holdingInvestmentAccountId: data.holdingAccountId,
								investmentSecurityId: data.investmentSecurity.id,
								accountingAccountName: 'Position'
							};
						}
						return false;
					}
				}]
			},

			{
				title: 'Trade Fills',
				items: [{
					name: 'tradeFillListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all trade fills for this account and security.',
					forceFit: false,

					columns: [
						{header: 'ID', width: 50, dataIndex: 'id', hidden: true},
						{header: 'Trade ID', width: 50, dataIndex: 'trade.id', hidden: true},
						{header: 'Workflow Status', width: 100, dataIndex: 'trade.workflowStatus.name', filter: {type: 'combo', searchFieldName: 'workflowStatusId', url: 'workflowStatusListFind.json?assignmentTableName=Trade'}, hidden: true},
						{header: 'Workflow State', width: 100, dataIndex: 'trade.workflowState.name', filter: {searchFieldName: 'workflowStateName'}},
						{header: 'Trade Type', width: 90, dataIndex: 'trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, hidden: true},
						{header: 'Trade Group Type', width: 100, dataIndex: 'trade.tradeGroup.tradeGroupType.name', filter: {type: 'combo', searchFieldName: 'tradeGroupTypeId', url: 'tradeGroupTypeListFind.json'}},
						{header: 'Destination', width: 90, dataIndex: 'trade.tradeDestination.name', filter: {type: 'combo', searchFieldName: 'tradeDestinationId', url: 'tradeDestinationListFind.json'}, hidden: true},
						{header: 'Account #', width: 70, dataIndex: 'trade.clientInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'Client Account', width: 250, dataIndex: 'trade.clientInvestmentAccount.name', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, hidden: true},
						{header: 'Holding Account', width: 100, dataIndex: 'trade.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}, hidden: true},
						{header: 'Holding Company', width: 150, dataIndex: 'trade.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingAccountIssuingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Executing Broker', width: 150, dataIndex: 'trade.executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
						{header: 'Description', width: 200, dataIndex: 'trade.description', hidden: true},
						{
							header: 'Buy/Sell', width: 55, dataIndex: 'trade.buy', filter: {searchFieldName: 'buy'}, type: 'boolean',
							renderer: function(v, metaData) {
								metaData.css = v ? 'buy-light' : 'sell-light';
								return v ? 'BUY' : 'SELL';
							}
						},
						{header: 'Original Face', width: 80, dataIndex: 'trade.originalFace', type: 'float', useNull: true, hidden: true},
						{header: 'Quantity', width: 80, dataIndex: 'quantity', type: 'float', useNull: true},
						{header: 'Notional Multiplier', width: 100, dataIndex: 'trade.notionalMultiplier', type: 'float', useNull: true, hidden: true},
						{header: 'Price Multiplier', width: 100, dataIndex: 'trade.investmentSecurity.priceMultiplier', type: 'float', hidden: true, filter: {searchFieldName: 'priceMultiplier'}},
						{header: 'Security', width: 70, dataIndex: 'trade.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
						{header: 'Investment Hierarchy', width: 150, dataIndex: 'trade.investmentSecurity.instrument.hierarchy.labelExpanded', filter: {type: 'combo', searchFieldName: 'investmentHierarchyId', displayField: 'labelExpanded', url: 'investmentInstrumentHierarchyListFind.json?assignmentAllowed=true', queryParam: 'labelExpanded', listWidth: 600}, hidden: true},
						{header: 'Security Name', width: 100, dataIndex: 'trade.investmentSecurity.name', hidden: true},
						{header: 'CCY Denom', width: 100, dataIndex: 'trade.investmentSecurity.instrument.tradingCurrency.symbol', filter: {type: 'combo', searchFieldName: 'securityCurrencyDenominationId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
						{header: 'Exchange Rate', width: 100, dataIndex: 'trade.exchangeRateToBase', filter: {searchFieldName: 'exchangeRateToBase'}, type: 'float', useNull: true},
						{header: 'Fill Price', width: 90, dataIndex: 'notionalUnitPrice', type: 'float', useNull: true},
						{header: 'Accrual', width: 90, dataIndex: 'trade.accrualAmount', type: 'currency', hidden: true, useNull: true},
						{header: 'Commission Per Unit', width: 120, dataIndex: 'trade.commissionPerUnit', filter: {searchFieldName: 'tradeCommissionPerUnit'}, type: 'float', hidden: true},
						{header: 'Commission Amount', width: 120, dataIndex: 'trade.commissionAmount', filter: {searchFieldName: 'tradeCommissionAmount'}, type: 'currency'},
						{header: 'Fee Amount', width: 90, dataIndex: 'trade.feeAmount', filter: {searchFieldName: 'tradeFeeAmount'}, type: 'currency'},
						{header: 'Block', width: 50, dataIndex: 'trade.blockTrade', type: 'boolean', hidden: true, tooltip: 'A Block Trade is a privately negotiated futures, options or combination transaction that is permitted to be executed apart from the public auction market. Block Trades will be charged a different commission rate than non-block trades.'},
						{header: 'Trader', width: 100, dataIndex: 'trade.traderUser.label', filter: {type: 'combo', searchFieldName: 'traderId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Traded On', width: 90, dataIndex: 'trade.tradeDate', filter: {searchFieldName: 'tradeDate'}, defaultSortColumn: true, defaultSortDirection: 'desc'},
						{header: 'Settled On', width: 90, dataIndex: 'trade.settlementDate', filter: {searchFieldName: 'settlementDate'}}
					],
					getLoadParams: function(firstLoad) {
						const data = this.getWindow().defaultData;
						if (data) {
							return {
								requestedMaxDepth: 6,
								holdingInvestmentAccountId: data.holdingAccountId,
								securityId: data.investmentSecurity.id
							};
						}
						return false;
					},
					editor: {
						detailPageClass: 'Clifton.trade.TradeWindow',
						drillDownOnly: true,
						getDetailPageId: function(gridPanel, row) {
							return row.json.trade.id;
						}
					}
				}]
			}]
	}]

});
