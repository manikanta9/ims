Clifton.reconcile.position.ReconcilePositionSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'reconcilePositionSetupWindow',
	title: 'Position and Activity Reconciliation',
	iconCls: 'reconcile',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		reloadOnChange: true,
		items: [
			{
				title: 'Reconciliation',
				items: [{
					name: 'reconcilePositionAccountExtendedListFind',
					xtype: 'gridpanel',
					instructions: 'Account level summary for positions reconciled on the specified date. Double click on a specific account to see details.',
					viewNames: ['Activity Reconciliation', 'Position Reconciliation', 'Full Reconciliation'],
					additionalPropertiesToRequest: 'label|definition.id|holdingAccount.id|positionDate|clientAccount.id',
					getRowSelectionModel: function() {
						if (typeof this.rowSelectionModel !== 'object') {
							this.rowSelectionModel = new Ext.grid.RowSelectionModel({singleSelect: false});
						}
						return this.rowSelectionModel;
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account Group', name: 'clientAccountGroup', valueField: 'id', displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
							{fieldLabel: 'Position Date', xtype: 'toolbar-datefield', name: 'positionDate'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Client Account', width: 150, dataIndex: 'clientAccount.label', filter: {searchFieldName: 'clientAccount'}, defaultSortColumn: true},
						{header: 'Holding Account', width: 45, dataIndex: 'holdingAccount.number', filter: {searchFieldName: 'holdingAccount'}},
						{header: 'Holding Company', width: 80, dataIndex: 'holdingAccount.issuingCompany.name', filter: {searchFieldName: 'holdingAccountIssuerName'}},
						{header: 'Positions', width: 32, dataIndex: 'positionCount', type: 'int'},
						{
							header: 'Market Value Diff', width: 51, dataIndex: 'marketValueDifference', type: 'currency', hidden: true, viewNames: ['Full Reconciliation', 'Position Reconciliation'],
							renderer: function(v, metaData, r) {
								if (v === 0) {
									return '';
								}
								if (v > 50 || v < -50) {
									metaData.css = 'ruleViolationBig';
								}
								else if (v > 5 || v < -5) {
									metaData.css = 'ruleViolation';
								}
								return Ext.util.Format.number(v, '0,000.00');
							}
						},
						{
							header: 'OTE Diff', width: 45, dataIndex: 'oteValueDifference', type: 'currency', hidden: true, viewNames: ['Full Reconciliation', 'Position Reconciliation'],
							renderer: function(v, metaData, r) {
								if (v === 0) {
									return '';
								}
								if (v > 50 || v < -50) {
									metaData.css = 'ruleViolationBig';
								}
								else if (v > 5 || v < -5) {
									metaData.css = 'ruleViolation';
								}
								return Ext.util.Format.number(v, '0,000.00');
							}
						},
						{
							header: 'Realized Diff', width: 50, dataIndex: 'todayRealizedGainLossDifference', type: 'currency', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation'],
							renderer: function(v, metaData, r) {
								if (v === 0) {
									return '';
								}
								if (v > 50 || v < -50) {
									metaData.css = 'ruleViolationBig';
								}
								else if (v > 5 || v < -5) {
									metaData.css = 'ruleViolation';
								}
								return Ext.util.Format.number(v, '0,000.00');
							}
						},
						{
							header: 'Commission Diff', width: 50, dataIndex: 'todayCommissionDifference', type: 'currency', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation'],
							renderer: function(v, metaData, r) {
								if (v === 0) {
									return '';
								}
								if (v > 50 || v < -50) {
									metaData.css = 'ruleViolationBig';
								}
								else if (v > 5 || v < -5) {
									metaData.css = 'ruleViolation';
								}
								return Ext.util.Format.number(v, '0,000.00');
							}
						},
						{header: 'Qty Rec', tooltip: 'Does our Quantity match external Quantity?', width: 30, dataIndex: 'quantityReconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', hidden: true, viewNames: ['Full Reconciliation', 'Position Reconciliation']},
						{header: 'Position Rec', tooltip: 'Do all of our Positions match all external Positions?', width: 36, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', hidden: true, viewNames: ['Full Reconciliation', 'Position Reconciliation']},
						{header: 'Activity Rec', width: 36, dataIndex: 'realizedAndCommissionReconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation']}
					],

					listeners: {
						afterrender: function(gridPanel) {
							const el = gridPanel.getEl();
							el.on('contextmenu', function(e, target) {
								const g = gridPanel.grid;
								const rowIndex = g.view.findRowIndex(target);
								if (rowIndex !== false) {
									e.preventDefault();
									if (!g.drillDownMenu) {
										g.drillDownMenu = new Ext.menu.Menu({
											items: [
												{
													text: 'Reconcile Selected', iconCls: 'reconcile',
													handler: function() {
														gridPanel.reconcileHandler();
													}
												},
												{
													text: 'Open M2M', iconCls: 'exchange',
													handler: function() {
														gridPanel.openM2M();
													}
												}
											]
										});
									}
									g.drillDownMenu.rowIndex = rowIndex;
									TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems(g.ownerGridPanel, g.drillDownMenu, rowIndex, g.view.findCellIndex(target), e);
								}
							}, gridPanel);
						}
					},

					openM2M: function() {
						const gridPanel = this;
						const rowIndex = gridPanel.grid.drillDownMenu.rowIndex;
						const t = this.getTopToolbar();
						const positionDate = (TCG.getChildByName(t, 'positionDate').getValue()).format('m/d/Y');

						const row = gridPanel.grid.getStore().getAt(rowIndex);
						const m2mLoader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							params: {clientAccountId: row.json.clientAccount.id, date: positionDate},
							onLoad: function(record, conf) {
								if (record) {
									const config = {
										defaultDataIsReal: true,
										defaultData: record,
										params: {id: record.id}
									};
									TCG.createComponent('Clifton.accounting.m2m.M2MDailyWindow', config);
								}
								else {
									TCG.showError('Cannot find M2M daily associated with that record.');
								}
							}
						});
						m2mLoader.load('accountingM2MDailyByClientAccountAndDate.json?enableOpenSessionInView=true&requestedMaxDepth=4');

					},

					reconcileHandler: function() {
						const gridPanel = this;
						const t = this.getTopToolbar();
						const positionDate = (TCG.getChildByName(t, 'positionDate').getValue()).format('m/d/Y');
						const sm = gridPanel.grid.getSelectionModel();

						if (TCG.isBlank(positionDate)) {
							TCG.showError('You must select a date.');
						}
						if (sm.getCount() === 0) {
							TCG.showError('Please select an account to be reconciled.', 'No Account(s) Selected');
						}

						const paramsList = [];

						for (let i = 0; i < sm.getSelections().length; i++) {
							paramsList[i] = {
								reconcilePositionDefinitionId: sm.getSelections()[i].json.definition.id,
								positionDate: positionDate,
								holdingAccountId: sm.getSelections()[i].json.holdingAccount.id
							};
						}

						this.doReconcile(paramsList, gridPanel);

					},
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setDefaultView('Position Reconciliation');
						}

						const t = this.getTopToolbar();

						let dateValue;
						const bd = TCG.getChildByName(t, 'positionDate');
						if (TCG.isNotBlank(bd.getValue())) {
							dateValue = (bd.getValue()).format('m/d/Y');
						}
						else {
							const dd = this.getWindow().defaultData;
							if (firstLoad && dd && dd.positionDate) { // positionDate can be defaulted by the caller of window open
								dateValue = dd.positionDate.format('m/d/Y');
							}
							else {
								dateValue = Clifton.calendar.getBusinessDayFrom(-1).format('m/d/Y');
							}
							bd.setValue(dateValue);
						}

						const accountGroupCombo = TCG.getChildByName(t, 'clientAccountGroup');
						const clientAccountGroupId = accountGroupCombo.getValue();

						const excludeClosedPositions = this.viewName === 'Position Reconciliation';
						return {
							positionDate: dateValue,
							requestedMaxDepth: 4,
							excludeClosedPositions: excludeClosedPositions,
							clientAccountGroupId: clientAccountGroupId
							//excludeInvestmentAccountGroupName: 'Non-WorkingAccountGroup'
						};
					},
					doReconcile: function(paramsList, gridPanel, count) {
						if (!count) {
							count = 0;
						}
						const params = paramsList[count];
						const loader = new TCG.data.JsonLoader({
							waitMsg: 'Reconciling...',
							waitTarget: this,
							params: params,
							timeout: 120000,
							onLoad: function(record, conf) {
								count++;
								if (count === paramsList.length) {
									gridPanel.reload();
									gridPanel.grid.getSelectionModel().clearSelections(true);
								}
								else {
									gridPanel.doReconcile(paramsList, gridPanel, count);
								}
							},
							onFailure: function() {
								// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
								gridPanel.reload();
							}
						});
						loader.load('reconcilePositionRebuild.json');
					},
					switchToViewBeforeReload: function(viewName) {
						this.viewName = viewName;
					},
					editor: {
						detailPageClass: 'Clifton.reconcile.position.ReconcileAccountWindow',
						drillDownOnly: true,
						getDefaultData: function(gridPanel, row) {
							const t = gridPanel.getTopToolbar();
							const positionDate = (TCG.getChildByName(t, 'positionDate').getValue()).format('m/d/Y');
							// use selected row parameters for drill down filtering
							return {
								label: row.json.label,
								reconcilePositionDefinitionId: row.json.definition.id,
								positionDate: positionDate,
								holdingAccountId: row.json.holdingAccount.id,
								viewName: gridPanel.viewName
							};
						},
						addEditButtons: function(t, gridPanel) {

							t.add({
								text: 'Auto-Reconcile...',
								tooltip: 'Auto-reconcile accounts within a specified threshold.',
								iconCls: 'row_reconciled',
								scope: gridPanel,
								handler: function() {
									const fb = TCG.data.getData('investmentAccountTypeByName.json?name=Futures Broker', t, 'investment.account.type.Futures Broker');
									TCG.createComponent('Clifton.reconcile.position.AutoReconcileWindow', {
										defaultData: {
											positionDate: TCG.getChildByName(t, 'positionDate').getValue().format('m/d/Y'),
											holdingAccountTypeId: {value: fb.id, text: fb.name}
										},
										openerCt: gridPanel
									});
								}
							});
							t.add('-');

							t.add({
								text: 'Copy Notes to M2M',
								tooltip: 'Copy all reconciliation notes to the M2M note.',
								iconCls: 'copy',
								scope: this,
								handler: function() {
									const t = gridPanel.getTopToolbar();
									const bd = TCG.getChildByName(t, 'positionDate');
									const dateValue = (bd.getValue()).format('m/d/Y');
									Ext.Msg.confirm('Copy Notes', 'Would you like to copy all reconciliation notes to the M2M?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: gridPanel,
												waitMsg: 'Copying...',
												params: {
													positionDate: dateValue
												}
											});
											loader.load('reconcilePositionNotesToM2MCopy.json');
										}
									});
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Detailed Rec',
				items: [{
					xtype: 'reconcile-position-match-grid',
					instructions: 'A list of positions grouped by security across all account. Hover over highlighted mismatch to see details. OTE is Open Trade Equity (unrealized gain/loss).',
					autoHideFxFields: false,
					showBreaksOnly: true,
					afterInitComponent: function() {
						const cm = this.getColumnModel();
						cm.setHidden(1, false);
						cm.setHidden(2, false);
					},
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Client Account Group', name: 'clientAccountGroup', valueField: 'id', displayField: 'label', xtype: 'toolbar-combo', url: 'investmentAccountGroupListFind.json', detailPageClass: 'Clifton.investment.account.AccountGroupWindow'},
							{fieldLabel: 'Position Date', xtype: 'toolbar-datefield', name: 'positionDate'}
						];
					},
					plugins: undefined,
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setDefaultView('Position Rec (Side by Side)');
						}
						const t = this.getTopToolbar();
						let dateValue;
						const bd = TCG.getChildByName(t, 'positionDate');
						if (TCG.isNotBlank(bd.getValue())) {
							dateValue = (bd.getValue()).format('m/d/Y');
						}
						else {
							const prevBD = Clifton.calendar.getBusinessDayFrom(-1);
							dateValue = prevBD.format('m/d/Y');
							bd.setValue(dateValue);
						}

						const accountGroupCombo = TCG.getChildByName(t, 'clientAccountGroup');
						const clientAccountGroupId = accountGroupCombo.getValue();

						return Ext.apply({
								positionDate: dateValue,
								clientAccountGroupId: clientAccountGroupId
							}, this.getViewsParams()
						);
					}
				}]
			},


			{
				title: 'Price Overrides',
				items: [{
					name: 'reconcilePositionExternalPriceOverrideListFind',
					xtype: 'gridpanel',
					instructions: 'Use this screen to override external security end of day prices that have been confirmed to be inaccurate.',
					getLoadParams: function() {
						this.setFilterValue('positionDate', {'after': Clifton.calendar.getBusinessDayFrom(-5)});
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Holding Company', width: 90, dataIndex: 'holdingCompany.name', filter: {searchFieldName: 'holdingCompanyName'}},
						{header: 'Security', width: 45, dataIndex: 'investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', url: 'investmentSecurityListFind.json'}},
						{header: 'Date', width: 50, dataIndex: 'positionDate', type: 'date'},
						{header: 'External Price', width: 50, dataIndex: 'externalPrice', type: 'float', useNull: true},
						{header: 'Override Price', width: 50, dataIndex: 'overridePrice', type: 'float', useNull: true},
						{header: 'Note', width: 200, dataIndex: 'note'}
					],
					editor: {
						detailPageClass: 'Clifton.reconcile.position.ReconcileBrokerPriceOverride'
					}
				}]
			},


			{
				title: 'Administration',
				defaultHoldingAccountTypeName: 'Futures Broker',
				items: [Clifton.accounting.DailyPositionsRebuildForm]
			},


			{
				title: 'External Loads',
				items: [{
					xtype: 'integration-importRunEventGrid',
					targetApplicationName: 'IMS',
					importEventName: 'Broker Daily Positions'
				}]
			},


			{
				title: 'Definitions',
				items: [{
					name: 'reconcilePositionDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'The following reconciliation definitions have been setup in the system.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Name', width: 150, dataIndex: 'name'},
						{header: 'Description', width: 250, dataIndex: 'description', hidden: true},
						{header: 'Our Table', width: 100, dataIndex: 'ourSystemTable.name', hidden: true},
						{header: 'External Table', width: 100, dataIndex: 'externalSystemTable.name', hidden: true},
						{header: 'Instrument Group', width: 100, dataIndex: 'investmentGroup.name'},
						{header: 'Holding Company', width: 100, dataIndex: 'holdingCompany.name'},
						{header: 'Purpose', width: 80, dataIndex: 'accountRelationshipPurpose.name'},
						{header: 'Local Only', width: 50, dataIndex: 'localOnly', type: 'boolean'},
						{header: 'Zero Remaining Quantity', width: 80, dataIndex: 'zeroRemainingQuantity', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.reconcile.position.ReconcilePositionDefinitionWindow',
						drillDownOnly: true
					}
				}]
			}]
	}]
});


Clifton.reconcile.position.AutoReconcileWindow = Ext.extend(TCG.app.Window, {
	title: 'Auto-Reconcile',
	iconCls: 'row_reconciled',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	buttonAlign: 'right',
	height: 500,
	width: 700,
	modal: true,
	// savedSinceOpen -  indicates whether records have been updated due to a reconciliation

	items: [
		{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'Limit the accounts to be reconciled for faster processing.  Enter maximum threshold amounts to reconcile within a margin of difference.',
			labelWidth: 170,

			items: [
				{name: 'positionDate', xtype: 'hidden'},
				{fieldLabel: 'Holding Account Type', xtype: 'combo', hiddenName: 'holdingAccountTypeId', url: 'investmentAccountTypeListFind.json?ourAccount=false', pageSize: 10, qtip: 'Filter holding company and/or holding account selections by account type'},
				{
					fieldLabel: 'Holding Company', xtype: 'combo', hiddenName: 'holdingCompanyId',
					url: 'businessCompanyListFind.json?issuer=true', pageSize: 10, qtip: 'Filter the reconciliation by selected holding company',
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const f = combo.getParentForm().getForm();
						const atv = f.findField('holdingAccountTypeId').getValue();
						combo.store.baseParams = atv ? {activeIssuerInvestmentAccountTypeId: atv} : {};
					}

				},
				{
					fieldLabel: 'Holding Account', xtype: 'combo', hiddenName: 'holdingAccountId', url: 'investmentAccountListFind.json?ourAccount=false', displayField: 'label', pageSize: 10, qtip: 'Filter the reconciliation by selected holding account',
					listeners: {
						beforequery: function(queryEvent) {
							const bp = {};

							const combo = queryEvent.combo;
							const f = combo.getParentForm().getForm();
							const v = f.findField('holdingCompanyId').getValue();
							if (v) {
								bp.issuingCompanyId = v;
							}
							const atv = f.findField('holdingAccountTypeId').getValue();
							if (atv) {
								bp.accountTypeId = atv;
							}
						}
					}
				},

				{boxLabel: 'Clean build (delete all un-reconciled entries).', name: 'cleanRebuild', xtype: 'checkbox'},

				{xtype: 'label', html: '&nbsp;'},

				{fieldLabel: 'Max OTE Threshold', name: 'oteMaxThreshold', xtype: 'currencyfield', value: '0.01', minValue: 0, allowBlank: false, qtip: 'If difference in Open Trade Equity (OTE) is less than the stated threshold, the position will be marked as "reconciled."  NOTE: This threshold will be applied to every position in this account.'},
				{fieldLabel: 'Max Market Value Threshold', name: 'marketValueMaxThreshold', xtype: 'currencyfield', value: '0.01', minValue: 0, allowBlank: false, qtip: 'If difference in Market Value is less than the stated threshold, the position will be marked as "reconciled."  NOTE: This threshold will be applied to every position in this account.'},

				{xtype: 'label', html: '&nbsp;'},

				{fieldLabel: 'Max Realized Threshold', name: 'realizedGainLossThreshold', xtype: 'currencyfield', value: '0.00', minValue: 0, allowBlank: false, qtip: 'If difference in Realized Gain Loss is less than the stated threshold, activity will be marked as "reconciled."  NOTE: This threshold will be applied to every closed position in this account.'},
				{fieldLabel: 'Max Commission Threshold', name: 'commissionThreshold', xtype: 'currencyfield', value: '0.00', minValue: 0, allowBlank: false, qtip: 'If difference in Commission is less than the stated threshold, activity will be marked as "reconciled."  NOTE: This threshold will be applied to every closed position in this account.'}

			],

			buttons: [{
				text: 'Reconcile Positions',
				iconCls: 'run',
				width: 150,
				handler: function() {
					const owner = this.findParentByType('formpanel');
					const form = owner.getForm();
					form.submit(Ext.applyIf({
						url: 'reconcilePositionRebuild.json',
						success: function(form, action) {
							owner.getWindow().savedSinceOpen = true;

							Ext.Msg.alert('Success', action.result.result, function() {
								const grid = owner.ownerCt.items.get(1);
								grid.reload.defer(300, grid);
							});
						}
					}, Ext.applyIf({timeout: 240}, TCG.form.submitDefaults)));
				}
			}]
		},

		{
			xtype: 'core-scheduled-runner-grid',
			typeName: 'RECONCILE-POSITION-AUTO',
			instantRunner: true,
			instructions: 'The status of this reconciliation is shown below.',
			title: 'Position Reconciliation',
			flex: 1
		}
	],

	render: function() {
		TCG.Window.superclass.render.apply(this, arguments);

		const win = this;
		win.on('beforeclose', function() {
			if (win.savedSinceOpen && win.openerCt && !win.openerCt.isDestroyed && win.openerCt.reload) {
				win.openerCt.reload();
			}
		});
	}
});
