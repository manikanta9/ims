Ext.ns('Clifton.reconcile', 'Clifton.reconcile.trade', 'Clifton.reconcile.position');


Clifton.reconcile.position.PositionMatchListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'reconcilePositionListFind',
	instructions: 'A list of positions grouped by security for selected account. Hover over highlighted mismatch to see details. OTE is Open Trade Equity (unrealized gain/loss).',
	additionalPropertiesToRequest: 'id|investmentSecurity.id|investmentSecurity.label|investmentSecurity.instrument.tradingCurrency.id|ourPrice|externalPrice|label|positionAccount.holdingAccount.id|' +
		'positionAccount.clientAccount.id|positionAccount.holdingAccount.number|positionAccount.holdingAccount.baseCurrency.id|positionAccount.definition.localOnly|positionAccount.holdingAccount.issuingCompany.id|' +
		'positionAccount.holdingAccount.issuingCompany.name|positionAccount.holdingAccount.issuingCompany.label|positionAccount.positionDate|fxRateDifference|priceDifference|' +
		'quantityDifference|closedQuantityDifference|marketValueLocalDifference|marketValueBaseDifference|oteLocalDifference|oteBaseDifference|commissionLocalDifference|realizedGainLossLocalDifference',
	missMatchValueCss: 'ruleViolation',
	autoHideFxFields: true, // auto hide FX fields for definitions that support local currency only
	pageSize: 200,

	viewNames: ['Activity Reconciliation', 'Activity Rec (Side by Side)', 'Position Reconciliation', 'Position Rec (Side by Side)', 'Mark to Market View', 'Full Reconciliation'],
	configureViews: function(menu, gridPanel) {
		menu.add('-');
		menu.add({
			text: 'Show Breaks Only',
			xtype: 'menucheckitem',
			checked: this.showBreaksOnly,
			handler: function(item, event) {
				gridPanel.showBreaksOnly = !item.checked;
				gridPanel.reload();
			}
		}, {
			text: 'Note Field Populated',
			xtype: 'menucheckitem',
			handler: function(item, event) {
				gridPanel.noteNotNull = !item.checked;
				gridPanel.reload();
			}
		});
	},
	getRowSelectionModel: function() {
		if (typeof this.rowSelectionModel != 'object') {
			this.rowSelectionModel = new Ext.grid.RowSelectionModel({singleSelect: false});
		}
		return this.rowSelectionModel;
	},
	getLoadParams: function(firstLoad) {
		// default position date to previous business day
		const win = this.getWindow();
		if (firstLoad) {
			this.setDefaultView(win.defaultData.viewName || 'Position Reconciliation');
		}
		return Ext.apply({reconcilePositionAccountId: win.params.id}, this.getViewsParams());
	},
	isCurrentViewForActivityRec: function() {
		return TCG.startsWith(this.currentViewName, 'Activity Rec');
	},
	isCurrentViewForPositionRec: function() {
		return TCG.startsWith(this.currentViewName, 'Position Rec');
	},
	getViewsParams: function() {
		const excludeClosedPositions = this.isCurrentViewForPositionRec();
		const realizedOrCommissionPresent = this.isCurrentViewForActivityRec();
		return {
			excludeClosedPositions: excludeClosedPositions,
			realizedOrCommissionPresent: realizedOrCommissionPresent,
			reconciliationBreaksPresent: this.showBreaksOnly,
			positionBreaksPresent: excludeClosedPositions && this.showBreaksOnly,
			activityBreaksPresent: realizedOrCommissionPresent && this.showBreaksOnly,
			noteNotNull: this.noteNotNull
		};
	},
	initComponent: function() {
		TCG.grid.GridPanel.prototype.initComponent.call(this, arguments);

		if (this.autoHideFxFields) {
			const win = this.getWindow();
			const reconcilePositionAccount = TCG.data.getData('reconcilePositionAccount.json?id=' + win.params.id, this);
			if (reconcilePositionAccount && reconcilePositionAccount.definition.localOnly === true) {
				for (let i = 0; i < this.columns.length; i++) {
					const column = this.columns[i];
					if ((column.dataIndex === 'ourFxRate') || (column.dataIndex === 'externalFxRate') || (column.dataIndex.indexOf('Base') >= 0)) {
						column.hidden = true;
					}
				}
			}
		}
		if (this.afterInitComponent) {
			this.afterInitComponent();
		}
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Client Account', width: 100, dataIndex: 'positionAccount.clientAccount.label', filter: {searchFieldName: 'clientAccount'}, hidden: true},
		{header: 'Holding Account', width: 45, dataIndex: 'positionAccount.holdingAccount.number', filter: {searchFieldName: 'holdingAccount'}, hidden: true},
		{header: 'Holding Company', width: 70, dataIndex: 'positionAccount.holdingAccount.issuingCompany.name', filter: {searchFieldName: 'holdingAccountIssuerName'}, hidden: true},
		{
			header: 'Security', width: 45, dataIndex: 'investmentSecurity.symbol', defaultSortColumn: true,
			filter: {type: 'combo', searchFieldName: 'investmentSecurityId', url: 'investmentSecurityListFind.json'}
		},

		{
			header: 'FX Rate', width: 40, dataIndex: 'ourFxRate', type: 'float',
			renderer: function(v, metaData, r) {
				if (!r.json.positionAccount.definition.localOnly) {
					const ours = r.data.ourFxRate;
					const external = r.data.externalFxRate;
					const difference = r.json.fxRateDifference;
					if (ours !== external) {
						metaData.css = 'ruleViolation';
						let qtip = '<table>';
						qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000', true) + '</td></tr>';
						qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000', true) + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true, '0,000', true) + '</td></tr>';
						qtip += '</table>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return TCG.numberFormat(v, '0,000', true);
				}
			}
		},
		{header: 'External FX Rate', width: 40, dataIndex: 'externalFxRate', type: 'float', useNull: true, hidden: true},
		{header: 'FX Rate Diff', width: 40, dataIndex: 'fxRateDifference', type: 'float', useNull: true, hidden: true, positiveInGreen: true, negativeInRed: true},

		{
			header: 'Market Price', width: 40, dataIndex: 'ourPrice', type: 'float',
			renderer: function(v, metaData, r) {
				const ours = r.data.ourPrice;
				const external = r.data.externalPrice;
				const difference = r.json.priceDifference;
				if (ours !== external) {
					metaData.css = ((difference > 1) || (difference < -1)) ? 'ruleViolationBig' : 'ruleViolation';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + TCG.numberFormat(ours, '0,000', true) + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + TCG.numberFormat(external, '0,000', true) + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true, '0,000', true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(v, '0,000', true);
			}
		},
		{header: 'External Market Price', width: 40, dataIndex: 'externalPrice', type: 'float', useNull: true, hidden: true},
		{header: 'Price Diff', width: 40, dataIndex: 'priceDifference', type: 'float', useNull: true, hidden: true, positiveInGreen: true, negativeInRed: true},

		{
			header: 'Quantity', width: 30, dataIndex: 'ourQuantity', type: 'float', viewNames: ['Full Reconciliation', 'Position Reconciliation', 'Position Rec (Side by Side)', 'Mark to Market View'],
			renderer: function(v, metaData, r) {
				const ours = r.data.ourQuantity;
				const external = r.data.externalQuantity;
				const difference = r.json.quantityDifference;
				if (ours !== external) {
					metaData.css = 'ruleViolationBig';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000') + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000') + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(v, '0,000', true);
			}
		},
		{header: 'External Qty', width: 30, dataIndex: 'externalQuantity', type: 'float', useNull: true, hidden: true, viewNames: ['Position Rec (Side by Side)']},
		{header: 'Qty Diff', width: 30, dataIndex: 'quantityDifference', type: 'float', useNull: true, hidden: true, viewNames: ['Position Rec (Side by Side)']},

		{
			header: 'Closed Qty', width: 30, dataIndex: 'ourTodayClosedQuantity', type: 'float', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation', 'Activity Rec (Side by Side)', 'Mark to Market View'],
			renderer: function(v, metaData, r) {
				const ours = r.data.ourTodayClosedQuantity;
				const external = r.data.externalTodayClosedQuantity;
				const difference = r.json.closedQuantityDifference;
				if (ours !== external) {
					metaData.css = 'ruleViolationBig';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000') + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000') + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return TCG.numberFormat(v, '0,000', true);
			}
		},
		{header: 'External Closed Qty', width: 30, dataIndex: 'externalTodayClosedQuantity', type: 'float', useNull: true, hidden: true},

		{
			header: 'Market Value', width: 45, dataIndex: 'ourMarketValueLocal', type: 'currency', viewNames: ['Full Reconciliation', 'Position Reconciliation'],
			renderer: function(v, metaData, r) {
				const ours = r.data.ourMarketValueLocal;
				const external = r.data.externalMarketValueLocal;
				const difference = r.json.marketValueLocalDifference;
				if (ours !== external) {
					metaData.css = ((difference > 50) || (difference < -50)) ? 'ruleViolationBig' : 'ruleViolation';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(v, '0,000.00');
			}
		},
		{header: 'External Market Value', width: 50, dataIndex: 'externalMarketValueLocal', type: 'currency', useNull: true, hidden: true},

		{
			header: 'Base Market Value', width: 55, dataIndex: 'ourMarketValueBase', type: 'currency', summaryType: 'sum', viewNames: ['Activity Reconciliation', 'Position Reconciliation', 'Mark to Market View', 'Full Reconciliation'],
			renderer: function(v, metaData, r) {
				if (!r.json.positionAccount.definition.localOnly) {
					const ours = r.data.ourMarketValueBase;
					const external = r.data.externalMarketValueBase;
					const difference = r.json.marketValueBaseDifference;
					if (ours !== external) {
						metaData.css = ((difference > 50) || (difference < -50)) ? 'ruleViolationBig' : 'ruleViolation';
						let qtip = '<table>';
						qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
						qtip += '</table>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return Ext.util.Format.number(v, '0,000.00');
				}
			},
			summaryRenderer: function(v, metaData, r) {
				return Ext.util.Format.number(v, '0,000.00');
			}
		},
		{header: 'Base External Market Value', width: 50, dataIndex: 'externalMarketValueBase', type: 'currency', hidden: true, useNull: true, summaryType: 'sum'},

		{
			header: 'OTE', width: 40, dataIndex: 'ourOpenTradeEquityLocal', type: 'currency', viewNames: ['Full Reconciliation', 'Position Reconciliation'],
			renderer: function(v, metaData, r) {
				const ours = r.data.ourOpenTradeEquityLocal;
				const external = r.data.externalOpenTradeEquityLocal;
				const difference = r.json.oteLocalDifference;
				if (ours !== external) {
					metaData.css = ((difference > 50) || (difference < -50)) ? 'ruleViolationBig' : 'ruleViolation';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(v, '0,000.00');
			}
		},
		{header: 'External OTE', width: 40, dataIndex: 'externalOpenTradeEquityLocal', type: 'currency', hidden: true, useNull: true},

		{
			header: 'Base OTE', width: 40, dataIndex: 'ourOpenTradeEquityBase', type: 'currency', summaryType: 'sum', viewNames: ['Full Reconciliation', 'Position Reconciliation', 'Position Rec (Side by Side)', 'Mark to Market View'],
			renderer: function(v, metaData, r) {
				if (!r.json.positionAccount.definition.localOnly) {
					const ours = r.data.ourOpenTradeEquityBase;
					const external = r.data.externalOpenTradeEquityBase;
					const difference = r.json.oteBaseDifference;
					if (ours !== external) {
						metaData.css = ((difference > 50) || (difference < -50)) ? 'ruleViolationBig' : 'ruleViolation';
						let qtip = '<table>';
						qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000.00') + '</td></tr>';
						qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
						qtip += '</table>';
						metaData.attr = 'qtip=\'' + qtip + '\'';
					}
					return Ext.util.Format.number(v, '0,000.00');
				}
			},
			summaryRenderer: function(v, metaData, r) {
				return Ext.util.Format.number(v, '0,000.00');
			}
		},
		{header: 'Base External OTE', width: 40, dataIndex: 'externalOpenTradeEquityBase', type: 'currency', hidden: true, useNull: true, summaryType: 'sum', viewNames: ['Position Rec (Side by Side)']},
		{
			header: 'Base OTE Diff', width: 30, dataIndex: 'oteBaseDifference', type: 'currency', summaryType: 'sum', hidden: true, viewNames: ['Position Rec (Side by Side)', 'Mark to Market View'],
			renderer: function(v, metaData, r) {
				const note = r.data.note;
				if (note) {
					metaData.attr = TCG.renderQtip(note);
				}
				return TCG.renderAmount(v, true);
			}
		},

		{
			header: 'Commission', width: 40, dataIndex: 'ourTodayCommissionLocal', type: 'currency', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation', 'Activity Rec (Side by Side)', 'Mark to Market View'],
			renderer: function(v, metaData, r) {
				const ours = r.data.ourTodayCommissionLocal;
				const external = r.data.externalTodayCommissionLocal;
				const difference = r.json.commissionLocalDifference;
				if (ours !== external) {
					metaData.css = ((difference > 50) || (difference < -50)) ? 'ruleViolationBig' : 'ruleViolation';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(v, '0,000.00');
			}
		},
		{header: 'External Commission', width: 40, dataIndex: 'externalTodayCommissionLocal', type: 'currency', hidden: true, useNull: true, viewNames: ['Activity Rec (Side by Side)']},
		{header: 'Local Diff', width: 35, dataIndex: 'commissionLocalDifference', type: 'currency', hidden: true, viewNames: ['Activity Rec (Side by Side)'], positiveInGreen: true, negativeInRed: true},
		{
			header: 'Base Diff', width: 35, dataIndex: 'commissionBaseDifference', type: 'currency', summaryType: 'sum', hidden: true, viewNames: ['Mark to Market View'],
			renderer: function(v, metaData, r) {
				const note = r.data.realizedAndCommissionNote;
				if (note) {
					metaData.attr = TCG.renderQtip(note);
				}
				return TCG.renderAmount(v, true);
			}
		},


		{
			header: 'Realized', width: 40, dataIndex: 'ourTodayRealizedGainLossLocal', type: 'currency', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation', 'Activity Rec (Side by Side)', 'Mark to Market View'],
			renderer: function(v, metaData, r) {
				const ours = r.data.ourTodayRealizedGainLossLocal;
				const external = r.data.externalTodayRealizedGainLossLocal;
				const difference = r.json.realizedGainLossLocalDifference;
				if (ours !== external) {
					metaData.css = ((difference > 50) || (difference < -50)) ? 'ruleViolationBig' : 'ruleViolation';
					let qtip = '<table>';
					qtip += '<tr><td>Ours:</td><td align="right">' + Ext.util.Format.number(ours, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>External:</td><td align="right">' + Ext.util.Format.number(external, '0,000.00') + '</td></tr>';
					qtip += '<tr><td>Difference:</td><td align="right">' + TCG.renderAmount(difference, true) + '</td></tr>';
					qtip += '</table>';
					metaData.attr = 'qtip=\'' + qtip + '\'';
				}
				return Ext.util.Format.number(v, '0,000.00');
			}
		},
		{header: 'External Realized', width: 40, dataIndex: 'externalTodayRealizedGainLossLocal', type: 'currency', hidden: true, useNull: true, viewNames: ['Activity Rec (Side by Side)']},
		{header: 'Local Diff', width: 35, dataIndex: 'realizedGainLossLocalDifference', type: 'currency', hidden: true, viewNames: ['Activity Rec (Side by Side)'], positiveInGreen: true, negativeInRed: true},
		{
			header: 'Base Diff', width: 35, dataIndex: 'realizedGainLossBaseDifference', type: 'currency', summaryType: 'sum', hidden: true, viewNames: ['Mark to Market View'],
			renderer: function(v, metaData, r) {
				const note = r.data.realizedAndCommissionNote;
				if (note) {
					metaData.attr = TCG.renderQtip(note);
				}
				return TCG.renderAmount(v, true);
			}
		},


		{
			header: 'Note', width: 55, dataIndex: 'note', tooltip: 'If auto-reconciled, indicates the OTE threshold and Market Value threshold respectively', viewNames: ['Full Reconciliation', 'Position Reconciliation', 'Position Rec (Side by Side)'],
			renderer: function(v, metaData, r) {
				if (v) {
					metaData.attr = TCG.renderQtip(v);
				}
				return v;
			}
		},
		{
			header: 'Activity Note', width: 55, dataIndex: 'realizedAndCommissionNote', tooltip: 'If auto-reconciled, indicates the Realized Gain Loss threshold and Commission threshold respectively', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation', 'Activity Rec (Side by Side)'],
			renderer: function(v, metaData, r) {
				if (v) {
					metaData.attr = TCG.renderQtip(v);
				}
				return v;
			}
		},
		{header: 'Matched', width: 25, dataIndex: 'matched', type: 'boolean', viewNames: ['Activity Reconciliation', 'Position Reconciliation', 'Full Reconciliation']},
		{header: 'Qty Reconciled', width: 40, dataIndex: 'quantityReconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', viewNames: ['Full Reconciliation', 'Position Reconciliation']},
		{header: 'Reconciled', width: 35, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', viewNames: ['Full Reconciliation', 'Position Reconciliation', 'Position Rec (Side by Side)']},
		{header: 'Activity Reconciled', width: 35, dataIndex: 'realizedAndCommissionReconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center', hidden: true, viewNames: ['Full Reconciliation', 'Activity Reconciliation', 'Activity Rec (Side by Side)']}
	],
	plugins: {ptype: 'gridsummary'},
	editor: {
		detailPageClass: 'Clifton.reconcile.position.ReconcilePositionComparisonWindow',
		addEnabled: false,
		deleteEnabled: false,
		copyEnable: false,
		openDetailPage: function(className, gridPanel, id, row) {
			const clsName = gridPanel.isCurrentViewForActivityRec() ? 'Clifton.accounting.gl.TransactionListWindow' : 'Clifton.reconcile.position.ReconcilePositionComparisonWindow';
			TCG.grid.GridEditor.prototype.openDetailPage.call(this, clsName, gridPanel, id, row);
		},
		getDefaultData: function(gridPanel, row) {
			// use selected row parameters for drill down filtering
			if (gridPanel.isCurrentViewForActivityRec()) {
				const filters = [];
				const data = row.json;

				filters.push({
					field: 'holdingAccountNumber',
					comparison: 'EQUALS',
					value: data.positionAccount.holdingAccount.number
				});
				filters.push({
					field: 'investmentSecuritySymbol',
					comparison: 'EQUALS',
					value: data.investmentSecurity.symbol
				});
				const nextDay = Date.parseDate(data.positionAccount.positionDate, Ext.data.Field.prototype.dateFormat).format('m/d/Y');
				filters.push({
					field: 'transactionDate',
					comparison: 'EQUALS',
					value: nextDay
				});
				return filters;
			}
			return {
				holdingCompany: row.json.positionAccount.holdingAccount.issuingCompany,
				holdingAccountId: row.json.positionAccount.holdingAccount.id,
				investmentSecurity: row.json.investmentSecurity,
				label: row.json.label,
				baseCurrencyId: row.json.investmentSecurity.instrument.tradingCurrency.id,
				positionDate: row.json.positionAccount.positionDate,
				externalPrice: row.json.externalPrice
			};
		},
		addEditButtons: function(t, gridPanel) {
			const adjustmentHandler = function(gridPanel, clazz, adjustment) {
				const sm = gridPanel.grid.getSelectionModel();
				const row = sm.getSelections()[0];
				const cmpId = TCG.getComponentId(clazz, row.id);
				const defaultData = {
					reconcilePositionId: row.id,
					adjustmentAmount: adjustment,
					threshold: Math.abs(adjustment) + 1
				};
				TCG.createComponent(clazz, {
					id: cmpId,
					defaultData: defaultData,
					openerCt: gridPanel
				});
			};
			t.add({
				text: 'Actions',
				xtype: 'splitbutton',
				iconCls: 'book-red',
				scope: gridPanel,
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Match Broker Realized Gain/Loss',
							tooltip: '',
							iconCls: 'book-red',
							scope: gridPanel,
							handler: function() {
								const sm = this.grid.getSelectionModel();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single position to adjust.', 'Incorrect Selection');
								}
								else {
									const row = sm.getSelections()[0];
									const adjustment = row.data.externalTodayRealizedGainLossLocal - row.data.ourTodayRealizedGainLossLocal;
									adjustmentHandler(this, 'Clifton.reconcile.position.RealizedAdjustmentWindow', adjustment);
								}
							}
						}, {
							text: 'Match Broker Commission',
							tooltip: '',
							iconCls: 'book-red',
							scope: gridPanel,
							handler: function() {
								const sm = this.grid.getSelectionModel();
								if (sm.getCount() !== 1) {
									TCG.showError('Please select a single position to adjust.', 'Incorrect Selection');
								}
								else {
									const row = sm.getSelections()[0];
									const adjustment = row.data.externalTodayCommissionLocal - row.data.ourTodayCommissionLocal;
									adjustmentHandler(this, 'Clifton.reconcile.position.CommissionAdjustmentWindow', adjustment);
								}
							}
						}, {
							text: 'Reconcile Account',
							tooltip: 'Re-process reconciliations for this account.',
							iconCls: 'run',
							scope: gridPanel,
							handler: function() {
								const gridPanel = this;
								const win = gridPanel.getWindow();
								const params = {
									reconcilePositionDefinitionId: win.defaultData.reconcilePositionDefinitionId,
									positionDate: win.defaultData.positionDate,
									holdingAccountId: win.defaultData.holdingAccountId
								};
								const loader = new TCG.data.JsonLoader({
									waitMsg: 'Reconciling...',
									waitTarget: this,
									params: params,
									timeout: 120000,
									onLoad: function(record, conf) {
										gridPanel.reload();
									},
									onFailure: function() {
										// Even if the Processing Fails, still need to reload so that dates are properly updated, and warnings adjusted
										gridPanel.reload();
									}
								});
								loader.load('reconcilePositionRebuild.json');
							}
						}, {
							text: 'Unreconcile Selected',
							tooltip: 'Mark selected row as un-reconciled.',
							iconCls: 'cancel',
							scope: gridPanel,
							handler: function() {
								const gridPanel = this;
								const rows = gridPanel.getRowSelectionModel().getSelections();
								if (!rows) {
									TCG.showError('Please select one or more rows to unreconcile.', 'Unreconcile');
									return;
								}

								const ids = [];
								for (let i = 0; i < rows.length; i++) {
									if ((gridPanel.isCurrentViewForActivityRec() && rows[i].data.realizedAndCommissionReconciled) ||
										(!gridPanel.isCurrentViewForActivityRec() && rows[i].data.reconciled)
										|| (!gridPanel.isCurrentViewForActivityRec() && rows[i].data.quantityReconciled)) {
										ids.push(rows[i].data.id);
									}
								}

								if (ids.length > 0) {
									TCG.createComponent('Clifton.reconcile.ForceUnreconcileWindow', {
										defaultData: {
											ids: ids,
											unreconcilePosition: !gridPanel.isCurrentViewForActivityRec(),
											unreconcileActivity: gridPanel.isCurrentViewForActivityRec()
										},
										openerCt: gridPanel
									});
								}
								else {
									TCG.showError('Please choose items that are reconciled.', 'Unreconcile');
								}
							}
						}
					]
				})
			});
			t.add('-');

			t.add({
				text: 'Force Rec',
				tooltip: 'Mark selected row as reconciled: use our data and ignore external.',
				iconCls: 'row_reconciled',
				scope: this,
				handler: function() {
					const rows = gridPanel.getRowSelectionModel().getSelections();
					const viewName = gridPanel.currentViewName;

					if (!rows) {
						TCG.showError('Please select one or more rows to force reconcile.', 'Force Reconcile');
						return;
					}

					const ids = [];
					for (let i = 0; i < rows.length; i++) {
						if ((gridPanel.isCurrentViewForActivityRec() && rows[i].data.realizedAndCommissionReconciled) ||
							(!gridPanel.isCurrentViewForActivityRec() && rows[i].data.reconciled)) {
							continue;
						}
						ids.push(rows[i].data.id);
					}

					if (ids.length > 0) {
						TCG.createComponent('Clifton.reconcile.ForceReconcileWindow', {
							defaultData: {ids: ids, viewName: viewName},
							openerCt: gridPanel
						});
					}
					else {
						TCG.showError('Please choose items that are not reconciled.', 'Force Reconcile');
					}
				}
			});
			t.add('-');

			t.add({
				text: 'Force Rec w/ Existing Note',
				tooltip: 'Mark selected row as reconciled (pre-load existing note): use our data and ignore external.',
				iconCls: 'row_reconciled',
				scope: this,
				handler: function() {
					const rows = gridPanel.getRowSelectionModel().getSelections();
					const viewName = gridPanel.currentViewName;

					if (!rows) {
						TCG.showError('Please select one or more rows to force reconcile.', 'Force Reconcile');
						return;
					}

					const ids = [];
					let note = null;
					for (let i = 0; i < rows.length; i++) {
						if ((gridPanel.isCurrentViewForActivityRec() && rows[i].data.realizedAndCommissionReconciled) ||
							(!gridPanel.isCurrentViewForActivityRec() && rows[i].data.reconciled)) {
							continue;
						}

						const tempNote = rows[i].json.note ? rows[i].json.note : rows[i].json.realizedAndCommissionNote;
						if (tempNote) {
							note = (note ? note : '') + tempNote;
						}
						ids.push(rows[i].data.id);
					}

					if (ids.length > 0) {
						TCG.createComponent('Clifton.reconcile.ForceReconcileWindow', {
							defaultData: {ids: ids, viewName: viewName, note: note},
							openerCt: gridPanel
						});
					}
					else {
						TCG.showError('Please choose items that are not reconciled.', 'Force Reconcile');
					}
				}
			});
			t.add('-');

			t.add({
				text: 'Open M2M',
				iconCls: 'exchange',
				handler: function() {
					const sm = gridPanel.grid.getSelectionModel();
					let row = gridPanel.grid.getStore().getAt(0); // first row for cases when holding account is the same
					if (sm.getCount() === 1) {
						row = sm.getSelected();
					}

					const positionDate = Date.parseDate(row.json.positionAccount.positionDate, Ext.data.Field.prototype.dateFormat).format('m/d/Y');
					const m2mLoader = new TCG.data.JsonLoader({
						waitTarget: gridPanel,
						params: {holdingInvestmentAccountId: row.json.positionAccount.holdingAccount.id, markDate: positionDate},
						onLoad: function(record, conf) {
							if (record && record.length === 1) {
								const m2m = record[0];
								const config = {
									defaultDataIsReal: true,
									defaultData: m2m,
									params: {id: m2m.id}
								};
								TCG.createComponent('Clifton.accounting.m2m.M2MDailyWindow', config);
							}
							else {
								TCG.showError('Cannot find M2M daily associated with that record.');
							}
						}
					});
					m2mLoader.load('accountingM2MDailyListPopulatedFind.json?enableOpenSessionInView=true&requestedMaxDepth=4');
				}
			});
			t.add('-');
		}
	}
});
Ext.reg('reconcile-position-match-grid', Clifton.reconcile.position.PositionMatchListGrid);

Clifton.reconcile.position.RealizedAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Realized Gain / Loss Adjustment',
	iconCls: 'book-red',
	height: 260,
	width: 510,
	modal: true,
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				// TODO: move the code to win.loadJsonResult (need to save the form and the panel)
				panel.fireEvent('afterload', panel, win.closeOnSuccess);
				win.closeWindow();
				win.openerCt.reload();
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Create and post a journal that adjusts Realized Gain / Loss for selected closing position by the specified amount (local currency).',
		items: [
			{name: 'reconcilePositionId', xtype: 'hidden'},
			{name: 'threshold', xtype: 'hidden'},
			{fieldLabel: 'Adjustment Amount', name: 'adjustmentAmount', xtype: 'currencyfield', allowBlank: false, submitValue: false, readonly: true},
			{fieldLabel: 'Private Note', name: 'note', xtype: 'textarea', height: 70}
		],
		getSaveURL: function() {
			return 'reconcileRealizedGainLossAdjust.json';
		},
		listeners: {
			beforerender: function() {
				const f = this;
				const win = f.getWindow();

				const note = f.getForm().findField('note');
				if (Math.abs(win.defaultData.adjustmentAmount) > 5) {
					note.allowBlank = false;
				}
				else {
					note.allowBlank = true;
				}
			}
		}
	}]
});


Clifton.reconcile.position.CommissionAdjustmentWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Commission Adjustment',
	iconCls: 'book-red',
	height: 260,
	width: 510,
	modal: true,
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;
		// TODO: data binding for partial field updates
		// TODO: concurrent updates validation
		form.trackResetOnLoad = true;
		form.submit(Ext.applyIf({
			url: encodeURI(panel.getSaveURL()),
			params: params,
			waitMsg: 'Saving...',
			success: function(form, action) {
				// TODO: move the code to win.loadJsonResult (need to save the form and the panel)
				panel.fireEvent('afterload', panel, win.closeOnSuccess);
				win.closeWindow();
				win.openerCt.reload();
			}
		}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
	},

	items: [{
		xtype: 'formpanel',
		labelWidth: 120,
		loadValidation: false,
		instructions: 'Create and post a journal that adjusts Commission for selected closing position by the specified amount (local currency).',
		items: [
			{name: 'reconcilePositionId', xtype: 'hidden'},
			{name: 'threshold', xtype: 'hidden'},
			{fieldLabel: 'Adjustment Amount', name: 'adjustmentAmount', xtype: 'currencyfield', allowBlank: false, submitValue: false, readonly: true},
			{fieldLabel: 'Private Note', name: 'note', xtype: 'textarea', height: 70, allowBlank: false}
		],
		getSaveURL: function() {
			return 'reconcileCommissionAdjust.json';
		},
		listeners: {
			beforerender: function() {
				const f = this;
				const win = f.getWindow();

				const note = f.getForm().findField('note');
				if (Math.abs(win.defaultData.adjustmentAmount) > 20) {
					note.allowBlank = false;
				}
				else {
					note.allowBlank = true;
				}
			}
		}
	}]
});


Clifton.trade.DefaultTradeWindowAdditionalTabs[Clifton.trade.DefaultTradeWindowAdditionalTabs.length] = {
	title: 'Reconciliation',
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},

	items: [
		{
			title: 'Trade Fill Reconciliation Matches',
			name: 'reconcileTradeFillExtendedList',
			xtype: 'gridpanel',
			instructions: 'The following reconciliation matches have been identified for selected trade fills.',
			additionalPropertiesToRequest: 'match.id',
			border: 1,
			flex: 1,
			columns: [
				{header: 'Fill ID', width: 25, dataIndex: 'id', type: 'int', hidden: true},
				{header: 'Match ID', width: 25, dataIndex: 'match.id', type: 'int', hidden: true},
				{header: 'Match Size', width: 25, dataIndex: 'matchListSize', type: 'int', hidden: true},
				{header: 'Quantity', width: 30, dataIndex: 'quantity', type: 'float', useNull: true, defaultSortColumn: true},
				{header: 'Price', width: 30, dataIndex: 'notionalUnitPrice', type: 'float', useNull: true},
				{header: 'Match Type', width: 40, dataIndex: 'match.matchType.name'},
				{header: 'Contact', width: 40, dataIndex: 'match.contact.label'},
				{header: 'Note', width: 80, dataIndex: 'match.note'},
				{header: 'Workflow State', width: 45, dataIndex: 'match.workflowState.name'},
				{header: 'Workflow Status', width: 45, dataIndex: 'match.workflowStatus.name', hidden: true},
				{header: 'Reconciled', width: 24, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
				{header: 'Confirmed', width: 24, dataIndex: 'match.confirmed', type: 'boolean'}
			],
			getLoadParams: function(firstLoad) {
				return {
					readUncommittedRequested: true,
					tradeId: this.getWindow().getMainFormId()
				};
			},
			editor: {
				drillDownOnly: true,
				getDetailPageClass: function(grid, row) {
					if (row.json.match && row.json.match.id) {
						return 'Clifton.reconcile.trade.MatchWindow';
					}
					return 'Clifton.reconcile.trade.TradeFillMatchListWindow';
				},
				getDetailPageId: function(gridPanel, row) {
					if (row.json.match && row.json.match.id) {
						return row.json.match.id;
					}
					if (row.json.matchListSize > 1) {
						return row.json.id;
					}
					return undefined;
				}

			}
		},

		{
			title: 'Reconciliation Notes and Supporting Documents',
			xtype: 'document-system-note-grid',
			tableName: 'Trade',
			showInternalInfo: false,
			showPrivateInfo: false,
			defaultActiveFilter: false,
			border: 1,
			flex: 1.5,
			// Set to true to see notes for related entities - i.e. on Trades also see security notes
			includeNotesForLinkedEntity: true,

			addFirstToolbarButtons: function(t) {
				t.add({
					text: 'Trade Summary',
					iconCls: 'pdf',
					scope: this,
					handler: function() {
						const tradeId = this.getWindow().getMainFormId();
						Clifton.trade.downloadTradeReport(tradeId, this);
					}
				});
				t.add('-');
			}
		}
	]
};
// Added here so Instructions Show Up After Reconciliation Tab
Clifton.trade.DefaultTradeWindowAdditionalTabs[Clifton.trade.DefaultTradeWindowAdditionalTabs.length] = {
	title: 'Trade Instructions',
	items: [{
		xtype: 'investment-instruction-item-grid',
		tableName: 'Trade',
		defaultCategoryName: 'Trade Counterparty',
		getEntityId: function() {
			return TCG.getValue('id', this.getWindow().getMainForm().formValues);
		},
		getInstructionDate: function() {
			return TCG.getValue('tradeDate', this.getWindow().getMainForm().formValues);
		},
		configureToolsMenu: function(menu) {
			const owner = this.getWindow();
			menu.add({
				text: 'Update Settlement Company',
				tooltip: 'Update Trade Settlement Company used for the place of settlement BIC',
				iconCls: 'group',
				handler: function() {
					const formValues = owner.getMainForm().formValues;
					let settlementCompanyId = null;
					let settlementCompanyName = null;
					if (formValues.hasOwnProperty('settlementCompany')) {
						settlementCompanyId = formValues.settlementCompany.id;
						settlementCompanyName = formValues.settlementCompany.name;
					}
					TCG.createComponent('Clifton.trade.UpdateTradeSettlementCompanyWindow', {
						defaultData: {
							id: formValues.id,
							investmentSecurityId: formValues.investmentSecurity.id,
							settlementCompanyId: settlementCompanyId,
							settlementCompanyName: settlementCompanyName
						},
						openerCt: owner
					});
				}
			});
		}
	}]
};

Clifton.reconcile.trade.TradeFillMatchListWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Trade Fill Matches',
	iconCls: 'reconcile',
	height: 300,
	width: 750,

	items: [{
		xtype: 'gridpanel',
		name: 'reconcileTradeIntradayMatchByFill.json?enableOpenSessionInView=true',
		instructions: 'The following reconciliation matches have been identified for the selected trade fill.',
		columns: [
			{header: 'MatchID', width: 15, dataIndex: 'id', hidden: true},
			{header: 'Match Type', width: 20, dataIndex: 'matchType.name'},
			{header: 'Contact', width: 20, dataIndex: 'contact.label'},
			{header: 'Note', width: 40, dataIndex: 'note'},
			{header: 'Workflow State', width: 25, dataIndex: 'workflowState.name'},
			{header: 'Workflow Status', width: 25, dataIndex: 'workflowStatus.name', hidden: true},
			{header: 'Reconciled', width: 15, dataIndex: 'reconciled', type: 'boolean', renderer: TCG.renderCheck, align: 'center'},
			{header: 'Confirmed', width: 15, dataIndex: 'confirmed', type: 'boolean'}
		],
		getLoadParams: function(firstLoad) {
			return {
				readUncommittedRequested: true,
				tradeFillId: this.getWindow().params.id
			};
		},
		editor: {
			drillDownOnly: true,
			detailPageClass: 'Clifton.reconcile.trade.MatchWindow'
		}
	}]
});


Clifton.reconcile.ForceReconcileWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Force Reconcile',
	iconCls: 'reconcile',
	height: 250,
	modal: true,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		instructions: 'Enter a comment explaining why you chose to ignore external positions and use ours instead.',
		items: [
			{fieldLabel: 'Note', name: 'note', xtype: 'textarea', allowBlank: true}
		],
		getSaveURL: function() {
			return 'reconcilePositionForce.json';
		},
		getSubmitParams: function() {
			const data = this.getWindow().defaultData;
			return {ids: data.ids, viewName: data.viewName};
		}
	}]
});


Clifton.reconcile.ForceUnreconcileWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Un-Reconcile',
	iconCls: 'reconcile',
	height: 250,
	modal: true,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		labelWidth: 100,
		items: [
			{boxLabel: 'Clear existing note?', name: 'clearNote', xtype: 'checkbox', checked: true},
			{boxLabel: 'Unreconcile position?', name: 'unreconcilePosition', xtype: 'checkbox'},
			{boxLabel: 'Unreconcile activity?', name: 'unreconcileActivity', xtype: 'checkbox'}
		],
		getSaveURL: function() {
			return 'reconcilePositionUnreconcile.json';
		},
		getSubmitParams: function() {
			const data = this.getWindow().defaultData;
			return {ids: data.ids};
		}
	}]
});


Clifton.reconcile.trade.MatchListGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'reconcileTradeIntradayMatchListFind',
	xtype: 'gridpanel',
	wikiPage: 'IT/Trade Reconciliation',

	workflowStateName: undefined,
	workflowStateNames: undefined,
	excludeWorkflowStateName: undefined, // Allow to view by status, but exclude a specific state
	workflowStatusName: undefined,
	includeNoWorkflowUnconfirmed: false, // For Operations Pending Confirmation - also include those that don't use workflow but aren't confirmed so they can be confirmed by ops
	additionalPropertiesToRequest: 'id|workflowState.id',
	remoteSort: true,
	groupField: 'tradeFill.trade.tradeDate',
	groupTextTpl: '{values.group}: {[values.rs.length]} {[values.rs.length > 1 ? "Trades" : "Trade"]}',

	rowSelectionModel: 'multiple',
	viewNames: ['Default View', 'ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)'],

	getTopToolbarFilters: function(toolbar) {
		let filters = [
			{
				fieldLabel: 'Security', xtype: 'combo', name: 'investmentSecurityId', displayField: 'label', width: 120, url: 'investmentSecurityListFind.json',
				linkedFilter: 'tradeFill.trade.investmentSecurity.symbol',
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			},
			{
				fieldLabel: 'Trade Type', xtype: 'combo', name: 'tradeTypeId', displayField: 'name', width: 120, url: 'tradeTypeListFind.json',
				linkedFilter: 'tradeFill.trade.tradeType.name',
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			},
			{
				fieldLabel: 'Account Type', xtype: 'combo', name: 'holdingInvestmentAccountTypeId', displayField: 'label', width: 120, url: 'investmentAccountTypeListFind.json',
				linkedFilter: 'tradeFill.trade.holdingInvestmentAccount.type.name',
				listeners: {
					select: function(field) {
						TCG.getParentByClass(field, Ext.Panel).reload();
					}
				}
			}
		];
		if (this.appendAdditionalToolbarFilters) {
			filters = this.appendAdditionalToolbarFilters(toolbar, filters);
		}
		return filters;
	},
	switchToViewBeforeReload: function(viewName) {
		const tt = TCG.getChildByName(this.getTopToolbar(), 'tradeTypeId');

		if (viewName === 'ALERT - Equities (Funds)') {
			const type = TCG.data.getData('tradeTypeByName.json?name=Funds', this, 'trade.type.Funds');
			tt.setValue({text: type.name, value: type.id});
			tt.fireEvent('select', tt);
		}
		else if (viewName === 'ALERT - Fixed Income (Bonds)') {
			const type = TCG.data.getData('tradeTypeByName.json?name=Bonds', this, 'trade.type.Bonds');
			tt.setValue({text: type.name, value: type.id});
			tt.fireEvent('select', tt);
		}
		else {
			tt.reload(null, true);
			tt.fireEvent('blur', tt);
		}
		return true; // cancel reload: select even will reload
	},

	columns: [
		{header: 'Trade ID', width: 10, dataIndex: 'tradeFill.trade.id', filter: false, sortable: false, hidden: true},
		{header: 'Trade Type', width: 18, dataIndex: 'tradeFill.trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json', showNotEquals: true}, hidden: true, viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)']},
		{header: 'Trade Date', width: 22, dataIndex: 'tradeFill.trade.tradeDate', filter: {searchFieldName: 'tradeDate'}, defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{header: 'Settlement Date', width: 22, dataIndex: 'tradeFill.trade.settlementDate', filter: {searchFieldName: 'settlementDate'}, hidden: true, viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)']},
		{header: 'Client', width: 65, dataIndex: 'tradeFill.trade.clientInvestmentAccount.businessClient.label', hidden: true, filter: {type: 'combo', searchFieldName: 'clientId', displayField: 'label', url: 'businessClientListFind.json'}},
		{header: 'Client Account', width: 65, dataIndex: 'tradeFill.trade.clientInvestmentAccount.label', filter: {type: 'combo', searchFieldName: 'clientInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=true'}, viewNames: ['Default View']},
		{
			header: 'Holding Account ID', width: 17, dataIndex: 'tradeFill.trade.holdingInvestmentAccount.id', type: 'int', numberFormat: '000', hidden: true, filter: {searchFieldName: 'holdingInvestmentAccountId'}, tooltip: 'Used by ALERT platform to help locate account-specific Standard Settlement Instructions (SSIs)',
			viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)'],
			viewNameHeaders: [{name: 'ALERT - Equities (Funds)', label: 'Alert Code'}, {name: 'ALERT - Fixed Income (Bonds)', label: 'Alert Code'}]
		},
		{header: 'Holding Account', width: 24, dataIndex: 'tradeFill.trade.holdingInvestmentAccount.number', filter: {type: 'combo', searchFieldName: 'holdingInvestmentAccountId', displayField: 'label', url: 'investmentAccountListFind.json?ourAccount=false'}},
		{header: 'Holding Company', width: 35, dataIndex: 'tradeFill.trade.holdingInvestmentAccount.issuingCompany.name', filter: {type: 'combo', searchFieldName: 'holdingCompanyId', url: 'businessCompanyListFind.json?issuer=true'}},
		{header: 'Executing Broker', width: 35, dataIndex: 'tradeFill.trade.executingBrokerCompany.name', filter: {type: 'combo', searchFieldName: 'executingBrokerCompanyId', url: 'businessCompanyListFind.json?issuer=true'}, hidden: true, viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)']},
		{header: 'Account Type', width: 20, dataIndex: 'tradeFill.trade.holdingInvestmentAccount.type.name', filter: {searchFieldName: 'holdingInvestmentAccountTypeId', type: 'combo', url: 'investmentAccountTypeListFind.json'}, hidden: true},
		{header: 'Recon State', width: 30, dataIndex: 'workflowState.name', filter: {searchFieldName: 'workflowStateName'}, viewNames: ['Default View']},
		{header: 'Recon Status', width: 30, dataIndex: 'workflowStatus.name', filter: {searchFieldName: 'workflowStatusName'}},
		{
			header: 'Buy/Sell', width: 13, dataIndex: 'tradeFill.trade.buy', type: 'boolean', searchFieldName: 'buy',
			renderer: function(v, metaData) {
				metaData.css = v ? 'buy-light' : 'sell-light';
				return v ? 'BUY' : 'SELL';
			}
		},
		{header: 'Original Face', width: 22, dataIndex: 'tradeFill.trade.originalFace', type: 'float', hidden: true, useNull: true, viewNames: ['ALERT - Fixed Income (Bonds)']},
		{
			header: 'Quantity', width: 18, dataIndex: 'tradeFill.quantity', type: 'float', useNull: true, searchFieldName: 'quantity',
			viewNames: ['Default View', 'ALERT - Equities (Funds)'], viewNameHeaders: [{name: 'Default View', label: 'Quantity'}, {name: 'ALERT - Equities (Funds)', label: 'Shares'}, {name: 'ALERT - Fixed Income (Bonds)', label: 'Face'}]
		},
		{header: 'Trade Type', width: 18, dataIndex: 'tradeFill.trade.tradeType.name', filter: {type: 'combo', searchFieldName: 'tradeTypeId', url: 'tradeTypeListFind.json'}, hidden: true},
		{header: 'Security', width: 22, dataIndex: 'tradeFill.trade.investmentSecurity.symbol', filter: {type: 'combo', searchFieldName: 'investmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Security Name', width: 40, dataIndex: 'tradeFill.trade.investmentSecurity.name', filter: {type: 'combo', searchFieldName: 'securityId', displayField: 'label', url: 'investmentSecurityListFind.json'}, hidden: true},
		{header: 'Underlying Security', width: 22, dataIndex: 'tradeFill.trade.investmentSecurity.underlyingSecurity.symbol', hidden: true, filter: {type: 'combo', searchFieldName: 'underlyingInvestmentSecurityId', displayField: 'label', url: 'investmentSecurityListFind.json'}},
		{header: 'Maturity Date', width: 15, dataIndex: 'tradeFill.trade.investmentSecurity.endDate', hidden: true, filter: {searchFieldName: 'securityEndDate'}},
		{header: 'Price', width: 20, dataIndex: 'tradeFill.notionalUnitPrice', type: 'float', useNull: true, searchFieldName: 'notionalUnitPrice'},
		{header: 'Notional', width: 20, dataIndex: 'tradeFill.trade.accountingNotional', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Fixed Income (Bonds)'], viewNameHeaders: [{name: 'ALERT - Fixed Income (Bonds)', label: 'Principal'}]},
		{header: 'Accrual', width: 20, dataIndex: 'tradeFill.trade.accrualAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Fixed Income (Bonds)']},
		{header: 'Commission Per Unit', width: 20, dataIndex: 'tradeFill.trade.commissionPerUnit', type: 'float', hidden: true},
		{header: 'Commission', width: 18, dataIndex: 'tradeFill.trade.commissionAmount', type: 'currency', hidden: true, viewNames: ['ALERT - Equities (Funds)'], tooltip: 'Total Commission Amount for this trade (including adjusting journals). Negative amount means that the client pays commission.'},
		{header: 'Fee', width: 15, dataIndex: 'tradeFill.trade.feeAmount', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Equities (Funds)']},
		{header: 'Net Payment', width: 20, dataIndex: 'tradeFill.trade.netPayment', type: 'currency', hidden: true, useNull: true, viewNames: ['ALERT - Equities (Funds)', 'ALERT - Fixed Income (Bonds)'], negativeInRed: true, positiveInGreen: true, tooltip: 'Applies only to securities that have "Payment On Open". Uses local currency and includes accruals, commissions and fees. Positive amount indicates that the client receives money and negative amount that the client pays.'},
		{header: 'Net Payment (Base)', width: 20, dataIndex: 'tradeFill.trade.netPaymentBase', type: 'currency', hidden: true, useNull: true, tooltip: 'Net Payment converted to Client Account\'s base currency.'},
		{header: 'Match Type', width: 25, dataIndex: 'matchType.name', hidden: true},
		{header: 'Match Contact', width: 25, dataIndex: 'contact.label', hidden: true},
		{header: 'Note', width: 50, dataIndex: 'note', hidden: true, viewNames: ['Default View'], renderer: TCG.renderValueWithTooltip},
		{header: 'Internal Use Only Note', width: 50, dataIndex: 'internalTradeNote', hidden: true, renderer: TCG.renderValueWithTooltip, tooltip: 'Populated from the Trade Note type "Internal Use Only - Ops"'},
		{header: 'External Note', width: 50, dataIndex: 'externalTradeNote', hidden: true, renderer: TCG.renderValueWithTooltip, tooltip: 'Populated from the Trade Note type "External Note - Ops"'},
		{header: 'Confirmed', width: 15, dataIndex: 'confirmed', type: 'boolean', hidden: true, renderer: TCG.renderCheck, align: 'center', viewNames: ['Default View']}
	],
	getLoadParams: function(firstLoad) {
		// Ops/Documentation Confirmation list windows - can see each others (same status) but default to their own
		this.setDefaultFilters(firstLoad);

		let params = {};
		if (this.workflowStateName) {
			if (this.includeNoWorkflowUnconfirmed === true) {
				params = {workflowStateNameOrNull: this.workflowStateName, confirmed: false};
			}
			else {
				params = {workflowStateNameEquals: this.workflowStateName};
			}
		}
		else if (this.workflowStateNames) {
			if (this.includeNoWorkflowUnconfirmed === true) {
				params = {workflowStateNamesOrNull: this.workflowStateNames, confirmed: false};
			}
			else {
				params = {workflowStateNames: this.workflowStateNames};
			}
		}
		else {
			if (this.workflowStatusName) {
				if (this.includeNoWorkflowUnconfirmed === true) {
					params = {workflowStatusNameOrNull: this.workflowStatusName, confirmed: false};
				}
				else {
					params = {workflowStatusNameEquals: this.workflowStatusName};
				}
			}
			if (this.excludeWorkflowStateName) {
				params = Ext.apply(params, {excludeWorkflowStateName: this.excludeWorkflowStateName});
			}
		}
		params = Ext.apply(params, {readUncommittedRequested: true, requestedMaxDepth: 6});
		return params;
	},
	setDefaultFilters: function(firstLoad) {
		// override if necessary
	},
	editor: {
		ptype: 'workflow-transition-grid',
		detailPageClass: 'Clifton.reconcile.trade.MatchWindow',
		drillDownOnly: true,
		tableName: 'ReconcileTradeIntradayMatch',
		transitionWindowTitle: 'Transition Match Records',
		transitionWindowInstructions: 'Select Transition for selected trade match records',
		addBeforeTransitionButtons: function(toolBar, gridPanel) {
			const gridEditor = this;
			toolBar.add({
				text: 'Add Note',
				tooltip: 'Add a note linked to selected trades (NOTE: All trades must be for the same Trade Type)',
				iconCls: 'pencil',
				scope: this,
				handler: function() {
					const editor = gridEditor;
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select at least one trade to add note to.', 'No Row(s) Selected');
					}
					else {
						const ut = sm.getSelections();
						editor.addTradeNote(ut, editor, gridPanel);
					}
				}
			});
			toolBar.add('-');
		},
		addAfterTransitionButtons: function(toolBar, gridPanel) {
			const gridEditor = this;
			toolBar.add({
				text: 'Re-Assign',
				tooltip: 'Reassign workflow for the selected entities if workflow did not previously apply or assignment has changed.',
				iconCls: 'workflow',
				scope: this,
				handler: function() {
					const editor = gridEditor;
					const grid = gridPanel.grid;
					const sm = grid.getSelectionModel();
					if (sm.getCount() === 0) {
						TCG.showError('Please select a row to re-assign.', 'No Row(s) Selected');
					}
					else {
						const assignmentCount = 0;
						const ut = sm.getSelections();
						gridEditor.reassignRow(ut, assignmentCount, editor, gridPanel, 'ReconcileTradeIntradayMatch');
					}
				}
			});
			toolBar.add('-');
		},
		addTradeNote: function(rows, editor, gridPanel) {
			Clifton.trade.addTradeNote(rows, editor, gridPanel, 'tradeFill.trade.id', 'tradeFill.trade.tradeType.name', true);
		},
		reassignRow: function(rows, assignmentCount, gridPanel, tableName) {
			const gridEditor = this;
			const rowId = rows[assignmentCount].json.id;
			if (!rowId) {
				assignmentCount++;
				if (assignmentCount === rows.length) { // refresh after all rows were transitioned
					gridPanel.reload();
				}
				else {
					gridEditor.reassignRow(rows, assignmentCount, gridPanel, tableName);
				}
			}
			else {
				const loader = new TCG.data.JsonLoader({
					waitMsg: 'Executing Re-Assignment',
					waitTarget: gridPanel,
					params: {
						tableName: tableName,
						fkFieldId: rowId,

						enableOpenSessionInView: gridEditor.enableOpenSessionInView,
						requestedPropertiesToExclude: gridEditor.requestedPropertiesToExclude,
						requestedMaxDepth: gridEditor.requestedMaxDepth
					},
					onLoad: function(record, conf) {
						assignmentCount++;
						if (assignmentCount === rows.length) { // refresh after all rows were transitioned
							gridPanel.reload();
						}
						else {
							gridEditor.reassignRow(rows, assignmentCount, gridPanel, tableName);
						}
					}
				});
				loader.load('workflowReassign.json');
			}
		}

	}
});
Ext.reg('reconcile-trade-match-grid', Clifton.reconcile.trade.MatchListGrid);


Clifton.reconcile.trade.MatchWorkflowTransitionListWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Transition Match Records',
	iconCls: 'run',
	height: 200,
	width: 510,
	modal: true,

	tableName: undefined,
	matchIds: undefined,
	transitionOptions: undefined,

	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		instructions: 'Transition Match RecordsSelect Transition for selected trade match records',
		labelWidth: 130,
		items: [
			{
				fieldLabel: 'Transition', name: 'transitionName', hiddenName: 'transitionId', xtype: 'combo', mode: 'local',
				displayField: 'name',
				valueField: 'id',
				tooltipField: 'description',
				store: {
					xtype: 'arraystore',
					fields: ['name', 'id', 'description'],
					data: [] //will be overridden
				},
				listeners: {
					beforequery: function(queryEvent) {
						const combo = queryEvent.combo;
						const fp = combo.getParentForm();
						const window = fp.getWindow();

						const data = window.transitionOptions;
						combo.getStore().loadData(data, false);
					}
				}
			}
		]
	}],
	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		win.closeOnSuccess = closeOnSuccess;
		win.modifiedFormCount = forms.length;

		const transitionCount = 0;
		const transition = form.findField('transitionId').getValue();
		this.transitionRecord(win.matchIds, transitionCount, transition, form, win.tableName);
	},
	transitionRecord: function(matchIds, transitionCount, transitionId, form, tableName) {
		const win = this;
		const matchId = matchIds[transitionCount];

		const loader = new TCG.data.JsonLoader({
			waitMsg: 'Executing Transition for Match Id: ' + matchId,
			waitTarget: form,
			params: {
				tableName: tableName,
				id: matchId,
				workflowTransitionId: transitionId,
				enableOpenSessionInView: true
			},
			onLoad: function(record, conf) {
				transitionCount++;
				if (transitionCount === matchIds.length) { // set saved since open and close window to refresh grid
					win.savedSinceOpen = true;
					win.closeWindow();
				}
				else {
					win.transitionRecord(matchIds, transitionCount, transitionId, form, tableName);
				}
			}
		});
		loader.load('workflowTransitionExecuteByTransition.json');
	}
});
