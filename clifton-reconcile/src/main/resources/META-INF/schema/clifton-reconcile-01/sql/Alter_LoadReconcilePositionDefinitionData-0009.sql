-- exec reconcile.LoadReconcilePositionDefinitionData '6/10/11',1,NULL,0,1
ALTER PROC reconcile.LoadReconcilePositionDefinitionData
	@PositionDate DATETIME,
	@ReconcilePositionDefinitionID INT,
	@InvestmentSecurityID INT,
	@HoldingInvestmentAccountID INT,
	@UserID INT,
	@IsCleanRebuild BIT = 0
AS
BEGIN

	DECLARE @HoldingAccountID INT, @ClientAccountID INT
	
	IF @IsCleanRebuild = 1
	BEGIN
		DELETE FROM ReconcilePosition WHERE ReconcilePositionID IN
		(
			SELECT ReconcilePositionID
			FROM ReconcilePosition  rp
				INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
			WHERE rpa.PositionDate = @PositionDate AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
				AND (rp.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		)
		
		IF @InvestmentSecurityID IS NULL
		BEGIN
			DELETE FROM ReconcilePositionAccount
			WHERE PositionDate = @PositionDate AND ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
		END
	END
	
	
    DECLARE @ReconcilePosition TABLE(
		HoldingAccountID INT,
		PositionDate INT,
		ReconcilePositionID INT,
		ReconcilePositionAccountID INT,
		AccountingAccountID  INT,
		InvestmentSecurityID  INT,
		OurFxRate DECIMAL(19, 10),
		OurPrice DECIMAL(19, 10),
		OurQuantity	DECIMAL(28, 10),
		OurMarketValueLocal DECIMAL(19, 2),
		OurMarketValueBase DECIMAL(19, 2),
		OurOTEVariation DECIMAL(19, 2),
		ExternalFxRate DECIMAL(19, 10),
		ExternalPrice DECIMAL(19, 10),
		ExternalQuantity DECIMAL(28, 10),
		ExternalMarketValueLocal DECIMAL(19, 2),
		ExternalMarketValueBase DECIMAL(19, 2),
		ExternalOTEVariation DECIMAL(19, 2),
		OurOpenTradeEquityLocal DECIMAL(19, 2),
		OurOpenTradeEquityBase DECIMAL(19, 2),
		ExternalOpenTradeEquityLocal DECIMAL(19, 2),
		ExternalOpenTradeEquityBase DECIMAL(19, 2),
		IsMatched BIT)
		

	INSERT INTO @ReconcilePosition(HoldingAccountID,ReconcilePositionID,ReconcilePositionAccountID,AccountingAccountID,InvestmentSecurityID,OurFxRate,OurPrice,OurQuantity,OurMarketValueLocal,OurMarketValueBase,OurOTEVariation,ExternalFxRate,ExternalPrice,ExternalQuantity,ExternalMarketValueLocal,ExternalMarketValueBase,ExternalOTEVariation,OurOpenTradeEquityLocal,OurOpenTradeEquityBase,ExternalOpenTradeEquityLocal,ExternalOpenTradeEquityBase,IsMatched)
	SELECT * FROM [Reconcile].[ReconcilePositionAccountingPositionDailyData](@PositionDate, @ReconcilePositionDefinitionID,@InvestmentSecurityID)
	
	
	
	DECLARE account_cursor CURSOR local forward_only FOR 
	SELECT InvestmentAccountID, ClientAccountID = MAX(iar.MainAccountID)
	FROM InvestmentAccount ia
		INNER JOIN ReconcilePositionDefinition rpd ON 
			(rpd.HoldingCompanyID  IS NULL OR rpd.HoldingCompanyID = ia.IssuingCompanyID)
		INNER JOIN InvestmentAccountType iat ON iat.InvestmentAccountTypeID = ia.InvestmentAccountTypeID
		INNER JOIN InvestmentAccountRelationship iar ON (iar.RelatedAccountID IS NULL OR (iar.RelatedAccountID = ia.InvestmentAccountID AND rpd.AccountRelationshipPurposeID = iar.InvestmentAccountRelationshipPurposeID))
	WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID AND IsOurAccount = 0
		AND @PositionDate between iar.StartDate and ISNULL(iar.EndDate,GETDATE())
		AND (ia.InvestmentAccountID = @HoldingInvestmentAccountID OR @HoldingInvestmentAccountID IS NULL)
	GROUP BY InvestmentAccountID
	
	DECLARE @ReconcilePositionAccountID INT
	
    OPEN account_cursor;

	FETCH NEXT FROM account_cursor 
	INTO @HoldingAccountID, @ClientAccountID
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SELECT @ReconcilePositionAccountID = ReconcilePositionAccountID, @ClientAccountID = ClientAccountID
		FROM ReconcilePositionAccount WHERE 
			HoldingAccountID = @HoldingAccountID AND 
			PositionDate = @PositionDate AND 
			ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

		IF NOT EXISTS(SELECT ReconcilePositionAccountID FROM ReconcilePositionAccount WHERE 
			HoldingAccountID = @HoldingAccountID AND 
			PositionDate = @PositionDate AND 
			ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID)
		BEGIN
			INSERT INTO ReconcilePositionAccount
			(
				ReconcilePositionDefinitionID,
				PositionDate,
				HoldingAccountID,
				ClientAccountID,
				[CreateUserID],
				[CreateDate],
				[UpdateUserID],
				[UpdateDate]
			)
			SELECT
				@ReconcilePositionDefinitionID,
				@PositionDate,
				@HoldingAccountID,
				@ClientAccountID,
				@UserID,
				GETDATE(),
				@UserID,
				GETDATE();
				
			SET @ReconcilePositionAccountID = @@IDENTITY
		END
		
		DELETE FROM ReconcilePosition WHERE ReconcilePositionID NOT IN (SELECT ReconcilePositionID FROM @ReconcilePosition WHERE HoldingAccountID = @HoldingAccountID) AND ReconcilePositionAccountID = @ReconcilePositionAccountID

		UPDATE rp SET
			AccountingAccountID = rpTemp.AccountingAccountID,
			InvestmentSecurityID = rpTemp.InvestmentSecurityID,
			OurPrice = rpTemp.OurPrice,
			OurFxRate = rpTemp.OurFxRate,
			OurQuantity = rpTemp.OurQuantity,
			OurMarketValueLocal = rpTemp.OurMarketValueLocal,
			OurMarketValueBase = rpTemp.OurMarketValueBase,
			OurOpenTradeEquityLocal = rpTemp.OurOpenTradeEquityLocal,
			OurOpenTradeEquityBase = rpTemp.OurOpenTradeEquityBase,
			ExternalFxRate = rpTemp.ExternalFxRate,
			ExternalQuantity = rpTemp.ExternalQuantity,
			ExternalPrice = rpTemp.ExternalPrice,
			ExternalMarketValueLocal = rpTemp.ExternalMarketValueLocal,
			ExternalMarketValueBase = rpTemp.ExternalMarketValueBase,
			ExternalOpenTradeEquityLocal = rpTemp.ExternalOpenTradeEquityLocal,
			ExternalOpenTradeEquityBase = rpTemp.ExternalOpenTradeEquityBase,
			IsMatched = rpTemp.IsMatched
		FROM ReconcilePosition rp
			INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
			INNER JOIN @ReconcilePosition rpTemp ON rpTemp.ReconcilePositionID = rp.ReconcilePositionID  
		WHERE rpTemp.ReconcilePositionAccountID = @ReconcilePositionAccountID AND 
			rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

		INSERT INTO ReconcilePosition 
		(
			ReconcilePositionAccountID,
			AccountingAccountID,
			InvestmentSecurityID,
			OurPrice,
			OurFxRate,
			OurQuantity,
			OurMarketValueLocal,
			OurMarketValueBase,
			OurOpenTradeEquityLocal,
			OurOpenTradeEquityBase,
			ExternalFxRate,
			ExternalQuantity,
			ExternalPrice,
			ExternalMarketValueLocal,
			ExternalMarketValueBase,
			ExternalOpenTradeEquityLocal,
			ExternalOpenTradeEquityBase,
			IsMatched,
			[CreateUserID],
			[CreateDate],
			[UpdateUserID],
			[UpdateDate]
		)
		SELECT
			@ReconcilePositionAccountID,
			AccountingAccountID,
			InvestmentSecurityID,
			OurPrice,
			OurFxRate,
			OurQuantity,
			OurMarketValueLocal,
			OurMarketValueBase,
			OurOpenTradeEquityLocal,
			OurOpenTradeEquityBase,
			ExternalFxRate,
			ExternalQuantity,
			ExternalPrice,
			ExternalMarketValueLocal,
			ExternalMarketValueBase,
			ExternalOpenTradeEquityLocal,
			ExternalOpenTradeEquityBase,
			IsMatched,
			@UserID,
			GETDATE(),
			@UserID,
			GETDATE()
		FROM @ReconcilePosition
		WHERE ReconcilePositionID IS NULL
			AND (ReconcilePositionAccountID = @ReconcilePositionAccountID OR ReconcilePositionAccountID IS NULL)
			AND HoldingAccountID = @HoldingAccountID
	
		--IF EXISTS (SELECT AccountingPositionDailyID FROM AccountingPositionDaily apd
		--		INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = apd.AccountingTransactionID
		--		WHERE t.HoldingInvestmentAccountID = @HoldingAccountID)
		--	OR EXISTS (SELECT ReconcilePositionExternalDailyID FROM ReconcilePositionExternalDaily WHERE HoldingAccountID = @HoldingAccountID)
		--BEGIN
		--	EXEC reconcile.LoadReconcilePositionAccountingPositionDailyData @PositionDate, @HoldingAccountID, @ClientAccountID, @ReconcilePositionDefinitionID, @UserID, @InvestmentSecurityID
		--END
	
		FETCH NEXT FROM account_cursor 
		INTO @HoldingAccountID, @ClientAccountID
	END
	CLOSE account_cursor;
    DEALLOCATE account_cursor;
    
END
