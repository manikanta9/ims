CREATE FUNCTION [Reconcile].[ReconcilePositionAccountingPositionDailyData]
(
	--@ReconcilePositionAccountID INT,
	@PositionDate DATETIME,
	@ReconcilePositionDefinitionID INT,
	@InvestmentSecurityID INT = NULL
)
RETURNS @ReconcilePosition TABLE
(
	HoldingAccountID INT,
	ReconcilePositionID INT,
	ReconcilePositionAccountID INT,
	AccountingAccountID  INT,
	InvestmentSecurityID  INT,
	OurFxRate DECIMAL(19, 10),
	OurPrice DECIMAL(19, 10),
	OurQuantity	DECIMAL(28, 10),
	OurMarketValueLocal DECIMAL(19, 2),
	OurMarketValueBase DECIMAL(19, 2),
	OurOTEVariation DECIMAL(19, 2),
	ExternalFxRate DECIMAL(19, 10),
	ExternalPrice DECIMAL(19, 10),
	ExternalQuantity DECIMAL(28, 10),
	ExternalMarketValueLocal DECIMAL(19, 2),
	ExternalMarketValueBase DECIMAL(19, 2),
	ExternalOTEVariation DECIMAL(19, 2),
	OurOpenTradeEquityLocal DECIMAL(19, 2),
	OurOpenTradeEquityBase DECIMAL(19, 2),
	ExternalOpenTradeEquityLocal DECIMAL(19, 2),
	ExternalOpenTradeEquityBase DECIMAL(19, 2),
	IsMatched BIT
)
AS
BEGIN
	--DECLARE @ReconcilePositionDefinitionID INT, @InvestmentGroupID INT, @HoldingAccountID INT, @PositionDate DATETIME
	DECLARE @InvestmentGroupID INT
	
	SELECT 
		@InvestmentGroupID = InvestmentGroupID
	FROM ReconcilePositionDefinition  rpd
	WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

	INSERT INTO @ReconcilePosition 
	(
		HoldingAccountID,
		InvestmentSecurityID,
		ReconcilePositionAccountID,
		ReconcilePositionID,
		AccountingAccountID,
		OurPrice,
		OurFxRate,
		OurQuantity,
		OurMarketValueLocal,
		OurMarketValueBase,
		OurOpenTradeEquityLocal,
		OurOpenTradeEquityBase,
		IsMatched
	)
	SELECT
		HoldingInvestmentAccountID,
		s.InvestmentSecurityID,
		MAX(rp.ReconcilePositionAccountID),
		rp.ReconcilePositionID,
		MAX(t.AccountingAccountID),
		AVG(MarketPrice),
		AVG(MarketFxRate),
		SUM(RemainingQuantity),
		SUM(MarketValueLocal),
		SUM(MarketValueBase),
		SUM(OpenTradeEquityLocal),
		SUM(OpenTradeEquityBase),
		0
	FROM AccountingPositionDaily apd
		INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = apd.AccountingTransactionID
		INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = t.InvestmentSecurityID
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
		LEFT JOIN ReconcilePositionAccount rpa ON rpa.HoldingAccountID = t.HoldingInvestmentAccountID 
				AND rpa.PositionDate = apd.PositionDate 
				AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
		LEFT JOIN ReconcilePosition rp ON rp.InvestmentSecurityID = s.InvestmentSecurityID AND
			 rp.ReconcilePositionAccountID = rpa.ReconcilePositionAccountID
	WHERE apd.PositionDate = @PositionDate
		--AND (rpa.ReconcilePositionAccountID IS NOT NULL OR (rp.ReconcilePositionID IS NULL AND rpa.ReconcilePositionAccountID IS NULL))
		AND (s.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		AND (s.InvestmentSecurityID IN (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s 
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
				INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
			WHERE ig.InvestmentGroupID = 12) )
	GROUP BY HoldingInvestmentAccountID, s.InvestmentSecurityID, rp.ReconcilePositionID
	 
	
	UPDATE r SET
		ExternalFxRate = ext.ExternalFxRate,
		ExternalPrice = ext.ExternalPrice,
		ExternalQuantity = ext.ExternalQuantity,
		ExternalMarketValueLocal = ext.ExternalMarketValueLocal,
		ExternalMarketValueBase = ext.ExternalMarketValueBase,
		ExternalOpenTradeEquityLocal = ext.ExternalOpenTradeEquityLocal,
		ExternalOpenTradeEquityBase = ext.ExternalOpenTradeEquityBase,
		IsMatched = 1
	FROM @ReconcilePosition r
		INNER JOIN
		(
			SELECT
				--rp.ReconcilePositionID, --MAX(rpCurrent.ReconcilePositionID) as ReconcilePositionID,
				apd.HoldingAccountID,
				apd.InvestmentSecurityID,
				ExternalFxRate = AVG(FxRate),
				ExternalQuantity = SUM(Quantity),
				ExternalPrice = AVG(MarketPrice),
				ExternalMarketValueLocal = SUM(MarketValueLocal),
				ExternalMarketValueBase = SUM(MarketValueBase),
				ExternalOpenTradeEquityLocal = SUM(OpenTradeEquityLocal),
				ExternalOpenTradeEquityBase = SUM(OpenTradeEquityBase)
			FROM ReconcilePositionExternalDaily apd
				INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = apd.InvestmentSecurityID
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN @ReconcilePosition rp ON rp.InvestmentSecurityID = s.InvestmentSecurityID AND apd.HoldingAccountID = rp.HoldingAccountID			
			WHERE apd.PositionDate = @PositionDate
				--AND rp.ReconcilePositionID IS NOT NULL
				AND (apd.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
				AND (s.InvestmentSecurityID IN (
					SELECT InvestmentSecurityID
					FROM InvestmentSecurity s 
						INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
						INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
						INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
						INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
					WHERE ig.InvestmentGroupID = @InvestmentGroupID) OR @InvestmentGroupID IS NULL)
			GROUP BY apd.HoldingAccountID, apd.InvestmentSecurityID--, rp.ReconcilePositionID
		) ext ON r.HoldingAccountID = ext.HoldingAccountID AND r.InvestmentSecurityID = ext.InvestmentSecurityID
    
   
   
    INSERT INTO @ReconcilePosition 
	(
		HoldingAccountID,
		ReconcilePositionAccountID,
		ReconcilePositionID,
		InvestmentSecurityID,
		ExternalFxRate,
		ExternalQuantity,
		ExternalPrice,
		ExternalMarketValueLocal,
		ExternalMarketValueBase,
		ExternalOpenTradeEquityLocal,
		ExternalOpenTradeEquityBase,
		IsMatched
	)
	SELECT
		apd.HoldingAccountID,
		MAX(rpa.ReconcilePositionAccountID),
		MAX(rpCurrent.ReconcilePositionID),
		apd.InvestmentSecurityID,
		ExternalFxRate = AVG(FxRate),
		ExternalQuantity = SUM(Quantity),
		ExternalPrice = AVG(MarketPrice),
		ExternalMarketValueLocal = SUM(MarketValueLocal),
		ExternalMarketValueBase = SUM(MarketValueBase),
		ExternalOpenTradeEquityLocal = SUM(OpenTradeEquityLocal),
		ExternalOpenTradeEquityBase = SUM(OpenTradeEquityBase),
		0
	FROM ReconcilePositionExternalDaily apd
		INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = apd.InvestmentSecurityID
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
		LEFT JOIN @ReconcilePosition rp ON rp.InvestmentSecurityID = s.InvestmentSecurityID AND apd.HoldingAccountID = rp.HoldingAccountID
		LEFT JOIN ReconcilePositionAccount rpa ON rpa.HoldingAccountID = apd.HoldingAccountID 
					AND rpa.PositionDate = apd.PositionDate 
					AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID 
		LEFT JOIN ReconcilePosition rpCurrent ON rpCurrent.InvestmentSecurityID = s.InvestmentSecurityID 
					AND rpCurrent.ReconcilePositionAccountID = rpa.ReconcilePositionAccountID
	WHERE apd.PositionDate = @PositionDate
		AND rp.InvestmentSecurityID IS NULL
		AND ((rpa.ReconcilePositionAccountID IS NULL OR rp.ReconcilePositionID IS NULL) OR
			(rpa.ReconcilePositionAccountID IS NOT NULL OR rp.ReconcilePositionID IS NOT NULL))
		AND (apd.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		AND (s.InvestmentSecurityID IN (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s 
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
				INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
			WHERE ig.InvestmentGroupID = @InvestmentGroupID) OR @InvestmentGroupID IS NULL)
	GROUP BY apd.HoldingAccountID, apd.InvestmentSecurityID--, rpCurrent.ReconcilePositionID
	
	RETURN
END
