-- exec reconcile.LoadReconcilePositionDefinitionData '5/9/11',1,0
ALTER PROC reconcile.LoadReconcilePositionDefinitionData
	@PositionDate DATETIME,
	@ReconcilePositionDefinitionID INT,
	@UserID INT
AS
BEGIN

	DECLARE @HoldingAccountID INT, @ClientAccountID INT
	
	DELETE FROM ReconcilePosition WHERE ReconcilePositionID IN
    (
		SELECT ReconcilePositionID
		FROM ReconcilePosition  rp
			INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
		WHERE rpa.PositionDate = @PositionDate AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
	)
	
	DELETE FROM ReconcilePositionAccount
	WHERE PositionDate = @PositionDate AND ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

	
	DECLARE account_cursor CURSOR local forward_only FOR 
	SELECT InvestmentAccountID, ClientAccountID = iar.MainAccountID
	FROM InvestmentAccount ia
		INNER JOIN ReconcilePositionDefinition rpd ON 
			(rpd.HoldingCompanyID  IS NULL OR rpd.HoldingCompanyID = ia.IssuingCompanyID)
		INNER JOIN InvestmentAccountType iat ON iat.InvestmentAccountTypeID = ia.InvestmentAccountTypeID
		INNER JOIN InvestmentAccountRelationship iar ON (iar.RelatedAccountID IS NULL OR (iar.RelatedAccountID = ia.InvestmentAccountID AND rpd.AccountRelationshipPurposeID = iar.InvestmentAccountRelationshipPurposeID))
	WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID AND IsOurAccount = 0
  
    
    OPEN account_cursor;

	FETCH NEXT FROM account_cursor 
	INTO @HoldingAccountID, @ClientAccountID
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		--IF EXISTS (SELECT AccountingPositionDailyID FROM AccountingPositionDaily apd
		--		INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = apd.AccountingTransactionID
		--		WHERE t.HoldingInvestmentAccountID = @HoldingAccountID)
		--	OR EXISTS (SELECT ReconcilePositionExternalDailyID FROM ReconcilePositionExternalDaily WHERE HoldingAccountID = @HoldingAccountID)
		--BEGIN
			EXEC reconcile.LoadReconcilePositionAccountingPositionDailyData @PositionDate, @HoldingAccountID, @ClientAccountID, @ReconcilePositionDefinitionID, @UserID
		--END
	
		FETCH NEXT FROM account_cursor 
		INTO @HoldingAccountID, @ClientAccountID
	END
	CLOSE account_cursor;
    DEALLOCATE account_cursor;
    
END
