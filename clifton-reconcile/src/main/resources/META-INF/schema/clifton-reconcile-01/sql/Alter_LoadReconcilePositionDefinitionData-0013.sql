-- exec reconcile.LoadReconcilePositionDefinitionData '7/1/11',2,NULL,NULL,1,1
ALTER PROC reconcile.LoadReconcilePositionDefinitionData
	@PositionDate DATETIME,
	@ReconcilePositionDefinitionID INT,
	@InvestmentSecurityID INT,
	@HoldingInvestmentAccountID INT,
	@UserID INT,
	@IsCleanRebuild BIT = 0
AS
BEGIN

	DECLARE @HoldingAccountID INT, @ClientAccountID INT
	
	IF @IsCleanRebuild = 1
	BEGIN
		DELETE FROM ReconcilePosition WHERE ReconcilePositionID IN
		(
			SELECT ReconcilePositionID
			FROM ReconcilePosition  rp
				INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
			WHERE rpa.PositionDate = @PositionDate AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
				AND (rp.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		)
		
		IF @InvestmentSecurityID IS NULL
		BEGIN
			DELETE FROM ReconcilePositionAccount
			WHERE PositionDate = @PositionDate AND ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
		END
	END
	
	
    DECLARE @ReconcilePosition TABLE(
		HoldingAccountID INT,
		ClientAccountID INT,
		ReconcilePositionID INT,
		ReconcilePositionAccountID INT,
		AccountingAccountID  INT,
		InvestmentSecurityID  INT,
		OurFxRate DECIMAL(19, 10),
		OurPrice DECIMAL(19, 10),
		OurQuantity	DECIMAL(28, 10),
		OurPriorQuantity DECIMAL(28, 10),
		OurTodayClosedQuantity	DECIMAL(28, 10),
		OurMarketValueLocal DECIMAL(19, 2),
		OurMarketValueBase DECIMAL(19, 2),
		OurOpenTradeEquityLocal DECIMAL(19, 2),
		OurOpenTradeEquityBase DECIMAL(19, 2),
		OurPriorOpenTradeEquityLocal DECIMAL(19, 2),
		OurPriorOpenTradeEquityBase DECIMAL(19, 2),
		OurTodayCommissionLocal DECIMAL(19, 2),
		OurTodayRealizedGainLossLocal DECIMAL(19, 2),
		
		ExternalFxRate DECIMAL(19, 10),
		ExternalPrice DECIMAL(19, 10),
		ExternalQuantity DECIMAL(28, 10),
		ExternalPriorQuantity DECIMAL(28, 10),
		ExternalTodayClosedQuantity	DECIMAL(28, 10),
		ExternalMarketValueLocal DECIMAL(19, 2),
		ExternalMarketValueBase DECIMAL(19, 2),
		ExternalOpenTradeEquityLocal DECIMAL(19, 2),
		ExternalOpenTradeEquityBase DECIMAL(19, 2),
		ExternalPriorOpenTradeEquityLocal DECIMAL(19, 2),
		ExternalPriorOpenTradeEquityBase DECIMAL(19, 2),
		ExternalTodayCommissionLocal DECIMAL(19, 2),
		ExternalTodayRealizedGainLossLocal DECIMAL(19, 2),
		IsMatched BIT)
		

	INSERT INTO @ReconcilePosition
	SELECT * FROM [Reconcile].[ReconcilePositionAccountingPositionDailyData](@PositionDate, @ReconcilePositionDefinitionID, @InvestmentSecurityID, @HoldingInvestmentAccountID)
	
	
	
	DECLARE account_cursor CURSOR local forward_only FOR 
	SELECT InvestmentAccountID, ClientAccountID = MAX(iar.MainAccountID)
	FROM InvestmentAccount ia
		INNER JOIN ReconcilePositionDefinition rpd ON 
			(rpd.HoldingCompanyID  IS NULL OR rpd.HoldingCompanyID = ia.IssuingCompanyID)
		INNER JOIN InvestmentAccountType iat ON iat.InvestmentAccountTypeID = ia.InvestmentAccountTypeID
		INNER JOIN InvestmentAccountRelationship iar ON (iar.RelatedAccountID IS NULL OR (iar.RelatedAccountID = ia.InvestmentAccountID AND rpd.AccountRelationshipPurposeID = iar.InvestmentAccountRelationshipPurposeID))
	WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID AND IsOurAccount = 0
		AND @PositionDate between iar.StartDate and ISNULL(iar.EndDate,GETDATE())
		AND (ia.InvestmentAccountID = @HoldingInvestmentAccountID OR @HoldingInvestmentAccountID IS NULL)
	GROUP BY InvestmentAccountID
	
	DECLARE @ReconcilePositionAccountID INT
	
    OPEN account_cursor;

	FETCH NEXT FROM account_cursor 
	INTO @HoldingAccountID, @ClientAccountID
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		SELECT @ReconcilePositionAccountID = ReconcilePositionAccountID
		FROM ReconcilePositionAccount WHERE 
			HoldingAccountID = @HoldingAccountID AND 
			PositionDate = @PositionDate AND 
			ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
		
		IF NOT EXISTS(SELECT ReconcilePositionAccountID FROM ReconcilePositionAccount WHERE 
			HoldingAccountID = @HoldingAccountID AND 
			PositionDate = @PositionDate AND 
			ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID)
		BEGIN
			SELECT @ClientAccountID = coalesce(MAX(ClientAccountID),@ClientAccountID)
			FROM @ReconcilePosition 
			WHERE HoldingAccountID = @HoldingAccountID
			
			INSERT INTO ReconcilePositionAccount
			(
				ReconcilePositionDefinitionID,
				PositionDate,
				HoldingAccountID,
				ClientAccountID,
				[CreateUserID],
				[CreateDate],
				[UpdateUserID],
				[UpdateDate]
			)
			SELECT
				@ReconcilePositionDefinitionID,
				@PositionDate,
				@HoldingAccountID,
				@ClientAccountID,
				@UserID,
				GETDATE(),
				@UserID,
				GETDATE();
				
			SET @ReconcilePositionAccountID = @@IDENTITY
		END
		
		
		DELETE FROM ReconcilePosition 
		WHERE ReconcilePositionID NOT IN (SELECT ReconcilePositionID FROM @ReconcilePosition WHERE HoldingAccountID = @HoldingAccountID)
			AND ReconcilePositionAccountID = @ReconcilePositionAccountID
			AND (InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
			
		UPDATE rp SET
			AccountingAccountID = rpTemp.AccountingAccountID,
			InvestmentSecurityID = rpTemp.InvestmentSecurityID,
			OurPrice = rpTemp.OurPrice,
			OurFxRate = rpTemp.OurFxRate,
			OurQuantity = ISNULL(rpTemp.OurQuantity, 0),
			OurTodayClosedQuantity = ISNULL(rpTemp.OurTodayClosedQuantity, 0),
			OurMarketValueLocal = ISNULL(rpTemp.OurMarketValueLocal, 0),
			OurMarketValueBase = ISNULL(rpTemp.OurMarketValueBase, 0),
			OurOpenTradeEquityLocal = ISNULL(rpTemp.OurOpenTradeEquityLocal, 0),
			OurOpenTradeEquityBase = ISNULL(rpTemp.OurOpenTradeEquityBase, 0),
			OurTodayCommissionLocal = ISNULL(rpTemp.OurTodayCommissionLocal, 0),
			OurTodayRealizedGainLossLocal = ISNULL(rpTemp.OurTodayRealizedGainLossLocal, 0),
			
			ExternalFxRate = rpTemp.ExternalFxRate,
			ExternalQuantity = ISNULL(rpTemp.ExternalQuantity, 0),
			ExternalTodayClosedQuantity = ISNULL(rpTemp.ExternalTodayClosedQuantity, 0),
			ExternalPrice = rpTemp.ExternalPrice,
			ExternalMarketValueLocal = ISNULL(rpTemp.ExternalMarketValueLocal, 0),
			ExternalMarketValueBase = ISNULL(rpTemp.ExternalMarketValueBase, 0),
			ExternalOpenTradeEquityLocal = ISNULL(rpTemp.ExternalOpenTradeEquityLocal, 0),
			ExternalOpenTradeEquityBase = ISNULL(rpTemp.ExternalOpenTradeEquityBase, 0),
			ExternalTodayCommissionLocal = ISNULL(rpTemp.ExternalTodayCommissionLocal, 0),
			ExternalTodayRealizedGainLossLocal = ISNULL(rpTemp.ExternalTodayRealizedGainLossLocal, 0),
			IsMatched = rpTemp.IsMatched
		FROM ReconcilePosition rp
			INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
			INNER JOIN @ReconcilePosition rpTemp ON rpTemp.ReconcilePositionID = rp.ReconcilePositionID  
		WHERE rpTemp.ReconcilePositionAccountID = @ReconcilePositionAccountID AND 
			rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

		INSERT INTO ReconcilePosition 
		(
			ReconcilePositionAccountID,
			AccountingAccountID,
			InvestmentSecurityID,
			OurPrice,
			OurFxRate,
			OurQuantity,
			OurTodayClosedQuantity,
			OurMarketValueLocal,
			OurMarketValueBase,
			OurOpenTradeEquityLocal,
			OurOpenTradeEquityBase,
			OurTodayCommissionLocal,
			OurTodayRealizedGainLossLocal,
			
			ExternalFxRate,
			ExternalQuantity,
			ExternalTodayClosedQuantity,
			ExternalPrice,
			ExternalMarketValueLocal,
			ExternalMarketValueBase,
			ExternalOpenTradeEquityLocal,
			ExternalOpenTradeEquityBase,
			ExternalTodayCommissionLocal,
			ExternalTodayRealizedGainLossLocal,
			IsMatched,
			[CreateUserID],
			[CreateDate],
			[UpdateUserID],
			[UpdateDate]
		)
		SELECT
			@ReconcilePositionAccountID,
			AccountingAccountID,
			InvestmentSecurityID,
			OurPrice,
			OurFxRate,
			ISNULL(OurQuantity, 0),
			ISNULL(OurTodayClosedQuantity, 0),
			ISNULL(OurMarketValueLocal, 0),
			ISNULL(OurMarketValueBase, 0),
			ISNULL(OurOpenTradeEquityLocal, 0),
			ISNULL(OurOpenTradeEquityBase, 0),
			ISNULL(OurTodayCommissionLocal, 0),
			ISNULL(OurTodayRealizedGainLossLocal, 0),
			
			ExternalFxRate,
			ISNULL(ExternalQuantity, 0),
			ISNULL(ExternalTodayClosedQuantity, 0),
			ExternalPrice,
			ISNULL(ExternalMarketValueLocal, 0),
			ISNULL(ExternalMarketValueBase, 0),
			ISNULL(ExternalOpenTradeEquityLocal, 0),
			ISNULL(ExternalOpenTradeEquityBase, 0),
			ISNULL(ExternalTodayCommissionLocal, 0),
			ISNULL(ExternalTodayRealizedGainLossLocal, 0),
			IsMatched,
			@UserID,
			GETDATE(),
			@UserID,
			GETDATE()
		FROM @ReconcilePosition
		WHERE ReconcilePositionID IS NULL
			AND (ReconcilePositionAccountID = @ReconcilePositionAccountID OR ReconcilePositionAccountID IS NULL)
			AND HoldingAccountID = @HoldingAccountID
	
		--IF EXISTS (SELECT AccountingPositionDailyID FROM AccountingPositionDaily apd
		--		INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = apd.AccountingTransactionID
		--		WHERE t.HoldingInvestmentAccountID = @HoldingAccountID)
		--	OR EXISTS (SELECT ReconcilePositionExternalDailyID FROM ReconcilePositionExternalDaily WHERE HoldingAccountID = @HoldingAccountID)
		--BEGIN
		--	EXEC reconcile.LoadReconcilePositionAccountingPositionDailyData @PositionDate, @HoldingAccountID, @ClientAccountID, @ReconcilePositionDefinitionID, @UserID, @InvestmentSecurityID
		--END
	
		FETCH NEXT FROM account_cursor 
		INTO @HoldingAccountID, @ClientAccountID
	END
	CLOSE account_cursor;
    DEALLOCATE account_cursor;
    
END
