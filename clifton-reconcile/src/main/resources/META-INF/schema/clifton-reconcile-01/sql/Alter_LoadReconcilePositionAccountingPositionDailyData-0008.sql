-- exec dbo.LoadReconcilePositionAccountingPositionDailyData '1/3/11',1
ALTER PROC reconcile.LoadReconcilePositionAccountingPositionDailyData
	@PositionDate DATETIME,
	@HoldingAccountID INT,
	@ClientAccountID INT,
	@ReconcilePositionDefinitionID INT,
	@UserID INT,
	@InvestmentSecurityID INT
AS
BEGIN
	DECLARE @ReconcilePositionAccountID INT, @InvestmentGroupID INT

	SELECT @ReconcilePositionAccountID = ReconcilePositionAccountID 
	FROM ReconcilePositionAccount WHERE 
		HoldingAccountID = @HoldingAccountID AND 
		PositionDate = @PositionDate AND 
		ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

	SELECT @InvestmentGroupID=InvestmentGroupID FROM ReconcilePositionDefinition WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

	IF @ReconcilePositionAccountID IS NULL AND @InvestmentSecurityID IS NOT NULL
	BEGIN
		RETURN;
	END

	IF @ReconcilePositionAccountID IS NOT NULL AND @InvestmentSecurityID IS NULL
	BEGIN
	
		DELETE FROM ReconcilePosition WHERE ReconcilePositionAccountID = @ReconcilePositionAccountID
		DELETE FROM ReconcilePositionAccount WHERE ReconcilePositionAccountID = @ReconcilePositionAccountID
	END
	ELSE
	BEGIN
		DELETE FROM rp
		FROM ReconcilePosition rp
			INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
		WHERE InvestmentSecurityID = @InvestmentSecurityID AND HoldingAccountID = @HoldingAccountID AND PositionDate = @PositionDate
	END
	
	
	IF @InvestmentSecurityID IS NULL
	BEGIN
		INSERT INTO ReconcilePositionAccount
		(
			ReconcilePositionDefinitionID,
			PositionDate,
			HoldingAccountID,
			ClientAccountID,
			[CreateUserID],
			[CreateDate],
			[UpdateUserID],
			[UpdateDate]
		)
		SELECT
			@ReconcilePositionDefinitionID,
			@PositionDate,
			@HoldingAccountID,
			@ClientAccountID,
			@UserID,
			GETDATE(),
			@UserID,
			GETDATE();
			
		SET @ReconcilePositionAccountID = @@IDENTITY
	END
	
	
	INSERT INTO ReconcilePosition (
		ReconcilePositionAccountID,
		AccountingAccountID,
		InvestmentSecurityID,
		OurPrice,
		OurFxRate,
		OurQuantity,
		OurMarketValueLocal,
		OurMarketValueBase,
		OurOpenTradeEquityLocal,
		OurOpenTradeEquityBase,
		CreateUserID,
		CreateDate,
		UpdateUserID,
		UpdateDate)
	SELECT
		@ReconcilePositionAccountID,
		--apd.AccountingTransactionID,
		MAX(AccountingAccountID),
		t.InvestmentSecurityID,
		AVG(MarketPrice),
		AVG(MarketFxRate),
		SUM(RemainingQuantity),
		SUM(MarketValueLocal),
		SUM(MarketValueBase),
		SUM(OpenTradeEquityLocal),
		SUM(OpenTradeEquityBase),
		@UserID,
		GETDATE(),
		@UserID,
		GETDATE()		
	FROM AccountingPositionDaily apd
		INNER JOIN AccountingTransaction t ON t.AccountingTransactionID = apd.AccountingTransactionID
		INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = t.InvestmentSecurityID
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
		
	WHERE t.HoldingInvestmentAccountID = @HoldingAccountID AND PositionDate = @PositionDate
		AND (s.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		AND (s.InvestmentSecurityID IN (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s 
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
				INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
			WHERE ig.InvestmentGroupID = @InvestmentGroupID) OR @InvestmentGroupID IS NULL)
	GROUP BY HoldingInvestmentAccountID, t.InvestmentSecurityID
	
	
	
	
	DECLARE @SecurityID INT, 
		@ExternalFxRate decimal(19, 10),
		@ExternalQuantity decimal(28, 10),
		@ExternalPrice decimal(28, 10),
		@ExternalMarketValueLocal decimal(19, 2),
		@ExternalMarketValueBase decimal(19, 2),
		@ExternalOpenTradeEquityLocal decimal(19, 2),
		@ExternalOpenTradeEquityBase decimal(19, 2)
		
	
	DECLARE position_cursor CURSOR FOR --local forward_only FOR 
	SELECT
		apd.InvestmentSecurityID,
		ExternalFxRate = AVG(FxRate),
		ExternalQuantity = SUM(Quantity),
		ExternalPrice = AVG(MarketPrice),
		ExternalMarketValueLocal = SUM(MarketValueLocal),
		ExternalMarketValueBase = SUM(MarketValueBase),
		ExternalOpenTradeEquityLocal = SUM(OpenTradeEquityLocal),
		ExternalOpenTradeEquityBase = SUM(OpenTradeEquityBase)
	FROM ReconcilePositionExternalDaily apd
		INNER JOIN InvestmentSecurity s ON s.InvestmentSecurityID = apd.InvestmentSecurityID
		INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
	WHERE HoldingAccountID = @HoldingAccountID AND PositionDate = @PositionDate
		AND (apd.InvestmentSecurityID = @InvestmentSecurityID OR @InvestmentSecurityID IS NULL)
		AND (s.InvestmentSecurityID IN (
			SELECT InvestmentSecurityID
			FROM InvestmentSecurity s 
				INNER JOIN InvestmentInstrument ii ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItemInstrument iigi ON iigi.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN InvestmentGroupItem igi ON igi.InvestmentGroupItemID = iigi.InvestmentGroupItemID
				INNER JOIN InvestmentGroup ig ON ig.InvestmentGroupID = igi.InvestmentGroupID
			WHERE ig.InvestmentGroupID = @InvestmentGroupID) OR @InvestmentGroupID IS NULL)
	GROUP BY HoldingAccountID, apd.InvestmentSecurityID	
	ORDER BY SUM(OpenTradeEquityBase)
    
    
    OPEN position_cursor;

	FETCH NEXT FROM position_cursor 
	INTO @SecurityID,@ExternalFxRate,@ExternalQuantity,@ExternalPrice,@ExternalMarketValueLocal,@ExternalMarketValueBase,@ExternalOpenTradeEquityLocal,@ExternalOpenTradeEquityBase;
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		

		IF EXISTS (SELECT ReconcilePositionID FROM ReconcilePosition rp WHERE InvestmentSecurityID = @SecurityID AND ReconcilePositionAccountID = @ReconcilePositionAccountID)
		BEGIN
			DECLARE @ReconcilePositionID INT;
			
			SELECT @ReconcilePositionID = ReconcilePositionID
			FROM ReconcilePosition rp
			WHERE InvestmentSecurityID = @SecurityID AND ReconcilePositionAccountID = @ReconcilePositionAccountID
		
			UPDATE ReconcilePosition SET
				[ExternalFxRate] = @ExternalFxRate,
				[ExternalQuantity] = @ExternalQuantity,
				[ExternalPrice] = @ExternalPrice,
				[ExternalMarketValueLocal] = @ExternalMarketValueLocal,
				[ExternalMarketValueBase] = @ExternalMarketValueBase,
				[ExternalOpenTradeEquityLocal] = @ExternalOpenTradeEquityLocal,
				[ExternalOpenTradeEquityBase] = @ExternalOpenTradeEquityBase,
				IsMatched = 1,
				UpdateUserID = @UserID,
				UpdateDate = GETDATE()
			WHERE ReconcilePositionID = @ReconcilePositionID
		END
		ELSE
		BEGIN
			INSERT INTO ReconcilePosition
			(
				ReconcilePositionAccountID,
				InvestmentSecurityID,
				ExternalFxRate,
				ExternalQuantity,
				ExternalMarketValueLocal,
				ExternalMarketValueBase,
				ExternalOpenTradeEquityLocal,
				ExternalOpenTradeEquityBase,
				CreateUserID,
				CreateDate,
				UpdateUserID,
				UpdateDate
			)
			VALUES
			(
				@ReconcilePositionAccountID,
				@SecurityID,
				@ExternalFxRate,
				@ExternalQuantity,
				@ExternalMarketValueLocal,
				@ExternalMarketValueBase,
				@ExternalOpenTradeEquityLocal,
				@ExternalOpenTradeEquityBase,
				@UserID,
				GETDATE(),
				@UserID,
				GETDATE()	
			)
		END
	
		FETCH NEXT FROM position_cursor 
		INTO @SecurityID,@ExternalFxRate,@ExternalQuantity,@ExternalPrice,@ExternalMarketValueLocal,@ExternalMarketValueBase,@ExternalOpenTradeEquityLocal,@ExternalOpenTradeEquityBase;
	END
	CLOSE position_cursor;
    DEALLOCATE position_cursor;
    
END
