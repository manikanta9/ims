-- exec reconcile.LoadReconcilePositionDefinitionData '1/3/11',1
CREATE PROC reconcile.LoadReconcilePositionDefinitionData
	@PositionDate DATETIME,
	@ReconcilePositionDefinitionID INT,
	@UserID INT
AS
BEGIN

	DECLARE @HoldingAccountID INT
	
	DELETE FROM ReconcilePosition WHERE ReconcilePositionID IN
    (
		SELECT ReconcilePositionID
		FROM ReconcilePosition  rp
			INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
		WHERE rpa.PositionDate = @PositionDate AND rpa.ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
	)
	
	DELETE FROM ReconcilePositionAccount
	WHERE PositionDate = @PositionDate AND ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID

	
	DECLARE account_cursor CURSOR local forward_only FOR 
	SELECT InvestmentAccountID
	FROM InvestmentAccount ia
		INNER JOIN ReconcilePositionDefinition rpd ON 
			(rpd.HoldingCompanyID  IS NULL OR rpd.HoldingCompanyID = ia.IssuingCompanyID)
			AND (rpd.InvestmentGroupID IS NULL OR rpd.InvestmentGroupID = ia.TeamSecurityGroupID)
	WHERE ReconcilePositionDefinitionID = @ReconcilePositionDefinitionID
  
    
    OPEN account_cursor;

	FETCH NEXT FROM account_cursor 
	INTO @HoldingAccountID
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
		
		IF EXISTS (SELECT AccountingPositionDailyID FROM AccountingPositionDaily apd WHERE HoldingAccountID = @HoldingAccountID)
		BEGIN
			EXEC reconcile.LoadReconcilePositionAccountingPositionDailyData @PositionDate, @HoldingAccountID, @ReconcilePositionDefinitionID, @UserID
		END
	
		FETCH NEXT FROM account_cursor 
		INTO @HoldingAccountID
	END
	CLOSE account_cursor;
    DEALLOCATE account_cursor;
    
END
