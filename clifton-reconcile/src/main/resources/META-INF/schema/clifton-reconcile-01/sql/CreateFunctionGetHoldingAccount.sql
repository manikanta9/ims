CREATE FUNCTION [Reconcile].[GetHoldingAccount]
(
	@AccountNumber NVARCHAR(50)
)
RETURNS INT
AS
BEGIN
	
	DECLARE @Result INT
	SET @Result = (SELECT TOP 1 InvestmentAccountID
			FROM
			(
				SELECT 
					InvestmentAccountID, REPLACE(AccountNumber,'-','') as AccountNumber,
					CASE WHEN CHARINDEX('-',AccountNumber) = 0 OR CHARINDEX('-',AccountNumber) IS NULL THEN 0 ELSE 1 END HasDash
				FROM InvestmentAccount
			) x
			WHERE AccountNumber =  REPLACE(@AccountNumber,'-','')
			GROUP BY InvestmentAccountID, AccountNumber, HasDash
			ORDER BY AccountNumber, HasDash DESC)
			
	RETURN @Result
END
