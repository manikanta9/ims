-- exec dbo.LoadReconcilePositionAccountingPositionDailyData '1/3/11',1
CREATE PROC reconcile.LoadReconcilePositionAccountingPositionDailyData
	@PositionDate DATETIME,
	@HoldingAccountID INT,
	@ReconcilePositionDefinitionID INT,
	@UserID INT
AS
BEGIN
	DECLARE @ReconcilePositionAccountID INT

	SELECT @ReconcilePositionAccountID = ReconcilePositionAccountID 
	FROM ReconcilePositionAccount WHERE HoldingAccountID = @HoldingAccountID AND PositionDate = @PositionDate


	IF @ReconcilePositionAccountID IS NOT NULL
	BEGIN
	
		DELETE FROM ReconcilePosition WHERE ReconcilePositionAccountID = @ReconcilePositionAccountID
		DELETE FROM ReconcilePositionAccount WHERE ReconcilePositionAccountID = @ReconcilePositionAccountID
	END
	
	INSERT INTO ReconcilePositionAccount(ReconcilePositionDefinitionID, PositionDate, HoldingAccountID,[CreateUserID],[CreateDate],[UpdateUserID],[UpdateDate])
	SELECT
		@ReconcilePositionDefinitionID,
		@PositionDate,
		@HoldingAccountID,
		@UserID,
		GETDATE(),
		@UserID,
		GETDATE();
		
	SET @ReconcilePositionAccountID = @@IDENTITY
	
	
	INSERT INTO ReconcilePosition (
		ReconcilePositionAccountID,
		AccountingAccountID,
		InvestmentSecurityID,
		OurFxRate,
		OurQuantity,
		OurMarketValueLocal,
		OurMarketValueBase,
		--OurOTEVariation,
		CreateUserID,
		CreateDate,
		UpdateUserID,
		UpdateDate)
	SELECT
		@ReconcilePositionAccountID,
		MAX(AccountingAccountID),
		InvestmentSecurityID,
		AVG(MarketFxRate),
		SUM(RemainingQuantity),
		SUM(MarketValueLocal),
		SUM(MarketValueBase),
		@UserID,
		GETDATE(),
		@UserID,
		GETDATE()		
	FROM AccountingPositionDaily apd
	WHERE HoldingAccountID = @HoldingAccountID AND PositionDate = @PositionDate
	GROUP BY HoldingAccountID, InvestmentSecurityID

	
	DECLARE @InvestmentSecurityID INT, 
		@ExternalFxRate decimal(19, 10),
		@ExternalQuantity decimal(19, 10),
		@ExternalMarketValueLocal decimal(19, 10),
		@ExternalMarketValueBase decimal(19, 10)
		
	
	DECLARE position_cursor CURSOR local forward_only FOR 
	SELECT
		InvestmentSecurityID,
		ExternalFxRate = AVG(FxRate),
		ExternalQuantity = SUM(Quantity),
		ExternalMarketValueLocal = SUM(MarketValueLocal),
		ExternalMarketValueBase = SUM(MarketValueBase)
	FROM ReconcilePositionExternalDaily apd
	WHERE HoldingAccountID = @HoldingAccountID AND PositionDate = @PositionDate
	GROUP BY HoldingAccountID, InvestmentSecurityID	
    
    
    OPEN position_cursor;

	FETCH NEXT FROM position_cursor 
	INTO @InvestmentSecurityID,@ExternalFxRate,@ExternalQuantity,@ExternalMarketValueLocal,@ExternalMarketValueBase;
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		DECLARE @ReconcilePositionID INT;
		SELECT @ReconcilePositionID = ReconcilePositionID
		FROM ReconcilePosition rp
			INNER JOIN ReconcilePositionAccount rpa ON rpa.ReconcilePositionAccountID = rp.ReconcilePositionAccountID
		WHERE HoldingAccountID = @HoldingAccountID AND InvestmentSecurityID = @InvestmentSecurityID
	
		IF NOT @ReconcilePositionID IS NULL
		BEGIN
			UPDATE ReconcilePosition SET
				[ExternalFxRate] = @ExternalFxRate,
				[ExternalQuantity] = @ExternalQuantity,
				[ExternalMarketValueLocal] = @ExternalMarketValueLocal,
				[ExternalMarketValueBase] = @ExternalMarketValueBase,
				IsMatched = 1,
				UpdateUserID = @UserID,
				UpdateDate = GETDATE()
			WHERE ReconcilePositionID = @ReconcilePositionID
		END
		ELSE
		BEGIN
			INSERT INTO ReconcilePosition
			(
				ReconcilePositionAccountID,
				InvestmentSecurityID,
				ExternalFxRate,
				ExternalQuantity,
				ExternalMarketValueLocal,
				ExternalMarketValueBase,
				--OurOTEVariation,
				CreateUserID,
				CreateDate,
				UpdateUserID,
				UpdateDate
			)
			VALUES
			(
				@ReconcilePositionAccountID,
				@InvestmentSecurityID,
				@ExternalFxRate,
				@ExternalQuantity,
				@ExternalMarketValueLocal,
				@ExternalMarketValueBase,
				@UserID,
				GETDATE(),
				@UserID,
				GETDATE()	
			)
		END
	
		FETCH NEXT FROM position_cursor 
		INTO @InvestmentSecurityID,@ExternalFxRate,@ExternalQuantity,@ExternalMarketValueLocal,@ExternalMarketValueBase;
	END
	CLOSE position_cursor;
    DEALLOCATE position_cursor;
    
END
