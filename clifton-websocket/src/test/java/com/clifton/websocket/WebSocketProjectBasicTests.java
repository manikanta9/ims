package com.clifton.websocket;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class WebSocketProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "websocket";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("broker");
		approvedList.add("management");
		approvedList.add("registry");
		approvedList.add("entity");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.security.Principal");
		imports.add("org.springframework.beans.");
		imports.add("org.springframework.context.annotation.");
		imports.add("org.springframework.core.MethodParameter");
		imports.add("org.springframework.core.Ordered");
		imports.add("org.springframework.core.PriorityOrdered");
		imports.add("org.springframework.messaging.");
		imports.add("org.springframework.scheduling.concurrent.");
		imports.add("org.springframework.security.config.annotation.web.");
		imports.add("org.springframework.security.messaging.context.SecurityContextChannelInterceptor");
		imports.add("org.springframework.util.");
		imports.add("org.springframework.web.socket.");
	}
}
