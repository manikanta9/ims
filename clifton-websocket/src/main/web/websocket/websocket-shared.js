/**
 * The utility object for interfacing with a WebSocket API.
 *
 * This object provides functions for managing a single connection, subscribing to channels, and sending messages.
 *
 * This object can only maintain a single connection at a time. For multiple connections, multiple instances of the object should be created.
 */
TCG.websocket = new class {

	constructor() {
		this.enabled = false;
		this.debug = null;
		this.endpoint = null;
		this.errorChannel = '/user/topic/error';

		this.stomp = null;
		this.sessionId = null;
		this.defaultHeaders = null;
		this.subscriptions = null;

		this.clientMessageIdNum = null;
		this.receiptHandlers = null;
		this.receiptTimeout = 10000;
		this.reconnectDelay = 30000;

		this.autoSubscriptions = {};
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Public Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Connects to the STOMP host at the given target using the provided headers.
	 *
	 * If no target is given, the value of {@link #endpoint} is used. The <tt>headers</tt> field is particularly useful for required headers such as CSRF validation tokens.
	 *
	 * This function disconnects any existing WebSocket connection before attempting to re-connect.
	 *
	 * @param {string} [target] the STOMP host URI, or {@link #endpoint} if no target is provided
	 * @param {*} [headers] the headers to attach to the request, or {@link #defaultHeaders} if no headers are provided
	 * @param {boolean} [disableReconnect] if <tt>true</tt>, reconnects will not be enabled for this connection
	 * @return {Promise} a promise which is resolved with the result of the connection attempt
	 */
	connect(target, headers, disableReconnect) {
		let result;
		if (!this.enabled) {
			// WebSockets disabled
			result = Promise.reject('WebSockets are disabled. The WebSocket connection will not proceed.')
				.catch(err => console.warn(err));
		}
		else {
			// Force disconnect before connection
			result = this.disconnect(this.getMessageHeaders())
				.then(() => new Promise((resolve, reject) => {
					// Attempt connection
					const stomp = this.initStomp(target || this.endpoint, disableReconnect);
					this.stomp = stomp;
					stomp.connect(this.getMessageHeaders(headers), connectFrame => {
						if (stomp === this.stomp) {
							// Successful connection
							this.initSession();
							resolve(connectFrame);
						}
						else {
							// Multiple connections detected
							stomp.disconnect();
							reject('The established connection is no longer active. Disconnecting.');
						}
					}, reject);
				}));
		}
		return result;
	}


	/**
	 * Disconnects the active WebSocket connection if such a connection exists.
	 *
	 * @param [headers] the headers to attach to the request, or {@link #defaultHeaders} if no headers are provided
	 * @return {Promise} the WebSocket operation promise chain
	 * @return {Promise} a promise which is resolved with the result of the disconnect operation, or with <tt>null</tt> if no previous connection was established
	 */
	disconnect(headers) {
		const stomp = this.stomp;
		const result = (stomp && stomp.connected)
			? new Promise((resolve, reject) => stomp.disconnect(resolve, this.getMessageHeaders(headers)))
			: Promise.resolve();
		this.clearSession();
		return result;
	}


	/**
	 * Sends the specified message on the given channel.
	 *
	 * @param {string} channel the channel to send on
	 * @param {string|object} payload the payload to send
	 * @param config the message configuration
	 * @param [config.headers] the headers to attach to the request, or {@link #defaultHeaders} if no headers are provided
	 * @param [config.successCallback] the callback to execute on message success
	 * @param [config.errorCallback] the callback to execute on message failure
	 * @return the send result
	 */
	send(channel, payload, config = {}) {
		const clientMessageId = this.generateClientMessageId();
		const addReceipt = this.registerReceiptHandler(clientMessageId, config.successCallback, config.errorCallback);
		const serializedPayload = typeof payload === 'object' ? JSON.stringify(payload) : payload;
		return this.stomp.send(channel, this.getMessageHeaders(config.headers, clientMessageId, addReceipt), serializedPayload);
	}


	/**
	 * Subscribes to the given channel, executing the provided callback when frames are received.
	 *
	 * @param {string} channel the channel to subscribe to
	 * @param {function} callback the callback to execute when frames are received on the given channel
	 * @param config the message configuration
	 * @param [config.id] the subscription ID to use, or <tt>null</tt> to auto-generate a subscription ID
	 * @param [config.headers] the headers to attach to the request, or {@link #defaultHeaders} if no headers are provided
	 * @param [config.successCallback] the callback to execute on message success
	 * @param [config.errorCallback] the callback to execute on message failure
	 * @return the subscription result
	 */
	subscribe(channel, callback = Ext.emptyFn, config = {}) {
		const id = config.id || `s-${this.clientMessageIdNum}`;
		const clientMessageId = this.generateClientMessageId();
		const addReceipt = this.registerReceiptHandler(clientMessageId, config.successCallback, config.errorCallback);
		const subscription = this.stomp.subscribe(channel, callback, this.getMessageHeaders({...config.headers, id}, clientMessageId, addReceipt));
		this.subscriptions[id] = subscription;
		return subscription;
	}


	/**
	 * Unsubscribes from the given subscription ID.
	 *
	 * @param {string} id the subscription ID to unsubscribe from
	 * @param [config] the message configuration
	 * @param [config.headers] the headers to attach to the request, or {@link #defaultHeaders} if no headers are provided
	 * @param [config.successCallback] the callback to execute on message success
	 * @param [config.errorCallback] the callback to execute on message failure
	 * @return the unsubscription result
	 */
	unsubscribe(id, config = {}) {
		const clientMessageId = this.generateClientMessageId();
		const addReceipt = this.registerReceiptHandler(clientMessageId, config.successCallback, config.errorCallback);
		const subscription = this.subscriptions[id];
		if (!subscription) {
			throw new Error(`Unable to find an existing subscription with ID [${id}].`);
		}
		delete this.subscriptions[id];
		return subscription.unsubscribe(this.getMessageHeaders(config.headers, clientMessageId, addReceipt));
	}


	/**
	 * Registers a subscription to be enabled on connect. This subscription will be re-initialized each time that a connection is made, including during reconnection attempts.
	 *
	 * If a connection has already been established then this will initiate the subscription.
	 *
	 * @see #subscribe
	 * @see #unregisterSubscription
	 */
	registerSubscription(id, channel, callback = Ext.emptyFn, config = {}) {
		if (!this.autoSubscriptions[id]) {
			this.autoSubscriptions[id] = {channel, callback, config};
			if (this.isConnected()) {
				this.subscribe(channel, callback, {id, ...config});
			}
		}
	}


	/**
	 * Removes the subscription registration under the given ID.
	 *
	 * If a connection has already been established and the subscription exists then this will also remove the existing subscription.
	 *
	 * @see #unsubscribe
	 * @see #registerSubscription
	 */
	unregisterSubscription(id) {
		if (this.autoSubscriptions[id]) {
			delete this.autoSubscriptions[id];
			if (this.isConnected() && this.subscriptions[id]) {
				this.unsubscribe(id);
			}
		}
	}


	/**
	 * Returns <tt>true</tt> if an active connected session exists, or <tt>false</tt> otherwise.
	 */
	isConnected() {
		return TCG.websocket.stomp && TCG.websocket.stomp.connected;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Internal Methods                                ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Initializes a STOMP connection.
	 *
	 * @param {string} target the STOMP target URI
	 * @param {boolean} [disableReconnect] if <tt>true</tt>, reconnects will not be enabled for this connection
	 * @return {*} the initialized STOMP object
	 * @private
	 */
	initStomp(target, disableReconnect) {
		// Define and execute STOMP initialization; setting a connection function automatically enables retries
		let connectAttempt = 0;
		const stomp = Stomp.over(() => {
			if (++connectAttempt > 0 && disableReconnect) {
				throw new Error('Connection attempt terminated. Reconnection is disabled.');
			}
			// Initiate connection if and only if this is still the active STOMP instance
			return (stomp === this.stomp) ? this.initSocket(target) : {};
		});

		/* Configure the STOMP object properties */
		// Proxy object settings to STOMP object
		Object.defineProperties(stomp, {
			debug: {get: () => this.debug || Ext.emptyFn},
			reconnect_delay: {get: () => this.reconnectDelay || 0}
		});

		// Setup receipt handling
		stomp.onreceipt = this.handleSuccess;

		return stomp;
	}


	/**
	 * @return {*} the socket connection object
	 * @private
	 */
	initSocket(target) {
		let ws;
		if (TCG.data.AuthHandler.isLoginWindowActive()) {
			// Prevent superfluous initiation requests while the login window is open; emulate disconnect after delay
			const dummyWs = {onclose: Ext.emptyFn};
			setTimeout(() => dummyWs.onclose(), 1000);
			ws = dummyWs;
		}
		else {
			ws = new SockJS(target);
		}
		return ws;
	}


	/**
	 * @private
	 */
	initSession() {
		// Deconstruct transport URL to retrieve generated session ID
		const [, sessionId] = /\/([^/]+)\/[^/]+$/.exec(this.stomp.ws._transport.url) || [];
		if (!sessionId) {
			throw new Error(`Unable to determine the session ID from the transport URL: [${this.stomp.ws._transport.url}]`);
		}
		this.sessionId = sessionId;
		this.clientMessageIdNum = 0;
		this.receiptHandlers = {};
		this.subscriptions = {};

		// Enable configured subscriptions
		Object.entries(this.autoSubscriptions).forEach(([id, sub]) => this.subscribe(sub.channel, sub.callback, {...sub.config, id}));
	}


	/**
	 * @private
	 */
	clearSession() {
		this.stomp = null;
		this.sessionId = null;
		this.clientMessageIdNum = null;
		this.receiptHandlers = null;
		this.subscriptions = null;
	}


	/**
	 * Generates a new client message ID to be sent along with a WebSocket frame. This message ID will also be attached to any direct responses to this frame.
	 *
	 * @private
	 */
	generateClientMessageId() {
		// Normalize client message ID
		this.clientMessageIdNum = (this.clientMessageIdNum || 0);
		return `${this.sessionId}-${this.clientMessageIdNum++}`;
	}


	/**
	 * Gets the headers to use when sending a WebSocket frame.
	 *
	 * @private
	 */
	getMessageHeaders(overrides, clientMessageId, addReceipt) {
		return Object.assign(
			this.defaultHeaders || {},
			{'client-message-id': clientMessageId || this.generateClientMessageId()},
			addReceipt ? {'receipt': clientMessageId} : {},
			overrides
		);
	}


	/**
	 * Registers a handler for the response for the given message ID.
	 *
	 * When a frame of type <tt>RECEIPT</tt> is received from the server, it is checked against any existing receipt handlers for an identical message ID. If a matching receipt
	 * handler is found, its success callback will be executed and the handler will be cleared (i.e., removed to allow GC as the function <i>must</i> no longer be called).
	 *
	 * When a frame of type <tt>MESSAGE</tt> for the error channel is received from the server, it is also checked against any existing receipt handlers in the same way. If a
	 * matching receipt handler is found, its error callback will be executed and the handler will be cleared.
	 *
	 * <tt>RECEIPT</tt> frames will <i>always</i> be sent by the server if the <tt>receipt</tt> header is included in the client frame except in the case that the client frame
	 * cannot be parsed. This means that both an error message and a receipt frame may be received for a single client frame. Synchronous processing is necessary on the server-side
	 * to send the error message so that it is processed by the client <b>before</b> the receipt frame. Since the WebSockets protocol communicates over TCP, frame order is
	 * guaranteed as long as the server waits for verification of receipt (at the TCP level) before continuing. Asynchronous processing, however, may lead to a receipt frame being
	 * incorrectly processed <i>before</i> the error message.
	 *
	 * @param clientMessageId the client message ID for which a handler should be registered
	 * @param successCallback the callback to execute on message processing success, or <tt>null</tt> if no callback is needed
	 * @param errorCallback the callback to execute on message processing failure, or <tt>null</tt> if no callback is needed
	 * @return {boolean} <tt>true</tt> if a receipt handler was registered, or <tt>false</tt> otherwise
	 * @private
	 */
	registerReceiptHandler(clientMessageId, successCallback, errorCallback) {
		// Guard-clause: No callbacks; do not preform registration
		if (!successCallback && !errorCallback) {
			return false;
		}

		// Add receipt handler
		this.receiptHandlers[clientMessageId] = {
			success: successCallback,
			error: errorCallback,
			timeoutId: setTimeout(() => {
				// Remove receipt after timeout
				if (this.receiptHandlers[clientMessageId]) {
					console.warn(`Timeout encountered before a receipt for client message ID [${clientMessageId}] was received.`);
				}
				delete this.receiptHandlers[clientMessageId];
			}, this.receiptTimeout)
		};

		// Indicate that callbacks were registered
		return true;
	}


	/**
	 * Handles a <tt>RECEIPT</tt> message.
	 *
	 * @private
	 */
	handleSuccess(receiptMessage) {
		const receiptId = receiptMessage.headers['receipt-id'];
		const handler = this.receiptHandlers[receiptId];
		if (!handler) {
			console.warn(`A receipt was received for message ID [${receiptId}] but no callback was found.`);
		}
		else if (handler.success) {
			handler.success(receiptMessage);
		}
		delete this.receiptHandlers[receiptId];
	}
};

/*
 * Attach delayed fall-back connection attempts. This checks incrementally to see if a connection is active. If not, it will attempt to reconnect. This is a fall-back for cases in
 * which the connection has been disabled when it should not be (e.g., a connection has been terminated due to expired authentication, but authentication is renewed in a separate
 * tab).
 */
{
	const wsConnectCheckInterval = 1000 * 60 * 30; // 30 minutes
	setTimeout(() => Ext.TaskMgr.start({
		run: () => {
			if (TCG.websocket.enabled && !TCG.websocket.isConnected()) {
				TCG.websocket.connect(null, null, true)
					.catch(err => {
						// Do nothing; an exception is automatically thrown on disconnect
					});
			}
		},
		interval: wsConnectCheckInterval
	}), wsConnectCheckInterval);
}


// Handle messages on the error channel
TCG.websocket.registerSubscription('error', '/user/topic/error', message => {
	const error = JSON.parse(message.body);
	const clientMessageId = error['client-message-id'];
	const handler = TCG.websocket.receiptHandlers[clientMessageId];
	if (handler && handler.error) {
		handler.error(error);
	}
	else {
		console.warn(`An error was received for message ID [${clientMessageId}] but no error handler was found.`);
		TCG.showError(error.content || error.message || error, `Subscription error for [${error.destination || 'Unknown Destination'}]`);
	}
	console.warn('An error message was received over the WebSocket connection:', error);
	delete TCG.websocket.receiptHandlers[clientMessageId];
});


// Attach login/logout listeners for the WebSocket connection
TCG.data.AuthHandler.addLoginListener(() => TCG.websocket.connect(), true);
TCG.data.AuthHandler.addLogoutListener(() => TCG.websocket.disconnect());


/*
 * Patch SockJS to handle unsuccessful responses with HTTP 200 status codes gracefully.
 *
 * The SockJS info-receiver accepts all 200 responses as success indicators. Since we currently send 200s even during failed calls, even failed attempts will be handled as
 * successful negotiations. This hook attempts to handle the response to indicate a failed connection attempt when necessary.
 */
window.SockJS = class extends SockJS {
	// noinspection FunctionNamingConventionJS: This is a superclass function override
	_receiveInfo(info, rtt) {
		const validConnection = info && Object.getOwnPropertyNames(info).length > 0 && info.success !== false;
		const processedInfo = validConnection ? info : null;
		if (!validConnection) {
			console.debug('The WebSocket compatibility information negotiation failed. Probable causes are that the connection cannot be established or that the user session is invalid.');
		}
		return super._receiveInfo(processedInfo, rtt);
	}
};


/*
 * Add WebSockets statistics information to the stats administrative window. This tab is very similar to the "Runner Handler Stats" window.
 */
Clifton.core.stats.StatsListTabs.push(
	{
		title: 'WebSocket Stats',
		items: [
			{
				xtype: 'formpanel',
				loadValidation: false,
				labelWidth: 80,
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				defaults: {flex: 0},
				getLoadURL: function() {
					return 'webSocketStatus.json';
				},
				reload: function(params) {
					this.load({
						url: encodeURI(this.getLoadURL()),
						params: params || this.getLoadParams(this.getWindow()),
						waitMsg: 'Loading...',
						success: (form, action) => {
							this.loadJsonResult(action.result, true);
							this.fireEvent('afterload', this);
						},
						timeout: this.saveTimeout,
						...TCG.form.submitDefaults
					});
				},
				tbar: [
					{xtype: 'sectionheaderfield', header: 'WebSocket Stats'},
					'-',
					{
						text: 'Reload',
						xtype: 'button',
						iconCls: 'table-refresh',
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							fp.reload();
						}
					},
					'-',
					{
						text: 'Send Message',
						xtype: 'button',
						iconCls: 'run',
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							const className = 'Clifton.websocket.WebSocketSendMessageWindow';
							const cmpId = TCG.getComponentId(className, fp.getWindow().getId());
							TCG.createComponent(className, {id: cmpId, openerCt: fp});
						}
					}
				],
				items: [
					// Summary information
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {
							labelWidth: 140,
							columnWidth: (1 - (0.02 * 3)) / 4,
							layout: 'form',
							defaults: {xtype: 'integerfield', readOnly: true, anchor: '0'}
						},
						items: [
							{
								items: [
									{fieldLabel: 'Unique Users', name: 'numUniqueUsers', qtip: 'The current number of unique users connected via any of the supported protocols.'},
									{fieldLabel: 'WebSocket Connections', name: 'numWsConnections', qtip: 'The current number of connections via the WebSockets method.'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								items: [
									{fieldLabel: 'WebSocket Sessions', name: 'numWsSessions', qtip: 'The current number of connections via any of the supported protocols.'},
									{fieldLabel: 'Streaming Connections', name: 'numHttpStreamingConnections', qtip: 'The current number of connections via the HTTP streaming method.'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								items: [
									{fieldLabel: 'STOMP Sessions', name: 'numStompSessions', qtip: 'The current number of active STOMP sessions over the current connections.'},
									{fieldLabel: 'Polling Connections', name: 'numHttpPollingConnections', qtip: 'The current number of connections via the HTTP polling method.'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								items: [
									{fieldLabel: 'Terminated Sessions', name: 'numWsSessionsTerminated', qtip: 'The number of sessions which have been terminated unexpectedly.'},
									{fieldLabel: 'Transport Errors', name: 'numTransportErrors', qtip: 'The number of transport errors which have occurred.'}
								]
							}
						]
					},
					// Thread information
					{
						xtype: 'formgrid',
						title: '<div qtip="View the details associated with the current thread pools responsible for executing runners.">Thread Pool Executor Stats</div>',
						collapsible: false,
						readOnly: true,
						hideInstructions: true,
						storeRoot: 'executorServiceStatusList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						height: 125,
						bindToFormLoad: true,
						columnsConfig: [
							{header: 'Thread Pool Name', width: 225, dataIndex: 'name'},
							{header: 'Thread Prefix', width: 225, dataIndex: 'threadPrefix', hidden: true}, //hidden since it is likely the same as the thread pool name
							{header: 'Running', width: 65, dataIndex: 'running', type: 'boolean'},
							{header: 'Scheduled Tasks', width: 130, dataIndex: 'taskCount', type: 'int'},
							{header: 'Completed Tasks', width: 130, dataIndex: 'completedTaskCount', type: 'int'},
							{header: 'Active Tasks', width: 100, dataIndex: 'activeTaskCount', type: 'int'},
							{header: 'Queued Tasks', width: 110, dataIndex: 'queuedCount', type: 'int'},
							{header: 'Pool Size', width: 90, dataIndex: 'poolSize', type: 'int'},
							{header: 'Core Pool Size', width: 110, dataIndex: 'corePoolSize', type: 'int'},
							{header: 'Max Pool Size', width: 100, dataIndex: 'maxPoolSize', type: 'int', hidden: true},
							{header: 'Largest Pool Size', width: 130, dataIndex: 'largestPoolSize', type: 'int'},
							{header: 'Keep Alive Seconds', width: 135, dataIndex: 'keepAliveSeconds', type: 'int', hidden: true}
						]
					},
					// User information
					{
						xtype: 'formgrid',
						title: 'WebSocket Connections',
						flex: 0.5,
						collapsible: false,
						readOnly: true,
						storeRoot: 'userStatsList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						bindToFormLoad: true,
						autoExpandColumn: true,
						columnsConfig: [
							{header: 'User Name', dataIndex: 'userName', width: 300, defaultSortColumn: true},
							{header: 'Active Sessions', dataIndex: 'numActiveSessions', width: 100, type: 'int', summaryType: 'sum'},
							{header: 'Active Subscriptions', dataIndex: 'numActiveSubscriptions', width: 200, type: 'int', summaryType: 'sum'}
						],
						plugins: {ptype: 'gridsummary'}
					},
					// Subscription information
					{
						xtype: 'formgrid',
						title: 'WebSocket Subscriptions',
						flex: 0.5,
						collapsible: false,
						readOnly: true,
						storeRoot: 'subscriptionStatsList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						bindToFormLoad: true,
						useFilters: true,
						autoExpandColumn: 'user',
						groupField: 'destination',
						groupTextTpl: '{values.group} ({[values.rs.length]} {[values.rs.length > 1 ? "Subscriptions" : "Subscription"]})',
						columnsConfig: [
							{header: 'User Name', id: 'user', dataIndex: 'userName', defaultSortColumn: true, width: 300},
							{header: 'Destination', dataIndex: 'destination', width: 300, hidden: true},
							{header: 'Subscription ID', dataIndex: 'subId', width: 100}
						]
					}
				]
			}
		]
	}
);


/**
 * A class to manage subscriptions to entities over a websocket. Listeners can register callback functions to be invoked when entities are updated.
 * Callback functions should take a single parameter which is a list of entities that is returned by the WebSocket Subscription.
 * Example:  myCallback(entityList) where entityList is an array of entities.
 */
TCG.websocket.WebSocketSubscriptionHandler = class {
	/**
	 * Constructor for this class
	 *
	 * @param channel a string that identifies this subscription
	 * @param topic a string that identifies the topic for the subscription
	 */
	constructor(channel, topic) {
		this.listenerCallbacks = [];
		this.entityMap = {};
		this.initialized = false;
		this.channel = channel;
		this.topic = topic;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Internal method to establish the WebSocket subscription.
	 * @protected
	 */
	initializeSubscription() {
		if (!this.initialized) {
			TCG.websocket.registerSubscription(this.channel, this.topic, message => this.updateEntityList(JSON.parse(message.body)));
			this.entityBuffer = new TCG.DebounceBuffer(() => this.applyEntityList(), 500, 1000);
			this.initialized = true;
		}
	}


	/**
	 * Internal method to updates the current  state using the provided message
	 * @protected
	 */
	updateEntityList(message) {
		if (message.reset) {
			this.entityMap = {};
		}
		message.added.forEach(entity => this.entityMap[entity.id] = entity);
		message.removed.forEach(entityId => delete this.entityMap[entityId]);
		this.entityBuffer.next();
	}


	/**
	 * Internal method to invoke registered callbacks upon updates or expired delay
	 * @protected
	 */
	applyEntityList() {
		if (this.listenerCallbacks.length > 0) {
			const entityList = this.getEntityList();
			this.listenerCallbacks.forEach(listener => listener(entityList));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Method to register a callback that should be invoked when entities are updated
	 * @param callback the callback to invoke upon entity updates.
	 */
	addCallback(callback) {
		this.initializeSubscription();
		this.listenerCallbacks.push(callback);
	}


	/**
	 * Method to remove a registered callback, if it exists.
	 * @param callback the callback to remove
	 */
	removeCallback(callback) {
		this.listenerCallbacks.remove(callback);
		if (this.listenerCallbacks.length === 0) {
			TCG.websocket.unregisterSubscription(this.channel);
			this.initialized = false;
		}
	}


	/**
	 * Returns a list of entities managed by this instance.
	 * @returns {any[]}
	 */
	getEntityList() {
		return Object.values(this.entityMap);
	}
};
