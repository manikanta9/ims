Ext.ns('Clifton.websocket.alert');

Clifton.websocket.alert.ToastManager = new class {

	constructor() {
		this.toasts = [];
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Public Methods                                  ////////
	////////////////////////////////////////////////////////////////////////////


	addToast(toast) {
		if (!this.toasts.includes(toast)) {
			toast.previousTooltip = this.toasts[this.toasts.length - 1];
			this.toasts.push(toast);
			toast.on('hide', tip => this.cascade());
		}
	}


	updatePositions() {
		this.cascade();
	}


	showAndHighlightToasts() {
		for (const toast of this.toasts) {
			toast.showAndHighlightToast();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Internal Methods                                ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @private
	 */
	cascade() {
		this.toasts
			.filter(toast => toast.isVisible())
			.sort((a, b) => a.stackOrder - b.stackOrder)
			.forEach((toast, index, array) => {
				toast.previousToast = array[index - 1];
				toast.updateToastPosition();
			});
	}
};


Clifton.websocket.alert.TransientToast = Ext.extend(Ext.Tip, {
	id: 'transientMessageTip',
	renderTo: Ext.getBody(),
	closable: true,
	closeAction: 'hide',
	title: 'Transient Notifications',
	minWidth: 500,
	maxWidth: 500,
	floating: {shadow: false},
	stackOrder: 1,
	bodyStyle: {
		'background-color': 'white',
		'border-style': 'solid',
		'border-width': '1px',
		'border-color': 'grey',
		'padding': '3px'
	},
	style: {
		'border-style': 'solid',
		'border-width': '1px',
		'border-radius': '5px',
		'border-color': 'transparent'
	},
	listeners: {
		afterrender: function(p) {
			this.el.on('click', this.itemClicked);
		}
	},
	// language=HTML
	tpl: TCG.trimWhitespace(`
		<tpl for=".">
			<p style="padding-top: 5px; padding-bottom: 3px; white-space: nowrap; text-overflow: ellipsis; overflow: hidden; color:#5f5f5f;">
				<b id="notification_category_{definitionId}">{[fm.htmlEncode(values.category)]}</b>
			</p>
			<tpl for="details">
				<div style="display: flex;">
					<div style="flex: 95%; text-overflow: ellipsis; overflow: hidden; white-space: nowrap;">
						<!--suppress BadExpressionStatementJS -->
						<p style="text-indent: 5px; padding-bottom: 3px; {styles};"
						   id="notification_cause_{values.fkFieldId}_{parent.detailScreenClass}" onmouseover="{[values.virtual ? '' : 'this.style.cursor=\\'pointer\\'']}">
							&nbsp;{[fm.htmlEncode(values.detail)]}
						</p>
					</div>
					<tpl if="!values.virtual">
						<div style="flex: 5%;">
							<span style="width: 16px; height: 16px; float: right; margin-right: 5px; margin-left: 5px; "
							      onmouseover="this.style.cursor=\'pointer\'" class="cross" ext:qtip="Permanently dismiss this notification."
							      id="notification_dismiss_{values.id}">&nbsp;</span>
						</div>
					</tpl>
				</div>
			</tpl>
		</tpl>
	`),

	data: [],
	recordList: [],

	itemClicked: function(event, target) {
		if (target && target.id != null) {
			const splits = target.id.split('_');
			if (splits.length >= 3) {
				if (splits[2].length > 0) {
					if (splits[1] === 'dismiss') {
						const id = splits[2];
						Ext.getCmp('transientMessageTip').removeRecords([parseInt(id)]);
					}
					else if (splits[1] === 'cause' && splits.length === 4) {
						TCG.createComponent(splits[3], {
							id: splits[2],
							params: {id: splits[2]}
						});
					}
				}
			}
		}
	},


	// will transform then refresh.
	updateToastContent: function(data) {
		this.data = data;
		const transformed = this.transform(data);
		this.refreshToastContent(transformed);
	},

	addRecords: function(data) {
		this.updateToastContent([...this.data, ...data]);
	},

	removeRecords: function(idList) {
		const deleteItemList = this.recordList
			.flatMap(group => group.details)
			.filter(record => idList.includes(record.id))
			.map(record => record.data);
		const filteredRecords = this.data.filter(item => !deleteItemList.includes(item));
		this.updateToastContent(filteredRecords);
	},

	// data must be transformed.
	refreshToastContent: function(recordList) {
		this.recordList = recordList;
		const visible = this.isVisible();
		if (visible && recordList.length === 0) {
			// Hide toast if no data is present; this allows us to hide the tooltip without first clearing its contents when the last notification is completed
			this.update(recordList);
			this.el.fadeOut();
			this.setVisible(false);
		}
		else {
			// Update toast content and layout
			this.update(recordList);
			if (visible) {
				this.updateToastPosition();
			}
		}
	},

	highlightThrottle: false,
	showAndHighlightToast: function() {
		const toast = this;
		const visible = toast.isVisible();
		if (toast.data.length > 0) {
			// Show and highlight toast if data exists
			toast.updateToastPosition();
			if (!visible) {
				toast.el.fadeIn({duration: 1.5});
			}
			// Throttle highlight animation triggers
			if (!toast.highlightThrottle) {
				toast.highlightThrottle = true;
				setTimeout(function() {
					toast.highlightThrottle = false;
				}, 1500);
				toast.el.highlight('ff0000', {attr: 'border-color', duration: 1.5});
			}
		}
	},

	previousToast: null,
	updateToastPosition: function() {
		// Update toast layout
		this.doLayout();
		if (TCG.isNull(this.previousToast)) {
			this.showBy(Ext.getBody(), 'br-br');
			this.showAt(this.el.getAlignToXY(Ext.getBody(), 'br-br', [-10, -10]));
		}
		else {
			this.showBy(this.previousToast.id);
			this.showAt(this.el.getAlignToXY(this.previousToast.id));
		}
	},

	// Transformation functions
	transform: function(data) {
		// Process the given data for display
		const currentUser = TCG.getCurrentUser();
		const notificationsByDefinition = data
			.map(this.mapRecordToNotification, this)
			.reduce(this.groupByDefinitionAndPriority, {});

		// Sort notification map into list
		const sortedNotifications = this.sortNotifications(notificationsByDefinition, currentUser);

		// Truncate to display limits
		return this.truncateNotifications(sortedNotifications);
	},

	notificationCount: 1,
	mapRecordToNotification: function(data) {
		// Map the data to a simple object with its relevant details
		return {
			// Individual notification
			id: this.notificationCount++,
			detail: data.subject,
			styles: data.priority.cssStyle,
			fkFieldId: data.entityId,
			priorityName: data.priority.name,
			priorityOrder: data.priority.order,

			// Category (definition)ESM8 (S&P500 EMINI FUT  Jun18)
			definitionId: data.category,
			category: data.category,
			detailScreenClass: data.entityTable.detailScreenClass,

			// Link to original data
			data: data
		};
	},

	groupByDefinitionAndPriority: function(notificationsByDefinition, notification) {
		// Distinguish by concatenated definition ID and priority for notifications under the same definition with different priorities
		const key = String(notification.definitionId + '-' + notification.priorityOrder);
		let definition = notificationsByDefinition[key];
		if (!definition) {
			// Add definition
			definition = {
				definitionId: notification.definitionId,
				category: notification.category + ' (' + notification.priorityName + ')',
				detailScreenClass: notification.detailScreenClass,
				priorityOrder: notification.priorityOrder,
				priorityName: notification.priorityName,
				details: []
			};
			notificationsByDefinition[key] = definition;
		}

		// Add notification to definition
		definition.details.push({
			id: notification.id,
			detail: notification.detail,
			styles: notification.styles,
			assigneeUser: notification.assigneeUser,
			assigneeGroup: notification.assigneeGroup,
			fkFieldId: notification.fkFieldId,
			data: notification.data
		});

		return notificationsByDefinition;
	},

	sortNotifications: function(notificationsByDefinition, currentUser) {
		const notificationGroups = Object.values(notificationsByDefinition);

		// Sort list of groups; sort by priority (ASC) then definition ID (ASC)
		const sortedGroups = [...notificationGroups].sort((group1, group2) =>
			(group1.priorityOrder - group2.priorityOrder)
			|| (group1.definitionId - group2.definitionId));

		// Sort notifications within each group
		const sortedGroupsWithDetails = sortedGroups.map(group => Object.assign({}, group, {
			// Sort order: Self as assignee (DESC); Any assignee group (possibly containing self) (DESC); Any assignee user (likely other assignee) (ASC)
			details: group.details.sort((notification1, notification2) =>
				((notification2.assigneeUser === currentUser.id) - (notification1.assigneeUser === currentUser.id))
				|| !!notification2.assigneeGroup - !!notification1.assigneeGroup
				|| !!notification1.assigneeUser - !!notification2.assigneeUser)
		}));

		return sortedGroupsWithDetails;
	},

	truncateNotifications: function(notificationGroups) {
		const maxItems = 6;

		// Truncate the number of details within each group
		const groupsWithTruncatedDetails = notificationGroups.map(group => {
			let newGroup = group;
			if (group.details.length > maxItems) {
				const hideCount = group.details.length - maxItems + 1;
				const newDetails = group.details.slice(0, maxItems - 1);
				newDetails.push({detail: '(' + hideCount + ') More', virtual: true});
				newGroup = Object.assign({}, group, {details: newDetails});
			}
			return newGroup;
		});

		// Truncate the number of groups
		let truncatedGroups = groupsWithTruncatedDetails;
		if (groupsWithTruncatedDetails.length > maxItems) {
			const hideCount = groupsWithTruncatedDetails.length - maxItems + 1;
			truncatedGroups = groupsWithTruncatedDetails.slice(0, maxItems - 1);
			truncatedGroups.push({category: '(' + hideCount + ') More', details: [], virtual: true});
		}

		return truncatedGroups;
	}
});

// Manage toast subscriptions
{
	// transient toast subscription
	const transientToastCmp = new Clifton.websocket.alert.TransientToast({renderTo: Ext.getBody()});
	Clifton.websocket.alert.ToastManager.addToast(transientToastCmp);
	TCG.websocket.registerSubscription('alert', '/user/topic/alert', message => {
		const notificationMessage = Ext.util.JSON.decode(message.body);
		transientToastCmp.addRecords([notificationMessage]);
		transientToastCmp.showAndHighlightToast();
		Clifton.websocket.alert.ToastManager.updatePositions();
	});

	// Handle urgent toast regular pop-ups
	let toastTask;
	const disableToastTask = () => {
		if (toastTask != null) {
			Ext.TaskMgr.stop(toastTask);
			toastTask = null;
		}
	};
	const enableToastTask = () => {
		disableToastTask();
		// Forcibly pop-up urgent toast on an interval
		toastTask = Ext.TaskMgr.start({
			run: () => {
				Clifton.websocket.alert.ToastManager.showAndHighlightToasts();
				Clifton.websocket.alert.ToastManager.updatePositions();
			},
			interval: 1000 * 60 * 30 // 30 minutes
		});
	};
	TCG.data.AuthHandler.addLoginListener(enableToastTask);
	TCG.data.AuthHandler.addLogoutListener(disableToastTask);
}
