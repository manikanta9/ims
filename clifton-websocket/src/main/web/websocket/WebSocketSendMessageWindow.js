/**
 * The window for sending WebSocket messages to arbitrary users or on arbitrary broadcast channels.
 */
Clifton.websocket.WebSocketSendMessageWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'WebSocket - Send Message',
	iconCls: 'run',
	enableShowInfo: false,
	okButtonText: 'Send',
	okButtonTooltip: 'Send the WebSocket message.',
	width: 800,
	height: 500,

	items: [{
		xtype: 'formpanel',
		instructions: 'Use this window to send a WebSocket message to a user. Select a user from the list and then select a channel on which the message should be sent. Finally, enter the content to send to the user and then click on the "Send" button.',
		layout: 'vbox',
		layoutConfig: {align: 'stretch'},
		items: [
			{
				xtype: 'panel',
				layout: 'form',
				defaults: {anchor: '-35'},
				items: [
					// Channel type selector
					{
						xtype: 'radiogroup', columns: 2, fieldLabel: 'Channel Type', name: 'channelType', allowBlank: false, submitValue: false,
						items: [
							{boxLabel: 'User-Specific', xtype: 'radio', name: 'channelType', inputValue: 'user-specific'},
							{boxLabel: 'Broadcast', xtype: 'radio', name: 'channelType', inputValue: 'broadcast'}
						],
						listeners: {
							change: (radioGroup, checkedItem) => {
								const formPanel = TCG.getParentFormPanel(radioGroup);

								// Disable fields
								formPanel.disableField('user', false);
								formPanel.disableField('user-channel', true);
								formPanel.disableField('broadcast-channel', true);

								// Re-enable fields
								switch (checkedItem && checkedItem.inputValue) {
									case 'user-specific':
										formPanel.enableField('user', false);
										formPanel.enableField('user-channel', true);
										break;
									case 'broadcast':
										formPanel.enableField('broadcast-channel', true);
										break;
									default:
									// Do nothing
								}
							}
						}
					},

					// Hidden fields
					{name: 'channel', xtype: 'hidden'},
					{name: 'userId', xtype: 'hidden'},

					// Primary fields
					{fieldLabel: 'User', name: 'user', xtype: 'combo', allowBlank: false, submitValue: false, url: 'webSocketUserList.json', loadAll: true, valueField: 'id', displayField: 'label', detailPageClass: 'Clifton.security.user.UserWindow', qtip: 'The user to whom the message should be sent. Only connected users are displayed.'},
					{fieldLabel: 'Channel', name: 'broadcast-channel', xtype: 'textfield', allowBlank: false, submitValue: false, hidden: true, qtip: 'The broadcast channel on which the message should be sent.'},
					{
						fieldLabel: 'Channel', name: 'user-channel', xtype: 'combo', allowBlank: false, submitValue: false, url: 'webSocketUserSubscriptionList.json?userSpecificOnly=true', loadAll: true, valueField: 'destination', displayField: 'destination', requiredFields: ['user'], qtip: 'The channel on which the message should be sent. Only user-specific subscribed channels are displayed.',
						beforequery: queryEvent => {
							const cmb = queryEvent.combo;
							const f = TCG.getParentFormPanel(cmb).getForm();
							cmb.store.baseParams = {userId: f.findField('user').getValue()};
						}
					},
					{fieldLabel: 'Message', name: 'message', xtype: 'script-textarea', allowBlank: false, qtip: 'The message to send to the user. In most cases, this should be sent as valid JSON.'}
				]
			},
			// The list of previously-sent messages
			{
				xtype: 'formgrid',
				itemId: 'webSocketMessageGrid',
				title: 'Sent Messages',
				hideInstructions: true,
				collapsible: false,
				readOnly: true,
				flex: 1,
				bindToFormLoad: false,
				autoHeight: false,
				autoExpandColumn: 'message',
				viewConfig: {markDirty: false},

				columnsConfig: [
					{header: 'User ID', width: 50, dataIndex: 'userId', hidden: true, type: 'int'},
					{header: 'User', width: 100, dataIndex: 'user'},
					{header: 'Channel', width: 150, dataIndex: 'channel'},
					{header: 'Message', id: 'message', width: 300, dataIndex: 'message'},
					{header: 'Date', width: 150, dataIndex: 'date', fixed: true, defaultSortColumn: true, defaultSortDirection: 'DESC'},
					{header: '', width: 25, fixed: true, renderer: () => TCG.renderActionColumn('copy', '', 'Copy the message data.', 'copy')}
				],

				listeners: {
					rowclick: (grid, index, event) => {
						if (TCG.isActionColumn(event.target) && TCG.getActionColumnEventName(event) === 'copy') {
							const formPanel = TCG.getParentFormPanel(grid);
							const record = grid.store.getAt(index);
							const broadcast = !record.get('userId');
							const newFormValues = {};
							if (broadcast) {
								Ext.apply(newFormValues, {
									'channelType': 'broadcast',
									'broadcast-channel': record.get('channel'),
									'message': record.get('message')
								});
							}
							else {
								Ext.apply(newFormValues, {
									'channelType': 'user-specific',
									'user': record.get('userId'),
									'user-channel': record.get('channel'),
									'message': record.get('message')
								});
							}
							formPanel.getForm().setValues(newFormValues);
						}
					}
				}
			}
		]
	}],

	listeners: {
		afterrender: panel => panel.getMainFormPanel().setFormValue('channelType', 'user-specific')
	},

	saveWindow: function() {
		const formPanel = this.getMainFormPanel();

		// Validate form
		if (formPanel.getFirstInValidField()) {
			TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
			return;
		}

		// Get fields
		const broadcast = formPanel.getFormValue('channelType', true, true).inputValue === 'broadcast';
		const channel = broadcast ? formPanel.getFormValue('broadcast-channel', true, true) : formPanel.getFormValue('user-channel', true, true);
		const userId = broadcast ? null : formPanel.getFormValue('user', true, true);
		const user = broadcast ? null : formPanel.getForm().findField('user').el.dom.value;
		const message = formPanel.getFormValue('message', true, true);
		const date = new Date();
		formPanel.setFormValue('userId', userId);
		formPanel.setFormValue('channel', channel);

		// Send message
		formPanel.getForm().submit(Ext.apply({}, TCG.form.submitDefaults, {
			url: 'webSocketMessageSend.json',
			waitMsg: 'Sending...',
			success: () => {
				// Update grid
				const store = formPanel.getComponent('webSocketMessageGrid').store;
				store.insert(0, new store.recordType({userId, user, channel, message, date}));
			}
		}));
	}
});
