package com.clifton.websocket;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.websocket.broker.message.StompMessageAccessor;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.DefaultStompSession;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.messaging.simp.stomp.StompHeaders;
import org.springframework.messaging.simp.user.SimpSubscription;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.messaging.support.AbstractSubscribableChannel;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.messaging.SubProtocolWebSocketHandler;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The standard {@link WebSocketHandler} implementation.
 * <p>
 * This implementation should be used when WebSockets are enabled.
 *
 * @author MikeH
 */
public class WebSocketHandlerImpl implements WebSocketHandler {

	private AbstractSubscribableChannel clientOutboundChannel;
	private SecurityUserService securityUserService;
	private SimpUserRegistry userRegistry;
	private SimpMessagingTemplate brokerMessagingTemplate;
	private SubProtocolWebSocketHandler subProtocolWebSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////            Messaging Methods                               ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void sendMessage(String channel, Object payload) {
		getBrokerMessagingTemplate().convertAndSend(channel, payload);
	}


	@Override
	public void sendMessageToUser(SecurityUser user, String channel, Object payload) {
		getBrokerMessagingTemplate().convertAndSendToUser(user.getIntegrationPseudonym(), WebSocketUtils.trimUserChannel(channel), payload);
	}


	@Override
	public void sendMessageToUserList(List<SecurityUser> userList, String channel, Object payload) {
		CollectionUtils.asNonNullList(userList).forEach(user -> sendMessageToUser(user, channel, payload));
	}


	@Override
	public void sendErrorToUser(Message<?> message, Throwable error) {
		StompMessageAccessor accessor = StompMessageAccessor.of(message);
		Map<String, Object> responseMap = new HashMap<>();
		responseMap.put("content", error.getMessage());
		responseMap.put(StompMessageAccessor.CLIENT_MESSAGE_ID_HEADER, accessor.getClientMessageId());
		if (accessor.getDestination() != null) {
			responseMap.put(StompHeaders.DESTINATION, accessor.getDestination());
		}
		if (accessor.getSubscriptionId() != null) {
			responseMap.put(StompHeaders.SUBSCRIPTION, accessor.getSubscriptionId());
		}
		SecurityUser user = getSecurityUserService().getSecurityUserByPseudonym(accessor.getUsername());
		sendMessageToUser(user, CHANNEL_USER_TOPIC_ERROR, responseMap);
	}


	@Override
	public void sendReceiptFrame(Message<?> message) {
		StompMessageAccessor messageAccessor = StompMessageAccessor.of(message);
		StompHeaderAccessor receiptHeaderAccessor = StompHeaderAccessor.create(StompCommand.RECEIPT);
		receiptHeaderAccessor.setSessionId(messageAccessor.getSessionId());
		receiptHeaderAccessor.setReceiptId(messageAccessor.getReceipt());
		receiptHeaderAccessor.setNativeHeader(StompMessageAccessor.CLIENT_MESSAGE_ID_HEADER, messageAccessor.getClientMessageId());
		receiptHeaderAccessor.setImmutable();
		Message<?> receiptMessage = MessageBuilder.createMessage(DefaultStompSession.EMPTY_PAYLOAD, receiptHeaderAccessor.getMessageHeaders());
		getClientOutboundChannel().send(receiptMessage);
	}


	@Override
	public void sendErrorFrame(Message<?> message, String errorText) {
		// Use StompHeaderAccessor here as error may have occurred due to StompMessageAccessor instantiation error
		StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
		StompHeaderAccessor errorHeaderAccessor = StompHeaderAccessor.create(StompCommand.ERROR);
		errorHeaderAccessor.setSessionId(headerAccessor.getSessionId());
		errorHeaderAccessor.setReceiptId(headerAccessor.getReceipt());
		errorHeaderAccessor.setNativeHeader(StompMessageAccessor.CLIENT_MESSAGE_ID_HEADER, headerAccessor.getFirstNativeHeader(StompMessageAccessor.CLIENT_MESSAGE_ID_HEADER));
		errorHeaderAccessor.setMessage(errorText);
		errorHeaderAccessor.setImmutable();
		Message<?> errorMessage = MessageBuilder.createMessage(DefaultStompSession.EMPTY_PAYLOAD, headerAccessor.getMessageHeaders());
		getClientOutboundChannel().send(errorMessage);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Channel Listing Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<String> getActiveChannelList() {
		return getSubscriptionList().stream()
				.map(SimpSubscription::getDestination)
				.distinct()
				.collect(Collectors.toSet());
	}


	@Override
	public boolean isActiveChannel(String channel) {
		return !getChannelSubscriptionList(channel).isEmpty();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Subscription Management Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<SimpSubscription> getSubscriptionList() {
		return getUserRegistry().findSubscriptions(subscription -> true);
	}


	@Override
	public Collection<SimpSubscription> getChannelSubscriptionList(String channel) {
		return getUserRegistry().findSubscriptions(subscription -> channel.equals(subscription.getDestination()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            User Management Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<SimpUser> getConnectedSimpUserList() {
		return getUserRegistry().getUsers();
	}


	@Override
	public Collection<SecurityUser> getConnectedUserList() {
		return getConnectedSimpUserList().stream()
				.map(SimpUser::getName)
				.map(getSecurityUserService()::getSecurityUserByPseudonym)
				.collect(Collectors.toList());
	}


	@Override
	public Collection<SecurityUser> getSubscribedUserList(String channel) {
		return getChannelSubscriptionList(channel).stream()
				.map(subscription -> subscription.getSession().getUser().getName())
				.distinct()
				.map(getSecurityUserService()::getSecurityUserByPseudonym)
				.collect(Collectors.toSet());
	}


	@Override
	public Collection<SimpSubscription> getUserSubscriptionList(SecurityUser user) {
		SimpUser simpUser = getUserRegistry().getUser(user.getIntegrationPseudonym());
		ValidationUtils.assertNotNull(simpUser, () -> String.format("The user [%s] is not connected.", user));
		return simpUser.getSessions().stream()
				.flatMap(session -> session.getSubscriptions().stream())
				.collect(Collectors.toSet());
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Session Management Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<WebSocketSession> getWebSocketSessionList() {
		@SuppressWarnings("unchecked")
		Map<String, ?> sessions = (Map<String, ?>) new DirectFieldAccessor(getSubProtocolWebSocketHandler()).getPropertyValue("sessions");
		AssertUtils.assertNotNull(sessions, "Could not find sessions for DirectFieldAccessor");
		return sessions.values().stream()
				.map(sessionHolder -> (WebSocketSession) BeanUtils.getPropertyValue(sessionHolder, "session"))
				.collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractSubscribableChannel getClientOutboundChannel() {
		return this.clientOutboundChannel;
	}


	public void setClientOutboundChannel(AbstractSubscribableChannel clientOutboundChannel) {
		this.clientOutboundChannel = clientOutboundChannel;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SimpUserRegistry getUserRegistry() {
		return this.userRegistry;
	}


	public void setUserRegistry(SimpUserRegistry userRegistry) {
		this.userRegistry = userRegistry;
	}


	public SimpMessagingTemplate getBrokerMessagingTemplate() {
		return this.brokerMessagingTemplate;
	}


	public void setBrokerMessagingTemplate(SimpMessagingTemplate brokerMessagingTemplate) {
		this.brokerMessagingTemplate = brokerMessagingTemplate;
	}


	public SubProtocolWebSocketHandler getSubProtocolWebSocketHandler() {
		return this.subProtocolWebSocketHandler;
	}


	public void setSubProtocolWebSocketHandler(SubProtocolWebSocketHandler subProtocolWebSocketHandler) {
		this.subProtocolWebSocketHandler = subProtocolWebSocketHandler;
	}
}
