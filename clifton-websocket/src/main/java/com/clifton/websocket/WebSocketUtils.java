package com.clifton.websocket;

import java.util.regex.Pattern;


/**
 * The utility class for common WebSocket operations.
 *
 * @author MikeH
 */
public class WebSocketUtils {


	private static final Pattern USER_CHANNEL_PATTERN = Pattern.compile("^/user(?=/)");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WebSocketUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines whether the given channel name is a <i>user</i>-channel rather than a broadcast channel.
	 */
	public static boolean isUserChannel(String channel) {
		return USER_CHANNEL_PATTERN.matcher(channel).find();
	}


	/**
	 * Trims the given channel to normalize it to its base channel name.
	 */
	public static String trimUserChannel(String channel) {
		return USER_CHANNEL_PATTERN.matcher(channel).replaceFirst("");
	}
}
