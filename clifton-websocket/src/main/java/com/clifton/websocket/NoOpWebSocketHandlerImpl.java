package com.clifton.websocket;

import com.clifton.security.user.SecurityUser;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.user.SimpSubscription;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

import java.util.Collection;
import java.util.Collections;
import java.util.List;


/**
 * The simple no-op implementation of {@link WebSocketHandler}.
 * <p>
 * This implementation is used by default. If WebSockets are enabled and this functionality is desired, then this bean should be overridden appropriately.
 *
 * @author MikeH
 */
@Component("webSocketHandler")
public class NoOpWebSocketHandlerImpl implements WebSocketHandler {

	@Override
	public void sendMessage(String channel, Object payload) {
		// No op
	}


	@Override
	public void sendMessageToUser(SecurityUser user, String channel, Object payload) {
		// No op
	}


	@Override
	public void sendErrorToUser(Message<?> message, Throwable error) {
		// No op
	}


	@Override
	public void sendMessageToUserList(List<SecurityUser> userList, String channel, Object payload) {
		// No op
	}


	@Override
	public void sendReceiptFrame(Message<?> message) {
		// No op
	}


	@Override
	public void sendErrorFrame(Message<?> message, String errorText) {
		// No op
	}


	@Override
	public Collection<String> getActiveChannelList() {
		// No op
		return Collections.emptyList();
	}


	@Override
	public boolean isActiveChannel(String channel) {
		// No op
		return false;
	}


	@Override
	public Collection<SimpSubscription> getSubscriptionList() {
		// No op
		return Collections.emptyList();
	}


	@Override
	public Collection<SimpSubscription> getChannelSubscriptionList(String channel) {
		// No op
		return Collections.emptyList();
	}


	@Override
	public Collection<SimpUser> getConnectedSimpUserList() {
		// No op
		return Collections.emptyList();
	}


	@Override
	public Collection<SecurityUser> getConnectedUserList() {
		// No op
		return Collections.emptyList();
	}


	@Override
	public Collection<SecurityUser> getSubscribedUserList(String channel) {
		// No op
		return Collections.emptyList();
	}


	@Override
	public Collection<SimpSubscription> getUserSubscriptionList(SecurityUser user) {
		// No op
		return Collections.emptyList();
	}


	@Override
	public Collection<WebSocketSession> getWebSocketSessionList() {
		// No op
		return Collections.emptyList();
	}
}
