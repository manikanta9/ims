package com.clifton.websocket;

import com.clifton.security.user.SecurityUser;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.stomp.StompCommand;
import org.springframework.messaging.simp.user.SimpSubscription;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.messaging.StompSubProtocolErrorHandler;

import java.util.Collection;
import java.util.List;


/**
 * The handler for interfacing with WebSockets.
 * <p>
 * This handler includes methods for monitoring the current state of WebSocket connections, registered users, and active subscriptions. The handler also includes methods for
 * distributing messages to clients.
 *
 * @author MikeH
 */
public interface WebSocketHandler {

	/**
	 * The error channel. This channel is used to pass error messages to users.
	 */
	public String CHANNEL_USER_TOPIC_ERROR = "/user/topic/error";


	////////////////////////////////////////////////////////////////////////////
	////////            Messaging Methods                               ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Distributes the given payload on the given channel.
	 */
	void sendMessage(String channel, Object payload);


	/**
	 * Distributes the given payload to the specified user on the given channel.
	 * <p>
	 * For example, this method may be used to send a message to a client who has subscribed to channel "{@code /user<channel>}". The <tt>channel</tt> argument may or may not
	 * include the "{@code /user}" prefix. If present, the prefix will be removed from the channel before being passed to the internal {@link SimpMessagingTemplate} for processing.
	 * <p>
	 * This sends a message on the channel "{@code <channel>-user<STOMP session ID>}". These channels are managed application-side when clients attempt to subscribe to user
	 * channels by using destinations in the format "{@code /user<channel>}".
	 */
	void sendMessageToUser(SecurityUser user, String channel, Object payload);


	/**
	 * Distributes the given payload to the specified users on the given channel.
	 */
	void sendMessageToUserList(List<SecurityUser> userList, String channel, Object payload);


	/**
	 * Constructs an error message from the given exception and sends it to the triggering user for the given message.
	 *
	 * @param message the triggering message from the client
	 * @param error   the triggering error
	 */
	void sendErrorToUser(Message<?> message, Throwable error);


	/**
	 * Constructs a {@link StompCommand#RECEIPT receipt} frame for the given message from the client and sends it to the triggering user.
	 *
	 * @param message the triggering message from the client
	 */
	void sendReceiptFrame(Message<?> message);


	/**
	 * Constructs a {@link StompCommand#ERROR error} frame for the given message from the client and sends it to the triggering user.
	 * <p>
	 * This method is intended to be executed during custom message processing. Standard message processing should already have been completed and processed by the default
	 * implementation in {@link StompSubProtocolErrorHandler#handleClientMessageProcessingError(org.springframework.messaging.Message, java.lang.Throwable)
	 * StompSubProtocolErrorHandler#handleClientMessageProcessingError} before the message can be accessed.
	 * <p>
	 * This method is not intended to be executed when a recoverable error has occurred. If a client needs to be notified of a recoverable error, an error message should be
	 * constructed and sent to the user over the {@link #CHANNEL_USER_TOPIC_ERROR error channel}. See {@link #sendErrorToUser(Message, Throwable)} for details.
	 * <p>
	 * Error frames indicate that a message is unable to be processed. The STOMP specification indicates that error frames <i>must</i> sever the client connection.
	 * <p>
	 * See the <a href="https://stomp.github.io/stomp-specification-1.2.html#ERROR">STOMP specification</a> for more details.
	 *
	 * @param message   the triggering message from the client
	 * @param errorText the message to attach to the error frame
	 */
	void sendErrorFrame(Message<?> message, String errorText);


	////////////////////////////////////////////////////////////////////////////
	////////            Channel Listing Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of channels with at least one subscription.
	 */
	Collection<String> getActiveChannelList();


	/**
	 * Determines if the given channel has at least one subscription.
	 */
	boolean isActiveChannel(String channel);


	////////////////////////////////////////////////////////////////////////////
	////////            Subscription Management Methods                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets all current subscription objects.
	 */
	Collection<SimpSubscription> getSubscriptionList();


	/**
	 * Gets all subscriptions for the given channel.
	 */
	Collection<SimpSubscription> getChannelSubscriptionList(String channel);


	////////////////////////////////////////////////////////////////////////////
	////////            User Management Methods                         ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of names for all connected users.
	 */
	Collection<SimpUser> getConnectedSimpUserList();


	/**
	 * Gets the list of connected users.
	 */
	Collection<SecurityUser> getConnectedUserList();


	/**
	 * Gets the list of users who are subscribed to the given channel.
	 */
	Collection<SecurityUser> getSubscribedUserList(String channel);


	/**
	 * Gets the list of active subscriptions for the given user.
	 */
	Collection<SimpSubscription> getUserSubscriptionList(SecurityUser user);


	////////////////////////////////////////////////////////////////////////////
	////////            Session Management Methods                      ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of active WebSocket sessions.
	 */
	Collection<WebSocketSession> getWebSocketSessionList();
}
