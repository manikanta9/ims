package com.clifton.websocket.management;

import com.clifton.core.util.CollectionUtils;
import org.springframework.messaging.simp.user.SimpUser;


/**
 * A DTO representing metadata for a single connected STOMP user.
 * <p>
 * Metadata only includes relevant data which may be exposed to clients. Private information, such as associated session IDs, is not available.
 *
 * @author MikeH
 */
public class WebSocketUserStats {

	private final String userName;
	private final long numActiveSessions;
	private final long numActiveSubscriptions;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WebSocketUserStats(SimpUser user) {
		this.userName = user.getName();
		this.numActiveSessions = user.getSessions().size();
		this.numActiveSubscriptions = CollectionUtils.getStream(user.getSessions())
				.flatMap(session -> CollectionUtils.getStream(session.getSubscriptions()))
				.count();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUserName() {
		return this.userName;
	}


	public long getNumActiveSessions() {
		return this.numActiveSessions;
	}


	public long getNumActiveSubscriptions() {
		return this.numActiveSubscriptions;
	}
}
