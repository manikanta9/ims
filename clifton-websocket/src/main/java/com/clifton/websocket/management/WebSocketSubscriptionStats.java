package com.clifton.websocket.management;

import org.springframework.messaging.simp.user.SimpSubscription;


/**
 * A DTO representing metadata for a single subscription.
 * <p>
 * Metadata only includes relevant data which may be exposed to clients. Private information, such as associated session IDs, is not available.
 *
 * @author MikeH
 */
public class WebSocketSubscriptionStats {

	private final String destination;
	private final String userName;
	private final String subId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WebSocketSubscriptionStats(SimpSubscription subscription) {
		this.destination = subscription.getDestination();
		this.userName = subscription.getSession().getUser().getName();
		this.subId = subscription.getId();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDestination() {
		return this.destination;
	}


	public String getUserName() {
		return this.userName;
	}


	public String getSubId() {
		return this.subId;
	}
}
