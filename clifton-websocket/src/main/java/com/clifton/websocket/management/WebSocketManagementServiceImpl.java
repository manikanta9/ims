package com.clifton.websocket.management;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.runner.ExecutorServiceStatus;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.websocket.WebSocketHandler;
import com.clifton.websocket.WebSocketUtils;
import com.clifton.websocket.registry.WebSocketSubscription;
import org.springframework.beans.DirectFieldAccessor;
import org.springframework.messaging.simp.user.SimpSubscription;
import org.springframework.stereotype.Service;
import org.springframework.web.socket.WebSocketSession;

import java.security.Principal;
import java.util.Collection;
import java.util.List;
import java.util.Objects;


@Service
public class WebSocketManagementServiceImpl implements WebSocketManagementService {


	private SecurityUserService securityUserService;
	private WebSocketMessageBrokerStatsMonitor webSocketMessageBrokerStatsMonitor;
	private WebSocketHandler webSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public WebSocketStatus getWebSocketStatus() {
		WebSocketStatus status = new WebSocketStatus();
		attachStatusWebSocketSessionInfo(status);
		attachStatusWebSocketHandlerInfo(status);
		attachStatusExecutorInfo(status);
		attachStatusSubscriptionInfo(status);
		return status;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<SecurityUser> getWebSocketUserList() {
		return getWebSocketHandler().getConnectedUserList();
	}


	@Override
	public Collection<WebSocketSubscription> getWebSocketUserSubscriptionList(short userId, boolean userSpecificOnly) {
		SecurityUser user = getSecurityUserService().getSecurityUser(userId);
		ValidationUtils.assertNotNull(user, () -> String.format("No user was found for the given ID [%d].", userId));
		List<WebSocketSubscription> subscriptionList = CollectionUtils.getConverted(getWebSocketHandler().getUserSubscriptionList(user), WebSocketSubscription::new);
		if (userSpecificOnly) {
			subscriptionList = CollectionUtils.getFiltered(subscriptionList, subscription -> WebSocketUtils.isUserChannel(subscription.getDestination()));
		}
		return subscriptionList;
	}


	/*
	 * Note:
	 * Only administrators should have access to arbitrary message sending. ExtJS does not prevent against XSS through message execution, and we rely on loose JSON deserialization
	 * through ExtJS for message parsing. Thus, messages may be used to execute arbitrary code within the client's browser.
	 */
	@Override
	public void sendWebSocketMessage(Short userId, String channel, String message) {
		if (userId == null) {
			// Broadcast message
			getWebSocketHandler().sendMessage(channel, message);
		}
		else {
			// Send message to specific user
			SecurityUser user = getSecurityUserService().getSecurityUser(userId);
			ValidationUtils.assertNotNull(user, () -> String.format("No user was found for the given ID [%d].", userId));
			Collection<String> userChannels = CollectionUtils.getConverted(getWebSocketHandler().getUserSubscriptionList(user), SimpSubscription::getDestination);
			ValidationUtils.assertTrue(userChannels.contains(channel), () -> String.format("The given user [%s] is not subscribed to the given channel [%s].", user.getUserName(), channel));
			getWebSocketHandler().sendMessageToUser(user, channel, message);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void attachStatusWebSocketSessionInfo(WebSocketStatus status) {
		Collection<WebSocketSession> sessionCollection = getWebSocketHandler().getWebSocketSessionList();
		// Session information
		status.setNumWsSessions(CollectionUtils.getSize(sessionCollection));
		status.setNumUniqueUsers(CollectionUtils.getStream(sessionCollection)
				.map(WebSocketSession::getPrincipal)
				.filter(Objects::nonNull)
				.map(Principal::getName)
				.distinct()
				.mapToInt(val -> 1)
				.sum());
		status.setNumStompSessions(CollectionUtils.getStream(getWebSocketHandler().getConnectedSimpUserList())
				.flatMap(simpUser -> CollectionUtils.getStream(simpUser.getSessions()))
				.mapToInt(val -> 1)
				.sum());
	}


	private void attachStatusWebSocketHandlerInfo(WebSocketStatus status) {
		DirectFieldAccessor handlerAccessor = new DirectFieldAccessor(getWebSocketMessageBrokerStatsMonitor().getSubProtocolWebSocketHandler());
		// Session information
		Integer numTermLimitExceeded = (Integer) handlerAccessor.getPropertyValue("stats.limitExceeded.value");
		Integer numTermNoMessagesReceived = (Integer) handlerAccessor.getPropertyValue("stats.noMessagesReceived.value");
		AssertUtils.assertNotNull(numTermLimitExceeded, "Limit exceeded value was null");
		AssertUtils.assertNotNull(numTermNoMessagesReceived, "Messages received value was null");
		status.setNumWsSessionsTerminated(numTermLimitExceeded + numTermNoMessagesReceived);
		// Connection information
		status.setNumWsConnections((Integer) handlerAccessor.getPropertyValue("stats.webSocket.value"));
		status.setNumHttpStreamingConnections((Integer) handlerAccessor.getPropertyValue("stats.httpStreaming.value"));
		status.setNumHttpPollingConnections((Integer) handlerAccessor.getPropertyValue("stats.httpPolling.value"));
		status.setNumTransportErrors((Integer) handlerAccessor.getPropertyValue("stats.transportError.value"));
	}


	private void attachStatusExecutorInfo(WebSocketStatus status) {
		status.setExecutorServiceStatusList(CollectionUtils.createList(
				new ExecutorServiceStatus("SockJS Task Scheduler", getWebSocketMessageBrokerStatsMonitor().getSockJsTaskScheduler()),
				new ExecutorServiceStatus("Inbound Channel Executor", getWebSocketMessageBrokerStatsMonitor().getInboundChannelExecutor()),
				new ExecutorServiceStatus("Outbound Channel Executor", getWebSocketMessageBrokerStatsMonitor().getOutboundChannelExecutor())
		));
	}


	private void attachStatusSubscriptionInfo(WebSocketStatus status) {
		status.setUserStatsList(CollectionUtils.getConverted(getWebSocketHandler().getConnectedSimpUserList(), WebSocketUserStats::new));
		status.setSubscriptionStatsList(CollectionUtils.getConverted(getWebSocketHandler().getSubscriptionList(), WebSocketSubscriptionStats::new));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WebSocketMessageBrokerStatsMonitor getWebSocketMessageBrokerStatsMonitor() {
		return this.webSocketMessageBrokerStatsMonitor;
	}


	public void setWebSocketMessageBrokerStatsMonitor(WebSocketMessageBrokerStatsMonitor webSocketMessageBrokerStatsMonitor) {
		this.webSocketMessageBrokerStatsMonitor = webSocketMessageBrokerStatsMonitor;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}
}
