package com.clifton.websocket.management;

import com.clifton.core.util.runner.ExecutorServiceStatus;

import java.util.List;
import java.util.concurrent.ExecutorService;


/**
 * A snapshot of status information for connections to the system via WebSocket and <i>related protocols</i>. Related protocols include fall-back options through
 * <a href="https://github.com/sockjs/sockjs-client">SockJS</a>, such as XHR polling and XHR streaming.
 * <p>
 * This snapshot includes details for current connections, STOMP sessions, and STOMP subscriptions.
 *
 * @author MikeH
 */
public class WebSocketStatus {

	// Session summary
	/**
	 * The number of unique users connected via related protocols.
	 */
	private Integer numUniqueUsers;
	/**
	 * The total number of current connections for related protocols. This includes connections over any of the related protocols which have not performed a STOMP handshake with
	 * the system.
	 */
	private Integer numWsSessions;
	/**
	 * The total number of current STOMP sessions. A STOMP session is a session that has performed the appropriate STOMP handshake over a related protocol.
	 */
	private Integer numStompSessions;
	/**
	 * The number of connections for related protocols which have been terminated. Terminations are generally caused in one of two ways:
	 * <ul>
	 * <li>Timeout errors (heartbeats not received) from clients who appear to have unexpectedly disconnected from the host
	 * <li>Limit exceeded errors for connections which have exceeded some pre-determined limit, such as a maximum message buffer size or a message transmit time limit
	 * </ul>
	 */
	private Integer numWsSessionsTerminated;

	// Connection summary
	/**
	 * The current number of connections for related protocols which are WebSocket connections.
	 */
	private Integer numWsConnections;
	/**
	 * The current number of connections for related protocols which are HTTP streaming connections.
	 */
	private Integer numHttpStreamingConnections;
	/**
	 * The current number of connections for related protocols which are HTTP polling connections.
	 */
	private Integer numHttpPollingConnections;
	/**
	 * The total number of transport errors which have been seen.
	 */
	private Integer numTransportErrors;

	// Thread summary
	/**
	 * The list of status for related {@link ExecutorService} objects.
	 */
	private List<ExecutorServiceStatus> executorServiceStatusList;

	// Subscription summary
	/**
	 * The list of statistics for connected users.
	 */
	private List<WebSocketUserStats> userStatsList;
	/**
	 * The list of subscription statistics.
	 */
	private List<WebSocketSubscriptionStats> subscriptionStatsList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getNumUniqueUsers() {
		return this.numUniqueUsers;
	}


	public void setNumUniqueUsers(Integer numUniqueUsers) {
		this.numUniqueUsers = numUniqueUsers;
	}


	public Integer getNumWsSessions() {
		return this.numWsSessions;
	}


	public void setNumWsSessions(Integer numWsSessions) {
		this.numWsSessions = numWsSessions;
	}


	public Integer getNumStompSessions() {
		return this.numStompSessions;
	}


	public void setNumStompSessions(Integer numStompSessions) {
		this.numStompSessions = numStompSessions;
	}


	public Integer getNumWsSessionsTerminated() {
		return this.numWsSessionsTerminated;
	}


	public void setNumWsSessionsTerminated(Integer numWsSessionsTerminated) {
		this.numWsSessionsTerminated = numWsSessionsTerminated;
	}


	public Integer getNumWsConnections() {
		return this.numWsConnections;
	}


	public void setNumWsConnections(Integer numWsConnections) {
		this.numWsConnections = numWsConnections;
	}


	public Integer getNumHttpStreamingConnections() {
		return this.numHttpStreamingConnections;
	}


	public void setNumHttpStreamingConnections(Integer numHttpStreamingConnections) {
		this.numHttpStreamingConnections = numHttpStreamingConnections;
	}


	public Integer getNumHttpPollingConnections() {
		return this.numHttpPollingConnections;
	}


	public void setNumHttpPollingConnections(Integer numHttpPollingConnections) {
		this.numHttpPollingConnections = numHttpPollingConnections;
	}


	public Integer getNumTransportErrors() {
		return this.numTransportErrors;
	}


	public void setNumTransportErrors(Integer numTransportErrors) {
		this.numTransportErrors = numTransportErrors;
	}


	public List<ExecutorServiceStatus> getExecutorServiceStatusList() {
		return this.executorServiceStatusList;
	}


	public void setExecutorServiceStatusList(List<ExecutorServiceStatus> executorServiceStatusList) {
		this.executorServiceStatusList = executorServiceStatusList;
	}


	public List<WebSocketUserStats> getUserStatsList() {
		return this.userStatsList;
	}


	public void setUserStatsList(List<WebSocketUserStats> userStatsList) {
		this.userStatsList = userStatsList;
	}


	public List<WebSocketSubscriptionStats> getSubscriptionStatsList() {
		return this.subscriptionStatsList;
	}


	public void setSubscriptionStatsList(List<WebSocketSubscriptionStats> subscriptionStatsList) {
		this.subscriptionStatsList = subscriptionStatsList;
	}
}
