package com.clifton.websocket.management;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.security.user.SecurityUser;
import com.clifton.websocket.registry.WebSocketSubscription;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;


/**
 * The service for retrieving and managing WebSocket connection statistics.
 * <p>
 * This service provides endpoints for reporting and managing WebSocket connection and registry data. WebSocket registry data includes information about active STOMP sessions.
 * Information such as user connections, user subscriptions, and channel data may be retrieved.
 *
 * @author MikeH
 */
public interface WebSocketManagementService {

	/**
	 * Retrieves a snapshot of current WebSocket information within the system.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	WebSocketStatus getWebSocketStatus();


	/**
	 * Gets the list of connected WebSocket users.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	Collection<SecurityUser> getWebSocketUserList();


	/**
	 * Gets the list of subscription information for the given user.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	Collection<WebSocketSubscription> getWebSocketUserSubscriptionList(short userId, boolean userSpecificOnly);


	/**
	 * Sends a WebSocket message to the given user on the given channel.
	 */
	@RequestMapping("webSocketMessageSend")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	void sendWebSocketMessage(Short userId, String channel, String message);
}
