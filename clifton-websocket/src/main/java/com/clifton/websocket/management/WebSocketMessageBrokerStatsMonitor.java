package com.clifton.websocket.management;

import org.springframework.messaging.simp.stomp.StompBrokerRelayMessageHandler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.config.WebSocketMessageBrokerStats;
import org.springframework.web.socket.messaging.StompSubProtocolHandler;
import org.springframework.web.socket.messaging.SubProtocolHandler;
import org.springframework.web.socket.messaging.SubProtocolWebSocketHandler;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;


/**
 * The monitor for WebSocket message broker statistics. This monitor holds all necessary objects for reporting the current state of WebSocket connections and sessions.
 * <p>
 * This monitor is a near replication of {@link WebSocketMessageBrokerStats}. This monitor excludes the logging scheduler from the mentioned class.
 *
 * @author MikeH
 */
public class WebSocketMessageBrokerStatsMonitor {

	private SubProtocolWebSocketHandler subProtocolWebSocketHandler;
	private StompSubProtocolHandler stompSubProtocolHandler;
	private StompBrokerRelayMessageHandler stompBrokerRelay;
	private ThreadPoolExecutor inboundChannelExecutor;
	private ThreadPoolExecutor outboundChannelExecutor;
	private ScheduledThreadPoolExecutor sockJsTaskScheduler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setSubProtocolWebSocketHandler(SubProtocolWebSocketHandler subProtocolWebSocketHandler) {
		this.subProtocolWebSocketHandler = subProtocolWebSocketHandler;
		this.stompSubProtocolHandler = initStompSubProtocolHandler();
	}


	public void setInboundChannelExecutor(ThreadPoolTaskExecutor inboundChannelExecutor) {
		this.inboundChannelExecutor = inboundChannelExecutor.getThreadPoolExecutor();
	}


	public void setOutboundChannelExecutor(ThreadPoolTaskExecutor outboundChannelExecutor) {
		this.outboundChannelExecutor = outboundChannelExecutor.getThreadPoolExecutor();
	}


	public void setSockJsTaskScheduler(ThreadPoolTaskScheduler sockJsTaskScheduler) {
		this.sockJsTaskScheduler = sockJsTaskScheduler.getScheduledThreadPoolExecutor();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StompSubProtocolHandler initStompSubProtocolHandler() {
		// Find STOMP handler from protocol handlers
		for (SubProtocolHandler handler : this.subProtocolWebSocketHandler.getProtocolHandlers()) {
			if (handler instanceof StompSubProtocolHandler) {
				return (StompSubProtocolHandler) handler;
			}
		}
		// Fall-back to default protocol handler
		SubProtocolHandler defaultHandler = this.subProtocolWebSocketHandler.getDefaultProtocolHandler();
		if (defaultHandler instanceof StompSubProtocolHandler) {
			return (StompSubProtocolHandler) defaultHandler;
		}
		// No STOMP handler found
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SubProtocolWebSocketHandler getSubProtocolWebSocketHandler() {
		return this.subProtocolWebSocketHandler;
	}


	public StompSubProtocolHandler getStompSubProtocolHandler() {
		return this.stompSubProtocolHandler;
	}


	public void setStompSubProtocolHandler(StompSubProtocolHandler stompSubProtocolHandler) {
		this.stompSubProtocolHandler = stompSubProtocolHandler;
	}


	public StompBrokerRelayMessageHandler getStompBrokerRelay() {
		return this.stompBrokerRelay;
	}


	public void setStompBrokerRelay(StompBrokerRelayMessageHandler stompBrokerRelay) {
		this.stompBrokerRelay = stompBrokerRelay;
	}


	public ThreadPoolExecutor getInboundChannelExecutor() {
		return this.inboundChannelExecutor;
	}


	public void setInboundChannelExecutor(ThreadPoolExecutor inboundChannelExecutor) {
		this.inboundChannelExecutor = inboundChannelExecutor;
	}


	public ThreadPoolExecutor getOutboundChannelExecutor() {
		return this.outboundChannelExecutor;
	}


	public void setOutboundChannelExecutor(ThreadPoolExecutor outboundChannelExecutor) {
		this.outboundChannelExecutor = outboundChannelExecutor;
	}


	public ScheduledThreadPoolExecutor getSockJsTaskScheduler() {
		return this.sockJsTaskScheduler;
	}


	public void setSockJsTaskScheduler(ScheduledThreadPoolExecutor sockJsTaskScheduler) {
		this.sockJsTaskScheduler = sockJsTaskScheduler;
	}
}
