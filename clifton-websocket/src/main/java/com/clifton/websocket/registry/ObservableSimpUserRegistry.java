package com.clifton.websocket.registry;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.context.ApplicationListener;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.user.DestinationUserNameProvider;
import org.springframework.messaging.simp.user.SimpSession;
import org.springframework.messaging.simp.user.SimpSubscription;
import org.springframework.messaging.simp.user.SimpSubscriptionMatcher;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.messaging.support.MessageHeaderAccessor;
import org.springframework.util.Assert;
import org.springframework.web.socket.messaging.AbstractSubProtocolEvent;
import org.springframework.web.socket.messaging.DefaultSimpUserRegistry;
import org.springframework.web.socket.messaging.SessionConnectedEvent;
import org.springframework.web.socket.messaging.SessionDisconnectEvent;
import org.springframework.web.socket.messaging.SessionSubscribeEvent;
import org.springframework.web.socket.messaging.SessionUnsubscribeEvent;

import java.security.Principal;
import java.util.Collection;
import java.util.HashSet;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * An implementation of {@link SimpUserRegistry} that provides hooks for monitoring events that occur within the user registry.
 * <p>
 * Observable events include subscription and unsubscription events. Observers are autowired by type via the {@link SimpUserRegistryObserver} interface.
 * <p>
 * This is a modified version of {@link DefaultSimpUserRegistry} as of spring-websocket version 4.3.14.
 *
 * @author MikeH
 */
public class ObservableSimpUserRegistry implements SimpUserRegistry, ApplicationListener<AbstractSubProtocolEvent> {

	private final Map<String, LocalSimpUser> users = new ConcurrentHashMap<>();
	private final Map<String, LocalSimpSession> sessions = new ConcurrentHashMap<>();
	private final Object sessionLock = new Object();

	private final Collection<SimpUserRegistryObserver> registryObservers;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ObservableSimpUserRegistry(Collection<SimpUserRegistryObserver> registryObservers) {
		Objects.requireNonNull(registryObservers);
		this.registryObservers = registryObservers;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            ApplicationListener Methods                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onApplicationEvent(AbstractSubProtocolEvent event) {
		handleSubProtocolEvent(event);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            SimpUserRegistry Methods                        ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SimpUser getUser(String userName) {
		return this.users.get(userName);
	}


	@Override
	public Set<SimpUser> getUsers() {
		return new HashSet<>(this.users.values());
	}


	@Override
	public int getUserCount() {
		return this.users.size();
	}


	@Override
	public Set<SimpSubscription> findSubscriptions(SimpSubscriptionMatcher matcher) {
		Set<SimpSubscription> result = new HashSet<>();
		for (LocalSimpSession session : this.sessions.values()) {
			for (SimpSubscription subscription : session.subscriptions.values()) {
				if (matcher.match(subscription)) {
					result.add(subscription);
				}
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void onSubscribe(SimpSubscription subscription) {
		CollectionUtils.getStream(getRegistryObservers())
				.filter(observer -> observer.isMonitoredActivity(subscription))
				.forEach(observer -> observer.onSubscribe(subscription));
	}


	private void onUnsubscribe(SimpSubscription subscription) {
		CollectionUtils.getStream(getRegistryObservers())
				.filter(observer -> observer.isMonitoredActivity(subscription))
				.forEach(observer -> observer.onUnsubscribe(subscription));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Modified Methods                                ////////
	////////////////////////////////////////////////////////////////////////////


	private void handleSubProtocolEvent(AbstractSubProtocolEvent event) {
		Message<?> message = event.getMessage();
		SimpMessageHeaderAccessor accessor = MessageHeaderAccessor.getAccessor(message, SimpMessageHeaderAccessor.class);
		AssertUtils.assertNotNull(accessor, "MessageHeaderAccessor.getAccessor returned null");
		String sessionId = accessor.getSessionId();

		if (event instanceof SessionSubscribeEvent) {
			LocalSimpSession session = this.sessions.get(sessionId);
			if (session != null) {
				String id = accessor.getSubscriptionId();
				String destination = accessor.getDestination();
				LocalSimpSubscription subscription = session.addSubscription(id, destination);
				this.onSubscribe(subscription);
			}
		}
		else if (event instanceof SessionConnectedEvent) {
			Principal user = event.getUser();
			if (user == null) {
				return;
			}
			String name = user.getName();
			if (user instanceof DestinationUserNameProvider) {
				name = ((DestinationUserNameProvider) user).getDestinationUserName();
			}
			synchronized (this.sessionLock) {
				LocalSimpUser simpUser = this.users.get(name);
				if (simpUser == null) {
					simpUser = new LocalSimpUser(name);
					this.users.put(name, simpUser);
				}
				LocalSimpSession session = new LocalSimpSession(sessionId, simpUser);
				simpUser.addSession(session);
				this.sessions.put(sessionId, session);
			}
		}
		else if (event instanceof SessionDisconnectEvent) {
			synchronized (this.sessionLock) {
				LocalSimpSession session = this.sessions.remove(sessionId);
				if (session != null) {
					session.getSubscriptions().forEach(this::onUnsubscribe);
					LocalSimpUser user = session.getUser();
					user.removeSession(sessionId);
					if (!user.hasSessions()) {
						this.users.remove(user.getName());
					}
				}
			}
		}
		else if (event instanceof SessionUnsubscribeEvent) {
			LocalSimpSession session = this.sessions.get(sessionId);
			if (session != null) {
				String subscriptionId = accessor.getSubscriptionId();
				SimpSubscription subscription = session.removeSubscription(subscriptionId);
				if (subscription != null) {
					this.onUnsubscribe(subscription);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "users=" + this.users;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class LocalSimpUser implements SimpUser {

		private final String name;
		private final Map<String, SimpSession> userSessions = new ConcurrentHashMap<>(1);


		public LocalSimpUser(String userName) {
			Assert.notNull(userName, "User name must not be null");
			this.name = userName;
		}


		@Override
		public String getName() {
			return this.name;
		}


		@Override
		public boolean hasSessions() {
			return !this.userSessions.isEmpty();
		}


		@Override
		public SimpSession getSession(String sessionId) {
			return (sessionId != null ? this.userSessions.get(sessionId) : null);
		}


		@Override
		public Set<SimpSession> getSessions() {
			return new HashSet<>(this.userSessions.values());
		}


		void addSession(SimpSession session) {
			this.userSessions.put(session.getId(), session);
		}


		void removeSession(String sessionId) {
			this.userSessions.remove(sessionId);
		}


		@Override
		public boolean equals(Object other) {
			return (this == other ||
					(other instanceof SimpUser && this.name.equals(((SimpUser) other).getName())));
		}


		@Override
		public int hashCode() {
			return this.name.hashCode();
		}


		@Override
		public String toString() {
			return "name=" + this.name + ", sessions=" + this.userSessions;
		}
	}


	private static class LocalSimpSession implements SimpSession {

		private final String id;
		private final LocalSimpUser user;
		private final Map<String, SimpSubscription> subscriptions = new ConcurrentHashMap<>(4);


		public LocalSimpSession(String id, LocalSimpUser user) {
			Assert.notNull(id, "Id must not be null");
			Assert.notNull(user, "User must not be null");
			this.id = id;
			this.user = user;
		}


		@Override
		public String getId() {
			return this.id;
		}


		@Override
		public LocalSimpUser getUser() {
			return this.user;
		}


		@Override
		public Set<SimpSubscription> getSubscriptions() {
			return new HashSet<>(this.subscriptions.values());
		}


		LocalSimpSubscription addSubscription(String id, String destination) {
			LocalSimpSubscription subscription = new LocalSimpSubscription(id, destination, this);
			this.subscriptions.put(id, subscription);
			return subscription;
		}


		SimpSubscription removeSubscription(String id) {
			return this.subscriptions.remove(id);
		}


		@Override
		public boolean equals(Object other) {
			return (this == other ||
					(other instanceof SimpSubscription && this.id.equals(((SimpSubscription) other).getId())));
		}


		@Override
		public int hashCode() {
			return this.id.hashCode();
		}


		@Override
		public String toString() {
			return "id=" + this.id + ", subscriptions=" + this.subscriptions;
		}
	}


	private static class LocalSimpSubscription implements SimpSubscription {

		private final String id;
		private final LocalSimpSession session;
		private final String destination;


		public LocalSimpSubscription(String id, String destination, LocalSimpSession session) {
			Assert.notNull(id, "Id must not be null");
			Assert.hasText(destination, "Destination must not be empty");
			Assert.notNull(session, "Session must not be null");
			this.id = id;
			this.destination = destination;
			this.session = session;
		}


		@Override
		public String getId() {
			return this.id;
		}


		@Override
		public LocalSimpSession getSession() {
			return this.session;
		}


		@Override
		public String getDestination() {
			return this.destination;
		}


		@Override
		public boolean equals(Object other) {
			if (this == other) {
				return true;
			}
			if (!(other instanceof SimpSubscription)) {
				return false;
			}
			SimpSubscription otherSubscription = (SimpSubscription) other;
			return (this.id.equals(otherSubscription.getId()) &&
					getSession().getId().equals(otherSubscription.getSession().getId()));
		}


		@Override
		public int hashCode() {
			return this.id.hashCode() * 31 + getSession().getId().hashCode();
		}


		@Override
		public String toString() {
			return "destination=" + this.destination;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Collection<SimpUserRegistryObserver> getRegistryObservers() {
		return this.registryObservers;
	}
}
