package com.clifton.websocket.registry;

import org.springframework.messaging.simp.user.SimpSubscription;


/**
 * The interface for user registry observers to be registered in the {@link ObservableSimpUserRegistry} bean.
 * <p>
 * Implementors of this interface may be used to listen for subscribe and unsubscribe events within the user registry. For example, this may provide functionality to perform an
 * operation as soon as a subscription changes state from active to inactive or vice versa.
 *
 * @author MikeH
 */
public interface SimpUserRegistryObserver {

	/**
	 * Indicates whether the given event is a monitored activity.
	 *
	 * @param subscription the subscription being created or destroyed
	 * @return <tt>true</tt> if the event should be handled by this observer, or <tt>false</tt> otherwise
	 */
	boolean isMonitoredActivity(SimpSubscription subscription);


	/**
	 * Handles the given subscription being removed from the registry.
	 *
	 * @param subscription the subscription being destroyed
	 */
	default void onUnsubscribe(SimpSubscription subscription) {
	}


	/**
	 * Handles the given subscription being added to the registry.
	 *
	 * @param subscription the subscription being created
	 */
	default void onSubscribe(SimpSubscription subscription) {
	}
}
