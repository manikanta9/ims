package com.clifton.websocket.registry;

import org.springframework.messaging.simp.user.SimpSubscription;


/**
 * A container class for WebSocket subscription information.
 * <p>
 * This class wraps the information contained in a {@link SimpSubscription} object, removing sensitive information such as session IDs.
 *
 * @author MikeH
 */
public class WebSocketSubscription {

	private final String id;
	private final String user;
	private final String destination;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WebSocketSubscription(SimpSubscription subscription) {
		this.id = subscription.getId();
		this.user = subscription.getSession().getUser().getName();
		this.destination = subscription.getDestination();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getId() {
		return this.id;
	}


	public String getUser() {
		return this.user;
	}


	public String getDestination() {
		return this.destination;
	}
}
