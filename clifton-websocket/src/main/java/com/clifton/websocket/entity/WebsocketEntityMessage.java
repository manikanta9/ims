package com.clifton.websocket.entity;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.SimpleEntity;
import com.clifton.core.util.CollectionUtils;

import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;


/**
 * A Websocket message to indicate a CRUD operation has been performed on a specific entity.
 */
public class WebsocketEntityMessage<E extends SimpleEntity<? extends Number>> {

	public enum OPERATION {
		UPDATE,
		CREATE,
		DELETE,
		REFRESH
	}

	private final Map<String, Object> entityMap;
	private final OPERATION operation;
	private final String classname;


	private WebsocketEntityMessage(E entity, OPERATION operation) {
		this.operation = operation;
		this.classname = entity.getClass().getName();
		this.entityMap = generateCachedEntityMap(entity);
	}


	private WebsocketEntityMessage(Class<E> entityClass) {
		this.operation = OPERATION.REFRESH;
		this.classname = entityClass.getName();
		this.entityMap = Collections.emptyMap();
	}


	public static <E extends SimpleEntity<? extends Number>> Map<String, Object> generateCachedEntityMap(E entity) {
		if (entity != null) {
			Map<String, CachedEntityField> cachedPropertyMap = BeanUtils.getPropertyAnnotations(entity.getClass(), CachedEntityField.class);
			if (!CollectionUtils.isEmpty(cachedPropertyMap)) {
				Collection<String> fieldNames = new HashSet<>(cachedPropertyMap.keySet());
				fieldNames.add("id");
				return BeanUtils.getPropertyValueMap(entity, fieldNames);
			}
		}
		return Collections.emptyMap();
	}


	public static <E extends SimpleEntity<? extends Number>> WebsocketEntityMessage<E> updateMessage(E entity) {
		return new WebsocketEntityMessage<>(entity, OPERATION.UPDATE);
	}


	public static <E extends SimpleEntity<? extends Number>> WebsocketEntityMessage<E> createMessage(E entity) {
		return new WebsocketEntityMessage<>(entity, OPERATION.CREATE);
	}


	public static <E extends SimpleEntity<? extends Number>> WebsocketEntityMessage<E> deleteMessage(E entity) {
		return new WebsocketEntityMessage<>(entity, OPERATION.DELETE);
	}


	public static <E extends SimpleEntity<? extends Number>> WebsocketEntityMessage<E> refresh(Class<E> entityClass) {
		return new WebsocketEntityMessage<>(entityClass);
	}


	public Map<String, Object> getEntityMap() {
		return this.entityMap;
	}


	public OPERATION getOperation() {
		return this.operation;
	}


	public String getClassname() {
		return this.classname;
	}
}
