package com.clifton.websocket.alert;

import com.clifton.system.priority.SystemPriority;
import com.clifton.system.schema.SystemTable;


/**
 * The <code>SystemNotificationMessage</code> DTO class for sending notification message with subject, text, {@link SystemPriority} and {@link SystemTable}
 * with corresponding entityId.
 */
public class WebsocketAlertMessage {

	/**
	 * The WebSocket channel for immediate status alerts.
	 */
	public static final String CHANNEL_USER_TOPIC_IMMEDIATE_SYSTEM_TOAST_MESSAGES = "/user/topic/alert";

	private final String category;
	private final String subject;
	private final SystemPriority priority;
	private final SystemTable entityTable;
	private final Integer entityId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WebsocketAlertMessage(String category, String subject, SystemPriority priority, SystemTable entityTable, Integer entityId) {
		this.category = category;
		this.subject = subject;
		this.priority = priority;
		this.entityTable = entityTable;
		this.entityId = entityId;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCategory() {
		return this.category;
	}


	public String getSubject() {
		return this.subject;
	}


	public SystemPriority getPriority() {
		return this.priority;
	}


	public SystemTable getEntityTable() {
		return this.entityTable;
	}


	public Integer getEntityId() {
		return this.entityId;
	}
}

