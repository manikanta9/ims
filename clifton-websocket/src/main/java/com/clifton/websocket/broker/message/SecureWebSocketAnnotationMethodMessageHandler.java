package com.clifton.websocket.broker.message;

import com.clifton.core.util.AssertUtils;
import com.clifton.security.web.MethodSecurityHandler;
import com.clifton.websocket.WebSocketHandler;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.SubscribableChannel;
import org.springframework.messaging.handler.HandlerMethod;
import org.springframework.messaging.handler.annotation.support.AnnotationExceptionHandlerMethodResolver;
import org.springframework.messaging.simp.SimpMessageMappingInfo;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.web.socket.messaging.WebSocketAnnotationMethodMessageHandler;


/**
 * A customized implementation of {@link WebSocketAnnotationMethodMessageHandler} which enables customized authorization validation.
 * <p>
 * This implementation also applies additional customizations, such as restricting classes that will be scanned for eligible mapped methods and adding exception handling.
 *
 * @author MikeH
 */
public class SecureWebSocketAnnotationMethodMessageHandler extends WebSocketAnnotationMethodMessageHandler {

	private MethodSecurityHandler methodSecurityHandler;
	private WebSocketHandler webSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecureWebSocketAnnotationMethodMessageHandler(SubscribableChannel clientInChannel, MessageChannel clientOutChannel, SimpMessageSendingOperations brokerTemplate) {
		super(clientInChannel, clientOutChannel, brokerTemplate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		// Register generic message exception handler
		ExceptionHandlerMessagingAdvice exceptionAdviceBean = new ExceptionHandlerMessagingAdvice(getWebSocketHandler());
		AssertUtils.assertNotNull(exceptionAdviceBean.getBeanType(), "Bean type is null for ExceptionHandlerMessagingAdvice bean.");
		AnnotationExceptionHandlerMethodResolver resolver = new AnnotationExceptionHandlerMethodResolver(exceptionAdviceBean.getBeanType());
		registerExceptionHandlerAdvice(exceptionAdviceBean, resolver);
	}


	@Override
	protected void handleMessageInternal(Message<?> message, String lookupDestination) {
		super.handleMessageInternal(message, lookupDestination);

		// Support receipt requests, even if the request had no matched handler method
		StompMessageAccessor messageAccessor = getMessageAccessor(message);
		if (messageAccessor.getReceipt() != null) {
			getWebSocketHandler().sendReceiptFrame(message);
		}
	}


	@Override
	protected void handleMatch(SimpMessageMappingInfo mapping, HandlerMethod handlerMethod, String lookupDestination, Message<?> message) {
		StompMessageAccessor messageAccessor = getMessageAccessor(message);
		try {
			// Perform security validation before executing handler method
			getMethodSecurityHandler().verifySecurityAccess(handlerMethod.getMethod(), () -> lookupDestination, message.getHeaders());
			super.handleMatch(mapping, handlerMethod, lookupDestination, message);
		}
		catch (Exception e) {
			// Send message to user; this is primarily intended for authorization failures
			getWebSocketHandler().sendErrorToUser(message, e);

			// Add details for logged exceptions
			throw new RuntimeException(String.format("An error occurred while processing the request for [%s] from user [%s]: [%s]",
					lookupDestination, messageAccessor.getUsername(), e.getMessage()),
					e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StompMessageAccessor getMessageAccessor(Message<?> message) {
		try {
			return StompMessageAccessor.of(message);
		}
		catch (Exception e) {
			getWebSocketHandler().sendErrorFrame(message, "The STOMP message is malformed: " + e.getMessage());
			throw new RuntimeException("The STOMP message is malformed.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MethodSecurityHandler getMethodSecurityHandler() {
		return this.methodSecurityHandler;
	}


	public void setMethodSecurityHandler(MethodSecurityHandler methodSecurityHandler) {
		this.methodSecurityHandler = methodSecurityHandler;
	}


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}


	public void setWebSocketHandler(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = webSocketHandler;
	}
}
