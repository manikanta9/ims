package com.clifton.websocket.broker;

import com.clifton.core.context.AutowireByNameBean;
import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.util.date.DateFormatFull;
import com.clifton.websocket.broker.message.SecureWebSocketAnnotationMethodMessageHandler;
import com.clifton.websocket.config.WebSocketConfiguration;
import com.clifton.websocket.management.WebSocketMessageBrokerStatsMonitor;
import com.clifton.websocket.registry.ObservableSimpUserRegistry;
import com.clifton.websocket.registry.SimpUserRegistryObserver;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.monitorjbl.json.JsonViewModule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.simp.annotation.support.SimpAnnotationMethodMessageHandler;
import org.springframework.messaging.simp.broker.AbstractBrokerMessageHandler;
import org.springframework.messaging.simp.stomp.StompBrokerRelayMessageHandler;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.web.socket.config.WebSocketMessageBrokerStats;
import org.springframework.web.socket.config.annotation.DelegatingWebSocketMessageBrokerConfiguration;
import org.springframework.web.socket.messaging.SubProtocolWebSocketHandler;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


/**
 * The custom configuration class for the built-in WebSocket message broker.
 * <p>
 * This configuration class hooks into the message broker generation to customize initialization logic.
 * <p>
 * This class will only be enabled if explicitly imported.
 *
 * @author MikeH
 * @see WebSocketConfiguration
 */
@Configuration // This annotation must be present so that Spring can proxy this class with CGLIB to wrap @Bean methods for singleton functionality
@ExcludeFromComponentScan // Prevent executing this configuration unless explicitly imported
public class CliftonWebSocketMessageBrokerConfiguration extends DelegatingWebSocketMessageBrokerConfiguration {

	@Override
	@AutowireByNameBean
	public SimpAnnotationMethodMessageHandler simpAnnotationMethodMessageHandler() {
		// Override bean definition to configure autowire by name so that custom implementation is autowired
		return super.simpAnnotationMethodMessageHandler();
	}


	@Override
	protected SimpAnnotationMethodMessageHandler createAnnotationMethodMessageHandler() {
		return new SecureWebSocketAnnotationMethodMessageHandler(clientInboundChannel(), clientOutboundChannel(), brokerMessagingTemplate());
	}


	@Override
	public WebSocketMessageBrokerStats webSocketMessageBrokerStats() {
		// Disable the default message broker stats bean which logs current statistics at regular intervals
		return null;
	}


	@Override
	public SimpUserRegistry userRegistry() {
		// Disable the default user registry
		return null;
	}


	@Override
	protected MappingJackson2MessageConverter createJacksonConverter() {
		MappingJackson2MessageConverter converter = super.createJacksonConverter();
		// Add custom JsonView serialization
		converter.getObjectMapper().registerModule(new JsonViewModule());
		// Format dates
		converter.getObjectMapper().disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
		converter.getObjectMapper().setDateFormat(new DateFormatFull());
		return converter;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
	@Bean
	public SimpUserRegistry userRegistry(Optional<List<SimpUserRegistryObserver>> registryObserverList) {
		// Use customized user registry
		return new ObservableSimpUserRegistry(registryObserverList.orElseGet(ArrayList::new));
	}


	@Bean
	public WebSocketMessageBrokerStatsMonitor webSocketMessageBrokerStatsMonitor() {
		// This is adapted from the original webSocketMessageBrokerStats bean creation method in the parent class
		AbstractBrokerMessageHandler relayBean = stompBrokerRelayMessageHandler();
		StompBrokerRelayMessageHandler brokerRelay = (relayBean instanceof StompBrokerRelayMessageHandler ?
				(StompBrokerRelayMessageHandler) relayBean : null);

		WebSocketMessageBrokerStatsMonitor stats = new WebSocketMessageBrokerStatsMonitor();
		stats.setSubProtocolWebSocketHandler((SubProtocolWebSocketHandler) subProtocolWebSocketHandler());
		stats.setStompBrokerRelay(brokerRelay);
		stats.setInboundChannelExecutor((ThreadPoolTaskExecutor) clientInboundChannelExecutor());
		stats.setOutboundChannelExecutor((ThreadPoolTaskExecutor) clientOutboundChannelExecutor());
		stats.setSockJsTaskScheduler((ThreadPoolTaskScheduler) messageBrokerTaskScheduler());
		return stats;
	}


	@AutowireByNameBean
	public ChannelInterceptor cliftonSecurityContextChannelInterceptor() {
		return new CliftonSecurityContextChannelInterceptor();
	}
}
