package com.clifton.websocket.broker;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.util.ArrayUtils;
import com.clifton.websocket.config.WebSocketConfigurationProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.simp.SimpMessageType;
import org.springframework.messaging.simp.config.ChannelRegistration;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.security.config.annotation.web.messaging.MessageSecurityMetadataSourceRegistry;
import org.springframework.security.config.annotation.web.socket.AbstractSecurityWebSocketMessageBrokerConfigurer;
import org.springframework.web.socket.config.annotation.DelegatingWebSocketMessageBrokerConfiguration;


/**
 * The configurer for WebSocket message security. This configurer is used to configure security rules for WebSocket messages into and out of the built-in WebSocket message broker.
 * <p>
 * This configurer is injected into {@link DelegatingWebSocketMessageBrokerConfiguration} on context initialization.
 *
 * @author MikeH
 */
@Configuration
@ExcludeFromComponentScan
public class WebSocketSecurityConfigurer extends AbstractSecurityWebSocketMessageBrokerConfigurer {

	private final ChannelInterceptor cliftonSecurityContextChannelInterceptor;
	private final WebSocketConfigurationProperties webSocketConfigurationProperties;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Autowired
	public WebSocketSecurityConfigurer(ChannelInterceptor cliftonSecurityContextChannelInterceptor, WebSocketConfigurationProperties webSocketConfigurationProperties) {
		this.cliftonSecurityContextChannelInterceptor = cliftonSecurityContextChannelInterceptor;
		this.webSocketConfigurationProperties = webSocketConfigurationProperties;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean sameOriginDisabled() {
		// Disable CSRF protection
		return true;
	}


	@Override
	protected void configureInbound(MessageSecurityMetadataSourceRegistry messages) {
		/*
		 * This method configures the inbound security rules for WebSocket messages through the built-in WebSocket message broker. See
		 * org.springframework.messaging.simp.SimpMessageType for possible message types.
		 */
		// Authorize requests with no destination (e.g., CONNECT, DISCONNECT, and HEARTBEAT)
		messages.nullDestMatcher().fullyAuthenticated();
		/*
		 * Deny all requests to user-specific channels. User-specific channels are initiated by subscribing to "/user/..."). The internal user registry maps these subscriptions to
		 * "/user/...-user<STOMP session ID>". These channels are not naturally protected, meaning that other users may subscribe to them and send messages to them. This rule
		 * disallows subscriptions and messages from clients directly to these channels so that only the application may interacted with these channels.
		 */
		messages.simpDestMatchers("/**/*-user*").denyAll();
		// Allow configured MESSAGE frame destinations
		messages.simpMessageDestMatchers(getMessageDestinationPatterns()).fullyAuthenticated();
		// Allow configured SUBSCRIBE frame destinations
		messages.simpSubscribeDestMatchers(getSubscriptionDestinationPatterns()).fullyAuthenticated();
		// Deny all other SUBSCRIBE and MESSAGE frames
		messages.simpTypeMatchers(SimpMessageType.MESSAGE, SimpMessageType.SUBSCRIBE).denyAll();
		// Catch-all deny rule for any remaining frames
		messages.anyMessage().denyAll();
	}


	@Override
	protected void customizeClientInboundChannel(ChannelRegistration registration) {
		registration.interceptors(getCliftonSecurityContextChannelInterceptor());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String[] getMessageDestinationPatterns() {
		return ArrayUtils.getStream(getWebSocketConfigurationProperties().getStompMessagePrefixes())
				.map(s -> s + "/**")
				.toArray(String[]::new);
	}


	private String[] getSubscriptionDestinationPatterns() {
		return ArrayUtils.getStream(getWebSocketConfigurationProperties().getStompSubscriptionPrefixes())
				.flatMap(s -> ArrayUtils.getStream(s + "/**", "/user" + s + "/**"))
				.toArray(String[]::new);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ChannelInterceptor getCliftonSecurityContextChannelInterceptor() {
		return this.cliftonSecurityContextChannelInterceptor;
	}


	public WebSocketConfigurationProperties getWebSocketConfigurationProperties() {
		return this.webSocketConfigurationProperties;
	}
}
