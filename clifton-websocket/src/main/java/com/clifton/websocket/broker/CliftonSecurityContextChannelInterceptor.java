package com.clifton.websocket.broker;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.AssertUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.MessageHandler;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.ExecutorChannelInterceptor;
import org.springframework.security.messaging.context.SecurityContextChannelInterceptor;

import java.security.Principal;
import java.util.ArrayDeque;
import java.util.Deque;
import java.util.Optional;


/**
 * The {@code CliftonSecurityContextChannelInterceptor} channel interceptor attaches the current user to the thread context when handling messages.
 * <p>
 * This interceptor allows other methods to retrieve the current user via the {@link ContextHandler}. For example, this interceptor allows the current user to be retrieved via
 * {@link SecurityUserService#getSecurityUserCurrent()}, which uses the context to locate the user.
 * <p>
 * Since nested messages may be processed by the same thread, a stack is used to roll/un-roll the context user according to the frame currently being processed.
 * <p>
 * This class is modeled after {@link SecurityContextChannelInterceptor} which performs similar context management for {@link Principal} entities as used by Spring Security.
 *
 * @author MikeH
 */
public class CliftonSecurityContextChannelInterceptor implements ChannelInterceptor, ExecutorChannelInterceptor {

	private static final ThreadLocal<Deque<Optional<SecurityUser>>> ORIGINAL_USER = ThreadLocal.withInitial(ArrayDeque::new);
	private ContextHandler contextHandler;
	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message<?> preSend(Message<?> message, MessageChannel channel) {
		setup(message);
		return message;
	}


	@Override
	public void afterSendCompletion(Message<?> message, MessageChannel channel, boolean sent, Exception ex) {
		cleanup();
	}


	@Override
	public Message<?> beforeHandle(Message<?> message, MessageChannel channel, MessageHandler handler) {
		// This is executed before the message itself is handled
		setup(message);
		return message;
	}


	@Override
	public void afterMessageHandled(Message<?> message, MessageChannel channel, MessageHandler handler, Exception ex) {
		cleanup();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void setup(Message<?> message) {
		// Populate stack with old user
		Deque<Optional<SecurityUser>> userStack = ORIGINAL_USER.get();
		SecurityUser oldUser = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		userStack.push(Optional.ofNullable(oldUser));

		// Set new user in context
		Principal principal = SimpMessageHeaderAccessor.getUser(message.getHeaders());
		AssertUtils.assertNotNull(principal, "No principal was attached to the message.");
		SecurityUser user = getSecurityUserService().getSecurityUserByPseudonym(principal.getName());
		AssertUtils.assertNotNull(user, "No user was found for the message with principal [%s].", principal.getName());
		getContextHandler().setBean(Context.USER_BEAN_NAME, user);
	}


	private void cleanup() {
		// Retrieve old user from context
		Deque<Optional<SecurityUser>> userStack = ORIGINAL_USER.get();
		Optional<SecurityUser> oldUserOpt = userStack.pop();
		if (userStack.isEmpty()) {
			ORIGINAL_USER.remove();
		}
		if (oldUserOpt.isPresent()) {
			getContextHandler().setBean(Context.USER_BEAN_NAME, oldUserOpt.get());
		}
		else {
			getContextHandler().removeBean(Context.USER_BEAN_NAME);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
