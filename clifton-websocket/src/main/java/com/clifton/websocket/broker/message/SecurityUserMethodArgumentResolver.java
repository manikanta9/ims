package com.clifton.websocket.broker.message;

import com.clifton.core.util.AssertUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.core.MethodParameter;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver;

import java.util.Objects;


/**
 * A {@link HandlerMethodArgumentResolver} implementation for resolving message handler arguments of type {@link SecurityUser}.
 * <p>
 * This argument resolver will provide the current {@link SecurityUserService#getSecurityUserCurrent() user} for all parameters in message handlers of type {@link SecurityUser}.
 *
 * @author MikeH
 */
public class SecurityUserMethodArgumentResolver implements HandlerMethodArgumentResolver {

	private final SecurityUserService securityUserService;


	public SecurityUserMethodArgumentResolver(SecurityUserService securityUserService) {
		Objects.requireNonNull(securityUserService, "securityUserService must not be null");
		this.securityUserService = securityUserService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean supportsParameter(MethodParameter parameter) {
		Class<?> paramType = parameter.getParameterType();
		return SecurityUser.class.isAssignableFrom(paramType);
	}


	@Override
	public Object resolveArgument(MethodParameter parameter, Message<?> message) {
		SecurityUser user = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(user, "No valid security user was found while resolving arguments for the message handler: [%s].", parameter.toString());
		return user;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}
}
