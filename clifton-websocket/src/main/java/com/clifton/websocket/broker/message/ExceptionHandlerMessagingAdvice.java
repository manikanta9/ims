package com.clifton.websocket.broker.message;

import com.clifton.core.logging.LogUtils;
import com.clifton.websocket.WebSocketHandler;
import org.springframework.core.Ordered;
import org.springframework.messaging.Message;
import org.springframework.messaging.handler.HandlerMethod;
import org.springframework.messaging.handler.MessagingAdviceBean;
import org.springframework.messaging.handler.annotation.MessageExceptionHandler;

import java.util.Objects;


/**
 * The {@link MessagingAdviceBean} for handling message exceptions. This is intended to handle errors for WebSocket messages from authenticated clients only.
 * <p>
 * When an exception is triggered during message processing, this class constructs an exception message with relevant information and sends it to the {@link
 * WebSocketHandler#CHANNEL_USER_TOPIC_ERROR error channel}.
 *
 * @author MikeH
 */
public class ExceptionHandlerMessagingAdvice implements MessagingAdviceBean {

	private final WebSocketHandler webSocketHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExceptionHandlerMessagingAdvice(WebSocketHandler webSocketHandler) {
		this.webSocketHandler = Objects.requireNonNull(webSocketHandler, "webSocketHandler must not be null");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<?> getBeanType() {
		return ExceptionHandlerMessagingAdvice.class;
	}


	@Override
	public Object resolveBean() {
		return this;
	}


	@Override
	public boolean isApplicableToBeanType(Class<?> beanType) {
		return true;
	}


	@Override
	public int getOrder() {
		// Allow all other exception handlers to precede this
		return Ordered.LOWEST_PRECEDENCE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@MessageExceptionHandler
	public void handleMessageException(Message<?> msg, Exception e, HandlerMethod method) {
		LogUtils.error(this.getClass(), String.format("An error occurred while processing the given WebSocket message: %s", msg), e);
		getWebSocketHandler().sendErrorToUser(msg, e);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public WebSocketHandler getWebSocketHandler() {
		return this.webSocketHandler;
	}
}
