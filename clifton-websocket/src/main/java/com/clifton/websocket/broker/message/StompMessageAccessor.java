package com.clifton.websocket.broker.message;

import com.clifton.core.util.AssertUtils;
import org.springframework.messaging.Message;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.stomp.StompHeaderAccessor;
import org.springframework.util.MimeType;

import java.util.Objects;


/**
 * A message accessor for reading properties from STOMP messages. This accessor can be used to access common properties derived from the message.
 * <p>
 * This is a custom wrapper around {@link SimpMessageHeaderAccessor}.
 * <p>
 * A {@link NullPointerException} will be thrown on instantiation if the message is malformed according to system conventions.
 *
 * @author MikeH
 */
public class StompMessageAccessor {

	/**
	 * The custom message header for STOMP messages to uniquely identify a client message. This header must be present on all frames from the client and may be present on a frame
	 * sent to the client in order for the client to indicate a matching source frame.
	 */
	public static final String CLIENT_MESSAGE_ID_HEADER = "client-message-id";

	// Required properties
	private final String username;
	private final String sessionId;
	private final String clientMessageId;

	// Nullable properties
	private final String destination;
	private final String subscriptionId;
	/**
	 * The receipt designator for the message. If present, this indicates that a receipt is requested for this message. See the
	 * <a href="https://stomp.github.io/stomp-specification-1.2.html#RECEIPT">RECEIPT documentation</a> for more details.
	 */
	private final String receipt;
	private final MimeType contentType;
	private final Object payload;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StompMessageAccessor(String username, String sessionId, String clientMessageId, String destination, String subscriptionId, String receipt, MimeType contentType, Object payload) {
		// Fail early on non-null properties
		Objects.requireNonNull(username, "the username must not be null");
		Objects.requireNonNull(sessionId, "the session ID must not be null");
		Objects.requireNonNull(clientMessageId, "the client message ID must not be null");

		this.username = username;
		this.sessionId = sessionId;
		this.clientMessageId = clientMessageId;
		this.destination = destination;
		this.subscriptionId = subscriptionId;
		this.receipt = receipt;
		this.contentType = contentType;
		this.payload = payload;
	}


	public static StompMessageAccessor of(Message<?> message) {
		StompHeaderAccessor headerAccessor = StompHeaderAccessor.wrap(message);
		AssertUtils.assertNotNull(headerAccessor.getUser(), "Cannot get username for null user in StompMessageAccessor");
		String username = headerAccessor.getUser().getName();
		String sessionId = headerAccessor.getSessionId();
		String clientMessageId = headerAccessor.getFirstNativeHeader(CLIENT_MESSAGE_ID_HEADER);
		String destination = headerAccessor.getDestination();
		String subscriptionId = headerAccessor.getSubscriptionId();
		String receipt = headerAccessor.getReceipt();
		MimeType contentType = headerAccessor.getContentType();
		Object payload = message.getPayload();
		return new StompMessageAccessor(username, sessionId, clientMessageId, destination, subscriptionId, receipt, contentType, payload);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		// Exclude payload to reduce log pollution
		return "StompMessageAccessor{" +
				"username='" + this.username + '\'' +
				", sessionId='" + this.sessionId + '\'' +
				", clientMessageId='" + this.clientMessageId + '\'' +
				", destination='" + this.destination + '\'' +
				", subscriptionId='" + this.subscriptionId + '\'' +
				", receipt='" + this.receipt + '\'' +
				", contentType=" + this.contentType +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUsername() {
		return this.username;
	}


	public String getSessionId() {
		return this.sessionId;
	}


	public String getClientMessageId() {
		return this.clientMessageId;
	}


	public String getDestination() {
		return this.destination;
	}


	public String getSubscriptionId() {
		return this.subscriptionId;
	}


	public String getReceipt() {
		return this.receipt;
	}


	public MimeType getContentType() {
		return this.contentType;
	}


	public Object getPayload() {
		return this.payload;
	}
}
