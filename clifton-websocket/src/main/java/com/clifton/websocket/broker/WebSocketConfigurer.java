package com.clifton.websocket.broker;

import com.clifton.security.user.SecurityUserService;
import com.clifton.websocket.broker.message.SecurityUserMethodArgumentResolver;
import com.clifton.websocket.config.WebSocketConfigurationProperties;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.handler.invocation.HandlerMethodArgumentResolver;
import org.springframework.messaging.simp.SimpMessageHeaderAccessor;
import org.springframework.messaging.simp.config.MessageBrokerRegistry;
import org.springframework.messaging.support.ChannelInterceptor;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.socket.config.annotation.DelegatingWebSocketMessageBrokerConfiguration;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.config.annotation.WebSocketMessageBrokerConfigurer;

import java.util.List;
import java.util.regex.Pattern;


/**
 * The standard WebSocket message broker configurer. This bean is used to configure standard broker properties, such as STOMP endpoints to listen to and available destinations for
 * the built-in message broker.
 * <p>
 * This bean is autowired into the {@link DelegatingWebSocketMessageBrokerConfiguration} to customize broker properties.
 *
 * @author MikeH
 */
public class WebSocketConfigurer implements WebSocketMessageBrokerConfigurer {

	private static final String USER_DESTINATION_PREFIX = "/user/";

	private SecurityUserService securityUserService;
	private WebSocketConfigurationProperties webSocketConfigurationProperties;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void registerStompEndpoints(StompEndpointRegistry registry) {
		/*
		 * Enable the STOMP endpoint for STOMP message translation. This also enables SockJS client/server connection negotiation at this endpoint.
		 */
		registry.addEndpoint(getWebSocketConfigurationProperties().getStompEndpoints())
				.withSockJS()
				// Set the client library URL for generated SockJS iframes for compatibility with old browsers
				.setClientLibraryUrl("../../webjars/sockjs-client/1.1.4/dist/sockjs.min.js");
	}


	@Override
	public void configureMessageBroker(MessageBrokerRegistry registry) {
		// Enable a subscribable broker to handle subscription prefixes
		registry.enableSimpleBroker(getWebSocketConfigurationProperties().getStompSubscriptionPrefixes());

		// Normalize user destinations to prevent channel configuration bloat (e.g., @SendToUser("/user/topic/my-channel") -> @SendToUser("/topic/my-channel"))
		registry.setUserDestinationPrefix(USER_DESTINATION_PREFIX);
		registry.configureBrokerChannel().interceptors(new ChannelInterceptor() {
			@Override
			public Message<?> preSend(Message<?> message, MessageChannel channel) {
				SimpMessageHeaderAccessor headerAccessor = SimpMessageHeaderAccessor.wrap(message);
				String destination = headerAccessor.getDestination();
				if (destination != null && destination.startsWith(USER_DESTINATION_PREFIX)) {
					String publicDestination = destination.replaceAll(Pattern.quote(USER_DESTINATION_PREFIX), "/");
					String userDestination = (USER_DESTINATION_PREFIX + publicDestination).replaceAll("//+", "/");
					headerAccessor.setDestination(userDestination);
				}
				return MessageBuilder.createMessage(message.getPayload(), headerAccessor.getMessageHeaders());
			}
		});
	}


	@Override
	public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
		// Add argument resolver to automatically populate SecurityUser parameters
		argumentResolvers.add(new SecurityUserMethodArgumentResolver(getSecurityUserService()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public WebSocketConfigurationProperties getWebSocketConfigurationProperties() {
		return this.webSocketConfigurationProperties;
	}


	public void setWebSocketConfigurationProperties(WebSocketConfigurationProperties webSocketConfigurationProperties) {
		this.webSocketConfigurationProperties = webSocketConfigurationProperties;
	}
}
