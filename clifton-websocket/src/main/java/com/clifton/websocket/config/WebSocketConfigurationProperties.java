package com.clifton.websocket.config;

import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.messaging.simp.SimpMessageType;

import java.util.Arrays;


/**
 * The configuration properties class for WebSockets. This class defines the properties which will be used when initializing the WebSockets feature.
 * <p>
 * When defined, beans of this type are instantiated in {@link WebSocketConfigurationRegistryPostProcessor}. This violates the constraint that beans are not generated before the
 * {@link BeanFactoryPostProcessor} phase is completed. This means that this bean should be a simple POJO and should have no autowired properties of its own. When defining this
 * bean, bean references should be avoided and autowiring should be explicitly disabled for the bean if possible. See {@code ImsBeanFactoryLifecycleTests} for more information.
 *
 * @author MikeH
 */
public class WebSocketConfigurationProperties {

	/**
	 * If {@code true}, then WebSockets will be enabled. Otherwise, WebSockets will be disabled.
	 */
	private final boolean enabled;
	/**
	 * The servlet mapping path for the WebSocket servlet.
	 */
	private final String servletPath;
	/**
	 * The list of endpoints which will be translated and handled by the STOMP message broker.
	 */
	private final String[] stompEndpoints;
	/**
	 * Ths list of channels which may receive messages of type {@link SimpMessageType#SUBSCRIBE SUBSCRIBE}.
	 * <p>
	 * This is also the list of channels on which the system will send out messages to clients. This is effectively the list of permitted channels used by the bundled message
	 * broker. Any attempt to broadcast messages to other channels, including both application-side attempts (via code, for example) and client-side attempts (via STOMP messages
	 * sent to the host) will be silently rejected.
	 */
	private final String[] stompSubscriptionPrefixes;
	/**
	 * The list of channels which may receive messages of type {@link SimpMessageType#MESSAGE MESSAGE}.
	 * <p>
	 * Messages of type {@link SimpMessageType#MESSAGE MESSAGE} are generally used for sending data from the client to the server without initiating a new subscription.
	 */
	private final String[] stompMessagePrefixes;


	public WebSocketConfigurationProperties(boolean enabled, String servletPath, String[] stompEndpoints, String[] stompSubscriptionPrefixes, String[] stompMessagePrefixes) {
		this.enabled = enabled;
		this.servletPath = servletPath;
		this.stompEndpoints = stompEndpoints;
		this.stompSubscriptionPrefixes = stompSubscriptionPrefixes;
		this.stompMessagePrefixes = stompMessagePrefixes;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "WebSocketConfigurationProperties{" +
				"enabled=" + this.enabled +
				", servletPath='" + this.servletPath + '\'' +
				", stompEndpoints=" + Arrays.toString(this.stompEndpoints) +
				", stompSubscriptionPrefixes=" + Arrays.toString(this.stompSubscriptionPrefixes) +
				", stompMessagePrefixes=" + Arrays.toString(this.stompMessagePrefixes) +
				'}';
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isEnabled() {
		return this.enabled;
	}


	public String getServletPath() {
		return this.servletPath;
	}


	public String[] getStompEndpoints() {
		return this.stompEndpoints;
	}


	public String[] getStompSubscriptionPrefixes() {
		return this.stompSubscriptionPrefixes;
	}


	public String[] getStompMessagePrefixes() {
		return this.stompMessagePrefixes;
	}
}
