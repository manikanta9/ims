package com.clifton.websocket.config;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.spring.SpringApplicationContextUtils;
import com.clifton.core.logging.LogUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.core.PriorityOrdered;


/**
 * The {@link BeanDefinitionRegistryPostProcessor} responsible for setting up the active {@link BeanFactory} for the current WebSockets configuration.
 * <p>
 * This post processor will only act if all of the following constraints are passed:
 * <ul>
 * <li>The {@link WebSocketConfigurationProperties} is registered under the convention-based bean ID
 * <li>The properties bean has its {@code enabled} field set to {@code true}
 * </ul>
 * <p>
 * If the constraints are passed, then this will register the {@link WebSocketConfiguration} configuration class, which defines the beans which initialize the WebSocket feature.
 * <p>
 * The phase in which this post processor is executed is critical. Because this post processor generates a {@link Configuration} bean definition, it must be executed before the
 * {@link ConfigurationClassPostProcessor} post processor.
 *
 * @author MikeH
 */
public class WebSocketConfigurationRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor, PriorityOrdered {

	@Override
	public int getOrder() {
		/*
		 * This must be executed prior to the execution of the org.springframework.context.annotation.ConfigurationClassPostProcessor BeanDefinitionRegistryPostProcessor
		 * (PriorityOrdered at LOWEST_PRECEDENCE), which detects bean definitions generated from @Configuration classes and proxies @Configuration classes via CGLIB enhancement. We
		 * preempt this so that we can conditionally add our own @Configuration bean definition.
		 */
		return 0;
	}


	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		WebSocketConfigurationProperties webSocketProperties = getWebSocketProperties((ConfigurableListableBeanFactory) registry);
		if (webSocketProperties != null && webSocketProperties.isEnabled()) {
			// WebSockets are enabled
			LogUtils.info(this.getClass(), "WebSockets enabled with configuration: " + webSocketProperties);
			SpringApplicationContextUtils.registerBeanDefinition(registry, WebSocketConfiguration.class);
		}
		else {
			// WebSockets are disabled
			LogUtils.info(this.getClass(), "WebSockets disabled. The WebSockets properties bean has its \"enabled\" flag set to \"false\".");
		}
	}


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// Do nothing -- all actions were taken during the bean definition registry post-processing phase
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the {@link WebSocketConfigurationProperties} bean from the given bean factory, or {@code null} if the bean does not exist.
	 * <p>
	 * This method instantiates the properties bean, which violates the bean factory/registry post-processor constraint that beans are not instantiated. See {@link
	 * WebSocketConfigurationProperties} for more details.
	 */
	private WebSocketConfigurationProperties getWebSocketProperties(ConfigurableListableBeanFactory beanFactory) {
		String propertiesBeanName = ContextConventionUtils.getBeanNameFromClassName(WebSocketConfigurationProperties.class.getName());
		if (!beanFactory.containsBeanDefinition(propertiesBeanName)) {
			return null;
		}
		BeanDefinition configurationDefinition = beanFactory.getBeanDefinition(propertiesBeanName);

		// Normalize definition and retrieve prototype instance
		((AbstractBeanDefinition) configurationDefinition).setAutowireMode(AbstractBeanDefinition.AUTOWIRE_NO);
		configurationDefinition.setScope(AbstractBeanDefinition.SCOPE_PROTOTYPE);
		return beanFactory.getBean(propertiesBeanName, WebSocketConfigurationProperties.class);
	}
}
