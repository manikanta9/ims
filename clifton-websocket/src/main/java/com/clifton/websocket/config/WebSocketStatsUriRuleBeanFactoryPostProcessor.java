package com.clifton.websocket.config;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.spring.SpringApplicationContextUtils;
import com.clifton.core.web.stats.SystemRequestStats;
import com.clifton.core.web.stats.SystemRequestStatsUriRule;
import com.clifton.core.web.stats.SystemRequestStatsUriRule.RuleEffect;
import com.clifton.core.web.stats.SystemRequestStatsUriRule.RulePatternType;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;


/**
 * The {@link WebSocketStatsUriRuleBeanFactoryPostProcessor} {@link BeanDefinitionRegistryPostProcessor} automatically assigns {@link SystemRequestStatsUriRule} rules for
 * WebSocket requests. This is used to aggregate and anonymize individual WebSocket requests which would otherwise include unique session tokens, adding potentially sensitive
 * tokens as well as undesirable noise to the {@link SystemRequestStats} collection.
 *
 * @author MikeH
 */
public class WebSocketStatsUriRuleBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	/**
	 * The bean name prefix used when generating {@link SystemRequestStatsUriRule} beans.
	 */
	private static final String WS_STATS_RULE_BEAN_NAME_PREFIX = "webSocketEndpointStatsUriRule-";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		WebSocketConfigurationProperties webSocketProperties = getWebSocketProperties(beanFactory);
		if (webSocketProperties != null && webSocketProperties.isEnabled()) {
			BeanDefinitionRegistry beanDefinitionRegistry = (BeanDefinitionRegistry) beanFactory;
			registerStatsUriRules(beanDefinitionRegistry, webSocketProperties);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Registers {@link SystemRequestStatsUriRule} rules based on the configured WebSocket patterns. This prevents unique WebSocket-related URIs (such as URIs with STOMP session
	 * IDs) from being logged independently within the system request stats.
	 */
	private void registerStatsUriRules(BeanDefinitionRegistry registry, WebSocketConfigurationProperties webSocketProperties) {
		int index = 0;
		for (String endpoint : webSocketProperties.getStompEndpoints()) {
			String beanName = WS_STATS_RULE_BEAN_NAME_PREFIX + index++;
			String uriPrefix = webSocketProperties.getServletPath() + endpoint;
			SpringApplicationContextUtils.registerBeanDefinition(registry, beanName, SystemRequestStatsUriRule.class, uriPrefix, RulePatternType.PREFIX.name(), RuleEffect.PATTERN_ONLY.name());
		}
	}


	/**
	 * Gets the {@link WebSocketConfigurationProperties} bean from the given bean factory, or {@code null} if the bean does not exist.
	 * <p>
	 * This method instantiates the properties bean, which violates the bean factory/registry post-processor constraint that beans are not instantiated. See {@link
	 * WebSocketConfigurationProperties} for more details.
	 */
	private WebSocketConfigurationProperties getWebSocketProperties(ConfigurableListableBeanFactory beanFactory) {
		String propertiesBeanName = ContextConventionUtils.getBeanNameFromClassName(WebSocketConfigurationProperties.class.getName());
		if (!beanFactory.containsBeanDefinition(propertiesBeanName)) {
			return null;
		}
		BeanDefinition configurationDefinition = beanFactory.getBeanDefinition(propertiesBeanName);

		// Normalize definition and retrieve prototype instance
		((AbstractBeanDefinition) configurationDefinition).setAutowireMode(AbstractBeanDefinition.AUTOWIRE_NO);
		configurationDefinition.setScope(AbstractBeanDefinition.SCOPE_PROTOTYPE);
		return beanFactory.getBean(propertiesBeanName, WebSocketConfigurationProperties.class);
	}
}
