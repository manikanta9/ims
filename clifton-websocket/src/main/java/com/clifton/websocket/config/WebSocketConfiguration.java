package com.clifton.websocket.config;

import com.clifton.core.context.AutowireByNameBean;
import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.websocket.NoOpWebSocketHandlerImpl;
import com.clifton.websocket.WebSocketHandler;
import com.clifton.websocket.WebSocketHandlerImpl;
import com.clifton.websocket.broker.CliftonWebSocketMessageBrokerConfiguration;
import com.clifton.websocket.broker.WebSocketConfigurer;
import com.clifton.websocket.broker.WebSocketSecurityConfigurer;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.web.socket.config.annotation.DelegatingWebSocketMessageBrokerConfiguration;
import org.springframework.web.socket.config.annotation.EnableWebSocketMessageBroker;


/**
 * The configuration class for WebSocket properties and beans.
 * <p>
 * This is the central configuration class for WebSockets. In order to allow WebSockets to be enabled and disabled on a per-context basis, this configuration class is used to load
 * all relevant WebSocket beans.
 * <p>
 * This configuration is enabled by defining a bean of type {@link WebSocketConfigurationProperties}. See {@link WebSocketConfigurationRegistryPostProcessor} for more information.
 * <p>
 * This class also manually imports the {@link CliftonWebSocketMessageBrokerConfiguration} custom message broker configuration class. This is done in preference to the {@link
 * EnableWebSocketMessageBroker} annotation or the {@code <websocket:message-broker>} XML configuration element as it allows us to override the configuration class to customize
 * built-in logic and properties. The standard message broker activation procedures enable the {@link DelegatingWebSocketMessageBrokerConfiguration} type by default.
 *
 * @author MikeH
 */
@Configuration
@ExcludeFromComponentScan // Prevent executing this configuration unless WebSockets are explicitly enabled
@Import({CliftonWebSocketMessageBrokerConfiguration.class, WebSocketSecurityConfigurer.class})
public class WebSocketConfiguration {

	@AutowireByNameBean
	public WebSocketConfigurer webSocketConfigurer() {
		return new WebSocketConfigurer();
	}


	/**
	 * This overrides the {@link NoOpWebSocketHandlerImpl} in favor of the standard {@link WebSocketHandlerImpl}.
	 */
	@Primary
	@AutowireByNameBean
	public WebSocketHandler webSocketHandler() {
		return new WebSocketHandlerImpl();
	}
}
