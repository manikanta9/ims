package com.clifton.security.authorization.setup;

import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>SecurityAuthorizationSetupServiceImplTests</code> class defines methods for testing the {@link SecurityAuthorizationSetupServiceImpl}.
 *
 * @author vgomelsky
 */
@ContextConfiguration(locations = "../SecurityAuthorizationServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class SecurityAuthorizationSetupServiceImplTests {

	@Resource
	private SecurityAuthorizationSetupService securityAuthorizationSetupService;


	@Test
	public void testGetSecurityPermissionAssignmentListByGroup() {
		List<SecurityPermissionAssignment> result = this.securityAuthorizationSetupService.getSecurityPermissionAssignmentListByGroup(Short.parseShort("101"));
		Assertions.assertNotNull(result, "expecting 2 assignments to the group");
		Assertions.assertEquals(2, result.size(), "expecting 2 assignments to the group");
	}


	@Test
	public void testGetSecurityResourceByName() {
		SecurityResource resource = this.securityAuthorizationSetupService.getSecurityResourceByName("Resource 1");
		Assertions.assertNotNull(resource, "expected 1 security resource");
		Assertions.assertEquals(200, resource.getId().intValue(), "should get one security resource");

		Assertions.assertNull(this.securityAuthorizationSetupService.getSecurityResourceByName("NOT TO BE FOUND"), "no resource should be returned for a bogus name");
	}


	@Test
	public void testGetSecurityResourceListRoot() {
		List<SecurityResource> result = this.securityAuthorizationSetupService.getSecurityResourceListRoot();
		Assertions.assertNotNull(result, "expecting 3 root resources instead of null");
		Assertions.assertEquals(3, result.size(), "expecting 3 root resources");
	}


	@Test
	public void testGetSecurityResourceByTable() {
		SecurityResource result = this.securityAuthorizationSetupService.getSecurityResourceByTable("TestTable");
		Assertions.assertNull(result, "expecting no resource for a fake table");

		result = this.securityAuthorizationSetupService.getSecurityResourceByTable("SecurityUser");
		Assertions.assertNotNull(result, "expecting resource for SecurityUser table");
		Assertions.assertEquals(result.getName(), "Resource 2");
	}
}
