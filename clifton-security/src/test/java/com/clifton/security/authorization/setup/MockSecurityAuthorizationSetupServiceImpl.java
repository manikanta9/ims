package com.clifton.security.authorization.setup;


import com.clifton.core.security.authorization.SecurityResource;


/**
 * The <code>MockSecurityAuthorizationSetupServiceImpl</code> is a mock of the service
 * with implementation of the getSecurityResourceByTableName method that is
 * actually implemented in System project
 *
 * @author manderson
 */
public class MockSecurityAuthorizationSetupServiceImpl extends SecurityAuthorizationSetupServiceImpl {

	@Override
	public SecurityResource getSecurityResourceByTable(String tableName) {
		if ("SecurityUser".equals(tableName)) {
			return getSecurityResourceByName("Resource 2");
		}
		return null;
	}
}
