package com.clifton.security.authorization.retriever;

import com.clifton.core.security.authorization.SecurityPermission;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;


/**
 * The <code>UserAccessHolderTests</code> class tests {@link UserAccessHolder} methods.
 *
 * @author vgomelsky
 */
public class UserAccessHolderTests {

	@Test
	public void testIsAdmin() {
		UserAccessHolder holder = new UserAccessHolder(false, false, null);
		Assertions.assertFalse(holder.isAdmin());

		holder = new UserAccessHolder(true, false, null);
		Assertions.assertTrue(holder.isAdmin());

		holder = new UserAccessHolder(false, true, null);
		Assertions.assertFalse(holder.isAdmin());

		holder = new UserAccessHolder(true, true, null);
		Assertions.assertFalse(holder.isAdmin());
	}


	@Test
	public void testIsAccessAllowed() {
		Map<String, SecurityPermission> resourcePermissionMap = new HashMap<>();
		UserAccessHolder holder = new UserAccessHolder(false, false, resourcePermissionMap);
		Assertions.assertFalse(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_READ));

		holder = new UserAccessHolder(true, false, resourcePermissionMap);
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_READ));

		resourcePermissionMap.put("my resource", new SecurityPermission(SecurityPermission.PERMISSION_READ));
		holder = new UserAccessHolder(false, false, resourcePermissionMap);
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_READ));
		Assertions.assertFalse(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_WRITE));
		Assertions.assertFalse(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_CREATE));
		Assertions.assertFalse(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_FULL_CONTROL));

		resourcePermissionMap.put("my resource", new SecurityPermission(SecurityPermission.MASK_RW));
		holder = new UserAccessHolder(false, false, resourcePermissionMap);
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_READ));
		Assertions.assertFalse(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_CREATE));
		Assertions.assertFalse(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_FULL_CONTROL));

		resourcePermissionMap.put("my resource", new SecurityPermission(SecurityPermission.PERMISSION_FULL_CONTROL));
		holder = new UserAccessHolder(false, false, resourcePermissionMap);
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_READ));
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_WRITE));
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_CREATE));
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_DELETE));
		Assertions.assertTrue(holder.isAccessAllowed("my resource", SecurityPermission.PERMISSION_FULL_CONTROL));
	}
}
