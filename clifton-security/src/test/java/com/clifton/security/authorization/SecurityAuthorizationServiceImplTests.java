package com.clifton.security.authorization;

import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.authorization.retriever.SecurityResourceUserAccess;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>SecurityAuthorizationServiceImplTests</code> class tests {@link SecurityAuthorizationServiceImpl} methods.
 *
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SecurityAuthorizationServiceImplTests {

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;
	@Resource
	private SecurityUserService securityUserService;


	@Test
	public void testIsSecurityUserAdmin() {
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityUserAdmin("simpleUser"));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityUserAdmin("regularUserName"));
	}


	@Test
	public void testIsSecurityAccessAllowed() {
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("adminUser", "Resource 2", SecurityPermission.PERMISSION_READ), "admin user has access to everything");
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("adminUser", "Resource 2", SecurityPermission.PERMISSION_FULL_CONTROL), "admin user has access to everything");
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("regularUserName", "Resource 2", SecurityPermission.PERMISSION_READ));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityAccessAllowed("regularUserName", "Resource 2", SecurityPermission.PERMISSION_CREATE));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityAccessAllowed("regularUserName", "Resource 2", SecurityPermission.PERMISSION_DELETE));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityAccessAllowed("simpleUser", "Resource 2", SecurityPermission.PERMISSION_READ));

		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("regularUserName", "Resource under 2", SecurityPermission.PERMISSION_READ));
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("regularUserName", "Resource under 2", SecurityPermission.PERMISSION_CREATE));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityAccessAllowed("regularUserName", "Resource under 2", SecurityPermission.PERMISSION_DELETE));

		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("directUser", "Resource 2", SecurityPermission.PERMISSION_READ));
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityAccessAllowed("directUser", "Resource under 2", SecurityPermission.PERMISSION_READ), "does not inherit parent's permissions");
	}


	@Test
	public void testObservers() {
		// cached permissions should be reset after updates
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"));
		SecurityUser user = this.securityUserService.getSecurityUserByName("adminUser");
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"));
		user.setDisabled(true);
		this.securityUserService.saveSecurityUser(user);
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"), "disabled user cannot be an admin");

		user.setDisabled(false);
		this.securityUserService.saveSecurityUser(user);
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"), "re-enabled user is an admin");

		SecurityGroup group = this.securityUserService.getSecurityGroup(Short.parseShort("100"));
		group.setAdmin(false);
		this.securityUserService.saveSecurityGroup(group);
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"), "no admin group cannot be an admin");

		group.setAdmin(true);
		this.securityUserService.saveSecurityGroup(group);
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityUserAdmin("adminUser"), "admin group is an admin");
	}


	@Test
	public void testInheritedSecurityAccess() {
		Assertions.assertFalse(this.securityAuthorizationService.isSecurityAccessAllowed("simpleUser", "Resource 5 under 4", SecurityPermission.PERMISSION_WRITE),
				"Expect [simpleUser] should not have write access to [Resource 5] but it does.");
		Assertions.assertTrue(this.securityAuthorizationService.isSecurityAccessAllowed("simpleUser", "Resource 5 under 4", SecurityPermission.PERMISSION_READ),
				"Expect [simpleUser] should have read access to [Resource 5] but it does not.");
	}


	@Test
	public void testGetSecurityResourceUserAccessList() {
		String resourceName = "Resource 1";
		Map<String, String> userResults = new HashMap<>();
		userResults.put("adminUser", "Admin-RWCDEF");
		List<SecurityResourceUserAccess> accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, false);
		validateAccessList(resourceName, accessList, 1, userResults);

		resourceName = "Resource 2";
		userResults.clear();
		userResults.put("regularUserName", "RW");
		userResults.put("directUser", "RW");
		userResults.put("adminUser", "Admin-RWCDEF");
		userResults.put("regularUserWithCreate", "RWC");
		accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, false);
		validateAccessList(resourceName, accessList, 4, userResults);

		resourceName = "Resource under 2";
		userResults.clear();
		userResults.put("regularUserName", "RWC");
		userResults.put("directUser", "C");
		userResults.put("adminUser", "Admin-RWCDEF");
		userResults.put("regularUserWithCreate", "RWC");
		accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, false);
		validateAccessList(resourceName, accessList, 4, userResults);

		resourceName = "Resource 3";
		userResults.clear();
		userResults.put("adminUser", "Admin-RWCDEF");
		accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, false);
		validateAccessList(resourceName, accessList, 1, userResults);

		resourceName = "Resource 4 under 3";
		userResults.clear();
		userResults.put("regularUserName", "R");
		userResults.put("simpleUser", "RWCD");
		userResults.put("adminUser", "Admin-RWCDEF");
		accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, false);
		validateAccessList(resourceName, accessList, 3, userResults);

		resourceName = "Resource 5 under 4";
		userResults.clear();
		userResults.put("regularUserName", "R");
		userResults.put("simpleUser", "R");
		userResults.put("adminUser", "Admin-RWCDEF");
		accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, false);
		validateAccessList(resourceName, accessList, 3, userResults);

		// Shouldn't have any disabled users - same results expected
		accessList = this.securityAuthorizationService.getSecurityResourceUserAccessList(resourceName, true);
		validateAccessList(resourceName, accessList, 3, userResults);
	}


	private void validateAccessList(String resource, List<SecurityResourceUserAccess> accessList, int size, Map<String, String> userResults) {
		Assertions.assertEquals(size, CollectionUtils.getSize(accessList), resource + ": Expected " + size + " users with permissions.");
		for (SecurityResourceUserAccess a : CollectionUtils.getIterable(accessList)) {
			String userName = a.getSecurityUser().getUserName();
			Assertions.assertTrue(userResults.containsKey(userName), "Did not expect user [" + userName + "] to have any permissions.");
			String perm = (a.isAdmin() ? "Admin-" : "") + (a.isReadAllowed() ? "R" : "") + (a.isWriteAllowed() ? "W" : "") + (a.isCreateAllowed() ? "C" : "") + (a.isDeleteAllowed() ? "D" : "")
					+ (a.isExecuteAllowed() ? "E" : "") + (a.isFullControlAllowed() ? "F" : "");
			Assertions.assertEquals(userResults.get(userName),
					perm, "Expected user [" + userName + "] to have the following permissions for resource [" + resource + "]: " + userResults.get(userName) + " but got " + perm);
		}
	}
}
