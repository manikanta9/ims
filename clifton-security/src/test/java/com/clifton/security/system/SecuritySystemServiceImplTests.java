package com.clifton.security.system;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>SecuritySystemServiceImplTests</code> class defines methods for testing the {@link SecuritySystemServiceImpl}.
 *
 * @author lnaylor
 */
@ContextConfiguration(locations = "SecuritySystemServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class SecuritySystemServiceImplTests {

	@Resource
	private SecuritySystemService securitySystemService;


	@Test
	public void testGetSecuritySystemUserBySystemIdAndUserIdDisabledFalse() {
		SecuritySystemUser securitySystemUser = this.securitySystemService.getSecuritySystemUserBySystemIdAndUserId((short) 1, (short) 1);
		Assertions.assertEquals("SecuritySystemUser1", securitySystemUser.getUserName());
	}


	@Test
	public void testGetSecuritySystemUserBySystemIdAndUserIdDisabledTrue() {
		SecuritySystemUser securitySystemUser = this.securitySystemService.getSecuritySystemUserBySystemIdAndUserId((short) 1, (short) 2);
		Assertions.assertNull(securitySystemUser);
	}
}
