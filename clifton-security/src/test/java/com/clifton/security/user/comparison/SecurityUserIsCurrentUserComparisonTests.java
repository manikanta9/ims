package com.clifton.security.user.comparison;

import com.clifton.security.SecurityTestsUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SecurityUserIsCurrentUserComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;


	private SecurityUserIsCurrentUserComparison getComparisonBean() {
		SecurityUserIsCurrentUserComparison comparison = new SecurityUserIsCurrentUserComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testEvaluate1() {
		SecurityUserIsCurrentUserComparison comparison = getComparisonBean();
		comparison.setSecurityUserId(new Integer(1).shortValue());

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "testUser");
		Assertions.assertTrue(comparison.evaluate(null, null), "the user is set");

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "adminUser");
		Assertions.assertFalse(comparison.evaluate(null, null), "the user is not set");

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "anotherUser");
		Assertions.assertFalse(comparison.evaluate(null, null), "the user is NOT set");
	}


	@Test
	public void testEvaluate2() {
		SecurityUserIsCurrentUserComparison comparison = getComparisonBean();
		comparison.setSecurityUserId(new Integer(2).shortValue());

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "testUser");
		Assertions.assertFalse(comparison.evaluate(null, null), "the user is not set");

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "adminUser");
		Assertions.assertTrue(comparison.evaluate(null, null), "the user is set");

		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "anotherUser");
		Assertions.assertFalse(comparison.evaluate(null, null), "the user is not set");
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
