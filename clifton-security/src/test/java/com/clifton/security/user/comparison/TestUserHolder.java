package com.clifton.security.user.comparison;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.security.user.SecurityUser;


/**
 * @author vgomelsky
 */
public class TestUserHolder extends BaseSimpleEntity<Short> {

	private SecurityUser assigneeUser;
	private SecurityUser approverUser;


	public SecurityUser getAssigneeUser() {
		return this.assigneeUser;
	}


	public void setAssigneeUser(SecurityUser assigneeUser) {
		this.assigneeUser = assigneeUser;
	}


	public SecurityUser getApproverUser() {
		return this.approverUser;
	}


	public void setApproverUser(SecurityUser approverUser) {
		this.approverUser = approverUser;
	}
}
