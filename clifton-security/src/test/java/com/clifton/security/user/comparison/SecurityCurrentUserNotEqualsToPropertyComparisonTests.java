package com.clifton.security.user.comparison;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration("SecurityUserIsCurrentUserComparisonTests-context.xml")
@ExtendWith(SpringExtension.class)
public class SecurityCurrentUserNotEqualsToPropertyComparisonTests {

	@Resource
	private ApplicationContextService applicationContextService;
	@Resource
	private ContextHandler contextHandler;
	@Resource
	private SecurityUserService securityUserService;


	private SecurityCurrentUserNotEqualsToPropertyComparison getComparisonBean() {
		SecurityCurrentUserNotEqualsToPropertyComparison comparison = new SecurityCurrentUserNotEqualsToPropertyComparison();
		comparison.setBeanPropertyName("createUserId");
		this.applicationContextService.autowireBean(comparison);
		return comparison;
	}


	@Test
	public void testEvaluate1() {
		SecurityCurrentUserNotEqualsToPropertyComparison comparison = getComparisonBean();

		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "testUser");

		SecurityResource bean = new SecurityResource();
		Assertions.assertTrue(comparison.evaluate(bean, null));

		bean.setCreateUserId(new Integer(1).shortValue());
		Assertions.assertFalse(comparison.evaluate(bean, null));

		comparison.setBeanPropertyName("fakeProperty");
		Assertions.assertTrue(comparison.evaluate(bean, null));
	}
}
