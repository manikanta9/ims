package com.clifton.security.user.comparison;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.security.user.SecurityUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * @author vgomelsky
 */
@ContextConfiguration(locations = "SecurityGroupHasCurrentUserComparisonTests-context.xml")
@ExtendWith(SpringExtension.class)
public class SecurityGroupHasUserPropertyComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;


	private SecurityGroupHasUserPropertyComparison getComparisonBean() {
		SecurityGroupHasUserPropertyComparison comparison = new SecurityGroupHasUserPropertyComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testEvaluate() {
		SecurityGroupHasUserPropertyComparison comparison = getComparisonBean();
		comparison.setSecurityGroupId(Short.parseShort("111"));
		comparison.setUserBeanPropertyName("assigneeUser");

		SimpleComparisonContext context = new SimpleComparisonContext();
		TestUserHolder bean = new TestUserHolder();
		bean.setAssigneeUser(new SecurityUser());
		bean.getAssigneeUser().setUserName("testUser");
		bean.getAssigneeUser().setId((short) 1);

		Assertions.assertFalse(comparison.evaluate(bean, context), "the user is not a member of admin group");
		Assertions.assertEquals(context.getFalseMessage(), "(Assignee User testUser is not in group Admin Group)");
		Assertions.assertNull(context.getTrueMessage());

		context = new SimpleComparisonContext();
		bean.getAssigneeUser().setUserName("adminUser");
		bean.getAssigneeUser().setId((short) 2);

		Assertions.assertTrue(comparison.evaluate(bean, context), "the user is a member of admin group");
		Assertions.assertEquals(context.getTrueMessage(), "(Assignee User adminUser is in group Admin Group)");
		Assertions.assertNull(context.getFalseMessage());
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
