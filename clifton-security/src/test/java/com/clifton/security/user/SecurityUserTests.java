package com.clifton.security.user;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>SecurityUserTests</code> class defines test for testing the {@link SecurityUser} methods.
 *
 * @author vgomelsky
 */
public class SecurityUserTests {

	@Test
	public void testGetLabel() {
		SecurityUser user = new SecurityUser();
		Assertions.assertNull(user.getLabel(), "user label must be null");

		user.setUserName("testUser");
		Assertions.assertEquals(user.getLabel(), "testUser");
	}


	@Test
	public void testToString() {
		SecurityUser user = new SecurityUser();
		Assertions.assertEquals(user.toString(), "{id=null,userName=null}");

		user.setId(new Integer(5).shortValue());
		user.setUserName("testUser");
		Assertions.assertEquals(user.toString(), "{id=5,userName=testUser}");
	}
}
