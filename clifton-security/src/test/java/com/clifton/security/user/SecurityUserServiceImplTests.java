package com.clifton.security.user;

import com.clifton.core.context.ContextHandler;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupServiceImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>SecurityAuthorizationSetupServiceImplTests</code> class defines methods for testing the {@link SecurityAuthorizationSetupServiceImpl}.
 *
 * @author vgomelsky
 */
@ContextConfiguration(locations = "../authorization/SecurityAuthorizationServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class SecurityUserServiceImplTests {

	@Resource
	private ContextHandler contextHandler;
	@Resource
	private SecurityUserService securityUserService;


	@Test
	public void testGetSecurityUserCurrent() {
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, "regularUser");
		SecurityUser user = this.securityUserService.getSecurityUserCurrent();
		Assertions.assertNotNull(user, "current user cannot be null");
		Assertions.assertEquals(user.getIntegrationPseudonym(), "regularUser");
	}


	@Test
	public void testGetSecurityUserByPseudonym() {
		Assertions.assertNotNull(this.securityUserService.getSecurityUserByPseudonym("regularUser"), "expecting 1 user with pseudonym 'regularUser'");
		Assertions.assertNull(this.securityUserService.getSecurityUserByPseudonym("fake"), "expecting no user with pseudonym 'fake'");
	}


	@Test
	public void testGetSecurityUserListByGroup() {
		Assertions.assertEquals(1, this.securityUserService.getSecurityUserListByGroup(Short.parseShort("100")).size(), "expecting 1 user in group 100");
		Assertions.assertTrue(this.securityUserService.getSecurityUserListByGroup(Short.parseShort("1234")).isEmpty(), "expecting no user in group 1234");
	}


	@Test
	public void testGetSecurityGroupListByUser() {
		Assertions.assertEquals(2, this.securityUserService.getSecurityGroupListByUser((short) 1).size(), "expecting 2 groups for user 1");
		Assertions.assertEquals(1, this.securityUserService.getSecurityGroupListByUser((short) 2).size(), "expecting 1 group for user 2");
		Assertions.assertTrue(this.securityUserService.getSecurityGroupListByUser((short) 4).isEmpty(), "expecting no groups for user 4");
		Assertions.assertEquals(Short.parseShort("100"), this.securityUserService.getSecurityGroupListByUser((short) 2).get(0).getId(), "expecting group id 302 for user 2");
	}


	@Test
	public void testRemoveUserUpdatesCache() {
		int numGroups = this.securityUserService.getSecurityGroupListByUser((short) 1).size();
		this.securityUserService.unlinkSecurityUserFromGroup((short) 1, (short) 101);
		Assertions.assertNotEquals(numGroups, this.securityUserService.getSecurityGroupListByUser((short) 1).size(), "Failed to remove group from user's cache: expecting only 1 group for user 1");
		this.securityUserService.linkSecurityUserToGroup((short) 1, (short) 101);
	}


	@Test
	public void testUpdatesToSecurityUserToGroup() {
		Assertions.assertNotNull(this.securityUserService.linkSecurityUserToGroup(Short.parseShort("4"), Short.parseShort("100")), "cannot link a user to a group");
		this.securityUserService.unlinkSecurityUserFromGroup(Short.parseShort("4"), Short.parseShort("100"));
	}
}
