package com.clifton.security.user.comparison;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.security.SecurityTestsUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SecurityGroupHasCurrentUserComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;


	private SecurityGroupHasCurrentUserComparison getComparisonBean() {
		SecurityGroupHasCurrentUserComparison comparison = new SecurityGroupHasCurrentUserComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testEvaluate() {
		SecurityGroupHasCurrentUserComparison comparison = getComparisonBean();
		comparison.setSecurityGroupId(Short.parseShort("111"));

		SimpleComparisonContext context = new SimpleComparisonContext();
		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "testUser");
		Assertions.assertFalse(comparison.evaluate(null, context), "the user is not a member of admin group");
		Assertions.assertEquals(context.getFalseMessage(), "(Current User testUser is not in group Admin Group)");
		Assertions.assertNull(context.getTrueMessage());

		context = new SimpleComparisonContext();
		SecurityTestsUtils.setSecurityContextPrincipal(this.applicationContext, "adminUser");
		Assertions.assertTrue(comparison.evaluate(null, context), "the user is a member of admin group");
		Assertions.assertEquals(context.getTrueMessage(), "(Current User adminUser is in group Admin Group)");
		Assertions.assertNull(context.getFalseMessage());
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
