package com.clifton.security.web.filter;

import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;


/**
 * The <code>SecurityUserManagementFilterTests</code> class tests the {@link SecurityUserManagementFilter} methods.
 *
 * @author vgomelsky
 */
@ContextConfiguration(locations = "../../authorization/SecurityAuthorizationServiceImplTests-context.xml")
@ExtendWith(SpringExtension.class)
public class SecurityUserManagementFilterTests {

	@Resource
	private ContextHandler contextHandler;
	@Resource
	private SecurityUserManagementFilter securityUserManagementFilter;


	@Test
	public void testDoFilter() throws IOException, ServletException {
		// clear user in case some other test set it
		this.contextHandler.removeBean(Context.USER_BEAN_NAME);

		// set user in Spring Security Context
		CrowdUserDetails crowdUser = Mockito.mock(CrowdUserDetails.class);
		Mockito.when(crowdUser.getUsername()).thenReturn("directUser");
		Authentication auth = Mockito.mock(Authentication.class);
		Mockito.when(auth.getPrincipal()).thenReturn(crowdUser);
		SecurityContextHolder.getContext().setAuthentication(auth);

		this.securityUserManagementFilter.init(null);

		Assertions.assertNull(this.contextHandler.getBean(Context.USER_BEAN_NAME), "no current user before filter");

		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getRequestURI()).thenReturn("someURL");
		Mockito.when(request.getContextPath()).thenReturn("/ims/");
		this.securityUserManagementFilter.doFilter(request, null, new TestFilterChain());
		Assertions.assertNull(this.contextHandler.getBean(Context.USER_BEAN_NAME), "no current user after filter");

		this.securityUserManagementFilter.destroy();
	}


	class TestFilterChain implements FilterChain {

		@Override
		public void doFilter(@SuppressWarnings("unused") ServletRequest req, @SuppressWarnings("unused") ServletResponse res) {
			SecurityUser user = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
			Assertions.assertNotNull(user, "current user present inside of filter");
			Assertions.assertEquals("directUser", user.getUserName());
		}
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}
}
