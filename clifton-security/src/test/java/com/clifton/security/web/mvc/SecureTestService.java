package com.clifton.security.web.mvc;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecureMethodPermissionResolver;
import com.clifton.core.security.authorization.SecureMethodSecurityEvaluator;
import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.security.authorization.SecureMethodTableResolver;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Controller
public class SecureTestService {

	@RequestMapping("methodThatNeedsAdminRights")
	@SecureMethod(adminSecurity = true)
	public void methodThatNeedsAdminRights() {
		// do nothing
	}


	@RequestMapping("methodWithSecurityDisabled")
	@SecureMethod(disableSecurity = true)
	public void methodWithSecurityDisabled() {
		// do nothing
	}


	@RequestMapping("getMethodWithSecurityResource")
	@SecureMethod(securityResource = "Resource 2")
	public void getMethodWithSecurityResource() {
		// do nothing
	}


	@RequestMapping("saveMethodWithSecurityResource")
	@SecureMethod(securityResource = "Resource 2")
	public void saveMethodWithSecurityResource() {
		// do nothing
	}


	@RequestMapping("deleteMethodWithSecurityResource")
	@SecureMethod(securityResource = "Resource 2")
	public void deleteMethodWithSecurityResource() {
		// do nothing
	}


	@RequestMapping("saveMethodWithDtoClass")
	@SecureMethod(dtoClass = SecurityUser.class)
	public void saveMethodWithDtoClass() {
		// do nothing
	}


	@RequestMapping("saveMethodWithTableName")
	@SecureMethod(table = "SecurityUser")
	public void saveMethodWithTableName() {
		// do nothing
	}


	@RequestMapping("saveMethodWithDynamicTableName")
	@SecureMethod(dynamicTableNameUrlParameter = "tableName")
	public void saveMethodWithDynamicTableName() {
		// do nothing
	}


	@RequestMapping("saveMethodWithPermissionResolver")
	@SecureMethod(permissionResolverBeanName = "permissionResolver", securityResourceResolverBeanName = "resourceResolver")
	public void saveMethodWithPermissionResolver() {
		// do nothing
	}


	@RequestMapping("saveMethodWithSecurityEvaluator")
	@SecureMethod(securityEvaluatorBeanName = "securityEvaluator")
	public void saveMethodWithSecurityEvaluator() {
		// do nothing
	}


	@RequestMapping("saveMethodWithResourceResolver")
	@SecureMethod(securityResourceResolverBeanName = "resourceResolver")
	public void saveMethodWithResourceResolver() {
		// do nothing
	}


	@RequestMapping("saveMethodWithTableResolver")
	@SecureMethod(tableResolverBeanName = "tableResolver")
	public void saveMethodWithTableResolver() {
		// do nothing
	}


	@RequestMapping("saveMethodWithDynamicSecurityResourceAndTableName")
	@SecureMethod(dynamicSecurityResourceBeanPath = "name", table = "SecurityResource")
	public void saveMethodWithDynamicSecurityResourceAndTableName() {
		// do nothing
	}


	@RequestMapping("saveMethodWithDynamicSecurityResourceAndDtoClass")
	@SecureMethod(dynamicSecurityResourceBeanPath = "securityResource.name", dtoClass = SecurityPermissionAssignment.class)
	public void saveMethodWithDynamicSecurityResourceAndDtoClass() {
		// do nothing
	}


	@RequestMapping("saveMethodWithDynamicTableNameAndTableName")
	@SecureMethod(dynamicTableNameBeanPath = "name", table = "SecurityGroup", dynamicSecurityResourceTableNameSuffix = " 2")
	public void saveMethodWithDynamicTableNameAndTableName() {
		// do nothing
	}


	@RequestMapping("saveMethodWithDynamicTableNameAndDtoClass")
	@SecureMethod(dynamicTableNameBeanPath = "referenceTwo.name", dtoClass = SecurityUserGroup.class)
	public void saveMethodWithDynamicTableNameAndDtoClass() {
		// do nothing
	}


	@RequestMapping("saveMethod")
	public void saveMethod(@SuppressWarnings("unused") SecurityUser user) {
		// do nothing
	}


	@RequestMapping("getMethod")
	public SecurityUser getMethod() {
		return null;
	}


	@RequestMapping("getAllMethod")
	public List<SecurityUser> getAllMethod() {
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Configuration
	static class ContextConfiguration {

		@Bean
		public SecureMethodSecurityEvaluator securityEvaluator() {
			return (secureAnnotation, method, config) -> {
				// do nothing
			};
		}


		@Bean
		public SecureMethodSecurityResourceResolver resourceResolver() {
			return (method, config) -> "Resource 2";
		}


		@Bean
		public SecureMethodTableResolver tableResolver() {
			return (method, config) -> "SecurityUser";
		}


		@Bean
		public SecureMethodPermissionResolver permissionResolver() {
			return (method, config, securityResourceName) -> SecurityPermission.PERMISSION_CREATE;
		}
	}
}
