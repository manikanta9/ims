package com.clifton.security.web.mvc;


import com.clifton.core.context.ContextHandler;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecureMethodPermissionResolver;
import com.clifton.core.security.authorization.SecureMethodSecurityEvaluator;
import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.security.authorization.SecureMethodTableResolver;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.security.SecurityTestsUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.method.HandlerMethod;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SecureAnnotationMethodHandlerAdapterTests {

	@Resource
	private ContextHandler contextHandler;
	@Resource
	private SecurityUserService securityUserService;
	@Resource
	private SecureHandlerInterceptorAdapter secureAnnotationMethodHandlerAdapter;


	private void setCurrentUser(String userName) {
		SecurityTestsUtils.setSecurityContextPrincipal(this.contextHandler, this.securityUserService, userName);
	}


	private HttpServletRequest getMockedRequest(String url) {
		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getContextPath()).thenReturn("/");
		Mockito.when(request.getServletPath()).thenReturn("/" + url);
		Mockito.when(request.getRequestURI()).thenReturn("/" + url);
		return request;
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testVerifySecurityAccessForAdmin() throws Exception {
		setCurrentUser("adminUser");
		{
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("methodThatNeedsAdminRights"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("methodThatNeedsAdminRights"), null, hm);
		}
		{
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithSecurityResource"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithSecurityResource"), null, hm);
		}
		{
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("deleteMethodWithSecurityResource"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("deleteMethodWithSecurityResource"), null, hm);
		}
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForMethodWithSecurityDisabled() throws Exception {
		setCurrentUser("simpleUser");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("methodWithSecurityDisabled"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("methodWithSecurityDisabled"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForGetMethodWithSecurityResource() throws Exception {
		setCurrentUser("regularUser");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("getMethodWithSecurityResource"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("getMethodWithSecurityResource"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithSecurityResource() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithSecurityResource"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithSecurityResource"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForDeleteMethodWithSecurityResource() {
		Assertions.assertThrows(AccessDeniedException.class, () -> {
			setCurrentUser("regularUser");
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("deleteMethodWithSecurityResource"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("deleteMethodWithSecurityResource"), null, hm);
		});
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDtoClass() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDtoClass"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithDtoClass"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithTableName() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithTableName"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithTableName"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithPermissionResolver() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithPermissionResolver"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithPermissionResolver"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithSecurityEvaluator() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithSecurityEvaluator"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithSecurityEvaluator"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithResourceResolver() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithResourceResolver"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithResourceResolver"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithTableResolver() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithTableResolver"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethodWithTableResolver"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDynamicSecurityResourceAndTableName() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HttpServletRequest request = getMockedRequest("saveMethodWithDynamicSecurityResourceAndTableName");
		Map<String, String[]> params = new HashMap<>();
		params.put("id", new String[]{"201"});
		Mockito.when(request.getParameterMap()).thenReturn(params);
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDynamicSecurityResourceAndTableName"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDynamicSecurityResourceAndDtoClass() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HttpServletRequest request = getMockedRequest("saveMethodWithDynamicSecurityResourceAndDtoClass");
		Map<String, String[]> params = new HashMap<>();
		params.put("securityResource.id", new String[]{"201"});
		Mockito.when(request.getParameterMap()).thenReturn(params);
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDynamicSecurityResourceAndDtoClass"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDynamicTableNameAndTableName() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HttpServletRequest request = getMockedRequest("saveMethodWithDynamicTableNameAndTableName");
		Map<String, String[]> params = new HashMap<>();
		params.put("id", new String[]{"106"});
		Mockito.when(request.getParameterMap()).thenReturn(params);
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDynamicTableNameAndTableName"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDynamicTableNameAndDtoClass() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HttpServletRequest request = getMockedRequest("saveMethodWithDynamicTableNameAndDtoClass");
		Map<String, String[]> params = new HashMap<>();
		params.put("referenceTwo.id", new String[]{"107"});
		Mockito.when(request.getParameterMap()).thenReturn(params);
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDynamicTableNameAndDtoClass"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDynamicTableName_HaveAccess() throws Exception {
		setCurrentUser("regularUser");
		HttpServletRequest request = getMockedRequest("saveMethodWithDynamicTableName");
		Map<String, String[]> params = new HashMap<>();
		params.put("tableName", new String[]{"SecurityUser"});
		// Pass ID so it uses WRITE access
		params.put("id", new String[]{"1"});
		Mockito.when(request.getParameterMap()).thenReturn(params);
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDynamicTableName"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethodWithDynamicTableName_NoAccess() {
		Assertions.assertThrows(AccessDeniedException.class, () -> {
			setCurrentUser("regularUser");
			HttpServletRequest request = getMockedRequest("saveMethodWithDynamicTableName");
			Map<String, String[]> params = new HashMap<>();
			params.put("tableName", new String[]{"SecurityGroup"});
			// Pass ID so it uses WRITE access
			params.put("id", new String[]{"1"});
			Mockito.when(request.getParameterMap()).thenReturn(params);
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethodWithDynamicTableName"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
		});
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethod_NoCreateAccess_AccessDenied() {
		Assertions.assertThrows(AccessDeniedException.class, () -> {
			setCurrentUser("regularUser");
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethod", SecurityUser.class));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethod"), null, hm);
		});
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethod_NoCreateAccess() throws Exception {
		setCurrentUser("regularUser");
		HttpServletRequest request = getMockedRequest("saveMethod");
		Map<String, String[]> params = new HashMap<>();
		// Pass ID so it uses WRITE access
		params.put("id", new String[]{"1"});
		Mockito.when(request.getParameterMap()).thenReturn(params);
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethod", SecurityUser.class));
		this.secureAnnotationMethodHandlerAdapter.preHandle(request, null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForSaveMethod_WithCreateAccess() throws Exception {
		setCurrentUser("regularUserWithCreate");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("saveMethod", SecurityUser.class));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("saveMethod"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForGetMethod() throws Exception {
		setCurrentUser("regularUser");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("getMethod"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("getMethod"), null, hm);
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForGetMethodAccessDenied() {
		Assertions.assertThrows(AccessDeniedException.class, () -> {
			setCurrentUser("simpleUser");
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("getMethod"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("getMethod"), null, hm);
		});
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForAdminNeededMethodAccessDenied() {
		Assertions.assertThrows(AccessDeniedException.class, () -> {
			setCurrentUser("simpleUser");
			HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("methodThatNeedsAdminRights"));
			this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("methodThatNeedsAdminRights"), null, hm);
		});
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testInvokeForGetAllMethod() throws Exception {
		setCurrentUser("regularUser");
		HandlerMethod hm = new HandlerMethod(new SecureTestService(), SecureTestService.class.getMethod("getAllMethod"));
		this.secureAnnotationMethodHandlerAdapter.preHandle(getMockedRequest("getAllMethod"), null, hm);
	}
}
