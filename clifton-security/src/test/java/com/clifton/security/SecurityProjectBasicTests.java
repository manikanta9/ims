package com.clifton.security;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class SecurityProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "security";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("authentication");
		approvedList.add("encryption");
		approvedList.add("impersonation");
		approvedList.add("oauth");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.naming.AuthenticationException");
		imports.add("javax.servlet.");

		imports.add("com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails");
		imports.add("org.springframework.security.authentication");
		imports.add("org.springframework.security.core.GrantedAuthority");
		imports.add("org.springframework.security.core.Authentication");
		imports.add("org.springframework.security.core.userdetails.UserDetails");
		imports.add("org.springframework.security.core.userdetails.UsernameNotFoundException");
		imports.add("org.springframework.security.core.context.SecurityContextHolder");
		imports.add("org.springframework.web.");
		imports.add("org.springframework.beans.factory.NoSuchBeanDefinitionException");
		imports.add("org.springframework.beans.factory.config.AutowireCapableBeanFactory");
		imports.add("org.springframework.http.HttpRequest");
		imports.add("org.springframework.http.HttpStatus");
		imports.add("org.springframework.http.client.");
		imports.add("org.springframework.security.oauth2.");
		imports.add("org.springframework.beans.factory.config.ConfigurableListableBeanFactory");
		imports.add("java.security.PublicKey");
	}


	@Override
	protected void configureApprovedTestClassImports(@SuppressWarnings("unused") List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.springframework.security.core.Authentication");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		Set<String> skippedMethods = new HashSet<>();
		skippedMethods.add("getSecurityUserByName"); // userName vs name cache key
		skippedMethods.add("saveSecurityCredential");
		skippedMethods.add("saveSecurityPermissionAssignment");
		skippedMethods.add("saveSecuritySecret");
		return skippedMethods.contains(method.getName());
	}


	@Override
	protected boolean isRowVersionRequired() {
		return true;
	}
}
