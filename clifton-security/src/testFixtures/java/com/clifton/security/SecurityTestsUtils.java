package com.clifton.security;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.context.ApplicationContext;


/**
 * The <code>SecurityTestsUtils</code> class provides utility methods for simplifying testing.
 *
 * @author vgomelsky
 */
public class SecurityTestsUtils {

	public static void setSecurityContextPrincipal(ApplicationContext context, String userName) {
		SecurityUserService userService = (SecurityUserService) context.getBean("securityUserService");
		ContextHandler contextHandler = (ContextHandler) context.getBean("contextHandler");

		setSecurityContextPrincipal(contextHandler, userService, userName);
	}


	public static void setSecurityContextPrincipal(ContextHandler contextHandler, SecurityUserService userService, String userName) {
		SecurityUser user = null;
		if (userService != null) {
			user = userService.getSecurityUserByPseudonym(userName);
		}
		if (user == null) {
			user = new SecurityUser();
			user.setUserName(userName);
		}
		contextHandler.setBean(Context.USER_BEAN_NAME, user);
	}
}
