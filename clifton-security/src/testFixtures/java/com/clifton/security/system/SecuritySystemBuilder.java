package com.clifton.security.system;

public class SecuritySystemBuilder {

	private SecuritySystem securitySystem;


	private SecuritySystemBuilder(SecuritySystem securitySystem) {
		this.securitySystem = securitySystem;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SecuritySystemBuilder createRedi() {
		SecuritySystem securitySystem = new SecuritySystem();

		securitySystem.setId((short) 1);
		securitySystem.setName("REDI+");
		securitySystem.setDescription("REDI+ trading platform.");

		return new SecuritySystemBuilder(securitySystem);
	}


	public static SecuritySystemBuilder createBloomberg() {
		SecuritySystem securitySystem = new SecuritySystem();

		securitySystem.setId((short) 3);
		securitySystem.setName("Bloomberg");
		securitySystem.setDescription("Bloomberg terminal and trading.");

		return new SecuritySystemBuilder(securitySystem);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecuritySystemBuilder withId(short id) {
		getSecuritySystem().setId(id);
		return this;
	}


	public SecuritySystemBuilder withName(String name) {
		getSecuritySystem().setName(name);
		return this;
	}


	public SecuritySystemBuilder withDescription(String description) {
		getSecuritySystem().setDescription(description);
		return this;
	}


	public SecuritySystem toSecuritySystem() {
		return this.securitySystem;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SecuritySystem getSecuritySystem() {
		return this.securitySystem;
	}
}
