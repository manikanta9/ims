package com.clifton.security.shared.user;

import java.io.Serializable;


/**
 * Represents a User in a system with corresponding permissions and roles.
 *
 * @author manderson
 */
public class User implements Serializable {

	private short id;

	private String userName;

	private String displayName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.displayName == null ? this.userName : this.displayName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public short getId() {
		return this.id;
	}


	public void setId(short id) {
		this.id = id;
	}


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
}
