-- ==============================================================
-- ==============================================================
-- Add the following users to the system (if they don't exist) and assign them to the Administrators group
-- If the Administrators Group Does not exist, it will be created and flagged as IsAdmin = true
-- All new software developers should be added to this script that need to run applications locally (PLEASE KEEP IN ALPHA ORDER SO IT'S EASIER TO FIND USERS TO ADD/REMOVE)
-- ==============================================================
-- ==============================================================
DECLARE @AdminUserTable TABLE
						(
							UserName    VARCHAR(50),
							DisplayName VARCHAR(100)
						)
INSERT INTO @AdminUserTable
SELECT 'AbhinayaM', 'Abhinaya Myana'
UNION
SELECT 'Angelom', 'Angelo Bruce Magpantay'
UNION
SELECT 'AnkitJ', 'Ankit Jain'
UNION
SELECT 'AnthonyE', 'Anthony Evans'
UNION
SELECT 'BrianH', 'Brian Hollingsworth'
UNION
SELECT 'CefeQ', 'Cefe Quesada'
UNION
SELECT 'DanielH', 'Daniel Haugen'
UNION
SELECT 'DavidI', 'David Irizarry'
UNION
SELECT 'DillonM', 'Dillon McMahon'
UNION
SELECT 'DipinC', 'Dipin Chandra'
UNION
SELECT 'FacundoO', 'Facundo Oviedo'
UNION
SELECT 'IgnacioA', 'Ignacio Arrascaeta'
UNION
SELECT 'JasonS', 'Jason Siverson'
UNION
SELECT 'JonathanE', 'Jon Eytinge'
UNION
SELECT 'jonathanr', 'Jonathan Richter'
UNION
SELECT 'JonathaW', 'Jonathan Wong'
UNION
SELECT 'KevinD', 'Kevin Duong'
UNION
SELECT 'KevinS', 'Kevin Sullivan'
UNION
SELECT 'LaurenN', 'Lauren Naylor'
UNION
SELECT 'MangC', 'Marc Chin'
UNION
SELECT 'mwacker', 'Mark Wacker'
UNION
SELECT 'manderson', 'Mary Anderson'
UNION
SELECT 'MattL', 'Matt Linebaugh'
UNION
SELECT 'MaxG', 'Max Guillen'
UNION
SELECT 'MichaelH', 'Michael Hwang'
UNION
SELECT 'michaelm', 'Michael Major'
UNION
SELECT 'MichaelPa', 'Michael Passinsky'
UNION
SELECT 'MikeH', 'Mike Hill'
UNION
SELECT 'MitchellF', 'Mitchell Fontaine'
UNION
SELECT 'MufaddaK', 'Mufaddal Karachiwala'
UNION
SELECT 'NickK', 'Nick Kirsch'
UNION
SELECT 'OliviaH', 'Olivia Hansen'
UNION
SELECT 'PaulJ', 'Paul Jungels'
UNION
SELECT 'StevenF', 'Steve Fahey'
UNION
SELECT 'SurjeetB', 'Surjeet Banga'
UNION
SELECT 'SyedZ', 'Syed Zaidi'
UNION
SELECT 'theodorez', 'Ted Zwieg'
UNION
SELECT 'TerryS', 'Terry Stebner'
UNION
SELECT 'TSievel', 'Todd Sievel'
UNION
SELECT 'TsungW', 'Tsung Wei Wu'
UNION
SELECT 'VishalA', 'Vishal Anand'
UNION
SELECT 'vgomelsky', 'Vlad Gomelsky'
UNION
SELECT 'YLam',
	   'Ying Lam'


	   -- QA Users
UNION
SELECT 'DanielM', 'Dan Morseburg'
UNION
SELECT 'GabrielS',
	   'Gabriela Sauma'

	   -- Other
UNION
SELECT 'agomelsky', 'Alex Gomelsky'
UNION
SELECT 'aneese', 'Allie Neese'
UNION
SELECT 'AshleyA', 'Ashley Allan'
UNION
SELECT 'bhammes', 'Ben Hammes'
UNION
SELECT 'JamesBa', 'Britt Barrineau'
UNION
SELECT 'BijayatS', 'Bijayata Shrestha'
UNION
SELECT 'ChristoR', 'Christopher Richards'
UNION
SELECT 'DavidL', 'David Lowe'
UNION
SELECT 'eprawalsky', 'Eric Prawalsky'
UNION
SELECT 'gliebl', 'Greg Liebl'
UNION
SELECT 'jpawelk', 'Jared Pawelk'
UNION
SELECT 'JulieB', 'Julie Brezen'
UNION
SELECT 'mattj', 'Matt Johnson'
UNION
SELECT 'SMcElreath', 'Stephan McElreath'
UNION
SELECT 'wge', 'Wei Ge'
UNION
SELECT 'WilliamJ', 'Will Jackson'
UNION
SELECT 'ZhannaR', 'Zhanna Robblee'

-- Any users that already exists, make sure they are enabled
UPDATE su
SET IsDisabled = 0
FROM SecurityUser su
		 INNER JOIN @AdminUserTable aut ON su.SecurityUserName = aut.UserName
WHERE su.IsDisabled = 1;


-- Add in whomever is still missing
INSERT INTO SecurityUser(SecurityUserName, DisplayName, IntegrationPseudonym, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.DisplayName, x.UserName, 0, GETDATE(), 0, GETDATE()
FROM @AdminUserTable x
WHERE NOT EXISTS(
		SELECT * FROM SecurityUser WHERE SecurityUserName = x.UserName
	);


DECLARE @AdminGroupName NVARCHAR(50) = 'Administrators'
INSERT INTO SecurityGroup (SecurityGroupName, SecurityGroupDescription, IntegrationPseudonym, IsAdmin, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT @AdminGroupName,
	   @AdminGroupName,
	   @AdminGroupName,
	   1,
	   0,
	   GETDATE(),
	   0,
	   GETDATE()
WHERE NOT EXISTS(
		SELECT SecurityGroupName FROM SecurityGroup WHERE SecurityGroupName = @AdminGroupName
	);

DECLARE @AdminGroupID INT
SELECT @AdminGroupID = SecurityGroupID
FROM SecurityGroup
WHERE SecurityGroupName = @AdminGroupName

INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, @AdminGroupID, 0, GETDATE(), 0, GETDATE()
FROM SecurityUser u
WHERE u.SecurityUserName IN (SELECT UserName FROM @AdminUserTable)
  AND NOT EXISTS(SELECT * FROM SecurityUserGroup ag WHERE ag.SecurityGroupID = @AdminGroupID AND ag.SecurityUserID = u.SecurityUserID);


-- ==============================================================
-- ==============================================================
-- Test Users
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group (groups may not be consistent across applications)
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE
				   (
					   UserName             NVARCHAR(50),
					   DisplayName          NVARCHAR(50),
					   GroupName            NVARCHAR(500),
					   OMSGroupNameOverride NVARCHAR(500) -- uses different groups and specifically separates the roles of PMs vs Traders performances a coalsce so use null to keep standard groupname
				   )
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|', NULL
UNION
-- PW: password2
SELECT 'imstestuser2', 'Test User 2', NULL, 'Operations|'
UNION
-- PW: password3
SELECT 'imstestuser3', 'Test User 3', NULL, 'Portfolio Managers|'
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', 'Analysts|', NULL
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', 'Documentation Team|', NULL
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|Compliance Violation Support|', 'Compliance|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trade Confirmation|Analysts|Portfolio Managers|', 'Trading|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL, NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', 'Client Admins|Investment Account Admins|Reporting Admins|Operations|', ''

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)

IF ('@application.name@' = 'OMS')
	BEGIN
		INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
		FROM @TestUsers x
				 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
				 INNER JOIN SecurityGroup g ON COALESCE(x.OMSGroupNameOverride, x.GroupName) LIKE '%' + g.SecurityGroupName + '|%'
		WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)
	END
ELSE
	BEGIN
		INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
		FROM @TestUsers x
				 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
				 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
		WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)
	END




