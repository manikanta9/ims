package com.clifton.security.impersonation;

import com.clifton.security.user.SecurityUser;

import java.util.Map;
import java.util.function.Supplier;


/**
 * The SecurityImpersonationHandler interface defines method that can be used to run as a different users.
 * Run as functionality saves current user information before running the task and then restores it after.
 * <p>
 * <b>
 * WARNING: This functionality can be very dangerous and should be used ONLY where appropriate. All DAO CRUD
 * operations will be executed as Run As user and will set Created By and Updated By to Run As user.
 * It will be impossible to trace back to the actual user that ran the task.
 * </b>
 *
 * @author vgomelsky
 */
public interface SecurityImpersonationHandler {

	/**
	 * Executes the specified task as the specified Run As User.
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 */
	public void runAsSecurityUser(SecurityUser runAsUser, Runnable task);


	/**
	 * Executes the specified task as the specified Run As User.
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 * <p>
	 * If a map of context overrides is provided, each entry in the map will be added/set on the context for the duration of the task execution and cleaned up after.
	 */
	public void runAsSecurityUser(SecurityUser runAsUser, Runnable task, Map<String, Object> contextOverridesMap);


	/**
	 * Executes the specified task as the specified Run As User.
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 */
	public void runAsSecurityUserName(String runAsUserName, Runnable task);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes the specified task as the specified Run As User.
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 */
	public <T> T runAsSecurityUserAndReturn(SecurityUser runAsUser, Supplier<T> task);


	/**
	 * Executes the specified task as the specified Run As User.
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 * <p>
	 * If a map of context overrides is provided, each entry in the map will be added/set on the context for the duration of the task execution and cleaned up after.
	 */
	public <T> T runAsSecurityUserAndReturn(SecurityUser runAsUser, Supplier<T> task, Map<String, Object> contextOverridesMap);


	/**
	 * Executes the specified task as the specified Run As User.
	 * Will save current user first and restore it after running the task (even if there is an exception).
	 */
	public <T> T runAsSecurityUserNameAndReturn(String runAsUserName, Supplier<T> task);
}
