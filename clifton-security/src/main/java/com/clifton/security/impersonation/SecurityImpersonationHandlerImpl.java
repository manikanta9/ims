package com.clifton.security.impersonation;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;


/**
 * @author vgomelsky
 */
@Component
public class SecurityImpersonationHandlerImpl implements SecurityImpersonationHandler {

	private ContextHandler contextHandler;
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void runAsSecurityUser(SecurityUser runAsUser, Runnable task) {
		runAsSecurityUser(runAsUser, task, Collections.emptyMap());
	}


	@Override
	public void runAsSecurityUser(SecurityUser runAsUser, Runnable task, Map<String, Object> contextOverridesMap) {
		Supplier<Void> taskSupplier = () -> {
			task.run();
			return null;
		};
		runAsSecurityUserAndReturn(runAsUser, taskSupplier, contextOverridesMap);
	}


	@Override
	public void runAsSecurityUserName(String runAsUserName, Runnable task) {
		SecurityUser runAsUser = getSecurityUserService().getSecurityUserByName(runAsUserName);
		ValidationUtils.assertNotNull(runAsUser, "Cannot find Run As Security User for: " + runAsUserName);

		runAsSecurityUser(runAsUser, task);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T> T runAsSecurityUserAndReturn(SecurityUser runAsUser, Supplier<T> task) {
		return runAsSecurityUserAndReturn(runAsUser, task, Collections.emptyMap());
	}


	@Override
	public <T> T runAsSecurityUserAndReturn(SecurityUser runAsUser, Supplier<T> task, Map<String, Object> contextOverridesMap) {
		Map<String, Object> overriddenContextValuesMap = new HashMap<>();
		overriddenContextValuesMap.put(Context.USER_BEAN_NAME, getContextHandler().getBean(Context.USER_BEAN_NAME));
		try {
			// set the Run As User as current user
			getContextHandler().setBean(Context.USER_BEAN_NAME, runAsUser);

			// set any context overrides
			if (contextOverridesMap != null) {
				contextOverridesMap.forEach((key, value) -> {
					// ignore null keys and values as these entries would result in the same behavior as nonexistent anyway
					if (key != null && value != null && !Context.USER_BEAN_NAME.equals(key)) {
						overriddenContextValuesMap.put(key, getContextHandler().getBean(key));
						getContextHandler().setBean(key, value);
					}
				});
			}

			return task.get();
		}
		finally {
			// restore the overridden values
			overriddenContextValuesMap.forEach((key, value) -> {
				if (value == null) {
					getContextHandler().removeBean(key);
				}
				else {
					getContextHandler().setBean(key, value);
				}
			});
		}
	}


	@Override
	public <T> T runAsSecurityUserNameAndReturn(String runAsUserName, Supplier<T> task) {
		SecurityUser runAsUser = getSecurityUserService().getSecurityUserByName(runAsUserName);
		ValidationUtils.assertNotNull(runAsUser, "Cannot find Run As Security User for: " + runAsUserName);

		return runAsSecurityUserAndReturn(runAsUser, task);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
