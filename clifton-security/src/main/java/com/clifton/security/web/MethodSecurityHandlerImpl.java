package com.clifton.security.web;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecureMethodPermissionResolver;
import com.clifton.core.security.authorization.SecureMethodSecurityEvaluator;
import com.clifton.core.security.authorization.SecureMethodSecurityResourceResolver;
import com.clifton.core.security.authorization.SecureMethodTableResolver;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.security.web.mvc.SecurityMethodToResourceCache;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.Supplier;


/**
 * The default implementation for the {@link MethodSecurityHandler} interface.
 * <p>
 * This class performs authorization checks based on inferred security resources. Security resources are determined through conventions, such as method names. Inferred results may
 * be overridden with {@link SecureMethod} annotations on methods.
 *
 * @author vgomelsky
 */
@Component
public class MethodSecurityHandlerImpl implements MethodSecurityHandler {

	private DaoLocator daoLocator;
	private ApplicationContextService applicationContextService;
	private SecurityMethodToResourceCache<? extends IdentityObject> securityMethodToResourceCache;
	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityAuthorizationSetupService securityAuthorizationSetupService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void verifySecurityAccess(Method handlerMethod, Supplier<String> targetSupplier, Map<String, ?> parameterMap) {
		// Guard-clause: Skip security if not required
		SecureMethod secureAnnotation = AnnotationUtils.findAnnotation(handlerMethod, SecureMethod.class);
		if (!isSecurityRequired(handlerMethod, secureAnnotation, targetSupplier)) {
			return;
		}

		String methodName = handlerMethod.getName();
		validateAdminRequired(secureAnnotation, methodName);
		if (secureAnnotation != null && !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.securityEvaluatorBeanName())) {
			SecureMethodSecurityEvaluator securityEvaluator = (SecureMethodSecurityEvaluator) getApplicationContextService().getContextBean(
					secureAnnotation.securityEvaluatorBeanName());
			securityEvaluator.checkPermissions(secureAnnotation, handlerMethod, parameterMap);
		}
		else {
			String securityResource = getSecurityResource(handlerMethod, methodName, secureAnnotation, parameterMap);
			int requiredPermissions = getRequiredPermissions(handlerMethod, methodName, secureAnnotation, parameterMap, securityResource);

			// check if current user is allowed to access the specified resource
			if (SecurityMethodToResourceCache.RESOURCE_NOT_MAPPED.equals(securityResource)) {
				throw new AccessDeniedException("UNMAPPED RESOURCE for method: " + methodName, requiredPermissions);
			}
			if (!getSecurityAuthorizationService().isSecurityAccessAllowed(securityResource, requiredPermissions)) {
				throw new AccessDeniedException(securityResource, requiredPermissions);
			}
		}
	}


	@Override
	public int getRequiredPermissions(Method handlerMethod, String methodName, SecureMethod secureAnnotation, Map<String, ?> parameterMap, String securityResourceName) {
		final int requiredPermissions;
		// annotation properties have priority over defaults
		if (secureAnnotation != null && !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.permissionResolverBeanName())) {
			// custom security permission implementation: based on method arguments
			String beanName = secureAnnotation.permissionResolverBeanName();
			SecureMethodPermissionResolver permissionResolver = (SecureMethodPermissionResolver) getApplicationContextService().getContextBean(beanName);
			requiredPermissions = permissionResolver.getRequiredPermissions(handlerMethod, parameterMap, securityResourceName);
		}
		else if (secureAnnotation != null && secureAnnotation.permissions() != 0) {
			// Annotation Override with Specific Permissions
			requiredPermissions = secureAnnotation.permissions();
		}
		else {
			// Otherwise - Determine Permission Level Based on Method Name (i.e. get = READ), where Default Permission Returned is WRITE
			// We pass Parameter map for saves which will use WRITE if ID parameter is present > 0 else CREATE
			requiredPermissions = ContextConventionUtils.getRequiredPermissionFromMethodName(methodName, parameterMap);
		}
		return requiredPermissions;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the name of SecurityResource that the specified method is mapped to.
	 * Throws an exception if it cannot determine the resource.
	 */
	private String getSecurityResource(Method handlerMethod, String methodName, SecureMethod secureAnnotation, Map<String, ?> parameterMap) {
		// find security resource that corresponds to the method (use table and dtoClass if necessary)
		String securityResource = getSecurityMethodToResourceCache().getSecurityResourceName(handlerMethod);
		SecurityResource resource = null;
		boolean cacheResource = true;
		if (securityResource == null) {
			String tableName = null;
			Class<? extends IdentityObject> dtoClass = null;
			// annotation properties have priority over defaults
			if (secureAnnotation != null) {
				if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.securityResource())) {
					securityResource = secureAnnotation.securityResource();
				}
				else if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.table())) {
					tableName = secureAnnotation.table();
				}
				else if (!IdentityObject.class.equals(secureAnnotation.dtoClass())) {
					dtoClass = secureAnnotation.dtoClass();
				}
				else if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.securityResourceResolverBeanName()) || !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.tableResolverBeanName())) {
					// custom security implementation: based on method arguments
					cacheResource = false; // cannot cache as it can be different based on parameter names
					if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.securityResourceResolverBeanName())) {
						SecureMethodSecurityResourceResolver resourceResolver = (SecureMethodSecurityResourceResolver) getApplicationContextService().getContextBean(
								secureAnnotation.securityResourceResolverBeanName());
						securityResource = resourceResolver.getSecurityResourceName(handlerMethod, parameterMap);
						// Check if the securityResource exists
						resource = getSecurityAuthorizationSetupService().getSecurityResourceByName(securityResource);
					}
					// If using securityResourceResolverBean name - and it returns null or didn't map successfully, see if there is also a table resolver bean name and use that
					if (resource == null && !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.tableResolverBeanName())) {
						securityResource = null;
						// custom security implementation: based on method arguments
						SecureMethodTableResolver tableNameResolver = (SecureMethodTableResolver) getApplicationContextService().getContextBean(secureAnnotation.tableResolverBeanName());
						tableName = tableNameResolver.getTableName(handlerMethod, parameterMap);
					}
				}
				// Note: Can be used with DTO Class or Table Name to determine look ups - so if securityResource is not null and dynamicTableNameBeanPath is not null then continue with dynamic look ups
				if (securityResource == null && !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicSecurityResourceBeanPath())) {
					// custom security implementation: based on securityResource property
					cacheResource = false; // cannot cache as it can be different based on parameter names
					securityResource = getDynamicSecurityResourceNameFromParameters(handlerMethod, methodName, secureAnnotation, parameterMap);
					if (!StringUtils.isEmpty(securityResource)) {
						resource = getSecurityAuthorizationSetupService().getSecurityResourceByName(securityResource);
						if (resource == null) {
							securityResource = null;
						}
					}
				}
				if (securityResource == null && (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameBeanPath()) || !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameUrlParameter()))) {
					// custom security implementation: based on tableName property
					cacheResource = false; // cannot cache as it can be different based on parameter names

					tableName = getDynamicTableNameFromParameters(handlerMethod, methodName, secureAnnotation, parameterMap);
					if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicSecurityResourceTableNameSuffix())) {
						// Check if the securityResource exists - will be use if it does, otherwise will just use the table name
						resource = getSecurityAuthorizationSetupService().getSecurityResourceByName(tableName + secureAnnotation.dynamicSecurityResourceTableNameSuffix());
						if (resource != null) {
							securityResource = tableName + secureAnnotation.dynamicSecurityResourceTableNameSuffix();
						}
					}
				}
			}

			if (securityResource == null) {
				if (tableName == null) {
					// try to determine table name
					@SuppressWarnings("unchecked")
					Class<IdentityObject> dtoClassIdentityObject = (Class<IdentityObject>) dtoClass;
					ObjectWrapper<Class<IdentityObject>> dtoClassHolder = new ObjectWrapper<>(dtoClassIdentityObject);
					tableName = ContextConventionUtils.getTableNameFromMethod(handlerMethod, methodName, dtoClassHolder, getDaoLocator());
					if (tableName == null) {
						throw new AccessDeniedException("Cannot determine table or DTO for method '" + methodName + "' for non-admin user.");
					}
					dtoClass = dtoClassHolder.getObject();
				}

				// find security resource that maps to the table
				try {
					resource = getSecurityAuthorizationSetupService().getSecurityResourceByTable(tableName);
				}
				catch (ValidationException e) {
					// If table name is invalid, a Validation Exception is thrown - which means our attempt to determine the table name was wrong
					throw new AccessDeniedException("Cannot determine table or DTO for method '" + methodName + "' for non-admin user: " + ExceptionUtils.getOriginalMessage(e));
				}
				if (resource != null) {
					securityResource = resource.getName();
				}
			}
			LogUtils.info(getClass(), "METHOD " + methodName + " RESOURCE: " + securityResource + " TABLE: " + tableName + " DTO: " + dtoClass);

			if (securityResource == null) {
				securityResource = SecurityMethodToResourceCache.RESOURCE_NOT_MAPPED;
			}
			if (cacheResource) {
				// cache looked up data but clear cache on changes to table to security mappings
				getSecurityMethodToResourceCache().storeSecurityResourceName(handlerMethod, securityResource);
			}
		}
		return securityResource;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	private String getDynamicTableNameFromParameters(Method handlerMethod, String methodName, SecureMethod secureAnnotation, Map<String, ?> requestParameters) {
		if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameBeanPath())) {
			String securityTableNamePath = secureAnnotation.dynamicTableNameBeanPath();
			String entityTableName = getEntityTableName(handlerMethod, methodName, secureAnnotation);
			ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(entityTableName);

			Integer id = MapUtils.getParameterAsInteger(secureAnnotation.dynamicIdUrlParameter(), requestParameters);
			if (id != null) {
				IdentityObject bean = getBean(dao, id);
				if (bean != null) {
					return (String) BeanUtils.getPropertyValue(bean, securityTableNamePath, true);
				}
			}
			else if (securityTableNamePath.indexOf('.') > -1) {
				String nestedProperty = securityTableNamePath.substring(0, securityTableNamePath.indexOf('.'));
				String nestedSecurityTableNamePath = securityTableNamePath.substring(securityTableNamePath.indexOf('.') + 1);
				IdentityObject bean = dao.newBean(nestedProperty);
				Class<?> nestedPropClass = BeanUtils.getPropertyType(bean, nestedProperty);
				if (IdentityObject.class.isAssignableFrom(nestedPropClass)) {
					@SuppressWarnings("unchecked")
					Class<? extends IdentityObject> nestedDtoClass = (Class<? extends IdentityObject>) nestedPropClass;
					id = MapUtils.getParameterAsInteger(nestedProperty + ".id", requestParameters);
					if (id != null) {
						dao = getDaoLocator().locate((Class<IdentityObject>) nestedDtoClass);
						bean = getBean(dao, id);
						if (bean != null) {
							return (String) BeanUtils.getPropertyValue(bean, nestedSecurityTableNamePath, true);
						}
					}
				}
			}
			throw new AccessDeniedException("Cannot determine dynamic table for method '" + methodName + "' for non-admin user using path [" + secureAnnotation.dynamicTableNameBeanPath() + "] for entity from table [" + entityTableName + "]");
		}
		if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameUrlParameter())) {
			String tableName = MapUtils.getParameterAsString(secureAnnotation.dynamicTableNameUrlParameter(), requestParameters);
			if (!StringUtils.isEmpty(tableName)) {
				return tableName;
			}
		}
		throw new AccessDeniedException("Cannot determine dynamic table for method '" + methodName + "' for non-admin.");
	}


	private String getDynamicSecurityResourceNameFromParameters(Method handlerMethod, String methodName, SecureMethod secureAnnotation, Map<String, ?> requestParameters) {
		if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicSecurityResourceBeanPath())) {
			String securityResourceNamePath = secureAnnotation.dynamicSecurityResourceBeanPath();
			String entityTableName = getEntityTableName(handlerMethod, methodName, secureAnnotation);
			ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(entityTableName);

			Integer id = MapUtils.getParameterAsInteger(secureAnnotation.dynamicIdUrlParameter(), requestParameters);
			if (id != null) {
				IdentityObject bean = getBean(dao, id);
				if (bean != null) {
					return (String) BeanUtils.getPropertyValue(bean, securityResourceNamePath, true);
				}
			}
			else if (securityResourceNamePath.indexOf('.') > -1) {
				String nestedProperty = securityResourceNamePath.substring(0, securityResourceNamePath.indexOf('.'));
				String nestedSecurityResourceNamePath = securityResourceNamePath.substring(securityResourceNamePath.indexOf('.') + 1);
				IdentityObject bean = dao.newBean(nestedProperty);
				Class<?> nestedPropClass = BeanUtils.getPropertyType(bean, nestedProperty);
				if (IdentityObject.class.isAssignableFrom(nestedPropClass)) {
					@SuppressWarnings("unchecked")
					Class<? extends IdentityObject> nestedDtoClass = (Class<? extends IdentityObject>) nestedPropClass;
					id = MapUtils.getParameterAsInteger(nestedProperty + ".id", requestParameters);
					if (id != null) {
						@SuppressWarnings("unchecked")
						Class<IdentityObject> nestedDtoClassIdentityObject = (Class<IdentityObject>) nestedDtoClass;
						dao = getDaoLocator().locate(nestedDtoClassIdentityObject);
						bean = getBean(dao, id);
						if (bean != null) {
							return (String) BeanUtils.getPropertyValue(bean, nestedSecurityResourceNamePath, true);
						}
					}
				}
			}
		}
		return null;
	}


	private String getEntityTableName(Method handlerMethod, String methodName, SecureMethod secureAnnotation) {
		String entityTableName = null;
		Class<? extends IdentityObject> dtoClass = null;
		if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.table())) {
			entityTableName = secureAnnotation.table();
		}
		else if (!IdentityObject.class.equals(secureAnnotation.dtoClass())) {
			dtoClass = secureAnnotation.dtoClass();
		}
		if (entityTableName == null) {
			// try to determine table name
			@SuppressWarnings("unchecked")
			Class<IdentityObject> dtoClassIdentityObject = (Class<IdentityObject>) dtoClass;
			ObjectWrapper<Class<IdentityObject>> dtoClassHolder = new ObjectWrapper<>(dtoClassIdentityObject);
			entityTableName = ContextConventionUtils.getTableNameFromMethod(handlerMethod, methodName, dtoClassHolder, getDaoLocator());
		}
		if (entityTableName == null) {
			throw new AccessDeniedException("Cannot determine table or DTO for method '" + methodName + "' for non-admin user.");
		}
		return entityTableName;
	}


	private <T extends IdentityObject> T getBean(ReadOnlyDAO<T> dao, Integer id) {
		return dao.findByPrimaryKey(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean isSecurityRequired(Method handlerMethod, SecureMethod secureAnnotation, Supplier<String> targetSupplier) {
		final boolean securityRequired;
		if (getSecurityAuthorizationService().isSecurityUserAdmin()) {
			// Skip security validation for administrators
			LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> ("CURRENT USER IS AN ADMIN: UNRESTRICTED ACCESS TO EVERYTHING: " + targetSupplier.get())));
			securityRequired = false;
		}
		else if (secureAnnotation != null && secureAnnotation.disableSecurity()) {
			// Skip security when disabled
			LogUtils.info(getClass(), "METHOD " + handlerMethod.getName() + " IS NOT SECURED");
			securityRequired = false;
		}
		else {
			// Default: Do not skip security
			securityRequired = true;
		}
		return securityRequired;
	}


	private void validateAdminRequired(SecureMethod secureAnnotation, String methodName) {
		if (secureAnnotation != null && secureAnnotation.adminSecurity()) {
			throw new AccessDeniedException("Only Administrators can access: " + methodName);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SecurityAuthorizationSetupService getSecurityAuthorizationSetupService() {
		return this.securityAuthorizationSetupService;
	}


	public void setSecurityAuthorizationSetupService(SecurityAuthorizationSetupService securityAuthorizationSetupService) {
		this.securityAuthorizationSetupService = securityAuthorizationSetupService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public SecurityMethodToResourceCache<? extends IdentityObject> getSecurityMethodToResourceCache() {
		return this.securityMethodToResourceCache;
	}


	public void setSecurityMethodToResourceCache(SecurityMethodToResourceCache<? extends IdentityObject> securityMethodToResourceCache) {
		this.securityMethodToResourceCache = securityMethodToResourceCache;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
