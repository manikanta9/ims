package com.clifton.security.web.filter;


import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.beans.common.Contact;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.timer.TimerHandler;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.web.stats.SystemRequestStatsService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.security.user.SpringSecurityUser;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.FrameworkServlet;
import org.springframework.web.servlet.HandlerExceptionResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * The <code>SecurityUserManagementFilter</code> class is a servlet filter that puts current {@link SecurityUser}
 * into request {@link Context} before its execution, then chains remaining filter and finally clears the user in the end.
 * <p>
 * Also does timing of execution.
 *
 * @author vgomelsky
 */
@Component
public class SecurityUserManagementFilter implements Filter {

	private ContextHandler contextHandler;
	private HandlerExceptionResolver webExceptionResolver;
	private SecurityUserService securityUserService;
	private SystemRequestStatsService systemRequestStatsService;
	private TimerHandler timerHandler;

	private FilterConfig filterConfig;


	@Override
	public void init(FilterConfig config) throws ServletException {
		this.filterConfig = config;
	}


	@Override
	public void destroy() {
		// do nothing
	}


	@Override
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		getTimerHandler().startTimer();
		HttpServletRequest request = (HttpServletRequest) req;

		try {
			SecurityUser user = getPrincipalUser();
			if (user != null) {
				String clientAddress = req.getRemoteAddr();
				// 2. store current user in context
				getContextHandler().setBean(Context.USER_BEAN_NAME, user);
				getContextHandler().setBean(Context.REQUEST_PARAMETER_MAP, req.getParameterMap());
				if (clientAddress != null) {
					getContextHandler().setBean(Context.REQUEST_CLIENT_ADDRESS, clientAddress);
				}
			}
			// 3. execute everything else
			chain.doFilter(req, res);
		}
		catch (Exception e) {
			try {
				// in case there's an exception, resolve it and render results
				HttpServletResponse response = (HttpServletResponse) res;
				ModelAndView modelAndView = getWebExceptionResolver().resolveException(request, response, null, e);
				AssertUtils.assertNotNull(modelAndView, "Exception resolver returned null ModelAndView");
				View view = modelAndView.getView();
				if (view == null) {
					view = (View) getWebApplicationContext().getBean("webJsonView");
				}
				view.render(modelAndView.getModel(), request, response);
			}
			catch (Throwable innerException) {
				// do not throw exception if connection to the client was closed: there's nowhere to write response
				if (!ExceptionUtils.isConnectionBrokenException(innerException)) {
					// let's hope that this never happens
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "ORIGINAL: " + ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(e))).withRequest(request));

					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), innerException, () -> "INNER: " + ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(innerException))).withRequest(request));

					throw new ServletException("Error in generation of output for another error.", e);
				}
			}
		}
		finally {
			// 3. clear the user in the end
			Object user = getContextHandler().removeBean(Context.USER_BEAN_NAME);

			// clear full context so that the next request can have a clean start
			// NOTE: assumes this is the last part of request to be executed that may need the context
			getContextHandler().clear();

			// stop the timer and record stats
			getTimerHandler().stopTimer();
			getSystemRequestStatsService().saveSystemRequestStats(request, (HttpServletResponse) res, user, getTimerHandler().getDurationNano(), 0, 0);
			getTimerHandler().reset();
		}
	}


	protected SecurityUser getPrincipalUser() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication == null) {
			throw new RuntimeException("Cannot find authentication in security context - Authentication is null.");
		}

		// 1. retrieve authenticated principal from security context
		Object principal = authentication.getPrincipal();
		if (principal == null) {
			throw new RuntimeException("Cannot find authentication principal in security context.");
		}
		else if (principal instanceof CrowdUserDetails) {
			CrowdUserDetails crowdUser = (CrowdUserDetails) principal;
			SecurityUser user = getSecurityUserService().getSecurityUserByPseudonym(crowdUser.getUsername());
			if (user == null) {
				throw new FieldValidationException("User with pseudonym [" + crowdUser.getUsername() + "] does not exist in SecurityUser table.", "integrationPseudonym");
			}

			Contact contact = new Contact();
			contact.setFirstName(crowdUser.getFirstName());
			contact.setLastName(crowdUser.getLastName());
			contact.setEmailAddress(crowdUser.getEmail());
			user.setContact(contact);
			return user;
		}
		else if (principal instanceof SecurityUser) {
			return getSecurityUserService().getSecurityUserByName(((SecurityUser) principal).getUserName());
		}
		else if (principal instanceof SpringSecurityUser) {
			SecurityUser user = ((SpringSecurityUser) principal).getSecurityUser();
			Contact contact = new Contact();
			contact.setFirstName("");
			contact.setLastName(user.getDisplayName());
			contact.setEmailAddress(user.getEmailAddress());
			user.setContact(contact);
			return user;
		}
		else if (principal instanceof String) {
			return getSecurityUserService().getSecurityUserByName((String) principal);
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WebApplicationContext getWebApplicationContext() {
		// ugly but works; don't know a better way to get web context
		// filters are defined in main application context and can't be wired with beans from web context
		// the following commented out line for getting web context doesn't work - returns application context
		// WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(this.filterConfig.getServletContext());
		String contextAttributeName = FrameworkServlet.SERVLET_CONTEXT_PREFIX + "jsonAppDispatcherServlet";
		return (WebApplicationContext) this.filterConfig.getServletContext().getAttribute(contextAttributeName);
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	@ValueIgnoringGetter
	public HandlerExceptionResolver getWebExceptionResolver() {
		if (this.webExceptionResolver == null) {
			this.webExceptionResolver = (HandlerExceptionResolver) getWebApplicationContext().getBean("webExceptionResolver");
		}
		return this.webExceptionResolver;
	}


	public void setWebExceptionResolver(HandlerExceptionResolver webExceptionResolver) {
		this.webExceptionResolver = webExceptionResolver;
	}


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}
}
