package com.clifton.security.web;

import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecureMethodPermissionResolver;

import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.Supplier;


/**
 * The handler interface for processing method-level security. This interface includes methods for processing security for a given method.
 *
 * @author MikeH
 */
public interface MethodSecurityHandler {

	/**
	 * Verifies that the current user is authorized to execute requested handler method.
	 * <p>
	 * If the current user is not authorized to execute the requested handler method, an {@link AccessDeniedException} will be thrown.
	 *
	 * @param handlerMethod  the method for which user authorization should be verified
	 * @param targetSupplier a supplier which shall provide a label for the requested resource, such as the requested URI
	 * @param parameterMap   the map of parameters attached to the request to be used for authorization processing
	 */
	void verifySecurityAccess(Method handlerMethod, Supplier<String> targetSupplier, Map<String, ?> parameterMap);


	/**
	 * Returns the permissions required to access the given handlerMethod. This method first checks the secureAnnotation for a
	 * {@link SecureMethodPermissionResolver}, otherwise determines permission level based on methodName (i.e. get = READ), where default
	 * permission returned is WRITE. For saves, we check parameterMap and will return WRITE if ID parameter is present > 0, else CREATE.
	 *
	 * @param handlerMethod        the method for which permissions should be retrieved
	 * @param methodName           the name of the method for which permissions should be retrieved
	 * @param secureAnnotation     the {@link SecureMethod} annotation, if present, for the handlerMethod
	 * @param parameterMap         the map of parameters attached to the request invoking the handlerMethod
	 * @param securityResourceName the name of the {@link com.clifton.core.security.authorization.SecurityResource} associated with the handlerMethod
	 * @return the required permissions for the given method
	 */
	int getRequiredPermissions(Method handlerMethod, String methodName, SecureMethod secureAnnotation, Map<String, ?> parameterMap, String securityResourceName);
}
