package com.clifton.security.web.mvc;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.SecurityResource;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * The <code>SecurityMethodToResourceCache</code> class allows caching and retrieval from cache of {@link SecurityResource} names
 * that correspond to service layer methods.
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding security DAO's.
 * <p>
 * NOTE: This is currently registered in system project for SystemTable
 *
 * @param <T>
 * @author vgomelsky
 */
@Component
public class SecurityMethodToResourceCache<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<Method, String> {

	public static final String RESOURCE_NOT_MAPPED = "RESOURCE_NOT_MAPPED";

	private CacheHandler<Method, String> cacheHandler;


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	public String getSecurityResourceName(Method method) {
		return getCacheHandler().get(getCacheName(), method);
	}


	public void storeSecurityResourceName(Method method, String securityResourceName) {
		getCacheHandler().put(getCacheName(), method, securityResourceName);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, @SuppressWarnings("unused") T bean, Throwable e) {
		if (e == null) {
			getCacheHandler().clear(getCacheName());
			LogUtils.info(getClass(), "Cleared cache for all methods");
		}
	}


	@Override
	public CacheHandler<Method, String> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Method, String> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
