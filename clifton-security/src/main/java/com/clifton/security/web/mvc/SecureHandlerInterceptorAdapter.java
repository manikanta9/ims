package com.clifton.security.web.mvc;


import com.clifton.core.web.mvc.WebRequestUtils;
import com.clifton.security.web.MethodSecurityHandler;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


/**
 * A {@link HandlerInterceptorAdapter} for processing authorization before handling method endpoints mapped through Spring MVC.
 * <p>
 * This interceptor verifies that the current user has appropriate authorization to access the requested method.
 *
 * @author MikeH
 */
public class SecureHandlerInterceptorAdapter extends HandlerInterceptorAdapter {

	private MethodSecurityHandler methodSecurityHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		getMethodSecurityHandler().verifySecurityAccess(((HandlerMethod) handler).getMethod(), () -> WebRequestUtils.getFullURL(request), request.getParameterMap());
		return super.preHandle(request, response, handler);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MethodSecurityHandler getMethodSecurityHandler() {
		return this.methodSecurityHandler;
	}


	public void setMethodSecurityHandler(MethodSecurityHandler methodSecurityHandler) {
		this.methodSecurityHandler = methodSecurityHandler;
	}
}
