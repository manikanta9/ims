package com.clifton.security.secret;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author theodorez
 */
public interface SecuritySecretService {

	////////////////////////////////////////////////////////////////////////////
	////////        SecuritySecret Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	@DoNotAddRequestMapping
	public SecuritySecret getSecuritySecret(int id);


	@DoNotAddRequestMapping
	public SecuritySecret getSecuritySecretPopulated(SecuritySecret secret);


	/**
	 * @return the saved Secret, or null if secret was not valid for saving (the bean is null, secretString matches the placeholder, the bean is new and has an empty secretString)
	 */
	@DoNotAddRequestMapping
	public SecuritySecret saveSecuritySecret(SecuritySecret bean);


	@DoNotAddRequestMapping
	public void deleteSecuritySecret(int id);


	@RequestMapping("securitySecretDecrypt")
	@SecureMethod(permissions = SecurityPermission.PERMISSION_READ)
	public SecuritySecret decryptSecuritySecret(int secretId, String tableName, int entityId, String fieldPath);


	@DoNotAddRequestMapping
	public SecuritySecret decryptSecuritySecret(SecuritySecret bean);


	@DoNotAddRequestMapping
	public boolean doHealthCheck();
}
