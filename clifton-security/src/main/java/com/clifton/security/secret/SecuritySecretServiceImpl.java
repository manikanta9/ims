package com.clifton.security.secret;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


/**
 * @author theodorez
 */
@Service
public class SecuritySecretServiceImpl implements SecuritySecretService {


	private SecuritySecretEvaluationHandler securitySecretEvaluationHandler;

	private AdvancedUpdatableDAO<SecuritySecret, Criteria> securitySecretDAO;

	private DaoLocator daoLocator;

	/**
	 * The location of the server specific key
	 */
	private String serverKeyLocation;

	private String serverKeyChecksum;

	////////////////////////////////////////////////////////////////////////////
	////////        SecuritySecret Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Not accessible from the UI - returns all fields from the DB
	 */
	@Override
	public SecuritySecret getSecuritySecret(int id) {
		return getSecuritySecretDAO().findByPrimaryKey(id);
	}


	/**
	 * Ensures that the secret is correctly hydrated. Used in cases where only the secretID is stored (i.e. SystemBeans)
	 */
	@Override
	public SecuritySecret getSecuritySecretPopulated(SecuritySecret secret) {
		if (secret != null && secret.getId() != null && secret.getEncryptionKey() == null) {
			return getSecuritySecret(secret.getId());
		}
		return secret;
	}


	/**
	 * Not accessible from the UI - should only be called by save methods on entities that make use of the secret
	 */
	@Override
	@Transactional
	public SecuritySecret saveSecuritySecret(SecuritySecret bean) {
		//If the bean isn't new (i.e. preexisting) OR the secret string value is populated (includes new beans), then it is valid to save
		if (bean != null && (!bean.isNewBean() || !StringUtils.isEmpty(bean.getSecretString()))) {
			//If the secret string is populated but it is equal to the placeholder, that means the value hasn't been modified
			//  Then return the original unmodified bean
			if (StringUtils.isEqual(bean.getSecretString(), EncryptionUtilsAES.ENCRYPTED_STRING_PLACEHOLDER)) {
				return bean;
			}
			SecuritySecretUtils.encryptSecuritySecret(bean, getSecuritySecretDAO(), getServerKeyLocation(), getServerKeyChecksum());
			return getSecuritySecretDAO().save(bean);
		}
		return null;
	}


	@Override
	public void deleteSecuritySecret(int id) {
		getSecuritySecretDAO().delete(id);
	}


	/**
	 * Accessible from the UI - returns the decrypted secret for the given secret ID, table, entity ID, and field path
	 */
	@Override
	public SecuritySecret decryptSecuritySecret(int secretId, String tableName, int entityId, String fieldPath) {
		ReadOnlyDAO<?> systemTableDao = getDaoLocator().locate(tableName);
		IdentityObject entity = systemTableDao.findByPrimaryKey(entityId);
		getSecuritySecretEvaluationHandler().evaluateEntitySystemCondition(entity, tableName);
		int entitySecretId = getSecuritySecretEvaluationHandler().getEntitySecretIdFromFieldPath(entity, fieldPath);
		ValidationUtils.assertTrue(secretId == entitySecretId, "The secret specified is invalid for the given entity.");
		SecuritySecret bean = getSecuritySecretDAO().findByPrimaryKey(secretId);
		ValidationUtils.assertTrue(bean != null, "Cannot find secret with ID " + secretId);
		return decryptSecuritySecret(bean);
	}


	/**
	 * Not accessible from the UI - returns the decrypted secret for a given secret bean
	 */
	@Override
	public SecuritySecret decryptSecuritySecret(SecuritySecret bean) {
		return SecuritySecretUtils.decryptSecuritySecret(bean, getServerKeyLocation(), getServerKeyChecksum());
	}


	/**
	 * Not accessible from the UI - returns True if encryption/decryption is functioning correctly
	 */
	@Override
	public boolean doHealthCheck() {
		final String secretValue = "test";
		SecuritySecret bean = new SecuritySecret();
		bean.setSecretString(secretValue);

		SecuritySecretUtils.encryptSecuritySecret(bean, getSecuritySecretDAO(), getServerKeyLocation(), getServerKeyChecksum());

		SecuritySecret decryptedBean = decryptSecuritySecret(bean);

		return StringUtils.isEqual(secretValue, decryptedBean.getSecretString());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SecuritySecret, Criteria> getSecuritySecretDAO() {
		return this.securitySecretDAO;
	}


	public void setSecuritySecretDAO(AdvancedUpdatableDAO<SecuritySecret, Criteria> securitySecretDAO) {
		this.securitySecretDAO = securitySecretDAO;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public String getServerKeyLocation() {
		return this.serverKeyLocation;
	}


	public void setServerKeyLocation(String serverKeyLocation) {
		this.serverKeyLocation = serverKeyLocation;
	}


	public String getServerKeyChecksum() {
		return this.serverKeyChecksum;
	}


	public void setServerKeyChecksum(String serverKeyChecksum) {
		this.serverKeyChecksum = serverKeyChecksum;
	}


	public SecuritySecretEvaluationHandler getSecuritySecretEvaluationHandler() {
		return this.securitySecretEvaluationHandler;
	}


	public void setSecuritySecretEvaluationHandler(SecuritySecretEvaluationHandler securitySecretEvaluationHandler) {
		this.securitySecretEvaluationHandler = securitySecretEvaluationHandler;
	}
}
