package com.clifton.security.secret;

import com.clifton.core.beans.IdentityObject;


/**
 * @author theodorez
 */
public interface SecuritySecretEvaluationHandler {

	public void evaluateEntitySystemCondition(IdentityObject entity, String systemTableName);


	public int getEntitySecretIdFromFieldPath(IdentityObject entity, String fieldPath);
}
