package com.clifton.security.secret;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;


/**
 * @author theodorez
 */
public class SecuritySecret extends BaseEntity<Integer> {

	/**
	 * The value to be protected, will be encrypted with the serverkey and encryption key
	 */
	private byte[] secretValue;

	/**
	 * Readable version of the secretValue - NOT STORED IN DB
	 */
	@NonPersistentField
	private String secretString;

	/**
	 * The key that is specific to this secret, will be combined with the server key for encryption/decryption
	 */
	private byte[] encryptionKey;

	/**
	 * The initialization vector provides a random set of bytes that will be used to initialize the AES encryption algorithm
	 */
	private byte[] initializationVector;


	public byte[] getSecretValue() {
		return this.secretValue;
	}


	public void setSecretValue(byte[] secretValue) {
		this.secretValue = secretValue;
	}


	public String getSecretString() {
		return this.secretString;
	}


	public void setSecretString(String secretString) {
		this.secretString = secretString;
	}


	public byte[] getEncryptionKey() {
		return this.encryptionKey;
	}


	public void setEncryptionKey(byte[] encryptionKey) {
		this.encryptionKey = encryptionKey;
	}


	public byte[] getInitializationVector() {
		return this.initializationVector;
	}


	public void setInitializationVector(byte[] initializationVector) {
		this.initializationVector = initializationVector;
	}
}

