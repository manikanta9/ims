package com.clifton.security.secret;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;


/**
 * Ensures that Secrets are deleted after the object they are linked to is deleted
 *
 * @author theodorez
 */
@Component
public class SecuritySecretObserver<T extends SecretAware> extends BaseDaoEventObserver<T> {

	private SecuritySecretService securitySecretService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (DaoEventTypes.DELETE == event && !CollectionUtils.isEmpty(bean.getSecuritySecretIds())) {
			for (Integer securityId : CollectionUtils.getIterable(bean.getSecuritySecretIds())) {
				getSecuritySecretService().deleteSecuritySecret(securityId);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
