package com.clifton.security.secret;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.encryption.EncryptionUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.core.util.validation.ValidationUtils;

import java.nio.charset.StandardCharsets;


/**
 * @author theodorez
 */
public class SecuritySecretUtils {

	public static void encryptSecuritySecret(SecuritySecret bean, ReadOnlyDAO<?> dao, String serverKeyLocation, String serverKeyChecksum) {
		byte[] rowKey;
		byte[] iv;
		if (bean.isNewBean() && bean.getEncryptionKey() == null) {
			//generate and set new encryption key and IV
			rowKey = EncryptionUtilsAES.generateKey();
		}
		else {
			//Use existing key and IV to encrypt the new secret
			SecuritySecret existingSecret = (SecuritySecret) dao.findByPrimaryKey(bean.getId());
			rowKey = existingSecret.getEncryptionKey();
		}

		//Rotate initialization vector
		iv = EncryptionUtilsAES.generateInitializationVector();

		if (StringUtils.isEmpty(bean.getSecretString())) {
			bean.setSecretString(StringUtils.EMPTY_STRING);
		}
		byte[] finalKey = EncryptionUtilsAES.hashKeys(EncryptionUtilsAES.readAndOrCreateKeyFile(serverKeyLocation, serverKeyChecksum), rowKey);
		bean.setSecretValue(EncryptionUtilsAES.encrypt(bean.getSecretString().getBytes(StandardCharsets.UTF_8), finalKey, iv));

		EncryptionUtils.cleanupByteArray(finalKey);
		bean.setEncryptionKey(rowKey);
		bean.setInitializationVector(iv);
		bean.setSecretString(null);
	}


	public static SecuritySecret decryptSecuritySecret(SecuritySecret bean, String serverKeyLocation, String serverKeyChecksum) {
		ValidationUtils.assertNotNull(bean, "Cannot decrypt null secret.");
		byte[] finalKey = null;
		try {
			ValidationUtils.assertTrue(bean.getEncryptionKey() != null && bean.getEncryptionKey().length > 0, "The Secret has no key and cannot be decrypted. ID: " + bean.getId());
			ValidationUtils.assertTrue(bean.getInitializationVector() != null && bean.getInitializationVector().length > 0, "The Secret has no initialization vector and cannot be decrypted. ID: " + bean.getId());
			finalKey = EncryptionUtilsAES.hashKeys(EncryptionUtilsAES.readAndOrCreateKeyFile(serverKeyLocation, serverKeyChecksum), bean.getEncryptionKey());
			bean.setSecretString(new String(EncryptionUtilsAES.decrypt(bean.getSecretValue(), finalKey, bean.getInitializationVector())));
			if (StringUtils.isEmpty(bean.getSecretString())) {
				bean.setSecretString(null);
			}
			return bean;
			//may need to garbage collect to wipe out the string pool? Unencrypted password could still be in memory
		}
		finally {
			EncryptionUtils.cleanupByteArray(finalKey);
		}
	}


	public static SecuritySecret sanitizeSecuritySecret(SecuritySecret bean) {
		bean.setSecretString(EncryptionUtilsAES.ENCRYPTED_STRING_PLACEHOLDER);
		bean.setSecretValue(null);
		bean.setEncryptionKey(null);
		bean.setInitializationVector(null);
		return bean;
	}
}
