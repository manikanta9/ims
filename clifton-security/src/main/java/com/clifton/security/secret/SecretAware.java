package com.clifton.security.secret;

import com.clifton.core.beans.IdentityObject;

import java.util.List;


/**
 * Should be implemented by DTO's that make use of SecuritySecrets to ensure that the
 * secrets are deleted after the object is deleted.
 *
 * @author theodorez
 */
public interface SecretAware extends IdentityObject {

	/**
	 * Returns all secret IDs associated with the DTO implementing the interface.
	 */
	public List<Integer> getSecuritySecretIds();
}
