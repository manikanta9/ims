package com.clifton.security.credential;

import com.clifton.security.credential.search.SecurityCredentialSearchForm;

import java.util.List;


/**
 * @author mwacker
 */
public interface SecurityCredentialService {

	public SecurityCredential getSecurityCredential(short id);

	public SecurityCredential getSecurityCredentialByName(String name);


	public List<SecurityCredential> getSecurityCredentialList(SecurityCredentialSearchForm searchForm);


	public SecurityCredential saveSecurityCredential(SecurityCredential bean);


	public void deleteSecurityCredential(short id);
}
