package com.clifton.security.credential;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.credential.search.SecurityCredentialSearchForm;
import com.clifton.security.secret.SecuritySecretService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author mwacker
 */
@Service
public class SecurityCredentialServiceImpl implements SecurityCredentialService {

	private AdvancedUpdatableDAO<SecurityCredential, Criteria> securityCredentialDAO;

	private DaoNamedEntityCache<SecurityCredential> securityCredentialCache;

	private SecuritySecretService securitySecretService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityCredential getSecurityCredential(short id) {
		return getSecurityCredentialDAO().findByPrimaryKey(id);
	}


	@Override
	public SecurityCredential getSecurityCredentialByName(String name) {
		return getSecurityCredentialCache().getBeanForKeyValueStrict(getSecurityCredentialDAO(), name);
	}


	@Override
	public List<SecurityCredential> getSecurityCredentialList(SecurityCredentialSearchForm searchForm) {
		return getSecurityCredentialDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SecurityCredential saveSecurityCredential(SecurityCredential bean) {
		ValidationUtils.assertFalse(bean.getPasswordSecret() == null || StringUtils.isEmpty(bean.getPasswordSecret().getSecretString()), "Password Secret is required");
		bean.setPasswordSecret(ObjectUtils.coalesce(getSecuritySecretService().saveSecuritySecret(bean.getPasswordSecret()), bean.getPasswordSecret()));
		return getSecurityCredentialDAO().save(bean);
	}


	@Override
	public void deleteSecurityCredential(short id) {
		getSecurityCredentialDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SecurityCredential, Criteria> getSecurityCredentialDAO() {
		return this.securityCredentialDAO;
	}


	public void setSecurityCredentialDAO(AdvancedUpdatableDAO<SecurityCredential, Criteria> securityCredentialDAO) {
		this.securityCredentialDAO = securityCredentialDAO;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public DaoNamedEntityCache<SecurityCredential> getSecurityCredentialCache() {
		return this.securityCredentialCache;
	}


	public void setSecurityCredentialCache(DaoNamedEntityCache<SecurityCredential> securityCredentialCache) {
		this.securityCredentialCache = securityCredentialCache;
	}
}
