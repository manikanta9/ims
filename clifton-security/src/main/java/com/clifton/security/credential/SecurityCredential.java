package com.clifton.security.credential;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.security.secret.SecuritySecret;


/**
 * The <code>SecurityCredential</code> stores credentials used to access websites and databases for integration purposes.
 *
 * @author mwacker
 */
@CacheByName
public class SecurityCredential extends NamedEntity<Short> {

	private String userName;
	private SecuritySecret passwordSecret;

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////

	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public SecuritySecret getPasswordSecret() {
		return this.passwordSecret;
	}


	public void setPasswordSecret(SecuritySecret passwordSecret) {
		this.passwordSecret = passwordSecret;
	}
}
