package com.clifton.security.encryption.transport;

/**
 * @author theodorez
 */
public class SecurityRSATransportEncryptionKeyDetail {

	private String publicKeyFileLocation;
	private String publicKeyFileChecksum;

	private String privateKeyFileChecksum;


	public SecurityRSATransportEncryptionKeyDetail() {
		//
	}


	public SecurityRSATransportEncryptionKeyDetail(String publicKeyFileLocation, String publicKeyFileChecksum) {
		this.publicKeyFileLocation = publicKeyFileLocation;
		this.publicKeyFileChecksum = publicKeyFileChecksum;
	}


	public String getPublicKeyFileLocation() {
		return this.publicKeyFileLocation;
	}


	public void setPublicKeyFileLocation(String publicKeyFileLocation) {
		this.publicKeyFileLocation = publicKeyFileLocation;
	}


	public String getPublicKeyFileChecksum() {
		return this.publicKeyFileChecksum;
	}


	public void setPublicKeyFileChecksum(String publicKeyFileChecksum) {
		this.publicKeyFileChecksum = publicKeyFileChecksum;
	}


	public String getPrivateKeyFileChecksum() {
		return this.privateKeyFileChecksum;
	}


	public void setPrivateKeyFileChecksum(String privateKeyFileChecksum) {
		this.privateKeyFileChecksum = privateKeyFileChecksum;
	}
}
