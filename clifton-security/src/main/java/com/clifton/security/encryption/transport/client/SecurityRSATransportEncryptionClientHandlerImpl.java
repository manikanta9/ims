package com.clifton.security.encryption.transport.client;

import com.clifton.core.util.encryption.EncryptionUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.core.util.encryption.EncryptionUtilsRSA;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.encryption.transport.SecurityRSATransportEncryptionKeyDetail;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.PublicKey;


/**
 * @author theodorez
 */
public class SecurityRSATransportEncryptionClientHandlerImpl implements SecurityRSATransportEncryptionClientHandler {

	private String publicEncryptionKeyLocation;
	private String publicEncryptionKeyChecksum;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a random symmetric (AES) key and iv and encrypts the plaintext with that symmetric key and iv
	 * <p>
	 * Then encrypts the key and iv with the public key.
	 */
	@Override
	public String encryptValueForTransport(String plainText) {
		ValidationUtils.assertNotEmpty(getPublicEncryptionKeyLocation(), "Cannot encrypt value for transport when Public Key location is empty.");
		StringBuilder output = new StringBuilder();
		byte[] symmetricKey = EncryptionUtilsAES.generateKey();
		byte[] symmetricIv = EncryptionUtilsAES.generateInitializationVector();
		output.append(EncryptionUtils.byteArrayToHexString(symmetricKey));
		output.append("|");
		output.append(EncryptionUtils.byteArrayToHexString(symmetricIv));

		output = new StringBuilder(EncryptionUtilsRSA.encryptValueWithRSAPublicKey(output.toString(), getPublicEncryptionKeyLocation(), getPublicEncryptionKeyChecksum()));
		output.append("|");
		output.append(EncryptionUtils.byteArrayToHexString(EncryptionUtilsAES.encrypt(plainText.getBytes(StandardCharsets.UTF_8), symmetricKey, symmetricIv)));
		return output.toString();
	}


	@Override
	public SecurityRSATransportEncryptionKeyDetail getPublicEncryptionKeyDetails() {
		return new SecurityRSATransportEncryptionKeyDetail(getPublicEncryptionKeyLocation(), getPublicEncryptionKeyChecksum());
	}


	@Override
	public void setupPublicEncryptionKey(String key) {
		ValidationUtils.assertNotEmpty(getPublicEncryptionKeyLocation(), "An encryption key location must be specified in the properties file.");
		File keyFile = new File(getPublicEncryptionKeyLocation());
		PublicKey publicKey = EncryptionUtilsRSA.getPublicKeyRSAFromString(key);
		EncryptionUtilsRSA.savePublicKeyToFile(keyFile, publicKey);
		setPublicEncryptionKeyChecksum(EncryptionUtilsRSA.getKeyFileChecksum(keyFile));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getPublicEncryptionKeyLocation() {
		return this.publicEncryptionKeyLocation;
	}


	public void setPublicEncryptionKeyLocation(String publicEncryptionKeyLocation) {
		this.publicEncryptionKeyLocation = publicEncryptionKeyLocation;
	}


	public String getPublicEncryptionKeyChecksum() {
		return this.publicEncryptionKeyChecksum;
	}


	public void setPublicEncryptionKeyChecksum(String publicEncryptionKeyChecksum) {
		this.publicEncryptionKeyChecksum = publicEncryptionKeyChecksum;
	}
}
