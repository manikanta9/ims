package com.clifton.security.encryption.transport.server;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.encryption.EncryptionUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.core.util.encryption.EncryptionUtilsRSA;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.File;
import java.security.PublicKey;
import java.util.List;


/**
 * @author theodorez
 */
public class SecurityRSATransportEncryptionServerHandlerImpl implements SecurityRSATransportEncryptionServerHandler {

	private String publicEncryptionKeyLocation;
	private String publicEncryptionKeyChecksum;

	private String privateEncryptionKeyLocation;
	private String privateEncryptionKeyChecksum;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public String decryptValueFromTransport(String cipherText) {
		ValidationUtils.assertNotEmpty(getPrivateEncryptionKeyLocation(), "Cannot decrypt value when Private Key location is empty");
		//Key and IV that were encrypted using the public key are in index 0
		//Encrypted ciphertext is in index 1
		List<String> messageComponents = StringUtils.delimitedStringToList(cipherText, "\\|");
		ValidationUtils.assertNotEmpty(messageComponents, "Cannot decrypt empty cipher text");
		//Key in 0, IV in 1
		List<String> keyAndIv = StringUtils.delimitedStringToList(EncryptionUtilsRSA.decryptValueWithRSAPrivateKey(messageComponents.get(0), getPrivateEncryptionKeyLocation(), getPrivateEncryptionKeyChecksum()), "\\|");
		ValidationUtils.assertNotEmpty(keyAndIv, "The key and initialization vector were not properly decrypted.");
		byte[] key = EncryptionUtils.hexStringToByteArray(keyAndIv.get(0));
		byte[] iv = EncryptionUtils.hexStringToByteArray(keyAndIv.get(1));

		return new String(EncryptionUtilsAES.decrypt(EncryptionUtils.hexStringToByteArray(messageComponents.get(1)), key, iv));
	}


	/**
	 * Validates the keys if they exist, otherwise creates new ones.
	 * <p>
	 * Returns a string representation of the public key
	 */
	@Override
	public String getPublicEncryptionKey() {
		//Validate keys if they do not exist, create them if they do not
		EncryptionUtilsRSA.setupRSAEncryptionKeys(getPublicEncryptionKeyLocation(), getPublicEncryptionKeyChecksum(), getPrivateEncryptionKeyLocation(), getPrivateEncryptionKeyChecksum());

		File keyFile = new File(getPublicEncryptionKeyLocation());
		EncryptionUtilsRSA.validateRSAKeyFile(keyFile, getPublicEncryptionKeyChecksum());
		PublicKey key = EncryptionUtilsRSA.readFileToRSAPublicKey(keyFile);
		return EncryptionUtilsRSA.getPublicKeyRSAAsString(key);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getPublicEncryptionKeyLocation() {
		return this.publicEncryptionKeyLocation;
	}


	public void setPublicEncryptionKeyLocation(String publicEncryptionKeyLocation) {
		this.publicEncryptionKeyLocation = publicEncryptionKeyLocation;
	}


	public String getPublicEncryptionKeyChecksum() {
		return this.publicEncryptionKeyChecksum;
	}


	public void setPublicEncryptionKeyChecksum(String publicEncryptionKeyChecksum) {
		this.publicEncryptionKeyChecksum = publicEncryptionKeyChecksum;
	}


	public String getPrivateEncryptionKeyLocation() {
		return this.privateEncryptionKeyLocation;
	}


	public void setPrivateEncryptionKeyLocation(String privateEncryptionKeyLocation) {
		this.privateEncryptionKeyLocation = privateEncryptionKeyLocation;
	}


	public String getPrivateEncryptionKeyChecksum() {
		return this.privateEncryptionKeyChecksum;
	}


	public void setPrivateEncryptionKeyChecksum(String privateEncryptionKeyChecksum) {
		this.privateEncryptionKeyChecksum = privateEncryptionKeyChecksum;
	}
}
