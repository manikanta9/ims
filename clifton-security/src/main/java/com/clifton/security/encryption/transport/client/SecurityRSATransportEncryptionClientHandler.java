package com.clifton.security.encryption.transport.client;

import com.clifton.security.encryption.transport.SecurityRSATransportEncryptionKeyDetail;


/**
 * Handler for encrypting data for transport
 *
 * @author theodorez
 */
public interface SecurityRSATransportEncryptionClientHandler {

	public String encryptValueForTransport(String plainText);


	public SecurityRSATransportEncryptionKeyDetail getPublicEncryptionKeyDetails();


	public void setupPublicEncryptionKey(String key);
}
