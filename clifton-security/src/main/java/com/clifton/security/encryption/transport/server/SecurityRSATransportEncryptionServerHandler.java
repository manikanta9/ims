package com.clifton.security.encryption.transport.server;

/**
 * Handler for decrypting values
 *
 * @author theodorez
 */
public interface SecurityRSATransportEncryptionServerHandler {

	public String decryptValueFromTransport(String cipherText);


	public String getPublicEncryptionKey();
}
