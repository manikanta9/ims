package com.clifton.security.user.comparison;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;

import java.util.List;


/**
 * The <code>SecurityGroupHasCurrentUserComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user is a member of the group allowed by this comparison.
 *
 * @author vgomelsky
 */
public class SecurityGroupHasCurrentUserComparison implements Comparison<IdentityObject> {

	private SecurityUserService securityUserService;

	/**
	 * Id of user group that make this comparison evaluate to true.
	 */
	private short securityGroupId;


	/**
	 * Return true to keep comparison: check in group
	 */
	protected boolean isEqualComparison() {
		return true;
	}


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		SecurityUser currentUser = getUser(bean);
		AssertUtils.assertNotNull(currentUser, getUserLabel() + " is not set.");

		boolean result = !isEqualComparison();
		List<SecurityUser> userList = getSecurityUserService().getSecurityUserListByGroup(getSecurityGroupId());
		for (SecurityUser user : CollectionUtils.getIterable(userList)) {
			if (currentUser.getId().equals(user.getId())) {
				result = isEqualComparison();
				break;
			}
		}

		// record comparison result message
		if (context != null) {
			String messageNotEqual = "(" + getUserLabel() + " " + currentUser.getUserName() + " is not in group " + getSecurityUserService().getSecurityGroup(getSecurityGroupId()).getName() + ")";
			String messageEqual = "(" + getUserLabel() + " " + currentUser.getUserName() + " is in group " + getSecurityUserService().getSecurityGroup(getSecurityGroupId()).getName() + ")";

			if (result) {
				context.recordTrueMessage(isEqualComparison() ? messageEqual : messageNotEqual);
			}
			else {
				context.recordFalseMessage(isEqualComparison() ? messageNotEqual : messageEqual);
			}
		}

		return result;
	}


	/**
	 * Returns the user that will be used for checking user group membership
	 */
	protected SecurityUser getUser(@SuppressWarnings("unused") IdentityObject bean) {
		return getSecurityUserService().getSecurityUserCurrent();
	}


	protected String getUserLabel() {
		return "Current User";
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}
}
