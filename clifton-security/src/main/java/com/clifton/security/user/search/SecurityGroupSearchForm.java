package com.clifton.security.user.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;


/**
 * @author manderson
 */
public class SecurityGroupSearchForm extends BaseEntitySearchForm {

	@SearchField(leftJoin = true, searchField = "name,description,ownerSecurityUser.displayName,emailAddress")
	private String searchPattern;

	@SearchField
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField(searchField = "userList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short userId;

	// Custom Search Field where Group is Linked to Selected Hierarchy
	// Used for Company Departments Tags
	private Short securityGroupHierarchyId;

	@SearchField(searchField = "ownerSecurityUser.id")
	private Short ownerSecurityUserId;

	@SearchField
	private String name;

	@SearchField
	private String emailAddress;

	@SearchField
	private String integrationPseudonym;

	@SearchField
	private String description;

	@SearchField
	private Boolean admin;

	@SearchField
	private Boolean disabled;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Short getUserId() {
		return this.userId;
	}


	public void setUserId(Short userId) {
		this.userId = userId;
	}


	public Short getSecurityGroupHierarchyId() {
		return this.securityGroupHierarchyId;
	}


	public void setSecurityGroupHierarchyId(Short securityGroupHierarchyId) {
		this.securityGroupHierarchyId = securityGroupHierarchyId;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getOwnerSecurityUserId() {
		return this.ownerSecurityUserId;
	}


	public void setOwnerSecurityUserId(Short ownerSecurityUserId) {
		this.ownerSecurityUserId = ownerSecurityUserId;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getIntegrationPseudonym() {
		return this.integrationPseudonym;
	}


	public void setIntegrationPseudonym(String integrationPseudonym) {
		this.integrationPseudonym = integrationPseudonym;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getAdmin() {
		return this.admin;
	}


	public void setAdmin(Boolean admin) {
		this.admin = admin;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
