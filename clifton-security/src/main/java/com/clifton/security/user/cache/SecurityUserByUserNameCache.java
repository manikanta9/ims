package com.clifton.security.user.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Component;


/**
 * The <code>SecurityUserByPseudonymCache</code> class provides caching and retrieval from cache of SecurityUser
 * objects by user name.
 *
 * @author vgomelsky
 */
@Component
public class SecurityUserByUserNameCache extends SelfRegisteringSingleKeyDaoCache<SecurityUser, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "userName";
	}


	@Override
	protected String getBeanKeyValue(SecurityUser bean) {
		return bean.getUserName();
	}


	@Override
	protected boolean isLogEmptyResults() {
		return true;
	}
}


