package com.clifton.security.user;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;


/*
 * The <code>SecurityUser</code> class represents a user DTO.
 *
 * @author vgomelsky
 */
public class SpringSecurityUser implements UserDetails {

	private SecurityUser securityUser;


	private SpringSecurityUser() {
		//force use of explicit constructor
	}


	public SpringSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}


	@Override
	public String getPassword() {
		return null;
	}


	@Override
	public String getUsername() {
		if (this.securityUser != null) {
			return this.securityUser.getUserName();
		}
		return null;
	}


	@Override
	public boolean isAccountNonExpired() {
		return true;
	}


	@Override
	public boolean isAccountNonLocked() {
		return true;
	}


	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}


	@Override
	public boolean isEnabled() {
		if (getSecurityUser() != null) {
			return !getSecurityUser().isDisabled();
		}
		return false;
	}
}
