package com.clifton.security.user;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>SecurityGroup</code> class represents a group DTO.
 *
 * @author vgomelsky
 */
@CacheByName
public class SecurityGroup extends NamedEntity<Short> {

	public static final String INVESTMENT_ACCOUNT_ADMIN = "Investment Account Admins";
	public static final String ACCOUNTING = "Accounting";
	public static final String OPERATIONS = "Operations";
	public static final String COMPLIANCE = "Compliance";
	public static final String CLIENT_ADMIN = "Client Admins";
	public static final String BILLING_ADMIN = "Billing Admins";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private String integrationPseudonym;
	private boolean admin;
	/**
	 * Used for SOX controls. The owner is the {@link SecurityUser} responsible for reviewing and approving changes.
	 */
	private SecurityUser ownerSecurityUser;

	/**
	 * Email email address for this group, providing a contact for the team or organizational unit associated with the group.
	 */
	private String emailAddress;

	/**
	 * Determine whether the use group is disabled (preferred over deleting to preserve audit trail)
	 */
	private boolean disabled;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isAdmin() {
		return this.admin;
	}


	public void setAdmin(boolean admin) {
		this.admin = admin;
	}


	public String getIntegrationPseudonym() {
		return this.integrationPseudonym;
	}


	public void setIntegrationPseudonym(String integrationPseudonym) {
		this.integrationPseudonym = integrationPseudonym;
	}


	public SecurityUser getOwnerSecurityUser() {
		return this.ownerSecurityUser;
	}


	public void setOwnerSecurityUser(SecurityUser ownerSecurityUser) {
		this.ownerSecurityUser = ownerSecurityUser;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
