package com.clifton.security.user.comparison;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;


/**
 * The <code>SecurityUserIsCurrentUserComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user is the specified user.
 *
 * @author vgomelsky
 */
public class SecurityUserIsCurrentUserComparison implements Comparison<IdentityObject> {

	private SecurityUserService securityUserService;

	/**
	 * Id of the user that make this comparison evaluate to true.
	 */
	private short securityUserId;


	/**
	 * Return false to reverse the comparison: check not equal
	 */
	protected boolean isEqualComparison() {
		return true;
	}


	@Override
	public boolean evaluate(@SuppressWarnings("unused") IdentityObject bean, ComparisonContext context) {
		SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(currentUser, "Current user is not set.");

		boolean result = false;
		if (getSecurityUserId() == currentUser.getId()) {
			result = true;
		}
		if (!isEqualComparison()) {
			result = !result;
		}

		// record comparison result message
		if (context != null) {
			SecurityUser securityUser = getSecurityUserService().getSecurityUser(this.securityUserId);
			String messageNotEqual = "(Current user '" + currentUser.getLabel() + "' is not '" + securityUser.getLabel() + "')";
			String messageEqual = "(Current user is '" + securityUser.getLabel() + "')";

			if (result) {
				context.recordTrueMessage(isEqualComparison() ? messageEqual : messageNotEqual);
			}
			else {
				context.recordFalseMessage(isEqualComparison() ? messageNotEqual : messageEqual);
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public short getSecurityUserId() {
		return this.securityUserId;
	}


	public void setSecurityUserId(short securityUserId) {
		this.securityUserId = securityUserId;
	}
}
