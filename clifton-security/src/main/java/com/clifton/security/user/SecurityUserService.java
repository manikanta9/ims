package com.clifton.security.user;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.security.user.search.SecurityGroupSearchForm;
import com.clifton.security.user.search.SecurityUserGroupSearchForm;
import com.clifton.security.user.search.SecurityUserSearchForm;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>SecurityUserService</code> defines methods for working with user objects.
 *
 * @author vgomelsky
 */
public interface SecurityUserService {

	////////////////////////////////////////////////////////////////////////////
	////////             SecurityGroup Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityGroup getSecurityGroup(short id);


	public SecurityGroup getSecurityGroupByName(String name);


	/**
	 * Returns a list of all Groups that are used as Teams, i.e. name starts with "Team"
	 */
	public List<SecurityGroup> getSecurityGroupTeamList();


	@ResponseBody
	@SecureMethod(disableSecurity = true)
	public boolean isSecurityUserCurrentInGroup(String groupName);


	@ResponseBody
	@SecureMethod(disableSecurity = true)
	public boolean isSecurityUserInGroup(short userId, String groupName);


	/**
	 * Unsecured method for retrieving the list of groups the current user is in
	 */
	@SecureMethod(disableSecurity = true)
	public List<SecurityGroup> getSecurityGroupListForCurrentUser();


	public List<SecurityGroup> getSecurityGroupListByUser(short userId);


	public List<SecurityGroup> getSecurityGroupList(SecurityGroupSearchForm searchForm);


	public SecurityGroup saveSecurityGroup(SecurityGroup securityGroup);


	public void deleteSecurityGroup(short id);


	////////////////////////////////////////////////////////////////////////////
	////////             SecurityUser Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUser getSecurityUser(short id);


	public SecurityUser getSecurityUserByName(String userName);


	public SecurityUser getSecurityUserByPseudonym(String pseudonym);


	/**
	 * Returns current {@link SecurityUser} object associated with current request (thread).
	 */
	@SecureMethod(disableSecurity = true)
	public SecurityUser getSecurityUserCurrent();


	/**
	 * This method returns all users and is unsecured.  It is used only by UI to cache users for created by / updated by columns
	 */
	@SecureMethod(disableSecurity = true)
	public List<SecurityUser> getSecurityUserList();


	/**
	 * Returns a list of all admin users
	 */
	public List<SecurityUser> getSecurityUserListByAdmin();


	public List<SecurityUser> getSecurityUserListByGroup(short groupId);


	public List<SecurityUser> getSecurityUserList(SecurityUserSearchForm searchForm);


	public SecurityUser saveSecurityUser(SecurityUser securityUser);


	public void deleteSecurityUser(short id);

	////////////////////////////////////////////////////////////////////////////
	////////          SecurityUserGroup Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserGroup getSecurityUserGroup(int id);


	public List<SecurityUserGroup> getSecurityUserGroupList(SecurityUserGroupSearchForm searchForm);


	/**
	 * Used solely for replication, which requires a save method to exist that conforms to our naming conventions.
	 * Previously only the 'linkSecurityUserToGroup' method existed.
	 */
	@DoNotAddRequestMapping(allowAPI = true)
	public SecurityUserGroup saveSecurityUserGroup(SecurityUserGroup securityUserGroup);


	/**
	 * Links the specified user to the specified group.
	 */
	public SecurityUserGroup linkSecurityUserToGroup(short userId, short groupId);


	/**
	 * Used solely for replication, which requires a delete method to exist that conforms to our naming conventions.
	 */
	@DoNotAddRequestMapping(allowAPI = true)
	public void deleteSecurityUserGroup(int id);


	/**
	 * Removes the specified user form the specified group.
	 */
	@SecureMethod(dtoClass = SecurityUserGroup.class)
	public void unlinkSecurityUserFromGroup(short userId, short groupId);
}
