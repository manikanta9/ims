package com.clifton.security.user.comparison;

import com.clifton.core.comparison.Comparison;


/**
 * The <code>SecurityGroupDoesNotHaveUserPropertyComparison</code> class is a {@link Comparison} that evaluates to true
 * if the user identified by bean's property DOES NOT belong to the specified user group.
 *
 * @author vgomelsky
 */
public class SecurityGroupDoesNotHaveUserPropertyComparison extends SecurityGroupHasUserPropertyComparison {

	/**
	 * Return false to reverse the comparison: check not in group
	 */
	@Override
	protected boolean isEqualComparison() {
		return false;
	}
}
