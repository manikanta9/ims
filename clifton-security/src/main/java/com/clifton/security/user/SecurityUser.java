package com.clifton.security.user;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.common.Contact;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.StringUtils;
import com.clifton.security.shared.user.User;


/*
 * The <code>SecurityUser</code> class represents a user DTO.
 *
 * @author vgomelsky
 */
public class SecurityUser extends BaseEntity<Short> implements LabeledObject, com.clifton.core.security.SecurityUser {

	/**
	 * System user account used to run various system processes. Not a real person.
	 */
	public static final String SYSTEM_USER = "systemuser";

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private String userName;

	/**
	 * Optional user name that can be used for display purposes instead of user name.
	 * For example, "Vlad" vs. "vgomelsky"
	 */
	private String displayName;
	private String integrationPseudonym;
	/**
	 * Optional email address added to SecurityUser
	 */
	private String emailAddress;
	private boolean disabled;
	@NonPersistentField
	private Contact contact;
	private Integer contactIdentifier;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public User toUser() {
		User user = new User();
		if (getId() != null) {
			user.setId(getId());
		}
		user.setUserName(getUserName());
		user.setDisplayName(getDisplayName());
		return user;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getLabel() {
		return this.displayName == null ? this.userName : this.displayName;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",userName=");
		result.append(this.userName);
		result.append('}');
		return result.toString();
	}


	@Override
	public String getEmailAddress() {

		return StringUtils.coalesce(this.emailAddress, getContact() != null ? getContact().getEmailAddress() : null);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getIntegrationPseudonym() {
		return this.integrationPseudonym;
	}


	public void setIntegrationPseudonym(String integrationPseudonym) {
		this.integrationPseudonym = integrationPseudonym;
	}


	public boolean isDisabled() {
		return this.disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}


	public Contact getContact() {
		return this.contact;
	}


	public void setContact(Contact contact) {
		this.contact = contact;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public Integer getContactIdentifier() {
		return this.contactIdentifier;
	}


	public void setContactIdentifier(Integer contactIdentifier) {
		this.contactIdentifier = contactIdentifier;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
