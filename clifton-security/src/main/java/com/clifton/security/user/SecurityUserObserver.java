package com.clifton.security.user;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.security.system.SecuritySystemService;
import com.clifton.security.system.SecuritySystemUser;
import com.clifton.security.system.search.SecuritySystemUserSearchForm;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * The {@link SecurityUserObserver} is an observer on {@link SecurityUser} that updates the
 * {@link com.clifton.security.system.SecuritySystemUser#disabled} field when {@link SecurityUser#disabled} is updated.
 *
 * @author lnaylor
 */
@Component
public class SecurityUserObserver extends SelfRegisteringDaoObserver<SecurityUser> {

	private SecuritySystemService securitySystemService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<SecurityUser> dao, DaoEventTypes event, SecurityUser bean) {
		if (event.isUpdate() && bean.isDisabled()) {
			SecurityUser securityUser = dao.findByPrimaryKey(bean.getId());
			if (!securityUser.isDisabled()) {
				SecuritySystemUserSearchForm searchForm = new SecuritySystemUserSearchForm();
				searchForm.setSecurityUserId(bean.getId());
				List<SecuritySystemUser> securitySystemUserList = getSecuritySystemService().getSecuritySystemUserList(searchForm);

				for (SecuritySystemUser securitySystemUser : securitySystemUserList) {
					if (securitySystemUser.isDisabled() != bean.isDisabled()) {
						securitySystemUser.setDisabled(bean.isDisabled());
					}
					getSecuritySystemService().saveSecuritySystemUser(securitySystemUser);
				}
			}
		}
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.UPDATE;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecuritySystemService getSecuritySystemService() {
		return this.securitySystemService;
	}


	public void setSecuritySystemService(SecuritySystemService securitySystemService) {
		this.securitySystemService = securitySystemService;
	}
}
