package com.clifton.security.user.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;


/**
 * The <code>SecurityCurrentUserEqualsToPropertyComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user id is the same as the specified bean property value.
 *
 * @author vgomelsky
 */
public class SecurityCurrentUserEqualsToPropertyComparison implements Comparison<IdentityObject> {

	private SecurityUserService securityUserService;

	/**
	 * Bean property name which value is compared against current user id.
	 */
	private String beanPropertyName;


	/**
	 * Return false to reverse the comparison: check not equal
	 */
	protected boolean isEqualComparison() {
		return true;
	}


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		// get bean's user property
		Short securityUserId = (Short) BeanUtils.getPropertyValue(bean, getBeanPropertyName(), true);

		// get current user
		SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(currentUser, "Current user is not set.");

		boolean result = false;
		if (currentUser.getId().equals(securityUserId)) {
			result = true;
		}
		if (!isEqualComparison()) {
			result = !result;
		}

		// record comparison result message
		if (context != null) {
			SecurityUser securityUser = (securityUserId == null) ? null : getSecurityUserService().getSecurityUser(securityUserId);
			String userMessage = securityUser == null ? "[UNKNOWN USER WITH ID = " + securityUserId + "]" : securityUser.getLabel();
			String messageNotEqual = "(Current user '" + currentUser.getLabel() + "' is not '" + userMessage + "')";
			String messageEqual = "(Current user is '" + userMessage + "')";

			if (result) {
				context.recordTrueMessage(isEqualComparison() ? messageEqual : messageNotEqual);
			}
			else {
				context.recordFalseMessage(isEqualComparison() ? messageNotEqual : messageEqual);
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}
}
