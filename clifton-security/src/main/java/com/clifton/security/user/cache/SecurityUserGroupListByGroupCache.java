package com.clifton.security.user.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserGroup;
import org.springframework.stereotype.Component;


/**
 * An entity cache for retrieving lists of {@link SecurityUser} entities by {@link SecurityGroup} ID.
 *
 * @author MikeH
 */
@Component
public class SecurityUserGroupListByGroupCache extends SelfRegisteringSingleKeyDaoListCache<SecurityUserGroup, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "referenceTwo.id";
	}


	@Override
	protected Short getBeanKeyValue(SecurityUserGroup bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getReferenceTwo());
		}
		return null;
	}
}
