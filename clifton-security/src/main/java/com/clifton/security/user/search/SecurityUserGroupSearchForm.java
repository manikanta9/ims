package com.clifton.security.user.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;

import java.util.Date;


public class SecurityUserGroupSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "referenceOne.userName,referenceOne.displayName,referenceTwo.name")
	private String searchPattern;

	@SearchField(searchField = "referenceOne.id")
	private Short userId;

	@SearchField(searchField = "referenceOne.id")
	private Short[] userIds;

	@SearchField(searchField = "userName", searchFieldPath = "referenceOne")
	private String userName;

	@SearchField(searchField = "displayName", searchFieldPath = "referenceOne")
	private String userDisplayName;

	@SearchField(searchField = "disabled", searchFieldPath = "referenceOne")
	private Boolean userDisabled;

	@SearchField(searchField = "disabled", searchFieldPath = "referenceTwo")
	private Boolean groupDisabled;


	@SearchField(searchField = "updateDate", searchFieldPath = "referenceOne")
	private Date userUpdateDate;

	@SearchField(searchField = "referenceTwo.id")
	private Short groupId;

	@SearchField(searchField = "referenceTwo.id")
	private Short[] groupIds;

	@SearchField(searchField = "name", searchFieldPath = "referenceTwo")
	private String groupName;

	@SearchField(searchField = "description", searchFieldPath = "referenceTwo")
	private String groupDescription;

	@SearchField(searchField = "admin", searchFieldPath = "referenceTwo")
	private Boolean adminGroup;

	// Custom Search Field where Group is Linked to Selected Hierarchy
	// Used for Company Departments Tags
	private Short securityGroupHierarchyId;

	// Either the user or the group is disabled
	private Boolean disabled;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getUserId() {
		return this.userId;
	}


	public void setUserId(Short userId) {
		this.userId = userId;
	}


	public Short[] getUserIds() {
		return this.userIds;
	}


	public void setUserIds(Short[] userIds) {
		this.userIds = userIds;
	}


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserDisplayName() {
		return this.userDisplayName;
	}


	public void setUserDisplayName(String userDisplayName) {
		this.userDisplayName = userDisplayName;
	}


	public Boolean getUserDisabled() {
		return this.userDisabled;
	}


	public void setUserDisabled(Boolean userDisabled) {
		this.userDisabled = userDisabled;
	}


	public Boolean getGroupDisabled() {
		return this.groupDisabled;
	}


	public void setGroupDisabled(Boolean groupDisabled) {
		this.groupDisabled = groupDisabled;
	}


	public Date getUserUpdateDate() {
		return this.userUpdateDate;
	}


	public void setUserUpdateDate(Date userUpdateDate) {
		this.userUpdateDate = userUpdateDate;
	}


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public Short[] getGroupIds() {
		return this.groupIds;
	}


	public void setGroupIds(Short[] groupIds) {
		this.groupIds = groupIds;
	}


	public String getGroupName() {
		return this.groupName;
	}


	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}


	public String getGroupDescription() {
		return this.groupDescription;
	}


	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}


	public Boolean getAdminGroup() {
		return this.adminGroup;
	}


	public void setAdminGroup(Boolean adminGroup) {
		this.adminGroup = adminGroup;
	}


	public Short getSecurityGroupHierarchyId() {
		return this.securityGroupHierarchyId;
	}


	public void setSecurityGroupHierarchyId(Short securityGroupHierarchyId) {
		this.securityGroupHierarchyId = securityGroupHierarchyId;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
