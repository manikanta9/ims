package com.clifton.security.user.comparison;


import com.clifton.core.comparison.Comparison;


/**
 * The <code>SecurityCurrentUserNotEqualsToPropertyComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user id is NOT the same as the specified bean property value.
 *
 * @author vgomelsky
 */
public class SecurityCurrentUserNotEqualsToPropertyComparison extends SecurityCurrentUserEqualsToPropertyComparison {

	@Override
	protected boolean isEqualComparison() {
		return false;
	}
}
