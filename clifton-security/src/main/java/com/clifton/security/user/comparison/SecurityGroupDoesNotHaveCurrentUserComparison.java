package com.clifton.security.user.comparison;


import com.clifton.core.comparison.Comparison;


/**
 * The <code>SecurityGroupDoesNotHaveCurrentUserComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user is NOT a member of the group defined by this comparison.
 *
 * @author vgomelsky
 */
public class SecurityGroupDoesNotHaveCurrentUserComparison extends SecurityGroupHasCurrentUserComparison {

	/**
	 * Return false to reverse the comparison: check not in group
	 */
	@Override
	protected boolean isEqualComparison() {
		return false;
	}
}
