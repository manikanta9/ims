package com.clifton.security.user.comparison;

import com.clifton.core.comparison.Comparison;


/**
 * The <code>SecurityUserIsNotCurrentUserComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user is NOT the specified user.
 *
 * @author vgomelsky
 */
public class SecurityUserIsNotCurrentUserComparison extends SecurityUserIsCurrentUserComparison {

	@Override
	protected boolean isEqualComparison() {
		return false;
	}
}
