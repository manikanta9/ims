package com.clifton.security.user.cache;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.security.user.SecurityUserGroup;
import org.springframework.stereotype.Component;


/**
 * The <code>SecurityUserGroupByUserCache</code> caches the security groups for a user.
 */
@Component
public class SecurityUserGroupListByUserCache extends SelfRegisteringSingleKeyDaoListCache<SecurityUserGroup, Short> {

	@Override
	protected String getBeanKeyProperty() {
		return "referenceOne.id";
	}


	@Override
	protected Short getBeanKeyValue(SecurityUserGroup bean) {
		if (bean != null) {
			return BeanUtils.getBeanIdentity(bean.getReferenceOne());
		}
		return null;
	}
}
