package com.clifton.security.user;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.user.search.SecurityUserSearchForm;

import java.util.List;


/**
 * The <code>SecurityUserReversableConverter</code> class is a ReversableConverter for SecurityUsers
 * <p>
 * It converts Short securityUserId to Display Name strings (Used for exporting grids)
 * and it converts String display name to security user id (Not currently used)
 *
 * @author manderson
 */
public class SecurityUserReversableConverter implements ReversableConverter<String, Short> {

	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	// Used when user id = 0 (usually a backend insert or update)
	private static final String DEFAULT_SYSTEM_USER = "System User";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Note: This is not currently used - Implementation attempts lookup by Display name first
	 * If not found or more than one, tries it as user name - if not found that way would require username to be passed instead of display name
	 */
	@Override
	public Short convert(String from) {
		if (from == null) {
			return null;
		}
		if (DEFAULT_SYSTEM_USER.equals(from)) {
			return 0;
		}
		SecurityUserSearchForm searchForm = new SecurityUserSearchForm();
		searchForm.setDisplayNameEquals(from);
		List<SecurityUser> userList = getSecurityUserService().getSecurityUserList(searchForm);
		SecurityUser user;
		int size = CollectionUtils.getSize(userList);
		if (size == 1) {
			user = userList.get(0);
		}
		else {
			// If none, or more than one = try username which has a unique index
			user = getSecurityUserService().getSecurityUserByName(from);
		}
		if (user == null) {
			if (size > 1) {
				throw new ValidationException("Found [" + size + "] security users with display name [" + from + "] in the system. Please instead use unique user name ["
						+ BeanUtils.getPropertyValues(userList, "userName", ",", "[", "]", "", 10));
			}
			throw new ValidationException("Unable to find security user [" + from + "] in the system.");
		}
		return user.getId();
	}


	@Override
	public String reverseConvert(Short to) {
		if (to == null) {
			return null;
		}
		if (MathUtils.isEqual(to, 0)) {
			return DEFAULT_SYSTEM_USER;
		}
		SecurityUser user = getSecurityUserService().getSecurityUser(to);
		if (user != null) {
			return user.getDisplayName();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
