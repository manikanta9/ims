package com.clifton.security.user.comparison;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.util.StringUtils;
import com.clifton.security.user.SecurityUser;


/**
 * The <code>SecurityGroupHasUserPropertyComparison</code> class is a {@link Comparison} that evaluates to true
 * if the user identified by bean's property belongs to the specified user group.
 * It also supports bean property that references id of the user.
 *
 * @author vgomelsky
 */
public class SecurityGroupHasUserPropertyComparison extends SecurityGroupHasCurrentUserComparison {

	/**
	 * Bean property name used that identifies the user that will be checked against the specified group membership.
	 * It also supports bean property that references id of the user.
	 */
	private String userBeanPropertyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SecurityUser getUser(IdentityObject bean) {
		Object user = BeanUtils.getPropertyValue(bean, getUserBeanPropertyName(), true);
		if (user instanceof Number) {
			// support lookups by user id
			user = getSecurityUserService().getSecurityUser(((Number) user).shortValue());
		}

		if (user == null) {
			return null;
		}
		if (user instanceof SecurityUser) {
			return (SecurityUser) user;
		}
		throw new IllegalArgumentException("Cannot retrieve SecurityUser using property '" + getUserBeanPropertyName() + "' from " + bean);
	}


	@Override
	protected String getUserLabel() {
		return StringUtils.splitWords(StringUtils.capitalize(getUserBeanPropertyName()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUserBeanPropertyName() {
		return this.userBeanPropertyName;
	}


	public void setUserBeanPropertyName(String userBeanPropertyName) {
		this.userBeanPropertyName = userBeanPropertyName;
	}
}
