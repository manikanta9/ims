package com.clifton.security.user;


import com.clifton.core.beans.ManyToManyEntity;


/**
 * The <code>SecurityUserGroup</code> class represents a link between users and groups
 *
 * @author vgomelsky
 */
public class SecurityUserGroup extends ManyToManyEntity<SecurityUser, SecurityGroup, Integer> {

	public boolean isDisabled() {
		return this.getReferenceOne().isDisabled() || this.getReferenceTwo().isDisabled();
	}
}
