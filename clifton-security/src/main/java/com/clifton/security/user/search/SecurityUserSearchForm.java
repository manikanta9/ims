package com.clifton.security.user.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * The <code>SecurityUserSearchForm</code> class defines search configuration for SecurityUser objects.
 *
 * @author vgomelsky
 */
public class SecurityUserSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Short id;

	@SearchField(searchField = "id")
	private Short[] ids;

	@SearchField
	private Integer contactIdentifier;

	@SearchField(searchField = "userName,displayName,integrationPseudonym,emailAddress")
	private String searchPattern;

	@SearchField(searchField = "groupList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Short groupId;

	@SearchField(searchField = "groupList.name", comparisonConditions = ComparisonConditions.EXISTS_LIKE)
	private String groupNameLike;
	@SearchField(searchField = "groupList.name", comparisonConditions = ComparisonConditions.EXISTS)
	private String[] groupNames;

	@SearchField
	private String userName;

	@SearchField(searchField = "userName", comparisonConditions = ComparisonConditions.EQUALS)
	private String userNameEquals;

	@SearchField
	private String displayName;

	@SearchField(searchField = "displayName", comparisonConditions = {ComparisonConditions.EQUALS})
	private String displayNameEquals;

	@SearchField
	private String integrationPseudonym;

	@SearchField
	private String emailAddress;
	@SearchField
	private Boolean disabled;


	// Custom Search Field where User is assigned to a Group that is Linked to Selected Hierarchy
	// Used for Company Departments Tags
	private Short securityGroupHierarchyId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getUserNameEquals() {
		return this.userNameEquals;
	}


	public void setUserNameEquals(String userNameEquals) {
		this.userNameEquals = userNameEquals;
	}


	public String getDisplayName() {
		return this.displayName;
	}


	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}


	public String getIntegrationPseudonym() {
		return this.integrationPseudonym;
	}


	public void setIntegrationPseudonym(String integrationPseudonym) {
		this.integrationPseudonym = integrationPseudonym;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Short getGroupId() {
		return this.groupId;
	}


	public void setGroupId(Short groupId) {
		this.groupId = groupId;
	}


	public String getGroupNameLike() {
		return this.groupNameLike;
	}


	public void setGroupNameLike(String groupNameLike) {
		this.groupNameLike = groupNameLike;
	}


	public String[] getGroupNames() {
		return this.groupNames;
	}


	public void setGroupNames(String[] groupNames) {
		this.groupNames = groupNames;
	}


	public String getDisplayNameEquals() {
		return this.displayNameEquals;
	}


	public void setDisplayNameEquals(String displayNameEquals) {
		this.displayNameEquals = displayNameEquals;
	}


	public Short getId() {
		return this.id;
	}


	public void setId(Short id) {
		this.id = id;
	}


	public Short[] getIds() {
		return this.ids;
	}


	public void setIds(Short[] ids) {
		this.ids = ids;
	}


	public Integer getContactIdentifier() {
		return this.contactIdentifier;
	}


	public void setContactIdentifier(Integer contactIdentifier) {
		this.contactIdentifier = contactIdentifier;
	}


	public Short getSecurityGroupHierarchyId() {
		return this.securityGroupHierarchyId;
	}


	public void setSecurityGroupHierarchyId(Short securityGroupHierarchyId) {
		this.securityGroupHierarchyId = securityGroupHierarchyId;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}
}
