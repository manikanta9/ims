package com.clifton.security.user;


import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.user.search.SecurityGroupSearchForm;
import com.clifton.security.user.search.SecurityUserGroupSearchForm;
import com.clifton.security.user.search.SecurityUserSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>SecurityUserServiceImpl</code> provides basic implementation of SecurityUserService interface.
 *
 * @author vgomelsky
 */
@Service
public class SecurityUserServiceImpl implements SecurityUserService {

	private AdvancedUpdatableDAO<SecurityGroup, Criteria> securityGroupDAO;
	private AdvancedUpdatableDAO<SecurityUser, Criteria> securityUserDAO;
	private AdvancedUpdatableDAO<SecurityUserGroup, Criteria> securityUserGroupDAO;

	private ContextHandler contextHandler;
	private DaoNamedEntityCache<SecurityGroup> securityGroupCache;
	private DaoSingleKeyCache<SecurityUser, String> securityUserByPseudonymCache;
	private DaoSingleKeyCache<SecurityUser, String> securityUserByUserNameCache;
	private DaoSingleKeyListCache<SecurityUserGroup, Short> securityUserGroupListByGroupCache;
	private DaoSingleKeyListCache<SecurityUserGroup, Short> securityUserGroupListByUserCache;

	////////////////////////////////////////////////////////////////////////////////
	//////////              SecurityGroup Business Methods             /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityGroup getSecurityGroup(short id) {
		return getSecurityGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public SecurityGroup getSecurityGroupByName(String name) {
		return getSecurityGroupCache().getBeanForKeyValue(getSecurityGroupDAO(), name);
	}


	@Override
	public List<SecurityGroup> getSecurityGroupList(final SecurityGroupSearchForm searchForm) {
		return getSecurityGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getSecurityGroupHierarchyId() != null) {
					// get all Security Groups that are assigned to the specified hierarchy
					DetachedCriteria hierarchyItemIds = getSecurityGroupHierarchyDetachedCriteria(searchForm.getSecurityGroupHierarchyId());
					criteria.add(Subqueries.propertyIn("id", hierarchyItemIds));
				}
			}
		});
	}


	@Override
	public List<SecurityGroup> getSecurityGroupTeamList() {
		HibernateSearchConfigurer searchConfig = criteria -> criteria.add(Restrictions.like("name", "Team", MatchMode.START));
		return getSecurityGroupDAO().findBySearchCriteria(searchConfig);
	}


	@Override
	public boolean isSecurityUserCurrentInGroup(String groupName) {
		return isSecurityUserInGroup(getSecurityUserCurrent().getId(), groupName);
	}


	@Override
	public boolean isSecurityUserInGroup(short userId, String groupName) {
		return CollectionUtils.contains(getSecurityGroupListByUser(userId).stream().map(SecurityGroup::getName).collect(Collectors.toSet()), groupName);
	}


	@Override
	public List<SecurityGroup> getSecurityGroupListForCurrentUser() {
		return getSecurityGroupListByUser(getSecurityUserCurrent().getId());
	}


	@Override
	public List<SecurityGroup> getSecurityGroupListByUser(final short userId) {
		List<SecurityUserGroup> securityUserGroupList = getSecurityUserGroupListByUserCache().getBeanListForKeyValue(getSecurityUserGroupDAO(), userId);
		return CollectionUtils.getConverted(securityUserGroupList, ManyToManyEntity::getReferenceTwo);
	}


	@Override
	public SecurityGroup saveSecurityGroup(SecurityGroup securityGroup) {
		return getSecurityGroupDAO().save(securityGroup);
	}


	@Override
	public void deleteSecurityGroup(short id) {
		getSecurityGroupDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////             SecurityUser Business Methods             //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityUser getSecurityUser(short id) {
		return getSecurityUserDAO().findByPrimaryKey(id);
	}


	@Override
	public SecurityUser getSecurityUserByName(String userName) {
		return getSecurityUserByUserNameCache().getBeanForKeyValue(getSecurityUserDAO(), userName);
	}


	@Override
	public SecurityUser getSecurityUserByPseudonym(String pseudonym) {
		return getSecurityUserByPseudonymCache().getBeanForKeyValue(getSecurityUserDAO(), pseudonym);
	}


	@Override
	public SecurityUser getSecurityUserCurrent() {
		// the user is put in the Context by SecurityUserManagementFilter
		return (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
	}


	@Override
	public List<SecurityUser> getSecurityUserList() {
		return getSecurityUserDAO().findAll();
	}


	@Override
	public List<SecurityUser> getSecurityUserList(final SecurityUserSearchForm searchForm) {
		return getSecurityUserDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getSecurityGroupHierarchyId() != null) {
					// get all Security Groups that are assigned to the specified hierarchy
					DetachedCriteria hierarchyItemIds = getSecurityGroupHierarchyDetachedCriteria(searchForm.getSecurityGroupHierarchyId());
					criteria.createAlias("groupList", "g");
					criteria.add(Subqueries.propertyIn("g.id", hierarchyItemIds));
				}
			}
		});
	}


	@Override
	public List<SecurityUser> getSecurityUserListByAdmin() {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("groupList", "g").add(Restrictions.eq("g.admin", true));
		return getSecurityUserDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<SecurityUser> getSecurityUserListByGroup(final short groupId) {
		List<SecurityUserGroup> securityUserGroupList = getSecurityUserGroupListByGroupCache().getBeanListForKeyValue(getSecurityUserGroupDAO(), groupId);
		return CollectionUtils.getConverted(securityUserGroupList, ManyToManyEntity::getReferenceOne);
	}


	@Override
	public SecurityUser saveSecurityUser(SecurityUser securityUser) {
		return getSecurityUserDAO().save(securityUser);
	}


	@Override
	public void deleteSecurityUser(short id) {
		getSecurityUserDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////           SecurityUserGroup Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityUserGroup getSecurityUserGroup(int id) {
		return getSecurityUserGroupDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SecurityUserGroup> getSecurityUserGroupList(final SecurityUserGroupSearchForm searchForm) {
		return getSecurityUserGroupDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getSecurityGroupHierarchyId() != null) {
					// get all Security Groups that are assigned to the specified hierarchy
					DetachedCriteria hierarchyItemIds = getSecurityGroupHierarchyDetachedCriteria(searchForm.getSecurityGroupHierarchyId());
					criteria.add(Subqueries.propertyIn("referenceTwo.id", hierarchyItemIds));
				}

				if (searchForm.getDisabled() != null) {
					String refOneAlias = getPathAlias("referenceOne", criteria);
					String refTwoAlias = getPathAlias("referenceTwo", criteria);
					if (searchForm.getDisabled()) {
						criteria.add(Restrictions.or(Restrictions.eq(refOneAlias + ".disabled", searchForm.getDisabled()), Restrictions.eq(refTwoAlias + ".disabled", searchForm.getDisabled())));
					}
					else {
						criteria.add(Restrictions.and(Restrictions.eq(refOneAlias + ".disabled", searchForm.getDisabled()), Restrictions.eq(refTwoAlias + ".disabled", searchForm.getDisabled())));
					}
				}
			}
		});
	}


	@Override
	public SecurityUserGroup saveSecurityUserGroup(SecurityUserGroup securityUserGroup) {
		return getSecurityUserGroupDAO().save(securityUserGroup);
	}


	@Override
	public SecurityUserGroup linkSecurityUserToGroup(short userId, short groupId) {
		SecurityUserGroup securityUserGroup = new SecurityUserGroup();
		SecurityUser user = getSecurityUserDAO().findByPrimaryKey(userId);
		SecurityGroup group = getSecurityGroupDAO().findByPrimaryKey(groupId);
		securityUserGroup.setReferenceOne(user);
		securityUserGroup.setReferenceTwo(group);
		return saveSecurityUserGroup(securityUserGroup);
	}


	@Override
	public void deleteSecurityUserGroup(int id) {
		getSecurityUserGroupDAO().delete(id);
	}


	@Override
	public void unlinkSecurityUserFromGroup(short userId, short groupId) {
		SecurityUserGroup securityUserGroup = getSecurityUserGroupDAO().findOneByFields(new String[]{"referenceOne.id", "referenceTwo.id"}, new Object[]{userId, groupId});
		ValidationUtils.assertNotNull(securityUserGroup, "SecurityUserGroup may not be null!");
		deleteSecurityUserGroup(securityUserGroup.getId());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Company Department Tags Custom Filter    /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the common detached criteria that can be used by actual search to search for Security Group ID in this Detached Criteria
	 */
	private DetachedCriteria getSecurityGroupHierarchyDetachedCriteria(short securityGroupHierarchyId) {
		// get all entities that are assigned to the specified hierarchy
		DetachedCriteria hierarchyItemIds = DetachedCriteria.forEntityName("com.clifton.system.hierarchy.assignment.SystemHierarchyLink", "l");
		hierarchyItemIds.setProjection(Projections.property("fkFieldId"));
		hierarchyItemIds.createCriteria("assignment", "a").createAlias("table", "t").add(Restrictions.eq("t.name", "SecurityGroup"));
		hierarchyItemIds.add(Restrictions.eq("l.hierarchy.id", securityGroupHierarchyId));
		return hierarchyItemIds;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SecurityGroup, Criteria> getSecurityGroupDAO() {
		return this.securityGroupDAO;
	}


	public void setSecurityGroupDAO(AdvancedUpdatableDAO<SecurityGroup, Criteria> securityGroupDAO) {
		this.securityGroupDAO = securityGroupDAO;
	}


	public AdvancedUpdatableDAO<SecurityUser, Criteria> getSecurityUserDAO() {
		return this.securityUserDAO;
	}


	public void setSecurityUserDAO(AdvancedUpdatableDAO<SecurityUser, Criteria> securityUserDAO) {
		this.securityUserDAO = securityUserDAO;
	}


	public AdvancedUpdatableDAO<SecurityUserGroup, Criteria> getSecurityUserGroupDAO() {
		return this.securityUserGroupDAO;
	}


	public void setSecurityUserGroupDAO(AdvancedUpdatableDAO<SecurityUserGroup, Criteria> securityUserGroupDAO) {
		this.securityUserGroupDAO = securityUserGroupDAO;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public DaoNamedEntityCache<SecurityGroup> getSecurityGroupCache() {
		return this.securityGroupCache;
	}


	public void setSecurityGroupCache(DaoNamedEntityCache<SecurityGroup> securityGroupCache) {
		this.securityGroupCache = securityGroupCache;
	}


	public DaoSingleKeyCache<SecurityUser, String> getSecurityUserByPseudonymCache() {
		return this.securityUserByPseudonymCache;
	}


	public void setSecurityUserByPseudonymCache(DaoSingleKeyCache<SecurityUser, String> securityUserByPseudonymCache) {
		this.securityUserByPseudonymCache = securityUserByPseudonymCache;
	}


	public DaoSingleKeyCache<SecurityUser, String> getSecurityUserByUserNameCache() {
		return this.securityUserByUserNameCache;
	}


	public void setSecurityUserByUserNameCache(DaoSingleKeyCache<SecurityUser, String> securityUserByUserNameCache) {
		this.securityUserByUserNameCache = securityUserByUserNameCache;
	}


	public DaoSingleKeyListCache<SecurityUserGroup, Short> getSecurityUserGroupListByGroupCache() {
		return this.securityUserGroupListByGroupCache;
	}


	public void setSecurityUserGroupListByGroupCache(DaoSingleKeyListCache<SecurityUserGroup, Short> securityUserGroupListByGroupCache) {
		this.securityUserGroupListByGroupCache = securityUserGroupListByGroupCache;
	}


	public DaoSingleKeyListCache<SecurityUserGroup, Short> getSecurityUserGroupListByUserCache() {
		return this.securityUserGroupListByUserCache;
	}


	public void setSecurityUserGroupListByUserCache(DaoSingleKeyListCache<SecurityUserGroup, Short> securityUserGroupListByUserCache) {
		this.securityUserGroupListByUserCache = securityUserGroupListByUserCache;
	}
}
