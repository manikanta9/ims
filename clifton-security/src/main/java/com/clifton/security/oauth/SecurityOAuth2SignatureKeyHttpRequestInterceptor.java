package com.clifton.security.oauth;

import com.clifton.core.security.oauth.SecurityOAuthUserTokenService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;

import java.io.IOException;


/**
 * The {@link SecurityOAuth2SignatureKeyHttpRequestInterceptor} is used to intercept outgoing OpenApi REST calls and add the
 * OAuth token to the headers.
 *
 * @author lnaylor
 */
public class SecurityOAuth2SignatureKeyHttpRequestInterceptor implements ClientHttpRequestInterceptor {

	private SecurityOAuthUserTokenService securityOAuthUserTokenService;

	private String endpointUrl;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		String requestEndpointUrl = StringUtils.substringBeforeFirst(request.getURI().toString(), getEndpointUrl());
		String bearerToken = getSecurityOAuthUserTokenService().getTokenForCurrentUserIfPresent(requestEndpointUrl, false);
		AssertUtils.assertNotNull(bearerToken, "Bearer token is null for endpoint " + requestEndpointUrl);
		request.getHeaders().set("Authorization", "Bearer " + bearerToken);
		ClientHttpResponse response = execution.execute(request, body);

		if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			bearerToken = getSecurityOAuthUserTokenService().getTokenForCurrentUserIfPresent(requestEndpointUrl, true);
			AssertUtils.assertNotNull(bearerToken, "Bearer token is null for endpoint " + requestEndpointUrl);
			request.getHeaders().set("Authorization", "Bearer " + bearerToken);
			response = execution.execute(request, body);
		}
		return response;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityOAuthUserTokenService getSecurityOAuthUserTokenService() {
		return this.securityOAuthUserTokenService;
	}


	public void setSecurityOAuthUserTokenService(SecurityOAuthUserTokenService securityOAuthUserTokenService) {
		this.securityOAuthUserTokenService = securityOAuthUserTokenService;
	}


	public String getEndpointUrl() {
		return this.endpointUrl;
	}


	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}
}
