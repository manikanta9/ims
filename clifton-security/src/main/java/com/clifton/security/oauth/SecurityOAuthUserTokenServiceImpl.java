package com.clifton.security.oauth;

import com.clifton.core.security.oauth.BaseSecurityOAuthUserTokenServiceImpl;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;


/**
 * @author theodorez
 */
@Service
public class SecurityOAuthUserTokenServiceImpl extends BaseSecurityOAuthUserTokenServiceImpl<SecurityUser> {


	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected SecurityUser getSecurityUserCurrent() {
		return getSecurityUserService().getSecurityUserCurrent();
	}


	@Override
	protected SecurityUser getSecurityUserByName(String userName) {
		return getSecurityUserService().getSecurityUserByName(userName);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
