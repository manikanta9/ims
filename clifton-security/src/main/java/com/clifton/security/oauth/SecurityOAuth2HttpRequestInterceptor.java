package com.clifton.security.oauth;

import com.clifton.core.util.AssertUtils;
import org.springframework.http.HttpRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.OAuth2AuthorizeRequest;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.core.OAuth2AccessToken;

import java.io.IOException;
import java.util.Objects;


/**
 * The {@link SecurityOAuth2HttpRequestInterceptor} is used to intercept outgoing OpenApi REST calls and add the
 * OAuth2 token to the headers.
 *
 * @author lnaylor
 */
public class SecurityOAuth2HttpRequestInterceptor implements ClientHttpRequestInterceptor {

	private AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager;

	private OAuth2AuthorizedClientService authorizedClientService;

	/**
	 * The client registration ID set when creating the {@link ClientRegistration} in {@link SecurityOAuth2RestTemplateConfigurer}
	 */
	private String clientRegistrationId;

	/**
	 * The name of the OAuth2 service to be called
	 */
	private String serviceName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
		OAuth2AuthorizeRequest authorizeRequest = OAuth2AuthorizeRequest.withClientRegistrationId(getClientRegistrationId())
				.principal(getServiceName())
				.build();

		OAuth2AuthorizedClient authorizedClient = getAuthorizedClientServiceAndManager().authorize(authorizeRequest);
		AssertUtils.assertNotNull(authorizedClient, "Unable to get authorized client from OAuth2 service [%s] and client registration ID [%s]", getServiceName(), getClientRegistrationId());
		OAuth2AccessToken accessToken = Objects.requireNonNull(authorizedClient).getAccessToken();

		request.getHeaders().add("Authorization", "Bearer " + accessToken.getTokenValue());
		ClientHttpResponse response = execution.execute(request, body);

		if (response.getStatusCode() == HttpStatus.UNAUTHORIZED) {
			getAuthorizedClientService().removeAuthorizedClient(authorizeRequest.getClientRegistrationId(), authorizeRequest.getPrincipal().getName());
			authorizedClient = getAuthorizedClientServiceAndManager().authorize(authorizeRequest);
			AssertUtils.assertNotNull(authorizedClient, "Unable to get authorized client from OAuth2 service [%s] and client registration ID [%s]", getServiceName(), getClientRegistrationId());
			accessToken = Objects.requireNonNull(authorizedClient).getAccessToken();
			request.getHeaders().set("Authorization", "Bearer " + accessToken.getTokenValue());
			response = execution.execute(request, body);
		}
		return response;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AuthorizedClientServiceOAuth2AuthorizedClientManager getAuthorizedClientServiceAndManager() {
		return this.authorizedClientServiceAndManager;
	}


	public void setAuthorizedClientServiceAndManager(AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager) {
		this.authorizedClientServiceAndManager = authorizedClientServiceAndManager;
	}


	public OAuth2AuthorizedClientService getAuthorizedClientService() {
		return this.authorizedClientService;
	}


	public void setAuthorizedClientService(OAuth2AuthorizedClientService authorizedClientService) {
		this.authorizedClientService = authorizedClientService;
	}


	public String getClientRegistrationId() {
		return this.clientRegistrationId;
	}


	public void setClientRegistrationId(String clientRegistrationId) {
		this.clientRegistrationId = clientRegistrationId;
	}


	public String getServiceName() {
		return this.serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
