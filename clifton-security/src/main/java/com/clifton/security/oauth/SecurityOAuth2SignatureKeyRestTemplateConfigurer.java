package com.clifton.security.oauth;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.web.api.client.openapi.RestTemplateConfigurer;
import org.springframework.web.client.RestTemplate;


/**
 * The {@link SecurityOAuth2SignatureKeyRestTemplateConfigurer} provides methods for configuring OpenApi apis to use OAuth authentication when making calls.
 *
 * @author lnaylor
 */
public class SecurityOAuth2SignatureKeyRestTemplateConfigurer implements RestTemplateConfigurer {

	private ApplicationContextService applicationContextService;

	/**
	 * The endpoint url used in the {@link SecurityOAuth2SignatureKeyHttpRequestInterceptor} to determine the base url from which to request a token
	 */
	private String endpointUrl;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void configure(RestTemplate restTemplate) {
		SecurityOAuth2SignatureKeyHttpRequestInterceptor oAuthHttpRequestInterceptor = new SecurityOAuth2SignatureKeyHttpRequestInterceptor();
		getApplicationContextService().autowireBean(oAuthHttpRequestInterceptor);
		oAuthHttpRequestInterceptor.setEndpointUrl(getEndpointUrl());
		restTemplate.getInterceptors().add(oAuthHttpRequestInterceptor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public String getEndpointUrl() {
		return this.endpointUrl;
	}


	public void setEndpointUrl(String endpointUrl) {
		this.endpointUrl = endpointUrl;
	}
}
