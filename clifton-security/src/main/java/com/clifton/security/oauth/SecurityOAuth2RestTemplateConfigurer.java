package com.clifton.security.oauth;

import com.clifton.core.util.StringUtils;
import com.clifton.core.web.api.client.openapi.RestTemplateConfigurer;
import org.springframework.security.oauth2.client.AuthorizedClientServiceOAuth2AuthorizedClientManager;
import org.springframework.security.oauth2.client.InMemoryOAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProvider;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientProviderBuilder;
import org.springframework.security.oauth2.client.registration.ClientRegistration;
import org.springframework.security.oauth2.client.registration.InMemoryClientRegistrationRepository;
import org.springframework.security.oauth2.core.AuthorizationGrantType;
import org.springframework.web.client.RestTemplate;

import java.util.List;


/**
 * The {@link SecurityOAuth2RestTemplateConfigurer} provides methods for configuring OpenApi apis to use OAuth2 authentication/authorization when making calls.
 *
 * @author lnaylor
 */
public class SecurityOAuth2RestTemplateConfigurer implements RestTemplateConfigurer {

	/**
	 * The OAuth2 server token uri
	 */
	private String tokenUri;

	/**
	 * The scope(s) to request authorization for
	 */
	private List<String> scopeList;

	/**
	 * The calling application's OAuth2 client ID
	 */
	private String clientId;

	/**
	 * The calling application's OAuth2 client secret
	 */
	private String clientSecret;

	/**
	 * The method to use when accessing an OAuth2 token
	 */
	private String authorizationGrantType;

	/**
	 * The name of the OAuth2 service to be called
	 */
	private String serviceName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void configure(RestTemplate restTemplate) {
		ClientRegistration clientRegistration = ClientRegistration
				.withRegistrationId(getRegistrationId())
				.tokenUri(getTokenUri())
				.clientId(getClientId())
				.clientSecret(getClientSecret())
				.scope(getScopeList())
				.authorizationGrantType(new AuthorizationGrantType(getAuthorizationGrantType()))
				.build();

		InMemoryClientRegistrationRepository clientRegistrationRepository = new InMemoryClientRegistrationRepository(clientRegistration);

		InMemoryOAuth2AuthorizedClientService auth2AuthorizedClientService = new InMemoryOAuth2AuthorizedClientService(clientRegistrationRepository);

		OAuth2AuthorizedClientProvider authorizedClientProvider = OAuth2AuthorizedClientProviderBuilder.builder()
				.clientCredentials()
				.build();

		AuthorizedClientServiceOAuth2AuthorizedClientManager authorizedClientServiceAndManager = new AuthorizedClientServiceOAuth2AuthorizedClientManager(clientRegistrationRepository, auth2AuthorizedClientService);
		authorizedClientServiceAndManager.setAuthorizedClientProvider(authorizedClientProvider);

		SecurityOAuth2HttpRequestInterceptor oauth2RequestInterceptor = new SecurityOAuth2HttpRequestInterceptor();
		oauth2RequestInterceptor.setClientRegistrationId(getRegistrationId());
		oauth2RequestInterceptor.setServiceName(getServiceName());
		oauth2RequestInterceptor.setAuthorizedClientServiceAndManager(authorizedClientServiceAndManager);
		oauth2RequestInterceptor.setAuthorizedClientService(auth2AuthorizedClientService);

		restTemplate.getInterceptors().add(oauth2RequestInterceptor);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRegistrationId() {
		return "oauth2" + StringUtils.removeAll(getServiceName(), "\\s");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTokenUri() {
		return this.tokenUri;
	}


	public void setTokenUri(String tokenUri) {
		this.tokenUri = tokenUri;
	}


	public List<String> getScopeList() {
		return this.scopeList;
	}


	public void setScopeList(List<String> scopeList) {
		this.scopeList = scopeList;
	}


	public String getClientId() {
		return this.clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}


	public String getClientSecret() {
		return this.clientSecret;
	}


	public void setClientSecret(String clientSecret) {
		this.clientSecret = clientSecret;
	}


	public String getAuthorizationGrantType() {
		return this.authorizationGrantType;
	}


	public void setAuthorizationGrantType(String authorizationGrantType) {
		this.authorizationGrantType = authorizationGrantType;
	}


	public String getServiceName() {
		return this.serviceName;
	}


	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}
}
