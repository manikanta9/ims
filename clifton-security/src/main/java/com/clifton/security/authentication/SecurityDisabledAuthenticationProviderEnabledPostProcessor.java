package com.clifton.security.authentication;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.stereotype.Component;


/**
 * On application startup, checks if authentication is disabled and prevents startup from continuing
 *
 * @author theodorez
 */
@Component
public class SecurityDisabledAuthenticationProviderEnabledPostProcessor implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	@Value("${application.environment.level:prod}")
	private String environment;

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		if (!StringUtils.isEmpty(getEnvironment())) {
			LogUtils.debug(getClass(), "Authentication Provider Populated. Environment: " + getEnvironment());

			//If this is a production environment AND the Provider that Disables Authentication is instantiated, then throw an error to abort startup
			if (StringUtils.equalsAnyIgnoreCase(getEnvironment(), "prod", "production") &&
					event.getApplicationContext().getBeansOfType(AuthenticationProvider.class).values().stream().anyMatch(SecurityDisabledAuthenticationProvider.class::isInstance)
			) {
				throw new RuntimeException("ABORTING STARTUP - Attempted to start in production environment with Authentication Disabled");
			}
		}
		else {
			LogUtils.warn(getClass(), "Authentication Provider validation not functioning - Environment is not populated.");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
