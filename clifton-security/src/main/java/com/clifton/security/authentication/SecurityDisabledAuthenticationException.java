package com.clifton.security.authentication;

import org.springframework.security.core.userdetails.UserDetails;

import javax.naming.AuthenticationException;


/**
 * The <code>SecurityDisabledAuthenticationException</code> is a specific type of exception that is used to log when users are logged in using disabled security measures.
 *
 * @author manderson
 */
public class SecurityDisabledAuthenticationException extends AuthenticationException {


	public static SecurityDisabledAuthenticationException ofUserDetails(UserDetails userDetails) {
		String message = "";
		if (userDetails != null) {
			message = "Security Disabled Login Succeeded for User [" + userDetails.getUsername() + "]";
		}
		return new SecurityDisabledAuthenticationException(message);
	}


	public SecurityDisabledAuthenticationException(String message) {
		super(message);
	}


	public SecurityDisabledAuthenticationException() {
		super();
	}
}
