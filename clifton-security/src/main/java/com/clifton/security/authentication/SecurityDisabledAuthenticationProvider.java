package com.clifton.security.authentication;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.password.passay.PassayPasswordValidator;
import com.clifton.core.util.ExceptionUtils;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;


/**
 * The <code>SecurityDisabledAuthenticationProvider</code> is used to authenticate any user that exists in the database as long as the password they enter follows our password rules
 *
 * @author manderson
 */
public class SecurityDisabledAuthenticationProvider extends DaoAuthenticationProvider {


	/**
	 * Copied from {@link DaoAuthenticationProvider} however just validates the password passes password rules
	 */
	@Override
	protected void additionalAuthenticationChecks(UserDetails userDetails, UsernamePasswordAuthenticationToken authentication) throws AuthenticationException {
		if (authentication.getCredentials() == null) {
			LogUtils.debug(getClass(), "Authentication failed: no credentials provided");

			throw new BadCredentialsException("Invalid password");
		}

		String presentedPassword = authentication.getCredentials().toString();
		try {
			new PassayPasswordValidator().validatePassword(userDetails.getUsername(), presentedPassword);
		}
		catch (Throwable e) {
			LogUtils.errorOrInfo(getClass(), ExceptionUtils.getOriginalMessage(e), e);
			throw new BadCredentialsException("Invalid password");
		}

		LogUtils.error(getClass(), "Security Authentication is Disabled", SecurityDisabledAuthenticationException.ofUserDetails(userDetails));
	}
}
