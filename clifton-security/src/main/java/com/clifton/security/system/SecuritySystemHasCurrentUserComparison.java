package com.clifton.security.system;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.AssertUtils;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;


/**
 * The <code>SecuritySystemHasCurrentUserComparison</code> class is a {@link Comparison} that evaluates to true
 * if current user is a member of the security system allowed by this comparison.
 *
 * @author tstebner
 */
public class SecuritySystemHasCurrentUserComparison implements Comparison<IdentityObject> {

	private static final String CURRENT_USER = "Current User";

	private SecurityUserService securityUserService;
	private SecuritySystemService securitySystemService;

	/**
	 * Id of system that make this comparison evaluate to true.
	 */
	private short securitySystemId;


	/**
	 * Return true to keep comparison: check in group
	 */
	protected boolean isEqualComparison() {
		return true;
	}


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(currentUser, CURRENT_USER + " is not set.");

		boolean result = !isEqualComparison();
		SecuritySystemUser securitySystemUser = getSecuritySystemService().getSecuritySystemUserBySystemIdAndUserId(getSecuritySystemId(), currentUser.getId());
		if (securitySystemUser != null) {
			result = isEqualComparison();
		}

		// record comparison result message
		if (context != null) {
			String messageNotEqual = "(" + CURRENT_USER + " " + currentUser.getUserName() + " is not in group " + getSecuritySystemService().getSecuritySystem(getSecuritySystemId()).getName() + ")";
			String messageEqual = "(" + CURRENT_USER + " " + currentUser.getUserName() + " is in group " + getSecuritySystemService().getSecuritySystem(getSecuritySystemId()).getName() + ")";

			if (result) {
				context.recordTrueMessage(isEqualComparison() ? messageEqual : messageNotEqual);
			}
			else {
				context.recordFalseMessage(isEqualComparison() ? messageNotEqual : messageEqual);
			}
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public short getSecuritySystemId() {
		return this.securitySystemId;
	}


	public void setSecuritySystemId(short securitySystemId) {
		this.securitySystemId = securitySystemId;
	}


	public SecuritySystemService getSecuritySystemService() {
		return this.securitySystemService;
	}


	public void setSecuritySystemService(SecuritySystemService securitySystemService) {
		this.securitySystemService = securitySystemService;
	}
}
