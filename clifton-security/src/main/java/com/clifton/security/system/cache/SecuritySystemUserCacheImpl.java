package com.clifton.security.system.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSimpleMultiKeyDaoCache;
import com.clifton.security.system.SecuritySystemUser;
import org.springframework.stereotype.Component;


/**
 * @author NickK
 */
@Component
public class SecuritySystemUserCacheImpl extends SelfRegisteringSimpleMultiKeyDaoCache<SecuritySystemUser> implements SecuritySystemUserCache {

	private static final String SECURITY_SYSTEM_ID = "securitySystem.id";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected Class<SecuritySystemUser> getDtoClass() {
		return SecuritySystemUser.class;
	}


	@Override
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserId(short securitySystemId, short securityUserId) {
		return getSecuritySystemUserBySystemIdAndUserId(securitySystemId, securityUserId, false);
	}


	@Override
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserId(short securitySystemId, short securityUserId, Boolean disabled) {
		if (disabled == null) {
			return getValue(new Object[]{securitySystemId, securityUserId, null},
					() -> getDao().findOneByFields(new String[]{SECURITY_SYSTEM_ID, "securityUser.id"}, new Object[]{securitySystemId, securityUserId}));
		}
		return getValue(new Object[]{securitySystemId, securityUserId, null, disabled},
				() -> getDao().findOneByFields(new String[]{SECURITY_SYSTEM_ID, "securityUser.id", "disabled"}, new Object[]{securitySystemId, securityUserId, disabled}));
	}


	@Override
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserAlias(short securitySystemId, String userAlias) {
		return getSecuritySystemUserBySystemIdAndUserAlias(securitySystemId, userAlias, false);
	}


	@Override
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserAlias(short securitySystemId, String userAlias, Boolean disabled) {
		if (disabled == null) {
			return getValue(new Object[]{securitySystemId, null, userAlias},
					() -> getDao().findOneByFields(new String[]{SECURITY_SYSTEM_ID, "userName"}, new Object[]{securitySystemId, userAlias}));
		}
		return getValue(new Object[]{securitySystemId, null, userAlias, disabled},
				() -> getDao().findOneByFields(new String[]{SECURITY_SYSTEM_ID, "userName", "disabled"}, new Object[]{securitySystemId, userAlias, disabled}));
	}
}
