package com.clifton.security.system;


import com.clifton.core.beans.BaseEntity;
import com.clifton.security.user.SecurityUser;


/**
 * The <code>SecurityUserAlias</code> provides a user alias for external systems.
 *
 * @author mwacker
 */
public class SecuritySystemUser extends BaseEntity<Short> {

	private SecurityUser securityUser;

	private SecuritySystem securitySystem;
	/**
	 * User name (alias) used by external system to represent our user defined by SecurityUser.
	 */
	private String userName;

	private boolean readOnly;

	private boolean disabled;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser user) {
		this.securityUser = user;
	}


	public SecuritySystem getSecuritySystem() {
		return this.securitySystem;
	}


	public void setSecuritySystem(SecuritySystem userSystem) {
		this.securitySystem = userSystem;
	}


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public boolean isReadOnly() {
		return this.readOnly;
	}


	public void setReadOnly(boolean readOnly) {
		this.readOnly = readOnly;
	}


	public boolean isDisabled() {
		return disabled;
	}


	public void setDisabled(boolean disabled) {
		this.disabled = disabled;
	}
}
