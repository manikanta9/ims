package com.clifton.security.system.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class SecuritySystemUserSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "securityUser.userName,securityUser.displayName,securitySystem.name")
	private String searchPattern;

	@SearchField(searchField = "securityUser.id")
	private Short securityUserId;

	@SearchField(searchField = "securitySystem.id")
	private Short securitySystemId;

	@SearchField
	private String userName;

	@SearchField
	private Boolean readOnly;

	@SearchField
	private Boolean disabled;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Short getSecurityUserId() {
		return this.securityUserId;
	}


	public void setSecurityUserId(Short userId) {
		this.securityUserId = userId;
	}


	public Short getSecuritySystemId() {
		return this.securitySystemId;
	}


	public void setSecuritySystemId(Short userSystemId) {
		this.securitySystemId = userSystemId;
	}


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public Boolean getReadOnly() {
		return this.readOnly;
	}


	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}


	public Boolean getDisabled() {
		return disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}
}
