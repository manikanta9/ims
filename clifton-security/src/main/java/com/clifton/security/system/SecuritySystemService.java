package com.clifton.security.system;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.security.system.search.SecuritySystemSearchForm;
import com.clifton.security.system.search.SecuritySystemUserSearchForm;

import java.util.List;


/**
 * The <code>SecuritySystemService</code> interface defines methods for working with SecuritySystem objects
 * and corresponding additional information.
 *
 * @author mwacker
 */
public interface SecuritySystemService {

	////////////////////////////////////////////////////////////////////////////
	////////            SecuritySystem Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecuritySystem getSecuritySystem(short id);


	public SecuritySystem getSecuritySystemByName(String name);


	public List<SecuritySystem> getSecuritySystemList(SecuritySystemSearchForm searchForm);


	public SecuritySystem saveSecuritySystem(SecuritySystem securitySystem);


	////////////////////////////////////////////////////////////////////////////
	////////          SecuritySystemUser Business Methods          /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecuritySystemUser getSecuritySystemUser(short id);


	@DoNotAddRequestMapping
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserId(short securitySystemId, short securityUserId);


	@DoNotAddRequestMapping
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserAlias(short securitySystemId, String userAlias);


	public List<SecuritySystemUser> getSecuritySystemUserList(SecuritySystemUserSearchForm searchForm);


	public SecuritySystemUser saveSecuritySystemUser(SecuritySystemUser bean);


	public void deleteSecuritySystemUser(short id);
}
