package com.clifton.security.system;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.security.system.cache.SecuritySystemUserCache;
import com.clifton.security.system.search.SecuritySystemSearchForm;
import com.clifton.security.system.search.SecuritySystemUserSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SecuritySystemServiceImpl implements SecuritySystemService {

	private AdvancedUpdatableDAO<SecuritySystem, Criteria> securitySystemDAO;
	private AdvancedUpdatableDAO<SecuritySystemUser, Criteria> securitySystemUserDAO;

	private DaoNamedEntityCache<SecuritySystem> securitySystemCache;
	private SecuritySystemUserCache securitySystemUserCache;

	////////////////////////////////////////////////////////////////////////////
	////////            SecuritySystem Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecuritySystem getSecuritySystem(short id) {
		return getSecuritySystemDAO().findByPrimaryKey(id);
	}


	@Override
	public SecuritySystem getSecuritySystemByName(String name) {
		return getSecuritySystemCache().getBeanForKeyValue(getSecuritySystemDAO(), name);
	}


	@Override
	public List<SecuritySystem> getSecuritySystemList(SecuritySystemSearchForm searchForm) {
		return getSecuritySystemDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SecuritySystem saveSecuritySystem(SecuritySystem securitySystem) {
		return getSecuritySystemDAO().save(securitySystem);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            SecuritySystem Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecuritySystemUser getSecuritySystemUser(short id) {
		return getSecuritySystemUserDAO().findByPrimaryKey(id);
	}


	@Override
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserId(short securitySystemId, short securityUserId) {
		return getSecuritySystemUserCache().getSecuritySystemUserBySystemIdAndUserId(securitySystemId, securityUserId);
	}


	@Override
	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserAlias(short securitySystemId, String userAlias) {
		return getSecuritySystemUserCache().getSecuritySystemUserBySystemIdAndUserAlias(securitySystemId, userAlias);
	}


	@Override
	public List<SecuritySystemUser> getSecuritySystemUserList(SecuritySystemUserSearchForm searchForm) {
		return getSecuritySystemUserDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SecuritySystemUser saveSecuritySystemUser(SecuritySystemUser bean) {
		return getSecuritySystemUserDAO().save(bean);
	}


	@Override
	public void deleteSecuritySystemUser(short id) {
		getSecuritySystemUserDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SecuritySystem, Criteria> getSecuritySystemDAO() {
		return this.securitySystemDAO;
	}


	public void setSecuritySystemDAO(AdvancedUpdatableDAO<SecuritySystem, Criteria> securityUserSystemDAO) {
		this.securitySystemDAO = securityUserSystemDAO;
	}


	public AdvancedUpdatableDAO<SecuritySystemUser, Criteria> getSecuritySystemUserDAO() {
		return this.securitySystemUserDAO;
	}


	public void setSecuritySystemUserDAO(AdvancedUpdatableDAO<SecuritySystemUser, Criteria> securitySystemUserDAO) {
		this.securitySystemUserDAO = securitySystemUserDAO;
	}


	public DaoNamedEntityCache<SecuritySystem> getSecuritySystemCache() {
		return this.securitySystemCache;
	}


	public void setSecuritySystemCache(DaoNamedEntityCache<SecuritySystem> securitySystemCache) {
		this.securitySystemCache = securitySystemCache;
	}


	public SecuritySystemUserCache getSecuritySystemUserCache() {
		return this.securitySystemUserCache;
	}


	public void setSecuritySystemUserCache(SecuritySystemUserCache securitySystemUserCache) {
		this.securitySystemUserCache = securitySystemUserCache;
	}
}
