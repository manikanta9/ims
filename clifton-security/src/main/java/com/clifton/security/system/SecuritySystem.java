package com.clifton.security.system;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


/**
 * The <code>SecuritySystem</code> class represents an external system that our system integrations with.
 * For example: Bloomberg, FX Connect, REDI+, etc.
 * <p/>
 * Additional information like user name mappings (SecuritySystemUser entity) maybe associated with each system.
 *
 * @author mwacker
 */
@CacheByName
public class SecuritySystem extends NamedEntity<Short> {

	// empty
}
