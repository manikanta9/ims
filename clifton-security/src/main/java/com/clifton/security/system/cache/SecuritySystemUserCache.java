package com.clifton.security.system.cache;

import com.clifton.security.system.SecuritySystemUser;


/**
 * <code>SecuritySystemUserCache</code> is a {@link SecuritySystemUser} cache that caches values
 * by more than one set of keys.
 *
 * @author NickK
 */
public interface SecuritySystemUserCache {

	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserId(short securitySystemId, short securityUserId);


	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserId(short securitySystemId, short securityUserId, Boolean disabled);


	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserAlias(short securitySystemId, String userAlias);


	public SecuritySystemUser getSecuritySystemUserBySystemIdAndUserAlias(short securitySystemId, String userAlias, Boolean disabled);
}
