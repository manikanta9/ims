package com.clifton.security.authorization.setup.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.security.authorization.SecurityPermissionAssignment;


/**
 * The <code>SecurityPermissionAssignmentSearchForm</code> class defines search configuration for {@link SecurityPermissionAssignment} objects.
 *
 * @author vgomelsky
 */
public class SecurityPermissionAssignmentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField(searchField = "securityUser.userName,securityUser.displayName,securityGroup.name,securityResource.parent.parent.parent.label,securityResource.parent.parent.label,securityResource.parent.label,securityResource.label", leftJoin = true)
	private String searchPattern;

	@SearchField(searchField = "securityResource.id")
	private Integer securityResourceId;

	@SearchField(searchField = "securityResource.id")
	private Integer[] securityResourceIds;

	@SearchField(searchField = "securityResource.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Integer[] securityResourceIdsOrNull;

	@SearchField(searchField = "securityResource.parent.parent.parent.label,securityResource.parent.parent.label,securityResource.parent.label,securityResource.label", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String securityResourceLabelExpanded;

	@SearchField(searchField = "securityUser.id")
	private Short securityUserId;

	@SearchField(searchField = "securityUser.id")
	private Short[] securityUserIds;

	@SearchField(searchField = "securityUser.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Short[] securityUserIdsOrNull;

	@SearchField(searchField = "securityGroup.id")
	private Short securityGroupId;

	@SearchField(searchField = "securityGroup.id")
	private Short[] securityGroupIds;

	@SearchField(searchField = "securityGroup.id", comparisonConditions = ComparisonConditions.IN_OR_IS_NULL)
	private Short[] securityGroupIdsOrNull;

	@SearchField(searchField = "grantedPermissionMask")
	private Integer securityPermission;

	@SearchField
	private Boolean inheritedByChildResources;

	@SearchField
	private String assignmentNotes;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getSecurityResourceId() {
		return this.securityResourceId;
	}


	public void setSecurityResourceId(Integer securityResourceId) {
		this.securityResourceId = securityResourceId;
	}


	public Integer[] getSecurityResourceIds() {
		return this.securityResourceIds;
	}


	public void setSecurityResourceIds(Integer[] securityResourceIds) {
		this.securityResourceIds = securityResourceIds;
	}


	public Integer[] getSecurityResourceIdsOrNull() {
		return this.securityResourceIdsOrNull;
	}


	public void setSecurityResourceIdsOrNull(Integer[] securityResourceIdsOrNull) {
		this.securityResourceIdsOrNull = securityResourceIdsOrNull;
	}


	public String getSecurityResourceLabelExpanded() {
		return this.securityResourceLabelExpanded;
	}


	public void setSecurityResourceLabelExpanded(String securityResourceLabelExpanded) {
		this.securityResourceLabelExpanded = securityResourceLabelExpanded;
	}


	public Short getSecurityUserId() {
		return this.securityUserId;
	}


	public void setSecurityUserId(Short securityUserId) {
		this.securityUserId = securityUserId;
	}


	public Short[] getSecurityUserIds() {
		return this.securityUserIds;
	}


	public void setSecurityUserIds(Short[] securityUserIds) {
		this.securityUserIds = securityUserIds;
	}


	public Short[] getSecurityUserIdsOrNull() {
		return this.securityUserIdsOrNull;
	}


	public void setSecurityUserIdsOrNull(Short[] securityUserIdsOrNull) {
		this.securityUserIdsOrNull = securityUserIdsOrNull;
	}


	public Short getSecurityGroupId() {
		return this.securityGroupId;
	}


	public void setSecurityGroupId(Short securityGroupId) {
		this.securityGroupId = securityGroupId;
	}


	public Short[] getSecurityGroupIds() {
		return this.securityGroupIds;
	}


	public void setSecurityGroupIds(Short[] securityGroupIds) {
		this.securityGroupIds = securityGroupIds;
	}


	public Short[] getSecurityGroupIdsOrNull() {
		return this.securityGroupIdsOrNull;
	}


	public void setSecurityGroupIdsOrNull(Short[] securityGroupIdsOrNull) {
		this.securityGroupIdsOrNull = securityGroupIdsOrNull;
	}


	public Integer getSecurityPermission() {
		return this.securityPermission;
	}


	public void setSecurityPermission(Integer securityPermission) {
		this.securityPermission = securityPermission;
	}


	public Boolean getInheritedByChildResources() {
		return this.inheritedByChildResources;
	}


	public void setInheritedByChildResources(Boolean inheritedByChildResources) {
		this.inheritedByChildResources = inheritedByChildResources;
	}


	public String getAssignmentNotes() {
		return this.assignmentNotes;
	}


	public void setAssignmentNotes(String assignmentNotes) {
		this.assignmentNotes = assignmentNotes;
	}
}
