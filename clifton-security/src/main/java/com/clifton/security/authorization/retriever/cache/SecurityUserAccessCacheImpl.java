package com.clifton.security.authorization.retriever.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.security.authorization.retriever.UserAccessHolder;
import com.clifton.security.user.SecurityUser;
import org.springframework.stereotype.Component;


/**
 * The <code>SecurityUserAccessCacheImpl</code> class allows caching and retrieval from cache of {@link UserAccessHolder} objects.
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding security DAO's.
 *
 * @param <T>
 * @author vgomelsky
 */
@Component
public class SecurityUserAccessCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<String, UserAccessHolder>, SecurityUserAccessCache {

	private CacheHandler<String, UserAccessHolder> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public UserAccessHolder getUserAccessHolder(String userName) {
		return getCacheHandler().get(getCacheName(), userName);
	}


	@Override
	public void storeUserAccessHolder(String userName, UserAccessHolder userAccessHolder) {
		getCacheHandler().put(getCacheName(), userName, userAccessHolder);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			if (bean instanceof SecurityUser) {
				SecurityUser securityUser = (SecurityUser) bean;
				if (!StringUtils.isEmpty(securityUser.getUserName())) {
					getCacheHandler().remove(getCacheName(), securityUser.getUserName());
					LogUtils.info(getClass(), "Cleared cache for user: " + securityUser.getUserName());
					return;
				}
			}
			getCacheHandler().clear(getCacheName());
			LogUtils.info(getClass(), "Cleared cache for all users");
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, UserAccessHolder> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, UserAccessHolder> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
