package com.clifton.security.authorization.retriever;


import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.security.authorization.retriever.cache.SecurityResourceAccessCache;
import com.clifton.security.authorization.retriever.cache.SecurityUserAccessCache;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.security.authorization.setup.search.SecurityPermissionAssignmentSearchForm;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>SecurityUserAccessRetrieverImpl</code> class is a default implementation of {@link SecurityUserAccessRetriever} interface.
 *
 * @author vgomelsky
 */
@Component
public class SecurityUserAccessRetrieverImpl implements SecurityUserAccessRetriever {

	private SecurityAuthorizationSetupService securityAuthorizationSetupService;
	private SecurityUserService securityUserService;

	private SecurityResourceAccessCache securityResourceAccessCache;
	private SecurityUserAccessCache securityUserAccessCache;


	////////////////////////////////////////////////////////////////
	///////////        Access Methods - By User         ////////////
	////////////////////////////////////////////////////////////////


	@Override
	public UserAccessHolder getUserAccessHolder(String userName) {
		UserAccessHolder result = getSecurityUserAccessCache().getUserAccessHolder(userName);
		if (result == null) {
			SecurityUser user = getSecurityUserService().getSecurityUserByName(userName);
			AssertUtils.assertNotNull(user, "Cannot find user: %s", userName);
			// determine if the user is an administrator
			boolean admin = false;
			List<SecurityGroup> groupList = getSecurityUserService().getSecurityGroupListByUser(user.getId());
			for (SecurityGroup group : CollectionUtils.getIterable(groupList)) {
				if (group.isAdmin()) {
					admin = true;
					break;
				}
			}
			// get user permissions expanded (support inheritance)
			Map<String, SecurityPermission> resourcePermissionMap = new ConcurrentHashMap<>();
			List<SecurityPermissionAssignment> permissionAssignmentList = getSecurityAuthorizationSetupService().getSecurityPermissionAssignmentListByUser(user.getId());
			for (SecurityPermissionAssignment assignment : CollectionUtils.getIterable(permissionAssignmentList)) {
				String resourceName = assignment.getSecurityResource().getName();
				int permissionMask = assignment.getSecurityPermission().getPermissionMask();
				if (resourcePermissionMap.containsKey(resourceName)) {
					// merge permissions if multiple are set for the same resource
					permissionMask = permissionMask | resourcePermissionMap.get(resourceName).getPermissionMask();
				}
				resourcePermissionMap.put(resourceName, new SecurityPermission(permissionMask));
				// handle children recursively
				if (assignment.isInheritedByChildResources()) {
					addChildResources(assignment.getSecurityResource().getId(), resourcePermissionMap, assignment.getSecurityPermission().getPermissionMask());
				}
			}
			result = new UserAccessHolder(admin, user.isDisabled(), resourcePermissionMap);
			getSecurityUserAccessCache().storeUserAccessHolder(userName, result);
		}
		return result;
	}


	/**
	 * Recursively applies the specified permissions mark to children of the specified security resource.
	 * Adds child resources to the specified Map. If the Map already had that resource, merges permission masks.
	 */
	private void addChildResources(int securityResourceId, Map<String, SecurityPermission> resourcePermissionMap, int permissionMask) {
		List<SecurityResource> childResourceList = getSecurityAuthorizationSetupService().getSecurityResourceListByParent(securityResourceId);
		for (SecurityResource resource : CollectionUtils.getIterable(childResourceList)) {
			int newMask = permissionMask;
			if (resourcePermissionMap.containsKey(resource.getName())) {
				// merge permissions if multiple are set for the same resource
				newMask = newMask | resourcePermissionMap.get(resource.getName()).getPermissionMask();
			}
			resourcePermissionMap.put(resource.getName(), new SecurityPermission(newMask));
			// continue recursively
			addChildResources(resource.getId(), resourcePermissionMap, permissionMask);
		}
	}


	////////////////////////////////////////////////////////////////
	///////////        Access Methods - By Resource     ////////////
	////////////////////////////////////////////////////////////////


	@Override
	public ResourceAccessHolder getResourceAccessHolder(String securityResourceName) {
		SecurityResource resource = getSecurityAuthorizationSetupService().getSecurityResourceByName(securityResourceName);
		if (resource == null) {
			throw new ValidationException("Cannot find security resource with name [" + securityResourceName + "]");
		}

		ResourceAccessHolder accessHolder = getSecurityResourceAccessCache().getResourceAccessHolder(securityResourceName);
		if (accessHolder == null) {
			accessHolder = new ResourceAccessHolder(getSecurityUserService().getSecurityUserListByAdmin());
			applyUserPermissionsForResource(resource, accessHolder, false);
			// Include parent access only where inherited
			while (resource.getParent() != null) {
				applyUserPermissionsForResource(resource.getParent(), accessHolder, true);
				resource = resource.getParent();
			}
			getSecurityResourceAccessCache().storeResourceAccessHolder(securityResourceName, accessHolder);
		}
		return accessHolder;
	}


	private void applyUserPermissionsForResource(SecurityResource resource, ResourceAccessHolder accessHolder, boolean inheritedOnly) {
		SecurityPermissionAssignmentSearchForm searchForm = new SecurityPermissionAssignmentSearchForm();
		searchForm.setSecurityResourceId(resource.getId());
		if (inheritedOnly) {
			searchForm.setInheritedByChildResources(true);
		}
		List<SecurityPermissionAssignment> assignmentList = getSecurityAuthorizationSetupService().getSecurityPermissionAssignmentList(searchForm);
		for (SecurityPermissionAssignment assign : CollectionUtils.getIterable(assignmentList)) {
			if (assign.getSecurityUser() != null) {
				accessHolder.addSecurityUserAccess(assign.getSecurityUser(), assign);
			}
			// Skip Admin Groups - already added
			else if (assign.getSecurityGroup() != null && !assign.getSecurityGroup().isAdmin()) {
				List<SecurityUser> userList = getSecurityUserService().getSecurityUserListByGroup(assign.getSecurityGroup().getId());
				for (SecurityUser user : CollectionUtils.getIterable(userList)) {
					accessHolder.addSecurityUserAccess(user, assign);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationSetupService getSecurityAuthorizationSetupService() {
		return this.securityAuthorizationSetupService;
	}


	public void setSecurityAuthorizationSetupService(SecurityAuthorizationSetupService securityAuthorizationSetupService) {
		this.securityAuthorizationSetupService = securityAuthorizationSetupService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SecurityUserAccessCache getSecurityUserAccessCache() {
		return this.securityUserAccessCache;
	}


	public void setSecurityUserAccessCache(SecurityUserAccessCache securityUserAccessCache) {
		this.securityUserAccessCache = securityUserAccessCache;
	}


	public SecurityResourceAccessCache getSecurityResourceAccessCache() {
		return this.securityResourceAccessCache;
	}


	public void setSecurityResourceAccessCache(SecurityResourceAccessCache securityResourceAccessCache) {
		this.securityResourceAccessCache = securityResourceAccessCache;
	}
}
