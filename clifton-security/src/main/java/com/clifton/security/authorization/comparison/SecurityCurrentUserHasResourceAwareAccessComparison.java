package com.clifton.security.authorization.comparison;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.security.authorization.SecurityResourceAware;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>SecurityCurrentUserHasResourceAwareAccessComparison</code> can be used
 * by entities that implement the {@link SecurityResourceAware} interface to
 * verify security access
 * <p/>
 * Note: If security resource is null, will use security resource associated with the bean's table
 *
 * @author manderson
 */
public class SecurityCurrentUserHasResourceAwareAccessComparison extends SecurityCurrentUserHasAccessComparison {

	private DaoLocator daoLocator;


	@SuppressWarnings("unchecked")
	@Override
	public String getSecurityResource(IdentityObject bean) {
		if (!(bean instanceof SecurityResourceAware)) {
			throw new RuntimeException(getClass().getName() + " can only be performed against SecurityResourceAware beans. " + bean.getClass().getName()
					+ " does not implement SecurityResourceAware interface.");
		}

		SecurityResource resource = ((SecurityResourceAware) bean).getSecurityResource();
		if (resource == null) {
			String tableName = getDaoLocator().locate((Class<IdentityObject>) bean.getClass()).getConfiguration().getTableName();
			resource = getSecurityAuthorizationSetupService().getSecurityResourceByTable(tableName);
			if (resource == null) {
				throw new ValidationException(
						"Cannot determine security resource to use: No explicit security resource defined on SecurityResourceAware bean and cannot find Security Resource for Table [" + tableName
								+ "]");
			}
		}
		return resource.getName();
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
