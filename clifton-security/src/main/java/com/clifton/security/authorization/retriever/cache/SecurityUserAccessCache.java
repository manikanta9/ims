package com.clifton.security.authorization.retriever.cache;

import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.security.authorization.retriever.UserAccessHolder;


/**
 * The <code>SecurityUserAccessCache</code> class allows caching and retrieval from cache of {@link UserAccessHolder} objects.
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding security DAO's.
 *
 * @author vgomelsky
 */
public interface SecurityUserAccessCache {

	public UserAccessHolder getUserAccessHolder(String userName);


	public void storeUserAccessHolder(String userName, UserAccessHolder userAccessHolder);
}
