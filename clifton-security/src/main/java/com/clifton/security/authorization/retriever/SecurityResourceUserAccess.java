package com.clifton.security.authorization.retriever;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.AssertUtils;
import com.clifton.security.user.SecurityUser;


/**
 * The <code>SecurityResourceUserAccess</code> contains user access
 * populated for a specific resource via {@link ResourceAccessHolder}
 *
 * @author manderson
 */
@NonPersistentObject
public class SecurityResourceUserAccess {

	private final SecurityUser securityUser;
	private SecurityPermission permission;
	private final boolean admin;


	public SecurityResourceUserAccess(SecurityUser securityUser, Integer permissionMask, boolean admin) {
		this.securityUser = securityUser;
		this.admin = admin;
		if (!admin) {
			AssertUtils.assertNotNull(permissionMask, "Permission Mask is required if user isn't an admin");
			this.permission = new SecurityPermission(permissionMask);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isReadAllowed() {
		return isAdmin() || getPermission().isReadAllowed();
	}


	public boolean isWriteAllowed() {
		return isAdmin() || getPermission().isWriteAllowed();
	}


	public boolean isCreateAllowed() {
		return isAdmin() || getPermission().isCreateAllowed();
	}


	public boolean isDeleteAllowed() {
		return isAdmin() || getPermission().isDeleteAllowed();
	}


	public boolean isExecuteAllowed() {
		return isAdmin() || getPermission().isExecuteAllowed();
	}


	public boolean isFullControlAllowed() {
		return isAdmin() || getPermission().isFullControlAllowed();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public SecurityPermission getPermission() {
		return this.permission;
	}


	public boolean isAdmin() {
		return this.admin;
	}
}
