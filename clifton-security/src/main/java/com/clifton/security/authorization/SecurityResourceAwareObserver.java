package com.clifton.security.authorization;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.security.authorization.AccessDeniedException;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.security.authorization.SecurityResourceAware;
import org.springframework.stereotype.Component;


/**
 * The <code>SecurityResourceAwareObserver</code> provides specific security resource verification prior
 * to inserts/updates/deletes for a bean based on the SecurityResource returned by the getSecurityResource() method implementation.
 * <p>
 * If no security resource returned - does not do additional security check as method has already checked basic security
 *
 * @author manderson
 */
@Component
public class SecurityResourceAwareObserver<T extends SecurityResourceAware> extends BaseDaoEventObserver<T> {

	private SecurityAuthorizationService securityAuthorizationService;


	@Override
	protected void beforeTransactionMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// do not check security if current user is an admin
		if (getSecurityAuthorizationService().isSecurityUserAdmin()) {
			return;
		}

		// Otherwise - check if a resource is defined
		// Note: If no override defined, then assumes it uses the same security resource as was already determined by the method security
		if (bean != null) {
			SecurityResource resource = bean.getSecurityResource();
			if (resource != null) {
				int requiredPermissions = getRequiredPermissions(event);
				if (!getSecurityAuthorizationService().isSecurityAccessAllowed(resource.getName(), requiredPermissions)) {
					throw new AccessDeniedException(resource.getName(), requiredPermissions);
				}
			}
		}
	}


	private int getRequiredPermissions(DaoEventTypes event) {
		if (event.isDelete()) {
			return SecurityPermission.PERMISSION_DELETE;
		}
		// Secure Methods currently do not appear to differentiate between Create/Update because all use "save" method signatures
		// To keep with the same convention - just checking Write permission here
		return SecurityPermission.PERMISSION_WRITE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
