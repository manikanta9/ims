package com.clifton.security.authorization.retriever;


import com.clifton.core.security.authorization.SecurityPermission;

import java.io.Serializable;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>UserAccessHolder</code> class holds security permissions for a given user. It provides very fast authorization checks
 * and should be used for caching user access rights.
 *
 * @author vgomelsky
 */
public class UserAccessHolder implements Serializable {

	private final boolean admin;
	private final boolean disabled;
	private Map<String, SecurityPermission> resourcePermissionMap;


	/**
	 * Constructs a new instance of {@link UserAccessHolder} using the specified arguments.
	 */
	public UserAccessHolder(boolean admin, boolean disabled, Map<String, SecurityPermission> resourcePermissionMap) {
		this.admin = admin;
		this.disabled = disabled;
		this.resourcePermissionMap = resourcePermissionMap;
		if (this.resourcePermissionMap == null) {
			this.resourcePermissionMap = new ConcurrentHashMap<>();
		}
	}


	/**
	 * Returns true if the user is not disabled and is an admin.
	 */
	public boolean isAdmin() {
		if (this.disabled) {
			return false;
		}
		return this.admin;
	}


	/**
	 * Returns true if the user is an admin or has required access to the specified security resource.
	 */
	public boolean isAccessAllowed(String securityResourceName, int requiredPermissions) {
		if (isAdmin()) {
			return true;
		}

		SecurityPermission grantedPermissions = this.resourcePermissionMap.get(securityResourceName);
		if (grantedPermissions == null) {
			return false;
		}
		return grantedPermissions.isMaskAllowed(requiredPermissions);
	}
}
