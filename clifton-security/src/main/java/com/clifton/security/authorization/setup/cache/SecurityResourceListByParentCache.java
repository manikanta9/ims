package com.clifton.security.authorization.setup.cache;

import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.core.security.authorization.SecurityResource;
import org.springframework.stereotype.Component;


/**
 * A cache implementation that is designed to cache a list of SecurityResource using a the parent resource ID.
 *
 * @author davidi
 */
@Component
public class SecurityResourceListByParentCache extends SelfRegisteringSingleKeyDaoListCache<SecurityResource, Integer> {


	@Override
	protected String getBeanKeyProperty() {
		return "parent.id";
	}


	@Override
	protected Integer getBeanKeyValue(SecurityResource bean) {
		return bean.getParent() != null ? bean.getParent().getId() : null;
	}
}
