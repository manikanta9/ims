package com.clifton.security.authorization;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;


/**
 * The <code>SecurityPermissionAssignment</code> class represents security permission assignments for a
 * give {@link SecurityResource} to either {@link SecurityUser} or {@link SecurityGroup}.
 *
 * @author vgomelsky
 */
public class SecurityPermissionAssignment extends BaseEntity<Integer> implements LabeledObject {

	private SecurityResource securityResource;
	private SecurityUser securityUser;
	private SecurityGroup securityGroup;
	private boolean inheritedByChildResources = true;
	/**
	 * Optional comments about this assignment.  For example, when an exception is made, can explain why.
	 * If there's more complex security (additional row/field level restrictions for DAO), can explain what they are.  Etc.
	 */
	private String assignmentNotes;

	/**
	 * The actual database field is 'GrantedPermissionMask', upon retrieval it is used to set the securityPermission object
	 * by passing the int value to it's constructor.  When permissions are modified in the UI the securityPermission object
	 * is bound to from the UI form post and the new int value saved to the database is derived from the 'getPermissionMask'
	 * method of the 'securityPermission' object.
	 */
	@NonPersistentField
	private SecurityPermission securityPermission;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getGrantedPermissionMask() {
		if (this.securityPermission == null) {
			return 0;
		}
		return this.securityPermission.getPermissionMask();
	}


	public void setGrantedPermissionMask(int grantedPermissionMask) {
		this.securityPermission = new SecurityPermission(grantedPermissionMask);
	}


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder(100);
		if (this.securityGroup != null) {
			result.append(this.securityGroup.getName());
		}
		if (this.securityUser != null) {
			result.append(this.securityUser.getUserName());
		}
		if (this.securityResource != null) {
			result.append(" [");
			result.append(this.securityResource.getLabelExpanded());
			result.append(']');
		}
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityPermission getSecurityPermission() {
		return this.securityPermission;
	}


	public void setSecurityPermission(SecurityPermission securityPermission) {
		this.securityPermission = securityPermission;
	}


	public SecurityResource getSecurityResource() {
		return this.securityResource;
	}


	public void setSecurityResource(SecurityResource securityResource) {
		this.securityResource = securityResource;
	}


	public SecurityUser getSecurityUser() {
		return this.securityUser;
	}


	public void setSecurityUser(SecurityUser securityUser) {
		this.securityUser = securityUser;
	}


	public SecurityGroup getSecurityGroup() {
		return this.securityGroup;
	}


	public void setSecurityGroup(SecurityGroup securityGroup) {
		this.securityGroup = securityGroup;
	}


	public boolean isInheritedByChildResources() {
		return this.inheritedByChildResources;
	}


	public void setInheritedByChildResources(boolean inheritedByChildResources) {
		this.inheritedByChildResources = inheritedByChildResources;
	}


	public String getAssignmentNotes() {
		return this.assignmentNotes;
	}


	public void setAssignmentNotes(String assignmentNotes) {
		this.assignmentNotes = assignmentNotes;
	}
}
