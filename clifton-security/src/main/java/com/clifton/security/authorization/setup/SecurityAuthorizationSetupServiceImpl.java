package com.clifton.security.authorization.setup;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.security.authorization.SecurityPermissionMask;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.security.authorization.setup.search.SecurityPermissionAssignmentSearchForm;
import com.clifton.security.authorization.setup.search.SecurityResourceSearchForm;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SecurityAuthorizationSetupServiceImpl</code> provides basic implementation of {@link SecurityAuthorizationSetupService} interface.
 *
 * @author vgomelsky
 */
@Service
public class SecurityAuthorizationSetupServiceImpl implements SecurityAuthorizationSetupService {

	private AdvancedUpdatableDAO<SecurityPermissionAssignment, Criteria> securityPermissionAssignmentDAO;
	private AdvancedUpdatableDAO<SecurityResource, Criteria> securityResourceDAO;

	private DaoNamedEntityCache<SecurityResource> securityResourceCache;
	private DaoSingleKeyListCache<SecurityResource, Integer> securityResourceListByParentCache;


	////////////////////////////////////////////////////////////////////////////
	////////         SecurityPermission Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<SecurityPermissionMask> getSecurityPermissionMaskList() {
		return SecurityPermissionMask.getAllMasks();
	}


	////////////////////////////////////////////////////////////////////////////
	////////     SecurityPermissionAssignment Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityPermissionAssignment getSecurityPermissionAssignment(int id) {
		return getSecurityPermissionAssignmentDAO().findByPrimaryKey(id);
	}


	@Override
	public List<SecurityPermissionAssignment> getSecurityPermissionAssignmentList(SecurityPermissionAssignmentSearchForm searchForm) {
		return getSecurityPermissionAssignmentDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public List<SecurityPermissionAssignment> getSecurityPermissionAssignmentListByGroup(short groupId) {
		return getSecurityPermissionAssignmentDAO().findByField("securityGroup.id", groupId);
	}


	@Override
	public List<SecurityPermissionAssignment> getSecurityPermissionAssignmentListByUser(final short userId) {
		List<SecurityPermissionAssignment> userAssignments = getSecurityPermissionAssignmentDAO().findByField("securityUser.id", userId);
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createCriteria("securityGroup", "g").createAlias("userList", "u").add(Restrictions.eq("u.id", userId));
		List<SecurityPermissionAssignment> groupAssignments = getSecurityPermissionAssignmentDAO().findBySearchCriteria(searchConfigurer);
		// merge both lists
		if (groupAssignments == null) {
			return userAssignments;
		}
		if (userAssignments != null) {
			groupAssignments.addAll(userAssignments);
		}
		return groupAssignments;
	}


	@Override
	public SecurityPermissionAssignment saveSecurityPermissionAssignment(SecurityPermissionAssignment securityPermissionAssignment) {
		ValidationUtils.assertFalse(securityPermissionAssignment.getSecurityUser() == null && securityPermissionAssignment.getSecurityGroup() == null,
				"Permissions must be assigned to either User or Group.");
		ValidationUtils.assertFalse(securityPermissionAssignment.getSecurityUser() != null && securityPermissionAssignment.getSecurityGroup() != null,
				"Permissions cannot be assigned to both User or Group at the same time.");
		ValidationUtils.assertFalse(securityPermissionAssignment.getGrantedPermissionMask() == 0, "At least one permission must be selected.");

		return getSecurityPermissionAssignmentDAO().save(securityPermissionAssignment);
	}


	@Override
	public void deleteSecurityPermissionAssignment(int id) {
		getSecurityPermissionAssignmentDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////          SecurityResource Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityResource getSecurityResource(int id) {
		return getSecurityResourceDAO().findByPrimaryKey(id);
	}


	@Override
	public SecurityResource getSecurityResourceByName(String name) {
		return getSecurityResourceCache().getBeanForKeyValue(getSecurityResourceDAO(), name);
	}


	@Override
	public SecurityResource getSecurityResourceByTable(@SuppressWarnings("unused") final String tableName) {
		// Expects to be overridden as it depends on System Project
		// See System Project for current override
		throw new IllegalStateException("SecurityAuthorizationSetupService.getSecurityResourceByTable must be overridden by project that knows about Tables.");
	}


	@Override
	public List<SecurityResource> getSecurityResourceListRoot() {
		return getSecurityResourceDAO().findByField("parent", (Object) null);
	}


	@Override
	public List<SecurityResource> getSecurityResourceListByParent(int parentSecurityResourceId) {
		return getSecurityResourceListByParentCache().getBeanListForKeyValue(getSecurityResourceDAO(), parentSecurityResourceId);
	}


	@Override
	public List<SecurityResource> getSecurityResourceList(SecurityResourceSearchForm searchForm) {
		// Default Sorting if none defined
		if (StringUtils.isEmpty(searchForm.getOrderBy())) {
			searchForm.setOrderBy("labelExpanded");
		}
		return getSecurityResourceDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public SecurityResource saveSecurityResource(SecurityResource securityResource) {
		return getSecurityResourceDAO().save(securityResource);
	}


	@Override
	public void deleteSecurityResource(int id) {
		getSecurityResourceDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<SecurityResource, Criteria> getSecurityResourceDAO() {
		return this.securityResourceDAO;
	}


	public void setSecurityResourceDAO(AdvancedUpdatableDAO<SecurityResource, Criteria> securityResourceDAO) {
		this.securityResourceDAO = securityResourceDAO;
	}


	public AdvancedUpdatableDAO<SecurityPermissionAssignment, Criteria> getSecurityPermissionAssignmentDAO() {
		return this.securityPermissionAssignmentDAO;
	}


	public void setSecurityPermissionAssignmentDAO(AdvancedUpdatableDAO<SecurityPermissionAssignment, Criteria> securityPermissionAssignmentDAO) {
		this.securityPermissionAssignmentDAO = securityPermissionAssignmentDAO;
	}


	public DaoNamedEntityCache<SecurityResource> getSecurityResourceCache() {
		return this.securityResourceCache;
	}


	public void setSecurityResourceCache(DaoNamedEntityCache<SecurityResource> securityResourceCache) {
		this.securityResourceCache = securityResourceCache;
	}


	public DaoSingleKeyListCache<SecurityResource, Integer> getSecurityResourceListByParentCache() {
		return this.securityResourceListByParentCache;
	}


	public void setSecurityResourceListByParentCache(DaoSingleKeyListCache<SecurityResource, Integer> securityResourceListByParentCache) {
		this.securityResourceListByParentCache = securityResourceListByParentCache;
	}
}
