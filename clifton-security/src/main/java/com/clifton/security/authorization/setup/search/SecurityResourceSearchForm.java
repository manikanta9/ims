package com.clifton.security.authorization.setup.search;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.security.authorization.SecurityResource;


/**
 * The <code>SecurityResourceSearchForm</code> class defines search configuration for SecurityResource objects.
 *
 * @author vgomelsky
 */
@SearchForm(ormDtoClass = SecurityResource.class)
public class SecurityResourceSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,label,description", sortField = "label")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "id")
	private Integer[] ids;

	@SearchField
	private String name;

	@SearchField
	private String label;

	// Currently using max of 4 levels deep
	@SearchField(searchField = "parent.parent.parent.label,parent.parent.label,parent.label,label", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String labelExpanded;

	// Currently using max of 4 levels deep
	@SearchField(searchField = "parent.parent.parent.name,parent.parent.name,parent.name,name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.CONCATENATE_SORT_WITH_DELIMITER)
	private String nameExpanded;

	@SearchField
	private String description;

	@SearchField(searchField = "parent.id")
	private Integer parentId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer[] getIds() {
		return this.ids;
	}


	public void setIds(Integer[] ids) {
		this.ids = ids;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getLabelExpanded() {
		return this.labelExpanded;
	}


	public void setLabelExpanded(String labelExpanded) {
		this.labelExpanded = labelExpanded;
	}


	public String getNameExpanded() {
		return this.nameExpanded;
	}


	public void setNameExpanded(String nameExpanded) {
		this.nameExpanded = nameExpanded;
	}
}
