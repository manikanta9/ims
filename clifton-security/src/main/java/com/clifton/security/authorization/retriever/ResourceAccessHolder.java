package com.clifton.security.authorization.retriever;


import com.clifton.core.util.CollectionUtils;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.security.user.SecurityUser;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>ResourceAccessHolder</code> class holds security permissions for a given security resource
 * <p>
 * Cached by Resource Name (And special list to hold admin users) so can display list
 * of users and their permission for the resource. Also properly checks parent resource
 * permission assignments that are inherited by it's children.
 * <p>
 * If user belongs more than one group - or has explicit security permission assignment
 * to a resource - the highest permission is stored.  i.e. Group 1 gives them read access, but group 2 gives them write access - they are viewed as having write access
 *
 * @author manderson
 */
public class ResourceAccessHolder implements Serializable {

	private final Map<SecurityUser, Integer> userPermissionMaskMap = new ConcurrentHashMap<>();
	private final Map<Short, Boolean> adminUsersMap = new ConcurrentHashMap<>();


	public ResourceAccessHolder(List<SecurityUser> adminUsers) {
		for (SecurityUser admin : CollectionUtils.getIterable(adminUsers)) {
			this.userPermissionMaskMap.put(admin, 0);
			this.adminUsersMap.put(admin.getId(), true);
		}
	}


	public void addSecurityUserAccess(SecurityUser user, SecurityPermissionAssignment assignment) {
		// Only need to add if Not an Admin
		if (!isUserAdmin(user)) {
			int permissionMask = assignment.getSecurityPermission().getPermissionMask();
			if (this.userPermissionMaskMap.containsKey(user)) {
				// merge permissions if multiple are set for the same user
				permissionMask = permissionMask | this.userPermissionMaskMap.get(user);
			}
			this.userPermissionMaskMap.put(user, permissionMask);
		}
	}


	private boolean isUserAdmin(SecurityUser user) {
		return this.adminUsersMap.containsKey(user.getId());
	}


	public List<SecurityResourceUserAccess> getSecurityUserAccessList(boolean includeDisabled) {
		List<SecurityResourceUserAccess> list = new ArrayList<>();
		for (Map.Entry<SecurityUser, Integer> securityUserIntegerEntry : this.userPermissionMaskMap.entrySet()) {
			if (includeDisabled || !(securityUserIntegerEntry.getKey()).isDisabled()) {
				list.add(new SecurityResourceUserAccess(securityUserIntegerEntry.getKey(), securityUserIntegerEntry.getValue(), isUserAdmin(securityUserIntegerEntry.getKey())));
			}
		}
		return list;
	}
}
