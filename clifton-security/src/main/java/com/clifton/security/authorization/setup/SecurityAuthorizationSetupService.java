package com.clifton.security.authorization.setup;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermissionMask;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.security.authorization.setup.search.SecurityPermissionAssignmentSearchForm;
import com.clifton.security.authorization.setup.search.SecurityResourceSearchForm;

import java.util.List;


/**
 * The <code>SecurityAuthorizationSetupService</code> interface defines methods for setting up and configuring authorization related objects.
 *
 * @author vgomelsky
 */
public interface SecurityAuthorizationSetupService {

	////////////////////////////////////////////////////////////////////////////
	////////         SecurityPermission Business Methods           /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a List of possible permission masks (combinations of permissions).
	 */
	@SecureMethod(dtoClass = SecurityResource.class)
	public List<SecurityPermissionMask> getSecurityPermissionMaskList();


	////////////////////////////////////////////////////////////////////////////
	////////     SecurityPermissionAssignment Business Methods     /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityPermissionAssignment getSecurityPermissionAssignment(int id);


	public List<SecurityPermissionAssignment> getSecurityPermissionAssignmentList(SecurityPermissionAssignmentSearchForm searchForm);


	public List<SecurityPermissionAssignment> getSecurityPermissionAssignmentListByGroup(short groupId);


	/**
	 * Returns a List of all {@link SecurityPermissionAssignment} objects for the specified user.
	 * Includes direct user assignments as well as assignments for specified user's groups.
	 */
	public List<SecurityPermissionAssignment> getSecurityPermissionAssignmentListByUser(final short userId);


	public SecurityPermissionAssignment saveSecurityPermissionAssignment(SecurityPermissionAssignment securityPermissionAssignment);


	public void deleteSecurityPermissionAssignment(int id);


	////////////////////////////////////////////////////////////////////////////
	////////          SecurityResource Business Methods            /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityResource getSecurityResource(int id);


	public SecurityResource getSecurityResourceByName(String name);


	public SecurityResource getSecurityResourceByTable(String tableName);


	/**
	 * Returns a List of all root-level {@link SecurityResource} objects (don't have parents).
	 */
	public List<SecurityResource> getSecurityResourceListRoot();


	/**
	 * Returns a List of {@link SecurityResource} objects that are first level children of the specified security resource.
	 */
	public List<SecurityResource> getSecurityResourceListByParent(int parentSecurityResourceId);


	public List<SecurityResource> getSecurityResourceList(SecurityResourceSearchForm searchForm);


	public SecurityResource saveSecurityResource(SecurityResource securityResource);


	public void deleteSecurityResource(int id);
}
