package com.clifton.security.authorization.retriever.cache;

import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.security.authorization.retriever.ResourceAccessHolder;


/**
 * The <code>SecurityResourceAccessCacheImpl</code> class allows caching and retrieval from cache a {@link ResourceAccessHolder} object
 * for a specified security resource.
 * <p>
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding security DAO's.
 *
 * @author manderson
 */
public interface SecurityResourceAccessCache {

	public ResourceAccessHolder getResourceAccessHolder(String securityResourceName);


	public void storeResourceAccessHolder(String securityResourceName, ResourceAccessHolder holder);
}
