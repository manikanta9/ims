package com.clifton.security.authorization;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.security.authorization.retriever.SecurityResourceUserAccess;
import com.clifton.security.user.SecurityUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The <code>SecurityAuthorizationService</code> interface defines methods for checking whether a
 * user has required access to a particular resource.
 *
 * @author vgomelsky
 */
public interface SecurityAuthorizationService {

	@ResponseBody
	@SecureMethod(disableSecurity = true)
	@RequestMapping("securityCurrentUserIsAdmin")
	public boolean isSecurityUserAdmin();


	@RequestMapping("securityUserIsAdmin")
	@SecureMethod(dtoClass = SecurityUser.class)
	public boolean isSecurityUserAdmin(String userName);


	@ResponseBody
	@SecureMethod(disableSecurity = true)
	@RequestMapping("securityAccessIsAllowedForCurrentUser")
	public boolean isSecurityAccessAllowed(String securityResourceName, int requiredPermissions);


	@RequestMapping("securityAccessIsAllowed")
	@SecureMethod(dtoClass = SecurityPermissionAssignment.class)
	public boolean isSecurityAccessAllowed(String userName, String securityResourceName, int requiredPermissions);


	/**
	 * For a specific security resource - returns a list of users with permissions, whether that user is an admin
	 * Option to include disabled users in the results
	 */
	@SecureMethod(dtoClass = SecurityPermissionAssignment.class)
	public List<SecurityResourceUserAccess> getSecurityResourceUserAccessList(String securityResourceName, boolean includeDisabled);
}
