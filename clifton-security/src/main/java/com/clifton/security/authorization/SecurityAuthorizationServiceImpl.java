package com.clifton.security.authorization;


import com.clifton.core.util.AssertUtils;
import com.clifton.security.authorization.retriever.ResourceAccessHolder;
import com.clifton.security.authorization.retriever.SecurityResourceUserAccess;
import com.clifton.security.authorization.retriever.SecurityUserAccessRetriever;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * The <code>SecurityAuthorizationServiceImpl</code> provides basic implementation of {@link SecurityAuthorizationService} interface.
 *
 * @author vgomelsky
 */
@Service
public class SecurityAuthorizationServiceImpl implements SecurityAuthorizationService {

	private SecurityUserService securityUserService;
	private SecurityUserAccessRetriever securityUserAccessRetriever;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isSecurityUserAdmin() {
		SecurityUser user = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(user, "cannot find current user");
		return isSecurityUserAdmin(user.getUserName());
	}


	@Override
	public boolean isSecurityUserAdmin(String userName) {
		return getSecurityUserAccessRetriever().getUserAccessHolder(userName).isAdmin();
	}


	@Override
	public boolean isSecurityAccessAllowed(String securityResourceName, int requiredPermissions) {
		SecurityUser user = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(user, "cannot find current user");
		return isSecurityAccessAllowed(user.getUserName(), securityResourceName, requiredPermissions);
	}


	@Override
	public boolean isSecurityAccessAllowed(String userName, String securityResourceName, int requiredPermissions) {
		return getSecurityUserAccessRetriever().getUserAccessHolder(userName).isAccessAllowed(securityResourceName, requiredPermissions);
	}


	@Override
	public List<SecurityResourceUserAccess> getSecurityResourceUserAccessList(String securityResourceName, boolean includeDisabled) {
		ResourceAccessHolder accessHolder = getSecurityUserAccessRetriever().getResourceAccessHolder(securityResourceName);
		if (accessHolder != null) {
			return accessHolder.getSecurityUserAccessList(includeDisabled);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////               Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityUserAccessRetriever getSecurityUserAccessRetriever() {
		return this.securityUserAccessRetriever;
	}


	public void setSecurityUserAccessRetriever(SecurityUserAccessRetriever securityUserAccessRetriever) {
		this.securityUserAccessRetriever = securityUserAccessRetriever;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
