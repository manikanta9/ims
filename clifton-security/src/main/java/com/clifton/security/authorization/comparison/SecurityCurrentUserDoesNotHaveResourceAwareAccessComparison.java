package com.clifton.security.authorization.comparison;


import com.clifton.core.security.authorization.SecurityResourceAware;


/**
 * The <code>SecurityCurrentUserDoesNotHaveResourceAwareAccessComparison</code> can be used
 * by entities that implement the {@link SecurityResourceAware} interface to
 * verify no security access
 * <p/>
 * Note: If security resource is null, uses security resource associated with the bean's table
 *
 * @author manderson
 */
public class SecurityCurrentUserDoesNotHaveResourceAwareAccessComparison extends SecurityCurrentUserHasResourceAwareAccessComparison {

	@Override
	public boolean isReverse() {
		return true;
	}
}
