package com.clifton.security.authorization.comparison;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.security.authorization.SecurityAuthorizationService;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;


/**
 * The SecurityCurrentUserHasAccessComparison class is a {@link Comparison} that evaluates to true
 * when current user has specified access to the specified security resource.
 */
public class SecurityCurrentUserHasAccessComparison implements Comparison<IdentityObject> {

	private SecurityUserService securityUserService;
	private SecurityAuthorizationService securityAuthorizationService;
	private SecurityAuthorizationSetupService securityAuthorizationSetupService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Security Resource name which the current user's permissions are checked for
	 */
	private String securityResourceName;

	/**
	 * Table name property for the given bean (can be used to dynamically find security resource name instead of
	 * setting it)
	 */
	private String tableNameBeanPropertyName;

	/**
	 * Can be used to append a value to the table name to get the Security Resource name
	 * i.e. SystemNote security looks up table name on the note type, and then appends Note to the table
	 * name to get the security resource name;
	 */
	private String appendToTableNameValue;

	/**
	 * Permission level to check for - defaults to Read Access
	 */
	private int requiredPermission = SecurityPermission.PERMISSION_READ;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		// get current user
		SecurityUser currentUser = getSecurityUserService().getSecurityUserCurrent();
		AssertUtils.assertNotNull(currentUser, "Current user is not set.");

		boolean result = getSecurityAuthorizationService().isSecurityAccessAllowed(getSecurityResource(bean), getRequiredPermission());

		if (isReverse()) {
			result = !result;
		}

		// record comparison result message
		if (context != null) {
			String messageNotEqual = "(Current user '" + currentUser.getLabel() + "' does not have access to '" + getSecurityResourceName() + "')";
			String messageEqual = "(Current user '" + currentUser.getLabel() + "' does have access to '" + getSecurityResourceName() + "')";

			if (result) {
				context.recordTrueMessage(isReverse() ? messageNotEqual : messageEqual);
			}
			else {
				context.recordFalseMessage(isReverse() ? messageEqual : messageNotEqual);
			}
		}

		return result;
	}


	protected boolean isReverse() {
		return false;
	}


	/**
	 * Used by override methods that determine security resource based on the bean
	 * i.e. SystemNote document check out gets the table name of the note type and appends "Note" to
	 * it to generate the security resource name
	 */
	protected String getSecurityResource(IdentityObject bean) {
		SecurityResource resource = null;
		String msg = "";

		if (!StringUtils.isEmpty(getTableNameBeanPropertyName())) {
			String tableName = (String) BeanUtils.getPropertyValue(bean, getTableNameBeanPropertyName());
			if (!StringUtils.isEmpty(getAppendToTableNameValue())) {
				// Check if the securityResource exists
				resource = getSecurityAuthorizationSetupService().getSecurityResourceByName(tableName + getAppendToTableNameValue());
				msg = "Cannot find Security Resource with Name [" + tableName + getAppendToTableNameValue() + "]. ";
			}
			if (resource == null) {
				resource = getSecurityAuthorizationSetupService().getSecurityResourceByTable(tableName);
				msg = "Cannot find Security Resource for Table [" + tableName + "]. ";
			}
		}
		if (resource == null) {
			resource = getSecurityAuthorizationSetupService().getSecurityResourceByName(getSecurityResourceName());
			msg = "Cannot find Security Resource with name [" + getSecurityResourceName() + "]. ";
		}
		if (resource == null) {
			throw new ValidationException("Cannot determine security resource to use: " + msg);
		}
		return resource.getName();
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public String getSecurityResourceName() {
		return this.securityResourceName;
	}


	public void setSecurityResourceName(String securityResourceName) {
		this.securityResourceName = securityResourceName;
	}


	public int getRequiredPermission() {
		return this.requiredPermission;
	}


	public void setRequiredPermission(int requiredPermission) {
		this.requiredPermission = requiredPermission;
	}


	public String getTableNameBeanPropertyName() {
		return this.tableNameBeanPropertyName;
	}


	public void setTableNameBeanPropertyName(String tableNameBeanPropertyName) {
		this.tableNameBeanPropertyName = tableNameBeanPropertyName;
	}


	public String getAppendToTableNameValue() {
		return this.appendToTableNameValue;
	}


	public void setAppendToTableNameValue(String appendToTableNameValue) {
		this.appendToTableNameValue = appendToTableNameValue;
	}


	public SecurityAuthorizationSetupService getSecurityAuthorizationSetupService() {
		return this.securityAuthorizationSetupService;
	}


	public void setSecurityAuthorizationSetupService(SecurityAuthorizationSetupService securityAuthorizationSetupService) {
		this.securityAuthorizationSetupService = securityAuthorizationSetupService;
	}
}
