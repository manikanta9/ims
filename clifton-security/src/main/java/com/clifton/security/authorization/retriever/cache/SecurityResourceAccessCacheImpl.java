package com.clifton.security.authorization.retriever.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.security.authorization.retriever.ResourceAccessHolder;
import org.springframework.stereotype.Component;


/**
 * The <code>SecurityResourceAccessCacheImpl</code> class allows caching and retrieval from cache a {@link ResourceAccessHolder} object
 * for a specified security resource.
 * <p>
 * The class also implements {@link DaoEventObserver} and clears the cache whenever update methods to related DTO's happen.
 * Make sure to register this class as an observer to corresponding security DAO's.
 *
 * @param <T>
 * @author manderson
 */
@Component
public class SecurityResourceAccessCacheImpl<T extends IdentityObject> extends BaseDaoEventObserver<T> implements CustomCache<String, ResourceAccessHolder>, SecurityResourceAccessCache {

	private CacheHandler<String, ResourceAccessHolder> cacheHandler;

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public ResourceAccessHolder getResourceAccessHolder(String securityResourceName) {
		return getCacheHandler().get(getCacheName(), securityResourceName);
	}


	@Override
	public void storeResourceAccessHolder(String securityResourceName, ResourceAccessHolder holder) {
		getCacheHandler().put(getCacheName(), securityResourceName, holder);
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, @SuppressWarnings("unused") T bean, Throwable e) {
		if (e == null) {
			// Clear all as any change to a user/group/assignment can affect resource permissions - especially because of resource inheritance
			getCacheHandler().clear(getCacheName());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<String, ResourceAccessHolder> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, ResourceAccessHolder> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
