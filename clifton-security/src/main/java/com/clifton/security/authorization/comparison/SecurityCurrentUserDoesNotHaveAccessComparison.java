package com.clifton.security.authorization.comparison;


import com.clifton.core.comparison.Comparison;


/**
 * The SecurityCurrentUserDoesNotHaveAccessComparison class is a {@link Comparison} that evaluates to true
 * when current user does not have specified access to the specified security resource.
 *
 * @author Mary Anderson
 */
public class SecurityCurrentUserDoesNotHaveAccessComparison extends SecurityCurrentUserHasAccessComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
