package com.clifton.security.authorization.retriever;


/**
 * The <code>SecurityUserAccessRetriever</code> interface defines methods for retrieving pre-populated
 * and cached user access rights.
 *
 * @author vgomelsky
 */
public interface SecurityUserAccessRetriever {

	////////////////////////////////////////////////////////////////
	///////////        UserAccessHolder Methods         ////////////
	////////////////////////////////////////////////////////////////


	/**
	 * Returns populated {@link UserAccessHolder} object.  First looks it up in cache and if not found,
	 * retrieves that data necessary to populate the object and caches it in the end.
	 *
	 * @param userName
	 */
	public UserAccessHolder getUserAccessHolder(String userName);


	////////////////////////////////////////////////////////////////
	//////////       ResourceAccessHolder Methods       ////////////
	////////////////////////////////////////////////////////////////


	/**
	 * Returns populated {@link ResourceAccessHolder} object. First looks it up in the cache and if not found,
	 * retrieves that data necessary to populate the object and caches it in the end.
	 *
	 * @param securityResourceName
	 */
	public ResourceAccessHolder getResourceAccessHolder(String securityResourceName);
}
