Clifton.security.user.GroupWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Group',
	iconCls: 'group',
	width: 900,
	height: 500,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.security.user.GroupWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Groups are collections of users. Use <i>Pseudonym</i> field to map this group to an external authentication provider like Active Directory.',
					url: 'securityGroup.json',
					items: [
						{fieldLabel: 'Group Name', name: 'name'},
						{fieldLabel: 'Pseudonym', name: 'integrationPseudonym'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{
							fieldLabel: 'Group Owner', name: 'ownerSecurityUser.label', displayField: 'label', hiddenName: 'ownerSecurityUser.id', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', loadAll: true, detailPageClass: 'Clifton.security.user.UserWindow',
							qtip: 'The group owner is the Security User responsible for reviewing and approving changes to the group.'
						},
						{fieldLabel: 'Email', name: 'emailAddress', qtip: 'The email address associated with this group (e.g. email address for a team or organizational contact).'},
						{fieldLabel: 'Administrator', boxLabel: 'Members of this group have unrestricted access to everything.', name: 'admin', xtype: 'checkbox'},
						{fieldLabel: 'Disabled', name: 'disabled', xtype: 'checkbox'},
						{
							xtype: 'system-tags-fieldset',
							title: 'Tags',
							tableName: 'SecurityGroup'
						}
					]
				}]
			},


			{
				title: 'Users',
				items: [{
					name: 'securityUserListFind',
					xtype: 'gridpanel',
					instructions: 'Following users belong to this group. To add another user to this group, select desired user from the list and click the [Add] button.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to enabled
							this.setFilterValue('disabled', false);
						}
						return {groupId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'User Name', width: 100, dataIndex: 'userName', defaultSortColumn: true},
						{header: 'Display Name', width: 100, dataIndex: 'displayName'},
						{header: 'Pseudonym', width: 150, dataIndex: 'integrationPseudonym', hidden: true},
						{header: 'Disabled', width: 50, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.user.UserWindow',
						deleteURL: 'securityUserFromGroupUnlink.json',
						allowToDeleteMultiple: true,
						getDeleteParams: function(selectionModel) {
							return {
								groupId: this.getWindow().getMainFormId(),
								userId: selectionModel.getSelected().id
							};
						},
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'user', url: 'securityUserListFind.json?disabled=false', displayField: 'label', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add selected user to this group',
								iconCls: 'add',
								handler: function() {
									const userId = TCG.getChildByName(toolBar, 'user').getValue();
									if (TCG.isBlank(userId)) {
										TCG.showError('You must first select desired user User from the list.');
									}
									else {
										const groupId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {userId: userId, groupId: groupId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'user').reset();
											}
										});
										loader.load('securityUserToGroupLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Permissions',
				items: [{
					name: 'securityPermissionAssignmentListFind',
					xtype: 'gridpanel',
					instructions: 'The following permissions are assigned to this group.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {securityGroupId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Resource', width: 100, dataIndex: 'securityResource.labelExpanded', filter: {searchFieldName: 'securityResourceLabelExpanded'}, defaultSortColumn: true},
						{header: 'Notes', width: 100, dataIndex: 'assignmentNotes', hidden: true},
						{header: 'Full Control', width: 40, dataIndex: 'securityPermission.fullControlAllowed', type: 'boolean'},
						{header: 'Write', width: 30, dataIndex: 'securityPermission.writeAllowed', type: 'boolean'},
						{header: 'Create', width: 30, dataIndex: 'securityPermission.createAllowed', type: 'boolean'},
						{header: 'Delete', width: 30, dataIndex: 'securityPermission.deleteAllowed', type: 'boolean'},
						{header: 'Read', width: 30, dataIndex: 'securityPermission.readAllowed', type: 'boolean'},
						{header: 'Execute', width: 30, dataIndex: 'securityPermission.executeAllowed', type: 'boolean'},
						{header: 'Grant to Children', width: 50, dataIndex: 'inheritedByChildResources', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.authorization.PermissionAssignmentWindow',
						deleteURL: 'securityPermissionAssignmentDelete.json',
						allowToDeleteMultiple: true,
						getDefaultData: function() {
							return {
								securityGroup: {
									id: this.getWindow().getMainFormId(),
									name: this.getWindow().getMainForm().findField('name').value
								}
							};
						}
					}
				}]
			}
		]
	}]
});
