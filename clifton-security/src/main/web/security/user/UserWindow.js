Clifton.security.user.UserWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security User',
	iconCls: 'user',
	width: 900,

	windowOnShow: function(w) {
		const tabs = w.items.get(0);
		const arr = Clifton.security.user.UserWindowAdditionalTabs;
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					instructions: 'Use <i>Pseudonym</i> field to map this user to an external authentication provider like Active Directory.',
					url: 'securityUser.json',
					loadDefaultDataAfterRender: true,
					labelFieldName: 'userName',
					listeners: {
						afterload: function(panel) {
							const form = panel.getForm();
							const contactIdentifier = form.findField('contactIdentifier').getValue();
							if (contactIdentifier) {
								const loader = new TCG.data.JsonLoader({
									waitTarget: panel,
									params: {
										id: contactIdentifier
									},
									onLoad: function(record, conf) {
										const field = form.findField('contact.name');
										if (field) {
											field.setValue({value: record.id, text: record.label});
											field.originalValue = field.getValue();
										}
									}
								});
								loader.load('businessContact.json');
							}
						}
					},
					items: [
						{fieldLabel: 'User Name', name: 'userName'},
						{fieldLabel: 'Display Name', name: 'displayName'},
						{fieldLabel: 'Pseudonym', name: 'integrationPseudonym'},
						{
							fieldLabel: 'Business Contact',
							name: 'contact.name',
							hiddenName: 'contact.id',
							displayField: 'label',
							disableAddNewItem: true,
							xtype: 'combo',
							url: 'businessContactListFind.json',
							detailPageClass: 'Clifton.business.contact.ContactWindow',
							qtip: 'Allows selected contacts with an Employee, Intern, or Temporary or Contract Employee relationship to a company tagged with IMS User Access.',
							submitValue: false,
							listeners: {
								beforequery: function(queryEvent) {
									// Allow Employees, Interns, or Temps
									const store = queryEvent.combo.store;
									store.setBaseParam('contactTypeNames', ['Employee', 'Intern', 'Temporary or Contract Employee']);
									store.setBaseParam('companyContactCompanyHierarchyName', 'IMS User Access');
									store.setBaseParam('companyContactCompanyHierarchyCategoryName', 'Business Company Tags');
								},
								select: function(combo, record, index) {
									const panel = this.getParentForm();
									const form = panel.getForm();
									const field = form.findField('contactIdentifier');
									const id = record.id;
									if (!TCG.isEquals(field.getValue(), id)) {
										field.setValue(id);
									}
								},
								change: function(combo) {
									if (!combo.getValue()) {
										const panel = this.getParentForm();
										const form = panel.getForm();
										form.findField('contactIdentifier').setValue('');
									}
								}
							}
						},
						{
							fieldLabel: 'Contact Identifier',
							name: 'contactIdentifier',
							hiddenName: 'contactIdentifier',
							hidden: true
						},
						{fieldLabel: 'Email', name: 'emailAddress', vtype: 'email'},
						{fieldLabel: 'Disabled', name: 'disabled', xtype: 'checkbox'}
					]
				}]
			},


			{
				title: 'Groups',
				items: [{
					name: 'securityGroupListFind',
					xtype: 'gridpanel',
					instructions: 'Selected user is a member of the following group(s). To add the user to another group, select desired group from the list and click the [Add] button.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to enabled
							this.setFilterValue('disabled', false);
						}
						return {userId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Pseudonym', width: 150, dataIndex: 'integrationPseudonym', hidden: true},
						{header: 'Description', width: 200, dataIndex: 'description'},
						{header: 'Admin', width: 30, dataIndex: 'admin', type: 'boolean'},
						{header: 'Disabled', width: 35, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.user.GroupWindow',
						deleteURL: 'securityUserFromGroupUnlink.json',
						getDeleteParams: function(selectionModel) {
							return {
								userId: this.getWindow().getMainFormId(),
								groupId: selectionModel.getSelected().id
							};
						},
						allowToDeleteMultiple: true,
						addToolbarAddButton: function(toolBar) {
							const gridPanel = this.getGridPanel();
							toolBar.add(new TCG.form.ComboBox({name: 'group', url: 'securityGroupListFind.json', width: 150, listWidth: 230}));
							toolBar.add({
								text: 'Add',
								tooltip: 'Add the user to selected group',
								iconCls: 'add',
								handler: function() {
									const groupId = TCG.getChildByName(toolBar, 'group').getValue();
									if (TCG.isBlank(groupId)) {
										TCG.showError('You must first select desired user Group from the list.');
									}
									else {
										const userId = gridPanel.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gridPanel,
											waitMsg: 'Linking...',
											params: {userId: userId, groupId: groupId},
											onLoad: function(record, conf) {
												gridPanel.reload();
												TCG.getChildByName(toolBar, 'group').reset();
											}
										});
										loader.load('securityUserToGroupLink.json');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Permissions',
				items: [{
					name: 'securityPermissionAssignmentListByUser',
					xtype: 'gridpanel',
					instructions: 'The following permissions are assigned to this user and group(s) that the user belongs to.',
					getLoadParams: function() {
						return {userId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Resource', width: 90, dataIndex: 'securityResource.labelExpanded', defaultSortColumn: true},
						{header: 'Group', width: 60, dataIndex: 'securityGroup.name', tooltip: 'Displays nothing when permission(s) are assigned directly to the user.'},
						{header: 'Notes', width: 100, dataIndex: 'assignmentNotes', hidden: true},
						{header: 'Full Control', width: 40, dataIndex: 'securityPermission.fullControlAllowed', type: 'boolean'},
						{header: 'Write', width: 30, dataIndex: 'securityPermission.writeAllowed', type: 'boolean'},
						{header: 'Create', width: 30, dataIndex: 'securityPermission.createAllowed', type: 'boolean'},
						{header: 'Delete', width: 30, dataIndex: 'securityPermission.deleteAllowed', type: 'boolean'},
						{header: 'Read', width: 30, dataIndex: 'securityPermission.readAllowed', type: 'boolean'},
						{header: 'Execute', width: 30, dataIndex: 'securityPermission.executeAllowed', type: 'boolean'},
						{header: 'Grant to Children', width: 50, dataIndex: 'inheritedByChildResources', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.authorization.PermissionAssignmentWindow',
						deleteURL: 'securityPermissionAssignmentDelete.json',
						allowToDeleteMultiple: true,
						getDefaultData: function() {
							return {
								securityUser: {
									id: this.getWindow().getMainFormId(),
									label: this.getWindow().getMainForm().findField('userName').value
								}
							};
						}
					}
				}]
			},


			{
				title: 'System Aliases',
				items: [{
					name: 'securitySystemUserListFind',
					xtype: 'gridpanel',
					instructions: 'If this user has access to multiple systems, define corresponding aliases (user names in other systems) here.',
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('disabled', false);
						}
						return {securityUserId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Our User', width: 100, dataIndex: 'securityUser.userName', hidden: true},
						{header: 'System', width: 100, dataIndex: 'securitySystem.label', filter: {type: 'combo', searchFieldName: 'securitySystemId', displayField: 'label', url: 'securityUserSystemListFind.json'}, defaultSortColumn: true},
						{header: 'User Name (Alias)', width: 100, dataIndex: 'userName'},
						{header: 'Read Only', width: 50, dataIndex: 'readOnly', type: 'boolean'},
						{header: 'Disabled', width: 50, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.system.SystemUserWindow',
						deleteURL: 'securitySystemUserDelete.json',
						getDefaultData: function() {
							return {
								securityUser: this.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			}
		]
	}]
});
