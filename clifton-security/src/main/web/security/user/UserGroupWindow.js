Clifton.security.user.UserGroupWindow = Ext.extend(TCG.app.DetailWindowSelector, {
	url: 'securityUserGroup.json',

	getClassName: function(config, entity) {
		return 'Clifton.security.user.UserWindow';
	},

	prepareEntity: function(entity) {
		return entity ? entity.referenceOne : entity;
	}
});
