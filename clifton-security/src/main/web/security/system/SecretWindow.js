Clifton.security.system.SecretWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security System Secret',
	iconCls: 'lock',
	height: 350,
	hideApplyButton: true,
	hideCancelButton: true,
	modal: true,
	items: [{
		xtype: 'formpanel',
		instructions: 'The System Security Secret is a container for securely managing passwords.',
		url: 'securitySecretDecrypt.json',
		readOnly: true,
		labelFieldName: 'id',
		items: [
			{fieldLabel: 'Secret', name: 'secretString', hiddenName: 'id', xtype: 'textarea', anchor: '-35 -50'}
		]
	}]
});
