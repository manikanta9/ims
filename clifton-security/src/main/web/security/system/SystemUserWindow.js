Clifton.security.system.SystemUserWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security System User',
	iconCls: 'user',

	items: [{
		xtype: 'formpanel',
		instructions: '<i>User Name (Alias)</i> field maps our user to an external system like REDI.',
		url: 'securitySystemUser.json',
		labelFieldName: 'userName',
		items: [
			{fieldLabel: 'Security System', name: 'securitySystem.name', hiddenName: 'securitySystem.id', xtype: 'combo', url: 'securitySystemListFind.json', detailPageClass: 'Clifton.security.system.SystemWindow'},
			{fieldLabel: 'Our User', name: 'securityUser.userName', hiddenName: 'securityUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', detailPageClass: 'Clifton.security.user.UserWindow'},
			{fieldLabel: 'User Name (Alias)', name: 'userName'},
			{fieldLabel: 'Read Only', name: 'readOnly', xtype: 'checkbox'},
			{fieldLabel: 'Disabled', name: 'disabled', xtype: 'checkbox'}
		]
	}]
});
