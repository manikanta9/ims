Clifton.security.system.SystemWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security System',
	iconCls: 'database',

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'securitySystem.json',
					instructions: 'An external system that our system integrates with.',
					readOnly: true,
					items: [
						{fieldLabel: 'System Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'}
					]
				}]
			},


			{
				title: 'User Aliases',
				items: [{
					name: 'securitySystemUserListFind',
					xtype: 'gridpanel',
					instructions: 'If this user has access to multiple systems, define corresponding aliases (user names in other systems) here.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Our User Name', width: 100, dataIndex: 'securityUser.userName', filter: {type: 'combo', searchFieldName: 'securityUserId', displayField: 'label', url: 'securityUserSystemList.json'}, defaultSortColumn: true},
						{header: 'Our User Display Label', width: 100, dataIndex: 'securityUser.displayName'},
						{header: 'System', width: 100, dataIndex: 'securitySystem.name', filter: {type: 'combo', searchFieldName: 'securitySystemId', displayField: 'label', url: 'securityUserSystemListFind.json'}, hidden: true},
						{header: 'User Name (Alias)', width: 100, dataIndex: 'userName'},
						{header: 'Read Only', width: 50, dataIndex: 'readOnly', type: 'boolean'},
						{header: 'Disabled', width: 50, dataIndex: 'disabled', type: 'boolean'}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('disabled', false);
						}
						return {securitySystemId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.security.system.SystemUserWindow',
						getDefaultData: function() {
							return {
								securitySystem: this.getWindow().getMainForm().formValues
							};
						}
					}
				}]
			}
		]
	}]
});
