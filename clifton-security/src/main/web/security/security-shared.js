Ext.ns('Clifton.security', 'Clifton.security.authorization', 'Clifton.security.user', 'Clifton.security.system', 'Clifton.security.oauth', 'Clifton.security.credential');

//Optional additional tabs that can be added to the security setup window from other projects
Clifton.security.SecuritySetupWindowAdditionalTabs = [];

Clifton.security.user.UserWindowAdditionalTabs = [];

Clifton.security.user.GroupWindowAdditionalTabs = [];


/**
 * Asynchronously obtains the current user's team. Returns a Promise that will result in:
 * - the team the current user belongs to
 * - or null if the user is not a member of any team or belongs to more than 1 team.
 * @returns {Promise<null>}
 */
Clifton.security.user.getUserTeam = async function(componentScope) {
	const teams = await Clifton.security.user.getUserTeamList(componentScope);
	return teams.length === 1 ? teams[0] : null;
};

/**
 * Asynchronously obtains the current user's team list (use browser caching). Returns a Promise that will result in an array containing the teams the user is a member of.
 * The resulting array will be empty if the user does not belong to a team.
 * @returns {Promise<*>}
 */
Clifton.security.user.getUserTeamList = async function(componentScope) {
	return await TCG.data.getDataPromiseUsingCaching('securityGroupListForCurrentUser.json', componentScope, 'security.currentUser.groupList')
		.then(groups => groups ? groups.filter(group => group.name.startsWith('Team ')) : []);
};

Clifton.security.renderSecurityUser = function(val, metadataOrComponentScope) {
	if (Ext.isNumber(val)) {
		if (val === 0) {
			return 'System User';
		}
		const componentScope = (Object.getPrototypeOf(metadataOrComponentScope) !== Object.prototype && metadataOrComponentScope instanceof Ext.util.Observable)
			? metadataOrComponentScope
			: this.scope.gridPanel;
		const user = TCG.data.getData(`securityUser.json?id=${val}`, componentScope, `security.user.${val}`);
		if (user && user.label) {
			return user.label;
		}
	}
	return 'Unknown';
};

// Changes Standard Columns to include Create User/Update User
Clifton.security.GridPanel = Ext.override(TCG.grid.GridPanel, {
	standardColumns: [ // these columns will be added to the end of the grid if corresponding dataIndex fields are defined
		{
			header: 'Created By', width: 30, dataIndex: 'createUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'createUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Created On', width: 40, dataIndex: 'createDate', hidden: true},
		{
			header: 'Updated By', width: 30, dataIndex: 'updateUserId', hidden: true, filter: {type: 'combo', searchFieldName: 'updateUserId', url: 'securityUserListFind.json', displayField: 'label', showNotEquals: true},
			renderer: Clifton.security.renderSecurityUser,
			exportColumnValueConverter: 'securityUserColumnReversableConverter'
		},
		{header: 'Updated On', width: 40, dataIndex: 'updateDate', hidden: true}
	]
});


Clifton.security.authorization.SecurityResourceAccessGridPanel = Ext.extend(TCG.grid.GridPanel, {

	getSecurityResourceName: function() {
		// Override to return the security resource name to look up - false means no resource available
		return false;
	},

	name: 'securityResourceUserAccessList',
	xtype: 'gridpanel',
	instructions: 'The following users have permissions granted to this security resource (or to a parent resource that is flagged as inherited by children).  Permissions can be granted to the users explicitly or to a group the user is a member of.',
	columns: [
		{header: 'UserID', width: 15, dataIndex: 'securityUser.id', hidden: true},
		{header: 'User', width: 100, dataIndex: 'securityUser.label', defaultSortColumn: true},
		{header: 'Disabled', width: 50, dataIndex: 'securityUser.disabled', type: 'boolean', filter: false},
		{
			header: 'Admin', width: 50, dataIndex: 'admin', type: 'boolean',
			tooltip: 'Admins have unrestricted access to everything.'
		},
		{header: 'Read', width: 50, dataIndex: 'readAllowed', type: 'boolean'},
		{header: 'Write', width: 50, dataIndex: 'writeAllowed', type: 'boolean'},
		{header: 'Create', width: 50, dataIndex: 'createAllowed', type: 'boolean'},
		{header: 'Delete', width: 50, dataIndex: 'deleteAllowed', type: 'boolean'},
		{header: 'Execute', width: 50, dataIndex: 'executeAllowed', type: 'boolean'},
		{header: 'Full Control', width: 50, dataIndex: 'fullControlAllowed', type: 'boolean'}
	],
	getTopToolbarFilters: function(toolbar) {
		return [
			{boxLabel: 'Include Disabled Users', xtype: 'toolbar-checkbox', name: 'includeDisabledUsers'}
		];
	},
	getLoadParams: function(firstLoad) {
		const rName = this.getSecurityResourceName();
		if (rName === false) {
			TCG.showError('No Security Resource To Look Up Security Access For.');
			return false;
		}
		let disabled = false;
		if (TCG.getChildByName(this.getTopToolbar(), 'includeDisabledUsers').checked) {
			disabled = true;
		}
		return {securityResourceName: rName, disabled: disabled};
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.security.user.UserWindow',
		getDetailPageId: function(grid, row) {
			return row.json.securityUser.id;
		}
	}
});
Ext.reg('security-resource-access-grid', Clifton.security.authorization.SecurityResourceAccessGridPanel);
