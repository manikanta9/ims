Clifton.security.SecuritySetupWindow = Ext.extend(TCG.app.Window, {
	id: 'securitySetupWindow',
	title: 'Security (Authentication and Authorization)',
	iconCls: 'lock',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		listeners: {
			beforerender: function() {
				const tabs = this;
				for (let i = 0; i < Clifton.security.SecuritySetupWindowAdditionalTabs.length; i++) {
					tabs.add(Clifton.security.SecuritySetupWindowAdditionalTabs[i]);
				}
			}
		},
		items: [
			{
				title: 'Users',
				items: [{
					name: 'securityUserListFind',
					xtype: 'gridpanel',
					instructions: 'Users have login accounts that allow them to access the system and define security permissions that they have. Users can be assigned to Groups to simplify security management.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 250, xtype: 'toolbar-combo', name: 'tagHierarchyId', displayField: 'nameExpandedWithLabel', url: 'systemHierarchyListFind.json?tableName=SecurityGroup'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Contact Identifier', width: 50, dataIndex: 'contactIdentifier', type: 'int', doNotFormat: true, useNull: true, hidden: true},
						{header: 'User Name', width: 100, dataIndex: 'userName', defaultSortColumn: true},
						{header: 'Display Name', width: 100, dataIndex: 'displayName'},
						{header: 'Pseudonym', width: 100, dataIndex: 'integrationPseudonym'},
						{header: 'Email', width: 100, dataIndex: 'emailAddress'},
						{header: 'Disabled', width: 50, dataIndex: 'disabled', type: 'boolean'}

					],
					editor: {
						detailPageClass: 'Clifton.security.user.UserWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to enabled
							this.setFilterValue('disabled', false);
						}
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.securityGroupHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Groups',
				items: [{
					name: 'securityGroupListFind',
					xtype: 'gridpanel',
					instructions: 'Groups are collections of users. They should be used to simplify security management with most security permissions assigned at group level.',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 250, xtype: 'toolbar-combo', name: 'tagHierarchyId', displayField: 'nameExpandedWithLabel', url: 'systemHierarchyListFind.json?tableName=SecurityGroup'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 75, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Pseudonym', width: 75, dataIndex: 'integrationPseudonym'},
						{header: 'Description', width: 240, dataIndex: 'description'},
						{
							header: 'Group Owner', width: 50, dataIndex: 'ownerSecurityUser.label', filter: {type: 'combo', searchFieldName: 'ownerSecurityUserId', displayField: 'label', url: 'securityUserListFind.json'},
							tooltip: 'The group owner is the Security User responsible for reviewing and approving changes to the group.'
						},
						{header: 'Email', width: 60, dataIndex: 'emailAddress'},
						{header: 'Admin', width: 30, dataIndex: 'admin', type: 'boolean'},
						{header: 'Disabled', width: 30, dataIndex: 'disabled', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.user.GroupWindow'
					},
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to enabled
							this.setFilterValue('disabled', false);
						}
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.securityGroupHierarchyId = tag.getValue();
						}
						return params;
					}
				}]
			},


			{
				title: 'Resources',
				items: [{
					name: 'securityResourceListFind',
					xtype: 'gridpanel',
					instructions: 'A hierarchy of Security Resources that are used to define various sections of the system. Users and/or Groups are granted permissions to Security Resources defined here.',
					topToolbarSearchParameter: 'searchPattern',
					remoteSort: true,
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Expanded Label', width: 170, dataIndex: 'labelExpanded', defaultSortColumn: true},
						{header: 'Resource Name', width: 100, dataIndex: 'name'},
						{header: 'Resource Label', width: 100, dataIndex: 'label', hidden: true},
						{header: 'Description', width: 180, dataIndex: 'description'},
						{header: 'Allowed Permissions', width: 60, dataIndex: 'allowedSecurityPermissionMask.name'}
					],
					editor: {
						detailPageClass: 'Clifton.security.authorization.ResourceWindow'
					}
				}]
			},


			{
				title: 'Permission Assignments',
				items: [{
					name: 'securityPermissionAssignmentListFind',
					xtype: 'gridpanel',
					instructions: 'The following permissions are assigned to either users or user groups defined in the system.',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Security Resource', width: 100, dataIndex: 'securityResource.labelExpanded', filter: {searchFieldName: 'securityResourceLabelExpanded'}, defaultSortColumn: true},
						{header: 'Group', width: 60, dataIndex: 'securityGroup.name', filter: {type: 'combo', searchFieldName: 'securityGroupId', url: 'securityGroupListFind.json'}},
						{header: 'User', width: 50, dataIndex: 'securityUser.label', filter: {type: 'combo', searchFieldName: 'securityUserId', displayField: 'label', url: 'securityUserListFind.json'}},
						{header: 'Notes', width: 100, dataIndex: 'assignmentNotes'},
						{header: 'Full Control', width: 40, dataIndex: 'securityPermission.fullControlAllowed', type: 'boolean'},
						{header: 'Write', width: 30, dataIndex: 'securityPermission.writeAllowed', type: 'boolean'},
						{header: 'Create', width: 30, dataIndex: 'securityPermission.createAllowed', type: 'boolean'},
						{header: 'Delete', width: 30, dataIndex: 'securityPermission.deleteAllowed', type: 'boolean'},
						{header: 'Read', width: 30, dataIndex: 'securityPermission.readAllowed', type: 'boolean'},
						{header: 'Execute', width: 30, dataIndex: 'securityPermission.executeAllowed', type: 'boolean'},
						{header: 'Grant to Children', width: 45, dataIndex: 'inheritedByChildResources', type: 'boolean'}
					],
					editor: {
						detailPageClass: 'Clifton.security.authorization.PermissionAssignmentWindow'
					}
				}]
			},


			{
				title: 'User Group Assignments',
				items: [{
					name: 'securityUserGroupListFind',
					xtype: 'gridpanel',
					instructions: 'A list of all user group assignments defined in the system',
					additionalPropertiesToRequest: 'id|referenceOne.id',
					topToolbarSearchParameter: 'searchPattern',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Tag', width: 150, xtype: 'toolbar-combo', name: 'tagHierarchyId', displayField: 'nameExpandedWithLabel', url: 'systemHierarchyListFind.json?tableName=SecurityGroup'},
							{fieldLabel: 'Search', width: 150, xtype: 'toolbar-textfield', name: 'searchPattern'}
						];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Group Name', width: 100, dataIndex: 'referenceTwo.name', filter: {searchFieldName: 'groupName'}, defaultSortColumn: true},
						{
							header: 'Group Owner', width: 70, dataIndex: 'referenceTwo.ownerSecurityUser.label', filter: {type: 'combo', searchFieldName: 'ownerSecurityUserId', displayField: 'label', url: 'securityUserListFind.json'},
							tooltip: 'The group owner is the Security User responsible for reviewing and approving changes to the group.'
						},
						{header: 'Group Description', width: 200, dataIndex: 'referenceTwo.description', filter: {searchFieldName: 'groupDescription'}},
						{header: 'User Name', width: 75, dataIndex: 'referenceOne.userName', filter: {searchFieldName: 'userName'}},
						{header: 'Display Name', width: 75, dataIndex: 'referenceOne.displayName', filter: {searchFieldName: 'userDisplayName'}},
						{header: 'User Disabled', width: 35, dataIndex: 'referenceOne.disabled', type: 'boolean', filter: {searchFieldName: 'userDisabled'}, hidden: true},
						{header: 'Group Disabled', width: 35, dataIndex: 'referenceTwo.disabled', type: 'boolean', filter: {searchFieldName: 'groupDisabled'}, hidden: true},
						{
							header: 'Disabled', width: 35, dataIndex: 'disabled', type: 'boolean', filter: {searchFieldName: 'disabled'},
							tooltip: 'This field is true if either the SecurityUser or the SecurityGroup is disabled.'
						},
						{header: 'Admin', width: 30, dataIndex: 'referenceTwo.admin', type: 'boolean', filter: {searchFieldName: 'adminGroup'}},
						{header: 'User Updated On', width: 60, dataIndex: 'referenceOne.updateDate', filter: {searchFieldName: 'userUpdateDate'}}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						if (firstLoad) {
							// default to enabled
							this.setFilterValue('disabled', false);
						}
						const params = {};
						const tag = TCG.getChildByName(this.getTopToolbar(), 'tagHierarchyId');
						if (TCG.isNotBlank(tag.getValue())) {
							params.securityGroupHierarchyId = tag.getValue();
						}
						return params;
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.security.user.UserWindow',
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceOne.id;
						}
					}
				}]
			},


			{
				title: 'Systems',
				items: [{
					name: 'securitySystemListFind',
					xtype: 'gridpanel',
					instructions: 'Our system integrates with other systems defined here. Drill into each system to see additional information/configuration.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'System Name', width: 100, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 300, dataIndex: 'description'}
					],
					editor: {
						detailPageClass: 'Clifton.security.system.SystemWindow',
						drillDownOnly: true
					}
				}]
			},

			{
				title: 'OAuth Connections',
				items: [{
					xtype: 'oauth-client-grid'
				}]
			},

			{
				title: 'Credential Store',
				items: [{
					name: 'securityCredentialListFind',
					xtype: 'gridpanel',
					instructions: 'Security Credentials represent login details needed to access other systems.',
					columns: [
						{header: 'Name', width: 50, dataIndex: 'name', defaultSortColumn: true},
						{header: 'Description', width: 50, dataIndex: 'description'},
						{header: 'User Name', width: 100, dataIndex: 'userName'}
					],
					editor: {
						detailPageClass: 'Clifton.security.credential.SecurityCredentialWindow'
					}
				}]
			}
		]
	}]
});
