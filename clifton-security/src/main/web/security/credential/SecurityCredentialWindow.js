Clifton.security.credential.SecurityCredentialWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Credential',
	iconCls: 'user',
	enableRefreshWindow: true,

	items: [{
		xtype: 'formpanel',
		url: 'securityCredential.json',
		loadDefaultDataAfterRender: true,
		labelFieldName: 'name',
		listeners: {
			afterload: function(fp) {
				const f = this.getForm();
				const secret = TCG.getValue('passwordSecret', f.formValues);
				if (secret) {
					fp.setFormValue('passwordSecret.secretString', '[ENCRYPTED]', true);
				}
			}
		},
		items: [
			{fieldLabel: 'Name', name: 'name'},
			{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
			{fieldLabel: 'User Name', name: 'userName'},
			{fieldLabel: 'Password', xtype: 'secretfield', name: 'passwordSecret', tableName: 'SecurityCredential'}
		]
	}]
});
