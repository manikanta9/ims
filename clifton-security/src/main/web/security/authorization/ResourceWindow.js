Clifton.security.authorization.ResourceWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Resource',
	iconCls: 'key',
	width: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Info',
				items: [{
					xtype: 'formpanel',
					url: 'securityResource.json',
					instructions: 'Security Resources are used to define various sections of the system. Resources can be hierarchical to simplify configuration. Users and/or Groups are granted permissions to Security Resources. <br/><i>Allowed Permissions</i> define permissions that Users and Groups can be assigned to this Security Resource.',
					labelWidth: 120,
					items: [
						{fieldLabel: 'Expanded Label', name: 'labelExpanded', disabled: true},
						{fieldLabel: 'Parent Resource', name: 'parent.labelExpanded', hiddenName: 'parent.id', displayField: 'labelExpanded', queryParam: 'labelExpanded', xtype: 'combo', loadAll: false, url: 'securityResourceListFind.json', detailPageClass: 'Clifton.security.authorization.ResourceWindow'},
						{fieldLabel: 'Resource Name', name: 'name'},
						{fieldLabel: 'Resource Label', name: 'label'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
						{fieldLabel: 'Allowed Permissions', name: 'allowedSecurityPermissionMask.name', hiddenName: 'allowedPermissionMask', xtype: 'combo', mode: 'local', valueField: 'mask', url: 'securityPermissionMaskList.json'}
					]
				}]
			},


			{
				title: 'Security Access',
				items: [{
					xtype: 'security-resource-access-grid',
					getSecurityResourceName: function() {
						return TCG.getValue('name', this.getWindow().getMainForm().formValues);
					}
				}]
			},


			{
				title: 'Table Mappings',
				items: [{
					name: 'systemTableListFind',
					xtype: 'gridpanel',
					instructions: 'The following tables are mapped to this security resource.',
					getLoadParams: function() {
						return {securityResourceId: this.getWindow().getMainFormId()};
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Table Name', width: 150, dataIndex: 'name'},
						{header: 'Insert Condition', width: 150, dataIndex: 'insertEntityCondition.name'},
						{header: 'Update Condition', width: 150, dataIndex: 'updateEntityCondition.name'},
						{header: 'Delete Condition', width: 150, dataIndex: 'deleteEntityCondition.name'}
					],
					editor: {
						detailPageClass: 'Clifton.system.schema.TableWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
