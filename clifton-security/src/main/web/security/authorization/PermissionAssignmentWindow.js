Clifton.security.authorization.PermissionAssignmentWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Security Permission Assignment',
	iconCls: 'unlock',

	items: [{
		xtype: 'formpanel',
		url: 'securityPermissionAssignment.json',
		instructions: 'Assign permission(s) for selected Security Resource to selected User or Group. Selected permissions can optionally be applied to child Security Resources.',
		labelFieldName: 'securityResource.labelExpanded',
		labelWidth: 120,
		items: [
			{fieldLabel: 'Security Resource', name: 'securityResource.labelExpanded', hiddenName: 'securityResource.id', displayField: 'labelExpanded', queryParam: 'labelExpanded', xtype: 'combo', loadAll: false, url: 'securityResourceListFind.json', detailPageClass: 'Clifton.security.authorization.ResourceWindow'},
			{fieldLabel: 'User', name: 'securityUser.label', hiddenName: 'securityUser.id', displayField: 'label', xtype: 'combo', url: 'securityUserListFind.json?disabled=false', mutuallyExclusiveFields: ['securityGroup.id'], detailPageClass: 'Clifton.security.user.UserWindow'},
			{fieldLabel: 'Group', name: 'securityGroup.name', hiddenName: 'securityGroup.id', xtype: 'combo', url: 'securityGroupListFind.json', mutuallyExclusiveFields: ['securityUser.id'], detailPageClass: 'Clifton.security.user.GroupWindow'},
			{fieldLabel: 'Notes', name: 'assignmentNotes', xtype: 'textarea'},
			{boxLabel: 'Grant selected permission(s) to child security resource(s)', name: 'inheritedByChildResources', checked: true, xtype: 'checkbox', labelSeparator: ''},
			{
				fieldLabel: 'Permissions', xtype: 'checkboxgroup', columns: 3,
				items: [
					{boxLabel: 'Full Control', name: 'securityPermission.fullControlAllowed'},
					{boxLabel: 'Write', name: 'securityPermission.writeAllowed'},
					{boxLabel: 'Create', name: 'securityPermission.createAllowed'},
					{boxLabel: 'Delete', name: 'securityPermission.deleteAllowed'},
					{boxLabel: 'Read', name: 'securityPermission.readAllowed'},
					{boxLabel: 'Execute', name: 'securityPermission.executeAllowed'}
				]
			}
		]
	}]
});
