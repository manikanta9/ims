package com.clifton.core.util.matching;

import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class PermutationUtilsTests {

	@Test
	public void testNextIndexListOneThree() {
		int minCombinationSize = 1;
		int maxCombinationSize = 3;
		int[] indices = PermutationUtils.getNextIndexList(null, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1,2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1,2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("2,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,2,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1,2,3", intArrayToString(indices));
	}


	@Test
	public void testNextIndexListOneFour() {
		int minCombinationSize = 1;
		int maxCombinationSize = 4;
		int[] indices = PermutationUtils.getNextIndexList(null, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1,2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1,2", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("2,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,2,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("1,2,3", intArrayToString(indices));
		indices = PermutationUtils.getNextIndexList(indices, minCombinationSize, maxCombinationSize);
		Assertions.assertEquals("0,1,2,3", intArrayToString(indices));
	}


	private String intArrayToString(int[] array) {
		String[] strList = new String[array.length];
		for (int i = 0; i < array.length; i++) {
			strList[i] = array[i] + "";
		}
		return StringUtils.join(strList, ",");
	}
}
