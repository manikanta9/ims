package com.clifton.core.util.compare;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The {@link CompareUtilsTests} tests the {@link CompareUtils} class
 *
 * @author manderson
 */
public class CompareUtilsTests {


	@Test
	public void testCompareStrings() {
		String s1 = null;
		String s2 = null;

		Assertions.assertEquals(0, CompareUtils.compare(s1, s2));

		s1 = "";
		Assertions.assertTrue(CompareUtils.compare(s1, s2) > 0);
		Assertions.assertTrue(CompareUtils.compare(s2, s1) < 0);

		s2 = "";
		Assertions.assertEquals(0, CompareUtils.compare(s1, s2));

		s1 = "Test";
		s2 = "Test2";
		Assertions.assertTrue(CompareUtils.compare(s1, s2) < 0);
	}


	@Test
	public void testCompareIntegers() {
		Integer n1 = null;
		Integer n2 = null;

		Assertions.assertEquals(0, CompareUtils.compare(n1, n2));
		Assertions.assertEquals(0, CompareUtils.compare(n2, n1));

		n1 = 0;
		Assertions.assertTrue(CompareUtils.compare(n1, n2) > 0);
		Assertions.assertTrue(CompareUtils.compare(n2, n1) < 0);

		n1 = -5;
		n2 = 5;
		Assertions.assertTrue(CompareUtils.compare(n1, n2) < 0);
		Assertions.assertTrue(CompareUtils.compare(n2, n1) > 0);

		n2 = -5;
		Assertions.assertEquals(0, CompareUtils.compare(n1, n2));
		Assertions.assertEquals(0, CompareUtils.compare(n2, n1));
	}


	@Test
	public void testCompareDoubles() {
		Double n1 = null;
		Double n2 = null;

		Assertions.assertEquals(0, CompareUtils.compare(n1, n2));
		Assertions.assertEquals(0, CompareUtils.compare(n2, n1));

		n1 = 0.0;
		Assertions.assertTrue(CompareUtils.compare(n1, n2) > 0);
		Assertions.assertTrue(CompareUtils.compare(n2, n1) < 0);

		n1 = -5.35;
		n2 = 5.0;
		Assertions.assertTrue(CompareUtils.compare(n1, n2) < 0);
		Assertions.assertTrue(CompareUtils.compare(n2, n1) > 0);

		n2 = null;
		Assertions.assertTrue(CompareUtils.compare(n1, n2) > 0);
		Assertions.assertTrue(CompareUtils.compare(n2, n1) < 0);
	}


	@Test
	public void testCompareDates() {
		Date d1 = null;
		Date d2 = null;

		Assertions.assertEquals(0, CompareUtils.compare(d1, d2));
		Assertions.assertEquals(0, CompareUtils.compare(d2, d1));

		d1 = new Date();
		Assertions.assertFalse(CompareUtils.compare(d1, d2) < 0);
		Assertions.assertTrue(CompareUtils.compare(d2, d1) < 0);

		Assertions.assertTrue(CompareUtils.compare(d1, d2) > 0);
		Assertions.assertFalse(CompareUtils.compare(d2, d1) > 0);

		d2 = new Date();
		d2.setTime(d2.getTime() + 1);

		Assertions.assertTrue(CompareUtils.compare(d1, d2) < 0);
		Assertions.assertFalse(CompareUtils.compare(d2, d1) < 0);

		Assertions.assertFalse(CompareUtils.compare(d1, d2) > 0);
		Assertions.assertTrue(CompareUtils.compare(d2, d1) > 0);

		Assertions.assertNotSame(0, CompareUtils.compare(d1, d2));
		Assertions.assertNotSame(0, CompareUtils.compare(d2, d1));
	}


	@Test
	public void testCompareBooleans() {
		Boolean b1 = null;
		Boolean b2 = null;

		// b1 and b2 are null so they are equal
		Assertions.assertEquals(0, CompareUtils.compare(b1, b2));
		Assertions.assertEquals(0, CompareUtils.compare(b2, b1));

		// b1 is true so it's greater than null
		b1 = true;
		Assertions.assertTrue(CompareUtils.compare(b1, b2) > 0);
		Assertions.assertTrue(CompareUtils.compare(b2, b1) < 0);

		// b1 is true and b2 is false so b1 is greater than b2
		b2 = false;
		Assertions.assertTrue(CompareUtils.compare(b1, b2) > 0);
		Assertions.assertTrue(CompareUtils.compare(b2, b1) < 0);

		// b1 and b2 are false so they are equal
		b1 = false;
		Assertions.assertEquals(0, CompareUtils.compare(b1, b2));
		Assertions.assertEquals(0, CompareUtils.compare(b2, b1));

		b1 = true;
		b2 = true;
		// b1 and b2 are true so they are equal
		Assertions.assertEquals(0, CompareUtils.compare(b1, b2));
		Assertions.assertEquals(0, CompareUtils.compare(b2, b1));
	}


	@Test
	public void testCompareEnum() {
		ComparisonConditions c1 = ComparisonConditions.EQUALS;
		ComparisonConditions c2 = ComparisonConditions.NOT_EQUALS;

		Assertions.assertEquals(CompareUtils.compare(c1, c1), 0);
		Assertions.assertTrue(CompareUtils.compare(c1, c2) < 0);
		Assertions.assertTrue(CompareUtils.compare(c2, c1) > 0);
	}


	@Test
	public void testCompareBigDecimals() {
		BigDecimal v1 = BigDecimal.valueOf(25.61);
		BigDecimal v2 = BigDecimal.valueOf(25.610);

		Assertions.assertEquals(CompareUtils.compare(v1, v1), 0);
		Assertions.assertEquals(CompareUtils.compare(v1, v2), 0);

		v2 = BigDecimal.valueOf(25.6101);

		Assertions.assertNotEquals(CompareUtils.compare(v1, v2), 0);
		Assertions.assertTrue(CompareUtils.compare(v1, v2) < 0);
		Assertions.assertTrue(CompareUtils.compare(v2, v1) > 0);

		v1 = new BigDecimal("25.61000");
		v2 = new BigDecimal("25.61");

		Assertions.assertEquals(CompareUtils.compare(v1, v1), 0);
		Assertions.assertEquals(CompareUtils.compare(v1, v2), 0);

		v1 = new BigDecimal("105.712743981516460");
		v2 = new BigDecimal("105.712743981516460");

		Assertions.assertEquals(CompareUtils.compare(v1, v2), 0);
	}


	@Test
	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	public void testCompareIsEqualLists() {
		List<Object> list1 = null;
		List<Object> list2 = null;

		// Validate null/empty contents comparison
		Assertions.assertTrue(CompareUtils.isEqual(list1, list2));
		list1 = new ArrayList<>();
		Assertions.assertFalse(CompareUtils.isEqual(list1, list2));
		list2 = new ArrayList<>();
		Assertions.assertTrue(CompareUtils.isEqual(list1, list2));

		// Validate simple comparison
		list1 = Arrays.asList(1);
		Assertions.assertFalse(CompareUtils.isEqual(list1, list2));
		list2 = Arrays.asList(1);
		Assertions.assertTrue(CompareUtils.isEqual(list1, list2));

		// Validate non-ordered comparison
		list1 = Arrays.asList(2);
		list2 = Arrays.asList(3);
		Assertions.assertFalse(CompareUtils.isEqual(list1, list2));
		list1 = Arrays.asList(2, 3);
		list2 = Arrays.asList(3, 2);
		Assertions.assertTrue(CompareUtils.isEqual(list1, list2));

		// Validate non-ordered comparison with duplicates
		list1 = Arrays.asList(4, 4, 5);
		list2 = Arrays.asList(5, 5, 4);
		Assertions.assertFalse(CompareUtils.isEqual(list1, list2));
		list1 = Arrays.asList(4, 4, 5, 5);
		list2 = Arrays.asList(5, 5, 4, 4);
		Assertions.assertTrue(CompareUtils.isEqual(list1, list2));
	}
}
