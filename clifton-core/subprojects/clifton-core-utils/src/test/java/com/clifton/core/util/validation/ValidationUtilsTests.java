package com.clifton.core.util.validation;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;


public class ValidationUtilsTests {

	private static final String VALIDATION_MESSAGE = "Validation message";
	private static final String VALIDATION_FIELD = "validationField";


	@Test
	public void testAssertTrue() {
		ValidationUtils.assertTrue(true, VALIDATION_MESSAGE);
		ValidationUtils.assertTrue(true, VALIDATION_MESSAGE, VALIDATION_FIELD);
	}


	@Test
	public void testAssertNotTrue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertTrue(false, VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertFieldNotTrue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertTrue(false, VALIDATION_MESSAGE, VALIDATION_FIELD));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertFalse() {
		ValidationUtils.assertFalse(false, VALIDATION_MESSAGE);
		ValidationUtils.assertFalse(false, VALIDATION_MESSAGE, VALIDATION_FIELD);
	}


	@Test
	public void testAssertNotFalse() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertFalse(true, VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertFieldNotFalse() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertFalse(true, VALIDATION_MESSAGE, VALIDATION_FIELD));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAssertNull() {
		ValidationUtils.assertNull(null, VALIDATION_MESSAGE);
		ValidationUtils.assertNull(null, VALIDATION_MESSAGE, VALIDATION_FIELD);
	}


	@Test
	public void testAssertNullViolated() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertNull("nope", VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertFieldNullViolated() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertNull("nope", VALIDATION_MESSAGE, VALIDATION_FIELD));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertNotNull() {
		ValidationUtils.assertNotNull(false, VALIDATION_MESSAGE);
		ValidationUtils.assertNotNull(123, VALIDATION_MESSAGE);
		ValidationUtils.assertNotNull("null", VALIDATION_MESSAGE);
		ValidationUtils.assertNotNull("abc", VALIDATION_MESSAGE, VALIDATION_FIELD);
	}


	@Test
	public void testAssertNotNullViolated() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertNotNull(null, VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertFieldNotNullViolated() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertNotNull(null, VALIDATION_MESSAGE, VALIDATION_FIELD));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAssertEmpty() {
		ValidationUtils.assertEmpty((String) null, VALIDATION_MESSAGE);
		ValidationUtils.assertEmpty((Collection<?>) null, VALIDATION_MESSAGE);
		ValidationUtils.assertEmpty(new ArrayList<>(), VALIDATION_MESSAGE);
	}


	@Test
	public void testAssertEmptyViolated() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertEmpty(Collections.singletonList(123), VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertNotEmpty() {
		ValidationUtils.assertNotEmpty(Collections.singletonList(123), VALIDATION_MESSAGE);
	}


	@Test
	public void testAssertNotEmptyViolated() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertNotEmpty((Collection<?>) null, VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAssertDateBefore() {
		Date date1 = new Date();
		Date date2 = new Date(date1.getTime() + 100);
		ValidationUtils.assertBefore(date1, null, VALIDATION_FIELD);
		ValidationUtils.assertBefore(date1, date1, VALIDATION_FIELD);
		ValidationUtils.assertBefore(date1, date2, VALIDATION_FIELD);
	}


	@Test
	public void testAssertNotDateBefore() {
		Date date1 = new Date();
		Date date2 = new Date(date1.getTime() + 100);
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertBefore(date2, date1, VALIDATION_FIELD));
		Assertions.assertEquals("End date must be after start date " + DateUtils.fromDateSmart(date2), validationException.getMessage());
	}


	@Test
	public void testAssertTimeBefore() {
		Date date1 = new Date();
		Date date2 = new Date(date1.getTime() + 1000);
		Time time1 = new Time(date1);
		Time time2 = new Time(date2);
		ValidationUtils.assertBefore(time1, null, VALIDATION_FIELD);
		ValidationUtils.assertBefore(time1, time1, VALIDATION_FIELD);
		ValidationUtils.assertBefore(time1, time2, VALIDATION_FIELD);
	}


	@Test
	public void testAssertNotTimeBefore() {
		Date date1 = new Date();
		Date date2 = new Date(date1.getTime() + 1000);
		Time time1 = new Time(date1);
		Time time2 = new Time(date2);
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertBefore(time2, time1, VALIDATION_FIELD));
		Assertions.assertEquals("End time must be after start time " + time2, validationException.getMessage());
	}


	@Test
	public void testAssertEquals() {
		String s1 = "test";
		// String constructor produces a new object reference, whereas literal will refer to identical object reference
		@SuppressWarnings("RedundantStringConstructorCall")
		String s2 = new String("test");
		ValidationUtils.assertEquals(s1, s2, VALIDATION_MESSAGE);
		ValidationUtils.assertEquals(1, 1, VALIDATION_MESSAGE);
	}


	@Test
	public void testAssertEqualsBothNull() {
		ValidationUtils.assertEquals(null, null, VALIDATION_MESSAGE);
	}


	@Test
	public void testAssertEqualsFirstValueNull() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertEquals(null, "not null", VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertEqualsSecondValueNull() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertEquals("not null", null, VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertEqualsFailWithObject() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertEquals("test", "test2", VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertEqualsFailWithPrimitive() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertEquals(1, 2, VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAssertMutuallyExclusiveOneHasValue() {
		// test with 2 parameters
		ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object(), null);

		// test with more than 2 parameters
		ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object(), null, null);
	}


	@Test
	public void testIsMutuallyExclusiveMultipleHaveValue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object(), new Object(), null));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testIsMutuallyExclusiveAllHaveValue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object(), new Object(), new Object()));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testIsMutuallyExclusiveNoneHaveValue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, null, null, null));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testIsMutuallyExclusiveNoParameters() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testIsMutuallyExclusiveOneParameterWithValue() {
		ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object());
	}


	@Test
	public void testIsMutuallyExclusiveOneParameterNoValue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, (Object) null));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testIsMutuallyExclusiveOnePrimitiveHasValue() {
		ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, null, 1);
		ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, null, true);
	}


	@Test
	public void testIsMutuallyExclusiveTwoPrimitivesHaveValue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, null, 1, 2));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	@SuppressWarnings("RedundantArrayCreation")
	public void testIsMutuallyExclusiveArgsAsArray() {
		ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object[]{new Object(), null});
	}


	@Test
	@SuppressWarnings("RedundantArrayCreation")
	public void testIsMutuallyExclusiveArgsAsArrayTwoWithValue() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMutuallyExclusive(VALIDATION_MESSAGE, new Object[]{new Object(), new Object()}));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertOneValueMatching_SingleMatchObject() {
		String matchValue = "alpha";
		String obj1 = "alpha";
		String obj2 = "beta";
		String obj3 = "gamma";

		Assertions.assertDoesNotThrow(() -> ValidationUtils.assertOneValueMatching(VALIDATION_MESSAGE, matchValue, obj1, obj2, obj3));
	}


	@Test
	public void testAssertOneValueMatching_MultipleObjectMatch() {
		String matchValue = "alpha";
		String obj1 = "alpha";
		String obj2 = "beta";
		String obj3 = "alpha";

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> ValidationUtils.assertOneValueMatching(VALIDATION_MESSAGE, matchValue, obj1, obj2, obj3));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertOneValueMatching_NoMatch() {
		String matchValue = "alpha";
		String obj1 = "delta";
		String obj2 = "beta";
		String obj3 = "gamma";

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> ValidationUtils.assertOneValueMatching(VALIDATION_MESSAGE, matchValue, obj1, obj2, obj3));
		Assertions.assertEquals(VALIDATION_MESSAGE, validationException.getMessage());
	}


	@Test
	public void testAssertOneValueMatching_SingleMatchPrimitive() {
		boolean matchValue = true;
		boolean obj1 = false;
		boolean obj2 = true;
		boolean obj3 = false;

		Assertions.assertDoesNotThrow(() -> ValidationUtils.assertOneValueMatching(VALIDATION_MESSAGE, matchValue, obj1, obj2, obj3));
	}


	@Test
	public void testAssertOneValueMatching_MultipleMatchPrimitive() {
		boolean matchValue = true;
		boolean obj1 = false;
		boolean obj2 = true;
		boolean obj3 = true;

		Assertions.assertThrows(ValidationException.class, () -> ValidationUtils.assertOneValueMatching(VALIDATION_MESSAGE, matchValue, obj1, obj2, obj3));
	}


	@Test
	public void testAssertOneValueMatching_NoMatchPrimitive() {
		boolean matchValue = true;
		boolean obj1 = false;
		boolean obj2 = false;
		boolean obj3 = false;

		Assertions.assertThrows(ValidationException.class, () -> ValidationUtils.assertOneValueMatching(VALIDATION_MESSAGE, matchValue, obj1, obj2, obj3));
	}


	@Test
	public void testAssertMaxDecimalPrecision() {
		ValidationUtils.assertMaxDecimalPrecision(new BigDecimal("100.0"), 0, VALIDATION_FIELD);
		ValidationUtils.assertMaxDecimalPrecision(new BigDecimal("100.123"), 3, VALIDATION_FIELD);
		ValidationUtils.assertMaxDecimalPrecision(new BigDecimal("100.1230"), 3, VALIDATION_FIELD);
		ValidationUtils.assertMaxDecimalPrecision(new BigDecimal("100.123000000"), 3, VALIDATION_FIELD);
	}


	@Test
	public void testAssertMaxDecimalPrecision_Required() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMaxDecimalPrecision(null, 3, VALIDATION_FIELD));
		Assertions.assertEquals(String.format("'%s' field is required", StringUtils.splitWords(StringUtils.capitalize(VALIDATION_FIELD))), validationException.getMessage());
	}


	@Test
	public void testAssertMaxDecimalPrecision_Failed() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () ->
				ValidationUtils.assertMaxDecimalPrecision(new BigDecimal("100.123000"), 2, VALIDATION_FIELD));
		Assertions.assertEquals(String.format("'%s' 100.123 cannot have more than 2 decimal places", StringUtils.splitWords(StringUtils.capitalize(VALIDATION_FIELD))), validationException.getMessage());
	}
}
