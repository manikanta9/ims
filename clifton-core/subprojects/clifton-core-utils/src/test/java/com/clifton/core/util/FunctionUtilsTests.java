package com.clifton.core.util;


import com.clifton.core.test.util.TestUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.function.Predicate;


/**
 * The {@link FunctionUtilsTests} provides unit tests for the {@link FunctionUtils} type.
 *
 * @author MikeH
 */
public class FunctionUtilsTests {

	@Test
	@SuppressWarnings("TrivialFunctionalExpressionUsage")
	public void testNegate() {
		Assertions.assertTrue(((Predicate<String>) "My String"::equals).test("My String"));
		Assertions.assertFalse(((Predicate<String>) "My String"::equals).test("Not My String"));
		Assertions.assertFalse(FunctionUtils.negate((Predicate<String>) "My String"::equals).test("My String"));
		Assertions.assertTrue(FunctionUtils.negate((Predicate<String>) "My String"::equals).test("Not My String"));
	}


	@Test
	public void testUncheckedWrappers() {
		Assertions.assertDoesNotThrow(() -> FunctionUtils.uncheckedRunnable(() -> {}).run());
		TestUtils.expectException(RuntimeException.class,
				() -> FunctionUtils.uncheckedRunnable(() -> {
					throw new RuntimeException("Unchecked runnable");
				}).run(),
				"Unchecked runnable");
		Assertions.assertEquals(4, FunctionUtils.uncheckedFunction((Integer v) -> v * 2).apply(2));
		TestUtils.expectException(RuntimeException.class,
				() -> FunctionUtils.uncheckedFunction(v -> {
					throw new RuntimeException("Unchecked function");
				}).apply(null),
				"Unchecked function");
		Assertions.assertTrue(FunctionUtils.uncheckedPredicate((Integer v) -> v > 0).test(1));
		Assertions.assertFalse(FunctionUtils.uncheckedPredicate((Integer v) -> v > 0).test(-1));
		TestUtils.expectException(RuntimeException.class,
				() -> FunctionUtils.uncheckedPredicate(v -> {
					throw new RuntimeException("Unchecked predicate");
				}).test(null),
				"Unchecked predicate");
		Assertions.assertDoesNotThrow(() -> FunctionUtils.uncheckedConsumer(v -> {}).accept(null));
		TestUtils.expectException(RuntimeException.class,
				() -> FunctionUtils.uncheckedConsumer(v -> {
					throw new RuntimeException("Unchecked consumer");
				}).accept(null),
				"Unchecked consumer");
		Assertions.assertEquals(2, FunctionUtils.uncheckedSupplier(() -> 2).get());
		TestUtils.expectException(RuntimeException.class,
				() -> FunctionUtils.uncheckedSupplier(() -> {
					throw new RuntimeException("Unchecked supplier");
				}).get(),
				"Unchecked supplier");
	}
}
