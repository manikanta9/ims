package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;


/**
 * The <code>StringUtilsTests</code> class tests the <code>StringUtils</code>.
 *
 * @author vgomelsky
 */
@SuppressWarnings({"SpellCheckingInspection"})
public class StringUtilsTests {

	@Test
	public void testIsLikeIgnoringCase() {
		Assertions.assertTrue(StringUtils.isLikeIgnoringCase(null, null));
		Assertions.assertTrue(StringUtils.isLikeIgnoringCase("Test String", "TEST"));
		Assertions.assertTrue(StringUtils.isLikeIgnoringCase("Test String", "string"));
		Assertions.assertTrue(StringUtils.isLikeIgnoringCase("Test String", "TEST string"));
		Assertions.assertTrue(StringUtils.isLikeIgnoringCase("Test String", "T S"));

		Assertions.assertFalse(StringUtils.isLikeIgnoringCase(null, "test"));
		Assertions.assertFalse(StringUtils.isLikeIgnoringCase("test", null));
		Assertions.assertFalse(StringUtils.isLikeIgnoringCase("1234", "2345"));
	}


	@Test
	public void testIsEmpty() {
		Assertions.assertTrue(StringUtils.isEmpty(null));
		Assertions.assertTrue(StringUtils.isEmpty(""));
		Assertions.assertTrue(StringUtils.isEmpty(" "));
		Assertions.assertTrue(StringUtils.isEmpty(" 	"));
		Assertions.assertFalse(StringUtils.isEmpty("abc"));
	}


	@Test
	public void testRemoveAll() {
		Assertions.assertNull(StringUtils.removeAll(null, "-"));
		Assertions.assertEquals("", StringUtils.removeAll("", "-"));
		Assertions.assertEquals("abc", StringUtils.removeAll("abc", "-"));
		Assertions.assertEquals("abc", StringUtils.removeAll("-abc", "-"));
		Assertions.assertEquals("abc", StringUtils.removeAll("a-b-c", "-"));
		Assertions.assertEquals("abc", StringUtils.removeAll("abc--", "-"));
	}


	@Test
	public void testCapitalize() {
		Assertions.assertNull(StringUtils.capitalize(null));
		Assertions.assertEquals("", StringUtils.capitalize(""));
		Assertions.assertEquals("Test", StringUtils.capitalize("test"));
		Assertions.assertEquals("Test", StringUtils.capitalize("Test"));
		Assertions.assertEquals("TEST", StringUtils.capitalize("TEST"));
		Assertions.assertEquals("0test", StringUtils.capitalize("0test"));
	}


	@Test
	public void testDecapitalize() {
		Assertions.assertNull(StringUtils.deCapitalize(null));
		Assertions.assertEquals("", StringUtils.deCapitalize(""));
		Assertions.assertEquals("test", StringUtils.deCapitalize("Test"));
		Assertions.assertEquals("test", StringUtils.deCapitalize("test"));
		Assertions.assertEquals("tEST", StringUtils.deCapitalize("tEST"));
		Assertions.assertEquals("0test", StringUtils.deCapitalize("0test"));
	}


	@Test
	public void testSplitWords() {
		Assertions.assertNull(StringUtils.splitWords(null));
		Assertions.assertEquals("Test", StringUtils.splitWords("Test"));
		Assertions.assertEquals("TEST", StringUtils.splitWords("TEST"));
		Assertions.assertEquals("My Test", StringUtils.splitWords("MyTest"));
		Assertions.assertEquals("My Test", StringUtils.splitWords("My Test"));
		Assertions.assertEquals("My Test Two", StringUtils.splitWords("MyTestTwo"));
		Assertions.assertEquals("SUPER Test", StringUtils.splitWords("SUPERTest"));
		Assertions.assertEquals("A Test", StringUtils.splitWords("ATest"));
		Assertions.assertEquals("A Test", StringUtils.splitWords("A Test"));
		Assertions.assertEquals("a Test", StringUtils.splitWords("aTest"));
	}


	@Test
	public void testRemoveLeadingWord() {
		Assertions.assertNull(StringUtils.removeLeadingWord(null));
		Assertions.assertNull(StringUtils.removeLeadingWord("-"));
		Assertions.assertNull(StringUtils.removeLeadingWord("Test"));
		Assertions.assertNull(StringUtils.removeLeadingWord("TEST"));
		Assertions.assertEquals("-Test", StringUtils.removeLeadingWord("Test-Test"));
		Assertions.assertEquals("Test", StringUtils.removeLeadingWord("MyTest"));
		Assertions.assertEquals("TestTwo", StringUtils.removeLeadingWord("MyTestTwo"));
		Assertions.assertEquals("Test", StringUtils.removeLeadingWord("SUPERTest"));
		Assertions.assertEquals("Test", StringUtils.removeLeadingWord("ATest"));
		Assertions.assertEquals("Test", StringUtils.removeLeadingWord("TestingTest"));
		Assertions.assertEquals("Testing", StringUtils.removeLeadingWord("TestTesting"));
	}


	@Test
	public void testGetLeadingWord() {
		Assertions.assertNull(StringUtils.getLeadingWord(null));
		Assertions.assertEquals("", StringUtils.getLeadingWord(""));
		Assertions.assertEquals("-", StringUtils.getLeadingWord("-Test"));
		Assertions.assertEquals("Test", StringUtils.getLeadingWord("Test-Test"));
		Assertions.assertEquals("test", StringUtils.getLeadingWord("test-Test"));
		Assertions.assertEquals("test", StringUtils.getLeadingWord("test-"));
		Assertions.assertEquals("Test", StringUtils.getLeadingWord("Test"));
		Assertions.assertEquals("My", StringUtils.getLeadingWord("MyTest"));
		Assertions.assertEquals("My", StringUtils.getLeadingWord("MyTestTwo"));
		Assertions.assertEquals("SUPER", StringUtils.getLeadingWord("SUPERTest"));
		Assertions.assertEquals("A", StringUtils.getLeadingWord("ATest"));
		Assertions.assertEquals("Testing", StringUtils.getLeadingWord("TestingTest"));
		Assertions.assertEquals("Test", StringUtils.getLeadingWord("TestTesting"));
	}


	@Test
	public void testTruncateStringAfterWord() {
		Assertions.assertNull(StringUtils.truncateStringAfterWord(null, null, false));
		Assertions.assertNull(StringUtils.truncateStringAfterWord(null, null, true));

		Assertions.assertEquals("GetThisTest", StringUtils.truncateStringAfterWord("GetThisTest", "Get", false));
		Assertions.assertEquals("getThis", StringUtils.truncateStringAfterWord("getThisTest", "Test", true));
		Assertions.assertEquals("getThisTest", StringUtils.truncateStringAfterWord("getThisTest", "Test", false));
		Assertions.assertEquals("get", StringUtils.truncateStringAfterWord("getThisTest", "This", false));
		Assertions.assertEquals("get", StringUtils.truncateStringAfterWord("getThisTest", "This", true));
		Assertions.assertEquals("getThisisTest", StringUtils.truncateStringAfterWord("getThisisTest", "This", true));
		Assertions.assertEquals("getThisisTest", StringUtils.truncateStringAfterWord("getThisisTest", "This", false));
		Assertions.assertEquals("getThis!Test", StringUtils.truncateStringAfterWord("getThis!Test", "This", false));
		Assertions.assertEquals("getThis0Test", StringUtils.truncateStringAfterWord("getThis0Test", "This", false));
		Assertions.assertEquals("getThisTest0", StringUtils.truncateStringAfterWord("getThisTest0", "Test", false));
		Assertions.assertEquals("getThisTesting", StringUtils.truncateStringAfterWord("getThisTesting", "Test", false));
		Assertions.assertEquals("getThisTesting", StringUtils.truncateStringAfterWord("getThisTesting", "Test", true));

		boolean fail = false;
		try {
			StringUtils.truncateStringAfterWord("getThisTest", "get", false);
		}
		catch (Exception e) {
			fail = true;
			Assertions.assertEquals("Word [get] must start with an Uppercase letter.", e.getMessage());
		}
		Assertions.assertTrue(fail, "Expected Method to fail with message: Word [get] must start with an Uppercase letter.");

		fail = false;
		try {
			StringUtils.truncateStringAfterWord("getThis05Test", "05", false);
		}
		catch (Exception e) {
			fail = true;
			Assertions.assertEquals("Word [05] must start with an Uppercase letter.", e.getMessage());
		}
		Assertions.assertTrue(fail, "Expected Method to fail with message: Word [05] must start with an Uppercase letter.");

		fail = false;
		try {
			StringUtils.truncateStringAfterWord("getThis!!!Test", "!!!", false);
		}
		catch (Exception e) {
			fail = true;
			Assertions.assertEquals("Word [!!!] must start with an Uppercase letter.", e.getMessage());
		}
		Assertions.assertTrue(fail, "Expected Method to fail with message: Word [!!!] must start with an Uppercase letter.");
	}


	@Test
	public void testCoalesce() {
		Assertions.assertNull(StringUtils.coalesce());
		Assertions.assertNull(StringUtils.coalesce(false));
		Assertions.assertEquals("", StringUtils.coalesce(true));

		Assertions.assertEquals("", StringUtils.coalesce(""));
		Assertions.assertEquals("", StringUtils.coalesce(false, ""));
		Assertions.assertEquals("", StringUtils.coalesce(true, ""));

		Assertions.assertNull(StringUtils.coalesce((String) null));
		Assertions.assertNull(StringUtils.coalesce(false, (String) null));
		Assertions.assertEquals("", StringUtils.coalesce(true, (String) null));

		Assertions.assertEquals("Test", StringUtils.coalesce("Test"));
		Assertions.assertEquals("Test", StringUtils.coalesce(false, "Test"));
		Assertions.assertEquals("Test", StringUtils.coalesce(true, "Test"));

		Assertions.assertEquals("Test", StringUtils.coalesce("Test", "Test2"));
		Assertions.assertEquals("Test", StringUtils.coalesce(false, "Test", "Test2"));
		Assertions.assertEquals("Test", StringUtils.coalesce(true, "Test", "Test2"));

		Assertions.assertEquals("Test", StringUtils.coalesce(null, "Test", "Test2"));
		Assertions.assertEquals("Test", StringUtils.coalesce(false, null, "Test", "Test2"));
		Assertions.assertEquals("Test", StringUtils.coalesce(true, null, "Test", "Test2"));

		Assertions.assertEquals("Test", StringUtils.coalesce("", "Test", "Test2"));
		Assertions.assertEquals("Test", StringUtils.coalesce(false, "", "Test", "Test2"));
		Assertions.assertEquals("Test", StringUtils.coalesce(true, "", "Test", "Test2"));
	}


	@Test
	public void testFilterASCII() {
		String str = "Test is a test";
		Assertions.assertEquals(str, StringUtils.filterASCII(str));

		str = "TestTest2Test3";
		Assertions.assertEquals("Test Test2 Test3 ", StringUtils.filterASCII(str));
	}


	@Test
	public void testCollectionToCommaDelimitedString() {
		Assertions.assertNull(StringUtils.collectionToCommaDelimitedString(null));
		ArrayList<Integer> list = new ArrayList<>();
		Assertions.assertNull(StringUtils.collectionToCommaDelimitedString(list));
		list.add(55);
		Assertions.assertEquals("55", StringUtils.collectionToCommaDelimitedString(list));
		list.add(-7);
		Assertions.assertEquals("55, -7", StringUtils.collectionToCommaDelimitedString(list));
		list.add(1);
		Assertions.assertEquals("55, -7, 1", StringUtils.collectionToCommaDelimitedString(list));
	}


	@Test
	public void testStringToList() {
		String s = null;
		Assertions.assertNull(StringUtils.delimitedStringToList(s, ","));
		s = "";
		Assertions.assertNull(StringUtils.delimitedStringToList(s, ","));
		s = ",";
		List<String> list = StringUtils.delimitedStringToList(s, ",");
		Assertions.assertTrue(CollectionUtils.isEmpty(list));

		s = "Test 1, Test 2, Test 3";
		list = StringUtils.delimitedStringToList(s, ",");
		Assertions.assertEquals(3, CollectionUtils.getSize(list));

		s = "Test 1 Test 2 Test 3";
		list = StringUtils.delimitedStringToList(s, " ");
		Assertions.assertEquals(6, CollectionUtils.getSize(list));

		list = StringUtils.delimitedStringToList(s, null);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		list = StringUtils.delimitedStringToList(s, "");
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		s = "Test 1,,";
		list = StringUtils.delimitedStringToList(s, ",");
		Assertions.assertEquals(1, CollectionUtils.getSize(list));
	}


	@Test
	public void testCompare() {
		String s1 = null;
		String s2 = null;

		Assertions.assertEquals(0, StringUtils.compare(s1, s2));
		s1 = "";
		Assertions.assertEquals(0, StringUtils.compare(s1, s2));
		s1 = null;
		s2 = "";
		Assertions.assertEquals(0, StringUtils.compare(s1, s2));
		s1 = "";
		Assertions.assertEquals(0, StringUtils.compare(s1, s2));

		s1 = null;
		s2 = "test";
		Assertions.assertTrue(StringUtils.compare(s1, s2) > 0);

		s1 = "";
		Assertions.assertTrue(StringUtils.compare(s1, s2) > 0);

		s1 = "Test";
		Assertions.assertEquals(0, StringUtils.compare(s1, s2));

		s2 = "Test2";
		Assertions.assertTrue(StringUtils.compare(s1, s2) < 0);
	}


	@Test
	public void testGenerateKey() {
		// Default delimiter
		Assertions.assertEquals("null", StringUtils.generateKey((Object) null));
		Assertions.assertEquals("null:null", StringUtils.generateKey(null, null));
		Assertions.assertEquals("null:1", StringUtils.generateKey(null, 1));
		Assertions.assertEquals("1:2:3", StringUtils.generateKey(1, 2, 3));
		Assertions.assertEquals("4:5:null:1:a:null:bcd", StringUtils.generateKey(4, 5, null, 1, "a", null, "bcd"));

		// Custom delimiter
		Assertions.assertEquals("null", StringUtils.generateKeyWithDelimiter(", ", (Object) null));
		Assertions.assertEquals("null, null", StringUtils.generateKeyWithDelimiter(", ", null, null));
		Assertions.assertEquals("null, 1", StringUtils.generateKeyWithDelimiter(", ", null, 1));
		Assertions.assertEquals("1, 2, 3", StringUtils.generateKeyWithDelimiter(", ", 1, 2, 3));
		Assertions.assertEquals("4, 5, null, 1, a, null, bcd", StringUtils.generateKeyWithDelimiter(", ", 4, 5, null, 1, "a", null, "bcd"));
	}


	@Test
	public void testJoin() {
		// Collection of Strings
		Assertions.assertNull(StringUtils.join((Collection<String>) null, "--"));
		Assertions.assertNull(StringUtils.join((Collection<String>) null, null));
		Assertions.assertEquals("", StringUtils.join(new ArrayList<>(), "--"));
		Assertions.assertEquals("", StringUtils.join(CollectionUtils.createList((String) null), "--"));
		Assertions.assertEquals("a--b--c", StringUtils.join(CollectionUtils.createList("a", "b", "c"), "--"));
		Assertions.assertEquals("abc", StringUtils.join(CollectionUtils.createList("a", "b", "c"), null));
		Assertions.assertEquals("abc", StringUtils.join(CollectionUtils.createList("a", "b", "c"), ""));
		Assertions.assertEquals(",,a", StringUtils.join(CollectionUtils.createList(null, "", "a"), ","));
		// Array of strings
		Assertions.assertNull(StringUtils.join((String[]) null, "--"));
		Assertions.assertNull(StringUtils.join((String[]) null, null));
		Assertions.assertEquals("", StringUtils.join(new String[]{}, "--"));
		Assertions.assertEquals("", StringUtils.join(new String[]{null}, "--"));
		Assertions.assertEquals("a--b--c", StringUtils.join(new String[]{"a", "b", "c"}, "--"));
		Assertions.assertEquals("abc", StringUtils.join(new String[]{"a", "b", "c"}, null));
		Assertions.assertEquals("abc", StringUtils.join(new String[]{"a", "b", "c"}, ""));
		Assertions.assertEquals(",,a", StringUtils.join(new String[]{null, "", "a"}, ","));

		// Collection of Objects
		Assertions.assertNull(StringUtils.join((Collection<Integer>) null, Object::toString, "--"));
		Assertions.assertNull(StringUtils.join((Collection<Long>) null, Object::toString, null));
		Assertions.assertEquals("", StringUtils.join(new ArrayList<Integer>(), Object::toString, "--"));
		Assertions.assertEquals("", StringUtils.join(CollectionUtils.createList((Long) null), Object::toString, "--"));
		Assertions.assertEquals("1--2--3", StringUtils.join(CollectionUtils.createList(1, 2, 3), Object::toString, "--"));
		Assertions.assertEquals("123", StringUtils.join(CollectionUtils.createList(1L, 2L, 3L), Object::toString, null));
		Assertions.assertEquals("123", StringUtils.join(CollectionUtils.createList(1, 2, 3), Object::toString, ""));
		Assertions.assertEquals(",1,0", StringUtils.join(CollectionUtils.createList(null, BigDecimal.ONE, BigDecimal.ZERO), Object::toString, ","));
		// Array of Objects
		Assertions.assertNull(StringUtils.join((Integer[]) null, Object::toString, "--"));
		Assertions.assertNull(StringUtils.join((Long[]) null, Object::toString, null));
		Assertions.assertEquals("", StringUtils.join(new Integer[]{}, Object::toString, "--"));
		Assertions.assertEquals("", StringUtils.join(new Long[]{null}, Object::toString, "--"));
		Assertions.assertEquals("1--2--3", StringUtils.join(new Integer[]{1, 2, 3}, Object::toString, "--"));
		Assertions.assertEquals("123", StringUtils.join(new Long[]{1L, 2L, 3L}, Object::toString, null));
		Assertions.assertEquals("123", StringUtils.join(new Integer[]{1, 2, 3}, Object::toString, ""));
		Assertions.assertEquals(",1,0", StringUtils.join(new BigDecimal[]{null, BigDecimal.ONE, BigDecimal.ZERO}, Object::toString, ","));
	}


	@Test
	public void testJoinExcludingNulls() {
		Assertions.assertEquals("", StringUtils.joinExcludingNulls(null));
		Assertions.assertEquals("", StringUtils.joinExcludingNulls(null, null, null, null));
		Assertions.assertEquals("", StringUtils.joinExcludingNulls(null, "", "", ""));
		Assertions.assertEquals("", StringUtils.joinExcludingNulls("."));
		Assertions.assertEquals("", StringUtils.joinExcludingNulls(".", null, null, null));
		Assertions.assertEquals("", StringUtils.joinExcludingNulls(".", "", "", ""));
		Assertions.assertEquals("a.b.c.d", StringUtils.joinExcludingNulls(".", "a", "b", "c", "d"));
		Assertions.assertEquals("a.c.d", StringUtils.joinExcludingNulls(".", "a", null, "c", "d"));
		Assertions.assertEquals("1.2", StringUtils.joinExcludingNulls(".", null, "1", "", "2"));
		Assertions.assertEquals("abcd", StringUtils.joinExcludingNulls(null, "a", "b", "c", "d"));
		Assertions.assertEquals("a---b---c---d", StringUtils.joinExcludingNulls("---", "a", "b", "c", "d"));
	}


	@Test
	public void joinErrorListOfViolations() {
		Assertions.assertEquals("Message header:", StringUtils.joinErrorListOfViolations("Message header:", 0, null));
		Assertions.assertEquals("Message header:", StringUtils.joinErrorListOfViolations("Message header:", 1, null));
		Assertions.assertEquals("Message header:<br/><br/><li>violation 1</li><li>...</li>",
				StringUtils.joinErrorListOfViolations("Message header:", 1, Arrays.asList("violation 1", "violation 2")));
		Assertions.assertEquals("Message header:<br/><br/><li>violation 1</li><li>violation 2</li>",
				StringUtils.joinErrorListOfViolations("Message header:", 2, Arrays.asList("violation 1", "violation 2")));
		Assertions.assertEquals("Message header:<br/><br/><li>violation 1</li><li>violation 2</li>",
				StringUtils.joinErrorListOfViolations("Message header:", 3, Arrays.asList("violation 1", "violation 2")));
		Assertions.assertEquals("Message header:<br/><br/><li>violation 1</li><li>violation 2</li><li>violation 3</li><li>violation 4</li><li>violation 5</li>",
				StringUtils.joinErrorListOfViolations("Message header:", Arrays.asList("violation 1", "violation 2", "violation 3", "violation 4", "violation 5")));
		Assertions.assertEquals("Message header:<br/><br/><li>violation 1</li><li>violation 2</li><li>violation 3</li><li>violation 4</li><li>violation 5</li><li>...</li>",
				StringUtils.joinErrorListOfViolations("Message header:", Arrays.asList("violation 1", "violation 2", "violation 3", "violation 4", "violation 5", "violation 6")));
	}


	@Test
	public void testGetParameterValueFromUrl() {
		Assertions.assertNull(StringUtils.getParameterValueFromUrl(null, null));
		Assertions.assertNull(StringUtils.getParameterValueFromUrl(null, "test"));
		Assertions.assertNull(StringUtils.getParameterValueFromUrl("test.json", null));
		Assertions.assertNull(StringUtils.getParameterValueFromUrl("test.json?", null));
		Assertions.assertNull(StringUtils.getParameterValueFromUrl("test.json?", "secondValue"));
		Assertions.assertNull(StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1", "secondValue"));

		Assertions.assertEquals("Value1", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1", "firstValue"));
		Assertions.assertEquals("Value1", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1&secondValue=2", "firstValue"));
		Assertions.assertEquals("2", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1&secondValue=2", "secondValue"));
		Assertions.assertEquals("2", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1&secondValue=2&thirdValue=Three", "secondValue"));
		Assertions.assertEquals("Three", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1&secondValue=2&thirdValue=Three", "thirdValue"));
		Assertions.assertEquals("", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1&secondValue=&thirdValue=Three", "secondValue"));
		Assertions.assertEquals("", StringUtils.getParameterValueFromUrl("test.json?firstValue=Value1&secondValue=2&thirdValue=", "thirdValue"));
	}


	@Test
	public void testIsPhoneNumberValid() {
		// Null/Blanks are Valid
		Assertions.assertTrue(StringUtils.isPhoneNumberValid(null, true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid(" ", true));

		// Null/Blanks are Not Valid
		Assertions.assertFalse(StringUtils.isPhoneNumberValid(null, false));
		Assertions.assertFalse(StringUtils.isPhoneNumberValid("", false));
		Assertions.assertFalse(StringUtils.isPhoneNumberValid(" ", false));

		Assertions.assertFalse(StringUtils.isPhoneNumberValid("abc", false));
		Assertions.assertFalse(StringUtils.isPhoneNumberValid("1234560", false));
		Assertions.assertFalse(StringUtils.isPhoneNumberValid("123-4560", false));
		Assertions.assertFalse(StringUtils.isPhoneNumberValid("1-123-4567", false));

		Assertions.assertTrue(StringUtils.isPhoneNumberValid("1234567890", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("123-456-7890", true));

		Assertions.assertTrue(StringUtils.isPhoneNumberValid("123-456-7890 x1234", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("123-456-7890 ext1234", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("(123)-456-7890", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("123.456.7890", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("123 456 7890", true));

		Assertions.assertTrue(StringUtils.isPhoneNumberValid("+1 617 747 9335", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("00 353 21 438 0105", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("1 (617) 672-8732", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("1.805.339.4262", true));
		Assertions.assertTrue(StringUtils.isPhoneNumberValid("541-679-3311 x52491", true));
	}


	/**
	 * https://stackoverflow.com/questions/8511490/calculating-length-in-utf-8-of-java-string-without-actually-encoding-it
	 */
	@Test
	public void testLengthUTF8() {
		Charset utf8 = StandardCharsets.UTF_8;
		for (int codepoint = Character.MIN_CODE_POINT; codepoint <= Character.MAX_CODE_POINT; codepoint++) {
			if (codepoint == Character.MIN_SURROGATE) {
				codepoint = Character.MAX_SURROGATE + 1;
			}
			if (!Character.isDefined(codepoint)) {
				continue;
			}
			String test = new String(Character.toChars(codepoint));
			Assertions.assertEquals(test.getBytes(utf8).length, StringUtils.lengthUTF8(test));
		}

		// Test null string
		Assertions.assertEquals(0, StringUtils.lengthUTF8(null));
	}


	/**
	 * Tests {@link StringUtils#formatStringUpToNCharsWithDots(String, int, boolean)} for UTF8 and non-UTF8 encodings.
	 */
	@Test
	public void testFormatStringUpToNCharsWithDots() {
		Assertions.assertNull(StringUtils.formatStringUpToNCharsWithDots(null, 50));

		// format string where all characters are UTF8 data length 1
		String testString = "0123456789";
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 10));
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 10, true));
		Assertions.assertEquals("012345...", StringUtils.formatStringUpToNCharsWithDots(testString, 9, true));

		// format string that ends with a character that has UTF8 data length 2 (128 = '\u0080')
		testString = "123456789" + "\u0080";
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 10));
		Assertions.assertEquals("1234567...", StringUtils.formatStringUpToNCharsWithDots(testString, 10, true));
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 11, true));
		testString = "\u0080" + "123456789";
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 10));
		Assertions.assertEquals("\u0080" + "12345...", StringUtils.formatStringUpToNCharsWithDots(testString, 10, true));
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 11, true));

		// format string that ends with a character that has UTF8 data length 3 (MIN_LOW_SURROGATE  = '\uDC00')
		testString = "123456789" + Character.MIN_LOW_SURROGATE;
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 11));
		Assertions.assertEquals("12345678...", StringUtils.formatStringUpToNCharsWithDots(testString, 11, true));
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 12, true));
		testString = Character.MIN_LOW_SURROGATE + "123456789";
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 11));
		Assertions.assertEquals(Character.MIN_LOW_SURROGATE + "12345...", StringUtils.formatStringUpToNCharsWithDots(testString, 11, true));
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 12, true));
		// this is a long string that is hard to read, but it is an example from prod so it is a good test case where switching to UTF8 added two extra bytes in the DB
		testString = "[id = 2226946] \"GS futures group placed a redemption from money market for 400,000 yesterday, rather than a purchase into money market for 400,000. In order to resolve this properly, GS futures will need to return the wire of 400,000 that GS money market sent yesterday.   Additionally, today GS futures will need to place the correct purchase into the money market for 400,000 and send this wire to them. use our mark of (108,998.50) for today’s vm movement. (purchase into the mm).  I changed th...";
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 500));
		Assertions.assertEquals("[id = 2226946] \"GS futures group placed a redemption from money market for 400,000 yesterday, rather than a purchase into money market for 400,000. In order to resolve this properly, GS futures will need to return the wire of 400,000 that GS money market sent yesterday.   Additionally, today GS futures will need to place the correct purchase into the money market for 400,000 and send this wire to them. use our mark of (108,998.50) for today’s vm movement. (purchase into the mm).  I changed ...",
				StringUtils.formatStringUpToNCharsWithDots(testString, 500, true));

		// format string that ends with a character that has UTF8 data length 4 (MIN_HIGH_SURROGATE = '\uD800')
		testString = "123456789" + Character.MIN_HIGH_SURROGATE;
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 12));
		Assertions.assertEquals("123456789...", StringUtils.formatStringUpToNCharsWithDots(testString, 12, true));
		Assertions.assertEquals(testString, StringUtils.formatStringUpToNCharsWithDots(testString, 13, true));
		// no tests for leading high surrogates because they are not characters themselves, but represent supplementary characters
	}


	@Test
	public void testTrim() {
		// Spaces
		String value = "     123.45";
		Assertions.assertEquals("123.45", StringUtils.trim(value));
		// Tabs
		value = "       123.45";
		Assertions.assertEquals("123.45", StringUtils.trim(value));
		// Note: Value doesn't contain regular spaces but "non breaking space" character (#160) or A0 in HEX
		value = "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + "\u00A0" + " 114676698.11 ";
		Assertions.assertEquals("114676698.11", StringUtils.trim(value));
		value = "\u00A0" + "\u00A0" + "\u00A0" + "ABC DEF " + "\u00A0" + " XYZ " + "\u00A0" + "\u00A0";
		Assertions.assertEquals("ABC DEF   XYZ", StringUtils.trim(value));
	}


	@Test
	public void testSubstringAfterFirst() {
		Assertions.assertNull(StringUtils.substringAfterFirst(null, null));
		Assertions.assertNull(StringUtils.substringAfterFirst(null, "some string"));
		Assertions.assertEquals("", StringUtils.substringAfterFirst("", "some string"));
		Assertions.assertEquals("", StringUtils.substringAfterFirst("some string", ""));
		Assertions.assertEquals("", StringUtils.substringAfterFirst("some string", null));
		Assertions.assertEquals("bc", StringUtils.substringAfterFirst("abc", "a"));
		Assertions.assertEquals("arber", StringUtils.substringAfterFirst("barber", "b"));
		Assertions.assertEquals("c", StringUtils.substringAfterFirst("abc", "b"));
		Assertions.assertEquals("", StringUtils.substringAfterFirst("a", "a"));
		Assertions.assertEquals("", StringUtils.substringAfterFirst("a", "z"));
	}


	@Test
	public void testSubstringAfterLast() {
		Assertions.assertNull(StringUtils.substringAfterLast(null, null));
		Assertions.assertNull(StringUtils.substringAfterLast(null, "some string"));
		Assertions.assertEquals("", StringUtils.substringAfterLast("", "some string"));
		Assertions.assertEquals("", StringUtils.substringAfterLast("some string", ""));
		Assertions.assertEquals("", StringUtils.substringAfterLast("some string", null));
		Assertions.assertEquals("bc", StringUtils.substringAfterLast("abc", "a"));
		Assertions.assertEquals("er", StringUtils.substringAfterLast("barber", "b"));
		Assertions.assertEquals("", StringUtils.substringAfterLast("abc", "c"));
		Assertions.assertEquals("", StringUtils.substringAfterLast("a", "a"));
		Assertions.assertEquals("", StringUtils.substringAfterLast("a", "z"));
	}


	@Test
	public void testSubstringBeforeFirst() {
		Assertions.assertNull(StringUtils.substringBeforeFirst(null, null));
		Assertions.assertNull(StringUtils.substringBeforeFirst(null, "some string"));
		Assertions.assertEquals("", StringUtils.substringBeforeFirst("", "some string"));
		Assertions.assertEquals("", StringUtils.substringBeforeFirst("some string", ""));
		Assertions.assertEquals("", StringUtils.substringBeforeFirst("some string", null));
		Assertions.assertEquals("", StringUtils.substringBeforeFirst("abc", "a"));
		Assertions.assertEquals("ba", StringUtils.substringBeforeFirst("barber", "r"));
		Assertions.assertEquals("ab", StringUtils.substringBeforeFirst("abc", "c"));
		Assertions.assertEquals("", StringUtils.substringBeforeFirst("a", "a"));
		Assertions.assertEquals("", StringUtils.substringBeforeFirst("a", "z"));
	}


	@Test
	public void testSubstringBeforeLast() {
		Assertions.assertNull(StringUtils.substringBeforeLast(null, null));
		Assertions.assertNull(StringUtils.substringBeforeLast(null, "some string"));
		Assertions.assertEquals("", StringUtils.substringBeforeLast("", "some string"));
		Assertions.assertEquals("", StringUtils.substringBeforeLast("some string", ""));
		Assertions.assertEquals("", StringUtils.substringBeforeLast("some string", null));
		Assertions.assertEquals("", StringUtils.substringBeforeLast("abc", "a"));
		Assertions.assertEquals("bar", StringUtils.substringBeforeLast("barber", "b"));
		Assertions.assertEquals("ab", StringUtils.substringBeforeLast("abc", "c"));
		Assertions.assertEquals("", StringUtils.substringBeforeLast("a", "a"));
		Assertions.assertEquals("", StringUtils.substringBeforeLast("a", "z"));
	}


	@Test
	public void testEqualsAny() {
		Assertions.assertTrue(StringUtils.equalsAny(null, new String[]{null}));
		Assertions.assertTrue(StringUtils.equalsAny(null, (String) null));
		Assertions.assertTrue(StringUtils.equalsAny("ABC", "ABC"));
		Assertions.assertTrue(StringUtils.equalsAny("ABCD", "ABC", "ABCD"));
		Assertions.assertTrue(StringUtils.equalsAny("ABC", "ABCD", "ABC", "ABCDEF"));
		Assertions.assertTrue(StringUtils.equalsAny("ABC", "ABCD", "ABC", "ABC", "ABCDEF"));

		Assertions.assertFalse(StringUtils.equalsAny(null));
		Assertions.assertFalse(StringUtils.equalsAny(null, (String[]) null));
		Assertions.assertFalse(StringUtils.equalsAny("ABC"));
		Assertions.assertFalse(StringUtils.equalsAny("ABC", (String) null));
		Assertions.assertFalse(StringUtils.equalsAny("ABC", (String[]) null));
		Assertions.assertFalse(StringUtils.equalsAny("ABC", new String[]{null}));
		Assertions.assertFalse(StringUtils.equalsAny("ABC", "abc", "ABc"));
		Assertions.assertFalse(StringUtils.equalsAny("ABCDE", "ABC", "ABCD"));
		Assertions.assertFalse(StringUtils.equalsAny(null, "ABC"));
	}


	@Test
	public void testEqualsAnyIgnoreCase() {
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase(null, new String[]{null}));
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase(null, (String) null));
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase("ABC", "ABC"));
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase("ABC", "abc"));
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase("abcd", "ABC", "ABCD"));
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase("abc", "ABCD", "abc", "ABCDEF"));
		Assertions.assertTrue(StringUtils.equalsAnyIgnoreCase("abc", "ABCD", "ABC", "ABC", "ABCDEF"));

		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase(null));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase(null, (String[]) null));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase("ABC"));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase("ABC", (String) null));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase("ABC", (String[]) null));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase("ABC", new String[]{null}));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase("ABCDE", "ABC", "ABCD"));
		Assertions.assertFalse(StringUtils.equalsAnyIgnoreCase(null, "ABC"));
	}


	@Test
	public void testStartsWith() {
		Assertions.assertFalse(StringUtils.startsWith(null, null));
		Assertions.assertFalse(StringUtils.startsWith("test", null));
		Assertions.assertFalse(StringUtils.startsWith(null, "test"));

		Assertions.assertTrue(StringUtils.startsWith("test", "test"));
		Assertions.assertTrue(StringUtils.startsWith("testMe", "test"));
		Assertions.assertFalse(StringUtils.startsWith("testMe", "Me"));
	}


	@Test
	public void testEndsWith() {
		Assertions.assertFalse(StringUtils.endsWith(null, null));
		Assertions.assertFalse(StringUtils.endsWith("test", null));
		Assertions.assertFalse(StringUtils.endsWith(null, "test"));

		Assertions.assertTrue(StringUtils.endsWith("test", "test"));
		Assertions.assertFalse(StringUtils.endsWith("testMe", "test"));
		Assertions.assertTrue(StringUtils.endsWith("testMe", "Me"));
	}


	@Test
	public void testFormatFreemarkerVariable() {
		Assertions.assertTrue("Test_1".equalsIgnoreCase(StringUtils.formatFreemarkerVariable("Test 1")));
		Assertions.assertTrue("Test_1".equalsIgnoreCase(StringUtils.formatFreemarkerVariable("Test.1")));
		Assertions.assertTrue("Test__1".equalsIgnoreCase(StringUtils.formatFreemarkerVariable("Test .1")));
		Assertions.assertTrue("_Test_1".equalsIgnoreCase(StringUtils.formatFreemarkerVariable(" Test 1")));
		Assertions.assertTrue("Test_1_".equalsIgnoreCase(StringUtils.formatFreemarkerVariable("Test 1.")));
	}


	@Test
	public void testReplace() {
		String[] strings = {
				"aaa",
				"investmentSecurity.underlyingSecurity.priceMultiplier",
				"investmentSecurity.priceMultiplier",
				"The quick red fox ran slyly."
		};
		String[][] targetReplacements = {
				{"aa", "b"},
				{"ab", "b"},
				{"i", "eye"},
				{"priceM", "m"},
				{" ", ""},
				{".", "-"}
		};
		Arrays.stream(strings).forEach(string ->
				Arrays.stream(targetReplacements).forEach(targetReplacement -> {
					String target = targetReplacement[0];
					String replacement = targetReplacement[1];
					Assertions.assertEquals(string.replace(target, replacement), StringUtils.replace(string, target, replacement));
				}));
	}


	@Test
	public void testSplit() {
		Assertions.assertArrayEquals(array(), StringUtils.split(null, null));
		Assertions.assertArrayEquals(array(), StringUtils.split(null, ","));
		Assertions.assertArrayEquals(array(), StringUtils.split("", null)); // The pattern is allowed to be null only because the source string is null
		Assertions.assertArrayEquals(array(), StringUtils.split("", ","));
		Assertions.assertArrayEquals(array("a", "", "b", "", "c", "d", "e"), StringUtils.split("a,,b,,c,d,e", ","));
		Assertions.assertArrayEquals(array("a", "", "b", "", "c", "", "d", "", "e"), StringUtils.split("a,,b,,c,d,e", ",*"));
		Assertions.assertArrayEquals(array("a", "b", "c", "d", "e"), StringUtils.split("a,,b,,c,d,e", ",+"));
	}


	@Test
	public void testSplitOnDelimiter() {
		Assertions.assertEquals(list(), StringUtils.splitOnDelimiter(null, null));
		Assertions.assertEquals(list(), StringUtils.splitOnDelimiter(null, ","));
		Assertions.assertEquals(list(), StringUtils.splitOnDelimiter("", null)); // The delimiter is allowed to be null only because the source string is null
		Assertions.assertEquals(list(), StringUtils.splitOnDelimiter("", ","));
		Assertions.assertEquals(list("a", "", "b", "", "c", "d", "e"), StringUtils.splitOnDelimiter("a,,b,,c,d,e", ","));
		Assertions.assertEquals(list("a,,b,,c,d,e"), StringUtils.splitOnDelimiter("a,,b,,c,d,e", ",*"));
		Assertions.assertEquals(list("a,,b,", ",d,e"), StringUtils.splitOnDelimiter("a,,b,,c,d,e", ",c"));
	}


	@Test
	public void testGetGlobPattern() {
		Assertions.assertNull(StringUtils.getGlobPattern(null));
		Assertions.assertTrue(StringUtils.getGlobPattern("a").matcher("a").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("a").matcher("b").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("a").matcher("ab").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("ab").matcher("ab").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("the quick brown fox").matcher("the quick brown fox").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("the quick red fox").matcher("the quick brown fox").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("?").matcher("a").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("?").matcher("b").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("?").matcher("1").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("?").matcher("abc").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("a?c").matcher("abc").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("?bc").matcher("abc").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("???").matcher("abc").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("?a?").matcher("abc").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("the?qu?ck *").matcher("\n\nthe quick brown fox\n\n").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("\n\nthe?qu?ck *").matcher("\n\nthe quick brown fox\n\n").matches());
		Assertions.assertTrue(StringUtils.getGlobPattern("*?qu?ck *").matcher("\n\nthe quick brown fox\n\n").matches());
		Assertions.assertFalse(StringUtils.getGlobPattern("*?qu?ick *").matcher("\n\nthe quick brown fox\n\n").matches());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SafeVarargs
	public static <T> T[] array(T... elements) {
		return elements;
	}


	@SafeVarargs
	public static <T> List<T> list(T... elements) {
		return Arrays.asList(elements);
	}
}
