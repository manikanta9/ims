package com.clifton.core.util.matching;


import java.math.BigDecimal;


public class PermutationTestClass {

	public String value;
	public BigDecimal qty;


	public PermutationTestClass(String value) {
		this.value = value;
	}


	public PermutationTestClass(String value, BigDecimal qty) {
		this.value = value;
		this.qty = qty;
	}


	@Override
	public String toString() {
		return this.value;
	}


	public BigDecimal getQty() {
		return this.qty;
	}


	public void setQty(BigDecimal qty) {
		this.qty = qty;
	}
}
