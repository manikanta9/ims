package com.clifton.core.util.beans;


import com.clifton.core.beans.BaseEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The {@link MethodUtilsTests} class tests {@link MethodUtils} methods.
 *
 * @author vgomelsky
 */
public class MethodUtilsTests {

	@Test
	public void testIsMethodInClass() throws SecurityException, NoSuchMethodException {
		Assertions.assertTrue(MethodUtils.isMethodInClass(BaseEntity.class, Object.class.getMethod("hashCode")));
		Assertions.assertTrue(MethodUtils.isMethodInClass(BaseEntity.class, Object.class.getMethod("equals", Object.class)));
		Assertions.assertFalse(MethodUtils.isMethodInClass(Object.class, BaseEntity.class.getMethod("setCreateUserId", short.class)));
	}


	@Test
	public void testGetMethod() {
		Assertions.assertNull(MethodUtils.getMethod(Object.class, "NOT_A_REAL_METHOD"));
		Assertions.assertNotNull(MethodUtils.getMethod(Object.class, "hashCode"));
	}
}
