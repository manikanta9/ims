package com.clifton.core.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;


/**
 * The <code>ArrayUtilsTests</code> tests the {@link ArrayUtils} class
 *
 * @author manderson
 */
@SuppressWarnings({"SimplifiableJUnitAssertion", "ConstantConditions", "ResultOfMethodCallIgnored"})
public class ArrayUtilsTests {

	@Test
	public void testAsNonNullArray() {
		String[] array;
		Assertions.assertArrayEquals(new String[0], ArrayUtils.asNonNullArray(String.class, null));
		array = new String[]{};
		Assertions.assertSame(array, ArrayUtils.asNonNullArray(String.class, array));
		array = new String[]{"test"};
		Assertions.assertSame(array, ArrayUtils.asNonNullArray(String.class, array));
		array = new String[]{"test1", "test2"};
		Assertions.assertSame(array, ArrayUtils.asNonNullArray(String.class, array));
	}


	@Test
	public void testCreateArray() {
		Assertions.assertArrayEquals(new Integer[]{}, ArrayUtils.<Integer>createArray());
		Assertions.assertArrayEquals(new Integer[]{1}, ArrayUtils.createArray(1));
		Assertions.assertArrayEquals(new Integer[]{1, 2, 3}, ArrayUtils.createArray(1, 2, 3));
		// Delegated generics cause failures due to language limitations; verify this behavior
		// Verify that type-erasure occurs, resulting in an array of the bounded (or unbounded) generic argument type
		//noinspection RedundantCast
		Assertions.assertEquals(Object[].class, ((Object) createArrayTypeErasure(1)).getClass());
		// Verify that assignments fail due to type-erasure
		Assertions.assertThrows(ClassCastException.class, () -> {
			@SuppressWarnings("unused")
			Integer[] array = createArrayTypeErasure(1);
		});
	}


	@Test
	public void testCreateArrayOfType() {
		Assertions.assertArrayEquals(new Integer[]{}, ArrayUtils.createArrayOfType(Integer.class));
		Assertions.assertArrayEquals(new Integer[]{1}, ArrayUtils.createArrayOfType(Integer.class, 1));
		Assertions.assertArrayEquals(new Integer[]{1, 2, 3}, ArrayUtils.createArrayOfType(Integer.class, 1, 2, 3));
		Assertions.assertArrayEquals(new Integer[]{null}, ArrayUtils.createArrayOfType(Integer.class, (Integer) null));
		Assertions.assertArrayEquals(new Integer[]{}, ArrayUtils.createArrayOfType(Integer.class, (Integer[]) null));
		// Verify type erasure does not apply for delegated calls
		Assertions.assertEquals(Integer[].class, createArrayOfTypeTypeErasure(Integer.class, 1).getClass());
		Assertions.assertDoesNotThrow(() -> createArrayOfTypeTypeErasure(Integer.class, 1)[0] = 2);
	}


	@Test
	public void testCreateArrayOfTypeWithLength() {
		Assertions.assertArrayEquals(new Integer[]{}, ArrayUtils.createArrayOfTypeWithLength(Integer.class, 0));
		Assertions.assertArrayEquals(new Integer[]{null}, ArrayUtils.createArrayOfTypeWithLength(Integer.class, 1));
		Assertions.assertArrayEquals(new Integer[]{null, null}, ArrayUtils.createArrayOfTypeWithLength(Integer.class, 2));
		Assertions.assertThrows(NegativeArraySizeException.class, () -> ArrayUtils.createArrayOfTypeWithLength(Integer.class, -1));
		Assertions.assertEquals(Integer[].class, createArrayOfTypeWithLengthTypeErasure(Integer.class, 0).getClass());
		// Verify type erasure does not apply for delegated calls
		Assertions.assertEquals(Integer[].class, createArrayOfTypeWithLengthTypeErasure(Integer.class, 1).getClass());
		Assertions.assertDoesNotThrow(() -> createArrayOfTypeWithLengthTypeErasure(Integer.class, 1)[0] = 2);
	}


	@Test
	public void testCreateArrayFromSourceType() {
		Object[] objArray = new Object[0];
		Integer[] intArray = new Integer[0];
		Assertions.assertArrayEquals(new Object[]{}, ArrayUtils.createArrayFromSourceType(objArray));
		Assertions.assertArrayEquals(new Object[]{null}, ArrayUtils.createArrayFromSourceType(objArray, (Object) null));
		Assertions.assertArrayEquals(new Object[]{"string"}, ArrayUtils.createArrayFromSourceType(objArray, "string"));
		Assertions.assertArrayEquals(new Integer[]{}, ArrayUtils.createArrayFromSourceType(intArray));
		Assertions.assertArrayEquals(new Integer[]{1}, ArrayUtils.createArrayFromSourceType(intArray, 1));
		Assertions.assertArrayEquals(new Integer[]{1, 2}, ArrayUtils.createArrayFromSourceType(intArray, 1, 2));
		// Verify type erasure does not apply for delegated calls
		Assertions.assertEquals(Integer[].class, createArrayFromSourceTypeTypeErasure(intArray, 1).getClass());
		Assertions.assertDoesNotThrow(() -> createArrayFromSourceTypeTypeErasure(intArray, 1)[0] = 2);
		// Verify type erasure functionality for parameters
		Assertions.assertThrows(ClassCastException.class, () -> ArrayUtils.createArrayFromSourceType(createArrayTypeErasure(1), 1));
	}


	@Test
	public void testCreateArrayFromSourceTypeWithLength() {
		Object[] objArray = new Object[0];
		Integer[] intArray = new Integer[0];
		Assertions.assertArrayEquals(new Object[]{}, ArrayUtils.createArrayFromSourceTypeWithLength(objArray, 0));
		Assertions.assertArrayEquals(new Object[]{null}, ArrayUtils.createArrayFromSourceTypeWithLength(objArray, 1));
		Assertions.assertArrayEquals(new Object[]{null, null}, ArrayUtils.createArrayFromSourceTypeWithLength(objArray, 2));
		Assertions.assertArrayEquals(new Integer[]{}, ArrayUtils.createArrayFromSourceTypeWithLength(intArray, 0));
		Assertions.assertArrayEquals(new Integer[]{null}, ArrayUtils.createArrayFromSourceTypeWithLength(intArray, 1));
		Assertions.assertArrayEquals(new Integer[]{null, null}, ArrayUtils.createArrayFromSourceTypeWithLength(intArray, 2));
		// Verify type erasure does not apply for delegated calls
		Assertions.assertEquals(Integer[].class, createArrayFromSourceTypeWithLengthTypeErasure(intArray, 1).getClass());
		Assertions.assertDoesNotThrow(() -> createArrayFromSourceTypeWithLengthTypeErasure(intArray, 1)[0] = 2);
		// Verify type erasure functionality for parameters
		Assertions.assertThrows(ClassCastException.class, () -> ArrayUtils.createArrayFromSourceTypeWithLength(createArrayTypeErasure(1), 1));
	}


	/*
	 * Helper methods for verifying type-erasure behavior. Generic parameters are erased to Object types during compilation, so any array creation which relies solely on generic
	 * parameters will produce Object[] arrays, except in some special cases where the type is retained at compile-time such as vararg array generation.
	 */


	private <T> T[] createArrayTypeErasure(T arg) {
		return ArrayUtils.createArray(arg);
	}


	private <T> T[] createArrayOfTypeTypeErasure(Class<T> elementType, T arg) {
		return ArrayUtils.createArrayOfType(elementType, arg);
	}


	private <T> T[] createArrayOfTypeWithLengthTypeErasure(Class<T> elementType, int length) {
		return ArrayUtils.createArrayOfTypeWithLength(elementType, length);
	}


	@SafeVarargs
	private final <T> T[] createArrayFromSourceTypeTypeErasure(T[] sourceArray, T... elements) {
		return ArrayUtils.createArrayFromSourceType(sourceArray, elements);
	}


	private <T> T[] createArrayFromSourceTypeWithLengthTypeErasure(T[] sourceArray, int length) {
		return ArrayUtils.createArrayFromSourceTypeWithLength(sourceArray, length);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAdd() {
		Integer[] i1 = null;
		int i = 5;

		Integer[] addedArray = ArrayUtils.add(i1, null);
		Assertions.assertNull(addedArray);

		addedArray = ArrayUtils.add(i1, i);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(1, addedArray.length);

		addedArray = ArrayUtils.add(addedArray, i);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(2, addedArray.length);
		Assertions.assertEquals(new Integer(5), addedArray[0]);
		Assertions.assertEquals(new Integer(5), addedArray[1]);
	}


	@Test
	public void testAddAtIndex() {
		Assertions.assertArrayEquals(null, ArrayUtils.add(null, null, 0));
		Assertions.assertArrayEquals(new Integer[0], ArrayUtils.add(new Integer[0], null, 0));
		Assertions.assertArrayEquals(new Integer[]{5}, ArrayUtils.add(null, 5, 0));
		Assertions.assertArrayEquals(new Integer[]{5}, ArrayUtils.add(new Integer[0], 5, 0));
		Assertions.assertArrayEquals(new Integer[]{5, 1}, ArrayUtils.add(new Integer[]{1}, 5, 0));
		Assertions.assertArrayEquals(new Integer[]{1, 5}, ArrayUtils.add(new Integer[]{1}, 5, 1));
		Assertions.assertArrayEquals(new Integer[]{5, 1, 2, 3}, ArrayUtils.add(new Integer[]{1, 2, 3}, 5, 0));
		Assertions.assertArrayEquals(new Integer[]{1, 5, 2, 3}, ArrayUtils.add(new Integer[]{1, 2, 3}, 5, 1));
		Assertions.assertArrayEquals(new Integer[]{1, 2, 5, 3}, ArrayUtils.add(new Integer[]{1, 2, 3}, 5, 2));
		Assertions.assertArrayEquals(new Integer[]{1, 2, 3, 5}, ArrayUtils.add(new Integer[]{1, 2, 3}, 5, 3));
		// Assert array bounds constraints
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.add(null, null, -1));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.add(null, null, 1));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.add(new Integer[0], null, -1));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.add(new Integer[0], null, 1));
		Assertions.assertDoesNotThrow(() -> ArrayUtils.add(new Integer[5], 1, 0));
		Assertions.assertDoesNotThrow(() -> ArrayUtils.add(new Integer[5], 1, 1));
		Assertions.assertDoesNotThrow(() -> ArrayUtils.add(new Integer[5], 1, 5));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.add(new Integer[5], 1, 6));
	}


	@Test
	public void testAddAll() {
		String[] s1 = null;
		String[] s2 = null;
		String[] addedArray;
		addedArray = ArrayUtils.addAll(s1, s2);
		Assertions.assertNull(addedArray);

		s1 = new String[0];
		addedArray = ArrayUtils.addAll(s1, s2);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(0, addedArray.length);

		s1 = new String[]{"Test", "Test2"};
		addedArray = ArrayUtils.addAll(s1, s2);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(2, addedArray.length);
		Assertions.assertEquals("Test", addedArray[0]);
		Assertions.assertEquals("Test2", addedArray[1]);

		s2 = new String[]{"2Test", "2Test2"};
		addedArray = ArrayUtils.addAll(s1, s2);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(4, addedArray.length);
		Assertions.assertEquals("Test", addedArray[0]);
		Assertions.assertEquals("Test2", addedArray[1]);
		Assertions.assertEquals("2Test", addedArray[2]);
		Assertions.assertEquals("2Test2", addedArray[3]);
	}


	@Test
	public void testAddAll_ThreeArrays() {
		String[] s1 = null;
		String[] s2 = null;
		String[] s3 = null;
		String[] addedArray;
		addedArray = ArrayUtils.addAll(s1, s2, s3);
		Assertions.assertNull(addedArray);

		s1 = new String[0];
		addedArray = ArrayUtils.addAll(s1, s2, s3);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(0, addedArray.length);

		s1 = new String[]{"Test", "Test2"};
		addedArray = ArrayUtils.addAll(s1, s2, s3);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(2, addedArray.length);
		Assertions.assertEquals("Test", addedArray[0]);
		Assertions.assertEquals("Test2", addedArray[1]);

		s2 = new String[]{"2Test", "2Test2"};
		addedArray = ArrayUtils.addAll(s1, s2, s3);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(4, addedArray.length);
		Assertions.assertEquals("Test", addedArray[0]);
		Assertions.assertEquals("Test2", addedArray[1]);
		Assertions.assertEquals("2Test", addedArray[2]);
		Assertions.assertEquals("2Test2", addedArray[3]);

		s3 = new String[]{"3Test"};
		addedArray = ArrayUtils.addAll(s1, s2, s3);
		Assertions.assertNotNull(addedArray);
		Assertions.assertEquals(5, addedArray.length);
		Assertions.assertEquals("Test", addedArray[0]);
		Assertions.assertEquals("Test2", addedArray[1]);
		Assertions.assertEquals("2Test", addedArray[2]);
		Assertions.assertEquals("2Test2", addedArray[3]);
		Assertions.assertEquals("3Test", addedArray[4]);
	}


	@Test
	public void testAddAllAtIndex() {
		Assertions.assertArrayEquals(null, ArrayUtils.addAll(null, null, 0));
		Assertions.assertArrayEquals(new Integer[0], ArrayUtils.addAll(new Integer[0], null, 0));
		Assertions.assertArrayEquals(new Integer[]{1, 2}, ArrayUtils.addAll(new Integer[]{1, 2}, null, 0));
		Assertions.assertArrayEquals(new Integer[0], ArrayUtils.addAll(null, new Integer[0], 0));
		Assertions.assertArrayEquals(new Integer[]{5}, ArrayUtils.addAll(null, new Integer[]{5}, 0));
		Assertions.assertArrayEquals(new Integer[]{5, 6, 7}, ArrayUtils.addAll(new Integer[0], new Integer[]{5, 6, 7}, 0));
		Assertions.assertArrayEquals(new Integer[]{5, 6, 7, 1}, ArrayUtils.addAll(new Integer[]{1}, new Integer[]{5, 6, 7}, 0));
		Assertions.assertArrayEquals(new Integer[]{1, 5, 6, 7}, ArrayUtils.addAll(new Integer[]{1}, new Integer[]{5, 6, 7}, 1));
		Assertions.assertArrayEquals(new Integer[]{5, 6, 7, 1, 2, 3}, ArrayUtils.addAll(new Integer[]{1, 2, 3}, new Integer[]{5, 6, 7}, 0));
		Assertions.assertArrayEquals(new Integer[]{1, 5, 6, 7, 2, 3}, ArrayUtils.addAll(new Integer[]{1, 2, 3}, new Integer[]{5, 6, 7}, 1));
		Assertions.assertArrayEquals(new Integer[]{1, 2, 5, 6, 7, 3}, ArrayUtils.addAll(new Integer[]{1, 2, 3}, new Integer[]{5, 6, 7}, 2));
		Assertions.assertArrayEquals(new Integer[]{1, 2, 3, 5, 6, 7}, ArrayUtils.addAll(new Integer[]{1, 2, 3}, new Integer[]{5, 6, 7}, 3));
		// Assert array bounds constraints
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.addAll(null, null, -1));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.addAll(null, null, 1));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.addAll(new Integer[0], null, -1));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.addAll(new Integer[0], null, 1));
		Assertions.assertDoesNotThrow(() -> ArrayUtils.addAll(new Integer[5], new Integer[]{1, 2, 3}, 0));
		Assertions.assertDoesNotThrow(() -> ArrayUtils.addAll(new Integer[5], new Integer[]{1, 2, 3}, 1));
		Assertions.assertDoesNotThrow(() -> ArrayUtils.addAll(new Integer[5], new Integer[]{1, 2, 3}, 5));
		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> ArrayUtils.addAll(new Integer[5], new Integer[]{1, 2, 3}, 6));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testClone() {
		Assertions.assertArrayEquals(null, ArrayUtils.cloneArray((Object[]) null));
		Assertions.assertArrayEquals(new Object[]{null}, ArrayUtils.cloneArray(new Object[]{null}));
		Assertions.assertArrayEquals(new Integer[]{1}, ArrayUtils.cloneArray(new Integer[]{1}));
		Assertions.assertArrayEquals(new Object[]{1, "a", "b"}, ArrayUtils.cloneArray(new Object[]{1, "a", "b"}));
		Assertions.assertEquals(String[].class, ArrayUtils.cloneArray(new String[]{"a", "b"}).getClass());
	}


	@Test
	public void testContains() {
		Assertions.assertEquals(false, ArrayUtils.contains(null, null));
		Assertions.assertEquals(false, ArrayUtils.contains(null, 1));
		Assertions.assertEquals(false, ArrayUtils.contains(new Object[0], null));
		Assertions.assertEquals(false, ArrayUtils.contains(new Object[0], 1));
		Assertions.assertEquals(false, ArrayUtils.contains(new Object[0], "a"));
		Assertions.assertEquals(true, ArrayUtils.contains(new Object[]{null}, null));
		Assertions.assertEquals(true, ArrayUtils.contains(new Object[]{null, null}, null));
		Assertions.assertEquals(true, ArrayUtils.contains(new Integer[]{1, 2, 3, null}, null));
		Assertions.assertEquals(true, ArrayUtils.contains(new Integer[]{1, 2, 3, null}, 3));
		Assertions.assertEquals(true, ArrayUtils.contains(new Object[]{1, 2, 3, null, "hello"}, "hello"));
		Assertions.assertEquals(true, ArrayUtils.contains(new Object[]{1, 2, 3, null, "hello"}, 1));
	}


	@Test
	public void testIsEmpty() {
		Assertions.assertEquals(true, ArrayUtils.isEmpty((Object[]) null));
		Assertions.assertEquals(true, ArrayUtils.isEmpty(new Object[0]));
		Assertions.assertEquals(false, ArrayUtils.isEmpty(new Object[]{null}));
		Assertions.assertEquals(false, ArrayUtils.isEmpty(new Object[]{null, null}));
		Assertions.assertEquals(false, ArrayUtils.isEmpty(new Integer[]{null, 1}));
		Assertions.assertEquals(false, ArrayUtils.isEmpty(new Integer[]{1, 2, 3}));
		Assertions.assertEquals(false, ArrayUtils.isEmpty(new String[]{"a", "b", "c"}));
		Assertions.assertEquals(false, ArrayUtils.isEmpty(new String[]{""}));
	}


	@Test
	public void testGetAtIndex() {
		Assertions.assertEquals(null, ArrayUtils.getAtIndex((Object[]) null, 0));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex((Object[]) null, 10));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex(new Object[]{null}, 0));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex(new Object[]{null}, 10));
		Assertions.assertEquals(1, (int) ArrayUtils.getAtIndex(new Integer[]{1}, 0));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex(new Integer[]{1}, 1));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex(new Object[]{1, "a", null, 4L}, -1));
		Assertions.assertEquals(1, ArrayUtils.getAtIndex(new Object[]{1, "a", null, 4L}, 0));
		Assertions.assertEquals("a", ArrayUtils.getAtIndex(new Object[]{1, "a", null, 4L}, 1));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex(new Object[]{1, "a", null, 4L}, 2));
		Assertions.assertEquals(4L, ArrayUtils.getAtIndex(new Object[]{1, "a", null, 4L}, 3));
		Assertions.assertEquals(null, ArrayUtils.getAtIndex(new Object[]{1, "a", null, 4L}, 4));
	}


	@Test
	public void testIntersect() {
		Assertions.assertEquals(null, ArrayUtils.toString(ArrayUtils.intersect(null, null)));
		Assertions.assertEquals("1, 2, 3", ArrayUtils.toString(ArrayUtils.intersect(ArrayUtils.createArray(1, 2, 3), null)));
		Assertions.assertEquals("1, 2, 3", ArrayUtils.toString(ArrayUtils.intersect(null, ArrayUtils.createArray(1, 2, 3))));
		Assertions.assertEquals("2, 3", ArrayUtils.toString(ArrayUtils.intersect(ArrayUtils.createArray(1, 2, 3), ArrayUtils.createArray(2, 3, 4, 5))));
		Assertions.assertEquals(null, ArrayUtils.toString(ArrayUtils.intersect(ArrayUtils.createArray(1, 2, 3), ArrayUtils.createArray(4, 5))));
	}


	@Test
	public void testTranspose() {
		Object[][] array = new Object[0][1];
		Assertions.assertNull(ArrayUtils.transpose(array));
		array = new Object[0][0];
		Assertions.assertNull(ArrayUtils.transpose(array));

		array = new Object[2][1];
		array[0][0] = 1;
		array[1][0] = 2;
		Object[][] transposedArray = ArrayUtils.transpose(array);

		assert transposedArray != null;
		Assertions.assertEquals(1, transposedArray.length);
		Assertions.assertEquals(2, transposedArray[0].length);

		Assertions.assertEquals(1, transposedArray[0][0]);
		Assertions.assertEquals(2, transposedArray[0][1]);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testWrapCoalescingBothNull() {
		Integer[] array = null;
		Integer individualElement = null;
		Integer[] expectedResult = {};
		Integer[] actualResult = ArrayUtils.wrapCoalescing(array, individualElement, Integer.class);
		Assertions.assertArrayEquals(expectedResult, actualResult);
	}


	@Test
	public void testWrapCoalescingNoIndividualElement() {
		Integer[] array = {1, 2, 3};
		Integer individualElement = null;
		Integer[] expectedResult = {1, 2, 3};
		Integer[] actualResult = ArrayUtils.wrapCoalescing(array, individualElement, Integer.class);
		Assertions.assertArrayEquals(expectedResult, actualResult);
	}


	@Test
	public void testWrapCoalescingNoArray() {
		Integer[] array = null;
		Integer individualElement = 4;
		Integer[] expectedResult = {4};
		Integer[] actualResult = ArrayUtils.wrapCoalescing(array, individualElement, Integer.class);
		Assertions.assertArrayEquals(expectedResult, actualResult);
	}


	@Test
	public void testWrapCoalescingBothPresent() {
		Integer[] array = {1, 2, 3};
		Integer individualElement = 4;
		Integer[] expectedResult = {1, 2, 3};
		Integer[] actualResult = ArrayUtils.wrapCoalescing(array, individualElement, Integer.class);
		Assertions.assertArrayEquals(expectedResult, actualResult);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIndexOfPredicate() {
		Integer[] array = {1, 2, 3};
		Assertions.assertTrue(ArrayUtils.indexOf(array, val -> val == 1) == 0);
		Assertions.assertTrue(ArrayUtils.indexOf(array, val -> val > 1) == 1);
		Assertions.assertTrue(ArrayUtils.indexOf(array, val -> val > 3) == -1);
		Assertions.assertTrue(ArrayUtils.indexOf(array, val -> val < 1) == -1);
		Assertions.assertTrue(ArrayUtils.indexOf(null, (Integer val) -> val < 1) == -1);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAnyMatch() {
		Assertions.assertTrue(!ArrayUtils.anyMatch(null, null));
		Assertions.assertTrue(!ArrayUtils.anyMatch(null, Objects::isNull));
		Assertions.assertTrue(ArrayUtils.anyMatch(new Object[]{null}, Objects::isNull));
		Assertions.assertTrue(!ArrayUtils.anyMatch(new Object[]{null}, Objects::nonNull));
		Assertions.assertTrue(ArrayUtils.anyMatch(new Integer[]{1, 2, 3}, v -> v > 0));
		Assertions.assertTrue(ArrayUtils.anyMatch(new Integer[]{1, 2, 3}, v -> v > 1));
		Assertions.assertTrue(!ArrayUtils.anyMatch(new Integer[]{1, 2, 3}, v -> v > 3));
		Assertions.assertTrue(ArrayUtils.anyMatch(new String[]{"abc", "def", "ghijk"}, v -> !v.isEmpty()));
		Assertions.assertTrue(ArrayUtils.anyMatch(new String[]{"abc", "def", "ghijk"}, v -> v.length() > 3));
		Assertions.assertTrue(!ArrayUtils.anyMatch(new String[]{"abc", "def", "ghijk"}, v -> v.length() > 5));
	}


	@Test
	public void testAllMatch() {
		Assertions.assertTrue(ArrayUtils.allMatch(null, null));
		Assertions.assertTrue(ArrayUtils.allMatch(null, Objects::isNull));
		Assertions.assertTrue(ArrayUtils.allMatch(new Object[]{null}, Objects::isNull));
		Assertions.assertTrue(!ArrayUtils.allMatch(new Object[]{null}, Objects::nonNull));
		Assertions.assertTrue(ArrayUtils.allMatch(new Integer[]{1, 2, 3}, v -> v > 0));
		Assertions.assertTrue(!ArrayUtils.allMatch(new Integer[]{1, 2, 3}, v -> v > 1));
		Assertions.assertTrue(!ArrayUtils.allMatch(new Integer[]{1, 2, 3}, v -> v > 3));
		Assertions.assertTrue(ArrayUtils.allMatch(new String[]{"abc", "def", "ghijk"}, v -> !v.isEmpty()));
		Assertions.assertTrue(!ArrayUtils.allMatch(new String[]{"abc", "def", "ghijk"}, v -> v.length() > 3));
		Assertions.assertTrue(!ArrayUtils.allMatch(new String[]{"abc", "def", "ghijk"}, v -> v.length() > 5));
	}
}
