package com.clifton.core.util.date;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


public class TimeTests {

	@Test
	public void testCast() {
		Time time = Time.parse("12:56:15");
		Date date = DateUtils.toDate("01/01/2009 12:56:15.000", "MM/dd/yyyy HH:mm:ss.SSS");
		int milliseconds = DateUtils.getMillisecondsFromMidnight(date);
		Assertions.assertEquals(milliseconds, DateUtils.getMillisecondsFromMidnight(time.getDate()));

		time = Time.parse("12:56:15");
		milliseconds = DateUtils.getMillisecondsFromMidnight(date);
		Assertions.assertEquals(milliseconds, DateUtils.getMillisecondsFromMidnight(time.getDate()));

		time = new Time(date);
		milliseconds = DateUtils.getMillisecondsFromMidnight(date);
		Assertions.assertEquals(milliseconds, DateUtils.getMillisecondsFromMidnight(time.getDate()));

		time = Time.valueOf(milliseconds);
		Assertions.assertEquals(milliseconds, DateUtils.getMillisecondsFromMidnight(time.getDate()));

		time = Time.valueOf(date);
		Assertions.assertEquals(milliseconds, DateUtils.getMillisecondsFromMidnight(time.getDate()));

		time = Time.parse("12:56 AM", "h:mm aaa");
		date = DateUtils.toDate("01/01/2009 12:56 AM", "MM/dd/yyyy h:mm aaa");
		milliseconds = DateUtils.getMillisecondsFromMidnight(date);
		Assertions.assertEquals(milliseconds, DateUtils.getMillisecondsFromMidnight(time.getDate()));

		date = DateUtils.toDate("01/01/2009 9:56:45.250", "MM/dd/yyyy HH:mm:ss.SSS");
		time = new Time(date);
		Assertions.assertEquals((9 * 60 * 60 + 56 * 60 + 45) * 1000 + 250, DateUtils.getMillisecondsFromMidnight(time.getDate()));
		Assertions.assertEquals((9 * 60 + 56) * 60 + 45, time.getSeconds(date));
		Assertions.assertEquals(9 * 60 + 56, time.getMinutes(date));
		Assertions.assertEquals(9, time.getHours(date));

		time = Time.parse("12:56 AM", "h:mm aaa");
		date = DateUtils.toDate("01/01/2009 12:56 AM", "MM/dd/yyyy h:mm aaa");
		Assertions.assertEquals("00:56:00", time.toString());
		Assertions.assertEquals("12:56 AM", time.toString("h:mm aaa"));

		Time time2 = Time.parse("12:56 AM", "h:mm aaa");

		Assertions.assertEquals(time, time2);
		Assertions.assertEquals(time.hashCode(), time2.hashCode());
		Assertions.assertFalse(time.equals(DateUtils.getMillisecondsFromMidnight(time.getDate()) + 1));
		Assertions.assertFalse(time.equals(new Object()));

		Assertions.assertEquals(0, time.compareTo(time2));
		Assertions.assertEquals(-1, time.compareTo(new Time(DateUtils.getMillisecondsFromMidnight(time.getDate()) + 1)));
		Assertions.assertEquals(1, time.compareTo(new Time(DateUtils.getMillisecondsFromMidnight(time.getDate()) - 1)));
	}


	@Test
	public void testDayLightSavings() {
		Time time = new Time(61200000); // 17:00:00
		// if new Date is used for impl and executed on 3/13/2011 then will be off by an hour
		Assertions.assertEquals("17:00:00", time.toString());

		time = Time.parse("4:15:00:00");
		Date date = DateUtils.setTime(DateUtils.toDate("03/13/2011", "MM/dd/yyyy"), time);
		Date dateExpected = DateUtils.toDate("03/13/2011 04:15:00.000", "MM/dd/yyyy HH:mm:ss.SSS");

		Assertions.assertEquals(dateExpected, date);

		date = DateUtils.setTime(DateUtils.toDate("11/06/2011", "MM/dd/yyyy"), time);
		dateExpected = DateUtils.toDate("11/06/2011 04:15:00.000", "MM/dd/yyyy HH:mm:ss.SSS");

		Assertions.assertEquals(dateExpected, date);
	}


	@Test
	public void testBefore() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			Time time1 = new Time(5200);
			Time time2 = new Time(75467646);

			Assertions.assertTrue(time1.before(time2));
			Assertions.assertFalse(time2.before(time1));

			time2.before(null);
		});
	}


	@Test
	public void testAfter() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			Time time1 = new Time(5200);
			Time time2 = new Time(75467646);

			Assertions.assertFalse(time1.after(time2));
			Assertions.assertTrue(time2.after(time1));

			time1.after(null);
		});
	}
}
