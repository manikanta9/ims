package com.clifton.core.util.matching;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


public class PermutationTest {

	@Test
	public void testPermutationEmptyList() {
		List<PermutationTestClass> list = new ArrayList<>();

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list);
		Iterator<List<PermutationTestClass>> testIterator = testPermutation.iterator();
		Assertions.assertFalse(testIterator.hasNext());
	}


	@Test
	public void testMaxMinTwo() {
		List<PermutationTestClass> list = new ArrayList<>();
		list.add(new PermutationTestClass("x"));
		list.add(new PermutationTestClass("y"));
		list.add(new PermutationTestClass("z"));

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list, 2, 2);
		Iterator<List<PermutationTestClass>> testIterator = testPermutation.iterator();

		List<String> result = new ArrayList<>();
		while (testIterator.hasNext()) {
			List<PermutationTestClass> next = testIterator.next();
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (PermutationTestClass c : CollectionUtils.getIterable(next)) {
				sb.append(c.toString());
			}
			sb.append("}");
			result.add(sb.toString());
		}
		String expected = "{xy}\n" + "{xz}\n" + "{yz}";
		Assertions.assertEquals(expected, StringUtils.join(result, "\n"));
	}


	@Test
	public void testMaxMinOne() {
		List<PermutationTestClass> list = new ArrayList<>();
		list.add(new PermutationTestClass("x"));
		list.add(new PermutationTestClass("y"));
		list.add(new PermutationTestClass("z"));

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list, 1, 1);
		Iterator<List<PermutationTestClass>> testIterator = testPermutation.iterator();

		List<String> result = new ArrayList<>();
		while (testIterator.hasNext()) {
			List<PermutationTestClass> next = testIterator.next();
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (PermutationTestClass c : CollectionUtils.getIterable(next)) {
				sb.append(c.toString());
			}
			sb.append("}");
			result.add(sb.toString());
		}
		String expected = "{x}\n" + "{y}\n" + "{z}";
		Assertions.assertEquals(expected, StringUtils.join(result, "\n"));
	}


	@Test
	public void testPermutationXYZ() {
		List<PermutationTestClass> list = new ArrayList<>();
		list.add(new PermutationTestClass("x"));
		list.add(new PermutationTestClass("y"));
		list.add(new PermutationTestClass("z"));

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list);
		Assertions.assertEquals(0, BigInteger.valueOf(7).compareTo(testPermutation.getCombinationCount()));

		List<String> result = new ArrayList<>();
		for (List<PermutationTestClass> next : testPermutation) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (PermutationTestClass c : CollectionUtils.getIterable(next)) {
				sb.append(c.toString());
			}
			sb.append("}");
			result.add(sb.toString());
		}

		String expected = "{x}\n" + "{y}\n" + "{xy}\n" + "{z}\n" + "{xz}\n" + "{yz}\n" + "{xyz}";
		Assertions.assertEquals(expected, StringUtils.join(result, "\n"));

		Assertions.assertTrue(result.contains("{x}"));
		Assertions.assertTrue(result.contains("{y}"));
		Assertions.assertTrue(result.contains("{z}"));
		Assertions.assertTrue(result.contains("{xy}"));
		Assertions.assertTrue(result.contains("{xz}"));
		Assertions.assertTrue(result.contains("{yz}"));
		Assertions.assertTrue(result.contains("{xyz}"));
	}


	@Test
	public void testPermutationABC() {
		List<PermutationTestClass> list = new ArrayList<>();
		list.add(new PermutationTestClass("a"));
		list.add(new PermutationTestClass("b"));
		list.add(new PermutationTestClass("c"));
		list.add(new PermutationTestClass("d"));

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list, 3);
		Assertions.assertEquals(0, BigInteger.valueOf(14).compareTo(testPermutation.getCombinationCount()));

		List<String> result = new ArrayList<>();
		for (List<PermutationTestClass> next : testPermutation) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (PermutationTestClass c : CollectionUtils.getIterable(next)) {
				sb.append(c.toString());
			}
			sb.append("}");
			result.add(sb.toString());
		}

		String expected = "{a}\n{b}\n{ab}\n{c}\n{ac}\n{bc}\n{abc}\n{d}\n{ad}\n{bd}\n{abd}\n{cd}\n{acd}\n{bcd}";
		Assertions.assertEquals(expected, StringUtils.join(result, "\n"));

		Assertions.assertTrue(result.contains("{a}"));
		Assertions.assertTrue(result.contains("{b}"));
		Assertions.assertTrue(result.contains("{c}"));
		Assertions.assertTrue(result.contains("{d}"));
		Assertions.assertTrue(result.contains("{ab}"));
		Assertions.assertTrue(result.contains("{ac}"));
		Assertions.assertTrue(result.contains("{ad}"));
		Assertions.assertTrue(result.contains("{bc}"));
		Assertions.assertTrue(result.contains("{bd}"));
		Assertions.assertTrue(result.contains("{cd}"));
		Assertions.assertTrue(result.contains("{abc}"));
		Assertions.assertTrue(result.contains("{abd}"));
		Assertions.assertTrue(result.contains("{acd}"));
		Assertions.assertTrue(result.contains("{bcd}"));
	}


	@Test
	public void testPermutationABCSizeOfTwoOrThree() {
		List<PermutationTestClass> list = new ArrayList<>();
		list.add(new PermutationTestClass("a"));
		list.add(new PermutationTestClass("b"));
		list.add(new PermutationTestClass("c"));
		list.add(new PermutationTestClass("d"));

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list, 3, 2);
		Assertions.assertEquals(0, BigInteger.valueOf(10).compareTo(testPermutation.getCombinationCount()));

		List<String> result = new ArrayList<>();
		for (List<PermutationTestClass> next : testPermutation) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (PermutationTestClass c : CollectionUtils.getIterable(next)) {
				sb.append(c.toString());
			}
			sb.append("}");
			result.add(sb.toString());
		}

		Assertions.assertTrue(result.contains("{ab}"));
		Assertions.assertTrue(result.contains("{ac}"));
		Assertions.assertTrue(result.contains("{ad}"));
		Assertions.assertTrue(result.contains("{bc}"));
		Assertions.assertTrue(result.contains("{bd}"));
		Assertions.assertTrue(result.contains("{cd}"));
		Assertions.assertTrue(result.contains("{abc}"));
		Assertions.assertTrue(result.contains("{abd}"));
		Assertions.assertTrue(result.contains("{acd}"));
		Assertions.assertTrue(result.contains("{bcd}"));
	}


	@Test
	public void testPermutationAtoU() {
		List<PermutationTestClass> list = new ArrayList<>();
		list.add(new PermutationTestClass("a"));
		list.add(new PermutationTestClass("b"));
		list.add(new PermutationTestClass("c"));
		list.add(new PermutationTestClass("d"));
		list.add(new PermutationTestClass("e"));
		list.add(new PermutationTestClass("f"));
		list.add(new PermutationTestClass("g"));
		list.add(new PermutationTestClass("h"));
		list.add(new PermutationTestClass("i"));
		list.add(new PermutationTestClass("j"));
		list.add(new PermutationTestClass("k"));
		list.add(new PermutationTestClass("l"));
		list.add(new PermutationTestClass("m"));
		list.add(new PermutationTestClass("n"));
		list.add(new PermutationTestClass("o"));
		list.add(new PermutationTestClass("p"));
		list.add(new PermutationTestClass("q"));
		list.add(new PermutationTestClass("r"));
		list.add(new PermutationTestClass("s"));
		list.add(new PermutationTestClass("t"));
		list.add(new PermutationTestClass("u"));

		Permutation<PermutationTestClass> testPermutation = new Permutation<>(list);
		Assertions.assertEquals(0, BigInteger.valueOf(2097151).compareTo(testPermutation.getCombinationCount()));

		List<String> result = new ArrayList<>();
		for (List<PermutationTestClass> next : testPermutation) {
			StringBuilder sb = new StringBuilder();
			sb.append("{");
			for (PermutationTestClass c : CollectionUtils.getIterable(next)) {
				sb.append(c.toString());
			}
			sb.append("}");
			result.add(sb.toString());
		}

		Assertions.assertTrue(result.contains("{a}"));
		Assertions.assertTrue(result.contains("{b}"));
		Assertions.assertTrue(result.contains("{c}"));
		Assertions.assertTrue(result.contains("{d}"));
		Assertions.assertTrue(result.contains("{ab}"));
		Assertions.assertTrue(result.contains("{ac}"));
		Assertions.assertTrue(result.contains("{ad}"));
		Assertions.assertTrue(result.contains("{bc}"));
		Assertions.assertTrue(result.contains("{bd}"));
		Assertions.assertTrue(result.contains("{cd}"));
		Assertions.assertTrue(result.contains("{abc}"));
		Assertions.assertTrue(result.contains("{abd}"));
		Assertions.assertTrue(result.contains("{acd}"));
		Assertions.assertTrue(result.contains("{bcd}"));
		Assertions.assertTrue(result.contains("{abcdefghijklmnopqrstu}"));
	}


	@Test
	@Disabled
	public void testPermutationAtoZ() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {

			List<PermutationTestClass> list = new ArrayList<>();
			for (int i = 0; i < 75; i++) {
				list.add(new PermutationTestClass(i + ""));
			}

			Permutation<PermutationTestClass> testIterator = new Permutation<>(list);
			@SuppressWarnings("unused")
			BigInteger count = testIterator.getCombinationCount();
		});
		Assertions.assertEquals("[class com.clifton.reconcile.trade.intraday.matching.Permutation] does not support permution for a list of size [75] with a min combination size of [1] and max combination size of [75] because it exceeds the max number of combinations ["
				+ Long.MAX_VALUE + "].", runtimeException.getMessage());
	}
}
