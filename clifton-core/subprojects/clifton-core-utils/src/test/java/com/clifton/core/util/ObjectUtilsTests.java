package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


public class ObjectUtilsTests {

	@SuppressWarnings({"SimplifiableAssertion", "EqualsWithItself", "ConstantConditions"})
	@Test
	public void testIsEquals() {
		// had our own utility but the refactored it to use Java API: leaving tests to make sure Java API works as expected
		Assertions.assertTrue(Objects.equals(null, null));
		Assertions.assertFalse(Objects.equals("value", null));
		Assertions.assertFalse(Objects.equals(null, "value"));
		Assertions.assertFalse(Objects.equals("value1", "value2"));
		Assertions.assertTrue(Objects.equals("value", "value"));
		Assertions.assertFalse(Objects.equals(123, 1234));
		Assertions.assertTrue(Objects.equals(123, 123));
	}


	@Test
	public void testToString() {
		Assertions.assertNull(ObjectUtils.toString(null));
		Assertions.assertEquals("abc", ObjectUtils.toString("abc"));
		Assertions.assertEquals("123", ObjectUtils.toString(123));
		Assertions.assertEquals("[1, 2, 3]", ObjectUtils.toString(new int[]{1, 2, 3}));
		Assertions.assertEquals("[true, true, false]", ObjectUtils.toString(new boolean[]{true, true, false}));
		Assertions.assertEquals("[123, abc]", ObjectUtils.toString(new String[]{"123", "abc"}));
	}


	@SuppressWarnings("ConstantConditions")
	@Test
	public void testCoalesce() {
		Assertions.assertNull(ObjectUtils.coalesce());
		Assertions.assertNull(ObjectUtils.coalesce((Object[]) null));
		Assertions.assertNull(ObjectUtils.coalesce(null, null, null));
		Assertions.assertEquals("", ObjectUtils.coalesce(""));
		Assertions.assertEquals("Test", ObjectUtils.coalesce("Test"));
		Assertions.assertEquals("Test", ObjectUtils.coalesce("Test", "Test2"));
		Assertions.assertEquals("Test", ObjectUtils.coalesce(null, "Test", "Test2"));
	}


	@Test
	public void testIsNotNullPresent() {
		Assertions.assertFalse(ObjectUtils.isNotNullPresent());
		Assertions.assertFalse(ObjectUtils.isNotNullPresent((Object) null));
		Assertions.assertFalse(ObjectUtils.isNotNullPresent(null, null));

		Assertions.assertTrue(ObjectUtils.isNotNullPresent(1));
		Assertions.assertTrue(ObjectUtils.isNotNullPresent(1, null));
		Assertions.assertTrue(ObjectUtils.isNotNullPresent(null, 1));
		Assertions.assertTrue(ObjectUtils.isNotNullPresent(null, 1, null));
	}


	/**
	 * Validates standard behavior of conditional chains.
	 */
	@Test
	public void testDoIfPresentConditionalChains() {
		final Object resultObject = new Object();
		Supplier<Object> nullSupplier = () -> null;
		Supplier<Object> objectSupplier = () -> resultObject;
		Runnable noopRunnable = () -> {
		};
		Consumer<Object> noopConsumer = obj -> {
		};

		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer), false, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(1, noopConsumer), true, null);
		assertConditionalChainResult(() -> {
			ObjectUtils.ConditionalChain<Object> chain = ObjectUtils.doIfPresent(null, noopConsumer);
			chain.orElse(noopRunnable);
			return chain;
		}, true, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseSupply(nullSupplier), false, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseSupply(objectSupplier), true, resultObject);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseIfPresent(null, noopConsumer), false, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseIfPresent(1, noopConsumer), true, null);

		assertConditionalChainResult(() -> {
			ObjectUtils.ConditionalChain<Object> chain = ObjectUtils.doIfPresent(null, noopConsumer).orElseIfPresent(null, noopConsumer);
			chain.orElse(noopRunnable);
			return chain;
		}, true, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseIfPresent(null, noopConsumer).orElseSupply(objectSupplier), true, resultObject);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseIfPresent(null, noopConsumer).orElseIfPresent(1, noopConsumer), true, null);

		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseMap(null, Function.identity()), false, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseMap(resultObject, Function.identity()), true, resultObject);

		assertConditionalChainResult(() -> ObjectUtils.map(null, Function.identity()), false, null);
		assertConditionalChainResult(() -> ObjectUtils.map(resultObject, Function.identity()), true, resultObject);
	}


	/**
	 * Validates termination conditions of conditional chains.
	 */
	@Test
	public void testDoIfPresentTermination() {
		final String failureMsg = "Conditional chain action ran unexpectedly.";
		Runnable failureRunnable = () -> AssertUtils.fail(failureMsg);
		Consumer<Object> failureConsumer = obj -> AssertUtils.fail(failureMsg);
		Supplier<Object> failureSupplier = () -> {
			AssertUtils.fail(failureMsg);
			return new Object();
		};
		Function<Object, Object> failureFunction = obj -> {
			AssertUtils.fail(failureMsg);
			return new Object();
		};

		Supplier<ObjectUtils.ConditionalChain<Object>> terminatedChainSupplier = () -> ObjectUtils.doIfPresent(1, obj -> {
		});
		assertConditionalChainResult(terminatedChainSupplier, true, null);

		// Verify that actions are not committed by any terminated chain
		terminatedChainSupplier.get().orElse(failureRunnable);
		terminatedChainSupplier.get().orElseSupply(failureSupplier);
		terminatedChainSupplier.get().orElseIfPresent(1, failureConsumer);
		terminatedChainSupplier.get().orElseMap(1, failureFunction);
	}


	/**
	 * Validates "then" behaviors of conditional chains.
	 */
	@Test
	public void testDoIfPresentThenChains() {
		final Object resultObject = new Object();
		Runnable noopRunnable = () -> {
		};
		Consumer<Object> noopConsumer = obj -> {
		};
		Supplier<Object> objectSupplier = () -> resultObject;
		Function<Object, Object> objectFunction = obj -> resultObject;

		assertConditionalChainResult(() -> {
			ObjectUtils.ConditionalChain<Object> chain = ObjectUtils.doIfPresent(1, noopConsumer).thenConsume(noopConsumer);
			chain.orElse(noopRunnable);
			return chain;
		}, true, null);
		assertConditionalChainResult(() -> {
			ObjectUtils.ConditionalChain<Object> chain = ObjectUtils.doIfPresent(1, noopConsumer).thenMap(objectFunction);
			chain.orElse(noopRunnable);
			return chain;
		}, true, null);

		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).thenConsume(noopConsumer), false, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(1, noopConsumer).thenConsume(noopConsumer), false, null);
		assertConditionalChainResult(() -> ObjectUtils.doIfPresent(null, noopConsumer).orElseSupply(objectSupplier).thenConsume(noopConsumer), true, null);
		assertConditionalChainResult(() -> ObjectUtils.map(null, objectFunction).thenConsume(noopConsumer), false, null);
		assertConditionalChainResult(() -> ObjectUtils.map(1, objectFunction).thenConsume(noopConsumer), true, null);
		assertConditionalChainResult(() -> ObjectUtils.map(null, objectFunction).thenMap(objectFunction), false, null);
		assertConditionalChainResult(() -> ObjectUtils.map(1, objectFunction).thenMap(objectFunction), true, resultObject);
		assertConditionalChainResult(() -> ObjectUtils.map(null, objectFunction).thenMap(objectFunction).orElseSupply(objectSupplier), true, resultObject);
	}


	@SuppressWarnings("SimplifiableAssertion")
	@Test
	public void testDoIfPresentGetOrElse() {
		Assertions.assertEquals(1, ObjectUtils.map(1, Function.identity()).get());
		Assertions.assertEquals(1, ObjectUtils.map(1, Function.identity()).thenMap(Function.identity()).get());
		Assertions.assertEquals(1, ObjectUtils.map(1, Function.identity()).thenMap(Function.identity()).getOrElse(2));
		Assertions.assertEquals(null, ObjectUtils.map(null, Function.identity()).get());
		Assertions.assertEquals(null, ObjectUtils.map(null, Function.identity()).thenMap(Function.identity()).get());
		Assertions.assertEquals(2, ObjectUtils.map(null, Function.identity()).thenMap(Function.identity()).getOrElse(2));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static <R> void assertConditionalChainResult(Supplier<ObjectUtils.ConditionalChain<R>> chainSupplier, boolean expectedTerminationStatus, Object expectedResult) {
		ObjectUtils.ConditionalChain<R> chain = chainSupplier.get();
		Assertions.assertEquals(expectedTerminationStatus, chain.isTerminated(), "Unexpected chain termination status.");
		Assertions.assertEquals(expectedResult, chain.get(), "Unexpected chain resulting value.");
	}
}
