package com.clifton.core.util;

import com.clifton.core.util.dataaccess.PagingArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;


/**
 * The {@link CollectionUtilsTests} class is a test class for {@link CollectionUtils}.
 *
 * @author vgomelsky
 */
@SuppressWarnings({"ArraysAsListWithZeroOrOneArgument", "ConstantConditions"})
public class CollectionUtilsTests {

	@Test
	public void testIsEmpty() {
		Assertions.assertTrue(CollectionUtils.isEmpty((Collection<Object>) null));
		ArrayList<Integer> list = new ArrayList<>();
		Assertions.assertTrue(CollectionUtils.isEmpty(list));
		list.add(1);
		Assertions.assertFalse(CollectionUtils.isEmpty(list));
	}


	@SuppressWarnings("OverwrittenKey")
	@Test
	public void testIsSingleElement() {
		Assertions.assertFalse(CollectionUtils.isSingleElement(null));
		Assertions.assertFalse(CollectionUtils.isSingleElement(Arrays.asList()));
		Assertions.assertTrue(CollectionUtils.isSingleElement(Arrays.asList(1)));
		Assertions.assertTrue(CollectionUtils.isSingleElement(new HashSet<Object>() {{
			add(1);
		}}));
		Assertions.assertTrue(CollectionUtils.isSingleElement(new HashSet<Object>() {{
			add(1);
			add(1);
		}}));
		Assertions.assertFalse(CollectionUtils.isSingleElement(new HashSet<Object>() {{
			add(1);
			add(2);
		}}));
		Assertions.assertFalse(CollectionUtils.isSingleElement(Arrays.asList(1, 1)));
		Assertions.assertFalse(CollectionUtils.isSingleElement(Arrays.asList(1, 2)));
		Assertions.assertFalse(CollectionUtils.isSingleElement(Arrays.asList("1", "2")));
		Assertions.assertFalse(CollectionUtils.isSingleElement(Arrays.asList(1, 2, 3, 4, 5)));
	}


	@Test
	public void testToString() {
		// Default string
		Assertions.assertNull(CollectionUtils.toString(null, 5));
		Assertions.assertEquals("[]", CollectionUtils.toString(Arrays.asList(), 5));
		Assertions.assertEquals("[1]", CollectionUtils.toString(Arrays.asList(1), 5));
		Assertions.assertEquals("[1, 2]", CollectionUtils.toString(Arrays.asList(1, 2), 5));
		Assertions.assertEquals("[1, 2, 3, 4, 5]", CollectionUtils.toString(Arrays.asList(1, 2, 3, 4, 5), 5));
		Assertions.assertEquals("[1, 2, 3, 4, 5, 1 more elements]", CollectionUtils.toString(Arrays.asList(1, 2, 3, 4, 5, 6), 5));
		Assertions.assertEquals("[1, 2, 3, 4, 5, 11 more elements]", CollectionUtils.toString(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16), 5));
		Assertions.assertEquals("[]", CollectionUtils.toString(Arrays.asList(), 0));
		// When maxElements is 0 or less, all elements are printed
		Assertions.assertEquals("[1]", CollectionUtils.toString(Arrays.asList(1), 0));
		Assertions.assertEquals("[1, 2]", CollectionUtils.toString(Arrays.asList(1, 2), 0));
		Assertions.assertEquals("[1, 2]", CollectionUtils.toString(Arrays.asList(1, 2), -1));
		Assertions.assertEquals("[1, 1 more elements]", CollectionUtils.toString(Arrays.asList(1, 2), 1));

		// Custom string
		IntFunction<String> remainderStrFn = remainder -> "and " + remainder + " more elements...";
		Assertions.assertNull(CollectionUtils.toString(null, 5, remainderStrFn));
		Assertions.assertEquals("[]", CollectionUtils.toString(Arrays.asList(), 5, remainderStrFn));
		Assertions.assertEquals("[1]", CollectionUtils.toString(Arrays.asList(1), 5, remainderStrFn));
		Assertions.assertEquals("[1, 2]", CollectionUtils.toString(Arrays.asList(1, 2), 5, remainderStrFn));
		Assertions.assertEquals("[1, 2, 3, 4, 5]", CollectionUtils.toString(Arrays.asList(1, 2, 3, 4, 5), 5, remainderStrFn));
		Assertions.assertEquals("[1, 2, 3, 4, 5, and 1 more elements...]", CollectionUtils.toString(Arrays.asList(1, 2, 3, 4, 5, 6), 5, remainderStrFn));
		Assertions.assertEquals("[1, 2, 3, 4, 5, and 11 more elements...]", CollectionUtils.toString(Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16), 5, remainderStrFn));
		Assertions.assertEquals("[]", CollectionUtils.toString(Arrays.asList(), 0, remainderStrFn));
		Assertions.assertEquals("[1]", CollectionUtils.toString(Arrays.asList(1), 0, remainderStrFn));
		Assertions.assertEquals("[1, 2]", CollectionUtils.toString(Arrays.asList(1, 2), 0, remainderStrFn));
		Assertions.assertEquals("[1, 2]", CollectionUtils.toString(Arrays.asList(1, 2), -1, remainderStrFn));
		Assertions.assertEquals("[1, and 1 more elements...]", CollectionUtils.toString(Arrays.asList(1, 2), 1, remainderStrFn));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testToPagingArrayList() {
		Assertions.assertEquals(new PagingArrayList<>(), CollectionUtils.toPagingArrayList(null, 0, 15));

		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14);

		PagingArrayList<Integer> result = (PagingArrayList<Integer>) CollectionUtils.toPagingArrayList(list, 0, 5);
		Assertions.assertEquals(5, result.size());
		Assertions.assertEquals(5, result.getPageSize());
		Assertions.assertEquals(3, result.getTotalPageCount());
		Assertions.assertEquals(1, result.getCurrentPageNumber());
		Assertions.assertEquals(14, result.getTotalElementCount());
		Assertions.assertEquals(0, result.getFirstElementIndex());
		Assertions.assertEquals(new Integer(1), result.get(0));
		Assertions.assertEquals(new Integer(2), result.get(1));
		Assertions.assertEquals(new Integer(3), result.get(2));
		Assertions.assertEquals(new Integer(4), result.get(3));
		Assertions.assertEquals(new Integer(5), result.get(4));

		result = (PagingArrayList<Integer>) CollectionUtils.toPagingArrayList(list, 5, 5);
		Assertions.assertEquals(5, result.size());
		Assertions.assertEquals(5, result.getPageSize());
		Assertions.assertEquals(3, result.getTotalPageCount());
		Assertions.assertEquals(2, result.getCurrentPageNumber());
		Assertions.assertEquals(14, result.getTotalElementCount());
		Assertions.assertEquals(5, result.getFirstElementIndex());
		Assertions.assertEquals(new Integer(6), result.get(0));
		Assertions.assertEquals(new Integer(7), result.get(1));
		Assertions.assertEquals(new Integer(8), result.get(2));
		Assertions.assertEquals(new Integer(9), result.get(3));
		Assertions.assertEquals(new Integer(10), result.get(4));

		result = (PagingArrayList<Integer>) CollectionUtils.toPagingArrayList(list, 10, 5);
		Assertions.assertEquals(4, result.size());
		Assertions.assertEquals(5, result.getPageSize());
		Assertions.assertEquals(3, result.getTotalPageCount());
		Assertions.assertEquals(3, result.getCurrentPageNumber());
		Assertions.assertEquals(14, result.getTotalElementCount());
		Assertions.assertEquals(10, result.getFirstElementIndex());
		Assertions.assertEquals(new Integer(11), result.get(0));
		Assertions.assertEquals(new Integer(12), result.get(1));
		Assertions.assertEquals(new Integer(13), result.get(2));
		Assertions.assertEquals(new Integer(14), result.get(3));
	}


	@Test
	public void testToPagingArrayListWithPageProperties() {
		// Validate list contents
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.toPagingArrayList(null, null));
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.toPagingArrayList(Arrays.asList(), null));
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.toPagingArrayList(null, Arrays.asList()));
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.toPagingArrayList(null, new PagingArrayList<>()));
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.toPagingArrayList(Arrays.asList(), new PagingArrayList<>()));
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.toPagingArrayList(Arrays.asList(), Arrays.asList()));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), null));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), Arrays.asList(1, 3)));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), Arrays.asList(1, 3)));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), new PagingArrayList<>(Arrays.asList(1, 3), 0, 50)));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), new PagingArrayList<>(Arrays.asList(1, 3, 10), 10, 50)));

		// Validate page properties
		Assertions.assertEquals(0, ((PagingArrayList<?>) CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), null)).getFirstElementIndex());
		Assertions.assertEquals(3, ((PagingArrayList<?>) CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), null)).getPageSize());
		Assertions.assertEquals(3, ((PagingArrayList<?>) CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), null)).getTotalElementCount());
		Assertions.assertEquals(0, ((PagingArrayList<?>) CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), new PagingArrayList<>(Arrays.asList(1, 3), 0, 50))).getFirstElementIndex());
		Assertions.assertEquals(50, ((PagingArrayList<?>) CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), new PagingArrayList<>(Arrays.asList(1, 3), 0, 50, 50))).getPageSize());
		Assertions.assertEquals(50, ((PagingArrayList<?>) CollectionUtils.toPagingArrayList(Arrays.asList(1, 2, 3), new PagingArrayList<>(Arrays.asList(1, 3), 0, 50, 50))).getTotalElementCount());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetFirstElement() {
		Assertions.assertNull(CollectionUtils.getFirstElement(null));

		List<Integer> list = new ArrayList<>();
		Assertions.assertNull(CollectionUtils.getFirstElement(list));

		list.add(5);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getFirstElement(list));

		list.add(6);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getFirstElement(list));
	}


	@Test
	public void testGetFirstElement_FirstElementIsNull() {
		Assertions.assertNull(CollectionUtils.getFirstElement(null));

		List<String> list = new ArrayList<>();
		Assertions.assertNull(CollectionUtils.getFirstElement(list));

		list.add(null);
		Assertions.assertNull(CollectionUtils.getFirstElement(list));

		list.add("test");
		Assertions.assertNull(CollectionUtils.getFirstElement(list));
	}

	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetLastElement_LinkedHashSet() {
		Set<Integer> set = new LinkedHashSet<>();
		Assertions.assertNull(CollectionUtils.getLastElement(set));

		set.add(5);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getLastElement(set));

		set.add(6);
		Assertions.assertEquals(new Integer(6), CollectionUtils.getLastElement(set));
	}


	@Test
	public void testGetLastElement_LinkedHashSet_LastElementIsNull() {
		Set<String> set = new LinkedHashSet<>();
		Assertions.assertNull(CollectionUtils.getLastElement(set));

		set.add("test");
		Assertions.assertEquals("test", CollectionUtils.getLastElement(set));

		set.add(null);
		Assertions.assertNull(CollectionUtils.getLastElement(set));
	}


	@Test
	public void testGetLastElement_List() {
		List<Integer> list = new ArrayList<>();
		Assertions.assertNull(CollectionUtils.getLastElement(list));

		list.add(5);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getLastElement(list));

		list.add(6);
		Assertions.assertEquals(new Integer(6), CollectionUtils.getLastElement(list));
	}


	@Test
	public void testGetLastElement_List_LastElementIsNull() {
		List<String> list = new ArrayList<>();
		Assertions.assertNull(CollectionUtils.getLastElement(list));

		list.add("test");
		Assertions.assertEquals("test", CollectionUtils.getLastElement(list));

		list.add(null);
		Assertions.assertNull(CollectionUtils.getLastElement(list));
	}

	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetFirstElementStrict() {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getFirstElementStrict(list));
	}


	@Test
	public void testGetFirstElementStrict_FailOnEmpty() {
		Assertions.assertThrows(NoSuchElementException.class, () -> Assertions.assertNull(CollectionUtils.getFirstElementStrict(new ArrayList<>())));
	}


	@Test
	public void testGetFirstElementStrict_FailOnNull() {
		Assertions.assertThrows(NoSuchElementException.class, () -> Assertions.assertNull(CollectionUtils.getFirstElementStrict(null)));
	}


	@Test
	public void testGetFirstElementStrict_FailOnFirstElementNull() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			List<String> list = new ArrayList<>();
			list.add(null);
			Assertions.assertNull(CollectionUtils.getFirstElementStrict(list));
		});
	}


	@Test
	public void testFirstElementStrict_FirstOfTwoElements() {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		list.add(6);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getFirstElementStrict(list));
	}

	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetOnlyElement() {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		Integer expected = 5;
		Assertions.assertEquals(expected, CollectionUtils.getOnlyElement(list));
	}


	@Test
	public void testGetOnlyElement_NullOnEmpty() {
		Assertions.assertNull(CollectionUtils.getOnlyElement(new ArrayList<>()));
	}


	@Test
	public void testGetOnlyElement_NullOnNull() {
		Assertions.assertNull(CollectionUtils.getOnlyElement(null));
	}


	@Test
	public void testGetOnlyElement_FirstElementIsNull() {
		List<String> list = new ArrayList<>();
		list.add(null);
		Assertions.assertNull(CollectionUtils.getOnlyElement(list));
	}


	@Test
	public void testOnlyElement_FailOn2() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			ArrayList<Integer> list = new ArrayList<>();
			list.add(5);
			list.add(6);
			CollectionUtils.getOnlyElement(list);
		});
	}

	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetOnlyElementStrict() {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(5);
		Assertions.assertEquals(new Integer(5), CollectionUtils.getOnlyElementStrict(list));
	}


	@Test
	public void testGetOnlyElementStrict_FailOnEmpty() {
		Assertions.assertThrows(NoSuchElementException.class, () -> CollectionUtils.getOnlyElementStrict(new ArrayList<>()));
	}


	@Test
	public void testGetOnlyElementStrict_FailOnNull() {
		Assertions.assertThrows(NoSuchElementException.class, () -> CollectionUtils.getOnlyElementStrict(null));
	}


	@Test
	public void testGetOnlyElementStrict_FailOnFirstElementNull() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			List<String> list = new ArrayList<>();
			list.add(null);
			Assertions.assertNull(CollectionUtils.getFirstElementStrict(list));
		});
	}


	@Test
	public void testOnlyElementStrict_FailOnTwoElementList() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			ArrayList<Integer> list = new ArrayList<>();
			list.add(5);
			list.add(6);
			CollectionUtils.getOnlyElementStrict(list);
		});
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testContains() {
		Collection<Object> list = null;
		String entity = "Test";
		Assertions.assertFalse(CollectionUtils.contains(list, entity));
		list = new ArrayList<>();
		Assertions.assertFalse(CollectionUtils.contains(list, entity));
		list.add("Test-Different");
		Assertions.assertFalse(CollectionUtils.contains(list, entity));
		list.add("Test");
		Assertions.assertTrue(CollectionUtils.contains(list, entity));

		BigDecimal numberEntity = BigDecimal.valueOf(5.55428);
		Assertions.assertFalse(CollectionUtils.contains(list, numberEntity));
		list.add(5.55428);
		// Not the same - looking for BigDecimal, added double
		Assertions.assertFalse(CollectionUtils.contains(list, numberEntity));
		list.add(BigDecimal.valueOf(5.55428));
		Assertions.assertTrue(CollectionUtils.contains(list, numberEntity));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testAnyMatch() {
		Assertions.assertFalse(CollectionUtils.anyMatch(null, null));
		Assertions.assertFalse(CollectionUtils.anyMatch(null, Predicate.isEqual(1)));
		Assertions.assertFalse(CollectionUtils.anyMatch(Collections.emptyList(), Predicate.isEqual(1)));
		Assertions.assertFalse(CollectionUtils.anyMatch(Collections.emptyList(), Predicate.isEqual(null)));
		Assertions.assertFalse(CollectionUtils.anyMatch(Collections.singletonList(1), Predicate.isEqual(2)));
		Assertions.assertTrue(CollectionUtils.anyMatch(Collections.singletonList(1), Predicate.isEqual(1)));
		Assertions.assertFalse(CollectionUtils.anyMatch(Arrays.asList("a", "b", "c"), Predicate.isEqual("d")));
		Assertions.assertTrue(CollectionUtils.anyMatch(Arrays.asList("a", "b", "c"), Predicate.isEqual("b")));
	}


	@Test
	public void testAnyMatchFail() {
		// Null matchers are ignored in guard-clause scenario
		Assertions.assertFalse(CollectionUtils.anyMatch(Collections.emptyList(), null));
		// Null matchers are not supported
		Assertions.assertThrows(NullPointerException.class, () -> CollectionUtils.anyMatch(Collections.singletonList(1), null));
	}


	@Test
	public void testAllMatch() {
		Assertions.assertTrue(CollectionUtils.allMatch(null, null));
		Assertions.assertTrue(CollectionUtils.allMatch(null, Predicate.isEqual(1)));
		Assertions.assertTrue(CollectionUtils.allMatch(Collections.emptyList(), Predicate.isEqual(1)));
		Assertions.assertTrue(CollectionUtils.allMatch(Collections.emptyList(), Predicate.isEqual(null)));
		Assertions.assertFalse(CollectionUtils.allMatch(Collections.singletonList(1), Predicate.isEqual(2)));
		Assertions.assertTrue(CollectionUtils.allMatch(Collections.singletonList(1), Predicate.isEqual(1)));
		Assertions.assertFalse(CollectionUtils.allMatch(Arrays.asList("a", "b", "c"), Predicate.isEqual("d")));
		Assertions.assertFalse(CollectionUtils.allMatch(Arrays.asList("a", "b", "c"), Predicate.isEqual("b")));
		Assertions.assertTrue(CollectionUtils.allMatch(Arrays.asList("a", "b", "c"), val -> val instanceof String));
		Assertions.assertTrue(CollectionUtils.allMatch(Arrays.asList("a", "b", "c"), val -> Arrays.asList("a", "b", "c").contains(val)));
	}


	@Test
	public void testAllMatchFail() {
		// Null matchers are ignored in guard-clause scenario
		Assertions.assertTrue(CollectionUtils.allMatch(Collections.emptyList(), null));
		// Null matchers are not supported
		Assertions.assertThrows(NullPointerException.class, () -> CollectionUtils.allMatch(Collections.singletonList(1), null));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetDistinct() {
		Assertions.assertEquals(CollectionUtils.getDistinct(null), Arrays.asList());
		Assertions.assertEquals(CollectionUtils.getDistinct(Arrays.asList()), Arrays.asList());
		Assertions.assertEquals(CollectionUtils.getDistinct(getDistinctSampleCollection()), getDistinctSampleCollection());
		Assertions.assertEquals(CollectionUtils.getDistinct(Arrays.asList(1)), Arrays.asList(1));
		Assertions.assertEquals(CollectionUtils.getDistinct(Arrays.asList(1, 2, 3)), Arrays.asList(1, 2, 3));
		Assertions.assertEquals(CollectionUtils.getDistinct(Arrays.asList(1, 2, 1, 2, 3, 4)), Arrays.asList(1, 2, 3, 4));
		Assertions.assertEquals(CollectionUtils.getDistinct(Arrays.asList("a", "b", "c")), Arrays.asList("a", "b", "c"));
		List<Object> objectList = Arrays.asList(new Object(), new Object(), new Object());
		Assertions.assertEquals(CollectionUtils.getDistinct(objectList), objectList);
	}


	@Test
	public void testGetDistinctForKey() {
		final Collection<DistinctItemsSampleType<String, Integer>> sampleCollection = getDistinctSampleCollection();
		{
			Collection<DistinctItemsSampleType<String, Integer>> distinctItems = CollectionUtils.getDistinct(sampleCollection, item -> item.field1);
			Collection<DistinctItemsSampleType<String, Integer>> expectedItems = new ArrayList<>();
			expectedItems.add(new DistinctItemsSampleType<>("group1", 1));
			expectedItems.add(new DistinctItemsSampleType<>("group2", 1));
			expectedItems.add(new DistinctItemsSampleType<>("group3", 1));
			Assertions.assertEquals(expectedItems, distinctItems);
		}

		{
			Collection<DistinctItemsSampleType<String, Integer>> distinctItems = CollectionUtils.getDistinct(sampleCollection, item -> item.field2);
			Collection<DistinctItemsSampleType<String, Integer>> expectedItems = new ArrayList<>();
			expectedItems.add(new DistinctItemsSampleType<>("group1", 1));
			expectedItems.add(new DistinctItemsSampleType<>("group1", 2));
			expectedItems.add(new DistinctItemsSampleType<>("group1", 3));
			Assertions.assertEquals(expectedItems, distinctItems);
		}

		{
			List<DistinctItemsSampleType<String, Integer>> distinctItems = CollectionUtils.getDistinct(sampleCollection, item -> item.field1 + item.field2);
			Assertions.assertEquals(sampleCollection, distinctItems);
		}
	}


	@Test
	public void testGetDistinctWithReduction() {
		final Collection<DistinctItemsSampleType<String, Integer>> sampleCollection = getDistinctSampleCollection();
		{
			Collection<DistinctItemsSampleType<String, Integer>> distinctItems = CollectionUtils.getDistinct(sampleCollection, item -> item.field1,
					(cur, next) -> new DistinctItemsSampleType<>(cur.field1, cur.field2 + next.field2));
			Collection<DistinctItemsSampleType<String, Integer>> expectedItems = new ArrayList<>();
			expectedItems.add(new DistinctItemsSampleType<>("group1", 6));
			expectedItems.add(new DistinctItemsSampleType<>("group2", 6));
			expectedItems.add(new DistinctItemsSampleType<>("group3", 3));
			Assertions.assertEquals(expectedItems, distinctItems);
		}

		{
			Collection<DistinctItemsSampleType<String, Integer>> distinctItems = CollectionUtils.getDistinct(sampleCollection, item -> item.field2,
					(cur, next) -> new DistinctItemsSampleType<>(cur.field1 + "/" + next.field1, cur.field2));
			Collection<DistinctItemsSampleType<String, Integer>> expectedItems = new ArrayList<>();
			expectedItems.add(new DistinctItemsSampleType<>("group1/group2/group3", 1));
			expectedItems.add(new DistinctItemsSampleType<>("group1/group2/group3", 2));
			expectedItems.add(new DistinctItemsSampleType<>("group1/group2", 3));
			Assertions.assertEquals(expectedItems, distinctItems);
		}

		{
			// No reduction performed when only a single element exists of each type
			List<DistinctItemsSampleType<String, Integer>> distinctItems = CollectionUtils.getDistinct(sampleCollection, item -> item.field1 + item.field2,
					(cur, next) -> new DistinctItemsSampleType<>("non-matching", -1));
			Assertions.assertEquals(sampleCollection, distinctItems);
		}
	}


	@Test
	public void testCreateDistinctPredicate() {
		Assertions.assertEquals(
				getDistinctSampleCollection(),
				getDistinctSampleCollection().stream()
						.filter(CollectionUtils.createDistinctPredicate(Function.identity()))
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				Arrays.asList(
						new DistinctItemsSampleType<>("group1", 1),
						new DistinctItemsSampleType<>("group2", 1),
						new DistinctItemsSampleType<>("group3", 1)
				),
				getDistinctSampleCollection().stream()
						.filter(CollectionUtils.createDistinctPredicate(DistinctItemsSampleType::getField1))
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				Arrays.asList(
						new DistinctItemsSampleType<>("group1", 1),
						new DistinctItemsSampleType<>("group1", 2),
						new DistinctItemsSampleType<>("group1", 3)
				),
				getDistinctSampleCollection().stream()
						.filter(CollectionUtils.createDistinctPredicate(DistinctItemsSampleType::getField2))
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				getDistinctSampleCollection(),
				getDistinctSampleCollection().stream()
						.filter(CollectionUtils.createDistinctPredicate(item -> item.field1 + '/' + item.field2))
						.collect(Collectors.toList()));
		Assertions.assertEquals(
				Arrays.asList(new DistinctItemsSampleType<>("group1", 1)),
				getDistinctSampleCollection().stream()
						.filter(CollectionUtils.createDistinctPredicate(item -> item.field1.length()))
						.collect(Collectors.toList()));
	}


	private Collection<DistinctItemsSampleType<String, Integer>> getDistinctSampleCollection() {
		Collection<DistinctItemsSampleType<String, Integer>> result = new ArrayList<>();
		result.add(new DistinctItemsSampleType<>("group1", 1));
		result.add(new DistinctItemsSampleType<>("group1", 2));
		result.add(new DistinctItemsSampleType<>("group1", 3));
		result.add(new DistinctItemsSampleType<>("group2", 1));
		result.add(new DistinctItemsSampleType<>("group2", 2));
		result.add(new DistinctItemsSampleType<>("group2", 3));
		result.add(new DistinctItemsSampleType<>("group3", 1));
		result.add(new DistinctItemsSampleType<>("group3", 2));
		return result;
	}


	/**
	 * Sample type for testing methods related to finding distinct items.
	 *
	 * @param <T> the type of the first field
	 * @param <S> the type of the second field
	 */
	private static class DistinctItemsSampleType<T, S> {

		public final T field1;
		public final S field2;


		public DistinctItemsSampleType(T field1, S field2) {
			this.field1 = field1;
			this.field2 = field2;
		}


		@Override
		public String toString() {
			return String.format("([%s], [%s])", this.field1, this.field2);
		}


		@SuppressWarnings({"EqualsWhichDoesntCheckParameterClass", "com.haulmont.jpb.EqualsDoesntCheckParameterClass"})
		@Override
		public boolean equals(Object o) {
			// Minimal equals method: Assumes objects of same type and non-null fields are being compared
			DistinctItemsSampleType<?, ?> that = (DistinctItemsSampleType<?, ?>) o;
			return this.field1.equals(that.field1) && this.field2.equals(that.field2);
		}


		public T getField1() {
			return this.field1;
		}


		public S getField2() {
			return this.field2;
		}
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMapGetValue() {
		Assertions.assertNull(CollectionUtils.getValue(null, null, () -> "Test Value"));
		Assertions.assertEquals("Test Value", CollectionUtils.getValue(new HashMap<String, String>(), null, () -> "Test Value"));

		Map<String, String> testMap = new HashMap<>();
		Assertions.assertEquals("Test Value 1", CollectionUtils.getValue(testMap, "1", () -> "Test Value 1"));
		Assertions.assertEquals("Test Value 2", CollectionUtils.getValue(testMap, "2", () -> "Test Value 2"));
		Assertions.assertNull(CollectionUtils.getValue(testMap, "3", () -> null));

		Assertions.assertEquals("Test Value 1", testMap.get("1"));
		Assertions.assertEquals("Test Value 2", testMap.get("2"));
		Assertions.assertNull(testMap.get("3"));
	}


	@Test
	public void testMapGetValueWithDefaultSupplier() {
		Assertions.assertNull(CollectionUtils.getValue(null, null, () -> "Test Value", () -> "Should Not Be Used"));
		Assertions.assertEquals("Test Value", CollectionUtils.getValue(new HashMap<String, String>(), null, () -> "Test Value", () -> "Should Not Be Used"));

		Map<String, String> testMap = new HashMap<>();
		Assertions.assertEquals("Test Value 1", CollectionUtils.getValue(testMap, "1", () -> "Test Value 1", () -> "Should Not Be Used"));
		Assertions.assertEquals("Test Value 2", CollectionUtils.getValue(testMap, "2", () -> "Test Value 2", () -> "Should Not Be Used"));
		Assertions.assertNull(CollectionUtils.getValue(testMap, "3", () -> null, () -> null));
		Assertions.assertEquals("Test Value 4", CollectionUtils.getValue(testMap, "4", () -> null, () -> "Test Value 4"));

		Assertions.assertEquals("Test Value 1", testMap.get("1"));
		Assertions.assertEquals("Test Value 2", testMap.get("2"));
		Assertions.assertNull(testMap.get("3"));
		Assertions.assertEquals("Test Value 4", testMap.get("4"));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetStreamNonNull() {
		List<Object> sampleList = Arrays.asList(1, 2, 3);
		List<Object> actualList = CollectionUtils.getStream(sampleList).collect(Collectors.toList());
		Assertions.assertEquals(sampleList, actualList);
	}


	@Test
	public void testGetStreamNull() {
		List<Object> sampleList = null;
		List<Object> actualList = CollectionUtils.getStream(sampleList).collect(Collectors.toList());
		Assertions.assertEquals(Collections.emptyList(), actualList);
	}


	@Test
	public void testBuildStreamLazy() {
		/*
		 * Verify that lazy behavior works properly for flattened streams. This requires the application of https://bugs.openjdk.java.net/browse/JDK-8225328 for JDK 8 or
		 * https://bugs.openjdk.java.net/browse/JDK-8075939 for JDK 10 and above. In previous JDK releases, the laziness of intermediate operations would only apply on the top-
		 * level stream if such a stream contains flattened inner streams. This would prevent terminal operations from short-circuiting sub-streams as early as they should. See
		 * <a href="http://stackoverflow.com/questions/29229373/why-filter-after-flatmap-is-not-completely-lazy-in-java-streams?rq=1">this thread</a> for more information.
		 */
		List<Integer> sampleList1 = Arrays.asList(1, 2, 3);
		List<Integer> sampleList2 = Arrays.asList(4, 5, 6);
		List<Integer> sampleList3 = Arrays.asList(7, 8, 9);
		List<Integer> actualList = new ArrayList<>();
		Optional<Integer> result = CollectionUtils.buildStream(sampleList1, sampleList2, sampleList3)
				.peek(actualList::add)
				.filter(i -> i > 3)
				.findAny();

		// Assert terminal result
		Assertions.assertTrue(result.isPresent());
		Assertions.assertEquals(Integer.valueOf(4), result.get());

		// Assert visited elements
		Assertions.assertEquals(Arrays.asList(1, 2, 3, 4), actualList);
	}


	@Test
	public void testBuildStreamNonNull() {
		List<Object> sampleList1 = Arrays.asList(1, 2, 3);
		List<Object> sampleList2 = Arrays.asList(4, 5, 6);
		List<Object> expectedList = new ArrayList<>();
		expectedList.addAll(sampleList1);
		expectedList.addAll(sampleList2);
		List<Object> actualList = CollectionUtils.buildStream(sampleList1, sampleList2).collect(Collectors.toList());
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testBuildStreamNull() {
		List<Object> sampleList = null;
		List<Object> actualList = CollectionUtils.buildStream(sampleList).collect(Collectors.toList());
		Assertions.assertEquals(Collections.emptyList(), actualList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testCombineCollectionsNull() {
		List<Integer> firstList = null;
		List<Integer> secondList = null;
		List<Integer> expectedList = Collections.emptyList();
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testCombineCollectionsEmpty() {
		List<Integer> firstList = Collections.emptyList();
		List<Integer> secondList = Collections.emptyList();
		List<Integer> expectedList = Collections.emptyList();
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testCombineCollectionsFirstNull() {
		List<Integer> firstList = null;
		List<Integer> secondList = Arrays.asList(4, 5, 6);
		List<Integer> expectedList = Arrays.asList(4, 5, 6);
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testCombineCollectionsFirstEmpty() {
		List<Integer> firstList = Collections.emptyList();
		List<Integer> secondList = Arrays.asList(4, 5, 6);
		List<Integer> expectedList = Arrays.asList(4, 5, 6);
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testCombineCollectionsSecondNull() {
		List<Integer> firstList = Arrays.asList(1, 2, 3);
		List<Integer> secondList = null;
		List<Integer> expectedList = Arrays.asList(1, 2, 3);
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testCombineCollectionsSecondEmpty() {
		List<Integer> firstList = Arrays.asList(1, 2, 3);
		List<Integer> secondList = Collections.emptyList();
		List<Integer> expectedList = Arrays.asList(1, 2, 3);
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testCombineCollectionsNonEmpty() {
		List<Integer> firstList = Arrays.asList(1, 2, 3);
		List<Integer> secondList = Arrays.asList(4, 5, 6);
		List<Integer> expectedList = Arrays.asList(1, 2, 3, 4, 5, 6);
		List<Integer> actualList = CollectionUtils.combineCollections(firstList, secondList);
		Assertions.assertEquals(expectedList, actualList);
	}

	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testToArray() {
		List<Integer> list = null;
		Assertions.assertNull(CollectionUtils.toArrayOrNull(list, Integer.class));
		Assertions.assertNotNull(CollectionUtils.toArray(list, Integer.class));
		Assertions.assertEquals(0, CollectionUtils.toArray(list, Integer.class).length);

		list = new ArrayList<>();
		Assertions.assertNull(CollectionUtils.toArrayOrNull(list, Integer.class));
		Assertions.assertNotNull(CollectionUtils.toArray(list, Integer.class));
		Assertions.assertEquals(0, CollectionUtils.toArray(list, Integer.class).length);

		list = CollectionUtils.createList(1, 2, 3);
		Assertions.assertNotNull(CollectionUtils.toArray(list, Integer.class));
		Assertions.assertEquals(3, CollectionUtils.toArray(list, Integer.class).length);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testGetIntersectionEmpty() {
		List<Integer> expectedResult = Collections.emptyList();
		List<Integer> actualResult = CollectionUtils.getIntersection();
		Assertions.assertEquals(expectedResult, actualResult);
	}


	@Test
	public void testGetIntersectionSingle() {
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3)));
	}


	@Test
	public void testGetIntersectionEmptyFirst() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getIntersection(Collections.emptyList(), Arrays.asList(1, 2, 3), Arrays.asList(1, 2, 3)));
	}


	@Test
	public void testGetIntersectionEmptySecond() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3), Collections.emptyList(), Arrays.asList(1, 2, 3)));
	}


	@Test
	public void testGetIntersectionEmptyThird() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3), Arrays.asList(1, 2, 3), Collections.emptyList()));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3), Arrays.asList(1, 2, 3), null));
	}


	@Test
	public void testGetIntersectionEmptyAll() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getIntersection(Collections.emptyList(), Collections.emptyList(), Collections.emptyList()));
	}


	@Test
	public void testGetIntersectionSingleResult() {
		Assertions.assertEquals(Collections.singletonList(3), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3), Arrays.asList(2, 3, 4), Arrays.asList(3, 4, 5)));
	}


	@Test
	public void testGetIntersectionSomeResults() {
		Assertions.assertEquals(Arrays.asList(3, 4, 5), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(2, 3, 4, 5, 6), Arrays.asList(3, 4, 5, 6, 7)));
	}


	@Test
	public void testGetIntersectionAllResults() {
		Assertions.assertEquals(Arrays.asList(1, 2, 3, 4, 5), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(1, 2, 3, 4, 5)));
	}


	@Test
	public void testGetIntersectionOrder() {
		Assertions.assertEquals(Arrays.asList(1, 2, 3, 4, 5), CollectionUtils.getIntersection(Arrays.asList(1, 2, 3, 4, 5), Arrays.asList(5, 4, 3, 2, 1), Arrays.asList(3, 1, 4, 2, 5)));
	}


	@Test
	public void testGetIntersectionDuplicates() {
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.getIntersection(Arrays.asList(0, 0, 1, 2, 3, 3, 4), Arrays.asList(1, 2, 3, 3)));
		Assertions.assertEquals(Arrays.asList(1), CollectionUtils.getIntersection(Arrays.asList(1, 2), Arrays.asList(1, 1, 1)));
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.getIntersection(Arrays.asList(1, 1), Arrays.asList(2, 2, 2)));
		Assertions.assertEquals(Arrays.asList(1), CollectionUtils.getIntersection(Arrays.asList(1, 1), Arrays.asList(1, 1, 1)));
	}


	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	@Test
	public void testCreateSorted() {
		// Natural ordering
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.createSorted(Arrays.<Integer>asList()));
		Assertions.assertEquals(Arrays.asList(1), CollectionUtils.createSorted(Arrays.asList(1)));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.createSorted(Arrays.asList(1, 2, 3)));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.createSorted(Arrays.asList(3, 2, 1)));
		Assertions.assertEquals(Arrays.asList(-1, 1, 2, 3, 4, 8), CollectionUtils.createSorted(Arrays.asList(1, 2, -1, 4, 3, 8)));
		Assertions.assertEquals(Arrays.asList(1, 1, 2, 2, 3, 4, 4), CollectionUtils.createSorted(Arrays.asList(1, 2, 4, 3, 4, 1, 2)));

		// Custom comparator
		Comparator<String> comparator = Comparator.comparingInt(String::length);
		Assertions.assertEquals(Arrays.asList(), CollectionUtils.createSorted(Arrays.asList(), comparator));
		Assertions.assertEquals(Arrays.asList("9"), CollectionUtils.createSorted(Arrays.asList("9"), comparator));
		Assertions.assertEquals(Arrays.asList("9", "89", "789", "6789"), CollectionUtils.createSorted(Arrays.asList("9", "89", "789", "6789"), comparator));
		Assertions.assertEquals(Arrays.asList("9", "89", "789", "6789"), CollectionUtils.createSorted(Arrays.asList("789", "6789", "89", "9"), comparator));
		Assertions.assertEquals(Arrays.asList("", "9", "89", "789", "6789", "123456789"), CollectionUtils.createSorted(Arrays.asList("9", "89", "", "6789", "789", "123456789"), comparator));
		Assertions.assertEquals(Arrays.asList("", "", "9", "9", "89", "6789", "6789"), CollectionUtils.createSorted(Arrays.asList("", "9", "6789", "89", "6789", "", "9"), comparator));
	}


	@Test
	public void testGetMapped() {
		Assertions.assertEquals(Collections.emptyMap(), CollectionUtils.getMapped(null, Object::toString));
		Assertions.assertEquals(Collections.emptyMap(), CollectionUtils.getMapped(Collections.emptyList(), Object::toString));
		{
			Map<Object, Object> expected = new HashMap<>();
			expected.put("abc", "ABC");
			expected.put("def", "DEF");
			expected.put("hij", "HIJ");
			Map<String, String> actual = CollectionUtils.getMapped(Arrays.asList("abc", "def", "hij"), String::toUpperCase);
			Assertions.assertEquals(expected, actual);
		}
		{
			Map<Object, Object> expected = new HashMap<>();
			expected.put(1, -1);
			expected.put(2, -2);
			expected.put(3, -3);
			Map<Integer, Integer> actual = CollectionUtils.getMapped(Arrays.asList(1, 2, 3), val -> -val);
			Assertions.assertEquals(expected, actual);
		}
	}


	@Test
	public void testGetFiltered() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getFiltered(null, val -> true));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getFiltered(Collections.emptyList(), val -> true));
		Assertions.assertEquals(Arrays.asList('A', 'C'), CollectionUtils.getFiltered(Arrays.asList('A', 'b', 'C', 'd'), Character::isUpperCase));
		Assertions.assertEquals(Arrays.asList(-3, -2, -1), CollectionUtils.getFiltered(Arrays.asList(-3, 3, -2, 2, -1, 1), val -> val < 0));
	}


	@Test
	public void testGetConverted() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getConverted(null, Object::toString));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getConverted(Collections.emptyList(), Object::toString));
		Assertions.assertEquals(Arrays.asList("ABC", "DEF", "HIJ"), CollectionUtils.getConverted(Arrays.asList("abc", "def", "hij"), String::toUpperCase));
		Assertions.assertEquals(Arrays.asList(-1, -2, -3), CollectionUtils.getConverted(Arrays.asList(1, 2, 3), val -> -val));
	}


	@Test
	public void testGetFlattened() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getFlattened(null));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getFlattened(Collections.emptyList()));
		Assertions.assertEquals(Arrays.asList(1), CollectionUtils.getFlattened(Arrays.asList(Arrays.asList(1))));
		Assertions.assertEquals(Arrays.asList(1, 2), CollectionUtils.getFlattened(Arrays.asList(Arrays.asList(1, 2))));
		Assertions.assertEquals(Arrays.asList(1, 2), CollectionUtils.getFlattened(Arrays.asList(Arrays.asList(1), Arrays.asList(2))));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), CollectionUtils.getFlattened(Arrays.asList(Arrays.asList(1), Arrays.asList(2, 3))));
		Assertions.assertEquals(Arrays.asList(1, 2, Arrays.asList(3, 4)), CollectionUtils.getFlattened(Arrays.asList(Arrays.asList(1), Arrays.asList(2, Arrays.asList(3, 4)))));
	}


	@Test
	public void testGetReduced() {
		final Object identity = new Object();
		final BiFunction<Object, Object, Object> returnFirstFn = (a, b) -> a;
		final BiFunction<Object, Object, Object> returnSecondFn = (a, b) -> b;
		final BiFunction<Object, Object, Object> returnNullFn = (a, b) -> null;
		final BiFunction<Integer, Integer, Integer> addFn = Integer::sum;
		Assertions.assertNull(CollectionUtils.getReduced(null, null, null));
		Assertions.assertNull(CollectionUtils.getReduced(null, null, returnFirstFn));
		Assertions.assertNull(CollectionUtils.getReduced(Collections.emptyList(), null, null));
		Assertions.assertNull(CollectionUtils.getReduced(Collections.emptyList(), null, returnFirstFn));
		Assertions.assertNull(CollectionUtils.getReduced(Arrays.asList(1, 2, 3), null, returnFirstFn));
		Assertions.assertNull(CollectionUtils.getReduced(Arrays.asList(1, 2, 3), identity, returnNullFn));
		Assertions.assertEquals(identity, CollectionUtils.getReduced(null, identity, null));
		Assertions.assertEquals(identity, CollectionUtils.getReduced(null, identity, returnFirstFn));
		Assertions.assertEquals(identity, CollectionUtils.getReduced(Collections.emptyList(), identity, null));
		Assertions.assertEquals(identity, CollectionUtils.getReduced(Collections.emptyList(), identity, returnFirstFn));
		Assertions.assertEquals(identity, CollectionUtils.getReduced(Arrays.asList(1, 2, 3), identity, returnFirstFn));
		Assertions.assertEquals(3, CollectionUtils.getReduced(Arrays.asList(1, 2, 3), identity, returnSecondFn));
		Assertions.assertEquals((Integer) 6, CollectionUtils.getReduced(Arrays.asList(1, 2, 3), 0, addFn));
		Assertions.assertEquals((Integer) 16, CollectionUtils.getReduced(Arrays.asList(1, 2, 3), 10, addFn));
	}


	@Test
	public void testGetConvertedFlattened() {
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getConvertedFlattened(null, val -> Arrays.asList(val, val.toString())));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getConvertedFlattened(Collections.emptyList(), val -> Arrays.asList(val, val.toString())));
		Assertions.assertEquals(Arrays.asList("abc", "ABC", "def", "DEF", "hij", "HIJ"), CollectionUtils.getConvertedFlattened(Arrays.asList("abc", "def", "hij"), val -> Arrays.asList(val, val.toUpperCase())));
		Assertions.assertEquals(Arrays.asList(1, -1, 2, -2, 3, -3), CollectionUtils.getConvertedFlattened(Arrays.asList(1, 2, 3), val -> Arrays.asList(val, -val)));
	}


	@Test
	public void testGetFlattenedConverted() {
		Collection<Collection<String>> results = CollectionUtils.createList(
				CollectionUtils.getFlattenedConverted(Object::toString, true),
				CollectionUtils.getFlattenedConverted(Object::toString, true, (Collection<Object>) null),
				CollectionUtils.getFlattenedConverted(Object::toString, false),
				CollectionUtils.getFlattenedConverted(Object::toString, false, (Collection<Object>) null)
		);
		int i = 0;
		for (Collection<String> result : results) {
			Assertions.assertNotNull(result);
			Assertions.assertTrue(result.isEmpty());
			if (i < 2) {
				Assertions.assertTrue(result instanceof Set);
			}
			else {
				Assertions.assertTrue(result instanceof List);
			}
			i++;
		}

		List<String> expectedList = CollectionUtils.createList("a", "b", "c", "a", "b");
		Set<String> expectedSet = CollectionUtils.createHashSet("a", "b", "c");
		Assertions.assertEquals(expectedSet, CollectionUtils.getFlattenedConverted(Object::toString, true, expectedList));
		Assertions.assertEquals(expectedSet, CollectionUtils.getFlattenedConverted(Object::toString, true, expectedList, expectedSet));

		Assertions.assertEquals(expectedList, CollectionUtils.getFlattenedConverted(Object::toString, false, expectedList));
		Assertions.assertEquals(CollectionUtils.combineCollections(expectedList, expectedSet), CollectionUtils.getFlattenedConverted(Object::toString, false, expectedList, expectedSet));
	}


	@Test
	public void testGetHistogram() {
		Assertions.assertTrue(CollectionUtils.getHistogram(null).isEmpty(), "An empty map was expected.");
		Assertions.assertTrue(CollectionUtils.getHistogram(Collections.emptyList()).isEmpty(), "An empty map was expected.");
		{
			Map<Integer, Long> histogram = CollectionUtils.getHistogram(Collections.singletonList(1));
			Assertions.assertEquals(1, histogram.size());
			Assertions.assertEquals(1, histogram.get(1).longValue());
		}
		{
			Map<Integer, Long> histogram = CollectionUtils.getHistogram(Arrays.asList(1, 1, 1, 2, 2));
			Assertions.assertEquals(2, histogram.size());
			Assertions.assertEquals(3, histogram.get(1).longValue());
			Assertions.assertEquals(2, histogram.get(2).longValue());
		}
		{
			Map<Integer, Long> histogram = CollectionUtils.getHistogram(Arrays.asList(-1, -2, 0, -2, 3, 4, 4));
			Assertions.assertEquals(5, histogram.size());
			Assertions.assertEquals(1, histogram.get(-1).longValue());
			Assertions.assertEquals(2, histogram.get(-2).longValue());
			Assertions.assertEquals(1, histogram.get(0).longValue());
			Assertions.assertEquals(1, histogram.get(3).longValue());
			Assertions.assertEquals(2, histogram.get(4).longValue());
		}
	}


	@Test
	public void testGetPartitioned() {
		{
			Map<Boolean, List<Integer>> partitionMap = CollectionUtils.getPartitioned(null, v -> v > 0);
			Assertions.assertEquals(new HashSet<>(Arrays.asList(true, false)), partitionMap.keySet());
			Assertions.assertEquals(Arrays.asList(), partitionMap.get(true));
			Assertions.assertEquals(Arrays.asList(), partitionMap.get(false));
		}
		{
			Map<Boolean, List<Integer>> partitionMap = CollectionUtils.getPartitioned(Arrays.asList(1), v -> v > 0);
			Assertions.assertEquals(new HashSet<>(Arrays.asList(true, false)), partitionMap.keySet());
			Assertions.assertEquals(Arrays.asList(1), partitionMap.get(true));
			Assertions.assertEquals(Arrays.asList(), partitionMap.get(false));
		}
		{
			Map<Boolean, List<Integer>> partitionMap = CollectionUtils.getPartitioned(Arrays.asList(-1), v -> v > 0);
			Assertions.assertEquals(new HashSet<>(Arrays.asList(true, false)), partitionMap.keySet());
			Assertions.assertEquals(Arrays.asList(), partitionMap.get(true));
			Assertions.assertEquals(Arrays.asList(-1), partitionMap.get(false));
		}
		{
			Map<Boolean, List<Integer>> partitionMap = CollectionUtils.getPartitioned(Arrays.asList(-1, 1), v -> v > 0);
			Assertions.assertEquals(new HashSet<>(Arrays.asList(true, false)), partitionMap.keySet());
			Assertions.assertEquals(Arrays.asList(1), partitionMap.get(true));
			Assertions.assertEquals(Arrays.asList(-1), partitionMap.get(false));
		}
		{
			Map<Boolean, List<Integer>> partitionMap = CollectionUtils.getPartitioned(Arrays.asList(-1, 1, -2, 3, -2, 0), v -> v > 0);
			Assertions.assertEquals(new HashSet<>(Arrays.asList(true, false)), partitionMap.keySet());
			Assertions.assertEquals(Arrays.asList(1, 3), partitionMap.get(true));
			Assertions.assertEquals(Arrays.asList(-1, -2, -2, 0), partitionMap.get(false));
		}
	}


	@Test
	public void testGetChain() {
		// Generates a chain function which iterates from the starting value to the given limit value
		IntFunction<UnaryOperator<Integer>> createChainFn = limit -> (value -> (value < limit) ? (value + 1) : null);
		Assertions.assertEquals(Arrays.asList(1, 2, 3, 4, 5), CollectionUtils.getChain(1, createChainFn.apply(5)));
		Assertions.assertEquals(Collections.emptyList(), CollectionUtils.getChain(null, createChainFn.apply(5)));
		Assertions.assertEquals(Collections.singletonList(1), CollectionUtils.getChain(1, createChainFn.apply(1)));
		Assertions.assertEquals(Arrays.asList(-2, -1, 0, 1, 2), CollectionUtils.getChain(-2, createChainFn.apply(2)));
	}


	@Test
	public void testGetChainFailure() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> CollectionUtils.getChain(1, value -> (value + 1) % 5));
		Assertions.assertEquals(String.format("Recursion detected during chain traversal. Discovered items: %s. Repeated item: [%s].", Arrays.asList(1, 2, 3, 4, 0), 1), runtimeException.getMessage());
	}


	@Test
	public void testAnyMatchInChain() {
		// Generates a chain function which iterates from the starting value to the given limit value
		IntFunction<UnaryOperator<Integer>> createChainFn = limit -> (value -> (value < limit) ? (value + 1) : null);
		Predicate<Integer> matchesTwo = Integer.valueOf(2)::equals;
		Assertions.assertTrue(CollectionUtils.anyMatchInChain(1, createChainFn.apply(5), matchesTwo));
		Assertions.assertFalse(CollectionUtils.anyMatchInChain(null, createChainFn.apply(5), matchesTwo));
		Assertions.assertFalse(CollectionUtils.anyMatchInChain(1, createChainFn.apply(1), matchesTwo));
		Assertions.assertTrue(CollectionUtils.anyMatchInChain(-2, createChainFn.apply(2), matchesTwo));
	}


	@Test
	public void testAnyMatchInChainFailure() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> CollectionUtils.anyMatchInChain(1, value -> (value + 1) % 5, Integer.valueOf(6)::equals));
		Assertions.assertEquals("Recursion detected during chain traversal. Discovered items: [0, 1, 2, 3, 4]. Repeated item: [1]", runtimeException.getMessage());
	}


	@Test
	public void testCreateList() {
		Integer[] integers = {};
		Assertions.assertEquals(CollectionUtils.createList(integers), Collections.emptyList());
		Assertions.assertEquals(CollectionUtils.createList(1, 2, 3), Arrays.asList(1, 2, 3));
		Assertions.assertEquals(CollectionUtils.createList(1), Arrays.asList(1));
		Assertions.assertEquals(CollectionUtils.createList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6)), Arrays.asList(Arrays.asList(1, 2, 3), Arrays.asList(4, 5, 6)));
		Assertions.assertEquals(CollectionUtils.createList((Object[]) null), Collections.emptyList());
	}


	@Test
	public void testGetAsList() {
		Integer[] integers = {1, 2, 3};
		Assertions.assertEquals(CollectionUtils.getAsList(integers), Arrays.asList(1, 2, 3));
		Assertions.assertEquals(CollectionUtils.getAsList(Arrays.asList(1, 2, 3)), Arrays.asList(1, 2, 3));
		Assertions.assertEquals(CollectionUtils.createList(integers, Arrays.asList(4, 5, 6)), Arrays.asList(integers, Arrays.asList(4, 5, 6)));
		Assertions.assertEquals(CollectionUtils.getAsList(1), Arrays.asList(1));
		Assertions.assertNull(CollectionUtils.getAsList(null));
	}
}
