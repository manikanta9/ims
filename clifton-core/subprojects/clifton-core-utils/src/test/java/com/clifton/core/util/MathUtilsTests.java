package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.DoubleAccumulator;
import java.util.concurrent.atomic.LongAccumulator;


/**
 * he <code>MathUtilsTests</code> class tests the {@link MathUtils}.
 *
 * @author AbhinayaM
 */
public class MathUtilsTests {

	@Test
	public void testAbsoluteDiff() {
		Assertions.assertEquals(BigDecimal.ONE, MathUtils.absoluteDiff(BigDecimal.ONE, null));
		Assertions.assertEquals(BigDecimal.ONE, MathUtils.absoluteDiff(null, BigDecimal.ONE));
		Assertions.assertEquals(MathUtils.absoluteDiff(BigDecimal.ONE, BigDecimal.TEN), new BigDecimal(9));
		Assertions.assertEquals(MathUtils.absoluteDiff(BigDecimal.TEN, BigDecimal.ONE), new BigDecimal(9));
	}


	@Test
	public void testPow() {
		Assertions.assertEquals(new BigDecimal("2.8284271247"), MathUtils.pow(new BigDecimal(2), BigDecimal.valueOf(1.5)).setScale(10, RoundingMode.HALF_UP));
	}


	@Test
	public void testIsNullOrZero() {
		Assertions.assertTrue(MathUtils.isNullOrZero(null));
		Assertions.assertTrue(MathUtils.isNullOrZero(0));
		Assertions.assertTrue(MathUtils.isNullOrZero(0.0));
		Assertions.assertTrue(MathUtils.isNullOrZero(0.0000));

		Assertions.assertFalse(MathUtils.isNullOrZero(0.00000000001));
		Assertions.assertFalse(MathUtils.isNullOrZero(-0.00001));
		Assertions.assertFalse(MathUtils.isNullOrZero(100.1));
	}


	@Test
	public void testIsNullOrZeroWithScale() {
		BigDecimal value = null;
		Assertions.assertTrue(MathUtils.isNullOrZeroWithScale(null, 5));
		Assertions.assertTrue(MathUtils.isNullOrZeroWithScale(BigDecimal.ZERO, 5));
		Assertions.assertTrue(MathUtils.isNullOrZeroWithScale(BigDecimal.valueOf(0.00), 1));
		Assertions.assertTrue(MathUtils.isNullOrZeroWithScale(BigDecimal.valueOf(0.0001), 2));
		Assertions.assertTrue(MathUtils.isNullOrZeroWithScale(BigDecimal.valueOf(0.0000), 7));

		Assertions.assertFalse(MathUtils.isNullOrZeroWithScale(BigDecimal.valueOf(0.0001), 4));
		Assertions.assertFalse(MathUtils.isNullOrZeroWithScale(BigDecimal.valueOf(0.0007), 3));
		Assertions.assertFalse(MathUtils.isNullOrZeroWithScale(BigDecimal.valueOf(-0.0007), 3));
	}


	@Test
	public void testIsZeroOrOne() {
		Assertions.assertTrue(MathUtils.isZeroOrOne(0));
		Assertions.assertTrue(MathUtils.isZeroOrOne(0.0));
		Assertions.assertTrue(MathUtils.isZeroOrOne(1));
		Assertions.assertTrue(MathUtils.isZeroOrOne(1.0));

		Assertions.assertFalse(MathUtils.isZeroOrOne(null));
		Assertions.assertFalse(MathUtils.isZeroOrOne(-5));
		Assertions.assertFalse(MathUtils.isZeroOrOne(100.1));
	}


	@Test
	public void testIsBetween() {
		Assertions.assertTrue(MathUtils.isBetween(2, 2, 2));
		Assertions.assertFalse(MathUtils.isBetween(1, 2, 3));

		Assertions.assertTrue(MathUtils.isBetween(5, 2, null));
		Assertions.assertTrue(MathUtils.isBetween(2, 2, null));
		Assertions.assertFalse(MathUtils.isBetween(1, 2, null));

		Assertions.assertTrue(MathUtils.isBetween(5, null, 5));
		Assertions.assertTrue(MathUtils.isBetween(2, null, 5));
		Assertions.assertFalse(MathUtils.isBetween(10, null, 5));

		Assertions.assertTrue(MathUtils.isBetween(5, null, null));

		Assertions.assertTrue(MathUtils.isBetween(0, null, 10));
	}


	@Test
	public void testIsBetweenAndNotEqualEnd() {
		Assertions.assertFalse(MathUtils.isBetweenAndNotEqualEnd(2, 2, 2));
		Assertions.assertFalse(MathUtils.isBetweenAndNotEqualEnd(1, 2, 3));
		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(2, 2, 4));
		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(3, 2, 4));
		Assertions.assertFalse(MathUtils.isBetweenAndNotEqualEnd(4, 2, 4));

		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(5, 2, null));
		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(2, 2, null));
		Assertions.assertFalse(MathUtils.isBetweenAndNotEqualEnd(1, 2, null));

		Assertions.assertFalse(MathUtils.isBetweenAndNotEqualEnd(5, null, 5));
		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(2, null, 5));
		Assertions.assertFalse(MathUtils.isBetweenAndNotEqualEnd(10, null, 5));

		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(5, null, null));

		Assertions.assertTrue(MathUtils.isBetweenAndNotEqualEnd(0, null, 10));
	}


	@Test
	public void testIntegerCompares() {
		Integer n1 = null;
		Integer n2 = null;

		Assertions.assertTrue(MathUtils.isEqual(n1, n2));
		Assertions.assertTrue(MathUtils.isEqual(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		n1 = 0;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertTrue(MathUtils.isLessThan(n2, n1));

		n1 = -5;
		n2 = 5;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertTrue(MathUtils.isGreaterThan(n2, n1));

		n2 = -5;
		Assertions.assertTrue(MathUtils.isEqual(n1, n2));
		Assertions.assertTrue(MathUtils.isEqual(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		n2 = null;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertTrue(MathUtils.isLessThan(n2, n1));
	}


	@Test
	public void testIntegerShortCompares() {
		Integer n1 = new Integer("1");
		Short s1 = new Short("1");

		Assertions.assertTrue(MathUtils.isEqual(n1, s1));
		Assertions.assertTrue(MathUtils.isEqual(s1, n1));

		Short s2 = new Short("2");
		Assertions.assertTrue(!MathUtils.isEqual(n1, s2));
		Assertions.assertTrue(!MathUtils.isEqual(s2, n1));
	}


	@Test
	public void testDoubleCompares() {
		Double n1 = null;
		Double n2 = null;

		Assertions.assertTrue(MathUtils.isEqual(n1, n2));
		Assertions.assertTrue(MathUtils.isEqual(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		n1 = 0.0;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertTrue(MathUtils.isLessThan(n2, n1));

		n1 = -5.35;
		n2 = 5.0;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertTrue(MathUtils.isGreaterThan(n2, n1));

		n2 = -5.35;
		Assertions.assertTrue(MathUtils.isEqual(n1, n2));
		Assertions.assertTrue(MathUtils.isEqual(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		n2 = null;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertFalse(MathUtils.isGreaterThan(n2, n1));

		Assertions.assertFalse(MathUtils.isLessThan(n1, n2));
		Assertions.assertTrue(MathUtils.isLessThan(n2, n1));

		n1 = -5.35;
		n2 = -5.30;
		Assertions.assertFalse(MathUtils.isEqual(n1, n2));
		Assertions.assertFalse(MathUtils.isEqual(n2, n1));

		Assertions.assertTrue(MathUtils.isLessThan(n1, n2));
		Assertions.assertFalse(MathUtils.isLessThan(n2, n1));

		Assertions.assertFalse(MathUtils.isGreaterThan(n1, n2));
		Assertions.assertTrue(MathUtils.isGreaterThan(n2, n1));
	}


	@Test
	public void testBigDecimalCompares() {
		BigDecimal b1 = BigDecimal.valueOf(10);
		BigDecimal b2 = BigDecimal.valueOf(10);
		Assertions.assertTrue(MathUtils.isEqual(b1, b2));
		Assertions.assertTrue(MathUtils.isEqual(b2, b1));

		Assertions.assertFalse(MathUtils.isLessThan(b1, b2));
		Assertions.assertFalse(MathUtils.isLessThan(b2, b1));

		Assertions.assertFalse(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertFalse(MathUtils.isGreaterThan(b2, b1));

		b2 = BigDecimal.valueOf(10.0);
		Assertions.assertTrue(MathUtils.isEqual(b1, b2));
		Assertions.assertTrue(MathUtils.isEqual(b2, b1));

		Assertions.assertFalse(MathUtils.isLessThan(b1, b2));
		Assertions.assertFalse(MathUtils.isLessThan(b2, b1));

		Assertions.assertFalse(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertFalse(MathUtils.isGreaterThan(b2, b1));

		b1 = null;
		b2 = null;
		Assertions.assertTrue(MathUtils.isEqual(b1, b2));
		Assertions.assertTrue(MathUtils.isEqual(b2, b1));

		Assertions.assertFalse(MathUtils.isLessThan(b1, b2));
		Assertions.assertFalse(MathUtils.isLessThan(b2, b1));

		Assertions.assertFalse(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertFalse(MathUtils.isGreaterThan(b2, b1));

		b1 = BigDecimal.ZERO;
		Assertions.assertFalse(MathUtils.isEqual(b1, b2));
		Assertions.assertFalse(MathUtils.isEqual(b2, b1));

		Assertions.assertTrue(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertFalse(MathUtils.isGreaterThan(b2, b1));

		Assertions.assertFalse(MathUtils.isLessThan(b1, b2));
		Assertions.assertTrue(MathUtils.isLessThan(b2, b1));

		b1 = BigDecimal.valueOf(-5.35);
		b2 = BigDecimal.valueOf(5.0);
		Assertions.assertFalse(MathUtils.isEqual(b1, b2));
		Assertions.assertFalse(MathUtils.isEqual(b2, b1));

		Assertions.assertTrue(MathUtils.isLessThan(b1, b2));
		Assertions.assertFalse(MathUtils.isLessThan(b2, b1));

		Assertions.assertFalse(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertTrue(MathUtils.isGreaterThan(b2, b1));

		b2 = BigDecimal.valueOf(-5.35);
		Assertions.assertTrue(MathUtils.isEqual(b1, b2));
		Assertions.assertTrue(MathUtils.isEqual(b2, b1));

		Assertions.assertFalse(MathUtils.isLessThan(b1, b2));
		Assertions.assertFalse(MathUtils.isLessThan(b2, b1));

		Assertions.assertFalse(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertFalse(MathUtils.isGreaterThan(b2, b1));

		b2 = null;
		Assertions.assertFalse(MathUtils.isEqual(b1, b2));
		Assertions.assertFalse(MathUtils.isEqual(b2, b1));

		Assertions.assertTrue(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertFalse(MathUtils.isGreaterThan(b2, b1));

		Assertions.assertFalse(MathUtils.isLessThan(b1, b2));
		Assertions.assertTrue(MathUtils.isLessThan(b2, b1));

		b1 = BigDecimal.valueOf(-5.35);
		b2 = BigDecimal.valueOf(-5.30);
		Assertions.assertFalse(MathUtils.isEqual(b1, b2));
		Assertions.assertFalse(MathUtils.isEqual(b2, b1));

		Assertions.assertTrue(MathUtils.isLessThan(b1, b2));
		Assertions.assertFalse(MathUtils.isLessThan(b2, b1));

		Assertions.assertFalse(MathUtils.isGreaterThan(b1, b2));
		Assertions.assertTrue(MathUtils.isGreaterThan(b2, b1));
	}


	@Test
	public void testGetNumberAsBigDecimal() {
		int intVal = 6;
		long longVal = 6L;
		double doubleVal = 6.0;
		float floatVal = 6.0f;
		short shortVal = 6;
		Assertions.assertEquals(null, MathUtils.getNumberAsBigDecimal(null));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(intVal));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(longVal));
		Assertions.assertEquals(new BigDecimal("6.0"), MathUtils.getNumberAsBigDecimal(doubleVal));
		Assertions.assertEquals(new BigDecimal("6.0"), MathUtils.getNumberAsBigDecimal(floatVal));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(shortVal));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new Integer("6")));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new Long("6")));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new LongAccumulator(Long::sum, 6L)));
		// Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new LongAdder()));
		Assertions.assertEquals(new BigDecimal("6.0"), MathUtils.getNumberAsBigDecimal(new Double("6.0")));
		Assertions.assertEquals(new BigDecimal("6.0"), MathUtils.getNumberAsBigDecimal(new DoubleAccumulator(Double::sum, 6.0)));
		// Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new DoubleAdder()));
		Assertions.assertEquals(new BigDecimal("6.0"), MathUtils.getNumberAsBigDecimal(new Float("6.0")));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new Short("6")));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new BigInteger("6")));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new BigDecimal(doubleVal)));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new AtomicInteger(intVal)));
		Assertions.assertEquals(new BigDecimal(6), MathUtils.getNumberAsBigDecimal(new AtomicLong(longVal)));
		Assertions.assertEquals(new BigDecimal("6.0"), MathUtils.getNumberAsBigDecimal(new Float("6.0")));
	}


	@Test
	public void testIsSameSignum() {
		Assertions.assertTrue(MathUtils.isSameSignum(null, null));
		Assertions.assertFalse(MathUtils.isSameSignum(BigDecimal.ZERO, null));
		Assertions.assertFalse(MathUtils.isSameSignum(null, BigDecimal.ZERO));
		Assertions.assertTrue(MathUtils.isSameSignum(BigDecimal.ZERO, BigDecimal.ZERO));
		Assertions.assertTrue(MathUtils.isSameSignum(BigDecimal.ONE, BigDecimal.TEN));
		Assertions.assertFalse(MathUtils.isSameSignum(MathUtils.negate(BigDecimal.ONE), BigDecimal.TEN));
	}


	@Test
	public void testRound() {
		Assertions.assertNull(MathUtils.round(null, 2));

		BigDecimal d1 = BigDecimal.valueOf(5.7589453);

		BigDecimal rounded = MathUtils.round(d1, 5, BigDecimal.ROUND_HALF_UP);
		Assertions.assertEquals(BigDecimal.valueOf(5.75895), rounded);

		rounded = MathUtils.round(d1, 5);
		Assertions.assertEquals(BigDecimal.valueOf(5.75895), rounded);

		rounded = MathUtils.round(d1, 6, BigDecimal.ROUND_HALF_UP);
		Assertions.assertEquals(BigDecimal.valueOf(5.758945), rounded);

		rounded = MathUtils.round(d1, 0, BigDecimal.ROUND_DOWN);
		Assertions.assertEquals(BigDecimal.valueOf(5), rounded);

		rounded = MathUtils.round(d1, 0);
		Assertions.assertEquals(BigDecimal.valueOf(6), rounded);

		rounded = MathUtils.round(d1, 0, BigDecimal.ROUND_CEILING);
		Assertions.assertEquals(BigDecimal.valueOf(6), rounded);

		BigDecimal d2 = BigDecimal.valueOf(-5.7589453);

		rounded = MathUtils.round(d2, 5, BigDecimal.ROUND_HALF_UP);
		Assertions.assertEquals(BigDecimal.valueOf(-5.75895), rounded);

		rounded = MathUtils.round(d2, 6, BigDecimal.ROUND_HALF_UP);
		Assertions.assertEquals(BigDecimal.valueOf(-5.758945), rounded);

		rounded = MathUtils.round(d2, 6);
		Assertions.assertEquals(BigDecimal.valueOf(-5.758945), rounded);

		rounded = MathUtils.round(d2, 0, BigDecimal.ROUND_DOWN);
		Assertions.assertEquals(BigDecimal.valueOf(-5), rounded);

		rounded = MathUtils.round(d2, 0, BigDecimal.ROUND_CEILING);
		Assertions.assertEquals(BigDecimal.valueOf(-5), rounded);

		rounded = MathUtils.round(d2, 0);
		Assertions.assertEquals(BigDecimal.valueOf(-6), rounded);
	}


	@Test
	public void testRoundToSmallestPrecision() {
		BigDecimal value = new BigDecimal("100.123456789");

		Assertions.assertEquals(value, MathUtils.roundToSmallestPrecision(value, (testValue) -> false));
		Assertions.assertEquals(new BigDecimal("100"), MathUtils.roundToSmallestPrecision(value, (testValue) -> true));
		Assertions.assertEquals(new BigDecimal("100.1235"), MathUtils.roundToSmallestPrecision(value, (testValue) -> MathUtils.isEqual(new BigDecimal("10012.35"), MathUtils.round(MathUtils.multiply(testValue, new BigDecimal("100")), 2))));
	}


	@Test
	public void testIsScaleViolated() {
		Assertions.assertFalse(MathUtils.isScaleViolated(BigDecimal.valueOf(11000), 2));
		Assertions.assertFalse(MathUtils.isScaleViolated(BigDecimal.valueOf(1.00), 2));
		Assertions.assertFalse(MathUtils.isScaleViolated(BigDecimal.valueOf(1.20), 2));
		Assertions.assertFalse(MathUtils.isScaleViolated(BigDecimal.valueOf(1.23), 2));
		Assertions.assertFalse(MathUtils.isScaleViolated(BigDecimal.valueOf(123), 0));

		Assertions.assertTrue(MathUtils.isScaleViolated(BigDecimal.valueOf(1.001), 0));
		Assertions.assertTrue(MathUtils.isScaleViolated(BigDecimal.valueOf(1.23), 1));
		Assertions.assertTrue(MathUtils.isScaleViolated(BigDecimal.valueOf(1.234), 2));
		Assertions.assertTrue(MathUtils.isScaleViolated(new BigDecimal("1.23000000000000001"), 2));
		Assertions.assertTrue(MathUtils.isScaleViolated(new BigDecimal("1.29999999999999999"), 2));
	}


	@Test
	public void testAdd() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal result = MathUtils.add(d1, d2);
		Assertions.assertNull(result);

		d1 = BigDecimal.valueOf(5.35);
		result = MathUtils.add(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(5.35), result);

		result = MathUtils.add(d2, d1);
		Assertions.assertEquals(BigDecimal.valueOf(5.35), result);

		d2 = BigDecimal.valueOf(13.789);
		result = MathUtils.add(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(19.139), result);

		double value = -12.13;
		result = MathUtils.add(d2, value);
		Assertions.assertEquals(BigDecimal.valueOf(1.659), result);

		result = MathUtils.add(d1, value);
		Assertions.assertEquals(BigDecimal.valueOf(-6.78), result);
	}


	@Test
	public void testSubtract() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal result = MathUtils.subtract(d1, d2);
		Assertions.assertNull(result);

		d1 = BigDecimal.valueOf(5.35);
		result = MathUtils.subtract(d1, d2);
		Assertions.assertEquals(d1, result);

		result = MathUtils.subtract(d2, d1);
		Assertions.assertEquals(d1.negate(), result);

		d2 = BigDecimal.valueOf(13.789);
		result = MathUtils.subtract(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(-8.439), result);

		double value = -12.13;
		result = MathUtils.subtract(d2, value);
		Assertions.assertEquals(BigDecimal.valueOf(25.919), result);

		result = MathUtils.subtract(d1, value);
		Assertions.assertEquals(BigDecimal.valueOf(17.48), result);
	}


	@Test
	public void testMaxPair() {
		Assertions.assertNull(MathUtils.max(null, null));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.max(null, new BigDecimal("0")));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.max(new BigDecimal("0"), null));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.max(new BigDecimal("-5.39"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.max(new BigDecimal("5.39"), new BigDecimal("-5.39")));
		Assertions.assertEquals(new BigDecimal("6"), MathUtils.max(new BigDecimal("6"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("6"), MathUtils.max(new BigDecimal("5.39"), new BigDecimal("6")));
	}


	@Test
	public void testMaxVarargs() {
		Assertions.assertNull(MathUtils.max());
		Assertions.assertEquals(null, MathUtils.max((Number) null));
		Assertions.assertEquals(null, MathUtils.max((Number) null, null));
		Assertions.assertNull(MathUtils.max(null, null, null));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.max((Number) null, new BigDecimal("0")));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.max(null, BigDecimal.ZERO, -1));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.max((Number) new BigDecimal("0"), null));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.max(new BigDecimal("0"), null, BigDecimal.TEN.negate()));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.max((Number) new BigDecimal("-5.39"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.max(new BigDecimal("-5.39"), new BigDecimal("5.39"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.max((Number) new BigDecimal("5.39"), new BigDecimal("-5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.max(new BigDecimal("5.39"), new BigDecimal("-5.39"), new BigDecimal("-5.39")));
		Assertions.assertEquals(new BigDecimal("6"), MathUtils.max((Number) new BigDecimal("6"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("6"), MathUtils.max(new BigDecimal("6"), new BigDecimal("5.39"), new BigDecimal("6")));
		Assertions.assertEquals(new BigDecimal("6"), MathUtils.max((Number) new BigDecimal("5.39"), new BigDecimal("6")));
		Assertions.assertEquals(new BigDecimal("6"), MathUtils.max(new BigDecimal("5.39"), new BigDecimal("6"), new BigDecimal("6")));
		Assertions.assertEquals(5, (int) MathUtils.max(0, 5, 1, 2, 3));
		Assertions.assertEquals(-5, (int) MathUtils.max(-10, -9, -7, -5));
		Assertions.assertEquals((short) 10, MathUtils.max((short) 0, (short) 1, (short) 10, 10L));
	}


	@Test
	public void testMinPair() {
		Assertions.assertNull(MathUtils.min(null, null));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.min(null, new BigDecimal("0")));
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.min(new BigDecimal("0"), null));
		Assertions.assertEquals(new BigDecimal("-5.39"), MathUtils.min(new BigDecimal("-5.39"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("-5.39"), MathUtils.min(new BigDecimal("5.39"), new BigDecimal("-5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.min(new BigDecimal("6"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), MathUtils.min(new BigDecimal("5.39"), new BigDecimal("6")));
	}


	@Test
	public void testMultiply() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal result = MathUtils.multiply(d1, d2);
		Assertions.assertNull(result);

		d1 = BigDecimal.ZERO;
		result = MathUtils.multiply(d1, d2);
		Assertions.assertNull(result);
		result = MathUtils.multiply(d2, d1);
		Assertions.assertNull(result);

		d2 = BigDecimal.valueOf(5.12);
		result = MathUtils.multiply(d1, d2);
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.round(result, 0));

		result = MathUtils.multiply(d2, d1);
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.round(result, 0));

		d1 = BigDecimal.ONE;
		result = MathUtils.multiply(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(5.12), MathUtils.round(result, 2));

		result = MathUtils.multiply(d2, d1);
		Assertions.assertEquals(BigDecimal.valueOf(5.12), MathUtils.round(result, 2));

		result = MathUtils.multiply(d1, d2, 2);
		Assertions.assertEquals(BigDecimal.valueOf(5.12), result);

		result = MathUtils.multiply(d2, d2);
		Assertions.assertEquals(BigDecimal.valueOf(26.2144), MathUtils.round(result, 4));

		result = MathUtils.multiply(d2, 3);
		Assertions.assertEquals(BigDecimal.valueOf(15.36), MathUtils.round(result, 2));

		result = MathUtils.multiply(d2, -3);
		Assertions.assertEquals(BigDecimal.valueOf(-15.36), MathUtils.round(result, 2));
	}


	@Test
	public void testMultiply_WithScale() {
		BigDecimal result = MathUtils.multiply(null, null, 2);
		Assertions.assertNull(result);

		result = MathUtils.multiply(new BigDecimal("2.5"), new BigDecimal("1.3"), 1);
		Assertions.assertEquals(new BigDecimal("3.3"), result);
	}


	@Test
	public void testDivideByZero() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			BigDecimal d1 = BigDecimal.TEN;
			BigDecimal d2 = BigDecimal.ZERO;
			MathUtils.divide(d1, d2);
		});
	}


	private void validateDivideByZeroReturnDefaultValue(String d1String, String d2String, String defaultValue, String expectedValue) {
		BigDecimal d1 = new BigDecimal(d1String);
		BigDecimal d2 = new BigDecimal(d2String);
		BigDecimal expectedResult = new BigDecimal(expectedValue);

		BigDecimal defaultDivideByZeroValue = new BigDecimal(defaultValue);
		BigDecimal resultValue = MathUtils.divide(d1, d2, defaultDivideByZeroValue);

		Assertions.assertTrue(MathUtils.isEqual(resultValue, expectedResult));
	}


	@Test
	public void testDivide() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal result = MathUtils.divide(d1, d2);
		Assertions.assertNull(result);

		d1 = BigDecimal.ZERO;
		result = MathUtils.divide(d1, d2);
		Assertions.assertNull(result);
		result = MathUtils.divide(d2, d1);
		Assertions.assertNull(result);

		d2 = BigDecimal.valueOf(5.12);
		result = MathUtils.divide(d1, d2);
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.round(result, 0));

		d1 = BigDecimal.ONE;
		result = MathUtils.divide(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(0.2), MathUtils.round(result, 1));

		result = MathUtils.divide(d2, d1);
		Assertions.assertEquals(BigDecimal.valueOf(5.12), MathUtils.round(result, 2));

		result = MathUtils.divide(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(0.195), MathUtils.round(result, 3));

		result = MathUtils.divide(d2, d2);
		Assertions.assertEquals(BigDecimal.ONE, MathUtils.round(result, 0));

		result = MathUtils.divide(d2, 3);
		Assertions.assertEquals(BigDecimal.valueOf(1.71), MathUtils.round(result, 2));

		result = MathUtils.divide(d2, -3);
		Assertions.assertEquals(BigDecimal.valueOf(-1.71), MathUtils.round(result, 2));
	}


	@Test
	public void testDivideToIntegralValue() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal result = MathUtils.divideToIntegralValue(d1, d2);
		Assertions.assertNull(result);

		d1 = BigDecimal.ZERO;
		result = MathUtils.divideToIntegralValue(d1, d2);
		Assertions.assertNull(result);
		result = MathUtils.divideToIntegralValue(d2, d1);
		Assertions.assertNull(result);

		d1 = new BigDecimal("10");
		d2 = new BigDecimal("7");

		result = MathUtils.divideToIntegralValue(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(1), result);

		result = MathUtils.divideToIntegralValue(d2, d1);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		d2 = new BigDecimal("5");
		result = MathUtils.divideToIntegralValue(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(2), result);
	}


	@Test
	public void testDivideAndRemainder() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal[] result = MathUtils.divideAndRemainder(d1, d2);
		Assertions.assertNull(result);

		d1 = BigDecimal.ZERO;
		result = MathUtils.divideAndRemainder(d1, d2);
		Assertions.assertNull(result);
		result = MathUtils.divideAndRemainder(d2, d1);
		Assertions.assertNull(result);

		d1 = new BigDecimal("10");
		d2 = new BigDecimal("7");

		result = MathUtils.divideAndRemainder(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(1), result[0]);
		Assertions.assertEquals(BigDecimal.valueOf(3), result[1]);

		result = MathUtils.divideAndRemainder(d2, d1);
		Assertions.assertEquals(BigDecimal.ZERO, result[0]);
		Assertions.assertEquals(BigDecimal.valueOf(7), result[1]);

		d2 = new BigDecimal("5");
		result = MathUtils.divideAndRemainder(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(2), result[0]);
		Assertions.assertEquals(BigDecimal.ZERO, result[1]);
	}


	@Test
	public void testNegate() {
		BigDecimal d1 = null;

		BigDecimal result = MathUtils.negate(d1);
		Assertions.assertNull(result);

		d1 = BigDecimal.ZERO;
		result = MathUtils.negate(d1);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		d1 = BigDecimal.ONE;
		result = MathUtils.negate(d1);
		Assertions.assertEquals(BigDecimal.valueOf(-1), result);

		d1 = BigDecimal.valueOf(12.85);
		result = MathUtils.negate(d1);
		Assertions.assertEquals(BigDecimal.valueOf(-12.85), result);

		d1 = BigDecimal.valueOf(-23.78);
		result = MathUtils.negate(d1);
		Assertions.assertEquals(BigDecimal.valueOf(23.78), result);
	}


	@Test
	public void testPercentageOf() {
		BigDecimal percentage = null;
		BigDecimal total = null;

		BigDecimal result = MathUtils.getPercentageOf(percentage, total, false);
		Assertions.assertNull(result);
		result = MathUtils.getPercentageOf(percentage, total, true);
		Assertions.assertNull(result);

		percentage = BigDecimal.ZERO;
		result = MathUtils.getPercentageOf(percentage, total);
		Assertions.assertNull(result);
		result = MathUtils.getPercentageOf(percentage, total, true);
		Assertions.assertNull(result);

		total = BigDecimal.ZERO;
		result = MathUtils.getPercentageOf(percentage, total, false);
		Assertions.assertEquals(BigDecimal.ZERO, result);
		result = MathUtils.getPercentageOf(percentage, total, true);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		percentage = BigDecimal.valueOf(0.33);
		total = BigDecimal.valueOf(75);
		result = MathUtils.getPercentageOf(percentage, total);
		Assertions.assertEquals(BigDecimal.valueOf(25), MathUtils.round(result, 0));

		percentage = BigDecimal.valueOf(33.33);
		result = MathUtils.getPercentageOf(percentage, total, true);
		Assertions.assertEquals(BigDecimal.valueOf(25), MathUtils.round(result, 0));
	}


	@Test
	public void testPercentChange() {
		BigDecimal d1 = null;
		BigDecimal d2 = null;

		BigDecimal result = MathUtils.getPercentChange(d1, d2);
		Assertions.assertNull(result);

		result = MathUtils.getPercentChange(d1, d2, true);
		Assertions.assertNull(result);

		result = MathUtils.getPercentChange(d2, d1, false);
		Assertions.assertNull(result);

		d1 = BigDecimal.valueOf(366.425);
		d2 = BigDecimal.valueOf(428.75);

		result = MathUtils.getPercentChange(d1, d2);
		Assertions.assertEquals(BigDecimal.valueOf(0.1701), MathUtils.round(result, 4));

		result = MathUtils.getPercentChange(d1, d2, true);
		Assertions.assertEquals(BigDecimal.valueOf(17.01), MathUtils.round(result, 2));

		result = MathUtils.getPercentChange(d2, d1, false);
		Assertions.assertEquals(BigDecimal.valueOf(-0.1454), MathUtils.round(result, 4));

		result = MathUtils.getPercentChange(d2, d1, true);
		Assertions.assertEquals(BigDecimal.valueOf(-14.5364), MathUtils.round(result, 4));

		result = MathUtils.getPercentChange(null, BigDecimal.ZERO, true);
		Assertions.assertNull(result);

		result = MathUtils.getPercentChange(BigDecimal.ZERO, null);
		Assertions.assertNull(result);

		result = MathUtils.getPercentChange(BigDecimal.ZERO, BigDecimal.ZERO);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		d1 = BigDecimal.valueOf(5.321);
		d2 = BigDecimal.valueOf(5.321);
		result = MathUtils.getPercentChange(d1, d2, false);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		result = MathUtils.getPercentChange(d2, d1, true);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		d2 = BigDecimal.valueOf(30.123);
		result = MathUtils.getPercentChange(d1, d2, false);
		Assertions.assertEquals(BigDecimal.valueOf(4.66), MathUtils.round(result, 2));

		result = MathUtils.getPercentChange(d2, d1, true);
		Assertions.assertEquals(BigDecimal.valueOf(-82.34), MathUtils.round(result, 2));

		d2 = BigDecimal.valueOf(5);
		result = MathUtils.getPercentChange(d2, d1);
		Assertions.assertEquals(BigDecimal.valueOf(0.0642), MathUtils.round(result, 4));
		result = MathUtils.getPercentChange(d2, d1, true);
		Assertions.assertEquals(BigDecimal.valueOf(6.42), MathUtils.round(result, 2));

		d2 = BigDecimal.valueOf(-5.1);
		result = MathUtils.getPercentChange(d2, d1);
		Assertions.assertEquals(BigDecimal.valueOf(-2.0433), MathUtils.round(result, 4));

		result = MathUtils.getPercentChange(d2, d1, true);
		Assertions.assertEquals(BigDecimal.valueOf(-204.33), MathUtils.round(result, 2));

		result = MathUtils.getPercentChange(null, d1, false);
		Assertions.assertNull(result);
	}


	@Test
	public void testIsInRange() {
		BigDecimal value = null;
		BigDecimal min = null;
		BigDecimal max = null;

		Assertions.assertTrue(MathUtils.isInRange(value, min, max));

		value = BigDecimal.ZERO;
		Assertions.assertTrue(MathUtils.isInRange(value, min, max));

		min = BigDecimal.ZERO;
		Assertions.assertTrue(MathUtils.isInRange(value, min, max));

		max = BigDecimal.ZERO;
		Assertions.assertTrue(MathUtils.isInRange(value, min, max));

		value = BigDecimal.ONE;
		Assertions.assertFalse(MathUtils.isInRange(value, min, max));

		value = BigDecimal.valueOf(35219.15);
		min = BigDecimal.valueOf(-500);
		max = BigDecimal.valueOf(500);
		Assertions.assertFalse(MathUtils.isInRange(value, min, max));

		max = BigDecimal.valueOf(35220.59);
		Assertions.assertTrue(MathUtils.isInRange(value, min, max));

		value = BigDecimal.ZERO;
		Assertions.assertTrue(MathUtils.isInRange(value, min, max));
	}


	@Test
	public void testGetDeAnnualizedRate() {
		BigDecimal annualRate = null;
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.getDeAnnualizedRate(annualRate));

		annualRate = BigDecimal.ZERO;
		Assertions.assertEquals(BigDecimal.ZERO, MathUtils.getDeAnnualizedRate(annualRate));

		annualRate = BigDecimal.valueOf(0.30281);
		Assertions.assertEquals(0.000008, MathUtils.round(MathUtils.getDeAnnualizedRate(annualRate), 6).doubleValue(), 0.0d);
		Assertions.assertEquals(0.00001, MathUtils.round(MathUtils.getDeAnnualizedRate(annualRate), 5).doubleValue(), 0.0d);

		annualRate = BigDecimal.valueOf(0.314);
		Assertions.assertEquals(0.000009, MathUtils.round(MathUtils.getDeAnnualizedRate(annualRate), 6).doubleValue(), 0.0d);
		Assertions.assertEquals(0.00001, MathUtils.round(MathUtils.getDeAnnualizedRate(annualRate), 5).doubleValue(), 0.0d);

		annualRate = BigDecimal.ONE;
		Assertions.assertEquals(0.000028, MathUtils.round(MathUtils.getDeAnnualizedRate(annualRate), 6).doubleValue(), 0.0d);
		Assertions.assertEquals(0.00003, MathUtils.round(MathUtils.getDeAnnualizedRate(annualRate), 5).doubleValue(), 0.0d);
	}


	@Test
	public void testGetDv01Value() {
		BigDecimal value = null;
		Assertions.assertNull(MathUtils.getDV01Value(value), "DV01 of null should be null");

		value = BigDecimal.ZERO;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getDV01Value(value)), "DV01 of 0 should be zero.");

		value = BigDecimal.ONE;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.0001), MathUtils.getDV01Value(value)), "DV01 of 1 should be 0.0001");

		value = BigDecimal.valueOf(5643.2148);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.56432148), MathUtils.getDV01Value(value)), "DV01 of 5643.2148 should be 0.56432148");

		value = BigDecimal.valueOf(994685643.855);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(99468.5643855), MathUtils.getDV01Value(value)), "DV01 of 994685643.855 should be 99468.5643855");
	}


	@Test
	public void testGetValueAfterHaircut() {
		BigDecimal value = null;
		BigDecimal haircut = null;

		Assertions.assertNull(MathUtils.getValueAfterHaircut(value, haircut, false), "Value after haircut should be null");
		Assertions.assertNull(MathUtils.getValueAfterHaircut(value, haircut, true), "Value after haircut should be null");

		haircut = BigDecimal.valueOf(0.3);
		Assertions.assertNull(MathUtils.getValueAfterHaircut(value, haircut, false), "Value after haircut should be null");
		haircut = BigDecimal.valueOf(30);
		Assertions.assertNull(MathUtils.getValueAfterHaircut(value, haircut, true), "Value after haircut should be null");

		haircut = null;
		value = BigDecimal.valueOf(1000);
		Assertions.assertTrue(MathUtils.isEqual(value, MathUtils.getValueAfterHaircut(value, haircut, false)));
		Assertions.assertTrue(MathUtils.isEqual(value, MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(0.01);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(990), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.ONE;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(990), MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(0.3);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(700), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.valueOf(30);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(700), MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(0.99);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(10), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.valueOf(99);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(10), MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(0.05);
		value = BigDecimal.valueOf(1499280);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(1424316), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.valueOf(5);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(1424316), MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.ONE;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = MathUtils.BIG_DECIMAL_ONE_HUNDRED;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(-0.0125);
		value = BigDecimal.valueOf(9833631.81);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(9712228.95), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.valueOf(-1.25);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(9712228.95), MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(0.0125);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(9710711.41), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.valueOf(1.25);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(9710711.41), MathUtils.getValueAfterHaircut(value, haircut, true)));

		haircut = BigDecimal.valueOf(-0.035);
		value = BigDecimal.valueOf(4402303.33);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(4253433.17), MathUtils.getValueAfterHaircut(value, haircut, false)));
		haircut = BigDecimal.valueOf(-3.5);
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(4253433.17), MathUtils.getValueAfterHaircut(value, haircut, true)));
	}


	@Test
	public void testSquareValue() {
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1"), MathUtils.squareValue(new BigDecimal("1"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("4"), MathUtils.squareValue(new BigDecimal("2"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("9"), MathUtils.squareValue(new BigDecimal("3"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("16"), MathUtils.squareValue(new BigDecimal("4"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("25"), MathUtils.squareValue(new BigDecimal("5"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("36"), MathUtils.squareValue(new BigDecimal("6"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("49"), MathUtils.squareValue(new BigDecimal("7"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("64"), MathUtils.squareValue(new BigDecimal("8"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("81"), MathUtils.squareValue(new BigDecimal("9"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("100"), MathUtils.squareValue(new BigDecimal("10"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("1.5625"), MathUtils.squareValue(new BigDecimal("1.25"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("7.5625"), MathUtils.squareValue(new BigDecimal("2.75"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("10.460269566756"), MathUtils.squareValue(new BigDecimal("3.234234"))));
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal("15161.2998013129"), MathUtils.squareValue(new BigDecimal("123.13123"))));
	}


	private void validateSqrt(String value, String expectedResult, int scale) {
		Assertions.assertTrue(MathUtils.isEqual(new BigDecimal(expectedResult), MathUtils.sqrt(new BigDecimal(value), scale)));
	}


	@Test
	public void testSqrtToFiftyDecimals() {
		validateSqrt("0", "0", 50);
		validateSqrt("1", "1.0000000000000000000000000000000000000000000000000", 50);
		validateSqrt("2", "1.4142135623730950488016887242096980785696718753769", 50);
		validateSqrt("3", "1.7320508075688772935274463415058723669428052538104", 50);
		validateSqrt("4", "2.0000000000000000000000000000000000000000000000000", 50);
		validateSqrt("5", "2.2360679774997896964091736687312762354406183596115", 50);
		validateSqrt("6", "2.4494897427831780981972840747058913919659474806567", 50);
		validateSqrt("7", "2.6457513110645905905016157536392604257102591830825", 50);
		validateSqrt("8", "2.8284271247461900976033774484193961571393437507539", 50);
		validateSqrt("9", "3.0000000000000000000000000000000000000000000000000", 50);
		validateSqrt("10", "3.1622776601683793319988935444327185337195551393252", 50);
	}


	private void testModalFeeReturn(double modalFeePercent, double grossReturn, double expectedResult) {
		BigDecimal modalFeeReturn = MathUtils.modelFeeReturn(BigDecimal.valueOf(modalFeePercent), BigDecimal.valueOf(grossReturn));
		Assertions.assertTrue(MathUtils.isEqual(modalFeeReturn, expectedResult), "Modal Fee Return expected [" + expectedResult + "] received [" + modalFeeReturn + "]");
	}


	@Test
	public void testModelFeeReturn() {
		testModalFeeReturn(5, 5.470396, 5.04244009651034487000);
		testModalFeeReturn(5, 0.70998492217, 0.30134482767564721700);
		testModalFeeReturn(5, 7.6034989324783, 7.16688776621040344200);
		testModalFeeReturn(5, 5.1431347737, 4.71650676295646055800);
		testModalFeeReturn(5, 0.421317692726085, 0.01384889225991170900);

		testModalFeeReturn(1.5, 5.470396, 5.33961815800138164700);
		testModalFeeReturn(1.5, 0.70998492217, 0.58510974396516245600);
		testModalFeeReturn(1.5, 7.6034989324783, 7.47007615304841954600);
		testModalFeeReturn(1.5, 5.1431347737, 5.01276271871431118500);
		testModalFeeReturn(1.5, 0.421317692726085, 0.29680044697200259700);

		testModalFeeReturn(2.5, 5.470396, 5.25359083606469071600);
		testModalFeeReturn(2.5, 0.70998492217, 0.50296526908200369600);
		testModalFeeReturn(2.5, 7.6034989324783, 7.38230895774767667500);
		testModalFeeReturn(2.5, 5.1431347737, 4.92700232861764393600);
		testModalFeeReturn(2.5, 0.421317692726085, 0.21489142459136354200);
	}
}
