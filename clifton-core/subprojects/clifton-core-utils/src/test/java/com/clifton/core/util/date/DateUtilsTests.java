package com.clifton.core.util.date;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;


/**
 * The {@link DateUtilsTests} class tests the {@link DateUtils}.
 *
 * @author manderson
 */
public class DateUtilsTests {

	@Test
	public void testIsValidDate() {
		Assertions.assertTrue(DateUtils.isValidDate("01/01/2011"));
		Assertions.assertTrue(DateUtils.isValidDate("5/2/2011"));
		Assertions.assertTrue(DateUtils.isValidDate("2011-01-01 08:41:21"));
		Assertions.assertTrue(DateUtils.isValidDate("2020-12-15 1:51:32"));
		Assertions.assertTrue(DateUtils.isValidDate("01/01/2011 08:41:21"));

		Assertions.assertFalse(DateUtils.isValidDate("01012011"));
		Assertions.assertFalse(DateUtils.isValidDate("2011-01-01"));
		Assertions.assertFalse(DateUtils.isValidDate("20110405"));
	}


	@Test
	public void testToDate() {
		validateToDate("01/01/2011", "01/01/2011", DateUtils.DATE_FORMAT_INPUT);
		validateToDate("5/2/2011", "05/02/2011", DateUtils.DATE_FORMAT_INPUT);
		validateToDate("11/20/2011", "11/20/2011", DateUtils.DATE_FORMAT_INPUT);
		validateToDate("2011-01-01 08:41:21", "2011-01-01 08:41:21", DateUtils.DATE_FORMAT_FULL);
		validateToDate("2020-12-15 1:51:32", "2020-12-15 01:51:32", DateUtils.DATE_FORMAT_FULL);
		validateToDate("01/01/2011 08:41:21", "2011-01-01 00:00:00", DateUtils.DATE_FORMAT_FULL);
		validateToDate("01/01/2011 08:41:21", "01/01/2011", DateUtils.DATE_FORMAT_INPUT);

		validateToDateInvalid("asdf");
		validateToDateInvalid("01.01.2011 08:41:21");
		validateToDateInvalid("15Sep20");
	}


	@Test
	public void testNthWeekdayOfMonth() {
		Date date = DateUtils.toDate("06/10/2012");

		Date result = DateUtils.getNthWeekdayOfMonth(date, 1, Calendar.THURSDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/07/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 2, Calendar.THURSDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/14/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 3, Calendar.THURSDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/21/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 4, Calendar.THURSDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/28/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 1, Calendar.FRIDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/01/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 2, Calendar.FRIDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/08/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 3, Calendar.FRIDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/15/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 4, Calendar.FRIDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/22/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 5, Calendar.FRIDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("06/29/2012"), result, true));

		date = DateUtils.toDate("07/04/2012");

		result = DateUtils.getNthWeekdayOfMonth(date, 1, Calendar.MONDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("07/02/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 2, Calendar.MONDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("07/09/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 3, Calendar.MONDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("07/16/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 4, Calendar.MONDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("07/23/2012"), result, true));

		result = DateUtils.getNthWeekdayOfMonth(date, 5, Calendar.MONDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("07/30/2012"), result, true));

		date = DateUtils.toDate("12/03/2012 12:00 PM", DateUtils.DATE_FORMAT_SHORT);

		result = DateUtils.getNthWeekdayOfMonth(date, 1, Calendar.FRIDAY);
		Assertions.assertEquals(0, DateUtils.compare(DateUtils.toDate("12/07/2012 12:00 PM", DateUtils.DATE_FORMAT_SHORT), result, true));
	}


	@Test
	public void testDateCompare() {
		Date d1 = null;
		Date d2 = null;

		Assertions.assertEquals(0, DateUtils.compare(d1, d2, true));
		Assertions.assertEquals(0, DateUtils.compare(d2, d1, true));

		Assertions.assertEquals(0, DateUtils.compare(d1, d2, false));
		Assertions.assertEquals(0, DateUtils.compare(d2, d1, false));

		d1 = new Date();
		Assertions.assertFalse(DateUtils.compare(d1, d2, true) < 0);
		Assertions.assertTrue(DateUtils.compare(d2, d1, true) < 0);

		Assertions.assertFalse(DateUtils.compare(d1, d2, false) < 0);
		Assertions.assertTrue(DateUtils.compare(d2, d1, false) < 0);

		Assertions.assertTrue(DateUtils.compare(d1, d2, true) > 0);
		Assertions.assertFalse(DateUtils.compare(d2, d1, true) > 0);

		Assertions.assertTrue(DateUtils.compare(d1, d2, false) > 0);
		Assertions.assertFalse(DateUtils.compare(d2, d1, false) > 0);

		Assertions.assertNotSame(0, DateUtils.compare(d1, d2, true));
		Assertions.assertNotSame(0, DateUtils.compare(d2, d1, true));

		Assertions.assertNotSame(0, DateUtils.compare(d1, d2, false));
		Assertions.assertNotSame(0, DateUtils.compare(d2, d1, false));

		d2 = new Date();
		d2.setTime(d2.getTime() + 1);

		Assertions.assertTrue(DateUtils.compare(d1, d2, true) < 0);
		Assertions.assertFalse(DateUtils.compare(d2, d1, true) < 0);

		Assertions.assertFalse(DateUtils.compare(d1, d2, false) < 0);
		Assertions.assertFalse(DateUtils.compare(d2, d1, false) < 0);

		Assertions.assertFalse(DateUtils.compare(d1, d2, true) > 0);
		Assertions.assertTrue(DateUtils.compare(d2, d1, true) > 0);

		Assertions.assertFalse(DateUtils.compare(d1, d2, false) > 0);
		Assertions.assertFalse(DateUtils.compare(d2, d1, false) > 0);

		Assertions.assertNotSame(0, DateUtils.compare(d1, d2, true));
		Assertions.assertNotSame(0, DateUtils.compare(d2, d1, true));

		Assertions.assertEquals(0, DateUtils.compare(d1, d2, false));
		Assertions.assertEquals(0, DateUtils.compare(d2, d1, false));
	}


	@Test
	public void testDateCompareWithThreshold() {
		Date date = DateUtils.toDate("2019-01-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);

		// Millis
		Date date_ms1 = DateUtils.toDate("2019-01-01 00:00:00.005", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_ms2 = DateUtils.toDate("2019-01-01 00:00:00.020", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_ms3 = DateUtils.toDate("2019-01-01 00:00:00.021", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_ms1, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_ms1, date, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_ms1, ChronoUnit.MILLIS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_ms1, date, ChronoUnit.MILLIS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_ms1, ChronoUnit.MILLIS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_ms1, date, ChronoUnit.MILLIS, 0));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_ms3, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_ms3, date, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_ms2, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_ms2, date, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_ms2, ChronoUnit.MILLIS, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_ms2, date, ChronoUnit.MILLIS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_ms2, ChronoUnit.MILLIS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_ms2, date, ChronoUnit.MILLIS, 0));

		// Seconds
		Date date_s1 = DateUtils.toDate("2019-01-01 00:00:05.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_s2 = DateUtils.toDate("2019-01-01 00:00:20.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_s1, ChronoUnit.SECONDS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_s1, date, ChronoUnit.SECONDS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_s1, ChronoUnit.SECONDS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_s1, date, ChronoUnit.SECONDS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_s1, ChronoUnit.SECONDS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_s1, date, ChronoUnit.SECONDS, 0));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_s2, ChronoUnit.SECONDS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_s2, date, ChronoUnit.SECONDS, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_s2, ChronoUnit.SECONDS, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_s2, date, ChronoUnit.SECONDS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_s2, ChronoUnit.SECONDS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_s2, date, ChronoUnit.SECONDS, 0));

		// Minutes
		Date date_m1 = DateUtils.toDate("2019-01-01 00:05:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_m2 = DateUtils.toDate("2019-01-01 00:20:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_m1, ChronoUnit.MINUTES, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_m1, date, ChronoUnit.MINUTES, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_m1, ChronoUnit.MINUTES, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_m1, date, ChronoUnit.MINUTES, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_m1, ChronoUnit.MINUTES, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_m1, date, ChronoUnit.MINUTES, 0));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_m2, ChronoUnit.MINUTES, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_m2, date, ChronoUnit.MINUTES, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_m2, ChronoUnit.MINUTES, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_m2, date, ChronoUnit.MINUTES, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_m2, ChronoUnit.MINUTES, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_m2, date, ChronoUnit.MINUTES, 0));

		// Hours
		Date date_H1 = DateUtils.toDate("2019-01-01 05:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_H2 = DateUtils.toDate("2019-01-01 20:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_H1, ChronoUnit.HOURS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_H1, date, ChronoUnit.HOURS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_H1, ChronoUnit.HOURS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_H1, date, ChronoUnit.HOURS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_H1, ChronoUnit.HOURS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_H1, date, ChronoUnit.HOURS, 0));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_H2, ChronoUnit.HOURS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_H2, date, ChronoUnit.HOURS, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_H2, ChronoUnit.HOURS, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_H2, date, ChronoUnit.HOURS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_H2, ChronoUnit.HOURS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_H2, date, ChronoUnit.HOURS, 0));

		// Days
		Date date_d1 = DateUtils.toDate("2019-01-06 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_d2 = DateUtils.toDate("2019-01-20 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_d1, ChronoUnit.DAYS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_d1, date, ChronoUnit.DAYS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_d1, ChronoUnit.DAYS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_d1, date, ChronoUnit.DAYS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_d1, ChronoUnit.DAYS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_d1, date, ChronoUnit.DAYS, 0));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_d2, ChronoUnit.DAYS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_d2, date, ChronoUnit.DAYS, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_d2, ChronoUnit.DAYS, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_d2, date, ChronoUnit.DAYS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_d2, ChronoUnit.DAYS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_d2, date, ChronoUnit.DAYS, 0));

		// Months
		Date date_M1 = DateUtils.toDate("2019-06-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_M2 = DateUtils.toDate("2020-09-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_M1, ChronoUnit.MONTHS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_M1, date, ChronoUnit.MONTHS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_M1, ChronoUnit.MONTHS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_M1, date, ChronoUnit.MONTHS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_M1, ChronoUnit.MONTHS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_M1, date, ChronoUnit.MONTHS, 0));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_M2, ChronoUnit.MONTHS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_M2, date, ChronoUnit.MONTHS, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_M2, ChronoUnit.MONTHS, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_M2, date, ChronoUnit.MONTHS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_M2, ChronoUnit.MONTHS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_M2, date, ChronoUnit.MONTHS, 0));

		// Years
		Date date_y1 = DateUtils.toDate("2024-01-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_y2 = DateUtils.toDate("2039-01-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_y1, ChronoUnit.YEARS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_y1, date, ChronoUnit.YEARS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_y1, ChronoUnit.YEARS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_y1, date, ChronoUnit.YEARS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_y1, ChronoUnit.YEARS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_y1, date, ChronoUnit.YEARS, 0));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_y2, ChronoUnit.YEARS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_y2, date, ChronoUnit.YEARS, 20));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_y2, ChronoUnit.YEARS, 5));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_y2, date, ChronoUnit.YEARS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_y2, ChronoUnit.YEARS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_y2, date, ChronoUnit.YEARS, 0));

		// Combination of estimated-duration time units and specific time units
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_ms1, ChronoUnit.HOURS, 1));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_M1, ChronoUnit.HOURS, 1));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_ms1, ChronoUnit.MONTHS, 1));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_ms1, ChronoUnit.MONTHS, 0));
		{
			Date date_mixed = DateUtils.toDate("2019-01-01 00:00:00.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.MILLIS, 7));
		}
		{
			Date date_mixed = DateUtils.toDate("2019-01-01 00:00:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.SECONDS, 7));
			Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.SECONDS, 6));
		}
		{
			Date date_mixed = DateUtils.toDate("2019-01-01 00:05:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.MINUTES, 6));
			Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.MINUTES, 5));
		}
		{
			Date date_mixed = DateUtils.toDate("2019-01-01 04:05:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.HOURS, 5));
			Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.HOURS, 4));
		}
		{
			Date date_mixed = DateUtils.toDate("2019-01-04 04:05:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.DAYS, 4));
			Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.DAYS, 3));
		}
		{
			Date date_mixed = DateUtils.toDate("2019-03-04 04:05:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.MONTHS, 3));
			Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.MONTHS, 2));
		}
		{
			Date date_mixed = DateUtils.toDate("2020-03-04 04:05:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
			Assertions.assertEquals(0, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.YEARS, 2));
			Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date, date_mixed, ChronoUnit.YEARS, 1));
		}

		// SQL date
		Date date_sql = new java.sql.Date(date.getTime());
		Date date_sql_ms1 = new java.sql.Date(DateUtils.toDate("2019-01-01 00:00:00.005", DateUtils.DATE_FORMAT_FULL_PRECISE).getTime());
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_sql, date_sql_ms1, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_sql_ms1, date_sql, ChronoUnit.MILLIS, 20));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_sql, date_sql_ms1, ChronoUnit.MILLIS, 5));
		Assertions.assertEquals(0, DateUtils.compareWithThreshold(date_sql_ms1, date_sql, ChronoUnit.MILLIS, 5));
		Assertions.assertEquals(-1, DateUtils.compareWithThreshold(date_sql, date_sql_ms1, ChronoUnit.MILLIS, 0));
		Assertions.assertEquals(1, DateUtils.compareWithThreshold(date_sql_ms1, date_sql, ChronoUnit.MILLIS, 0));
	}


	@Test
	public void testDateCompareWithThresholdFail() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> DateUtils.compareWithThreshold(new Date(), new Date(), ChronoUnit.NANOS, -1));
		Assertions.assertEquals("The threshold value [-1] must be greater than or equal to zero.", runtimeException.getMessage());
	}


	@Test
	public void testGetTimeBetween() {
		// Simple date comparisons
		Date date = DateUtils.toDate("2019-01-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_ms = DateUtils.toDate("2019-01-01 00:00:00.005", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_s = DateUtils.toDate("2019-01-01 00:00:05.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_m = DateUtils.toDate("2019-01-01 00:05:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_H = DateUtils.toDate("2019-01-01 05:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_d = DateUtils.toDate("2019-01-06 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_M = DateUtils.toDate("2019-06-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Date date_y = DateUtils.toDate("2024-01-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.MILLIS));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.SECONDS));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.MINUTES));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.HOURS));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.DAYS));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.MONTHS));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date, ChronoUnit.YEARS));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_ms, ChronoUnit.MILLIS));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_ms, date, ChronoUnit.MILLIS));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_s, ChronoUnit.SECONDS));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_s, date, ChronoUnit.SECONDS));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_m, ChronoUnit.MINUTES));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_m, date, ChronoUnit.MINUTES));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_H, ChronoUnit.HOURS));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_H, date, ChronoUnit.HOURS));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_d, ChronoUnit.DAYS));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_d, date, ChronoUnit.DAYS));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_M, ChronoUnit.MONTHS));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_M, date, ChronoUnit.MONTHS));
		Assertions.assertEquals(5, DateUtils.getTimeBetween(date, date_y, ChronoUnit.YEARS));
		Assertions.assertEquals(-5, DateUtils.getTimeBetween(date_y, date, ChronoUnit.YEARS));

		// Mixed date comparisons
		Date date_mixed = DateUtils.toDate("2020-03-04 04:05:06.007", DateUtils.DATE_FORMAT_FULL_PRECISE);
		long dateMixedYears = 1;
		long dateMixedMonths = (dateMixedYears * 12L) + 2L;
		long dateMixedDays = (dateMixedYears * 365L) + 31L + 29L + 3L;
		long dateMixedWeeks = Math.floorDiv(dateMixedDays, 7);
		long dateMixedHalfDays = dateMixedDays * 2L;
		long dateMixedHours = dateMixedDays * 24L + 4L;
		long dateMixedMinutes = dateMixedHours * 60L + 5L;
		long dateMixedSeconds = dateMixedMinutes * 60L + 6L;
		long dateMixedMillis = dateMixedSeconds * 1000L + 7L;
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.ERAS));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.MILLENNIA));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.CENTURIES));
		Assertions.assertEquals(0, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.DECADES));
		Assertions.assertEquals(dateMixedYears, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.YEARS));
		Assertions.assertEquals(dateMixedMonths, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.MONTHS));
		Assertions.assertEquals(dateMixedWeeks, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.WEEKS));
		Assertions.assertEquals(dateMixedDays, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.DAYS));
		Assertions.assertEquals(dateMixedHalfDays, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.HALF_DAYS));
		Assertions.assertEquals(dateMixedHours, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.HOURS));
		Assertions.assertEquals(dateMixedMinutes, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.MINUTES));
		Assertions.assertEquals(dateMixedSeconds, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.SECONDS));
		Assertions.assertEquals(dateMixedMillis, DateUtils.getTimeBetween(date, date_mixed, ChronoUnit.MILLIS));
	}


	@Test
	public void testDateEqual() {
		Date d1 = null;
		Date d2 = null;

		Assertions.assertTrue(DateUtils.isEqual(d1, d2));
		Assertions.assertTrue(DateUtils.isEqual(d2, d1));

		d1 = DateUtils.toDate("01/01/2016");
		Assertions.assertFalse(DateUtils.isEqual(d1, d2));

		d2 = DateUtils.toDate("01/01/2016");
		Assertions.assertTrue(DateUtils.isEqual(d1, d2));

		d1 = DateUtils.toDate("2016-01-01 05:55:55", DateUtils.DATE_FORMAT_FULL);
		Assertions.assertFalse(DateUtils.isEqual(d1, d2));

		d1 = DateUtils.toDate("01/02/2016");
		Assertions.assertFalse(DateUtils.isEqual(d1, d2));
	}


	@Test
	public void testDateEqualWithoutTime() {
		Date d1 = null;
		Date d2 = null;

		Assertions.assertTrue(DateUtils.isEqualWithoutTime(d1, d2));
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(d2, d1));

		d1 = DateUtils.toDate("01/01/2016");
		Assertions.assertFalse(DateUtils.isEqualWithoutTime(d1, d2));

		d2 = DateUtils.toDate("01/01/2016");
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(d1, d2));

		d1 = DateUtils.toDate("2016-01-01 05:55:55", DateUtils.DATE_FORMAT_FULL);
		Assertions.assertTrue(DateUtils.isEqualWithoutTime(d1, d2));

		d1 = DateUtils.toDate("01/02/2016");
		Assertions.assertFalse(DateUtils.isEqualWithoutTime(d1, d2));
	}


	@Test
	public void testCurrentDateBetween() {
		// An open ended start & end date should always return true
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(null, null, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(null, null, false));

		Date today = DateUtils.addMilliseconds(new Date(), -1);
		Date yesterday = DateUtils.addDays(today, -1);
		Date tomorrow = DateUtils.addDays(today, 1);

		// A start date of yesterday and an open ended end date should return true
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(yesterday, null, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(yesterday, null, false));

		// A start date of tomorrow and an open ended end date should return false
		Assertions.assertFalse(DateUtils.isCurrentDateBetween(tomorrow, null, true));
		Assertions.assertFalse(DateUtils.isCurrentDateBetween(tomorrow, null, false));

		// A start date of today and an open ended end date should return true
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(today, null, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(today, null, false));

		// An end date of yesterday and an open ended start date should return false
		Assertions.assertFalse(DateUtils.isCurrentDateBetween(null, yesterday, true));
		Assertions.assertFalse(DateUtils.isCurrentDateBetween(null, yesterday, false));

		// An end date of tomorrow and an open ended start date should return true
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(null, tomorrow, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(null, tomorrow, false));

		// An end date of today and an open ended end start should return true
		Assertions.assertFalse(DateUtils.isCurrentDateBetween(null, today, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(null, today, false));

		// A start date of yesterday and an end date of tomorrow should return true;
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(yesterday, tomorrow, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(yesterday, tomorrow, false));

		// A start date of today and an end date of today should return true;
		Assertions.assertFalse(DateUtils.isCurrentDateBetween(today, today, true));
		Assertions.assertTrue(DateUtils.isCurrentDateBetween(today, today, false));
	}


	@Test
	public void testIsDateAfter() {
		// *** Test data (dates) ***
		Date dateA = DateUtils.toDate("10/01/2017");

		// has 10 minute time offset added.
		Date dateB = DateUtils.toDate("10/02/2017");
		dateB.setTime(dateB.getTime() + 600000);

		// like dateB, but offset by 10 more minutes relative to dateB.
		Date dateC = DateUtils.toDate("10/02/2017");
		dateC.setTime(dateC.getTime() + 1200000);

		// first date is before second date;  expect False result
		Assertions.assertFalse(DateUtils.isDateAfter(dateA, dateB));

		// first date is after second date;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfter(dateB, dateA));

		// first date is equal to second date;  expect false result
		Assertions.assertFalse(DateUtils.isDateAfter(dateA, dateA));

		// first date is earlier than second date by 10 minutes;  expect false result
		Assertions.assertFalse(DateUtils.isDateAfterOrEqual(dateB, dateC));

		// first date is later than second date by 10 minutes;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfterOrEqual(dateC, dateB));

		// *** Test with null date values ***

		// null date is before second date;  expect True result (null treated as infinite future date)
		Assertions.assertTrue(DateUtils.isDateAfter(null, dateB));

		// first date is after second date;  expect false result
		Assertions.assertFalse(DateUtils.isDateAfter(dateB, null));

		// both dates null;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfter(null, null));
	}


	@Test
	public void testisDateAfterOrEqual() {
		// *** Test data (dates) ***
		Date dateA = DateUtils.toDate("10/01/2017");

		// has 10 minute time offset added.
		Date dateB = DateUtils.toDate("10/02/2017");
		dateB.setTime(dateB.getTime() + 600000);

		// like dateB, but offset by 10 more minutes relative to dateB.
		Date dateC = DateUtils.toDate("10/02/2017");
		dateC.setTime(dateC.getTime() + 1200000);

		// first date is before second date;  expect False result
		Assertions.assertFalse(DateUtils.isDateAfterOrEqual(dateA, dateB));

		// first date is after second date;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfterOrEqual(dateB, dateA));

		// first date is equal to second date;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfterOrEqual(dateA, dateA));

		// first date is earlier than second date by 10 minutes;  expect false result
		Assertions.assertFalse(DateUtils.isDateAfterOrEqual(dateB, dateC));

		// first date is later than second date by 10 minutes;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfterOrEqual(dateC, dateB));

		// *** Test with null date values ***

		// null date is before second date;  expect True result (null treated as infinite future date)
		Assertions.assertTrue(DateUtils.isDateAfterOrEqual(null, dateB));

		// first date is before second date (null);  expect false result (null treated as infinite future date)
		Assertions.assertFalse(DateUtils.isDateAfterOrEqual(dateB, null));

		// both dates null;  expect true result
		Assertions.assertTrue(DateUtils.isDateAfterOrEqual(null, null));
	}


	@Test
	public void testIsDateBefore() {
		// *** Test data (dates) ***
		Date dateA = DateUtils.toDate("10/01/2017");

		// has 10 minute time offset added.
		Date dateB = DateUtils.toDate("10/02/2017");
		dateB.setTime(dateB.getTime() + 600000);

		// like dateB, but offset by 10 more minutes relative to dateB.
		Date dateC = DateUtils.toDate("10/02/2017");
		dateC.setTime(dateC.getTime() + 1200000);

		// *** Tests that ignore time for date comparisons ***

		// first date is before second date, includeTime = false:  expect True result
		Assertions.assertTrue(DateUtils.isDateBefore(dateA, dateB, false));

		// first date is after second date, includeTime = false:  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateB, dateA, false));

		// first date is equal to second date, includeTime = false;  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateA, dateA, false));

		// *** These tests include the time when comparing dates ***

		// first date is before second date, includeTime = true:  expect true result
		Assertions.assertTrue(DateUtils.isDateBefore(dateA, dateB, true));

		// first date is after second date, includeTime = true:  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateB, dateA, true));

		// first date is equal to second date, includeTime = true;  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateA, dateA, true));

		// first date and second date have same date, but first date's time is prior to second date's time, includeTime = true; expect true result
		Assertions.assertTrue(DateUtils.isDateBefore(dateB, dateC, true));

		// first date and second date have same date, but first date's time is after to second date's time, includeTime = true; expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateC, dateB, true));

		// *** Test with null date values ***

		// null date is before second date, includeTime = false:  expect True result
		Assertions.assertTrue(DateUtils.isDateBefore(null, dateB, false));

		// first date is after second date, includeTime = false:  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateB, null, false));

		// both dates null, includeTime = false:  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(null, null, false));

		// null date is before second date, includeTime = true:  expect True result
		Assertions.assertTrue(DateUtils.isDateBefore(null, dateB, true));

		// first date is after second date, includeTime = true:  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(dateB, null, true));

		// both dates null, includeTime = true:  expect false result
		Assertions.assertFalse(DateUtils.isDateBefore(null, null, true));
	}


	@Test
	public void testIsDateBeforeOrEqual() {
		// *** Test data (dates) ***
		Date dateA = DateUtils.toDate("10/01/2017");

		// has 10 minute time offset added.
		Date dateB = DateUtils.toDate("10/02/2017");
		dateB.setTime(dateB.getTime() + 600000);

		// like dateB, but offset by 10 more minutes relative to dateB.
		Date dateC = DateUtils.toDate("10/02/2017");
		dateC.setTime(dateC.getTime() + 1200000);

		// *** Tests that ignore time for date comparisons ***

		// first date is before second date, includeTime = false:  expect True result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(dateA, dateB, false));

		// first date is after second date, includeTime = false:  expect false result
		Assertions.assertFalse(DateUtils.isDateBeforeOrEqual(dateB, dateA, false));

		// first date is equal to second date, includeTime = false;  expect true result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(dateA, dateA, false));

		// *** These tests include the time when comparing dates ***

		// first date is before second date, includeTime = true:  expect true result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(dateA, dateB, true));

		// first date is after second date, includeTime = true:  expect false result
		Assertions.assertFalse(DateUtils.isDateBeforeOrEqual(dateB, dateA, true));

		// first date is equal to second date, includeTime = true;  expect true result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(dateA, dateA, true));

		// first date and second date have same date, but first date's time is prior to second date's time, includeTime = true; expect true result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(dateB, dateC, true));

		// first date and second date have same date, but first date's time is after to second date's time, includeTime = true; expect false result
		Assertions.assertFalse(DateUtils.isDateBeforeOrEqual(dateC, dateB, true));

		// *** Test with null date values ***

		// first date (null) is before second date, includeTime = false:  expect True result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(null, dateB, false));

		// first date is after second date (null), includeTime = false:  expect false result
		Assertions.assertFalse(DateUtils.isDateBeforeOrEqual(dateB, null, false));

		// both dates are null, includeTime = false:  expect true result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(null, null, false));

		// first date (null) is before second date, includeTime = true:  expect True result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(null, dateB, true));

		// first date is after second date (null), includeTime = true:  expect false result
		Assertions.assertFalse(DateUtils.isDateBeforeOrEqual(dateB, null, true));

		// first date is after second date, includeTime = true:  expect true result
		Assertions.assertTrue(DateUtils.isDateBeforeOrEqual(null, null, true));
	}


	@Test
	public void testIsDateBetween() {
		LocalDate today = LocalDate.now();
		LocalDate yesterday = today.minusDays(1);
		LocalDate tomorrow = today.plusDays(1);

		// An open ended start & end date should always return true
		Assertions.assertTrue(DateUtils.isDateBetween(today, null, null));

		// A start date of yesterday and an open ended end date should return true
		Assertions.assertTrue(DateUtils.isDateBetween(today, null, tomorrow));

		// A start date of tomorrow and an open ended end date should return false
		Assertions.assertFalse(DateUtils.isDateBetween(today, tomorrow, null));

		// A start date of today and an open ended end date should return true
		Assertions.assertTrue(DateUtils.isDateBetween(today, today, null));

		// An end date of yesterday and an open ended start date should return false
		Assertions.assertFalse(DateUtils.isDateBetween(today, null, yesterday));

		// An end date of tomorrow and an open ended start date should return true
		Assertions.assertTrue(DateUtils.isDateBetween(today, null, tomorrow));

		// An end date of today and an open ended end start should return true
		Assertions.assertTrue(DateUtils.isDateBetween(today, null, today));

		// A start date of yesterday and an end date of tomorrow should return true;
		Assertions.assertTrue(DateUtils.isDateBetween(today, yesterday, tomorrow));

		// A start date of today and an end date of today should return true;
		Assertions.assertTrue(DateUtils.isDateBetween(today, today, today));

		// A null comparison date should throw and illegal argument exception
		try {
			DateUtils.isDateBetween(null, yesterday, tomorrow);
		}
		catch (IllegalArgumentException iae) {
			Assertions.assertEquals("Required date argument for comparison with isDateBetween cannot be null - startDate and endDate may be null.", iae.getMessage());
		}
	}


	@Test
	public void testOverlapInDates() {
		Date date1 = DateUtils.toDate("01/01/2000");
		Date date2 = DateUtils.toDate("01/05/2000");
		Date date3 = DateUtils.toDate("01/10/2000");
		Date date4 = DateUtils.toDate("01/15/2000");

		Assertions.assertTrue(DateUtils.isOverlapInDates(date1, date3, date2, date4));
		Assertions.assertTrue(DateUtils.isOverlapInDates(date1, date4, date2, date3));
		Assertions.assertTrue(DateUtils.isOverlapInDates(date2, date3, date1, date4));
		Assertions.assertTrue(DateUtils.isOverlapInDates(date2, date4, date1, date3));
		Assertions.assertTrue(DateUtils.isOverlapInDates(null, date2, date1, date3));
		Assertions.assertTrue(DateUtils.isOverlapInDates(date1, null, date3, date4));

		Assertions.assertFalse(DateUtils.isOverlapInDates(date1, date2, date3, date4));
		Assertions.assertFalse(DateUtils.isOverlapInDates(null, date2, date3, date4));
		Assertions.assertFalse(DateUtils.isOverlapInDates(date1, date2, date3, null));
		Assertions.assertFalse(DateUtils.isOverlapInDates(null, date2, date3, null));
	}


	@Test
	public void testDaysSinceNaturalStart() {
		Date date = DateUtils.toDate("01/01/1900");

		int days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(1, days);

		date = DateUtils.toDate("01/01/1910");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(3653, days);
		date = DateUtils.toDate("05/01/2009");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(39933, days);

		date = DateUtils.toDate("12/28/2008");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(39809, days);

		date = DateUtils.toDate("03/09/2008");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(39515, days);

		date = DateUtils.toDate("03/10/2008");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(39516, days);

		date = DateUtils.toDate("03/11/2008");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(39517, days);

		date = DateUtils.toDate("03/11/2012");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(40978, days);

		date = DateUtils.toDate("03/12/2012");
		days = DateUtils.getDaysSinceNaturalStart(date);
		Assertions.assertEquals(40979, days);
	}


	@Test
	public void testFirstDayOfWeek() {
		Date date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("01/01/2010"));
		DateUtils.assertDatesEqual(DateUtils.toDate("12/27/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), date);

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("12/23/2009"));
		DateUtils.assertDatesEqual(DateUtils.toDate("12/20/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), date);

		IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> DateUtils.getFirstDayOfWeek(null));
		Assertions.assertEquals("Date is required to determine the first day of the week.", e.getMessage());
	}


	@Test
	public void testLastDayOfWeek() {
		Date date = DateUtils.getLastDayOfWeek(DateUtils.toDate("01/01/2010"));
		DateUtils.assertDatesEqual(DateUtils.toDate("01/02/2010 11:59 PM", DateUtils.DATE_FORMAT_SHORT), date);

		date = DateUtils.getLastDayOfWeek(DateUtils.toDate("12/23/2009"));
		DateUtils.assertDatesEqual(DateUtils.toDate("12/26/2009 11:59 PM", DateUtils.DATE_FORMAT_SHORT), date);

		IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> DateUtils.getLastDayOfWeek(null));
		Assertions.assertEquals("Date is required to determine the last day of the week.", e.getMessage());
	}


	@Test
	public void testGetYear() {
		int year = DateUtils.getYear(DateUtils.toDate("01/01/2010"));
		Assertions.assertEquals(2010, year);

		year = DateUtils.getYear(DateUtils.toDate("01/01/1989"));
		Assertions.assertEquals(1989, year);

		year = DateUtils.getYear(DateUtils.toDate("01/01/1900"));
		Assertions.assertEquals(1900, year);

		IllegalArgumentException e = null;
		try {
			DateUtils.getYear(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getYear'");
	}


	@Test
	public void testGetDayOfWeek() {
		int dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/01/2010"));
		Assertions.assertEquals(Calendar.FRIDAY, dayOfWeek);

		dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/02/2010"));
		Assertions.assertEquals(Calendar.SATURDAY, dayOfWeek);

		dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/03/2010"));
		Assertions.assertEquals(Calendar.SUNDAY, dayOfWeek);

		dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/04/2010"));
		Assertions.assertEquals(Calendar.MONDAY, dayOfWeek);

		dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/05/2010"));
		Assertions.assertEquals(Calendar.TUESDAY, dayOfWeek);

		dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/06/2010"));
		Assertions.assertEquals(Calendar.WEDNESDAY, dayOfWeek);

		dayOfWeek = DateUtils.getDayOfWeek(DateUtils.toDate("01/07/2010"));
		Assertions.assertEquals(Calendar.THURSDAY, dayOfWeek);

		IllegalArgumentException e = null;
		try {
			DateUtils.getDayOfWeek((Date) null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getDayOfWeek'");
	}


	@Test
	public void testGetWeekdayName() {
		Assertions.assertEquals("Sunday", DateUtils.getWeekdayName(1));
		Assertions.assertEquals("Monday", DateUtils.getWeekdayName(2));
		Assertions.assertEquals("Tuesday", DateUtils.getWeekdayName(3));
		Assertions.assertEquals("Wednesday", DateUtils.getWeekdayName(4));
		Assertions.assertEquals("Thursday", DateUtils.getWeekdayName(5));
		Assertions.assertEquals("Friday", DateUtils.getWeekdayName(6));
		Assertions.assertEquals("Saturday", DateUtils.getWeekdayName(7));
		Assertions.assertThrows(Exception.class, () -> DateUtils.getWeekdayName(-1));
		Assertions.assertThrows(Exception.class, () -> DateUtils.getWeekdayName(0));
		Assertions.assertThrows(Exception.class, () -> DateUtils.getWeekdayName(8));
		Assertions.assertThrows(Exception.class, () -> DateUtils.getWeekdayName(9));
	}


	@Test
	public void testGetWeekdayNameOrNull() {
		Assertions.assertEquals("Sunday", DateUtils.getWeekdayNameOrNull(1));
		Assertions.assertEquals("Monday", DateUtils.getWeekdayNameOrNull(2));
		Assertions.assertEquals("Tuesday", DateUtils.getWeekdayNameOrNull(3));
		Assertions.assertEquals("Wednesday", DateUtils.getWeekdayNameOrNull(4));
		Assertions.assertEquals("Thursday", DateUtils.getWeekdayNameOrNull(5));
		Assertions.assertEquals("Friday", DateUtils.getWeekdayNameOrNull(6));
		Assertions.assertEquals("Saturday", DateUtils.getWeekdayNameOrNull(7));
		Assertions.assertNull(DateUtils.getWeekdayNameOrNull(-1));
		Assertions.assertNull(DateUtils.getWeekdayNameOrNull(0));
		Assertions.assertNull(DateUtils.getWeekdayNameOrNull(8));
		Assertions.assertNull(DateUtils.getWeekdayNameOrNull(9));
	}


	@Test
	public void testGetDayOfMonth() {
		Assertions.assertEquals(1, DateUtils.getDayOfMonth(DateUtils.toDate("01/01/2010")));
		Assertions.assertEquals(2, DateUtils.getDayOfMonth(DateUtils.toDate("01/02/2010")));
		Assertions.assertEquals(10, DateUtils.getDayOfMonth(DateUtils.toDate("01/10/2010")));
		Assertions.assertEquals(15, DateUtils.getDayOfMonth(DateUtils.toDate("01/15/2010")));
		Assertions.assertEquals(20, DateUtils.getDayOfMonth(DateUtils.toDate("01/20/2010")));
		Assertions.assertEquals(30, DateUtils.getDayOfMonth(DateUtils.toDate("01/30/2010")));

		IllegalArgumentException e = null;
		try {
			DateUtils.getDayOfMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getDayOfMonth'");
	}


	@Test
	public void testGetMonthOfYear() {
		Assertions.assertEquals(1, DateUtils.getMonthOfYear(DateUtils.toDate("01/01/2010")));
		Assertions.assertEquals(2, DateUtils.getMonthOfYear(DateUtils.toDate("02/01/2010")));
		Assertions.assertEquals(3, DateUtils.getMonthOfYear(DateUtils.toDate("03/01/2010")));
		Assertions.assertEquals(4, DateUtils.getMonthOfYear(DateUtils.toDate("04/01/2010")));
		Assertions.assertEquals(5, DateUtils.getMonthOfYear(DateUtils.toDate("05/01/2010")));
		Assertions.assertEquals(6, DateUtils.getMonthOfYear(DateUtils.toDate("06/01/2010")));
		Assertions.assertEquals(7, DateUtils.getMonthOfYear(DateUtils.toDate("07/01/2010")));
		Assertions.assertEquals(8, DateUtils.getMonthOfYear(DateUtils.toDate("08/01/2010")));
		Assertions.assertEquals(9, DateUtils.getMonthOfYear(DateUtils.toDate("09/01/2010")));
		Assertions.assertEquals(10, DateUtils.getMonthOfYear(DateUtils.toDate("10/01/2010")));
		Assertions.assertEquals(11, DateUtils.getMonthOfYear(DateUtils.toDate("11/01/2010")));
		Assertions.assertEquals(12, DateUtils.getMonthOfYear(DateUtils.toDate("12/01/2010")));

		IllegalArgumentException e = null;
		try {
			DateUtils.getMonthOfYear(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getMonthOfYear'");
	}


	@Test
	public void testGetMonthOfQuarter() {
		Assertions.assertEquals(1, DateUtils.getMonthOfQuarter(DateUtils.toDate("01/01/2010")));
		Assertions.assertEquals(2, DateUtils.getMonthOfQuarter(DateUtils.toDate("02/01/2010")));
		Assertions.assertEquals(3, DateUtils.getMonthOfQuarter(DateUtils.toDate("03/01/2010")));
		Assertions.assertEquals(1, DateUtils.getMonthOfQuarter(DateUtils.toDate("04/01/2010")));
		Assertions.assertEquals(2, DateUtils.getMonthOfQuarter(DateUtils.toDate("05/01/2010")));
		Assertions.assertEquals(3, DateUtils.getMonthOfQuarter(DateUtils.toDate("06/01/2010")));
		Assertions.assertEquals(1, DateUtils.getMonthOfQuarter(DateUtils.toDate("07/01/2010")));
		Assertions.assertEquals(2, DateUtils.getMonthOfQuarter(DateUtils.toDate("08/01/2010")));
		Assertions.assertEquals(3, DateUtils.getMonthOfQuarter(DateUtils.toDate("09/01/2010")));
		Assertions.assertEquals(1, DateUtils.getMonthOfQuarter(DateUtils.toDate("10/01/2010")));
		Assertions.assertEquals(2, DateUtils.getMonthOfQuarter(DateUtils.toDate("11/01/2010")));
		Assertions.assertEquals(3, DateUtils.getMonthOfQuarter(DateUtils.toDate("12/01/2010")));

		IllegalArgumentException e = null;
		try {
			DateUtils.getMonthOfQuarter(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getMonthOfQuarter'");
	}


	@Test
	public void testGetLastDayOfMonth() {
		Date date = DateUtils.getLastDayOfMonth(DateUtils.toDate("1/15/1900"));
		Assertions.assertEquals("01/31/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("2/1/1900"));
		Assertions.assertEquals("02/28/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("2/1/1904"));
		Assertions.assertEquals("02/29/1904", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("3/10/1900"));
		Assertions.assertEquals("03/31/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("4/9/1900"));
		Assertions.assertEquals("04/30/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("5/31/1900"));
		Assertions.assertEquals("05/31/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("9/22/1900"));
		Assertions.assertEquals("09/30/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfMonth(DateUtils.toDate("5/14/2012"));
		Assertions.assertEquals("Thursday, May 31, 2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SPELLED));

		IllegalArgumentException e = null;
		try {
			DateUtils.getLastDayOfMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getLastDayOfMonth'");
	}


	@Test
	public void testGetLastWeekdayOfMonth() {
		Date date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("1/15/2013"));
		Assertions.assertEquals("01/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("12/31/2012"));
		Assertions.assertEquals("12/31/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("11/1/2012"));
		Assertions.assertEquals("11/30/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("2/1/2013"));
		Assertions.assertEquals("02/28/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("2/28/2012"));
		Assertions.assertEquals("02/29/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("3/31/2013"));
		Assertions.assertEquals("03/29/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("4/1/2013"));
		Assertions.assertEquals("04/30/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("5/9/2013"));
		Assertions.assertEquals("05/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfMonth(DateUtils.toDate("5/14/2012"));
		Assertions.assertEquals("Thursday, May 31, 2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SPELLED));

		IllegalArgumentException e = null;
		try {
			DateUtils.getLastWeekdayOfMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getLastWeekdayOfMonth'");
	}


	@Test
	public void testGetLastWeekdayOfPreviousMonth() {
		Date date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("2/15/2013"));
		Assertions.assertEquals("01/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("1/31/2013"));
		Assertions.assertEquals("12/31/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("12/1/2012"));
		Assertions.assertEquals("11/30/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("3/1/2013"));
		Assertions.assertEquals("02/28/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("3/28/2012"));
		Assertions.assertEquals("02/29/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("4/30/2013"));
		Assertions.assertEquals("03/29/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("5/1/2013"));
		Assertions.assertEquals("04/30/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("6/9/2013"));
		Assertions.assertEquals("05/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastWeekdayOfPreviousMonth(DateUtils.toDate("6/14/2012"));
		Assertions.assertEquals("Thursday, May 31, 2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SPELLED));

		IllegalArgumentException e = null;
		try {
			DateUtils.getLastWeekdayOfPreviousMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getLastWeekdayOfPreviousMonth'");
	}


	@Test
	public void testGetLastDayOfPreviousMonth() {
		Date date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("2/15/2013"));
		Assertions.assertEquals("01/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("1/31/2013"));
		Assertions.assertEquals("12/31/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("12/1/2012"));
		Assertions.assertEquals("11/30/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("3/1/2013"));
		Assertions.assertEquals("02/28/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("3/28/2012"));
		Assertions.assertEquals("02/29/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("4/30/2013"));
		Assertions.assertEquals("03/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("5/1/2013"));
		Assertions.assertEquals("04/30/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("6/9/2013"));
		Assertions.assertEquals("05/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("11/2/2015"));
		Assertions.assertEquals("10/31/2015", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousMonth(DateUtils.toDate("6/14/2012"));
		Assertions.assertEquals("Thursday, May 31, 2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SPELLED));

		IllegalArgumentException e = null;
		try {
			DateUtils.getLastDayOfPreviousMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getLastDayOfPreviousMonth'");
	}


	@Test
	public void testGetLastDayOfPreviousQuarter() {
		Date date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("2/15/2013"));
		Assertions.assertEquals("12/31/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("1/31/2013"));
		Assertions.assertEquals("12/31/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("12/1/2012"));
		Assertions.assertEquals("09/30/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("3/1/2013"));
		Assertions.assertEquals("12/31/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("3/28/2012"));
		Assertions.assertEquals("12/31/2011", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("4/30/2013"));
		Assertions.assertEquals("03/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("5/1/2013"));
		Assertions.assertEquals("03/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("6/9/2013"));
		Assertions.assertEquals("03/31/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("11/2/2015"));
		Assertions.assertEquals("09/30/2015", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getLastDayOfPreviousQuarter(DateUtils.toDate("6/14/2012"));
		Assertions.assertEquals("Saturday, March 31, 2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SPELLED));

		IllegalArgumentException e = null;
		try {
			DateUtils.getLastDayOfPreviousQuarter(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getLastDayOfPreviousQuarter'");
	}


	@Test
	public void testGetFirstWeekdayOfMonth() {
		Date date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("1/15/2013"));
		Assertions.assertEquals("01/01/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("12/31/2012"));
		Assertions.assertEquals("12/03/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("11/1/2012"));
		Assertions.assertEquals("11/01/2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("2/1/2013"));
		Assertions.assertEquals("02/01/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("3/31/2013"));
		Assertions.assertEquals("03/01/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("4/1/2013"));
		Assertions.assertEquals("04/01/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("5/9/2013"));
		Assertions.assertEquals("05/01/2013", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstWeekdayOfMonth(DateUtils.toDate("5/14/2012"));
		Assertions.assertEquals("Tuesday, May 1, 2012", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SPELLED));

		IllegalArgumentException e = null;
		try {
			DateUtils.getFirstWeekdayOfMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getFirstWeekdayOfMonth'");
	}


	@Test
	public void testGetFirstDayOfMonth() {
		Date date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("1/15/1900"));
		Assertions.assertEquals("01/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("2/1/1900"));
		Assertions.assertEquals("02/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("2/1/1904"));
		Assertions.assertEquals("02/01/1904", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("3/10/1900"));
		Assertions.assertEquals("03/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("4/9/1900"));
		Assertions.assertEquals("04/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("5/31/1900"));
		Assertions.assertEquals("05/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfMonth(DateUtils.toDate("9/22/1900"));
		Assertions.assertEquals("09/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		IllegalArgumentException e = null;
		try {
			DateUtils.getFirstDayOfMonth(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getFirstDayOfMonth'");
	}


	@Test
	public void testGetLastDayOfMonthQuarterYear() {
		Date date = DateUtils.toDate("1/15/1900");
		Assertions.assertEquals("01/31/1900", DateUtils.fromDateShort(DateUtils.getLastDayOfMonth(date)));
		Assertions.assertEquals("03/31/1900", DateUtils.fromDateShort(DateUtils.getLastDayOfQuarter(date)));
		Assertions.assertEquals("12/31/1900", DateUtils.fromDateShort(DateUtils.getLastDayOfYear(date)));

		date = DateUtils.toDate("02/01/2004");
		Assertions.assertEquals("02/29/2004", DateUtils.fromDateShort(DateUtils.getLastDayOfMonth(date)));
		Assertions.assertEquals("03/31/2004", DateUtils.fromDateShort(DateUtils.getLastDayOfQuarter(date)));
		Assertions.assertEquals("12/31/2004", DateUtils.fromDateShort(DateUtils.getLastDayOfYear(date)));

		date = DateUtils.toDate("09/30/2011");
		Assertions.assertEquals("09/30/2011", DateUtils.fromDateShort(DateUtils.getLastDayOfMonth(date)));
		Assertions.assertEquals("09/30/2011", DateUtils.fromDateShort(DateUtils.getLastDayOfQuarter(date)));
		Assertions.assertEquals("12/31/2011", DateUtils.fromDateShort(DateUtils.getLastDayOfYear(date)));

		date = DateUtils.toDate("12/31/2011");
		Assertions.assertEquals("12/31/2011", DateUtils.fromDateShort(DateUtils.getLastDayOfMonth(date)));
		Assertions.assertEquals("12/31/2011", DateUtils.fromDateShort(DateUtils.getLastDayOfQuarter(date)));
		Assertions.assertEquals("12/31/2011", DateUtils.fromDateShort(DateUtils.getLastDayOfYear(date)));

		date = DateUtils.toDate("05/05/2009");
		Assertions.assertEquals("05/31/2009", DateUtils.fromDateShort(DateUtils.getLastDayOfMonth(date)));
		Assertions.assertEquals("06/30/2009", DateUtils.fromDateShort(DateUtils.getLastDayOfQuarter(date)));
		Assertions.assertEquals("12/31/2009", DateUtils.fromDateShort(DateUtils.getLastDayOfYear(date)));
	}


	@Test
	public void testGetDayOfYear() {
		Assertions.assertEquals(365, DateUtils.getDayOfYear(DateUtils.toDate("12/31/1900")));
		Assertions.assertEquals(15, DateUtils.getDayOfYear(DateUtils.toDate("1/15/2008")));
		Assertions.assertEquals(171, DateUtils.getDayOfYear(DateUtils.toDate("6/20/2009")));

		IllegalArgumentException e = null;
		try {
			DateUtils.getDayOfYear(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getDayOfYear'");
	}


	@Test
	public void testGetDaysInYear() {
		Assertions.assertEquals(365, DateUtils.getDaysInYear(DateUtils.toDate("1/15/1900")));
		Assertions.assertEquals(366, DateUtils.getDaysInYear(DateUtils.toDate("1/15/2008")));
		Assertions.assertEquals(365, DateUtils.getDaysInYear(DateUtils.toDate("1/15/2009")));

		IllegalArgumentException e = null;
		try {
			DateUtils.getDaysInYear(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getDaysInYear'");
	}


	@Test
	public void testYearsDifference() {
		Date dateOne = DateUtils.toDate("12/31/1900");
		Date dateTwo = DateUtils.toDate("01/01/1901");

		int dif = DateUtils.getYearsDifference(dateOne, dateTwo);
		Assertions.assertEquals(-1, dif);

		dif = DateUtils.getYearsDifference(dateTwo, dateOne);
		Assertions.assertEquals(1, dif);

		dateOne = DateUtils.toDate("12/31/2000");
		dateTwo = DateUtils.toDate("01/01/2009");
		dif = DateUtils.getYearsDifference(dateTwo, dateOne);
		Assertions.assertEquals(9, dif);

		dateOne = DateUtils.toDate("12/31/2005");
		dateTwo = DateUtils.toDate("01/01/1989");
		dif = DateUtils.getYearsDifference(dateTwo, dateOne);
		Assertions.assertEquals(-16, dif);

		dif = DateUtils.getYearsDifference(dateOne, dateTwo);
		Assertions.assertEquals(16, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getYearsDifference(null, dateTwo);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getYearsDifference'");
	}


	@Test
	public void testMonthsDifference() {
		Date dateOne = DateUtils.toDate("12/31/1900");
		Date dateTwo = DateUtils.toDate("01/01/1901");

		int dif = DateUtils.getMonthsDifference(dateOne, dateTwo);
		Assertions.assertEquals(-1, dif);

		dif = DateUtils.getMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(1, dif);

		dateOne = DateUtils.toDate("12/31/2000");
		dateTwo = DateUtils.toDate("01/01/2009");
		dif = DateUtils.getMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(97, dif);

		dateOne = DateUtils.toDate("12/31/2005");
		dateTwo = DateUtils.toDate("01/01/1989");
		dif = DateUtils.getMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(-203, dif);

		dif = DateUtils.getMonthsDifference(dateOne, dateTwo);
		Assertions.assertEquals(203, dif);

		dateOne = DateUtils.toDate("12/31/2005");
		dateTwo = DateUtils.toDate("12/30/2004");
		dif = DateUtils.getMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(-12, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getMonthsDifference(null, dateTwo);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getMonthsDifference'");
	}


	@Test
	public void testFullMonthsDifference() {
		Date dateOne = DateUtils.toDate("12/31/1900");
		Date dateTwo = DateUtils.toDate("01/01/1901");

		int dif = DateUtils.getFullMonthsDifference(dateOne, dateTwo);
		Assertions.assertEquals(0, dif);

		dif = DateUtils.getFullMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(0, dif);

		dateOne = DateUtils.toDate("12/31/2000");
		dateTwo = DateUtils.toDate("01/01/2009");
		dif = DateUtils.getFullMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(96, dif);

		dateOne = DateUtils.toDate("12/31/2005");
		dateTwo = DateUtils.toDate("01/01/1989");
		dif = DateUtils.getFullMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(-203, dif);

		dif = DateUtils.getFullMonthsDifference(dateOne, dateTwo);
		Assertions.assertEquals(203, dif);

		dateOne = DateUtils.toDate("12/31/2005");
		dateTwo = DateUtils.toDate("12/30/2004");
		dif = DateUtils.getFullMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(-12, dif);

		dif = DateUtils.getFullMonthsDifference(dateOne, dateTwo);
		Assertions.assertEquals(12, dif);

		dateOne = DateUtils.toDate("12/01/2005");
		dateTwo = DateUtils.toDate("12/30/2005");
		dif = DateUtils.getFullMonthsDifference(dateTwo, dateOne);
		Assertions.assertEquals(0, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getYearsDifference(null, dateTwo);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getYearsDifference'");
	}


	@Test
	public void testWeeksDifference() {
		Date dateOne = DateUtils.toDate("12/31/1900");
		Date dateTwo = DateUtils.toDate("01/01/1901");

		int dif = DateUtils.getWeeksDifference(dateOne, dateTwo);
		Assertions.assertEquals(0, dif);

		dateTwo = DateUtils.toDate("01/07/1901");
		dif = DateUtils.getWeeksDifference(dateOne, dateTwo);
		Assertions.assertEquals(-1, dif);

		dif = DateUtils.getWeeksDifference(dateTwo, dateOne);
		Assertions.assertEquals(1, dif);

		dateOne = DateUtils.toDate("12/31/2000");
		dateTwo = DateUtils.toDate("01/01/2009");
		dif = DateUtils.getWeeksDifference(dateTwo, dateOne);
		Assertions.assertEquals(417, dif);

		dateOne = DateUtils.toDate("12/31/2005");
		dateTwo = DateUtils.toDate("01/01/1989");
		dif = DateUtils.getWeeksDifference(dateTwo, dateOne);
		Assertions.assertEquals(-886, dif);

		dif = DateUtils.getWeeksDifference(dateOne, dateTwo);
		Assertions.assertEquals(886, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getWeeksDifference(null, dateTwo);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getWeeksDifference'");
	}


	@Test
	public void testDaysDifference() {
		Date dateOne = DateUtils.toDate("12/31/1900");
		Date dateTwo = DateUtils.toDate("01/01/1901");

		int dif = DateUtils.getDaysDifference(dateOne, dateTwo);
		Assertions.assertEquals(-1, dif);
		Assertions.assertEquals(-2, DateUtils.getDaysDifferenceInclusive(dateOne, dateTwo));

		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(1, dif);
		Assertions.assertEquals(2, DateUtils.getDaysDifferenceInclusive(dateTwo, dateOne));

		dateOne = DateUtils.toDate("1/1/1900");
		dateTwo = DateUtils.toDate("01/01/1910");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(3652, dif);
		Assertions.assertEquals(3653, DateUtils.getDaysDifferenceInclusive(dateTwo, dateOne));

		dateTwo = DateUtils.toDate("05/01/2009");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(39932, dif);
		Assertions.assertEquals(39933, DateUtils.getDaysDifferenceInclusive(dateTwo, dateOne));

		dif = DateUtils.getDaysDifference(dateOne, dateTwo);
		Assertions.assertEquals(-39932, dif);
		Assertions.assertEquals(-39933, DateUtils.getDaysDifferenceInclusive(dateOne, dateTwo));

		dateTwo = DateUtils.toDate("12/28/2008");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(39808, dif);

		dateTwo = DateUtils.toDate("03/09/2008");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(39514, dif);

		dateTwo = DateUtils.toDate("03/10/2008");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(39515, dif);

		dateTwo = DateUtils.toDate("03/11/2008");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(39516, dif);

		dateTwo = DateUtils.toDate("03/11/2012");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(40977, dif);

		dateTwo = DateUtils.toDate("03/12/2012");
		dif = DateUtils.getDaysDifference(dateTwo, dateOne);
		Assertions.assertEquals(40978, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getDaysDifference(null, dateTwo);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'getDaysDifference'");
	}


	@Test
	public void testGetMinutesDifference() {
		Date dateOne = DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date dateTwo = DateUtils.toDate("1/4/2009 12:00 PM", DateUtils.DATE_FORMAT_SHORT);

		long dif = DateUtils.getMinutesDifference(dateTwo, dateOne);
		Assertions.assertEquals(5040, dif);

		dif = DateUtils.getMinutesDifference(dateOne, dateTwo);
		Assertions.assertEquals(-5040, dif);

		dateTwo = DateUtils.toDate("1/4/2009 8:45 AM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesDifference(dateTwo, dateOne);
		Assertions.assertEquals(4845, dif);

		dateTwo = DateUtils.toDate("1/2/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesDifference(dateTwo, dateOne);
		Assertions.assertEquals(1440, dif);

		dateOne = DateUtils.toDate("1/1/2009 8:15 AM", DateUtils.DATE_FORMAT_SHORT);
		dateTwo = DateUtils.toDate("1/3/2009 8:15 PM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesDifference(dateTwo, dateOne);
		Assertions.assertEquals(3600, dif);

		dateOne = DateUtils.toDate("1/1/2009 8:15 AM", DateUtils.DATE_FORMAT_SHORT);
		dateTwo = DateUtils.toDate("1/3/2009 8:15 PM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesDifference(dateOne, dateTwo);
		Assertions.assertEquals(-3600, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getMinutesDifference(null, dateOne);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.getMinutesDifference'");
		try {
			DateUtils.getMinutesDifference(dateTwo, null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.getMinutesDifference'");
	}


	@Test
	public void testGetSecondsDifference() {
		Date dateOne = DateUtils.toDate("2009-01-01 00:00:56", DateUtils.DATE_FORMAT_FULL);
		Date dateTwo = DateUtils.toDate("2009-01-04 12:00:00", DateUtils.DATE_FORMAT_FULL);

		long dif = DateUtils.getSecondsDifference(dateTwo, dateOne);
		Assertions.assertEquals(3 * 24 * 60 * 60 + 12 * 60 * 60 - 56, dif);

		dif = DateUtils.getSecondsDifference(dateOne, dateTwo);
		Assertions.assertEquals(-(3 * 24 * 60 * 60 + 12 * 60 * 60 - 56), dif);

		dateOne = DateUtils.toDate("2009-01-01 00:00:00", DateUtils.DATE_FORMAT_FULL);
		dateTwo = DateUtils.toDate("2009-01-01 00:00:56", DateUtils.DATE_FORMAT_FULL);

		dif = DateUtils.getSecondsDifference(dateTwo, dateOne);
		Assertions.assertEquals(56, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getMinutesDifference(dateTwo, null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.getSecondsDifference'");
	}


	@Test
	public void testGetMillisecondsDifference() {
		Date dateOne = DateUtils.toDate("2009-01-01 00:00:00.000", "yyyy-MM-dd HH:mm:ss.SSS");
		Date dateTwo = DateUtils.toDate("2009-01-01 12:00:00.250", "yyyy-MM-dd HH:mm:ss.SSS");

		long dif = DateUtils.getMillisecondsDifference(dateTwo, dateOne);
		Assertions.assertEquals(12 * 60 * 60 * 1000 + 250, dif);

		dif = DateUtils.getMillisecondsDifference(dateOne, dateTwo);
		Assertions.assertEquals(-(12 * 60 * 60 * 1000 + 250), dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getMillisecondsDifference(dateTwo, null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.getMillisecondsDifference'");
	}


	@Test
	public void testGetTimeDifferenceLong() {
		Date d1 = DateUtils.toDate("2010-04-22 12:13:55", DateUtils.DATE_FORMAT_FULL);
		Date d2 = d1;

		Assertions.assertEquals("0 seconds", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("0 seconds", DateUtils.getTimeDifferenceLong(d2, d1));

		d2 = DateUtils.toDate("2010-04-22 12:14:20", DateUtils.DATE_FORMAT_FULL);
		Assertions.assertEquals("25 seconds ", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("25 seconds ", DateUtils.getTimeDifferenceLong(d2, d1));

		d2 = DateUtils.addMinutes(d2, 1);
		Assertions.assertEquals("1 minute 25 seconds ", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("1 minute 25 seconds ", DateUtils.getTimeDifferenceLong(d2, d1));

		d2 = DateUtils.addMinutes(d2, 62);
		Assertions.assertEquals("1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d2, d1));

		d2 = DateUtils.addDays(d2, 9);
		Assertions.assertEquals("9 days 1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("9 days 1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d2, d1));

		d2 = DateUtils.addMonths(d2, 5);
		Assertions.assertEquals("162 days 1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("162 days 1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d2, d1));

		d2 = DateUtils.addYears(d2, 3);
		Assertions.assertEquals("1258 days 1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d1, d2));
		Assertions.assertEquals("1258 days 1 hour 3 minutes 25 seconds ", DateUtils.getTimeDifferenceLong(d2, d1));
	}


	@Test
	public void testGetTimeDifferenceShort() {
		Date d1 = DateUtils.toDate("05/01/2010 9:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Date d2 = d1;

		Assertions.assertEquals("0 seconds", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("0 seconds", DateUtils.getTimeDifferenceShort(d2, d1));

		d2 = DateUtils.addSeconds(d2, 25);
		Assertions.assertEquals("25 seconds ", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("25 seconds ", DateUtils.getTimeDifferenceShort(d2, d1));

		d2 = DateUtils.addMinutes(d2, 1);
		Assertions.assertEquals("1 minute 25 seconds ", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("1 minute 25 seconds ", DateUtils.getTimeDifferenceShort(d2, d1));

		d2 = DateUtils.addMinutes(d2, 62);
		Assertions.assertEquals("1 hour 3 minutes ", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("1 hour 3 minutes ", DateUtils.getTimeDifferenceShort(d2, d1));

		d2 = DateUtils.addDays(d2, 9);
		Assertions.assertEquals("9 days 1 hour ", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("9 days 1 hour ", DateUtils.getTimeDifferenceShort(d2, d1));

		d2 = DateUtils.addMonths(d2, 5);
		Assertions.assertEquals("162 days 1 hour ", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("162 days 1 hour ", DateUtils.getTimeDifferenceShort(d2, d1));

		d2 = DateUtils.addYears(d2, 3);
		Assertions.assertEquals("1258 days 1 hour ", DateUtils.getTimeDifferenceShort(d1, d2));
		Assertions.assertEquals("1258 days 1 hour ", DateUtils.getTimeDifferenceShort(d2, d1));
	}


	@Test
	public void testAddYears() {
		Date date = DateUtils.addYears(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), 1);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("1/1/2010 12:00 AM", DateUtils.DATE_FORMAT_SHORT), true));

		date = DateUtils.addYears(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), 10);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("1/1/2019 12:00 AM", DateUtils.DATE_FORMAT_SHORT), true));

		IllegalArgumentException e = null;
		try {
			DateUtils.addYears(null, -525);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.addYears'");
	}


	@Test
	public void testAddMilliseconds() {
		Date date = DateUtils.addMilliseconds(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), (8 * 60 * 60 + 45 * 60 + 15) * 1000 + 454);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("2009-01-01 08:45:15.454", "yyyy-MM-dd HH:mm:ss.SSS"), true));

		date = DateUtils.addMilliseconds(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), -(8 * 60 * 60 + 45 * 60 + 15) * 1000 - 454);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("2008-12-31 15:14:44.546", "yyyy-MM-dd HH:mm:ss.SSS"), true));

		IllegalArgumentException e = null;
		try {
			DateUtils.addMilliseconds(null, -525);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.addMillisecond'");
	}


	@Test
	public void testAddSeconds() {
		Date date = DateUtils.addSeconds(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), 8 * 60 * 60 + 45 * 60 + 15);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("2009-01-01 08:45:15", DateUtils.DATE_FORMAT_FULL), true));

		date = DateUtils.addSeconds(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), -(8 * 60 * 60 + 45 * 60 + 15));
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("2008-12-31 15:14:45", DateUtils.DATE_FORMAT_FULL), true));

		IllegalArgumentException e = null;
		try {
			DateUtils.addSeconds(null, -525);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.addSeconds'");
	}


	@Test
	public void testAddMinutes() {
		Date date = DateUtils.addMinutes(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), 525);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("1/1/2009 08:45 AM", DateUtils.DATE_FORMAT_SHORT), true));

		date = DateUtils.addMinutes(DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT), -525);
		Assertions.assertEquals(0, DateUtils.compare(date, DateUtils.toDate("12/31/2008 03:15 pM", DateUtils.DATE_FORMAT_SHORT), true));

		IllegalArgumentException e = null;
		try {
			DateUtils.addMinutes(null, -525);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.addMinutes'");
	}


	@Test
	public void testSetTime() {
		Date date = DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);
		int dif = DateUtils.getMinutesFromMidnight(date);
		Assertions.assertEquals(0, dif);

		date = DateUtils.toDate("1/1/2009 1:58 AM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesFromMidnight(date);
		Assertions.assertEquals(118, dif);

		date = DateUtils.toDate("1/1/2009 12:00 PM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesFromMidnight(date);
		Assertions.assertEquals(720, dif);

		date = DateUtils.toDate("1/1/2009 3:43 PM", DateUtils.DATE_FORMAT_SHORT);
		dif = DateUtils.getMinutesFromMidnight(date);
		Assertions.assertEquals(943, dif);

		IllegalArgumentException e = null;
		try {
			DateUtils.getMinutesFromMidnight(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.setTime'");
	}


	@Test
	public void testGetMinutesFromMidnight() {
		Date date = DateUtils.toDate("1/1/2009 12:00 AM", DateUtils.DATE_FORMAT_SHORT);
		Assertions.assertEquals(0, DateUtils.getMinutesFromMidnight(date));

		date = DateUtils.toDate("1/1/2009 8:45 AM", DateUtils.DATE_FORMAT_SHORT);
		Assertions.assertEquals(525, DateUtils.getMinutesFromMidnight(date));

		date = DateUtils.toDate("1/1/2009 3:43 PM", DateUtils.DATE_FORMAT_SHORT);
		Assertions.assertEquals(943, DateUtils.getMinutesFromMidnight(date));

		IllegalArgumentException e = null;
		try {
			DateUtils.getMillisecondsFromMidnight(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.setTime'");
	}


	@Test
	public void testGetMillisecondsFromMidnight() {
		Date date = DateUtils.toDate("1/1/2009 12:00 PM", DateUtils.DATE_FORMAT_SHORT);
		Assertions.assertEquals(12 * 60 * 60 * 1000, DateUtils.getMillisecondsFromMidnight(date));

		date = DateUtils.toDate("1/1/2009 12:00:15.500", "MM/dd/yyyy HH:mm:ss.SSS");
		Assertions.assertEquals(12 * 60 * 60 * 1000 + 15 * 1000 + 500, DateUtils.getMillisecondsFromMidnight(date));

		IllegalArgumentException e = null;
		try {
			DateUtils.getMillisecondsFromMidnight(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.getMillisecondsFromMidnight'");
	}


	@Test
	public void testFromDateSmart() {
		Assertions.assertEquals("2021-12-08 10:41:30", DateUtils.fromDateSmart(DateUtils.toDate("2021-12-08 10:41:30")));
		Assertions.assertEquals("2021-12-08", DateUtils.fromDateSmart(DateUtils.toDate("2021-12-08 00:00:00")));
	}


	@Test
	public void testGetFirstDayOfWeek() {
		Date date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/07/2009"));
		Assertions.assertEquals("11/01/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/08/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/09/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/10/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/11/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/12/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/13/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfWeek(DateUtils.toDate("11/14/2009"));
		Assertions.assertEquals("11/08/2009", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		IllegalArgumentException e = null;
		try {
			DateUtils.setTime(null, new Time(1));
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.setTime'");
	}


	@Test
	public void testGetFirstDayOfYear() {
		Date date = DateUtils.getFirstDayOfYear(DateUtils.toDate("6/15/1900"));
		Assertions.assertEquals("01/01/1900", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		date = DateUtils.getFirstDayOfYear(DateUtils.toDate("7/1/2007"));
		Assertions.assertEquals("01/01/2007", DateUtils.fromDate(date, DateUtils.DATE_FORMAT_INPUT));

		IllegalArgumentException e = null;
		try {
			DateUtils.getFirstDayOfYear(null);
		}
		catch (IllegalArgumentException ex) {
			e = ex;
		}
		Assertions.assertNotNull(e, "Expected error when passing null date into 'DateUtils.getFirstDayOfYear'");
	}


	@Test
	public void testAddDays() {

		Date dateOne = DateUtils.toDate("12/31/1899");
		Date dateTwo = DateUtils.addDays(dateOne, 1);

		Assertions.assertEquals("01/01/1900", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateOne = DateUtils.addDays(dateTwo, -1);
		Assertions.assertEquals("12/31/1899", DateUtils.fromDate(dateOne, DateUtils.DATE_FORMAT_INPUT));

		dateTwo = DateUtils.addDays(dateOne, 3653);
		Assertions.assertEquals("01/01/1910", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateTwo = DateUtils.addDays(dateOne, 39515);
		Assertions.assertEquals("03/09/2008", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateTwo = DateUtils.addDays(dateOne, 39516);
		Assertions.assertEquals("03/10/2008", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateOne = DateUtils.toDate("05/01/2009");
		dateTwo = DateUtils.addDays(dateOne, -365);
		Assertions.assertEquals("05/01/2008", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));
	}


	@Test
	public void testAddWeekDays() {
		Assertions.assertEquals(DateUtils.toDate("01/01/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/01/2015"), 0));
		Assertions.assertEquals(DateUtils.toDate("01/02/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/01/2015"), 1));
		Assertions.assertEquals(DateUtils.toDate("01/05/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/01/2015"), 2));
		Assertions.assertEquals(DateUtils.toDate("01/06/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/01/2015"), 3));
		Assertions.assertEquals(DateUtils.toDate("01/07/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/01/2015"), 4));
		Assertions.assertEquals(DateUtils.toDate("01/08/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/01/2015"), 5));

		Assertions.assertEquals(DateUtils.toDate("01/01/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/08/2015"), -5));
		Assertions.assertEquals(DateUtils.toDate("01/02/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/08/2015"), -4));
		Assertions.assertEquals(DateUtils.toDate("01/05/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/08/2015"), -3));
		Assertions.assertEquals(DateUtils.toDate("01/06/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/08/2015"), -2));
		Assertions.assertEquals(DateUtils.toDate("01/07/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/08/2015"), -1));
		Assertions.assertEquals(DateUtils.toDate("01/08/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/08/2015"), 0));

		Assertions.assertEquals(DateUtils.toDate("01/05/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/03/2015"), 0));
		Assertions.assertEquals(DateUtils.toDate("01/05/2015"), DateUtils.addWeekDays(DateUtils.toDate("01/04/2015"), 0));
	}


	@Test
	public void testAddMonths() {

		Date dateOne = DateUtils.toDate("12/31/1899");
		Date dateTwo = DateUtils.addMonths(dateOne, 1);
		Assertions.assertEquals("01/31/1900", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateTwo = DateUtils.addMonths(dateOne, -1);
		Assertions.assertEquals("11/30/1899", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateTwo = DateUtils.addMonths(dateOne, 2);
		Assertions.assertEquals("02/28/1900", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));

		dateOne = DateUtils.toDate("12/31/1900");
		dateTwo = DateUtils.addMonths(dateOne, 2);
		Assertions.assertEquals("02/28/1901", DateUtils.fromDate(dateTwo, DateUtils.DATE_FORMAT_INPUT));
	}


	@Test
	public void testGetDateInTimeZone() {
		String localTimeZone = "America/Chicago";
		Calendar cal = new GregorianCalendar(TimeZone.getTimeZone(localTimeZone));
		cal.set(Calendar.YEAR, 2011);
		cal.set(Calendar.MONTH, 9);
		cal.set(Calendar.DAY_OF_MONTH, 10);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);

		// 8 AM on 10/10/2011 AT THE CLIFTON GROUP IS...
		cal.set(Calendar.HOUR_OF_DAY, 8);
		Date newYorkTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/New_York");
		Assertions.assertEquals("10/10/2011 9:00 AM", DateUtils.fromDate(newYorkTime, DateUtils.DATE_FORMAT_SHORT));
		Date laTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/Los_Angeles");
		Assertions.assertEquals("10/10/2011 6:00 AM", DateUtils.fromDate(laTime, DateUtils.DATE_FORMAT_SHORT));
		Date sydneyTime = DateUtils.getDateInTimeZone(cal.getTime(), "Australia/Sydney");
		Assertions.assertEquals("10/11/2011 12:00 AM", DateUtils.fromDate(sydneyTime, DateUtils.DATE_FORMAT_SHORT));
		Date japanTime = DateUtils.getDateInTimeZone(cal.getTime(), "Japan");
		Assertions.assertEquals("10/10/2011 10:00 PM", DateUtils.fromDate(japanTime, DateUtils.DATE_FORMAT_SHORT));
		Date londonTime = DateUtils.getDateInTimeZone(cal.getTime(), "Europe/London");
		Assertions.assertEquals("10/10/2011 2:00 PM", DateUtils.fromDate(londonTime, DateUtils.DATE_FORMAT_SHORT));

		// 12 PM on 10/10/2011 AT THE CLIFTON GROUP IS...
		cal.set(Calendar.HOUR_OF_DAY, 12);
		newYorkTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/New_York");
		Assertions.assertEquals("10/10/2011 1:00 PM", DateUtils.fromDate(newYorkTime, DateUtils.DATE_FORMAT_SHORT));
		laTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/Los_Angeles");
		Assertions.assertEquals("10/10/2011 10:00 AM", DateUtils.fromDate(laTime, DateUtils.DATE_FORMAT_SHORT));
		sydneyTime = DateUtils.getDateInTimeZone(cal.getTime(), "Australia/Sydney");
		Assertions.assertEquals("10/11/2011 4:00 AM", DateUtils.fromDate(sydneyTime, DateUtils.DATE_FORMAT_SHORT));
		japanTime = DateUtils.getDateInTimeZone(cal.getTime(), "Japan");
		Assertions.assertEquals("10/11/2011 2:00 AM", DateUtils.fromDate(japanTime, DateUtils.DATE_FORMAT_SHORT));
		londonTime = DateUtils.getDateInTimeZone(cal.getTime(), "Europe/London");
		Assertions.assertEquals("10/10/2011 6:00 PM", DateUtils.fromDate(londonTime, DateUtils.DATE_FORMAT_SHORT));

		// 5 PM on 10/10/2011 AT THE CLIFTON GROUP IS...
		cal.set(Calendar.HOUR_OF_DAY, 17);
		newYorkTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/New_York");
		Assertions.assertEquals("10/10/2011 6:00 PM", DateUtils.fromDate(newYorkTime, DateUtils.DATE_FORMAT_SHORT));
		laTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/Los_Angeles");
		Assertions.assertEquals("10/10/2011 3:00 PM", DateUtils.fromDate(laTime, DateUtils.DATE_FORMAT_SHORT));
		sydneyTime = DateUtils.getDateInTimeZone(cal.getTime(), "Australia/Sydney");
		Assertions.assertEquals("10/11/2011 9:00 AM", DateUtils.fromDate(sydneyTime, DateUtils.DATE_FORMAT_SHORT));
		japanTime = DateUtils.getDateInTimeZone(cal.getTime(), "Japan");
		Assertions.assertEquals("10/11/2011 7:00 AM", DateUtils.fromDate(japanTime, DateUtils.DATE_FORMAT_SHORT));
		londonTime = DateUtils.getDateInTimeZone(cal.getTime(), "Europe/London");
		Assertions.assertEquals("10/10/2011 11:00 PM", DateUtils.fromDate(londonTime, DateUtils.DATE_FORMAT_SHORT));

		// 9 PM on 10/10/2011 AT THE CLIFTON GROUP IS...
		cal.set(Calendar.HOUR_OF_DAY, 21);
		newYorkTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/New_York");
		Assertions.assertEquals("10/10/2011 10:00 PM", DateUtils.fromDate(newYorkTime, DateUtils.DATE_FORMAT_SHORT));
		laTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/Los_Angeles");
		Assertions.assertEquals("10/10/2011 7:00 PM", DateUtils.fromDate(laTime, DateUtils.DATE_FORMAT_SHORT));
		sydneyTime = DateUtils.getDateInTimeZone(cal.getTime(), "Australia/Sydney");
		Assertions.assertEquals("10/11/2011 1:00 PM", DateUtils.fromDate(sydneyTime, DateUtils.DATE_FORMAT_SHORT));
		japanTime = DateUtils.getDateInTimeZone(cal.getTime(), "Japan");
		Assertions.assertEquals("10/11/2011 11:00 AM", DateUtils.fromDate(japanTime, DateUtils.DATE_FORMAT_SHORT));
		londonTime = DateUtils.getDateInTimeZone(cal.getTime(), "Europe/London");
		Assertions.assertEquals("10/11/2011 3:00 AM", DateUtils.fromDate(londonTime, DateUtils.DATE_FORMAT_SHORT));

		// 12 AM on 10/11/2011 AT THE CLIFTON GROUP IS...
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.DAY_OF_MONTH, 11);
		newYorkTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/New_York");
		Assertions.assertEquals("10/11/2011 1:00 AM", DateUtils.fromDate(newYorkTime, DateUtils.DATE_FORMAT_SHORT));
		laTime = DateUtils.getDateInTimeZone(cal.getTime(), "America/Los_Angeles");
		Assertions.assertEquals("10/10/2011 10:00 PM", DateUtils.fromDate(laTime, DateUtils.DATE_FORMAT_SHORT));
		sydneyTime = DateUtils.getDateInTimeZone(cal.getTime(), "Australia/Sydney");
		Assertions.assertEquals("10/11/2011 4:00 PM", DateUtils.fromDate(sydneyTime, DateUtils.DATE_FORMAT_SHORT));
		japanTime = DateUtils.getDateInTimeZone(cal.getTime(), "Japan");
		Assertions.assertEquals("10/11/2011 2:00 PM", DateUtils.fromDate(japanTime, DateUtils.DATE_FORMAT_SHORT));
		londonTime = DateUtils.getDateInTimeZone(cal.getTime(), "Europe/London");
		Assertions.assertEquals("10/11/2011 6:00 AM", DateUtils.fromDate(londonTime, DateUtils.DATE_FORMAT_SHORT));
	}


	@Test
	public void testIsWeekday() {
		// 12/5/2011 is Monday
		Assertions.assertFalse(DateUtils.isWeekday(null));
		Assertions.assertTrue(DateUtils.isWeekday(DateUtils.toDate("12/5/2011")));
		Assertions.assertTrue(DateUtils.isWeekday(DateUtils.toDate("12/6/2011")));
		Assertions.assertTrue(DateUtils.isWeekday(DateUtils.toDate("12/7/2011")));
		Assertions.assertTrue(DateUtils.isWeekday(DateUtils.toDate("12/8/2011")));
		Assertions.assertTrue(DateUtils.isWeekday(DateUtils.toDate("12/9/2011")));
		Assertions.assertFalse(DateUtils.isWeekday(DateUtils.toDate("12/10/2011")));
		Assertions.assertFalse(DateUtils.isWeekday(DateUtils.toDate("12/11/2011")));
	}


	@Test
	public void testIsWeekend() {
		// 12/5/2011 is Monday
		Assertions.assertFalse(DateUtils.isWeekend(null));
		Assertions.assertFalse(DateUtils.isWeekend(DateUtils.toDate("12/5/2011")));
		Assertions.assertFalse(DateUtils.isWeekend(DateUtils.toDate("12/6/2011")));
		Assertions.assertFalse(DateUtils.isWeekend(DateUtils.toDate("12/7/2011")));
		Assertions.assertFalse(DateUtils.isWeekend(DateUtils.toDate("12/8/2011")));
		Assertions.assertFalse(DateUtils.isWeekend(DateUtils.toDate("12/9/2011")));
		Assertions.assertTrue(DateUtils.isWeekend(DateUtils.toDate("12/10/2011")));
		Assertions.assertTrue(DateUtils.isWeekend(DateUtils.toDate("12/11/2011")));
	}


	@Test
	public void testIsFriday() {
		// 1/2/2015 is Friday
		Assertions.assertFalse(DateUtils.isFriday(null));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/1/2015")));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/3/2015")));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/4/2015")));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/5/2015")));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/6/2015")));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/7/2015")));
		Assertions.assertFalse(DateUtils.isFriday(DateUtils.toDate("1/8/2015")));
		Assertions.assertTrue(DateUtils.isFriday(DateUtils.toDate("1/2/2015")));
		Assertions.assertTrue(DateUtils.isFriday(DateUtils.toDate("1/9/2015")));
	}


	@Test
	public void testIsSaturday() {
		// 1/3/2015 is Friday
		Assertions.assertFalse(DateUtils.isSaturday(null));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/2/2015")));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/4/2015")));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/5/2015")));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/6/2015")));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/7/2015")));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/8/2015")));
		Assertions.assertFalse(DateUtils.isSaturday(DateUtils.toDate("1/9/2015")));
		Assertions.assertTrue(DateUtils.isSaturday(DateUtils.toDate("1/3/2015")));
		Assertions.assertTrue(DateUtils.isSaturday(DateUtils.toDate("1/10/2015")));
	}


	@Test
	public void testGetPreviousWeekday() {
		// 12/5/2011 is Monday
		Assertions.assertEquals(DateUtils.toDate("12/2/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/5/2011")));
		Assertions.assertEquals(DateUtils.toDate("12/5/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/6/2011")));
		Assertions.assertEquals(DateUtils.toDate("12/6/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/7/2011")));
		Assertions.assertEquals(DateUtils.toDate("12/7/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/8/2011")));
		Assertions.assertEquals(DateUtils.toDate("12/8/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/9/2011")));
		Assertions.assertEquals(DateUtils.toDate("12/9/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/10/2011")));
		Assertions.assertEquals(DateUtils.toDate("12/9/2011"), DateUtils.getPreviousWeekday(DateUtils.toDate("12/11/2011")));
	}


	@Test
	public void testFromDateRange() {
		Date startDate = null;
		Date endDate = null;

		Assertions.assertEquals("", DateUtils.fromDateRange(startDate, endDate, true));
		Assertions.assertEquals(" - ", DateUtils.fromDateRange(startDate, endDate, false));
		Assertions.assertEquals("", DateUtils.fromDateRange(startDate, endDate, false, true));
		Assertions.assertEquals("( - )", DateUtils.fromDateRange(startDate, endDate, true, false));
		Assertions.assertEquals(" - ", DateUtils.fromDateRange(startDate, endDate, false, false));

		startDate = DateUtils.toDate("05/01/2013");
		Assertions.assertEquals("05/01/2013 - ", DateUtils.fromDateRange(startDate, endDate, true));
		Assertions.assertEquals("05/01/2013 - ", DateUtils.fromDateRange(startDate, endDate, false, true));
		Assertions.assertEquals("(05/01/2013 - )", DateUtils.fromDateRange(startDate, endDate, true, false));
		Assertions.assertEquals("05/01/2013 - ", DateUtils.fromDateRange(startDate, endDate, false));

		endDate = DateUtils.toDate("08/01/2013");
		Assertions.assertEquals("05/01/2013 - 08/01/2013", DateUtils.fromDateRange(startDate, endDate, true));
		Assertions.assertEquals("05/01/2013 - 08/01/2013", DateUtils.fromDateRange(startDate, endDate, false, true));
		Assertions.assertEquals("(05/01/2013 - 08/01/2013)", DateUtils.fromDateRange(startDate, endDate, true, false));
		Assertions.assertEquals("05/01/2013 - 08/01/2013", DateUtils.fromDateRange(startDate, endDate, false));

		startDate = null;
		Assertions.assertEquals(" - 08/01/2013", DateUtils.fromDateRange(startDate, endDate, true));
		Assertions.assertEquals(" - 08/01/2013", DateUtils.fromDateRange(startDate, endDate, false, true));
		Assertions.assertEquals("( - 08/01/2013)", DateUtils.fromDateRange(startDate, endDate, true, false));
		Assertions.assertEquals(" - 08/01/2013", DateUtils.fromDateRange(startDate, endDate, false));
	}


	@Test
	public void testGetDateListForRange() {
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/01/2011"), DateUtils.toDate("01/01/2011")), "01/01/2011");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/01/2011"), DateUtils.toDate("01/02/2011")), "01/01/2011", "01/02/2011");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/02/2011"), DateUtils.toDate("01/01/2011")));
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/01/2011"), DateUtils.toDate("01/05/2011")), "01/01/2011", "01/02/2011", "01/03/2011", "01/04/2011", "01/05/2011");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/05/2011"), DateUtils.toDate("01/01/2011")));
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/05/2011"), DateUtils.toDate("01/10/2011")), "01/05/2011", "01/06/2011", "01/07/2011", "01/08/2011", "01/09/2011", "01/10/2011");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("01/25/2011"), DateUtils.toDate("02/05/2011")), "01/25/2011", "01/26/2011", "01/27/2011", "01/28/2011", "01/29/2011", "01/30/2011", "01/31/2011", "02/01/2011", "02/02/2011", "02/03/2011", "02/04/2011", "02/05/2011");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("02/25/2011"), DateUtils.toDate("03/05/2011")), "02/25/2011", "02/26/2011", "02/27/2011", "02/28/2011", "03/01/2011", "03/02/2011", "03/03/2011", "03/04/2011", "03/05/2011");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("02/25/2012"), DateUtils.toDate("03/05/2012")), "02/25/2012", "02/26/2012", "02/27/2012", "02/28/2012", "02/29/2012", "03/01/2012", "03/02/2012", "03/03/2012", "03/04/2012", "03/05/2012");
		validateGeneratedDateRange(DateUtils.getDateListForRange(DateUtils.toDate("12/25/2011"), DateUtils.toDate("01/05/2012")), "12/25/2011", "12/26/2011", "12/27/2011", "12/28/2011", "12/29/2011", "12/30/2011", "12/31/2011", "01/01/2012", "01/02/2012", "01/03/2012", "01/04/2012", "01/05/2012");
	}


	@Test
	public void testWeekdayListForRange() {
		List<Date> dateList = DateUtils.getWeekdayListForRange(DateUtils.toDate("12/2/2011"), DateUtils.toDate("12/2/2011"));
		validateGeneratedDateRange(dateList, "12/02/2011");

		dateList = DateUtils.getWeekdayListForRange(DateUtils.toDate("12/2/2011"), DateUtils.toDate("12/10/2011"));
		validateGeneratedDateRange(dateList, "12/02/2011,12/05/2011,12/06/2011,12/07/2011,12/08/2011,12/09/2011");

		dateList = DateUtils.getWeekdayListForRange(DateUtils.toDate("12/2/2011"), DateUtils.toDate("12/04/2011"));
		validateGeneratedDateRange(dateList, "12/02/2011");

		dateList = DateUtils.getWeekdayListForRange(DateUtils.toDate("9/24/2015"), DateUtils.toDate("10/03/2015"));
		validateGeneratedDateRange(dateList, "09/24/2015,09/25/2015,09/28/2015,09/29/2015,09/30/2015,10/01/2015,10/02/2015");
	}


	@Test
	public void testToDateRangesString() {
		List<Date> dateList = null;
		String dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertNull(dateRangesString);

		dateList = new ArrayList<>();
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertNull(dateRangesString);

		dateList = CollectionUtils.createList((Date) null);
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertNull(dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("10/31/2019"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019", dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("10/31/2019"), DateUtils.toDate("10/31/2019"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019", dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("10/31/2019"), DateUtils.toDate("2019-10-31 08:00:00"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019", dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("11/01/2019"), DateUtils.toDate("2019-11-01 08:00:00"), DateUtils.toDate("2019-11-04 08:00:00"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("11/01/2019 - 11/04/2019", dateRangesString);

		dateRangesString = DateUtils.toDateRangesString(dateList, false, DateUtils::fromDateShort);
		Assertions.assertEquals("11/01/2019, 11/04/2019", dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("10/31/2019"), DateUtils.toDate("11/01/2019"), DateUtils.toDate("11/06/2019"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019 - 11/01/2019, 11/06/2019", dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("10/31/2019"), DateUtils.toDate("11/01/2019"), null, DateUtils.toDate("11/06/2019"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019 - 11/01/2019, 11/06/2019", dateRangesString);

		dateList = CollectionUtils.createList(DateUtils.toDate("10/31/2019"), DateUtils.toDate("11/01/2019"), DateUtils.toDate("11/06/2019"));
		dateRangesString = DateUtils.toDateRangesString(dateList, false, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019 - 11/01/2019, 11/06/2019", dateRangesString);

		dateList = DateUtils.getWeekdayListForRange(DateUtils.toDate("10/31/2019"), DateUtils.toDate("11/06/2019"));
		dateRangesString = DateUtils.toDateRangesString(dateList, true, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019 - 11/06/2019", dateRangesString);

		dateRangesString = DateUtils.toDateRangesString(dateList, false, DateUtils::fromDateShort);
		Assertions.assertEquals("10/31/2019 - 11/01/2019, 11/04/2019 - 11/06/2019", dateRangesString);
	}


	@Test
	public void testIsWithinInterval() {
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(10, 0), "10:00 AM - 10:30 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(10, 30), "10:00 AM - 10:30 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(10, 15), "10:00 AM - 10:30 AM"));
		Assertions.assertFalse(DateUtils.isWithinInterval(LocalTime.of(9, 59), "10:00 AM - 10:30 AM"));
		Assertions.assertFalse(DateUtils.isWithinInterval(LocalTime.of(10, 31), "10:00 AM - 10:30 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(21, 15), "9:00 PM - 9:30 PM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(1, 0), "11:30 PM - 1:00 AM"));
		Assertions.assertFalse(DateUtils.isWithinInterval(LocalTime.of(2, 30), "3:00 AM - 2:00 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(1, 0), "12:00 AM - 2:00 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(2, 0), "12:00 AM  - 3:00 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(0, 0), "11:59 PM - 12:00 AM"));
		Assertions.assertTrue(DateUtils.isWithinInterval(LocalTime.of(0, 0), "12:00 AM - 12:00 AM"));
	}


	@Test
	public void testIsWithinAnyInterval() {
		List<String> intervals = Arrays.asList("10:00 AM - 10:30 AM", "9:00 PM - 9:30 PM", "11:00 PM - 1:00 PM", "12:00 AM - 2:00 AM", "11:59 PM - 12:00 AM", "12:00 AM - 12:00 AM");
		Assertions.assertFalse(DateUtils.isWithinAnyInterval(LocalTime.of(22, 0), intervals));
		Assertions.assertTrue(DateUtils.isWithinAnyInterval(LocalTime.of(0, 0), intervals));
	}


	@Test
	public void testGetWithinIntervalList() {
		List<String> intervals = Arrays.asList("10:00 AM - 10:30 AM", "9:00 PM - 9:30 PM", "11:00 PM - 1:00 PM", "12:00 AM - 2:00 AM", "11:59 PM - 12:00 AM", "12:00 AM - 12:00 AM");
		Assertions.assertTrue(DateUtils.getWithinIntervalList(LocalTime.of(22, 0), intervals).isEmpty());
		List<String> foundIntervals = DateUtils.getWithinIntervalList(LocalTime.of(0, 0), intervals);
		Assertions.assertEquals(4, foundIntervals.size());
		MatcherAssert.assertThat(foundIntervals.toString(), IsEqual.equalTo("[11:00 PM - 1:00 PM, 12:00 AM - 2:00 AM, 11:59 PM - 12:00 AM, 12:00 AM - 12:00 AM]"));
	}


	@Test
	public void testGetQuarter() {
		Assertions.assertEquals(1, DateUtils.getQuarter(DateUtils.toDate("1/1/2016")));
		Assertions.assertEquals(1, DateUtils.getQuarter(DateUtils.toDate("2/12/2016")));
		Assertions.assertEquals(1, DateUtils.getQuarter(DateUtils.toDate("3/31/2016")));
		Assertions.assertNotEquals(2, DateUtils.getQuarter(DateUtils.toDate("1/1/2016")));
		Assertions.assertNotEquals(3, DateUtils.getQuarter(DateUtils.toDate("2/12/2016")));
		Assertions.assertNotEquals(4, DateUtils.getQuarter(DateUtils.toDate("2/12/2016")));

		Assertions.assertEquals(2, DateUtils.getQuarter(DateUtils.toDate("4/1/2016")));
		Assertions.assertEquals(2, DateUtils.getQuarter(DateUtils.toDate("5/12/2016")));
		Assertions.assertEquals(2, DateUtils.getQuarter(DateUtils.toDate("6/30/2016")));
		Assertions.assertNotEquals(1, DateUtils.getQuarter(DateUtils.toDate("4/1/2016")));
		Assertions.assertNotEquals(3, DateUtils.getQuarter(DateUtils.toDate("5/12/2016")));
		Assertions.assertNotEquals(4, DateUtils.getQuarter(DateUtils.toDate("6/30/2016")));

		Assertions.assertEquals(3, DateUtils.getQuarter(DateUtils.toDate("7/1/2016")));
		Assertions.assertEquals(3, DateUtils.getQuarter(DateUtils.toDate("8/15/2016")));
		Assertions.assertEquals(3, DateUtils.getQuarter(DateUtils.toDate("9/30/2016")));
		Assertions.assertNotEquals(4, DateUtils.getQuarter(DateUtils.toDate("7/1/2016")));
		Assertions.assertNotEquals(1, DateUtils.getQuarter(DateUtils.toDate("8/15/2016")));
		Assertions.assertNotEquals(2, DateUtils.getQuarter(DateUtils.toDate("9/30/2016")));

		Assertions.assertEquals(4, DateUtils.getQuarter(DateUtils.toDate("10/1/2016")));
		Assertions.assertEquals(4, DateUtils.getQuarter(DateUtils.toDate("11/11/2016")));
		Assertions.assertEquals(4, DateUtils.getQuarter(DateUtils.toDate("12/31/2016")));
		Assertions.assertNotEquals(1, DateUtils.getQuarter(DateUtils.toDate("10/1/2016")));
		Assertions.assertNotEquals(2, DateUtils.getQuarter(DateUtils.toDate("11/11/2016")));
		Assertions.assertNotEquals(3, DateUtils.getQuarter(DateUtils.toDate("12/31/2016")));
	}


	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDateUtilFunctions() {
		// 12/5/2011 is Monday
		Date testDate = DateUtils.toDate("12/05/2011");
		Assertions.assertEquals(DateUtils.toDate("12/31/2011"), DateUtilFunctions.MONTH_END.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/01/2011"), DateUtilFunctions.MONTH_START.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/30/2011"), DateUtilFunctions.MONTH_WEEKDAY_END.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/01/2011"), DateUtilFunctions.MONTH_WEEKDAY_START.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/06/2011"), DateUtilFunctions.NEXT_WEEKDAY.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("11/30/2011"), DateUtilFunctions.PREVIOUS_MONTH_END.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("11/30/2011"), DateUtilFunctions.PREVIOUS_MONTH_WEEKDAY_END.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/02/2011"), DateUtilFunctions.PREVIOUS_WEEKDAY.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/31/2011"), DateUtilFunctions.QUARTER_END.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("10/01/2011"), DateUtilFunctions.QUARTER_START.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("12/31/2011"), DateUtilFunctions.YEAR_END.evaluateDate(testDate));
		Assertions.assertEquals(DateUtils.toDate("01/01/2011"), DateUtilFunctions.YEAR_START.evaluateDate(testDate));
	}


	@Test
	public void testAsUtilDate() {
		String df = DateUtils.DATE_FORMAT_FULL;
		Assertions.assertEquals(DateUtils.toDate("2011-12-05 00:00:00", df), DateUtils.asUtilDate(LocalDate.of(2011, 12, 5)));
		Assertions.assertEquals(DateUtils.toDate("2025-01-01 00:00:00", df), DateUtils.asUtilDate(LocalDate.of(2025, 1, 1)));
		Assertions.assertEquals(DateUtils.toDate("2050-12-31 00:00:00", df), DateUtils.asUtilDate(LocalDate.of(2050, 12, 31)));
		Assertions.assertEquals(DateUtils.toDate("2011-12-05 15:39:00", df), DateUtils.asUtilDate(LocalDateTime.of(2011, 12, 5, 15, 39, 0)));
		Assertions.assertEquals(DateUtils.toDate("2025-01-01 12:34:56", df), DateUtils.asUtilDate(LocalDateTime.of(2025, 1, 1, 12, 34, 56)));
		Assertions.assertEquals(DateUtils.toDate("2050-12-31 23:45:12", df), DateUtils.asUtilDate(LocalDateTime.of(2050, 12, 31, 23, 45, 12)));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void validateGeneratedDateRange(List<Date> dateList, String... commaDelimitedDateResults) {
		String[] dateResults = Arrays.stream(commaDelimitedDateResults)
				.map(commaDelimitedList -> commaDelimitedList.split(","))
				.flatMap(Arrays::stream)
				.toArray(String[]::new);
		ValidationUtils.assertEquals(dateList.size(), dateResults.length, "Date list for range failure. Expected [" + dateResults.length + "] dates but generated [" + dateList.size() + "]");

		for (int i = 0; i < dateResults.length; i++) {
			ValidationUtils.assertEquals(DateUtils.fromDateShort(dateList.get(i)), dateResults[i], "Failure: Expected dates and order not equal. Expected [" + dateResults[i] + "], generated [" + DateUtils.fromDateShort(dateList.get(i)) + "]");
		}
	}


	private void validateToDate(String actualDate, String expectedDate, String format) {
		Date d = null;
		Throwable error = null;
		try {
			d = DateUtils.toDate(actualDate);
		}
		catch (Throwable e) {
			error = e;
		}
		Assertions.assertNull(error);
		Assertions.assertNotNull(d);
		Assertions.assertEquals(expectedDate, DateUtils.fromDate(d, format));
	}


	private void validateToDateInvalid(String actualDate) {
		Date d = null;
		Throwable error = null;
		try {
			d = DateUtils.toDate(actualDate);
		}
		catch (Throwable e) {
			error = e;
		}
		Assertions.assertNotNull(error);
		Assertions.assertNull(d);
		Assertions.assertEquals(String.format("Cannot parse date field '%s' using formats: [MM/dd/yyyy, yyyy-MM-dd HH:mm:ss]", actualDate), error.getMessage());
	}
}
