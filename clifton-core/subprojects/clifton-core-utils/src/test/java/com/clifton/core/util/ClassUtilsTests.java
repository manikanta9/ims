package com.clifton.core.util;

import com.clifton.core.util.dataaccess.PagingList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The {@link ClassUtilsTests} contains tests for the {@link ClassUtils} class
 *
 * @author manderson
 */
public class ClassUtilsTests {


	@Test
	public void testGetFiles() {
		// One Matching File
		String directory = "src/main/java/com/clifton/core/util";
		List<?> classFiles = ClassUtils.getClassFiles(directory, ClassUtils.class);
		Assertions.assertEquals(1, CollectionUtils.getSize(classFiles));

		// No Matching Files
		directory = "src/main/java/com/clifton/core/util";
		classFiles = ClassUtils.getClassFiles(directory, String.class);
		Assertions.assertTrue(CollectionUtils.isEmpty(classFiles));

		// Directory Missing - i.e. No Matching Files
		directory = "src/main/java/do/not/exist";
		classFiles = ClassUtils.getClassFiles(directory, Object.class);
		Assertions.assertTrue(CollectionUtils.isEmpty(classFiles));

		// 2 Matching Classes for Interface
		directory = "src/main/java/com/clifton/core/util/dataaccess";
		classFiles = ClassUtils.getClassFiles(directory, PagingList.class);
		Assertions.assertEquals(2, CollectionUtils.getSize(classFiles));
	}


	@Test
	public void testGetClassFields() {
		Class<?> clazz = TestClassChild2.class;
		{
			Field[] fields = ClassUtils.getClassFields(clazz, false, false);
			Assertions.assertEquals(2, fields.length, () -> String.format("Found fields: [%s]", Arrays.stream(fields).map(Field::getName).collect(Collectors.joining(", "))));
		}
		{
			Field[] fields = ClassUtils.getClassFields(clazz, false, true);
			Assertions.assertEquals(5, fields.length, () -> String.format("Found fields: [%s]", Arrays.stream(fields).map(Field::getName).collect(Collectors.joining(", "))));
		}
		{
			Field[] fields = ClassUtils.getClassFields(clazz, true, false);
			Assertions.assertEquals(6, fields.length, () -> String.format("Found fields: [%s]", Arrays.stream(fields).map(Field::getName).collect(Collectors.joining(", "))));
		}
		{
			// Parent type final and transient fields are not included
			Field[] fields = ClassUtils.getClassFields(clazz, true, true);
			Assertions.assertEquals(9, fields.length, () -> String.format("Found fields: [%s]", Arrays.stream(fields).map(Field::getName).collect(Collectors.joining(", "))));
		}
	}


	@Test
	@SuppressWarnings("ConstantConditions")
	public void testGetClassForObject() {
		Assertions.assertNull(ClassUtils.getClassForObject(null));
		Assertions.assertEquals(Integer.class, ClassUtils.getClassForObject(1));
		Assertions.assertEquals(Long.class, ClassUtils.getClassForObject(1L));
		Assertions.assertEquals(Short.class, ClassUtils.getClassForObject((short) 1));
		Assertions.assertEquals(Object.class, ClassUtils.getClassForObject(new Object()));
		Assertions.assertEquals(String.class, ClassUtils.getClassForObject("myString"));
		Assertions.assertEquals(ArrayList.class, ClassUtils.getClassForObject(new ArrayList<>()));
	}


	@Test
	public void testGetClassNameForObject() {
		Assertions.assertNull(ClassUtils.getClassNameForObject(null));
		Assertions.assertEquals("java.lang.Integer", ClassUtils.getClassNameForObject(1));
		Assertions.assertEquals("java.lang.Long", ClassUtils.getClassNameForObject(1L));
		Assertions.assertEquals("java.lang.Short", ClassUtils.getClassNameForObject((short) 1));
		Assertions.assertEquals("java.lang.Object", ClassUtils.getClassNameForObject(new Object()));
		Assertions.assertEquals("java.lang.String", ClassUtils.getClassNameForObject("myString"));
		Assertions.assertEquals("java.util.ArrayList", ClassUtils.getClassNameForObject(new ArrayList<>()));
	}


	@Test
	public void testIsDefaultConstructorPresent() {
		Assertions.assertTrue(ClassUtils.isDefaultConstructorPresent(ClassUtilsTests.class));
		Assertions.assertFalse(ClassUtils.isDefaultConstructorPresent(NoDefaultConstructorClass.class));
	}


	@Test
	public void testGetClasspathJarList() {
		List<String> classpathJarList = ClassUtils.getClasspathElementList();
		Assertions.assertTrue(classpathJarList.size() > 1, String.format("The class path JAR list of size [%d] is smaller than the expected size [%d]. Class path JARs: %s", classpathJarList.size(), 1, CollectionUtils.toString(classpathJarList, 10)));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testWrapperToPrimitive() {
		Assertions.assertEquals(short.class, ClassUtils.wrapperToPrimitive(Short.class));
		Assertions.assertEquals(int.class, ClassUtils.wrapperToPrimitive(Integer.class));
		Assertions.assertEquals(long.class, ClassUtils.wrapperToPrimitive(Long.class));
		Assertions.assertEquals(float.class, ClassUtils.wrapperToPrimitive(Float.class));
		Assertions.assertEquals(double.class, ClassUtils.wrapperToPrimitive(Double.class));
		Assertions.assertEquals(boolean.class, ClassUtils.wrapperToPrimitive(Boolean.class));
		Assertions.assertEquals(char.class, ClassUtils.wrapperToPrimitive(Character.class));
		Assertions.assertEquals(byte.class, ClassUtils.wrapperToPrimitive(Byte.class));
		Assertions.assertEquals(void.class, ClassUtils.wrapperToPrimitive(Void.class));
		Assertions.assertNull(ClassUtils.wrapperToPrimitive(String.class));
		Assertions.assertNull(ClassUtils.wrapperToPrimitive(Object.class));
	}


	@Test
	public void testPrimitiveToWrapper() {
		Assertions.assertEquals(Short.class, ClassUtils.primitiveToWrapperIfNecessary(short.class));
		Assertions.assertEquals(Integer.class, ClassUtils.primitiveToWrapperIfNecessary(int.class));
		Assertions.assertEquals(Long.class, ClassUtils.primitiveToWrapperIfNecessary(long.class));
		Assertions.assertEquals(Float.class, ClassUtils.primitiveToWrapperIfNecessary(float.class));
		Assertions.assertEquals(Double.class, ClassUtils.primitiveToWrapperIfNecessary(double.class));
		Assertions.assertEquals(Boolean.class, ClassUtils.primitiveToWrapperIfNecessary(boolean.class));
		Assertions.assertEquals(Character.class, ClassUtils.primitiveToWrapperIfNecessary(char.class));
		Assertions.assertEquals(Byte.class, ClassUtils.primitiveToWrapperIfNecessary(byte.class));
		Assertions.assertEquals(Void.class, ClassUtils.primitiveToWrapperIfNecessary(void.class));
		Assertions.assertEquals(String.class, ClassUtils.primitiveToWrapperIfNecessary(String.class));
		Assertions.assertEquals(Object.class, ClassUtils.primitiveToWrapperIfNecessary(Object.class));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	private static class TestClassRoot {

		private String field1;
		private int field2;
		private final boolean field3 = true;
		private final transient boolean field4 = true;
		private transient boolean field5;
	}

	@SuppressWarnings("unused")
	private static class TestClassChild1 extends TestClassRoot {

		private String field6;
		private int field7;
		private final boolean field8 = true;
		private final transient boolean field9 = true;
		private transient boolean field10;
	}

	@SuppressWarnings("unused")
	private static class TestClassChild2 extends TestClassChild1 {

		private String field11;
		private int field12;
		private final boolean field13 = true;
		private final transient boolean field14 = true;
		private transient boolean field15;
	}
}
