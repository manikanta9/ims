package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author vgomelsky
 */
public class ExceptionUtilsTests {

	@Test
	public void testGetDetailedMessage() {
		Throwable e = null;
		Assertions.assertEquals("ERROR GETTING MESSAGE: Argument Throwable is NULL", ExceptionUtils.getDetailedMessage(e));

		e = new RuntimeException();
		Assertions.assertEquals("NO MESSAGE FOR class java.lang.RuntimeException", ExceptionUtils.getDetailedMessage(e));

		e = new RuntimeException("First Error");
		Assertions.assertEquals("First Error", ExceptionUtils.getDetailedMessage(e));

		e = new RuntimeException("Higher Error", e);
		Assertions.assertEquals("Higher Error\nCAUSED BY First Error", ExceptionUtils.getDetailedMessage(e));
	}


	@Test
	public void testGetDetailedMessageReversed() {
		Throwable e = null;
		Assertions.assertEquals("ERROR GETTING MESSAGE: Argument Throwable is NULL", ExceptionUtils.getDetailedMessageReversed(e));

		e = new RuntimeException();
		Assertions.assertEquals("NO MESSAGE FOR class java.lang.RuntimeException", ExceptionUtils.getDetailedMessageReversed(e));

		e = new RuntimeException("First Error");
		Assertions.assertEquals("First Error", ExceptionUtils.getDetailedMessageReversed(e));

		e = new RuntimeException("Higher Error", e);
		Assertions.assertEquals("First Error\nCAUSED Higher Error", ExceptionUtils.getDetailedMessageReversed(e));
	}
}
