package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The test class for {@link MapUtils}.
 *
 * @author MikeH
 */
@SuppressWarnings({"ArraysAsListWithZeroOrOneArgument", "SimplifiableJUnitAssertion", "ConstantConditions"})
public class MapUtilsTests {

	@Test
	public void testGetConverted() {
		Assertions.assertEquals(Arrays.asList(), MapUtils.getConverted(null, null));
		Assertions.assertEquals(Arrays.asList(), MapUtils.getConverted(map(), null));
		Assertions.assertEquals(Arrays.asList(null, null, null), MapUtils.getConverted(map(entry(1, 1), entry(2, 2), entry(3, 3)), (key, value) -> null));
		Assertions.assertEquals(Arrays.asList(null, 1), MapUtils.getConverted(map(entry(null, 1), entry(1, null), entry(null, null)), (key, value) -> key));
		Assertions.assertEquals(Arrays.asList("1", "2", "3"), MapUtils.getConverted(map(entry("1", 1), entry("2", 2), entry("3", 3)), (key, value) -> key));
		Assertions.assertEquals(Arrays.asList(1, 2, 3), MapUtils.getConverted(map(entry("1", 1), entry("2", 2), entry("3", 3)), (key, value) -> value));
		Assertions.assertEquals(Arrays.asList(2, 4, 6), MapUtils.getConverted(map(entry(1, 1), entry(2, 2), entry(3, 3)), Integer::sum));
		Assertions.assertEquals(Arrays.asList(6, 4, 2), MapUtils.getConverted(map(entry(3, 3), entry(2, 2), entry(1, 1)), Integer::sum));
		Assertions.assertEquals(Arrays.asList(6, 4, 2), MapUtils.getConverted(map(entry(3, 3), entry(2, 2), entry(1, 1)), Integer::sum));
	}


	@Test
	public void testPutIfNotNull() {
		Map<Object, Object> map = map();
		Assertions.assertEquals(false, MapUtils.putIfNotNull(null, 1, 1));
		Assertions.assertEquals(false, MapUtils.putIfNotNull(map, null, 1));
		Assertions.assertEquals(false, MapUtils.putIfNotNull(map, 1, null));
		Assertions.assertEquals(false, MapUtils.putIfNotNull(map, null, null));
		Assertions.assertEquals(map(), map);
		Assertions.assertEquals(true, MapUtils.putIfNotNull(map, 1, 1));
		Assertions.assertEquals(map(entry(1, 1)), map);
		Assertions.assertEquals(true, MapUtils.putIfNotNull(map, 2, 2));
		Assertions.assertEquals(map(entry(1, 1), entry(2, 2)), map);
		Assertions.assertEquals(true, MapUtils.putIfNotNull(map, 1, 2));
		Assertions.assertEquals(map(entry(1, 2), entry(2, 2)), map);
		Assertions.assertEquals(false, MapUtils.putIfNotNull(map, 1, null));
		Assertions.assertEquals(map(entry(1, 2), entry(2, 2)), map);
	}


	@Test
	public void testParseMap() {
		Assertions.assertEquals(null, MapUtils.parseMap(null));
		Assertions.assertEquals(null, MapUtils.parseMap(""));
		Assertions.assertEquals(list(), MapUtils.parseMap(";"));
		Assertions.assertEquals(list(map(entry("1", "1"))), MapUtils.parseMap("1=1"));
		Assertions.assertEquals(list(map(entry("1", "1"))), MapUtils.parseMap("1=1;"));
		Assertions.assertEquals(list(map(entry("", "")), map(entry("1", "1"))), MapUtils.parseMap(";1=1"));
		Assertions.assertEquals(list(map(entry("", "")), map(entry("1", "2"), entry("3", "4"))), MapUtils.parseMap(";1=2,3=4"));
		Assertions.assertEquals(list(map(entry("5", "6")), map(entry("1", "2"), entry("3", "4"))), MapUtils.parseMap("5=6;1=2,3=4"));
		Assertions.assertEquals(list(map(entry("5", "6")), map(entry("", "")), map(entry("1", "2"), entry("3", "4"))), MapUtils.parseMap("5=6;;1=2,3=4;;"));
	}


	@Test
	public void testGetParameterAsInteger() {
		Assertions.assertEquals(null, MapUtils.getParameterAsInteger("a", map()));
		Assertions.assertEquals(null, MapUtils.getParameterAsInteger("a", map(entry("a", null))));
		Assertions.assertEquals(1, (int) MapUtils.getParameterAsInteger("a", map(entry("a", 1))));
		Assertions.assertEquals(1, (int) MapUtils.getParameterAsInteger("a", map(entry("a", "1"))));
		Assertions.assertEquals(100, (int) MapUtils.getParameterAsInteger("a", map(entry("a", "100"))));
	}


	@Test
	public void testGetParameterAsShort() {
		Assertions.assertEquals(null, MapUtils.getParameterAsShort("a", map(), 0));
		Assertions.assertEquals(null, MapUtils.getParameterAsShort("a", map(entry("a", null)), 0));
		Assertions.assertEquals((short) 1, (short) MapUtils.getParameterAsShort("a", map(entry("a", 1)), 0));
		Assertions.assertEquals((short) 1, (short) MapUtils.getParameterAsShort("a", map(entry("a", "1")), 0));
		Assertions.assertEquals((short) 100, (short) MapUtils.getParameterAsShort("a", map(entry("a", "100")), 0));
	}


	@Test
	public void testGetParameterAsLong() {
		Assertions.assertEquals(null, MapUtils.getParameterAsLong("a", map()));
		Assertions.assertEquals(null, MapUtils.getParameterAsLong("a", map(entry("a", null))));
		Assertions.assertEquals(1, (long) MapUtils.getParameterAsLong("a", map(entry("a", 1))));
		Assertions.assertEquals(1, (long) MapUtils.getParameterAsLong("a", map(entry("a", "1"))));
		Assertions.assertEquals(100, (long) MapUtils.getParameterAsLong("a", map(entry("a", "100"))));
	}


	@Test
	public void testGetParameterAsString() {
		Assertions.assertEquals(null, MapUtils.getParameterAsString("a", map()));
		Assertions.assertEquals(null, MapUtils.getParameterAsString("a", map(entry("a", null))));
		Assertions.assertEquals("1", MapUtils.getParameterAsString("a", map(entry("a", 1))));
		Assertions.assertEquals("1", MapUtils.getParameterAsString("a", map(entry("a", "1"))));
		Assertions.assertEquals("1", MapUtils.getParameterAsString("a", map(entry("a", list("1", 2, "3")))));
		Assertions.assertEquals("1", MapUtils.getParameterAsString("a", map(entry("a", list("1", 2, "3", map(entry(4, 5), entry("6", "7")))))));
	}


	@Test
	public void testOf() {
		Object obj = new Object();
		Assertions.assertEquals(map(entry(null, null)), MapUtils.of(null, null));
		Assertions.assertEquals(map(entry(null, 1)), MapUtils.of(null, 1));
		Assertions.assertEquals(map(entry(2, null)), MapUtils.of(2, null));
		Assertions.assertEquals(map(entry(2, 1)), MapUtils.of(2, 1));
		Assertions.assertEquals(map(entry(1L, obj)), MapUtils.of(1L, obj));
		Assertions.assertEquals(map(entry(1, 2)), MapUtils.of(1, 2));
		Assertions.assertEquals(map(entry("a", true)), MapUtils.of("a", true));
		Assertions.assertEquals(map(entry("1", 1)), MapUtils.of("1", 1));
	}


	@Test
	public void testOfEntries() {
		Object obj = new Object();
		Assertions.assertEquals(map(), MapUtils.ofEntries());
		Assertions.assertEquals(map(), MapUtils.ofEntries((Map.Entry<Object, Object>[]) null));
		Assertions.assertEquals(map(entry(1, 2)), MapUtils.ofEntries(MapUtils.entry(1, 2)));
		Assertions.assertEquals(map(entry(1, 2), entry(3, 4)), MapUtils.ofEntries(MapUtils.entry(1, 2), MapUtils.entry(3, 4)));
		Assertions.assertEquals(map(entry(3, 4), entry(1, 2)), MapUtils.ofEntries(MapUtils.entry(1, 2), MapUtils.entry(3, 4)));
		Assertions.assertEquals(map(entry("a", null), entry(1, null), entry("b", true), entry(2, obj)), MapUtils.ofEntries(MapUtils.entry("a", null), MapUtils.entry(1, null), MapUtils.entry("b", true), MapUtils.entry(2, obj)));
	}


	@Test
	public void testOfEntriesFailure() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			Assertions.assertEquals(map(), MapUtils.ofEntries((Map.Entry<?, ?>) null));
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsOneToOne() {
		Assertions.assertTrue(MapUtils.isOneToOne(null));
		Assertions.assertTrue(MapUtils.isOneToOne(map()));
		Assertions.assertTrue(MapUtils.isOneToOne(map(entry(1, 1))));
		Assertions.assertTrue(MapUtils.isOneToOne(map(entry(1, 1), entry(2, 2))));
		Assertions.assertTrue(MapUtils.isOneToOne(map(entry(1, 1), entry(2, "1"))));
		Assertions.assertTrue(MapUtils.isOneToOne(map(entry(1, 1), entry(2, "1"), entry(3, 3))));
		Assertions.assertTrue(MapUtils.isOneToOne(map(entry(1, 1), entry(2, "1"), entry(3, 3), entry(4, null))));
		Assertions.assertFalse(MapUtils.isOneToOne(map(entry(1, 1), entry(2, 1))));
		Assertions.assertFalse(MapUtils.isOneToOne(map(entry(1, 1), entry(2, 1), entry(3, 3))));
		Assertions.assertFalse(MapUtils.isOneToOne(map(entry(1, null), entry(2, null))));
		Assertions.assertFalse(MapUtils.isOneToOne(map(entry(1, null), entry(2, null), entry(3, 3))));
		Assertions.assertFalse(MapUtils.isOneToOne(map(entry(1, null), entry(2, null), entry(3, null))));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testContains() {
		Assertions.assertTrue(MapUtils.contains(1, map(entry(1, null), entry(2, null))));
		Assertions.assertTrue(MapUtils.contains(2, map(entry(1, null), entry(2, null))));
		Assertions.assertTrue(MapUtils.contains(2, map(entry(1, null), entry(2, null), entry(3, null))));
		Assertions.assertTrue(MapUtils.contains(null, map(entry(null, 3), entry(2, 4))));
		Assertions.assertTrue(MapUtils.contains(null, map(entry(1, 3), entry(null, 4))));
		Assertions.assertTrue(MapUtils.contains(null, map(entry(1, 3), entry(null, 4), entry(3, 5))));
		Assertions.assertFalse(MapUtils.contains(null, map(entry(1, 3), entry(2, 4))));
		Assertions.assertFalse(MapUtils.contains(3, map(entry(1, 3), entry(2, 4))));
		Assertions.assertFalse(MapUtils.contains(3, map(entry(1, 5), entry(2, 4))));
		Assertions.assertFalse(MapUtils.contains(3, null));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SafeVarargs
	private final <K, V> Map<K, V> map(Map.Entry<K, V>... entries) {
		HashMap<K, V> result = new LinkedHashMap<>();
		for (Map.Entry<K, V> entry : entries) {
			result.put(entry.getKey(), entry.getValue());
		}
		return result;
	}


	private <K, V> Map.Entry<K, V> entry(K key, V value) {
		return new AbstractMap.SimpleImmutableEntry<>(key, value);
	}


	@SafeVarargs
	private final <T> List<T> list(T... items) {
		return Arrays.asList(items);
	}
}
