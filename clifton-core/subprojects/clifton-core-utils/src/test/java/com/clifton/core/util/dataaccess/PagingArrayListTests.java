package com.clifton.core.util.dataaccess;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>PagingArrayListTests</code> class contains methods for testing the <code>PagingArrayList</code> class.
 *
 * @author vgomelsky
 */
public class PagingArrayListTests {

	@Test
	public void add_to_empty() {
		PagingArrayList<Integer> list = new PagingArrayList<>();
		list.add(1);
		list.add(2);
		list.add(3);
		Assertions.assertEquals(3, list.getTotalElementCount(), "check total element count");
		Assertions.assertEquals(3, list.getCurrentPageElementCount(), "check current page element count");
		Assertions.assertEquals(1, list.getCurrentPageNumber(), "check current page number");
		Assertions.assertEquals(0, list.getFirstElementIndex(), "check first element index");
		Assertions.assertEquals(3, list.getPageSize(), "check page size");
		Assertions.assertEquals(1, list.getTotalPageCount(), "check total page count");
	}


	@Test
	public void add_to_existing_page() {
		PagingArrayList<Integer> list = new PagingArrayList<>(1, 5);
		list.add(1);
		list.add(2);
		list.add(3);
		Assertions.assertEquals(3, list.getTotalElementCount(), "check total element count");
		Assertions.assertEquals(3, list.getCurrentPageElementCount(), "check current page element count");
		Assertions.assertEquals(1, list.getCurrentPageNumber(), "check current page number");
		Assertions.assertEquals(1, list.getFirstElementIndex(), "check first element index");
		Assertions.assertEquals(5, list.getPageSize(), "check page size");
		Assertions.assertEquals(1, list.getTotalPageCount(), "check total page count");

		list.add(4);
		list.add(5);
		list.add(6);
		Assertions.assertEquals(6, list.getTotalElementCount(), "check total element count");
		Assertions.assertEquals(6, list.getCurrentPageElementCount(), "check current page element count");
		Assertions.assertEquals(1, list.getCurrentPageNumber(), "check current page number");
		Assertions.assertEquals(1, list.getFirstElementIndex(), "check first element index");
		Assertions.assertEquals(6, list.getPageSize(), "check page size");
		Assertions.assertEquals(1, list.getTotalPageCount(), "check total page count");
	}


	@Test
	public void total_page_count() {
		PagingArrayList<Integer> list = new PagingArrayList<>(0, 5);

		list.setTotalElementCount(6);
		//Check number of pages for 6 elements with a page size of 5
		Assertions.assertEquals(2, list.getTotalPageCount(), "check total page count");

		list.setTotalElementCount(9);
		//Check number of pages for 9 elements with a page size of 5
		Assertions.assertEquals(2, list.getTotalPageCount(), "check total page count");

		list.setTotalElementCount(11);
		//Check number of pages for 11 elements with a page size of 5
		Assertions.assertEquals(3, list.getTotalPageCount(), "check total page count");

		list.setTotalElementCount(20);
		//Check number of pages for 20 elements with a page size of 5
		Assertions.assertEquals(list.getTotalPageCount(), 4);
	}
}
