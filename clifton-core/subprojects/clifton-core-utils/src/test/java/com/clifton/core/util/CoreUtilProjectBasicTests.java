package com.clifton.core.util;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Set;


/**
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CoreUtilProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "core-util";
	}


	@Override
	public boolean isServiceSkipped(String beanName) {
		return "coreUtilService".equals(beanName);
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.");
		imports.add("javax.management.");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("compare");
		approvedList.add("jmx");
	}
}
