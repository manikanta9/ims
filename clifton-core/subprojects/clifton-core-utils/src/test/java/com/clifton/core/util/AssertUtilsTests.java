package com.clifton.core.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The {@link AssertUtilsTests} class tests the {@link AssertUtils}.
 *
 * @author vgomelsky
 */
public class AssertUtilsTests {

	@Test
	public void testAssertFalse() {
		AssertUtils.assertFalse(false, "The value is false");
	}


	@Test
	public void testAssertFalseFail() {
		Assertions.assertThrows(RuntimeException.class, () -> AssertUtils.assertFalse(true, "The value is true"));
	}


	@Test
	public void testAssertTrue() {
		AssertUtils.assertTrue(true, "The value is true");
	}


	@Test
	public void testAssertTrueFail() {
		Assertions.assertThrows(RuntimeException.class, () -> AssertUtils.assertTrue(false, "The value is false"));
	}


	@Test
	public void testAssertNull() {
		AssertUtils.assertNull(null, "The value is null");
	}


	@Test
	public void testAssertNullFail() {
		Assertions.assertThrows(RuntimeException.class, () -> AssertUtils.assertNull("test", "The value is null"));
	}
}
