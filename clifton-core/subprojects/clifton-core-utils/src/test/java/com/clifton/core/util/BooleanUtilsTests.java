package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.Executors;


/**
 * The {@link BooleanUtilsTests} class tests the {@link BooleanUtils}.
 *
 * @author NickK
 */
@SuppressWarnings("RedundantCast")
public class BooleanUtilsTests {

	@Test
	public void testBooleanIsTrue() {
		Assertions.assertFalse(BooleanUtils.isTrue((Boolean) null));
		Assertions.assertFalse(BooleanUtils.isTrue(false));
		Assertions.assertFalse(BooleanUtils.isTrue(Boolean.FALSE));
		Assertions.assertTrue(BooleanUtils.isTrue(true));
		Assertions.assertTrue(BooleanUtils.isTrue(Boolean.TRUE));
	}


	@Test
	public void testObjectIsTrue() {
		Assertions.assertTrue(BooleanUtils.isTrue("true"));
		Assertions.assertTrue(BooleanUtils.isTrue("yes"));
		Assertions.assertTrue(BooleanUtils.isTrue("TRUE"));
		Assertions.assertTrue(BooleanUtils.isTrue("YES"));
		Assertions.assertTrue(BooleanUtils.isTrue("1"));
		Assertions.assertTrue(BooleanUtils.isTrue(1L));
		Assertions.assertTrue(BooleanUtils.isTrue(new Long("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(Long.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(1));
		Assertions.assertTrue(BooleanUtils.isTrue(Integer.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(new Integer("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(new BigDecimal("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(BigDecimal.ONE));
		Assertions.assertTrue(BooleanUtils.isTrue(new Short("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(Short.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrue((Object) Boolean.TRUE));
		Assertions.assertTrue(BooleanUtils.isTrue((Object) true));
		Assertions.assertTrue(BooleanUtils.isTrue(new Double("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(new Float("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(Double.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrue(Float.valueOf("1")));

		Assertions.assertFalse(BooleanUtils.isTrue("tru"));
		Assertions.assertFalse(BooleanUtils.isTrue("false"));
		Assertions.assertFalse(BooleanUtils.isTrue("FALSE"));
		Assertions.assertFalse(BooleanUtils.isTrue("2"));
		Assertions.assertFalse(BooleanUtils.isTrue((Object) null));
		Assertions.assertFalse(BooleanUtils.isTrue(0L));
		Assertions.assertFalse(BooleanUtils.isTrue(2L));
		Assertions.assertFalse(BooleanUtils.isTrue((Object) Boolean.FALSE));
		Assertions.assertFalse(BooleanUtils.isTrue((Object) false));
	}


	@Test
	public void testObjectIsTrueStrict() {
		Assertions.assertTrue(BooleanUtils.isTrueStrict("true"));
		Assertions.assertTrue(BooleanUtils.isTrueStrict("yes"));
		Assertions.assertTrue(BooleanUtils.isTrueStrict("1"));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(1L));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(new Long("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(Long.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(1));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(Integer.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(new Integer("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict((Object) Boolean.TRUE));
		Assertions.assertTrue(BooleanUtils.isTrueStrict((Object) true));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(BigDecimal.ONE));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(new BigDecimal("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(new Double("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(new Float("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(Double.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(Float.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(Short.valueOf("1")));
		Assertions.assertTrue(BooleanUtils.isTrueStrict(new Short("1")));

		Assertions.assertFalse(BooleanUtils.isTrueStrict("TRUE"));
		Assertions.assertFalse(BooleanUtils.isTrueStrict("YES"));
		Assertions.assertFalse(BooleanUtils.isTrueStrict("tru"));
		Assertions.assertFalse(BooleanUtils.isTrueStrict("false"));
		Assertions.assertFalse(BooleanUtils.isTrueStrict("FALSE"));
		Assertions.assertFalse(BooleanUtils.isTrueStrict("2"));
		Assertions.assertFalse(BooleanUtils.isTrueStrict((Object) null));
		Assertions.assertFalse(BooleanUtils.isTrueStrict(0L));
		Assertions.assertFalse(BooleanUtils.isTrueStrict(2L));
		Assertions.assertFalse(BooleanUtils.isTrueStrict((Object) Boolean.FALSE));
		Assertions.assertFalse(BooleanUtils.isTrueStrict((Object) false));
	}


	@Test
	public void testBooleanAnyTrue() {
		Assertions.assertFalse(BooleanUtils.anyTrue(false, false, false, false));
		Assertions.assertTrue(BooleanUtils.anyTrue(true, false, false, false));
		Assertions.assertTrue(BooleanUtils.anyTrue(false, true, false, false));
		Assertions.assertTrue(BooleanUtils.anyTrue(false, false, true, false));
		Assertions.assertTrue(BooleanUtils.anyTrue(false, false, false, true));

		Assertions.assertFalse(BooleanUtils.anyTrue(null, false, false, false));
		Assertions.assertTrue(BooleanUtils.anyTrue(null, true, false, true));
		Assertions.assertTrue(BooleanUtils.anyTrue(null, "yes", false, false));
	}


	@Test
	public void testMultiThreadedObjectIsTrue() {
		CompletionService<Boolean> pool = new ExecutorCompletionService<>(Executors.newFixedThreadPool(10));
		final int taskCount = 1000;
		// submit tasks
		for (int i = 0; i < taskCount; i++) {
			pool.submit(() -> {
				testObjectIsTrue();
				testObjectIsTrueStrict();
				return Boolean.TRUE;
			});
		}
		// get task results - if a task fails, ExecutionException should be thrown
		for (int i = 0; i < taskCount; i++) {
			try {
				pool.take().get();
			}
			catch (InterruptedException e) {
				Assertions.fail("Test was interrupted.");
			}
			catch (ExecutionException e) {
				Assertions.fail("Thread failed to get correct result: " + e.getMessage());
			}
		}
	}
}
