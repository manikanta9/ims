package com.clifton.core.util.concurrent;


import java.util.HashMap;


/**
 * The <code>Lockable</code> interface indicates that an object is "lockable" for atomic operations. It is intended to be
 * used with implementations that use more advanced synchronization policies for greater performance, but who also wish to
 * provide support for atomic operations required by the client.
 * <p>
 * For example, {@link java.util.concurrent.ConcurrentHashMap} was introduced to provide better performance than a synchronized {@link HashMap} (which simply just synchronizes all
 * access to the map). Although {@link java.util.concurrent.ConcurrentHashMap} has much better performance, the drawback is that the caller has no way to safely perform
 * atomic operations on it due to not having visibility into the locking strategy of the implementation. The client code is forced to use the
 * less-performant synchronized {@link HashMap} implementation so that it can use the map's intrinsic lock for atomic operations.
 * <p>
 * This interface is intended for concurrent implementations that wish to provide a balance between these two approaches.
 *
 * @author jgommels
 */
public interface Lockable {

	/**
	 * Locks the object so that atomic actions can be safely performed. The underlying implementation should prevent
	 * any reading and writing of the object by other threads while this lock is held.
	 * <p>
	 * {@link #unlock()} must be called when the atomic operation is finished.
	 */
	public void lock();


	/**
	 * Releases the lock on the object.
	 * <p>
	 * <b>Warning: The invocation of <code>unlock()</code> should <i>always</i> be within a finally block</b>
	 * <p>
	 * Example:
	 * <pre><code>
	 * Lockable structure = new MyLockableImpl();
	 * structure.lock();
	 * try {
	 *     structure.doSomething();
	 * }
	 * finally {
	 *     structure.unlock();
	 * }
	 * </code></pre>
	 */
	public void unlock();
}
