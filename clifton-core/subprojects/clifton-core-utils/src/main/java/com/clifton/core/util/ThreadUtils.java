package com.clifton.core.util;

import java.lang.management.ManagementFactory;
import java.lang.management.ThreadInfo;
import java.lang.management.ThreadMXBean;


/**
 * The {@link ThreadUtils} class provides a collection of static utility methods for use when working with threads.
 *
 * @author MikeH
 */
public class ThreadUtils {

	private ThreadUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the thread of the given thread ID.
	 *
	 * @implNote While more complex and more prone to race conditions than the simpler {@link Thread#getAllStackTraces()} method of retrieving all running threads, enumerating all
	 * active threads from the root thread group, as is done in this method, is significantly faster. Race conditions present can generally be ignored for monitoring and debugging
	 * purposes.
	 */
	@SuppressWarnings("DuplicatedCode") // Duplicates JMH benchmarking code
	public static Thread getThreadById(long id) {
		// Identify root thread group
		@SuppressWarnings("java:S3014")
		ThreadGroup tg = Thread.currentThread().getThreadGroup();
		while (tg.getParent() != null) {
			tg = tg.getParent();
		}

		// Populate all threads; see org.apache.commons.lang3.ThreadUtils.findThreads(java.lang.ThreadGroup, boolean, org.apache.commons.lang3.ThreadUtils.ThreadPredicate)
		int activeCount = tg.activeCount();
		Thread[] threads;
		do {
			threads = new Thread[activeCount + (activeCount / 2) + 1];
			activeCount = tg.enumerate(threads);
		}
		while (activeCount >= threads.length);

		// Find expected thread
		Thread seekThread = null;
		for (Thread thread : threads) {
			if (thread != null && id == thread.getId()) {
				seekThread = thread;
				break;
			}
		}
		if (seekThread == null) {
			throw new RuntimeException(String.format("Thread with ID [%d] could not be found.", id));
		}
		return seekThread;
	}


	/**
	 * Retrieves the array of {@link ThreadInfo} objects representing all threads currently active in the JVM. The full stack is populated for all {@link ThreadInfo} objects.
	 */
	public static ThreadInfo[] getAllThreadInfo() {
		ThreadMXBean threadMxBean = ManagementFactory.getThreadMXBean();
		return threadMxBean.getThreadInfo(threadMxBean.getAllThreadIds(), Integer.MAX_VALUE);
	}


	/**
	 * Retrieves the array of {@link ThreadInfo} objects representing all threads currently active in the JVM. The {@link ThreadInfo} objects are populated to the given stack
	 * depth.
	 */
	public static ThreadInfo[] getAllThreadInfo(int stackDepth) {
		ThreadMXBean threadMxBean = ManagementFactory.getThreadMXBean();
		return threadMxBean.getThreadInfo(threadMxBean.getAllThreadIds(), stackDepth);
	}


	/**
	 * Generates the thread dump string content for the {@link Thread} corresponding to the given {@link ThreadInfo} object.
	 * <p>
	 * Example output:
	 * <pre>
	 * "QF/J Session dispatcher: FIX.4.4:PARAMETRIC->FIXSIM" Id=670, State=TIMED_WAITING on java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject@7e6511cc
	 *     at sun.misc.Unsafe.park(Native Method)
	 *     -  waiting on java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject@7e6511cc
	 *     at java.util.concurrent.locks.LockSupport.parkNanos(LockSupport.java:215)
	 *     at java.util.concurrent.locks.AbstractQueuedSynchronizer$ConditionObject.awaitNanos(AbstractQueuedSynchronizer.java:2078)
	 *     at java.util.concurrent.LinkedBlockingQueue.poll(LinkedBlockingQueue.java:467)
	 *     at quickfix.mina.QueueTrackers$1.poll(QueueTrackers.java:45)
	 *     at quickfix.mina.ThreadPerSessionEventHandlingStrategy.getNextMessage(ThreadPerSessionEventHandlingStrategy.java:278)
	 *     at quickfix.mina.ThreadPerSessionEventHandlingStrategy$MessageDispatchingThread.doRun(ThreadPerSessionEventHandlingStrategy.java:217)
	 *     at quickfix.mina.ThreadPerSessionEventHandlingStrategy$ThreadAdapter.run(ThreadPerSessionEventHandlingStrategy.java:146)
	 *     at java.lang.Thread.run(Thread.java:748)
	 * </pre>
	 */
	public static String getThreadDump(ThreadInfo threadInfo) {
		StringBuilder threadDumpBuilder = new StringBuilder();
		if (threadInfo != null) {
			threadDumpBuilder.append(String.format("\"%s\" Id=%d, State=%s", threadInfo.getThreadName(), threadInfo.getThreadId(), threadInfo.getThreadState()));
			if (threadInfo.getLockName() != null) {
				threadDumpBuilder.append(" on ").append(threadInfo.getLockName());
			}
			if (threadInfo.getLockOwnerName() != null) {
				threadDumpBuilder.append(String.format(" owned by \"%s\" Id=%d", threadInfo.getLockOwnerName(), threadInfo.getLockOwnerId()));
			}
			if (threadInfo.isSuspended()) {
				threadDumpBuilder.append(" (suspended)");
			}
			if (threadInfo.isInNative()) {
				threadDumpBuilder.append(" (in native)");
			}
			boolean firstLockInfoElement = true;
			for (StackTraceElement stackTraceElement : threadInfo.getStackTrace()) {
				threadDumpBuilder.append("\n\tat ").append(stackTraceElement);
				if (firstLockInfoElement && threadInfo.getLockInfo() != null) {
					switch (threadInfo.getThreadState()) {
						case BLOCKED:
							threadDumpBuilder.append("\n\t-  blocked on ").append(threadInfo.getLockInfo());
							break;
						case WAITING:
						case TIMED_WAITING:
							threadDumpBuilder.append("\n\t-  waiting on ").append(threadInfo.getLockInfo());
							break;
					}
					firstLockInfoElement = false;
				}
			}
		}
		return threadDumpBuilder.toString();
	}
}
