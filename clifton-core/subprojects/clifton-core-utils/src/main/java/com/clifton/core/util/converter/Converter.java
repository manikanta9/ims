package com.clifton.core.util.converter;


/**
 * The <code>Converter</code> interface should be implemented when converting from one object type to another.
 *
 * @param <F> convert from this
 * @param <T> convert to this
 * @author vgomelsky
 */
public interface Converter<F, T> {

	/**
	 * Converts the specified from type and returns the converted type.
	 */
	public T convert(F from);
}
