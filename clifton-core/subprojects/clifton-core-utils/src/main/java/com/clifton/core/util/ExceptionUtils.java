package com.clifton.core.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.SocketException;
import java.util.List;


/**
 * The <code>ExceptionUtils</code> class provides utility methods for working with exceptions.
 *
 * @author vgomelsky
 */
public class ExceptionUtils {

	/**
	 * When writing response to web client (file download, etc.), connection may go bad and there would be nowhere to
	 * delivery the response (browser got disconnected, etc.). Tries to identify exceptions of this type and return
	 * true when one is detected.
	 */
	public static boolean isConnectionBrokenException(Throwable e) {
		if (e != null) {
			if ("getOutputStream() has already been called for this response".equals(e.getMessage())) {
				return true;
			}
			if (e.getClass().getName().endsWith("ClientAbortException")) {
				return true;
			}
			Throwable original = getOriginalException(e);
			if (original instanceof SocketException) {
				// Tomcat wraps SocketException
				if ("Connection reset by peer: socket write error".equals(original.getMessage())) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Returns original exception's toString() result.
	 */
	public static String getOriginalMessage(Throwable e) {
		return getOriginalException(e).getMessage();
	}


	/**
	 * Returns detailed message which includes the specified Throwable message as well as message of all of its causes.
	 */
	public static String getDetailedMessage(Throwable e) {
		return getDetailedMessage(e, false);
	}


	/**
	 * Returns detailed message which includes the specified Throwable message as well as message of all of its causes in reverse order.
	 * Use this method to see the original exception message first. Can be useful when full message is truncated.
	 */
	public static String getDetailedMessageReversed(Throwable e) {
		return getDetailedMessage(e, true);
	}


	private static String getDetailedMessage(Throwable e, boolean reversed) {
		if (e == null) {
			return "ERROR GETTING MESSAGE: Argument Throwable is NULL";
		}
		StringBuilder result = new StringBuilder();
		String m = e.getMessage();
		result.append(m == null ? "NO MESSAGE FOR " + e.getClass() : m);
		e = e.getCause();
		while (e != null) {
			m = e.getMessage();
			if (m != null) {
				if (reversed) {
					result.insert(0, m + "\nCAUSED ");
				}
				else {
					result.append("\nCAUSED BY ").append(m);
				}
			}
			e = e.getCause();
		}
		return result.toString();
	}


	/**
	 * Returns getMessage() on the specified Throwable.  If not defined,
	 * returns corresponding message containing the class of Throwable.
	 */
	public static String getNormalizedMessage(Throwable e) {
		if (e == null) {
			return "ERROR GETTING MESSAGE: Argument Throwable is NULL";
		}
		String message = e.getMessage();
		if (message == null || message.isEmpty()) {
			message = "NO MESSAGE FOR " + e.getClass();
		}
		return message;
	}


	/**
	 * Returns original exception (deepest cause).
	 */
	public static Throwable getOriginalException(Throwable e) {
		if (e == null) {
			return null;
		}
		return (e.getCause() == null) ? e : getOriginalException(e.getCause());
	}


	/**
	 * Returns the argument Throwable if its of the same type as the specified class or its
	 * first cause that's of the specified type.  If the argument Throwable and none of its
	 * causes are of the specified type, returns null.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Throwable> T getCauseException(Throwable e, Class<T> causeType) {
		Throwable result = e;
		while (result != null && !result.getClass().equals(causeType) && !causeType.isAssignableFrom(result.getClass())) {
			result = result.getCause();
		}
		return (T) result;
	}


	/**
	 * Gets full stack trace (including causes) of the specified Throwable and returns it as a String.
	 */
	public static String getFullStackTrace(Throwable e) {
		if (e == null) {
			return null;
		}
		StringWriter stringWriter = new StringWriter();
		PrintWriter printWriter = new PrintWriter(stringWriter, true);
		do {
			e.printStackTrace(printWriter);
			e = e.getCause();
		}
		while (e != null);
		return stringWriter.getBuffer().toString();
	}


	public static String toString(Throwable e) {
		if (e == null) {
			return "ARGUMENT EXCEPTION IS NULL IN ExceptionUtils.toString(Throwable)";
		}
		StringWriter stringWriter = new StringWriter();
		e.printStackTrace(new PrintWriter(stringWriter));
		return stringWriter.toString();
	}


	/**
	 * Two common scenarios for error handling:
	 * - throw an exception as soon as first error is encountered
	 * - keep accumulating all errors and then pass them to the caller (usually batch job) for reporting
	 * <p>
	 * This method helps with both scenarios: if the specified errors parameters is null, then an exception will be thrown.
	 * Otherwise, the error message is added to the specified errors List.
	 */
	public static void addErrorOrThrowException(List<String> errors, String errorMessage, Throwable e) {
		if (errors == null) {
			throw new RuntimeException(errorMessage, e);
		}
		errors.add(errorMessage + ": " + getDetailedMessage(e));
	}
}
