package com.clifton.core.util.converter.string;


import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>ObjectToStringConverter</code> class converts objects of various types to corresponding
 * String representations.
 *
 * @author vgomelsky
 */
public class ObjectToStringConverter implements Converter<Object, String> {

	@Override
	public String convert(Object from) {
		String result = null;
		if (from != null) {
			if (from instanceof Date) {
				result = DateUtils.fromDateShort((Date) from);
			}
			else if (from instanceof Boolean) {
				result = Boolean.TRUE.equals(from) ? "true" : "false";
			}
			else if (from instanceof BigDecimal) {
				result = ((BigDecimal) from).toPlainString();
			}
			else if (from instanceof Time) {
				result = ((Time) from).toString(Time.TIME_FORMAT_SHORT);
			}
			else {
				result = from.toString();
			}
		}
		return result;
	}
}
