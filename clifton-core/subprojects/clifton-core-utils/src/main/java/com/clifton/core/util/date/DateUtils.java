package com.clifton.core.util.date;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.FormatUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.text.ParseException;
import java.time.DayOfWeek;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.TextStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;
import java.time.temporal.TemporalUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Objects;
import java.util.TimeZone;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The {@link DateUtils} class provides utility methods that simplify working with {@link Date} objects.
 *
 * @author vgomelsky
 */
public class DateUtils {

	public static final String DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss";
	public static final String DATE_FORMAT_FULL_PRECISE = "yyyy-MM-dd HH:mm:ss.SSS";
	public static final String DATE_FORMAT_FULL_NO_SEPARATORS = "yyyyMMddHHmmss";
	public static final String DATE_FORMAT_FULL_PRECISE_NO_SEPARATORS = "yyyyMMddHHmmssSSS";
	public static final String DATE_FORMAT_FILE = "yyyy-MM-dd_HH_mm_ss";
	public static final String DATE_FORMAT_FILE_PRECISE = "yyyy-MM-dd_HH_mm_ss.SSS";
	public static final String DATE_FORMAT_SQL_PRECISE = "yyyy-MM-dd HH:mm:ss.SSSSSSS";
	public static final String DATE_FORMAT_INPUT = "MM/dd/yyyy";
	public static final String DATE_FORMAT_SHORT = "MM/dd/yyyy h:mm aaa";
	public static final String DATE_FORMAT_SPELLED = "EEEE, MMMM d, yyyy";
	public static final String DATE_FORMAT_SQL = "yyyy-MM-dd";
	public static final String DATE_FORMAT_MONTH_YEAR = "MM/yyyy";
	public static final String TIME_INTERVAL_FORMAT = "h:m a";

	public static final String FIX_DATE_FORMAT_INPUT = "yyyyMMdd";

	// International Standard date formats
	// Although it is currently the same as DATE_FORMAT_SQL, we want to differentiate from the SQL format in case that would ever change.
	public static final String DATE_FORMAT_ISO = "yyyy-MM-dd";
	public static final String DATE_FORMAT_ISO_SIMPLE = "yyyyMMdd";

	public static final String DATE_FORMAT_WITH_TIMEZONE = "yyyy-MM-dd'T'HH:mm:ssZZZZ";

	public static final int DATE_NATURAL_START_YEAR = 1900;
	public static final int DATE_NATURAL_MAX_YEAR = 2999;
	public static final long ONE_SECOND = 1000;
	public static final long ONE_MINUTE = ONE_SECOND * 60;
	public static final long ONE_HOUR = ONE_MINUTE * 60;
	public static final long ONE_DAY = ONE_HOUR * 24;
	private static final int[] YEAR_DAYS_SINCE_NATURAL_START = getYearDaysSinceNaturalStart();

	private static final DateTimeFormatter TIME_INTERVAL_FORMATTER = DateTimeFormatter.ofPattern(TIME_INTERVAL_FORMAT);

	public static final List<String> VALID_DATE_FORMATS_LIST = Collections.unmodifiableList(Arrays.asList(DATE_FORMAT_INPUT, DATE_FORMAT_FULL));


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DateUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////
	////            Java 8 DateUtil methods                 ////
	////////////////////////////////////////////////////////////


	/**
	 * Calls {@link #asLocalDate(Date, ZoneId)} with the system default time zone.
	 */
	public static LocalDate asLocalDate(java.util.Date date) {
		return asLocalDate(date, ZoneId.systemDefault());
	}


	/**
	 * Creates {@link LocalDate} from {@code java.util.Date} or it's subclasses. Null-safe.
	 */
	public static LocalDate asLocalDate(java.util.Date date, ZoneId zone) {
		if (date == null) {
			return null;
		}

		if (date instanceof java.sql.Date) {
			return ((java.sql.Date) date).toLocalDate();
		}
		else {
			return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDate();
		}
	}


	/**
	 * Calls {@link #asLocalDate(Date, ZoneId)} with the system default time zone.
	 */
	public static LocalTime asLocalTime(java.util.Date date) {
		return asLocalTime(date, ZoneId.systemDefault());
	}


	/**
	 * Creates {@link LocalDate} from {@code java.util.Date} or it's subclasses. Null-safe.
	 */
	public static LocalTime asLocalTime(java.util.Date date, ZoneId zone) {
		if (date == null) {
			return null;
		}
		return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalTime();
	}


	/**
	 * Calls {@link #asLocalDateTime(Date, ZoneId)} with the system default time zone.
	 */
	public static LocalDateTime asLocalDateTime(java.util.Date date) {
		return asLocalDateTime(date, ZoneId.systemDefault());
	}


	/**
	 * Creates {@link LocalDateTime} from {@code java.util.Date} or it's subclasses. Null-safe.
	 */
	public static LocalDateTime asLocalDateTime(java.util.Date date, ZoneId zone) {
		if (date == null) {
			return null;
		}
		return Instant.ofEpochMilli(date.getTime()).atZone(zone).toLocalDateTime();
	}


	/**
	 * Calls {@link #asUtilDate(Object, ZoneId)} with the system default time zone.
	 */
	public static java.util.Date asUtilDate(Object date) {
		return asUtilDate(date, ZoneId.systemDefault());
	}


	/**
	 * Creates a {@link java.util.Date} from various date objects. Is null-safe. Currently supports:<ul>
	 * <li>{@link java.util.Date}
	 * <li>{@link java.sql.Date}
	 * <li>{@link java.sql.Timestamp}
	 * <li>{@link java.time.LocalDate}
	 * <li>{@link java.time.LocalDateTime}
	 * <li>{@link java.time.ZonedDateTime}
	 * <li>{@link java.time.Instant}
	 * </ul>
	 *
	 * @param zone Time zone, used only if the input object is LocalDate or LocalDateTime.
	 * @return {@link java.util.Date} (exactly this class, not a subclass, such as java.sql.Date)
	 */
	public static java.util.Date asUtilDate(Object date, ZoneId zone) {
		if (date == null) {
			return null;
		}

		if (date instanceof java.sql.Date || date instanceof java.sql.Timestamp || date instanceof java.sql.Time) {
			return new java.util.Date(((java.util.Date) date).getTime());
		}
		if (date instanceof java.util.Date) {
			return (java.util.Date) date;
		}
		if (date instanceof LocalDate) {
			return java.util.Date.from(((LocalDate) date).atStartOfDay(zone).toInstant());
		}
		if (date instanceof LocalDateTime) {
			return asUtilDate(date, zone);
		}
		if (date instanceof ZonedDateTime) {
			return java.util.Date.from(((ZonedDateTime) date).toInstant());
		}
		if (date instanceof Instant) {
			return java.util.Date.from((Instant) date);
		}

		throw new UnsupportedOperationException("Don't know hot to convert " + date.getClass().getName() + " to java.util.Date");
	}


	/**
	 * Creates a {@link java.util.Date} instance from the provided {@link LocalDateTime} object.
	 */
	public static java.util.Date asUtilDate(LocalDateTime date) {
		return java.util.Date.from(date.atZone(ZoneId.systemDefault()).toInstant());
	}


	/**
	 * Creates a {@link java.util.Date} instance from the provided {@link LocalDateTime} object.
	 */
	public static java.util.Date asUtilDate(LocalDateTime date, ZoneId zone) {
		return java.util.Date.from(date.atZone(zone).toInstant());
	}


	/**
	 * Creates an {@link Instant} from {@code java.util.Date} or it's subclasses. Null-safe.
	 */
	public static Instant asInstant(Date date) {
		if (date == null) {
			return null;
		}
		else {
			return Instant.ofEpochMilli(date.getTime());
		}
	}


	/**
	 * Calls {@link #asZonedDateTime(Date, ZoneId)} with the system default time zone.
	 */
	public static ZonedDateTime asZonedDateTime(Date date) {
		return asZonedDateTime(date, ZoneId.systemDefault());
	}


	/**
	 * Creates {@link ZonedDateTime} from {@code java.util.Date} or it's subclasses. Null-safe.
	 */
	public static ZonedDateTime asZonedDateTime(Date date, ZoneId zone) {
		if (date == null) {
			return null;
		}
		return asInstant(date).atZone(zone);
	}


	/**
	 * Determines if today's date is strictly before the data referenced by the given <code>dateString</code> (ignoring time).
	 * One use case is using this method in Spring Expression Language to conditionally disable a test via
	 * {@link org.springframework.test.context.junit.jupiter.DisabledIf}
	 */
	public static boolean isTodayBefore(String dateString) {
		return isDateBefore(new Date(), DateUtils.toDate(dateString), false);
	}


	////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////


	/**
	 * Returns the current date and time using the default {@link #DATE_FORMAT_FULL} date format.
	 */
	public static String currentDateTimeFormatted() {
		return fromDate(new Date(), DATE_FORMAT_FULL);
	}


	/**
	 * Converts the specified date object to String using default {@link #DATE_FORMAT_FULL} date format.
	 * If the date doesn't have the time (ends with " 00:00:00") then it strips the time.
	 */
	public static String fromDateSmart(Date date) {
		String result = fromDate(date, DATE_FORMAT_FULL);
		return StringUtils.contains(result, " 00:00:00") ? StringUtils.substringBeforeLast(result, " 00:00:00") : result;
	}


	/**
	 * Converts the specified date object to String using default {@link #DATE_FORMAT_FULL} date format.
	 */
	public static String fromDate(Date date) {
		return fromDate(date, DATE_FORMAT_FULL);
	}


	/**
	 * Converts the specified date object to String using default {@link #DATE_FORMAT_ISO} date format.
	 */
	public static String fromDateISO(Date date) {
		return fromDate(date, DATE_FORMAT_ISO);
	}


	/**
	 * Converts the specified date object to String using default {@link #DATE_FORMAT_ISO_SIMPLE} date format.
	 * 'Simple' refers to including no separation characters (e.g. - dashes) in the resulting string
	 */
	public static String fromDateISOSimple(Date date) {
		return fromDate(date, DATE_FORMAT_ISO_SIMPLE);
	}


	/**
	 * Converts the specified date object to String using {@link #DATE_FORMAT_INPUT} date format (date with no time).
	 */
	public static String fromDateShort(Date date) {
		return fromDate(date, DATE_FORMAT_INPUT);
	}


	/**
	 * Converts the specified date object to String using the specified dateFormat.
	 */
	public static String fromDate(Date date, String dateFormat) {
		if (date == null) {
			return null;
		}
		return FormatUtils.getSimpleDateFormat(dateFormat).format(date);
	}


	/**
	 * Provides formatting for displaying start/end dates in labels
	 * Uses short formatting
	 */
	public static String fromDateRange(Date startDate, Date endDate, boolean emptyStringIfNull) {
		return fromDateRange(startDate, endDate, false, emptyStringIfNull);
	}


	/**
	 * Gets the list of days (inclusively) between the given <code>startDate</code> and the given <code>endDate</code>.
	 */
	public static List<Date> getDateListForRange(Date startDate, Date endDate) {
		if (startDate == null || endDate == null) {
			return Collections.emptyList();
		}
		List<Date> dayList = new ArrayList<>();
		Date currentDate = startDate;
		while (DateUtils.compare(currentDate, endDate, false) <= 0) {
			dayList.add(currentDate);
			currentDate = DateUtils.addDays(currentDate, 1);
		}
		return dayList;
	}


	/**
	 * Provides an ordered list of all weekday dates inclusively between start and end date
	 */
	public static List<Date> getWeekdayListForRange(Date startDate, Date endDate) {
		List<Date> weekdayList = new ArrayList<>();
		if (startDate == null || endDate == null) {
			return weekdayList;
		}
		Date date = startDate;
		if (!DateUtils.isWeekday(date)) {
			date = DateUtils.getNextWeekday(date);
		}
		while (DateUtils.compare(date, endDate, false) <= 0) {
			// Move to Next Weekday
			weekdayList.add(date);
			date = DateUtils.getNextWeekday(date);
		}
		return weekdayList;
	}


	/**
	 * Provides formatting for displaying start/end dates in labels
	 * Uses short formatting
	 *
	 * @param useParenthesis - will return the result in parenthesis
	 */
	public static String fromDateRange(Date startDate, Date endDate, boolean useParenthesis, boolean emptyStringIfNull) {
		if (emptyStringIfNull && startDate == null && endDate == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder(16);
		if (useParenthesis) {
			sb.append("(");
		}
		if (startDate != null) {
			sb.append(DateUtils.fromDateShort(startDate));
		}
		sb.append(" - ");
		if (endDate != null) {
			sb.append(DateUtils.fromDateShort(endDate));
		}
		if (useParenthesis) {
			sb.append(")");
		}
		return sb.toString();
	}


	/**
	 * Returns a string of comma separated date ranges for the given list of dates, creating a new date range when there is a break in the dates
	 * Excludes time in all comparisons
	 * <p>
	 * Sorts the dateList as a unique list of non-null date objects in ascending order.
	 *
	 * @param dateList     - list of dates to iterate through
	 * @param weekdaysOnly - ignores missing weekend dates
	 */
	public static String toDateRangesString(List<Date> dateList, boolean weekdaysOnly, Function<Date, String> dateConverter) {
		dateList = CollectionUtils.getStream(CollectionUtils.getDistinct(dateList, DateUtils::clearTime)).filter(Objects::nonNull).collect(Collectors.toList());
		if (CollectionUtils.isEmpty(dateList)) {
			return null;
		}
		if (CollectionUtils.getSize(dateList) == 1) {
			return dateConverter.apply(dateList.get(0));
		}
		StringBuilder dateRangeString = new StringBuilder(16);
		dateList = CollectionUtils.sort(dateList);
		Date startDate = dateList.get(0);
		Date endDate = null;
		boolean added = false;

		for (Date date : dateList) {
			if (endDate != null) {
				if (DateUtils.isEqualWithoutTime(date, (weekdaysOnly) ? DateUtils.addWeekDays(endDate, 1) : DateUtils.addDays(endDate, 1))) {
					endDate = date;
				}
				else {
					if (added) {
						dateRangeString.append(", ");
					}
					dateRangeString.append(dateConverter.apply(startDate));
					added = true;
					if (!DateUtils.isEqualWithoutTime(endDate, startDate)) {
						dateRangeString.append(" - ").append(dateConverter.apply(endDate));
					}
					startDate = date;
					endDate = null;
				}
			}
			else {
				endDate = date;
			}
		}

		if (added) {
			dateRangeString.append(", ");
		}
		dateRangeString.append(dateConverter.apply(startDate));
		if (endDate != null && !DateUtils.isEqual(endDate, startDate)) {
			dateRangeString.append(" - ").append(dateConverter.apply(endDate));
		}
		return dateRangeString.toString();
	}


	/**
	 * Converts the specified String to {@link Date} using default input {@link #DATE_FORMAT_INPUT} date format or the {@link #DATE_FORMAT_FULL} format.
	 * Note - if the date is in the MM/dd/yyyy HH:mm:ss format the time will be stripped.  Time values must use the yyyy-MM-dd HH:mm:ss format.
	 */
	public static Date toDate(String date) {
		for (String format : VALID_DATE_FORMATS_LIST) {
			try {
				return toDate(date, format);
			}
			catch (IllegalArgumentException e) {
				// do nothing, try to parse the next format
			}
		}
		throw new IllegalArgumentException(String.format("Cannot parse date field '%s' using formats: %s", date, VALID_DATE_FORMATS_LIST));
	}


	/**
	 * Converts the specified String to {@link Date} using the specified date format.
	 */
	public static Date toDate(String date, String dateFormat) {
		try {
			return FormatUtils.getSimpleDateFormat(dateFormat).parse(date);
		}
		catch (ParseException e) {
			throw new IllegalArgumentException("Cannot parse date field '" + date + "' using format '" + dateFormat + "'", e);
		}
	}


	/**
	 * Converts the specified String to {@link Date} trying the specified date formats in the order given.
	 *
	 * @param date                  the string containing the date to be parsed
	 * @param dateFormat            the first date format string
	 * @param additionalDateFormats any remaining date format strings
	 */
	public static Date toDate(String date, String dateFormat, String... additionalDateFormats) {
		ParseException lastException = null;
		List<String> dateFormatList = CollectionUtils.createList(dateFormat);
		Collections.addAll(dateFormatList, additionalDateFormats);
		for (String format : dateFormatList) {
			try {
				return FormatUtils.getSimpleDateFormat(format).parse(date);
			}
			catch (ParseException e) {
				// All non-matching date formats will throw parse exceptions -- this is expected
				lastException = e;
			}
		}
		throw new IllegalArgumentException("Cannot parse date field '" + date + "' using any of the provided formats: "
				+ CollectionUtils.toString(dateFormatList, 5), lastException);
	}


	/**
	 * Returns true if the specified String is a valid date using the default {@link #DATE_FORMAT_INPUT} date format or the {@link #DATE_FORMAT_FULL} format.
	 */
	public static boolean isValidDate(String date) {
		return VALID_DATE_FORMATS_LIST.stream().anyMatch(format -> isValidDate(date, format));
	}


	/**
	 * Returns true if the specified String is a valid date using the specified dateFormat.
	 */
	public static boolean isValidDate(String date, String dateFormat) {
		try {
			return (FormatUtils.getSimpleDateFormat(dateFormat).parse(date) != null);
		}
		catch (ParseException e) {
			return false;
		}
	}


	/**
	 * Returns true if the specified date is Saturday or Sunday.
	 */
	public static boolean isWeekend(Date date) {
		if (date == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return (dayOfWeek == Calendar.SATURDAY || dayOfWeek == Calendar.SUNDAY);
	}


	/**
	 * Returns true if the specified date is Monday.
	 */
	public static boolean isMonday(Date date) {
		if (date == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return (dayOfWeek == Calendar.MONDAY);
	}


	/**
	 * Returns true if the specified date is Friday.
	 */
	public static boolean isFriday(Date date) {
		if (date == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return (dayOfWeek == Calendar.FRIDAY);
	}


	/**
	 * Returns true if the specified date is Saturday.
	 */
	public static boolean isSaturday(Date date) {
		if (date == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return (dayOfWeek == Calendar.SATURDAY);
	}


	/**
	 * Returns true if the specified date is Sunday.
	 */
	public static boolean isSunday(Date date) {
		if (date == null) {
			return false;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
		return (dayOfWeek == Calendar.SUNDAY);
	}


	/**
	 * Returns true if the specified date is Monday through Friday.
	 */
	public static boolean isWeekday(Date date) {
		if (date == null) {
			return false;
		}
		return !isWeekend(date);
	}


	/**
	 * Returns previous weekday for the specified date: Friday for Monday, Wednesday for Thursday, etc.
	 */
	public static Date getPreviousWeekday(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Required date argument to getPreviousWeekday cannot be null.");
		}
		Date result = addDays(date, -1);
		while (isWeekend(result)) {
			result = addDays(result, -1);
		}
		return result;
	}


	/**
	 * Returns next weekday for the specified date: Monday for Friday, Wednesday for Tuesday, etc.
	 */
	public static Date getNextWeekday(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Required date argument to getNextWeekday cannot be null.");
		}
		Date result = addDays(date, 1);
		while (isWeekend(result)) {
			result = addDays(result, 1);
		}
		return result;
	}


	/**
	 * Returns true if the specified LocalDate is between the given start & end LocalDate.
	 * <p>
	 * Note: If start is null then the date is considered as coming after the start date.
	 * If end is null then the date is considered as coming before the end date.
	 */
	public static boolean isDateBetween(LocalDate date, LocalDate startDate, LocalDate endDate) {
		if (date == null) {
			throw new IllegalArgumentException("Required date argument for comparison with isDateBetween cannot be null - startDate and endDate may be null.");
		}
		if (startDate == null || (date.isEqual(startDate) || date.isAfter(startDate))) {
			if (endDate == null || (date.isEqual(endDate) || date.isBefore(endDate))) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if the specified date is between the given start & end date.  You can optionally
	 * include time in the comparison.
	 * <p>
	 * Note: If start is null then the date is considered as coming after the start date.
	 * If end is null then the date is considered as coming before the end date.
	 */
	public static boolean isDateBetween(Date date, Date startDate, Date endDate, boolean includeTime) {
		if (startDate == null || (compare(startDate, date, includeTime) <= 0)) {
			if (endDate == null || (compare(date, endDate, includeTime) <= 0)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if the current date is between the given start & end date.  You can optionally
	 * include time in the comparison.
	 * <p>
	 * Note: If start is null then current date is considered as coming after the start date.
	 * If end is null then current date is considered as coming before the end date.
	 */
	public static boolean isCurrentDateBetween(Date startDate, Date endDate, boolean includeTime) {
		return isDateBetween(new Date(), startDate, endDate, includeTime);
	}


	/**
	 * Returns true if the data range specified by the first 2 arguments overlaps with the date range specified by the
	 * last 2 arguments. null value for start date the means beginning of time and for end date, the end of time.
	 */
	public static boolean isOverlapInDates(Date date1Start, Date date1End, Date date2Start, Date date2End) {
		if (compare(date1Start, date2Start, true) == 0 || compare(date1End, date2End, true) == 0) {
			return true;
		}
		if (isDateAfterOrEqual(date1End, date2Start, false, true) && isDateAfterOrEqual(date2End, date1End, false, false)) {
			return true;
		}
		if (isDateAfterOrEqual(date1Start, date2Start, true, true) && isDateAfterOrEqual(date2End, date1Start, false, true)) {
			return true;
		}
		if (isDateAfterOrEqual(date1End, date2End, false, false) && isDateAfterOrEqual(date2Start, date1Start, true, true)) {
			return true;
		}
		return false;
	}


	/**
	 * Returns true if firstDate is after secondDate. A null date value is treated as
	 * a date in the far future and is always considered greater than a defined date during comparisons.
	 */
	public static boolean isDateAfter(Date firstDate, Date secondDate) {
		return isDateAfter(firstDate, secondDate, false);
	}


	/**
	 * Returns true if firstDate is on or after secondDate.  A null date value is treated as
	 * a date in the far future and is always considered greater than a defined date during comparisons.
	 */
	public static boolean isDateAfterOrEqual(Date firstDate, Date secondDate) {
		return isDateAfter(firstDate, secondDate, true);
	}


	/**
	 * Returns true if firstDate is greater than the secondDate. If equalIsAfter is true, the comparison will also
	 * return true if firstDate and secondDate are equal. Null date value is treated as
	 * a date in the far future (maximum date) and is always considered greater than a defined date during comparisons.
	 *
	 * @returns true if firstDate is null
	 * false if secondDate is null
	 * true if firstDate > secondDate and equalIsAfter == false
	 * true if firstDate >= secondDate and equalIsAfter == true
	 * false if fistDate < secondDate
	 */
	private static boolean isDateAfter(Date firstDate, Date secondDate, boolean equalIsAfter) {
		if (firstDate == null) {
			return true;
		}
		if (secondDate == null) {
			return false;
		}
		return (equalIsAfter ? firstDate.compareTo(secondDate) >= 0 : firstDate.compareTo(secondDate) > 0);
	}


	/**
	 * Compares firstDate with secondDate and returns true if the firstDate is greater or equal to
	 * the second date.  Processing of null values is described in the @return section. Date comparisons include the time component of the date.
	 *
	 * @param firstDate             first date for comparison
	 * @param secondDate            second date for comparison
	 * @param firstDateIsStartDate  set to true if the firstDate is the start date
	 * @param secondDateIsStartDate set to true if the secondDate is the start date
	 * @return true if firstDate is greater than secondDate or if firstDate and secondDate are equal.
	 * false if firstDate is null and firstDateIsStartDate is set to true.
	 * false if firstDate is null and secondDate is null and secondDate is null.
	 * true if the firstDate is null and the previous two conditions (above) are both false.
	 * false otherwise.
	 */
	private static boolean isDateAfterOrEqual(Date firstDate, Date secondDate, boolean firstDateIsStartDate, boolean secondDateIsStartDate) {
		return isDateAfter(firstDate, secondDate, firstDateIsStartDate, secondDateIsStartDate, true);
	}


	/**
	 * Compares firstDate with secondDate and returns true if the firstDate is greater or equal to
	 * the second date. If equalIsAfter is true, equal dates will return true.
	 * Processing of null values is described in the @return section. Date comparisons include the time component of the date.
	 *
	 * @param firstDate             first date for comparison
	 * @param secondDate            second date for comparison
	 * @param firstDateIsStartDate  set to true if the firstDate is the start date
	 * @param secondDateIsStartDate set to true if the secondDate is the start date
	 * @param equalIsAfter          if set to true, will return true if firstDate and secondDate are equal
	 * @return true if firstDate is greater than secondDate or if firstDate and secondDate are equal and equalIsAfter is set to true.
	 * false if firstDate is null and firstDateIsStartDate is set to true.
	 * false if firstDate is null and secondDate is null and secondDateIsStartDate is false.
	 * true if the firstDate is null and the previous two conditions (above) are both false.
	 * false otherwise.
	 */
	private static boolean isDateAfter(Date firstDate, Date secondDate, boolean firstDateIsStartDate, boolean secondDateIsStartDate, boolean equalIsAfter) {
		if (firstDate == null) {
			if (firstDateIsStartDate) {
				// first date is the min date
				return false;
			}
			// first date is the max date
			if (secondDate == null && !secondDateIsStartDate) {
				// second date is also the max date
				return false;
			}
			return true;
		}
		else if (secondDate == null) {
			if (secondDateIsStartDate) {
				// second date is the min date
				return true;
			}
			// second date is the max date
			return false;
		}
		return (equalIsAfter ? firstDate.compareTo(secondDate) >= 0 : firstDate.compareTo(secondDate) > 0);
	}


	/**
	 * Returns true if firstDate is on or before secondDate otherwise false. Null dates are treated as
	 * minimum dates during comparisons.  If includeTime is set to true, the time component of the dates
	 * is considered during comparison.
	 */
	public static boolean isDateBeforeOrEqual(Date firstDate, Date secondDate, boolean includeTime) {
		return isDateBefore(firstDate, secondDate, true, includeTime);
	}


	/**
	 * Returns true if firstDate is before secondDate otherwise false.  Null dates are treated as
	 * minimum dates during comparisons.  If includeTime is set to true, the time component of the dates
	 * is considered during comparison.
	 */
	public static boolean isDateBefore(Date firstDate, Date secondDate, boolean includeTime) {
		return isDateBefore(firstDate, secondDate, false, includeTime);
	}


	/**
	 * A date comparison function that returns true if firstDate occurs before the second date.  If equalIsBefore flag is set,
	 * equal dates will aso return true.  If includeTime is set to true, time is considered in the comparison.
	 * This class behavior regarding null Date values is documented at: {@link DateUtils#compare(Date, Date, boolean)}
	 *
	 * @return true if firstDate < secondDate or if dates are equal and equalIsBefore is set to true.  Otherwise returns false.
	 */
	private static boolean isDateBefore(Date firstDate, Date secondDate, boolean equalIsBefore, boolean includeTime) {
		int dateComparisonResult = DateUtils.compare(firstDate, secondDate, includeTime);
		return (equalIsBefore ? dateComparisonResult <= 0 : dateComparisonResult < 0);
	}


	/**
	 * Gets the Nth weekday of the month.  For example, 3 Thursday of the month.
	 */
	public static Date getNthWeekdayOfMonth(Date date, int n, int targetWeekDay) {

		// strip all date fields below month
		Date startOfMonth = getFirstDayOfMonth(date);
		Calendar cal = Calendar.getInstance();
		cal.setTime(startOfMonth);

		int weekDay = cal.get(Calendar.DAY_OF_WEEK);
		int modifier = (n - 1) * 7 + ((targetWeekDay - weekDay) < 0 ? 7 : 0) + (targetWeekDay - weekDay);
		return modifier > 0 ? DateUtils.addDays(startOfMonth, modifier) : startOfMonth;
	}


	/**
	 * Returns end of day (last millisecond) representation of the specified date.
	 */
	public static Date getEndOfDay(Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar = clearTime(calendar);
		calendar.add(Calendar.DATE, 1);
		calendar.add(Calendar.MILLISECOND, -1);
		return calendar.getTime();
	}


	/**
	 * Returns the first day of the month for the specified date.
	 */
	public static Date getFirstDayOfMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the first day of the month.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		return calendar.getTime();
	}


	/**
	 * Returns the first weekday of the month
	 */
	public static Date getFirstWeekdayOfMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the first weekday of the month.");
		}
		return getNextWeekday(addDays(getFirstDayOfMonth(date), -1));
	}


	/**
	 * Returns the first day of the next month for the specified date.
	 */
	public static Date getFirstDayOfNextMonth(Date date) {
		return addDays(getLastDayOfMonth(date), 1);
	}


	/**
	 * Returns the first day of the next month with the time cleared for the specified date.
	 */
	public static Date getFirstDayOfNextMonthWithoutTime(Date date) {
		return clearTime(addDays(getLastDayOfMonth(date), 1));
	}


	/**
	 * Returns the last weekday of the month
	 */
	public static Date getLastWeekdayOfMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the last weekday of the month.");
		}
		return getPreviousWeekday(addDays(getLastDayOfMonth(date), 1));
	}


	/**
	 * Returns the last weekday of the previous month
	 */
	public static Date getLastWeekdayOfPreviousMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the last weekday of the previous month.");
		}
		return getLastWeekdayOfMonth(addDays(getFirstDayOfMonth(date), -1));
	}


	/**
	 * Returns the last day of the previous month
	 */
	public static Date getLastDayOfPreviousMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the last of the previous month.");
		}
		return addDays(getFirstDayOfMonth(date), -1);
	}


	/**
	 * Returns the last day of the month for the specified date.
	 */
	public static Date getLastDayOfMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the last day of the month.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int lastDayOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
		calendar.set(Calendar.DAY_OF_MONTH, lastDayOfMonth);
		return calendar.getTime();
	}


	/**
	 * Returns true if the specified date is the last day of its month.
	 */
	public static boolean isLastDayOfMonth(Date date) {
		if (date != null) {
			if (getDayOfMonth(date) == getDayOfMonth(getLastDayOfMonth(date))) {
				return true;
			}
		}
		return false;
	}


	public static Date getFirstDayOfQuarter(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the first day of the quarter.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.set(Calendar.DAY_OF_MONTH, 1);

		// Quarter Start Months are 0, 3, 6, 9
		int month = calendar.get(Calendar.MONTH);
		if (month < 3) {
			month = 0;
		}
		else if (month < 6) {
			month = 3;
		}
		else if (month < 9) {
			month = 6;
		}
		else {
			month = 9;
		}
		calendar.set(Calendar.MONTH, month);
		return calendar.getTime();
	}


	public static Date getLastDayOfQuarter(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the last day of the quarter.");
		}
		Date firstDayOfQuarter = getFirstDayOfQuarter(date);
		return addDays(addMonths(firstDayOfQuarter, 3), -1);
	}


	public static Date getLastDayOfPreviousQuarter(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the last day of the previous quarter.");
		}
		// Get the First Day of the Current Quarter and Subtract One Day
		Date firstDayOfQuarter = getFirstDayOfQuarter(date);
		return addDays(firstDayOfQuarter, -1);
	}


	public static int getQuarter(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the first day of the quarter.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		int month = calendar.get(Calendar.MONTH);

		return ((month / 3) + 1);
	}


	/**
	 * Gets the integer that represents of the year.
	 */
	public static int getYear(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the year.");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.YEAR);
	}


	/**
	 * Gets the hours of the date
	 * <p>
	 * I.e. the hours in 24 hour format
	 */
	public static int getHours(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to get the hours.");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.HOUR_OF_DAY);
	}


	/**
	 * Returns the minutes of the date
	 */
	public static int getMinutes(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to get the hours.");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MINUTE);
	}


	/**
	 * Gets the number of seconds of the given date
	 */
	public static int getSeconds(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to get the seconds.");
		}

		Calendar calOne = Calendar.getInstance();
		calOne.setTime(date);
		return calOne.get(Calendar.SECOND);
	}


	/**
	 * Returns the milliseconds of the date
	 */
	public static int getMilliseconds(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to get the milliseconds.");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		return cal.get(Calendar.MILLISECOND);
	}


	/**
	 * Gets the integer that represents the day of the year.
	 */
	public static int getDayOfYear(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the day of the year.");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_YEAR);
	}


	/**
	 * Returns the number of days in the year for the specified date.
	 */
	public static int getDaysInYear(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the number of days in the year.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.getActualMaximum(Calendar.DAY_OF_YEAR);
	}


	public static Date getLastDayOfYear(Date date) {
		return toDate("12/31/" + getYear(date));
	}


	/**
	 * Clears  time (hours, minutes, seconds, milliseconds) for the specified calendar.
	 */
	public static Date clearTime(Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar = clearTime(calendar);
		return calendar.getTime();
	}


	/**
	 * Clears  time (hours, minutes, seconds, milliseconds) for the specified calendar.
	 */
	public static Calendar clearTime(Calendar calendar) {
		if (calendar != null) {
			calendar.set(Calendar.AM_PM, Calendar.AM);
			calendar.clear(Calendar.HOUR);
			calendar.clear(Calendar.HOUR_OF_DAY);
			calendar.clear(Calendar.MINUTE);
			calendar.clear(Calendar.SECOND);
			calendar.clear(Calendar.MILLISECOND);
		}
		return calendar;
	}


	/**
	 * Clears  time (hours, minutes, seconds, milliseconds) for the specified calendar.
	 */
	public static Date clearMilliseconds(Date date) {
		if (date == null) {
			return null;
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.clear(Calendar.MILLISECOND);
		return calendar.getTime();
	}


	/**
	 * Sets the time of the date parameter with the values from the time parameter.
	 */
	public static Date setTime(Date date, Date time) {
		if ((date == null) || (time == null)) {
			throw new IllegalArgumentException("Both date and time are required in order to set time.");
		}
		Calendar dateCalendar = Calendar.getInstance();
		dateCalendar.setTime(date);
		Calendar timeCalendar = Calendar.getInstance();
		timeCalendar.setTime(time);

		dateCalendar.set(Calendar.MINUTE, timeCalendar.get(Calendar.MINUTE));
		dateCalendar.set(Calendar.HOUR_OF_DAY, timeCalendar.get(Calendar.HOUR_OF_DAY));
		dateCalendar.set(Calendar.SECOND, timeCalendar.get(Calendar.SECOND));
		dateCalendar.set(Calendar.MILLISECOND, timeCalendar.get(Calendar.MILLISECOND));
		//dateCalendar.clear(Calendar.MILLISECOND);

		return dateCalendar.getTime();
	}


	/**
	 * Sets the time of the date parameter with the values from the time parameter.
	 */
	public static Date setTime(Date date, Time time) {
		if ((date == null) || (time == null)) {
			throw new IllegalArgumentException("Both date and time are required in order to set time.");
		}

		return setTime(date, time.getDate());
	}


	public static Date getDateInTimeZone(Date currentDate, String timeZoneId) {
		Calendar mbCal = new GregorianCalendar(TimeZone.getTimeZone(timeZoneId));
		mbCal.setTimeInMillis(currentDate.getTime());

		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, mbCal.get(Calendar.YEAR));
		cal.set(Calendar.MONTH, mbCal.get(Calendar.MONTH));
		cal.set(Calendar.DAY_OF_MONTH, mbCal.get(Calendar.DAY_OF_MONTH));
		cal.set(Calendar.HOUR_OF_DAY, mbCal.get(Calendar.HOUR_OF_DAY));
		cal.set(Calendar.MINUTE, mbCal.get(Calendar.MINUTE));
		cal.set(Calendar.SECOND, mbCal.get(Calendar.SECOND));
		cal.set(Calendar.MILLISECOND, mbCal.get(Calendar.MILLISECOND));

		return cal.getTime();
	}


	/**
	 * Gets the integer that represents the day of the week. The values correspond to Calendar.SUNDAY, Calendar.MONDAY, Calendar.TUESDAY, Calendar.WEDNESDAY,
	 * Calendar.THURSDAY, Calendar.FRIDAY, and Calendar.SATURDAY.
	 */
	public static int getDayOfWeek(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the day of the week.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_WEEK);
	}


	/**
	 * Get the integer that represents the day of the week adjusted from ISO calendar system (Monday = 1) to the IMS standard (Sunday = 1).
	 *
	 * @see java.time.DayOfWeek
	 */
	public static int getDayOfWeek(LocalDateTime localDateTime) {
		AssertUtils.assertNotNull(localDateTime, "Date is required.");

		int dayOfWeek = localDateTime.get(ChronoField.DAY_OF_WEEK);
		return (dayOfWeek == 7) ? 1 : (dayOfWeek + 1);
	}


	/**
	 * Get the string that represents the day of the week adjusted from ISO calendar system (Monday = 1) to the IMS standard (Sunday = 1).
	 *
	 * @see java.time.DayOfWeek
	 */
	public static String getWeekdayName(int dayOfWeek) {
		AssertUtils.assertTrue(dayOfWeek >= 1 && dayOfWeek <= 7, "The provided day of week value ([%d]) is outside of the allowed range ([%d] to [%d]).", dayOfWeek, 1, 7);
		dayOfWeek = (dayOfWeek == 1) ? 7 : (dayOfWeek - 1);
		return DayOfWeek.of(dayOfWeek).getDisplayName(TextStyle.FULL, Locale.US);
	}


	/**
	 * Get the string that represents the day of the week adjusted from ISO calendar system (Monday = 1) to the IMS standard (Sunday = 1). Returns <code>null</code> if the provided
	 * <code>dayOfWeek</code> value is outside of the allowed weekday range.
	 *
	 * @see #getWeekdayName(int)
	 * @see java.time.DayOfWeek
	 */
	public static String getWeekdayNameOrNull(int dayOfWeek) {
		if (!(dayOfWeek >= 1 && dayOfWeek <= 7)) {
			return null;
		}
		return getWeekdayName(dayOfWeek);
	}


	/**
	 * Gets the integer that represents the day of the month.
	 */
	public static int getDayOfMonth(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the day of the month.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.DAY_OF_MONTH);
	}


	/**
	 * Gets the integer that represents the month of the year where January = 1, February = 2, etc.
	 * <p>
	 * Not values DO NOT correspond to Calendar.JANUARY, Calendar.FEBRUARY, etc. which start at 0 (i.e. Calendar.JANUARY = 0).
	 */
	public static int getMonthOfYear(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the month of the year.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar.get(Calendar.MONTH) + 1;
	}


	/**
	 * Get month of quarter (1, 2, or 3) January = 1, February = 2, March = 3, April = 1, May = 2, etc.
	 * <p>
	 */
	public static int getMonthOfQuarter(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required in order to get the quarter of the month.");
		}
		int month = getMonthOfYear(date);
		switch (month) {
			case 1:
			case 4:
			case 7:
			case 10:
				return 1;
			case 2:
			case 5:
			case 8:
			case 11:
				return 2;
			default:
				return 3;
		}
	}


	/**
	 * Returns the number of minutes since midnight or the start of the day.
	 */
	public static int getMillisecondsFromMidnight(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to determine the milliseconds since midnight.");
		}
		//Integer overflow not possible here (there are not enough millis in a given day)
		return Math.toIntExact(getMillisecondsDifference(date, clearTime(date)));
	}


	/**
	 * Returns the number of minutes since midnight or the start of the day.
	 */
	public static int getMinutesFromMidnight(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to determine the minutes since midnight.");
		}
		return Math.toIntExact(getMinutesDifference(date, clearTime(date)));
	}


	/**
	 * Returns the first day of the week or Sunday at 12:00 AM of the same week.
	 */
	public static Date getFirstDayOfWeek(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to determine the first day of the week.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int dayOfWeek = getDayOfWeek(date);
		Date result = addDays(date, -(dayOfWeek - 1));

		return clearTime(result);
	}


	/**
	 * Returns the last day of the week or Saturday at 11:59 PM of the same week.
	 */
	public static Date getLastDayOfWeek(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to determine the last day of the week.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int dayOfWeek = getDayOfWeek(date);
		Date result = addDays(date, 7 - dayOfWeek);

		return setTime(result, Time.parse("23:59:00"));
	}


	/**
	 * Returns the first day of the week or sunday as 12:00 AM of the same week.
	 */
	public static Date getFirstDayOfYear(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to determine the first day of the year.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		int dayOfYear = getDayOfYear(date);
		Date result = addDays(date, -(dayOfYear - 1));

		return clearTime(result);
	}


	/**
	 * Compares two dates for sorting.  If the value returned is negative, the first
	 * date is before the second, if it is zero the dates are equal, and if it is positive
	 * the first date is after the second date.
	 * <p>
	 * If not including time, will remove the time from the dates before comparison
	 * In this implementation, null values for dates are treated as absolute minimum date values,
	 * and are therefore always less compared to a non-null Date .
	 */
	public static int compare(Date d1, Date d2, boolean includeTime) {
		if (d1 == null) {
			if (d2 == null) {
				return 0;
			}
			return -1;
		}
		if (d2 == null) {
			return 1;
		}
		if (!includeTime) {
			Calendar c = Calendar.getInstance();
			c.setTime(d1);
			c = clearTime(c);
			d1 = c.getTime();

			c.setTime(d2);
			c = clearTime(c);
			d2 = c.getTime();
		}
		return d1.compareTo(d2);
	}


	/**
	 * Compares the given {@link Date} objects, using the given threshold for equality constraints. If the given dates differ by less than the provided threshold then they will be
	 * considered equal. Otherwise, behavior will be the same as {@link #compare(Date, Date, boolean)}.
	 * <p>
	 * This method returns <code>0</code> if the two dates are equal within the given threshold, <code>-1</code> if the first date precedes the second date by more than the given
	 * threshold, and <code>1</code> if the first date follows the second date by more than the given threshold.
	 * <p>
	 * For example, the following comparison would test for differences greater than 5 seconds in the provided dates:
	 * <code><pre>
	 * Date date1 = DateUtils.toDate("2019-01-01 00:00:00.000", DateUtils.DATE_FORMAT_FULL_PRECISE);
	 * Date date2 = DateUtils.toDate("2019-01-01 00:00:00.005", DateUtils.DATE_FORMAT_FULL_PRECISE);
	 * Date date3 = DateUtils.toDate("2019-01-01 00:00:00.006", DateUtils.DATE_FORMAT_FULL_PRECISE);
	 *
	 * DateUtils.compareWithThreshold(date1, date2, ChronoUnit.MILLIS, 5); // 0. Dates are within 5 millisecond threshold.
	 * DateUtils.compareWithThreshold(date1, date3, ChronoUnit.MILLIS, 5); // -1. date1 is more than 5 milliseconds before date3.
	 * </pre></code>
	 * <p>
	 * For {@link ChronoUnit chrono-units} of {@link TemporalUnit#isDurationEstimated() estimated durations}, threshold criteria will be estimated by adding the given threshold to
	 * the lower of the two provided dates. If adding this threshold produces a date which is greater than the higher of the two dates, then the dates will be considered to be
	 * equal within the given threshold.
	 *
	 * @param date1     the first date to compare
	 * @param date2     the second date to compare
	 * @param timeUnit  the unit of time to use for the threshold comparison
	 * @param threshold the threshold quantifier (e.g., <code>5</code> and <code>ChronoUnit.DAYS</code> for a threshold of 5 days), which must be greater than or equal to zero
	 * @return <code>0</code> if the dates are similar within the given threshold, <code>-1</code> if <code>date1</code> is before <code>date2</code> (beyond the specified
	 * threshold), and <code>1</code> if <code>date1</code> is after <code>date2</code> (beyond the specified threshold)
	 */
	public static int compareWithThreshold(Date date1, Date date2, ChronoUnit timeUnit, long threshold) {
		// There is no logical representation for negative thresholds (e.g., what value is returned for equal dates when a negative threshold is used?)
		AssertUtils.assertTrue(threshold >= 0, "The threshold value [%d] must be greater than or equal to zero.", threshold);

		// Guard-clause: Trivial cases
		if (date1 == null) {
			if (date2 == null) {
				return 0;
			}
			return -1;
		}
		if (date2 == null) {
			return 1;
		}
		int dateCompare = date1.compareTo(date2);
		if (dateCompare == 0) {
			return 0;
		}

		// Compare difference between dates
		final int result;
		/*
		 * Use Instant.ofEpochMilli generator instead of Date#toInstant since Date sub-types may not implement #toInstant in a functional way (e.g., java.sql.Date#toInstant). All
		 * values will have millisecond precision at most. This reduces precision in the case of java.sql.Timestamp, which supports up to nanosecond precision.
		 */
		Instant instant1 = Instant.ofEpochMilli(date1.getTime());
		Instant instant2 = Instant.ofEpochMilli(date2.getTime());
		if (timeUnit.isDurationEstimated()) {
			/*
			 * Translation to an exact duration value cannot be accurately used. For example, a month may be 28, 29, 30, or 31 days, and does not correspond to an exact amount of
			 * time. Determine if adding the threshold value to the earlier of the two dates produces a date which is greater than the higher of the two dates. If such a condition
			 * holds, then the two dates match within the given threshold.
			 */
			Instant lowerInstant = dateCompare < 0 ? instant1 : instant2;
			Instant higherInstant = dateCompare < 0 ? instant2 : instant1;
			LocalDateTime thresholdMaxDateTime = lowerInstant.atZone(ZoneOffset.UTC).toLocalDateTime().plus(threshold, timeUnit);
			LocalDateTime maxHigherDateTime = higherInstant.atZone(ZoneOffset.UTC).toLocalDateTime();
			int thresholdToHigherDateCompare = thresholdMaxDateTime.compareTo(maxHigherDateTime);
			boolean withinThreshold = thresholdToHigherDateCompare >= 0;
			result = withinThreshold ? 0 : dateCompare;
		}
		else {
			// Use exact comparison (i.e., always include milliseconds, seconds, etc.)
			Duration dateDifferenceDuration = Duration.between(instant2, instant1);
			Duration thresholdDuration = Duration.of(threshold, timeUnit);
			result = dateDifferenceDuration.abs().compareTo(thresholdDuration.abs()) <= 0
					? 0
					: dateDifferenceDuration.isNegative() ? -1 : 1;
		}
		return result;
	}


	/**
	 * Gets the amount of time between the given dates in units of the given <code>timeUnit</code>.
	 * <p>
	 * This will return the number of <i>completable</i> units of the given <code>timeUnit</code>. For example, the time in {@link ChronoUnit#HOURS} between 11:30 PM and 13:29 PM
	 * will be <code>1</code>, since the difference is one minute shy of two hours.
	 *
	 * @param date1    the first date to compare
	 * @param date2    the second date to compare
	 * @param timeUnit the unit of time to count
	 * @return the number of completable <code>timeUnit</code> units between the given dates when starting from <code>date1</code>
	 * @see TemporalUnit#between(Temporal, Temporal)
	 */
	public static long getTimeBetween(Date date1, Date date2, ChronoUnit timeUnit) {
		final Temporal temporal1;
		final Temporal temporal2;
		switch (timeUnit) {
			case NANOS:
			case MICROS:
			case MILLIS:
			case SECONDS:
			case MINUTES:
			case HOURS:
			case HALF_DAYS:
			case DAYS:
				temporal1 = date1.toInstant();
				temporal2 = date2.toInstant();
				break;
			case WEEKS:
			case MONTHS:
			case YEARS:
			case DECADES:
			case CENTURIES:
			case MILLENNIA:
			case ERAS:
			case FOREVER:
				temporal1 = date1.toInstant().atZone(ZoneOffset.UTC).toLocalDate();
				temporal2 = date2.toInstant().atZone(ZoneOffset.UTC).toLocalDate();
				break;
			default:
				throw new IllegalArgumentException("Unsupported time unit: " + timeUnit);
		}
		return timeUnit.between(temporal1, temporal2);
	}


	public static void assertDatesEqual(Date expectedDate, Date actualDate) {
		AssertUtils.assertTrue(DateUtils.isEqual(expectedDate, actualDate), "Expected [%s] but was [%s].", expectedDate, actualDate);
	}


	/**
	 * Returns true if the two dates are equal, including time component
	 */
	public static boolean isEqual(Date d1, Date d2) {
		return compare(d1, d2, true) == 0;
	}


	/**
	 * Returns true if the two dates are equal, excluding time component
	 */
	public static boolean isEqualWithoutTime(Date d1, Date d2) {
		return compare(d1, d2, false) == 0;
	}


	/**
	 * Returns the day number of the given date from the natural start date.
	 * <p>
	 * Assuming Day 1 = 01/01/1900 and the last day supported is 12/31/2999
	 */
	public static int getDaysSinceNaturalStart(Date date) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to determine the days since natural start date.");
		}

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);

		try {
			int days = YEAR_DAYS_SINCE_NATURAL_START[calendar.get(Calendar.YEAR) - 1900];
			return days + (calendar.get(Calendar.DAY_OF_YEAR)) - 1;
		}
		catch (ArrayIndexOutOfBoundsException e) {
			throw new ValidationException("Date passed does not fall within year range allowed [" + DATE_NATURAL_START_YEAR + " - " + DATE_NATURAL_MAX_YEAR + "]");
		}
	}


	private static int[] getYearDaysSinceNaturalStart() {
		int[] yearDays = new int[DATE_NATURAL_MAX_YEAR - DATE_NATURAL_START_YEAR + 1];

		Calendar cal = Calendar.getInstance();
		cal.set(DATE_NATURAL_START_YEAR, Calendar.JANUARY, 1);
		yearDays[0] = 1;

		int dayCount = 1;
		for (int currentYear = DATE_NATURAL_START_YEAR + 1; currentYear <= DATE_NATURAL_MAX_YEAR; currentYear++) {
			dayCount += cal.getActualMaximum(Calendar.DAY_OF_YEAR);
			yearDays[currentYear - 1900] = dayCount;
			cal.set(Calendar.YEAR, currentYear);
		}
		return yearDays;
	}


	/**
	 * Gets the number of years between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static int getYearsDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the difference in years.");
		}

		Calendar calOne = Calendar.getInstance();
		calOne.setTime(endDate);

		Calendar calTwo = Calendar.getInstance();
		calTwo.setTime(startDate);

		return calOne.get(Calendar.YEAR) - calTwo.get(Calendar.YEAR);
	}


	/**
	 * Gets the number of months between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static int getMonthsDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the difference in years.");
		}

		Calendar calOne = Calendar.getInstance();
		calOne.setTime(endDate);

		Calendar calTwo = Calendar.getInstance();
		calTwo.setTime(startDate);

		int years = getYearsDifference(endDate, startDate);
		return years * 12 + calOne.get(Calendar.MONTH) - calTwo.get(Calendar.MONTH);
	}


	/**
	 * Gets the number of full months between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static int getFullMonthsDifference(Date endDate, Date startDate) {
		final int oneDayOfMonth = getDayOfMonth(endDate);
		final int twoDayOfMonth = getDayOfMonth(startDate);
		final int monthDifference = getMonthsDifference(endDate, startDate);
		if (monthDifference == 0) { // same month
			return monthDifference;
		}
		else if (monthDifference < 0) { // one is before two
			return oneDayOfMonth > twoDayOfMonth ? monthDifference + 1 : monthDifference;
		}
		else { // one is after two
			return oneDayOfMonth < twoDayOfMonth ? monthDifference - 1 : monthDifference;
		}
	}


	/**
	 * Gets the difference in minutes between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static int getWeeksDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the difference in weeks.");
		}
		int days = getDaysDifference(clearTime(endDate), clearTime(startDate));
		int signMultiplier = days < 0 ? -1 : 1;
		return signMultiplier * Math.abs(days) / 7;
	}


	/**
	 * Returns the days difference between the 2 dates.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 * <p>
	 * Because of issues with Daylight Savings Time, cannot just compare Time in Milliseconds / Milliseconds per day.
	 * Unfortunately, this is less efficient.
	 */
	public static int getDaysDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the difference in days.");
		}
		return getDaysSinceNaturalStart(endDate) - getDaysSinceNaturalStart(startDate);
	}


	/**
	 * Returns the days difference between the 2 dates inclusive (so adds one day to the result)
	 * For example - difference between today and yesterday is 1 day, inclusive would return 2 days
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static int getDaysDifferenceInclusive(Date endDate, Date startDate) {
		int daysDifference = getDaysDifference(endDate, startDate);
		if (daysDifference < 0) {
			return daysDifference - 1;
		}
		return daysDifference + 1;
	}


	/**
	 * Gets the difference in hours between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 * <p>
	 * NOTE: This method may throw an ArithmeticException if the number of hours between the two dates
	 * overflows the max value an integer can hold.
	 */
	public static int getHoursDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the hours difference");
		}
		return Math.toIntExact(getMinutesDifference(endDate, startDate) / 60);
	}


	/**
	 * Gets the difference in minutes between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static long getMinutesDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the minutes difference");
		}
		return getSecondsDifference(endDate, startDate) / 60;
	}


	/**
	 * Gets the difference in seconds between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static long getSecondsDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the minutes difference");
		}
		return getMillisecondsDifference(endDate, startDate) / 1000;
	}


	/**
	 * Gets the difference in milliseconds between the startTime and endTime.
	 * <p>
	 * If endDate occurs before the startDate the result will be negative, else it will be positive.
	 */
	public static long getMillisecondsDifference(Date endDate, Date startDate) {
		if (endDate == null || startDate == null) {
			throw new IllegalArgumentException("Both dates are required to determine the minutes difference");
		}
		return endDate.getTime() - startDate.getTime();
	}


	/**
	 * Returns a formatted string representing the time difference
	 * i.e. 4 days 3 hours to a max of 2 metrics
	 * <p>
	 * if seconds, only seconds
	 * if minutes, both minutes and seconds
	 * if more than an hour, then hour and minutes
	 */
	public static String getTimeDifferenceShort(Date endDate, Date startDate) {
		return getTimeDifference(endDate, startDate, true);
	}


	/**
	 * Returns a formatted string representing the time difference
	 * i.e. 4 days 3 hours 5 minutes
	 */
	public static String getTimeDifferenceLong(Date endDate, Date startDate) {
		return getTimeDifference(endDate, startDate, false);
	}


	/**
	 * Returns a formatted string representing the time difference
	 * i.e. 4 days 3 hours 5 minutes
	 * <p>
	 * If shortVersion is true will return the first 2 metrics:
	 * if seconds, only seconds
	 * if minutes, both minutes and seconds
	 * if more than an hour, then hour and minutes
	 */
	public static String getTimeDifference(Date dateOne, Date dateTwo, boolean shortVersion) {
		if (dateOne == null || dateTwo == null) {
			return null;
		}
		long duration = dateOne.getTime() - dateTwo.getTime();
		return getTimeDifference(duration, shortVersion);
	}


	/**
	 * Returns a formatted string representing the specified duration (in milliseconds).
	 * <p>
	 * If shortVersion is true will return the first 2 metrics:
	 * if seconds, only seconds
	 * if minutes, both minutes and seconds
	 * if more than an hour, then hour and minutes
	 */
	public static String getTimeDifference(long durationMillis, boolean shortVersion) {
		StringBuilder result = new StringBuilder();

		if (durationMillis < 0) {
			durationMillis = durationMillis * -1;
		}
		if (durationMillis < ONE_SECOND) {
			return "0 seconds";
		}

		Integer count = (shortVersion ? 2 : null);
		long temp = durationMillis / ONE_DAY;
		result.append(getFormattedTimePiece(temp, "day"));
		if (temp != 0 && count != null) {
			count--;
			if (count == 0) {
				return result.toString();
			}
		}
		durationMillis = durationMillis - (ONE_DAY * temp);

		temp = durationMillis / ONE_HOUR;
		result.append(getFormattedTimePiece(temp, "hour"));
		if (temp != 0 && count != null) {
			count--;
			if (count == 0) {
				return result.toString();
			}
		}
		durationMillis = durationMillis - (ONE_HOUR * temp);

		temp = durationMillis / ONE_MINUTE;
		result.append(getFormattedTimePiece(temp, "minute"));
		if (temp != 0 && count != null) {
			count--;
			if (count == 0) {
				return result.toString();
			}
		}
		durationMillis = durationMillis - (ONE_MINUTE * temp);

		temp = durationMillis / ONE_SECOND;
		result.append(getFormattedTimePiece(temp, "second"));
		return result.toString();
	}


	private static String getFormattedTimePiece(long value, String type) {
		if (value == 0) {
			return "";
		}
		return value + " " + type + (value > 1 ? "s" : "") + " ";
	}


	public static Date addYears(Date date, int years) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add years.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.YEAR, years);

		return calendar.getTime();
	}


	public static Date addMonths(Date date, int months) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add months.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MONTH, months);

		return calendar.getTime();
	}


	/**
	 * Returns the date that is the given amount of days away.
	 */
	public static Date addDays(Date date, int days) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add days.");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}


	/**
	 * Returns the date that is the given amount of days away (skip weekends).
	 * For example, if date is Thursday and we add 3 days, the result will be Tuesday.
	 * If the specified date is a weekend, moves to the next week day.
	 */
	public static Date addWeekDays(Date date, int days) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add days.");
		}
		Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		if (days == 0) {
			int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
			if (dayOfWeek == Calendar.SATURDAY) {
				cal.add(Calendar.DATE, 2);
			}
			else if (dayOfWeek == Calendar.SUNDAY) {
				cal.add(Calendar.DATE, 1);
			}
		}
		else {
			for (int i = 0; i < Math.abs(days); i++) {
				int dayOfWeek = cal.get(Calendar.DAY_OF_WEEK);
				int daysToAdd;
				if (days > 0) {
					daysToAdd = 1;
					if (dayOfWeek == Calendar.FRIDAY) {
						daysToAdd = 3;
					}
					else if (dayOfWeek == Calendar.SATURDAY) {
						daysToAdd = 2;
					}
				}
				else {
					daysToAdd = -1;
					if (dayOfWeek == Calendar.MONDAY) {
						daysToAdd = -3;
					}
					else if (dayOfWeek == Calendar.SUNDAY) {
						daysToAdd = -2;
					}
				}
				cal.add(Calendar.DATE, daysToAdd);
			}
		}
		return cal.getTime();
	}


	/**
	 * Adds minutes to the date.
	 */
	public static Date addMinutes(Date date, int minutes) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add minutes.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MINUTE, minutes);

		return calendar.getTime();
	}


	public static Date addMilliseconds(Date date, int milliseconds) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add milliseconds.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.MILLISECOND, milliseconds);

		return calendar.getTime();
	}


	public static Date addSeconds(Date date, int seconds) {
		if (date == null) {
			throw new IllegalArgumentException("Date is required to add seconds.");
		}
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		calendar.add(Calendar.SECOND, seconds);

		return calendar.getTime();
	}


	/**
	 * The accepted format is '11:00 PM - 1:00 AM', the smallest unit of time is one minute.
	 */
	public static boolean isWithinInterval(LocalTime current, String interval) {
		AssertUtils.assertNotNull(current, "Parameter current cannot be null");
		AssertUtils.assertNotNull(interval, "Parameter interval cannot be null");

		List<String> segments = Arrays.asList(interval.split("-"));
		AssertUtils.assertTrue(segments.size() == 2, "The interval designator '-' is required.");

		LocalTime start = LocalTime.parse(segments.get(0).trim(), TIME_INTERVAL_FORMATTER);
		LocalTime end = LocalTime.parse(segments.get(1).trim(), TIME_INTERVAL_FORMATTER);
		current = current.truncatedTo(ChronoUnit.MINUTES);

		return Duration.between(start, end).isNegative() ?
				(current.compareTo(LocalTime.MIDNIGHT) >= 0 && current.compareTo(end) <= 0) ||
						(current.compareTo(start) >= 0 && current.compareTo(LocalTime.MAX) <= 0)
				: current.compareTo(start) >= 0 && current.compareTo(end) <= 0;
	}


	/**
	 * Determine if the provided time is within any of the provided intervals.
	 */
	public static boolean isWithinAnyInterval(LocalTime current, List<String> intervals) {
		AssertUtils.assertNotNull(current, "Parameter current cannot be null");
		AssertUtils.assertNotNull(intervals, "Parameter intervals cannot be null");

		return intervals.stream().anyMatch(s -> DateUtils.isWithinInterval(current, s));
	}


	/**
	 * Returns those intervals in which the provided time falls.
	 */
	public static List<String> getWithinIntervalList(LocalTime current, List<String> intervals) {
		AssertUtils.assertNotNull(current, "Parameter current cannot be null");
		AssertUtils.assertNotNull(intervals, "Parameter intervals cannot be null");

		return intervals.stream().filter(s -> DateUtils.isWithinInterval(current, s)).collect(Collectors.toList());
	}
}
