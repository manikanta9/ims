package com.clifton.core.util;

import java.util.Arrays;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Supplier;


/**
 * The <code>ObjectUtils</code> class contains various utility methods for working with objects of different data types.
 *
 * @author vgomelsky
 */
public class ObjectUtils {

	/**
	 * Converts the specified object to a String using toString method. Supports user friendly output for arrays.
	 */
	public static String toString(Object value) {
		if (value != null) {
			if (value.getClass().isArray()) {
				Class<?> elementClass = value.getClass().getComponentType();
				if (short.class.equals(elementClass)) {
					return Arrays.toString((short[]) value);
				}
				if (int.class.equals(elementClass)) {
					return Arrays.toString((int[]) value);
				}
				if (long.class.equals(elementClass)) {
					return Arrays.toString((long[]) value);
				}
				if (byte.class.equals(elementClass)) {
					return Arrays.toString((byte[]) value);
				}
				if (boolean.class.equals(elementClass)) {
					return Arrays.toString((boolean[]) value);
				}
				return Arrays.toString((Object[]) value);
			}
			return value.toString();
		}
		return null;
	}


	/**
	 * Returns first non-null argument.
	 */
	@SafeVarargs
	public static <T> T coalesce(T... args) {
		if (args != null && args.length != 0) {
			for (T arg : args) {
				if (arg != null) {
					return arg;
				}
			}
		}

		return null;
	}


	/**
	 * Returns true if at least one fo the argument objects is not null.
	 */
	public static boolean isNotNullPresent(Object... objects) {
		if (objects != null) {
			for (Object o : objects) {
				if (o != null) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Performs the given action on the provided object if the provided object is non-null.
	 * <p>
	 * This method may be chained such that an alternative action will be performed if the object is null via methods such as {@link
	 * ConditionalChain#orElseIfPresent(Object, Consumer) orElseIfPresent(object, action)} or {@link ConditionalChain#orElse(Runnable)
	 * orElseIfPresent(action)}.
	 * <p>
	 * This is a convenience method to reduce bloat from excessive if-not-null-then-action code blocks. For example:
	 * <pre>
	 * if (getName() != null) {
	 *     doWithName(getName());
	 * }
	 * else if (getLabel() != null) (
	 *     doWithLabel(getLabel());
	 * }
	 * else {
	 *     doDefault("A default value");
	 * }
	 * </pre>
	 * can be converted to:
	 * <pre>
	 * {@link #doIfPresent(Object, Consumer) ObjectUtils.doIfPresent}(getName(), this::doWithName)
	 *     .{@link ConditionalChain#orElseIfPresent(Object, Consumer) orElseIfPresent}(getLabel(), this::doWithLabel)
	 *     .{@link ConditionalChain#orElse(Runnable) orElse}(() -> doDefault("A default value"));
	 * </pre>
	 * <p>
	 * This method functions somewhat similarly to {@link Optional#ifPresent(Consumer)}, but extends the {@link Optional} functionality to allow chained methods
	 * on presence conditions.
	 * <p>
	 * {@link #coalesce(Object[])} may be more performant where its usage is applicable.
	 *
	 * @param object the object on which to act
	 * @param action the action to perform on the given object
	 * @param <T>    the type of the object
	 * @param <R>    the type of the resulting value which may be produced by the chain
	 * @return the {@link ConditionalChain} object for chaining commands
	 * @see Optional#ifPresent(Consumer)
	 * @see #map(Object, Function)
	 */
	public static <T, R> ConditionalChain<R> doIfPresent(T object, Consumer<T> action) {
		return new ConditionalChain<R>().orElseIfPresent(object, action);
	}


	/**
	 * Performs the action following the behavior detailed in {@link #doIfPresent(Object, Consumer)}, but if the given object is non-null then produces a result
	 * which may be later obtained from the resulting {@link ConditionalChain}.
	 *
	 * @param object the object on which to act
	 * @param action the action to perform on the given object, producing a result
	 * @param <T>    the type of the object
	 * @param <R>    the type of the resulting value which may be produced by the chain
	 * @return the {@link ConditionalChain} object for chaining commands
	 * @see #doIfPresent(Object, Consumer)
	 */
	public static <T, R> ConditionalChain<R> map(T object, Function<T, R> action) {
		return new ConditionalChain<R>().orElseMap(object, action);
	}


	/**
	 * A class for chaining conditional operations, intended to work similarly to the {@link Optional} type.
	 * <p>
	 * To use this class, see {@link #doIfPresent(Object, Consumer)} or {@link #map(Object, Function)}. The return-values of calls to those methods may be
	 * chained, providing a functional-like interface for mapping objects and branching conditions.
	 * <p>
	 * <h4>Types of operations</h4>
	 * <table>
	 * <tr>
	 * <td>Terminal</td>
	 * <td>Sets the {@link #terminated} status to {@code true}. This prevents execution of future <tt>orElse</tt> methods.<br><b>Only runs if the current chain
	 * is not terminated.</b></td>
	 * </tr>
	 * <tr>
	 * <td>Supplying</td>
	 * <td>Produces a {@link #result} value for the current chain. This result may be obtained via the {@link #get()} method.<br><b>Only runs if the supplied
	 * value is non-null and a result does not already exist.</b></td>
	 * </tr>
	 * <tr>
	 * <td>Result-Based</td>
	 * <td>Performs an action using the existing {@link #result} value of the chain. These may be chained back-to-back to perform multiple mapping actions in a
	 * single sequence.<br><b>Only runs if a result exists.</b></td>
	 * </tr>
	 * </table>
	 *
	 * @param <R> the type of the resulting value which may be produced by this chain
	 * @see #doIfPresent(Object, Consumer)
	 * @see #map(Object, Function)
	 * @see Optional
	 */
	public static class ConditionalChain<R> {

		/**
		 * Holds state indicating whether the chain is <i>terminated</i>.
		 * <p>
		 * A chain becomes terminated when the prerequisites for any {@link ConditionalChain terminal operation} are satisfied and that operation is executed. A
		 * terminated state prevents future "orElse" methods.
		 */
		private boolean terminated;

		/**
		 * Holds the current result of the operation chain.
		 * <p>
		 * A result is generated when the prerequisites for any {@link ConditionalChain supplying operation} are satisfied and that operation is executed,
		 * generating a non-null result. An existing result enables "then" methods, but enables a {@link #terminated} state in the process.
		 */
		private R result;


		private ConditionalChain() {
		}


		/**
		 * Performs the given action.
		 * <p>
		 * This is a {@link ConditionalChain terminal operation}.
		 *
		 * @param action the action to perform
		 */
		public void orElse(Runnable action) {
			if (!isTerminated()) {
				action.run();
				setTerminated(true);
			}
		}


		/**
		 * Assigns a result using the given supplier.
		 * <p>
		 * This is a {@link ConditionalChain terminal operation}. This is a {@link ConditionalChain supplying operation}.
		 *
		 * @param action the result supplier
		 * @return the current chain
		 */
		public ConditionalChain<R> orElseSupply(Supplier<R> action) {
			if (!isTerminated()) {
				supply(action.get());
			}
			return this;
		}


		/**
		 * Performs the given consuming action on the provided object. <b>Only runs if the given object is non-null</b>.
		 * <p>
		 * This is a {@link ConditionalChain terminal operation}.
		 *
		 * @param object the object on which to act
		 * @param action the action to perform on the given object
		 * @param <T>    the type of the object
		 * @return the current chain
		 */
		public <T> ConditionalChain<R> orElseIfPresent(T object, Consumer<T> action) {
			if (!isTerminated() && object != null) {
				action.accept(object);
				setTerminated(true);
			}
			return this;
		}


		/**
		 * Performs the given mapping action on the provided object.
		 * <p>
		 * This is a {@link ConditionalChain terminal operation}. This is a {@link ConditionalChain supplying operation}.
		 *
		 * @param object the object on which to act
		 * @param action the action to perform on the given object, supplying a result
		 * @param <T>    the type of the object
		 * @return the current chain
		 */
		public <T> ConditionalChain<R> orElseMap(T object, Function<T, R> action) {
			if (!isTerminated() && object != null) {
				supply(action.apply(object));
			}
			return this;
		}


		/**
		 * Performs the given consuming action on the current {@link #result}.
		 * <p>
		 * This is a {@link ConditionalChain result-based operation}.
		 *
		 * @param action the action to perform on the current result
		 * @return the new chain object
		 */
		public <R2> ConditionalChain<R2> thenConsume(Consumer<R> action) {
			ConditionalChain<R2> nextChain = new ConditionalChain<>();
			if (get() != null) {
				action.accept(get());
				nextChain.setTerminated(true);
			}
			return nextChain;
		}


		/**
		 * Performs the given mapping operation on the current {@link #result}.
		 * <p>
		 * This is a {@link ConditionalChain result-based operation}. This is a {@link ConditionalChain supplying operation}.
		 *
		 * @param action the action to perform on the current result
		 * @return the new chain object
		 */
		public <R2> ConditionalChain<R2> thenMap(Function<R, R2> action) {
			ConditionalChain<R2> nextChain = new ConditionalChain<>();
			if (get() != null) {
				nextChain.supply(action.apply(get()));
			}
			return nextChain;
		}


		/**
		 * Returns the current {@link #result} of this chain.
		 */
		public R get() {
			return this.result;
		}


		/**
		 * Returns the current {@link #result} of this chain, or <code>altResult</code> if the current result is <code>null</code>.
		 */
		public R getOrElse(R altResult) {
			return get() != null
					? get()
					: altResult;
		}


		/**
		 * Sets the {@link #result} for this chain.
		 * <p>
		 * This is a {@link ConditionalChain supplying operation}.
		 */
		private void supply(R result) {
			if (get() == null) {
				if (result != null) {
					setTerminated(true);
				}
				this.result = result;
			}
		}


		public boolean isTerminated() {
			return this.terminated;
		}


		private void setTerminated(boolean terminated) {
			this.terminated = terminated;
		}
	}
}
