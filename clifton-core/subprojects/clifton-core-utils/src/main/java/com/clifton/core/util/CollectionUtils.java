package com.clifton.core.util;


import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.dataaccess.PagingArrayList;
import org.jetbrains.annotations.Contract;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiConsumer;
import java.util.function.BiFunction;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.Collector;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The {@link CollectionUtils} class contains helper methods for working with Collection(s).
 *
 * @author vgomelsky
 */
public class CollectionUtils {

	/**
	 * Returns an EmptyList if the specified argument is null. Returns the specified argument otherwise. Use this method
	 * to avoid null check when iterating through a collection of elements.
	 */
	public static <T> Iterable<T> getIterable(Iterable<T> iterable) {
		if (iterable == null) {
			return Collections.emptyList();
		}
		return iterable;
	}


	/**
	 * Checks if the list is null before calling {@link Collections#reverse(List)}
	 */
	public static void reverse(List<?> list) {
		if (list != null) {
			Collections.reverse(list);
		}
	}


	/**
	 * Returns a stream for the provided collection if it is not null. Returns {@link Stream#empty()} if the collection is null.
	 * This method can be used to obtain a stream without having to check for null.
	 */
	public static <T> Stream<T> getStream(Collection<T> collection) {
		if (collection != null) {
			return collection.stream();
		}
		return Stream.empty();
	}


	/**
	 * Builds a stream from the given list of collections. This stream consists of the flattened streams of each of the collections given.
	 *
	 * @param collections the collections which shall compose the resulting stream
	 * @param <T>         the stream type
	 * @return the composed stream
	 */
	@SafeVarargs
	public static <T> Stream<T> buildStream(Collection<? extends T>... collections) {
		return Stream.of(collections)
				.filter(Objects::nonNull)
				.flatMap(Collection::stream);
	}


	/**
	 * Returns an empty Set if the Map is null. Otherwise returns map.keySet().
	 */
	public static <K> Set<K> getKeys(Map<K, ?> map) {
		if (map == null) {
			return new HashSet<>();
		}

		return map.keySet();
	}


	/**
	 * Returns an empty list if the Map is null. Otherwise returns map.values().
	 */
	public static <V> Collection<V> getValues(Map<?, V> map) {
		if (map == null) {
			return Collections.emptyList();
		}

		return map.values();
	}


	/**
	 * Returns the value in the map stored under the specified key.  If null/not found the uses the function to retrieve the bean and stores
	 * it in the map.  This is a convenience method to {@link #getValue(Map, Object, Supplier, Supplier)} using null for the Supplier for the
	 * default value.
	 */
	public static <T, V> V getValue(Map<T, V> map, T key) {
		return getValue(map, key, null);
	}


	/**
	 * Returns the value in the map stored under the specified key.  If null/not found the uses the function to retrieve the bean and stores
	 * it in the map.  This is a convenience method to {@link #getValue(Map, Object, Supplier, Supplier)} using null for the Supplier for the
	 * default value.
	 */
	public static <T, V> V getValue(Map<T, V> map, T key, Supplier<V> functionReturningValueIfNotInMap) {
		return getValue(map, key, functionReturningValueIfNotInMap, null);
	}


	/**
	 * Returns the value in the map stored under the specified key.  If null/not found the uses the function to retrieve the bean and stores
	 * it in the map.  Optional function returning default value if null can be used to prevent attempts to look up missing values - for example if the
	 * object returned is a list, can return new ArrayList() as the default value
	 */
	public static <T, V> V getValue(Map<T, V> map, T key, Supplier<V> functionReturningValueIfNotInMap, Supplier<V> functionReturningDefaultValueIfNull) {
		if (map == null) {
			return null;
		}
		V value = map.get(key);
		if (value == null) {
			// computeIfAbsent will honor synchronization if the map provided allows it
			value = map.computeIfAbsent(key, k -> {
				V newValue = functionReturningValueIfNotInMap == null ? null : functionReturningValueIfNotInMap.get();
				if (newValue == null && functionReturningDefaultValueIfNull != null) {
					newValue = functionReturningDefaultValueIfNull.get();
				}
				return newValue;
			});
		}
		return value;
	}


	/**
	 * Returns a List of type V that is all collections combined
	 */
	public static <V, E extends List<V>, I extends Collection<E>> List<V> combineCollectionOfCollections(I collectionOfCollections) {
		if (collectionOfCollections == null || collectionOfCollections.isEmpty()) {
			return Collections.emptyList();
		}
		List<V> newCollection = new ArrayList<>();
		for (E collection : getIterable(collectionOfCollections)) {
			newCollection.addAll(collection);
		}
		return newCollection;
	}


	/**
	 * Combines the given collections into a single list.
	 *
	 * @param collections the collections to combine
	 * @param <T>         the collection element type
	 * @return a new list which is the combination of the given collections
	 * @apiNote This is an <i>O(n*m)</i> operation, where <i>m</i> is the cost per iteration step for each collection (generally <i>1</i>).
	 */
	@SafeVarargs
	public static <T> List<T> combineCollections(Collection<? extends T>... collections) {
		return ArrayUtils.getStream(collections)
				.filter(Objects::nonNull)
				.flatMap(Collection::stream)
				.collect(Collectors.toList());
	}


	/**
	 * Generates a list of the items that exist in all given collections. If no such items exist or if no collections are given or any of the collections are empty, then an empty list will be
	 * returned.
	 * <p>
	 * Elements in the resulting list will follow in the iteration order for the first given collection before item removal. Items will be compared with {@link
	 * Object#equals(Object)}. All duplicate items will be removed. All elements in the resulting list will be the matching items from the first provided list.
	 * <p>
	 * There are no guarantees on the type, mutability, serializability, or thread-safety of the {@link List} returned.
	 *
	 * @param collections the collections whose intersection shall be retrieved
	 * @param <T>         the common supertype for the elements of all given collections
	 * @return the list of items that exist in all given collections
	 */
	@SafeVarargs
	public static <T> List<T> getIntersection(Collection<? extends T>... collections) {
		// Guard-clause: Handle trivial case in which no collections are given
		if (ArrayUtils.isEmpty(collections)) {
			return Collections.emptyList();
		}

		// Generate intersection
		final List<T> intersection = new ArrayList<>();
		boolean isPopulated = false;
		for (Collection<? extends T> comparisonCollection : collections) {
			// If collection is empty, then return empty list
			if (isEmpty(comparisonCollection)) {
				return Collections.emptyList();
			}
			if (!isPopulated) {
				// Remove duplicate items from the first list to prevent duplicate items precipitating during retention
				Set<? extends T> firstListDistinct = new LinkedHashSet<>(comparisonCollection);
				intersection.addAll(firstListDistinct);
				isPopulated = true;
			}
			else {
				intersection.retainAll(comparisonCollection);
			}
		}
		return intersection;
	}



	/**
	 * Get object at the specified index of a collection.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T getObjectAtIndex(Collection<T> collection, int index) {
		if (CollectionUtils.getSize(collection) > index) {
			return (T) collection.toArray()[index];
		}
		return null;
	}


	/**
	 * Casts the specified value to List if it is a list or converts it to an List if it's an array.
	 * If the specified value is neither List nor Array, creates a new ArrayList and add the value as its only element.
	 */
	public static List<?> getAsList(Object value) {
		if (value == null) {
			return null;
		}
		if (value instanceof List) {
			return (List<?>) value;
		}
		if (value.getClass().isArray()) {
			return createList((Object[]) value);
		}
		return createList(value);
	}


	/**
	 * Creates a sorted list from the given collection.
	 *
	 * @param collection the collection of values which shall be sorted
	 * @param <T>        the type of values in the collection
	 * @return a new sorted list of the values in the given collection
	 */
	public static <T extends Comparable<? super T>> List<T> createSorted(Collection<T> collection) {
		// Guard-clause: Handle null and empty lists
		if (CollectionUtils.isEmpty(collection)) {
			return Collections.emptyList();
		}

		// Clone and sort list
		List<T> clonedList = new ArrayList<>(collection);
		Collections.sort(clonedList);
		return clonedList;
	}


	/**
	 * Creates a sorted list from the given collection using the given comparator.
	 *
	 * @param collection the collection of values which shall be sorted
	 * @param comparator the comparator to use when sorting the list
	 * @param <T>        the type of values in the collection
	 * @return a new sorted list of the values in the given collection
	 */
	public static <T> List<T> createSorted(Collection<T> collection, Comparator<T> comparator) {
		// Guard-clause: Handle null and empty lists
		if (CollectionUtils.isEmpty(collection)) {
			return Collections.emptyList();
		}

		// Clone and sort list
		List<T> clonedList = new ArrayList<>(collection);
		clonedList.sort(comparator);
		return clonedList;
	}


	/**
	 * Sorts the given list in place according to its natural ordering. The list must be a mutable list.
	 */
	public static <T extends Comparable<? super T>> List<T> sort(List<T> list) {
		if (list != null) {
			Collections.sort(list);
		}
		return list;
	}


	/**
	 * Sorts the given list in place using the ordering determined by the given {@code comparator}. The list must be a mutable list.
	 */
	public static <T> List<T> sort(List<T> list, Comparator<? super T> comparator) {
		if (list != null) {
			list.sort(comparator);
		}
		return list;
	}


	/**
	 * Returns true if the specified collection has no elements or is null.
	 */
	public static boolean isEmpty(Collection<?> collection) {
		return (collection == null || collection.isEmpty());
	}


	/**
	 * Returns true if the specified argument is null, an empty collection or an empty array.
	 *
	 * @throws IllegalArgumentException when the specified argument is not null, not a Collection, and not an Array
	 */
	public static boolean isEmptyCollectionOrArray(Object collectionOrArray) {
		if (collectionOrArray == null) {
			return true;
		}
		if (collectionOrArray instanceof Collection<?>) {
			return isEmpty((Collection<?>) collectionOrArray);
		}
		if (collectionOrArray.getClass().isArray()) {
			return (Array.getLength(collectionOrArray) == 0);
		}
		throw new IllegalArgumentException("The specified argument must be a Collection or an Array: " + collectionOrArray);
	}


	/**
	 * Returns true if the specified Map has no elements or is null.
	 */
	public static boolean isEmpty(Map<?, ?> map) {
		return (map == null || map.isEmpty());
	}


	/**
	 * Returns true if the specified Map has no elements or is null.
	 */
	public static boolean isEmpty(MultiValueMap<?, ?> map) {
		return (map == null || map.isEmpty());
	}


	/**
	 * Returns the number of elements in the specified Collection or 0 if the argument is null.
	 */
	public static <T> int getSize(Collection<T> collection) {
		if (collection == null) {
			return 0;
		}
		return collection.size();
	}


	/**
	 * Returns true if the specified collection has a single element.
	 */
	@Contract("null -> false")
	public static boolean isSingleElement(Collection<?> collection) {
		return (getSize(collection) == 1);
	}


	/**
	 * Returns a String representation of the specified collection that uses toString method to serialize each element. The result will be enclosed in square brackets of
	 * comma-separated elements. If the collection has more elements than the specified <code>maxElements</code>, the number of remaining elements will be appended as the final
	 * element.
	 * <p>
	 * If <code>maxElements</code> is less than one, than all elements will be printed.
	 * <p>
	 * Example: <code>["hello", "world", and 7 more elements]</code>
	 *
	 * @see #toString(Collection, int, IntFunction)
	 */
	public static String toString(Collection<?> collection, int maxElements) {
		return toString(collection, maxElements, remainder -> remainder + " more elements");
	}


	/**
	 * Returns a String representation of the specified collection that uses toString method to serialize each element. The result will be enclosed in square brackets of
	 * comma-separated elements. If the collection has more elements than the specified <code>maxElements</code>, the number of remaining elements will be appended as the final
	 * element.
	 * <p>
	 * If <code>maxElements</code> is less than one, than all elements will be printed.
	 * <p>
	 * Examples:
	 * <ul>
	 * <li><code>["hello", "world", and 7 more elements]</code>
	 * <li><code>["hello", "world", +7 more]</code>
	 * <li><code>["hello", "world", ...]</code>
	 * </ul>
	 */
	public static <T> String toString(Collection<T> collection, int maxElements, IntFunction<String> remainderStrFn) {
		if (collection != null) {
			StringBuilder result = new StringBuilder();
			result.append('[');
			int size = collection.size();
			int index = 0;
			for (Object element : collection) {
				if (index > 0) {
					result.append(", ");
				}
				result.append(element);
				index++;
				if (index == maxElements) {
					if (size > maxElements) {
						result.append(", ").append(remainderStrFn.apply(size - maxElements));
					}
					break;
				}
			}
			result.append(']');
			return result.toString();
		}
		return null;
	}


	/**
	 * Return the first element of the specified collection which can have multiple elements.
	 * Returns null if collection is empty or the first element is null.
	 */
	public static <T> T getFirstElement(Collection<T> collection) {
		int size = getSize(collection);
		if (size == 0) {
			return null;
		}
		return collection.iterator().next();
	}


	/**
	 * Returns the first element of the specified collection. The collection can have more than 1 element.
	 *
	 * @throws NoSuchElementException   if the collection contains no elements.
	 * @throws IllegalArgumentException if the first element is null.
	 */
	public static <T> T getFirstElementStrict(Collection<T> collection) {
		return getFirstElementStrict(collection, "Cannot retrieve first element of collection because it is empty.", "The first element in the specified collection cannot be null: " + collection);
	}


	public static <T> T getFirstElementStrict(Collection<T> collection, String exceptionMessage) {
		return getFirstElementStrict(collection, exceptionMessage, exceptionMessage);
	}


	public static <T> T getFirstElementStrict(Collection<T> collection, String noElementsExceptionMessage, String nullValueExceptionMessage) {
		int size = getSize(collection);
		if (size == 0) {
			throw new NoSuchElementException(noElementsExceptionMessage);
		}
		T result = collection.iterator().next();
		if (result == null) {
			throw new IllegalArgumentException(nullValueExceptionMessage);
		}
		return result;
	}


	/**
	 * Return the last element of the specified collection which can have multiple elements.
	 * Returns null if collection is empty or the last element is null.
	 * Has optimizations for some types of collections (e.g. {@link List}).
	 */
	public static <T> T getLastElement(Collection<T> collection) {
		if (CollectionUtils.isEmpty(collection)) {
			return null;
		}
		if (collection instanceof List) {
			List<T> list = (List<T>) collection;
			return list.get(CollectionUtils.getSize(list) - 1);
		}
		return getLastElement(collection.iterator());
	}


	/**
	 * Return the last element for the specified iterator which can have multiple elements.
	 * Returns null if iterator has no elements or the last element is null.
	 */
	public static <T> T getLastElement(Iterator<T> iterator) {
		if (iterator == null || !iterator.hasNext()) {
			return null;
		}
		T current = iterator.next();
		while (iterator.hasNext()) {
			current = iterator.next();
		}
		return current;
	}


	/**
	 * Returns the last element of the specified collection. The collection can have more than 1 element.
	 *
	 * @throws NoSuchElementException   if the collection contains no elements.
	 * @throws IllegalArgumentException if the last element is null.
	 */
	public static <T> T getLastElementStrict(Collection<T> collection) {
		return getLastElementStrict(collection, "Cannot retrieve last element of collection because it is empty.", "The last element in the specified collection cannot be null: " + collection);
	}


	public static <T> T getLastElementStrict(Collection<T> collection, String exceptionMessage) {
		return getLastElementStrict(collection, exceptionMessage, exceptionMessage);
	}


	public static <T> T getLastElementStrict(Collection<T> collection, String noElementsExceptionMessage, String nullValueExceptionMessage) {
		int size = getSize(collection);
		if (size == 0) {
			throw new NoSuchElementException(noElementsExceptionMessage);
		}

		if (collection instanceof List) {
			List<T> list = (List<T>) collection;
			T result = list.get(CollectionUtils.getSize(list) - 1);
			if (result == null) {
				throw new IllegalArgumentException(nullValueExceptionMessage);
			}
			return result;
		}

		Iterator<T> iterator = collection.iterator();
		if (!iterator.hasNext()) {
			throw new NoSuchElementException(noElementsExceptionMessage);
		}
		T current = iterator.next();
		while (iterator.hasNext()) {
			current = iterator.next();
		}
		if (current == null) {
			throw new IllegalArgumentException(nullValueExceptionMessage);
		}
		return current;
	}


	/**
	 * Returns the first element of the specified collection. The collection cannot have more than 1 element.
	 * Returns null if the collection is empty or the first element is empty.
	 *
	 * @throws IllegalArgumentException if the specified collection has more than one element
	 */
	public static <T> T getOnlyElement(Collection<T> collection) {
		return getOnlyElement(collection, null);
	}


	public static <T> T getOnlyElement(Collection<T> collection, String tooManyElementsExceptionMessage) {
		int size = getSize(collection);
		if (size == 0) {
			return null;
		}
		T result = collection.iterator().next();
		if (size > 1) {
			if (tooManyElementsExceptionMessage != null) {
				throw new IllegalArgumentException(tooManyElementsExceptionMessage);
			}
			else {
				String elementClass = (result == null) ? null : result.getClass().getName();
				throw new IllegalArgumentException("The size of the specified collection = " + size + " cannot be greater than 1. Elements: " + toString(collection, 2) + " of type " + elementClass);
			}
		}
		return result;
	}


	/**
	 * Returns the first element of the specified collection. The collection must have only 1 element.
	 *
	 * @throws NoSuchElementException   if the collection contains no elements.
	 * @throws IllegalArgumentException if the specified collection is empty, has more than one element, or the first element is null.
	 */
	public static <T> T getOnlyElementStrict(Collection<T> collection) {
		int size = getSize(collection);
		if (size == 0) {
			throw new NoSuchElementException("Cannot retrieve first element of collection because it is empty.");
		}
		T result = collection.iterator().next();
		if (size > 1) {
			String elementClass = (result == null) ? null : result.getClass().getName();
			throw new IllegalArgumentException("The size of the specified collection = " + size + " cannot be greater than 1. Elements: " + toString(collection, 2) + " of type " + elementClass);
		}
		if (result == null) {
			throw new IllegalArgumentException("The specified collection has only one element but it cannot be null: " + collection);
		}
		return result;
	}


	/**
	 * Returns a new ArrayList object populated with elements of the specified elements. Returns an empty list if the specified argument is null.
	 */
	@SafeVarargs
	public static <T> List<T> createList(T... elements) {
		if (elements == null || elements.length == 0) {
			return Collections.emptyList();
		}
		List<T> list = new ArrayList<>();
		Collections.addAll(list, elements);
		return list;
	}


	/**
	 * Returns the provided list if it is not null, or an empty list of type T if list is null.
	 */
	public static <T> List<T> asNonNullList(List<T> list) {
		return list == null ? Collections.emptyList() : list;
	}


	/**
	 * Returns a new HashSet object populated with the specified element(s).
	 * Returns an empty HashSet if the argument is null or missing.
	 */
	@SafeVarargs
	public static <T> Set<T> createHashSet(T... elements) {
		return createHashSet(new HashSet<>(), elements);
	}


	/**
	 * Returns a new LinkedHashSet object populated with the specified element(s).
	 * Returns an empty HashSet if the argument is null or missing.
	 */
	@SafeVarargs
	public static <T> Set<T> createLinkedHashSet(T... elements) {
		return createHashSet(new LinkedHashSet<>(), elements);
	}


	/**
	 * Returns a new Set populated with the specified element(s).
	 * Returns an empty HashSet if the argument is null or missing. This method
	 * allows a Set instance to be passed in (e.g. HashSet or LinkedHashSet).
	 */
	@SafeVarargs
	private static <T> Set<T> createHashSet(Set<T> hashSet, T... elements) {
		if (elements != null && elements.length != 0) {
			Collections.addAll(hashSet, elements);
		}
		return hashSet;
	}


	/**
	 * Converts the specified Set into ArrayList of the same elements.
	 */
	public static <T> List<T> toArrayList(Set<T> set) {
		if (set == null) {
			return null;
		}
		return new ArrayList<>(set);
	}


	public static <T> List<T> toPagingArrayList(List<T> list, int start, int pageSize) {
		if (list == null) {
			return new PagingArrayList<>();
		}
		int end = Math.min(start + pageSize, list.size());
		List<T> result = new ArrayList<>();
		for (int i = start; i < end; i++) {
			result.add(list.get(i));
		}
		return new PagingArrayList<>(result, start, list.size(), pageSize);
	}


	/**
	 * Converts the provided <code>newList</code> to a {@link PagingArrayList} using the paging properties from the <code>originalList</code> if provided. Paging properties include
	 * fields such as the start index, the page size, and the total count.
	 * <p>
	 * This method is useful when a new standard list has been created from an existing paged list, such as by copying and modifying individual elements or by adding or removing
	 * elements. This method allows the new list to inherit the paging properties from the original list so that the new list can include properties such as total element count.
	 *
	 * @param newList      the list of elements to include in the resulting list
	 * @param originalList the original list from which to copy paging properties; if this is not a paged list, then it will be ignored
	 * @param <T>          the list element type
	 * @return the resulting paged list
	 */
	public static <T> List<T> toPagingArrayList(List<T> newList, List<T> originalList) {
		// Guard-clause: Trivial case
		if (newList == null) {
			return new PagingArrayList<>();
		}
		// Convert to paged list
		final int firstElementIndex;
		final int totalElementCount;
		final int pageSize;
		if (originalList instanceof PagingArrayList) {
			PagingArrayList<T> pagedResultList = (PagingArrayList<T>) originalList;
			firstElementIndex = pagedResultList.getFirstElementIndex();
			totalElementCount = pagedResultList.getTotalElementCount();
			pageSize = pagedResultList.getPageSize();
		}
		else {
			firstElementIndex = 0;
			totalElementCount = CollectionUtils.getSize(newList);
			pageSize = CollectionUtils.getSize(newList);
		}
		return new PagingArrayList<>(newList, firstElementIndex, totalElementCount, pageSize);
	}


	/**
	 * Generates an array of the specified type from the given collection. If the collection is {@code null}, then an empty array of the specified type is
	 * returned.
	 * <p>
	 * An exception will be thrown if the given class is not a supertype of the runtime type of each element of the collection.
	 * <p>
	 * The order of the resulting array shall be as implemented by the {@link Collection#toArray(Object[])} method of the collection.
	 *
	 * @param collection the collection whose elements shall be inserted into the array
	 * @param clazz      the class type of the array to be generated
	 * @param <T>        the type of the array, which must also be a supertype of the generic type of the collection
	 * @return the generated array
	 * @see Collection#toArray(Object[])
	 * @see CollectionUtils#toArrayOrNull(Collection, Class)
	 */
	public static <T> T[] toArray(Collection<? extends T> collection, Class<T> clazz) {
		return toArray(collection, clazz, false);
	}


	/**
	 * Generates an array of the specified type from the given collection. If the collection is {@code null}, then a {@code null} value is returned.
	 *
	 * @see CollectionUtils#toArray(Collection, Class, boolean)
	 */
	public static <T> T[] toArrayOrNull(Collection<? extends T> collection, Class<T> clazz) {
		return toArray(collection, clazz, true);
	}


	/**
	 * Generates an array of the specified type from the given collection. If the collection is {@code null}, then an empty array of the specified type is
	 * returned.
	 * <p>
	 * An exception will be thrown if the given class is not a supertype of the runtime type of each element of the collection.
	 * <p>
	 * The order of the resulting array shall be as implemented by the {@link Collection#toArray(Object[])} method of the collection.
	 *
	 * @param collection  the collection whose elements shall be inserted into the array
	 * @param clazz       the class type of the array to be generated
	 * @param nullIfEmpty if {@code true} and the given collection is {@code null} or empty, then a {@code null} value will be returned; if {@code false} and
	 *                    the given collection is {@code null} or empty, then an empty array of the specified type will be returned
	 * @param <T>         the type of the array, which must also be a supertype of the generic type of the collection
	 * @return the generated array
	 * @see Collection#toArray(Object[])
	 * @see CollectionUtils#toArray(Collection, Class)
	 * @see CollectionUtils#toArrayOrNull(Collection, Class)
	 */
	private static <T> T[] toArray(Collection<? extends T> collection, Class<T> clazz, boolean nullIfEmpty) {
		final T[] result;
		if (nullIfEmpty && CollectionUtils.isEmpty(collection)) {
			// Trivial case: Return null
			result = null;
		}
		else if (CollectionUtils.isEmpty(collection)) {
			// Trivial case: Return empty array
			@SuppressWarnings("unchecked")
			T[] emptyArray = (T[]) Array.newInstance(clazz, 0);
			result = emptyArray;
		}
		else {
			// Generate array from collection
			@SuppressWarnings("unchecked")
			T[] populatedArray = (T[]) Array.newInstance(clazz, collection.size());
			result = collection.toArray(populatedArray);
		}
		return result;
	}



	/**
	 * Returns true if the list is not null and contains the entity
	 */
	public static <T> boolean contains(Collection<T> list, T entity) {
		if (list != null) {
			return list.contains(entity);
		}
		return false;
	}


	/**
	 * Returns {@code true} if the list contains any element which matches the given predicate.
	 */
	public static <T> boolean anyMatch(Collection<T> list, Predicate<T> matcher) {
		return !isEmpty(list) && list.stream().anyMatch(matcher);
	}


	/**
	 * Returns {@code true} if all elements within the list match the given predicate or when there are no elements in the list.
	 */
	public static <T> boolean allMatch(Collection<T> list, Predicate<T> matcher) {
		return isEmpty(list) || list.stream().allMatch(matcher);
	}


	/**
	 * Returns true if the list contains at least one duplicate value.
	 */
	public static <T> boolean containsDuplicate(List<T> list) {
		if (isEmpty(list)) {
			return false;
		}

		for (int i = 0; i < list.size() - 1; i++) {
			for (int j = i + 1; j < list.size(); j++) {
				if (list.get(i).equals(list.get(j))) {
					return true;
				}
			}
		}

		return false;
	}



	/**
	 * Removes any duplicates from the specified list.
	 *
	 * @return a clone of the specified list with duplicates removed
	 */
	public static <T> List<T> removeDuplicates(Collection<T> list) {
		if (list == null) {
			return null;
		}

		Set<T> deDupeSet = new LinkedHashSet<>(list);
		return new ArrayList<>(deDupeSet);
	}


	/**
	 * @return a new concurrent {@link Set} backed by a {@link ConcurrentHashMap#newKeySet()}.
	 */
	public static <T> Set<T> newConcurrentHashSet() {
		return ConcurrentHashMap.newKeySet();
	}


	/**
	 * Creates a new concurrent {@link Set} backed by a {@link ConcurrentHashMap#newKeySet()}
	 *
	 * @param elements optional elements to load into the new set
	 * @param <T>      the generic type of the elements in the set
	 * @return {@link Set} populated with the elements provided.
	 */
	@SafeVarargs
	public static <T> Set<T> newConcurrentHashSet(T... elements) {
		Set<T> result = newConcurrentHashSet();
		if (elements != null && elements.length != 0) {
			Collections.addAll(result, elements);
		}
		return result;
	}


	/**
	 * Creates a new unmodifiable {@link Set} from the given elements.
	 *
	 * @param elements the elements which shall exist in the resulting set
	 * @param <T>      the type of the elements
	 * @return an unmodifiable {@link Set} of the provided elements
	 * @implNote the resulting set is backed by {@link #createHashSet(Object[])}
	 */
	@SafeVarargs
	public static <T> Set<T> newUnmodifiableSet(T... elements) {
		return Collections.unmodifiableSet(createHashSet(elements));
	}


	/**
	 * @param initialCapacity the initial capacity of the underlying {@link ConcurrentHashMap#newKeySet(int)}.
	 * @return a new concurrent {@link Set} backed by a {@link ConcurrentHashMap#newKeySet(int)} with the specified initial capacity.
	 */
	public static <T> Set<T> newConcurrentHashSet(int initialCapacity) {
		return ConcurrentHashMap.newKeySet(initialCapacity);
	}


	/**
	 * Filters the given list into distinct items based on object equality.
	 *
	 * @param itemCollection the collection of items to be processed
	 * @param <T>            the type of the items
	 * @return the list of distinct items in the collection
	 */
	public static <T> List<T> getDistinct(Collection<T> itemCollection) {
		return getStream(itemCollection)
				.distinct()
				.collect(Collectors.toList());
	}


	/**
	 * Filters the given list into distinct items, performing no reductions.
	 *
	 * @see #getDistinct(Collection, Function, BinaryOperator)
	 */
	public static <T> List<T> getDistinct(Collection<T> itemCollection, Function<T, Object> groupingKeyGenerator) {
		return getDistinct(itemCollection, groupingKeyGenerator, null);
	}


	/**
	 * Filters the given list into distinct items. Entities are regarded as similar if they yield the same key via the grouping key generator. For each set of
	 * similar items, only the first item discovered will exist in the resulting list.
	 * <p>
	 * If a reduction function is provided, then that function will be applied to each set of similar items in the order of items discovered. The function takes
	 * as its parameters the current item (including all prior reductions) and the next similar item, and returns the reduction of those two items.
	 *
	 * @param <T>                  the type of the items
	 * @param itemCollection       the collection of items to be processed
	 * @param groupingKeyGenerator function generating a key for an item, for which the key determines the distinctness of the item
	 * @param reductionFunction    function taking in the current item (including previous reductions) and the next item and returning the resulting reduced
	 *                             item
	 * @return the filtered and reduced list of items
	 */
	public static <T> List<T> getDistinct(Collection<T> itemCollection, Function<T, Object> groupingKeyGenerator, BinaryOperator<T> reductionFunction) {
		// Group items into ordered buckets of duplicate items
		Object nullKey = new Object(); // Object placeholder to allow null keys
		Map<?, List<T>> itemListByGroup = getStream(itemCollection)
				.collect(Collectors.groupingBy(item -> ObjectUtils.coalesce(groupingKeyGenerator.apply(item), nullKey), LinkedHashMap::new, Collectors.toList()));

		// Reduce each bucket to a single item
		List<T> groupedEntityList = new ArrayList<>();
		for (List<T> singleGroupEntityList : itemListByGroup.values()) {
			// Take first item, and if applicable then perform reduction method on all remaining items
			T reducedEntity = singleGroupEntityList.get(0);
			if (reductionFunction != null) {
				for (int itemIndex = 1; itemIndex < singleGroupEntityList.size(); itemIndex++) {
					reducedEntity = reductionFunction.apply(reducedEntity, singleGroupEntityList.get(itemIndex));
				}
			}
			groupedEntityList.add(reducedEntity);
		}
		return groupedEntityList;
	}


	/**
	 * Generates a {@link Predicate} for use with {@link Stream#filter(Predicate) stream filtering} operations to filter to distinct elements by the given key-generation function.
	 * <p>
	 * This can be used to produce a proxy for equality comparisons when finding distinct elements of a stream.
	 * <p>
	 * Take, for example, a scenario in which new entities are being generated where the {@link Object#equals(Object) equality operator} for these entities compares the IDs and
	 * types of those entities only. If we wish to find distinct entities in this set, the standard equality operator may be insufficient since the IDs of these entities may not
	 * yet be set. In this case, we could use a distinct predicate to find distinct entities by a specified key, such as an entity name. Such a scenario may look something like the
	 * following:
	 * <pre><code>
	 * Collection&lt;Entity&gt; newEntityList = createEntityList();
	 * Collection&lt;Entity&gt; distinctEntityList = newEntityList.stream()
	 *         .filter(CollectionUtils.createDistinctPredicate(Entity::getName))
	 *         .collect(Collectors.toList());
	 * </code></pre>
	 * <p>
	 *
	 * @param distinctByFunction the key-generating {@link Function} to be used for finding distinct elements
	 * @param <T>                the type of the source object
	 * @param <U>                the type of the key object
	 * @return a {@link Predicate} which returns <code>true</code> if the key generated for the object has <i>not yet</i> been seen, or <code>false</code> otherwise
	 */
	public static <T, U> Predicate<T> createDistinctPredicate(Function<T, U> distinctByFunction) {
		Collection<U> existingValueList = ConcurrentHashMap.newKeySet();
		return item -> existingValueList.add(distinctByFunction.apply(item));
	}


	/**
	 * Generates a one-to-one mapping of items to corresponding values via the specified <tt>valueMapper</tt>. This method can be used to generate values that remain associated
	 * with their source item.
	 *
	 * @param itemCollection the collection of items for which corresponding values should be generated
	 * @param valueMapper    the function which shall be used to generate a value from each source item
	 * @param <T>            the type of the source item
	 * @param <R>            the type of the generated item
	 * @return the map of items to their corresponding values
	 */
	public static <T, R> Map<T, R> getMapped(Collection<T> itemCollection, Function<T, R> valueMapper) {
		if (CollectionUtils.isEmpty(itemCollection)) {
			return Collections.emptyMap();
		}

		/*
		 * Collect values into a map. This intentionally avoids the use of Collectors#toMap in order to allow null map entry values.
		 *
		 * Collectors generated via Collectors#toMap use the Map#merge method. This method has special handling for null return values and thus does not allow null input values.
		 * Attempting to handle a null value with such a collector would result in a NullPointerException being thrown.
		 */
		return CollectionUtils.getStream(itemCollection)
				.collect(HashMap::new, (map, value) -> map.put(value, valueMapper.apply(value)), Map::putAll);
	}


	/**
	 * Generates a new collection of all elements in the given collection which satisfy the given predicate. The elements of the resulting collection will follow their iteration
	 * order from the original collection.
	 * <p>
	 * If the given collection is {@code null}, an empty list will be returned.
	 * <p>
	 * There are no guarantees on the type, mutability, serializability, or thread-safety of the {@link List} returned.
	 *
	 * @param itemCollection  the original collection
	 * @param filterPredicate the predicate which shall be used to filter items from the given collection
	 * @param <T>             the type of the collection
	 * @return a collection of the elements from the given collection which satisfy the given predicate
	 */
	public static <T> List<T> getFiltered(Collection<T> itemCollection, Predicate<T> filterPredicate) {
		if (CollectionUtils.isEmpty(itemCollection)) {
			return Collections.emptyList();
		}

		return CollectionUtils.getStream(itemCollection)
				.filter(filterPredicate)
				.collect(Collectors.toList());
	}


	/**
	 * Generates a new collection from the given collection, converting all elements according to the provided conversion function. The resulting collection will follow their
	 * iteration order from the original collection.
	 * <p>
	 * If the given collection is {@code null}, an empty list will be returned.
	 * <p>
	 * There are no guarantees on the type, mutability, serializability, or thread-safety of the {@link List} returned.
	 *
	 * @param itemCollection     the original collection
	 * @param conversionFunction the function converting elements from the original collection
	 * @param <T>                the type of the original collection
	 * @param <R>                the type of the resulting collection
	 * @return a collection of the converted elements using the iteration order of the original collection
	 */
	public static <T, R> List<R> getConverted(Collection<T> itemCollection, Function<T, R> conversionFunction) {
		if (CollectionUtils.isEmpty(itemCollection)) {
			return Collections.emptyList();
		}

		return CollectionUtils.getStream(itemCollection)
				.map(conversionFunction)
				.collect(Collectors.toList());
	}


	/**
	 * Generates a flattened collection from the given collection. All non-null elements in the collection of {@link Collection} objects are flattened into the resulting list based
	 * on their iteration order.
	 *
	 * @param itemCollection the original collection
	 * @param <T>            the type of the original collection
	 * @param <R>            the type of the resulting collection
	 * @return a collection of the flattened elements using the iteration order of the original collection and its sub-collections
	 */
	public static <T extends Collection<R>, R> List<R> getFlattened(Collection<T> itemCollection) {
		if (CollectionUtils.isEmpty(itemCollection)) {
			return Collections.emptyList();
		}

		return CollectionUtils.getStream(itemCollection)
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());
	}


	/**
	 * Reduces the collection into a single object. Each element will be applied sequentially to the provided <code>identity</code> object using the given <code>accumulator</code>.
	 * The result of the final accumulation will be returned.
	 * <p>
	 * If the collection is <code>null</code> or empty, then the <code>identity</code> will be returned.
	 *
	 * @param itemCollection the collection to reduce
	 * @param identity       the object which will be used as the initial object upon which accumulation is performed
	 * @param accumulator    the accumulation function which ingests the current state of the <code>identity</code> object and the next collection element to produce a new object
	 * @param <T>            the type of the collection
	 * @param <U>            the type of the result
	 * @return the reduction of the given collection
	 * @see Stream#reduce(Object, BiFunction, BinaryOperator)
	 */
	public static <T, U> U getReduced(Collection<T> itemCollection, U identity, BiFunction<U, T, U> accumulator) {
		if (CollectionUtils.isEmpty(itemCollection)) {
			return identity;
		}

		return CollectionUtils.getStream(itemCollection)
				.reduce(identity, accumulator, throwingCombiner());
	}


	/**
	 * Generates a new collection from the given collection, converting all elements into lists according to the provided conversion function and then flattening the resulting
	 * lists.
	 * <p>
	 * If the given collection is {@code null}, an empty list will be returned.
	 * <p>
	 * There are no guarantees on the type, mutability, serializability, or thread-safety of the {@link List} returned.
	 *
	 * @param itemCollection     the original collection
	 * @param conversionFunction the function converting elements from the original collection into lists
	 * @param <T>                the type of the original collection
	 * @param <R>                the type of the resulting collection
	 * @return a flattened collection of the converted elements using the iteration order of the original collection
	 */
	public static <T, R> List<R> getConvertedFlattened(Collection<T> itemCollection, Function<T, Collection<R>> conversionFunction) {
		// Guard-clause: Trivial-case (empty)
		if (CollectionUtils.isEmpty(itemCollection)) {
			return Collections.emptyList();
		}

		// Guard-clause: Minor optimization for single-element collections
		if (CollectionUtils.getSize(itemCollection) == 1) {
			T singleElement = CollectionUtils.getFirstElement(itemCollection);
			return new ArrayList<>(conversionFunction.apply(singleElement));
		}

		return CollectionUtils.getStream(itemCollection)
				.map(conversionFunction)
				.flatMap(CollectionUtils::getStream)
				.collect(Collectors.toList());
	}


	/**
	 * Generates a flattened collection from the given collections and converts each element using the conversion function into a new collection.
	 * <p>
	 * If no collections are provided, an empty collection will be returned.
	 * <p>
	 * Null collection elements will be removed.
	 *
	 * @param conversionFunction the conversion function for converting type of the original collection into the resulting collection
	 * @param distinct           <code>true</code> to return a set, <code>false</code> for list
	 * @param itemCollections    one or more collections for converting elements
	 * @param <T>                the type of the original collection
	 * @param <R>                the type of the resulting collection
	 */
	@SafeVarargs
	public static <T, R> Collection<R> getFlattenedConverted(Function<T, R> conversionFunction, boolean distinct, Collection<? extends T>... itemCollections) {
		return buildStream(itemCollections)
				.filter(Objects::nonNull)
				.map(conversionFunction)
				.collect(Collectors.toCollection(() -> distinct ? new HashSet<>() : new ArrayList<>()));
	}


	/**
	 * Generates a <b>histogram</b> from the given collection, where each key is a collection element (compared via its {@link Object#equals(Object) equals} method) and each value
	 * is the frequency of that element within the provided collection.
	 * <p>
	 * A <a href="https://en.wikipedia.org/wiki/Histogram">histogram</a> is a representation of the frequency of elements within a set.
	 * <ul>
	 * <li><b>Key:</b> The element within the original collection</li>
	 * <li><b>Value:</b> The number of instances of equivalent elements within the original collection</li>
	 * </ul>
	 *
	 * @param itemCollection the collection of elements
	 * @param <T>            the type of the collection elements
	 * @return a histogram reporting the frequency of each element in the collection
	 */
	public static <T> Map<T, Long> getHistogram(Collection<T> itemCollection) {
		return CollectionUtils.getStream(itemCollection)
				.collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
	}


	/**
	 * Partitions the collection using the provided predicate.
	 * <p>
	 * The resulting map will contain two keys: <code>true</code> and <code>false</code>. The mapped value for each of these keys is a list containing all elements for which the
	 * predicate returns the key value.
	 *
	 * @param itemCollection the collection of elements
	 * @param predicate      the partitioning predicate determining which bucket results should be placed in
	 * @param <T>            the type of the collection elements
	 * @return a map with two keys, <code>true</code> and <code>false</code>, whose map values are lists containing the partitioned items
	 */
	public static <T> Map<Boolean, List<T>> getPartitioned(Collection<T> itemCollection, Predicate<T> predicate) {
		return CollectionUtils.getStream(itemCollection)
				.collect(Collectors.partitioningBy(predicate));
	}


	/**
	 * Traverses an item chain using the given iterator function, producing a list of each of the items in the chain in order.
	 * <p>
	 * The resulting chain includes the <code>head</code> and all subsequent items via the <code>chainIteratorFn</code> until a <code>null</code> element is reached:
	 * <pre><code>
	 * [head, child1, child2, ..., childN]
	 * </code></pre>
	 * <p>
	 * This utility method is helpful for traversing linked entities, such as hierarchies, to produce a collection of items that can be analyzed and operated as a standard {@link
	 * Collection}.
	 *
	 * @param <T>             the element type for the chain
	 * @param head            the head of the chain
	 * @param chainIteratorFn the function which produces the next item from any element in a chain
	 * @return the generated chain
	 */
	public static <T> List<T> getChain(T head, UnaryOperator<T> chainIteratorFn) {
		Collection<T> chainList = new LinkedHashSet<>();
		T currentItem = head;
		while (currentItem != null) {
			AssertUtils.assertTrue(chainList.add(currentItem), "Recursion detected during chain traversal. Discovered items: %s. Repeated item: [%s].", chainList, currentItem);
			currentItem = chainIteratorFn.apply(currentItem);
		}
		return new ArrayList<>(chainList);
	}


	/**
	 * Traverses an item chain using the given iterator function, checking to see if any elements in the chain match the given <code>matcher</code> predicate. This will return
	 * <code>true</code> if any elements are matched or <code>false</code> otherwise.
	 * <p>
	 * This utility method is useful when traversing linked entities, such as hierarchies, to check if any link satisfies the provided condition. This method uses short-circuiting
	 * to avoid unnecessary operations that could be executed by generating the entire chain before evaluation.
	 *
	 * @param <T>             the type of the chain elements
	 * @param head            the head of the chain
	 * @param chainIteratorFn the function which produces the next item from any element in a chain
	 * @param matcher         the predicate to evaluate against each item in the chain
	 * @return <code>true</code> if the <code>matcher</code> is matched by any item in the chain or <code>false</code> otherwise
	 * @see #getChain(Object, UnaryOperator)
	 */
	public static <T> boolean anyMatchInChain(T head, UnaryOperator<T> chainIteratorFn, Predicate<T> matcher) {
		boolean result = false;
		Collection<T> chainCollection = new HashSet<>();
		T currentItem = head;
		while (currentItem != null) {
			if (matcher.test(currentItem)) {
				result = true;
				break;
			}
			AssertUtils.assertTrue(chainCollection.add(currentItem), "Recursion detected during chain traversal. Discovered items: %s. Repeated item: [%s]", chainCollection, currentItem);
			currentItem = chainIteratorFn.apply(currentItem);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #throwingMerger(String)
	 */
	public static <T> BinaryOperator<T> throwingMerger() {
		return throwingMerger("Unexpected key collision with values: [%s], [%s]");
	}


	/**
	 * Generates a <i>merge function</i> which always throws an exception with the given message if executed.
	 * <p>
	 * The returned operator is a {@link BinaryOperator} suitable for use as the remapping function when calling {@link Map#merge(Object, Object, BiFunction)} or {@link
	 * Collectors#toMap(Function, Function, BinaryOperator, Supplier)}. This operator can be used to enforce the assumption that the elements being collected are distinct.
	 * <p>
	 * The placeholders in the provided message format are substituted with the first value and the second value passed to the merge function, respectively.
	 *
	 * @param messageFormat the message format to apply to the thrown exception
	 * @param <T>           the type of input arguments to the merge function
	 * @see Collectors#throwingMerger()
	 */
	public static <T> BinaryOperator<T> throwingMerger(String messageFormat) {
		return (val1, val2) -> {
			throw new IllegalStateException(String.format(messageFormat, val1, val2));
		};
	}


	/**
	 * @see #throwingCombiner(String)
	 */
	public static <T> BinaryOperator<T> throwingCombiner() {
		return throwingCombiner("Unexpected collection combination attempt. Parallel execution may have been used when serial execution was expected." +
				" Combination values: [%s], [%s]");
	}


	/**
	 * Generates a <i>combiner function</i> which always throws an exception if executed.
	 * <p>
	 * The returned function is a {@link BinaryOperator} suitable for use as a combiner function to be returned by {@link Collector#combiner()}, such as when calling {@link
	 * Stream#reduce(Object, BiFunction, BinaryOperator)} or {@link Collectors#reducing(Object, Function, BinaryOperator)}. This operator can be used to enforce the assumption the
	 * stream is not executed in parallel and thus the combiner function is never executed.
	 * <p>
	 * The placeholders in the provided message format are substituted with the first value and the second value passed to the combiner function, respectively.
	 *
	 * @param messageFormat the message format to apply to the thrown exception
	 * @param <T>           the type of the elements to be combined
	 */
	public static <T> BinaryOperator<T> throwingCombiner(String messageFormat) {
		return (val1, val2) -> {
			throw new IllegalStateException(String.format(messageFormat, val1, val2));
		};
	}


	/**
	 * @see #throwingCollectCombiner(String)
	 */
	public static <T> BiConsumer<T, T> throwingCollectCombiner() {
		return throwingCollectCombiner("Unexpected collection combination attempt. Parallel execution may have been used when serial execution was expected." +
				" Combination values: [%s], [%s]");
	}


	/**
	 * Generates a <i>combiner function</i> which always throws an exception if executed.
	 * <p>
	 * The returned function is a {@link BiConsumer} suitable for use as a combiner function when calling {@link Stream#collect(Supplier, BiConsumer, BiConsumer)}. This operator
	 * can be used to enforce the assumption the stream is not executed in parallel and thus the combiner function is never executed.
	 * <p>
	 * The placeholders in the provided message format are substituted with the first value and the second value passed to the combiner function, respectively.
	 *
	 * @param messageFormat the message format to apply to the thrown exception
	 * @param <T>           the type of the elements to be combined
	 */
	public static <T> BiConsumer<T, T> throwingCollectCombiner(String messageFormat) {
		return (val1, val2) -> {
			throw new IllegalStateException(String.format(messageFormat, val1, val2));
		};
	}
}
