package com.clifton.core.util.collections;


import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>MultiValueHashMap</code> is an implementation of {@link MultiValueMap} that is backed by a {@link HashMap} that maps a key to a collection of
 * values. Static factory methods are provided for constructing thread-safe instances.
 *
 * @param <K> the type of keys maintained
 * @param <V> the type of mapped values
 * @author jgommels
 */
public class MultiValueHashMap<K, V> implements MultiValueMap<K, V> {

	/**
	 * The default initial capacity to instantiate {@link ArrayList}s with.
	 */
	private static final int DEFAULT_INITIAL_COLLECTION_CAPACITY = 8;

	private final Map<K, Collection<V>> map;
	private final CollectionFactory<V> collectionFactory;


	/**
	 * Constructs an empty non-concurrent <code>MultiValueHashMap</code>.
	 *
	 * @param allowDuplicates whether to allow duplicate values <b>within each collection that a key maps to</b>
	 */
	public MultiValueHashMap(boolean allowDuplicates) {
		this(allowDuplicates, DEFAULT_INITIAL_COLLECTION_CAPACITY);
	}


	public MultiValueHashMap(CollectionFactory<V> collectionFactory) {
		this.collectionFactory = collectionFactory;
		this.map = new HashMap<>();
	}


	/**
	 * Creates a shallow copy of a {@link MultiValueMap}.
	 *
	 * @param multiValueMap   the {@link MultiValueMap} to make a copy of
	 * @param allowDuplicates whether to allow duplicate values <b>within each collection that a key maps to</b>
	 */
	public MultiValueHashMap(MultiValueMap<K, V> multiValueMap, boolean allowDuplicates) {
		this(allowDuplicates);

		for (Map.Entry<K, Collection<V>> entry : multiValueMap.entrySet()) {
			putAll(entry.getKey(), entry.getValue());
		}
	}


	/**
	 * Constructs an empty non-concurrent <code>MultiValueHashMap</code> which will instantiate new value collections with the supplied initial capacity.
	 *
	 * @param allowDuplicates           whether to allow duplicate values <b>within each collection that a key maps to</b>
	 * @param initialCollectionCapacity the initial capacity of the collections instantiated by this <code>MultiValueHashMap</code>
	 */
	public MultiValueHashMap(boolean allowDuplicates, int initialCollectionCapacity) {
		this(getCollectionFactory(allowDuplicates, initialCollectionCapacity));
	}


	private static <V> CollectionFactory<V> getCollectionFactory(boolean allowDuplicates, final int initialCollectionCapacity) {
		CollectionFactory<V> collectionFactory;
		if (allowDuplicates) {
			collectionFactory = () -> new ArrayList<>(initialCollectionCapacity);
		}
		else {
			collectionFactory = () -> new HashSet<>(initialCollectionCapacity);
		}

		return collectionFactory;
	}


	@Override
	public int keyCount() {
		return keySet().size();
	}


	@Override
	public int valueCount() {
		int count = 0;
		for (Collection<V> collection : CollectionUtils.getIterable(this.map.values())) {
			count += collection.size();
		}

		return count;
	}


	@Override
	public boolean isEmpty() {
		return keyCount() == 0;
	}


	@Override
	public List<V> get(K key) {
		Collection<V> result = this.map.get(key);

		//If result is not null, then make a copy of the list before returning it.
		if (result != null) {
			return new ArrayList<>(result);
		}

		return null;
	}


	@Override
	public Set<Map.Entry<K, Collection<V>>> entrySet() {
		return new HashSet<>(this.map.entrySet());
	}


	@Override
	public Set<K> keySet() {
		return new HashSet<>(this.map.keySet());
	}


	@Override
	public List<V> values() {
		List<V> allValues = new ArrayList<>();
		for (Collection<V> keyValues : CollectionUtils.getValues(this.map)) {
			allValues.addAll(keyValues);
		}

		return allValues;
	}


	@Override
	public Map<K, Collection<V>> cloneMap() {
		return new HashMap<>(this.map);
	}


	@Override
	public void put(K key, V value) {
		//If there isn't a match, then this is a new key
		Collection<V> valueList = this.map.computeIfAbsent(key, k -> this.collectionFactory.createCollection());

		valueList.add(value);
	}


	@Override
	public void putAll(K key, Collection<? extends V> values) {
		//If there isn't a match, then this is a new key
		Collection<V> valueList = this.map.computeIfAbsent(key, k -> this.collectionFactory.createCollection());

		valueList.addAll(values);
	}


	@Override
	public void putAll(MultiValueMap<? extends K, ? extends V> otherMap) {
		for (Map.Entry<? extends K, ? extends Collection<? extends V>> entry : otherMap.entrySet()) {
			putAll(entry.getKey(), entry.getValue());
		}
	}


	@Override
	public boolean remove(K key, V value) {

		boolean valueRemoved = false; //initialize

		Collection<V> values = this.map.get(key);

		if (values != null) {
			valueRemoved = values.remove(value);
			if (CollectionUtils.isEmpty(values)) {
				this.map.remove(key);
			}
		}

		return valueRemoved;
	}


	@Override
	public List<V> removeAll(K key) {

		Collection<V> valuesBeingRemoved = this.map.remove(key);

		List<V> listToReturn;

		if (valuesBeingRemoved == null) {
			listToReturn = null;
		}
		else if (valuesBeingRemoved instanceof List) {
			listToReturn = (List<V>) valuesBeingRemoved;
		}
		else {
			listToReturn = new ArrayList<>(valuesBeingRemoved);
		}

		return listToReturn;
	}


	@Override
	public boolean replace(K key, V oldValue, V newValue) {
		boolean removed = remove(key, oldValue);
		put(key, newValue);

		return removed;
	}


	@Override
	public boolean replace(K oldKey, V oldValue, K newKey, V newValue) {
		boolean removed = remove(oldKey, oldValue);
		put(newKey, newValue);

		return removed;
	}
}
