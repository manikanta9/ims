package com.clifton.core.util.validation;


/**
 * The <code>FieldValidationException</code> class represents a validation problem with one field.
 *
 * @author vgomelsky
 */
public class FieldValidationException extends ValidationException {

	private final String fieldName;


	/**
	 * Constructs a new field validation runtime exception. Preserves the specified cause.
	 *
	 * @param message   the detail message
	 * @param fieldName the bean field name that fails validation
	 * @param cause
	 */
	public FieldValidationException(String message, String fieldName, Throwable cause) {
		super(message, cause);
		this.fieldName = fieldName;
	}


	/**
	 * Constructs a new field validation runtime exception.
	 *
	 * @param message   the detail message
	 * @param fieldName the bean field name that fails validation
	 */
	public FieldValidationException(String message, String fieldName) {
		super(message);
		this.fieldName = fieldName;
	}


	/**
	 * Constructs a new field validation runtime exception using the "id" field.
	 * Use the other constructor and specified invalid field name when possible.
	 *
	 * @param message the detail message
	 */
	public FieldValidationException(String message) {
		this(message, "id");
	}


	/**
	 * Returns bean name of the field that caused validation problem.
	 *
	 * @return the fieldName
	 */
	public String getFieldName() {
		return this.fieldName;
	}
}
