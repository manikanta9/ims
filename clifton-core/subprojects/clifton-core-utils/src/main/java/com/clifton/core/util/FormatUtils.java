package com.clifton.core.util;


import java.text.DecimalFormat;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>FormatUtils</code> class provides formatting utilities.
 * It caches various Format objects to improve performance.
 *
 * @author vgomelsky
 */
public class FormatUtils {

	private static final ThreadLocal<Map<String, Format>> formatMap = ThreadLocal.withInitial(HashMap::new);


	/**
	 * Returns the {@link SimpleDateFormat} for the specified dateFormat string.
	 * Stores instances of {@link SimpleDateFormat} using {@link ThreadLocal} to improve performance
	 * because SimpleDateFormat is not thread safe (cannot cache static).
	 * <p/>
	 * Don't really need to clean them.
	 *
	 * @param dateFormat
	 */
	public static SimpleDateFormat getSimpleDateFormat(String dateFormat) {
		Map<String, Format> map = formatMap.get();
		SimpleDateFormat result = (SimpleDateFormat) map.get(dateFormat);
		if (result == null) {
			result = new SimpleDateFormat(dateFormat);
			map.put(dateFormat, result);
			formatMap.set(map);
		}
		return result;
	}


	/**
	 * Returns the {@link DecimalFormat} for the specified decimalFormat string.
	 * Stores instances of {@link DecimalFormat} using {@link ThreadLocal} to improve performance
	 * because DecimalFormat is not thread safe (cannot cache static).
	 * <p/>
	 * Don't really need to clean them.
	 *
	 * @param decimalFormat
	 */
	public static DecimalFormat getDecimalFormat(String decimalFormat) {
		Map<String, Format> map = formatMap.get();
		DecimalFormat result = (DecimalFormat) map.get(decimalFormat);
		if (result == null) {
			result = new DecimalFormat(decimalFormat);
			map.put(decimalFormat, result);
			formatMap.set(map);
		}
		return result;
	}
}
