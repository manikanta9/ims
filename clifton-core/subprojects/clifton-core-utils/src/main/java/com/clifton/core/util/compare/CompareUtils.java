package com.clifton.core.util.compare;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;

import java.lang.reflect.Array;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


/**
 * The {@link CompareUtils} class contains helper utility methods for comparing various objects.
 *
 * @author vgomelsky
 */
public class CompareUtils {

	/**
	 * Returns true if the specified objects are equal (using equals method).
	 * Returns true if both objects are nulls.
	 */
	public static boolean isEqual(Object o1, Object o2) {
		if (o1 == null) {
			return o2 == null;
		}
		if (o1.getClass().isArray()) {
			if (o2 == null || !o2.getClass().isArray()) {
				return false;
			}
			if (Array.getLength(o1) != Array.getLength(o2)) {
				return false;
			}
			Class<?> elementType = o1.getClass().getComponentType();
			if (long.class.equals(elementType)) {
				return Arrays.equals((long[]) o1, (long[]) o2);
			}
			else if (int.class.equals(elementType)) {
				return Arrays.equals((int[]) o1, (int[]) o2);
			}
			else if (short.class.equals(elementType)) {
				return Arrays.equals((short[]) o1, (short[]) o2);
			}
			else if (char.class.equals(elementType)) {
				return Arrays.equals((char[]) o1, (char[]) o2);
			}
			else if (byte.class.equals(elementType)) {
				return Arrays.equals((byte[]) o1, (byte[]) o2);
			}
			else if (boolean.class.equals(elementType)) {
				return Arrays.equals((boolean[]) o1, (boolean[]) o2);
			}
			else if (double.class.equals(elementType)) {
				return Arrays.equals((double[]) o1, (double[]) o2);
			}
			else if (float.class.equals(elementType)) {
				return Arrays.equals((float[]) o1, (float[]) o2);
			}
			else if (Object.class.isAssignableFrom(elementType)) {
				return Arrays.equals((Object[]) o1, (Object[]) o2);
			}
			else {
				// Unsupported comparison
				throw new RuntimeException(String.format("Comparison of arrays of type [%s] is not supported.", elementType.getName()));
			}
		}
		return o1.equals(o2);
	}


	/**
	 * Returns true if the two objects are considered equal using "smart" logic. The smart logic is used if the object is of a certain type, such as {@link Number} or {@link Date}.
	 * For example, {@link Number} objects are compared using {@link MathUtils#isEqual(Number, Number)}. If neither objects match one of these special types, then
	 * the default behavior is to use the standard equals() method on the object.
	 */
	public static boolean isEqualWithSmartLogic(Object o1, Object o2) {
		if (o1 == null) {
			return o2 == null;
		}
		if (o1 == o2) {
			return true;
		}
		if (o1 instanceof Number && o2 instanceof Number) {
			return MathUtils.isEqual((Number) o1, (Number) o2);
		}
		if (o1 instanceof Date && o2 instanceof Date) {
			return DateUtils.compare((Date) o1, (Date) o2, true) == 0;
		}

		return o1.equals(o2);
	}


	/**
	 * Determines if the specified filter value is matched by the given actual value.
	 * <p>
	 * If the filter value is {@code null}, then no filtering will be applied and this method will return {@code true}.
	 *
	 * @param filterValue the filter value, or {@code null} if no filter shall be applied
	 * @param actualValue the value to compare against the filter value
	 * @return {@code true} if the filter value is {@code null} or equal to the actual value, or {@code false} otherwise
	 */
	public static <T> boolean isFilterMatched(T filterValue, T actualValue) {
		return filterValue == null || CompareUtils.isEqual(filterValue, actualValue);
	}


	/**
	 * Returns {@code true} if the both lists are non-{@code null} and their contents are equal, disregarding order, or if both lists are {@code null}.
	 * Duplicates are accounted for by requiring a one-to-one equality mapping for each element between the two lists.
	 */
	public static <T> boolean isEqual(List<T> c1, List<T> c2) {
		// Guard-clause: Null-check
		if (c1 == null || c2 == null) {
			return c1 == null && c2 == null;
		}

		// Guard-clause: Size check
		if (CollectionUtils.getSize(c1) != CollectionUtils.getSize(c2)) {
			return false;
		}

		List<T> c2Copy = new ArrayList<>(CollectionUtils.asNonNullList(c2));
		for (T element : c1) {
			if (!c2Copy.remove(element)) {
				return false;
			}
		}

		return CollectionUtils.isEmpty(c2Copy);
	}


	/**
	 * Compares 2 objects and returns a negative number if the first object is less than the second,
	 * 0 if the two objects are equal and a positive number if the first object is greater than the second.
	 */
	public static int compare(Object o1, Object o2) {
		if (isEqual(o1, o2)) {
			return 0;
		}
		if (o1 == null) {
			return -1;
		}
		if (o2 == null) {
			return 1;
		}
		if (o1 instanceof String && o2 instanceof String) {
			return StringUtils.compare((String) o1, (String) o2);
		}
		if (o1 instanceof Number && o2 instanceof Number) {
			return MathUtils.compare((Number) o1, (Number) o2);
		}
		if (o1 instanceof Date && o2 instanceof Date) {
			Date d1 = (Date) o1;
			if (d1 instanceof Timestamp) {
				d1 = new Date(d1.getTime());
			}
			Date d2 = (Date) o2;
			if (d2 instanceof Timestamp) {
				d2 = new Date(d2.getTime());
			}
			return DateUtils.compare(d1, d2, true);
		}
		if (o1 instanceof Boolean && o2 instanceof Boolean) {
			// Consider False to be Less Than True
			if (!(Boolean) o1) {
				return -1;
			}
			return 1;
		}
		if (o1 instanceof List && o2 instanceof List) {
			return isEqual((List) o1, (List) o2) ? 0 : -1;
		}
		// do string comparison
		return StringUtils.compare(o1.toString(), o2.toString());
	}
}
