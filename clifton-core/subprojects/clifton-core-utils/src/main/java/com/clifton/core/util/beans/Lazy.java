package com.clifton.core.util.beans;

import java.util.function.Supplier;


/**
 * A thread-safe lazy supplier. This defers calculation of the held value until an attempt is made to retrieve the value.
 * <p>
 * A supplier can be wrapped in this type if it's value may optionally be needed later. This is useful when complex logic is required to determine whether a value is needed, such
 * as an {@code if-else-if}-statement with several branches, some of which use the value and some of which do not. It is also useful when complex logic surrounds a value which may
 * be used several times, such as disparate {@code if}-statements whose branches use the value.
 * <p>
 * This class is modeled after similar deferment patterns in other libraries, such as Kotlin's <a href="https://kotlinlang.org/docs/reference/delegated-properties.html#lazy">lazy
 * delegate</a> and <a href="http://www.speedment.org/">Speedment's</a>
 * <a href="https://github.com/speedment/speedment/blob/131b8a0ddeead746e95e2b2c3fec2b77dc84b427/common-parent/lazy/src/main/java/com/speedment/common/lazy/LazyReference.java">Lazy
 * </a> class. See <a href="https://dzone.com/articles/be-lazy-with-java-8">"Be Lazy With Java 8"</a> for more information.
 * <p>
 * Example:
 * <blockquote><pre>
 * public void myFunction() {
 *     Lazy&lt;List&lt;MyEntities&gt;&gt; lazyEntityList = new Lazy&lt;&gt;(this::expensiveRetrievalMethod);
 *     if (needsFirstAction) {
 *         performFirstAction(lazyEntityList.get());
 *     }
 *     if (needsSecondAction) {
 *         performSecondAction(lazyEntityList.get());
 *     }
 * }
 * </pre></blockquote>
 *
 * @author MikeH
 */
public class Lazy<T> implements Supplier<T> {

	private final Supplier<T> supplier;

	// Use volatile fields to prevent cached registers providing stale values during concurrent calls
	private volatile boolean computed;
	private volatile T value;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Lazy(Supplier<T> supplier) {
		this.supplier = supplier;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the value held by this object. The value will be initialized if this is its first retrieval.
	 *
	 * @return the supplied value
	 */
	@Override
	public T get() {
		return this.computed ? this.value : compute();
	}


	/**
	 * Returns {@code true} if the value has already been computed, or {@code false} otherwise.
	 */
	public boolean isComputed() {
		return this.computed;
	}


	/**
	 * Gets the current value held by this object without performing the initialization step.
	 *
	 * @return the current value held by this object, or {@code null} if the value has not yet been computed
	 */
	public T currentValue() {
		return this.value;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private synchronized T compute() {
		if (!this.computed) {
			this.value = this.supplier.get();
			this.computed = true;
		}
		return this.value;
	}
}
