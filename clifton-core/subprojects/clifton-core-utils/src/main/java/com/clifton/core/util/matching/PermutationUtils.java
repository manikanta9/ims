package com.clifton.core.util.matching;


public class PermutationUtils {

	/**
	 * Returns the next array of indices that represent the permutations of a list.
	 * <p/>
	 * For example, with inputs:
	 * maxCombinationSize = 3
	 * minCombinationSize = 1
	 * <p/>
	 * Results:
	 * INPUT	BITS	INT		RESULT
	 * null		0001	1		0
	 * 0		0010	2		1
	 * 1		0011	3		0,1
	 * 0,1		0100	4		2
	 * 2		0101	5		0,2
	 * 0,2		0110	6		1,2
	 * 1,2		0111	7		0,1,2
	 * 0,1,2	1000	8		3
	 * 3		1001	9		0,3
	 * 0,3		1010	10		1,3
	 * 1,3		1011	11		0,1,3
	 * 0,1,3	1100	12		2,3
	 * 2,3		1101	13		0,2,3
	 * 0,2,3	1110	14		1,2,3
	 * <p/>
	 * NOTE: This will always return a result because it does not consider the size of list that indices in
	 * indexList represent.
	 *
	 * @param indexList
	 * @param minCombinationSize
	 * @param maxCombinationSize
	 */
	public static int[] getNextIndexList(int[] indexList, int minCombinationSize, int maxCombinationSize) {
		int[] result = null;
		if (indexList == null || indexList.length == 0) {
			result = new int[minCombinationSize];
			for (int i = 0; i < minCombinationSize; i++) {
				result[i] = i;
			}
		}
		else if (indexList.length == 1) {
			if (indexList[0] == 0 || maxCombinationSize == 1) {
				result = indexList;
				result[0] += 1;
			}
			else if (indexList.length + 1 <= maxCombinationSize) {
				result = new int[indexList.length + 1];
				result[0] = 0;
				result[result.length - 1] = indexList[0];
			}
		}
		else {
			int indexToIncrement = getNextIndexToIncrement(indexList);
			if (indexToIncrement == -1) {
				if ((indexList[0] != 0) && (indexList.length < maxCombinationSize)) {
					result = new int[indexList.length + 1];
					System.arraycopy(indexList, 0, result, 1, indexList.length);
					result[0] = 0;
				}
				else {
					result = new int[minCombinationSize];
					for (int i = 0; i < minCombinationSize - 1; i++) {
						result[i] = 0;
					}
					result[minCombinationSize - 1] = indexList[indexList.length - 1] + 1;
				}
			}
			else if (indexToIncrement > 0) {
				result = new int[indexList.length - 1];
				System.arraycopy(indexList, 1, result, 0, indexList.length - 1);
				result[0] += 1;
			}
			else if (indexList[indexToIncrement] > 0) {
				result = new int[indexList.length + 1];
				System.arraycopy(indexList, 0, result, 1, indexList.length);
				result[0] = 0;
			}
			else {
				result = indexList;
				result[indexToIncrement] += 1;
			}
		}
		return result;
	}


	private static int getNextIndexToIncrement(int[] indexList) {
		for (int i = 0; i < indexList.length - 1; i++) {
			if (indexList[i + 1] - indexList[i] > 1) {
				return i;
			}
		}
		return -1;
	}
}
