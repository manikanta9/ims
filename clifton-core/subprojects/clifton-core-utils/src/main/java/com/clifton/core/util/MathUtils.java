package com.clifton.core.util;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.math.RoundingMode;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;


/*
 * The <code>CoreMathUtils</code> class contains methods used for working with Numbers
 *
 * @author AbhinayaM
 */
public class MathUtils {

	// SHORTS
	// Instead of having to use new Short("1") throughout code, these three are most commonly used for Shorts
	public static final Short SHORT_ONE = 1;
	public static final Short SHORT_TWO = 2;
	public static final Short SHORT_THREE = 3;

	// Percent fields in Database are stored as 9,5 data type - this max value can be used to validate large percentages
	// when calculating the percent value before saving and getting the arithmetic overflow exception.
	public static final BigDecimal MAX_PERCENT_VALUE = BigDecimal.valueOf(9999.99999);

	//Default scale for calculations
	public static final int DEFAULT_SCALE = 20;

	public static final BigDecimal BIG_DECIMAL_ONE_HUNDRED = BigDecimal.valueOf(100);
	public static final BigDecimal BIG_DECIMAL_PENNY = new BigDecimal("0.01");

	public static final String NUMBER_FORMAT_MONEY = "#,##0.00";
	public static final String NUMBER_FORMAT_MONEY4 = "#,##0.00##";
	public static final String NUMBER_FORMAT_DECIMAL = "#,###.##########";
	public static final String NUMBER_FORMAT_DECIMAL_MAX = "#,###.################"; // Current Max Is 16 Decimal Places
	public static final String NUMBER_FORMAT_INTEGER = "#,###";
	public static final String NUMBER_FORMAT_PERCENT = "0.0000%";


	private MathUtils() {
		// Disable instantiation
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static Long getNumberAsLong(Number number) {
		if (number == null) {
			return null;
		}
		return number.longValue();
	}


	public static Integer getNumberAsInteger(Number number) {
		if (number == null) {
			return null;
		}
		return number.intValue();
	}


	public static Short getNumberAsShort(Number number) {
		if (number == null) {
			return null;
		}
		return number.shortValue();
	}


	public static BigDecimal getNumberAsBigDecimal(Number number) {
		if (number == null) {
			return null;
		}
		return new BigDecimal(number.toString());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BigDecimal getAsBigDecimal(String number) {
		if (number == null) {
			return null;
		}
		return new BigDecimal(number);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Calculates x^n
	 */
	public static BigDecimal pow(BigDecimal x, BigDecimal n) {
		int signOf2 = n.signum();

		// Perform X^(A+B)=X^A*X^B (B = remainder)
		double dx = x.doubleValue();
		// Compare the same row of digits according to context
		if (!isEqual(x, dx)) {
			throw new RuntimeException("Cannot convert [" + x + "] to double."); // Cannot convert x to double
		}
		n = n.multiply(new BigDecimal(signOf2)); // n is now positive
		BigDecimal remainderOf2 = n.remainder(BigDecimal.ONE);
		BigDecimal nIntPart = n.subtract(remainderOf2);
		// Calculate big part of the power using context -
		// bigger range and performance but lower accuracy
		BigDecimal intPow = x.pow(nIntPart.intValueExact());
		BigDecimal doublePow = BigDecimal.valueOf(Math.pow(dx, remainderOf2.doubleValue()));
		BigDecimal result = intPow.multiply(doublePow);

		// Fix negative power
		if (signOf2 == -1) {
			result = BigDecimal.ONE.divide(result, 20, RoundingMode.HALF_UP);
		}

		return result;
	}


	/**
	 * Calculates the factorial of a BigInteger.
	 */
	public static BigInteger factorial(BigInteger n) {
		if (n.compareTo(BigInteger.ZERO) == 0) {
			return BigInteger.ONE;
		}
		return n.multiply(factorial(n.subtract(BigInteger.ONE)));
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Number Comparison Methods                       ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method compares 2 number's double values and returns
	 * -1 if n1 is less than n2, 0 if they are equal, and 1 if
	 * n1 is greater than n2
	 * <p>
	 * <code>null</code> numbers are considered less than all other numbers.
	 */
	public static int compare(Number n1, Number n2) {
		if (n1 == null) {
			if (n2 == null) {
				return 0;
			}
			return -1;
		}
		if (n2 == null) {
			return 1;
		}
		double l1 = n1.doubleValue();
		double l2 = n2.doubleValue();

		return Double.compare(l1, l2);
	}


	/**
	 * Returns true if the 2 number's double values are equal
	 */
	public static boolean isEqual(Number n1, Number n2) {
		return (0 == compare(n1, n2));
	}


	/**
	 * Returns true if the 2 number's double values are NOT equal
	 */
	public static boolean isNotEqual(Number n1, Number n2) {
		return !isEqual(n1, n2);
	}


	/**
	 * Returns true if the first number is less than the second
	 */
	public static boolean isLessThan(Number n1, Number n2) {
		return (compare(n1, n2) < 0);
	}


	/**
	 * Returns true if the first number is greater than the second
	 */
	public static boolean isGreaterThan(Number n1, Number n2) {
		return (compare(n1, n2) > 0);
	}


	/**
	 * Returns true if the first number is greater than or equal to the second
	 */
	public static boolean isGreaterThanOrEqual(Number n1, Number n2) {
		return (compare(n1, n2) >= 0);
	}


	/**
	 * Returns true if the first number is less than or equal to the second
	 */
	public static boolean isLessThanOrEqual(Number n1, Number n2) {
		return (compare(n1, n2) <= 0);
	}


	/**
	 * Returns true if the specified number is between the given start & end numbers.
	 * <p>
	 * Note: If start is null then the number is considered as coming after the start.
	 * If end is null then the number is considered as coming before the end.
	 */
	public static boolean isBetween(Number n, Number start, Number end) {
		if (n == null) {
			return false;
		}
		if (start == null || (compare(start, n) <= 0)) {
			if (end == null || (compare(n, end) <= 0)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if the specified number is between the given start & end numbers, but
	 * n==end is not considered between.
	 * <p>
	 * Note: If start is null then the number is considered as coming after the start.
	 * If end is null then the number is considered as coming before the end.
	 */
	public static boolean isBetweenAndNotEqualEnd(Number n, Number start, Number end) {
		if (n == null) {
			return false;
		}
		if (start == null || (compare(start, n) <= 0)) {
			if (end == null || (compare(n, end) < 0)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if the specified string is a number (can be parsed to BigDecimal)
	 * and false otherwise.
	 */
	@SuppressWarnings("unused")
	public static boolean isNumber(String number) {
		try {
			new BigDecimal(number);
		}
		catch (Throwable e) {
			return false;
		}
		return true;
	}


	public static boolean isNullOrZero(Number value) {
		if (value == null) {
			return true;
		}
		return isEqual(value, 0);
	}


	/**
	 * Similar to is null or zero, but only takes a BigDecimal and first rounds the value to the given scale
	 */
	public static boolean isNullOrZeroWithScale(BigDecimal value, int scale) {
		if (value == null) {
			return true;
		}
		value = round(value, scale);
		return isEqual(value, 0);
	}


	public static boolean isZeroOrOne(Number value) {
		if (value != null) {
			if (isEqual(value, 0) || isEqual(value, 1)) {
				return true;
			}
		}
		return false;
	}


	public static boolean isPositive(Number value) {
		return compare(value, 0) > 0;
	}


	public static boolean isNegative(Number value) {
		return compare(value, 0) < 0;
	}


	/**
	 * Returns true if both values have the same BigDecimal.signum() or if they are both null.
	 */
	public static boolean isSameSignum(BigDecimal value1, BigDecimal value2) {
		if (value1 == null) {
			return (value2 == null);
		}
		if (value2 == null) {
			return false;
		}
		return (value1.signum() == value2.signum());
	}

	////////////////////////////////////////////////////////////////////////////
	//////                    Summation Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	public static <T> void addValueToProperty(T bean, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal value) {
		if (bean != null && !isNullOrZero(value)) {
			BigDecimal curVal = valueGetter.apply(bean);
			valueSetter.accept(bean, add(curVal, value));
		}
	}


	/**
	 * Addition:  If d1 is null returns d2. If d2 is null, returns d1, else adds them and returns result
	 */
	public static BigDecimal add(BigDecimal d1, BigDecimal d2) {
		if (d1 == null) {
			return d2;
		}
		if (d2 == null) {
			return d1;
		}
		return d1.add(d2);
	}


	public static BigDecimal add(BigDecimal d1, double d2) {
		return add(d1, BigDecimal.valueOf(d2));
	}


	/**
	 * Subtraction:  If d2 is null, return d1.  If d1 is null, returns -d2. Else returns d1 - d2
	 * Basically treats a null as 0 unless both arguments are null.
	 */
	public static BigDecimal subtract(BigDecimal d1, BigDecimal d2) {
		if (d2 == null) {
			return d1;
		}
		if (d1 == null) {
			return d2.negate();
		}
		return d1.subtract(d2);
	}


	public static BigDecimal subtract(BigDecimal d1, double d2) {
		return subtract(d1, BigDecimal.valueOf(d2));
	}


	/**
	 * Negate:  If d1 is null returns null, else returns (-) d1
	 */
	public static BigDecimal negate(BigDecimal d1) {
		if (d1 == null) {
			return null;
		}
		return d1.negate();
	}


	/**
	 * Absolute Value:  If d1 is null returns null, else returns absolute value of d1
	 */
	public static BigDecimal abs(BigDecimal d1) {
		if (d1 == null) {
			return null;
		}
		return d1.abs();
	}


	/**
	 * Returns [signum(d1) * (d1.abs() - d2)]. That is, reduces the absolute value of d1 by d2, while maintaining the sign
	 * of d1. If d1.abs() < d2.abs(), then 0 is returned. d2 must be non-negative.
	 */
	public static BigDecimal reduceAbsValue(BigDecimal d1, BigDecimal d2) {
		if (isNegative(d2)) {
			throw new RuntimeException("d2 must be a non-negative number.");
		}

		return multiply(new BigDecimal(d1.signum()), max(BigDecimal.ZERO, subtract(d1.abs(), d2)));
	}


	/**
	 * Max:  If d1 is null returns d2. If d2 is null, returns d1, else returns the larger value
	 */
	public static BigDecimal max(BigDecimal d1, BigDecimal d2) {
		if (d1 == null) {
			return d2;
		}
		if (d2 == null) {
			return d1;
		}
		return d1.max(d2);
	}


	/**
	 * Retrieves the maximum value out of the given array of numbers. If no numbers exist or all numbers are <code>null</code>, then <code>null</code> will be returned.
	 */
	@SafeVarargs
	public static <T extends Number> T max(T... numbers) {
		T currentMax = null;
		for (T number : numbers) {
			if (number != null
					&& (currentMax == null || MathUtils.compare(number, currentMax) > 0)) {
				currentMax = number;
			}
		}
		return currentMax;
	}


	/**
	 * Min:  If d1 is null returns d2. If d2 is null, returns d1, else returns the smaller value
	 */
	public static BigDecimal min(BigDecimal d1, BigDecimal d2) {
		if (d1 == null) {
			return d2;
		}
		if (d2 == null) {
			return d1;
		}
		return d1.min(d2);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                    Multiply Methods                           ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Multiply:  If d1 or d2 is null returns null, else returns the result of d1 * d2
	 * <p>
	 * NOTE: Uses high precision (20) for multiplication so that values aren't lost, but big enough for when used (some numbers can get extremely large when multiplying decimal values).  Values can be rounded later if necessary
	 */
	public static BigDecimal multiply(BigDecimal d1, BigDecimal d2) {
		return multiply(d1, d2, 20);
	}


	public static BigDecimal multiply(BigDecimal d1, BigDecimal d2, int scale) {
		if (d1 == null || d2 == null) {
			return null;
		}
		BigDecimal result = d1.multiply(d2);
		if (result == null) {
			return null;
		}
		return result.setScale(scale, RoundingMode.HALF_UP);
	}


	public static BigDecimal multiply(BigDecimal d1, double d2) {
		return multiply(d1, BigDecimal.valueOf(d2));
	}
////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


	/**
	 * Square root of value
	 */
	public static BigDecimal sqrt(BigDecimal value) {
		if (value == null) {
			return null;
		}

		return sqrt(value, MathUtils.DEFAULT_SCALE);
	}


	/**
	 * Babylonian method for calculating the square root of a number to a specified accuracy (Scale).
	 * <p>
	 * If x is an overestimate to the square root of a non-negative real number S then s/x,
	 * will be an underestimate and so the average of these two numbers may reasonably be expected
	 * to provide a better approximation
	 * <p>
	 * 1) Begin with an arbitrary positive starting value x_0 (We will use one as starting value).
	 * 2) Let x_n+1 be the average of x_n and S / x_n.
	 * 3) Repeat step 2 until the desired accuracy is achieved.
	 */
	public static BigDecimal sqrt(BigDecimal value, int scale) {
		if (value == null) {
			return null;
		}
		if (MathUtils.isEqual(value, BigDecimal.ZERO)) {
			return BigDecimal.ZERO;
		}

		//Select our approximation of the square root (One)
		BigDecimal sqrt = new BigDecimal(1);
		sqrt = sqrt.setScale(scale + 3, RoundingMode.FLOOR);
		BigDecimal store = new BigDecimal(value.toString());

		boolean first = true;

		do {
			if (!first) {
				store = new BigDecimal(sqrt.toString());
			}
			else {
				first = false;
			}
			store = store.setScale(scale + 3, RoundingMode.FLOOR);
			sqrt = value.divide(store, scale + 3, RoundingMode.FLOOR).add(store).divide(BigDecimal.valueOf(2), scale + 3, RoundingMode.FLOOR);
		}
		while (!store.equals(sqrt));
		return sqrt.setScale(scale, RoundingMode.FLOOR);
	}


	/**
	 * Calculates value * value  (value squared)
	 */


	public static BigDecimal squareValue(BigDecimal value) {
		return MathUtils.multiply(value, value);
	}


	/**
	 * The Sharpe ratio is a way to examine the performance of an investment by adjusting for its risk.
	 * The ratio measures the excess return (or risk premium) per unit of deviation in an investment asset
	 * or a trading strategy.
	 * <p>
	 * (returnValue - riskFreeReturn) / (standard deviation of return)
	 */
	public static BigDecimal sharpeRatio(BigDecimal returnValue, BigDecimal riskFreeReturn, BigDecimal standardDeviation) {
		if (riskFreeReturn == null) {
			riskFreeReturn = BigDecimal.ZERO;
		}

		return MathUtils.divide(MathUtils.subtract(returnValue, riskFreeReturn), standardDeviation);
	}


	/**
	 * Model return is calculated by geometrically removing the monthly fee rate from the gross return.
	 * Note that both modelFeePercent and grossReturn should be passed in as percentages and result is returned as the percentage value (i.e. 5% = 5, not 0.05)
	 * <p>
	 * Example: [(1+Gross Return) / (1+ Annual Fee Ratio)^(1/12)]-1
	 */
	public static BigDecimal modelFeeReturn(BigDecimal modelFeePercent, BigDecimal grossReturn) {
		//modelFee =  (1+annual fee percent)^(1/12);
		BigDecimal modelFee = MathUtils.pow(MathUtils.add(BigDecimal.ONE, MathUtils.divide(modelFeePercent, BIG_DECIMAL_ONE_HUNDRED)), MathUtils.divide(BigDecimal.ONE, new BigDecimal(12)));
		BigDecimal onePlusGross = MathUtils.add(BigDecimal.ONE, MathUtils.divide(grossReturn, BIG_DECIMAL_ONE_HUNDRED));

		//(onePlusGross/modelFee)-1
		return MathUtils.multiply(MathUtils.subtract(MathUtils.divide(onePlusGross, modelFee), BigDecimal.ONE), BIG_DECIMAL_ONE_HUNDRED);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                      Divide Methods                           ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Divide:  If d1 or d2 is null returns null. If d2 has the equivalent of a zero value throws an IllegalArgumentException
	 * Else returns d1 / d2
	 * NOTE: Uses high precision (20) for division so that values aren't lost.  Values can be rounded later if necessary
	 */
	public static BigDecimal divide(BigDecimal d1, BigDecimal d2) {
		return divide(d1, d2, 20);
	}


	/**
	 * Divide:  If d1 or d2 is null returns null. If d2 has the equivalent of a zero value throws an IllegalArgumentException
	 * Else returns d1 / d2.
	 * Uses HALF_UP rounding mode to the specified number of decimal places
	 */
	public static BigDecimal divide(BigDecimal d1, BigDecimal d2, int scale) {
		if (d1 == null || d2 == null) {
			return null;
		}
		if (isEqual(d2, 0)) {
			throw new IllegalArgumentException("Divide By Zero Encountered");
		}
		// Use a high scale for division, can round it later
		return d1.divide(d2, scale, RoundingMode.HALF_UP);
	}


	public static BigDecimal divide(BigDecimal d1, double d2) {
		return divide(d1, BigDecimal.valueOf(d2));
	}


	public static BigDecimal divide(long l1, long l2) {
		return divide(BigDecimal.valueOf(l1), BigDecimal.valueOf(l2));
	}


	/**
	 * Divide:  If d1 is null return null, if d2 is null or zero return divideByZeroDefaultValue
	 * Else returns d1 / d2
	 * NOTE: Uses high precision (20) for division so that values aren't lost.  Values can be rounded later if necessary
	 */
	public static BigDecimal divide(BigDecimal d1, BigDecimal d2, BigDecimal divideByZeroDefaultValue) {
		if (isNullOrZero(d2)) {
			return divideByZeroDefaultValue;
		}
		return divide(d1, d2);
	}


	/**
	 * Determines the remainder when dividing <tt>d1</tt> by <tt>d2</tt>. This is equivalent to {@code d1 % d2} when the values are primitive numeric types.
	 *
	 * @param d1 the dividend
	 * @param d2 the divisor
	 * @return the remainder when <tt>d1</tt> is divided by <tt>d2</tt>
	 */
	public static BigDecimal modulo(BigDecimal d1, BigDecimal d2) {
		return d1.remainder(d2);
	}


	/**
	 * Returns a {@code BigDecimal} whose value is the integer part of the quotient {@code (dividend / divisor)} rounded down.  The
	 * preferred scale of the result is {@code (dividend.scale() - divisor.scale())}.
	 *
	 * @param d1 the dividend
	 * @param d2 the divisor
	 * @return The integer part of {@code dividend / divisor}.
	 */
	public static BigDecimal divideToIntegralValue(BigDecimal d1, BigDecimal d2) {
		if (d1 == null || d2 == null) {
			return null;
		}
		if (isEqual(d2, 0)) {
			throw new IllegalArgumentException("Divide By Zero Encountered");
		}
		return d1.divideToIntegralValue(d2);
	}


	/**
	 * Wraps the {@code BigDecimal.divideAndRemainder} functionality.
	 * If d1 or d2 is null returns null. If d2 has the equivalent of a zero value throws an IllegalArgumentException
	 *
	 * @param d1 the dividend
	 * @param d2 the divisor
	 * @return a two element {@code BigDecimal} array: the quotient
	 * (the result of {@code divideToIntegralValue}) is the initial element
	 * and the remainder is the final element.
	 */
	public static BigDecimal[] divideAndRemainder(BigDecimal d1, BigDecimal d2) {
		if (d1 == null || d2 == null) {
			return null;
		}
		if (isEqual(d2, 0)) {
			throw new IllegalArgumentException("Divide By Zero Encountered");
		}

		return d1.divideAndRemainder(d2);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                   Percentage Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Percentage Of:  Returns the value of x% of total. Where percentage is a fractional value, i.e. 0.17 represents 17%
	 */
	public static BigDecimal getPercentageOf(BigDecimal percentage, BigDecimal total) {
		return getPercentageOf(percentage, total, false);
	}


	/**
	 * Percentage Of:  Returns the value of x% of total. Where percentage is a fractional value, i.e. 0.17 represents 17%
	 * If percentageAsHundredsValue is true, considers percentage to be a whole value, i.e. 17 for 17%
	 * Otherwise considers percentage to be a fractional value, i.e. 0.17 for 17%
	 */
	public static BigDecimal getPercentageOf(BigDecimal percentage, BigDecimal total, boolean percentageAsHundredsValue) {
		if (percentage == null || total == null) {
			return null;
		}
		if (MathUtils.isEqual(0, percentage)) {
			return BigDecimal.ZERO;
		}
		if (percentageAsHundredsValue) {
			percentage = divide(percentage, BIG_DECIMAL_ONE_HUNDRED);
		}
		return MathUtils.multiply(percentage, total);
	}


	/**
	 * Percentage Change:  Returns the percentage change from the from value to the to value as a fraction
	 */
	public static BigDecimal getPercentChange(BigDecimal from, BigDecimal to) {
		return getPercentChange(from, to, false);
	}


	/**
	 * Percentage Change:  Returns the percentage change from the from value to the to value
	 * If returnAsHundreds is true, will return the fraction result * 100.
	 * For example 0.17 fraction value would be returned as 17 if returnAsHundredsValue is true
	 */
	public static BigDecimal getPercentChange(BigDecimal from, BigDecimal to, boolean returnAsHundredsValue) {
		if (from == null || to == null) {
			return null;
		}
		if (MathUtils.isEqual(from, to)) {
			return BigDecimal.ZERO;
		}
		if (MathUtils.isEqual(0, from)) {
			return BIG_DECIMAL_ONE_HUNDRED;
		}
		BigDecimal percent = MathUtils.multiply(to, BIG_DECIMAL_ONE_HUNDRED);
		percent = divide(percent, from);
		percent = subtract(percent, BIG_DECIMAL_ONE_HUNDRED);
		if (returnAsHundredsValue) {
			return percent;
		}
		return divide(percent, BIG_DECIMAL_ONE_HUNDRED);
	}


	/**
	 * Returns a BigDecimal Rate de-annualized, i.e. daily %
	 * <p>
	 * ((1+(Rate/100))^(1/360)-1)
	 */
	public static BigDecimal getDeAnnualizedRate(BigDecimal rate) {
		if (MathUtils.isNullOrZero(rate)) {
			return BigDecimal.ZERO;
		}
		BigDecimal result = divide(rate, BIG_DECIMAL_ONE_HUNDRED);
		result = MathUtils.add(result, BigDecimal.ONE);
		BigDecimal power = MathUtils.divide(BigDecimal.ONE, BigDecimal.valueOf(360));
		result = BigDecimal.valueOf(Math.pow(result.doubleValue(), power.doubleValue()));
		result = subtract(result, BigDecimal.ONE);
		return result;
	}


	/**
	 * Works similar to Excel Function FVSCHEDULE(1, values) - 1
	 * Actual Calculation is ((1 + Value1) * (1 + Value2) * ... (1 + ValueN)) - 1
	 * NOTE: IF a value is NULL, BigDecimal.ZERO will be used in it's place
	 */
	public static BigDecimal getTotalPercentChange(Object[] percentages, boolean percentageAsHundredsValue) {
		if (percentages == null || percentages.length == 0) {
			return BigDecimal.ZERO;
		}
		BigDecimal value = BigDecimal.ONE;
		for (Object obj : percentages) {
			if (obj == null) {
				obj = BigDecimal.ZERO;
			}
			if (!(obj instanceof BigDecimal)) {
				throw new IllegalArgumentException("percentages should contain BigDecimals only");
			}
			BigDecimal v = (BigDecimal) obj;
			if (percentageAsHundredsValue) {
				v = divide(v, BIG_DECIMAL_ONE_HUNDRED);
			}
			BigDecimal thisV = MathUtils.add(BigDecimal.ONE, v);
			value = MathUtils.multiply(value, thisV);
		}

		value = MathUtils.subtract(value, BigDecimal.ONE);
		if (percentageAsHundredsValue) {
			value = MathUtils.multiply(value, BIG_DECIMAL_ONE_HUNDRED);
		}

		return value;
	}


	/**
	 * Takes a value, applies the haircut and returns the adjusted value.
	 * <p>
	 * Formula: If: haircut is negative then: Value / (1-Haircut)
	 * Else: Value * (1-Haircut)
	 * <p>
	 * Result is rounded to 2 decimals using ROUND_HALF_UP
	 *
	 * @param value                  - value to cut
	 * @param haircut                - percentage to apply haircut
	 * @param haircutAsHundredsValue - true if haircut percent is 30 instead of .3, etc.
	 */
	public static BigDecimal getValueAfterHaircut(BigDecimal value, BigDecimal haircut, boolean haircutAsHundredsValue) {
		// No value or no haircut - return the value
		if (isNullOrZero(value) || isNullOrZero(haircut)) {
			return value;
		}
		if (haircutAsHundredsValue) {
			haircut = divide(haircut, BIG_DECIMAL_ONE_HUNDRED);
		}
		BigDecimal adjustedValue;
		if (isLessThan(haircut, BigDecimal.ZERO)) {
			adjustedValue = divide(value, BigDecimal.ONE.subtract(haircut));
		}
		else {
			adjustedValue = multiply(value, BigDecimal.ONE.subtract(haircut));
		}
		return adjustedValue.setScale(2, BigDecimal.ROUND_HALF_UP);
	}


	/**
	 * Returns a BigDecimal whose value is rounded using the given scale
	 * Scale = number of places after the decimal point
	 * Rounding uses Half Up Rounding
	 */
	public static BigDecimal round(BigDecimal value, int scale) {
		return round(value, scale, BigDecimal.ROUND_HALF_UP);
	}


	public static BigDecimal round(BigDecimal value, int scale, int roundingMode) {
		if (value == null) {
			return null;
		}
		return value.setScale(scale, roundingMode);
	}


	public static BigDecimal round(BigDecimal value, int scale, RoundingMode roundingMode) {
		if (value == null) {
			return null;
		}
		return value.setScale(scale, roundingMode);
	}


	/**
	 * Rounds the specified value to the smallest decimal precision possible such that testCondition returns true.
	 */
	public static BigDecimal roundToSmallestPrecision(BigDecimal value, Predicate<BigDecimal> testCondition) {
		if (value != null && value.precision() > 0) {
			int precision = 0;
			while (precision < value.precision()) {
				BigDecimal roundedNumber = round(value, precision);
				if (testCondition.test(roundedNumber)) {
					return roundedNumber;
				}
				precision++;
			}
		}
		return value;
	}


	/**
	 * Returns true if the specified number has higher precision than the specified scale.
	 */
	public static boolean isScaleViolated(BigDecimal value, int scale) {
		if (value == null) {
			return false;
		}
		return !value.setScale(scale, RoundingMode.DOWN).equals(value.setScale(scale, RoundingMode.UP));
	}


	/**
	 * Returns true if the value is >= min and <= max
	 * Otherwise returns false
	 * <p>
	 * Returns true if both min/max are null, otherwise evaluates against the non null range
	 * If value is null returns true
	 */
	public static boolean isInRange(BigDecimal value, BigDecimal min, BigDecimal max) {
		if (value == null) {
			return true;
		}
		if (min == null) {
			if (max == null) {
				return true;
			}
			return (isLessThan(value, max) || isEqual(value, max));
		}
		if (max == null) {
			return (isGreaterThan(value, min) || isEqual(value, min));
		}
		return (isLessThan(value, max) || isEqual(value, max)) && (isGreaterThan(value, min) || isEqual(value, min));
	}


	/**
	 * Truncated decimals past the given precision.  i.e. for 1256.7894561 @ 10 precision places the function
	 * returns 1256.789456 and 45678.4561237 and 10 precision places the function returns 45678.45612.
	 */
	public static BigDecimal truncateDecimals(BigDecimal value, int precision) {
		value = value.stripTrailingZeros();
		int p = value.precision();
		int scale = value.scale();
		if (p <= precision) {
			return value;
		}
		int endScale = precision - (p - scale);
		BigDecimal divisor = BigDecimal.valueOf(Math.pow(10, precision + 1));
		if (value.compareTo(divisor) > 0) {
			throw new RuntimeException("[" + value + "] is to large to truncate to a [" + precision + "] precision.");
		}
		double multiplier = Math.pow(10, precision + 1 - scale - (endScale < 0 ? Math.abs(endScale) : 0));
		return divide(new BigDecimal(value.unscaledValue()), BigDecimal.valueOf(Math.pow(10, precision + 1)))
				.setScale(precision, RoundingMode.FLOOR)
				.multiply(BigDecimal.valueOf(multiplier))
				.setScale(endScale < 0 ? 0 : precision - (p - scale), RoundingMode.FLOOR);
	}


	public static BigDecimal absoluteDiff(BigDecimal val1, BigDecimal val2) {
		return abs(subtract(val1, val2));
	}


	/**
	 * Returns the DV01 of the given value.
	 * DV01 is the Dollar Value of on basis point = (0.01%) or the value divided by 10,000
	 */
	public static BigDecimal getDV01Value(BigDecimal value) {
		return MathUtils.divide(value, BigDecimal.valueOf(10000));
	}


	/**
	 * A result classification for scenarios of applying a numeric adjustment to an object's property value.
	 */
	public enum PropertyValueAdjustmentResults {
		NO_ADJUSTMENT(true, false),
		VALUE_ADJUSTED_TO_MATCH(true, true),
		ADJUSTMENT_TOO_GREAT(false, false);

		final boolean valueMatch;
		final boolean valueAdjusted;


		PropertyValueAdjustmentResults(boolean valueMatch, boolean valueAdjusted) {
			this.valueMatch = valueMatch;
			this.valueAdjusted = valueAdjusted;
		}


		public boolean isValueMatch() {
			return this.valueMatch;
		}


		public boolean isValueAdjusted() {
			return this.valueAdjusted;
		}
	}
}
