package com.clifton.core.util;


import com.clifton.core.util.converter.string.ObjectToStringConverter;

import java.lang.reflect.Array;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import java.util.stream.Collectors;


public class MapUtils {

	public static final char KEY_SEPARATOR = '#';

	private static final ObjectToStringConverter converter = new ObjectToStringConverter();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MapUtils() {
		// Disable instantiation
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a map with the given key-value pair. This is similar to the JDK 9 <code>Map#of(K, V)</code> method.
	 */
	public static <K, V> Map<K, V> of(K key, V value) {
		return Collections.singletonMap(key, value);
	}


	/**
	 * Generates a map from the given {@link Map.Entry entries}.
	 * <p>
	 * If the given array of <code>entries</code> is <code>null</code> then an empty map will be returned. This does not accept individual entries which are <code>null</code>.
	 */
	@SafeVarargs
	public static <K, V> Map<K, V> ofEntries(Map.Entry<K, V>... entries) {
		// Guard-clause: Trivial case
		if (entries == null) {
			return Collections.emptyMap();
		}
		Map<K, V> map = new HashMap<>();
		for (Map.Entry<K, V> entry : entries) {
			map.put(entry.getKey(), entry.getValue());
		}
		return Collections.unmodifiableMap(map);
	}


	/**
	 * Generates an individual {@link Map.Entry}, particularly for use with {@link #ofEntries(Map.Entry[])}.
	 */
	public static <K, V> Map.Entry<K, V> entry(K key, V value) {
		return new AbstractMap.SimpleImmutableEntry<>(key, value);
	}


	/**
	 * Generates a new list from the given map, converting all map entries according to the provided conversion function. The resulting collection will follow the iteration order
	 * of the original map.
	 * <p>
	 * If the given map is {@code null} then an empty list will be returned.
	 * <p>
	 * There are no guarantees on the type, mutability, serializability, or thread-safety of the {@link List} returned.
	 *
	 * @param entryMap           the original map
	 * @param conversionFunction the function converting elements from the original map
	 * @param <T>                the key type of the original map
	 * @param <U>                the value type of the original map
	 * @param <R>                the type of the resulting collection
	 * @return a collection of the converted elements using the iteration order of the original map
	 */
	public static <T, U, R> List<R> getConverted(Map<T, U> entryMap, BiFunction<T, U, R> conversionFunction) {
		if (CollectionUtils.isEmpty(entryMap)) {
			return Collections.emptyList();
		}

		return CollectionUtils.getStream(entryMap.entrySet())
				.map(entry -> conversionFunction.apply(entry.getKey(), entry.getValue()))
				.collect(Collectors.toList());
	}


	/**
	 * Puts the specified value under the specified key in the specified map and return true.
	 * If any of the arguments are null, doesn't do anything and returns false.
	 */
	public static <K, V> boolean putIfNotNull(Map<K, V> map, K key, V value) {
		if (map == null || key == null || value == null) {
			return false;
		}
		map.put(key, value);
		return true;
	}


	/**
	 * Parses a string to a map.  Only supports Map<String, String> for now.
	 * <p>
	 * Format:
	 * name=value,name1=value1;name2=value2,name3=value3;name4=value4
	 */
	public static List<Map<String, String>> parseMap(String mapString) {
		if ((mapString == null) || mapString.isEmpty()) {
			return null;
		}
		List<Map<String, String>> result = new ArrayList<>();
		String[] mapStringList = mapString.split(";");
		for (String mapValueString : mapStringList) {
			Map<String, String> map = new HashMap<>();

			for (String keyValue : mapValueString.split(",")) {
				String[] pairs = keyValue.split("=", 2);
				map.put(pairs[0].trim(), pairs.length == 1 ? "" : pairs[1].trim());
			}
			result.add(map);
		}
		return result;
	}


	public static Long getParameterAsLong(Object key, Map<?, ?> config) {
		String result = getParameterAsString(key, config);
		if (!StringUtils.isEmpty(result)) {
			return new Long(result);
		}
		return null;
	}


	public static Integer getParameterAsInteger(Object key, Map<?, ?> config) {
		String result = getParameterAsString(key, config);
		if (!StringUtils.isEmpty(result)) {
			return new Integer(result);
		}
		return null;
	}


	public static Short getParameterAsShort(Object key, Map<?, ?> config, int index) {
		String result = getParameterAsString(key, config, index);
		if (!StringUtils.isEmpty(result)) {
			return new Short(result);
		}
		return null;
	}


	public static String getParameterAsString(Object key, Map<?, ?> config) {
		return getParameterAsString(key, config, 0);
	}


	public static String getParameterAsString(Object key, Map<?, ?> config, int index) {
		Object result = null;
		Object value = config.get(key);
		if (value != null) {
			if (value instanceof Collection) {
				value = ((Collection<?>) value).toArray();
			}
			if (value.getClass().isArray()) {
				if (Array.getLength(value) > index) {
					result = Array.get(value, index);
				}
			}
			else {
				result = value;
			}
		}
		return converter.convert(result);
	}


	/**
	 * Returns <code>true</code> if the map is one-to-one, meaning that no two values are equal (via {@link Object#equals(Object)}). This is useful for verifying applicability for
	 * bi-directional mappings and for asserting that the map consists of unique pairs.
	 */
	public static boolean isOneToOne(Map<?, ?> map) {
		if (map == null) {
			return true;
		}
		return CollectionUtils.getDistinct(map.values()).size() == map.size();
	}


	/**
	 * Returns true if the key is in the specified map.
	 */
	public static <K, V> boolean contains(K key, Map<K, V> map) {
		if (map == null) {
			return false;
		}
		return map.containsKey(key);
	}
}
