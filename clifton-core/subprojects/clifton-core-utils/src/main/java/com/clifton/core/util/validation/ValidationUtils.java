package com.clifton.core.util.validation;


import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.jetbrains.annotations.Contract;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import java.util.function.Supplier;


/**
 * The {@link ValidationUtils} class provides validation utility/helper methods.
 *
 * @author vgomelsky
 */
public class ValidationUtils {

	/**
	 * Throws a {@link ValidationException} with the specified message on assertion failure.
	 *
	 * @param messageSupplier the supplier for the exception message
	 * @throws ValidationException always
	 */
	public static void fail(Supplier<String> messageSupplier) {
		throw new ValidationException(messageSupplier.get());
	}


	/**
	 * Throws a {@link ValidationException} with the specified message on assertion failure.
	 * <p>
	 * If any message arguments are provided, the <tt>messageFormat</tt> string will be used as a {@link String#format(String, Object...) format string}.
	 * Otherwise, it will be treated as a literal message.
	 *
	 * @param messageFormat a {@link String#format(String, Object...) format string}, or the exact message string if no format string arguments are provided
	 * @param messageArgs   the format string arguments
	 * @throws ValidationException always
	 * @implNote If no message arguments are given, then <tt>messageFormat</tt> is used as the literal message rather than being interpreted with {@link
	 * String#format(String, Object...)}. This is because many pre-existing calls have a single argument for the message and have not yet been adapted to use a
	 * string parameter when the message may contain special characters. For variable message input, the standard adaptation would be the following:
	 * <pre>ValidationUtils.fail(msg); -> ValidationUtils.fail("%s", msg);</pre>
	 * @see #fail(Supplier)
	 */
	public static void fail(String messageFormat, Object... messageArgs) {
		fail(() -> ArrayUtils.isEmpty(messageArgs) ? messageFormat : String.format(messageFormat, messageArgs));
	}


	/**
	 * Throws a {@link FieldValidationException} with the specified message on assertion failure.
	 *
	 * @param fieldName       the name of the field to mark for validation failure
	 * @param messageSupplier the supplier for the exception message
	 * @throws FieldValidationException always
	 */
	public static void failWithField(String fieldName, Supplier<String> messageSupplier) {
		throw new FieldValidationException(messageSupplier.get(), fieldName);
	}


	/**
	 * Throws a {@link FieldValidationException} with the specified message on assertion failure.
	 *
	 * @param fieldName     the name of the field to mark for validation failure
	 * @param messageFormat a {@link String#format(String, Object...) format string}
	 * @param messageArgs   the format string arguments
	 * @throws FieldValidationException always
	 * @see #failWithField(String, Supplier)
	 */
	public static void failWithField(String fieldName, String messageFormat, Object... messageArgs) {
		failWithField(fieldName, () -> String.format(messageFormat, messageArgs));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("false, _ -> fail")
	public static void assertTrue(boolean condition, Supplier<String> messageSupplier) {
		if (!condition) {
			fail(messageSupplier);
		}
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("false, _ -> fail")
	public static void assertTrue(boolean condition, String message) {
		if (!condition) {
			fail(message);
		}
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	@Contract("false, _, _ -> fail")
	public static void assertTrue(boolean condition, Supplier<String> messageSupplier, String fieldName) {
		if (!condition) {
			failWithField(fieldName, messageSupplier);
		}
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("false, _, _ -> fail")
	public static void assertTrue(boolean condition, String message, String fieldName) {
		if (!condition) {
			failWithField(fieldName, message);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("true, _ -> fail")
	public static void assertFalse(boolean condition, Supplier<String> messageSupplier) {
		assertTrue(!condition, messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("true, _ -> fail")
	public static void assertFalse(boolean condition, String message) {
		assertTrue(!condition, message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	@Contract("true, _, _ -> fail")
	public static void assertFalse(boolean condition, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(!condition, messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("true, _, _ -> fail")
	public static void assertFalse(boolean condition, String message, String fieldName) {
		assertTrue(!condition, message, fieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("!null, _ -> fail")
	public static void assertNull(Object value, Supplier<String> messageSupplier) {
		assertTrue(value == null, messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("!null, _ -> fail")
	public static void assertNull(Object value, String message) {
		assertTrue(value == null, message);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("!null, _, _ -> fail")
	public static void assertNull(Object value, String message, String fieldName) {
		assertTrue(value == null, message, fieldName);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static <T> T assertNotNull(T value, Supplier<String> messageSupplier) {
		assertTrue(value != null, messageSupplier);
		return value;
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _ -> fail")
	public static <T> T assertNotNull(T value, String message) {
		assertTrue(value != null, message);
		return value;
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static <T> T assertNotNull(T value, String message, String fieldName) {
		assertTrue(value != null, message, fieldName);
		return value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	public static void assertEmpty(String str, Supplier<String> messageSupplier) {
		assertTrue(StringUtils.isEmpty(str), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	public static void assertEmpty(String str, String message) {
		assertTrue(StringUtils.isEmpty(str), message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	public static void assertEmpty(String str, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(StringUtils.isEmpty(str), messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	public static void assertEmpty(String str, String message, String fieldName) {
		assertTrue(StringUtils.isEmpty(str), message, fieldName);
	}


	/**
	 * @see #fail(Supplier)
	 */
	public static void assertEmpty(Collection<?> collection, Supplier<String> messageSupplier) {
		assertTrue(CollectionUtils.isEmpty(collection), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	public static void assertEmpty(Collection<?> collection, String message) {
		assertTrue(CollectionUtils.isEmpty(collection), message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	public static void assertEmpty(Collection<?> collection, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(CollectionUtils.isEmpty(collection), messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	public static void assertEmpty(Collection<?> collection, String message, String fieldName) {
		assertTrue(CollectionUtils.isEmpty(collection), message, fieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(String str, Supplier<String> messageSupplier) {
		assertTrue(!StringUtils.isEmpty(str), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(String str, String message) {
		assertTrue(!StringUtils.isEmpty(str), message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(String str, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(!StringUtils.isEmpty(str), messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(String str, String message, String fieldName) {
		assertTrue(!StringUtils.isEmpty(str), message, fieldName);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(Collection<?> collection, Supplier<String> messageSupplier) {
		assertTrue(!CollectionUtils.isEmpty(collection), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(Collection<?> collection, String message) {
		assertTrue(!CollectionUtils.isEmpty(collection), message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(Collection<?> collection, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(!CollectionUtils.isEmpty(collection), messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(Collection<?> collection, String message, String fieldName) {
		assertTrue(!CollectionUtils.isEmpty(collection), message, fieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("!null, null, _ -> fail; null, !null, _ -> fail")
	public static void assertEquals(Object object1, Object object2, Supplier<String> messageSupplier) {
		assertTrue(CompareUtils.isEqual(object1, object2), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("!null, null, _ -> fail; null, !null, _ -> fail")
	public static void assertEquals(Object object1, Object object2, String message) {
		assertTrue(CompareUtils.isEqual(object1, object2), message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	@Contract("!null, null, _, _ -> fail; null, !null, _, _ -> fail")
	public static void assertEquals(Object object1, Object object2, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(CompareUtils.isEqual(object1, object2), messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("!null, null, _, _ -> fail; null, !null, _, _ -> fail")
	public static void assertEquals(Object object1, Object object2, String message, String fieldName) {
		assertTrue(CompareUtils.isEqual(object1, object2), message, fieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, null, _ -> fail")
	public static void assertNotEquals(Object object1, Object object2, Supplier<String> messageSupplier) {
		assertTrue(!CompareUtils.isEqual(object1, object2), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, null, _ -> fail")
	public static void assertNotEquals(Object object1, Object object2, String message) {
		assertTrue(!CompareUtils.isEqual(object1, object2), message);
	}


	/**
	 * @see #failWithField(String, Supplier)
	 */
	@Contract("null, null, _, _ -> fail")
	public static void assertNotEquals(Object object1, Object object2, Supplier<String> messageSupplier, String fieldName) {
		assertTrue(!CompareUtils.isEqual(object1, object2), messageSupplier, fieldName);
	}


	/**
	 * @see #failWithField(String, String, Object...)
	 */
	@Contract("null, null, _, _ -> fail")
	public static void assertNotEquals(Object object1, Object object2, String message, String fieldName) {
		assertTrue(!CompareUtils.isEqual(object1, object2), message, fieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Throws a new {@link FieldValidationException} if the specified end date is not null and is before the specified start date.
	 */
	public static void assertBefore(Date startDate, Date endDate, String endDateFieldName) {
		ValidationUtils.assertFalse(endDate != null && endDate.before(startDate), "End date must be after start date " + DateUtils.fromDateSmart(startDate), endDateFieldName);
	}


	/**
	 * Throws a new {@link FieldValidationException} if the specified end time is not null and is before the specified start time.
	 */
	public static void assertBefore(Time startTime, Time endTime, String endTimeFieldName) {
		ValidationUtils.assertFalse(endTime != null && endTime.before(startTime), "End time must be after start time " + startTime, endTimeFieldName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Throws a new {@link ValidationException} with the specified message if the set of objects
	 * does not meet the following condition:  One object must be initialized and all others must be null
	 * <p>
	 * Note: If no arguments are passed, this method will throw an exception.  If one argument
	 * is passed, an exception will be thrown if the argument is null
	 */
	public static void assertMutuallyExclusive(String message, Object... objects) {
		boolean onlyOneHasValue = false;

		for (Object object : objects) {
			if (object != null) {
				if (!onlyOneHasValue) {
					onlyOneHasValue = true;
				}
				else {
					onlyOneHasValue = false; // one object already has value
					break;
				}
			}
		}
		if (!onlyOneHasValue) {
			throw new ValidationException(message);
		}
	}


	/**
	 * Throws a new {@link ValidationException} with the specified message if the set of objects
	 * does not meet the following condition:  One object must be equal to the valueToMatch argument and all others must be null
	 * or have a value that does not match the valueToMatch argument.
	 * <p>
	 * Note: If no arguments are passed, this method will throw an exception.
	 */
	public static void assertOneValueMatching(String message, Object valueToMatch, Object... objects) {
		boolean matchFound = false;

		for (Object object : objects) {
			if (CompareUtils.isEqual(valueToMatch, object)) {
				if (matchFound) {
					throw new ValidationException(message);
				}
				matchFound = true;
			}
		}

		if (!matchFound) {
			throw new ValidationException(message);
		}
	}


	@Contract("null, _, _ -> fail")
	public static void assertInstanceOf(Class<?> clazz, Object obj, String message) {
		assertNotNull(clazz, "Type to check against must not be null");
		if (!clazz.isInstance(obj)) {
			throw new ValidationException(message + "Object of class [" + (obj != null ? obj.getClass().getName() : "null") + "] must be an instance of " + clazz);
		}
	}


	/**
	 * Throws {@link FieldValidationException} when the specified decimal value is null or has precision greater than the specified max precision.
	 * The logic ignores trailing zeros.
	 */
	@Contract("null, _, _ -> fail")
	public static void assertMaxDecimalPrecision(BigDecimal value, int maxDecimalPrecision, String fieldName) {
		String friendlyName = StringUtils.splitWords(StringUtils.capitalize(fieldName));
		assertNotNull(value, "'" + friendlyName + "' field is required", fieldName);
		int decimalPlaces = Math.max(0, value.stripTrailingZeros().scale());
		if (maxDecimalPrecision < decimalPlaces) {
			throw new FieldValidationException("'" + friendlyName + "' " + value.stripTrailingZeros() + " cannot have more than " + maxDecimalPrecision + " decimal places", fieldName);
		}
	}
}
