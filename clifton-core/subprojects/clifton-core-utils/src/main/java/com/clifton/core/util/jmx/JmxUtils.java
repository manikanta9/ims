package com.clifton.core.util.jmx;

import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.validation.ValidationUtils;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanInfo;
import javax.management.MBeanServer;
import javax.management.MXBean;
import javax.management.ObjectName;
import javax.management.openmbean.CompositeData;
import javax.management.openmbean.TabularData;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.StringJoiner;
import java.util.stream.Collectors;


/**
 * The {@link JmxUtils} class provides static utility methods for accessing managed beans and their attributes from the locally registered {@link MBeanServer}.
 * <p>
 * Managed beans, such as {@link MXBean MXBeans}, may provide details about significant JVM services, objects, and attributes. Some examples include the {@link RuntimeMXBean},
 * which can be used to access JVM properties, the {@link MemoryMXBean}, which provides JVM memory details, and the {@link ThreadMXBean}, which provides details around the JVM
 * thread system. Other standard <i>MBeans</i> (a superset of {@link MXBean MXBeans} with less stringent constraints) may also be registered. These can be registered by various
 * applications. For example, Tomcat registers helpful MBeans to monitor host properties, such as servlets, configurations, and endpoints.
 * <p>
 * Although JMX does not provide a standard method of accessing entities referenced by MBeans, MBeans of type <code>org.apache.tomcat.util.modeler.BaseModelMBean</code> may be
 * dereferenced by the <code>CoreJmxUtils#getMBeanResource(ObjectName)</code> method.
 * <p>
 * MBean and other JMX-based diagnostic information can be viewed through the <a href="org.apache.tomcat.util.modeler.BaseModelMBean">jconsole</a> graphical tool, which is provided
 * with some distributions of the JDK.
 *
 * @author MikeH
 * @see ManagementFactory
 */
public class JmxUtils {

	private JmxUtils() {
		// Private constructor
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Platform MBean Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the registered platform {@link MBeanServer} instance.
	 */
	public static MBeanServer getPlatformMBeanServer() {
		return ManagementFactory.getPlatformMBeanServer();
	}


	/**
	 * Returns <code>true</code> if an MBean is registered for the object name constructed from the given <code>domain</code> and <code>propertyMap</code>.
	 */
	public static boolean isMBeanRegistered(String domain, Map<String, String> propertyMap) {
		return isMBeanRegistered(generateObjectNameQuery(domain, propertyMap, true));
	}


	/**
	 * Returns <code>true</code> if an MBean is registered for the given object name.
	 */
	public static boolean isMBeanRegistered(ObjectName objectName) {
		return getPlatformMBeanServer().isRegistered(objectName);
	}


	/**
	 * Gets the {@link MBeanWrapper} instance for the object name constructed from the given <code>domain</code> and <code>propertyMap</code>.
	 */
	public static MBeanWrapper getMBeanWrapper(String domain, Map<String, String> propertyMap) {
		return getMBeanWrapper(generateObjectNameQuery(domain, propertyMap, true));
	}


	/**
	 * Gets the {@link MBeanWrapper} instance for the given {@link ObjectName}.
	 */
	public static MBeanWrapper getMBeanWrapper(ObjectName objectName) {
		return generateMBeanWrapper(objectName, false);
	}


	/**
	 * Gets the {@link MBeanWrapper} instance for the object name constructed from the given <code>domain</code> and <code>propertyMap</code> with all non-primitive {@link
	 * MBeanWrapper#attributeMap attribute} values normalized to their string representations.
	 * .
	 */
	public static MBeanWrapper getMBeanWrapperNormalized(String domain, Map<String, String> propertyMap) {
		return getMBeanWrapperNormalized(generateObjectNameQuery(domain, propertyMap, true));
	}


	/**
	 * Gets the {@link MBeanWrapper} instance for the given {@link ObjectName} with all non-primitive {@link MBeanWrapper#attributeMap attribute} values normalized to their string
	 * representations.
	 */
	public static MBeanWrapper getMBeanWrapperNormalized(ObjectName objectName) {
		return generateMBeanWrapper(objectName, true);
	}


	/**
	 * Queries the {@link MBeanServer} for {@link ObjectName} entities which match the given domain and include the listed key-value pairs.
	 *
	 * @param domain           the domain under which to search, such as <code>"Catalina"</code> or <code>"java.lang"</code>
	 * @param keyValueQueryMap the map of key-value pairs for {@link ObjectName} filtering, such as <code>"type"</code> or <code>"name"</code> filters
	 * @return the collection of matching {@link ObjectName} entities registered in the {@link MBeanServer}
	 */
	public static Collection<ObjectName> queryMBeans(String domain, Map<String, String> keyValueQueryMap) {
		ObjectName objectNameQuery = generateObjectNameQuery(domain, keyValueQueryMap, false);
		return getPlatformMBeanServer().queryNames(objectNameQuery, null);
	}


	/**
	 * Queries the {@link MBeanServer} for {@link ObjectName} entities which match the given domain and have the exact listed key-value pairs.
	 * <p>
	 * This differs from {@link #queryMBeans(String, Map)} in that the queried results must <i>strictly</i> match the given list of key-value pairs and none others.
	 *
	 * @param domain           the domain under which to search, such as <code>"Catalina"</code> or <code>"java.lang"</code>
	 * @param keyValueQueryMap the map of key-value pairs for {@link ObjectName} filtering, such as <code>"type"</code> or <code>"name"</code> filters
	 * @return the collection of matching {@link ObjectName} entities registered in the {@link MBeanServer}
	 */
	public static Collection<ObjectName> queryMBeansStrict(String domain, Map<String, String> keyValueQueryMap) {
		ObjectName objectNameQuery = generateObjectNameQuery(domain, keyValueQueryMap, true);
		return getPlatformMBeanServer().queryNames(objectNameQuery, null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates the given <code>objectNameParam</code> for use as a singular {@link ObjectName} domain, key, or value.
	 * <p>
	 * This method acts as a pre-validation step before passing the string to the {@link ObjectName} constructor for validation. Additionally, this validates individual components
	 * to prevent incidental delimiter violations which could cause the {@link ObjectName} query to be read in a way other than intended.
	 */
	private static void validateObjectNameParam(String objectNameParam) {
		if (objectNameParam.startsWith("\"") || objectNameParam.endsWith("\"")) {
			// Validate quoted value usage; quotes must be first and last characters only. Quoted keys are not allowed and will throw during retrieval.
			ValidationUtils.assertTrue(objectNameParam.length() > 1
							&& objectNameParam.indexOf('"') == 0 // First char is quote
							&& objectNameParam.indexOf('"', 1) == (objectNameParam.length() - 1), // Only other quote is final char
					() -> String.format("Invalid ObjectName parameter: [%s]. ObjectName parameters may only contain quotes if those quotes only exist as both the first and last characters in the parameter.", objectNameParam));
		}
		else {
			// Validate disallowed parameters, including delimiters (disallow incidental delimiters in parameters)
			ValidationUtils.assertFalse(objectNameParam.contains(",")
							|| objectNameParam.contains(":")
							|| objectNameParam.contains("=")
							|| objectNameParam.contains("\""),
					() -> String.format("Invalid ObjectName parameter: [%s]. ObjectName parameters may not contain any of the characters \",\", \":\", \"=\", or \"\"\" (double-quote).", objectNameParam));
		}
	}


	/**
	 * Generates an {@link ObjectName} to use as when querying the {@link MBeanServer} for registered {@link ObjectName} entities with the given domain and key-value pairs.
	 *
	 * @param domain           the domain under which to query, such as <code>"Catalina"</code> or <code>"java.lang"</code>
	 * @param keyValueQueryMap the map of key-value pairs for {@link ObjectName} filtering, such as <code>"type"</code> or <code>"name"</code> filters
	 * @param strict           if <code>true</code>, the object name query will match objects with <i>only</i> the given key-value pairs and no others; otherwise, the query will
	 *                         match any objects which <i>include</i> the given key-value pairs
	 * @return the generated {@link ObjectName} to use when querying the {@link MBeanServer}
	 */
	private static ObjectName generateObjectNameQuery(String domain, Map<String, String> keyValueQueryMap, boolean strict) {
		try {
			String normalizedDomain = ObjectUtils.coalesce(domain, "*");
			StringJoiner keyValueStringJoiner = new StringJoiner(",");
			if (!strict) {
				// Aggregated filters are narrowing; use "*" to allow all key-value pairs on which we are not explicitly filtering
				keyValueStringJoiner.add("*");
			}
			if (keyValueQueryMap != null) {
				for (Map.Entry<String, String> entry : keyValueQueryMap.entrySet()) {
					validateObjectNameParam(entry.getKey());
					validateObjectNameParam(entry.getValue());
					keyValueStringJoiner.add(entry.getKey() + '=' + entry.getValue());
				}
			}
			return new ObjectName(normalizedDomain + ':' + keyValueStringJoiner);
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while attempting to generate an ObjectName query for domain [%s] and key-value pairs [%s].", domain, keyValueQueryMap), e);
		}
	}


	/**
	 * Generates an {@link MBeanWrapper} object for the given {@link ObjectName}. This wrapper encapsulates relevant properties for the {@link ObjectName}, such as its domain,
	 * properties, and currently-applicable attributes.
	 *
	 * @param objectName          the {@link ObjectName} for which to generate an {@link MBeanWrapper}
	 * @param normalizeAttributes if {@code true}, all attributes that are not primitives or boxed-primitives will be converted to their {@link String} representations; otherwise,
	 *                            attributes will be referenced directly
	 * @return the generated {@link MBeanWrapper}
	 */
	private static MBeanWrapper generateMBeanWrapper(ObjectName objectName, boolean normalizeAttributes) {
		try {
			// Get attribute info
			MBeanInfo beanInfo = getPlatformMBeanServer().getMBeanInfo(objectName);
			MBeanAttributeInfo[] attributeInfos = beanInfo.getAttributes();

			// Get attribute values
			Map<MBeanAttributeInfo, Object> attributeInfoToValueMap = new HashMap<>();
			for (MBeanAttributeInfo attribute : attributeInfos) {
				try {
					Object value = getPlatformMBeanServer().getAttribute(objectName, attribute.getName());
					// Normalize attributes, if designated, to remove nesting
					final Object normalizedValue;
					if (!normalizeAttributes || value == null || value instanceof String || value instanceof Number || value instanceof Boolean) {
						normalizedValue = value;
					}
					else if (value instanceof TabularData) {
						// Convert tabular data into string of newline-delimited key-value pairs
						@SuppressWarnings("unchecked")
						Collection<CompositeData> dataList = (Collection<CompositeData>) ((TabularData) value).values();
						normalizedValue = dataList.stream()
								.map(data -> data.getCompositeType().keySet().stream()
										.map(field -> field + " = " + data.get(field))
										.collect(Collectors.joining("\n")))
								.collect(Collectors.joining("\n\n"));
					}
					else {
						normalizedValue = String.valueOf(value);
					}
					attributeInfoToValueMap.put(attribute, normalizedValue);
				}
				catch (Exception e) {
					// Several exceptions can be thrown during attribute retrieval; swallow these and only accept successful attribute retrievals
				}
			}

			// Generate MBean wrapper
			return new MBeanWrapper(objectName, attributeInfoToValueMap);
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while attempting to generate an MBeanWrapper for ObjectName [%s].", objectName), e);
		}
	}
}
