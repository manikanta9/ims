package com.clifton.core.util.dataaccess;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <code>PagingArrayList</code> class is an <code>ArrayList</code> based implementation of <code>PagingList</code>.
 *
 * @param <E>
 * @author vgomelsky
 */
public class PagingArrayList<E> extends ArrayList<E> implements PagingList {

	/**
	 * Index of the first element in the List. First element index is 0.
	 */
	private final int firstElementIndex;
	/**
	 * Total number of elements (not just current page)
	 */
	private int totalElementCount;
	/**
	 * Page size or maximum number of elements per page.
	 */
	private int pageSize;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructs an empty <code>PagingList</code> with specified parameters.
	 */
	public PagingArrayList(int firstElementIndex, int pageSize) {
		super(pageSize < 50 ? pageSize : 32);
		AssertUtils.assertTrue(firstElementIndex >= 0, "first element index must be greater than or equal to 0");
		AssertUtils.assertFalse(pageSize < 0, "page size cannot be negative");
		this.firstElementIndex = firstElementIndex;
		this.pageSize = pageSize;
	}


	/**
	 * Constructs an empty <code>PagingList</code> with specified parameters.
	 */
	public PagingArrayList(int firstElementIndex) {
		this(firstElementIndex, 0);
	}


	/**
	 * Constructs an empty <code>PagingList</code>.
	 */
	public PagingArrayList() {
		this(0);
	}


	/**
	 * Constructs a new <code>PagingList</code> from the specified List.
	 */
	public PagingArrayList(List<? extends E> elements) {
		this(elements, 0, 0);
	}


	/**
	 * Constructs a new paging array list from the specified list using the specified arguments.
	 */
	public PagingArrayList(List<? extends E> elements, int firstElementIndex, int totalElementCount) {
		super(elements);
		this.firstElementIndex = firstElementIndex;
		this.pageSize = CollectionUtils.getSize(elements);
		this.totalElementCount = totalElementCount;
	}


	/**
	 * Constructs a new paging array list from the specified list using the specified arguments.
	 */
	public PagingArrayList(List<? extends E> elements, int firstElementIndex, int totalElementCount, int pageSize) {
		super(elements);
		this.firstElementIndex = firstElementIndex;
		this.pageSize = pageSize;
		this.totalElementCount = totalElementCount;
	}


	/**
	 * Appends the specified element to the end of this list. Increments totalElementCount and pageSize accordingly.
	 */
	@Override
	public boolean add(E element) {
		if (getTotalElementCount() == getPageSize() && getTotalElementCount() == getCurrentPageElementCount()) {
			this.pageSize++;
		}
		this.totalElementCount++;
		return super.add(element);
	}


	/**
	 * Inserts the specified element at the position in this list. Increments totalElementCount and pageSize
	 * accordingly.
	 */
	@Override
	public void add(int index, E element) {
		if (getTotalElementCount() == getPageSize() && getTotalElementCount() == getCurrentPageElementCount()) {
			this.pageSize++;
		}
		this.totalElementCount++;
		super.add(index, element);
	}


	/**
	 * Appends the specified List to the end of this list. New total element count is equal to the sum of both list
	 * element counts.
	 *
	 * @param list
	 * @return Returns <tt>true</tt> if this list changed as a result of the call.
	 */
	@Override
	public boolean addAll(Collection<? extends E> list) {
		if (!CollectionUtils.isEmpty(list)) {
			this.totalElementCount += list.size();
			this.pageSize += list.size();
			return super.addAll(list);
		}
		return false;
	}


	/**
	 * Removes the element at the specified position in this list. Shifts any subsequent elements to the left (subtracts
	 * one from their indices).
	 */
	@Override
	public E remove(int index) {
		E bean = super.remove(index);
		if (this.pageSize > 0) {
			this.totalElementCount--;
			this.pageSize--;
		}
		return bean;
	}


	@Override
	public int getCurrentPageElementCount() {
		return size();
	}


	@Override
	public int getCurrentPageNumber() {
		if (getPageSize() == 0) {
			return 0;
		}
		return (getFirstElementIndex() / getPageSize() + 1);
	}


	@Override
	public int getFirstElementIndex() {
		return this.firstElementIndex;
	}


	@Override
	public int getPageSize() {
		return this.pageSize;
	}


	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}


	@Override
	public int getTotalElementCount() {
		return this.totalElementCount;
	}


	public void setTotalElementCount(int totalElementCount) {
		AssertUtils.assertFalse(totalElementCount < 0, "total element count cannot be negative");
		this.totalElementCount = totalElementCount;
	}


	@Override
	public int getTotalPageCount() {
		if (getPageSize() == 0) {
			return 0;
		}
		return (getTotalElementCount() / getPageSize()) + ((getTotalElementCount() % getPageSize()) == 0 ? 0 : 1);
	}


	@Override
	public boolean isFirstPage() {
		return (getCurrentPageNumber() == 1);
	}


	@Override
	public boolean isLastPage() {
		return (getCurrentPageNumber() == getTotalPageCount());
	}
}
