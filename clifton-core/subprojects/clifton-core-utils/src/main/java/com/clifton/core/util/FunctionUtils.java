package com.clifton.core.util;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;


/**
 * The {@link FunctionUtils} type provides static utility methods for working with {@link FunctionalInterface functional interfaces}. See
 * <a href="https://docs.oracle.com/javase/specs/jls/se8/html/jls-9.html#jls-9.8">JLS 9.8. Functional Interfaces</a> for more information.
 *
 * @author MikeH
 */
public class FunctionUtils {

	private FunctionUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Produces a predicate which is a negation of the given predicate.
	 * <p>
	 * This is equivalent to the static <a href="https://docs.oracle.com/en/java/javase/11/docs/api/java.base/java/util/function/Predicate.html#not(java.util.function.Predicate)">
	 * Predicate#not(Predicate)</a> method added in JDK 11.
	 * <p>
	 * Example:
	 * <pre><code>
	 * List&lt;String&gt; stringList = Arrays.asList("My String", "Not My String");
	 *
	 * Predicate&lt;String&gt; myStringPredicate = string -> "My String".equals(string);
	 * Predicate&lt;String&gt; notMyStringPredicate = FunctionUtils.negate(myStringPredicate);
	 *
	 * List&lt;String&gt; myStringList = stringList.filter(myStringPredicate).collect(Collectors.toList()) // ["My String"];
	 * List&lt;String&gt; notMyStringList = stringList.filter(notMyStringPredicate).collect(Collectors.toList()) // ["Not My String"];
	 * </code></pre>
	 *
	 * @param predicate the predicate to negate
	 * @param <T>       the predicate parameter type
	 * @return the negated predicate
	 */
	public static <T> Predicate<T> negate(Predicate<T> predicate) {
		return predicate.negate();
	}


	/**
	 * Wraps the given {@link Runnable} in an unchecked exception. This is intended to provide an interface by which exception-throwing lambdas may be used.
	 */
	public static Runnable uncheckedRunnable(ThrowingRunnable fn) {
		return () -> {
			try {
				fn.run();
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}


	/**
	 * Wraps the given {@link Function} in an unchecked exception. This is intended to provide an interface by which exception-throwing lambdas may be used.
	 */
	public static <T, R> Function<T, R> uncheckedFunction(ThrowingFunction<T, R> fn) {
		return t -> {
			try {
				return fn.apply(t);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}


	/**
	 * Wraps the given {@link Predicate} in an unchecked exception. This is intended to provide an interface by which exception-throwing lambdas may be used.
	 */
	public static <T> Predicate<T> uncheckedPredicate(ThrowingPredicate<T> fn) {
		return t -> {
			try {
				return fn.test(t);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}


	/**
	 * Wraps the given {@link Consumer} in an unchecked exception. This is intended to provide an interface by which exception-throwing lambdas may be used.
	 */
	public static <T> Consumer<T> uncheckedConsumer(ThrowingConsumer<T> fn) {
		return t -> {
			try {
				fn.accept(t);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}


	/**
	 * Wraps the given {@link Supplier} in an unchecked exception. This is intended to provide an interface by which exception-throwing lambdas may be used.
	 */
	public static <T> Supplier<T> uncheckedSupplier(ThrowingSupplier<T> fn) {
		return () -> {
			try {
				return fn.get();
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * An exception-compatible delegate type for the {@link Runnable} interface.
	 *
	 * @see #uncheckedRunnable(ThrowingRunnable)
	 */
	public static interface ThrowingRunnable {

		public void run() throws Exception;
	}


	/**
	 * An exception-compatible delegate type for the {@link Function} interface.
	 *
	 * @see #uncheckedFunction(ThrowingFunction)
	 */
	public static interface ThrowingFunction<T, R> {

		public R apply(T t) throws Exception;
	}


	/**
	 * An exception-compatible delegate type for the {@link Predicate} interface.
	 *
	 * @see #uncheckedPredicate(ThrowingPredicate)
	 */
	public static interface ThrowingPredicate<T> {

		public boolean test(T t) throws Exception;
	}


	/**
	 * An exception-compatible delegate type for the {@link Consumer} interface.
	 *
	 * @see #uncheckedConsumer(ThrowingConsumer)
	 */
	public static interface ThrowingConsumer<T> {

		public void accept(T t) throws Exception;
	}

	/**
	 * An exception-compatible delegate type for the {@link Supplier} interface.
	 *
	 * @see #uncheckedSupplier(ThrowingSupplier)
	 */
	public static interface ThrowingSupplier<T> {

		public T get() throws Exception;
	}
}
