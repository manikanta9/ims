package com.clifton.core.util;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.StringTokenizer;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>StringUtils</code> provides utilities for working with String objects.
 *
 * @author vgomelsky
 */
public class StringUtils {


	private static final String DEFAULT_KEY_DELIMITER = ":";
	//Regex for identifying characters that are invalid for use in a freemarker variable name
	private static final String FREEMARKER_VARIABLE_REGEX = "[^a-zA-Z0-9_$@]";
	// Alpha Numeric string - must start with a letter and contain no spaces
	public static final String ALPHA_NUMERIC_NO_SPACE_START_WITH_ALPHA_ONLY_REGEX = "^[a-zA-Z]{1}[a-zA-Z0-9]*$";
	public static final String ALPHA_NUMERIC_START_WITH_ALPHA_ONLY_REGEX = "^[a-zA-Z]{1}[a-zA-Z0-9 ]*$";
	//Constant for replacing invalid characters in freemarker variable names
	private static final String FREEMARKER_VARIABLE_REPLACEMENT = "_";

	public static final String EMPTY_STRING = "";
	public static final String NULL_STRING = "NULL";
	public static final String NEW_LINE = "\r\n";
	public static final String TAB = "\t";
	public static final String HTML_BR = "<br/>";
	public static final String HTML_LIST_ITEM_OPEN = "<li>";
	public static final String HTML_LIST_ITEM_CLOSE = "</li>";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the specified string is empty (null, only spaces).
	 */
	public static boolean isEmpty(CharSequence str) {
		return str == null || str.codePoints().allMatch(Character::isWhitespace);
	}


	/**
	 * Returns true if both strings are equal (nulls are considered to be equal).
	 */
	public static boolean isEqual(String s1, String s2) {
		if (s1 == null) {
			return (s2 == null);
		}
		return s1.equals(s2);
	}


	/**
	 * Determines if the source string has any match within the array of comparison strings. {@code null} values are considered equal.
	 *
	 * @param s1               the source string
	 * @param stringsToCompare the array of comparison strings
	 * @return {@code true} if the any string in the array of strings to compare is equal to the source string
	 */
	public static boolean equalsAny(String s1, String... stringsToCompare) {
		// Guard-clause: No strings to compare
		if (ArrayUtils.isEmpty(stringsToCompare)) {
			return false;
		}

		return ArrayUtils.getStream(stringsToCompare).anyMatch(s2 -> isEqual(s1, s2));
	}


	/**
	 * Determines if the source string has any match within the array of comparison strings. Strings are compared case-insensitively. {@code null} values are
	 * considered equal.
	 *
	 * @param s1               the source string
	 * @param stringsToCompare the array of comparison strings
	 * @return {@code true} if the any string in the array of strings to compare is equal to the source string
	 */
	public static boolean equalsAnyIgnoreCase(String s1, String... stringsToCompare) {
		// Guard-clause: No strings to compare
		if (ArrayUtils.isEmpty(stringsToCompare)) {
			return false;
		}

		return ArrayUtils.getStream(stringsToCompare).anyMatch(s2 -> isEqualIgnoreCase(s1, s2));
	}


	/**
	 * Two strings are considered equal ignoring case if they
	 * are of the same length and corresponding characters in the two strings
	 * are equal ignoring case. (nulls are considered to be equal)
	 */
	public static boolean isEqualIgnoreCase(String s1, String s2) {
		if (s1 == null) {
			return (s2 == null);
		}
		return s1.equalsIgnoreCase(s2);
	}


	/**
	 * Returns if searchForString is contained in searchInString or if both Strings are null.
	 */
	public static boolean isLikeIgnoringCase(String searchInString, String searchForString) {
		if (searchInString == null) {
			return (searchForString == null);
		}
		if (searchForString == null) {
			return false;
		}

		return searchInString.toLowerCase().contains(searchForString.toLowerCase());
	}


	/**
	 * Returns the number of characters in the specified <code>String</code>
	 */
	public static int length(String str) {
		return (str == null) ? 0 : str.length();
	}


	/**
	 * Returns the number of characters in the specified <code>String</code> excluding special characters.
	 */
	public static int lengthUTF8(CharSequence sequence) {
		int count = 0;
		if (sequence != null) {
			for (int i = 0, len = sequence.length(); i < len; i++) {
				char ch = sequence.charAt(i);
				int currCharCount = getUTF8LengthForChar(ch);
				if (currCharCount == 4) {
					++i; // this only happens with high surrogates because they are not characters themselves, but represent supplementary characters
				}
				count += currCharCount;
			}
		}
		return count;
	}


	private static int getUTF8LengthForChar(char ch) {
		if (ch <= 0x7F) {
			return 1;
		}
		else if (ch <= 0x7FF) {
			return 2;
		}
		else if (Character.isHighSurrogate(ch)) {
			return 4;
		}
		else {
			return 3;
		}
	}


	public static String toNullableString(Object obj) {
		if (obj == null) {
			return null;
		}

		return obj.toString();
	}


	/**
	 * Returns SQL friendly representation of the specified string (put inside of quotes and escape single quotes).
	 */
	public static String toSQL(String str) {
		if (str == null) {
			return NULL_STRING;
		}
		if (str.isEmpty()) {
			return "''";
		}
		return "'" + str.replaceAll("'", "''") + "'";
	}


	public static boolean contains(String str, char searchChar) {
		if (isEmpty(str)) {
			return false;
		}
		return str.indexOf(searchChar) >= 0;
	}


	public static boolean contains(String str, String searchStr) {
		if (str == null || searchStr == null) {
			return false;
		}
		return str.contains(searchStr);
	}


	public static boolean startsWith(String str, String chars) {
		if (isEmpty(str) || isEmpty(chars)) {
			return false;
		}
		return str.startsWith(chars);
	}


	public static boolean endsWith(String str, String chars) {
		if (isEmpty(str) || isEmpty(chars)) {
			return false;
		}
		return str.endsWith(chars);
	}


	/**
	 * Removes all occurrences of the specified regular expression from the specified string.
	 */
	public static String removeAll(String str, String removeRegExp) {
		if (isEmpty(str)) {
			return str;
		}
		return str.replaceAll(removeRegExp, "");
	}


	/**
	 * Capitalizes the first character of the argument string.
	 */
	public static String capitalize(String str) {
		if (length(str) != 0) {
			char firstChar = str.charAt(0);
			if (Character.isLowerCase(firstChar)) {
				return Character.toUpperCase(firstChar) + str.substring(1);
			}
		}
		return str;
	}


	/**
	 * Lowercase the first character of the argument string.
	 */
	public static String deCapitalize(String str) {
		if (length(str) != 0) {
			char firstChar = str.charAt(0);
			if (Character.isUpperCase(firstChar)) {
				return Character.toLowerCase(firstChar) + str.substring(1);
			}
		}
		return str;
	}


	/**
	 * Inserts a space between words (words starts with a capital letter).
	 */
	public static String splitWords(String str) {
		if (str == null) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		String remainingStr;
		do {
			remainingStr = removeLeadingWord(str);
			String strToAdd;
			if (remainingStr != null) {
				strToAdd = str.substring(0, str.length() - remainingStr.length());
				str = remainingStr;
			}
			else {
				strToAdd = str;
			}
			if (result.length() > 0 && result.charAt(result.length() - 1) != ' ' && strToAdd.charAt(0) != ' ') {
				result.append(' ');
			}
			result.append(strToAdd);
		}
		while (remainingStr != null);
		return result.toString();
	}


	/**
	 * Removes the leading word from the specified {@link String}. Assumes no spaces between words and that each
	 * new word starts with a capital letter. Returns null if the specified argument is null or one word.
	 */
	public static String removeLeadingWord(String str) {
		int length = length(str);
		if (length < 2) {
			return null;
		}
		// skip leading upper case characters
		int start = 1;
		int adjust = 0;
		while (start < length && Character.isUpperCase(str.charAt(start))) {
			start++;
		}
		// find beginning of next word
		if (start == 1) {
			while (start < length && Character.isLowerCase(str.charAt(start))) {
				start++;
			}
		}
		else {
			adjust = -1;
		}
		if (start == length) {
			return null;
		}
		return str.substring(start + adjust);
	}


	/**
	 * Truncates the string at the last index of passed in Word (which cannot be the first word)
	 *
	 * @param str      - String to truncate and return
	 * @param word     - String to search for (Must start with a capital letter)
	 * @param allowEnd - if true allows the string to end with the word, otherwise REQUIRES another word after
	 */
	public static String truncateStringAfterWord(String str, String word, boolean allowEnd) {
		if (str == null || word == null) {
			return null;
		}
		if (!Character.isUpperCase(word.charAt(0))) {
			throw new RuntimeException("Word [" + word + "] must start with an Uppercase letter.");
		}

		int index = str.lastIndexOf(word);
		// Word is not present, or word was first - Don't do anything
		if (index <= 0) {
			return str;
		}

		// Otherwise - contained in the String
		int endIndex = index + word.length() - 1;

		// If it is the last word and that is allowed - remove it
		if (endIndex == str.length() - 1) {
			if (allowEnd) {
				return str.substring(0, index);
			}
		}
		// Otherwise make sure the text following the word is New word
		else if (Character.isUpperCase(str.charAt(endIndex + 1))) {
			// If so - truncate it
			return str.substring(0, index);
		}

		return str;
	}


	/**
	 * Retrieves and returns the leading word from the specified {@link String}. Assumes no spaces between words and that each new word starts with a capital
	 * letter. Returns null if the specified argument is null.
	 */
	public static String getLeadingWord(String str) {
		String without = removeLeadingWord(str);
		if (without == null) {
			return str;
		}
		return str.substring(0, str.length() - without.length());
	}


	/**
	 * Finds the first non-empty {@link String} in the argument list, or <code>null</code> if no such item is found.
	 *
	 * @param args the items to coalesce
	 * @return the first non-empty {@link String}
	 */
	public static String coalesce(String... args) {
		return coalesce(false, args);
	}


	public static String coalesce(boolean emptyStringIfNull, String... args) {
		// No arguments passed, return empty String or null
		if (args == null || args.length == 0) {
			return (emptyStringIfNull ? "" : null);
		}

		// Find the first not empty String and return that
		for (String arg : args) {
			if (!isEmpty(arg)) {
				return arg;
			}
		}

		//  All values are empty, return the first value
		String arg = args[0];
		return (emptyStringIfNull ? "" : arg);
	}


	/**
	 * Filters out special ASCII characters
	 */
	public static String filterASCII(String str) {
		// Filter out SOH (Start of Heading) characters & NUL (NULL Characters)
		// If the communication primarily exists of commands and messages, the SOH can be used to mark the beginning of each message header.
		// In the original 1963 definition of the ASCII standard the name start of message was used, which has been renamed to start of heading in the final release.
		// Nowadays we often see the SOH used in serial RS232 communications where there is a master-slave configuration. Each command from the master starts with the SOH.
		// This makes it possible for the slave or slaves to resynchronize on the next command when data errors occured.
		// Without a clear marking of the start of each command a resync might be problematic to implement.
		if (!isEmpty(str)) {
			char soh = 1;
			str = str.replace(soh, ' ');

			char nullValue = 0;
			str = str.replace(nullValue, ' ');

			return str;
		}
		return null;
	}


	/**
	 * Returns a comma delimited String of specified list values.
	 */
	public static String collectionToCommaDelimitedString(Collection<?> list) {
		return collectionToDelimitedString(list, ", ");
	}


	/**
	 * Returns a delimited String of specified list values.
	 */
	public static <T> String collectionToDelimitedString(Collection<T> list, String delimiter) {
		int size = (list == null) ? 0 : list.size();
		if (size == 0) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		for (Object item : list) {
			result.append(item);
			if (size-- > 1) {
				result.append(delimiter);
			}
		}
		return result.toString();
	}


	/**
	 * Returns a comma delimited String of specified list values.  Uses the specified converter to convert list values before adding them to the result.
	 */
	public static <F, T> String collectionToCommaDelimitedString(Collection<F> list, Function<F, T> converter) {
		int size = (list == null) ? 0 : list.size();
		if (size == 0) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		for (F item : list) {
			result.append(converter.apply(item));
			if (size-- > 1) {
				result.append(", ");
			}
		}
		return result.toString();
	}


	public static List<String> delimitedStringToList(String delimitedString, String delimiter) {
		if (isEmpty(delimitedString)) {
			return null;
		}
		List<String> stringList = new ArrayList<>();
		if (delimiter == null || delimiter.isEmpty()) {
			stringList.add(delimitedString);
			return stringList;
		}
		String[] arr = delimitedString.split(delimiter);
		for (String s : arr) {
			if (!isEmpty(s)) {
				stringList.add(s);
			}
		}
		return stringList;
	}


	/**
	 * Compares 2 strings lexicographically and returns a negative number if s1 is alphabetically before s2,
	 * 0 if they are equal, and a positive number if s2 is alphabetically before s1.
	 * <p>
	 * Note: This method ignores cases.
	 */
	public static int compare(String s1, String s2) {
		if (isEmpty(s1) && isEmpty(s2)) {
			return 0;
		}
		if (isEmpty(s1)) {
			return 1;
		}
		if (isEmpty(s2)) {
			return -1;
		}
		return s1.compareToIgnoreCase(s2);
	}


	/**
	 * Generates a key for the given set of values.
	 * <p>
	 * The produced key will be the concatenation of the string representation each of the provided values delimited with {@value #DEFAULT_KEY_DELIMITER}.
	 *
	 * @param values the values to concatenate into the key
	 * @return the generated key for the given list of values
	 * @see #generateKeyWithDelimiter(String, Object[])
	 */
	@SafeVarargs
	public static <T> String generateKey(T... values) {
		return generateKeyWithDelimiter(DEFAULT_KEY_DELIMITER, values);
	}


	/**
	 * Generates a key for the given set of values.
	 * <p>
	 * The produced key will be the concatenation of the string representation each of the provided values delimited with the given delimiter.
	 *
	 * @param delimiter the delimiter to use
	 * @param values    the values to concatenate into the key
	 * @return the generated key for the given list of values
	 */
	@SafeVarargs
	public static <T> String generateKeyWithDelimiter(String delimiter, T... values) {
		return ArrayUtils.getStream(values).map(String::valueOf).collect(Collectors.joining(delimiter));
	}


	/**
	 * <p>Joins the elements of the provided collection into a single String
	 * containing the provided list of elements.</p>
	 * <p/>
	 * <p>No delimiter is added before or after the list.
	 * A <code>null</code> separator is the same as an empty String ("").
	 * Null objects or empty strings within the array are represented by
	 * empty strings.</p>
	 * <p>
	 * <pre>
	 * StringUtils.join(null, *)                = null
	 * StringUtils.join([], *)                  = ""
	 * StringUtils.join([null], *)              = ""
	 * StringUtils.join(["a", "b", "c"], "--")  = "a--b--c"
	 * StringUtils.join(["a", "b", "c"], null)  = "abc"
	 * StringUtils.join(["a", "b", "c"], "")    = "abc"
	 * StringUtils.join([null, "", "a"], ',')   = ",,a"
	 * </pre>
	 */
	public static String join(Collection<String> strings, String separator) {
		if (strings == null) {
			return null;
		}
		return join(strings.stream(), string -> string == null ? "" : string, separator);
	}


	/**
	 * <p>Joins the elements of the provided array into a single String
	 * containing the provided list of elements.</p>
	 * <p/>
	 * <p>No delimiter is added before or after the list.
	 * A <code>null</code> separator is the same as an empty String ("").
	 * Null objects or empty strings within the array are represented by
	 * empty strings.</p>
	 * <p>
	 * <pre>
	 * StringUtils.join(null, *)                = null
	 * StringUtils.join([], *)                  = ""
	 * StringUtils.join([null], *)              = ""
	 * StringUtils.join(["a", "b", "c"], "--")  = "a--b--c"
	 * StringUtils.join(["a", "b", "c"], null)  = "abc"
	 * StringUtils.join(["a", "b", "c"], "")    = "abc"
	 * StringUtils.join([null, "", "a"], ',')   = ",,a"
	 * </pre>
	 *
	 * @param strings   the array of values to join together, may be null
	 * @param separator the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null array input
	 */
	public static String join(String[] strings, String separator) {
		if (strings == null) {
			return null;
		}
		return join(Arrays.stream(strings), string -> string == null ? "" : string, separator);
	}


	/**
	 * Follows the same logic as {@link #join(String[], String)} for a collection of objects.
	 * The provided function is used to map/convert each non null object to a string.
	 *
	 * @param objects              the collection of objects to join together, may be null
	 * @param objectToStringMapper the function used to convert each object to a string if the object is not null
	 * @param separator            the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null array input
	 */
	public static <T> String join(Collection<T> objects, Function<T, String> objectToStringMapper, String separator) {
		if (objects == null) {
			return null;
		}
		return join(objects.stream(), objectToStringMapper, separator);
	}


	/**
	 * Follows the same logic as {@link #join(Collection, String)} for an array of objects.
	 * The provided function is used to map/convert each non null object to a string.
	 *
	 * @param objects              the array of objects to join together, may be null
	 * @param objectToStringMapper the function used to convert each object to a string if the object is not null
	 * @param separator            the separator character to use, null treated as ""
	 * @return the joined String, <code>null</code> if null array input
	 */
	public static <T> String join(T[] objects, Function<T, String> objectToStringMapper, String separator) {
		if (objects == null) {
			return null;
		}
		return join(Arrays.stream(objects), objectToStringMapper, separator);
	}


	private static <T> String join(Stream<T> objectStream, Function<T, String> objectToStringMapper, String separator) {
		return objectStream.map(object -> {
			if (object == null) {
				return "";
			}
			return objectToStringMapper == null ? object.toString() : objectToStringMapper.apply(object);
		}).collect(Collectors.joining(separator == null ? "" : separator));
	}


	/**
	 * Joins the given list of strings with the given <code>delimiter</code>, excluding all <code>null</code> and empty string values.
	 * <p>
	 * Examples:
	 * <p>
	 * <pre><code>
	 * StringUtils#joinExcludingNulls(".", "a", "b", "c", "d");   // "a.b.c.d"
	 * StringUtils#joinExcludingNulls(".", "a", null, "c", "d");  // "a.c.d"
	 * StringUtils#joinExcludingNulls(".", null, "1", "", "2");   // "1.2"
	 * StringUtils#joinExcludingNulls(".", null, null, null);     // ""
	 * StringUtils#joinExcludingNulls(null, "a", "b", "c", "d");  // "abcd"
	 * </code></pre>
	 *
	 * @param delimiter the delimiter to use when joining the given strings
	 * @param values    the strings to join
	 * @return the delimited string, excluding nulls and empty strings
	 */
	public static String joinExcludingNulls(String delimiter, String... values) {
		StringBuilder sb = new StringBuilder();
		String appliedDelimiter = delimiter != null ? delimiter : "";
		boolean firstEl = true;
		for (String value : values) {
			if (!isEmpty(value)) {
				if (firstEl) {
					firstEl = false;
				}
				else {
					sb.append(appliedDelimiter);
				}
				sb.append(value);
			}
		}
		return sb.toString();
	}


	/**
	 * Defaults the maxNumberOfPreviews in {@link #joinErrorListOfViolations(String, int, Collection)} to 5.
	 */
	public static String joinErrorListOfViolations(String message, Collection<String> violations) {
		return joinErrorListOfViolations(message, 5, violations);
	}


	/**
	 * Creates an html error message consisting of the given header message and list of at most <code>maxNumberOfPreviews</code> violations.
	 *
	 * <p>
	 * StringUtils#joinErrorListOfViolations("Message header:", 2, ["violation 1", "violation 2", "violation 3"] ->
	 * </p><br>
	 * <div>
	 *     Message header:
	 *     <ul>
	 *       <li>violation 1</li>
	 *       <li>violation 2</li>
	 *       <li>...</li>
	 *     </ul>
	 * </div>
	 *
	 * @param messageHeader       the header message for the list of violations
	 * @param maxNumberOfPreviews maximum number of violations to show before ellipsis
	 * @param violations          violation messages
	 * @return message header with list of violations
	 */
	public static String joinErrorListOfViolations(String messageHeader, int maxNumberOfPreviews, Collection<String> violations) {
		if (violations == null || violations.isEmpty()) {
			return messageHeader;
		}
		List<String> violationStrings = new ArrayList<>(violations);
		StringBuilder sb = new StringBuilder(messageHeader).append(HTML_BR).append(HTML_BR);
		sb.append(HTML_LIST_ITEM_OPEN)
				.append(join(violationStrings.subList(0, Integer.min(violationStrings.size(), maxNumberOfPreviews)), HTML_LIST_ITEM_CLOSE + HTML_LIST_ITEM_OPEN))
				.append(HTML_LIST_ITEM_CLOSE);
		if (violationStrings.size() > maxNumberOfPreviews) {
			sb.append(HTML_LIST_ITEM_OPEN).append("...").append(HTML_LIST_ITEM_CLOSE);
		}
		return sb.toString();
	}


	public static String substring(String str, int beginIndex, int endIndex) {
		if (isEmpty(str)) {
			return str;
		}
		return str.substring(beginIndex, endIndex);
	}


	/**
	 * <p>Gets the substring after the first occurrence of a separator.
	 * The separator is not returned.</p>
	 * <p/>
	 * <p>A <code>null</code> string input will return <code>null</code>.
	 * An empty ("") string input will return the empty string.
	 * An empty or <code>null</code> separator will return the empty string if
	 * the input string is not <code>null</code>.</p>
	 * <p>
	 * <pre>
	 * StringUtils.substringAfterFirst(null, *) = null
	 * StringUtils.substringAfterFirst("", *) = ""
	 * StringUtils.substringAfterFirst(*, "") = ""
	 * StringUtils.substringAfterFirst(*, null) = ""
	 * StringUtils.substringAfterFirst("abc", "a") = "bc"
	 * StringUtils.substringAfterFirst("barber", "b") = "arber"
	 * StringUtils.substringAfterFirst("abc", "b") = "c"
	 * StringUtils.substringAfterFirst("a", "a") = ""
	 * StringUtils.substringAfterFirst("a", "z") = ""
	 * </pre>
	 *
	 * @param str       the String to get a substring from, may be null
	 * @param separator the String to search for, may be null
	 * @return the substring after the first occurrence of the separator, <code>null</code> if null String input
	 */
	public static String substringAfterFirst(String str, String separator) {
		if (isEmpty(str)) {
			return str;
		}
		if (isEmpty(separator)) {
			return EMPTY_STRING;
		}
		int pos = str.indexOf(separator);
		if (pos == -1 || pos == (str.length() - separator.length())) {
			return EMPTY_STRING;
		}
		return str.substring(pos + separator.length());
	}


	/**
	 * <p>Gets the substring before the first occurrence of a separator.
	 * The separator is not returned.</p>
	 * <p/>
	 * <p>A <code>null</code> string input will return <code>null</code>.
	 * An empty ("") string input will return the empty string.
	 * An empty or <code>null</code> separator will return the empty string if
	 * the input string is not <code>null</code>.</p>
	 * <p>
	 * <pre>
	 * StringUtils.substringBeforeFirst(null, *) = null
	 * StringUtils.substringBeforeFirst("", *) = ""
	 * StringUtils.substringBeforeFirst(*, "") = ""
	 * StringUtils.substringBeforeFirst(*, null) = ""
	 * StringUtils.substringBeforeFirst("abc", "a") = ""
	 * StringUtils.substringBeforeFirst("barber", "r") = "ba"
	 * StringUtils.substringBeforeFirst("abc", "b") = "a"
	 * StringUtils.substringBeforeFirst("a", "a") = ""
	 * StringUtils.substringBeforeFirst("a", "z") = ""
	 * </pre>
	 *
	 * @param str       the String to get a substring from, may be null
	 * @param separator the String to search for, may be null
	 * @return the substring before the first occurrence of the separator, <code>null</code> if null String input
	 */
	public static String substringBeforeFirst(String str, String separator) {
		if (isEmpty(str)) {
			return str;
		}
		if (isEmpty(separator)) {
			return EMPTY_STRING;
		}
		int pos = str.indexOf(separator);
		if (pos == -1) {
			return EMPTY_STRING;
		}
		return str.substring(0, pos);
	}


	/**
	 * <p>Gets the substring after the last occurrence of a separator.
	 * The separator is not returned.</p>
	 * <p/>
	 * <p>A <code>null</code> string input will return <code>null</code>.
	 * An empty ("") string input will return the empty string.
	 * An empty or <code>null</code> separator will return the empty string if
	 * the input string is not <code>null</code>.</p>
	 * <p>
	 * <pre>
	 * StringUtils.substringAfterLast(null, *) = null
	 * StringUtils.substringAfterLast("", *) = ""
	 * StringUtils.substringAfterLast(*, "") = ""
	 * StringUtils.substringAfterLast(*, null) = ""
	 * StringUtils.substringAfterLast("abc", "a") = "bc"
	 * StringUtils.substringAfterLast("barber", "b") = "er"
	 * StringUtils.substringAfterLast("abc", "c") = ""
	 * StringUtils.substringAfterLast("a", "a") = ""
	 * StringUtils.substringAfterLast("a", "z") = ""
	 * </pre>
	 *
	 * @param str       the String to get a substring from, may be null
	 * @param separator the String to search for, may be null
	 * @return the substring after the last occurrence of the separator, <code>null</code> if null String input
	 */
	public static String substringAfterLast(String str, String separator) {
		if (isEmpty(str)) {
			return str;
		}
		if (isEmpty(separator)) {
			return "";
		}
		int pos = str.lastIndexOf(separator);
		if (pos == -1 || pos == (str.length() - separator.length())) {
			return "";
		}
		return str.substring(pos + separator.length());
	}


	/**
	 * <p>Gets the substring from the start of the string to the last occurrence of the separator.
	 * The separator is not returned.</p>
	 * <p/>
	 * <p>A <code>null</code> string input will return <code>null</code>.
	 * An empty ("") string input will return the empty string.
	 * An empty or <code>null</code> separator will return the empty string if
	 * the input string is not <code>null</code>.</p>
	 * <p>
	 * <pre>
	 * StringUtils.substringBeforeLast(null, *) = null
	 * StringUtils.substringBeforeLast("", *) = ""
	 * StringUtils.substringBeforeLast(*, "") = ""
	 * StringUtils.substringBeforeLast(*, null) = ""
	 * StringUtils.substringBeforeLast("abc", "a") = ""
	 * StringUtils.substringBeforeLast("barber", "b") = "bar"
	 * StringUtils.substringBeforeLast("abc", "c") = "ab"
	 * StringUtils.substringBeforeLast("a", "a") = ""
	 * StringUtils.substringBeforeLast("a", "z") = ""
	 * </pre>
	 */
	public static String substringBeforeLast(String str, String separator) {
		if (isEmpty(str)) {
			return str;
		}
		if (isEmpty(separator)) {
			return "";
		}
		int pos = str.lastIndexOf(separator);
		if (pos == -1) {
			return "";
		}
		return str.substring(0, pos);
	}


	/**
	 * Supports appending string to StringBuilder ONLY if it's not null, otherwise
	 * does nothing.
	 */
	public static void appendString(StringBuilder strBuilder, String str) {
		if (strBuilder != null && str != null) {
			strBuilder.append(str);
		}
	}


	public static String getParameterValueFromUrl(String url, String parameterName) {
		if (url != null && url.contains("?") && parameterName != null) {
			String paramUrl = url.substring(url.indexOf("?") + 1);
			String[] params = paramUrl.split("&");
			for (String param : params) {
				String[] paramNameValue = param.split("=");
				String paramName = paramNameValue[0];
				if (isEqual(parameterName, paramName)) {
					if (paramNameValue.length == 1) {
						return "";
					}
					return paramNameValue[1];
				}
			}
		}
		return null;
	}


	/**
	 * Extracts the name of the document by returning the substring after the
	 * last occurrence of the path separator in the specified {@code path}.
	 * <p>
	 * For example, on Windows if the specified {@code path} is:<br>
	 * c:\Blah\Path\SomeDir\FileName.doc<br>
	 * the method will return 'FileName.doc'
	 *
	 * @param path e.g {@code /usr/local/file.txt}
	 * @return substring after last occurrence of the path separator (slash or backslash.) .e.g {@code file.txt}
	 */
	public static String extractFileName(String path) {

		//In case the specified path is specified with forward slashes
		//Due to the fact that underlying abstract layers manipulate the path
		//names we have to do this check so that the code works on all platforms
		if (path.contains("/")) {
			return path.substring(path.lastIndexOf("/") + 1);
		}

		return path.substring(path.lastIndexOf(System.getProperty("file.separator")) + 1);
	}


	/**
	 * Verifies that the specified {@code phoneNumber} is valid.
	 * This is very loose validation, and just strips all non digits and then confirms that the length is at least 10
	 *
	 * @param allowEmpty considers null/blank phoneNumbers to be valid
	 */
	public static boolean isPhoneNumberValid(String phoneNumber, boolean allowEmpty) {
		if (!isEmpty(phoneNumber)) {
			String phoneNumberDigits = phoneNumber.replaceAll("[^\\d]", "");
			if (phoneNumberDigits.length() < 10) {
				return false;
			}
			return true;
		}
		return allowEmpty;
	}


	/**
	 * If the length of the specified longString is longer than n, then trims it to
	 * length of n with 3 dots in the end. Otherwise returns the argument string.
	 */
	public static String formatStringUpToNCharsWithDots(String longString, int n) {
		return formatStringUpToNCharsWithDots(longString, n, false);
	}


	/**
	 * If the length of the specified longString is longer than n, then trims it to
	 * length of n with 3 dots in the end. Otherwise returns the argument string.
	 * This method takes a boolean to allow calculating using {@link #lengthUTF8(CharSequence)} in order to handle special characters.
	 */
	public static String formatStringUpToNCharsWithDots(String longString, int n, boolean utf8Encoding) {
		if (utf8Encoding) {
			return formatStringUpToNCharsWithDotsUTF8(longString, n);
		}
		if (!isEmpty(longString) && length(longString) > n) {
			return longString.substring(0, (n - 3)) + "...";
		}
		return longString;
	}


	/**
	 * If the UTF8 length of the specified longString is longer than maxTotalLength, then trims it to
	 * length of n with 3 dots in the end. Otherwise returns the argument string.
	 * NOTE: this method does NOT use {@link #lengthUTF8(CharSequence)} because that would require iterating the CharSequence twice in the case that trimming is necessary.
	 */
	public static String formatStringUpToNCharsWithDotsUTF8(String longString, int maxTotalLength) {
		CharSequence sequence = longString;
		int resultUTF8Length = 0;
		if (sequence != null) {
			StringBuilder resultStringBuilder = new StringBuilder();
			for (int i = 0, len = sequence.length(); i < len; i++) {
				char ch = sequence.charAt(i);
				int currCharLength = getUTF8LengthForChar(ch);
				if (currCharLength == 4) {
					++i; // this only happens with high surrogates because they are not characters themselves, but represent supplementary characters
				}
				if (resultUTF8Length + currCharLength > maxTotalLength) {
					return formatUTF8EllipsesForString(resultStringBuilder, resultUTF8Length, maxTotalLength);
				}
				else {
					resultUTF8Length += currCharLength;
					resultStringBuilder.append(ch);
				}
			}
			return resultStringBuilder.toString();
		}
		return longString;
	}


	/**
	 * Removes characters from the stringBuilder until there is sufficient UTF8 space to fit ellipses.
	 */
	private static String formatUTF8EllipsesForString(StringBuilder stringBuilder, int stringBuilderUTF8Length, int maxTotalLength) {
		// add ellipses
		int spaceForEllipses = maxTotalLength - stringBuilderUTF8Length;
		while (spaceForEllipses < 3) {
			char removedChar = stringBuilder.charAt(stringBuilder.length() - 1);
			stringBuilder.deleteCharAt(stringBuilder.length() - 1);
			spaceForEllipses += getUTF8LengthForChar(removedChar);
		}
		return stringBuilder.append("...").toString();
	}


	/**
	 * Tokenizes the specified {@code string} based on specified {@code
	 * delimiter} and return a array of {@code String} objects.
	 *
	 * @param string    if empty/null, {@code null} is returned
	 * @param delimiter if empty/null, {@code null} is returned
	 * @return array of {@code String} objects.
	 */
	public static String[] tokenizeToStringArray(String string, String delimiter) {
		if (isEmpty(string)) {
			return null;
		}

		StringTokenizer st = new StringTokenizer(string, delimiter);

		List<String> tokens = new ArrayList<>();

		while (st.hasMoreTokens()) {
			String token = st.nextToken();

			tokens.add(token);
		}
		return tokens.toArray(new String[0]);
	}


	/**
	 * Copy the given Collection into a String array. The Collection must
	 * contain String elements only.
	 *
	 * @param collection the Collection to copy
	 * @return the String array ({@code null} if the Collection was {@code null} or empty)
	 */
	public static String[] toStringArray(Collection<String> collection) {
		if (collection == null || collection.isEmpty()) {
			return null;
		}

		return collection.toArray(new String[0]);
	}


	/**
	 * <p>Gets the leftmost <code>len</code> characters of a String.</p>
	 * <pre>
	 * StringUtils.left("abc", 0)   = ""
	 * StringUtils.left("abc", 2)   = "ab"
	 * StringUtils.left("abc", 4)   = "abc"
	 * </pre>
	 *
	 * @param str the String to get the leftmost characters from, may be null
	 * @param len the length of the required String, must greater than or equal to zero.
	 * @return the leftmost characters, <code>null</code> if null String input
	 */
	public static String left(String str, int len) {
		if (len < 0) {
			throw new IllegalArgumentException("len parameter cannot be negative.");
		}
		if (str == null) {
			return null;
		}
		if (str.length() <= len) {
			return str;
		}
		return str.substring(0, len);
	}


	/**
	 * <p>Gets the rightmost <code>len</code> characters of a String.</p>
	 * <pre>
	 * StringUtils.right("abc", 0)   = ""
	 * StringUtils.right("abc", 2)   = "bc"
	 * StringUtils.right("abc", 4)   = "abc"
	 * </pre>
	 *
	 * @param str the String to get the leftmost characters from, may be null
	 * @param len the length of the required String, must greater than or equal to zero.
	 * @return the leftmost characters, <code>null</code> if null String input
	 */
	public static String right(String str, int len) {
		if (len < 0) {
			throw new IllegalArgumentException("len parameter cannot be negative.");
		}
		if (str == null) {
			return null;
		}
		if (str.length() <= len) {
			return str;
		}
		return str.substring(str.length() - len);
	}


	/**
	 * Returns "false" if the Boolean is null or false, otherwise returns true.
	 */
	public static String toString(Boolean bool) {
		if (bool == null) {
			return "false";
		}

		return bool.toString();
	}


	/**
	 * Calls str.trim() method after first converting "non-breaking space" characters to regular space which regular trim doesn't appear to handle
	 */
	public static String trim(String str) {
		if (str == null) {
			return str;
		}
		str = str.replace('\u00A0', ' ');
		return str.trim();
	}


	/**
	 * Replaces all whitespace in a string
	 */
	public static String trimAll(String str) {
		if (str == null) {
			return str;
		}
		str = str.replace('\u00A0', ' ');
		return str.replaceAll("\\s+", "");
	}


	/**
	 * When passed an intended variable name string this method will remove all invalid characters and replace them with an underscore
	 */
	public static String formatFreemarkerVariable(String variableName) {
		if (!isEmpty(variableName)) {
			return variableName.replaceAll(FREEMARKER_VARIABLE_REGEX, FREEMARKER_VARIABLE_REPLACEMENT);
		}
		return "";
	}


	/**
	 * Replaces each substring of the source string matching the literal target sequence with the specified literal replacement sequence.
	 * <p>
	 * This utility uses the enhancement to {@link String#replace(CharSequence, CharSequence)} for OpenJDK Java 9, which uses iterative {@link String#indexOf(String)}
	 * to replace the target rather than compiling the target as a literal regular expression and replacing all occurrences.
	 */
	public static String replace(String source, CharSequence target, CharSequence replacement) {
		String targetString = target.toString();
		String replacementString = replacement.toString();
		int j = source.indexOf(targetString);
		if (j < 0) {
			return source;
		}
		int targetLength = targetString.length();
		int targetLengthOrOne = Math.max(targetLength, 1);
		final char[] sourceChars = source.toCharArray();
		final char[] replacementChars = replacementString.toCharArray();
		int newLengthHint = sourceChars.length - targetLength + replacementChars.length;
		if (newLengthHint < 0) {
			throw new OutOfMemoryError();
		}
		StringBuilder sb = new StringBuilder(newLengthHint);
		int i = 0;
		do {
			sb.append(sourceChars, i, j - i)
					.append(replacementChars);
			i = j + targetLength;
		}
		while (j < sourceChars.length && (j = source.indexOf(targetString, j + targetLengthOrOne)) > 0);
		return sb.append(sourceChars, i, sourceChars.length - i).toString();
	}


	/**
	 * Splits the given string on the given regex pattern. If the provided string is empty or <code>null</code> then an empty array will be returned.
	 *
	 * @see String#split(String)
	 */
	public static String[] split(String str, String pattern) {
		if (isEmpty(str)) {
			return new String[0];
		}
		return str.split(pattern);
	}


	/**
	 * Splits the given string using the given delimiter. If the provided string is empty or <code>null</code> then an empty list will be returned.
	 * <p>
	 * This differs from {@link #split(String, String)} in that the <code>delimiter</code> is not interpreted as a regular expression.
	 */
	public static List<String> splitOnDelimiter(String str, String delimiter) {
		// Guard-clause: Trivial case
		if (isEmpty(str)) {
			return Collections.emptyList();
		}
		List<String> stringList = new ArrayList<>();
		int delimiterLength = delimiter.length();
		int lastTokenIndex = -1 * delimiterLength;
		int nextTokenIndex;
		while ((nextTokenIndex = str.indexOf(delimiter, lastTokenIndex + delimiterLength)) > -1) {
			stringList.add(str.substring(lastTokenIndex + delimiterLength, nextTokenIndex));
			lastTokenIndex = nextTokenIndex;
		}
		// Add final remaining element
		if (lastTokenIndex < str.length()) {
			stringList.add(str.substring(lastTokenIndex + delimiterLength));
		}
		return stringList;
	}


	/**
	 * Generates a {@link Pattern} for the given <a href="https://en.wikipedia.org/wiki/Glob_(programming)">glob</a> string.
	 * <p>
	 * This {@link Pattern} will apply the wildcards <code>"*"</code> and <code>"?"</code>, where <code>"*"</code> matches any number of characters and <code>"?"</code> matches any
	 * single character.
	 * <p>
	 * This uses the strategy provided here: <a href="https://stackoverflow.com/a/3619098/1941654">Is there an equivalent of java.util.regex for “glob” type patterns?</a>
	 *
	 * @param globPatternStr the glob pattern from which to generate a {@link Pattern}
	 * @return the {@link Pattern} generated from the glob string, or <code>null</code> if no glob string was given
	 */
	public static Pattern getGlobPattern(String globPatternStr) {
		// Guard-clause: A null value indicates that no pattern exists
		if (globPatternStr == null) {
			return null;
		}

		String patternStr = Pattern.quote(globPatternStr);
		patternStr = replace(patternStr, "*", "\\E.*\\Q");
		patternStr = replace(patternStr, "?", "\\E.\\Q");
		return Pattern.compile("^" + patternStr + "$", Pattern.CASE_INSENSITIVE | Pattern.DOTALL);
	}
}
