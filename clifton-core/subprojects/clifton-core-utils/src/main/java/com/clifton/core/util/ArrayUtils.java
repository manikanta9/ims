package com.clifton.core.util;


import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Stream;


/**
 * The <code>ArrayUtils</code> contains utility methods for dealing with Arrays
 *
 * @author manderson
 */
public class ArrayUtils {

	private ArrayUtils() {
		// Private constructor
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a stream for the provided array if it is not null. Returns {@link Stream#empty()} if the array is null. This method can be used to obtain a
	 * stream without having to check for null.
	 * <p>
	 * <b>Note:</b> This does not validate that the first element of the array is non-null. If a variable with a single {@code null} value is passed to this
	 * method, a single-element stream will be returned containing the given {@code null} value.
	 */
	@SafeVarargs
	public static <T> Stream<T> getStream(T... arr) {
		if (arr != null) {
			return Arrays.stream(arr);
		}
		return Stream.empty();
	}


	/**
	 * Serializes the specified parameters to a comma delimited string.
	 * Uses Object.toString()
	 */
	@SafeVarargs
	public static <T> String toString(T... arr) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		StringBuilder result = new StringBuilder();
		for (Object o : arr) {
			if (result.length() > 0) {
				result.append(", ");
			}
			result.append(o);
		}
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static Integer[] toIntegerArray(Object... arr) {
		if (arr == null || arr.length == 0) {
			return null;
		}
		Integer[] result = new Integer[arr.length];
		for (int i = 0; i < arr.length; i++) {
			result[i] = (Integer) arr[i];
		}
		return result;
	}


	/**
	 * Returns the array as a non-null object. If the given array is non-null, then the array will be returned. If the array is <code>null</code>, then an empty array of the
	 * specified type will be instantiated and returned.
	 *
	 * @param elementType the type for the generated array, if generation is necessary
	 * @param <T>         the array type
	 * @param array       the non-null array
	 */
	public static <T> T[] asNonNullArray(Class<T> elementType, T[] array) {
		return array != null
				? array
				: createArrayOfTypeWithLength(elementType, 0);
	}


	/**
	 * Generates a new array from the given arguments. This functions identically to literal array instantiations, such as <code>new T[] {&lt;arguments...&gt;}</code>.
	 * <p>
	 * If <tt>elements</tt> is {@code null} or an empty list then an empty array will be returned.
	 * <p>
	 * <b>Note:</b>
	 * <p>
	 * Due to <a href="https://docs.oracle.com/javase/tutorial/java/generics/erasure.html">type erasure</a>, if the argument type is not known at compile-time then the returned
	 * result will be of the bounded type (or {@code Object[]} if unbounded). If type erasure applies (i.e., a generic-type argument is passed to this method) then {@link
	 * #createArrayOfType(Class, Object[])}} should be used instead.
	 * <p>
	 * <b>Example of invalid usage:</b>
	 * <pre><code>
	 * private void myMethod() {
	 *     // A ClassCastException will be thrown here. Generics imply that Integer[] will be returned, but type-erasure causes Object[] to be returned intsead.
	 *     // Exception: "java.lang.ClassCastException: [Ljava.lang.Object; cannot be cast to [Ljava.lang.Integer;"
	 *     Integer[] myIntArray = createArrayProxy(1);
	 * }
	 *
	 * private &lt;T&gt; T[] createArrayProxy(T element) {
	 *     ArrayUtils.createArray(element);
	 * }
	 * </code></pre>
	 *
	 * @param elements the elements which shall constitute the generated array
	 * @param <T>      the array element type
	 * @return the generated array
	 * @see #createArrayOfType(Class, Object[])
	 */
	@SafeVarargs
	public static <T> T[] createArray(T... elements) {
		return elements;
	}


	/**
	 * Generates a new array from the given arguments. This functions identically to literal array instantiations, such as <code>new T[] {&lt;arguments...&gt;}</code>.
	 * <p>
	 * If <tt>elements</tt> is {@code null} or an empty list then an empty array will be returned.
	 *
	 * @param elementType the element type for the generated array
	 * @param elements    the elements which shall constitute the generated array
	 * @param <T>         the array element type
	 * @return the generated array
	 * @throws ArrayStoreException if any of the provided elements are not of a runtime type that can be stored in the generated array
	 */
	@SafeVarargs
	public static <T> T[] createArrayOfType(Class<T> elementType, T... elements) {
		final T[] result;
		if (isEmpty(elements)) {
			// Trivial case: Empty result
			return createArrayOfTypeWithLength(elementType, 0);
		}
		result = createArrayOfTypeWithLength(elementType, elements.length);
		System.arraycopy(elements, 0, result, 0, elements.length);
		return result;
	}


	/**
	 * Generates a new array of the given type and length.
	 *
	 * @param elementType the element type for the generated array
	 * @param <T>         the array element type
	 * @return the generated array
	 */
	public static <T> T[] createArrayOfTypeWithLength(Class<T> elementType, int length) {
		@SuppressWarnings("unchecked")
		T[] newArray = (T[]) Array.newInstance(elementType, length);
		return newArray;
	}


	/**
	 * Generates a new array of the same runtime type as {@code sourceArray} and populates it with the given {@code elements}.
	 * <p>
	 * If {@code sourceArray} is {@code null} or an empty list then an empty array will be returned.
	 *
	 * @param sourceArray the source array from which to determine the target array type
	 * @param elements    the elements which shall constitute the generated array
	 * @param <T>         the array element type
	 * @return the generated array
	 * @throws ArrayStoreException if any of the provided elements are not of a runtime type that can be stored in the generated array
	 */
	@SafeVarargs
	public static <T> T[] createArrayFromSourceType(T[] sourceArray, T... elements) {
		if (sourceArray == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		Class<T> elementType = (Class<T>) sourceArray.getClass().getComponentType();
		return createArrayOfType(elementType, elements);
	}


	/**
	 * Generates a new array of the same runtime type as {@code sourceArray} with the given length.
	 * <p>
	 * If {@code sourceArray} is {@code null} then an empty array will be returned.
	 *
	 * @param sourceArray the source array from which to determine the target array type
	 * @param <T>         the array element type
	 * @return the generated array
	 */
	public static <T> T[] createArrayFromSourceTypeWithLength(T[] sourceArray, int length) {
		if (sourceArray == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		Class<T> elementType = (Class<T>) sourceArray.getClass().getComponentType();
		return createArrayOfTypeWithLength(elementType, length);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method adds the Object to the end of the array and returns a new instance of the array
	 */
	public static <T> T[] add(T[] arr, T obj) {
		return add(arr, obj, arr != null ? arr.length : 0);
	}


	/**
	 * Generates a new array with the given object spliced into {@code arr} at index {@code index}.
	 */
	public static <T> T[] add(T[] arr, T obj, int index) {
		//noinspection DuplicatedCode : Duplicated for performance reasons
		if (index < 0 || (arr == null && index > 0) || (arr != null && index > arr.length)) {
			throw new IndexOutOfBoundsException(String.format("Index [%d] is out of bounds for array of size [%s].", index, arr != null ? arr.length : "null"));
		}
		if (obj == null) {
			if (arr == null) {
				return null;
			}
			return arr.clone();
		}
		/*
		 * Array type is not guaranteed to match the generic parameter. Subsequent element assignments may fail with ArrayStoreException errors due to Java array covariance, which
		 * restricts runtime assignments to types matching the array type but allows the array type to be implicitly assigned as an instance of a supertype.
		 */
		@SuppressWarnings("unchecked")
		Class<T> elementType = (Class<T>) obj.getClass();
		return addAll(arr, createArrayOfType(elementType, obj), index);
	}


	/**
	 * Generates a new array with all elements from {@code arr2} appended onto the end of {@code arr1}.
	 */
	public static <T> T[] addAll(T[] arr1, T[] arr2) {
		return addAll(arr1, arr2, arr1 != null ? arr1.length : 0);
	}


	/**
	 * Generates a new array with all elements from {@code arr2} spliced into {@code arr1} at index {@code index}.
	 */
	public static <T> T[] addAll(T[] arr1, T[] arr2, int index) {
		//noinspection DuplicatedCode : Duplicated for performance reasons
		if (index < 0 || (arr1 == null && index > 0) || (arr1 != null && index > arr1.length)) {
			throw new IndexOutOfBoundsException(String.format("Index [%d] is out of bounds for array of size [%s].", index, arr1 != null ? arr1.length : "null"));
		}
		if (arr1 == null) {
			if (arr2 == null) {
				return null;
			}
			return arr2.clone();
		}
		if (arr2 == null) {
			return arr1.clone();
		}

		T[] array = createArrayFromSourceTypeWithLength(arr1, arr1.length + arr2.length);
		System.arraycopy(arr1, 0, array, 0, index);
		System.arraycopy(arr2, 0, array, index, arr2.length);
		System.arraycopy(arr1, index, array, index + arr2.length, arr1.length - index);
		return array;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method combines all elements of all of the arrays and returns them in a new array.
	 */
	public static <T> T[] addAll(T[]... arraysToCopy) {
		if (isEmpty(arraysToCopy)) {
			return null;
		}
		boolean foudOneNonNullArray = false; // needed to match logic in addAll(T[] arr1, T[] arr2) for returning cloned array
		Class<?> componentType = null;
		int totalElementsToCopy = 0;
		for (int i = 0; i < getLength(arraysToCopy); i++) {
			T[] currentArray = arraysToCopy[i];
			if (currentArray != null) {
				foudOneNonNullArray = true;
				componentType = currentArray.getClass().getComponentType();
			}
			int currentArrayLength = getLength(currentArray);
			if (currentArrayLength > 0) {
				totalElementsToCopy += currentArrayLength;
			}
		}
		if (totalElementsToCopy < 1) {
			if (foudOneNonNullArray) {
				return (T[]) Array.newInstance(componentType, 0);
			}
			return null;
		}
		@SuppressWarnings("unchecked")
		T[] resultArray = (T[]) Array.newInstance(componentType, totalElementsToCopy);
		int copiedElementCount = 0;
		for (int i = 0; i < getLength(arraysToCopy); i++) {
			T[] currentArray = arraysToCopy[i];
			int currentArrayLength = getLength(currentArray);
			if (currentArrayLength > 0) {
				System.arraycopy(currentArray, 0, resultArray, copiedElementCount, currentArrayLength);
			}
			copiedElementCount += currentArrayLength;
		}
		if (copiedElementCount != totalElementsToCopy) {
			throw new RuntimeException(String.format("Expected to copy %d elements but copied %d elements.", totalElementsToCopy, copiedElementCount));
		}

		return resultArray;
	}


	/**
	 * Creates and returns a new array such that its elements are present in both arrays.
	 * If one of argument arrays is null, return the other array;
	 */
	public static <T> T[] intersect(T[] arr1, T[] arr2) {
		if (arr1 == null) {
			if (arr2 == null) {
				return null;
			}
			return arr2.clone();
		}
		if (arr2 == null) {
			return arr1.clone();
		}

		List<T> resultList = new ArrayList<>();
		Set<T> set2 = new HashSet<>(Arrays.asList(arr2));
		for (T v1 : arr1) {
			if (set2.contains(v1)) {
				resultList.add(v1);
			}
		}

		int size = resultList.size();
		if (size == 0) {
			return null;
		}

		T[] result = createArrayFromSourceTypeWithLength(arr1, size);
		for (int i = 0; i < size; i++) {
			result[i] = resultList.get(i);
		}

		return result;
	}


	public static byte[] cloneArray(byte[] array) {
		if (array == null) {
			return null;
		}
		return array.clone();
	}


	public static int[] cloneArray(int[] array) {
		if (array == null) {
			return null;
		}
		return array.clone();
	}


	public static boolean[] cloneArray(boolean[] array) {
		if (array == null) {
			return null;
		}
		return array.clone();
	}


	public static <T> T[] cloneArray(T[] array) {
		if (array == null) {
			return null;
		}
		return array.clone();
	}


	/**
	 * Returns the index of the specified element in the specified array.
	 * Returns -1 if the element is not found.
	 */
	public static <T> int indexOf(T[] arr, T element) {
		if (arr != null) {
			for (int i = 0; i < arr.length; i++) {
				T o = arr[i];
				if ((o == null && element == null) || (o != null && o.equals(element))) {
					return i;
				}
			}
		}
		return -1;
	}


	/**
	 * Returns the index of the first element matching the given predicate in the specified array. Returns <tt>-1</tt> if no such element is found.
	 */
	public static <T> int indexOf(T[] arr, Predicate<T> matches) {
		if (arr != null) {
			for (int i = 0; i < arr.length; i++) {
				if (matches.test(arr[i])) {
					return i;
				}
			}
		}
		return -1;
	}


	/**
	 * Returns {@code true} if the element exists in the specified array.
	 *
	 * @param arr     the array to scan for the element
	 * @param element the element for which to scan
	 * @param <T>     the array and element type
	 * @return {@code true} if the element exists in the given array, or {@code false} otherwise
	 */
	public static <T> boolean contains(T[] arr, T element) {
		return indexOf(arr, element) > -1;
	}


	/**
	 * Determines if the array is {@code null} or empty.
	 *
	 * @param arr the array to check
	 * @return {@code true} if the array is {@code null} or empty, or {@code false} otherwise
	 */
	public static boolean isEmpty(Object[] arr) {
		return arr == null || arr.length == 0;
	}


	/**
	 * @see #isEmpty(Object[])
	 */
	public static boolean isEmpty(int[] arr) {
		return arr == null || arr.length == 0;
	}


	/**
	 * Returns true if the specified array has a single element.
	 */
	public static boolean isSingleElement(Object[] array) {
		if (array == null) {
			return false;
		}
		return (Array.getLength(array) == 1);
	}


	/**
	 * Returns the element of the array at index <code>index</code>, or <code>null</code> if the given index is out of bounds for the array.
	 *
	 * @param arr   the array to access
	 * @param index the indexed item in the array to retrieve
	 * @param <T>   the type of the array
	 * @return the element of the array at index <code>index</code>, or <code>null</code> if <code>index</code> is out of bounds for the array
	 */
	public static <T> T getAtIndex(T[] arr, int index) {
		if (arr == null || index < 0 || index > arr.length - 1) {
			return null;
		}
		return arr[index];
	}


	/**
	 * Gets the length of the specified array, or {@code 0} if the array is {@code null}.
	 *
	 * @param arr the array to evaluate
	 * @return the length of the array
	 */
	public static int getLength(Object[] arr) {
		return arr == null ? 0 : arr.length;
	}


	/**
	 * Transposes an array.
	 *
	 * @return The transposed array, null if the array cannot be transposed.
	 */
	public static Object[][] transpose(Object[][] array) {
		int r = array.length;
		if (r == 0) {
			return null;
		}
		int c = array[r - 1].length;
		Object[][] t = new Object[c][r];
		for (int i = 0; i < r; ++i) {
			for (int j = 0; j < c; ++j) {
				t[j][i] = array[i][j];
			}
		}
		return t;
	}


	/**
	 * Coalesces the provided arguments and returns an array representation of the resulting non-{@code null} value.
	 * <p>
	 * If the array is non-{@code null}, then the array itself is returned. If the individual element is non-{@code null}, then it is wrapped in an array and
	 * returned. If all elements are {@code null}, then an empty array is returned.
	 * <p>
	 * This method may be used in cases where separate fields are provided for single values and for multiple values. When such values are passed as arguments,
	 * this method will consistently return an array containing the results.
	 *
	 * @param array             the array representation, or {@code null} if no array is given
	 * @param individualElement the individual element representation, or {@code null} if no individual element is given
	 * @param elementType       the element type for the resulting array
	 * @param <T>               the supertype for all elements
	 * @return the array representation of the resulting value after coalescing
	 */
	public static <T> T[] wrapCoalescing(T[] array, T individualElement, Class<T> elementType) {
		final T[] result;
		if (array != null) {
			result = array;
		}
		else if (individualElement != null) {
			result = createArrayOfType(elementType, individualElement);
		}
		else {
			result = createArrayOfTypeWithLength(elementType, 0);
		}
		return result;
	}


	/**
	 * Returns {@code true} if the array contains any element which matches the given predicate.
	 */
	public static <T> boolean anyMatch(T[] array, Predicate<T> matcher) {
		return array != null && Arrays.stream(array).anyMatch(matcher);
	}


	/**
	 * Returns {@code true} if all elements within the array match the given predicate or when there are no elements in the array.
	 */
	public static <T> boolean allMatch(T[] array, Predicate<T> matcher) {
		return array == null || Arrays.stream(array).allMatch(matcher);
	}
}
