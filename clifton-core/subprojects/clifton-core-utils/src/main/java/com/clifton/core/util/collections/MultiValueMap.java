package com.clifton.core.util.collections;


import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>MultiValueMap</code> is a variation of {@link Map} that allows a key to map to multiple values.
 *
 * @param <K> the type of keys maintained
 * @param <V> the type of mapped values
 * @author jgommels
 */
public interface MultiValueMap<K, V> {

	/**
	 * @param key the key to search for in the map
	 * @return the list of values that the key maps to. Changes to this list will not affect the underlying collection of the <code>MultiValueMap</code>. If
	 * there are no values to that this key maps to, then <code>null</code> is returned.
	 */
	List<V> get(K key);


	Set<K> keySet();


	Set<Map.Entry<K, Collection<V>>> entrySet();


	List<V> values();


	/**
	 * Clones and returns the underlying Map.
	 */
	Map<K, Collection<V>> cloneMap();


	/**
	 * @return the number of keys in the multi-value map
	 */
	int keyCount();


	/**
	 * @return the number of values in the multi-value map
	 */
	int valueCount();


	/**
	 * @return whether the multi-value map is empty
	 */
	boolean isEmpty();


	/**
	 * Adds the value to the collection of values that this key maps to. Duplicates may or may not be allowed, depending on the configuration of the underlying
	 * instance.
	 *
	 * @param key   the key to map to <code>value</code>
	 * @param value the value that <code>key</code> maps to
	 */
	void put(K key, V value);


	/**
	 * Adds the list of values to the collection of values that this key maps to. Duplicates may or may not be allowed, depending on the configuration of the
	 * underlying instance.
	 *
	 * @param key    the key to map to <code>value</code>
	 * @param values the collection of values that <code>key</code> maps to
	 */
	void putAll(K key, Collection<? extends V> values);


	/**
	 * Adds all of the elements in the specified map to this map. Duplicates may or may not be allowed, depending on the configuration of the underlying
	 * instance. The behavior of this operation is undefined if the specified map is modified while the operation is in progress. (This implies that the
	 * behavior of this call is undefined if the specified map is this map, and this map is nonempty.)
	 *
	 * @param otherMap the map containing the elements to be added to this map
	 */
	void putAll(MultiValueMap<? extends K, ? extends V> otherMap);


	/**
	 * Removes the key-value pair from the map.
	 *
	 * @param key   key of entry to remove from map
	 * @param value value of entry to remove from map
	 * @return whether the value was removed
	 */
	boolean remove(K key, V value);


	/**
	 * Removes all values associated with given key.
	 *
	 * @param key key of entries to remove from map
	 * @return list of removed values
	 */
	List<V> removeAll(K key);


	/**
	 * Replaces <code>oldValue</code> from the list of values that <code>key</code> maps to with <code>newValue</code>.
	 *
	 * @param key
	 * @param oldValue
	 * @param newValue
	 * @return whether <code>oldValue</code> was actually present
	 */
	boolean replace(K key, V oldValue, V newValue);


	/**
	 * Replaces the (<code>oldKey</code>, <code>oldValue</code>) pair with the (<code>newKey</code>, <code>newValue</code>) pair. Note that <code>oldKey</code>
	 * will not be removed if it still maps to other values.
	 *
	 * @param oldKey   the key that maps to <code>oldValue</code>
	 * @param oldValue the value to be replaced
	 * @param newKey   the key that maps to <code>newValue</code>
	 * @param newValue the value to replace <code>oldValue</code>
	 * @return whether or not <code>oldValue</code> was actually present
	 */
	boolean replace(K oldKey, V oldValue, K newKey, V newValue);
}
