package com.clifton.core.util.dataaccess;


/**
 * The <code>PagingList</code> interface defines methods for working with pagination.
 *
 * @author vgomelsky
 */
public interface PagingList {

	/**
	 * Returns the index of the first element index
	 *
	 * @return Returns the first element index
	 */
	public int getFirstElementIndex();


	/**
	 * Returns the total number of elements (not just current page)
	 *
	 * @return Returns the total size
	 */
	public int getTotalElementCount();


	/**
	 * Returns the number of elements per page
	 *
	 * @return Returns the page size.
	 */
	public int getPageSize();


	/**
	 * Returns page number for the current page
	 *
	 * @return Returns the current pageNumber
	 */
	public int getCurrentPageNumber();


	/**
	 * Returns the total number of pages
	 *
	 * @return Returns the total page count
	 */
	public int getTotalPageCount();


	/**
	 * Returns number of elements in current page
	 *
	 * @return Returns element count for current page
	 */
	public int getCurrentPageElementCount();


	/**
	 * Returns true if the currentPage is the firstPage
	 *
	 * @return Returns true if the currentPage is the firstPage
	 */
	public boolean isFirstPage();


	/**
	 * Returns true if the currentPage is the lastPage
	 *
	 * @return Returns true if the currentPage is the lastPage
	 */
	public boolean isLastPage();
}
