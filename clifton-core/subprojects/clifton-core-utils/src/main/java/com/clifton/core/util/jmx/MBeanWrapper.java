package com.clifton.core.util.jmx;

import javax.management.MBeanAttributeInfo;
import javax.management.MBeanServer;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The {@link MBeanWrapper} is a encapsulating type for registered <i>MBean</i> entities and their properties and attributes.
 *
 * @author MikeH
 * @see JmxUtils
 * @see MBeanServer
 */
public class MBeanWrapper {

	/**
	 * The {@link String} representation of the {@link ObjectName} under which the represented MBean is registered.
	 */
	private final String objectName;
	/**
	 * The domain name under which the represented MBean is registered.
	 */
	private final String domain;
	/**
	 * The map of property key-value pairs under which the represented MBean is registered, such as {@code type=RequestProcessor} or {@code name=HttpRequest1}.
	 */
	private final Map<String, String> keyValueMap;
	/**
	 * The map of {@link AttributeWrapper} entities for the represented MBean. These attributes are the managed properties for the MBean, such as backing resource properties and
	 * statistics.
	 */
	private final Map<String, AttributeWrapper> attributeMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MBeanWrapper(ObjectInstance objectInstance, Map<MBeanAttributeInfo, Object> attributeInfoToValueMap) {
		this(objectInstance.getObjectName(), attributeInfoToValueMap);
	}


	public MBeanWrapper(ObjectName objectName, Map<MBeanAttributeInfo, Object> attributeInfoToValueMap) {
		this.objectName = objectName.getCanonicalName();
		this.domain = objectName.getDomain();
		this.keyValueMap = objectName.getKeyPropertyList();
		this.attributeMap = attributeInfoToValueMap != null
				? attributeInfoToValueMap.entrySet().stream().collect(Collectors.toMap(entry -> entry.getKey().getName(), entry -> new AttributeWrapper(entry.getKey().getDescription(), entry.getValue())))
				: null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// This class is public in order to allow serialization
	public static class AttributeWrapper {

		private final String description;
		private final Object value;


		public AttributeWrapper(String description, Object value) {
			this.description = description;
			this.value = value;
		}


		public String getDescription() {
			return this.description;
		}


		public Object getValue() {
			return this.value;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getObjectName() {
		return this.objectName;
	}


	public String getDomain() {
		return this.domain;
	}


	public Map<String, String> getKeyValueMap() {
		return this.keyValueMap;
	}


	public Map<String, AttributeWrapper> getAttributeMap() {
		return this.attributeMap;
	}
}
