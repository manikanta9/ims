package com.clifton.core.util;


import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Set;


/**
 * The {@link BooleanUtils} class contains helper utility methods for condition testing.
 *
 * @author NickK
 */
public class BooleanUtils {

	/**
	 * The set of values which shall be considered truthy.
	 *
	 * @see #isTrue(Object)
	 */
	private static final Set<Object> TRUE_VALUES = CollectionUtils.newUnmodifiableSet(
			Boolean.TRUE,
			"true",
			"yes",
			"1",
			1.0F,
			1.0D,
			(short) 1,
			1,
			1L,
			BigDecimal.ONE,
			BigInteger.ONE
	);


	/**
	 * Determines whether or not the boolean object is non-null and is true.
	 *
	 * @param bool the value to test
	 * @return {@code true} if the boolean value is true, or {@code false} otherwise
	 */
	public static boolean isTrue(Boolean bool) {
		return Boolean.TRUE.equals(bool);
	}


	/**
	 * Determines whether or not the boolean object is non-null and is false.
	 *
	 * @param bool the value to test
	 * @return {@code true} if the boolean value is false, or {@code false} otherwise
	 */
	public static boolean isFalse(Boolean bool) {
		return Boolean.FALSE.equals(bool);
	}


	/**
	 * Determines whether the given object is a truthy value. Objects are considered truthy if they match any value in {@link #TRUE_VALUES}.
	 * <p>
	 * Strings are <b>not</b> compared case-sensitively.
	 *
	 * @param obj the object to test
	 * @return {@code true} if the given object is truthy, or {@code false} otherwise
	 * @see #isTrueStrict(Object)
	 */
	public static boolean isTrue(Object obj) {
		return obj != null && TRUE_VALUES.contains(obj instanceof String ? ((String) obj).toLowerCase() : obj);
	}


	/**
	 * Determines whether the given object is a truthy value. Objects are considered truthy if they match any value in {@link #TRUE_VALUES}.
	 * <p>
	 * Strings are compared case-sensitively.
	 *
	 * @param obj the object to test
	 * @return {@code true} if the given object is truthy, or {@code false} otherwise
	 * @see #isTrue(Object)
	 */
	public static boolean isTrueStrict(Object obj) {
		return obj != null && TRUE_VALUES.contains(obj);
	}


	/**
	 * Returns if any argument is true
	 */
	public static boolean anyTrue(Boolean... args) {
		if (args != null && args.length != 0) {
			for (Boolean arg : args) {
				if (isTrue(arg)) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Returns if any argument is true
	 */
	public static boolean anyFalse(Boolean... args) {
		if (args != null && args.length != 0) {
			for (Boolean arg : args) {
				if (isFalse(arg)) {
					return true;
				}
			}
		}
		return false;
	}


	/**
	 * Returns if any argument is true
	 */
	public static boolean anyTrue(Object... args) {
		if (args != null && args.length != 0) {
			for (Object arg : args) {
				if (isTrue(arg)) {
					return true;
				}
			}
		}
		return false;
	}
}
