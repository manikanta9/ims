package com.clifton.core.util;


import com.clifton.core.util.compare.CompareUtils;
import org.jetbrains.annotations.Contract;

import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;


/**
 * The {@link AssertUtils} class should be used to make assertions throughout the code to catch potentially invalid
 * states as soon as possible.
 * <p/>
 * Examples:
 * <pre>
 * <code>AssertUtils.assertNotNull(column, "The column cannot be null %1s.%2s", tableName, columnName);</code>
 * <code>AssertUtils.assertNotNull(column, () -> "The column cannot be null " + tableName + "." + columnName);</code>
 *
 * @author vgomelsky
 */
public class AssertUtils {

	private AssertUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Throws a {@link RuntimeException} with the specified message on assertion failure.
	 *
	 * @param messageSupplier the supplier for the exception message
	 * @throws RuntimeException always
	 */
	public static void fail(Supplier<String> messageSupplier) {
		throw new RuntimeException(messageSupplier.get());
	}


	/**
	 * Throws a {@link RuntimeException} with the specified message on assertion failure.
	 * <p>
	 * If any message arguments are provided, the <tt>messageFormat</tt> string will be used as a {@link String#format(String, Object...) format string}.
	 * Otherwise, it will be treated as a literal message.
	 *
	 * @param messageFormat a {@link String#format(String, Object...) format string}, or the exact message string if no format string arguments are provided
	 * @param messageArgs   the format string arguments
	 * @throws RuntimeException always
	 * @implNote If no message arguments are given, then <tt>messageFormat</tt> is used as the literal message rather than being interpreted with {@link
	 * String#format(String, Object...)}. This is because many pre-existing calls have a single argument for the message and have not yet been adapted to use a
	 * string parameter when the message may contain special characters. For variable message input, the standard adaptation would be the following:
	 * <pre>AssertUtils.fail(msg); -> AssertUtils.fail("%s", msg);</pre>
	 * @see #fail(Supplier)
	 */
	public static void fail(String messageFormat, Object... messageArgs) {
		fail(() -> ArrayUtils.isEmpty(messageArgs) ? messageFormat : String.format(messageFormat, messageArgs));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("false, _ -> fail")
	public static void assertTrue(boolean condition, Supplier<String> messageSupplier) {
		if (!condition) {
			fail(messageSupplier);
		}
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("false, _, _ -> fail")
	public static void assertTrue(boolean condition, String messageFormat, Object... messageArgs) {
		if (!condition) {
			fail(messageFormat, messageArgs);
		}
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("true, _ -> fail")
	public static void assertFalse(boolean condition, Supplier<String> messageSupplier) {
		if (condition) {
			fail(messageSupplier);
		}
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("true, _, _ -> fail")
	public static void assertFalse(boolean condition, String messageFormat, Object... messageArgs) {
		if (condition) {
			fail(messageFormat, messageArgs);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("!null, _ -> fail")
	public static void assertNull(Object arg, Supplier<String> messageSupplier) {
		assertTrue(arg == null, messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("!null, _, _, -> fail")
	public static void assertNull(Object arg, String messageFormat, Object... messageArgs) {
		assertTrue(arg == null, messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static <T> T assertNotNull(T arg, Supplier<String> messageSupplier) {
		assertTrue(arg != null, messageSupplier);
		return arg;
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static <T> T assertNotNull(T arg, String messageFormat, Object... messageArgs) {
		assertTrue(arg != null, messageFormat, messageArgs);
		return arg;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("!null, null, _ -> fail; null, !null, _ -> fail")
	public static void assertEquals(Object object1, Object object2, Supplier<String> messageSupplier) {
		assertTrue(CompareUtils.isEqual(object1, object2), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("!null, null, _, _ -> fail; null, !null, _, _ -> fail")
	public static void assertEquals(Object object1, Object object2, String messageFormat, Object... messageArgs) {
		assertTrue(CompareUtils.isEqual(object1, object2), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("!null, null, _ -> fail; null, !null, _ -> fail")
	public static void assertEquals(List<?> list1, List<?> list2, Supplier<String> messageSupplier) {
		assertTrue(CompareUtils.isEqual(list1, list2), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("!null, null, _, _ -> fail; null, !null, _, _ -> fail")
	public static void assertEquals(List<?> list1, List<?> list2, String messageFormat, Object... messageArgs) {
		assertTrue(CompareUtils.isEqual(list1, list2), messageFormat, messageArgs);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, null, _ -> fail")
	public static void assertNotEquals(Object object1, Object object2, Supplier<String> messageSupplier) {
		assertTrue(!CompareUtils.isEqual(object1, object2), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, null, _, _ -> fail")
	public static void assertNotEquals(Object object1, Object object2, String messageFormat, Object... messageArgs) {
		assertTrue(!CompareUtils.isEqual(object1, object2), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, null, _ -> fail")
	public static void assertNotEquals(List<?> list1, List<?> list2, Supplier<String> messageSupplier) {
		assertTrue(!CompareUtils.isEqual(list1, list2), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, null, _, _ -> fail")
	public static void assertNotEquals(List<?> list1, List<?> list2, String messageFormat, Object... messageArgs) {
		assertTrue(!CompareUtils.isEqual(list1, list2), messageFormat, messageArgs);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	public static void assertEmpty(String str, Supplier<String> messageSupplier) {
		assertTrue(StringUtils.isEmpty(str), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	public static void assertEmpty(String str, String messageFormat, Object... messageArgs) {
		assertTrue(StringUtils.isEmpty(str), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	public static void assertEmpty(Collection<?> collection, Supplier<String> messageSupplier) {
		assertTrue(CollectionUtils.isEmpty(collection), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	public static void assertEmpty(Collection<?> collection, String messageFormat, Object... messageArgs) {
		assertTrue(CollectionUtils.isEmpty(collection), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	public static void assertEmpty(Map<?, ?> map, Supplier<String> messageSupplier) {
		assertTrue(CollectionUtils.isEmpty(map), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	public static void assertEmpty(Map<?, ?> map, String messageFormat, Object... messageArgs) {
		assertTrue(CollectionUtils.isEmpty(map), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	public static void assertEmpty(Object[] arr, Supplier<String> messageSupplier) {
		assertTrue(ArrayUtils.isEmpty(arr), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	public static void assertEmpty(Object[] arr, String messageFormat, Object... messageArgs) {
		assertTrue(ArrayUtils.isEmpty(arr), messageFormat, messageArgs);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(String str, Supplier<String> messageSupplier) {
		assertTrue(!StringUtils.isEmpty(str), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(String str, String messageFormat, Object... messageArgs) {
		assertTrue(!StringUtils.isEmpty(str), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(Collection<?> collection, Supplier<String> messageSupplier) {
		assertTrue(!CollectionUtils.isEmpty(collection), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(Collection<?> collection, String messageFormat, Object... messageArgs) {
		assertTrue(!CollectionUtils.isEmpty(collection), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(Map<?, ?> map, Supplier<String> messageSupplier) {
		assertTrue(!CollectionUtils.isEmpty(map), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(Map<?, ?> map, String messageFormat, Object... messageArgs) {
		assertTrue(!CollectionUtils.isEmpty(map), messageFormat, messageArgs);
	}


	/**
	 * @see #fail(Supplier)
	 */
	@Contract("null, _ -> fail")
	public static void assertNotEmpty(Object[] arr, Supplier<String> messageSupplier) {
		assertTrue(!ArrayUtils.isEmpty(arr), messageSupplier);
	}


	/**
	 * @see #fail(String, Object...)
	 */
	@Contract("null, _, _ -> fail")
	public static void assertNotEmpty(Object[] arr, String messageFormat, Object... messageArgs) {
		assertTrue(!ArrayUtils.isEmpty(arr), messageFormat, messageArgs);
	}
}
