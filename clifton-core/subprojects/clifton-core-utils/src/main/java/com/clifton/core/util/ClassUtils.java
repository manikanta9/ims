package com.clifton.core.util;


import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.validation.ValidationException;

import java.io.File;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.IdentityHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;


/**
 * The {@link ClassUtils} class contains helper methods for working with Class File(s).
 *
 * @author manderson
 */
public class ClassUtils {


	private static final Lazy<List<String>> CLASSPATH_ELEMENT_LIST_LAZY = new Lazy<>(ClassUtils::generateClasspathElementList);
	private static final Map<Class<?>, Class<?>> PRIMITIVE_TO_WRAPPER_MAP = new IdentityHashMap<>();
	private static final Map<Class<?>, Class<?>> WRAPPER_TO_PRIMITIVE_MAP = new IdentityHashMap<>();


	static {
		PRIMITIVE_TO_WRAPPER_MAP.put(Short.TYPE, Short.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Integer.TYPE, Integer.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Long.TYPE, Long.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Float.TYPE, Float.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Double.TYPE, Double.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Boolean.TYPE, Boolean.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Character.TYPE, Character.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Byte.TYPE, Byte.class);
		PRIMITIVE_TO_WRAPPER_MAP.put(Void.TYPE, Void.class);

		for (Map.Entry<Class<?>, Class<?>> entry : PRIMITIVE_TO_WRAPPER_MAP.entrySet()) {
			WRAPPER_TO_PRIMITIVE_MAP.put(entry.getValue(), entry.getKey());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Simply wraps newInstance method to throw back a runTimeException
	 */
	public static <T> T newInstance(Class<T> clazz) {
		try {
			return clazz.newInstance();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	/**
	 * Returns a list of classes that matches the given Class<T> type
	 * i.e. is the same or a subclass, or implements Class<T>
	 */
	public static <T> List<Class<T>> getClassFiles(String startPath, Class<T> type) {
		List<Class<T>> matchList = new ArrayList<>();
		appendClassFile(matchList, new File(startPath), type);
		return matchList;
	}


	/**
	 * Returns a list of classes that matches the start path and
	 * class name suffix
	 */
	public static List<Class<?>> getClassFilesByName(String startPath, String suffix) {
		List<Class<?>> matchList = new ArrayList<>();
		appendClassFile(matchList, new File(startPath), suffix);
		return matchList;
	}


	/**
	 * If the file is a File, will validate if it is a Java Source File, get the corresponding
	 * Class and if that Class is a subclass of Class<T> type will then add it to the given list.
	 * <p/>
	 * If the file is a Directory, will recursively call this method to get all files within that directory.
	 */
	@SuppressWarnings("unchecked")
	private static <T> void appendClassFile(List<Class<T>> list, File thisFile, Class<T> type) {
		if (thisFile.isFile()) {
			Class<?> thisClass = getClassForFile(thisFile);
			if (thisClass != null && type.isAssignableFrom(thisClass)) {
				list.add((Class<T>) thisClass);
			}
			return;
		}
		if (thisFile.isDirectory()) {
			File[] fileList = thisFile.listFiles();
			if (fileList != null) {
				for (File file : fileList) {
					appendClassFile(list, file, type);
				}
			}
		}
	}


	/**
	 * If the file is a File, will validate if it is a Java Source File, get the corresponding
	 * Class and if that Class matches the name suffix then will then add it to the given list.
	 * <p/>
	 * If the file is a Directory, will recursively call this method to get all files within that directory.
	 */
	private static void appendClassFile(List<Class<?>> list, File thisFile, String suffix) {
		if (thisFile.isFile()) {
			if (thisFile.getName().endsWith(suffix)) {
				Class<?> thisClass = getClassForFile(thisFile);
				list.add(thisClass);
			}
			return;
		}
		if (thisFile.isDirectory()) {
			File[] fileList = thisFile.listFiles();
			if (fileList != null) {
				for (File file : fileList) {
					appendClassFile(list, file, suffix);
				}
			}
		}
	}


	/**
	 * Returns the Class of the given file.
	 * <p/>
	 * If the file is NOT a valid Java Source File, will return null
	 */
	public static Class<?> getClassForFile(File file) {
		if (!isFileJavaSourceFile(file)) {
			return null;
		}
		String className = file.getPath();
		// String class name at com and remove the file extension
		int start = className.indexOf("com" + File.separator);
		start = Math.max(start, 0);
		int end = className.lastIndexOf('.');
		end = end < 0 ? className.length() - 1 : end;
		try {
			className = className.substring(start, end);
		}
		catch (Exception e) {
			throw new RuntimeException("Error retrieving substring with start [" + start + "] and end [" + end + "] for string [" + className + "]", e);
		}
		className = className.replace(File.separator, ".");
		try {
			return Class.forName(className);
		}
		catch (Exception e) {
			throw new RuntimeException("Error getting Class for class name [" + className + "].", e);
		}
	}


	/**
	 * Returns the class of the given object, or <code>null</code> if the object is <code>null</code>.
	 */
	public static <T> Class<? extends T> getClassForObject(T object) {
		if (object == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		Class<? extends T> castClazz = (Class<? extends T>) object.getClass();
		return castClazz;
	}


	/**
	 * Returns the class name of the given object, or <code>null</code> if the object is <code>null</code>.
	 */
	public static String getClassNameForObject(Object object) {
		if (object == null) {
			return null;
		}
		return object.getClass().getName();
	}


	/**
	 * Returns true if <code>clazz</code> is assignable from any of the classes in <code>clazzArray</code>.
	 */
	public static boolean isAssignableFromAny(Class<?> clazz, Class<?>[] clazzArray) {
		for (Class<?> clazz2 : clazzArray) {
			if (clazz.isAssignableFrom(clazz2)) {
				return true;
			}
		}

		return false;
	}


	/**
	 * Returns the direct antecedent interface derived by stripping 'Impl' off the
	 * service name, by default throws and exception if the interface does not exist.
	 *
	 * @param clazz the ServiceImpl class from which the interface should be derived
	 */
	public static Class<?> getInterfaceForServiceImplClass(final Class<?> clazz) {
		return getInterfaceForServiceImplClass(clazz, true);
	}


	public static Class<?> getInterfaceForServiceImplClass(final Class<?> clazz, boolean throwExceptionIfMissing) {
		String interfaceName = clazz.getSimpleName().replace("Impl", "");
		for (Class<?> interfaceClass : CollectionUtils.getIterable(getAllInterfaces(clazz))) {
			if (interfaceClass.getSimpleName().equals(interfaceName)) {
				return interfaceClass;
			}
		}
		if (throwExceptionIfMissing) {
			throw new ValidationException("Failed to find interface named: " + interfaceName);
		}
		return null;
	}


	/**
	 * Returns a List of all interfaces that the specified class or any of its super classes implement.
	 * Also, includes all super-interfaces.
	 */
	public static List<Class<?>> getAllInterfaces(final Class<?> clazz) {
		if (clazz == null) {
			return null;
		}

		LinkedHashSet<Class<?>> allInterfaces = new LinkedHashSet<>();
		addClassInterfaces(clazz, allInterfaces);

		return new ArrayList<>(allInterfaces);
	}


	private static void addClassInterfaces(Class<?> clazz, HashSet<Class<?>> allInterfaces) {
		while (clazz != null) {
			Class<?>[] interfaces = clazz.getInterfaces();
			for (Class<?> thisInterface : interfaces) {
				if (allInterfaces.add(thisInterface)) {
					addClassInterfaces(thisInterface, allInterfaces);
				}
			}

			clazz = clazz.getSuperclass();
		}
	}


	/**
	 * Returns true if the given file has a .java extension,
	 * i.e. is a Java Source File
	 */
	private static boolean isFileJavaSourceFile(File file) {
		return file.getName().endsWith(".java");
	}


	/**
	 * Returns all {@link Field}s for a given class.  If deep is
	 * true it will also collect all fields declared in any superclasses.
	 * If includeFinalAndTransient is true will not filter out these fields.
	 */
	public static Field[] getClassFields(Class<?> clazz, boolean deep, boolean includeFinalAndTransient) {
		Field[] fields = clazz.getDeclaredFields();
		if (deep && clazz.getSuperclass() != null) {
			fields = ArrayUtils.addAll(fields, getClassFields(clazz.getSuperclass(), true, false));
		}

		if (!includeFinalAndTransient && fields != null && fields.length > 0) {
			List<Field> result = new ArrayList<>(fields.length);
			for (Field field : fields) {
				int modifiers = field.getModifiers();
				if (!Modifier.isFinal(modifiers) && !Modifier.isTransient(modifiers)) {
					result.add(field);
				}
			}
			return result.toArray(new Field[0]);
		}
		return fields;
	}


	/**
	 * Returns the Method of the specified class with the specified names and arguments.
	 * Returns null if the method is not found. Note. This is strict on the provided parameter types.
	 */
	public static Field getClassField(Class<?> clazz, String fieldName, boolean deep, boolean includeFinalAndTransient) {
		Field result = null;
		Field[] fields = getClassFields(clazz, deep, includeFinalAndTransient);
		for (Field field : fields) {
			if (field.getName().equals(fieldName)) {
				result = field;
				break;
			}
		}
		return result;
	}


	/**
	 * Gets the wrapper class if the given class is a primitive (e.g., <b>int.class</b>). If the given class is not a primitive, returns the given class.
	 *
	 * @param clazz the class for which to retrieve the wrapper
	 * @return the wrapper for the given primitive class, if the given class is primitive
	 */
	public static Class<?> primitiveToWrapperIfNecessary(Class<?> clazz) {
		return clazz.isPrimitive() ? PRIMITIVE_TO_WRAPPER_MAP.get(clazz) : clazz;
	}


	/**
	 * Gets the primitive class corresponding to the given <code>clazz</code>.
	 * <p>
	 * If the given class is already a primitive (e.g., <code>int.class</code>), then the same class is returned. If the given class does not have a primitive equivalent, then
	 * <code>null</code> is returned.
	 * <p>
	 * Example:
	 * <pre><code>
	 * ClassUtils.wrapperToPrimitive(Integer.class); // Result: int.class
	 * ClassUtils.wrapperToPrimitive(String.class); // Result: null
	 * </code></pre>
	 *
	 * @param clazz the class for which to retrieve the wrapper
	 * @return the corresponding primitive type for the given class, or <code>null</code> is no such type exists
	 */
	public static Class<?> wrapperToPrimitive(Class<?> clazz) {
		return !clazz.isPrimitive() ? WRAPPER_TO_PRIMITIVE_MAP.get(clazz) : clazz;
	}


	/**
	 * Returns true if the specified class has a no argument constructor.
	 * Returns false otherwise.
	 */
	public static boolean isDefaultConstructorPresent(Class<?> clazz) {
		try {
			clazz.getConstructor();
		}
		catch (NoSuchMethodException e) {
			return false;
		}
		return true;
	}


	/**
	 * Returns the list of all elements on the class path. This recursively scans the class path in order to include all elements which are referenced by <a
	 * href="https://docs.oracle.com/javase/tutorial/deployment/jar/downman.html">manifest class paths</a>.
	 */
	public static List<String> getClasspathElementList() {
		return CLASSPATH_ELEMENT_LIST_LAZY.get();
	}


	/**
	 * Generates the list of JAR files on the class path. This is supplementary to {@link #getClasspathElementList()}.
	 */
	private static List<String> generateClasspathElementList() {
		try {
			List<String> classpathElementList = StringUtils.splitOnDelimiter(System.getProperty("java.class.path"), System.getProperty("path.separator"));
			List<String> pathList = new ArrayList<>();
			Queue<String> pathQueue = new ArrayDeque<>(classpathElementList);
			while (!pathQueue.isEmpty()) {
				String path = pathQueue.poll();
				pathList.add(path);
				if ((path.toLowerCase().endsWith(".jar") || path.toLowerCase().endsWith(".war")) && new File(path).exists()) {
					JarFile jarFile = new JarFile(path);
					Manifest manifest = jarFile.getManifest();
					if (manifest != null) {
						String manifestClasspath = (String) manifest.getMainAttributes().get(Attributes.Name.CLASS_PATH);
						if (manifestClasspath != null) {
							String[] manifestClasspathElements = manifestClasspath.split(" +");
							pathQueue.addAll(Arrays.asList(manifestClasspathElements));
						}
					}
				}
			}
			return pathList;
		}
		catch (Exception e) {
			throw new RuntimeException("An error occured while reading the class path JARs.", e);
		}
	}
}
