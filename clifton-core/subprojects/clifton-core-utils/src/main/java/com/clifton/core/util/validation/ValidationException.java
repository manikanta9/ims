package com.clifton.core.util.validation;


/**
 * The {@link ValidationException} exception is a {@link RuntimeException} that identifies data validation
 * problems.  Validation exception are usually related to invalid inputs and do not need to be logged.
 *
 * @author vgomelsky
 */
public class ValidationException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with the specified detail message.
	 * The cause is not initialized, and may subsequently be initialized by a
	 * call to {@link #initCause}.
	 *
	 * @param message the detail message. The detail message is saved for
	 *                later retrieval by the {@link #getMessage()} method.
	 */
	public ValidationException(String message) {
		super(message);
	}


	/**
	 * Constructs a new runtime exception with the specified detail message and
	 * cause.  <p>Note that the detail message associated with
	 * <code>cause</code> is <i>not</i> automatically incorporated in
	 * this runtime exception's detail message.
	 *
	 * @param message the detail message (which is saved for later retrieval
	 *                by the {@link #getMessage()} method).
	 * @param cause   the cause (which is saved for later retrieval by the
	 *                {@link #getCause()} method).  (A <tt>null</tt> value is
	 *                permitted, and indicates that the cause is nonexistent or
	 *                unknown.)
	 * @since 1.4
	 */
	public ValidationException(String message, Throwable cause) {
		super(message, cause);
	}
}
