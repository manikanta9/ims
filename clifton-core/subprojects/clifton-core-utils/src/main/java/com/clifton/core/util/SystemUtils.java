package com.clifton.core.util;


import java.net.InetAddress;
import java.net.UnknownHostException;


public class SystemUtils {

	public static String getMachineName() {
		String machineName;
		try {
			machineName = InetAddress.getLocalHost().getHostName();
		}
		catch (UnknownHostException e) {
			machineName = "Unknown due to UnknownHostException";
		}
		return machineName;
	}
}
