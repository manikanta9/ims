package com.clifton.core.util.beans;


import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;

import java.beans.BeanInfo;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The {@link MethodUtils} class provides utility methods that simplify working with class methods.
 *
 * @author vgomelsky
 */
public class MethodUtils {


	/**
	 * Simply wraps method.invoke parameter to throw a runTimeException
	 */
	public static Object invoke(Method method, Object obj, Object... args) {
		try {
			return method.invoke(obj, args);
		}
		catch (Exception e) {
			throw new RuntimeException("Error invoking " + method.getName() + " on " + obj.getClass() + " using arguments " + ArrayUtils.toString(args), e);
		}
	}


	/**
	 * Executes the method of the given name on the given object. The method must be a unique declared method in the runtime type of the provided object.
	 */
	public static Object invoke(String methodName, Object obj, boolean makeAccessible, Object... args) {
		Method method = MethodUtils.getMethod(obj.getClass(), methodName, makeAccessible);
		return MethodUtils.invoke(method, obj, args);
	}


	/**
	 * Returns method with the specified name from the specified class. Returns null if the method doesn't exist.
	 * Throws {@link IllegalArgumentException} if more than one method matching the name is found.
	 *
	 * @param makeAccessible if true, sets returned method as accessible (in case it was private)
	 */
	public static Method getMethod(Class<?> clazz, String methodName, boolean makeAccessible) {
		Method result = null;
		Method[] methods = clazz.getDeclaredMethods();
		for (Method method : methods) {
			if (method.getName().equals(methodName)) {
				if (result == null) {
					result = method;
					if (makeAccessible) {
						result.setAccessible(true);
					}
				}
				else {
					throw new IllegalArgumentException(clazz + " has more than 1 method named '" + methodName + "':\n  " + result + "\n  " + method);
				}
			}
		}
		return result;
	}


	/**
	 * Checks interface classes of given class of method and returns the first method found that matches
	 */
	public static Method getInterfaceMethod(Method method) {
		Class<?> currentClass = method.getDeclaringClass();
		while (currentClass != null) {
			for (Class<?> classInterface : currentClass.getInterfaces()) {
				Method classMethod = getMatchingMethodInClass(classInterface, method);
				if (classMethod != null) {
					return classMethod;
				}
			}
			currentClass = currentClass.getSuperclass();
		}
		return null;
	}


	/**
	 * Returns true if the specified method has the same signature as a method on the specified class.
	 * Returns false otherwise.
	 */
	public static boolean isMethodInClass(Class<?> clazz, Method method) {
		return getMatchingMethodInClass(clazz, method) != null;
	}


	/**
	 * Returns method with matching number of parameters and parameter types
	 * if it exists in the specified class
	 */
	public static Method getMatchingMethodInClass(Class<?> clazz, Method method) {
		for (Method classMethod : clazz.getMethods()) {
			if (classMethod.getName().equals(method.getName()) && classMethod.getParameterTypes().length == method.getParameterTypes().length) {
				boolean found = true;
				for (int i = 0; i < method.getParameterTypes().length; i++) {
					if (!classMethod.getParameterTypes()[i].isAssignableFrom(method.getParameterTypes()[i])) {
						found = false;
					}
				}
				if (found) {
					return classMethod;
				}
			}
		}
		return null;
	}


	public static boolean isGetterOrSetter(BeanInfo beanInfo, Method method) {
		boolean isGetterOrSetter = false;
		// ignore getter/setter methods when both are present
		for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
			if (descriptor.getWriteMethod() != null && descriptor.getReadMethod() != null) {
				isGetterOrSetter = isGetterOrSetter || method.equals(descriptor.getReadMethod());
				isGetterOrSetter = isGetterOrSetter || method.equals(descriptor.getWriteMethod());
			}
		}
		return isGetterOrSetter;
	}


	/**
	 * Returns true if the specified method is declared on one of the interfaces that the specified clazz implements.
	 * Optionally limits interfaces to those that begin with the specified interfaceNamePrefix (if not null). For example, "com.clifton."
	 */
	public static boolean isMethodDeclaredOnOurInterface(Method method, Class<?> clazz, String interfaceNamePrefix) {
		for (Class<?> interfaceClass : CollectionUtils.getIterable(ClassUtils.getAllInterfaces(clazz))) {
			// ignore interfaces that are not ours
			if (interfaceNamePrefix == null || interfaceClass.getName().startsWith(interfaceNamePrefix)) {
				try {
					interfaceClass.getMethod(method.getName(), method.getParameterTypes());
					return true;
				}
				catch (NoSuchMethodException e) {
					// keep looking
				}
			}
		}
		return false;
	}


	/**
	 * Returns true if the specified method has at least one parameter that is an interface.
	 * Because interfaces cannot be instantiated, this could identify methods where we cannot use simple web binding.
	 * However; interfaces such as 'List' that contain additional generic information can be bound, so we must check for that.
	 */
	public static boolean isMethodWithInterfaceParameters(Method method) {
		Type[] genericParameterTypes = method.getGenericParameterTypes();
		Class<?>[] parameterTypes = method.getParameterTypes();
		for (int i = 0; i < parameterTypes.length; i++) {
			Class<?> parameterType = parameterTypes[i];
			if (parameterType.isInterface()) {
				if (genericParameterTypes != null && genericParameterTypes.length == parameterTypes.length) {
					return isParameterTypeWithInterfaceParameters(genericParameterTypes[i]);
				}
				else {
					return true;
				}
			}
		}
		return false;
	}


	public static boolean isParameterTypeWithInterfaceParameters(Type type) {
		boolean returnVal = true;
		if (type instanceof ParameterizedType) {
			Type[] types = ((ParameterizedType) type).getActualTypeArguments();
			for (Type t : types) {
				returnVal = isParameterTypeWithInterfaceParameters(t);
			}
		}
		else {
			try {
				Class<?> clazz = Class.forName(type.getTypeName());
				returnVal = clazz.isInterface();
			}
			catch (ClassNotFoundException e) {
				//Failed to instantiate likely because of interface, abstract, or generic parameter.
			}
		}
		return returnVal;
	}


	public static boolean isMethodUsingSameParamNames(Method method, Method[] methods) {
		boolean found = false;
		for (Method classMethod : methods) {
			if (classMethod.getName().equals(method.getName()) && classMethod.getParameterTypes().length == method.getParameterTypes().length) {
				found = true;
				String methodParamsNames = CollectionUtils.getStream(Arrays.asList(method.getParameters())).map(Parameter::getName).collect(Collectors.joining());
				String classMethodParamNames = CollectionUtils.getStream(Arrays.asList(classMethod.getParameters())).map(Parameter::getName).collect(Collectors.joining());
				if (methodParamsNames.equals(classMethodParamNames)) {
					return true;
				}
			}
		}
		//Possible to not be found when a class implements multiple interfaces.
		if (!found) {
			return true;
		}
		return false;
	}


	/**
	 * Gets the {@link Method} for the given class, or {@code null} if no such method is found.
	 * <p>
	 * This utility method will only include public methods in its search. Inherited methods for the provided class are included in the search.
	 */
	public static Method getMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes) {
		Method result = null;
		try {
			result = clazz.getMethod(methodName, parameterTypes);
		}
		catch (NoSuchMethodException e) {
			// ignore and return null when the method is not found
		}
		return result;
	}


	/**
	 * Gets the {@link Method} declared for the given class, or {@code null} if no such method is found.
	 * <p>
	 * This utility method will only include protected, private, and package-private methods in its search. Inherited methods will not be included in the search. Only methods which
	 * are declared in the given class may be returned.
	 */
	public static Method getDeclaredMethod(Class<?> clazz, String methodName, Class<?>... parameterTypes) {
		Method result = null;
		try {
			result = clazz.getDeclaredMethod(methodName, parameterTypes);
		}
		catch (NoSuchMethodException e) {
			// Method not found; return null
		}
		return result;
	}


	/**
	 * Returns a List of all declared methods that the specified class or any of its super classes have.
	 */
	public static List<Method> getAllDeclaredMethods(Class<?> clazz) {
		if (clazz == null) {
			return null;
		}

		LinkedHashSet<Method> result = new LinkedHashSet<>();
		while (clazz != null) {
			Method[] methods = clazz.getDeclaredMethods();
			Collections.addAll(result, methods);

			clazz = clazz.getSuperclass();
		}

		return new ArrayList<>(result);
	}


	/**
	 * Returns the Method of the specified class that matches the specified name and has parameters
	 * assignable to the specified parameter classes in the corresponding order.
	 */
	public static Method getMethodByAssignableToParameters(Class<?> clazz, String methodName, Class<?>... assignableToParameterTypes) {
		return getMethodByAssignableParameters(clazz, methodName, false, assignableToParameterTypes);
	}


	/**
	 * Returns the Method of the specified class that matches the specified name and has parameters
	 * assignable from the specified parameter classes in the corresponding order.
	 */
	public static Method getMethodByAssignableFromParameters(Class<?> clazz, String methodName, Class<?>... assignableFromParameterTypes) {
		return getMethodByAssignableParameters(clazz, methodName, true, assignableFromParameterTypes);
	}


	private static Method getMethodByAssignableParameters(Class<?> clazz, String methodName, boolean assignableFrom, Class<?>... assignableParameterTypes) {
		for (Method method : clazz.getMethods()) {
			if (method.getName().equals(methodName)) {
				Class<?>[] parameterTypes = method.getParameterTypes();
				if (parameterTypes.length == assignableParameterTypes.length) {
					boolean found = true;

					for (int i = 0; i < assignableParameterTypes.length; i++) {
						Class<?> paramType = assignableParameterTypes[i];
						if ((assignableFrom && !paramType.isAssignableFrom(parameterTypes[i])) || (!assignableFrom && !parameterTypes[i].isAssignableFrom(paramType))) {
							found = false;
							break;
						}
					}
					if (found) {
						return method;
					}
				}
			}
		}
		return null;
	}


	/**
	 * Returns String representation of parameters (data type and name) for the specified method.
	 */
	public static String getMethodParametersAsString(Method method, boolean shortTypes) {
		return CollectionUtils.getStream(Arrays.asList(method.getParameters())).map(p -> (shortTypes ? p.getType().getSimpleName() : p.getType().getName()) + " " + p.getName()).collect(Collectors.joining(", "));
	}
}
