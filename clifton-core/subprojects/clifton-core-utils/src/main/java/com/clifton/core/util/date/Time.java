package com.clifton.core.util.date;


import java.io.Serializable;
import java.util.Date;


/**
 * The <code>Time</code> represents a time of day.  The value is stored as the number of milliseconds since midnight.  When stored in the database,
 * the value will be stored as the integer representing the number of milliseconds since midnight see GenericTimeUserType.
 *
 * @author mwacker
 */
public final class Time implements Serializable, java.lang.Comparable<Time> {

	private int milliseconds = 0;
	private Date date;

	private static final Date EPOCH_DATE = DateUtils.clearTime(new Date(0));
	public static final String TIME_FORMAT_FULL = "HH:mm:ss";
	public static final String TIME_FORMAT_SHORT = "h:mm aaa";


	public Time() {
		// empty
	}


	/**
	 * Constructs a new time object by getting the number of milliseconds from midnight from the provided date.
	 */
	public Time(Date time) {
		this.date = DateUtils.setTime(EPOCH_DATE, time);
		this.milliseconds = DateUtils.getMillisecondsFromMidnight(this.date);
	}


	/**
	 * Constructs a new time object where the time of day is the number of provided milliseconds
	 *
	 * @param milliseconds
	 */
	public Time(int milliseconds) {
		this.date = DateUtils.addMilliseconds(EPOCH_DATE, milliseconds);
		this.milliseconds = DateUtils.getMillisecondsFromMidnight(this.date);
	}


	/**
	 * Tests if this time is before the specified time.
	 *
	 * @param when a time.
	 * @return <code>true</code> if and only if the instant of time
	 * represented by this <tt>Time</tt> object is strictly
	 * earlier than the instant represented by <tt>when</tt>;
	 * <code>false</code> otherwise.
	 * @throws NullPointerException if <code>when</code> is null.
	 */
	public boolean before(Time when) {
		if (when == null) {
			throw new IllegalArgumentException("The 'when' milliseconds cannot be null.");
		}
		return this.milliseconds < when.milliseconds;
	}


	/**
	 * Tests if this time is after the specified time.
	 *
	 * @param when a time.
	 * @return <code>true</code> if and only if the instant represented
	 * by this <tt>Time</tt> object is strictly later than the
	 * instant represented by <tt>when</tt>;
	 * <code>false</code> otherwise.
	 * @throws NullPointerException if <code>when</code> is null.
	 */
	public boolean after(Time when) {
		if (when == null) {
			throw new IllegalArgumentException("The 'when' milliseconds cannot be null.");
		}
		return this.milliseconds > when.milliseconds;
	}


	/**
	 * Returns the Time of the specified number of milliseconds.
	 */
	public static Time valueOf(int milliseconds) {
		return new Time(milliseconds);
	}


	/**
	 * Returns the Time for the date object.
	 */
	public static Time valueOf(Date date) {
		return new Time(date);
	}


	/**
	 * Returns the total number of milliseconds in the time milliseconds.
	 */
	public int getMilliseconds() {
		return this.milliseconds;
	}


	/**
	 * Returns the epoch date with time.
	 */
	public Date getDate() {
		return this.date;
	}


	/**
	 * Returns the total number of seconds in the time milliseconds.  Since the base milliseconds
	 * is milliseconds, any fraction of a second is discarded NOT rounded.
	 */
	public int getSeconds(Date theDate) {
		Date aDate = (Date) theDate.clone();
		return DateUtils.getMillisecondsFromMidnight(DateUtils.setTime(aDate, getDate())) / 1000;
	}


	/**
	 * Returns the total number of minutes in the time milliseconds.  Since the base milliseconds
	 * is milliseconds, any fraction of a minute is discarded NOT rounded.
	 */
	public int getMinutes(Date theDate) {
		return getSeconds(theDate) / 60;
	}


	/**
	 * Returns the total number of hours in the time milliseconds.  Since the base milliseconds
	 * is milliseconds, any fraction of an hour is discarded NOT rounded.
	 */
	public int getHours(Date theDate) {
		return getMinutes(theDate) / 60;
	}


	/**
	 * Returns the string representation of the time with the default format "HH:mm:ss".
	 */
	@Override
	public String toString() {
		return toString(TIME_FORMAT_FULL);
	}


	/**
	 * Returns the string representation of the time with the provided format.
	 * NOTE: formats are date formats.
	 *
	 * @param format
	 */
	public String toString(String format) {
		// can use any date as long as there's no daylight savings change on this date
		return DateUtils.fromDate(DateUtils.addMilliseconds(DateUtils.toDate("01/01/2000"), this.milliseconds), format);
	}


	/**
	 * Parses a sting representation of time to a Time milliseconds.  Default format is "HH:mm:ss".
	 */
	public static Time parse(String value) {
		return parse(value, TIME_FORMAT_FULL);
	}


	/**
	 * Parses a sting representation of time to a Time milliseconds with the provided format.
	 * NOTE: formats are date formats.
	 */
	public static Time parse(String value, String format) {
		return new Time(DateUtils.toDate(value, format));
	}


	@Override
	public int hashCode() {
		return this.milliseconds;
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Time) {
			return getMilliseconds() == ((Time) obj).getMilliseconds();
		}
		return false;
	}


	@Override
	public int compareTo(Time anotherTime) {
		int thisVal = this.milliseconds;
		int anotherVal = anotherTime.milliseconds;
		return Integer.compare(thisVal, anotherVal);
	}
}
