package com.clifton.core.util;


import java.lang.reflect.Method;


public class EnumUtils {

	public static String getEnumJSONString(Class<?> enumClass) {
		String[] data = getEnumStrings(enumClass);
		StringBuilder sb = new StringBuilder();
		sb.append("[");
		for (int i = 0; i < data.length; i++) {
			sb.append("[");
			sb.append(StringUtils.capitalize(data[i].toLowerCase()));
			sb.append(",");
			sb.append(data[i]);
			sb.append("]");
			if (i != data.length - 1) {
				sb.append(",");
			}
		}
		sb.append("]");
		return sb.toString();
	}


	public static String[] getEnumStrings(Class<?> enumClass) {
		if (!enumClass.isEnum()) {
			throw new IllegalArgumentException("The argument class must be an enum: " + enumClass);
		}
		try {
			Method method = enumClass.getMethod("values");
			Object[] values = (Object[]) method.invoke(enumClass);
			String[] result = new String[values.length];

			for (int i = 0; i < values.length; i++) {
				result[i] = values[i].toString();
			}

			return result;
		}
		catch (Exception e) {
			throw new IllegalArgumentException("Error reading enum values for: " + enumClass, e);
		}
	}
}
