package com.clifton.core.util.collections;


import java.util.Collection;


public interface CollectionFactory<T> {

	public Collection<T> createCollection();
}
