package com.clifton.core.util.collections;


import com.clifton.core.util.concurrent.Lockable;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


/**
 * The <code>ConcurrentMultiValueHashMap</code> is a thread-safe implementation of {@link MultiValueHashMap} that is backed by a {@link HashMap} that maps a key
 * to a collection of values. Concurrent reads are permitted without blocking each other, but atomic operations are protected from concurrent access.
 *
 * @param <K> the type of keys maintained
 * @param <V> the type of mapped values
 * @author jgommels
 */
public class ConcurrentMultiValueHashMap<K, V> implements MultiValueMap<K, V>, Lockable {

	private final MultiValueHashMap<K, V> map;

	/**
	 * A reentrant read-write lock that allows concurrent reads when there are no writes.
	 */
	private final ReadWriteLock lock = new ReentrantReadWriteLock();


	/**
	 * Constructs an empty non-concurrent <code>MultiValueHashMap</code>.
	 *
	 * @param allowDuplicates whether to allow duplicate values
	 */
	public ConcurrentMultiValueHashMap(boolean allowDuplicates) {
		this.map = new MultiValueHashMap<>(allowDuplicates);
	}


	/**
	 * Constructs an empty non-concurrent <code>MultiValueHashMap</code> which will instantiate new value collections with the supplied initial capacity.
	 *
	 * @param allowDuplicates           whether to allow duplicate values
	 * @param initialCollectionCapacity the initial capacity of the collections instantiated by this <code>MultiValueHashMap</code>
	 */
	public ConcurrentMultiValueHashMap(boolean allowDuplicates, int initialCollectionCapacity) {
		this.map = new MultiValueHashMap<>(allowDuplicates, initialCollectionCapacity);
	}


	public ConcurrentMultiValueHashMap(CollectionFactory<V> collectionFactory) {
		this.map = new MultiValueHashMap<>(collectionFactory);
	}


	@Override
	public void lock() {
		this.lock.writeLock().lock();
	}


	@Override
	public void unlock() {
		this.lock.writeLock().unlock();
	}


	@Override
	public int keyCount() {
		this.lock.readLock().lock();
		try {
			return this.map.keyCount();
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@Override
	public int valueCount() {
		this.lock.readLock().lock();
		try {
			return this.map.valueCount();
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@Override
	public boolean isEmpty() {
		return this.map.isEmpty();
	}


	@Override
	public List<V> get(K key) {
		this.lock.readLock().lock();
		try {
			return this.map.get(key);
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@Override
	public Set<Map.Entry<K, Collection<V>>> entrySet() {
		this.lock.readLock().lock();
		try {
			return this.map.entrySet();
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@Override
	public Set<K> keySet() {
		this.lock.readLock().lock();
		try {
			return this.map.keySet();
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@Override
	public List<V> values() {
		this.lock.readLock().lock();
		try {
			return this.map.values();
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@Override
	public Map<K, Collection<V>> cloneMap() {
		return this.map.cloneMap();
	}


	@Override
	public void put(K key, V value) {
		this.lock.writeLock().lock();
		try {
			this.map.put(key, value);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	@Override
	public void putAll(K key, Collection<? extends V> values) {
		this.lock.writeLock().lock();
		try {
			this.map.putAll(key, values);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	@Override
	public void putAll(MultiValueMap<? extends K, ? extends V> otherMap) {
		this.lock.writeLock().lock();
		try {
			for (Map.Entry<? extends K, ? extends Collection<? extends V>> entry : otherMap.entrySet()) {
				putAll(entry.getKey(), entry.getValue());
			}
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	@Override
	public boolean remove(K key, V value) {
		this.lock.writeLock().lock();
		try {
			return this.map.remove(key, value);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	@Override
	public List<V> removeAll(K key) {
		this.lock.writeLock().lock();
		try {
			return this.map.removeAll(key);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	@Override
	public boolean replace(K key, V oldValue, V newValue) {
		this.lock.writeLock().lock();
		try {
			return this.map.replace(key, oldValue, newValue);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	@Override
	public boolean replace(K oldKey, V oldValue, K newKey, V newValue) {
		this.lock.writeLock().lock();
		try {
			return this.map.replace(oldKey, oldValue, newKey, newValue);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}
}
