package com.clifton.core.messaging;

import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;


/**
 * The <code>AsynchronousMessagingTestProcessor</code> is a dummy processor used to listen to a queue during testing to capture client-server communication
 * done over JMS rather than HTTP and store it inside the <code>LinkedBlockingQueue responses</code> which can be accessed by the tests to view responses.
 * <p>
 * See:
 * <code>FixClientToSessionMessageTests</code>
 * <code>FixEntityMessagingTests</code>
 *
 * @author lnaylor
 */
public class AsynchronousMessagingTestProcessor<T extends AsynchronousMessage> implements AsynchronousProcessor<T> {

	private BlockingQueue<T> responses = new LinkedBlockingQueue<>();


	@Override
	public void process(T msg, AsynchronousMessageHandler handler) {
		getResponses().add(msg);
	}


	public T getNextMessage() {
		try {
			return getNextMessage(5000);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	public T getNextMessage(int msTimeout) {
		try {
			return getResponses().poll(msTimeout, TimeUnit.MILLISECONDS);
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BlockingQueue<T> getResponses() {
		return this.responses;
	}


	public void setResponses(BlockingQueue<T> responses) {
		this.responses = responses;
	}
}
