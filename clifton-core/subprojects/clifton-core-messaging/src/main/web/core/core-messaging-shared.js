Ext.ns('Clifton.core.messaging');

/*
 * Constants
 */

Clifton.core.messaging.MESSAGE_TYPES = [
	['XML', 'XML', 'XML format'],
	['JSON', 'JSON', 'JSON format']
];
