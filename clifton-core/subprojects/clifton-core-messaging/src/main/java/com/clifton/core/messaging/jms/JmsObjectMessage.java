package com.clifton.core.messaging.jms;

import com.clifton.core.beans.annotations.ValueChangingSetter;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;
import java.io.Serializable;
import java.util.Enumeration;


/**
 * <code>JmsObjectMessage</code> a wrapper for a JMS {@link TextMessage}, allowing a text message to be converted to an object message
 * while retaining all the original message properties.
 */
public class JmsObjectMessage implements ObjectMessage {


	private Message message;
	private Serializable object;


	JmsObjectMessage(Message message, Serializable object) {
		this.message = message;
		this.object = object;
	}


	@Override
	public void setObject(Serializable object) throws JMSException {
		this.object = object;
	}


	@Override
	public Serializable getObject() throws JMSException {
		return this.object;
	}


	@Override
	public String getJMSMessageID() throws JMSException {
		return this.message.getJMSMessageID();
	}


	@Override
	@ValueChangingSetter
	public void setJMSMessageID(String id) throws JMSException {
		this.message.setJMSMessageID(id);
	}


	@Override
	public long getJMSTimestamp() throws JMSException {
		return this.message.getJMSTimestamp();
	}


	@Override
	@ValueChangingSetter
	public void setJMSTimestamp(long timestamp) throws JMSException {
		this.message.setJMSTimestamp(timestamp);
	}


	@Override
	public byte[] getJMSCorrelationIDAsBytes() throws JMSException {
		return this.message.getJMSCorrelationIDAsBytes();
	}


	@Override
	@ValueChangingSetter
	public void setJMSCorrelationIDAsBytes(byte[] correlationID) throws JMSException {
		this.message.setJMSCorrelationIDAsBytes(correlationID);
	}


	@Override
	@ValueChangingSetter
	public void setJMSCorrelationID(String correlationID) throws JMSException {
		this.message.setJMSCorrelationID(correlationID);
	}


	@Override
	public String getJMSCorrelationID() throws JMSException {
		return this.message.getJMSCorrelationID();
	}


	@Override
	public Destination getJMSReplyTo() throws JMSException {
		return this.message.getJMSReplyTo();
	}


	@Override
	@ValueChangingSetter
	public void setJMSReplyTo(Destination replyTo) throws JMSException {
		this.message.setJMSReplyTo(replyTo);
	}


	@Override
	public Destination getJMSDestination() throws JMSException {
		return this.message.getJMSDestination();
	}


	@Override
	@ValueChangingSetter
	public void setJMSDestination(Destination destination) throws JMSException {
		this.message.setJMSDestination(destination);
	}


	@Override
	public int getJMSDeliveryMode() throws JMSException {
		return this.message.getJMSDeliveryMode();
	}


	@Override
	@ValueChangingSetter
	public void setJMSDeliveryMode(int deliveryMode) throws JMSException {
		this.message.setJMSDeliveryMode(deliveryMode);
	}


	@Override
	public boolean getJMSRedelivered() throws JMSException {
		return this.message.getJMSRedelivered();
	}


	@Override
	@ValueChangingSetter
	public void setJMSRedelivered(boolean redelivered) throws JMSException {
		this.message.setJMSRedelivered(redelivered);
	}


	@Override
	public String getJMSType() throws JMSException {
		return this.message.getJMSType();
	}


	@Override
	@ValueChangingSetter
	public void setJMSType(String type) throws JMSException {
		this.message.setJMSType(type);
	}


	@Override
	public long getJMSExpiration() throws JMSException {
		return this.message.getJMSExpiration();
	}


	@Override
	@ValueChangingSetter
	public void setJMSExpiration(long expiration) throws JMSException {
		this.message.setJMSExpiration(expiration);
	}


	@Override
	public int getJMSPriority() throws JMSException {
		return this.message.getJMSPriority();
	}


	@Override
	@ValueChangingSetter
	public void setJMSPriority(int priority) throws JMSException {
		this.message.setJMSPriority(priority);
	}


	@Override
	public void clearProperties() throws JMSException {
		this.message.clearProperties();
	}


	@Override
	public boolean propertyExists(String name) throws JMSException {
		return this.message.propertyExists(name);
	}


	@Override
	public boolean getBooleanProperty(String name) throws JMSException {
		return this.message.getBooleanProperty(name);
	}


	@Override
	public byte getByteProperty(String name) throws JMSException {
		return this.message.getByteProperty(name);
	}


	@Override
	public short getShortProperty(String name) throws JMSException {
		return this.message.getShortProperty(name);
	}


	@Override
	public int getIntProperty(String name) throws JMSException {
		return this.message.getIntProperty(name);
	}


	@Override
	public long getLongProperty(String name) throws JMSException {
		return this.message.getLongProperty(name);
	}


	@Override
	public float getFloatProperty(String name) throws JMSException {
		return this.message.getFloatProperty(name);
	}


	@Override
	public double getDoubleProperty(String name) throws JMSException {
		return this.message.getDoubleProperty(name);
	}


	@Override
	public String getStringProperty(String name) throws JMSException {
		return this.message.getStringProperty(name);
	}


	@Override
	public Object getObjectProperty(String name) throws JMSException {
		return this.message.getObjectProperty(name);
	}


	@SuppressWarnings("rawtypes")
	@Override
	public Enumeration getPropertyNames() throws JMSException {
		return this.message.getPropertyNames();
	}


	@Override
	public void setBooleanProperty(String name, boolean value) throws JMSException {
		this.message.setBooleanProperty(name, value);
	}


	@Override
	public void setByteProperty(String name, byte value) throws JMSException {
		this.message.setByteProperty(name, value);
	}


	@Override
	public void setShortProperty(String name, short value) throws JMSException {
		this.message.setShortProperty(name, value);
	}


	@Override
	public void setIntProperty(String name, int value) throws JMSException {
		this.message.setIntProperty(name, value);
	}


	@Override
	public void setLongProperty(String name, long value) throws JMSException {
		this.message.setLongProperty(name, value);
	}


	@Override
	public void setFloatProperty(String name, float value) throws JMSException {
		this.message.setFloatProperty(name, value);
	}


	@Override
	public void setDoubleProperty(String name, double value) throws JMSException {
		this.message.setDoubleProperty(name, value);
	}


	@Override
	public void setStringProperty(String name, String value) throws JMSException {
		this.message.setStringProperty(name, value);
	}


	@Override
	public void setObjectProperty(String name, Object value) throws JMSException {
		this.message.setObjectProperty(name, value);
	}


	@Override
	public void acknowledge() throws JMSException {
		this.message.acknowledge();
	}


	@Override
	public void clearBody() throws JMSException {
		this.message.clearBody();
	}
}
