package com.clifton.core.messaging.converter.json;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.Module;

import javax.jms.Message;


/**
 * The Jackson {@link Module.SetupContext#setMixInAnnotations(Class, Class) mixin} for {@link Message} serialization and deserialization. This mixin adds type information to fields
 * which are sub-types of {@link Message} and allows for deserialization to the provided sub-type.
 *
 * @author MikeH
 */
@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
public interface JacksonMessageMixin {}
