package com.clifton.core.messaging.jms.asynchronous.sender;

import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.JmsMessageCreatorCommand;
import com.clifton.core.messaging.jms.JmsMessageCreatorUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProducerCallback;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.beans.Lazy;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.annotation.PreDestroy;
import javax.jms.Destination;
import java.util.Map;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;


/**
 * The <code>AsynchronousJmsMessageSender</code> default asynchronous JMS message sender implementation.
 *
 * @author vanand
 */
public class JmsMessageSender implements MessageSender {

	private JmsTemplate jmsTemplate;
	/**
	 * The queue used to send messages.
	 */
	private String outgoingQueue;
	/**
	 * Message source used to filter messages on a queue.
	 * <p>
	 * For example, if 2 developer want to run their instances locally using the same JMS server, they could each set
	 * a unique message source and their instances will append the source to the outgoing messages and filter the incoming messages.
	 * <p>
	 * NOTE: The client message source must match the message source on the server application that is supposed to receive the messages.  On
	 * the server side the message source must be set in the spring context:
	 * <p>
	 * <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
	 * <property name="connectionFactory" ref="integrationServerJmsFactory"/>
	 * <property name="destination" ref="requestQueue" />
	 * <property name="messageListener" ref="integrationMessageConsumer" />
	 * <property name="messageSelector" value="MessageSource = '${integration.jms.messageSource}_${application.name}'"/>
	 * </bean>
	 */
	private String messageSource;
	private MessageTypes defaultMessageType;
	/**
	 * A list of default properties to append to out going messages.
	 */
	private Map<String, String> defaultMessageProperties;
	private String threadPoolName;

	/* Throughput logging */

	/**
	 * Lazily initialized {@link ScheduledExecutorService} for logging throughput on an interval.
	 * This executor is only initialized if {@link #isLogThroughputMessage()} returns <code>true</code>.
	 * If the executor is initialized, it will be shutdown on application shutdown via {@link #stop()}.
	 */
	private static final Lazy<ScheduledExecutorService> throughputLoggingExecutorService = new Lazy<>(() -> ConcurrentUtils.newSingleThreadScheduledExecutorService("Async Messaging Throughput Logger"));
	/**
	 * Fields for configuring log throughput messaging. By default, throughput logging is disabled.
	 */
	private boolean logThroughputMessage;
	/**
	 * Minutes interval between throughput logging task execution when {@link #isLogThroughputMessage()} returns true. The default is 60 minutes (1 hour).
	 */
	private long logThroughputMessageIntervalMinutes = 60L;
	/**
	 * Used to track throughput of sent messages between throughput log messages.
	 * The counters are incremented for each message if {@link #isLogThroughputMessage()} is true, and each are reset each time the throughput logging task runs.
	 */
	private final LongAdder throughputSentCounter = new LongAdder();
	/**
	 * Field that is initialized to the scheduled throughput logging task when {@link #isLogThroughputMessage()} returns true.
	 */
	private final Lazy<ScheduledFuture<?>> logThroughputMessageFuture = new Lazy<>(() -> {
		Runnable task = () -> {
			long sent = getThroughputSentCounter().sumThenReset();
			if (sent > 0) {
				LogUtils.info(LogCommand.ofMessage(getClass(), "[", getOutgoingQueue(), "] messages processed in the last", getLogThroughputMessageIntervalMinutes(), "minutes: sent (", sent, ")"));
			}
		};
		return throughputLoggingExecutorService.get().scheduleAtFixedRate(task, getLogThroughputMessageIntervalMinutes(), getLogThroughputMessageIntervalMinutes(), TimeUnit.MINUTES);
	});

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void send(AsynchronousMessage requestMessage) {
		send(requestMessage, null);
	}


	public void send(AsynchronousMessage requestMessage, String correlationId) {
		incrementThroughputCounter(getThroughputSentCounter());
		doSend(requestMessage, correlationId, getOutgoingQueue());
	}


	@PreDestroy
	public void stop() {
		if (isLogThroughputMessage() && throughputLoggingExecutorService.isComputed()) {
			ConcurrentUtils.shutdownExecutorService(throughputLoggingExecutorService.get());
		}
	}


	public void doSend(final AsynchronousMessage requestMessage, final String correlationId, String queue) {
		AssertUtils.assertNotNull(getJmsTemplate(), "Messages cannot be sent without a JMS template defined!");
		JmsMessageCreatorCommand messageCreatorCommand = new JmsMessageCreatorCommand();
		messageCreatorCommand.setCorrelationId(correlationId);
		messageCreatorCommand.setMessagingMessage(requestMessage);
		messageCreatorCommand.setDefaultMessageProperties(getDefaultMessageProperties());
		messageCreatorCommand.setMessageConverter(getJmsTemplate().getMessageConverter());
		messageCreatorCommand.setMessageSource(getMessageSource() + "_" + requestMessage.getProperties().get("TargetApplication"));
		messageCreatorCommand.setDefaultMessageType(getDefaultMessageType());
		MessageCreator messageCreator = JmsMessageCreatorUtils.getRequestMessageCreator(messageCreatorCommand);

		//Execute the request
		Destination destination = getJmsTemplate().getDefaultDestination();
		getJmsTemplate().execute(destination, new AsynchronousProducerCallback(messageCreator, queue, getJmsTemplate()));
	}


	private void incrementThroughputCounter(LongAdder throughputCounter) {
		if (isLogThroughputMessage()) {
			throughputCounter.increment();
			if (!getLogThroughputMessageFuture().isComputed()) {
				// Get the future, which will initialize it only if not yet computed. This will schedule the logging task.
				getLogThroughputMessageFuture().get();
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public LongAdder getThroughputSentCounter() {
		return this.throughputSentCounter;
	}


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public String getMessageSource() {
		return this.messageSource;
	}


	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}


	public String getOutgoingQueue() {
		return this.outgoingQueue;
	}


	public void setOutgoingQueue(String outgoingQueue) {
		this.outgoingQueue = outgoingQueue;
	}


	public MessageTypes getDefaultMessageType() {
		return this.defaultMessageType;
	}


	public void setDefaultMessageType(MessageTypes defaultMessageType) {
		this.defaultMessageType = defaultMessageType;
	}


	public Map<String, String> getDefaultMessageProperties() {
		return this.defaultMessageProperties;
	}


	public void setDefaultMessageProperties(Map<String, String> defaultMessageProperties) {
		this.defaultMessageProperties = defaultMessageProperties;
	}


	public String getThreadPoolName() {
		return this.threadPoolName;
	}


	public void setThreadPoolName(String threadPoolName) {
		this.threadPoolName = threadPoolName;
	}


	public boolean isLogThroughputMessage() {
		return this.logThroughputMessage;
	}


	public void setLogThroughputMessage(boolean logThroughputMessage) {
		this.logThroughputMessage = logThroughputMessage;
	}


	public long getLogThroughputMessageIntervalMinutes() {
		return this.logThroughputMessageIntervalMinutes;
	}


	public void setLogThroughputMessageIntervalMinutes(long logThroughputMessageIntervalMinutes) {
		this.logThroughputMessageIntervalMinutes = logThroughputMessageIntervalMinutes;
	}


	public Lazy<ScheduledFuture<?>> getLogThroughputMessageFuture() {
		return this.logThroughputMessageFuture;
	}
}
