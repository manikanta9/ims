package com.clifton.core.messaging.jms;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.core.ProducerCallback;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import java.util.UUID;


/**
 * <code>AbstractProducerCallback</code> is a base class for creating a {@link Message}, sending it on a queue,
 * and handling a response if necessary.
 *
 * @author NickK
 */
public abstract class AbstractProducerCallback implements ProducerCallback<Message> {

	/**
	 * The message creator used to create the message.
	 */
	private final MessageCreator messageCreator;
	/**
	 * The base queue name.
	 * <p/>
	 * For example, if the queue name is 'cool.dude', request messages will be sent on 'cool.dude.request' and then the
	 * handler will listen on 'cool.dude.response' for the response message.
	 */
	private final String queue;
	private final JmsTemplate jmsTemplate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractProducerCallback(MessageCreator messageCreator, String queue, JmsTemplate jmsTemplate) {
		this.messageCreator = messageCreator;
		this.queue = queue;
		this.jmsTemplate = jmsTemplate;
	}


	/**
	 * Returns a {@link Message} created using {@link #getMessageCreator()}.
	 */
	protected Message createMessage(Session session) throws JMSException {
		return createMessage(session, false);
	}


	/**
	 * Returns a {@link Message} created using {@link #getMessageCreator()}. If createCorrelationId is true,
	 * the returned message will have a JMS correlation ID set using {@code UUID.randomUUID().toString()}.
	 */
	protected Message createMessage(Session session, boolean createCorrelationId) throws JMSException {
		Message message = getMessageCreator().createMessage(session);
		if (createCorrelationId) {
			message.setJMSCorrelationID(UUID.randomUUID().toString());
		}
		return message;
	}


	/**
	 * Resolves a P2P {@link Destination} for {@link #getQueue()} using {@link #getJmsTemplate()}.
	 */
	protected Destination getRequestDestinationQueue(Session session) throws JMSException {
		return getRequestDestinationQueue(session, getQueue());
	}


	/**
	 * Resolves a P2P {@link Destination} for the provided queue name using {@link #getJmsTemplate()}.
	 */
	protected Destination getRequestDestinationQueue(Session session, String queueName) throws JMSException {
		return getJmsTemplate().getDestinationResolver().resolveDestinationName(session, queueName, false);
	}


	/**
	 * Uses the provided {@link Session} and {@link MessageProducer} to send a {@link Message} created using {@link #createMessage(Session)}
	 * and the {@link Destination} using {@link #getRequestDestinationQueue(Session)} with a timeout of {@code getJmsTemplate().getTimeToLive()}.
	 */
	protected void sendMessage(Session session, MessageProducer producer) throws JMSException {
		sendMessage(producer, createMessage(session), getRequestDestinationQueue(session), getJmsTemplate().getTimeToLive());
	}


	/**
	 * Sends the provided {@link Message} with the provided {@link MessageProducer} with the delivery mode and priority defined
	 * on the template from {@link #getJmsTemplate()} and the provided timeout.
	 */
	protected void sendMessage(MessageProducer producer, Message message, Destination queue, long timeout) throws JMSException {
		producer.send(queue, message, getJmsTemplate().getDeliveryMode(), getJmsTemplate().getPriority(), timeout);
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public MessageCreator getMessageCreator() {
		return this.messageCreator;
	}


	public String getQueue() {
		return this.queue;
	}


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}
}
