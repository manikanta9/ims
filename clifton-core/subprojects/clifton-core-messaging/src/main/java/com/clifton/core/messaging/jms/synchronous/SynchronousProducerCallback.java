package com.clifton.core.messaging.jms.synchronous;


import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.AbstractProducerCallback;
import com.clifton.core.messaging.jms.JmsUtils;
import com.clifton.core.messaging.jms.activemq.ActiveMQNonExpiringCustomDestination;
import com.clifton.core.util.AssertUtils;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.Session;


/**
 * The <code>SynchronousProducerCallback</code> class is a message ProducerCallback that implements a synchronous
 * message exchange between request and response queues.
 * <p/>
 * NOTE: An instance of this object is created be SynchronousMessageSender for message that is sent.
 *
 * @author mwacker
 * @author vgomelsky
 */
public final class SynchronousProducerCallback extends AbstractProducerCallback {

	/**
	 * Number of milliseconds to wait for a response message.
	 */
	private int timeout = 5000;
	/**
	 * The number milliseconds until the message expires and is removed from JMS queue if it's not read.
	 * <p/>
	 * NOTE: If this is not set, it will be set using the timeout.
	 */
	private Integer messageTimeOut;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SynchronousProducerCallback(final MessageCreator messageCreator, String queue, JmsTemplate jmsTemplate) {
		super(messageCreator, queue, jmsTemplate);
	}


	@Override
	public Message doInJms(Session session, MessageProducer messageProducer) throws JMSException {
		// create request and reply queues
		String requestQueueName = getQueue() + ".request";
		String replyQueueName = getQueue() + ".response";

		// create the message
		Message message = createMessage(session, true);
		message.setJMSReplyTo(getRequestDestinationQueue(session, replyQueueName));

		// create the consumer first so it can wait for reply
		Destination replyQueue = new ActiveMQNonExpiringCustomDestination(replyQueueName + "?consumer.prefetchSize=0", getJmsTemplate());
		MessageConsumer messageConsumer = session.createConsumer(replyQueue, MessagingMessage.MESSAGE_CORRELATION_ID_PROPERTY_NAME + " = '" + message.getJMSCorrelationID() + "'");
		try {
			// send the message
			Destination requestQueue = getRequestDestinationQueue(session, requestQueueName);
			sendMessage(messageProducer, message, requestQueue, getMessageTimeOut() != null ? getMessageTimeOut() : getTimeout());

			// block on receiving the response with a timeout
			long sendTime = System.currentTimeMillis();
			Message response = messageConsumer.receive(getTimeout());

			long currentTime = System.currentTimeMillis();
			if (response == null && (currentTime - sendTime) >= (getTimeout() - 100)) {
				throw new RuntimeException("Timed out after " + (currentTime - sendTime) + "ms of waiting for reply on '" + replyQueueName + "' queue. Request message was sent to '"
						+ requestQueueName + "' queue. Request Message:\n" + JmsUtils.getMessageText(message));
			}
			AssertUtils.assertNotNull(response, "Response cannot be null. Check your configuration properties or the server code.");
			return response;
		}
		finally {
			// Don't forget to close your resources
			JmsUtils.closeMessageConsumer(messageConsumer);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getTimeout() {
		return this.timeout;
	}


	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}


	public Integer getMessageTimeOut() {
		return this.messageTimeOut;
	}


	public void setMessageTimeOut(Integer messageTimeOut) {
		this.messageTimeOut = messageTimeOut;
	}
}
