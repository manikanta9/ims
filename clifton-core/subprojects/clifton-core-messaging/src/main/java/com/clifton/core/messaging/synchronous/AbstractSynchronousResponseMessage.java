package com.clifton.core.messaging.synchronous;


import com.clifton.core.messaging.AbstractMessagingMessage;


public abstract class AbstractSynchronousResponseMessage extends AbstractMessagingMessage implements SynchronousResponseMessage {

	private Integer requestTimeout;
	private Throwable error;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}


	@Override
	public Throwable getError() {
		return this.error;
	}


	@Override
	public void setError(Throwable error) {
		this.error = error;
	}
}
