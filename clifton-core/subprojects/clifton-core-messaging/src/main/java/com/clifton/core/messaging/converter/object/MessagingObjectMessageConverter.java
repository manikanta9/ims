package com.clifton.core.messaging.converter.object;

import com.clifton.core.util.AssertUtils;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import java.io.Serializable;


/**
 * The {@link MessagingObjectMessageConverter} handles conversion to and from {@link ObjectMessage object messages}.
 * <p>
 * Object messages are only supported for {@link Serializable} entities.
 *
 * @author MikeH
 */
public class MessagingObjectMessageConverter implements MessageConverter {

	@Override
	public Message toMessage(Object object, Session session) {
		AssertUtils.assertTrue(object instanceof Serializable, "Invalid object type: [%s]. Object message conversion is only supported for objects of type [%s].", object.getClass().getName(), Serializable.class.getName());
		try {
			@SuppressWarnings("ConstantConditions")
			Serializable serializableObject = (Serializable) object;
			return session.createObjectMessage(serializableObject);
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while converting the object to a message.", e);
		}
	}


	@Override
	public Object fromMessage(Message message) {
		AssertUtils.assertTrue(message instanceof ObjectMessage, "Invalid message type: [%s]. Object message conversion is only supported for messages of type [%s].", message.getClass().getName(), ObjectMessage.class.getName());
		try {
			@SuppressWarnings("ConstantConditions")
			ObjectMessage objectMessage = (ObjectMessage) message;
			return objectMessage.getObject();
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while converting the message to an object.", e);
		}
	}
}
