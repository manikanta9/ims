package com.clifton.core.messaging.converter.json;

import com.fasterxml.jackson.databind.module.SimpleModule;
import org.apache.activemq.command.ActiveMQMessage;

import javax.jms.Message;


/**
 * The {@link JmsMessagingModule} type is a Jackson {@link SimpleModule} which applies configurations specific to JMS serialization and deserialization.
 * <p>
 * Most notably, this module applies mixins for {@link Message} serialization and deserialization. See {@link JacksonMessageMixin} and {@link JacksonActiveMQMessageMixin} for more information.
 *
 * @author MikeH
 */
public class JmsMessagingModule extends SimpleModule {

	@Override
	public void setupModule(SetupContext context) {
		context.setMixInAnnotations(Message.class, JacksonMessageMixin.class);
		context.setMixInAnnotations(ActiveMQMessage.class, JacksonActiveMQMessageMixin.class);
		context.setMixInAnnotations(StackTraceElement.class, JacksonStackTraceElementMixin.class);
	}
}
