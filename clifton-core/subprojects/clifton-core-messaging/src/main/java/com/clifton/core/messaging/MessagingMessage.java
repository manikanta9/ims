package com.clifton.core.messaging;


import java.io.Serializable;
import java.util.Map;


/**
 * The <code>JmsMessage</code> interface defines a basic JMS message: income or outgoing.
 * More specific implementation can add additional functionality.
 */
public interface MessagingMessage extends Serializable {

	/**
	 * Property name for the target application of the outgoing message; used to filter messages
	 * to a specific target application when broadcasting virtualTopics to multiple consumers.
	 */
	public static final String TARGET_APPLICATION = "TargetApplication";
	/**
	 * Property name for the machine name property is added to each outgoing message.
	 */
	public static final String MESSAGE_MACHINE_NAME_PROPERTY_NAME = "machineName";
	/**
	 * Property name for the response class property added to outgoing messages.
	 */
	public static final String MESSAGE_RESPONSE_CLASS_PROPERTY_NAME = "responseClass";
	/**
	 * The property name for the serialized object type. This is used to determine the target object for deserialization.
	 * <p>
	 * <b>Note: Deserializers are responsible for implementing secure patterns to prevent deserialization exploits, such as limiting the set of allowable types.</b>
	 */
	public static final String MESSAGE_OBJECT_TYPE_PROPERTY_NAME = "objectType";
	/**
	 * The property name for content-type designations. This is used to determine the message serialization format.
	 */
	public static final String MESSAGE_CONTENT_TYPE_PROPERTY_NAME = "contentType";
	/**
	 * Property name for message object XML property added to outgoing messages.
	 */
	public static final String MESSAGE_OBJECT_XML_PROPERTY_NAME = "MessageObjectXml";
	/**
	 * Property name for message correlation ID for messages being sent. The correlation ID can be added to the
	 * selector to get the response for the request message.
	 */
	public static final String MESSAGE_CORRELATION_ID_PROPERTY_NAME = "JMSCorrelationID";
	/**
	 * Property name for the message group used in consumer message selectors. This property sets the group property
	 * of the JMS message and will produce a cached hash entry to a dedicated message consumer in Active MQ.
	 */
	public static final String MESSAGE_GROUP_PROPERTY_NAME = "JMSXGroupID";
	/**
	 * Property name for the message source used in consumer message selectors. This key is usually used for synchronous
	 * messages as a primary message selector. Unlike {@link #MESSAGE_GROUP_PROPERTY_NAME}, this property does not map
	 * a dedicated consumer and allows for multiple consumers with the same selector to be used round robin.
	 */
	public static final String MESSAGE_SOURCE_PROPERTY_NAME = "MessageSource";
	/**
	 * Property name for the message source tag used in message selectors. This key is usually used as a secondary message
	 * selector condition for asynchronous messages (e.g. "JMSXGroupID = 'someValue' AND sourceSystemTag = 'anotherValue'").
	 */
	public static final String MESSAGE_SOURCE_TAG_PROPERTY_NAME = "sourceSystemTag";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the name of the machine that generated this message.
	 * One could load balance across multiple consumer service instances and it maybe useful
	 * to know who served the request.
	 */
	public String getMachineName();


	/**
	 * Sets the name of the computer that generated this message.
	 * One could load balance across multiple consumer service instances and it maybe useful
	 * to know who served the request.
	 *
	 * @param machineName
	 */
	public void setMachineName(String machineName);


	/**
	 * String properties that will be added to JMS message.  This is not meant for payload of
	 * property values, but for extra properties that can be added to the JMS message for
	 * filtering or viewing.
	 */
	public Map<String, String> getProperties();
}
