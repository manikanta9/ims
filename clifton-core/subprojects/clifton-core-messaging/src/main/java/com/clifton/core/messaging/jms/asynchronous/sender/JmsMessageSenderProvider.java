package com.clifton.core.messaging.jms.asynchronous.sender;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.ConnectionFactory;


/**
 * This class is an implementation of {@link MessageSenderProvider} to handle the messages targeted to JMS connection factories defined as Spring beans.
 *
 * @author vanand
 */
public class JmsMessageSenderProvider implements MessageSenderProvider, ValidationAware {

	/**
	 * The name of the connection factory to retrieve from the spring application context
	 * Must be of type ConnectionFactory
	 */
	private String jmsConnectionFactoryBeanName;

	private ApplicationContextService applicationContextService;
	private MessageConverter jmsMessageConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		try {
			getApplicationContextService().getContextBean(getJmsConnectionFactoryBeanName(), ConnectionFactory.class);
		}
		catch (Exception e) {
			throw new ValidationException(String.format("An error occurred while querying the context for a bean of name [%s] and type [%s].", getJmsConnectionFactoryBeanName(), ConnectionFactory.class.getSimpleName()), e);
		}
	}


	@Override
	public MessageSender getMessageSender(MessageSenderContext messageSenderContext) {
		JmsTemplate jmsTemplate = new JmsTemplate();
		jmsTemplate.setConnectionFactory(getApplicationContextService().getContextBean(getJmsConnectionFactoryBeanName(), ConnectionFactory.class));
		jmsTemplate.setMessageConverter(getJmsMessageConverter());
		JmsMessageSender asynchronousJmsMessageSender = new JmsMessageSender();
		asynchronousJmsMessageSender.setJmsTemplate(jmsTemplate);
		asynchronousJmsMessageSender.setOutgoingQueue(messageSenderContext.getOutgoingQueue());
		asynchronousJmsMessageSender.setDefaultMessageProperties(messageSenderContext.getDefaultMessageProperties());
		asynchronousJmsMessageSender.setDefaultMessageType(messageSenderContext.getDefaultMessageType());
		getApplicationContextService().autowireBean(asynchronousJmsMessageSender);
		return asynchronousJmsMessageSender;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String getJmsConnectionFactoryBeanName() {
		return this.jmsConnectionFactoryBeanName;
	}


	public void setJmsConnectionFactoryBeanName(String jmsConnectionFactoryBeanName) {
		this.jmsConnectionFactoryBeanName = jmsConnectionFactoryBeanName;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public MessageConverter getJmsMessageConverter() {
		return this.jmsMessageConverter;
	}


	public void setJmsMessageConverter(MessageConverter jmsMessageConverter) {
		this.jmsMessageConverter = jmsMessageConverter;
	}
}
