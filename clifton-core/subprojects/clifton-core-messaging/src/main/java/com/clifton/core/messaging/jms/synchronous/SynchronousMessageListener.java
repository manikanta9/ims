package com.clifton.core.messaging.jms.synchronous;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.AbstractXStreamMessageHandler;
import com.clifton.core.messaging.jms.JmsMessageCreatorCommand;
import com.clifton.core.messaging.jms.JmsMessageCreatorUtils;
import com.clifton.core.messaging.jms.JmsUtils;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;
import com.clifton.core.messaging.synchronous.SynchronousStatusRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousStatusResponseMessage;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.timer.TimerHandler;
import com.clifton.core.web.stats.SystemRequestStatsService;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.MessageProducer;


/**
 * The <code>SynchronousMessageListener</code> defines the server side methods for receiving synchronous calls from a JMS queue.
 *
 * @param <I>
 * @param <O>
 * @author mwacker
 */
public class SynchronousMessageListener<I extends SynchronousRequestMessage, O extends SynchronousResponseMessage> extends AbstractXStreamMessageHandler implements MessageListener {

	private JmsTemplate jmsTemplate;

	/**
	 * The JMS message processor.
	 */
	private SynchronousProcessor<I, O> processor;

	/**
	 * The JMS status message processor.
	 */
	private SynchronousProcessor<I, O> statusProcessor;

	/**
	 * The name of the application processing the message (e.g. IMS, RECONCILIATION, etc);
	 * Used for status tracking in API and JMS responses.
	 */
	private String applicationName;
	/**
	 * The requestSource for logging system stats.
	 */
	private String requestStatsSource;
	private TimerHandler timerHandler;
	private SystemRequestStatsService systemRequestStatsService;
	private ContextHandler contextHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Receives JMS messages (this is implemented from javax.jms.MessageListener).
	 */
	@Override
	public void onMessage(Message message) {
		if (isRequestStatsEnabled()) {
			getTimerHandler().startTimer();
		}

		I request = null;
		try {
			if (message == null) {
				postResponse(null, message);
			}
			@SuppressWarnings("unchecked")
			I castRequest = (I) JmsUtils.deserializeMessage(message, getXStreamInstance(), getJmsTemplate().getMessageConverter());
			request = castRequest;
			if (request instanceof SynchronousStatusRequestMessage) {
				if (((SynchronousStatusRequestMessage) request).isHeartbeat()) {
					postResponse(new SynchronousStatusResponseMessage(), message);
				}
				else {
					O response = getStatusProcessor().process(request);
					postResponse(response, message);
				}
			}
			else {
				O response = getProcessor().process(request);
				postResponse(response, message);
			}
		}
		catch (Throwable e) {
			LogUtils.errorOrInfo(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Error onMessage: " + ExceptionUtils.getOriginalMessage(e)));
			postError(e, message);
		}
		finally {
			try {
				// stop the timer and record stats
				if (isRequestStatsEnabled()) {
					Object user = getContextHandler().getBean(Context.USER_BEAN_NAME);
					String uri = (message != null ? message.getJMSDestination().toString() : "") + (request != null ? "_" + request.getClass().getSimpleName() : "");
					getTimerHandler().stopTimer();
					getSystemRequestStatsService().saveSystemRequestStats(getRequestStatsSource(), uri, user, getTimerHandler().getDurationNano(), 0, 0);
					getTimerHandler().reset();
				}
			}
			catch (Throwable ex) {
				LogUtils.error(getClass(), "Failed to log request stats for [" + message + "]", ex);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Posts the response message.
	 */


	private void postResponse(final SynchronousResponseMessage response, final Message message) throws JMSException {
		if (response != null) {
			//Set the sourceTag property to indicate where the response came from.
			response.getProperties().put(MessagingMessage.MESSAGE_SOURCE_TAG_PROPERTY_NAME, getApplicationName());

			JmsMessageCreatorCommand command = new JmsMessageCreatorCommand();
			command.setMessagingMessage(response);
			command.setCorrelationId(message.getJMSCorrelationID());
			command.setDefaultMessageType(getDefaultMessageType());
			command.setMessageConverter(getJmsTemplate().getMessageConverter());
			final MessageCreator messageCreator = JmsMessageCreatorUtils.getResponseMessageCreator(command);
			final long timeout = response.getRequestTimeout() != null && response.getRequestTimeout() > 0 ? response.getRequestTimeout() : getJmsTemplate().getTimeToLive();
			getJmsTemplate().execute(session -> {
				final MessageProducer producer = session.createProducer(message.getJMSReplyTo()); //creates an unidentified producer
				try {
					// send the message
					Message msg = messageCreator.createMessage(session);
					producer.send(msg, getJmsTemplate().getDeliveryMode(), getJmsTemplate().getPriority(), timeout);
					return null;
				}
				finally {
					JmsUtils.closeMessageProducer(producer);
				}
			});
		}
		else {
			LogUtils.error(SynchronousMessageListener.class, "Attempted to post null response for message: " + message);
		}
	}


	/**
	 * Post an error response.
	 */
	private void postError(Throwable e, Message message) {
		try {
			String responseClassName = message.getStringProperty(MessagingMessage.MESSAGE_RESPONSE_CLASS_PROPERTY_NAME);
			SynchronousResponseMessage response;
			if (!StringUtils.isEmpty(responseClassName)) {
				Class<?> responseClass = CoreClassUtils.getClass(responseClassName);
				response = (SynchronousResponseMessage) BeanUtils.newInstance(responseClass);
			}
			else {
				response = new JmsSynchronousErrorMessage();
			}
			response.setError(e);
			postResponse(response, message);
		}
		catch (Throwable ex) {
			LogUtils.error(getClass(), "Error in postError for " + e, ex);
		}
	}


	private boolean isRequestStatsEnabled() {
		return !StringUtils.isEmpty(getRequestStatsSource());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public SynchronousProcessor<I, O> getProcessor() {
		return this.processor;
	}


	public void setProcessor(SynchronousProcessor<I, O> processor) {
		this.processor = processor;
	}


	public SynchronousProcessor<I, O> getStatusProcessor() {
		return this.statusProcessor;
	}


	public void setStatusProcessor(SynchronousProcessor<I, O> statusProcessor) {
		this.statusProcessor = statusProcessor;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}


	public String getRequestStatsSource() {
		return this.requestStatsSource;
	}


	public void setRequestStatsSource(String requestStatsSource) {
		this.requestStatsSource = requestStatsSource;
	}


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
