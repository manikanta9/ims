package com.clifton.core.messaging.asynchronous;

import com.clifton.core.messaging.AbstractMessagingMessage;


/**
 * @author mwacker
 */
public abstract class AbstractAsynchronousMessage extends AbstractMessagingMessage implements AsynchronousMessage {

	private Integer messageDelaySeconds;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getMessageDelaySeconds() {
		return this.messageDelaySeconds;
	}


	public void setMessageDelaySeconds(Integer messageDelaySeconds) {
		this.messageDelaySeconds = messageDelaySeconds;
	}
}
