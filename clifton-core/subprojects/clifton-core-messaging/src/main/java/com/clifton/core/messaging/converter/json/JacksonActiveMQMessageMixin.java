package com.clifton.core.messaging.converter.json;

import com.clifton.core.messaging.asynchronous.AsynchronousErrorMessage;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.Module.SetupContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.RawSerializer;
import org.apache.activemq.command.ActiveMQMessage;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;


/**
 * The {@link JacksonActiveMQMessageMixin} type is the Jackson {@link SetupContext#setMixInAnnotations(Class, Class) mixin} for {@link ActiveMQMessage} serialization and
 * deserialization. This mixin modifies serialization to use the XML format for message objects. This is necessary since the internal representations for {@link ActiveMQMessage}
 * and its sub-types cannot easily be serialized as JSON objects. The ActiveMQ library provides XML serialization logic only.
 * <p>
 * {@link ActiveMQMessage} objects are typically only serialized when wrapped inside an outer message, such as within {@link AsynchronousErrorMessage} messages.
 *
 * @author MikeH
 */
@JsonSerialize(using = JacksonActiveMQMessageMixin.ActiveMQMessageSerializer.class)
@JsonDeserialize(using = JacksonActiveMQMessageMixin.ActiveMQMessageDeserializer.class)
public interface JacksonActiveMQMessageMixin {

	// RawSerializer is used to prevent unnecessarily overriding the serializeWithType method
	public static class ActiveMQMessageSerializer extends RawSerializer<ActiveMQMessage> {

		private final Marshaller jmsXmlMarshaller;


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public ActiveMQMessageSerializer(Marshaller jmsXmlMarshaller) {
			super(ActiveMQMessage.class);
			this.jmsXmlMarshaller = jmsXmlMarshaller;
		}


		@Override
		public void serialize(ActiveMQMessage value, JsonGenerator jgen, SerializerProvider provider) throws IOException {
			// Serialize message object as XML, using ActiveMQ's internal serialization logic
			Writer writer = new StringWriter();
			Result result = new StreamResult(writer);
			getJmsXmlMarshaller().marshal(value, result);
			jgen.writeString(writer.toString());
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public Marshaller getJmsXmlMarshaller() {
			return this.jmsXmlMarshaller;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class ActiveMQMessageDeserializer extends JsonDeserializer<ActiveMQMessage> {

		private final Unmarshaller jmsXmlUnmarshaller;


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public ActiveMQMessageDeserializer(Unmarshaller jmsXmlUnmarshaller) {
			this.jmsXmlUnmarshaller = jmsXmlUnmarshaller;
		}


		@Override
		public ActiveMQMessage deserialize(JsonParser p, DeserializationContext context) throws IOException {
			// Deserialize message object from XML, using ActiveMQ's internal deserialization logic
			String messageXml = p.getValueAsString();
			StringReader reader = new StringReader(messageXml);
			Source source = new StreamSource(reader);
			ActiveMQMessage message = (ActiveMQMessage) getJmsXmlUnmarshaller().unmarshal(source);
			return message;
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public Unmarshaller getJmsXmlUnmarshaller() {
			return this.jmsXmlUnmarshaller;
		}
	}
}
