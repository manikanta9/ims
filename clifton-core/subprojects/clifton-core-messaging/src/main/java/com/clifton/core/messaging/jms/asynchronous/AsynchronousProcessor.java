package com.clifton.core.messaging.jms.asynchronous;


import com.clifton.core.messaging.asynchronous.AsynchronousMessage;


/**
 * The <code>JmsProcessor</code> interface must be implemented by all JMS message processors.
 */
public interface AsynchronousProcessor<T extends AsynchronousMessage> {

	/**
	 * Processes the specified requestMessage and returns corresponding response.
	 */
	public void process(T message, AsynchronousMessageHandler handler);
}
