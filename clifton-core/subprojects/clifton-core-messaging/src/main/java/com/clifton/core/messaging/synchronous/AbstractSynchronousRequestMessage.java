package com.clifton.core.messaging.synchronous;


import com.clifton.core.messaging.AbstractMessagingMessage;


public abstract class AbstractSynchronousRequestMessage extends AbstractMessagingMessage implements SynchronousRequestMessage {

	private Integer requestTimeout;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
