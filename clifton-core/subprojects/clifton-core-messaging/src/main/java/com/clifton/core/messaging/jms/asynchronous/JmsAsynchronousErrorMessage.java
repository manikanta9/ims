package com.clifton.core.messaging.jms.asynchronous;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.AbstractMessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousErrorMessage;

import javax.jms.JMSException;
import javax.jms.Message;
import java.util.Date;


/**
 * The <code>JmsAsynchronousErrorMessage</code> wraps an existing JMS message that failed to
 * process so it can be placed on an error queue when an asynchronous process fails for some
 * reason.
 * <p/>
 * For example, in the trade-fix project when a FIX message received via JMS fails to process, an AsynchronousErrorMessage
 * is created and placed on the error queue.
 *
 * @author mwacker
 */
public class JmsAsynchronousErrorMessage extends AbstractMessagingMessage implements AsynchronousErrorMessage<Message> {

	/**
	 * The number of seconds to delay message delivery.
	 */
	public Integer messageDelaySeconds;
	/**
	 * The original JMS message the caused the error.
	 */
	private Message message;
	/**
	 * The error the occurred in processing.
	 */
	private Throwable error;
	/**
	 * The queue for the original message.  This is used to move the failed message back to the original for re-processing.
	 */
	private String targetQueue;
	/**
	 * The JMS server where the target JMS queue resides.
	 */
	private String server;
	/**
	 * The id of the failed message.
	 */
	private String errorMessageId;
	/**
	 * The date and time of the message.
	 */
	private Date messageTimestamp;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getMessageId() {
		try {
			return getMessage() != null ? getMessage().getJMSMessageID() : null;
		}
		catch (JMSException e) {
			LogUtils.error(getClass(), "Failed to get message id.", e);
		}
		return null;
	}


	@ValueChangingSetter
	public void setMessageId(String messageId) {
		// can we delete this method?
	}


	@Override
	public Message getMessage() {
		return this.message;
	}


	public void setMessage(Message message) {
		this.message = message;
	}


	@Override
	public Throwable getError() {
		return this.error;
	}


	@Override
	public void setError(Throwable error) {
		this.error = error;
	}


	@Override
	public String getServer() {
		return this.server;
	}


	public void setServer(String server) {
		this.server = server;
	}


	@Override
	public String getErrorMessageId() {
		return this.errorMessageId;
	}


	@Override
	public void setErrorMessageId(String errorMessageId) {
		this.errorMessageId = errorMessageId;
	}


	@Override
	public String getTargetQueue() {
		return this.targetQueue;
	}


	public void setTargetQueue(String targetQueue) {
		this.targetQueue = targetQueue;
	}


	@Override
	public Date getMessageTimestamp() {
		return this.messageTimestamp;
	}


	@Override
	public void setMessageTimestamp(Date messageTimestamp) {
		this.messageTimestamp = messageTimestamp;
	}


	@Override
	public Integer getMessageDelaySeconds() {
		return this.messageDelaySeconds;
	}


	public void setMessageDelaySeconds(Integer messageDelaySeconds) {
		this.messageDelaySeconds = messageDelaySeconds;
	}
}
