package com.clifton.core.messaging.jms.asynchronous;


import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.AbstractXStreamMessageHandler;
import com.clifton.core.messaging.jms.JmsMessageCreatorCommand;
import com.clifton.core.messaging.jms.JmsMessageCreatorUtils;
import com.clifton.core.messaging.jms.JmsUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.timer.TimerHandler;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.stats.SystemRequestStatsService;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.annotation.PreDestroy;
import javax.jms.Destination;
import javax.jms.Message;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.LongAdder;


/**
 * The <code>AsynchronousMessageHandler</code> default asynchronous JMS message handler implementation.
 *
 * @author mwacker
 */
public class AsynchronousMessageHandlerImpl extends AbstractXStreamMessageHandler implements AsynchronousMessageHandler {

	private int maxWorkerThreadCount = 5;

	/**
	 * Lazily initialized {@link ScheduledExecutorService} for logging throughput on an interval.
	 * This executor is only initialized if {@link #isLogThroughputMessage()} returns <code>true</code>.
	 * If the executor is initialized, it will be shutdown on application shutdown via {@link #stop()}.
	 */
	private static final Lazy<ScheduledExecutorService> throughputLoggingExecutorService = new Lazy<>(() -> ConcurrentUtils.newSingleThreadScheduledExecutorService("Async Messaging Throughput Logger"));

	/**
	 * Lazily initialized {@link ExecutorService} for asynchronously processing messages.
	 * This executor is only initialized if {@link #isAsynchronousMessageProcessing()} returns <code>true</code>.
	 * If the executor is initialized, it will be shutdown on application shutdown via {@link #stop()}.
	 */
	private final Lazy<ExecutorService> executorService = new Lazy<>(() -> {
		ValidationUtils.assertFalse(StringUtils.isEmpty(getThreadPoolName()), "Thread Pool Name is not defined for the asynchronous processing");
		return ConcurrentUtils.newBoundedCachedThreadPool(getThreadPoolName(), getMaxWorkerThreadCount());
	});

	private JmsTemplate jmsTemplate;

	/**
	 * The name of the application processing the message (e.g. IMS, RECONCILIATION, etc);
	 * Used for status tracking in API and JMS responses.
	 */
	private String applicationName;

	/**
	 * Message source used to filter messages on a queue.
	 * <p>
	 * For example, if 2 developer want to run their instances locally using the same JMS server, they could each set
	 * a unique message source and their instances will append the source to the outgoing messages and filter the incoming messages.
	 * <p>
	 * NOTE: The client message source must match the message source on the server application that is supposed to receive the messages.  On
	 * the server side the message source must be set in the spring context:
	 * <p>
	 * <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
	 * <property name="connectionFactory" ref="integrationServerJmsFactory"/>
	 * <property name="destination" ref="requestQueue" />
	 * <property name="messageListener" ref="integrationMessageConsumer" />
	 * <property name="messageSelector" value="MessageSource = '${integration.jms.messageSource}_${application.name}'"/>
	 * </bean>
	 */
	private String messageSource;

	/**
	 * The queue used to send messages.
	 */
	private String outgoingQueue;
	/**
	 * The error queue to place error messages when processing fails.
	 */
	private String errorQueue;
	/**
	 * The message processor.
	 */
	private AsynchronousProcessor<AsynchronousMessage> processor;
	/**
	 * A list of default properties to append to out going messages.
	 */
	private Map<String, String> defaultMessageProperties;
	/**
	 * The JMS server url - used for statistics and error messages.
	 */
	private String brokerUrl;


	/**
	 * The requestSource for logging system stats.
	 */
	private String requestStatsSource;
	private TimerHandler timerHandler;
	private SystemRequestStatsService systemRequestStatsService;
	private ContextHandler contextHandler;

	/**
	 * Indicates that a thread pool should be used to process the incoming messages.
	 */
	private boolean asynchronousMessageProcessing;
	private String threadPoolName;

	/**
	 * Fields for configuring log throughput messaging. By default throughput logging is disabled.
	 */
	private boolean logThroughputMessage;
	/**
	 * Minutes interval between throughput logging task execution when {@link #isLogThroughputMessage()} returns true. The default is 60 minutes (1 hour).
	 */
	private long logThroughputMessageIntervalMinutes = 60L;
	/**
	 * Fields to track throughput of sent/received messages between throughput log messages.
	 * The counters are incremented for each message if {@link #isLogThroughputMessage()} is true, and each are reset each time the throughput logging task runs.
	 */
	private final LongAdder throughputReceivedCounter = new LongAdder();
	private final LongAdder throughputSentCounter = new LongAdder();
	/**
	 * Field that is initialized to the scheduled throughput logging task when {@link #isLogThroughputMessage()} returns true.
	 */
	private final Lazy<ScheduledFuture<?>> logThroughputMessageFuture = new Lazy<>(() -> {
		Runnable task = () -> {
			long sent = getThroughputSentCounter().sumThenReset();
			long received = getThroughputReceivedCounter().sumThenReset();
			if (sent > 0 || received > 0) {
				String processorTypeName = Optional.ofNullable(getProcessor()).map(p -> p.getClass().getSimpleName()).orElse(null);
				String identifier = ObjectUtils.coalesce(getRequestStatsSource(), processorTypeName, getOutgoingQueue(), getApplicationName());
				LogUtils.info(LogCommand.ofMessage(getClass(), "[", identifier, "] messages processed in the last", getLogThroughputMessageIntervalMinutes(), "minutes: sent (", sent, "), received (", received, ")"));
			}
		};
		return throughputLoggingExecutorService.get().scheduleAtFixedRate(task, getLogThroughputMessageIntervalMinutes(), getLogThroughputMessageIntervalMinutes(), TimeUnit.MINUTES);
	});

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Receives JMS message (this is implemented from javax.jms.MessageListener).
	 */
	@Override
	public void onMessage(final Message message) {
		AssertUtils.assertNotNull(getProcessor(), "Messages cannot be received without a processor defined!");
		if (message == null) {
			LogUtils.error(getClass(), "Null message received on the incoming queue for the '" + getOutgoingQueue() + "' outgoing queue.");
			return;
		}
		try {
			if (isAsynchronousMessageProcessing()) {
				final AsynchronousMessageHandlerImpl handler = this;
				getExecutorService().get().submit(() -> handler.processMessage(message));
			}
			else {
				processMessage(message);
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to processMessage: " + e.getMessage(), e);
		}
	}


	@Override
	public void send(AsynchronousMessage requestMessage) {
		send(requestMessage, null);
	}


	@Override
	public void send(AsynchronousMessage requestMessage, String correlationId) {
		incrementThroughputCounter(getThroughputSentCounter());
		doSend(requestMessage, correlationId, getOutgoingQueue());
	}


	@PreDestroy
	public void stop() {
		if (isAsynchronousMessageProcessing() && getExecutorService().isComputed()) {
			if (!ConcurrentUtils.shutdownExecutorService(getExecutorService().get())) {
				LogUtils.error(getClass(), "Failed to shutdown the [" + getThreadPoolName() + "] executor service.");
			}
		}
		if (isLogThroughputMessage() && throughputLoggingExecutorService.isComputed()) {
			ConcurrentUtils.shutdownExecutorService(throughputLoggingExecutorService.get());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processMessage(final Message message) {
		incrementThroughputCounter(getThroughputReceivedCounter());
		if (isRequestStatsEnabled()) {
			getTimerHandler().startTimer();
		}
		AsynchronousMessage response = null;
		try {
			response = (AsynchronousMessage) JmsUtils.deserializeMessage(message, getXStreamInstance(), getJmsTemplate().getMessageConverter());
			String targetApplication = response.getProperties().get(MessagingMessage.TARGET_APPLICATION);
			if (targetApplication == null || StringUtils.isEqual(getApplicationName(), targetApplication)) {
				getProcessor().process(response, this);
			}
			else {
				LogUtils.warn(AsynchronousMessageHandlerImpl.class, "Message not processed!  TargetApplication [" + targetApplication + "] is not set to 'ALL' or does not match applicationName [" + getApplicationName() + "]");
			}
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to process message", e);
			postError(e, message);
		}
		finally {
			// stop the timer and record stats
			if (isRequestStatsEnabled()) {
				try {
					Object user = getContextHandler().getBean(Context.USER_BEAN_NAME);
					String uri = getBrokerUrl() + (response != null ? ("_" + response.getClass().getSimpleName()) : "");
					getTimerHandler().stopTimer();
					getSystemRequestStatsService().saveSystemRequestStats(getRequestStatsSource(), uri, user, getTimerHandler().getDurationNano(), 0, 0);
				}
				finally {
					getTimerHandler().reset();
				}
			}
		}
	}


	private void doSend(final AsynchronousMessage requestMessage, final String correlationId, String queue) {
		AssertUtils.assertNotNull(getJmsTemplate(), "Messages cannot be sent without a JMS template defined!");
		JmsMessageCreatorCommand messageCreatorCommand = new JmsMessageCreatorCommand();
		messageCreatorCommand.setCorrelationId(correlationId);
		messageCreatorCommand.setMessagingMessage(requestMessage);
		messageCreatorCommand.setDefaultMessageProperties(getDefaultMessageProperties());
		messageCreatorCommand.setMessageConverter(getJmsTemplate().getMessageConverter());
		messageCreatorCommand.setMessageSource(getMessageSource() + "_" + requestMessage.getProperties().get("TargetApplication"));
		messageCreatorCommand.setDefaultMessageType(getDefaultMessageType());
		MessageCreator messageCreator = JmsMessageCreatorUtils.getRequestMessageCreator(messageCreatorCommand);

		//Execute the request
		Destination destination = getJmsTemplate().getDefaultDestination();
		getJmsTemplate().execute(destination, new AsynchronousProducerCallback(messageCreator, queue, getJmsTemplate()));
	}


	private void postError(Throwable e, final Message message) {
		if (!StringUtils.isEmpty(getErrorQueue())) {
			try {
				JmsAsynchronousErrorMessage errorMessage = new JmsAsynchronousErrorMessage();
				errorMessage.setMessage(message);
				errorMessage.setError(e);
				errorMessage.setServer(getBrokerUrl());
				errorMessage.setTargetQueue(message.getJMSDestination().toString().replace("queue://", ""));
				errorMessage.getProperties().put("MessageKey", message.getJMSMessageID() + "_" + errorMessage.getTargetQueue());

				if ((getErrorQueue() != null) && !getErrorQueue().isEmpty()) {
					doSend(errorMessage, message.getJMSCorrelationID(), getErrorQueue());
				}
				else {
					doSend(errorMessage, message.getJMSCorrelationID(), getOutgoingQueue());
				}
			}
			catch (Exception ex) {
				LogUtils.error(getClass(), "Failed to post error message to error queue.", ex);
				throw new RuntimeException(ex);
			}
		}
	}


	private boolean isRequestStatsEnabled() {
		return !StringUtils.isEmpty(getRequestStatsSource());
	}


	private void incrementThroughputCounter(LongAdder throughputCounter) {
		if (isLogThroughputMessage()) {
			throughputCounter.increment();
			if (!getLogThroughputMessageFuture().isComputed()) {
				// Get the future, which will initialize it only if not yet computed. This will schedule the logging task.
				getLogThroughputMessageFuture().get();
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public int getMaxWorkerThreadCount() {
		return this.maxWorkerThreadCount;
	}


	public void setMaxWorkerThreadCount(int maxWorkerThreadCount) {
		this.maxWorkerThreadCount = maxWorkerThreadCount;
	}


	public Lazy<ExecutorService> getExecutorService() {
		return this.executorService;
	}


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public String getApplicationName() {
		return this.applicationName;
	}


	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}


	public String getMessageSource() {
		return this.messageSource;
	}


	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}


	public String getOutgoingQueue() {
		return this.outgoingQueue;
	}


	public void setOutgoingQueue(String outgoingQueue) {
		this.outgoingQueue = outgoingQueue;
	}


	public String getErrorQueue() {
		return this.errorQueue;
	}


	public void setErrorQueue(String errorQueue) {
		this.errorQueue = errorQueue;
	}


	public AsynchronousProcessor<AsynchronousMessage> getProcessor() {
		return this.processor;
	}


	public void setProcessor(AsynchronousProcessor<AsynchronousMessage> processor) {
		this.processor = processor;
	}


	public Map<String, String> getDefaultMessageProperties() {
		return this.defaultMessageProperties;
	}


	public void setDefaultMessageProperties(Map<String, String> defaultMessageProperties) {
		this.defaultMessageProperties = defaultMessageProperties;
	}


	public String getBrokerUrl() {
		return this.brokerUrl;
	}


	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}


	public String getRequestStatsSource() {
		return this.requestStatsSource;
	}


	public void setRequestStatsSource(String requestStatsSource) {
		this.requestStatsSource = requestStatsSource;
	}


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public boolean isAsynchronousMessageProcessing() {
		return this.asynchronousMessageProcessing;
	}


	public void setAsynchronousMessageProcessing(boolean asynchronousMessageProcessing) {
		this.asynchronousMessageProcessing = asynchronousMessageProcessing;
	}


	public String getThreadPoolName() {
		return this.threadPoolName;
	}


	public void setThreadPoolName(String threadPoolName) {
		this.threadPoolName = threadPoolName;
	}


	public boolean isLogThroughputMessage() {
		return this.logThroughputMessage;
	}


	public void setLogThroughputMessage(boolean logThroughputMessage) {
		this.logThroughputMessage = logThroughputMessage;
	}


	public long getLogThroughputMessageIntervalMinutes() {
		return this.logThroughputMessageIntervalMinutes;
	}


	public void setLogThroughputMessageIntervalMinutes(long logThroughputMessageIntervalMinutes) {
		this.logThroughputMessageIntervalMinutes = logThroughputMessageIntervalMinutes;
	}


	public LongAdder getThroughputReceivedCounter() {
		return this.throughputReceivedCounter;
	}


	public LongAdder getThroughputSentCounter() {
		return this.throughputSentCounter;
	}


	public Lazy<ScheduledFuture<?>> getLogThroughputMessageFuture() {
		return this.logThroughputMessageFuture;
	}
}
