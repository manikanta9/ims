package com.clifton.core.messaging.converter.xml;

import com.clifton.core.util.StringUtils;
import com.thoughtworks.xstream.converters.Converter;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;

import java.util.AbstractMap;
import java.util.HashMap;
import java.util.Map;


/**
 * The {@link MessagingXStreamStringMapConverter} is an XML {@link Converter} which serializes and deserializes map keys and values as {@link String} values. This can significantly
 * reduce the size of serialized content for simple string-to-string maps, where the default serialization strategy would instead serialize each individual field on the {@link
 * Map.Entry} objects.
 * <p>
 * This uses the {@link Object#toString() toString} method on keys and values. If the serialized keys or values are not {@link String} values, then care should be taken to ensure
 * that the deserialization receiver can appropriately handle the values' {@link Object#toString() toString} representations.
 *
 * @author MikeH
 */
public class MessagingXStreamStringMapConverter implements Converter {

	@Override
	public boolean canConvert(Class clazz) {
		return AbstractMap.class.isAssignableFrom(clazz);
	}


	@Override
	@SuppressWarnings("rawtypes")
	public void marshal(Object value, HierarchicalStreamWriter writer, @SuppressWarnings("unused") MarshallingContext context) {
		AbstractMap map = (AbstractMap) value;
		for (Object obj : map.entrySet()) {
			java.util.Map.Entry entry = (java.util.Map.Entry) obj;
			writer.startNode("entry");
			writer.addAttribute("key", entry.getKey().toString());
			writer.addAttribute("value", entry.getValue() != null ? entry.getValue().toString() : "");
			writer.endNode();
		}
	}


	@Override
	public Object unmarshal(HierarchicalStreamReader reader, @SuppressWarnings("unused") UnmarshallingContext context) {
		Map<String, String> map = new HashMap<>();
		while (reader.hasMoreChildren()) {
			reader.moveDown();
			String value = reader.getAttribute("value");
			map.put(reader.getAttribute("key"), !StringUtils.isEmpty(value) ? value : reader.getValue());
			reader.moveUp();
		}
		return map;
	}
}
