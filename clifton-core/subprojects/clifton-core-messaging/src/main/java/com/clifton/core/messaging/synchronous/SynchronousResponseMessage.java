package com.clifton.core.messaging.synchronous;


import com.clifton.core.messaging.MessagingMessage;


/**
 * The <code>ResponseMessage</code> interface defines a generic response message.
 */
public interface SynchronousResponseMessage extends MessagingMessage {

	/**
	 * If request processing failed, will return exception that was encountered.
	 */
	public Throwable getError();


	/**
	 * Sets the exception that was encountered while processing the message.
	 *
	 * @param exception
	 */
	public void setError(Throwable exception);


	/**
	 * Overrides the default request timeout.
	 */
	public Integer getRequestTimeout();
}
