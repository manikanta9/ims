package com.clifton.core.messaging;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The {@link MessageType} annotation is used to specify the default message serialization format for the annotated type. This is typically applied to implementations of {@link
 * MessagingMessage}. This is used to populate the {@link MessagingMessage#MESSAGE_CONTENT_TYPE_PROPERTY_NAME} property to determine content-type serialization.
 *
 * @author mwacker
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface MessageType {

	MessageTypes value();
}
