package com.clifton.core.messaging.jms;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.SystemUtils;
import org.springframework.jms.core.MessageCreator;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.Message;
import javax.jms.Session;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>JmsMessageCreatorUtils</code> centralizes the creation of JMS MessageCreator objects
 * for re-use across message producers.
 */
public class JmsMessageCreatorUtils {

	private static final String SCHEDULED_DELAY_PROPERTY = "AMQ_SCHEDULED_DELAY";
	private static final String SCHEDULED_PERIOD_PROPERTY = "AMQ_SCHEDULED_PERIOD";
	private static final String SCHEDULED_REPEAT_PROPERTY = "AMQ_SCHEDULED_REPEAT";

	/**
	 * The cache of {@link Class} instances to corresponding {@link MessageTypes default message type} values.
	 *
	 * @see #getMessageType(MessagingMessage)
	 */
	private static final Map<Class<?>, MessageTypes> classToDefaultMessageTypeCache = new ConcurrentHashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static MessageCreator getRequestMessageCreator(JmsMessageCreatorCommand messageCreatorCommand) {
		MessagingMessage messagingMessage = messageCreatorCommand.getMessagingMessage();
		boolean synchronousMessage = messagingMessage instanceof SynchronousRequestMessage;
		return session -> {
			// add the machine name to the message
			String machineName = SystemUtils.getMachineName();
			messagingMessage.setMachineName(machineName);

			// Collect request properties - must be done prior to serializing the message because defaults may be populated
			Map<String, String> requestProperties = JmsUtils.consolidateMessageProperties(messagingMessage, messageCreatorCommand.getDefaultMessageProperties());

			// serialize the message
			MessageTypes messageType = ObjectUtils.coalesce(getMessageType(messagingMessage), messageCreatorCommand.getDefaultMessageType());
			Message message = createMessage(messagingMessage, session, messageType, messageCreatorCommand.getMessageConverter());

			// add the machine name to the actual JMS message
			message.setStringProperty(MessagingMessage.MESSAGE_MACHINE_NAME_PROPERTY_NAME, machineName);
			// add message source if not set; can happen if the request's property map is null
			if (!message.propertyExists(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME)) {
				message.setStringProperty(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME, messageCreatorCommand.getMessageSource());
			}
			if (synchronousMessage) {
				message.setStringProperty(MessagingMessage.MESSAGE_RESPONSE_CLASS_PROPERTY_NAME, messageCreatorCommand.getResponseClassName());
			}
			if (!synchronousMessage) {
				AsynchronousMessage asynchronousMessage = (AsynchronousMessage) messagingMessage;
				if (asynchronousMessage.getMessageDelaySeconds() != null) {
					message.setLongProperty(SCHEDULED_DELAY_PROPERTY, 0);
					message.setLongProperty(SCHEDULED_PERIOD_PROPERTY, (long) (asynchronousMessage.getMessageDelaySeconds()) * 1000);
					message.setLongProperty(SCHEDULED_REPEAT_PROPERTY, 0);
				}
				message.setJMSCorrelationID(StringUtils.isEmpty(messageCreatorCommand.getCorrelationId()) ? UUID.randomUUID().toString() : messageCreatorCommand.getCorrelationId());
			}
			// add request properties to the JMS message
			JmsUtils.populateMessageProperties(message, requestProperties);
			return message;
		};
	}


	public static MessageCreator getResponseMessageCreator(JmsMessageCreatorCommand messageCreatorCommand) {
		MessagingMessage messagingMessage = messageCreatorCommand.getMessagingMessage();
		return session -> {
			// add the machine name to the message
			String machineName = SystemUtils.getMachineName();
			messagingMessage.setMachineName(machineName);
			MessageTypes messageType = ObjectUtils.coalesce(getMessageType(messagingMessage), messageCreatorCommand.getDefaultMessageType());
			Message returnMessage = createMessage(messagingMessage, session, messageType, messageCreatorCommand.getMessageConverter());

			// add the machine name to the actual JMS message
			returnMessage.setStringProperty(MessagingMessage.MESSAGE_MACHINE_NAME_PROPERTY_NAME, machineName);
			returnMessage.setJMSCorrelationID(messageCreatorCommand.getCorrelationId());
			return returnMessage;
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a {@link Message} from the given {@link MessagingMessage}. The message will be serialized for the given {@link Session} using the provided arguments.
	 */
	public static Message createMessage(MessagingMessage messagingMessage, Session session, MessageTypes messageType, MessageConverter messageConverter) {
		try {
			messagingMessage.getProperties().put(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME, messageType.getContentType());
			Message message = messageConverter.toMessage(messagingMessage, session);
			message.setStringProperty(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME, messageType.getContentType());
			return message;
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred during message generation.", e);
		}
	}


	/**
	 * Gets the {@link MessageTypes message type} which shall be used for the given {@link MessagingMessage}. This is determined from the {@value
	 * MessagingMessage#MESSAGE_CONTENT_TYPE_PROPERTY_NAME} property if present, or the class {@link MessageType} annotation if otherwise. If neither of these are present,
	 * <code>null</code> will be returned.
	 */
	public static MessageTypes getMessageType(MessagingMessage message) {
		final MessageTypes messageType;
		String contentType = message.getProperties().get(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME);
		if (contentType != null) {
			// Determine type from content-type property
			MessageTypes result = null;
			for (MessageTypes type : MessageTypes.values()) {
				if (type.getContentType().equals(contentType)) {
					result = type;
					break;
				}
			}
			messageType = result;
		}
		else {
			// Determine type from class designation
			messageType = classToDefaultMessageTypeCache.computeIfAbsent(message.getClass(), clazz -> {
				MessageType typeAnnotation = clazz.getAnnotation(MessageType.class);
				return typeAnnotation != null ? typeAnnotation.value() : null;
			});
		}
		return messageType;
	}
}
