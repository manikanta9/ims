package com.clifton.core.messaging.jms.asynchronous.sender;

import com.clifton.core.messaging.asynchronous.AsynchronousMessage;


/**
 * The <code>AsynchronousMessageSender</code> defines an asynchronous message sender used for send messages.
 *
 * @author vanand
 */
public interface MessageSender {

	/**
	 * Send an asynchronous message to specified destination.
	 */
	public void send(AsynchronousMessage message);
}
