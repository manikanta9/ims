package com.clifton.core.messaging.synchronous;


import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;

import java.io.File;


/**
 * The <code>FileRequestMessage</code> defines a JMS message that sends a file along with the message.
 *
 * @author mwacker
 */
@MessageType(MessageTypes.FILE)
public interface SynchronousFileRequestMessage extends SynchronousRequestMessage {

	public File getFile();


	public void setFile(File file);


	public String getFileName();
}
