package com.clifton.core.messaging.analytics;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.util.StringUtils;

import java.util.Date;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;


/**
 * @author MitchellF
 */
public class BaseMessageServiceAnalytics implements MessageServiceAnalytics {

	private final LongAdder requestCount = new LongAdder();
	private final LongAdder successfulRequests = new LongAdder();
	private final LongAdder failedRequests = new LongAdder();

	/**
	 * All durations are in milliseconds
	 */
	private final AtomicLong averageRequestDuration = new AtomicLong();
	private final AtomicLong minimumRequestDuration = new AtomicLong();
	private final AtomicLong maximumRequestDuration = new AtomicLong();
	private final AtomicLong lastRequestDuration = new AtomicLong();

	private final String name;
	private final String serviceType;

	private volatile String lastErrorMessage;
	private volatile Boolean lastRequestSuccessful;
	private Date lastSuccessfulResponse;
	private Date lastFailedResponse;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public int hashCode() {
		return Objects.hash(this.name);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj instanceof BaseMessageServiceAnalytics) {
			if (this.name.equals(((BaseMessageServiceAnalytics) obj).getName())) {
				return true;
			}
		}
		return false;
	}


	public BaseMessageServiceAnalytics(String name, String serviceType) {
		this.name = name;
		this.serviceType = serviceType;
	}


	@Override
	public void updateAnalytics(Long requestTime, MessagingMessage request, MessagingMessage response, String messageSource) {
		boolean success = wasRequestSuccessful(request, response);

		updateRequestCount(success);
		updateDurations(requestTime);
		this.lastRequestSuccessful = success;

		if (success) {
			this.lastSuccessfulResponse = new Date();
		}
		else {
			if (!StringUtils.isEmpty(getErrorMessageForResponse(request, response))) {
				this.lastErrorMessage = getErrorMessageForResponse(request, response);
			}
			this.lastFailedResponse = new Date();
		}
	}


	@Override
	public void clear() {
		getRequestCount().reset();
		getSuccessfulRequests().reset();
		getFailedRequests().reset();

		getAverageRequestDuration().set(0);
		getMaximumRequestDuration().set(0);
		getMinimumRequestDuration().set(0);
		getLastRequestDuration().set(0);

		this.lastErrorMessage = null;
		this.lastRequestSuccessful = null;
		this.lastFailedResponse = null;
		this.lastSuccessfulResponse = null;
	}


	private boolean wasRequestSuccessful(MessagingMessage request, MessagingMessage response) {
		return (getErrorMessageForResponse(request, response) == null);
	}


	protected String getErrorMessageForResponse(MessagingMessage request, MessagingMessage response) {

		StringBuilder messageToReturnSb = new StringBuilder();

		Throwable err = (Throwable) BeanUtils.getPropertyValue(response, "error");
		if (err != null) {
			messageToReturnSb.append(err.getMessage());
		}

		if (messageToReturnSb.length() > 0) {
			return messageToReturnSb.append("\nRequest Details: ").append(request).toString();
		}
		return null;
	}


	private void updateRequestCount(boolean success) {
		this.requestCount.increment();
		if (success) {
			incrementSuccessfulRequests();
		}
		else {
			incrementFailedRequests();
		}
	}


	private void incrementSuccessfulRequests() {
		this.successfulRequests.increment();
	}


	private void incrementFailedRequests() {
		this.failedRequests.increment();
	}


	private void updateDurations(Long newTime) {

		this.averageRequestDuration.updateAndGet(currentValue -> {
			if (currentValue == 0) {
				return newTime;
			}
			long storedRequestCount = this.requestCount.sum();
			return ((currentValue * storedRequestCount) + newTime) / (storedRequestCount + 1);
		});

		this.minimumRequestDuration.updateAndGet(currentVal -> (currentVal == 0 || newTime < currentVal) ? newTime : currentVal);
		this.maximumRequestDuration.updateAndGet(currentVal -> (currentVal == 0 || newTime > currentVal) ? newTime : currentVal);

		this.lastRequestDuration.set(newTime);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////////                  Getters and setters                       //////////
	////////////////////////////////////////////////////////////////////////////////


	public LongAdder getRequestCount() {
		return this.requestCount;
	}


	public LongAdder getSuccessfulRequests() {
		return this.successfulRequests;
	}


	public LongAdder getFailedRequests() {
		return this.failedRequests;
	}


	public AtomicLong getAverageRequestDuration() {
		return this.averageRequestDuration;
	}


	public AtomicLong getMinimumRequestDuration() {
		return this.minimumRequestDuration;
	}


	public AtomicLong getMaximumRequestDuration() {
		return this.maximumRequestDuration;
	}


	public AtomicLong getLastRequestDuration() {
		return this.lastRequestDuration;
	}


	public String getName() {
		return this.name;
	}


	public String getServiceType() {
		return this.serviceType;
	}


	public String getLastErrorMessage() {
		return this.lastErrorMessage;
	}


	public Boolean getLastRequestSuccessful() {
		return this.lastRequestSuccessful;
	}


	public Date getLastSuccessfulResponse() {
		return this.lastSuccessfulResponse;
	}


	public Date getLastFailedResponse() {
		return this.lastFailedResponse;
	}
}
