package com.clifton.core.messaging.jms;


import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.asynchronous.AsynchronousErrorMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProducerCallback;
import com.clifton.core.util.StringUtils;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;


public class JmsQueueServiceImpl<T extends MessagingMessage> extends AbstractXStreamMessageHandler implements JmsQueueService<T> {

	private JmsTemplate jmsTemplate;
	private String messageSelector;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public List<T> getMessages(String queueName) {
		return getMessages(queueName, null);
	}


	@Override
	public List<T> getMessages(String queueName, String additionalMessageSelector) {
		return getJmsTemplate().browseSelected(queueName, getConcatenatedMessageSelector(additionalMessageSelector), (session, browser) -> {
			List<T> messageList = new ArrayList<>();
			@SuppressWarnings("unchecked")
			Enumeration<Message> messages = browser.getEnumeration();
			while (messages.hasMoreElements()) {
				Message message = messages.nextElement();
				messageList.add(convertMessage(message));
			}
			return messageList;
		});
	}


	@Override
	public List<T> receiveMessages(String queueName) {
		return receiveMessages(queueName, null);
	}


	@Override
	public List<T> receiveMessages(String queueName, String additionalMessageSelector) {
		return receiveMessages(queueName, additionalMessageSelector, DEFAULT_RECEIVE_TIMEOUT_MS);
	}


	@Override
	public List<T> receiveMessages(String queueName, String additionalMessageSelector, long receiveTimeoutMs) {
		return getJmsTemplate().execute(queueName, (session, producer) -> {
			List<T> messageList = new ArrayList<>();
			MessageConsumer consumer = session.createConsumer(producer.getDestination(), getConcatenatedMessageSelector(additionalMessageSelector));
			try {
				// Receive all messages
				Message message;
				while ((message = consumer.receive(receiveTimeoutMs)) != null) {
					messageList.add(convertMessage(message));
				}
			}
			finally {
				JmsUtils.closeMessageConsumer(consumer);
			}
			return messageList;
		});
	}


	@Override
	public void purgeMessages(String queueName) {
		purgeMessages(queueName, null);
	}


	@Override
	public void purgeMessages(String queueName, String additionalMessageSelector) {
		purgeMessages(queueName, additionalMessageSelector, DEFAULT_RECEIVE_TIMEOUT_MS);
	}


	@Override
	public void purgeMessages(String queueName, String additionalMessageSelector, long receiveTimeoutMs) {
		getJmsTemplate().execute(queueName, (session, producer) -> {
			MessageConsumer consumer = session.createConsumer(producer.getDestination(), getConcatenatedMessageSelector(additionalMessageSelector));
			try {
				// Receive all messages
				while (consumer.receive(receiveTimeoutMs) != null) {
					// Do nothing
				}
			}
			finally {
				JmsUtils.closeMessageConsumer(consumer);
			}
			return null;
		});
	}


	@Override
	public void moveErrorMessageToTarget(String messageId, String fromQueue, String toQueue) {
		try {
			Message message = getJmsTemplate().receiveSelected(fromQueue, "MessageKey = '" + messageId + "_" + toQueue + "'");
			@SuppressWarnings("unchecked")
			final AsynchronousErrorMessage<Message> response = (AsynchronousErrorMessage<Message>) JmsUtils.deserializeMessage(message, getXStreamInstance(), getJmsTemplate().getMessageConverter());
			getJmsTemplate().execute(new AsynchronousProducerCallback(session -> response.getMessage(), response.getTargetQueue(), getJmsTemplate()));
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Failed to move error message [%s] from error queue [%s] to original target queue [%s].", messageId, fromQueue, toQueue), e);
		}
	}


	@Override
	public void deleteErrorMessageFromQueue(String messageId, String fromQueue, String toQueue) {
		try {
			// Note: There is no API to delete a message, but by calling receiveSelected and NOT doing anything with it, it's taken off of the queue.
			getJmsTemplate().receiveSelected(fromQueue, "MessageKey = '" + messageId + "_" + toQueue + "'");
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Failed to delete error message [%s] from error queue [%s] originally intended for target queue [%s].", messageId, fromQueue, toQueue), e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the result of the default message selector ({@link #messageSelector}) concatenated with the provided <code>additionalMessageSelector</code> via the <tt>AND</tt>
	 * conjunction.
	 */
	private String getConcatenatedMessageSelector(String additionalMessageSelector) {
		String concatenatedMessageSelector = getMessageSelector();
		if (!StringUtils.isEmpty(additionalMessageSelector)) {
			concatenatedMessageSelector += " AND (" + additionalMessageSelector + ")";
		}
		return concatenatedMessageSelector;
	}


	/**
	 * Deserializes the provided message.
	 */
	private T convertMessage(Message message) throws JMSException {
		@SuppressWarnings("unchecked")
		T response = (T) JmsUtils.deserializeMessage(message, getXStreamInstance(), getJmsTemplate().getMessageConverter());
		if (response instanceof AsynchronousErrorMessage) {
			@SuppressWarnings("unchecked")
			AsynchronousErrorMessage<Message> errorResponse = (AsynchronousErrorMessage<Message>) response;
			errorResponse.setErrorMessageId(message.getJMSMessageID());
			errorResponse.setMessageTimestamp(new Date(message.getJMSTimestamp()));
		}
		return response;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public String getMessageSelector() {
		return this.messageSelector;
	}


	public void setMessageSelector(String messageSelector) {
		this.messageSelector = messageSelector;
	}
}
