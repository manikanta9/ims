package com.clifton.core.messaging.asynchronous;


import java.util.Date;


/**
 * The <code>AsynchronousErrorMessage</code> defines an wrapper for a message that failed to process.
 * <p/>
 * For example, in the trade-fix project when a FIX message received via JMS fails to process, an AsynchronousErrorMessage
 * is created and placed on the error queue.
 *
 * @author mwacker
 */
public interface AsynchronousErrorMessage<T> extends AsynchronousMessage {

	/**
	 * The error the occurred in processing.
	 */
	public Throwable getError();


	public void setError(Throwable exception);


	/**
	 * The original message the caused the error.
	 */
	public T getMessage();


	/**
	 * The JMS server where the target JMS queue resides.
	 */
	public String getServer();


	/**
	 * The queue for the original message.  This is used to move the failed message back to the original for re-processing.
	 */
	public String getTargetQueue();


	/**
	 * The id of the failed message.
	 */
	public String getMessageId();


	/**
	 * The id of the error message.
	 */
	public String getErrorMessageId();


	public void setErrorMessageId(String errorMessageId);


	public Date getMessageTimestamp();


	public void setMessageTimestamp(Date messageTimestamp);
}
