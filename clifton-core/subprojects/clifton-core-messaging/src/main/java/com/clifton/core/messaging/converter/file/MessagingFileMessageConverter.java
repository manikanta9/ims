package com.clifton.core.messaging.converter.file;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.synchronous.SynchronousFileRequestMessage;
import com.clifton.core.util.AssertUtils;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.BlobMessage;
import org.apache.activemq.jms.pool.PooledSession;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;

import javax.jms.Message;
import javax.jms.Session;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;


/**
 * The {@link MessagingFileMessageConverter} handles conversion to and from file messages.
 * <p>
 * File messages are only supported for {@link SynchronousFileRequestMessage} implementors. These messages are serialized as {@link BlobMessage} messages, and other message
 * contents (such as {@link SynchronousFileRequestMessage#getProperties() wrapped properties}) are serialized into the {@value MessagingMessage#MESSAGE_OBJECT_XML_PROPERTY_NAME}
 * message property.
 *
 * @author MikeH
 */
public class MessagingFileMessageConverter implements MessageConverter {

	private final Marshaller marshaller;
	private final Unmarshaller unmarshaller;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MessagingFileMessageConverter(Marshaller marshaller, Unmarshaller unmarshaller) {
		this.marshaller = marshaller;
		this.unmarshaller = unmarshaller;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message toMessage(Object object, Session session) {
		AssertUtils.assertTrue(object instanceof SynchronousFileRequestMessage, "Invalid object type: [%s]. File message conversion is only supported for synchronous file messages (objects of type [%s]).", object.getClass().getName(), SynchronousFileRequestMessage.class.getName());
		try {
			// Generate blob message for file contents
			@SuppressWarnings("ConstantConditions")
			SynchronousFileRequestMessage synchronousFileRequestMessage = (SynchronousFileRequestMessage) object;
			PooledSession ps = (PooledSession) session;
			ActiveMQSession s = (ActiveMQSession) ps.getInternalSession();
			Message message = s.createBlobMessage(synchronousFileRequestMessage.getFile());
			// Clear file field and serialize remaining content as message property
			synchronousFileRequestMessage.setFile(null);
			StringWriter xmlOutputWriter = new StringWriter();
			getMarshaller().marshal(synchronousFileRequestMessage, new StreamResult(xmlOutputWriter));
			message.setStringProperty(MessagingMessage.MESSAGE_OBJECT_XML_PROPERTY_NAME, xmlOutputWriter.toString());
			return message;
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while converting the object to a file message.", e);
		}
	}


	@Override
	public Object fromMessage(Message message) {
		AssertUtils.assertTrue(message instanceof BlobMessage, "Invalid message type: [%s]. Blob message conversion is only supported for messages of type [%s].", message.getClass().getName(), BlobMessage.class.getName());
		try {
			// Deserialize original message from property
			@SuppressWarnings("ConstantConditions")
			BlobMessage blobMessage = (BlobMessage) message;
			String messageXml = message.getStringProperty(MessagingMessage.MESSAGE_OBJECT_XML_PROPERTY_NAME);
			StreamSource messageSource = new StreamSource(new StringReader(messageXml));
			Object messageXmlObject = getUnmarshaller().unmarshal(messageSource);
			AssertUtils.assertTrue(messageXmlObject instanceof SynchronousFileRequestMessage, "Unable to unmarshall blob messages with the [%s] property type [%s]. Blob message conversion is only supported for original messages of type [%s].", MessagingMessage.MESSAGE_OBJECT_XML_PROPERTY_NAME, messageXmlObject.getClass().getName(), SynchronousFileRequestMessage.class.getName());
			SynchronousFileRequestMessage fileRequestMessage = (SynchronousFileRequestMessage) messageXmlObject;
			// Attach file object
			File file = FileUtils.convertInputStreamToFile(FileUtils.getFileNameWithoutExtension(fileRequestMessage.getFileName()), FileUtils.getFileExtension(fileRequestMessage.getFileName()), blobMessage.getInputStream());
			fileRequestMessage.setFile(file);
			return fileRequestMessage;
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while converting the message to an object.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Marshaller getMarshaller() {
		return this.marshaller;
	}


	public Unmarshaller getUnmarshaller() {
		return this.unmarshaller;
	}
}
