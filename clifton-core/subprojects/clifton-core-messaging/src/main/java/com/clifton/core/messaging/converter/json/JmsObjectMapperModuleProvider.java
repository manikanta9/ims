package com.clifton.core.messaging.converter.json;

import com.fasterxml.jackson.databind.Module;


/**
 * The {@link JmsObjectMapperModuleProvider} provides an abstract class for injecting modules into the jmsObjectMapper bean via
 * <code>MessagingJmsConfiguration</code>. Implementing classes should be annotated as a spring component and implement the
 * getModule function to return the desired module.
 *
 * @author lnaylor
 */
public interface JmsObjectMapperModuleProvider {

	public Module getModule();
}
