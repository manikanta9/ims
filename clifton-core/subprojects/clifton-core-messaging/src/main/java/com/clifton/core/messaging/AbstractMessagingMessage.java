package com.clifton.core.messaging;

import java.util.HashMap;
import java.util.Map;


/**
 * <code>AbstractMessagingMessage</code> contains some basic functionality for a {@link MessagingMessage}.
 *
 * @author NickK
 */
public class AbstractMessagingMessage implements MessagingMessage {

	/**
	 * Multiple listeners on physically different machines can be configured to serve requests.
	 * Specifies machine name.
	 */
	private String machineName;
	/**
	 * These are the generic parameter/data which both the client and the server side code must be able to communicate with.
	 */
	private final Map<String, String> properties = new HashMap<>();


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getMachineName() {
		return this.machineName;
	}


	@Override
	public void setMachineName(String machineName) {
		this.machineName = machineName;
	}


	@Override
	public Map<String, String> getProperties() {
		return this.properties;
	}
}
