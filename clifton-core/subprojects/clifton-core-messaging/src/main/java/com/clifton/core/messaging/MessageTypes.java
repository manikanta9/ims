package com.clifton.core.messaging;


/**
 * The <code>MessageTypes</code> indicated how a MessagingMessage will be serialized.
 *
 * @author mwacker
 * @see MessageType
 */
public enum MessageTypes {
	/**
	 * Serialize to XML.
	 */
	XML("application/xml"),
	/**
	 * Serialize to JSON.
	 */
	JSON("application/json"),
	/**
	 * Serialize to a byte array.
	 */
	OBJECT("application/x-java-serialized-object"),
	/**
	 * Serialize message to XML, but use a Blob message to send a file.
	 */
	FILE("application/octet-stream");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The content type designator for all messages which use the given {@link MessageTypes message type}.
	 */
	private final String contentType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	MessageTypes(String contentType) {
		this.contentType = contentType;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getContentType() {
		return this.contentType;
	}
}
