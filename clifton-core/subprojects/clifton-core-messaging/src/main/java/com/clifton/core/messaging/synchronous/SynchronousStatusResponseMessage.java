package com.clifton.core.messaging.synchronous;

import com.clifton.core.messaging.MessageStatus;

import java.util.ArrayList;
import java.util.List;


/**
 * This class is used to hold the response status messages for various requests.
 * <p>
 *
 * @author stevenf on 8/2/2016.
 */
public class SynchronousStatusResponseMessage extends AbstractSynchronousResponseMessage {

	private List<MessageStatus> messageStatusList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<MessageStatus> getMessageStatusList() {
		if (this.messageStatusList == null) {
			this.messageStatusList = new ArrayList<>();
		}
		return this.messageStatusList;
	}


	public void setMessageStatusList(List<MessageStatus> messageStatusList) {
		this.messageStatusList = messageStatusList;
	}
}
