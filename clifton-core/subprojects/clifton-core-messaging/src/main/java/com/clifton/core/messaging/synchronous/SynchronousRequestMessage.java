package com.clifton.core.messaging.synchronous;


import com.clifton.core.messaging.MessagingMessage;


/**
 * The <code>RequestMessage</code> interface defines a generic request message.
 */
public interface SynchronousRequestMessage extends MessagingMessage {

	/**
	 * Return the class the will be used by the server to respond to a request message.
	 */
	public Class<?> getResponseClass();


	/**
	 * Overrides the default request timeout.
	 */
	public Integer getRequestTimeout();


	/**
	 * Overrides the consecutive error count logic
	 */
	public default Boolean getConsecutiveErrorCountDisabled() {
		return Boolean.FALSE;
	}


	/**
	 * Overrides the number of a message should be retried
	 */
	public default Integer getRetryCountOverride() {
		return null;
	}
}
