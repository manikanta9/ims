package com.clifton.core.messaging.jms.job;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.messaging.jms.JmsQueueService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.TimeTrackedOperation;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;

import java.time.Duration;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The {@link JmsPurgeQueueJob} is a {@link Task} batch job implementation intended for purging messages from a JMS queue. Jobs can be configured to purge all messages or only
 * those entered before a certain relative date.
 *
 * @author MikeH
 */
public class JmsPurgeQueueJob implements Task, ValidationAware {

	private ApplicationContextService applicationContextService;

	private String jmsQueueServiceBeanName;
	private List<String> queueNameList;
	private Integer expiryDays;
	private Integer timeoutMs;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		// Get expiry date string and selector
		Date expiryDaysDate = getExpiryDaysDate();
		String expiryDaysDateStr = DateUtils.fromDate(expiryDaysDate);
		String additionalMessageSelector = "JMSTimestamp < " + expiryDaysDate.getTime();

		// Run purges
		JmsQueueService<?> jmsQueueService = getJmsQueueServiceBean();
		List<String> statusMessageList = new ArrayList<>();
		for (String queueName : getQueueNameList()) {
			try {
				// Purge queue
				TimeTrackedOperation<? extends List<?>> purgeQueueOp = TimeUtils.getElapsedTime(() -> getTimeoutMs() != null
						? jmsQueueService.receiveMessages(queueName, additionalMessageSelector, getTimeoutMs())
						: jmsQueueService.receiveMessages(queueName, additionalMessageSelector));
				statusMessageList.add(String.format("Cleared [%d] messages from queue [%s] as of [%s] (duration: [%s] seconds).", CollectionUtils.getSize(purgeQueueOp.getResult()), queueName, expiryDaysDateStr, purgeQueueOp.getElapsedSeconds()));
			}
			catch (Exception e) {
				statusMessageList.add(String.format("Error clearing messages from queue [%s] as of [%s]: %s", queueName, expiryDaysDateStr, ExceptionUtils.getOriginalMessage(e)));
			}
		}
		return Status.ofMessage(StringUtils.collectionToDelimitedString(statusMessageList, "\n\n"));
	}


	@Override
	public void validate() throws ValidationException {
		Collection<String> jmsQueueServiceBeanNameList = getApplicationContextService().getContextBeansOfType(JmsQueueService.class).keySet();
		ValidationUtils.assertTrue(
				jmsQueueServiceBeanNameList.contains(getJmsQueueServiceBeanName()),
				() -> String.format("A Spring context bean of name [%s] and type [%s] could not be found. Available names: %s", getJmsQueueServiceBeanName(), JmsQueueService.class.getSimpleName(), CollectionUtils.toString(jmsQueueServiceBeanNameList, 10)),
				"jmsQueueServiceBeanName"
		);
		if (getExpiryDays() != null) {
			ValidationUtils.assertTrue(getExpiryDays() >= 0, "If provided, the expiry days field must be greater than or equal to zero.", "expiryDays");
		}
		if (getTimeoutMs() != null) {
			ValidationUtils.assertTrue(getTimeoutMs() > 0, "If provided, the timeout field must be greater than zero.", "timeoutMs");
			ValidationUtils.assertTrue(Duration.ofMillis(getTimeoutMs()).compareTo(Duration.ofMinutes(30)) <= 0, "If provided, the timeout field must be no greater than 30 minutes.", "timeoutMs");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private JmsQueueService<?> getJmsQueueServiceBean() {
		return getApplicationContextService().getContextBean(getJmsQueueServiceBeanName(), JmsQueueService.class);
	}


	private Date getExpiryDaysDate() {
		// Guard-clause: Use current date
		if (getExpiryDays() == null) {
			return new Date();
		}
		Date expiryDateWithTime = DateUtils.addDays(new Date(), getExpiryDays() - 1);
		Date expiryDate = DateUtils.clearTime(expiryDateWithTime);
		return expiryDate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public String getJmsQueueServiceBeanName() {
		return this.jmsQueueServiceBeanName;
	}


	public void setJmsQueueServiceBeanName(String jmsQueueServiceBeanName) {
		this.jmsQueueServiceBeanName = jmsQueueServiceBeanName;
	}


	public List<String> getQueueNameList() {
		return this.queueNameList;
	}


	public void setQueueNameList(List<String> queueNameList) {
		this.queueNameList = queueNameList;
	}


	public Integer getExpiryDays() {
		return this.expiryDays;
	}


	public void setExpiryDays(Integer expiryDays) {
		this.expiryDays = expiryDays;
	}


	public Integer getTimeoutMs() {
		return this.timeoutMs;
	}


	public void setTimeoutMs(Integer timeoutMs) {
		this.timeoutMs = timeoutMs;
	}
}
