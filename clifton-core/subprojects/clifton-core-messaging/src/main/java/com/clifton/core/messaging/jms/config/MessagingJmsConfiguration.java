package com.clifton.core.messaging.jms.config;

import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.converter.ContentTypeDelegatingMessageConverter;
import com.clifton.core.messaging.converter.file.MessagingFileMessageConverter;
import com.clifton.core.messaging.converter.json.JmsMessagingModule;
import com.clifton.core.messaging.converter.json.JmsObjectMapperModuleProvider;
import com.clifton.core.messaging.converter.object.MessagingObjectMessageConverter;
import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.core.util.AssertUtils;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.Module;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.thoughtworks.xstream.XStream;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MarshallingMessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.oxm.Marshaller;
import org.springframework.oxm.Unmarshaller;
import org.springframework.oxm.xstream.XStreamMarshaller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * The {@link MessagingJmsConfiguration} {@link Configuration} provides bean definitions for JMS message conversion.
 *
 * @author MikeH
 */
@Configuration
public class MessagingJmsConfiguration {

	////////////////////////////////////////////////////////////////////////////
	////////            Message Converters                              ////////
	////////////////////////////////////////////////////////////////////////////


	@Bean
	public MessageConverter jmsMessageConverter(
			MessageConverter jmsJacksonMessageConverter,
			MessageConverter jmsXmlMessageConverter,
			MessageConverter jmsObjectMessageConverter,
			MessageConverter jmsFileMessageConverter
	) {
		ContentTypeDelegatingMessageConverter converter = new ContentTypeDelegatingMessageConverter();
		converter.addDelegate(MessageTypes.JSON.getContentType(), jmsJacksonMessageConverter);
		converter.addDelegate(MessageTypes.XML.getContentType(), jmsXmlMessageConverter);
		converter.addDelegate(MessageTypes.OBJECT.getContentType(), jmsObjectMessageConverter);
		converter.addDelegate(MessageTypes.FILE.getContentType(), jmsFileMessageConverter);
		converter.setDefaultMessageConverter(jmsJacksonMessageConverter);
		return converter;
	}


	@Bean
	public MessageConverter jmsJacksonMessageConverter(ObjectMapper jmsObjectMapper) {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setObjectMapper(jmsObjectMapper);
		converter.setTypeIdPropertyName(MessagingMessage.MESSAGE_OBJECT_TYPE_PROPERTY_NAME);
		return converter;
	}


	@Bean
	public MessageConverter jmsXmlMessageConverter(Marshaller jmsXmlMarshaller) {
		return new MarshallingMessageConverter(jmsXmlMarshaller);
	}


	@Bean
	public MessageConverter jmsFileMessageConverter(Marshaller jmsXmlMarshaller, Unmarshaller jmsXmlUnmarshaller) {
		return new MessagingFileMessageConverter(jmsXmlMarshaller, jmsXmlUnmarshaller);
	}


	@Bean
	public MessageConverter jmsObjectMessageConverter() {
		return new MessagingObjectMessageConverter();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Bean
	public ObjectMapper jmsObjectMapper(ApplicationContext applicationContext, ObjectProvider<JmsObjectMapperModuleProvider> objectMapperModuleProvider) {
		List<Module> moduleList = new ArrayList<>(Arrays.asList(new Hibernate5Module(), new JmsMessagingModule(), new JavaTimeModule()));
		for (JmsObjectMapperModuleProvider moduleProvider : objectMapperModuleProvider) {
			moduleList.add(moduleProvider.getModule());
		}
		return Jackson2ObjectMapperBuilder.json()
				.applicationContext(applicationContext) // Support autowiring within custom serializers and deserializers
				.modules(moduleList)
				.timeZone("America/Chicago")
				.serializationInclusion(JsonInclude.Include.NON_NULL)
				.featuresToEnable(
						MapperFeature.BLOCK_UNSAFE_POLYMORPHIC_BASE_TYPES, // Security feature; prevent deserialization of insecure types
						SerializationFeature.INDENT_OUTPUT // Enable pretty-printing for easier debugging
				)
				.featuresToDisable(
						DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES,
						MapperFeature.DEFAULT_VIEW_INCLUSION,
						SerializationFeature.FAIL_ON_EMPTY_BEANS,
						SerializationFeature.WRITE_DATES_AS_TIMESTAMPS // Use strings for dates instead of epoch-time longs
				)
				.build();
	}


	@Bean
	public Marshaller jmsXmlMarshaller(ObjectProvider<MessagingXStreamConfigurer> xmlMarshallerConfigurerProvider) {
		return new XStreamMarshaller() {
			@Override
			protected void customizeXStream(XStream xstream) {
				// Apply all customizers
				for (MessagingXStreamConfigurer configurer : xmlMarshallerConfigurerProvider) {
					configurer.configure(xstream);
				}
			}
		};
	}


	@Bean
	public Unmarshaller jmsXmlUnmarshaller(Marshaller jmsXmlMarshaller) {
		AssertUtils.assertTrue(jmsXmlMarshaller instanceof Unmarshaller, "Unexpected marshaller type. Found type: [%s]. Expected type: [%s].", jmsXmlMarshaller.getClass().getName(), Unmarshaller.class.getName());
		Unmarshaller unmarshaller = (Unmarshaller) jmsXmlMarshaller;
		return unmarshaller;
	}
}
