package com.clifton.core.messaging.jms.synchronous;


import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;


/**
 * The <code>SynchronousProcessor</code> interface must be implemented by all synchronous JMS message processors.
 *
 * @param <I> Request type
 * @param <O> Response type
 * @author mwacker
 */
public interface SynchronousProcessor<I extends SynchronousRequestMessage, O extends SynchronousResponseMessage> {

	/**
	 * Processes the specified requestMessage and returns corresponding response.
	 */
	public O process(I message);
}
