package com.clifton.core.messaging.converter.xml;

import com.thoughtworks.xstream.XStream;


/**
 * The <code>MessagingXStreamConfigurer</code> defines an object used to configure the an XML for custom XML serialization.
 *
 * @author mwacker
 */
public interface MessagingXStreamConfigurer {

	public void configure(XStream xStream);
}
