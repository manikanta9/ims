package com.clifton.core.messaging.asynchronous;


import com.clifton.core.messaging.MessagingMessage;


/**
 * The <code>AsynchronousMessage</code> defines an asynchronous message.
 *
 * @author mwacker
 */
public interface AsynchronousMessage extends MessagingMessage {

	/**
	 * The number of seconds to delay message delivery.
	 */
	public Integer getMessageDelaySeconds();
}
