package com.clifton.core.messaging.jms.synchronous;


import com.clifton.core.messaging.synchronous.SynchronousStatusRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousStatusResponseMessage;


/**
 * The <code>SynchronousProcessor</code> interface must be implemented by all synchronous JMS message processors.
 *
 * @param <I> Request type
 * @param <O> Response type
 * @author mwacker
 */
public interface SynchronousStatusProcessor<I extends SynchronousStatusRequestMessage, O extends SynchronousStatusResponseMessage> {

	/**
	 * Processes the specified requestMessage and returns corresponding response.
	 */
	public O process(I message);
}
