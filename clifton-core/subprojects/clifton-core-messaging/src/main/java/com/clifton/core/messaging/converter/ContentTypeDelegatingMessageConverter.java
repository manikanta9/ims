package com.clifton.core.messaging.converter;

import com.clifton.core.messaging.MessagingMessage;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.Message;
import javax.jms.Session;
import java.util.HashMap;
import java.util.Map;


/**
 * The {@link ContentTypeDelegatingMessageConverter} is a {@link MessageConverter} which delegates conversion based on the content type provided in the given message or object. The
 * message type is inferred from the {@value MessagingMessage#MESSAGE_CONTENT_TYPE_PROPERTY_NAME} {@link Message} property when consuming messages ({@link #fromMessage(Message)})
 * and the {@value MessagingMessage#MESSAGE_CONTENT_TYPE_PROPERTY_NAME} {@link MessagingMessage} property when producing messages ({@link #toMessage(Object, Session)}).
 * <p>
 * This implementation is similar to the type of the same name implemented by the Spring AMQP library:
 * <a href="https://github.com/spring-projects/spring-amqp/blob/30a573db45db9ce1f24b048441033412560b2a82/spring-amqp/src/main/java/org/springframework/amqp/support/converter/ContentTypeDelegatingMessageConverter.java"><code>org.springframework.amqp.support.converter.ContentTypeDelegatingMessageConverter</code></a>
 *
 * @author MikeH
 */
public class ContentTypeDelegatingMessageConverter implements MessageConverter {

	/**
	 * The map of content types to {@link MessageConverter} delegates.
	 */
	private final Map<String, MessageConverter> contentTypeToDelegateMap = new HashMap<>();

	/**
	 * The default message converter to use when mapped delegate is defined for a given content type.
	 */
	private MessageConverter defaultMessageConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Message toMessage(Object object, Session session) {
		try {
			String contentType = (object instanceof MessagingMessage)
					? ((MessagingMessage) object).getProperties().get(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME)
					: null;
			MessageConverter messageConverter = getMessageConverterForContentType(contentType);
			Message message = messageConverter.toMessage(object, session);
			if (contentType != null) {
				message.setStringProperty(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME, contentType);
			}
			return message;
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while converting the object to a message.", e);
		}
	}


	@Override
	public Object fromMessage(Message message) {
		try {
			String contentType = (message.propertyExists(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME))
					? message.getStringProperty(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME)
					: null;
			MessageConverter messageConverter = getMessageConverterForContentType(contentType);
			return messageConverter.fromMessage(message);
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while converting the message to an object.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the message converter for the given content type. If no content type is provided or a matching converter is not found, the {@link #defaultMessageConverter default
	 * message converter} will be returned.
	 */
	private MessageConverter getMessageConverterForContentType(String contentType) {
		// Guard-clause: No content-type provided
		if (contentType == null) {
			return getDefaultMessageConverter();
		}
		return getContentTypeToDelegateMap().getOrDefault(contentType, getDefaultMessageConverter());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addDelegate(String contentType, MessageConverter messageConverter) {
		getContentTypeToDelegateMap().put(contentType, messageConverter);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, MessageConverter> getContentTypeToDelegateMap() {
		return this.contentTypeToDelegateMap;
	}


	public MessageConverter getDefaultMessageConverter() {
		return this.defaultMessageConverter;
	}


	public void setDefaultMessageConverter(MessageConverter defaultMessageConverter) {
		this.defaultMessageConverter = defaultMessageConverter;
	}
}

