package com.clifton.core.messaging.jms;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.converter.xml.MessagingXStreamConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.beans.Lazy;
import com.thoughtworks.xstream.XStream;

import java.util.List;


/**
 * The <code>AbstractXStreamMessageHandler</code> is a abstract class that can be extended for processing XML messages via JMS.
 * A collection of {@link MessagingXStreamConfigurer} items can be configured to generate a thread-safe instance of {@link XStream} for serializing and deserializing XML messages.
 *
 * @author NickK
 */
public abstract class AbstractXStreamMessageHandler {

	/**
	 * The reference to the generated {@link XStream} instance.
	 * <p>
	 * This item is created once because creation is expensive and the object is thread safe.
	 */
	private final Lazy<XStream> xStreamInstance = new Lazy<>(this::createXStreamInstance);
	/**
	 * The list of XStream configurers for custom XML deserialization.
	 */
	private List<MessagingXStreamConfigurer> xmlXStreamConfigurerList;
	/**
	 * The default content type for all serialized messages. This will be used if a {@link MessageType} is not explicitly designated for the serialized object.
	 */
	private MessageTypes defaultMessageType = MessageTypes.XML;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	// Protected getter
	protected XStream getXStreamInstance() {
		return this.xStreamInstance.get();
	}


	protected XStream createXStreamInstance() {
		XStream stream = new XStream();
		for (MessagingXStreamConfigurer config : CollectionUtils.getIterable(getXmlXStreamConfigurerList())) {
			config.configure(stream);
		}
		return stream;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<MessagingXStreamConfigurer> getXmlXStreamConfigurerList() {
		return this.xmlXStreamConfigurerList;
	}


	public void setXmlXStreamConfigurerList(List<MessagingXStreamConfigurer> xmlXStreamConfigurerList) {
		this.xmlXStreamConfigurerList = xmlXStreamConfigurerList;
	}


	public MessageTypes getDefaultMessageType() {
		return this.defaultMessageType;
	}


	public void setDefaultMessageType(MessageTypes defaultMessageType) {
		this.defaultMessageType = defaultMessageType;
	}
}
