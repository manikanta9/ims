package com.clifton.core.messaging.jms;

import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.SystemUtils;
import org.springframework.jms.listener.DefaultMessageListenerContainer;

import javax.annotation.PostConstruct;


/**
 * <code>JmsMachineSelectorMessageListenerContainer</code> extends {@link DefaultMessageListenerContainer} by using
 * a {@link PostConstruct} annotation to update the message selector to include a selector condition for the current
 * machine name, obtained using {@link SystemUtils#getMachineName()}.
 * <p>
 * The machine selector can be configured to be added to the configured selector {@link #getMessageSelector()} with AND or OR.
 *
 * @author NickK
 */
public class JmsMachineSelectorMessageListenerContainer extends DefaultMessageListenerContainer {

	/**
	 * Defines whether the machine selector condition is appended to the existing  {@link #getMessageSelector()} with AND or OR.
	 * The default is false, which will update the configured selector to AND the configured selector with the machine name selector.
	 * e.g. selector "MessageSource = 'TERMINAL'" will become "MessageSource = 'TERMINAL' AND machineName='serverName'"
	 */
	private boolean useOrLogicalOperator;

	///////////////////////////////////////////////////////////////////////////


	@PostConstruct
	public void initializeMessageSelector() {
		StringBuilder builder = new StringBuilder(MessagingMessage.MESSAGE_MACHINE_NAME_PROPERTY_NAME)
				.append("='").append(SystemUtils.getMachineName()).append('\'');
		if (!StringUtils.isEmpty(getMessageSelector())) {
			if (isUseOrLogicalOperator()) {
				builder.insert(0, ") OR ")
						.insert(0, getMessageSelector())
						.insert(0, '(');
			}
			else {
				builder.insert(0, ") AND ")
						.insert(0, getMessageSelector())
						.insert(0, '(');
			}
		}
		setMessageSelector(builder.toString());
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isUseOrLogicalOperator() {
		return this.useOrLogicalOperator;
	}


	public void setUseOrLogicalOperator(boolean useOrLogicalOperator) {
		this.useOrLogicalOperator = useOrLogicalOperator;
	}
}
