package com.clifton.core.messaging.jms.asynchronous.sender;

import com.clifton.core.messaging.MessageType;
import com.clifton.core.messaging.MessageTypes;

import java.util.Map;


/**
 * This class sets the Message Sender Context parameters.
 * This is used by {@link MessageSenderProvider} for generating {@link MessageSender} instances
 *
 * @author vishala
 */
public class MessageSenderContext {

	private String outgoingQueue;

	/**
	 * A list of default properties to append to out going messages.
	 */
	private Map<String, String> defaultMessageProperties;

	/**
	 * The default content type for all serialized messages. This will be used if a {@link MessageType} is not explicitly designated for the serialized object.
	 */
	private MessageTypes defaultMessageType = MessageTypes.XML;

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public String getOutgoingQueue() {
		return this.outgoingQueue;
	}


	public void setOutgoingQueue(String outgoingQueue) {
		this.outgoingQueue = outgoingQueue;
	}


	public Map<String, String> getDefaultMessageProperties() {
		return this.defaultMessageProperties;
	}


	public void setDefaultMessageProperties(Map<String, String> defaultMessageProperties) {
		this.defaultMessageProperties = defaultMessageProperties;
	}


	public MessageTypes getDefaultMessageType() {
		return this.defaultMessageType;
	}


	public void setDefaultMessageType(MessageTypes defaultMessageType) {
		this.defaultMessageType = defaultMessageType;
	}
}
