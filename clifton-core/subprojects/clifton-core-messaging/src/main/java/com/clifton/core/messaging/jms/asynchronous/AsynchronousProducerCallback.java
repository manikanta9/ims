package com.clifton.core.messaging.jms.asynchronous;


import com.clifton.core.messaging.jms.AbstractProducerCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;


/**
 * The <code>AsynchronousProducerCallback</code> class is a message ProducerCallback that implements an asynchronous message on the provided queue.
 *
 * @author mwacker
 */
public class AsynchronousProducerCallback extends AbstractProducerCallback {


	public AsynchronousProducerCallback(final MessageCreator messageCreator, String queue, JmsTemplate jmsTemplate) {
		super(messageCreator, queue, jmsTemplate);
	}


	@Override
	public Message doInJms(Session session, MessageProducer producer) throws JMSException {
		sendMessage(session, producer);
		// return no response for asynchronous processing
		return null;
	}
}
