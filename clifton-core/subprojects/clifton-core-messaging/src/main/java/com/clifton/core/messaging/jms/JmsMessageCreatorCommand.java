package com.clifton.core.messaging.jms;

import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.MessagingMessage;
import org.springframework.jms.support.converter.MessageConverter;

import java.util.Map;


/**
 * The <code>JmsMessageCreatorCommand</code> holds necessary properties and configuration parameters needed
 * to convert our synchronous and asynchronous messages to standard JMS messages.
 */
public class JmsMessageCreatorCommand {

	/**
	 * An implementation of a synchronous or asynchronous message that will
	 * be converted to a standard jms message.
	 */
	private MessagingMessage messagingMessage;

	private String correlationId;
	/**
	 * Message source used to filter messages on a queue.
	 * <p>
	 * For example, if 2 developer want to run their instances locally using the same JMS server, they could each set
	 * a unique message source and their instances will append the source to the outgoing messages and filter the incoming messages.
	 * <p>
	 * NOTE: The client message source must match the message source on the server application that is supposed to receive the messages.  On
	 * the server side the message source must be set in the spring context:
	 * <p>
	 * <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
	 * <property name="connectionFactory" ref="integrationServerJmsFactory"/>
	 * <property name="destination" ref="requestQueue" />
	 * <property name="messageListener" ref="integrationMessageConsumer" />
	 * <property name="messageSelector" value="MessageSource = '${integration.jms.messageSource}_${application.name}'"/>
	 * </bean>
	 */
	private String messageSource;
	private String responseClassName;
	/**
	 * A list of default properties to append to out going messages.
	 */
	private Map<String, String> defaultMessageProperties;

	/**
	 * The {@link MessageConverter} to use when serializing messages. This is used for all {@link MessageTypes} where the {@link MessageConverter} is supported.
	 */
	private MessageConverter messageConverter;

	/**
	 * The default {@link MessageTypes} to use if a message type is not explicitly declared for the serialized object type.
	 */
	private MessageTypes defaultMessageType;


	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCorrelationId() {
		return this.correlationId;
	}


	public void setCorrelationId(String correlationId) {
		this.correlationId = correlationId;
	}


	public String getMessageSource() {
		return this.messageSource;
	}


	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}


	public String getResponseClassName() {
		return this.responseClassName;
	}


	public void setResponseClassName(String responseClassName) {
		this.responseClassName = responseClassName;
	}


	public Map<String, String> getDefaultMessageProperties() {
		return this.defaultMessageProperties;
	}


	public void setDefaultMessageProperties(Map<String, String> defaultMessageProperties) {
		this.defaultMessageProperties = defaultMessageProperties;
	}


	public MessageConverter getMessageConverter() {
		return this.messageConverter;
	}


	public void setMessageConverter(MessageConverter messageConverter) {
		this.messageConverter = messageConverter;
	}


	public MessagingMessage getMessagingMessage() {
		return this.messagingMessage;
	}


	public void setMessagingMessage(MessagingMessage messagingMessage) {
		this.messagingMessage = messagingMessage;
	}


	public MessageTypes getDefaultMessageType() {
		return this.defaultMessageType;
	}


	public void setDefaultMessageType(MessageTypes defaultMessageType) {
		this.defaultMessageType = defaultMessageType;
	}
}
