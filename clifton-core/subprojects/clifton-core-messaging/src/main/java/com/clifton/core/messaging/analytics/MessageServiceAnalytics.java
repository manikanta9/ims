package com.clifton.core.messaging.analytics;

import com.clifton.core.messaging.MessagingMessage;


/**
 * @author MitchellF
 */
public interface MessageServiceAnalytics {

	/**
	 * requestTime should be milliseconds
	 */
	public void updateAnalytics(Long requestTime, MessagingMessage request, MessagingMessage response, String messageSource);


	public void clear();
}
