package com.clifton.core.messaging.converter.xml;

import com.clifton.core.messaging.AbstractMessagingMessage;
import com.thoughtworks.xstream.XStream;
import org.apache.activemq.command.Message;
import org.springframework.stereotype.Component;


/**
 * The {@link StandardMessagingXStreamConfigurer} provides {@link XStream} customizations for general serialization and deserialization enhancements. Customizations may include
 * functionality to reduce the length of serialized content or support serialization and deserialization of otherwise-problematic types.
 *
 * @author MikeH
 */
@Component
public class StandardMessagingXStreamConfigurer implements MessagingXStreamConfigurer {

	@Override
	public void configure(XStream xStream) {
		// Reduce serialized length for primitives
		xStream.useAttributeFor(Boolean.class);
		xStream.useAttributeFor(Integer.class);
		xStream.useAttributeFor(Long.class);
		xStream.useAttributeFor(Short.class);
		xStream.useAttributeFor(boolean.class);
		xStream.useAttributeFor(int.class);
		xStream.useAttributeFor(long.class);
		xStream.useAttributeFor(short.class);

		// Entity-specific serialization customizations
		xStream.useAttributeFor(AbstractMessagingMessage.class, "machineName");
		xStream.registerLocalConverter(AbstractMessagingMessage.class, "properties", new MessagingXStreamStringMapConverter());
		xStream.registerLocalConverter(Message.class, "properties", new MessagingXStreamStringMapConverter());
	}
}
