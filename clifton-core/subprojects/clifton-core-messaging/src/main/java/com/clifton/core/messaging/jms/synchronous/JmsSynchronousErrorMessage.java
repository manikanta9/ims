package com.clifton.core.messaging.jms.synchronous;


import com.clifton.core.messaging.AbstractMessagingMessage;
import com.clifton.core.messaging.synchronous.SynchronousErrorMessage;


public class JmsSynchronousErrorMessage extends AbstractMessagingMessage implements SynchronousErrorMessage {

	/**
	 * The error the occurred in processing.
	 */
	private Throwable error;
	private Integer requestTimeout;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public Throwable getError() {
		return this.error;
	}


	@Override
	public void setError(Throwable error) {
		this.error = error;
	}


	@Override
	public Integer getRequestTimeout() {
		return this.requestTimeout;
	}


	public void setRequestTimeout(Integer requestTimeout) {
		this.requestTimeout = requestTimeout;
	}
}
