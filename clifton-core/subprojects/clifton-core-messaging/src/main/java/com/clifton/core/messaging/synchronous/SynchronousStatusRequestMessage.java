package com.clifton.core.messaging.synchronous;


/**
 *
 */
public class SynchronousStatusRequestMessage extends AbstractSynchronousRequestMessage {

	private boolean heartbeat;

	//Set an encrypted value here to test the transport encryption
	private String transportEncryptionValue;

	//Set to true to get the public key
	private boolean transportEncryptionPublicKey;

	//Set to true to run the transformation health check
	private boolean transformation;

	//Set to true to get data source information
	private boolean ftpStatus;


	@Override
	public Class<?> getResponseClass() {
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isHeartbeat() {
		return this.heartbeat;
	}


	public void setHeartbeat(boolean heartbeat) {
		this.heartbeat = heartbeat;
	}


	public String getTransportEncryptionValue() {
		return this.transportEncryptionValue;
	}


	public void setTransportEncryptionValue(String transportEncryptionValue) {
		this.transportEncryptionValue = transportEncryptionValue;
	}


	public boolean isTransformation() {
		return this.transformation;
	}


	public void setTransformation(boolean transformation) {
		this.transformation = transformation;
	}


	public boolean isTransportEncryptionPublicKey() {
		return this.transportEncryptionPublicKey;
	}


	public void setTransportEncryptionPublicKey(boolean transportEncryptionPublicKey) {
		this.transportEncryptionPublicKey = transportEncryptionPublicKey;
	}


	public boolean isFtpStatus() {
		return this.ftpStatus;
	}


	public void setFtpStatus(boolean ftpStatus) {
		this.ftpStatus = ftpStatus;
	}
}
