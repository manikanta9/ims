package com.clifton.core.messaging;

/**
 * This class is used to hold the response status messages for various requests.
 * <p>
 *
 * @author stevenf on 8/2/2016.
 */
public class MessageStatus {

	/**
	 * Constants for health check messages
	 */
	public static final String DATABASE_NAME = "Database_Name";
	public static final String DATA_SOURCE_URL = "Data_Source_URL";
	public static final String ENVIRONMENT = "Environment";

	public static final String INTEGRATION_DECRYPTION_STATUS = "Integration_Transport_Decryption";
	public static final String INTEGRATION_DECRYPTION_VALUE = "Integration_Transport_Decryption_Value";
	public static final String INTEGRATION_FTP_STATUS = "FTP_Connection_Status";
	public static final String INTEGRATION_PUBLIC_KEY = "Integration_Transport_Encryption_Public_Key";
	public static final String INTEGRATION_TRANSFORMATION = "Integration_Transformation";

	public static final String SWIFT_FOLDER_STATUS = "Swift Server Auto Client Folder Accessibility.";
	public static final String SWIFT_EMISSION_FOLDER_STATUS = "Swift Server Auto Client Emission Folder Processing.";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private boolean error;
	private Throwable exception;

	private String responseName;
	private String responseStatus;

	////////////////////////////////////////////////////////////////////////////////
	//////////                Getter and Setter Methods                   //////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isError() {
		return this.error;
	}


	public void setError(boolean error) {
		this.error = error;
	}


	public Throwable getException() {
		return this.exception;
	}


	public void setException(Throwable exception) {
		this.exception = exception;
	}


	public String getResponseName() {

		return this.responseName;
	}


	public void setResponseName(String responseName) {

		this.responseName = responseName;
	}


	public String getResponseStatus() {

		return this.responseStatus;
	}


	public void setResponseStatus(String responseStatus) {

		this.responseStatus = responseStatus;
	}
}
