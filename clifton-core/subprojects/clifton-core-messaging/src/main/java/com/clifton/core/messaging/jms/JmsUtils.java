package com.clifton.core.messaging.jms;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.synchronous.SynchronousFileRequestMessage;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.thoughtworks.xstream.XStream;
import org.apache.activemq.BlobMessage;
import org.springframework.jms.support.converter.MessageConverter;

import javax.jms.BytesMessage;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.ObjectMessage;
import javax.jms.StreamMessage;
import javax.jms.TextMessage;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


/**
 * The {@link JmsUtils} class contains helper utility methods for working with JMS messages.
 *
 * @author vgomelsky
 */
public class JmsUtils {

	/**
	 * Returns text representation of the specified message.
	 * If there is an error during message serialization, returns the error message.
	 */
	public static String getMessageText(Message message) {
		String result;
		try {
			if (message instanceof TextMessage) {
				result = ((TextMessage) message).getText();
			}
			else if (message instanceof BytesMessage) {
				// Read byte message content using standard charset encoding
				BytesMessage bytesMessage = (BytesMessage) message;
				bytesMessage.reset();
				byte[] messageData = new byte[(int) bytesMessage.getBodyLength()];
				bytesMessage.readBytes(messageData);
				result = new String(messageData);
				bytesMessage.reset();
			}
			else if (message instanceof ObjectMessage) {
				result = ((ObjectMessage) message).getObject().toString();
			}
			else {
				throw new RuntimeException(String.format("Unrecognized message type for message text retrieval: [%s]", ClassUtils.getClassNameForObject(message)));
			}
		}
		catch (Exception e) {
			result = "Error getting message text: " + e.getMessage();
		}
		return result;
	}


	/**
	 * Converts the given message to an {@link ObjectMessage} implementation, the converted message maintains all the
	 * original message properties.
	 */
	public static JmsObjectMessage convertTextMessage(Message message, Serializable object) {
		return new JmsObjectMessage(message, object);
	}


	/**
	 * Deserializes the given message, using the provided {@link XStream} or {@link MessageConverter} as necessary. Deserialization assumes that sufficient type information is
	 * included in the message such that the target type can be inferred. This type information is typically included by the {@link XStream} or {@link MessageConverter} during
	 * serialization.
	 */
	public static Object deserializeMessage(Message message, XStream xStream, MessageConverter messageConverter) {
		try {
			/*
			 * While messages are typically automatically ingested in read-only mode, internally-generated messages are initialized in write-only mode. These messages must be reset
			 * to allow reading. Write-only mode is only automatically used for the byte and stream message types. See message sub-type JavaDocs for more info.
			 */
			if (message instanceof BytesMessage) {
				((BytesMessage) message).reset();
			}
			else if (message instanceof StreamMessage) {
				((StreamMessage) message).reset();
			}
			final Object result;
			// All messages with the content-type header can be consumed by our message converter
			if (message.propertyExists(MessagingMessage.MESSAGE_CONTENT_TYPE_PROPERTY_NAME)) {
				result = messageConverter.fromMessage(message);
			}
			// Legacy support; handle messages generated before the content-type header was implemented
			else if (message instanceof TextMessage) {
				result = xStream.fromXML(getMessageText(message));
			}
			else if (message instanceof ObjectMessage) {
				result = ((ObjectMessage) message).getObject();
			}
			else if (message instanceof BlobMessage) {
				BlobMessage blobMessage = (BlobMessage) message;
				SynchronousFileRequestMessage requestMessage = (SynchronousFileRequestMessage) xStream.fromXML(message.getStringProperty(MessagingMessage.MESSAGE_OBJECT_XML_PROPERTY_NAME));
				File file = FileUtils.convertInputStreamToFile(FileUtils.getFileNameWithoutExtension(requestMessage.getFileName()), FileUtils.getFileExtension(requestMessage.getFileName()),
						blobMessage.getInputStream());
				requestMessage.setFile(file);
				result = requestMessage;
			}
			else {
				throw new RuntimeException(String.format("Unrecognized message type for deserialization: [%s]", ClassUtils.getClassNameForObject(message)));
			}
			return result;
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to deserialize message.", e);
		}
	}


	/**
	 * Returns a consolidated map of message properties from the unique message and optional default properties.
	 * Message properties will override the defaults. Properties of the message will be populated with default
	 * values if applicable and not currently defined.
	 */
	public static Map<String, String> consolidateMessageProperties(MessagingMessage message, Map<String, String> defaultProperties) {
		// Collect request properties
		Map<String, String> requestProperties = new HashMap<>();
		if (!CollectionUtils.isEmpty(defaultProperties)) {
			defaultProperties.forEach((key, value) -> {
				// Set message properties from the defaults to be serialized
				BeanUtils.setPropertyValueIfNotSet(message, key, value, true);
				// add defaults to be added to the JMS message
				requestProperties.put(key, value);
			});
		}
		// add message properties to be added to the JMS message, must be done after defaults
		requestProperties.putAll(message.getProperties());
		// Set the merged properties on the message
		message.getProperties().putAll(requestProperties);
		return requestProperties;
	}


	/**
	 * Populates the provided {@link Message} with the provided properties.
	 *
	 * @param message    the message to populate
	 * @param properties the properties to set on the message, may be null
	 * @throws JMSException if setting a property encounters an issue
	 */
	public static void populateMessageProperties(Message message, Map<String, String> properties) throws JMSException {
		if (!CollectionUtils.isEmpty(properties)) {
			for (Map.Entry<String, String> property : properties.entrySet()) {
				message.setStringProperty(property.getKey(), property.getValue());
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Delegated Spring JMS utilities                  ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Close the given JMS MessageConsumer and ignore any thrown exception.
	 * This is useful for typical <code>finally</code> blocks in manual JMS code.
	 *
	 * @param messageConsumer the JMS MessageConsumer to close (may be <code>null</code>)
	 */
	public static void closeMessageConsumer(MessageConsumer messageConsumer) {
		org.springframework.jms.support.JmsUtils.closeMessageConsumer(messageConsumer);
	}


	/**
	 * Close the given JMS MessageProducer and ignore any thrown exception.
	 * This is useful for typical <code>finally</code> blocks in manual JMS code.
	 *
	 * @param messageProducer the JMS MessageProducer to close (may be <code>null</code>)
	 */
	public static void closeMessageProducer(MessageProducer messageProducer) {
		org.springframework.jms.support.JmsUtils.closeMessageProducer(messageProducer);
	}
}
