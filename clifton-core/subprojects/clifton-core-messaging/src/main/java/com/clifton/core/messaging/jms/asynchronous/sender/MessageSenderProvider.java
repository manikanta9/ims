package com.clifton.core.messaging.jms.asynchronous.sender;


/**
 * This is an interface allowing users to create custom Message Sender for sending messages. The message destination could be ActiveMQ, AWS SQS, Kafka, etc.
 *
 * @author vanand
 */
public interface MessageSenderProvider {

	/**
	 * Interface method to be overridden to handle different connection factory.
	 */
	public MessageSender getMessageSender(MessageSenderContext messageSenderContext);
}
