package com.clifton.core.messaging.converter.json;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.Module.SetupContext;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The {@link JacksonStackTraceElementMixin} type is the Jackson {@link SetupContext#setMixInAnnotations(Class, Class) mixin} for {@link StackTraceElement} serialization and
 * deserialization. This mixin modifies serialization and deserialization to a friendly string-based format, rather than serializing each object field independently. This is
 * helpful for improving readability of serialized stacktraces and for reducing payload size.
 * <p>
 * This is heavily based off of the XStream <a href="https://github.com/x-stream/xstream/blob/e6b1e49801071fb4288f9118f6c528e898ea5874/xstream/src/java/com/thoughtworks/xstream/converters/extended/StackTraceElementConverter.java">com.thoughtworks.xstream.converters.extended.StackTraceElementConverter</a>
 * implementation, which provides similar functionality for XML serialization and deserialization.
 *
 * @author MikeH
 */
@JsonSerialize(using = JacksonStackTraceElementMixin.JacksonStackTraceElementSerializer.class)
@JsonDeserialize(using = JacksonStackTraceElementMixin.JacksonStackTraceElementDeserializer.class)
public interface JacksonStackTraceElementMixin {

	public static class JacksonStackTraceElementSerializer extends JsonSerializer<StackTraceElement> {

		@Override
		public void serialize(StackTraceElement value, JsonGenerator gen, SerializerProvider serializers) {
			try {
				String stackTraceElementStr = stackTraceElementToString(value);
				// JRockit adds ":???" for invalid line number
				stackTraceElementStr = stackTraceElementStr.replaceFirst(":\\?\\?\\?", "");
				gen.writeString(stackTraceElementStr);
			}
			catch (Exception e) {
				throw new RuntimeException(String.format("An error occurred while serializing the object of type [%s].", value.getClass().getName()), e);
			}
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private String stackTraceElementToString(StackTraceElement obj) {
			if (obj == null) {
				return null;
			}
			String result = obj.toString();
			return obj.isNativeMethod() && obj.getFileName() != null
					? result.replace("(Native Method)", String.format("(%s:-2)", obj.getFileName()))
					: result;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class JacksonStackTraceElementDeserializer extends JsonDeserializer<StackTraceElement> {

		/**
		 * Regular expression to parse a line of a stack trace. Returns 4 groups.
		 * <p>
		 * Example:
		 * <pre>
		 * com.blah.MyClass.doStuff(MyClass.java:123)
		 * |-------1------| |--2--| |----3-----| |4|
		 * </pre>
		 * Note that group 4 is optional and is only present if a colon character exists.
		 */
		private static final Pattern STACK_TRACE_ELEMENT_PATTERN = Pattern.compile("^(.+)\\.([^(]+)\\(([^:]*)(?::(\\d+))?\\)$");


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Override
		public StackTraceElement deserialize(JsonParser p, DeserializationContext ctxt) {
			try {
				// Parse element
				String stackTraceElementStr = p.getValueAsString();
				Matcher matcher = STACK_TRACE_ELEMENT_PATTERN.matcher(stackTraceElementStr);
				if (!matcher.matches()) {
					throw new RuntimeException(String.format("Could not parse StackTraceElement [%s]. The element did not match the expected pattern.", stackTraceElementStr));
				}
				String declaringClass = matcher.group(1);
				String methodName = matcher.group(2);
				String fileName = matcher.group(3);
				String lineNumberStr = matcher.group(4);

				// Generate object from element fields
				final StackTraceElement element;
				if ("Unknown Source".equals(fileName)) {
					element = new StackTraceElement(declaringClass, methodName, null, -1);
				}
				else if ("Native Method".equals(fileName)) {
					element = new StackTraceElement(declaringClass, methodName, null, -2);
				}
				else if (lineNumberStr == null) {
					// No line number available
					element = new StackTraceElement(declaringClass, methodName, fileName, -1);
				}
				else {
					// Full information available
					int lineNumber = Integer.parseInt(lineNumberStr);
					element = new StackTraceElement(declaringClass, methodName, fileName, lineNumber);
				}
				return element;
			}
			catch (Exception e) {
				throw new RuntimeException("An error occurred during deserialization.", e);
			}
		}
	}
}
