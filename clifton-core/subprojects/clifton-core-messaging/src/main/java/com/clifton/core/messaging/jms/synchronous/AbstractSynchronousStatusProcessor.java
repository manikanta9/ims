package com.clifton.core.messaging.jms.synchronous;

import com.clifton.core.dataaccess.DataSourceUtils;
import com.clifton.core.dataaccess.sql.SqlHandlerLocator;
import com.clifton.core.messaging.MessageStatus;
import com.clifton.core.messaging.synchronous.SynchronousStatusRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousStatusResponseMessage;

import javax.sql.DataSource;


/**
 * The <code>AbstractSynchronousStatusProcessor</code> implements common methods for Status Processors
 *
 * @author theodorez
 */
public abstract class AbstractSynchronousStatusProcessor implements SynchronousProcessor<SynchronousStatusRequestMessage, SynchronousStatusResponseMessage> {

	private static final String DEFAULT_DATA_SOURCE_NAME = "dataSource";

	private String databaseName;
	private String environment;

	private SqlHandlerLocator sqlHandlerLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SynchronousStatusResponseMessage process(SynchronousStatusRequestMessage message) {
		SynchronousStatusResponseMessage response = new SynchronousStatusResponseMessage();
		DataSource dataSource = getSqlHandlerLocator().locate(DEFAULT_DATA_SOURCE_NAME).getDataSource();
		String jdbcUrl = ((org.apache.tomcat.jdbc.pool.DataSource) DataSourceUtils.getTargetDataSource(dataSource)).getUrl();
		MessageStatus urlStatus = new MessageStatus();
		urlStatus.setResponseName(MessageStatus.DATA_SOURCE_URL);
		urlStatus.setResponseStatus(jdbcUrl);
		response.getMessageStatusList().add(urlStatus);

		MessageStatus environmentStatus = new MessageStatus();
		environmentStatus.setResponseName(MessageStatus.ENVIRONMENT);
		environmentStatus.setResponseStatus(getEnvironment());
		response.getMessageStatusList().add(environmentStatus);

		MessageStatus databaseStatus = new MessageStatus();
		databaseStatus.setResponseName(MessageStatus.DATABASE_NAME);
		databaseStatus.setResponseStatus(getDatabaseName());
		response.getMessageStatusList().add(databaseStatus);
		return response;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDatabaseName() {
		return this.databaseName;
	}


	public void setDatabaseName(String databaseName) {
		this.databaseName = databaseName;
	}


	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	public SqlHandlerLocator getSqlHandlerLocator() {
		return this.sqlHandlerLocator;
	}


	public void setSqlHandlerLocator(SqlHandlerLocator sqlHandlerLocator) {
		this.sqlHandlerLocator = sqlHandlerLocator;
	}
}
