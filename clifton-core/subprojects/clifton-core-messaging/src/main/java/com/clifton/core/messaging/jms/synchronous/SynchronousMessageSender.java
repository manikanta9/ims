package com.clifton.core.messaging.jms.synchronous;


import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.MessageTypes;
import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.analytics.MessageServiceAnalytics;
import com.clifton.core.messaging.jms.AbstractXStreamMessageHandler;
import com.clifton.core.messaging.jms.JmsMessageCreatorCommand;
import com.clifton.core.messaging.jms.JmsMessageCreatorUtils;
import com.clifton.core.messaging.jms.JmsUtils;
import com.clifton.core.messaging.synchronous.SynchronousErrorMessage;
import com.clifton.core.messaging.synchronous.SynchronousRequestMessage;
import com.clifton.core.messaging.synchronous.SynchronousResponseMessage;
import com.clifton.core.messaging.synchronous.SynchronousStatusRequestMessage;
import com.clifton.core.util.ObjectUtils;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;

import javax.jms.Message;
import javax.jms.MessageProducer;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;


/**
 * The <code>SynchronousMessageSender</code> defines the client side methods for making synchronous calls to a JMS queue.
 *
 * @author mwacker
 */
public class SynchronousMessageSender<T extends SynchronousRequestMessage> extends AbstractXStreamMessageHandler {

	private JmsTemplate jmsTemplate;

	/**
	 * The base queue name.
	 * <p>
	 * For example, if the queue name is 'cool.dude', request messages will be sent on 'cool.dude.request' and then the
	 * handler will listen on 'cool.dude.response' for the response message.
	 */
	private String queue;
	/**
	 * Number of milliseconds to wait for a response message.
	 * <p>
	 * NOTE: This will also set the expiration time of the JMS message if messageTimeOut is not set., so the message will be removed from the
	 * JMS queue if it's not read after the timeout.
	 */
	private int timeout = 10000;
	/**
	 * The number milliseconds until the message expires and is removed from JMS queue if it's not read.
	 * <p>
	 * NOTE: If this is not set, the timeout will be used.
	 */
	private Integer messageTimeOut;
	/**
	 * Message source used to filter messages on a queue.
	 * <p>
	 * For example, if 2 developer want to run their instances locally using the same JMS server, they could each set
	 * a unique message source and their instances will append the source to the outgoing messages and filter the incoming messages.
	 * <p>
	 * NOTE: The client message source must match the message source on the server application that is supposed to receive the messages.  On
	 * the server side the message source must be set in the spring context:
	 * <p>
	 * <bean id="jmsContainer" class="org.springframework.jms.listener.DefaultMessageListenerContainer">
	 * <property name="connectionFactory" ref="integrationServerJmsFactory"/>
	 * <property name="destination" ref="requestQueue" />
	 * <property name="messageListener" ref="integrationMessageConsumer" />
	 * <property name="messageSelector" value="MessageSource = '${integration.jms.messageSource}_${application.name}'"/>
	 * </bean>
	 */
	private String messageSource;
	/**
	 * A list of default properties to append to out going messages.
	 */
	private Map<String, String> defaultMessageProperties;
	/**
	 * The number of times to retry the message.
	 */
	private int retryAttempts = 0;
	/**
	 * If retry on error is true, the message will be retried when an error is received from the server.  When retry on error is false,
	 * the message will only be retried when no response is received from the server.
	 */
	private boolean retryOnError;

	/**
	 * Optionally defined Object containing analytic data about the message service
	 */
	private MessageServiceAnalytics analyticsContainer;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Performs a synchronous call to request queue and waits pre-configured timeout (in milliseconds)
	 * for ResponseMessage on reply queue.
	 *
	 * @param synchronousRequestMessage
	 */
	public SynchronousResponseMessage call(T synchronousRequestMessage) {
		return doCall(synchronousRequestMessage, 0);
	}


	public SynchronousResponseMessage call(final T synchronousRequestMessage, int retryCount) {
		return doCall(synchronousRequestMessage, retryCount);
	}


	private SynchronousResponseMessage doCall(final SynchronousRequestMessage synchronousRequestMessage, int retryCount) {
		// time request if info level logging is enabled
		long executionTime = System.nanoTime();

		MessageTypes messageType = ObjectUtils.coalesce(JmsMessageCreatorUtils.getMessageType(synchronousRequestMessage), getDefaultMessageType());
		String responseClassName = synchronousRequestMessage.getResponseClass() != null ? synchronousRequestMessage.getResponseClass().getName() : null;

		JmsMessageCreatorCommand messageCreatorCommand = new JmsMessageCreatorCommand();
		messageCreatorCommand.setMessageSource(getMessageSource());
		messageCreatorCommand.setResponseClassName(responseClassName);
		messageCreatorCommand.setMessagingMessage(synchronousRequestMessage);
		messageCreatorCommand.setDefaultMessageProperties(getDefaultMessageProperties());
		messageCreatorCommand.setDefaultMessageType(getDefaultMessageType());
		messageCreatorCommand.setMessageConverter(getJmsTemplate().getMessageConverter());
		MessageCreator messageCreator = JmsMessageCreatorUtils.getRequestMessageCreator(messageCreatorCommand);

		//Execute the request
		long startTime = System.nanoTime();
		Message response = request(messageCreator, messageType, synchronousRequestMessage.getRequestTimeout());
		long requestDuration = TimeUnit.MILLISECONDS.convert((System.nanoTime() - startTime), TimeUnit.NANOSECONDS);

		// process response message
		SynchronousResponseMessage result = (SynchronousResponseMessage) JmsUtils.deserializeMessage(response, getXStreamInstance(), getJmsTemplate().getMessageConverter());
		if (result != null) {
			// check for errors
			if (this.analyticsContainer != null) {
				String messageSpecificSource = Optional.ofNullable(synchronousRequestMessage.getProperties())
						.map(requestMessageProperties -> requestMessageProperties.get(MessagingMessage.MESSAGE_SOURCE_PROPERTY_NAME))
						.orElse(getMessageSource());
				this.analyticsContainer.updateAnalytics(requestDuration, synchronousRequestMessage, result, messageSpecificSource);
			}
			if (result.getError() != null) {
				String machineName;
				try {
					machineName = response.getStringProperty(MessagingMessage.MESSAGE_MACHINE_NAME_PROPERTY_NAME);
					if (shouldRetry(synchronousRequestMessage, retryCount)) {
						LogUtils.warn(getClass(), "Received error in response from " + machineName + " attempt #" + retryCount, result.getError());
						return doCall(synchronousRequestMessage, retryCount + 1);
					}
				}
				catch (Exception e) {
					throw new RuntimeException("Error requesting data from service.", e);
				}
				throw new RuntimeException("Received error in response from " + machineName, result.getError());
			}
		}
		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> ((double) (System.nanoTime() - executionTime) / 1_000_000_000) + " seconds to process " + synchronousRequestMessage));
		return result;
	}


	private boolean shouldRetry(final SynchronousRequestMessage synchronousRequestMessage, int retryCount) {
		if (isRetryOnError()) {
			LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> "synchronousRequestMessage.getRetryOverride() is null? " + (synchronousRequestMessage.getRetryCountOverride() == null) + "  synchronousRequestMessage.getRetryOverride() value: " + synchronousRequestMessage.getRetryCountOverride() + "   getRetryAttempts() value: " + getRetryAttempts()));

			int allowedAttempts = (synchronousRequestMessage.getRetryCountOverride() != null && synchronousRequestMessage.getRetryCountOverride() > -1) ? synchronousRequestMessage.getRetryCountOverride() : getRetryAttempts();
			LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> "allowedAttempts value: " + allowedAttempts));

			if (allowedAttempts > retryCount) {
				LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> "allowedAttempts > retryCount (" + retryCount + ") so proceeding to retry"));
				return true;
			}
		}
		return false;
	}


	public boolean sendHeartbeat() {
		try {
			SynchronousStatusRequestMessage heartbeatMessage = new SynchronousStatusRequestMessage();
			heartbeatMessage.setHeartbeat(true);
			return doCall(heartbeatMessage, 0).getError() == null;
		}
		catch (Throwable e) {
			throw new RuntimeException("Did not receive a heartbeat response from jms queue [" + getQueue() + ".response] when sending request on [" + getQueue() + ".request]", e);
		}
	}


	private Message request(MessageCreator messageCreator, final MessageTypes messageType, final Integer timeoutOverride) {
		final SynchronousProducerCallback producerCallback = new SynchronousProducerCallback(messageCreator, getQueue(), getJmsTemplate());
		producerCallback.setTimeout(timeoutOverride != null && timeoutOverride > 0 ? timeoutOverride : getTimeout());
		producerCallback.setMessageTimeOut(timeoutOverride != null && timeoutOverride > 0 ? timeoutOverride : getMessageTimeOut());

		return getJmsTemplate().execute(session -> {
			MessageProducer producer = session.createProducer(null); //creates an unidentified producer
			try {
				return producerCallback.doInJms(session, producer);
			}
			catch (Throwable e) {
				// return an error message so retries will work
				SynchronousErrorMessage response = new JmsSynchronousErrorMessage();
				response.setError(e);
				response.setMachineName("localhost");
				return JmsMessageCreatorUtils.createMessage(response, session, messageType, getJmsTemplate().getMessageConverter());
			}
			finally {
				JmsUtils.closeMessageProducer(producer);
			}
		}, true);
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public String getQueue() {
		return this.queue;
	}


	public void setQueue(String queue) {
		this.queue = queue;
	}


	public int getTimeout() {
		return this.timeout;
	}


	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}


	public Integer getMessageTimeOut() {
		return this.messageTimeOut;
	}


	public void setMessageTimeOut(Integer messageTimeOut) {
		this.messageTimeOut = messageTimeOut;
	}


	public String getMessageSource() {
		return this.messageSource;
	}


	public void setMessageSource(String messageSource) {
		this.messageSource = messageSource;
	}


	public Map<String, String> getDefaultMessageProperties() {
		return this.defaultMessageProperties;
	}


	public void setDefaultMessageProperties(Map<String, String> defaultMessageProperties) {
		this.defaultMessageProperties = defaultMessageProperties;
	}


	public int getRetryAttempts() {
		return this.retryAttempts;
	}


	public void setRetryAttempts(int retryAttempts) {
		this.retryAttempts = retryAttempts;
	}


	public boolean isRetryOnError() {
		return this.retryOnError;
	}


	public void setRetryOnError(boolean retryOnError) {
		this.retryOnError = retryOnError;
	}


	public MessageServiceAnalytics getAnalyticsContainer() {
		return this.analyticsContainer;
	}


	public void setAnalyticsContainer(MessageServiceAnalytics analyticsContainer) {
		this.analyticsContainer = analyticsContainer;
	}
}
