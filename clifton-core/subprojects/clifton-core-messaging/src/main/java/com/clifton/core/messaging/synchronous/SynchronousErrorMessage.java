package com.clifton.core.messaging.synchronous;


/**
 * The <code>AsynchronousErrorMessage</code> defines an wrapper for a message that failed to process.
 * <p/>
 * For example, in the trade-fix project when a FIX message received via JMS fails to process, an AsynchronousErrorMessage
 * is created and placed on the error queue.
 *
 * @author mwacker
 */
public interface SynchronousErrorMessage extends SynchronousResponseMessage {
	// nothing
}
