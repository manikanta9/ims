package com.clifton.core.messaging.jms.activemq;


import org.apache.activemq.ActiveMQMessageConsumer;
import org.apache.activemq.ActiveMQMessageTransformation;
import org.apache.activemq.ActiveMQPrefetchPolicy;
import org.apache.activemq.ActiveMQSession;
import org.apache.activemq.CustomDestination;
import org.apache.activemq.command.ActiveMQDestination;
import org.apache.activemq.command.ConsumerId;
import org.apache.activemq.command.MessageDispatch;
import org.apache.activemq.command.SessionId;
import org.apache.activemq.util.LongSequenceGenerator;
import org.springframework.jms.core.JmsTemplate;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.QueueReceiver;
import javax.jms.QueueSender;
import javax.jms.Topic;
import javax.jms.TopicPublisher;
import javax.jms.TopicSubscriber;


/**
 * The <code>ActiveMQNonExpiringCustomDestination</code> implements a custom activemq destination that will
 * create a custom message consumer that overrides the expiration before processing the message.  This is used
 * for the Bloomberg server where we can't guarantee that the system time on the Bloomberg terminal will match
 * the system time on the ActiveMQ server.
 * <p/>
 * <bean id="requestQueue" class="com.clifton.core.messaging.jms.activemq.ActiveMQNonExpiringCustomDestination">
 * <property name="physicalName" value="${bloomberg.synchronous.queue}.request"/>
 * <property name="jmsTemplate" ref="bloombergServerJmsTemplate"/>
 * </bean>
 *
 * @author mwacker
 */
public class ActiveMQNonExpiringCustomDestination implements CustomDestination {

	private final LongSequenceGenerator consumerIdGenerator = new LongSequenceGenerator();
	private JmsTemplate jmsTemplate;
	private String physicalName;


	public ActiveMQNonExpiringCustomDestination() {
		//
	}


	public ActiveMQNonExpiringCustomDestination(String physicalName, JmsTemplate jmsTemplate) {
		this.physicalName = physicalName;
		this.jmsTemplate = jmsTemplate;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return getPhysicalName();
	}


	private ConsumerId getNextConsumerId(SessionId sId) {
		return new ConsumerId(sId, this.consumerIdGenerator.getNextSequenceId());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// CustomDestination interface
	//-----------------------------------------------------------------------
	@Override
	public MessageConsumer createConsumer(ActiveMQSession session, String messageSelector) {
		return createConsumer(session, messageSelector, false);
	}


	@Override
	public MessageConsumer createConsumer(ActiveMQSession session, String messageSelector, boolean noLocal) {
		try {
			// this is s horrible hack, but I can't find another way to get the id at the moment
			String connectionId = session.getConnection().getConnectionInfo().getConnectionId().toString();
			Long sessionId = new Long(session.toString().substring(session.toString().indexOf(connectionId) + connectionId.length() + 1, session.toString().indexOf(',')));
			SessionId sId = new SessionId(session.getConnection().getConnectionInfo().getConnectionId(), sessionId);

			Destination destination = getJmsTemplate().getDestinationResolver().resolveDestinationName(session, getPhysicalName(), noLocal);

			ActiveMQPrefetchPolicy prefetchPolicy = session.getConnection().getPrefetchPolicy();
			int prefetch;
			if (destination instanceof Topic) {
				prefetch = prefetchPolicy.getTopicPrefetch();
			}
			else {
				prefetch = prefetchPolicy.getQueuePrefetch();
			}
			ActiveMQDestination activemqDestination = ActiveMQMessageTransformation.transformDestination(destination);

			return new ActiveMQMessageConsumer(session, getNextConsumerId(sId), activemqDestination, null, messageSelector, prefetch, prefetchPolicy.getMaximumPendingMessageLimit(), noLocal, false,
					session.isAsyncDispatch(), session.getMessageListener()) {

				@Override
				public void dispatch(MessageDispatch md) {
					// set the message expiration
					if ((md.getMessage() != null) && md.getMessage().isExpired()) {
						md.getMessage().setExpiration(0);
					}
					super.dispatch(md);
				}
			};
		}
		catch (JMSException e) {
			throw new RuntimeException("Failed to create ActiveMQMessageConsumer.", e);
		}
	}


	@Override
	public TopicSubscriber createSubscriber(ActiveMQSession session, String messageSelector, boolean noLocal) {
		return createDurableSubscriber(session, null, messageSelector, noLocal);
	}


	@Override
	public TopicSubscriber createDurableSubscriber(@SuppressWarnings("unused") ActiveMQSession session, @SuppressWarnings("unused") String name, @SuppressWarnings("unused") String messageSelector,
	                                               @SuppressWarnings("unused") boolean noLocal) {
		throw new UnsupportedOperationException("This destination is not a Topic: " + this);
	}


	@Override
	public QueueReceiver createReceiver(@SuppressWarnings("unused") ActiveMQSession session, @SuppressWarnings("unused") String messageSelector) {
		throw new UnsupportedOperationException("This destination is not a Queue: " + this);
	}


	// Producers
	//-----------------------------------------------------------------------
	@Override
	public MessageProducer createProducer(ActiveMQSession session) throws JMSException {
		return session.createProducer(getJmsTemplate().getDestinationResolver().resolveDestinationName(session, getPhysicalName(), false));
	}


	@Override
	public TopicPublisher createPublisher(@SuppressWarnings("unused") ActiveMQSession session) {
		throw new UnsupportedOperationException("This destination is not a Topic: " + this);
	}


	@Override
	public QueueSender createSender(@SuppressWarnings("unused") ActiveMQSession session) {
		throw new UnsupportedOperationException("This destination is not a Queue: " + this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JmsTemplate getJmsTemplate() {
		return this.jmsTemplate;
	}


	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}


	public String getPhysicalName() {
		return this.physicalName;
	}


	public void setPhysicalName(String physicalName) {
		this.physicalName = physicalName;
	}
}
