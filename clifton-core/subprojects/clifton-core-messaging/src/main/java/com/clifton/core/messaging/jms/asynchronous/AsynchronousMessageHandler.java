package com.clifton.core.messaging.jms.asynchronous;


import com.clifton.core.messaging.asynchronous.AsynchronousMessage;

import javax.jms.MessageListener;


/**
 * The <code>AsynchronousMessageHandler</code> defines an asynchronous JMS message handler used for send and receiving messages.
 *
 * @author mwacker
 */
public interface AsynchronousMessageHandler extends MessageListener {

	/**
	 * Send an asynchronous message.
	 */
	public void send(AsynchronousMessage message);


	/**
	 * Send an asynchronous message with a specific correlation id.  This would be used to
	 * track a message or link a received message to one that was sent.
	 */
	public void send(AsynchronousMessage message, String correlationId);
}
