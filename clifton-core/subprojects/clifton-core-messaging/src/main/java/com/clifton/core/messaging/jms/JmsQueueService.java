package com.clifton.core.messaging.jms;


import javax.jms.MessageConsumer;
import java.util.List;


/**
 * The <code>JmsQueueService</code> defines a service to view/edit messages on a JMS queue.
 *
 * @author mwacker
 */
public interface JmsQueueService<T> {

	/**
	 * The default number of milliseconds that {@link MessageConsumer#receive(long)} operations will wait for a message.
	 * <p>
	 * Note: Depending on the JMS provider implementation, {@link MessageConsumer#receiveNoWait()} may not receive a message, even if a message is on the queue. The JMS provider
	 * is given discretion to interpret the meaning of "immediately" such that {@link MessageConsumer#receiveNoWait()} may only return a message if one is already available on the
	 * client. {@link MessageConsumer#receive(long)} must be used instead to give the provider time to poll the broker and wait for a response.
	 *
	 * @see <a href="https://stackoverflow.com/a/36634896/1941654">https://stackoverflow.com/a/36634896/1941654</a>
	 * @see <a href="https://github.com/eclipse-ee4j/jms-api/issues/85">https://github.com/eclipse-ee4j/jms-api/issues/85</a>
	 */
	public static final long DEFAULT_RECEIVE_TIMEOUT_MS = 500;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Get all messages from the provided queue without pulling any of the messages off of the queue.
	 */
	public List<T> getMessages(String queueName);


	/**
	 * Get all messages from the provided queue using the provided <code>additionalMessageSelector</code> without pulling any of the messages off of the queue.
	 */
	public List<T> getMessages(String queueName, String additionalMessageSelector);


	/**
	 * Receives all messages from the provided queue, removing all messages from the queue in the process.
	 */
	public List<T> receiveMessages(String queueName);


	/**
	 * Receives all messages from the provided queue using the provided <code>additionalMessageSelector</code>, removing all such messages from the queue in the process.
	 */
	public List<T> receiveMessages(String queueName, String additionalMessageSelector);


	/**
	 * Receives all messages from the provided queue using the provided <code>additionalMessageSelector</code> and <code>receiveTimeoutMs</code>, removing all such messages from
	 * the queue in the process.
	 * <p>
	 * The receive timeout may need to be modified on a case-by-case basis to ensure that the application waits long enough for an initial response. The initial connection for
	 * queue-polling can be significantly delayed with some brokers, such as ActiveMQ, when using message selectors. See {@link #DEFAULT_RECEIVE_TIMEOUT_MS} for more information.
	 */
	public List<T> receiveMessages(String queueName, String additionalMessageSelector, long receiveTimeoutMs);


	/**
	 * Purges all messages from the provided queue.
	 */
	public void purgeMessages(String queueName);


	/**
	 * Purges all messages from the provided queue using the provided <code>additionalMessageSelector</code>.
	 */
	public void purgeMessages(String queueName, String additionalMessageSelector);


	/**
	 * Purges all messages from the provided queue using the provided <code>additionalMessageSelector</code> and <code>receiveTimeoutMs</code>.
	 * <p>
	 * The receive timeout may need to be modified on a case-by-case basis to ensure that the application waits long enough for an initial response. The initial connection for
	 * queue-polling can be significantly delayed with some brokers, such as ActiveMQ, when using message selectors. See {@link #DEFAULT_RECEIVE_TIMEOUT_MS} for more information.
	 */
	public void purgeMessages(String queueName, String additionalMessageSelector, long receiveTimeoutMs);


	/**
	 * Remove an error message from a queue, and place the original message (not the error) on the to queue.
	 */
	public void moveErrorMessageToTarget(String messageId, String fromQueue, String toQueue);


	/**
	 * Deletes an error message from a queue
	 * Note - both queues are used for the message selector
	 * JMS does not support a real delete call, however if you use receiveSelected it pulls it off the error queue and just don't do anything with it
	 */
	public void deleteErrorMessageFromQueue(String messageId, String fromQueue, String toQueue);
}
