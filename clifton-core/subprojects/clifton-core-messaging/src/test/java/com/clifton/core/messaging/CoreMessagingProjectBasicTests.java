package com.clifton.core.messaging;


import com.clifton.core.messaging.jms.JmsObjectMessage;
import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CoreMessagingProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "core-messaging";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("com.thoughtworks.xstream.");
		imports.add("javax.annotation.");
		imports.add("javax.jms.");
		imports.add("javax.sql.DataSource");
		imports.add("javax.xml.transform.");
		imports.add("org.apache.activemq.");
		imports.add("org.springframework.http.converter.json.Jackson2ObjectMapperBuilder");
		imports.add("org.springframework.jms.");
		imports.add("org.springframework.oxm.");
	}


	@Override
	protected boolean isBooleanGetterMethodsAreValidSkipped(String className) {
		// JmsObjectMessage class implements javax.jms.ObjectMessage which violates the boolean getter / setter method conventions.
		return JmsObjectMessage.class.getName().equals(className);
	}
}
