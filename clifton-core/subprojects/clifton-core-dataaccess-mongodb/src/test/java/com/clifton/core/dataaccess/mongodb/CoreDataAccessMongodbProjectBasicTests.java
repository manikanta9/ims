package com.clifton.core.dataaccess.mongodb;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CoreDataAccessMongodbProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "core-dataaccess-mongodb";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("com.mongodb.");
		imports.add("org.aspectj.");
		imports.add("org.bson.");
		imports.add("org.springframework.data.");
		imports.add("org.springframework.transaction.");
	}
}
