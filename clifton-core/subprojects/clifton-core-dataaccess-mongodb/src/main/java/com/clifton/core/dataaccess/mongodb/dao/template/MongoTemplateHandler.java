package com.clifton.core.dataaccess.mongodb.dao.template;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.SearchRestriction;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.UnaryOperator;


/**
 * The <code>MongoTemplateUtils</code> contains common utility methods used to interface with mongodb.
 *
 * @author stevenf
 */
public interface MongoTemplateHandler {

	public MongoTemplate getMongoTemplate();


	public long count(List<SearchRestriction> restrictionList, Class<?> objectClass);


	public long count(List<SearchRestriction> restrictionList, Class<?> objectClass, String collectionName);


	public <T> List<T> findDistinct(String fieldName, Class<?> objectClass, Class<T> resultClass);


	public <T> List<T> findDistinct(List<SearchRestriction> restrictionList, String fieldName, Class<?> objectClass, Class<T> resultClass);


	public <T> List<T> findDistinct(List<SearchRestriction> restrictionList, String fieldName, Class<?> objectClass, Class<T> resultClass, List<String> collectionNameList);


	public <T extends IdentityObject> void bulkWrite(List<T> beanList, Class<T> beanClass, List<String> collectionNameList, short currentUserId);


	public <T extends IdentityObject> void bulkWrite(List<T> beanList, Class<T> beanClass, List<String> collectionNameList, short currentUserId, int retryCount);


	public <T extends IdentityObject> List<T> executeFindQuery(List<SearchRestriction> restrictionList, Class<T> objectClass);


	public <T extends IdentityObject> List<T> executeFindQuery(List<SearchRestriction> restrictionList, List<String> fieldsToInclude, Class<T> objectClass);


	public <T extends IdentityObject> List<T> executeFindQuery(List<SearchRestriction> restrictionList, List<String> fieldsToInclude, Class<T> objectClass, List<String> collectionNameList);


	public <T extends IdentityObject> List<T> executeFindQuery(Query query, Class<T> objectClass);


	public <T extends IdentityObject> List<T> executeFindQuery(Query query, Class<T> objectClass, List<String> collectionNameList, AtomicLong rowCount);


	public <T extends IdentityObject> List<T> executeFindQuery(Query query, Class<T> objectClass, List<String> collectionNameList, UnaryOperator<T> rowCallback, AtomicLong rowCount);


	public <T extends IdentityObject> void updateMultiByIds(List<T> beanList, Update updateDocument, short currentUserId, Class<T> objectClass);


	public <T extends IdentityObject> long remove(List<T> objectList);


	public <T extends IdentityObject> long executeRemoveQuery(List<SearchRestriction> restrictionList, Class<T> objectClass, List<String> collectionNameList);
}
