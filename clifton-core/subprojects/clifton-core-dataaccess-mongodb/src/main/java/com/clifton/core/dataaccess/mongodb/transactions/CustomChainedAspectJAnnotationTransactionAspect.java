package com.clifton.core.dataaccess.mongodb.transactions;

import com.clifton.core.dataaccess.transactions.BaseCustomAspectJAnnotationTransactionAspect;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.data.transaction.ChainedTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.interceptor.TransactionAttribute;


/**
 * @author stevenf
 */
@Aspect
@Configurable
public class CustomChainedAspectJAnnotationTransactionAspect extends BaseCustomAspectJAnnotationTransactionAspect {

	@Override
	protected TransactionManager determineTransactionManager(TransactionAttribute txAttr) {
		TransactionManager transactionManager = super.determineTransactionManager(txAttr);
		//If the transaction manager is chained and no other transaction manager has been found - use it.
		if (getBeanFactory() != null && getBeanFactory().getBean(TRANSACTION_MANAGER_BEAN_NAME) instanceof ChainedTransactionManager) {
			transactionManager = (TransactionManager) getBeanFactory().getBean(TRANSACTION_MANAGER_BEAN_NAME);
		}
		return transactionManager;
	}
}
