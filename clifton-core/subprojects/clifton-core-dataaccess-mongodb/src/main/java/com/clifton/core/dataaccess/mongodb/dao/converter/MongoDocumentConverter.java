package com.clifton.core.dataaccess.mongodb.dao.converter;


import org.bson.Document;


/**
 * @author stevenf
 */
public interface MongoDocumentConverter {

	public <T> T convert(Document document);


	/**
	 * Returns the concrete class for the given DTO being converted to
	 */
	public Class<?> getDtoClass();
}
