package com.clifton.core.dataaccess.mongodb.dao.template;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.mongodb.dao.converter.MongoDocumentConverter;
import com.clifton.core.dataaccess.mongodb.dao.converter.MongoDocumentConverterLocator;
import com.clifton.core.dataaccess.mongodb.util.MongoTemplateUtils;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.mongodb.bulk.BulkWriteError;
import com.mongodb.bulk.BulkWriteResult;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.CountOptions;
import org.bson.Document;
import org.springframework.data.mongodb.BulkOperationException;
import org.springframework.data.mongodb.core.BulkOperations;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;


/**
 * The <code>MongoTemplateUtils</code> contains common utility methods used to interface with mongodb.
 *
 * @author stevenf
 */
public class MongoTemplateHandlerImpl implements MongoTemplateHandler {

	private MongoTemplate mongoTemplate;
	private MongoDocumentConverterLocator mongoDocumentConverterLocator;


	//////////////////////////////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public long count(List<SearchRestriction> restrictionList, Class<?> objectClass) {
		return count(restrictionList, objectClass, null);
	}


	@Override
	public long count(List<SearchRestriction> restrictionList, Class<?> objectClass, String collectionName) {
		if (collectionName != null) {
			return getMongoTemplate().count(MongoTemplateUtils.configureQuery(restrictionList), objectClass, collectionName);
		}
		else {
			return getMongoTemplate().count(MongoTemplateUtils.configureQuery(restrictionList), objectClass);
		}
	}


	@Override
	public <T> List<T> findDistinct(String fieldName, Class<?> objectClass, Class<T> resultClass) {
		return findDistinct(null, fieldName, objectClass, resultClass, null);
	}


	@Override
	public <T> List<T> findDistinct(List<SearchRestriction> restrictionList, String fieldName, Class<?> objectClass, Class<T> resultClass) {
		return findDistinct(restrictionList, fieldName, objectClass, resultClass, null);
	}


	@Override
	public <T> List<T> findDistinct(List<SearchRestriction> restrictionList, String fieldName, Class<?> objectClass, Class<T> resultClass, List<String> collectionNameList) {
		List<T> resultList = new ArrayList<>();
		Query query = MongoTemplateUtils.configureQuery(restrictionList);
		if (!CollectionUtils.isEmpty(collectionNameList)) {
			for (String collectionName : CollectionUtils.getIterable(collectionNameList)) {
				resultList.addAll(getMongoTemplate().findDistinct(query, fieldName, collectionName, objectClass, resultClass));
			}
		}
		else {
			resultList = getMongoTemplate().findDistinct(query, fieldName, objectClass, resultClass);
		}
		return resultList;
	}


	@Override
	public <T extends IdentityObject> void bulkWrite(List<T> beanList, Class<T> beanClass, List<String> collectionNameList, short currentUserId) {
		bulkWrite(beanList, beanClass, collectionNameList, currentUserId, 0);
	}


	@Override
	public <T extends IdentityObject> void bulkWrite(List<T> beanList, Class<T> beanClass, List<String> collectionNameList, short currentUserId, int retryCount) {
		try {
			if (!CollectionUtils.isEmpty(beanList)) {
				for (T bean : CollectionUtils.getIterable(beanList)) {
					MongoTemplateUtils.populateRecordStamp(bean, currentUserId);
				}
				if (!CollectionUtils.isEmpty(collectionNameList)) {
					for (String collectionName : CollectionUtils.getIterable(collectionNameList)) {
						getMongoTemplate().bulkOps(BulkOperations.BulkMode.UNORDERED, collectionName).insert(beanList).execute();
					}
				}
				else {
					getMongoTemplate().bulkOps(BulkOperations.BulkMode.UNORDERED, beanClass).insert(beanList).execute();
				}
			}
		}
		catch (BulkOperationException boe) {
			if (retryCount < 3) {
				List<T> retryBeanList = new ArrayList<>();
				for (BulkWriteError writeError : CollectionUtils.getIterable(boe.getErrors())) {
					retryBeanList.add(beanList.get(writeError.getIndex()));
				}
				bulkWrite(retryBeanList, beanClass, collectionNameList, currentUserId, retryCount + 1);
			}
			else {
				LogUtils.error(MongoTemplateHandlerImpl.class, "Some items [" + boe.getErrors().size() + "] failed during bulkWrite operation: " + boe.getMessage(), boe);
			}
		}
	}


	@Override
	public <T extends IdentityObject> List<T> executeFindQuery(List<SearchRestriction> restrictionList, Class<T> objectClass) {
		return executeFindQuery(restrictionList, null, objectClass, null);
	}


	@Override
	public <T extends IdentityObject> List<T> executeFindQuery(List<SearchRestriction> restrictionList, List<String> fieldsToInclude, Class<T> objectClass) {
		return executeFindQuery(restrictionList, null, objectClass, null);
	}


	@Override
	public <T extends IdentityObject> List<T> executeFindQuery(List<SearchRestriction> restrictionList, List<String> fieldsToInclude, Class<T> objectClass, List<String> collectionNameList) {
		Query query = MongoTemplateUtils.configureQuery(restrictionList);
		if (!CollectionUtils.isEmpty(fieldsToInclude)) {
			for (String field : CollectionUtils.getIterable(fieldsToInclude)) {
				query.fields().include(field);
			}
		}
		return executeFindQuery(query, objectClass, collectionNameList, new AtomicLong(-1));
	}


	@Override
	public <T extends IdentityObject> List<T> executeFindQuery(Query query, Class<T> objectClass) {
		return executeFindQuery(query, objectClass, null, new AtomicLong(-1));
	}


	@Override
	public <T extends IdentityObject> List<T> executeFindQuery(Query query, Class<T> objectClass, List<String> collectionNameList, AtomicLong rowCount) {
		return executeFindQuery(query, objectClass ,collectionNameList, null, rowCount);
	}


	@Override
	public <T extends IdentityObject> List<T> executeFindQuery(Query query, Class<T> objectClass, List<String> collectionNameList, UnaryOperator<T> rowCallback, AtomicLong rowCount) {
		MongoDocumentConverter mongoDocumentConverter = getMongoDocumentConverterLocator().locate(objectClass);
		LogUtils.info(this.getClass(), "Query: [" + query.getQueryObject().toJson() + " " + query.getSortObject().toJson() + "]");
		List<T> resultList = new ArrayList<>();
		if (CollectionUtils.isEmpty(collectionNameList)) {
			collectionNameList = Collections.singletonList(ContextConventionUtils.getBeanNameFromClassName(objectClass.getName()));
		}
		for (String collectionName : CollectionUtils.getIterable(collectionNameList)) {
			resultList.addAll(executeFindQuery(query, collectionName, rowCallback, mongoDocumentConverter, rowCount));
		}
		return resultList;
	}


	private <T> List<T> executeFindQuery(Query query, String collectionName, UnaryOperator<T> rowCallback, MongoDocumentConverter mongoDocumentConverter, AtomicLong rowCount) {
		long queryTime = System.currentTimeMillis();
		List<T> resultList = new ArrayList<>();
		Document queryObject = query.getQueryObject();
		Document sortObject = query.getSortObject();
		if (rowCount.intValue() != -1) {
			try {
				rowCount.set(getMongoTemplate().getCollection(collectionName).countDocuments(query.exhaust().getQueryObject(), new CountOptions().maxTime(5000, TimeUnit.MILLISECONDS)));
			}
			catch (Exception e) {
				//Log the query count as an error, set rowCount to max, and move on.
				LogUtils.warn(getClass(), "Query Count Exceeded Limit [" + (System.currentTimeMillis() - queryTime) + "]: [" + query.getQueryObject().toJson() + " " + query.getSortObject().toJson() + "]");
				rowCount.set(Integer.MAX_VALUE);
			}
		}
		FindIterable<Document> docList = getMongoTemplate().getCollection(collectionName)
				.find(queryObject, Document.class)
				.sort(sortObject)
				.skip((int) query.getSkip())
				.limit(query.getLimit());

		try (MongoCursor<Document> cursor = docList.iterator()) {
			while (cursor.hasNext()) {
				T result = mongoDocumentConverter.convert(cursor.next());
				if (rowCallback == null) {
					resultList.add(result);
				}
				else {
					rowCallback.apply(result);
				}
			}
		}
		if ((System.currentTimeMillis() - queryTime) > 5000) {
			LogUtils.warn(getClass(),"Query Time [ms: " + (System.currentTimeMillis() - queryTime) + "]:" + queryObject.toJson() + " " + sortObject.toJson() + "]");
		}
		return resultList;
	}


	@Override
	public <T extends IdentityObject> void updateMultiByIds(List<T> beanList, Update updateDocument, short currentUserId, Class<T> objectClass) {
		updateDocument.set("updateDate", new Date());
		updateDocument.set("updateUserId", currentUserId);

		Query query = new Query().addCriteria(org.springframework.data.mongodb.core.query.Criteria.where("_id")
				.in(beanList.stream().map(IdentityObject::getIdentity).collect(Collectors.toList())));
		getMongoTemplate().updateMulti(query, updateDocument, objectClass);
	}


	@Override
	public <T extends IdentityObject> long remove(List<T> objectList) {
		long deletedCount = 0;
		for (T object : CollectionUtils.getIterable(objectList)) {
			getMongoTemplate().remove(object);
			deletedCount++;
		}
		return deletedCount;
	}


	@Override
	public <T extends IdentityObject> long executeRemoveQuery(List<SearchRestriction> restrictionList, Class<T> objectClass, List<String> collectionNameList) {
		long deletedCount = 0;
		Query query = MongoTemplateUtils.configureQuery(restrictionList);
		AssertUtils.assertNotEmpty(collectionNameList, "You must specify a collectionName!");
		try {
			for (String collectionName : CollectionUtils.getIterable(collectionNameList)) {
				BulkWriteResult writeResult = getMongoTemplate().bulkOps(BulkOperations.BulkMode.UNORDERED, objectClass, collectionName).remove(query).execute();
				deletedCount += writeResult.getDeletedCount();
			}
		}
		catch (BulkOperationException boe) {
			LogUtils.error(MongoTemplateHandlerImpl.class, "Some items [" + boe.getErrors().size() + "] failed during remove operation: " + boe.getMessage(), boe);
		}
		return deletedCount;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public MongoTemplate getMongoTemplate() {
		return this.mongoTemplate;
	}


	public void setMongoTemplate(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}


	public MongoDocumentConverterLocator getMongoDocumentConverterLocator() {
		return this.mongoDocumentConverterLocator;
	}


	public void setMongoDocumentConverterLocator(MongoDocumentConverterLocator mongoDocumentConverterLocator) {
		this.mongoDocumentConverterLocator = mongoDocumentConverterLocator;
	}
}
