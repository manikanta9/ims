package com.clifton.core.dataaccess.mongodb.dao.converter;


/**
 * The <code>MongoDocumentConverterLocator</code> is used to identify all beans that implement the {@link MongoDocumentConverter} interface.
 * The beans are stored in a map and retrieved/executed when conversion from a mongo document to a given DTO object is triggered.
 *
 * @author StevenF
 */
public interface MongoDocumentConverterLocator {

	public <T> MongoDocumentConverter locate(Class<T> entityClass);
}
