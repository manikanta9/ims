package com.clifton.core.dataaccess.mongodb.search;

import com.clifton.core.dataaccess.search.SearchConfigurer;

import java.util.List;


/**
 * The <code>MongoTemplateSearchConfigurer</code> extends the base {@link SearchConfigurer} interface
 * to allow passing mongo specific collection names when a query needs to be run against multiple collections.
 *
 * @author stevenf
 */
public interface MongoTemplateSearchConfigurer<T> extends SearchConfigurer<T> {

	public List<String> getCollectionNameList();

	public boolean isArchiveQuery();

	public boolean isRetrieveRowCount();
}
