package com.clifton.core.dataaccess.mongodb.search;


import com.clifton.core.dataaccess.mongodb.util.MongoTemplateUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.restrictions.ArrayRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.BigDecimalRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.BooleanRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.DateRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.DoubleRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.EnumRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ForeignKeyRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.LongRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ShortRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.StringRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.TimeRestrictionDefinition;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.FieldValidationException;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.query.Query;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>MongoTemplateSearchFormConfigurer</code> class is a SearchConfigurer that configures mongo Queries
 * to filter using restrictions of the specified SearchForm.
 *
 * @author stevenf
 */
public class MongoTemplateSearchFormConfigurer implements MongoTemplateSearchConfigurer<Query> {

	private final Map<String, SearchRestrictionDefinition> restrictionDefinitionMap = new ConcurrentHashMap<>();

	private final BaseEntitySearchForm sortableSearchForm;
	private final List<String> collectionNameList;
	private final boolean skipDefaultSorting;


	public MongoTemplateSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		this.sortableSearchForm = searchForm;
		this.collectionNameList = null;
		this.skipDefaultSorting = false;
		configureSearchForm();
	}


	public MongoTemplateSearchFormConfigurer(BaseEntitySearchForm searchForm, List<String> collectionNameList) {
		this.sortableSearchForm = searchForm;
		this.collectionNameList = collectionNameList;
		this.skipDefaultSorting = false;
		configureSearchForm();
	}


	public MongoTemplateSearchFormConfigurer(BaseEntitySearchForm searchForm, List<String> collectionNameList, boolean skipDefaultSorting) {
		this.sortableSearchForm = searchForm;
		this.collectionNameList = collectionNameList;
		this.skipDefaultSorting = skipDefaultSorting;
		configureSearchForm();
	}


	@Override
	public boolean isReadUncommittedRequested() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<String> getCollectionNameList() {
		return this.collectionNameList;
	}


	@SuppressWarnings({"unchecked"})
	protected <E extends Enum<E>> void configureSearchForm() {
		BaseEntitySearchForm searchForm = getSortableSearchForm();
		// populate restriction definition map for each search field (form fields marked by @SearchField)
		// also checks inherited fields
		Field[] fields = ClassUtils.getClassFields(searchForm.getClass(), true, true);
		for (Field field : fields) {
			SearchField searchField = AnnotationUtils.findAnnotation(field, SearchField.class);
			if (searchField != null) {
				SearchFieldCustomTypes multipleFieldTypes = searchField.searchFieldCustomType();
				SearchRestrictionDefinition definition;
				Class<?> type = field.getType();
				String searchFieldName = SearchField.SAME_AS_MARKED_FIELD.equals(searchField.searchField()) ? field.getName() : searchField.searchField();
				String searchFieldPath = SearchField.SAME_TABLE.equals(searchField.searchFieldPath()) ? null : searchField.searchFieldPath();
				final String sortFieldName;
				if (SearchField.SORT_NOT_ALLOWED.equals(searchField.sortField())) {
					sortFieldName = null;
				}
				else if (SearchField.SORT_BY_SEARCH_FIELD.equals(searchField.sortField())) {
					if (ArrayUtils.anyMatch(searchField.comparisonConditions(), ComparisonConditions::isSubQuery)) {
						// Disable sorting by default for sub-query conditions
						sortFieldName = null;
					}
					else if (multipleFieldTypes.isOrderByMultipleSupported()) {
						sortFieldName = searchFieldName;
					}
					else {
						sortFieldName = searchFieldName.split(",")[0];
					}
				}
				else {
					sortFieldName = searchField.sortField();
				}

				// created the definition based on field's data type
				if (Integer.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL,
								ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.NOT_EQUALS_OR_IS_NULL};
					}

					if (searchFieldName.endsWith(".id") || StringUtils.isEqual("id", searchFieldName)) {
						definition = new ForeignKeyRestrictionDefinition<Integer>(new IntegerRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(),
								sortFieldName, searchField.required(), comparisonConditions, multipleFieldTypes), null);
					}
					else {
						definition = new IntegerRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions, multipleFieldTypes);
					}
				}
				else if (Long.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL,
								ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.NOT_EQUALS_OR_IS_NULL};
					}

					if (searchFieldName.endsWith(".id") || StringUtils.isEqual("id", searchFieldName)) {
						definition = new ForeignKeyRestrictionDefinition<Long>(new LongRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName,
								searchField.required(), comparisonConditions, multipleFieldTypes), null);
					}
					else {
						definition = new LongRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions, multipleFieldTypes);
					}
				}
				else if (Short.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL,
								ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.NOT_EQUALS_OR_IS_NULL};
					}

					if (searchFieldName.endsWith(".id") || StringUtils.isEqual("id", searchFieldName)) {
						definition = new ForeignKeyRestrictionDefinition<Long>(new ShortRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName,
								searchField.required(), comparisonConditions, multipleFieldTypes), null);
					}
					else {
						definition = new ShortRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions, multipleFieldTypes);
					}
				}
				else if (String.class.equals(type)) {
					// default to LIKE comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.LIKE, ComparisonConditions.EQUALS, ComparisonConditions.IN, ComparisonConditions.BEGINS_WITH,
								ComparisonConditions.NOT_LIKE, ComparisonConditions.NOT_EQUALS, ComparisonConditions.ENDS_WITH, ComparisonConditions.IS_NULL, ComparisonConditions.IS_NOT_NULL};
					}
					definition = new StringRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
							comparisonConditions, multipleFieldTypes);
				}
				else if (Date.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.EQUALS_OR_IS_NULL,
								ComparisonConditions.NOT_EQUALS_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL,
								ComparisonConditions.LESS_THAN_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, ComparisonConditions.IS_NULL};
					}
					definition = new DateRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(), comparisonConditions, multipleFieldTypes, searchField.dateFieldIncludesTime());
				}
				else if (Time.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.EQUALS_OR_IS_NULL,
								ComparisonConditions.NOT_EQUALS_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL,
								ComparisonConditions.LESS_THAN_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, ComparisonConditions.IS_NULL};
					}
					definition = new TimeRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(), comparisonConditions, multipleFieldTypes);
				}
				else if (BigDecimal.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS};
					}
					definition = new BigDecimalRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
							comparisonConditions, multipleFieldTypes);
				}
				else if (Boolean.class.equals(type)) {
					// default to EQ comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.IS_NULL, ComparisonConditions.IS_NOT_NULL};
					}
					definition = new BooleanRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
							comparisonConditions, multipleFieldTypes);
				}
				else if (Double.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS};
					}
					definition = new DoubleRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(), comparisonConditions);
				}
				else if (Enum.class.isAssignableFrom(type)) {
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						definition = new EnumRestrictionDefinition<>((Class<E>) type, field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required());
					}
					else {
						definition = new EnumRestrictionDefinition<>((Class<E>) type, field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions);
					}
				}
				else if (type.isArray()) {
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						definition = new ArrayRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, multipleFieldTypes);
					}
					else {
						definition = new ArrayRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, comparisonConditions, multipleFieldTypes);
					}
				}
				// else if (ObjectId.class.isAssignableFrom(type)) {
				// 	// default to LIKE comparison if none is defined
				// 	ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
				// 	if (comparisonConditions.length == 0) {
				// 		comparisonConditions = new ComparisonConditions[]{ComparisonConditions.LIKE, ComparisonConditions.EQUALS};
				// 	}
				// 	definition = new StringRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
				// 			comparisonConditions, multipleFieldTypes);
				// }
				else {
					throw new FieldValidationException("Unsupported search field data type '" + type + "' for field: " + field.getName(), field.getName());
				}
				definition.setSearchFieldDataType(type.isArray() ? type.getComponentType() : type);
				this.restrictionDefinitionMap.put(field.getName(), definition);

				// populate additional restrictions (field values that are not part of restrictionList
				Object value;
				try {
					field.setAccessible(true);
					value = field.get(searchForm);
				}
				catch (Exception e) {
					throw new RuntimeException("Cannot read value of field: " + field.getName(), e);
				}
				if (value != null) {
					boolean add = true;
					if (searchForm.getRestrictionList() == null) {
						searchForm.setRestrictionList(new ArrayList<>());
					}
					for (SearchRestriction restriction : searchForm.getRestrictionList()) {
						if (field.getName().equals(restriction.getField())) {
							add = false;
							break;
						}
					}
					if (add) {
						SearchRestriction restriction = new SearchRestriction();
						restriction.setField(field.getName());
						// use the first search comparison by default
						int conditionCount = CollectionUtils.getSize(definition.getSearchConditionList());
						if (conditionCount > 0) {
							ComparisonConditions comparison = definition.getSearchConditionList().iterator().next();
							restriction.setComparison(comparison);
							restriction.setValue(value);
							searchForm.getRestrictionList().add(restriction);
						}
						else {
							throw new FieldValidationException("When '" + field.getName()
									+ "' search field value is set directly, only 1 comparison condition must be defined or EQUALS must be present. Found "
									+ CollectionUtils.getSize(definition.getSearchConditionList()), field.getName());
						}
					}
				}
			}
		}
		// Remove items from the restriction list the are properties of the search form.
		SearchUtils.moveCustomRestrictions(searchForm, this.restrictionDefinitionMap);
		// validate everything (FormUtils??? move method from SqlSearchFormConfigurer???)
		//TODO - DO I need to validate
		//SearchUtils.validateRestrictionList(searchForm.getRestrictionList(), this.restrictionDefinitionMap, searchForm);
	}


	@Override
	//TODO - definitely doesn't handle all scenarios yet.
	public void configureCriteria(Query query) {
		BaseEntitySearchForm searchForm = getSortableSearchForm();
		MongoTemplateUtils.configureQuery(query, searchForm.getRestrictionList(), this.restrictionDefinitionMap, false);
	}


	@Override
	public boolean configureOrderBy(Query query) {
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(getSortableSearchForm().getOrderBy());
		if (CollectionUtils.isEmpty(orderByList)) {
			return this.skipDefaultSorting;
		}
		for (OrderByField orderByField : CollectionUtils.getIterable(orderByList)) {
			switch (orderByField.getDirection()) {
				case ASC: {
					query.with(Sort.by(Sort.Order.asc(orderByField.getName())));//TODO - error when I use ignore case - may just need case insensitive indexes?  Look later .ignoreCase()));
					break;
				}
				case DESC:
					query.with(Sort.by(Sort.Order.desc(orderByField.getName())));//.ignoreCase()));
			}
		}
		return true;
	}


	@Override
	public int getLimit() {
		return this.sortableSearchForm.getLimit();
	}


	@Override
	public Integer getMaxLimit() {
		return this.sortableSearchForm.getMaxLimit();
	}


	@Override
	public int getStart() {
		return this.sortableSearchForm.getStart();
	}


	@Override
	public boolean isArchiveQuery() {
		BaseEntitySearchForm searchForm = getSortableSearchForm();
		// populate restriction definition map for each search field (form fields marked by @SearchField)
		// also checks inherited fields
		Field[] fields = ClassUtils.getClassFields(searchForm.getClass(), true, true);
		Boolean archive = null;
		for (Field field : fields) {
			if ("archived".equals(field.getName())) {
				try {
					archive = (Boolean) field.get(searchForm);
				}
				catch (Exception e) {
					// If its not a Boolean object we probably dont care.
				}
			}
		}
		return BooleanUtils.isTrue(archive);
	}

	@Override
	public boolean isRetrieveRowCount() {
		return getSortableSearchForm().isRetrieveRowCount();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public BaseEntitySearchForm getSortableSearchForm() {
		return this.sortableSearchForm;
	}
}
