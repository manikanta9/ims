package com.clifton.core.dataaccess.mongodb.dao.converter;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.AssertUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author StevenF
 */
@Component
public class MongoDocumentConverterLocatorImpl implements MongoDocumentConverterLocator, InitializingBean, ApplicationContextAware {

	private ApplicationContextService applicationContextService;
	private Map<Class<?>, MongoDocumentConverter> beanConverterMap = new ConcurrentHashMap<>();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T> MongoDocumentConverter locate(Class<T> entityClass) {
		AssertUtils.assertNotNull(entityClass, "Required entity class cannot be null.");
		return getBeanConverterMap().get(entityClass);
	}


	/**
	 * Attempts to locate any converters that were created specifically for DTOs; if none are found,
	 */
	@Override
	public void afterPropertiesSet() {
		Map<String, MongoDocumentConverter> beanMap = getApplicationContext().getBeansOfType(MongoDocumentConverter.class);
		for (Map.Entry<String, MongoDocumentConverter> beanMapEntry : beanMap.entrySet()) {
			MongoDocumentConverter mongoDocumentConverter = beanMapEntry.getValue();
			if (getBeanConverterMap().containsKey(mongoDocumentConverter.getDtoClass())) {
				throw new RuntimeException("Cannot register '" + beanMapEntry.getKey() + "' as a converter '" + mongoDocumentConverter.getDtoClass()
						+ "' because this class already has a registered converter.");
			}
			getBeanConverterMap().put(mongoDocumentConverter.getDtoClass(), mongoDocumentConverter);
		}
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public Map<Class<?>, MongoDocumentConverter> getBeanConverterMap() {
		return this.beanConverterMap;
	}


	public void setBeanConverterMap(Map<Class<?>, MongoDocumentConverter> beanConverterMap) {
		this.beanConverterMap = beanConverterMap;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
