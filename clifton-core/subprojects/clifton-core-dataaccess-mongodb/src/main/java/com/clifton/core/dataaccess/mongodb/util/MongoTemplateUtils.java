package com.clifton.core.dataaccess.mongodb.util;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.bson.types.Decimal128;
import org.bson.types.ObjectId;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * The <code>MongoTemplateUtils</code> contains common utility methods used to interface with mongodb.
 *
 * @author stevenf
 */
public final class MongoTemplateUtils {

	public static Query configureQuery(List<SearchRestriction> restrictionList) {
		return configureQuery(restrictionList, null);
	}


	public static Query configureQuery(List<SearchRestriction> restrictionList, Map<String, SearchRestrictionDefinition> restrictionDefinitionMap) {
		return configureQuery(new Query(), restrictionList, restrictionDefinitionMap, false);
	}


	public static Query configureQuery(Query query, List<SearchRestriction> restrictionList, Map<String, SearchRestrictionDefinition> restrictionDefinitionMap, boolean orRestrictionList) {
		List<Criteria> criteriaList = new ArrayList<>();
		for (SearchRestriction restriction : CollectionUtils.getIterable(restrictionList)) {
			if (restriction.getOrRestrictionList() != null) {
				configureQuery(query, restriction.getOrRestrictionList(), restrictionDefinitionMap, true);
			}
			else {
				Criteria criteria;
				String searchField = restriction.getField();
				Object searchFieldValue = restriction.getValue();
				Class<?> dataType = restriction.getDataTypeName() != null ? restriction.getDataTypeName().getDataType() : null;
				ComparisonConditions comparisonCondition = restriction.getComparison();
				if (!CollectionUtils.isEmpty(restrictionDefinitionMap)) {
					SearchRestrictionDefinition definition = restrictionDefinitionMap.get(restriction.getField());
					if (definition != null) {
						//check to swap equals to the defined comparison condition
						if (!definition.getSearchConditionList().contains(comparisonCondition)) {
							if (comparisonCondition == ComparisonConditions.EQUALS && definition.getSearchConditionList().size() == 1) {
								ComparisonConditions definedComparison = definition.getSearchConditionList().get(0);
								if (definedComparison == ComparisonConditions.IS_NULL || definedComparison == ComparisonConditions.IS_NOT_NULL || definedComparison == ComparisonConditions.NOT_EQUALS) {
									// allow remapping EQUALS to IS NOT or IS NOT NULL
									comparisonCondition = definedComparison;
								}
							}
						}
						dataType = definition.getSearchFieldDataType();
						searchField = definition.getSearchFieldName();
						if (definition.getSearchFieldPath() != null) {
							searchField = definition.getSearchFieldPath() + "." + searchField;
						}
					}
				}
				//TODO - hack for id until I can force equals from UI restriction list
				if ("id".equals(restriction.getField())) {
					searchFieldValue = new ObjectId(restriction.getValue().toString());
					criteria = Criteria.where("_id").is(searchFieldValue);
				}
				else if ("ids".equals(restriction.getField())) {
					List<ObjectId> objectIdList = new ArrayList<>();
					if (restriction.getValue().getClass().isArray()) {
						for (String value : (String[]) restriction.getValue()) {
							objectIdList.add(new ObjectId(value));
						}
						searchFieldValue = objectIdList;
					}
					criteria = Criteria.where("_id").in(convertValueAsList(searchFieldValue, restriction.getDataTypeName()));
				}
				else if (comparisonCondition == ComparisonConditions.EQUALS) {
					criteria = Criteria.where(searchField).is(convertValue(searchFieldValue, dataType));
				}
				else if (comparisonCondition == ComparisonConditions.NOT_EQUALS) {
					criteria = Criteria.where(searchField).ne(convertValue(searchFieldValue, dataType));
				}
				else if (comparisonCondition == ComparisonConditions.GREATER_THAN) {
					criteria = Criteria.where(searchField).gt(convertValue(searchFieldValue, dataType));
				}
				else if (comparisonCondition == ComparisonConditions.GREATER_THAN_OR_EQUALS) {
					criteria = Criteria.where(searchField).gte(convertValue(searchFieldValue, dataType));
				}
				else if (comparisonCondition == ComparisonConditions.LESS_THAN) {
					criteria = Criteria.where(searchField).lt(convertValue(searchFieldValue, dataType));
				}
				else if (comparisonCondition == ComparisonConditions.LESS_THAN_OR_EQUALS) {
					criteria = Criteria.where(searchField).lte(convertValue(searchFieldValue, dataType));
				}
				else if (comparisonCondition == ComparisonConditions.BEGINS_WITH) {
					// //Case insensitive 'starts with' most regex expressions and not efficient in mongo - starts with is decent
					// criteria = Criteria.where(searchField).regex(Pattern.compile("(?i)^" + searchFieldValue));
					// Case sensitive 'starts with' most regex expressions and not efficient in mongo - starts with is decent
					if (searchFieldValue instanceof String) {
						criteria = Criteria.where(searchField).regex(Pattern.compile("(?i)^" + Pattern.quote(String.valueOf(searchFieldValue))));
					}
					else {
						criteria = Criteria.where(searchField).regex(Pattern.compile("(?i)^" + searchFieldValue, Pattern.LITERAL));
					}
				}
				else if (comparisonCondition == ComparisonConditions.ENDS_WITH) {
					if (searchFieldValue instanceof String) {
						criteria = Criteria.where(searchField).regex(Pattern.compile(Pattern.quote(String.valueOf(searchFieldValue) + "$")));
					}
					else {
						criteria = Criteria.where(searchField).regex(Pattern.compile(searchFieldValue + "$", Pattern.LITERAL));
					}
				}
				else if (comparisonCondition == ComparisonConditions.LIKE || comparisonCondition == ComparisonConditions.NOT_LIKE) {
					criteria = Criteria.where(searchField);
					if (comparisonCondition == ComparisonConditions.NOT_LIKE) {
						criteria = criteria.not();
					}
					// //Case insensitive 'starts with' most regex expressions and not efficient in mongo - starts with is decent
					// criteria = Criteria.where(searchField).regex(Pattern.compile("(?i)^" + searchFieldValue));
					// Case sensitive 'starts with' most regex expressions and not efficient in mongo - starts with is decent
					if (searchFieldValue instanceof String) {
						criteria = Criteria.where(searchField).regex(Pattern.compile("(?i).*" + Pattern.quote(String.valueOf(searchFieldValue)) + ".*"));
					}
					else {
						criteria.regex(Pattern.compile("(?i).*" + searchFieldValue + ".*", Pattern.LITERAL));
					}
				}
				else if (comparisonCondition == ComparisonConditions.IN) {
					criteria = Criteria.where(searchField).in(convertValueAsList(searchFieldValue, restriction.getDataTypeName()));
				}
				else if (comparisonCondition == ComparisonConditions.IS_NULL) {
					searchFieldValue = convertValue(searchFieldValue, dataType);
					if (searchFieldValue == null) {
						searchFieldValue = true;
					}
					if (searchField.contains(",")) {
						List<Criteria> orCriteriaList = new ArrayList<>();
						for (String field : searchField.split(",")) {
							orCriteriaList.add(Criteria.where(field).exists(!(boolean) convertValue(searchFieldValue, dataType)));
						}
						criteria = new Criteria().orOperator(orCriteriaList.toArray(new Criteria[0]));
					}
					else {
						criteria = Criteria.where(searchField).exists(!(boolean) convertValue(searchFieldValue, dataType));
					}
				}
				else if (comparisonCondition == ComparisonConditions.IS_NOT_NULL) {
					searchFieldValue = convertValue(searchFieldValue, dataType);
					if (searchFieldValue == null) {
						searchFieldValue = true;
					}
					if (searchField.contains(",")) {
						List<Criteria> orCriteriaList = new ArrayList<>();
						for (String field : searchField.split(",")) {
							orCriteriaList.add(Criteria.where(field).exists((boolean) convertValue(searchFieldValue, dataType)));
						}
						criteria = new Criteria().orOperator(orCriteriaList.toArray(new Criteria[0]));
					}
					else {
						criteria = Criteria.where(searchField).exists((boolean) convertValue(searchFieldValue, dataType));
					}
				}
				else {
					throw new UnsupportedOperationException("Unsupported Comparison Condition: " + comparisonCondition.name());
				}
				criteriaList.add(criteria);
			}
		}
		if (orRestrictionList) {
			if (!CollectionUtils.isEmpty(criteriaList)) {
				Criteria orCriteria = new Criteria().orOperator(criteriaList.toArray(new Criteria[0]));
				query.addCriteria(orCriteria);
			}
		}
		else {
			if (!CollectionUtils.isEmpty(criteriaList)) {
				for (Criteria criteria : CollectionUtils.getIterable(criteriaList)) {
					query.addCriteria(criteria);
				}
			}
		}
		query.maxTimeMsec(90000);
		return query;
	}


	private static Object convertValue(Object value, Class<?> dataType) {
		if (dataType != null && value instanceof String) {
			if (dataType.isAssignableFrom(Short.class)) {
				return Short.valueOf(value.toString());
			}
			if (dataType.isAssignableFrom(Integer.class)) {
				return Integer.valueOf(value.toString());
			}
			if (dataType.isAssignableFrom(BigDecimal.class)) {
				return new Decimal128(new BigDecimal(value.toString()));
			}
			if (dataType.isAssignableFrom(Date.class)) {
				return DateUtils.toDate(value.toString());
			}
			if (dataType.isAssignableFrom(Boolean.class)) {
				return BooleanUtils.isTrue(value);
			}
		}

		if (value instanceof java.sql.Date || value instanceof java.sql.Timestamp || value instanceof java.sql.Time) {
			return new java.util.Date(((java.util.Date) value).getTime());
		}
		return value;
	}


	/**
	 * Because of restrictionList UI bug it passes item as string when using toolbar filter...
	 */
	@SuppressWarnings("unchecked")
	private static List<Object> convertValueAsList(Object value, DataTypeNames dataTypeName) {
		if (dataTypeName != null) {
			Class<?> dataType = dataTypeName.getDataType();
			if (Collection.class.isAssignableFrom(value.getClass())) {
				List<Object> objectList = new ArrayList<>();
				for (Object o : (Collection<?>) value) {
					if (dataType.isAssignableFrom(o.getClass())) {
						objectList.add(o);
					}
					else {
						//TODO - Need tests
						objectList.add(DataTypeNameUtils.convertObjectToDataTypeName(value, dataTypeName));
					}
				}
				return objectList;
			}
			else if (!value.getClass().isArray()) {
				if (dataType != null) {
					if (dataType.isAssignableFrom(Short.class)) {
						return Collections.singletonList(Short.valueOf((String) value));
					}
					else if (dataType.isAssignableFrom(Integer.class)) {
						return Collections.singletonList(Integer.valueOf((String) value));
					}
				}
			}
		}
		else if (value instanceof String[]) {
			return Arrays.asList((String[]) value);
		}
		else if (value instanceof Short[]) {
			return Arrays.asList((Short[]) value);
		}
		else if (value instanceof Integer[]) {
			return Arrays.asList((Integer[]) value);
		}
		if (Collection.class.isAssignableFrom(value.getClass())) {
			return new ArrayList<>((Collection<Object>) value);
		}
		else {
			if (value instanceof String) {
				return Arrays.stream(((String) value).split("<OR>")).map(StringUtils::trim).collect(Collectors.toList());
			}
		}
		return Collections.singletonList(value);
	}


	public static <T extends IdentityObject> void populateRecordStamp(T bean, short currentUserId) {
		if (bean instanceof UpdatableOnlyEntity) {
			UpdatableOnlyEntity updatableOnlyEntity = (UpdatableOnlyEntity) bean;
			Date now = new Date();
			if (bean.isNewBean() && bean instanceof UpdatableEntity) {
				UpdatableEntity updatableBean = (UpdatableEntity) bean;
				updatableBean.setCreateUserId(currentUserId);
				updatableBean.setCreateDate(now);
			}
			updatableOnlyEntity.setUpdateUserId(currentUserId);
			updatableOnlyEntity.setUpdateDate(now);
		}
	}
}
