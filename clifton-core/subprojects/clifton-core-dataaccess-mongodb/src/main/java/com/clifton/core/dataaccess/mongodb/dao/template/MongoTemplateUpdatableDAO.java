package com.clifton.core.dataaccess.mongodb.dao.template;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapperWithBooleanResult;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.BaseObserverableDAOImpl;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.template.TemplateUpdatableDAO;
import com.clifton.core.dataaccess.mongodb.search.MongoTemplateSearchConfigurer;
import com.clifton.core.dataaccess.mongodb.util.MongoTemplateUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import org.bson.types.ObjectId;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Collation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;


/**
 * The <code>MongoTemplateUpdatableDAO</code> provides simple DAO CRUD implementation methods for MongoDB tables.
 *
 * @author stevenf
 */
public class MongoTemplateUpdatableDAO<T extends IdentityObject> extends BaseObserverableDAOImpl<T> implements TemplateUpdatableDAO<T, MongoTemplateSearchConfigurer<Query>, Query> {

	private static final String PRIMARY_KEY_COLUMN_NAME = "_id";

	private ContextHandler contextHandler;
	private MongoTemplateHandler mongoTemplateHandler;
	private MongoTemplateHandler dataLakeTemplateHandler;


	@Override
	public T findByNaturalKey(T searchObject) {
		return null;
	}


	@Override
	public T findByPrimaryKey(Serializable primaryKey) {
		T returnObject = CollectionUtils.getOnlyElement(getMongoTemplateHandler().executeFindQuery(new Query().addCriteria(Criteria.where(PRIMARY_KEY_COLUMN_NAME).is(new ObjectId(primaryKey.toString()))), getConfiguration().getBeanClass()));
		if (returnObject == null) {
			//Not ideal but since actual null returns(the worst case) should be minimal given we have the primaryKey and archive queries are expected to be slow
			if (getDataLakeTemplateHandler() != null) {
				returnObject = CollectionUtils.getOnlyElement(getDataLakeTemplateHandler().executeFindQuery(new Query().addCriteria(Criteria.where(PRIMARY_KEY_COLUMN_NAME).is(new ObjectId(primaryKey.toString()))), getConfiguration().getBeanClass()));
			}
		}
		return returnObject;
	}


	@Override
	public T findOneByField(String beanFieldName, Object value) {
		return CollectionUtils.getOnlyElement(findByField(beanFieldName, value));
	}


	@Override
	public T findOneByFields(String[] beanFieldNames, Object[] values) {
		return CollectionUtils.getOnlyElement(findByFields(beanFieldNames, values));
	}


	@Override
	public List<T> findAll() {
		//TODO
		return getMongoTemplateHandler().getMongoTemplate().findAll(getConfiguration().getBeanClass());
	}


	@Override
	public List<T> findByPrimaryKeys(Serializable[] ids) {
		List<ObjectId> objectIdList = new ArrayList<>();
		for (Serializable id : ids) {
			objectIdList.add(new ObjectId(id.toString()));
		}
		return getMongoTemplateHandler().executeFindQuery(new Query().addCriteria(Criteria.where(PRIMARY_KEY_COLUMN_NAME).in(objectIdList)), getConfiguration().getBeanClass());
	}


	@Override
	public List<T> findByField(String beanFieldName, Object value) {
		return getMongoTemplateHandler().executeFindQuery(new Query().addCriteria(Criteria.where(beanFieldName).is(value)), getConfiguration().getBeanClass());
	}


	@Override
	public List<T> findByField(String beanFieldName, Object[] values) {
		return getMongoTemplateHandler().executeFindQuery(new Query().addCriteria(Criteria.where(beanFieldName).in(Arrays.asList(values))), getConfiguration().getBeanClass());
	}


	@Override
	public List<T> findByFields(String[] beanFieldNames, Object[] values) {
		Query query = new Query();
		for (int i = 0; i < beanFieldNames.length; i++) {
			query.addCriteria(Criteria.where(beanFieldNames[i]).is(values[i]));
		}
		return getMongoTemplateHandler().executeFindQuery(query, getConfiguration().getBeanClass());
	}


	@Override
	public List<T> findByQuery(String query) {
		return getMongoTemplateHandler().executeFindQuery(new BasicQuery(query), getConfiguration().getBeanClass());
	}


	@Override
	public List<T> findBySearchCriteria(MongoTemplateSearchConfigurer<Query> searchConfigurer) {
		Query query = new Query();
		searchConfigurer.configureCriteria(query);
		searchConfigurer.configureOrderBy(query);
		query.with(PageRequest.of(searchConfigurer.getStart() / searchConfigurer.getLimit(), searchConfigurer.getLimit())).collation(Collation.of("en").strength(Collation.ComparisonLevel.primary()));
		if (getMongoTemplateHandler(searchConfigurer.isArchiveQuery()) != null) {
			AtomicLong rowCount = searchConfigurer.isRetrieveRowCount() ? new AtomicLong() : new AtomicLong(-1);
			List<T> resultList = getMongoTemplateHandler(searchConfigurer.isArchiveQuery()).executeFindQuery(query, getConfiguration().getBeanClass(), searchConfigurer.getCollectionNameList(), rowCount);
			return new PagingArrayList<>(resultList, searchConfigurer.getStart(), rowCount.intValue(), searchConfigurer.getLimit());
		}
		else {
			return new PagingArrayList<>(new ArrayList<>(), 0, 0, searchConfigurer.getLimit());
		}
	}


	@Override
	@Transactional
	public void saveList(List<T> beanList) {
		IdentityObject currentUser = (IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
		AssertUtils.assertNotNull(currentUser, "Current user is not set");
		AssertUtils.assertNotNull(currentUser.getIdentity(), "Current user identity is not set");
		short currentUserId = ((Short) currentUser.getIdentity());
		for (T bean : CollectionUtils.getIterable(beanList)) {
			MongoTemplateUtils.populateRecordStamp(bean, currentUserId);
		}
		MongoTemplate mongoTemplate = getMongoTemplateHandler().getMongoTemplate();
		List<T> newBeanList = beanList.stream().filter(IdentityObject::isNewBean).collect(Collectors.toList());
		if (CollectionUtils.getSize(newBeanList) > 0) {
			mongoTemplate.insertAll(newBeanList);
		}
		if (newBeanList.size() != beanList.size()) {
			for (T bean : CollectionUtils.getIterable(beanList.stream().filter(bean -> !bean.isNewBean()).collect(Collectors.toList()))) {
				save(bean);
			}
		}
	}


	@Override
	@Transactional
	public void saveList(List<T> newList, List<T> oldList) {
		IdentityObject currentUser = (IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
		AssertUtils.assertNotNull(currentUser, "Current user is not set");
		AssertUtils.assertNotNull(currentUser.getIdentity(), "Current user identity is not set");
		short currentUserId = ((Short) currentUser.getIdentity());
		for (int i = 0; i < CollectionUtils.getSize(newList); i++) {
			if (isUpsertNeeded(newList.get(i), oldList)) {
				newList.set(i, save(newList.get(i), currentUserId));
			}
		}
		for (T old : CollectionUtils.getIterable(oldList)) {
			if (newList == null || !newList.contains(old)) {
				delete(old);
			}
		}
	}


	/**
	 * Checks if the newBean is in the oldList and if it is equal to the oldBean in the oldList
	 * If not in the old list or in the list and not equal to the oldBean it will save the newBean.
	 **/
	private boolean isUpsertNeeded(T newBean, List<T> oldList) {
		boolean upsert = true;
		if (!CollectionUtils.isEmpty(oldList)) {
			int oldBeanIndex = oldList.indexOf(newBean);
			if (oldBeanIndex != -1) {
				if (CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualProperties(newBean, oldList.get(oldBeanIndex), false))) {
					upsert = false;
				}
			}
		}
		return upsert;
	}


	@Override
	public T save(T bean) {
		IdentityObject currentUser = (IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
		AssertUtils.assertNotNull(currentUser, "Current user is not set");
		AssertUtils.assertNotNull(currentUser.getIdentity(), "Current user identity is not set");
		short currentUserId = ((Short) currentUser.getIdentity());
		return save(bean, currentUserId);
	}


	private T save(T bean, short currentUserId) {
		T result;
		MongoTemplateUtils.populateRecordStamp(bean, currentUserId);
		boolean insert = bean.isNewBean();
		boolean observer = insert ? super.isInsertObserverRegistered() : super.isUpdateObserverRegistered();
		DaoEventTypes event = insert ? DaoEventTypes.INSERT : DaoEventTypes.UPDATE;

		if (observer) {
			super.getEventObserver(event).beforeTransactionMethodCall(this, event, bean);
		}
		try {
			if (insert) {
				result = insert(bean);
			}
			else {
				result = update(bean);
			}
		}
		catch (RuntimeException e) {
			if (observer) {
				super.getEventObserver(event).afterTransactionMethodCall(this, event, bean, e);
			}
			throw new DaoException("Error in " + (insert ? "insert" : "update") + "(" + bean + ")", bean, e);
		}

		if (observer) {
			// must pass persistent instance which may get modified on flush
			super.getEventObserver(event).afterTransactionMethodCall(this, event, result, null);
		}
		return result;
	}


	@Transactional
	protected T insert(T bean) {
		T result;
		boolean observer = super.isInsertObserverRegistered();
		if (observer) {
			super.getEventObserver(DaoEventTypes.INSERT).beforeMethodCall(this, DaoEventTypes.INSERT, bean);
		}

		try {
			result = getMongoTemplateHandler().getMongoTemplate().insert(bean);
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(DaoEventTypes.INSERT).afterMethodCall(this, DaoEventTypes.INSERT, bean, e);
			}
			if (e instanceof ValidationException) {
				throw e; // keep validation exceptions unchanged to prevent them from logging
			}
			throw new DaoException("Error in " + getConfiguration().getTableName() + " insert(" + bean + ")", bean, e);
		}

		if (observer) {
			super.getEventObserver(DaoEventTypes.INSERT).afterMethodCall(this, DaoEventTypes.INSERT, bean, null);
		}
		return result;
	}


	/**
	 * Returns persistent instance tied to current session.  It's important to use this object when the caller
	 * started a transaction because Hibernate session may not be flushed until that transaction is committed.
	 * Returned object fields but not passed detached object fields may still be updated: "version".
	 */
	@Transactional
	protected T update(final T bean) {
		boolean observer = super.isUpdateObserverRegistered();
		T result = bean;
		if (observer) {
			super.getEventObserver(DaoEventTypes.UPDATE).beforeMethodCall(this, DaoEventTypes.UPDATE, bean);
		}

		try {
			result = getMongoTemplateHandler().getMongoTemplate().save(bean);
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(DaoEventTypes.UPDATE).configurePersistedInstance(result, bean);
				super.getEventObserver(DaoEventTypes.UPDATE).afterMethodCall(this, DaoEventTypes.UPDATE, bean, e);
			}
			if (e instanceof ValidationException) {
				throw e; // keep validation exceptions unchanged to prevent them from logging
			}
			throw new DaoException("Error in " + getConfiguration().getTableName() + " update(" + bean + ")", bean, e);
		}

		if (observer) {
			// must pass persistent instance which may get modified on flush
			super.getEventObserver(DaoEventTypes.UPDATE).configurePersistedInstance(result, bean);
			super.getEventObserver(DaoEventTypes.UPDATE).afterMethodCall(this, DaoEventTypes.UPDATE, result, null);
		}
		return result;
	}


	/**
	 * Not implementing
	 */
	@Override
	public ObjectWrapperWithBooleanResult<T> saveSmart(T bean) {
		return ObjectWrapperWithBooleanResult.success(save(bean));
	}


	@Override
	public void delete(Serializable primaryKey) {
		getMongoTemplateHandler().getMongoTemplate().remove(new Query().addCriteria(Criteria.where("_id").is(new ObjectId(primaryKey.toString()))), getConfiguration().getBeanClass());
	}


	@Override
	public void delete(T bean) {
		getMongoTemplateHandler().getMongoTemplate().remove(bean);
	}


	@Override
	public void deleteList(List<T> deleteList) {
		if (CollectionUtils.isEmpty(deleteList)) {
			return;
		}
		for (T deleteBean : CollectionUtils.getIterable(deleteList)) {
			delete(deleteBean);
		}
	}


	@Override
	public void deleteByQuery(String query) {
		getMongoTemplateHandler().getMongoTemplate().remove(new BasicQuery(query), getConfiguration().getBeanClass());
	}


	private MongoTemplateHandler getMongoTemplateHandler(boolean archived) {
		if (archived) {
			return getDataLakeTemplateHandler();
		}
		return getMongoTemplateHandler();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public MongoTemplateHandler getMongoTemplateHandler() {
		return this.mongoTemplateHandler;
	}


	public void setMongoTemplateHandler(MongoTemplateHandler mongoTemplateHandler) {
		this.mongoTemplateHandler = mongoTemplateHandler;
	}


	public MongoTemplateHandler getDataLakeTemplateHandler() {
		return this.dataLakeTemplateHandler;
	}


	public void setDataLakeTemplateHandler(MongoTemplateHandler dataLakeTemplateHandler) {
		this.dataLakeTemplateHandler = dataLakeTemplateHandler;
	}
}
