package com.clifton.core.context;


import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.search.form.command.BaseEntitySearchCommand;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Criteria;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


/**
 * The <code>ContextConventionUtilsTestServiceImpl</code> is a test service implementation
 * for ContextConventionUtilsTests
 * <p/>
 * Note: @RequestMapping and @SecureMethod annotation is used for validation in tests
 *
 * @author manderson
 */
public class ContextConventionUtilsTestServiceImpl {

	private AdvancedUpdatableDAO<NamedEntityTestObject, Criteria> namedEntityTestObjectDAO;


	@RequestMapping("namedEntityTestObject")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public NamedEntityTestObject getNamedEntityTestObject(int id) {
		return getNamedEntityTestObjectDAO().findByPrimaryKey(id);
	}


	@SuppressWarnings("unused")
	@RequestMapping("namedEntityTestObjectByName")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public NamedEntityTestObject getNamedEntityTestObjectByName(String name) {
		// DO NOTHING
		return null;
	}


	@RequestMapping("namedEntityTestObjectLock")
	public void lockNamedEntityTestObject(String tableName, Integer fkFieldId) {
		// DO NOTHING
	}


	@SuppressWarnings("unused")
	@RequestMapping("namedEntityTestObjectForNameAndDescription")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public NamedEntityTestObject getNamedEntityTestObjectForNameAndDescription(String name, String description) {
		return null;
	}


	@RequestMapping("namedEntityTestObjectRecent")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public NamedEntityTestObject getNamedEntityTestObjectRecent() {
		return null;
	}


	@SuppressWarnings("unused")
	@RequestMapping("testName")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public String getTestName(NamedEntityTestObject bean) {
		return null;
	}


	@SuppressWarnings("unused")
	@RequestMapping("namedEntityTestObjectListBySomethingElse")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public List<NamedEntityTestObject> getNamedEntityTestObjectListBySomethingElse(String str) {
		return null;
	}


	@SuppressWarnings("unused")
	@RequestMapping("namedEntityTestObjectListForDataTableFind")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public DataTable getNamedEntityTestObjectListForDataTable(BaseEntitySearchForm searchForm) {
		return null;
	}


	@RequestMapping("namedEntityTestObjectList")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public List<NamedEntityTestObject> getNamedEntityTestObjectList() {
		return getNamedEntityTestObjectDAO().findAll();
	}


	@RequestMapping("namedEntityTestObjectListFind")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public List<NamedEntityTestObject> getNamedEntityTestObjectList(BaseEntitySearchForm searchForm) {
		return getNamedEntityTestObjectDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@RequestMapping("namedEntityTestObjectByCommand")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public NamedEntityTestObject getNamedEntityTestObjectByCommand(BaseEntitySearchForm searchForm) {
		return CollectionUtils.getFirstElement(getNamedEntityTestObjectDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm)));
	}


	@RequestMapping("namedEntityTestObjectListByCommandFind")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_READ)
	public List<NamedEntityTestObject> getNamedEntityTestObjectListByCommand(BaseEntitySearchCommand searchCommand) {
		return getNamedEntityTestObjectDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchCommand));
	}


	@RequestMapping("namedEntityTestObjectSave")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_CREATE) // Would be WRITE if parameter had ID value, but default is CREATE
	public void saveNamedEntityTestObject(NamedEntityTestObject bean) {
		getNamedEntityTestObjectDAO().save(bean);
	}


	@RequestMapping("namedEntityTestObjectDelete")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_DELETE)
	public void deleteNamedEntityTestObject(int id) {
		getNamedEntityTestObjectDAO().delete(id);
	}


	@RequestMapping("namedEntityTestObjectGenerate")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_WRITE)
	public void generateNamedEntityTestObject() {
		// DO NOTHING
	}


	@RequestMapping("namedEntityTestObjectProcess")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_WRITE)
	public void processNamedEntityTestObject() {
		// DO NOTHING
	}


	@RequestMapping("namedEntityTestObjectListGenerate")
	@SecureMethod(table = "NamedEntityTestObject", permissions = SecurityPermission.PERMISSION_WRITE)
	public void generateNamedEntityTestObjectList(BaseEntitySearchForm searchForm) {
		// DO NOTHING
	}


	@DoNotAddRequestMapping
	@RequestMapping("namedEntityTestObjectDoNotAddRequestMappingNoApiDelete")
	public void deleteNamedEntityTestObjectDoNotAddRequestMappingNoApi() {
		// DO NOTHING
	}


	@DoNotAddRequestMapping(allowAPI = true)
	@RequestMapping("namedEntityTestObjectDoNotAddRequestMappingDelete")
	public void deleteNamedEntityTestObjectDoNotAddRequestMapping() {
		// DO NOTHING
	}

	//////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<NamedEntityTestObject, Criteria> getNamedEntityTestObjectDAO() {
		return this.namedEntityTestObjectDAO;
	}


	public void setNamedEntityTestObjectDAO(AdvancedUpdatableDAO<NamedEntityTestObject, Criteria> namedEntityTestObjectDAO) {
		this.namedEntityTestObjectDAO = namedEntityTestObjectDAO;
	}
}
