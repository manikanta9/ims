package com.clifton.core.context.spring;


public interface NoMethodsOrFieldsToAnnotateTestObjectService {

	public void sampleMethodNoMethodsOrFieldsToAnnotateTest(Integer[] tradeOrderIdList);
}
