package com.clifton.core.context;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.context.spring.SpringApplicationContextServiceImpl;
import com.clifton.core.context.spring.TestDomainObject;
import com.clifton.core.context.spring.TestDomainObject2;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ContextConventionUtilsTests {

	@Resource
	private DaoLocator daoLocator;


	///////////////////////////////////////////////////////////////


	public void testGetBeanNameFromClassName() {
		validateBeanNameForClass("testService", "com.clifton.test.TestServiceImpl");
		validateBeanNameForClass("testService", "com.clifton.test.TestService");
		validateBeanNameForClass("testCalculator", "com.clifton.test.TestCalculator");
		validateBeanNameForClass("testCalculator", "com.clifton.test.TestCalculatorImpl");
		validateBeanNameForClass("daoLocator", "com.clifton.core.dataaccess.dao.locator.DaoLocatorInSpringContext");
	}


	private void validateBeanNameForClass(String beanName, String className) {
		String result = ContextConventionUtils.getBeanNameFromClassName(className);
		Assertions.assertEquals(beanName, result);
	}

	///////////////////////////////////////////////////////////////


	@Test
	public void testGetTableNameFromClass() {
		Assertions.assertEquals("TestDomainObject", ContextConventionUtils.getTableNameFromClass(TestDomainObject.class));
		Assertions.assertEquals("TestDomainObjectInDifferentTable", ContextConventionUtils.getTableNameFromClass(TestDomainObject2.class));
	}

	///////////////////////////////////////////////////////////////


	@Test
	public void testGetTableNameFromMethodName() {
		// GET
		validateTableNameFromMethodName("BusinessCompanyContact", "getBusinessCompanyContactListByCompany");
		validateTableNameFromMethodName("InvestmentSecurity", "getInvestmentSecurityBySymbol");
		validateTableNameFromMethodName("SystemColumn", "getSystemColumnList");

		// DELETE
		validateTableNameFromMethodName("BatchJob", "deleteBatchJob");
		validateTableNameFromMethodName("BillingBasisSnapshot", "deleteBillingBasisSnapshotListForInvoice");
		validateTableNameFromMethodName("BillingPaymentAllocation", "deleteBillingPaymentAllocationListByInvoice");
		validateTableNameFromMethodName("SystemListItem", "deleteSystemListItem");

		// SAVE
		validateTableNameFromMethodName("BillingPaymentAllocation", "saveBillingPaymentAllocation");

		// COPY
		validateTableNameFromMethodName("BusinessContractType", "copyBusinessContractType");

		// LINK
		validateTableNameFromMethodName("ProductHedgingAccountStrategyPosition", "linkProductHedgingAccountStrategyPosition");

		// PROCESS
		validateTableNameFromMethodName("BillingInvoiceGroup", "processBillingInvoiceGroup");
		validateTableNameFromMethodName("PortfolioRun", "processPortfolioRunList");
	}


	private void validateTableNameFromMethodName(String tableName, String methodName) {
		String result = ContextConventionUtils.getTableNameFromMethodName(methodName);
		Assertions.assertEquals(tableName, result);
	}


	///////////////////////////////////////////////////////////////


	@Test
	public void testGetTableNameFromMethod() {
		Class<?> clz = CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl");
		// Service Method Will Use Different Conventions, but all various conventions should resolve to the same table name
		for (Method method : clz.getMethods()) {
			SecureMethod sm = AnnotationUtils.getAnnotation(method, SecureMethod.class);
			if (sm != null) {
				ObjectWrapper<Class<IdentityObject>> dtoClassHolder = new ObjectWrapper<>(null);
				String tableName = ContextConventionUtils.getTableNameFromMethod(method, method.getName(), dtoClassHolder, this.daoLocator);
				Assertions.assertEquals(sm.table(), tableName, "Method [" + method.getName() + "]: ");
			}
		}
	}


	///////////////////////////////////////////////////////////////


	@Test
	public void testUrlFromMethod() {
		Class<?> clz = CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl");
		// Service Method Will Use Different Conventions, but all various conventions should resolve to the same table name
		for (Method method : clz.getMethods()) {
			RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
			if (rm != null) {
				String result = ContextConventionUtils.getUrlFromMethod(method);
				Assertions.assertEquals(result, "/" + rm.value()[0] + ContextConventionUtils.URL_DEFAULT_EXTENSION, "Method [" + method.getName() + "]: ");
			}
		}
	}


	@Test
	public void testDoNotAllowApiUrlFromMethod() throws NoSuchMethodException {
		Class<?> clz = CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl");
		Method method = clz.getMethod("deleteNamedEntityTestObjectDoNotAddRequestMappingNoApi");
		Assertions.assertFalse(ContextConventionUtils.isUrlFromMethodAllowed(method, clz));
	}


	@Test
	public void testAllowApiUrlFromMethod() throws NoSuchMethodException {
		Class<?> clz = CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl");
		Method method = clz.getMethod("deleteNamedEntityTestObjectDoNotAddRequestMapping");
		Assertions.assertTrue(ContextConventionUtils.isUrlFromMethodAllowed(method, clz));
		String result = ContextConventionUtils.getUrlFromMethod(method);
		RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
		Assertions.assertEquals("/" + rm.value()[0] + ContextConventionUtils.URL_DEFAULT_EXTENSION, result, "Method [" + method.getName() + "]: ");
	}


	@Test
	public void testUrlFromMethodClassAndName() {
		Class<?> clz = CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl");
		// Service Method Will Use Different Conventions, but all various conventions should resolve to the same table name
		for (Method method : clz.getMethods()) {
			RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
			if (rm != null && !StringUtils.isEqual("getNamedEntityTestObjectList", method.getName())) {
				String result = ContextConventionUtils.getUrlFromMethodClassAndName(clz, method.getName(), false);
				Assertions.assertEquals(rm.value()[0] + ContextConventionUtils.URL_DEFAULT_EXTENSION, result, "Method [" + method.getName() + "]: ");

				result = ContextConventionUtils.getUrlFromMethodClassAndName(clz, method.getName(), true);
				Assertions.assertEquals("/" + rm.value()[0] + ContextConventionUtils.URL_DEFAULT_EXTENSION, result, "Method [" + method.getName() + "]: ");
			}
		}


		Assertions.assertNull(ContextConventionUtils.getUrlFromMethodClassAndName(null, null, false));
		Assertions.assertNull(ContextConventionUtils.getUrlFromMethodClassAndName(null, "test", true));
		Assertions.assertNull(ContextConventionUtils.getUrlFromMethodClassAndName(SpringApplicationContextServiceImpl.class, null, false));
		Assertions.assertNull(ContextConventionUtils.getUrlFromMethodClassAndName(null, "", true));

		Assertions.assertEquals("contextBeanDefinitionList.json", ContextConventionUtils.getUrlFromMethodClassAndName(SpringApplicationContextServiceImpl.class, "getContextBeanDefinitionList", false));


		boolean error = false;
		try {
			ContextConventionUtils.getUrlFromMethodClassAndName(SpringApplicationContextServiceImpl.class, "method doesn't exist", false);
		}
		catch (Exception e) {
			error = true;
			Assertions.assertEquals("Bean of Class [class com.clifton.core.context.spring.SpringApplicationContextServiceImpl] is missing method [method doesn't exist] or method is not accessible.", e.getMessage());
		}
		Assertions.assertTrue(error, "Expected error to be thrown for method that doesn't exist.");

		error = false;
		try {
			ContextConventionUtils.getUrlFromMethodClassAndName(SpringApplicationContextServiceImpl.class, "getContextBean", false);
		}
		catch (Exception e) {
			error = true;
			Assertions.assertTrue(e.getMessage().startsWith("class com.clifton.core.context.spring.SpringApplicationContextServiceImpl has more than 1 method named 'getContextBean'"));
		}
		Assertions.assertTrue(error, "Expected error to be thrown for class with multiple methods with the same name");

		error = false;
		try {
			ContextConventionUtils.getUrlFromMethodClassAndName(SpringApplicationContextServiceImpl.class, "autowireBean", false);
		}
		catch (Exception e) {
			error = true;
			Assertions.assertEquals("Unable to determine URL from Method [class com.clifton.core.context.spring.SpringApplicationContextServiceImpl.autowireBean].  Method is either not accessible from the URL or is missing explicit RequestMapping annotation.", e.getMessage());
		}
		Assertions.assertTrue(error, "Expected error to be thrown for class with method not accessible via URL");
	}


	///////////////////////////////////////////////////////////////


	@Test
	public void testMainGetKeyFromModel() {
		Map<String, Object> model = new HashMap<>();

		// first key is annotated with NonPersistentObject
		model.put("key1", new AuditableEntitySearchForm());
		Assertions.assertNotNull(ContextConventionUtils.getMainKeyFromModel(model));

		model.put("key2", "value2");
		Assertions.assertNotNull(ContextConventionUtils.getMainKeyFromModel(model));

		model.put("key3", this);
		Assertions.assertNull(ContextConventionUtils.getMainKeyFromModel(model));

		// second key is annotated with NonPersistentObject
		model = new HashMap<>();
		model.put("key1", "value1");
		Assertions.assertNotNull(ContextConventionUtils.getMainKeyFromModel(model));

		model.put("key2", new AuditableEntitySearchForm());
		Assertions.assertNotNull(ContextConventionUtils.getMainKeyFromModel(model));

		model.put("key3", this);
		Assertions.assertNull(ContextConventionUtils.getMainKeyFromModel(model));
	}


	///////////////////////////////////////////////////////////////


	@Test
	public void testRequiredPermissionFromMethod() {
		Class<?> clz = CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl");
		// Service Method Will Use Different Conventions, but all various conventions should resolve to the same table name
		for (Method method : clz.getMethods()) {
			SecureMethod sm = AnnotationUtils.getAnnotation(method, SecureMethod.class);
			if (sm != null) {
				Integer permission = ContextConventionUtils.getRequiredPermissionFromMethodName(method.getName(), null);
				Assertions.assertEquals(new Integer(sm.permissions()), permission, "Method [" + method.getName() + "]:");
			}
		}
	}


	@Test
	public void testCreateOrWritePermissionForSaveMethod() {
		Method method = MethodUtils.getMethod(CoreClassUtils.getClass("com.clifton.core.context.ContextConventionUtilsTestServiceImpl"), "saveNamedEntityTestObject", false);
		NamedEntityTestObject testObject = new NamedEntityTestObject();
		testObject.setName("Test");

		Integer permission = ContextConventionUtils.getRequiredPermissionFromMethodName(method.getName(), BeanUtils.describe(testObject));
		Assertions.assertEquals(SecurityPermission.PERMISSION_CREATE, permission, "Expected CREATE permission when saving a new bean");

		testObject.setId(1); // No Longer a "new" bean
		permission = ContextConventionUtils.getRequiredPermissionFromMethodName(method.getName(), BeanUtils.describe(testObject));
		Assertions.assertEquals(SecurityPermission.PERMISSION_WRITE, permission, "Expected WRITE permission when saving an existing bean");
	}
}
