package com.clifton.core.context;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ThreadLocalContextHandlerTests {

	@Test
	public void test() {
		ContextHandler contextHandler = new ThreadLocalContextHandler();

		Assertions.assertArrayEquals(null, contextHandler.getBeanNames());
		Assertions.assertNull(contextHandler.getBean("test"));

		Object value = contextHandler.removeBean("test");
		Assertions.assertNull(value);

		contextHandler.clear();
		Assertions.assertArrayEquals(null, contextHandler.getBeanNames());

		contextHandler.setBean("test", "value");
		Assertions.assertEquals("value", contextHandler.getBean("test"));

		contextHandler.setBean("test2", 222);
		Assertions.assertEquals(2, contextHandler.getBeanNames().length);

		value = contextHandler.removeBean("test");
		Assertions.assertEquals(1, contextHandler.getBeanNames().length);
		Assertions.assertNull(contextHandler.getBean("test"));
		Assertions.assertEquals("value", value);

		contextHandler.clear();
		Assertions.assertArrayEquals(null, contextHandler.getBeanNames());
	}
}
