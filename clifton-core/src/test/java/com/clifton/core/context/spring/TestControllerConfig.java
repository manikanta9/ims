package com.clifton.core.context.spring;


public class TestControllerConfig {

	private int value;


	public int getValue() {
		return this.value;
	}


	public void setValue(int value) {
		this.value = value;
	}
}
