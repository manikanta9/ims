package com.clifton.core.context.spring;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CoreContextSanityTests {

	@Autowired
	private TestService testService;

	@Autowired
	private TestService testService2;

	private TestService testService3;


	@Test
	public void testIfTestWasAutoWired() {
		Assertions.assertNotNull(this.testService);
		Assertions.assertNotNull(this.testService2);
	}


	@Test
	public void testIfTestWasNotAutoWired() {
		Assertions.assertNull(this.testService3);
	}
}
