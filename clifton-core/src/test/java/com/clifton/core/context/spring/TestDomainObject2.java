package com.clifton.core.context.spring;

import com.clifton.core.beans.NamedEntity;

import javax.persistence.Table;


/**
 * @author vgomelsky
 */
@Table(name="TestDomainObjectInDifferentTable")
public class TestDomainObject2 extends NamedEntity<Integer> {
}
