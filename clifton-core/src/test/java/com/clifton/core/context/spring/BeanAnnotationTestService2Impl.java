package com.clifton.core.context.spring;


import org.springframework.stereotype.Service;


@Service("beanAnnotationTest2Service")
public class BeanAnnotationTestService2Impl {

	private String test;
	private String systemAuditColumnDAO;


	public String getTest() {
		return this.test;
	}


	public void setTest(String test) {
		this.test = test;
	}


	public void setTest() {
		this.test = "testValue";
	}


	/**
	 * @return the systemAuditColumnDAO
	 */
	public String getSystemAuditColumnDAO() {
		return this.systemAuditColumnDAO;
	}


	/**
	 * @param systemAuditColumnDAO the systemAuditColumnDAO to set
	 */
	public void setSystemAuditColumnDAO(String systemAuditColumnDAO) {
		this.systemAuditColumnDAO = systemAuditColumnDAO;
	}
}
