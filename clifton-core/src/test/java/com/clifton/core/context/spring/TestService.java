package com.clifton.core.context.spring;


import org.springframework.stereotype.Service;

import java.util.Map;


@Service
public abstract class TestService {

	private String test;
	private String propertyToBeInjected;
	private String propertyWithSetter;
	private Map<Integer, String> genericPropertyToBeInjected;


	public String getTest() {
		return this.test;
	}


	public void setTest(String test) {
		this.test = test;
	}


	public String getPropertyToBeInjected() {
		return this.propertyToBeInjected;
	}


	public String getPropertyWithSetter() {
		return this.propertyWithSetter;
	}


	public void setPropertyWithSetter(String propertyWithSetter) {
		this.propertyWithSetter = propertyWithSetter;
	}


	public Map<Integer, String> getGenericPropertyToBeInjected() {
		return this.genericPropertyToBeInjected;
	}


	public void setGenericPropertyToBeInjected(Map<Integer, String> genericPropertyToBeInjected) {
		this.genericPropertyToBeInjected = genericPropertyToBeInjected;
	}
}
