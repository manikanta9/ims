package com.clifton.core.context.spring;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;


@Service
public class NoMethodsOrFieldsToAnnotateTestObjectServiceImpl implements NoMethodsOrFieldsToAnnotateTestObjectService {

	@Override
	@Transactional
	@RequestMapping("sampleMethodNoMethodsOrFieldsToAnnotateTest")
	@SecureMethod(dtoClass = IdentityObject.class, permissions = SecurityPermission.PERMISSION_WRITE)
	public void sampleMethodNoMethodsOrFieldsToAnnotateTest(@SuppressWarnings("unused") Integer[] tradeOrderIdList) {
		//
	}
}
