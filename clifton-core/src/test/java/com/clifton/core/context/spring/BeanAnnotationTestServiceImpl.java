package com.clifton.core.context.spring;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;


@Service
public class BeanAnnotationTestServiceImpl {

	private String test;
	private String systemAuditColumnDAO;


	public void downloadTest() {
		// nothing 
	}


	public void uploadTest() {
		// nothing
	}


	public int getTestLength() {
		return (this.test == null ? 0 : this.test.length());
	}


	@RequestMapping("getTestOther")
	public String getTest() {
		return this.test;
	}


	public String getAccountingM2MDaily(@SuppressWarnings("unused") int id) {
		return this.test;
	}


	private String getAccountingM2MDaily(@SuppressWarnings("unused") int id, @SuppressWarnings("unused") boolean populated) {
		return this.test;
	}


	private String getDuplicateTest(@SuppressWarnings("unused") int i) {
		return this.test;
	}


	@DoNotAddRequestMapping
	public String getSkippedTest() {
		return this.test;
	}


	public void copyTest(String copiedTest) {
		this.test = copiedTest;
	}


	public void setTest(String test) {
		this.test = test;
	}


	public void setTest() {
		this.test = "testValue";
	}


	public void rebuildTest() {
		// Nothing here
	}


	public void processTest() {
		// Nothing here
	}


	public List<String> getTestListBySearchConfigurer(@SuppressWarnings("unused") SearchConfigurer<Criteria> searchCongfigurer) {
		return null;
	}


	@SuppressWarnings("unused")
	public List<String> getTestListByNameAndSearchConfigurer(String name, SearchConfigurer<Criteria> searchCongfigurer) {
		return null;
	}


	public void deleteMappingTest(int id) {
	}


	@DoNotAddRequestMapping
	public void deleteMappingTest(String testArg, int id) {
	}


	public <E> E save(E entity) {
		return entity;
	}


	public <E> E saveWithName(E entity, String name) {
		return entity;
	}


	/**
	 * @return the systemAuditColumnDAO
	 */
	public String getSystemAuditColumnDAO() {
		return this.systemAuditColumnDAO;
	}


	/**
	 * @param systemAuditColumnDAO the systemAuditColumnDAO to set
	 */
	public void setSystemAuditColumnDAO(String systemAuditColumnDAO) {
		this.systemAuditColumnDAO = systemAuditColumnDAO;
	}
}
