package com.clifton.core.context.spring;


import org.springframework.stereotype.Service;

import java.util.Map;


/**
 * The <code>Test2Service</code> class is a result of what is expected from code generation.
 *
 * @author vgomelsky
 */
@Service
public class Test2Service extends TestService {

	private String propertyToBeInjected;
	private String propertyWithSetter;
	private Map<Integer, String> genericPropertyToBeInjected;


	@Override
	public String getPropertyToBeInjected() {
		return this.propertyToBeInjected;
	}


	public void setPropertyToBeInjected(String v) {
		this.propertyToBeInjected = v;
	}


	@Override
	public String getPropertyWithSetter() {
		return this.propertyWithSetter;
	}


	@Override
	public void setPropertyWithSetter(String v) {
		this.propertyWithSetter = v;
	}


	@Override
	public Map<Integer, String> getGenericPropertyToBeInjected() {
		return this.genericPropertyToBeInjected;
	}
}
