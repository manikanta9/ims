package com.clifton.core;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.datasource.EnhancedDataSource;
import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CoreProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "core";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("org.aopalliance.intercept.MethodInterceptor");
		imports.add("org.aopalliance.intercept.MethodInvocation");
		imports.add("com.atlassian.crowd.service.AuthenticationManager");
		imports.add("com.auth0.jwt.");
		imports.add("com.jcraft.jsch.");
		imports.add("com.itextpdf.");
		imports.add("org.reflections.");
		imports.add("com.thoughtworks.");
		imports.add("freemarker.");
		imports.add("java.");
		imports.add("java.nio.file.");
		imports.add("javax.");
		imports.add("net.kemitix.wiser.assertions.WiserAssertions");
		imports.add("net.sf.ehcache.");
		imports.add("org.apache.commons.");
		imports.add("org.apache.http.");
		imports.add("org.apache.log4j.");
		imports.add("org.apache.logging.log4j");
		imports.add("org.apache.poi.");
		imports.add("org.apache.tomcat.");
		imports.add("org.aspectj.lang.");
		imports.add("org.aspectj.weaver.NameMangler");
		imports.add("org.hibernate.");
		imports.add("org.springframework.");
		imports.add("org.subethamail.wiser.Wiser");
		imports.add("com.hierynomus.");
		imports.add("org.passay");
		imports.add("com.amazonaws");
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("database");
		approvedList.add("health");
		approvedList.add("jackson");
		approvedList.add("jmx");
	}


	@Override
	public Set<String> getEnumAllowedNamesOverride() {
		// Data is technically plural - so allowing this enum class name
		Set<String> names = new HashSet<>();
		names.add("com.clifton.core.security.password.passay.PasswordGeneratorCharacterData");
		return names;
	}


	@Override
	protected boolean isBooleanGetterMethodsAreValidSkipped(String className) {
		// EnhancedDataSource class extends org.apache.tomcat.jdbc.pool.DataSource which violates the boolean getter / setter method conventions.
		return EnhancedDataSource.class.getName().equals(className);
	}


	@Test
	public void testProjectClassFilesScanning() {
		// it's critical that this functionality is working:
		// if scanning accidentally breaks, then many of our tests will succeed even when there is a problem
		int count = CollectionUtils.getSize(getProjectClassFiles(IdentityObject.class));
		Assertions.assertTrue(count > 20 && count < 100, "Scanning for IdentityObject in core project returned unexpected count = " + count);
	}
}
