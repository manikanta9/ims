package com.clifton.core.email;

import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.EmailHandlerImpl;
import com.clifton.core.util.validation.ValidationException;
import net.kemitix.wiser.assertions.WiserAssertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


/**
 * The <code>EmailHandlerTests</code> class executes tests based on the expected behavior of the {@link EmailHandlerImpl}.
 *
 * @author dillonm
 */
@ContextConfiguration
@TestPropertySource("classpath:/com/clifton/core/email/email-handler-tests.properties")
@ExtendWith(SpringExtension.class)
public class EmailHandlerTests {

	@Value("${mail.smtp.port}")
	private Integer smtpPort;

	private final Lazy<FakeSMTPServer> fakeSmtpServerLazy = new Lazy<>(() -> new FakeSMTPServer(getSmtpPort()));

	@Resource
	private List<Email> validEmails;

	@Resource
	private List<Email> invalidEmails;

	@Resource
	private List<Email> environmentDependentEmails;

	@Resource
	private EmailHandlerImpl emailHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		getFakeSmtpServer().start();
	}


	@AfterEach
	public void cleanup() {
		getFakeSmtpServer().stop();
	}

	////////////////////////////////////////////////////////////////////////////
	////////            TESTS                                           ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Test emails are received from always valid addresses
	 */
	@Test
	public void testValidEmails() {
		List<Email> emails = getValidEmails();

		getEmailHandler().setAllowedRecipientFilterEnabled(true);
		for (Email email : emails) {
			sendEmail(emailHandler, email);
		}
		getEmailHandler().setAllowedRecipientFilterEnabled(false);
		for (Email email : emails) {
			sendEmail(emailHandler, email);
		}
	}


	/**
	 * Test invalid emails throw validation exceptions
	 */
	@Test
	public void testInvalidEmails() {
		List<Email> emails = getInvalidEmails();

		getEmailHandler().setAllowedRecipientFilterEnabled(true);
		for (Email email : emails) {
			Assertions.assertThrows(ValidationException.class, () -> sendEmail(emailHandler, email));
		}
		getEmailHandler().setAllowedRecipientFilterEnabled(false);
		for (Email email : emails) {
			Assertions.assertThrows(ValidationException.class, () -> sendEmail(emailHandler, email));
		}
	}


	/**
	 * Test environment dependent emails are only sent in the right environment
	 */
	@Test
	public void testEnvironmentDependentEmails() {
		List<Email> emails = getEnvironmentDependentEmails();

		getEmailHandler().setAllowedRecipientFilterEnabled(true);
		for (Email email : emails) {
			Assertions.assertThrows(AssertionError.class, () -> sendEmail(emailHandler, email));
		}
		getEmailHandler().setAllowedRecipientFilterEnabled(false);
		for (Email email : emails) {
			sendEmail(emailHandler, email);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////            HELPERS                                         ////////
	////////////////////////////////////////////////////////////////////////////


	private void sendEmail(EmailHandler emailHandler, Email email) {
		emailHandler.send(email);
		for (String recipient : email.getToAddresses()) {
			try {
				WiserAssertions.assertReceivedMessage(getFakeSmtpServer())
						.from(email.getFromAddress())
						.to(recipient)
						.withSubjectContains(email.getSubject())
						.withContentContains(email.getContent().toString());
			}
			finally {
				cleanupEntities();
			}
		}
	}


	private void cleanupEntities() {
		getFakeSmtpServer().clearMessages();
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Getters                                 ////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getSmtpPort() {
		return this.smtpPort;
	}


	public FakeSMTPServer getFakeSmtpServer() {
		return this.fakeSmtpServerLazy.get();
	}


	public List<Email> getValidEmails() {
		return this.validEmails;
	}


	public List<Email> getInvalidEmails() {
		return this.invalidEmails;
	}


	public List<Email> getEnvironmentDependentEmails() {
		return this.environmentDependentEmails;
	}


	public EmailHandlerImpl getEmailHandler() {
		return this.emailHandler;
	}
}
