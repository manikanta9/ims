package com.clifton.core.util.event;


import com.clifton.core.util.CollectionUtils;

import java.util.List;


public class EventListenerForTesting extends BaseEventListener<Event<Object, Object>> {

	private List<String> eventNameList;

	private String messagePassedByEvent;


	public EventListenerForTesting() {
		// preserve default constructor
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public EventListenerForTesting(String eventName) {
		setEventNameList(CollectionUtils.createList(eventName));
	}


	public EventListenerForTesting(List<String> eventNameList) {
		setEventNameList(eventNameList);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void onEvent(Event<Object, Object> event) {
		// pass message from event to listener
		this.messagePassedByEvent = (String) event.getContextValue("passedMessage");

		//Set "sequence" in event context to record sequence of listeners called on this event
		String listenerSequence = (String) event.getContextValue("listenerSequence");
		if (listenerSequence == null) {
			event.addContextValue("listenerSequence", "" + getOrder());
		}
		else {
			event.addContextValue("listenerSequence", listenerSequence + " " + getOrder());
		}
	}


	public String getMessagePassedByEvent() {
		return this.messagePassedByEvent;
	}


	public void setEventName(String eventName) {
		setEventNameList(CollectionUtils.createList(eventName));
	}


	@Override
	public List<String> getEventNameList() {
		return this.eventNameList;
	}


	public void setEventNameList(List<String> eventNameList) {
		this.eventNameList = eventNameList;
	}
}
