package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>CoreStringUtilsTests</code> class tests the <code>CoreStringUtils</code>.
 *
 * @author vgomelsky
 */
public class CoreStringUtilsTests {


	@Test
	public void testIsEmailValid() {
		Assertions.assertTrue(CoreStringUtils.isEmailValid("testUser@example.com"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("g_s+gav@com.com"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("jim@jim.c.dc.ca"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("#FIOperations@swib.state.wi.us"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("3minvestments@mmm.com"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("a.m.parker@tcu.edu"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("adarr@iuoe302.com"));
		Assertions.assertTrue(CoreStringUtils.isEmailValid("brendan.o'connor@srpnet.com"));

		Assertions.assertFalse(CoreStringUtils.isEmailValid("gav@gav.c"));
		Assertions.assertFalse(CoreStringUtils.isEmailValid("jim@--c.ca"));
	}
}
