package com.clifton.core.util.observer;


public interface ObservedEvent extends Observer {

	public void afterUpdate(StringBuilder builder);
}
