package com.clifton.core.util.compare;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The test class for {@link NumberAwareStringComparator}.
 *
 * @author MikeH
 */
public class NumberAwareStringComparatorTests {

	private static final String OVERFLOW_LONG = Long.MAX_VALUE + "00000";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBlankString() {
		assertUniformCompareResult(0, null, null);
		assertUniformCompareResult(0, "", "");
		assertUniformCompareResult(-1, null, "");
		assertUniformCompareResult(-1, null, "-");
		assertUniformCompareResult(-1, null, "a");
		assertUniformCompareResult(-1, null, "1");
		assertUniformCompareResult(-1, "", "-");
		assertUniformCompareResult(1, "", null);
		assertUniformCompareResult(1, "-", null);
		assertUniformCompareResult(1, "a", null);
		assertUniformCompareResult(1, "1", null);
		assertUniformCompareResult(1, "-", "");
	}


	@Test
	public void testStringWithLetters() {
		assertUniformCompareResult(0, "abc", "abc");
		assertUniformCompareResult(-1, "ABC", "abc");
		assertUniformCompareResult(1, "abc", "ABC");
		assertUniformCompareResult(0, "ABC", "ABC");
		assertUniformCompareResult(-1, "abc", "abcd");
		assertUniformCompareResult(1, "abcd", "abc");
		assertUniformCompareResult(-1, "ABCD", "abc");
		assertUniformCompareResult(-1, "ab-c", "abc");
	}


	@Test
	public void testStringWithNumbers() {
		assertUniformCompareResult(0, "0", "0");
		assertUniformCompareResult(0, "0", "000");
		assertUniformCompareResult(-1, "0", "1");
		assertUniformCompareResult(-1, "0", "5");
		assertUniformCompareResult(-1, "0", "10");
		assertUniformCompareResult(-1, "1", "10");
		assertUniformCompareResult(-1, "2", "10");
		assertUniformCompareResult(1, "10", "2");
		assertUniformCompareResult(-1, "10", "20");
		assertUniformCompareResult(-1, "500", "1000");
		assertUniformCompareResult(1, "1000", "500");
		assertUniformCompareResult(-1, "500-10", "1000-10");
		assertUniformCompareResult(0, "1000-10", "1000-10");
		assertUniformCompareResult(-1, "1000-50", "1000-100");
		assertUniformCompareResult(-1, "1000-10", "1000-500");
		assertUniformCompareResult(-1, "01000-010", "1000-500");
		assertUniformCompareResult(-1, "1000-10", "01000-0500");
		assertUniformCompareResult(-1, "1000", "01000-0500");
		assertUniformCompareResult(1, "1000-10", "1000-");
	}


	@Test
	public void testStringWithLettersAndNumbers() {
		assertUniformCompareResult(-1, "", "abc123");
		assertUniformCompareResult(0, "abc123", "abc123");
		assertUniformCompareResult(1, "abc123", "ABC123");
		assertUniformCompareResult(0, "123abc", "123abc");
		assertUniformCompareResult(-1, "123ABC", "123abc");
		assertUniformCompareResult(0, "123-abc", "123-abc");
		assertUniformCompareResult(-1, "abc100", "abc1000");
		assertUniformCompareResult(-1, "abc200", "abc1000");
		assertUniformCompareResult(-1, "abc200", "abc500");
		assertUniformCompareResult(1, "abcd100", "abc1000");
		assertUniformCompareResult(-1, "100abc", "1000abc");
		assertUniformCompareResult(-1, "200abc", "1000abc");
		assertUniformCompareResult(-1, "200abc", "500abc");
		assertUniformCompareResult(-1, "abc-100", "abc-1000");
		assertUniformCompareResult(-1, "abc-100-100", "abc-1000");
		assertUniformCompareResult(-1, "abc-100-100", "abc-1000-0100");
		assertUniformCompareResult(0, "abc-100-100", "abc-0100-0100");
		assertUniformCompareResult(1, "abc-100-100-abc", "abc-0100-0100-ABC");
		assertUniformCompareResult(0, "abc 100 abc 200 abc 300", "abc 100 abc 200 abc 300");
		assertUniformCompareResult(-1, "abc 100 abc 200 abc 300", "abc 101 abc 200 abc 300");
		assertUniformCompareResult(1, "abc 100 abc 200 abc 300", "abc 99 abc 200 abc 300");
		assertUniformCompareResult(-1, "abc 100 abc 200 abc 300", "abc 100 abc 201 abc 300");
		assertUniformCompareResult(1, "abc 100 abc 200 abc 300", "abc 100 abc 199 abc 300");
		assertUniformCompareResult(-1, "abc 100 abc 200 abc 300", "abc 100 abc 200 abc 301");
		assertUniformCompareResult(1, "abc 100 abc 200 abc 300", "abc 100 abc 200 abc 299");
	}


	@Test
	public void testMixedString() {
		assertUniformCompareResult(1, "abc", "123");
		assertUniformCompareResult(-1, "123", "abc");
	}


	@Test
	public void testLongOverflowComparison() {
		NumberFormatException numberFormatException = Assertions.assertThrows(NumberFormatException.class, () -> {
			int compareResult = new NumberAwareStringComparator().compare("0", OVERFLOW_LONG);
			Assertions.fail(String.valueOf(compareResult));
		});
		Assertions.assertEquals(String.format("For input string: \"%s\"", OVERFLOW_LONG), numberFormatException.getMessage());
	}


	@Test
	public void testBigDecimalComparison() {
		Assertions.assertEquals(-1, new NumberAwareStringComparator(true).compare("0", OVERFLOW_LONG));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void assertUniformCompareResult(int expected, String str1, String str2) {
		Assertions.assertEquals(expected, new NumberAwareStringComparator().compare(str1, str2));
		Assertions.assertEquals(expected, new NumberAwareStringComparator(true).compare(str1, str2));
	}
}
