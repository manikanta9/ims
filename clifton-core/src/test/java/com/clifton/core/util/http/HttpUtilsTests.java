package com.clifton.core.util.http;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class HttpUtilsTests {

	@Test
	public void testGetBaseUrl() {
		String result = HttpUtils.getBaseUrl("http://localhost:8080/app-clifton-ims");
		Assertions.assertEquals("http://localhost:8080", HttpUtils.getBaseUrl("http://localhost:8080/app-clifton-ims"));
		Assertions.assertEquals("http://localhost:8080", HttpUtils.getBaseUrl("http://localhost:8080/app-clifton-ims/testMethod.json"));
		Assertions.assertEquals("http://localhost:8080", HttpUtils.getBaseUrl("http://localhost:8080/app-clifton-ims/api/testMethod.json"));
	}
}
