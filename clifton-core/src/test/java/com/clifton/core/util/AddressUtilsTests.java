package com.clifton.core.util;


import com.clifton.core.beans.common.AddressEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>AddressUtilsTests</code> tests the {@link AddressUtils} class
 *
 * @author manderson
 */
public class AddressUtilsTests {

	////////////////////////////////////////////////////
	///////          Comparison Methods          ///////
	////////////////////////////////////////////////////


	@Test
	public void testIsEmpty() {
		AddressEntity address = null;
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = new AddressEntity();
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, null, null, null, null, null);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity("", "", "", "", "", "", "");
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity("   ", "", null, " ", "  ", null, "	");
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity("123 Main Street", "", "", "", "", "", "");
		Assertions.assertFalse(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, "Suite 400", "", "", "", "");
		Assertions.assertFalse(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		Assertions.assertFalse(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, null, "", "", "", "USA");
		Assertions.assertFalse(AddressUtils.isEmpty(address));
	}


	@Test
	public void testIsEqual() {
		AddressEntity address1 = null;
		AddressEntity address2 = null;
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));

		address1 = new AddressEntity();
		address2 = new AddressEntity();
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));

		address1 = populateAddressEntity(null, null, null, null, null, null, null);
		address2 = populateAddressEntity("", "", "", "", "", "", "");
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));

		address1 = populateAddressEntity("  ", " ", "", "", "   ", "", "  ");
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));

		address1 = populateAddressEntity("123 Main Street", "2nd Floor", null, "AnyCity", "State", "12345", "USA");
		address2 = populateAddressEntity("123 Main Street", "2ND Floor", null, "AnyCity", "State", "12345", "USA");
		Assertions.assertFalse(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));

		address1 = populateAddressEntity("123 Main Street", "", "", "", "", "", "");
		address2 = populateAddressEntity("123 Main Street", null, null, "", "", null, null);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));

		address2 = populateAddressEntity("123 Main Street  ", "2nd Floor", null, "", "", null, null);
		Assertions.assertFalse(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertFalse(AddressUtils.isEqual(address1, address2, false));

		address1 = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		address2 = populateAddressEntity(null, null, null, "Edina", "", "", null);
		Assertions.assertFalse(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertFalse(AddressUtils.isEqual(address1, address2, false));

		address1 = populateAddressEntity(null, null, null, "", "", "", "USA");
		address2 = populateAddressEntity(null, null, null, "", "", "", "USA");
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, false));
	}


	////////////////////////////////////////////////////
	///////          Copy/Clear Methods           //////
	////////////////////////////////////////////////////


	@Test
	public void testCopyAddress() {
		AddressEntity address1 = null;
		AddressEntity address2 = null;
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = new AddressEntity();
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = null;
		address2 = new AddressEntity();
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address2 = populateAddressEntity("123 Main Street", "2ND Floor", null, "AnyCity", "State", "12345", "USA");
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity("123 Main Street", "2ND Floor", null, "AnyCity", "State", "12345", "USA");
		address2 = null;
		AddressUtils.copyAddress(address1, address2);
		// SHOULD NOT BE EQUAL IN THIS CASE BECAUSE COPY TO IS NULL
		Assertions.assertFalse(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity("  ", " ", "", "", "   ", "", "  ");
		address2 = new AddressEntity();
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity("123 Main Street", "2nd Floor", null, "AnyCity", "State", "12345", "USA");
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity("123 Main Street", "", "", "", "", "", "");
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity("123 Main Street  ", "2nd Floor", null, "", "", null, null);
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));

		address1 = populateAddressEntity(null, null, null, "", "", "", "USA");
		AddressUtils.copyAddress(address1, address2);
		Assertions.assertTrue(AddressUtils.isEqual(address1, address2, true));
	}


	@Test
	public void testClearAddress() {
		AddressEntity address = null;
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = new AddressEntity();
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, null, null, null, null, null);
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity("", "", "", "", "", "", "");
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity("   ", "", null, " ", "  ", null, "	");
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity("123 Main Street", "", "", "", "", "", "");
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, "Suite 400", "", "", "", "");
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));

		address = populateAddressEntity(null, null, null, "", "", "", "USA");
		AddressUtils.clearAddress(address);
		Assertions.assertTrue(AddressUtils.isEmpty(address));
	}


	////////////////////////////////////////////////////
	///////          Formatting Methods          ///////
	////////////////////////////////////////////////////


	@Test
	public void testGetAddressLabel() {
		AddressEntity address = null;
		Assertions.assertEquals("", AddressUtils.getAddressLabel(address));

		address = new AddressEntity();
		Assertions.assertEquals("", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity(null, null, null, null, null, null, null);
		Assertions.assertEquals("", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity("  ", " ", "", "", "   ", "", "  ");
		Assertions.assertEquals("", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity("123 Main Street", "2nd Floor", null, "AnyCity", "State", "12345", "USA");
		Assertions.assertEquals("123 Main Street 2nd Floor AnyCity, State 12345 USA", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity("123 Main Street", "", "", "", "", "", "");
		Assertions.assertEquals("123 Main Street", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity("123 Main Street", "", "2nd Floor", "", "", null, null);
		Assertions.assertEquals("123 Main Street 2nd Floor", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		Assertions.assertEquals("Edina, MN", AddressUtils.getAddressLabel(address));

		address = populateAddressEntity(null, null, null, "", "", "", "USA");
		Assertions.assertEquals("USA", AddressUtils.getAddressLabel(address));
	}


	@Test
	public void testGetCityState() {
		AddressEntity address = null;
		Assertions.assertEquals("", AddressUtils.getCityState(address));

		address = new AddressEntity();
		Assertions.assertEquals("", AddressUtils.getCityState(address));

		address = populateAddressEntity(null, null, null, null, null, null, null);
		Assertions.assertEquals("", AddressUtils.getCityState(address));

		address = populateAddressEntity("  ", " ", "", "", "   ", "", "  ");
		Assertions.assertEquals("", AddressUtils.getCityState(address));

		address = populateAddressEntity("123 Main Street", "2nd Floor", null, "AnyCity", "State", "12345", "USA");
		Assertions.assertEquals("AnyCity, State", AddressUtils.getCityState(address));

		address = populateAddressEntity("123 Main Street", "", "", "", "", "", "");
		Assertions.assertEquals("", AddressUtils.getCityState(address));

		address = populateAddressEntity("123 Main Street", "", "2nd Floor", "", "MN", "", null);
		Assertions.assertEquals("MN", AddressUtils.getCityState(address));

		address = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		Assertions.assertEquals("Edina, MN", AddressUtils.getCityState(address));

		address = populateAddressEntity(null, null, null, "Edina", "", "", "USA");
		Assertions.assertEquals("Edina", AddressUtils.getCityState(address));
	}


	@Test
	public void testGetCityStatePostalCode() {
		AddressEntity address = null;
		Assertions.assertEquals("", AddressUtils.getCityStatePostalCode(address));

		address = new AddressEntity();
		Assertions.assertEquals("", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity(null, null, null, null, null, null, null);
		Assertions.assertEquals("", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity("  ", " ", "", "", "   ", "", "  ");
		Assertions.assertEquals("", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity("123 Main Street", "2nd Floor", null, "AnyCity", "State", "12345", "USA");
		Assertions.assertEquals("AnyCity, State 12345", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity("123 Main Street", "", "", "", "", "12345", "");
		Assertions.assertEquals("12345", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity("123 Main Street", "", "2nd Floor", "", "", "MN", null);
		Assertions.assertEquals("MN", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity(null, null, null, "Edina", "MN", "", null);
		Assertions.assertEquals("Edina, MN", AddressUtils.getCityStatePostalCode(address));

		address = populateAddressEntity(null, null, null, "Edina", "", "12345", "USA");
		Assertions.assertEquals("Edina 12345", AddressUtils.getCityStatePostalCode(address));
	}


	/////////////////////////////////////////////////////////


	private AddressEntity populateAddressEntity(String line1, String line2, String line3, String city, String state, String postalCode, String country) {
		AddressEntity ae = new AddressEntity();
		ae.setAddressLine1(line1);
		ae.setAddressLine2(line2);
		ae.setAddressLine3(line3);
		ae.setCity(city);
		ae.setState(state);
		ae.setPostalCode(postalCode);
		ae.setCountry(country);
		return ae;
	}
}
