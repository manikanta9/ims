package com.clifton.core.util.encryption;

import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author theodorez
 */
public class EncryptionUtilsDESTests {

	private static final String TEST_PLAINTEXT_1 = "Some content here"; //17 bytes
	private static final String CIPHERTEXT_AS_HEX_1 = "c4aa90c88af8333cc3fad81fd939daf4f287353d2868c684";
	private static final String TEST_PLAINTEXT_2 = "this is some content the"; //is 24 bytes
	private static final String CIPHERTEXT_AS_HEX_2 = "8619f2ebaa41141bfa71bfcfb7e80e4d0f21d108f178c9d618a0788f4110a6f1";

	private static final String PASSWORD = "testtest";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDecryptDesOddBytes() {
		byte[] cipherText = EncryptionUtils.hexStringToByteArray(CIPHERTEXT_AS_HEX_1);
		byte[] decryptedBytes = EncryptionUtilsDES.decrypt(PASSWORD, cipherText);
		String decryptedPlainText = new String(decryptedBytes);
		Assertions.assertTrue(StringUtils.isEqual(decryptedPlainText, TEST_PLAINTEXT_1));
	}


	@Test
	public void testDecryptDesEvenBytes() {
		byte[] cipherText = EncryptionUtils.hexStringToByteArray(CIPHERTEXT_AS_HEX_2);
		byte[] decryptedBytes = EncryptionUtilsDES.decrypt(PASSWORD, cipherText);
		String decryptedPlainText = new String(decryptedBytes);
		Assertions.assertTrue(StringUtils.isEqual(decryptedPlainText, TEST_PLAINTEXT_2));
	}
}
