package com.clifton.core.util.json;

import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;


/**
 * @author stevenf
 */
public class JacksonJsonUtilsTests {

	private static AwesomeObject notReallyAwesomeObject = new AwesomeObject("I'm not.", "I don't have a dog.", new BigDecimal("42"), null);
	private static AwesomeObject awesomeObject = new AwesomeObject("I'm awesome", "My dog told me.", new BigDecimal("19136"), notReallyAwesomeObject);

	private static final Map<String, Object> bestMapEver;


	static {
		Map<String, Object> decentMap = new HashMap<>();
		decentMap.put("name", "Nunyabidness");
		decentMap.put("description", "My name is on a need to know basis...");
		decentMap.put("treasure", awesomeObject);
		bestMapEver = Collections.unmodifiableMap(decentMap);
	}


	@Test
	public void testGetJsonArgument() {
		JsonHandler<?> jsonHandler = new JacksonHandlerImpl();
		String json = jsonHandler.toJson(getBestMapEver());
		Assertions.assertEquals("Nunyabidness", JacksonJsonUtils.getJsonArgument("name", json));
	}


	@Test
	public void testGetAllJsonArgumentsAtDepth() {
		JsonHandler<?> jsonHandler = new JacksonHandlerImpl();
		String json = jsonHandler.toJson(getBestMapEver());
		Assertions.assertEquals(Arrays.asList("Nunyabidness", "My name is on a need to know basis..."), JacksonJsonUtils.getJsonArguments(Arrays.asList("name", "description"), json, 0, true, false));
	}


	@Test
	public void testGetAllJsonArgumentsNoDepth() {
		JsonHandler<?> jsonHandler = new JacksonHandlerImpl();
		String json = jsonHandler.toJson(getBestMapEver());
		Assertions.assertEquals(Arrays.asList("Nunyabidness", "My name is on a need to know basis...", "I'm awesome", "My dog told me.", "I'm not.", "I don't have a dog."), JacksonJsonUtils.getJsonArguments(Arrays.asList("name", "description"), json, -1, true, false));
	}


	static class AwesomeObject {

		private final String name;
		private final String description;
		private final BigDecimal bestNumberEver;
		private final AwesomeObject notReallyAwesome;


		public AwesomeObject(String name, String description, BigDecimal bestNumberEver, AwesomeObject notReallyAwesome) {
			this.name = name;
			this.description = description;
			this.bestNumberEver = bestNumberEver;
			this.notReallyAwesome = notReallyAwesome;
		}

		////////////////////////////////////////////////////////////////////////////////
		///////////////                Getter  Methods                 /////////////////
		////////////////////////////////////////////////////////////////////////////////


		public String getName() {
			return this.name;
		}


		public String getDescription() {
			return this.description;
		}


		public BigDecimal getBestNumberEver() {
			return this.bestNumberEver;
		}


		public AwesomeObject getNotReallyAwesome() {
			return this.notReallyAwesome;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////                Getter  Methods                 /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static Map<String, Object> getBestMapEver() {
		return bestMapEver;
	}
}
