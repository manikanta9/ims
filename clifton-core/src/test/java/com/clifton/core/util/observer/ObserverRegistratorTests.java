package com.clifton.core.util.observer;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class ObserverRegistratorTests {

	@Resource
	private ObserverHandler observerHandler;


	@Test
	public void testRegistration() {
		StringBuilder result = new StringBuilder("0");

		this.observerHandler.getNotifier(ObservedEvent.class, o -> true).afterUpdate(result);
		Assertions.assertEquals("01", result.toString());
	}
}
