package com.clifton.core.util.runner;


import com.clifton.core.util.status.Status;

import java.util.Date;


public class TestRunner extends AbstractRunner {

	private final TestObject testObject;


	public TestRunner(String type, String id, Date dateTime, TestObject testObject) {
		super(type, id, dateTime);
		this.testObject = testObject;
	}


	@Override
	public Status getStatus() {
		return null;
	}


	@Override
	public void run() {
		this.testObject.execute(this);
	}


	public TestObject getTestScheduleObject() {
		return this.testObject;
	}
}
