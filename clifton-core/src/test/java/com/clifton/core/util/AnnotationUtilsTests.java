package com.clifton.core.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


@SampleAnnotationClassHolder.SampleClassAnnotation(value = 0)
public class AnnotationUtilsTests {

	@SampleFieldAnnotation(value = 2)
	private String sampleField;


	@Test
	@SampleMethodAnnotation(value = 1)
	public void testGetClassAnnotation() {
		Class<?> clazz = this.getClass();
		SampleAnnotationClassHolder.SampleClassAnnotation sampleAnnotation = AnnotationUtils.getAnnotation(clazz, SampleAnnotationClassHolder.SampleClassAnnotation.class);
		Assertions.assertNotNull(sampleAnnotation);
		Assertions.assertEquals(0, sampleAnnotation.value());
	}


	@Test
	@SampleMethodAnnotation(value = 1)
	public void testGetMethodAnnotation() throws Exception {
		Method method = this.getClass().getMethod("testGetMethodAnnotation");
		Assertions.assertNotNull(AnnotationUtils.getAnnotation(method, Test.class));
		SampleMethodAnnotation sampleAnnotation = AnnotationUtils.getAnnotation(method, SampleMethodAnnotation.class);
		Assertions.assertNotNull(sampleAnnotation);
		Assertions.assertEquals(1, sampleAnnotation.value());
	}


	@Test
	public void testGetFieldAnnotation() throws Exception {
		Field field = this.getClass().getDeclaredField("sampleField");
		SampleFieldAnnotation sampleAnnotation = AnnotationUtils.getAnnotation(field, SampleFieldAnnotation.class);
		Assertions.assertNotNull(sampleAnnotation);
		Assertions.assertEquals(2, sampleAnnotation.value());
	}


	@Test
	@SampleMethodAnnotation(value = 1)
	public void testAnnotationIsTrue() throws Exception {
		Method badMethod = this.getClass().getMethod("testGetFieldAnnotation");
		Assertions.assertFalse(AnnotationUtils.isTrue(badMethod, SampleMethodAnnotation.class, a -> a.value() == 1));

		Method goodMethod = this.getClass().getMethod("testAnnotationIsTrue");
		Assertions.assertFalse(AnnotationUtils.isTrue(goodMethod, SampleMethodAnnotation.class, a -> a.value() == 2));
		Assertions.assertTrue(AnnotationUtils.isTrue(goodMethod, SampleMethodAnnotation.class, a -> a.value() == 1));
	}


	@Target(ElementType.METHOD)
	@Retention(RetentionPolicy.RUNTIME)
	private @interface SampleMethodAnnotation {

		int value() default -1;
	}

	@Target(ElementType.FIELD)
	@Retention(RetentionPolicy.RUNTIME)
	private @interface SampleFieldAnnotation {

		int value() default -1;
	}
}

class SampleAnnotationClassHolder {

	@Target(ElementType.TYPE)
	@Retention(RetentionPolicy.RUNTIME)
	public @interface SampleClassAnnotation {

		int value() default -1;
	}
}
