package com.clifton.core.util.compression;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author stevenf
 */
public class GzipCompressionUtilsTests {

	private static final String BIG_STRING = "This string is too big!";


	@Test
	public void testCompress() throws Exception {
		byte[] bytes = null;
		Assertions.assertEquals("", GzipCompressionUtils.decompress(GzipCompressionUtils.compress(bytes)));

		String string = null;
		Assertions.assertEquals("", GzipCompressionUtils.decompress(GzipCompressionUtils.compress(string)));
		byte[] compressedBytes = GzipCompressionUtils.compress(BIG_STRING);
		Assertions.assertEquals(BIG_STRING, GzipCompressionUtils.decompress(compressedBytes));
		Assertions.assertNotEquals(BIG_STRING, new String(compressedBytes));
	}


	@Test
	public void testDecompress() throws Exception {
		String bigString = GzipCompressionUtils.decompress(BIG_STRING.getBytes());
		Assertions.assertEquals(BIG_STRING, bigString);
		Assertions.assertNotEquals(BIG_STRING, GzipCompressionUtils.compress(BIG_STRING.getBytes()));

		bigString = GzipCompressionUtils.decompress(GzipCompressionUtils.compress(BIG_STRING.getBytes()));
		Assertions.assertEquals(BIG_STRING, bigString);
		Assertions.assertNotEquals(BIG_STRING, new String(GzipCompressionUtils.compress(BIG_STRING.getBytes())));
	}


	@Test
	public void testIsCompressed() throws Exception {
		Assertions.assertFalse(GzipCompressionUtils.isCompressed(BIG_STRING.getBytes()));
		Assertions.assertTrue(GzipCompressionUtils.isCompressed(GzipCompressionUtils.compress(BIG_STRING)));
	}
}
