package com.clifton.core.util.compare;

import com.clifton.core.beans.BeanUtilsTestBean;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * The {@link CoreCompareUtilsTests} tests the {@link CoreCompareUtils} class
 *
 * @author manderson
 */
public class CoreCompareUtilsTests {


	@Test
	public void testCompareIsNonNullPropertiesEqual() {
		BeanUtilsTestBean bean1 = new BeanUtilsTestBean();
		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		BeanUtilsTestBean parent1 = new BeanUtilsTestBean();
		BeanUtilsTestBean parent2 = new BeanUtilsTestBean();

		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, null));

		//Bean 1 parent is null so this should still pass
		bean2.setParent(parent2);
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, null));

		//Bean 1 parent is not null and bean 2 is null. These are not equal
		bean2.setParent(null);
		bean1.setParent(parent1);
		Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, null));

		//They should be the same because each bean has the same parent
		bean2.setParent(parent1);
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, null));

		//Should pass because parent1.equals(parent2) == true
		bean2.setParent(parent2);
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, null));

		//The parents are different objects but now we are saying do not compare the parents as objects
		//  but rather recursively compare their inner properties
		//  Since these parents fields are all null it should be equal
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, new Class[]{BeanUtilsTestBean.class}));


		//We are recursively investigating the parent but we then have field after the parent field that is not equal
		//  It's not null on bean1 so these two should not be considered equal
		bean1.setBigDecimal(BigDecimal.ONE);
		Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqual(bean1, bean2, false, new Class[]{BeanUtilsTestBean.class}));
	}


	@Test
	public void testCompareIsNonNullPropertiesEqualForLists() {
		testCompareIsNonNullPropertiesEqualForLists(true);
	}


	private void testCompareIsNonNullPropertiesEqualForLists(boolean sorted) {
		List<BeanUtilsTestBean> list1 = null;
		List<BeanUtilsTestBean> list2 = null;

		BeanUtilsTestBean bean1 = new BeanUtilsTestBean();
		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		BeanUtilsTestBean bean3 = new BeanUtilsTestBean();
		BeanUtilsTestBean bean4 = new BeanUtilsTestBean();
		BeanUtilsTestBean bean5 = new BeanUtilsTestBean();
		BeanUtilsTestBean bean6 = new BeanUtilsTestBean();

		// Validate null/empty contents comparison
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		list1 = new ArrayList<>();
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		list2 = new ArrayList<>();
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));

		// Validate simple comparison
		list1.add(bean1);
		Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		list2.add(bean2);
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));

		// bean1 should now be different
		bean1.setName("foo");
		bean2.setName("bar");

		final List<BeanUtilsTestBean> list3 = new ArrayList<>(list1);
		final List<BeanUtilsTestBean> list4 = new ArrayList<>(list2);
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, ()
				-> Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqualForLists(list3, list4, sorted, true)));
		Assertions.assertEquals(sorted ? "Error occurred at list index [0] while comparing lists for equality. Object from first list was: {id=null,label=foo}\nObject from second list was: {id=null,label=bar}" : "Error occurred while comparing lists for equality. Object from first list [{id=null,label=foo}] did not find a match.", validationException.getMessage());

		// Validate ordered comparison; bean1 = bean2 and bean3 = bean4
		bean2.setName("foo");
		list1.add(bean3);
		list2.add(bean4);
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));

		// Validate non-ordered comparison; bean1 = bean4 and bean2 = bean3
		bean2.setName("bar");
		bean3.setName("bar");
		bean4.setName("foo");
		if (sorted) {
			Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		}
		else {
			Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		}

		// Validate comparison with duplicates
		// All Beans equal
		bean2.setName("foo");
		bean3.setName("foo");
		Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		// bean1 = bean2 = bean3 = bean4 = bean6, bean5 different
		list1.add(bean5);
		list2.add(bean6);
		bean5.setName("bar");
		bean6.setName("foo");
		Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		// bean 1 = bean6, all others equal
		bean2.setName("bar");
		bean3.setName("bar");
		bean4.setName("bar");
		if (sorted) {
			Assertions.assertFalse(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		}
		else {
			Assertions.assertTrue(CoreCompareUtils.isNonNullPropertiesEqualForLists(list1, list2, sorted, false));
		}
	}


	@Test
	public void testCompareIsNonNullPropertiesEqualForUnorderedLists() {
		testCompareIsNonNullPropertiesEqualForLists(false);
	}


	@Test
	public void testGetNoEqualProperties() {
		// Generate test data
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setName("b1");
		b1.setId(1);
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setTestGetterSetterFieldSetter(true);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setName("b2");
		b2.setId(2);
		b2.setBigDecimal(BigDecimal.ONE);
		b2.setTestGetterSetterFieldSetter(false);

		// Run test
		List<String> expectedList;
		List<String> actualList;
		expectedList = Arrays.asList("id", "name", "label", "identity", "testGetterSetterFieldGetter");
		actualList = CoreCompareUtils.getNoEqualProperties(b1, b2, false);
		Collections.sort(expectedList);
		Collections.sort(actualList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testGetNoEqualPropertiesWithSystemFields() {
		// Generate test data
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setName("b1");
		b1.setId(1);
		b1.setRv(new byte[]{1, 2, 3, 4});
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setTestGetterSetterFieldSetter(true);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setName("b2");
		b2.setId(2);
		b2.setRv(new byte[]{5, 6, 7, 8});
		b2.setBigDecimal(BigDecimal.ONE);
		b2.setTestGetterSetterFieldSetter(false);

		// Run test
		List<String> expectedList;
		List<String> actualList;
		expectedList = Arrays.asList("id", "name", "label", "identity", "testGetterSetterFieldGetter");
		actualList = CoreCompareUtils.getNoEqualProperties(b1, b2, false);
		Collections.sort(expectedList);
		Collections.sort(actualList);
		Assertions.assertEquals(expectedList, actualList);

		expectedList = Arrays.asList("id", "name", "label", "identity", "testGetterSetterFieldGetter", "rv");
		actualList = CoreCompareUtils.getNoEqualProperties(b1, b2, true);
		Collections.sort(expectedList);
		Collections.sort(actualList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testGetNoEqualPropertiesWithIgnoredProperties() {
		// Generate test data
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setName("b1");
		b1.setId(1);
		b1.setDescription("some first description");
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setTestGetterSetterFieldSetter(true);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setName("b2");
		b2.setId(2);
		b2.setDescription("some second description");
		b2.setBigDecimal(BigDecimal.ONE);
		b2.setTestGetterSetterFieldSetter(false);

		// Run test
		List<String> expectedList;
		List<String> actualList;
		expectedList = Arrays.asList("id", "name", "label", "identity", "testGetterSetterFieldGetter", "description");
		actualList = CoreCompareUtils.getNoEqualProperties(b1, b2, false);
		Collections.sort(expectedList);
		Collections.sort(actualList);
		Assertions.assertEquals(expectedList, actualList);

		expectedList = Arrays.asList("id", "name", "label");
		actualList = CoreCompareUtils.getNoEqualProperties(b1, b2, false, "identity", "testGetterSetterFieldGetter", "description");
		Collections.sort(expectedList);
		Collections.sort(actualList);
		Assertions.assertEquals(expectedList, actualList);
	}


	@Test
	public void testGetNoEqualPropertiesWithSetter() {
		// Generate test data
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setName("b1");
		b1.setId(1);
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setTestGetterSetterFieldSetter(true);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setName("b2");
		b2.setId(2);
		b2.setBigDecimal(BigDecimal.ONE);
		b2.setTestGetterSetterFieldSetter(false);

		// Run test
		List<String> expectedList;
		List<String> actualList;
		expectedList = Arrays.asList("id", "name", "label");
		actualList = CoreCompareUtils.getNoEqualPropertiesWithSetters(b1, b2, false);
		Collections.sort(expectedList);
		Collections.sort(actualList);
		Assertions.assertEquals(expectedList, actualList);
	}
}
