package com.clifton.core.util.aws.s3;

import com.adobe.testing.s3mock.junit5.S3MockExtension;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3URI;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.RegisterExtension;

import java.io.File;

import static com.clifton.core.util.aws.s3.AWSS3Utils.getAmazonS3Uri;
import static com.clifton.core.util.aws.s3.AWSS3Utils.getS3FileKey;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;


/**
 * Test for validating that objects are sent to S3
 */
class AWSS3HandlerTests {
	@RegisterExtension
	static final S3MockExtension S3_MOCK =
			S3MockExtension.builder().silent().withSecureConnection(true).build();

	private static final AWSS3Handler awsS3Handler = new AWSS3HandlerImpl();

	private static final String BUCKET = "ppacoresoftsyssandbox-priv-mpls-testing";
	private static final String REGION = "us-east-1";

	private final String ENDPOINT = S3_MOCK.getServiceEndpoint();
	private final AmazonS3 s3Client = S3_MOCK.createS3Client(REGION);

	String filePath = FileUtils.normalizeFilePathSeparators("src\\test\\java\\com\\clifton\\core\\util\\");
	String fileName = "20141231.IRSTR_CLF_EOD.csv";

	@BeforeEach
	void beforeEach() {
		this.s3Client.createBucket(BUCKET);
	}

	@Test
	void testValidBucketName() {
		assertDoesNotThrow(() -> awsS3Handler.validateBucketName(BUCKET));
	}

	@Test
	void testInvalidBucketName() {
		assertThrows(ValidationException.class, () -> awsS3Handler.validateBucketName("TEST..1231"));
	}

	@Test
	void testSendObjectWithoutPath() {
		File uploadFile = new File(this.filePath + this.fileName);
		awsS3Handler.send(REGION, BUCKET, FileContainerFactory.getFileContainer(uploadFile), this.fileName, uploadFile.length(), this.ENDPOINT);

		assertTrue(this.s3Client.doesObjectExist(BUCKET, this.fileName));
	}

	@Test
	void testSendObjectWithPath() {
		File uploadFile = new File(this.filePath + this.fileName);
		String s3DestinationPath = BUCKET + "/path/to/file";
		AmazonS3URI s3Uri = getAmazonS3Uri(s3DestinationPath);
		String s3FileKey = getS3FileKey(s3Uri.getKey(), this.fileName);

		assertDoesNotThrow(() -> awsS3Handler.validateBucketName(s3Uri.getBucket()));
		assertEquals(s3DestinationPath, String.format("%s/%s", s3Uri.getBucket(), s3Uri.getKey()));

		awsS3Handler.send(REGION, s3DestinationPath, FileContainerFactory.getFileContainer(uploadFile), this.fileName, uploadFile.length(), this.ENDPOINT);

		assertTrue(this.s3Client.doesObjectExist(BUCKET, s3FileKey));
	}
}
