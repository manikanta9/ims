package com.clifton.core.util.math;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtilsTestBean;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>CoreMathUtilsTests</code> class tests the {@link CoreMathUtils}.
 *
 * @author manderson
 */
@SuppressWarnings("SimplifiableJUnitAssertion")
public class CoreMathUtilsTests {


	@Test
	public void testSumPropertyName_BadFieldName() {
		Assertions.assertThrows(RuntimeException.class, () -> {
			List<BeanUtilsTestBean> testList = new ArrayList<>();
			testList.add(new BeanUtilsTestBean());
			CoreMathUtils.sumPropertyName(testList, "BadFieldName");
		});
	}


	@Test
	public void testSumPropertyName_BadDataType() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			List<BeanUtilsTestBean> testList = new ArrayList<>();
			testList.add(new BeanUtilsTestBean());
			testList.get(0).setName("NOT A NUMBER");
			CoreMathUtils.sumPropertyName(testList, "name");
		});
	}


	@Test
	public void testSum() {
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sum(((List<BigDecimal>) null)));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sum(((BigDecimal[]) null)));

		BigDecimal[] values = new BigDecimal[]{BigDecimal.ONE, BigDecimal.TEN, BigDecimal.ZERO, null};
		List<BigDecimal> valueList = CollectionUtils.createList(values);
		BigDecimal expectedSum = new BigDecimal("11");
		Assertions.assertEquals(expectedSum, CoreMathUtils.sum(valueList));
		Assertions.assertEquals(expectedSum, CoreMathUtils.sum(values));
	}


	@Test
	public void testSumPropertyName() {
		List<BeanUtilsTestBean> testList = new ArrayList<>();
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));

		testList.add(new BeanUtilsTestBean());
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));

		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ZERO);
		testList.add(bean2);
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));

		BeanUtilsTestBean bean3 = new BeanUtilsTestBean();
		bean3.setBigDecimal(BigDecimal.valueOf(513.64));
		testList.add(bean3);
		Assertions.assertEquals(BigDecimal.valueOf(513.64), CoreMathUtils.sumPropertyName(testList, "bigDecimal"));

		BeanUtilsTestBean bean4 = new BeanUtilsTestBean();
		bean4.setBigDecimal(BigDecimal.valueOf(-5.12));
		testList.add(bean4);
		Assertions.assertEquals(BigDecimal.valueOf(508.52), CoreMathUtils.sumPropertyName(testList, "bigDecimal"));

		testList.add(null);
		Assertions.assertEquals(BigDecimal.valueOf(508.52), CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
	}


	@Test
	public void testSumPropertyFunctional() {
		List<BeanUtilsTestBean> testList = new ArrayList<>();
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		testList.add(new BeanUtilsTestBean());
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ZERO);
		testList.add(bean2);
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		BeanUtilsTestBean bean3 = new BeanUtilsTestBean();
		bean3.setBigDecimal(BigDecimal.valueOf(513.64));
		testList.add(bean3);
		Assertions.assertEquals(BigDecimal.valueOf(513.64), CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		BeanUtilsTestBean bean4 = new BeanUtilsTestBean();
		bean4.setBigDecimal(BigDecimal.valueOf(-5.12));
		testList.add(bean4);
		Assertions.assertEquals(BigDecimal.valueOf(508.52), CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		testList.add(null);
		Assertions.assertEquals(BigDecimal.valueOf(508.52), CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
	}


	@Test
	public void testSumAbsolutePropertyFunctional() {
		List<BeanUtilsTestBean> testList = new ArrayList<>();
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal, true));

		testList.add(new BeanUtilsTestBean());
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal, true));

		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ZERO);
		testList.add(bean2);
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal, true));

		BeanUtilsTestBean bean3 = new BeanUtilsTestBean();
		bean3.setBigDecimal(BigDecimal.valueOf(513.64));
		testList.add(bean3);
		Assertions.assertEquals(BigDecimal.valueOf(513.64), CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal, true));

		BeanUtilsTestBean bean4 = new BeanUtilsTestBean();
		bean4.setBigDecimal(BigDecimal.valueOf(-5.12));
		testList.add(bean4);
		Assertions.assertEquals(BigDecimal.valueOf(518.76), CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal, true));

		testList.add(null);
		Assertions.assertEquals(BigDecimal.valueOf(518.76), CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal, true));
	}


	@Test
	public void testApplySumPropertyDifferenceToMax() {
		BigDecimal totalExpected = BigDecimal.ZERO;

		List<BeanUtilsTestBean> testList = new ArrayList<>();
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		testList.add(bean);
		MathUtils.PropertyValueAdjustmentResults adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, BigDecimal.ZERO, true);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ONE);
		testList.add(bean2);
		totalExpected = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, true);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(totalExpected, bean2.getBigDecimal());

		BeanUtilsTestBean bean3 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ZERO);
		bean3.setBigDecimal(BigDecimal.valueOf(513.64));
		testList.add(bean3);
		totalExpected = bean3.getBigDecimal();
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, true);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.ZERO, bean2.getBigDecimal());
		Assertions.assertEquals(BigDecimal.valueOf(513.64), bean3.getBigDecimal());

		BeanUtilsTestBean bean4 = new BeanUtilsTestBean();
		bean4.setBigDecimal(BigDecimal.valueOf(-0.12));
		testList.add(bean4);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, true);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.valueOf(513.76), bean3.getBigDecimal());

		testList.add(null);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, true);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.valueOf(513.76), bean3.getBigDecimal());
	}


	@Test
	public void testApplySumPropertyDifferenceToMin() {
		BigDecimal totalExpected = BigDecimal.ZERO;

		List<BeanUtilsTestBean> testList = new ArrayList<>();
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		testList.add(bean);
		MathUtils.PropertyValueAdjustmentResults adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, BigDecimal.ZERO, false);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ONE);
		testList.add(bean2);
		totalExpected = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, false);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.ONE, bean2.getBigDecimal());
		Assertions.assertEquals(BigDecimal.valueOf(0.5), bean.getBigDecimal());

		BeanUtilsTestBean bean3 = new BeanUtilsTestBean();
		bean.setBigDecimal(BigDecimal.ZERO);
		bean3.setBigDecimal(BigDecimal.valueOf(513.64));
		testList.add(bean3);
		totalExpected = BigDecimal.valueOf(514.64);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, false);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.ZERO, bean.getBigDecimal());
		Assertions.assertEquals(BigDecimal.ONE, bean2.getBigDecimal());
		Assertions.assertEquals(BigDecimal.valueOf(513.64), bean3.getBigDecimal());

		BeanUtilsTestBean bean4 = new BeanUtilsTestBean();
		bean4.setBigDecimal(BigDecimal.valueOf(-1.5));
		testList.add(bean4);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, false);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.valueOf(513.64), bean3.getBigDecimal());
		Assertions.assertEquals(BigDecimal.valueOf(0.0), MathUtils.round(bean4.getBigDecimal(), 1));

		testList.add(null);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, false);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.valueOf(513.64), bean3.getBigDecimal());
	}


	public void testApplySumPropertyDifferenceToObject() {
		BigDecimal totalExpected = BigDecimal.ZERO;

		List<BeanUtilsTestBean> testList = new ArrayList<>();
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		testList.add(bean);
		MathUtils.PropertyValueAdjustmentResults adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, BigDecimal.ZERO, () -> bean);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));

		BeanUtilsTestBean bean2 = new BeanUtilsTestBean();
		bean2.setBigDecimal(BigDecimal.ONE);
		testList.add(bean2);
		totalExpected = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, () -> bean2);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(totalExpected, bean2.getBigDecimal());

		bean2.setBigDecimal(BigDecimal.ONE);
		totalExpected = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, () -> bean);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.ONE, bean2.getBigDecimal());
		Assertions.assertEquals(BigDecimal.valueOf(0.5), bean.getBigDecimal());

		bean.setBigDecimal(BigDecimal.ZERO);
		totalExpected = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, () -> bean, BigDecimal.valueOf(0.25));
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.ADJUSTMENT_TOO_GREAT);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
		Assertions.assertEquals(BigDecimal.ONE, bean2.getBigDecimal());
		Assertions.assertEquals(BigDecimal.ZERO, bean.getBigDecimal());

		testList.add(null);
		adjustmentResult = CoreMathUtils.applySumPropertyDifference(testList, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, totalExpected, () -> bean);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumPropertyName(testList, "bigDecimal"));
		Assertions.assertEquals(totalExpected, CoreMathUtils.sumProperty(testList, BeanUtilsTestBean::getBigDecimal));
	}


	public void testApplyPropertyAdjustment() {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		MathUtils.PropertyValueAdjustmentResults adjustmentResult = CoreMathUtils.applyPropertyAdjustment(() -> bean, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, BigDecimal.ZERO);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT);

		bean.setBigDecimal(BigDecimal.ONE);
		BigDecimal adjustment = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applyPropertyAdjustment(() -> bean, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, adjustment);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(adjustment, bean.getBigDecimal());

		bean.setBigDecimal(BigDecimal.ONE);
		adjustment = BigDecimal.valueOf(1.5);
		adjustmentResult = CoreMathUtils.applyPropertyAdjustment(() -> bean, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, adjustment, BigDecimal.valueOf(0.25));
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.ADJUSTMENT_TOO_GREAT);
		Assertions.assertEquals(BigDecimal.ONE, bean.getBigDecimal());

		adjustmentResult = CoreMathUtils.applyPropertyAdjustment(() -> bean, BeanUtilsTestBean::getBigDecimal, BeanUtilsTestBean::setBigDecimal, adjustment, BigDecimal.ONE);
		Assertions.assertEquals(adjustmentResult, MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH);
		Assertions.assertEquals(adjustment, bean.getBigDecimal());
	}


	@Test
	public void testMaxCollection() {
		Assertions.assertNull(CoreMathUtils.max((Collection<BigDecimal>) null));
		Assertions.assertNull(CoreMathUtils.max(bigDecimalList()));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.max(bigDecimalList("0")));
		Assertions.assertEquals(new BigDecimal("2"), CoreMathUtils.max(bigDecimalList("-1", "2")));
		Assertions.assertEquals(new BigDecimal("2"), CoreMathUtils.max(bigDecimalList("-1", "2", "-2")));
		Assertions.assertEquals(new BigDecimal("2"), CoreMathUtils.max(bigDecimalList("0", "1", "2")));
	}


	@Test
	public void testMaxNumericCollection() {
		Assertions.assertNull(CoreMathUtils.maxNumeric(null));
		Assertions.assertNull(CoreMathUtils.maxNumeric(numberList()));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.maxNumeric(numberList(new BigDecimal("0"))));
		Assertions.assertEquals(new BigDecimal("2"), CoreMathUtils.maxNumeric(numberList(new BigDecimal("-1"), new BigDecimal("2"))));
		Assertions.assertEquals(new BigDecimal("2"), CoreMathUtils.maxNumeric(numberList(new BigDecimal("-1"), new BigDecimal("2"), new BigDecimal("-2"))));
		Assertions.assertEquals(new BigDecimal("2"), CoreMathUtils.maxNumeric(numberList(new BigDecimal("0"), new BigDecimal("1"), new BigDecimal("2"))));
		Assertions.assertEquals(3, (long) CoreMathUtils.maxNumeric(numberList(1, 2, 3)));
		Assertions.assertEquals(-1, (long) CoreMathUtils.maxNumeric(numberList(-1, -2, -3)));
		Assertions.assertEquals(3, (long) CoreMathUtils.maxNumeric(numberList(-1, -2, -3, 1, 2, 3)));
		Assertions.assertEquals(9999.0F, CoreMathUtils.maxNumeric(numberList(1, 2, 3, 0.1F, 9999.0F, 16.0F)));
		Assertions.assertEquals(1, CoreMathUtils.maxNumeric(numberList(new BigDecimal("0"), 1)));
		Assertions.assertEquals(new BigDecimal("1"), CoreMathUtils.maxNumeric(numberList(new BigDecimal("1"), 1)));
	}


	@Test
	public void testMinVarargs() {
		Assertions.assertNull(CoreMathUtils.min());
		Assertions.assertEquals(null, CoreMathUtils.min((Number) null));
		Assertions.assertEquals(null, CoreMathUtils.min((Number) null, null));
		Assertions.assertNull(CoreMathUtils.min(null, null, null));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.min((Number) null, new BigDecimal("0")));
		Assertions.assertEquals(-1, CoreMathUtils.min(null, BigDecimal.ZERO, -1));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.min((Number) new BigDecimal("0"), null));
		Assertions.assertEquals(BigDecimal.TEN.negate(), CoreMathUtils.min(new BigDecimal("0"), null, BigDecimal.TEN.negate()));
		Assertions.assertEquals(new BigDecimal("-5.39"), CoreMathUtils.min((Number) new BigDecimal("-5.39"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("-5.39"), CoreMathUtils.min(new BigDecimal("-5.39"), new BigDecimal("5.39"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("-5.39"), CoreMathUtils.min((Number) new BigDecimal("5.39"), new BigDecimal("-5.39")));
		Assertions.assertEquals(new BigDecimal("-5.39"), CoreMathUtils.min(new BigDecimal("5.39"), new BigDecimal("-5.39"), new BigDecimal("-5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), CoreMathUtils.min((Number) new BigDecimal("6"), new BigDecimal("5.39")));
		Assertions.assertEquals(new BigDecimal("5.39"), CoreMathUtils.min(new BigDecimal("6"), new BigDecimal("5.39"), new BigDecimal("6")));
		Assertions.assertEquals(new BigDecimal("5.39"), CoreMathUtils.min((Number) new BigDecimal("5.39"), new BigDecimal("6")));
		Assertions.assertEquals(new BigDecimal("5.39"), CoreMathUtils.min(new BigDecimal("5.39"), new BigDecimal("6"), new BigDecimal("6")));
		Assertions.assertEquals(0, (int) CoreMathUtils.min(0, 5, 1, 2, 3));
		Assertions.assertEquals(-10, (int) CoreMathUtils.min(-10, -9, -7, -5));
		Assertions.assertEquals((short) 0, CoreMathUtils.min((short) 0, (short) 1, (short) 10, 10L));
	}


	@Test
	public void testMinCollection() {
		Assertions.assertNull(CoreMathUtils.min((Collection<BigDecimal>) null));
		Assertions.assertNull(CoreMathUtils.min(bigDecimalList()));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.min(bigDecimalList("0")));
		Assertions.assertEquals(new BigDecimal("-1"), CoreMathUtils.min(bigDecimalList("-1", "2")));
		Assertions.assertEquals(new BigDecimal("-2"), CoreMathUtils.min(bigDecimalList("-1", "2", "-2")));
		Assertions.assertEquals(new BigDecimal("0"), CoreMathUtils.min(bigDecimalList("0", "1", "2")));
	}


	/*@Test
	public void testDivideByZeroReturnDefaultValue() {
		validateDivideByZeroReturnDefaultValue("10", "0", "0", "0");
		validateDivideByZeroReturnDefaultValue("10", "0", "1", "1");
		validateDivideByZeroReturnDefaultValue("10", "2", "7", "5");
	}*/


	@Test
	public void testPercentValue() {
		BigDecimal value = null;
		BigDecimal total = null;

		BigDecimal result = CoreMathUtils.getPercentValue(value, total, false);
		Assertions.assertNull(result);
		result = CoreMathUtils.getPercentValue(value, total, true);
		Assertions.assertNull(result);

		value = BigDecimal.ZERO;
		result = CoreMathUtils.getPercentValue(value, total);
		Assertions.assertNull(result);
		result = CoreMathUtils.getPercentValue(value, total, true);
		Assertions.assertNull(result);

		total = BigDecimal.ZERO;
		result = CoreMathUtils.getPercentValue(value, total, false);
		Assertions.assertEquals(BigDecimal.ZERO, result);
		result = CoreMathUtils.getPercentValue(value, total, true);
		Assertions.assertEquals(BigDecimal.ZERO, result);

		value = BigDecimal.valueOf(25);
		total = BigDecimal.valueOf(75);
		result = CoreMathUtils.getPercentValue(value, total);
		Assertions.assertEquals(BigDecimal.valueOf(0.33), MathUtils.round(result, 2));

		result = CoreMathUtils.getPercentValue(value, total, true);
		Assertions.assertEquals(BigDecimal.valueOf(33.33), MathUtils.round(result, 2));
	}


	@Test
	public void testPercentValueWithValidation() {
		BigDecimal value = BigDecimal.valueOf(115090);
		BigDecimal total = BigDecimal.valueOf(85);

		validatePercentValueException(value, total, BigDecimal.valueOf(135400), true, true);
		validatePercentValueException(value, total, BigDecimal.valueOf(135400), false, true);

		total = BigDecimal.valueOf(-85);
		validatePercentValueException(value, total, BigDecimal.valueOf(-135400), true, true);
		validatePercentValueException(value, total, BigDecimal.valueOf(-135400), false, true);

		total = BigDecimal.valueOf(100000);
		validatePercentValueException(value, total, BigDecimal.valueOf(115.09), true, false);

		value = BigDecimal.valueOf(-115090);
		validatePercentValueException(value, total, BigDecimal.valueOf(-1.15), false, false);
	}


	private void validatePercentValueException(BigDecimal value, BigDecimal total, BigDecimal resultPercentage, boolean returnAsHundredsValue, boolean expectException) {
		BigDecimal result;
		try {
			result = CoreMathUtils.getPercentValue(value, total, returnAsHundredsValue, true);
		}
		catch (ValidationException e) {
			if (expectException) {
				Assertions.assertEquals(
						CoreMathUtils.formatNumberDecimal(value) + " as a percent of " + CoreMathUtils.formatNumberDecimal(total) + " results in a percentage that is too large ["
								+ CoreMathUtils.formatNumberDecimal(resultPercentage) + "].  The maximum percentage value cannot exceed [9,999.99999].", e.getMessage());
			}
			else {
				Assertions.fail("Did not expect validation exception for max percentage value, but instead received the following exception [" + e.getMessage() + "]");
			}
			return;
		}
		if (expectException) {
			Assertions.fail("Expected validation exception for max percentage value, but instead received the following result percentage [" + CoreMathUtils.formatNumberDecimal(result) + "]");
		}
		else {
			Assertions.assertEquals(resultPercentage, MathUtils.round(result, 2));
		}
	}


	@Test
	public void testGetTotalPercentChangeAnnualizedDaily() {
		BigDecimal[] values = null;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, null)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false, null)));

		values = new BigDecimal[]{};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, null)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false, null)));

		values = new BigDecimal[]{BigDecimal.valueOf(0.02), BigDecimal.valueOf(0.03), BigDecimal.valueOf(0.04), BigDecimal.valueOf(0.05), BigDecimal.valueOf(0.01), BigDecimal.valueOf(-0.05),
				BigDecimal.valueOf(-0.06), BigDecimal.valueOf(-0.07), BigDecimal.valueOf(0.05), BigDecimal.valueOf(0.06), BigDecimal.valueOf(0.02), BigDecimal.valueOf(0.03), BigDecimal.valueOf(-0.02),
				BigDecimal.valueOf(-0.02), BigDecimal.valueOf(-0.02), BigDecimal.valueOf(0.03), BigDecimal.valueOf(0.02)};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.0794), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false, 510), 4)));

		values = new BigDecimal[]{BigDecimal.valueOf(-2.25), BigDecimal.valueOf(-0.62), BigDecimal.valueOf(-1.18), BigDecimal.valueOf(-0.29), BigDecimal.valueOf(2.61), BigDecimal.valueOf(-0.13),
				BigDecimal.valueOf(2.28), BigDecimal.valueOf(1.3), BigDecimal.valueOf(3.38), BigDecimal.valueOf(-0.22), BigDecimal.valueOf(2.18), BigDecimal.valueOf(5.81), BigDecimal.valueOf(-2.09),
				BigDecimal.valueOf(5.06), BigDecimal.valueOf(-2.46), BigDecimal.valueOf(-4.05), BigDecimal.valueOf(1.34), BigDecimal.valueOf(3.51), BigDecimal.valueOf(2.09),
				BigDecimal.valueOf(-1.35), BigDecimal.valueOf(0.77), BigDecimal.valueOf(3.57), BigDecimal.valueOf(-1.6), BigDecimal.valueOf(2.86), BigDecimal.valueOf(-0.52), BigDecimal.valueOf(0),
				BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(-15.63), BigDecimal.valueOf(-5.67),
				BigDecimal.valueOf(0.63), BigDecimal.valueOf(-0.55), BigDecimal.valueOf(-5.09), BigDecimal.valueOf(0.78), BigDecimal.valueOf(2.5), BigDecimal.valueOf(1.4)};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-1.29), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, 1170), 2)));

		values = new BigDecimal[]{BigDecimal.valueOf(-2.56), BigDecimal.valueOf(-0.62), BigDecimal.valueOf(-1.18), BigDecimal.valueOf(-0.29), BigDecimal.valueOf(2.61), BigDecimal.valueOf(-0.13),
				BigDecimal.valueOf(2.28), BigDecimal.valueOf(1.3), BigDecimal.valueOf(3.38), BigDecimal.valueOf(-0.22), BigDecimal.valueOf(2.18), BigDecimal.valueOf(5.81), BigDecimal.valueOf(-2.09),
				BigDecimal.valueOf(5.06), BigDecimal.valueOf(-2.46), BigDecimal.valueOf(-4.05), BigDecimal.valueOf(1.34), BigDecimal.valueOf(3.51), BigDecimal.valueOf(2.09),
				BigDecimal.valueOf(-1.35), BigDecimal.valueOf(0.77), BigDecimal.valueOf(3.57), BigDecimal.valueOf(-1.6), BigDecimal.valueOf(2.86), BigDecimal.valueOf(-0.52),
				BigDecimal.valueOf(-15.63), BigDecimal.valueOf(-5.67), BigDecimal.valueOf(0.63), BigDecimal.valueOf(-0.55), BigDecimal.valueOf(-5.09), BigDecimal.valueOf(0.78),
				BigDecimal.valueOf(2.5), BigDecimal.valueOf(1.4)};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-1.29), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, 1260), 2)));

		values = new BigDecimal[]{BigDecimal.ZERO, BigDecimal.valueOf(-34.0400731792), BigDecimal.valueOf(-10.9285544838), BigDecimal.valueOf(-79.9553622755), BigDecimal.valueOf(16.6414792277), BigDecimal.valueOf(-42.4746903332),
				BigDecimal.valueOf(10.7161445300), BigDecimal.valueOf(-55.0471847978), BigDecimal.valueOf(-144.8356807739), BigDecimal.valueOf(-81.4409017160), BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-97.66343), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedDaily(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, 556), 5)));
	}


	@Test
	public void testGetTotalPercentChange() {
		BigDecimal[] values = null;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getTotalPercentChange(values, true)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getTotalPercentChange(values, false)));

		values = new BigDecimal[]{};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getTotalPercentChange(values, true)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, MathUtils.getTotalPercentChange(values, false)));

		values = new BigDecimal[]{BigDecimal.ONE};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ONE, MathUtils.getTotalPercentChange(values, true)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ONE, CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true)));

		values = new BigDecimal[]{BigDecimal.valueOf(-0.23), BigDecimal.valueOf(-1.34), BigDecimal.valueOf(0.13), BigDecimal.valueOf(-2.74), BigDecimal.valueOf(-0.71),
				BigDecimal.valueOf(-3.76), BigDecimal.valueOf(2.91), BigDecimal.valueOf(-2.19), BigDecimal.valueOf(2.31), BigDecimal.valueOf(0.61), BigDecimal.valueOf(1.29),
				BigDecimal.valueOf(-0.50), BigDecimal.valueOf(0.12), BigDecimal.valueOf(-2.75), BigDecimal.valueOf(-0.94), BigDecimal.valueOf(-0.04), BigDecimal.valueOf(1.85),
				BigDecimal.valueOf(0.54), BigDecimal.valueOf(-0.91), BigDecimal.valueOf(0.99), BigDecimal.valueOf(1.71), BigDecimal.valueOf(0.39), BigDecimal.valueOf(0.37)};


		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-3.15), MathUtils.round(MathUtils.getTotalPercentChange(values, true), 2)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-3.15), MathUtils.round(CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true), 2)));


		values = new BigDecimal[]{BigDecimal.valueOf(-0.3668), BigDecimal.valueOf(-1.3923), BigDecimal.valueOf(0.3165), BigDecimal.valueOf(-2.7927), BigDecimal.valueOf(-0.3570),
				BigDecimal.valueOf(-4.2291), BigDecimal.valueOf(4.0203), BigDecimal.valueOf(-2.4171), BigDecimal.valueOf(2.3215), BigDecimal.valueOf(0.7353), BigDecimal.valueOf(1.1353),
				BigDecimal.valueOf(-0.3049), BigDecimal.valueOf(-0.0531), BigDecimal.valueOf(-2.3835), BigDecimal.valueOf(-1.0354), BigDecimal.valueOf(-0.0925), BigDecimal.valueOf(1.9972979),
				BigDecimal.valueOf(0.3826), BigDecimal.valueOf(-0.8606), BigDecimal.valueOf(1.1682832)};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-4.46), MathUtils.round(MathUtils.getTotalPercentChange(values, true), 2)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-4.46), MathUtils.round(CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true), 2)));


		values = new BigDecimal[]{BigDecimal.valueOf(0.0309000), BigDecimal.valueOf(-0.0028000), BigDecimal.valueOf(-0.0106000)};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.01712), MathUtils.round(MathUtils.getTotalPercentChange(values, false), 5)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.01712), MathUtils.round(CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false), 5)));

		values = new BigDecimal[]{BigDecimal.valueOf(0.0133000), BigDecimal.valueOf(0.0208000), BigDecimal.valueOf(-0.0022000), BigDecimal.valueOf(0.0309000), BigDecimal.valueOf(-0.0028000),
				BigDecimal.valueOf(-0.0106000)};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.0498), MathUtils.round(MathUtils.getTotalPercentChange(values, false), 4)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.0498), MathUtils.round(CoreMathUtils.getTotalPercentChange(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false), 4)));
	}


	@Test
	public void testGetTotalPercentChangeAnnualizedMonthly() {
		BigDecimal[] values = null;
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, null)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false, null)));

		values = new BigDecimal[]{};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, null)));
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.ZERO, CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false, null)));

		values = new BigDecimal[]{BigDecimal.valueOf(0.02), BigDecimal.valueOf(0.03), BigDecimal.valueOf(0.04), BigDecimal.valueOf(0.05), BigDecimal.valueOf(0.01), BigDecimal.valueOf(-0.05),
				BigDecimal.valueOf(-0.06), BigDecimal.valueOf(-0.07), BigDecimal.valueOf(0.05), BigDecimal.valueOf(0.06), BigDecimal.valueOf(0.02), BigDecimal.valueOf(0.03), BigDecimal.valueOf(-0.02),
				BigDecimal.valueOf(-0.02), BigDecimal.valueOf(-0.02), BigDecimal.valueOf(0.03), BigDecimal.valueOf(0.02)};
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(0.0783), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, false, null), 4)));

		values = new BigDecimal[]{BigDecimal.valueOf(-2.25), BigDecimal.valueOf(-0.62), BigDecimal.valueOf(-1.18), BigDecimal.valueOf(-0.29), BigDecimal.valueOf(2.61), BigDecimal.valueOf(-0.13),
				BigDecimal.valueOf(2.28), BigDecimal.valueOf(1.3), BigDecimal.valueOf(3.38), BigDecimal.valueOf(-0.22), BigDecimal.valueOf(2.18), BigDecimal.valueOf(5.81), BigDecimal.valueOf(-2.09),
				BigDecimal.valueOf(5.06), BigDecimal.valueOf(-2.46), BigDecimal.valueOf(-4.05), BigDecimal.valueOf(1.34), BigDecimal.valueOf(3.51), BigDecimal.valueOf(2.09),
				BigDecimal.valueOf(-1.35), BigDecimal.valueOf(0.77), BigDecimal.valueOf(3.57), BigDecimal.valueOf(-1.6), BigDecimal.valueOf(2.86), BigDecimal.valueOf(-0.52), BigDecimal.valueOf(0),
				BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(0), BigDecimal.valueOf(-15.63), BigDecimal.valueOf(-5.67),
				BigDecimal.valueOf(0.63), BigDecimal.valueOf(-0.55), BigDecimal.valueOf(-5.09), BigDecimal.valueOf(0.78), BigDecimal.valueOf(2.5), BigDecimal.valueOf(1.4)};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-1.27), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, null), 2)));

		values = new BigDecimal[]{BigDecimal.valueOf(-2.56), BigDecimal.valueOf(-0.62), BigDecimal.valueOf(-1.18), BigDecimal.valueOf(-0.29), BigDecimal.valueOf(2.61), BigDecimal.valueOf(-0.13),
				BigDecimal.valueOf(2.28), BigDecimal.valueOf(1.3), BigDecimal.valueOf(3.38), BigDecimal.valueOf(-0.22), BigDecimal.valueOf(2.18), BigDecimal.valueOf(5.81), BigDecimal.valueOf(-2.09),
				BigDecimal.valueOf(5.06), BigDecimal.valueOf(-2.46), BigDecimal.valueOf(-4.05), BigDecimal.valueOf(1.34), BigDecimal.valueOf(3.51), BigDecimal.valueOf(2.09),
				BigDecimal.valueOf(-1.35), BigDecimal.valueOf(0.77), BigDecimal.valueOf(3.57), BigDecimal.valueOf(-1.6), BigDecimal.valueOf(2.86), BigDecimal.valueOf(-0.52),
				BigDecimal.valueOf(-15.63), BigDecimal.valueOf(-5.67), BigDecimal.valueOf(0.63), BigDecimal.valueOf(-0.55), BigDecimal.valueOf(-5.09), BigDecimal.valueOf(0.78),
				BigDecimal.valueOf(2.5), BigDecimal.valueOf(1.4)};

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(-1.27), MathUtils.round(CoreMathUtils.getTotalPercentChangeAnnualizedMonthly(getTestNumberBeanListForValues(values), TestNumberBean::getBigDecimalField, true, 42), 2)));
	}


	@Test
	public void testAverage() {
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.average(null));
		Assertions.assertEquals(BigDecimal.ZERO, CoreMathUtils.average(Collections.emptyList()));
		Assertions.assertEquals(new BigDecimal("3.00000000000000000000"), CoreMathUtils.average(bigDecimalList("1", "2", "3", "4", "5")));
	}


	@Test
	public void testMeanAverage() {
		Assertions.assertNull(CoreMathUtils.meanAverage(null));
		Assertions.assertNull(CoreMathUtils.meanAverage(Collections.emptyList()));
		Assertions.assertEquals(new BigDecimal("3.00000000000000000000"), CoreMathUtils.meanAverage(bigDecimalList("1", "2", "3", "4", "5")));
		Assertions.assertEquals(new BigDecimal("3.04000000000000000000"), CoreMathUtils.meanAverage(bigDecimalList("1", "2", "3", "4", "5.2")));
		Assertions.assertEquals(new BigDecimal("1.00000000000000000000"), CoreMathUtils.meanAverage(bigDecimalList("1", "1", "1", "1", "1")));
		Assertions.assertEquals(new BigDecimal("1.00000000000000000000"), CoreMathUtils.meanAverage(bigDecimalList("1")));
		Assertions.assertEquals(new BigDecimal("1.45846860000000000000"), CoreMathUtils.meanAverage(bigDecimalList("1.123", "1.54345", "1.51512", "1.123123", "1.98765")));
		Assertions.assertEquals(new BigDecimal("7.00000000000000000000"), CoreMathUtils.meanAverage(bigDecimalList("9", "2", "5", "4", "12", "7", "8", "11", "9", "3", "7", "4", "12", "5", "4", "10", "9", "6", "9", "4")));
	}


	@Test
	public void testMedianAverage() {
		Assertions.assertNull(CoreMathUtils.medianAverage(null));
		Assertions.assertNull(CoreMathUtils.medianAverage(Collections.emptyList()));
		Assertions.assertEquals(new BigDecimal("3"), CoreMathUtils.medianAverage(bigDecimalList("1", "2", "3", "4", "5")));
		Assertions.assertEquals(new BigDecimal("3"), CoreMathUtils.medianAverage(bigDecimalList("1", "2", "3", "4", "5.2")));
		Assertions.assertEquals(new BigDecimal("1"), CoreMathUtils.medianAverage(bigDecimalList("1", "1", "1", "1", "1")));
		Assertions.assertEquals(new BigDecimal("1"), CoreMathUtils.medianAverage(bigDecimalList("1")));
		Assertions.assertEquals(new BigDecimal("1.51512"), CoreMathUtils.medianAverage(bigDecimalList("1.123", "1.54345", "1.51512", "1.123123", "1.98765")));
		Assertions.assertEquals(new BigDecimal("7.00000000000000000000"), CoreMathUtils.medianAverage(bigDecimalList("9", "2", "5", "4", "12", "7", "8", "11", "9", "3", "7", "4", "12", "5", "4", "10", "9", "6", "9", "4")));
		Assertions.assertEquals(new BigDecimal("1.50000000000000000000"), CoreMathUtils.medianAverage(bigDecimalList("1", "1", "1", "2", "2", "2", "2", "1")));
	}


	@Test
	public void testModeAverage() {
		Assertions.assertNull(CoreMathUtils.modeAverage(null));
		Assertions.assertNull(CoreMathUtils.modeAverage(Collections.emptyList()));
		assertCollectionEquals(bigDecimalList("0"), CoreMathUtils.modeAverage(bigDecimalList("0")));
		assertCollectionEquals(bigDecimalList("0"), CoreMathUtils.modeAverage(bigDecimalList("0", "0", "1", "2", "3", "4")));
		assertCollectionEquals(bigDecimalList("1"), CoreMathUtils.modeAverage(bigDecimalList("0", "1", "1", "2", "3", "4")));
		assertCollectionEquals(bigDecimalList("4"), CoreMathUtils.modeAverage(bigDecimalList("0", "1", "1", "2", "3", "4", "4", "4", "4")));
		assertCollectionEquals(bigDecimalList("4"), CoreMathUtils.modeAverage(bigDecimalList("0", "4", "1", "4", "1", "4", "2", "3")));
		assertCollectionEquals(bigDecimalList("0", "1.12345"), CoreMathUtils.modeAverage(bigDecimalList("0", "0", "1.12345", "1.12345", "2")));
		assertCollectionEquals(bigDecimalList("0", "1", "2"), CoreMathUtils.modeAverage(bigDecimalList("0", "0", "1", "1", "2", "2")));
	}


	@Test
	public void testPopulationStandardDeviation() {
		Assertions.assertEquals(new BigDecimal("2.98328677803525955291"), CoreMathUtils.populationStandardDeviation(
				bigDecimalList("9", "2", "5", "4", "12", "7", "8", "11", "9", "3", "7", "4", "12", "5", "4", "10", "9", "6", "9", "4")));
		Assertions.assertEquals(new BigDecimal("2.96232221341209774887"), CoreMathUtils.populationStandardDeviation(
				bigDecimalList("9.234", "2.125", "5.908", "4", "12", "7", "8", "11", "9", "3.12", "7", "4", "12", "5.111", "4", "10.33211", "9", "6", "9", "4")));
	}


	@Test
	public void testSampleStandardDeviation() {
		Assertions.assertEquals(new BigDecimal("3.06078765232604448106"), CoreMathUtils.sampleStandardDeviation(
				bigDecimalList("9", "2", "5", "4", "12", "7", "8", "11", "9", "3", "7", "4", "12", "5", "4", "10", "9", "6", "9", "4")));
		Assertions.assertEquals(new BigDecimal("3.03927846286179021378"), CoreMathUtils.sampleStandardDeviation(
				bigDecimalList("9.234", "2.125", "5.908", "4", "12", "7", "8", "11", "9", "3.12", "7", "4", "12", "5.111", "4", "10.33211", "9", "6", "9", "4")));
	}


	@Test
	public void testPopulationVariance() {
		Assertions.assertEquals(new BigDecimal("8.90000000000000000000"), CoreMathUtils.populationVariance(
				bigDecimalList("9", "2", "5", "4", "12", "7", "8", "11", "9", "3", "7", "4", "12", "5", "4", "10", "9", "6", "9", "4")));
		Assertions.assertEquals(new BigDecimal("8.77535289607475000000"), CoreMathUtils.populationVariance(
				bigDecimalList("9.234", "2.125", "5.908", "4", "12", "7", "8", "11", "9", "3.12", "7", "4", "12", "5.111", "4", "10.33211", "9", "6", "9", "4")));
	}


	@Test
	public void testSampleVariance() {
		Assertions.assertEquals(new BigDecimal("9.36842105263157894737"), CoreMathUtils.sampleVariance(
				bigDecimalList("9", "2", "5", "4", "12", "7", "8", "11", "9", "3", "7", "4", "12", "5", "4", "10", "9", "6", "9", "4")));
		Assertions.assertEquals(new BigDecimal("9.23721357481552631579"), CoreMathUtils.sampleVariance(
				bigDecimalList("9.234", "2.125", "5.908", "4", "12", "7", "8", "11", "9", "3.12", "7", "4", "12", "5.111", "4", "10.33211", "9", "6", "9", "4")));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	private List<BigDecimal> bigDecimalList(String... valueList) {
		return Arrays.stream(valueList)
				.map(BigDecimal::new)
				.collect(Collectors.toList());
	}


	@SafeVarargs
	private final <T extends Number> List<T> numberList(T... numbers) {
		return Arrays.stream(numbers)
				.collect(Collectors.toList());
	}


	/**
	 * Asserts that the contents of each of the given collections are equal, disregarding order.
	 *
	 * @param expectedValueList the expected collection of values
	 * @param actualValueList   the actual collection of values
	 */
	private void assertCollectionEquals(Collection<BigDecimal> expectedValueList, Collection<BigDecimal> actualValueList) {
		// Guard-clause: Verify same nullity
		if (expectedValueList == null || actualValueList == null) {
			Assertions.assertEquals(expectedValueList, actualValueList);
			return;
		}

		// Verify same size
		Assertions.assertEquals(expectedValueList.size(), actualValueList.size(), String.format("Expected [%s] but got [%s].", expectedValueList, actualValueList));

		// Compare all elements of sorted lists
		List<BigDecimal> expectedValueListClone = new ArrayList<>(expectedValueList);
		List<BigDecimal> actualValueListClone = new ArrayList<>(actualValueList);
		expectedValueListClone.sort(BigDecimal::compareTo);
		actualValueListClone.sort(BigDecimal::compareTo);
		Assertions.assertEquals(expectedValueListClone, actualValueListClone);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<TestNumberBean> getTestNumberBeanListForValues(BigDecimal[] values) {
		if (values == null) {
			return null;
		}

		if (values.length == 0) {
			return new ArrayList<>();
		}

		List<TestNumberBean> list = new ArrayList<>();
		for (BigDecimal value : values) {
			list.add(new TestNumberBean(value));
		}
		return list;
	}


	public class TestNumberBean extends BaseSimpleEntity<Integer> {

		private BigDecimal bigDecimalField;


		public TestNumberBean(BigDecimal bigDecimal) {
			super();
			this.bigDecimalField = bigDecimal;
		}


		public BigDecimal getBigDecimalField() {
			return this.bigDecimalField;
		}


		public void setBigDecimalField(BigDecimal bigDecimalField) {
			this.bigDecimalField = bigDecimalField;
		}
	}
}
