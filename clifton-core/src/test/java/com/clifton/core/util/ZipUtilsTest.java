package com.clifton.core.util;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.dataaccess.file.container.FileContainerNative;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;
import java.util.zip.GZIPOutputStream;


public class ZipUtilsTest {

	private static final List<ClassPathResource> TestFiles = Arrays.asList(
			new ClassPathResource("com/clifton/core/util/1.pdf"),
			new ClassPathResource("com/clifton/core/util/2.pdf"),
			new ClassPathResource("com/clifton/core/util/3.pdf")
	);

	public static final Random Random = new Random();

	@TempDir
	public File temporaryFolder;


	@Test
	public void testZipUnzip() throws IOException {
		Assertions.assertTrue(this.temporaryFolder.exists());
		Assertions.assertNotNull(this.temporaryFolder.list());
		Assertions.assertEquals(Integer.valueOf(0), Optional.ofNullable(this.temporaryFolder.list()).map(s -> s.length).orElse(-1));
		List<FileWrapper> fileWrappers = TestFiles.stream()
				.map(r -> {
					try {
						return r.getFile();
					}
					catch (IOException e) {
						throw new RuntimeException(e);
					}
				})
				.map(File::getAbsolutePath)
				.map(FileContainerNative::new)
				.map(FileContainer.class::cast)
				.map(c -> new FileWrapper(FilePath.forPath(c.getPath()), c.getName(), false))
				.collect(Collectors.toList());
		String zippedFilePath = FileUtils.combinePath(this.temporaryFolder.getAbsolutePath(), Integer.toString(Random.nextInt()));
		FileContainer zippedFile = ZipUtils.zipFiles(fileWrappers, zippedFilePath);
		Assertions.assertNotNull(zippedFile);
		List<File> files = Arrays.stream(Optional.ofNullable(this.temporaryFolder).map(File::listFiles).orElse(new File[0])).collect(Collectors.toList());
		Assertions.assertEquals(1, files.size());
		File unzippedFolder = createTemporaryFolderIn(this.temporaryFolder);
		Assertions.assertNotNull(unzippedFolder);
		Assertions.assertEquals(Integer.valueOf(0), Optional.ofNullable(unzippedFolder.list()).map(s -> s.length).orElse(-1));
		List<FileWrapper> unzippedFiles = ZipUtils.unZipFiles(files.get(0), unzippedFolder.getAbsolutePath());
		Assertions.assertEquals(3, unzippedFiles.size());
		Map<String, byte[]> unzippedFileMap = unzippedFiles.stream()
				.map(FileWrapper::getFile)
				.map(FilePath::toFileContainer)
				.collect(Collectors.toMap(FileContainer::getName, c -> {
					try {
						return c.readBytes();
					}
					catch (IOException e) {
						e.printStackTrace();
					}
					return new byte[0];
				}));
		Map<String, byte[]> originalFileMap = fileWrappers.stream()
				.map(FileWrapper::getFile)
				.map(FilePath::toFileContainer)
				.collect(Collectors.toMap(FileContainer::getName, c -> {
					try {
						return c.readBytes();
					}
					catch (IOException e) {
						e.printStackTrace();
					}
					return new byte[0];
				}));
		Assertions.assertTrue(unzippedFileMap.keySet().containsAll(originalFileMap.keySet()));
		Assertions.assertTrue(originalFileMap.keySet().containsAll(unzippedFileMap.keySet()));
		for (Map.Entry<String, byte[]> entry : unzippedFileMap.entrySet()) {
			byte[] original = originalFileMap.get(entry.getKey());
			Assertions.assertArrayEquals(original, entry.getValue(), entry.getKey());
		}
		for (FileWrapper w : unzippedFiles) {
			FileContainer c = w.getFile().toFileContainer();
			c.deleteFile();
		}
	}


	@Test
	public void testGZipGUnzip() throws IOException {
		Assertions.assertTrue(this.temporaryFolder.exists());
		Assertions.assertNotNull(this.temporaryFolder.list());
		Assertions.assertEquals(Integer.valueOf(0), Optional.ofNullable(this.temporaryFolder.list()).map(s -> s.length).orElse(-1));
		File gzipFile = new File(this.temporaryFolder, Integer.toString(Random.nextInt()));
		gzip(TestFiles.get(0).getFile(), gzipFile);

		List<File> fileListing = Optional.ofNullable(this.temporaryFolder.listFiles()).map(Arrays::asList).orElse(Collections.emptyList());
		Assertions.assertFalse(fileListing.isEmpty());
		Assertions.assertTrue(Files.size(fileListing.get(0).toPath()) > 0);

		File subFolder = createTemporaryFolderIn(this.temporaryFolder);
		FileWrapper unGzippedFile = ZipUtils.unGzipFile(fileListing.get(0), subFolder.getAbsolutePath(), Integer.toString(Random.nextInt()));
		Assertions.assertNotNull(unGzippedFile);

		List<File> files = Arrays.stream(Optional.of(subFolder).map(File::listFiles).orElse(new File[0])).collect(Collectors.toList());
		Assertions.assertEquals(1, files.size());

		byte[] originalFile = FileContainerFactory.getFileContainer(TestFiles.get(0).getFile()).readBytes();
		byte[] convertedFile = FileContainerFactory.getFileContainer(files.get(0)).readBytes();

		Assertions.assertArrayEquals(originalFile, convertedFile);

		files.get(0).delete();
	}


	private void gzip(File input, File output) throws IOException {
		byte[] buffer = new byte[1024];
		try (
				FileOutputStream fileOutputStream = new FileOutputStream(output);
				GZIPOutputStream gzipOutputStream = new GZIPOutputStream(fileOutputStream);
				FileInputStream inputStream = new FileInputStream(input)
		) {
			int totalSize;
			while ((totalSize = inputStream.read(buffer)) > 0) {
				gzipOutputStream.write(buffer, 0, totalSize);
			}
			gzipOutputStream.finish();
		}
	}


	private File createTemporaryFolderIn(File parentFolder) throws IOException {
		File createdFolder = File.createTempFile("junit", "", parentFolder);
		createdFolder.delete();
		createdFolder.mkdir();
		return createdFolder;
	}
}
