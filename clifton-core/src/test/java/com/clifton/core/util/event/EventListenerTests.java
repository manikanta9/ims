package com.clifton.core.util.event;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
@SuppressWarnings("rawtypes")
public class EventListenerTests {

	@Resource
	private EventListener firstEventListener;

	@Resource
	private EventListener secondEventListener;

	@Resource
	private EventListenerRegistrator eventListenerRegistrator;


	/////////////////////////////////////////////
	/////   EventListenerRegistratorTests   /////
	/////////////////////////////////////////////


	@Test
	public void testBeansRegisteredFromContext() {

		// Assert that listeners from context have been registered by the registrator
		Assertions.assertTrue(isRegistered("TestEvent", this.eventListenerRegistrator.getEventHandler(), this.firstEventListener, this.secondEventListener));
	}


	/////////////////////////////////////////////
	/////      EventHandlerImplTests        /////
	/////////////////////////////////////////////


	@Test
	public void testRegisterEventListener() {
		EventListener eventListener = new EventListenerForTesting("TestEvent");

		EventHandler eventHandler = new EventHandlerImpl();
		eventHandler.registerEventListener(eventListener);

		Assertions.assertTrue(isRegistered("TestEvent", eventHandler, eventListener));
	}


	@Test
	public void testRegisterEventListenerExistingEventName() {
		// create two listeners
		EventListener firstListener = new EventListenerForTesting("TestEvent");
		EventListener secondListener = new EventListenerForTesting("TestEvent");

		// register event listeners
		EventHandler eventHandler = new EventHandlerImpl();
		eventHandler.registerEventListener(firstListener);
		eventHandler.registerEventListener(secondListener);

		// Assert that both listeners have been registered for the same eventName
		Assertions.assertTrue(isRegistered("TestEvent", eventHandler, firstListener, secondListener));
	}


	@Test
	public void testRegisterEventListenerNewEventName() {
		// Create 2 listeners with for 2 separate events
		EventListener eventListener = new EventListenerForTesting("TestEvent");
		EventListener newEventListener = new EventListenerForTesting("NewTestEvent");

		//register both listeners
		EventHandler handler = new EventHandlerImpl();
		handler.registerEventListener(eventListener);
		handler.registerEventListener(newEventListener);

		// Assert that each listener is registered for the correct eventName
		Assertions.assertTrue(isRegistered("TestEvent", handler, eventListener));
		Assertions.assertTrue(isRegistered("NewTestEvent", handler, newEventListener));
	}


	@Test
	public void testRegisterEventListenerDuplicateListener() {
		// Create listener and handler
		EventListener eventListener = new EventListenerForTesting("TestEvent");
		EventHandler handler = new EventHandlerImpl();

		// Assert successful registration
		Assertions.assertTrue(handler.registerEventListener(eventListener));

		// Assert unsuccessful registration
		Assertions.assertFalse(handler.registerEventListener(eventListener));
	}


	@Test
	public void testRegisterEventListenerExistingAndNewEventName() {
		// create 2 listeners, second listens to multiple events one of which overlaps with the first listener
		EventListener firstListener = new EventListenerForTesting("TestEvent1");
		EventListener secondListener = new EventListenerForTesting(CollectionUtils.createList("TestEvent1", "TestEvent2", "TestEvent3"));

		// register event listeners
		EventHandler eventHandler = new EventHandlerImpl();
		eventHandler.registerEventListener(firstListener);
		eventHandler.registerEventListener(secondListener);

		// Assert that both listeners have been registered for the same eventName
		Assertions.assertTrue(isRegistered("TestEvent1", eventHandler, firstListener, secondListener));

		// And second listener is also registered for the second and third eventNames
		Assertions.assertTrue(isRegistered("TestEvent2", eventHandler, secondListener));
		Assertions.assertTrue(isRegistered("TestEvent3", eventHandler, secondListener));
	}


	@Test
	public void testRegisterEventListenerFailNullListener() {

		EventHandler handler = new EventHandlerImpl();

		// Test expected exception and message
		boolean exceptionThrown = false;
		boolean correctMessage = false;

		try {
			handler.registerEventListener(null);
		}
		catch (RuntimeException e) {
			exceptionThrown = true;
			correctMessage = "EventHandler failed to register EventListener:  EventListener is null".equals(e.getMessage());
		}
		Assertions.assertTrue(exceptionThrown);
		Assertions.assertTrue(correctMessage);
	}


	@Test
	public void testRaiseEvent() {
		// Get listeners from context
		EventListenerForTesting firstListener = new EventListenerForTesting("TestEvent");
		EventListenerForTesting secondListener = new EventListenerForTesting("TestEvent");
		EventListenerForTesting thirdListener = new EventListenerForTesting("UnrelatedEvent");
		EventListenerForTesting fourthListener = new EventListenerForTesting("UnrelatedEvent");

		//Ensure that messagePassedByEvent is null
		Assertions.assertNull(firstListener.getMessagePassedByEvent());
		Assertions.assertNull(secondListener.getMessagePassedByEvent());
		Assertions.assertNull(thirdListener.getMessagePassedByEvent());
		Assertions.assertNull(fourthListener.getMessagePassedByEvent());

		EventHandler handler = new EventHandlerImpl();
		handler.registerEventListener(firstListener);
		handler.registerEventListener(secondListener);
		handler.registerEventListener(thirdListener);
		handler.registerEventListener(fourthListener);

		Event<Object, Object> event = EventObject.ofEvent("TestEvent");
		event.addContextValue("passedMessage", "Message passed successfully");
		handler.raiseEvent(event);

		// assert message passed successfully
		Assertions.assertEquals(firstListener.getMessagePassedByEvent(), "Message passed successfully");
		Assertions.assertEquals(secondListener.getMessagePassedByEvent(), "Message passed successfully");

		// assert message not passed on unrelated event listeners
		Assertions.assertNull(thirdListener.getMessagePassedByEvent());
		Assertions.assertNull(fourthListener.getMessagePassedByEvent());
	}


	@Test
	public void testRaiseEventOrderOfListenerNotification() {
		EventHandlerImpl handler = new EventHandlerImpl();

		//Create event listeners with different "orders" and register them
		EventListenerForTesting listenerWithOrderTwo = new EventListenerForTesting("CheckOrder");
		listenerWithOrderTwo.setOrder(2);
		handler.registerEventListener(listenerWithOrderTwo);

		EventListenerForTesting listenerWithOrderOne = new EventListenerForTesting("CheckOrder");
		listenerWithOrderOne.setOrder(1);
		handler.registerEventListener(listenerWithOrderOne);

		EventListenerForTesting listenerWithOrderThree = new EventListenerForTesting("CheckOrder");
		listenerWithOrderThree.setOrder(3);
		handler.registerEventListener(listenerWithOrderThree);

		// create event and raise it with the Event handler
		EventObject<Object, Object> event = new EventObject<>("CheckOrder");
		handler.raiseEvent(event);

		// check sequence of listeners recorded on the EventObject (see EventListenerForTesting.onEvent for details)
		Assertions.assertEquals("1 2 3", event.getContextValue("listenerSequence"));
	}


	@Test
	public void testRaiseEventFailNullEvent() {
		EventHandlerImpl handler = new EventHandlerImpl();

		// Create and register an event
		EventListenerForTesting listener = new EventListenerForTesting("TestEvent");
		handler.registerEventListener(listener);

		// Test expected exception and message
		boolean expectedException = false;
		boolean expectedMessage = false;

		try {
			handler.raiseEvent(null);
		}
		catch (RuntimeException e) {
			expectedException = true;
			expectedMessage = "EventHandler failed to raise event:  Event is null".equals(e.getMessage());
		}
		Assertions.assertTrue(expectedException);
		Assertions.assertTrue(expectedMessage);
	}


	@Test
	public void testRaiseEventFailNullEventName() {
		EventHandlerImpl handler = new EventHandlerImpl();

		// Create and register an event
		EventListenerForTesting listener = new EventListenerForTesting("TestEvent");
		handler.registerEventListener(listener);

		// Create an Event with a null eventName
		Event<Object, Object> event = new EventObject<>(null);

		// Test expected exception and message
		boolean expectedException = false;
		boolean expectedMessage = false;

		try {
			handler.raiseEvent(event);
		}
		catch (RuntimeException e) {
			expectedException = true;
			expectedMessage = "EventHandler failed to raise event:  Event name is null".equals(e.getMessage());
		}
		Assertions.assertTrue(expectedException);
		Assertions.assertTrue(expectedMessage);
	}


	/////////////////////////////////////////////
	/////         EventObjectTests          /////
	/////////////////////////////////////////////


	@Test
	public void testAddRemoveContextValue() {
		Event<Object, Object> event = new EventObject<>();
		event.addContextValue("TestKey", "TestValue");

		Assertions.assertEquals("TestValue", event.getContextValue("TestKey"));
	}


	@Test
	public void testGetContextValueNullContext() {
		Event<Object, Object> event = new EventObject<>();

		// No exception should be thrown, should return null
		Assertions.assertNull(event.getContextValue("TestKey"));
	}


	@Test
	public void testAddContextValueFailNullKey() {
		EventObject<Object, Object> event = new EventObject<>("TestEvent");

		boolean expectedException = false;
		boolean expectedMessage = false;

		try {
			event.addContextValue(null, "testValue");
		}
		catch (RuntimeException e) {
			expectedException = true;
			expectedMessage = "Cannot insert null key into event context".equals(e.getMessage());
		}
		Assertions.assertTrue(expectedException);
		Assertions.assertTrue(expectedMessage);
	}


	@Test
	public void testRemoveContextValue() {
		Event<Object, Object> event = new EventObject<>();
		event.addContextValue("TestKey", "TestValue");

		Assertions.assertEquals("TestValue", event.removeContextValue("TestKey"));

		// should TestKey mapping should no longer be present
		Assertions.assertNull(event.getContextValue("TestKey"));
	}


	@Test
	public void testRemoveContextValueNullContext() {
		Event<Object, Object> event = new EventObject<>();

		// No exception should be thrown, should return null
		Assertions.assertNull(event.removeContextValue("TestKey"));
	}


	// helper method for testing registration of listeners
	private boolean isRegistered(String eventName, EventHandler handler, EventListener... listeners) {

		// Create new event with the passed-in eventName
		Event<Object, Object> event = new EventObject<>(eventName);

		// add message to the eventContext
		event.addContextValue("passedMessage", "registration successful: " + eventName);

		// raise the event
		handler.raiseEvent(event);

		// for each listener, check that the message has been passed
		for (EventListener listener : listeners) {
			String messagePassedByEvent = ((EventListenerForTesting) listener).getMessagePassedByEvent();

			if (!StringUtils.isEqual(messagePassedByEvent, "registration successful: " + eventName)) {
				return false;
			}
		}
		return true;
	}
}
