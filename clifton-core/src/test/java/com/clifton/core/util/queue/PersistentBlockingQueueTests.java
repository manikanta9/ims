package com.clifton.core.util.queue;

import com.clifton.core.dataaccess.file.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class PersistentBlockingQueueTests {

	@Test
	public void testLoading() {
		try {
			PersistentBlockingQueue<QueueTestBean> queue = new PersistentBlockingQueue<>();
			queue.setFileNameAndPath("queue.tst");

			QueueTestBean bean1 = new QueueTestBean(10);
			queue.add(bean1);

			QueueTestBean bean2 = new QueueTestBean(15, bean1);
			queue.add(bean2);

			Assertions.assertEquals(2, queue.getElementList().size());

			PersistentBlockingQueue<QueueTestBean> queueLoaded = new PersistentBlockingQueue<>();
			queueLoaded.setFileNameAndPath("queue.tst");

			Assertions.assertEquals(2, queueLoaded.getElementList().size());

			QueueTestBean beanLoaded = queueLoaded.take();
			Assertions.assertEquals(10, beanLoaded.getId());

			beanLoaded = queueLoaded.take();
			Assertions.assertEquals(15, beanLoaded.getId());
			Assertions.assertEquals(10, beanLoaded.getParent().getId());

			// take on out of the original queue and verify that the when reloaded only bean 2 is left
			bean1 = queue.take();
			queueLoaded = new PersistentBlockingQueue<>();
			queueLoaded.setFileNameAndPath("queue.tst");

			Assertions.assertEquals(1, queueLoaded.getElementList().size());

			beanLoaded = queueLoaded.take();
			Assertions.assertEquals(15, beanLoaded.getId());
			Assertions.assertEquals(10, beanLoaded.getParent().getId());
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
		finally {
			FileUtils.delete(new File("queue.tst"));
		}
	}


	@Test
	public void testDrain() {
		try {
			PersistentBlockingQueue<QueueTestBean> queue = new PersistentBlockingQueue<>();
			queue.setFileNameAndPath("queue.tst");

			QueueTestBean bean1 = new QueueTestBean(10);
			queue.add(bean1);

			QueueTestBean bean2 = new QueueTestBean(15, bean1);
			queue.add(bean2);

			Assertions.assertEquals(2, queue.getElementList().size());

			List<QueueTestBean> beanList = new ArrayList<>();
			int count = queue.drainTo(beanList);

			Assertions.assertEquals(2, count);
			Assertions.assertEquals(2, beanList.size());

			Assertions.assertEquals(10, beanList.get(0).getId());

			Assertions.assertEquals(15, beanList.get(1).getId());
			Assertions.assertEquals(10, beanList.get(1).getParent().getId());

			PersistentBlockingQueue<QueueTestBean> queueLoaded = new PersistentBlockingQueue<>();
			queueLoaded.setFileNameAndPath("queue.tst");

			Assertions.assertEquals(0, queue.getElementList().size());
		}
		catch (Throwable e) {
			throw new RuntimeException(e);
		}
		finally {
			FileUtils.delete(new File("queue.tst"));
		}
	}
}
