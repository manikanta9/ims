package com.clifton.core.util;


import com.clifton.core.dataaccess.file.FileUtils;
import org.junit.jupiter.api.Test;

import java.io.File;


public class PGPUtilsTests {

	@Test
	public void testPGPEncryption() {
		String privateKey = FileUtils.readFileToString("src\\test\\java\\com\\clifton\\core\\util\\test_pgp_private.asc");
		String publicKey = FileUtils.readFileToString("src\\test\\java\\com\\clifton\\core\\util\\test_pgp_public.asc");
		String testPath = FileUtils.normalizeFilePathSeparators("src\\test\\java\\com\\clifton\\core\\util\\");
		try {
			PGPFileUtils.encrypt(testPath + "20141231.IRSTR_CLF_EOD.csv", testPath + "20141231.IRSTR_CLF_EOD.csv.pgp", publicKey);
			PGPFileUtils.decrypt(testPath + "20141231.IRSTR_CLF_EOD.csv.pgp", testPath + "20141231.IRSTR_CLF_EOD.done.csv", privateKey, "test_pgp");
		}
		finally {
			File encrypted = new File(testPath + "20141231.IRSTR_CLF_EOD.csv.pgp");
			if (encrypted.exists()) {
				FileUtils.delete(encrypted);
			}
			File decrypted = new File(testPath + "20141231.IRSTR_CLF_EOD.done.csv");
			if (decrypted.exists()) {
				FileUtils.delete(decrypted);
			}
		}
	}
}
