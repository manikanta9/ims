package com.clifton.core.util.token;

import com.clifton.core.util.StringUtils;
import com.clifton.core.web.token.JsonWebToken;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


/**
 * @author theodorez
 */
public class JsonWebTokenUtilsTests {

	private static final String TEST_TOKEN = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzY29wZSI6WyJBQ1RfQVNfVVNFUiJdLCJleHAiOjk5OTk5OTk5OTk5OSwiYXV0aG9yaXRpZXMiOlsiSVNfQVVUSEVOVElDQVRFRF9GVUxMWSJdLCJqdGkiOiIzZDVjYThhMS00NzgyLTQ2ZTUtYTRjNS1mZjFjNzQwYThlNzQiLCJjbGllbnRfaWQiOiJpbXMiLCJ1c2VyX25hbWUiOiJ0endpZWdAcGFyYXBvcnQuY29tIn0.JRifB_4WA145nMhUq5abkCIsqKd2O1oNr26WhM1Zjq0";
	private static final String USER_NAME = "tzwieg@paraport.com";
	private static final String ALGORITHM_NAME = "HMAC256";
	private static final String SIGNATURE_KEY = "secret";


	@Test
	public void testGetAlgorithmForKey() {
		Object algorithm = JsonWebTokenUtils.getAlgorithmForKey("1234567890", "HMAC256");
		Assertions.assertNotNull(algorithm);
	}


	@Test
	public void testDecodeJsonWebTokenWithValidation() {
		JsonWebToken decodedToken = JsonWebTokenUtils.decodeJsonWebTokenWithValidation(TEST_TOKEN, SIGNATURE_KEY, ALGORITHM_NAME);
		Assertions.assertNotNull(decodedToken);
	}


	@Test
	public void testGetClaimAsString() {
		JsonWebToken decodedToken = JsonWebTokenUtils.decodeJsonWebTokenWithValidation(TEST_TOKEN, SIGNATURE_KEY, ALGORITHM_NAME);
		String username = JsonWebTokenUtils.getClaimAsString("user_name", decodedToken);
		Assertions.assertTrue(StringUtils.isEqual(username, USER_NAME));
	}


	@Test
	public void testGetClaimAsStringList() {
		JsonWebToken decodedToken = JsonWebTokenUtils.decodeJsonWebTokenWithValidation(TEST_TOKEN, SIGNATURE_KEY, ALGORITHM_NAME);
		List<String> authorities = JsonWebTokenUtils.getClaimAsStringList("authorities", decodedToken);
		Assertions.assertNotNull(authorities);
	}
}
