package com.clifton.core.util;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;


/**
 * The test class for {@link RollingList}.
 */
public class RollingListTests {

	@Test
	public void testRollingListCapacity() {
		RollingList<Integer> list = new RollingList<>(3);
		assertListState(list);

		list.add(1);
		assertListState(list, 1);

		list.add(2);
		assertListState(list, 1, 2);

		list.add(3);
		assertListState(list, 1, 2, 3);

		list.add(4);
		assertListState(list, 2, 3, 4);

		list.add(5);
		assertListState(list, 3, 4, 5);

		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.remove(3));
		assertListState(list, 3, 4, 5);

		list.remove(1);
		assertListState(list, 3, 5);

		list.remove(1);
		assertListState(list, 3);

		list.remove(0);
		assertListState(list);

		Assertions.assertThrows(IndexOutOfBoundsException.class, () -> list.remove(0));
		assertListState(list);
	}


	@SuppressWarnings("ArraysAsListWithZeroOrOneArgument")
	@Test
	public void testRollingListCollectionOps() {
		RollingList<Integer> list = new RollingList<>(3);
		assertListState(list);

		list.addAll(Arrays.asList());
		assertListState(list);

		list.addAll(Arrays.asList(1));
		assertListState(list, 1);

		list.addAll(Arrays.asList(2, 3));
		assertListState(list, 1, 2, 3);

		list.addAll(Arrays.asList(4, 5));
		assertListState(list, 3, 4, 5);

		list.removeAll(Arrays.asList(1, 2));
		assertListState(list, 3, 4, 5);

		list.removeAll(Arrays.asList(3, 5));
		assertListState(list, 4);

		list.removeAll(Arrays.asList(3, 4, 5));
		assertListState(list);

		list.addAll(Arrays.asList(1, 2, 3, 4, 5));
		assertListState(list, 3, 4, 5);

		list.removeIf(value -> value >= 4);
		assertListState(list, 3);

		list.addAll(Arrays.asList(6, 7, 8, 9, 10));
		assertListState(list, 8, 9, 10);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SafeVarargs
	private final <T> void assertListState(RollingList<T> list, T... elements) {
		List<T> compareList = Arrays.asList(elements);
		Assertions.assertEquals(compareList.size(), list.size());
		Assertions.assertEquals(compareList, list);
		Assertions.assertEquals(compareList, list.getList());
	}
}
