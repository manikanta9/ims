package com.clifton.core.util.encryption;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.nio.charset.StandardCharsets;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;


public class EncryptionUtilsAESTests {

	@Test
	public void testJavaCryptographyExtensionInstalled() throws NoSuchAlgorithmException {
		Assertions.assertFalse(EncryptionUtilsAES.isRestrictedCryptography());
	}


	@Test
	public void testWriteAndReadFile() {
		String fileLocation = "file.key";
		byte[] key = EncryptionUtilsAES.generateKey();

		EncryptionUtilsAES.writeKeyToFile(key, fileLocation);
		Assertions.assertTrue(new File(fileLocation).exists());

		byte[] keyFromFile = EncryptionUtilsAES.readAndOrCreateKeyFile(fileLocation, null);
		Assertions.assertTrue(StringUtils.isEqual(Base64.getEncoder().encodeToString(key), Base64.getEncoder().encodeToString(keyFromFile)));

		FileUtils.delete(new File(fileLocation));
	}


	@Test
	public void testEncryptAndDecrypt() {
		String plainText = "This is some test data";
		byte[] data = plainText.getBytes(StandardCharsets.UTF_8);
		byte[] key = EncryptionUtilsAES.generateKey();
		byte[] iv = EncryptionUtilsAES.generateInitializationVector();

		byte[] ciphertext = EncryptionUtilsAES.encrypt(data, key, iv);
		Assertions.assertNotNull(ciphertext);

		byte[] decryptedCipherText = EncryptionUtilsAES.decrypt(ciphertext, key, iv);
		String decryptedPlainText = new String(decryptedCipherText, StandardCharsets.UTF_8);

		Assertions.assertTrue(StringUtils.isEqual(plainText, decryptedPlainText));
	}


	@Test
	public void testCleanupByteArray() {
		byte[] iv = EncryptionUtilsAES.generateInitializationVector();
		EncryptionUtils.cleanupByteArray(iv);
		for (byte value : iv) {
			Assertions.assertEquals(value, 0);
		}
	}


	@Test
	public void testGenerateKey() {
		byte[] key = EncryptionUtilsAES.generateKey();
		Assertions.assertTrue(key != null && key.length > 0);
	}


	@Test
	public void testGenerateInitializationVector() {
		byte[] iv = EncryptionUtilsAES.generateInitializationVector();
		Assertions.assertTrue(iv != null && iv.length > 0);
	}
}
