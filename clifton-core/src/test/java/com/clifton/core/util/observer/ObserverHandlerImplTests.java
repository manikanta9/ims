package com.clifton.core.util.observer;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>ObserverHandlerImplTests</code> class defines tests for {@link ObserverHandlerImpl}
 *
 * @author vgomelsky
 */
public class ObserverHandlerImplTests {

	@Test
	public void testGetNotifier() {
		ObserverHandler handler = new ObserverHandlerImpl();
		StringBuilder result = new StringBuilder("0");

		handler.getNotifier(ObservedEvent.class, o -> true).afterUpdate(result);
		Assertions.assertEquals("0", result.toString());

		handler.register(ObservedEvent.class, new ObservedEventImpl("1"));
		handler.getNotifier(ObservedEvent.class, o -> true).afterUpdate(result);
		Assertions.assertEquals("01", result.toString());

		handler.register(ObservedEvent.class, new ObservedEventImpl("2"));
		handler.getNotifier(ObservedEvent.class, o -> true).afterUpdate(result);
		Assertions.assertEquals("0112", result.toString());

		handler.register(ObservedEvent.class, new ObservedEventImpl("3"));
		handler.getNotifier(ObservedEvent.class, o -> true).afterUpdate(result);
		Assertions.assertEquals("0112123", result.toString());
	}
}
