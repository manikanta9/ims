package com.clifton.core.util;

import com.clifton.core.dataaccess.file.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.io.File;


/**
 * @author mwacker
 */
public class SecurityUtilsTests {

	@BeforeAll
	public static void setup() {
		/*
		 * By default, the termination of a concurrent JVM may attempt to delete the Apache POI-specific temporary directory, even if it is in use by another JVM. To address this, we
		 * disable deletion hooks for Apache POI temporary files (Apache POI temporary directory: org.apache.poi.util.DefaultTempFileCreationStrategy#POIFILES).
		 */
		System.setProperty("poi.keep.tmp.files", Boolean.TRUE.toString());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testPasswordProtectExcel_InOriginalFormat() {
		File originalTestFile = new File("src/test/java/com/clifton/core/util/SecurityUtilsTests_TestFile.xlsx");
		File testFileOut = new File("src/test/java/com/clifton/core/util/SecurityUtilsTests_TestFileOut.xlsx");
		runTestPasswordProtectExcel(originalTestFile, testFileOut);
	}


	@Test
	public void testPasswordProtectExcel() {
		File originalTestFile = new File("src/test/java/com/clifton/core/util/SecurityUtilsTests_TestFile2.xlsx");
		File testFileOut = new File("src/test/java/com/clifton/core/util/SecurityUtilsTests_TestFileOut2.xlsx");
		runTestPasswordProtectExcel(originalTestFile, testFileOut);
	}


	@Test
	public void testPasswordProtectExcelDefault() {
		File originalTestFile = new File("src/test/java/com/clifton/core/util/SecurityUtilsTests_TestFile3.xlsx");
		File testFileOut = new File("src/test/java/com/clifton/core/util/SecurityUtilsTests_TestFileOut3.xlsx");
		runTestPasswordProtectExcel(originalTestFile, testFileOut);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void runTestPasswordProtectExcel(File originalTestFile, File testFileOut) {
		try {
			SecurityUtils.secureFile(originalTestFile.getAbsolutePath(), testFileOut.getAbsolutePath(), "DOES_IT_WORK?");
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to encrypt excel file.", e);
		}
		finally {
			if (testFileOut.exists()) {
				FileUtils.delete(testFileOut);
			}
		}
	}
}
