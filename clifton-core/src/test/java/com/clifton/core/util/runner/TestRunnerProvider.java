package com.clifton.core.util.runner;


import com.clifton.core.util.date.DateUtils;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class TestRunnerProvider implements RunnerProvider<T> {

	private final TestObject testObject = new TestObject();
	private Date lastStartTime;


	@Override
	public List<Runner> getOccurrencesBetween(Date startDateTime, @SuppressWarnings("unused") Date endDateTime) {
		List<Runner> result = new ArrayList<>();
		result.add(new TestRunner("TEST", "1", startDateTime, this.testObject));
		result.add(new TestRunner("TEST", "2", DateUtils.addSeconds(startDateTime, 3), this.testObject));
		result.add(new TestRunner("TEST", "3", DateUtils.addSeconds(startDateTime, 6), this.testObject));
		result.add(new TestRunner("TEST", "4", DateUtils.addSeconds(startDateTime, 9), this.testObject));
		result.add(new TestRunner("TEST", "4", DateUtils.addMilliseconds(startDateTime, 9001), this.testObject));
		result.add(new TestRunner("TEST", "4", DateUtils.addMilliseconds(startDateTime, 9002), this.testObject));
		result.add(new TestRunner("TEST", "4", DateUtils.addMilliseconds(startDateTime, 9003), this.testObject));
		result.add(new TestRunner("TEST", "4", DateUtils.addMilliseconds(startDateTime, 9004), this.testObject));
		result.add(new TestRunner("TEST", "4", DateUtils.addMilliseconds(startDateTime, 9005), this.testObject));
		this.lastStartTime = startDateTime;
		return result;
	}


	@Override
	public Runner createRunnerForEntityAndDate(T entity, Date runnerDate) {
		return null;
	}


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(T entity, Date startDateTime, Date endDateTime) {
		return null;
	}


	public Date getLastStartTime() {
		return this.lastStartTime;
	}


	public TestObject getTestObject() {
		return this.testObject;
	}
}
