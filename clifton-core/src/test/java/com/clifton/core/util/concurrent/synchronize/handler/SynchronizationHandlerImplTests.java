package com.clifton.core.util.concurrent.synchronize.handler;

import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.concurrent.CallableAction;
import com.clifton.core.util.concurrent.ExecutableAction;
import com.clifton.core.util.concurrent.LockBusyException;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.timer.ThreadLocalTimerHandler;
import com.clifton.core.util.timer.TimerHandler;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * <code>SynchronizationHandlerImplTests</code> tests the use cases and functionality of {@link SynchronizationHandlerImpl}
 *
 * @author NickK
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SynchronizationHandlerImplTests {

	// Secure Areas
	private static final String TEST_SECURE_AREA = "Test Area";
	private static final String TEST_SECURE_AREA_TWO = "Test Area Two";
	// Lock Keys
	private static final String TEST_LOCK_KEY = "Test Key";
	private static final String TEST_LOCK_KEY_TWO = "Test Key Two";
	private static final String TEST_LOCK_KEY_THREE = "Test Key Three";
	// Lock Messages
	private static final String TEST_DEFAULT_LOCK_MESSAGE = "Default lock message";

	private static final ExecutableAction COUNT_EXECUTABLE = () -> {
		int count = 0;
		while (count < 100) {
			count++;
		}
	};
	private static final CallableAction<Integer> COUNT_CALLABLE = () -> {
		int count = 0;
		while (count < 100) {
			count++;
		}
		return count;
	};

	private static final ScheduledThreadPoolExecutor SCHEDULED_THREAD_POOL_EXECUTOR = new ScheduledThreadPoolExecutor(5);

	@Resource
	private SynchronizationHandler synchronizationHandler;

	private TimerHandler timerHandler = new ThreadLocalTimerHandler();


	@AfterAll
	public static void shutdownExecutor() {
		ConcurrentUtils.shutdownExecutorService(SCHEDULED_THREAD_POOL_EXECUTOR);
	}

	///////////////////////////////////////////////////////////////////////////
	////////////         Scope and Lock Key Tests                //////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void obtainLockForSameKeyAndDifferentScopeTest() {
		LockHoldingAction lockHoldingAction1 = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY),
				lockHoldingAction2 = submitControllableLockingAction(TEST_SECURE_AREA_TWO, TEST_LOCK_KEY);
		try {
			try {
				this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Scope 1 lock attempt", TEST_LOCK_KEY)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
				failExpectedLockBusyException();
			}
			catch (LockBusyException e) {
				validateLockBusyException(e, TEST_SECURE_AREA, TEST_LOCK_KEY, TEST_DEFAULT_LOCK_MESSAGE);
			}
			try {
				this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA_TWO, "Scope 2 lock attempt", TEST_LOCK_KEY)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
				failExpectedLockBusyException();
			}
			catch (LockBusyException e) {
				validateLockBusyException(e, TEST_SECURE_AREA_TWO, TEST_LOCK_KEY, TEST_DEFAULT_LOCK_MESSAGE);
			}
		}
		finally {
			lockHoldingAction1.release();
			lockHoldingAction2.release();
		}
	}


	@Test
	public void obtainLockForDifferentKeyAndSameScopeTest() {
		LockHoldingAction lockHoldingAction = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY);
		try {
			this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Scope 1 lock attempt different key", TEST_LOCK_KEY_TWO)
					.buildWithExecutableAction(COUNT_EXECUTABLE));
		}
		catch (LockBusyException e) {
			failDidNotExpectLockBusyException();
		}
		finally {
			lockHoldingAction.release();
		}
	}


	@Test
	public void obtainLockForDifferentKeysAndDifferentScopesTest() {
		LockHoldingAction lockHoldingAction1 = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY),
				lockHoldingAction2 = submitControllableLockingAction(TEST_SECURE_AREA_TWO, TEST_LOCK_KEY_TWO);
		try {
			try {
				this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Scope 1 lock attempt second key", TEST_LOCK_KEY_TWO)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
			}
			catch (LockBusyException e) {
				failDidNotExpectLockBusyException();
			}
			try {
				this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA_TWO, "Scope 2 lock attempt for different key", TEST_LOCK_KEY)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
			}
			catch (LockBusyException e) {
				failDidNotExpectLockBusyException();
			}
		}
		finally {
			lockHoldingAction1.release();
			lockHoldingAction2.release();
		}
	}


	@Test
	public void reentrantTest() {
		CountDownLatch initializingLatch = new CountDownLatch(1),
				finalizingLatch = new CountDownLatch(1);


		try {
			Future<Integer> actionResult = SCHEDULED_THREAD_POOL_EXECUTOR.submit(() -> {
				CallableAction<Integer> action = () -> {
					// obtain and release lock, should still own the lock due to reentry
					Integer count = this.synchronizationHandler.call(
							SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, TEST_LOCK_KEY)
									.buildWithCallableAction(COUNT_CALLABLE));
					initializingLatch.countDown();
					awaitCountDownLatch(finalizingLatch);
					return count;
				};
				return this.synchronizationHandler.call(
						SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, TEST_LOCK_KEY)
								.buildWithCallableAction(action));
			});

			// wait for the spawned thread to obtain the lock
			awaitCountDownLatch(initializingLatch);
			//try to obtain the lock - should fail
			try {
				this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Scope 1 lock attempt", TEST_LOCK_KEY)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
				failExpectedLockBusyException();
			}
			catch (LockBusyException e) {
				validateLockBusyException(e, TEST_SECURE_AREA, TEST_LOCK_KEY, TEST_DEFAULT_LOCK_MESSAGE);
			}

			finalizingLatch.countDown();
			try {
				Assertions.assertEquals(100, actionResult.get(5, TimeUnit.SECONDS).intValue());
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			catch (Exception e) {
				Assertions.fail();
			}
		}
		finally {
			finalizingLatch.countDown();
		}
	}

	///////////////////////////////////////////////////////////////////////////
	////////////       Executable Action Tests                   //////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void obtainLockWithoutWaitTest() {
		this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, TEST_LOCK_KEY).buildWithExecutableAction(COUNT_EXECUTABLE));
	}


	@Test
	public void obtainLockWithoutWaitAlreadyLockedTest() {
		obtainLockAlreadyLockedNoWaitTest(TEST_SECURE_AREA, false, TEST_LOCK_KEY);
	}


	@Test
	public void obtainLockWithWaitTest() {
		this.synchronizationHandler.execute(
				SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Lock with wait", TEST_LOCK_KEY)
						.withMillisecondsToWaitIfBusy(100)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
	}


	@Test
	public void obtainLockWithWaitAlreadyLockedTest() {
		obtainLockAlreadyLockedWithWaitTest(TEST_SECURE_AREA, false, true, TEST_LOCK_KEY);
		obtainLockAlreadyLockedWithWaitTest(TEST_SECURE_AREA, false, false, TEST_LOCK_KEY);
	}


	@Test
	public void obtainLockWithWaitFifoTest() {
		obtainLockWithWaitFifoTest(false);
	}

	///////////////////////////////////////////////////////////////////////////
	////////////         Callable Action Tests                   //////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void obtainLockWithoutWaitCallableTest() {
		Assertions.assertEquals(100,
				this.synchronizationHandler.call(
						SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, TEST_LOCK_KEY)
								.buildWithCallableAction(COUNT_CALLABLE)).intValue());
	}


	@Test
	public void obtainLockWithoutWaitAlreadyLockedCallableTest() {
		obtainLockAlreadyLockedNoWaitTest(TEST_SECURE_AREA, true, TEST_LOCK_KEY);
	}


	@Test
	public void obtainLockWithWaitCallableTest() {
		Assertions.assertEquals(100,
				this.synchronizationHandler.call(
						SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Lock with wait", TEST_LOCK_KEY)
								.withMillisecondsToWaitIfBusy(100)
								.buildWithCallableAction(COUNT_CALLABLE)).intValue());
	}


	@Test
	public void obtainLockWithWaitAlreadyLockedCallableTest() {
		obtainLockAlreadyLockedWithWaitTest(TEST_SECURE_AREA, true, true, TEST_LOCK_KEY);
		obtainLockAlreadyLockedWithWaitTest(TEST_SECURE_AREA, true, false, TEST_LOCK_KEY);
	}


	@Test
	public void obtainLockWithWaitFifoCallableTest() {
		obtainLockWithWaitFifoTest(true);
	}

	///////////////////////////////////////////////////////////////////////////
	////////////         Multiple Lock Key Tests                 //////////////
	///////////////////////////////////////////////////////////////////////////


	@Test
	public void obtainLockWithMultipleKeys() {
		Set<String> lockKeySet = CollectionUtils.createHashSet(TEST_LOCK_KEY, TEST_LOCK_KEY_TWO, TEST_LOCK_KEY_THREE);
		this.synchronizationHandler.execute(
				SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, lockKeySet)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
		Assertions.assertEquals(100,
				this.synchronizationHandler.call(
						SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, lockKeySet)
								.buildWithCallableAction(COUNT_CALLABLE)).intValue());

		// no wait
		obtainLockAlreadyLockedNoWaitTest(TEST_SECURE_AREA, false, TEST_LOCK_KEY, TEST_LOCK_KEY_TWO);
		// with wait
		obtainLockAlreadyLockedWithWaitTest(TEST_SECURE_AREA, true, false, TEST_LOCK_KEY, TEST_LOCK_KEY_TWO);
		obtainLockAlreadyLockedWithWaitTest(TEST_SECURE_AREA, true, true, TEST_LOCK_KEY, TEST_LOCK_KEY_TWO);
	}


	@Test
	public void obtainLockWithMultipleKeysAllowsSingleKeyWait() {
		LockHoldingAction lockHoldingAction = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY, TEST_LOCK_KEY_TWO);
		int millisecondWait = 500;

		try {
			this.timerHandler.startTimer();
			try {
				this.synchronizationHandler.execute(
						SynchronizableBuilder.forLocking(TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE, TEST_LOCK_KEY_TWO)
								.withMillisecondsToWaitIfBusy(millisecondWait)
								.buildWithExecutableAction(COUNT_EXECUTABLE));
			}
			finally {
				this.timerHandler.stopTimer();
			}
			failExpectedLockBusyException();
		}
		catch (LockBusyException e) {
			validateLockBusyException(e, TEST_SECURE_AREA, TEST_LOCK_KEY_TWO, TEST_DEFAULT_LOCK_MESSAGE);
			Assertions.assertTrue(TimeUnit.MILLISECONDS.convert(this.timerHandler.getDurationNano(), TimeUnit.NANOSECONDS) > (millisecondWait * 3 / 4));
		}
		finally {
			lockHoldingAction.release();
		}
	}


	@Test
	public void obtainLockWithMultipleKeysUnlocksOnExceptionDuringLock() {
		LockHoldingAction lockHoldingAction = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY);

		// Locks will be obtained in order with linked set; TEST_LOCK_KEY_THREE and TEST_LOCK_KEY_TWO will be obtained and block for TEST_LOCK_KEY
		Set<String> multiLockKeySet = new LinkedHashSet<>();
		multiLockKeySet.add(TEST_LOCK_KEY_THREE);
		multiLockKeySet.add(TEST_LOCK_KEY_TWO);
		multiLockKeySet.add(TEST_LOCK_KEY);
		try {
			this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Lock attempt all keys", multiLockKeySet)
					.buildWithExecutableAction(COUNT_EXECUTABLE));
			failExpectedLockBusyException();
		}
		catch (LockBusyException e) {
			validateLockBusyException(e, TEST_SECURE_AREA, TEST_LOCK_KEY, TEST_DEFAULT_LOCK_MESSAGE);
			try {
				// lock on TEST_LOCK_KEY_THREE and TEST_LOCK_KEY_TWO should have been unlocked upon getting LockBusyException and lock for TEST_LOCK_KEY should still be locked
				this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Lock attempt lock three", TEST_LOCK_KEY_THREE)
						.buildWithExecutableAction(COUNT_EXECUTABLE));
				try {
					this.synchronizationHandler.execute(SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Lock attempt lock three", TEST_LOCK_KEY)
							.buildWithExecutableAction(COUNT_EXECUTABLE));
					failExpectedLockBusyException();
				}
				catch (LockBusyException e2) {
					validateLockBusyException(e2, TEST_SECURE_AREA, TEST_LOCK_KEY, TEST_DEFAULT_LOCK_MESSAGE);
				}
			}
			catch (LockBusyException e2) {
				failDidNotExpectLockBusyException();
			}
		}
		finally {
			lockHoldingAction.release();
		}
	}

	///////////////////////////////////////////////////////////////////////////
	////////////                Helper Methods                   //////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Schedules an {@link ExecutableAction} in another thread that obtains a lock with {@link #synchronizationHandler} for the provided
	 * arguments and blocks until the calling thread resumes it. Returns a {@link LockHoldingAction} so the calling thread can
	 * control how long the lock is held. To resume the spawned thread, the calling thread must call {@link LockHoldingAction#release()}
	 * on the returned latched. Failing to resume the spawned task will result in the spawned thread to block indefinitely holding
	 * the specified lock.
	 * <p>
	 * Example:
	 * <pre>
	 * {@code
	 * LockHoldingAction lockHoldingAction = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY);
	 * try {
	 *     // this will cause a LockBusyException
	 *     this.synchronizationHandler.execute(...);
	 * }
	 * finally {
	 *     lockHoldingAction.countdown();
	 * }
	 * }
	 * </pre>
	 *
	 * @param secureArea globally unique name for an area (block of code or database table(s)) being secured by locks
	 * @param lockKey    lock key vararg that identifies a sub-section of the specified secure area that should be secured by the lock
	 * @return a LockHoldingAction that can be used to release the blocking thread holding the lock
	 */
	private LockHoldingAction submitControllableLockingAction(String secureArea, String... lockKey) {
		CountDownLatch initializingLatch = new CountDownLatch(1), // latch to wait for submitted task to start
				finalizingLatch = new CountDownLatch(1); // latch to let the submitted task end and unlock held locks

		Future<?> result = SCHEDULED_THREAD_POOL_EXECUTOR.submit(() ->
				this.synchronizationHandler.execute(
						SynchronizableBuilder.forLocking(secureArea, TEST_DEFAULT_LOCK_MESSAGE, CollectionUtils.createHashSet(lockKey))
								.buildWithExecutableAction(() -> {
									initializingLatch.countDown();
									awaitCountDownLatch(finalizingLatch);
								})));

		// wait for the spawned thread to obtain the lock
		awaitCountDownLatch(initializingLatch);
		return new LockHoldingAction(finalizingLatch, result);
	}


	private void obtainLockAlreadyLockedNoWaitTest(String secureArea, boolean useCallableAction, String... lockKeys) {
		obtainLockAlreadyLockedTest(secureArea, 0, useCallableAction, true, lockKeys);
	}


	private void obtainLockAlreadyLockedWithWaitTest(String secureArea, boolean useCallableAction, boolean letWaitingExpire, String... lockKeys) {
		obtainLockAlreadyLockedTest(secureArea, letWaitingExpire ? 500 : 3000, useCallableAction, letWaitingExpire, lockKeys);
	}


	private void obtainLockAlreadyLockedTest(String secureArea, int millisecondsToWaitIfBusy, boolean useCallableAction, boolean letWaitingExpire, String... lockKeys) {
		LockHoldingAction lockHoldingAction = submitControllableLockingAction(secureArea, lockKeys);
		Set<String> lockKeySet = CollectionUtils.createHashSet(lockKeys);
		try {
			try {
				if (millisecondsToWaitIfBusy == 0) {
					SynchronizableBuilder actionBuilder = SynchronizableBuilder.forLocking(secureArea, "LockBusyException without wait", lockKeySet);
					if (useCallableAction) {
						this.synchronizationHandler.call(actionBuilder.buildWithCallableAction(COUNT_CALLABLE));
					}
					else {
						this.synchronizationHandler.execute(actionBuilder.buildWithExecutableAction(COUNT_EXECUTABLE));
					}
					failExpectedLockBusyException();
				}
				else {
					if (!letWaitingExpire) {
						this.timerHandler.startTimer();
						SCHEDULED_THREAD_POOL_EXECUTOR.schedule(lockHoldingAction::release, Integer.valueOf(millisecondsToWaitIfBusy / 2).longValue(), TimeUnit.MILLISECONDS);
					}

					SynchronizableBuilder actionBuilder = SynchronizableBuilder.forLocking(secureArea, "LockBusyException with wait", lockKeySet)
							.withMillisecondsToWaitIfBusy(millisecondsToWaitIfBusy);
					Long duration;
					if (useCallableAction) {
						duration = this.synchronizationHandler.call(actionBuilder.buildWithCallableAction(() -> {
							this.timerHandler.stopTimer();
							return TimeUnit.MILLISECONDS.convert(this.timerHandler.getDurationNano(), TimeUnit.NANOSECONDS);
						}));
					}
					else {
						this.synchronizationHandler.execute(actionBuilder.buildWithExecutableAction(this.timerHandler::stopTimer));
						duration = TimeUnit.MILLISECONDS.convert(this.timerHandler.getDurationNano(), TimeUnit.NANOSECONDS);
					}
					if (letWaitingExpire) {
						failExpectedLockBusyException();
					}
					int expectedWaitThreshold = millisecondsToWaitIfBusy / 3;
					Assertions.assertTrue(duration.intValue() > expectedWaitThreshold, "Expected a wait for the lock to be greater than " + expectedWaitThreshold + ", and was " + duration.intValue());
				}
			}
			catch (LockBusyException e) {
				if (!letWaitingExpire && millisecondsToWaitIfBusy > 0) {
					// the lock should have been released and obtained by the waiting thread
					failDidNotExpectLockBusyException();
				}
				validateLockBusyException(e, TEST_SECURE_AREA, TEST_DEFAULT_LOCK_MESSAGE);
			}
		}
		finally {
			lockHoldingAction.release();
		}
	}


	/**
	 * This method will submit an action that obtains a lock with {@link #submitControllableLockingAction(String, String...)}
	 * and then tries to submit subsequent actions that attempt to obtain the same lock. The additional threads will wait if necessary.
	 *
	 * @param useCallableAction true to indicate that a {@link com.clifton.core.util.concurrent.synchronize.SynchronizableCallableAction}
	 *                          should be used instead of {@link com.clifton.core.util.concurrent.synchronize.SynchronizableExecutableAction}
	 */
	private void obtainLockWithWaitFifoTest(boolean useCallableAction) {
		int millisecondsToWaitIfBusy = 2000;
		LockHoldingAction lockHoldingAction = submitControllableLockingAction(TEST_SECURE_AREA, TEST_LOCK_KEY);

		try {
			AtomicInteger counter = new AtomicInteger(0),
					executionIndex = new AtomicInteger(0);
			int threadCount = 5;
			List<Future<Integer[]>> taskFutures = new ArrayList<>();
			CountDownLatch queuedLatch = new CountDownLatch(threadCount);
			try {
				// spawn actions to test they are handled in FIFO order
				for (int i = 0; i < threadCount; i++) {
					taskFutures.add(SCHEDULED_THREAD_POOL_EXECUTOR.schedule(() -> {
						try {
							int submittedIndex = counter.getAndIncrement();
							if (useCallableAction) {
								Integer index = this.synchronizationHandler.call(
										SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Waiting for lock", TEST_LOCK_KEY)
												.withMillisecondsToWaitIfBusy(millisecondsToWaitIfBusy)
												.buildWithCallableAction(executionIndex::getAndIncrement));
								return new Integer[]{submittedIndex, index};
							}
							else {
								Integer[] indexes = new Integer[2];
								this.synchronizationHandler.execute(
										SynchronizableBuilder.forLocking(TEST_SECURE_AREA, "Waiting for lock", TEST_LOCK_KEY)
												.withMillisecondsToWaitIfBusy(millisecondsToWaitIfBusy)
												.buildWithExecutableAction(() -> {
													indexes[0] = submittedIndex;
													indexes[1] = executionIndex.getAndIncrement();
												}));
								return indexes;
							}
						}
						finally {
							queuedLatch.countDown();
						}
					}, i * (millisecondsToWaitIfBusy / 2), TimeUnit.MILLISECONDS));
				}
				// pause for a bit to allow submitted actions to queue
				lockHoldingAction.await(millisecondsToWaitIfBusy * 3 / 4, TimeUnit.MILLISECONDS);
			}
			finally {
				// release the lock so the queued tasks can function without LockBusyException
				lockHoldingAction.release();
			}

			// wait for all queued actions to be spawned
			awaitCountDownLatch(queuedLatch);

			Assertions.assertEquals(threadCount, taskFutures.size());
			// validate that the queued tasks are in FIFO order
			for (Future<Integer[]> result : taskFutures) {
				try {
					Integer[] indexes = result.get();
					Assertions.assertEquals(indexes[0], indexes[1]);
				}
				catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				catch (ExecutionException e) {
					Assertions.fail("Did not expect exception in waiting thread: " + e.getMessage());
				}
			}
		}
		finally {
			lockHoldingAction.release();
		}
	}


	private static void awaitCountDownLatch(CountDownLatch latch) {
		try {
			latch.await(2, TimeUnit.MINUTES);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
	}


	private void validateLockBusyException(LockBusyException exception, String expectedLockBusySecureArea, String causeLockMessage) {
		validateLockBusyException(exception, expectedLockBusySecureArea, null, causeLockMessage);
	}


	private void validateLockBusyException(LockBusyException exception, String expectedLockBusySecureArea, String expectedLockBusyKey, String causeLockMessage) {
		Assertions.assertTrue(exception.getMessage().contains(expectedLockBusySecureArea), "Expected to get a LockBusyException with a message containing \"" + expectedLockBusySecureArea + "\"");
		if (expectedLockBusyKey != null) {
			Assertions.assertTrue(exception.getMessage().contains(expectedLockBusyKey), "Expected to get a LockBusyException with a message containing \"" + expectedLockBusyKey + "\"");
		}
		Assertions.assertTrue(ExceptionUtils.getOriginalMessage(exception).contains(causeLockMessage), "Expected to have LockBusyCause with message containing \"" + causeLockMessage + "\"");
	}


	private void failExpectedLockBusyException() {
		Assertions.fail("Expected to get LockBusyException");
	}


	private void failDidNotExpectLockBusyException() {
		Assertions.fail("Did not expect to get LockBusyException");
	}


	private static final class LockHoldingAction {

		private final CountDownLatch finalizingLatch;
		private final Future<?> future;


		private LockHoldingAction(CountDownLatch finalizingLatch, Future<?> future) {
			this.finalizingLatch = finalizingLatch;
			this.future = future;
		}


		public void await(long duration, TimeUnit units) {
			try {
				this.finalizingLatch.await(duration, units);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
		}


		public void release() {
			this.finalizingLatch.countDown();
			try {
				this.future.get(1, TimeUnit.MINUTES);
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			catch (Exception e) {
				// ignored
			}
		}
	}
}
