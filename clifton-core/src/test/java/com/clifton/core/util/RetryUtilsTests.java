package com.clifton.core.util;

import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.retry.RetryContext;
import com.clifton.core.util.retry.RetryFuture;
import com.clifton.core.util.retry.RetryJob;
import com.clifton.core.util.retry.RetryUtils;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;


/**
 * @author theodorez
 */
public class RetryUtilsTests {

	private static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors());
	private static final BlockingQueue<RetryFuture<Integer>> QUEUE = new LinkedBlockingQueue<>(Runtime.getRuntime().availableProcessors());


	@AfterAll
	public static void tearDown() {
		ConcurrentUtils.shutdownExecutorService(SCHEDULER);
	}


	//The task succeeds on the first run
	@Test
	public void testRetryNoRetry() {
		AtomicInteger counter1 = new AtomicInteger();
		final int expectedResult = 5;

		List<Integer> finalResults = doTest(
				() -> {
					System.out.println("(" + System.currentTimeMillis() + ") testRetryNoRetry -> Attempt: " + counter1.get());
					counter1.getAndIncrement();
					System.out.println("(" + System.currentTimeMillis() + ") testRetryNoRetry -> Returning: 5");
					return 5;
				},
				(r) -> {
					System.out.println("(" + System.currentTimeMillis() + ") testRetryNoRetry -> Attempt: " + counter1.get() + " Result: " + r);
					return MathUtils.isGreaterThanOrEqual(r, 5);
				},
				expectedResult,
				true);

		//Validate that it was run one time
		System.out.println("(" + System.currentTimeMillis() + ") testRetryNoRetry -> counter: " + counter1.get());
		Assertions.assertEquals(1, counter1.get());
		System.out.println("(" + System.currentTimeMillis() + ") testRetryNoRetry -> finalResults: " + finalResults);
		Assertions.assertEquals(expectedResult, CollectionUtils.getFirstElement(finalResults));
	}


	//Retries the task up to the set number of times
	@Test
	public void testRetryWithRetry() {
		final int expectedResult = 5;
		AtomicInteger counter1 = new AtomicInteger();

		List<Integer> finalResults = doTest(//Increments the counter on each run of the task
				() -> {
					System.out.println("(" + System.currentTimeMillis() + ") testRetryWithRetry -> Attempt: " + counter1.get());
					return counter1.incrementAndGet();
				},
				//The task will be 'successful' when the counter is equal to or greater than 6, which will never happen
				(r) -> {
					System.out.println("(" + System.currentTimeMillis() + ") testRetryWithRetry -> Attempt: " + counter1.get() + " Result: " + r);
					return MathUtils.isGreaterThanOrEqual(r, 6);
				},
				expectedResult,
				true);
		System.out.println("(" + System.currentTimeMillis() + ") testRetryWithRetry -> counter: " + counter1.get());
		Assertions.assertEquals(expectedResult, counter1.get());
		System.out.println("(" + System.currentTimeMillis() + ") testRetryWithRetry -> finalResults: " + finalResults);
		Assertions.assertEquals(expectedResult, CollectionUtils.getFirstElement(finalResults));
	}


	//Retries the task with an exception on the first run where the success condition is false
	@Test
	public void testRetryWithExceptionFailure() {
		testRetryWithException("testRetryWithExceptionFailure", false);
	}


	//Retries the task with an exception on the first run but the success condition is true
	@Test
	public void testRetryWithExceptionSuccessful() {
		testRetryWithException("testRetryWithExceptionSuccessful", true);
	}


	//Retries the task with an exception on the first run where the success condition is set to the value of the success variable
	//The task should run once, which produces an exception, leading to no results being returned.
	private void testRetryWithException(String name, boolean success) {
		final int expectedResult = 1;
		AtomicInteger counter1 = new AtomicInteger();

		List<Integer> finalResults = doTest(
				() -> {
					counter1.incrementAndGet();
					//This will cause the future to complete exceptionally
					throw new RuntimeException("Something bad happened");
				},
				(r) -> success,
				expectedResult,
				false);
		System.out.println("(" + System.currentTimeMillis() + ") " + name + " -> counter: " + counter1.get());
		Assertions.assertEquals(expectedResult, counter1.get());
		System.out.println("(" + System.currentTimeMillis() + ") " + name + " -> finalResults: " + finalResults);
		Assertions.assertTrue(CollectionUtils.isEmpty(finalResults));
	}


	private List<Integer> doTest(Supplier<Integer> action, Predicate<Integer> successCondition, int maxRetries, boolean resultsExpected) {
		final List<Integer> finalResults = new ArrayList<>();
		CountDownLatch latch = new CountDownLatch(1);
		final ScheduledFuture<?> retryFutureMonitor = RetryUtils.getFutureMonitoringFuture(SCHEDULER, QUEUE, (resultList) -> {
			System.out.println("(" + System.currentTimeMillis() + ") RetryMonitoringFuture found completed futures. Values: " + resultList);
			finalResults.addAll(resultList);
			latch.countDown();
		}, 1);
		try {
			RetryJob<Integer> job1 = createJob(action, successCondition, maxRetries);
			QUEUE.add(getRetryFuture(job1));
			System.out.println("(" + System.currentTimeMillis() + ") Waiting for up to 5 seconds for completion.");
			latch.await(5, TimeUnit.SECONDS);
			System.out.println("(" + System.currentTimeMillis() + ") Completed. Number of results: " + CollectionUtils.getSize(finalResults));
			if (resultsExpected) {
				Assertions.assertFalse(CollectionUtils.isEmpty(finalResults));
			}
			return finalResults;
		}
		catch (InterruptedException e) {
			LogUtils.error(RetryUtilsTests.class, "Error occurred running retry tests", e);
		}
		finally {
			retryFutureMonitor.cancel(true);
		}
		return null;
	}


	//Starts the job and creates a RetryFuture with a reference to it
	private RetryFuture<Integer> getRetryFuture(RetryJob<Integer> job) {
		List<CompletableFuture<Integer>> completableFutures = new ArrayList<>();
		System.out.println("(" + System.currentTimeMillis() + ") Submitting Job");
		completableFutures.add(job.submit());
		return new RetryFuture<>(completableFutures);
	}


	private RetryJob<Integer> createJob(Supplier<Integer> action, Predicate<Integer> successCondition, int maxRetries) {
		return new RetryJob<>(RetryContext.RetryContextBuilder.getInstanceForActionAndSuccessCondition(action, successCondition)
				.withMaxRetries(maxRetries)
				.withRetryDelayInSeconds(0)
				.withScheduler(SCHEDULER)
				.build()
		);
	}
}
