package com.clifton.core.util.aws.s3;

import com.amazonaws.services.s3.AmazonS3URI;
import org.junit.jupiter.api.Test;

import static com.clifton.core.util.aws.s3.AWSS3Utils.getAmazonS3Uri;
import static com.clifton.core.util.aws.s3.AWSS3Utils.getS3FileKey;
import static org.junit.jupiter.api.Assertions.assertEquals;


class AWSS3UtilsTest {
	private static final String BUCKET = "ppacoresoftsyssandbox-priv-mpls-testing";
	private static final String FILE_NAME = "testFile.csv";
	private static final String PATH_TO_FILE = "path/to/file/";

	@Test
	void testGetAmazonS3UriWithProtocol() {
		String s3DestinationPath = String.format("s3://%s/%s", BUCKET, PATH_TO_FILE);
		AmazonS3URI s3Uri = getAmazonS3Uri(s3DestinationPath);

		assertEquals(BUCKET, s3Uri.getBucket());
		assertEquals(PATH_TO_FILE, s3Uri.getKey());
	}

	@Test
	void testGetAmazonS3UriWithoutProtocol() {
		String s3DestinationPath = String.format("%s/%s", BUCKET, PATH_TO_FILE);
		AmazonS3URI s3Uri = getAmazonS3Uri(s3DestinationPath);

		assertEquals(BUCKET, s3Uri.getBucket());
		assertEquals(PATH_TO_FILE, s3Uri.getKey());
	}

	@Test
	void testGetS3FileKeyNoPath() {
		String fileKey = getS3FileKey(null, FILE_NAME);
		assertEquals(FILE_NAME, fileKey);
	}

	@Test
	void testGetS3FileKeyWithPathNoTrailingSlash() {
		AmazonS3URI s3Uri = getAmazonS3Uri(String.format("%s/%s", BUCKET, "path/to/file"));
		String s3FileKey = getS3FileKey(s3Uri.getKey(), FILE_NAME);

		assertEquals(PATH_TO_FILE + FILE_NAME, s3FileKey);
	}

	@Test
	void testGetS3FileKeyWithPathWithTrailingSlash() {
		AmazonS3URI s3Uri = getAmazonS3Uri(String.format("%s/%s", BUCKET, PATH_TO_FILE));
		String s3FileKey = getS3FileKey(s3Uri.getKey(), FILE_NAME);

		assertEquals(PATH_TO_FILE + FILE_NAME, s3FileKey);
	}
}
