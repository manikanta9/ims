package com.clifton.core.util.queue;


import java.io.Serializable;


public class QueueTestBean implements Serializable {

	private QueueTestBean parent;
	private int id;


	public QueueTestBean() {
		//
	}


	public QueueTestBean(int id) {
		this.id = id;
	}


	public QueueTestBean(int id, QueueTestBean parent) {
		this.id = id;
		this.parent = parent;
	}


	public QueueTestBean getParent() {
		return this.parent;
	}


	public void setParent(QueueTestBean parent) {
		this.parent = parent;
	}


	public int getId() {
		return this.id;
	}


	public void setId(int id) {
		this.id = id;
	}
}
