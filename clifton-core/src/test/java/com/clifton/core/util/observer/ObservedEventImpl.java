package com.clifton.core.util.observer;


public class ObservedEventImpl implements ObservedEvent {

	private String suffix;


	public ObservedEventImpl(String suffix) {
		this.suffix = suffix;
	}


	@Override
	public void afterUpdate(StringBuilder builder) {
		builder.append(this.suffix);
	}
}
