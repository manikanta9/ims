package com.clifton.core.util.runner;




import org.apache.poi.ss.formula.functions.T;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class TestRunnerProvider2 implements RunnerProvider<T> {

	private int count = 0;
	private Date secondDateTime;
	private final TestObject testObject = new TestObject();


	@Override
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		List<Runner> result = new ArrayList<>();
		if (this.secondDateTime == null) {
			this.secondDateTime = endDateTime;
		}
		if (this.count == 0) {
			result.add(new TestRunner("TEST", "1", startDateTime, this.testObject));
			result.add(new TestRunner("TEST", "2", this.secondDateTime, this.testObject));
			this.count++;
		}
		else {
			result.add(new TestRunner("TEST", "2", this.secondDateTime, this.testObject));
			result.add(new TestRunner("TEST", "3", endDateTime, this.testObject));
		}
		return result;
	}


	@Override
	public Runner createRunnerForEntityAndDate(T entity, Date runnerDate) {
		return null;
	}


	@Override
	public List<Runner> getOccurrencesBetweenForEntity(T entity, Date startDateTime, Date endDateTime) {
		return null;
	}


	public TestObject getTestObject() {
		return this.testObject;
	}
}
