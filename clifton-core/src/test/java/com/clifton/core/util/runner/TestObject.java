package com.clifton.core.util.runner;


import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


/**
 * The <code>TestObject</code> ...
 *
 * @author mwacker
 */
public class TestObject {

	private final Semaphore semaphore = new Semaphore(0);
	private final List<TestRunner> testRunnerList = new ArrayList<>();


	public int waitForExecutions(int executionCount) {
		try {
			this.semaphore.tryAcquire(executionCount, 5, TimeUnit.SECONDS);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		return this.testRunnerList.size();
	}


	public List<TestRunner> getExecutedRunners() {
		return new ArrayList<>(this.testRunnerList); // get snapshot to avoid concurrent modification
	}


	public void execute(TestRunner runner) {
		this.testRunnerList.add(runner);
		this.semaphore.release();
	}
}
