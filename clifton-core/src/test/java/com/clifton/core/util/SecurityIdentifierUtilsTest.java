package com.clifton.core.util;

import com.clifton.core.util.validation.FieldValidationException;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;


public class SecurityIdentifierUtilsTest {

	@Test
	public void testisValidCUSIP() {
		SecurityIdentifierUtils.isValidCUSIP("83164JYE0", true);
		SecurityIdentifierUtils.isValidCUSIP("31283KZW1", true);
		SecurityIdentifierUtils.isValidCUSIP("912833PA2", true);
		SecurityIdentifierUtils.isValidCUSIP("392690QT3", true);
		SecurityIdentifierUtils.isValidCUSIP("912828PN4", true);
		SecurityIdentifierUtils.isValidCUSIP("912828QV5", true);
		SecurityIdentifierUtils.isValidCUSIP("912795Y96", true);
		SecurityIdentifierUtils.isValidCUSIP("17306UBW7", true);
		SecurityIdentifierUtils.isValidCUSIP("07387BAG8", true);
		SecurityIdentifierUtils.isValidCUSIP("071813109", true);

		//Test for Red Code
		SecurityIdentifierUtils.isValidCUSIP("2I65BZEA6", true);

		//Test for CINS
		SecurityIdentifierUtils.isValidCUSIP("G1151C101", true);
		SecurityIdentifierUtils.isValidCUSIP("g1151c101", true);
		SecurityIdentifierUtils.isValidCUSIP("S08000AA9", true);
		SecurityIdentifierUtils.isValidCUSIP("V39076134", true);
		SecurityIdentifierUtils.isValidCUSIP("W02005101", true);
	}


	@Test
	public void testIsValidCUSIP_InvalidLength() {
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP(null, true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("83164JYE", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("71813108", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("71813109", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("0718131091", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("UCDF8C 81", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("VGU9", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("USISDA30", true));
	}


	@Test
	public void testIsValidCUSIP_InvalidCheckSum() {
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidCUSIP("83164JYE1", true));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsValidISIN() {
		SecurityIdentifierUtils.isValidISIN("GB00B85SFQ54", true);
		SecurityIdentifierUtils.isValidISIN("GB00B0V3WQ75", true);
		SecurityIdentifierUtils.isValidISIN("FR0011427848", true);
		SecurityIdentifierUtils.isValidISIN("DE0001030526", true);
		SecurityIdentifierUtils.isValidISIN("JP1120021462", true);
		SecurityIdentifierUtils.isValidISIN("JP1120161862", true);
		SecurityIdentifierUtils.isValidISIN("JP11201317A8", true);
		SecurityIdentifierUtils.isValidISIN("US912828MG20", true);
	}


	@Test
	public void testIsValidISIN_InvalidLength() {
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN(null, true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN("", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN("1234", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN("GB00B85SFQ4", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN("GB00B85SFQ5", true));
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN("US912828MG201", true));
	}


	@Test
	public void testIsValidISIN_InvalidCheckSum() {
		Assertions.assertThrows(FieldValidationException.class, () -> SecurityIdentifierUtils.isValidISIN("GB00B85SFQ56", true));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testisValidOccSymbol() {
		// OCC default format (used by IMS)
		SecurityIdentifierUtils.isValidOccSymbol("SPX   141122P00019500", true);
		SecurityIdentifierUtils.isValidOccSymbol("AAPL  150117C00052500", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPXPM 170120C02315000", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPX   170120C02205000", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPXW  170210P02215000", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPX   171215C01775000", true);
		SecurityIdentifierUtils.isValidOccSymbol("RUTW  170127C01410000", true);
		SecurityIdentifierUtils.isValidOccSymbol("EFA   171215C00063000", true);
		SecurityIdentifierUtils.isValidOccSymbol("4SPX  171031C01775000", true);
		SecurityIdentifierUtils.isValidOccSymbol("4RUT  170907P01195440", true);
		// OPRA format (allowed in validation)
		SecurityIdentifierUtils.isValidOccSymbol("SPXPM17A200023150", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPX  17A200022050", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPXW 17M100022150", true);
		SecurityIdentifierUtils.isValidOccSymbol("SPX  17L150017750", true);
		SecurityIdentifierUtils.isValidOccSymbol("RUTW 17A270014100", true);
		SecurityIdentifierUtils.isValidOccSymbol("EFA  17L150000630", true);
		SecurityIdentifierUtils.isValidOccSymbol("4SPX 17J310017750", true);
		SecurityIdentifierUtils.isValidOccSymbol("4RUT 17U070011954", true);

		//Invalid
		testInvalidOccSymbol("SPX   17A20C02205000", true);
		testInvalidOccSymbol("SPX   170120C0022050", true);
		testInvalidOccSymbol("170120C02205000", true);
		testInvalidOccSymbol("SPX   17A2000220500", true);
		testInvalidOccSymbol("170120C00220500", true);
		testInvalidOccSymbol("SPX  17A20C00220500", true);
		testInvalidOccSymbol("SPX  17A20022050000", true);

		testInvalidOccSymbol("SPX   17A20C02205000", false);
		testInvalidOccSymbol("SPX   170120C0022050", false);
		testInvalidOccSymbol("170120C02205000", false);
		testInvalidOccSymbol("SPX   17A2000220500", false);
		testInvalidOccSymbol("170120C00220500", false);
		testInvalidOccSymbol("SPX  17A20C00220500", false);
		testInvalidOccSymbol("SPX  17A20022050000", false);
	}


	@Test
	public void getOccSymbolExpirationDate() {
		// OCC default format (used by IMS)
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPX   141122P00019500", true), IsEqual.equalTo(LocalDate.of(2014, 11, 22)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("AAPL  150117C00052500", true), IsEqual.equalTo(LocalDate.of(2015, 1, 17)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPXPM 170120C02315000", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPX   170120C02205000", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPXW  170210P02215000", true), IsEqual.equalTo(LocalDate.of(2017, 2, 10)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPX   171215C01775000", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("RUTW  170127C01410000", true), IsEqual.equalTo(LocalDate.of(2017, 1, 27)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("EFA   171215C00063000", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("4SPX  171031C01775000", true), IsEqual.equalTo(LocalDate.of(2017, 10, 31)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("4RUT  170907P01195440", true), IsEqual.equalTo(LocalDate.of(2017, 9, 7)));

		// OPRA format (allowed in validation)
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPXPM17A200023150", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPX  17A200022050", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPXW 17M100022150", true), IsEqual.equalTo(LocalDate.of(2017, 1, 10)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("SPX  17L150017750", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("RUTW 17A270014100", true), IsEqual.equalTo(LocalDate.of(2017, 1, 27)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("EFA  17L150000630", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("4SPX 17J310017750", true), IsEqual.equalTo(LocalDate.of(2017, 10, 31)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDate("4RUT 17U070011954", true), IsEqual.equalTo(LocalDate.of(2017, 9, 7)));
	}


	@Test
	public void getOccSymbolExpirationDateFlexible() {
		// OCC default format (used by IMS)
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("EFA191115C67", true), IsEqual.equalTo(LocalDate.of(2019, 11, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("WMB191025C26.5", true), IsEqual.equalTo(LocalDate.of(2019, 10, 25)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("AAPL150117C00052500", true), IsEqual.equalTo(LocalDate.of(2015, 1, 17)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPXPM 170120C02315000", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPX  170120C02205000", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPXW170210P02215000", true), IsEqual.equalTo(LocalDate.of(2017, 2, 10)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPX171215C01775000", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("RUTW170127C01410000", true), IsEqual.equalTo(LocalDate.of(2017, 1, 27)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("EFA171215C00063000", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("4SPX  171031C01775000", true), IsEqual.equalTo(LocalDate.of(2017, 10, 31)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("4RUT170907P01195440", true), IsEqual.equalTo(LocalDate.of(2017, 9, 7)));

		// OPRA format (allowed in validation)
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPXPM17A200023150", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPX 17A200022050", true), IsEqual.equalTo(LocalDate.of(2017, 1, 20)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPXW 17M100022150", true), IsEqual.equalTo(LocalDate.of(2017, 1, 10)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("SPX 17L150017750", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("RUTW 17A270014100", true), IsEqual.equalTo(LocalDate.of(2017, 1, 27)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("EFA17L150000630", true), IsEqual.equalTo(LocalDate.of(2017, 12, 15)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("4SPX17J310017750", true), IsEqual.equalTo(LocalDate.of(2017, 10, 31)));
		MatcherAssert.assertThat(SecurityIdentifierUtils.getOccSymbolExpirationDateFlexible("4RUT17U070011954", true), IsEqual.equalTo(LocalDate.of(2017, 9, 7)));
	}


	private void testInvalidOccSymbol(String symbol, boolean exceptionIfInvalid) {
		try {
			boolean valid = SecurityIdentifierUtils.isValidOccSymbol(symbol, exceptionIfInvalid);
			if (valid || exceptionIfInvalid) {
				Assertions.fail("Expected an invalid OCC symbol.");
			}
		}
		catch (Exception e) {
			if (!exceptionIfInvalid) {
				Assertions.fail("Did not expect an exception during validation of the OCC symbol.");
			}
		}
	}
}
