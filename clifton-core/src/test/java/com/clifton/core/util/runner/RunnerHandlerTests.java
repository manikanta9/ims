package com.clifton.core.util.runner;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.util.concurrent.TimeUnit;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class RunnerHandlerTests {

	@Resource
	private ContextHandler contextHandler;

	@Resource
	private ApplicationContextService applicationContextService;


	private RunnerHandler setupRunnerHandler(RunnerProvider<?>... providers) {
		ThreadPoolRunnerHandler runnerHandler = new ThreadPoolRunnerHandler();
		runnerHandler.setContextHandler(this.contextHandler);
		runnerHandler.setNumberOfThreads(1);
		runnerHandler.setDelay(0);
		runnerHandler.setScheduleProviders(new ArrayList<>());
		for (RunnerProvider<?> provider : providers) {
			runnerHandler.getScheduleProviders().add(provider);
		}
		runnerHandler.setApplicationContextService(this.applicationContextService);
		runnerHandler.start();
		return runnerHandler;
	}


	@Test
	public void testScheduleControllerStartStop() {
		TestRunnerProvider provider = new TestRunnerProvider();
		RunnerHandler runnerHandler = setupRunnerHandler(provider);
		TestObject testObject = provider.getTestObject();

		testObject.waitForExecutions(1);
		runnerHandler.stop(false);

		// task 4 will not have executed before stop
		Assertions.assertTrue(CollectionUtils.isEmpty(runnerHandler.getScheduledRunnerList("TEST", "4")));
		assertExecutedRunners(testObject.getExecutedRunners(), "1");

		runnerHandler.start();
		testObject.waitForExecutions(1);
		runnerHandler.stop();
		// task 1 should execute again
		assertExecutedRunners(testObject.getExecutedRunners(), "1");
	}


	private void assertExecutedRunners(List<TestRunner> executedRunnerList, String... expectedTypeIds) {
		Assertions.assertNotNull(expectedTypeIds, "Must provide at least one type ID to validate.");
		for (TestRunner executedRunner : CollectionUtils.getIterable(executedRunnerList)) {
			boolean matchesExpected = false;
			for (String typeId : expectedTypeIds) {
				if (executedRunner.getTypeId().equals(typeId)) {
					matchesExpected = true;
					break;
				}
			}
			Assertions.assertTrue(matchesExpected, "Executed Runners [" + CollectionUtils.toString(executedRunnerList, 10) + "]did not match expected [" + ArrayUtils.toString(expectedTypeIds) + "].");
		}
	}


	@Test
	public void testScheduleController() {
		TestRunnerProvider provider = new TestRunnerProvider();
		RunnerHandler runnerHandler = setupRunnerHandler(provider);
		TestObject testObject = provider.getTestObject();
		// wait for tasks 1 and 2
		testObject.waitForExecutions(2);
		runnerHandler.stop(false);
		assertExecutedRunners(testObject.getExecutedRunners(), "1", "2");
	}


	@Test
	public void testScheduleControllerThreads() {
		TestRunnerProvider provider = new TestRunnerProvider();
		RunnerHandler runnerHandler = setupRunnerHandler(provider);
		TestObject testObject = provider.getTestObject();

		testObject.waitForExecutions(3);
		List<Runner> scheduledRunners = runnerHandler.getScheduledRunnerList("TEST", "4");
		runnerHandler.stop(false);
		Assertions.assertEquals(6, scheduledRunners.size());
	}


	@Test
	public void testControlOverlapSchedule() {
		ThreadPoolRunnerHandler threadPoolRunnerHandler = (ThreadPoolRunnerHandler) setupRunnerHandler();
		threadPoolRunnerHandler.stop(false);
		threadPoolRunnerHandler.setPeriod(5);
		try {
			TestRunnerProvider2 newProvider = new TestRunnerProvider2();
			TestObject testObject = newProvider.getTestObject();
			threadPoolRunnerHandler.getScheduleProviders().add(newProvider);
			threadPoolRunnerHandler.start();

			// wait for second fetch of occurrences between
			testObject.waitForExecutions(3);
			// task 1, 2 and 3 should have each executed once
			assertExecutedRunners(testObject.getExecutedRunners(), "1", "2", "3");
		}
		finally {
			threadPoolRunnerHandler.stop(false);
		}
	}


	@Test
	public void testScheduleBean() {
		Date startDateTime = new Date();
		Runner scheduleBean1 = new TestRunner("TEST", "1", startDateTime, new TestObject());
		Runner scheduleBean2 = new TestRunner("TEST", "2", DateUtils.addSeconds(startDateTime, 2), new TestObject());

		List<Runner> result = new ArrayList<>();
		result.add(scheduleBean1);
		result.add(scheduleBean2);

		AssertUtils.assertTrue(result.contains(scheduleBean1), "Runner list does not contain scheduled bean.");
		AssertUtils.assertTrue(result.contains(scheduleBean2), "Runner list does not contain scheduled bean.");
		AssertUtils.assertTrue(result.get(0).equals(scheduleBean1), "Runner list does not contain scheduled bean.");
		AssertUtils.assertTrue(result.get(1).equals(scheduleBean2), "Runner list does not contain scheduled bean.");

		TestRunner scheduleBean3 = new TestRunner("TEST", "1", DateUtils.addSeconds(startDateTime, 5), new TestObject());
		AssertUtils.assertFalse(result.contains(scheduleBean3), "Runner list should not contain the scheduled bean.");
		AssertUtils.assertFalse(result.get(0).equals(scheduleBean3), "Schedule beans should not be equal.");

		AssertUtils.assertFalse(scheduleBean3.equals(new Object()), "Runner should not be equal to a random object.");
	}


	@Test
	public void testScheduleBeanPreventSameParameters() {
		RunnerHandler runnerHandler = setupRunnerHandler();
		try {
			Date startDateTime = new Date();
			TestObject testObject = new TestObject();
			// schedule both to run now, the second will either fail or succeed depending on if the first completes
			runnerHandler.runNow(new TestRunner("TEST", "1", startDateTime, testObject));
			runnerHandler.runNow(new TestRunner("TEST", "1", DateUtils.addSeconds(startDateTime, 2), testObject));
			// At least one of the runners must have completed
			List<Runner> runnerList = runnerHandler.getScheduledRunnerList();
			Assertions.assertEquals(2, runnerList.size());
			int completedRunnerCount = 0;
			for (Runner runner : runnerList) {
				if (runner.getRunnerState() == Runner.RunnerStates.COMPLETED) {
					completedRunnerCount++;
				}
			}
			Assertions.assertTrue(completedRunnerCount > 0);
		}
		catch (ValidationException e) {
			Assertions.assertEquals("An instant runner for [1: TEST] is already running.", e.getMessage());
		}
		finally {
			runnerHandler.stop(false);
		}
	}


	@Test
	public void testScheduleBeanPreventSameParametersComplete() {
		RunnerHandler runnerHandler = setupRunnerHandler();
		try {
			Date startDateTime = new Date();
			TestObject testObject = new TestObject();
			Runner scheduleBean1 = new TestRunner("TEST", "1", startDateTime, testObject);
			Runner scheduleBean2 = new TestRunner("TEST", "1", DateUtils.addSeconds(startDateTime, 2), testObject);

			runnerHandler.runNow(scheduleBean1);
			/* Make sure the one executed test runs. We wait for two executions to allow some time
			 * for the scheduled task to complete execution before running the second. If we do not wait,
			 * it is possible for the second run test to be executed before the first finishes throwing an
			 * exception - "An instant runner for [1: TEST] is already running.".
			 */
			Assertions.assertEquals(1, testObject.waitForExecutions(2));
			List<Runner> runnerList = runnerHandler.getScheduledRunnerList();
			Assertions.assertEquals(1, runnerList.size());
			runnerHandler.runNow(scheduleBean2);

			runnerList = runnerHandler.getScheduledRunnerList();
			Assertions.assertEquals(2, runnerList.size());
		}
		finally {
			runnerHandler.stop(false);
		}
	}


	@Test
	public void testNonSoftStopWaitsForRunnersToComplete() {
		testStopWaitsForRunnerCompletion(false);
	}


	@Test
	public void testSoftStopDoesNotWaitForRunnersToComplete() {
		testStopWaitsForRunnerCompletion(true);
	}


	private void testStopWaitsForRunnerCompletion(boolean softStop) {
		RunnerHandler runnerHandler = setupRunnerHandler();
		Date startDateTime = DateUtils.addSeconds(new Date(), 2);
		Semaphore semaphore = new Semaphore(0, true);
		Runner controllableRunner = new AbstractStatusAwareRunner("TEST", "1", startDateTime) {

			@Override
			public void run() {
				System.out.println("\trunner releasing " + new Date());
				semaphore.release();
				try {
					/* Wait 20 seconds since the test will not release a permit. The next call
					 * to acquire a permit will not get the permit just released by this runner
					 * because the semaphore is fair, meaning it honors FIFO. Since the test
					 * is already waiting to acquire, it will get the first available permit.
					 */
					semaphore.tryAcquire(5, TimeUnit.SECONDS);
				}
				catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				getStatus().setMessage("COMPLETE");
			}
		};
		runnerHandler.addRunner(controllableRunner);
		try {
			// Wait for runner to start before stopping the runner handler
			semaphore.tryAcquire(5, TimeUnit.SECONDS);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
		}
		Assertions.assertEquals(1, runnerHandler.getScheduledRunnerList().size());
		runnerHandler.stop(softStop);
		Assertions.assertEquals(softStop ? "TEST-1 scheduled for " + DateUtils.fromDate(startDateTime) : "COMPLETE", controllableRunner.getStatus().getMessage());
	}
}
