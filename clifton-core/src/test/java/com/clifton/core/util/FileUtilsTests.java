package com.clifton.core.util;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>FileUtilsTests</code> class is a test class for <code>FileUtils</code>.
 *
 * @author vgomelsky
 */
public class FileUtilsTests {

	@Test
	public void testGetFileExtension() {
		String fileName = null;
		Assertions.assertNull(FileUtils.getFileExtension(fileName));

		fileName = "";
		Assertions.assertNull(FileUtils.getFileExtension(fileName));

		fileName = "TestFile";
		Assertions.assertNull(FileUtils.getFileExtension(fileName));

		fileName = "TestFile.";
		Assertions.assertNull(FileUtils.getFileExtension(fileName));

		fileName = "TestFile.doc";
		Assertions.assertEquals("doc", FileUtils.getFileExtension(fileName));

		fileName = "TestFile.DOC";
		Assertions.assertEquals("doc", FileUtils.getFileExtension(fileName));

		fileName = "Test/Test.Test/TestFile.xls";
		Assertions.assertEquals("xls", FileUtils.getFileExtension(fileName));
	}


	@Test
	public void testGetFileNameWithoutExtension() {
		String fileName = null;
		Assertions.assertNull(FileUtils.getFileNameWithoutExtension(fileName));

		fileName = "";
		Assertions.assertEquals("", FileUtils.getFileNameWithoutExtension(fileName));

		fileName = "TestFile";
		Assertions.assertEquals("TestFile", FileUtils.getFileNameWithoutExtension(fileName));

		fileName = "TestFile.";
		Assertions.assertEquals("TestFile", FileUtils.getFileNameWithoutExtension(fileName));

		fileName = "TestFile.doc";
		Assertions.assertEquals("TestFile", FileUtils.getFileNameWithoutExtension(fileName));

		fileName = "TestFile.DOC";
		Assertions.assertEquals("TestFile", FileUtils.getFileNameWithoutExtension(fileName));

		fileName = "Test/Test.Test/TestFile.xls";
		Assertions.assertEquals("Test/Test.Test/TestFile", FileUtils.getFileNameWithoutExtension(fileName));
	}


	@Test
	public void testGetFileName() {
		String fileName = null;
		Assertions.assertNull(FileUtils.getFileName(fileName));

		fileName = "";
		Assertions.assertEquals("", FileUtils.getFileName(fileName));

		fileName = "TestFile";
		Assertions.assertEquals("TestFile", FileUtils.getFileName(fileName));

		fileName = "TestFile.";
		Assertions.assertEquals("TestFile.", FileUtils.getFileName(fileName));

		fileName = "TestFile.doc";
		Assertions.assertEquals("TestFile.doc", FileUtils.getFileName(fileName));

		fileName = "Test/Test.Test/TestFile.xls";
		Assertions.assertEquals("TestFile.xls", FileUtils.getFileName(fileName));

		fileName = FileUtils.normalizeFilePathSeparators("Test\\Test.Test\\TestFile.xls");
		Assertions.assertEquals("TestFile.xls", FileUtils.getFileName(fileName));
	}


	@Test
	public void testGetFilePath() {
		String fileName = null;
		Assertions.assertEquals("", FileUtils.getFilePath(fileName));

		fileName = "";
		Assertions.assertEquals("", FileUtils.getFilePath(fileName));

		fileName = "TestFile";
		Assertions.assertEquals("", FileUtils.getFilePath(fileName));

		fileName = "TestFile.";
		Assertions.assertEquals("", FileUtils.getFilePath(fileName));

		fileName = "TestFile.doc";
		Assertions.assertEquals("", FileUtils.getFilePath(fileName));

		fileName = "Test/Test.Test/TestFile.xls";
		Assertions.assertEquals("Test/Test.Test/", FileUtils.getFilePath(fileName));

		fileName = "Test\\Test.Test\\TestFile.xls";
		Assertions.assertEquals("Test\\Test.Test\\", FileUtils.getFilePath(fileName));

		fileName = "Test\\Test.Test\\";
		Assertions.assertEquals("Test\\Test.Test\\", FileUtils.getFilePath(fileName));

		fileName = "Test\\Test.Test";
		Assertions.assertEquals("Test\\", FileUtils.getFilePath(fileName));

		fileName = "Test\\";
		Assertions.assertEquals("Test\\", FileUtils.getFilePath(fileName));
	}


	@Test
	public void testGetInputStream() throws IOException {
		InputStream in = FileUtils.getInputStream("com/clifton/core/util/SecurityUtilsTests.class");
		Assertions.assertNotNull(in);
	}


	@Test
	public void testGetInputStreamWithException() {
		Assertions.assertThrows(FileNotFoundException.class, () -> {
			InputStream in = FileUtils.getInputStream("A FILE THAT DOES NOT EXIST");
			Assertions.assertNull(in);
		});
	}


	@Test
	public void testCombinePath() {
		String expectedPath = FileUtils.normalizeFilePathSeparators("c:\\test\\one");
		Assertions.assertEquals(expectedPath, FileUtils.combinePath("c:\\test\\", "one"));
		Assertions.assertEquals(expectedPath, FileUtils.combinePath("c:\\test\\", "\\one"));
		Assertions.assertEquals(expectedPath, FileUtils.combinePath("c:/test/", "\\one"));
		Assertions.assertEquals(expectedPath, FileUtils.combinePath("c:\\test\\", "/one"));
		Assertions.assertEquals(expectedPath, FileUtils.combinePath("c:\\test", "/one"));
		Assertions.assertEquals(FileUtils.normalizeFilePathSeparators(expectedPath + "\\two"), FileUtils.combinePath("c:\\test", "/one/two"));
		Assertions.assertEquals(FileUtils.normalizeFilePathSeparators("\\\\tcgdevutil\\c$\\integration\\processed\\test.csv"), FileUtils.combinePath("\\\\tcgdevutil\\c$\\integration\\processed", "\\test.csv"));
	}


	@Test
	public void testCombinePaths() {
		Assertions.assertEquals("a" + File.separator + "b", FileUtils.combinePaths("a", "b"));
		Assertions.assertEquals("a" + File.separator + "b" + File.separator + "c", FileUtils.combinePaths("a", "b", "c"));
	}


	@Test
	public void testGetRelativePath() {
		//Test no subfolder - file is in the root of the share
		StringBuilder builder = new StringBuilder();
		builder.append(File.separator);
		builder.append("test.txt");
		String expectedValue = builder.toString();
		Assertions.assertEquals(expectedValue, FileUtils.getRelativePath("/integration/export/outgoing/test.txt", "/integration/export/outgoing"));
		Assertions.assertEquals(expectedValue, FileUtils.getRelativePath("C:\\integration\\export\\outgoing\\test.txt", "C:\\integration\\export\\outgoing"));

		//Test subfolder - file is in a subfolder of the share
		builder = new StringBuilder();
		builder.append(File.separator);
		builder.append("testFolder");
		builder.append(File.separator);
		builder.append("test.txt");
		expectedValue = builder.toString();
		Assertions.assertEquals(expectedValue, FileUtils.getRelativePath("/integration/export/outgoing/testFolder/test.txt", "/integration/export/outgoing"));
		Assertions.assertEquals(expectedValue, FileUtils.getRelativePath("C:\\integration\\export\\outgoing\\testFolder\\test.txt", "C:\\integration\\export\\outgoing"));
	}


	@Test
	public void testNormalizeFilePathSeparators() {
		StringBuilder builder = new StringBuilder();
		builder.append(File.separator);
		builder.append("integration");
		builder.append(File.separator);
		builder.append("export");
		builder.append(File.separator);
		builder.append("outgoing");
		String expected = builder.toString();
		Assertions.assertEquals(expected, FileUtils.normalizeFilePathSeparators("/integration/export/outgoing"));
		Assertions.assertEquals(expected, FileUtils.normalizeFilePathSeparators("\\integration\\export\\outgoing"));
		Assertions.assertEquals(expected, FileUtils.normalizeFilePathSeparators("/integration\\export\\outgoing"));
		Assertions.assertEquals(expected, FileUtils.normalizeFilePathSeparators("\\integration/export/outgoing"));
		Assertions.assertEquals(File.separator + expected, FileUtils.normalizeFilePathSeparators("\\/integration/export/outgoing"));
		Assertions.assertEquals(File.separator + expected, FileUtils.normalizeFilePathSeparators("/\\integration/export/outgoing"));
		Assertions.assertEquals(File.separator + expected, FileUtils.normalizeFilePathSeparators("//integration/export/outgoing"));
		Assertions.assertEquals("\\\\integration" + File.separator + "export" + File.separator + "outgoing", FileUtils.normalizeFilePathSeparators("\\\\integration\\export\\outgoing"));
		Assertions.assertEquals(File.separator + expected, FileUtils.normalizeFilePathSeparators("/\\integration\\export\\outgoing"));
	}


	@Test
	public void testPdfConcatenation() throws Exception {
		List<File> pdfFiles = new ArrayList<>();
		pdfFiles.add(new ClassPathResource("com/clifton/core/util/1.pdf").getFile());
		pdfFiles.add(new ClassPathResource("com/clifton/core/util/2.pdf").getFile());
		pdfFiles.add(new ClassPathResource("com/clifton/core/util/3.pdf").getFile());
		File output = FileUtils.concatenatePDFs("output", ".pdf", pdfFiles, true, false);
		Assertions.assertNotNull(output);
	}


	@Test
	public void testReplaceInvalidCharacters() {
		String fileName = "U Pitt – Liquid Alternatives.pdf";
		Assertions.assertEquals("U Pitt  Liquid Alternatives.pdf", FileUtils.replaceInvalidCharacters(fileName, ""));
		Assertions.assertEquals("U Pitt - Liquid Alternatives.pdf", FileUtils.replaceInvalidCharacters(fileName, "-"));
	}


	@Test
	public void testIsRemoteFile() {
		Assertions.assertTrue(FileUtils.isRemote("smb://EDN-700-02/Development/Ted/test/1/1-1/test.txt"));
		Assertions.assertTrue(FileUtils.isRemote("//EDN-700-02/Development/Ted/test/1/1-1/test.txt"));
		Assertions.assertTrue(FileUtils.isRemote("\\\\EDN-700-02\\Development\\Ted\\test\\1\\1-1\\test.txt"));

		Assertions.assertFalse(FileUtils.isRemote("/EDN-700-02/Development/Ted/test/1/1-1/test.txt"));
		Assertions.assertFalse(FileUtils.isRemote("C:/EDN-700-02/Development/Ted/test/1/1-1/test.txt"));
		Assertions.assertFalse(FileUtils.isRemote("\\EDN-700-02\\Development\\Ted\\test\\1\\1-1\\test.txt"));
		Assertions.assertFalse(FileUtils.isRemote("C:\\EDN-700-02\\Development\\Ted\\test\\1\\1-1\\test.txt"));
	}


	@Test
	public void testGetFilesFromDirectory() {
		String path1 = "src\\test\\java\\com\\clifton\\core\\converter";
		String path2 = ".\\src\\test\\java\\com\\clifton\\core\\converter";

		try {
			Map<FilePath, BasicFileAttributes> files = FileUtils.getFilesFromDirectory(FilePath.forPath(path1), false);
			files.entrySet().stream().forEach(e -> Assertions.assertTrue(e.getKey().toFile().isAbsolute()));
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to get files from directory!", e);
		}


		try {
			Map<FilePath, BasicFileAttributes> files = FileUtils.getFilesFromDirectory(FilePath.forPath(path1), true);
			files.entrySet().stream().forEach(e -> Assertions.assertTrue(e.getKey().toFile().isAbsolute()));
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to get files from directory!", e);
		}

		try {
			Map<FilePath, BasicFileAttributes> files = FileUtils.getFilesFromDirectory(FilePath.forPath(path2), false);
			files.entrySet().stream().forEach(e -> Assertions.assertTrue(e.getKey().toFile().isAbsolute()));
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to get files from directory!", e);
		}


		try {
			Map<FilePath, BasicFileAttributes> files = FileUtils.getFilesFromDirectory(FilePath.forPath(path2), true);
			files.entrySet().stream().forEach(e -> Assertions.assertTrue(e.getKey().toFile().isAbsolute()));
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to get files from directory!", e);
		}
	}
}
