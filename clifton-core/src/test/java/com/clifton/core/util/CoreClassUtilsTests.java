package com.clifton.core.util;

import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.Function;


/**
 * The {@link CoreClassUtilsTests} contains tests for the {@link CoreClassUtils} class
 *
 * @author manderson
 */
public class CoreClassUtilsTests {

	// Test fields
	private String string;
	@SuppressWarnings("OptionalUsedAsFieldOrParameterType")
	private Optional<String> optionalString;
	private String[] arrayString;
	private List<String[]> listOfArrayString;
	private List<String>[] arrayListOfString;
	@SuppressWarnings("rawtypes")
	private List list;
	private List<String> listOfString;
	private List<?> listOfWildcard;
	private List<? extends Number> listOfExtendsNumber;
	private Map<String, String> mapOfStringToString;
	private Map<String, List<String>> mapOfStringToListOfString;
	private Map<String, Map<String, String>> mapOfStringToMapOfStringToString;
	private Map<String, Map<String, List<String>>> mapOfStringToMapOfStringToListOfString;

	// Test class types
	private abstract static class NumberClass extends Number {}

	private abstract static class ListClass<T> implements List<T> {}

	private abstract static class ListOfStringClass implements List<String> {}

	private abstract static class MapOfStringToStringClass implements Map<String, String> {}

	private abstract static class GenericClass<T extends String> {}

	private abstract static class GenericClassExtended extends GenericClass<String> {}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testIsClassPresent() {
		Assertions.assertFalse(CoreClassUtils.isClassPresent(null));
		Assertions.assertFalse(CoreClassUtils.isClassPresent(""));
		Assertions.assertFalse(CoreClassUtils.isClassPresent("com.clifton.core.NonexistentClass"));
		Assertions.assertTrue(CoreClassUtils.isClassPresent("java.lang.String"));
		Assertions.assertTrue(CoreClassUtils.isClassPresent("java.lang.Integer"));
		Assertions.assertTrue(CoreClassUtils.isClassPresent("com.clifton.core.beans.NamedEntity"));
		Assertions.assertTrue(CoreClassUtils.isClassPresent("com.clifton.core.converter.SimpleConverter"));
	}


	@Test
	public void testGetClassForName() {
		Assertions.assertEquals(int.class, CoreClassUtils.getClass("int"));
		Assertions.assertEquals(Integer.class, CoreClassUtils.getClass("java.lang.Integer"));
		Assertions.assertEquals(String.class, CoreClassUtils.getClass("java.lang.String"));
		Assertions.assertEquals(boolean.class, CoreClassUtils.getClass("boolean"));
		Assertions.assertEquals(NamedHierarchicalEntity.class, CoreClassUtils.getClass("com.clifton.core.beans.hierarchy.NamedHierarchicalEntity"));
		Assertions.assertEquals(HierarchicalEntity.class, CoreClassUtils.getClass("com.clifton.core.beans.hierarchy.HierarchicalEntity"));
	}


	@Test
	public void testGetCanonicalName() {
		Function<String, String> getFieldCanonicalName = fieldName -> {
			Field field = ClassUtils.getClassField(CoreClassUtilsTests.class, fieldName, false, false);
			return CoreClassUtils.getCanonicalName(field.getGenericType());
		};

		// Canonical field type names
		Assertions.assertEquals("java.lang.String", getFieldCanonicalName.apply("string"));
		Assertions.assertEquals("java.util.Optional<java.lang.String>", getFieldCanonicalName.apply("optionalString"));
		Assertions.assertEquals("[Ljava.lang.String;", getFieldCanonicalName.apply("arrayString"));
		Assertions.assertEquals("java.util.List<[Ljava.lang.String;>", getFieldCanonicalName.apply("listOfArrayString"));
		Assertions.assertEquals("[Ljava.util.List;", getFieldCanonicalName.apply("arrayListOfString")); // Compile-time type erasure for array generics
		Assertions.assertEquals("java.util.List<java.lang.Object>", getFieldCanonicalName.apply("list"));
		Assertions.assertEquals("java.util.List<java.lang.String>", getFieldCanonicalName.apply("listOfString"));
		Assertions.assertEquals("java.util.List<java.lang.Object>", getFieldCanonicalName.apply("listOfWildcard"));
		Assertions.assertEquals("java.util.List<java.lang.Number>", getFieldCanonicalName.apply("listOfExtendsNumber"));
		Assertions.assertEquals("java.util.Map<java.lang.String,java.lang.String>", getFieldCanonicalName.apply("mapOfStringToString"));
		Assertions.assertEquals("java.util.Map<java.lang.String,java.util.List<java.lang.String>>", getFieldCanonicalName.apply("mapOfStringToListOfString"));
		Assertions.assertEquals("java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.lang.String>>", getFieldCanonicalName.apply("mapOfStringToMapOfStringToString"));
		Assertions.assertEquals("java.util.Map<java.lang.String,java.util.Map<java.lang.String,java.util.List<java.lang.String>>>", getFieldCanonicalName.apply("mapOfStringToMapOfStringToListOfString"));

		// Canonical class type names
		Assertions.assertEquals("java.lang.Object", CoreClassUtils.getCanonicalName(Object.class));
		Assertions.assertEquals("java.util.List<java.lang.Object>", CoreClassUtils.getCanonicalName(List.class));
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests", CoreClassUtils.getCanonicalName(CoreClassUtilsTests.class));
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests$NumberClass", CoreClassUtils.getCanonicalName(NumberClass.class));
		Assertions.assertEquals("[Ljava.lang.String;", CoreClassUtils.getCanonicalName(String[].class));
		// Collection and map types always include generic parameters
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests$ListClass<java.lang.Object>", CoreClassUtils.getCanonicalName(ListClass.class));
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests$ListOfStringClass<java.lang.String>", CoreClassUtils.getCanonicalName(ListOfStringClass.class));
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests$MapOfStringToStringClass<java.lang.String,java.lang.String>", CoreClassUtils.getCanonicalName(MapOfStringToStringClass.class));
		// Other generic types do not include generic parameters
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests$GenericClass", CoreClassUtils.getCanonicalName(GenericClass.class));
		Assertions.assertEquals("com.clifton.core.util.CoreClassUtilsTests$GenericClassExtended", CoreClassUtils.getCanonicalName(GenericClassExtended.class));
	}


	@Test
	public void testGetCanonicalNameFailure() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			CoreClassUtils.getCanonicalName(null);
			throw new RuntimeException("Should not be reached.");
		});
	}
}
