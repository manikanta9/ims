package com.clifton.core.converter.template;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.common.Contact;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import freemarker.core.Environment;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>FreemarkerTemplateConverterTests</code> tests the FreemarkerTemplateConverterImpl class
 * with various objects
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class FreemarkerTemplateConverterTests {

	@Resource
	private TemplateConverter templateConverter;
	@Resource
	private TemplateBuilder templateBuilder;
	@Resource
	private ReadOnlyDAO<NamedEntity<Integer>> namedEntityDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNullConfig() {
		IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> getTemplateConverter().convert(null));
		Assertions.assertEquals("Cannot generate a formatted string for a null Template Config.", illegalArgumentException.getMessage());
	}


	@Test
	public void testNullTemplateString() {
		IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> new TemplateConfig(null));
		Assertions.assertEquals("Template string is required.", illegalArgumentException.getMessage());
	}


	@Test
	public void testEmptyTemplateString() {
		IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> new TemplateConfig(""));
		Assertions.assertEquals("Template string is required.", illegalArgumentException.getMessage());
	}


	@Test
	public void testNullObject() {
		Assertions.assertThrows(NullPointerException.class, () -> {
			TemplateConfig config = new TemplateConfig("Test");
			config.addBeanToContext("entity", null);
		});
	}


	@Test
	public void testNullFieldOnObject() {
		Assertions.assertThrows(ValidationException.class, () -> {
			// Freemarker will throw an exception for null fields on objects if you try to render them
			NamedEntity<Integer> entity = new NamedEntity<>();
			TemplateConfig config = new TemplateConfig("This test entity's name is: ${entity.name} and has a description of ${entity.description}");
			config.addBeanToContext("entity", entity);
			getTemplateConverter().convert(config);
		});
	}


	@Test
	public void testSupportedNullFieldOnObject() {
		// Use proper syntax so Freemarker won't throw an exception if the field itself is null
		NamedEntity<Integer> entity = new NamedEntity<>();

		TemplateConfig config = new TemplateConfig("This test entity's name is: ${entity.name!} and has a description of ${entity.description!}");
		config.addBeanToContext("entity", entity);

		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("This test entity's name is:  and has a description of ", formattedString);
	}


	@Test
	public void testSimpleObject() {
		// Test rendering a formatted String for a bean
		NamedEntity<Integer> entity = new NamedEntity<>("Test Name", "Test Description");

		TemplateConfig config = new TemplateConfig("This test entity's name is: ${entity.name} and has a description of ${entity.description}");
		config.addBeanToContext("entity", entity);

		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("This test entity's name is: Test Name and has a description of Test Description", formattedString);
	}


	@Test
	public void testMultipleObjects() {
		// Test rendering a formatted String for multiple beans
		NamedEntity<Integer> entity = new NamedEntity<>("Test Name", "Test Description");

		Contact contact = new Contact();
		contact.setFirstName("Joe");
		contact.setLastName("Smith");

		TemplateConfig config = new TemplateConfig("Dear ${currentUser.firstName} ${currentUser.lastName},"
				+ StringUtils.NEW_LINE + "This test entity's name is: ${entity.name} and has a description of ${entity.description}");
		config.addBeanToContext("currentUser", contact);
		config.addBeanToContext("entity", entity);

		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("Dear Joe Smith,"
				+ StringUtils.NEW_LINE + "This test entity's name is: Test Name and has a description of Test Description", formattedString);

		// Test with if statements
		config = new TemplateConfig("Dear <#if currentUser.firstName != \"\">${currentUser.firstName}<#else>User</#if>,"
				+ StringUtils.NEW_LINE + "This test entity's name is: ${entity.name} and has a description of ${entity.description}");
		config.addBeanToContext("currentUser", contact);
		config.addBeanToContext("entity", entity);

		formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("Dear Joe,"
				+ StringUtils.NEW_LINE + "This test entity's name is: Test Name and has a description of Test Description", formattedString);

		contact.setFirstName("");
		formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("Dear User,"
				+ StringUtils.NEW_LINE + "This test entity's name is: Test Name and has a description of Test Description", formattedString);
	}


	@Test
	public void testList() {
		// Test rendering a formatted String for a list of beans
		List<NamedEntity<Integer>> entityList = new ArrayList<>();
		for (int i = 1; i < 4; i++) {
			NamedEntity<Integer> entity = new NamedEntity<>("Entity # " + i, null);
			entity.setId(i);
			entity.setCreateDate(DateUtils.toDate("01/" + i + "/2009"));
			entityList.add(entity);
		}
		String template = "The entities are:"
				+ StringUtils.NEW_LINE + "<#list seq as x>${x.id} - ${x.name} Created On: ${x.createDate?date?string.medium}"
				+ StringUtils.NEW_LINE + "</#list>";
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("seq", entityList);
		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("The entities are:"
				+ StringUtils.NEW_LINE + "1 - Entity # 1 Created On: Jan 1, 2009"
				+ StringUtils.NEW_LINE + "2 - Entity # 2 Created On: Jan 2, 2009"
				+ StringUtils.NEW_LINE + "3 - Entity # 3 Created On: Jan 3, 2009"
				+ StringUtils.NEW_LINE + "", formattedString);
	}


	@Test
	public void testDataTable() {
		// Test rendering a formatted String for a DataTable
		DataTable dataTable = new PagingDataTableImpl(new DataColumnImpl[3], 0, 0);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{1, "Entity # 1", DateUtils.toDate("01/01/2009")}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{2, "Entity # 2", DateUtils.toDate("01/02/2009")}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{3, "Entity # 3", DateUtils.toDate("01/03/2009")}));

		String template = "<#assign rowCount=table.totalRowCount-1/><#assign columnCount=table.columnCount-1/>The entities are:"
				+ StringUtils.NEW_LINE + "<#list 0..rowCount as i>${table.row[i].getValue(0)} - ${table.row[i].getValue(1)} Created On: ${table.row[i].getValue(2)?date?string.short}"
				+ StringUtils.NEW_LINE + "</#list>";
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("table", dataTable);
		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("The entities are:"
				+ StringUtils.NEW_LINE + "1 - Entity # 1 Created On: 1/1/09"
				+ StringUtils.NEW_LINE + "2 - Entity # 2 Created On: 1/2/09"
				+ StringUtils.NEW_LINE + "3 - Entity # 3 Created On: 1/3/09"
				+ StringUtils.NEW_LINE + "", formattedString);
	}


	@Test
	public void testDataTableUsingBuilder() {
		// Test rendering a formatted String for a DataTable
		DataTable dataTable = new PagingDataTableImpl(new DataColumnImpl[]{new DataColumnImpl("ID", java.sql.Types.INTEGER), new DataColumnImpl("Name", java.sql.Types.NVARCHAR),
				new DataColumnImpl("Create Date", java.sql.Types.DATE)}, 0, 0);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{1, "Entity # 1", DateUtils.toDate("01/01/2009")}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{2, "Entity # 2", DateUtils.toDate("01/02/2009")}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{3, "Entity # 3", DateUtils.toDate("01/03/2009")}));

		String template = getTemplateBuilder().buildDataTableTemplate(dataTable, "dataTable");
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("dataTable", dataTable);
		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals(
				"<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; border-collapse: collapse; padding: 5px 4px 5px 6px; border-style: solid; border-width: 1px;  border-color: #d0d0d0; background-color: white;\">"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr style=\"padding: 5px 4px 5px 6px; border: 1px solid #d0d0d0; border-left-color: #eee; background-color: #ededed;\">"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Name</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td align=\"center\">Create Date</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Entity # 1</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>01/01/2009</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Entity # 2</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>01/02/2009</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Entity # 3</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>01/03/2009</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
						+ StringUtils.NEW_LINE + "</table>", formattedString);
	}


	@Test
	public void testEmptyDataTableUsingBuilder() {
		// Test rendering a formatted String for a DataTable
		DataTable dataTable = new PagingDataTableImpl(new DataColumnImpl[]{new DataColumnImpl("ID", java.sql.Types.INTEGER), new DataColumnImpl("Name", java.sql.Types.NVARCHAR),
				new DataColumnImpl("Create Date", java.sql.Types.DATE)}, 0, 0);

		String template = getTemplateBuilder().buildDataTableTemplate(dataTable, "dataTable");
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("dataTable", dataTable);
		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals(
				"<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; border-collapse: collapse; padding: 5px 4px 5px 6px; border-style: solid; border-width: 1px;  border-color: #d0d0d0; background-color: white;\">"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr style=\"padding: 5px 4px 5px 6px; border: 1px solid #d0d0d0; border-left-color: #eee; background-color: #ededed;\">"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Name</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td align=\"center\">Create Date</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td align=\"center\" colspan=\"3\">None</td>"
						+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
						+ StringUtils.NEW_LINE + "</table>",
				formattedString);
	}


	@Test
	public void testDataRowUsingBuilder() {
		// Test rendering a formatted String for a DataTable
		DataTable dataTable = new PagingDataTableImpl(new DataColumnImpl[]{new DataColumnImpl("ID", java.sql.Types.INTEGER), new DataColumnImpl("Name", java.sql.Types.NVARCHAR),
				new DataColumnImpl("Create Date", java.sql.Types.DATE)}, 0, 0);
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{1, "Entity # 1", DateUtils.toDate("01/01/2009")}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{2, "Entity # 2", DateUtils.toDate("01/02/2009")}));
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{3, "Entity # 3", DateUtils.toDate("01/03/2009")}));

		String template = getTemplateBuilder().buildDataRowTemplate(dataTable, "row");
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("row", dataTable.getRow(0));
		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>Name</b></td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Entity # 1</td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB
				+ "<td><b>Create Date</b></td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>01/01/2009</td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
				+ StringUtils.NEW_LINE + "</table>", formattedString);
	}


	@Test
	public void testDTOObjectUsingBuilder() {
		NamedEntity<Integer> entity = new NamedEntity<>("Entity # 1", " This is a test Entity!");
		String template = getTemplateBuilder().buildDTOObjectTemplate(getNamedEntityDAO().getConfiguration(), "bean");
		TemplateConfig config = new TemplateConfig(template);
		config.addBeanToContext("bean", entity);
		String formattedString = getTemplateConverter().convert(config);
		Assertions.assertEquals("<table style=\"width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;\">"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td><b>Name</b></td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td>Entity # 1</td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "<tr>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB
				+ "<td><b>Description</b></td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + StringUtils.TAB + "<td> This is a test Entity!</td>"
				+ StringUtils.NEW_LINE + StringUtils.TAB + "</tr>"
				+ StringUtils.NEW_LINE + "</table>", formattedString);
	}


	@Test
	public void testConvertWithUtilsClasses() {
		Map<String, Object> params = new HashMap<>();
		params.put("customDate", DateUtils.toDate("11/10/2016"));
		String formattedString = getTemplateConverter().convert(TemplateConfig.ofTemplateWithUtilsClasses("Week Ending on ${DateUtils.addDays(customDate, -6)?date?string.full}", params));
		Assertions.assertEquals("Week Ending on Friday, November 4, 2016", formattedString);
	}


	@Test
	public void testConvertWithoutUtilsClasses() {
		Assertions.assertThrows(ValidationException.class, () -> {
			Map<String, Object> params = new HashMap<>();
			params.put("customDate", DateUtils.toDate("11/10/2016"));
			String formattedString = getTemplateConverter().convert(TemplateConfig.ofTemplate("Week Ending on ${DateUtils.addDays(customDate, -6)?date?string.full}", params));
			Assertions.assertEquals("Week Ending on Friday, November 4, 2016", formattedString);
		});
	}


	@Test
	public void testProcessTemplateStringResult() throws Exception {
		// Prepare and run template
		TemplateTestEntity entity = new TemplateTestEntity();
		String value3Str = DateUtils.fromDate(entity.getValue3(), DateUtils.DATE_FORMAT_FULL);
		String value4Str = "String 1";
		String value5Str = "5";
		Map<String, Object> params = new HashMap<>();
		params.put("bean", entity);
		params.put("string1", value4Str);
		Environment processedTemplate = getTemplateConverter().processTemplate(TemplateConfig.ofTemplate(""
						+ StringUtils.NEW_LINE + "<#assign output1 = bean.value1>"
						+ StringUtils.NEW_LINE + "<#assign output2 = bean.value2>"
						+ StringUtils.NEW_LINE + "<#assign output3 = bean.value3?string[\"" + DateUtils.DATE_FORMAT_FULL + "\"]>"
						+ StringUtils.NEW_LINE + "<#assign output4 = string1>"
						+ StringUtils.NEW_LINE + "<#assign output5 = 5>"
						+ StringUtils.NEW_LINE + "<#assign outputCompiled = \"${bean.value1}:${output2}:${bean.value3?string[\\\"" + DateUtils.DATE_FORMAT_FULL + "\\\"]}:${output4}:${output5}\">"
						+ StringUtils.NEW_LINE + "Template output: ${outputCompiled}",
				params));

		// Test results
		String output1 = processedTemplate.getVariable("output1").toString();
		String output2 = processedTemplate.getVariable("output2").toString();
		String output3 = processedTemplate.getVariable("output3").toString();
		String output4 = processedTemplate.getVariable("output4").toString();
		String output5 = processedTemplate.getVariable("output5").toString();
		String outputCompiled = processedTemplate.getVariable("outputCompiled").toString();

		Assertions.assertEquals(String.valueOf(entity.getValue1()), output1);
		Assertions.assertEquals(String.valueOf(entity.getValue2()), output2);
		Assertions.assertEquals(value3Str, output3);
		Assertions.assertEquals(value4Str, output4);
		Assertions.assertEquals(value5Str, output5);
		Assertions.assertEquals(String.format("%s:%s:%s:%s:%s", entity.getValue1(), entity.getValue2(), value3Str, value4Str, value5Str), outputCompiled);
		Assertions.assertEquals(String.format("Template output: %s", outputCompiled), processedTemplate.getOut().toString().trim());
	}


	@Test
	public void testNumericPrefixFields() {
		String template1 = "${1_NUMERIC_FIELD?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config = TemplateConfig.ofTemplate(template1);
		config.addBeanToContext("1_NUMERIC_FIELD", "Value");

		TestUtils.expectException(ValidationException.class, () -> {
			getTemplateConverter().convert(TemplateConfig.ofTemplate(template1));
		}, "Error generating formatted string from config");

		String template2 = "${__1_NUMERIC_FIELD?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config2 = TemplateConfig.ofTemplate(template2);
		config2.addBeanToContext("1_NUMERIC_FIELD", "Value");
		String formattedString = getTemplateConverter().convert(config2);
		Assertions.assertEquals("REPLACED", formattedString);
	}


	@Test
	public void testDashedFields() {
		String template = "${NOT-ESCAPED-DASHES?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config = TemplateConfig.ofTemplate(template);
		config.addBeanToContext("NOT-ESCAPED-DASHES", "Value");
		TestUtils.expectException(ValidationException.class, () -> {
			getTemplateConverter().convert(config);
		}, "Error generating formatted string from config");

		String template2 = "${NOT\\-ESCAPED\\-DASHES?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config2 = TemplateConfig.ofTemplate(template2);
		config2.addBeanToContext("NOT-ESCAPED-DASHES", "Value");
		String formattedString = getTemplateConverter().convert(config2);
		Assertions.assertEquals("REPLACED", formattedString);
	}


	@Test
	public void testColonFields() {
		String template = "${NOT:ESCAPED:COLON?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config = TemplateConfig.ofTemplate(template);
		config.addBeanToContext("NOT:ESCAPED:COLON", "Value");
		TestUtils.expectException(ValidationException.class, () -> {
			getTemplateConverter().convert(config);
		}, "Error generating formatted string from config");

		String template2 = "${NOT\\:ESCAPED\\:COLON?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config2 = TemplateConfig.ofTemplate(template2);
		config2.addBeanToContext("NOT:ESCAPED:COLON", "Value");
		String formattedString = getTemplateConverter().convert(config2);
		Assertions.assertEquals("REPLACED", formattedString);
	}


	@Test
	public void testDottedFields() {
		String template = "${NOT.ESCAPED.DOT?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config = TemplateConfig.ofTemplate(template);
		config.addBeanToContext("NOT.ESCAPED.DOT", "Value");
		TestUtils.expectException(ValidationException.class, () -> {
			getTemplateConverter().convert(config);
		}, "Error generating formatted string from config");

		String template2 = "${NOT\\.ESCAPED\\.DOT?replace(\"Value\",\"REPLACED\")}";
		TemplateConfig config2 = TemplateConfig.ofTemplate(template2);
		config2.addBeanToContext("NOT.ESCAPED.DOT", "Value");
		String formattedString = getTemplateConverter().convert(config2);
		Assertions.assertEquals("REPLACED", formattedString);
	}


	@Test
	public void testExtractFields() {
		String template = "${__1_NUMERIC_FIELD?replace(\"Value\",\"REPLACED\")}";
		Set<String> fields = getTemplateConverter().extractAllDataFields(template);
		Assertions.assertEquals(CollectionUtils.getOnlyElement(fields), "1_NUMERIC_FIELD");
	}

	@Test
	public void testCustomColumnFields() {
		TemplateTestEntity testObject = new TemplateTestEntity();
		testObject.setCustomColumns(new CustomJsonString("{\"field1\":{\"value\":\"1\",\"text\":\"A\"},\"field2\":\"B\",\"field3\":\"{\\\"listValues\\\":[{\\\"listValue\\\":\\\"C\\\"},{\\\"listValue\\\":\\\"D\\\"}]}\"}"));
		String template = "<#list CustomJsonStringUtils.getProperty(testObject.customColumns, \"field3.listValues\") as test>${test.listValue}</#list> ${CustomJsonStringUtils.getColumnText(testObject.customColumns, \"field1\")} ${CustomJsonStringUtils.getColumnText(testObject.customColumns, \"field2\")}";
		TemplateConfig config = TemplateConfig.ofTemplateWithUtilsClasses(template);
		config.addBeanToContext("testObject", testObject);
		String formattedString = getTemplateConverter().processTemplate(config).getOut().toString();
		Assertions.assertEquals("CD A B", formattedString);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static final class TemplateTestEntity {

		private String value1 = "Value 1";
		private int value2 = 2;
		private Date value3 = DateUtils.toDate("11/21/2016");
		private CustomJsonString customColumns;


		public String getValue1() {
			return this.value1;
		}


		public void setValue1(String value1) {
			this.value1 = value1;
		}


		public int getValue2() {
			return this.value2;
		}


		public void setValue2(int value2) {
			this.value2 = value2;
		}


		public Date getValue3() {
			return this.value3;
		}


		public void setValue3(Date value3) {
			this.value3 = value3;
		}

		public CustomJsonString getCustomColumns() {
			return this.customColumns;
		}

		public void setCustomColumns(CustomJsonString customColumns) {
			this.customColumns = customColumns;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public TemplateBuilder getTemplateBuilder() {
		return this.templateBuilder;
	}


	public void setTemplateBuilder(TemplateBuilder templateBuilder) {
		this.templateBuilder = templateBuilder;
	}


	public ReadOnlyDAO<NamedEntity<Integer>> getNamedEntityDAO() {
		return this.namedEntityDAO;
	}


	public void setNamedEntityDAO(ReadOnlyDAO<NamedEntity<Integer>> namedEntityDAO) {
		this.namedEntityDAO = namedEntityDAO;
	}
}
