package com.clifton.core.converter.json;


import com.clifton.core.util.date.Time;


public class TestTime {

	private Time time = Time.parse("02:15:00");
	private Time time2 = Time.parse("12:45:00");
	private Time time3 = Time.parse("15:00:00");


	public Time getTime() {
		return this.time;
	}


	public void setTime(Time time) {
		this.time = time;
	}


	public Time getTime2() {
		return this.time2;
	}


	public void setTime2(Time time2) {
		this.time2 = time2;
	}


	public Time getTime3() {
		return this.time3;
	}


	public void setTime3(Time time3) {
		this.time3 = time3;
	}
}
