package com.clifton.core.converter.json;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;


/**
 * The <code>JsonStringToMapConverterTests</code> class implements unit tests for testing {@link JsonStringToMapConverter} methods.
 *
 * @author vgomelsky
 */
public class JsonStringToMapConverterTests {

	@Test
	@SuppressWarnings("unchecked")
	public void testConvertArrayOfObjectsWithQuotedNameValue() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		Map<String, Object> result = converter.convert("[]");
		Assertions.assertEquals(1, result.size());
		Assertions.assertTrue(result.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME));
		Assertions.assertEquals(0, ((List<?>) result.get(JsonStringToMapConverter.ROOT_ARRAY_NAME)).size());

		// one object with multiple properties: [{"type":"date","value":"08/12/2009"}]
		result = converter.convert("[{\"type\":\"date\",\"value\":\"08/12/2009\"}]");
		Assertions.assertEquals(1, result.size());
		Assertions.assertTrue(result.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME));
		List<Object> arr = (List<Object>) result.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
		Assertions.assertEquals(1, arr.size());
		Map<String, String> bean = (Map<String, String>) arr.get(0);
		Assertions.assertEquals(2, bean.size());
		Assertions.assertEquals("date", bean.get("type"));
		Assertions.assertEquals("08/12/2009", bean.get("value"));

		// one object with one property [{"type":"date"}]
		result = converter.convert("[{\"type\":\"date\"}]");
		Assertions.assertEquals(1, result.size());
		Assertions.assertTrue(result.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME));
		arr = (List<Object>) result.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
		Assertions.assertEquals(1, arr.size());
		bean = (Map<String, String>) arr.get(0);
		Assertions.assertEquals(1, bean.size());
		Assertions.assertEquals("date", bean.get("type"));

		// two objects with one property [{"type":"date"},{"type":"number"}]
		result = converter.convert("[{\"type\":\"date\"},{\"type\":\"number\"}]");
		Assertions.assertEquals(1, result.size());
		Assertions.assertTrue(result.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME));
		arr = (List<Object>) result.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
		Assertions.assertEquals(2, arr.size());
		bean = (Map<String, String>) arr.get(0);
		Assertions.assertEquals(1, bean.size());
		Assertions.assertEquals("date", bean.get("type"));
		bean = (Map<String, String>) arr.get(1);
		Assertions.assertEquals(1, bean.size());
		Assertions.assertEquals("number", bean.get("type"));
	}


	@Test
	public void testConvertObjectOfObjectsWithQuotedNameValue() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();
		Map<String, Object> result = converter.convert("{}");
		Assertions.assertEquals(0, result.size());

		// one object with one property: {"type":"date"}
		result = converter.convert("{\"type\":\"date\"}");
		Assertions.assertEquals(1, result.size());
		Assertions.assertEquals("date", result.get("type"));

		// one object with multiple properties: {"type":"date","value":"08/12/2009"}
		result = converter.convert("{\"type\":\"date\",\"value\":\"08/12/2009\"}");
		Assertions.assertEquals(2, result.size());
		Assertions.assertEquals("date", result.get("type"));
		Assertions.assertEquals("08/12/2009", result.get("value"));
	}


	@Test
	public void testConvertObjectsWithQuotesInsideOfValue() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		Map<String, Object> result = converter.convert("{\"systemSearchAreaName\":\"Contacts\",\"displayBeanFieldName\":\"Orison \\\"Kip\\\" Chaffee (Parametric Clifton)\",\"tooltipBeanFieldName\":null,\"company.name\":\"Parametric Clifton\",\"firstName\":\"Orison \\\"Kip\\\"\",\"lastName\":\"Chaffee\",\"phoneNumber\":\"952-767-7742\"}");
		Assertions.assertEquals(7, result.size());
		Assertions.assertEquals("Orison \"Kip\"", result.get("firstName"));
		Assertions.assertEquals("Chaffee", result.get("lastName"));
		Assertions.assertEquals("952-767-7742", result.get("phoneNumber"));
	}


	@Test
	public void testConvertObjectWithNotQuotedValue() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		// one object with one property: {"number":1}
		Map<String, Object> result = converter.convert("{\"number\":1}");
		Assertions.assertEquals(1, result.size());
		Assertions.assertEquals("1", result.get("number"));

		// one object with one property: {"number":1.234}
		result = converter.convert("{\"number\":1.234}");
		Assertions.assertEquals(1, result.size());
		Assertions.assertEquals("1.234", result.get("number"));

		// one object with multiple properties: {"value":12,"type":"integer","required":true}
		result = converter.convert("{\"value\":12,\"type\":\"integer\",\"required\":true}");
		Assertions.assertEquals(3, result.size());
		Assertions.assertEquals("12", result.get("value"));
		Assertions.assertEquals("integer", result.get("type"));
		Assertions.assertEquals("true", result.get("required"));
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testConvertObjectWithQuotedArrayProperty() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		// {"value":["2"],"field":"auditTypeId"}
		Map<String, Object> result = converter.convert("{\"value\":[\"2\"],\"field\":\"auditTypeId\"}");
		Assertions.assertEquals(2, result.size());
		Assertions.assertEquals("auditTypeId", result.get("field"));
		List<String> list = (List<String>) result.get("value");
		Assertions.assertEquals(1, list.size());
		Assertions.assertEquals("2", list.get(0));

		// {"value":["2","333"],"field":"auditTypeId"}
		result = converter.convert("{\"value\":[\"2\",\"333\"],\"field\":\"auditTypeId\"}");
		Assertions.assertEquals(2, result.size());
		Assertions.assertEquals("auditTypeId", result.get("field"));
		list = (List<String>) result.get("value");
		Assertions.assertEquals(2, list.size());
		Assertions.assertEquals("333", list.get(1));
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testConvertObjectWithNotQuotedArrayProperty() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		// {"value":[2],"field":"auditTypeId"}
		Map<String, Object> result = converter.convert("{\"value\":[2],\"field\":\"auditTypeId\"}");
		Assertions.assertEquals(2, result.size());
		Assertions.assertEquals("auditTypeId", result.get("field"));
		List<String> list = (List<String>) result.get("value");
		Assertions.assertEquals(1, list.size());
		Assertions.assertEquals("2", list.get(0));

		// {"value":[2,-333],"field":"auditTypeId"}
		result = converter.convert("{\"value\":[2,-333],\"field\":\"auditTypeId\"}");
		Assertions.assertEquals(2, result.size());
		Assertions.assertEquals("auditTypeId", result.get("field"));
		list = (List<String>) result.get("value");
		Assertions.assertEquals(2, list.size());
		Assertions.assertEquals("-333", list.get(1));
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testConvertObjectWithChildObject() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		// {"data":{"childObjectField":"value1"},"success":true}
		Map<String, Object> result = converter.convert("{\"data\":{\"childObjectField\":\"value1\"},\"success\":true}");
		Assertions.assertEquals(2, result.size());
		Map<String, Object> dataObject = (Map<String, Object>) result.get("data");
		Assertions.assertNotNull(dataObject);
		Assertions.assertEquals(1, dataObject.size());
		Assertions.assertEquals("value1", dataObject.get("childObjectField"));

		// {"data":{"childObjectField":{"child object 2 field":"value1"}},"success":true}
		result = converter.convert("{\"data\":{\"childObjectField\":{\"child object 2 field\":\"value1\"}},\"success\":true}");
		Assertions.assertEquals(2, result.size());
		dataObject = (Map<String, Object>) result.get("data");
		Assertions.assertNotNull(dataObject);
		Assertions.assertEquals(1, dataObject.size());
		dataObject = (Map<String, Object>) dataObject.get("childObjectField");
		Assertions.assertNotNull(dataObject);
		Assertions.assertEquals("value1", dataObject.get("child object 2 field"));
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testConvertMapWithEmptyKey() {
		JsonStringToMapConverter converter = new JsonStringToMapConverter();

		// [{"":"txt"}]
		Map<String, Object> result = converter.convert("[{\"\":\"txt\"}]");
		List<Object> arr = (List<Object>) result.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
		Assertions.assertEquals(1, arr.size());
		Map<String, String> bean = (Map<String, String>) arr.get(0);
		Assertions.assertEquals(1, bean.entrySet().size());
		Assertions.assertTrue(bean.containsKey(""));
		Assertions.assertEquals("txt", bean.get(""));
	}
}
