package com.clifton.core.converter.json;


public class TestObj2 {

	private String name;
	private TestObj1 test;
	private Integer qty;


	@Override
	public String toString() {
		return "{Object2 = " + this.name + "}";
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the test
	 */
	public TestObj1 getTest() {
		return this.test;
	}


	/**
	 * @param test the test to set
	 */
	public void setTest(TestObj1 test) {
		this.test = test;
	}


	public Integer getQty() {
		return this.qty;
	}


	public void setQty(Integer qty) {
		this.qty = qty;
	}
}
