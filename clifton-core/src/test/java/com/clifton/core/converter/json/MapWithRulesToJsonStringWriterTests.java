package com.clifton.core.converter.json;

import com.clifton.core.converter.json.appenders.ObjectUsingFieldsValueAppender;
import com.clifton.core.util.dataaccess.PagingArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>MapWithRulesToJsonStringWriterTests</code> defines tests for the MapWithRulesToJsonStringWriter class.
 *
 * @author vgomelsky
 */
public class MapWithRulesToJsonStringWriterTests {

	private final MapWithRulesToJsonStringWriter writer = new MapWithRulesToJsonStringWriter();
	private Map<String, Object> testMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		this.testMap = new HashMap<>();
		List<TestObj2> list = new ArrayList<>();
		TestObj2 obj = new TestObj2();
		obj.setName("name1");
		list.add(obj);
		obj = new TestObj2();
		obj.setName("name2");
		TestObj1 obj1 = new TestObj1();
		obj1.setName("object1");
		obj1.setIntArray(new int[]{1, 2, 3});
		obj1.setFlag(true);
		obj1.setDescription("descr1");
		obj.setTest(obj1);
		list.add(obj);
		this.testMap.put("data", new PagingArrayList<>(list, 0, 2, 2));
	}


	@Test
	public void testNullArgs() {
		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(null, null, null));
		Assertions.assertEquals("{}", to.toString());
	}

	@Test
	public void testWithClassPropertyIncluded() {
		MapWithRules mapWithRules = new MapWithRules(this.testMap, 4, true) // without limiting depth, results can get very large
				.withJsonConfigInitializer(jsonConfig -> {
					jsonConfig.setDefaultAppender(new ObjectUsingFieldsValueAppender("ajc$").includeClassProperty()); // exclude properties added by AspectJ
				});
		StringBuilder to = this.writer.write(new StringBuilder(), mapWithRules);
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\",\"class\":\"com.clifton.core.converter.json.TestObj2\"},{\"test\":{\"flag\":true,\"name\":\"object1\",\"description\":\"descr1\",\"class\":\"com.clifton.core.converter.json.TestObj1\",\"intArray\":[1,2,3]},\"name\":\"name2\",\"class\":\"com.clifton.core.converter.json.TestObj2\"}],\"start\":0,\"total\":2}}", to.toString());
	}


	@Test
	public void testWithoutFilters() {
		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, null, null));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"test\":{\"flag\":true,\"name\":\"object1\",\"description\":\"descr1\",\"intArray\":[1,2,3]},\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());
	}


	@Test
	public void testObjectsWithFilters() {
		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", "name"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", "name|test.intArray"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"test\":{\"intArray\":[1,2,3]},\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", "test.intArray"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{},{\"test\":{\"intArray\":[1,2,3]}}],\"start\":0,\"total\":2}}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", "name|test.intArray|test.flag"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"test\":{\"flag\":true,\"intArray\":[1,2,3]},\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());
	}


	@Test
	public void testObjectsWithExcludes() {
		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", "name", ""));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", null, "test.intArray"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"test\":{\"flag\":true,\"name\":\"object1\",\"description\":\"descr1\"},\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", null, "name|test.intArray"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{},{\"test\":{\"flag\":true,\"name\":\"object1\",\"description\":\"descr1\"}}],\"start\":0,\"total\":2}}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(this.testMap, "data.rows", "name|test.intArray|test.flag", "test.flag"));
		Assertions.assertEquals("{\"data\":{\"rows\":[{\"name\":\"name1\"},{\"test\":{\"intArray\":[1,2,3]},\"name\":\"name2\"}],\"start\":0,\"total\":2}}", to.toString());
	}


	@Test
	public void testObjectsWithFilters_Complex() {
		Map<String, Object> map = new HashMap<>();
		List<TestObj2> list = new ArrayList<>();
		map.put("data", list);
		map.put("success", true);

		TestObj2 o2 = new TestObj2();
		list.add(o2);
		o2.setName("name2");
		TestObj1 o1 = new TestObj1();
		o2.setTest(o1);
		o1.setDescription("name1");
		o2 = new TestObj2();
		o1.setRef1(o2);
		o2.setName("name2_2");
		o2 = new TestObj2();
		o1.setRef2(o2);
		o2.setName("name2_3");
		o2.setQty(5);

		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(map, null, null));
		Assertions.assertEquals("{\"data\":[{\"test\":{\"description\":\"name1\",\"ref2\":{\"qty\":5,\"name\":\"name2_3\"},\"ref1\":{\"name\":\"name2_2\"}},\"name\":\"name2\"}],\"success\":true}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(map, "data", "test.ref1.name|test.ref2.name|test.description"));
		Assertions.assertEquals("{\"data\":[{\"test\":{\"description\":\"name1\",\"ref2\":{\"name\":\"name2_3\"},\"ref1\":{\"name\":\"name2_2\"}}}],\"success\":true}", to.toString());
	}


	@Test
	public void testObjectsWithExcludes_Complex() {
		Map<String, Object> map = new HashMap<>();
		List<TestObj2> list = new ArrayList<>();
		map.put("data", list);
		map.put("success", true);

		TestObj2 o2 = new TestObj2();
		list.add(o2);
		o2.setName("name2");
		TestObj1 o1 = new TestObj1();
		o2.setTest(o1);
		o1.setDescription("name1");
		o2 = new TestObj2();
		o1.setRef1(o2);
		o2.setName("name2_2");
		o2 = new TestObj2();
		o1.setRef2(o2);
		o2.setName("name2_3");
		o2.setQty(5);

		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(map, null, null, null));
		Assertions.assertEquals("{\"data\":[{\"test\":{\"description\":\"name1\",\"ref2\":{\"qty\":5,\"name\":\"name2_3\"},\"ref1\":{\"name\":\"name2_2\"}},\"name\":\"name2\"}],\"success\":true}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(map, "data", "test.ref1.name|test.ref2.name|test.description", "test.ref1.name"));
		Assertions.assertEquals("{\"data\":[{\"test\":{\"description\":\"name1\",\"ref2\":{\"name\":\"name2_3\"},\"ref1\":{}}}],\"success\":true}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(map, "data", null, "test.ref1.name"));
		Assertions.assertEquals("{\"data\":[{\"test\":{\"description\":\"name1\",\"ref2\":{\"qty\":5,\"name\":\"name2_3\"},\"ref1\":{}},\"name\":\"name2\"}],\"success\":true}", to.toString());

		to = this.writer.write(new StringBuilder(), new MapWithRules(map, "data", null, "test.ref1.name|name"));
		Assertions.assertEquals("{\"data\":[{\"test\":{\"description\":\"name1\",\"ref2\":{\"qty\":5,\"name\":\"name2_3\"},\"ref1\":{}}}],\"success\":true}", to.toString());
	}


	@Test
	public void testMapsWithFilters() {
		Map<String, Object> obj1 = new HashMap<>();
		obj1.put("key1", 111);
		obj1.put("key2", 222);

		Map<String, Object> map = new HashMap<>();
		map.put("one", obj1);
		map.put("two", "test2");

		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(map, "one", "key1"));
		Assertions.assertEquals("{\"one\":{\"key1\":111},\"two\":\"test2\"}", to.toString());
	}


	@Test
	public void testFilterStartPath() {
		Map<String, Object> obj1 = new HashMap<>();
		obj1.put("key1", 111);
		obj1.put("key2", 222);

		Map<String, Object> map = new HashMap<>();
		map.put("11", "test0");
		map.put("00", obj1);
		map.put("22", "test2");

		StringBuilder to = this.writer.write(new StringBuilder(), new MapWithRules(map, "00", "key1"));
		Assertions.assertEquals("{\"11\":\"test0\",\"00\":{\"key1\":111},\"22\":\"test2\"}", to.toString());
	}


	@Test
	public void testMaxAllowedDepth() {
		Map<String, Object> map = new HashMap<>();

		TestObj1 obj = new TestObj1();
		obj.setRef1(new TestObj2());
		obj.getRef1().setTest(new TestObj1());
		map.put("obj", obj);

		MapWithRules mapWithRules = new MapWithRules(map, null, null, null, null, null, 10, false);
		Assertions.assertEquals("{\"obj\":{\"ref1\":{\"test\":{}}}}", this.writer.write(new StringBuilder(), mapWithRules).toString());

		mapWithRules = new MapWithRules(map, null, null, null, null, null, 2, true);
		Assertions.assertEquals("{\"obj\":{\"ref1\":{}}}", this.writer.write(new StringBuilder(), mapWithRules).toString());

		mapWithRules = new MapWithRules(map, null, null, null, null, null, 2, false);
		try {
			Assertions.assertEquals("{\"obj\":{\"test\":{\"test\":{}}}}", this.writer.write(new StringBuilder(), mapWithRules).toString());
		}
		catch (IllegalStateException e) {
			Assertions.assertEquals("Exceeded max allowed depth of 2 during JSON generation for class com.clifton.core.converter.json.TestObj2\n\n0 = {Object2 = null}\n1 = {Object1 = null}\n",
					e.getMessage());
		}
	}
}
