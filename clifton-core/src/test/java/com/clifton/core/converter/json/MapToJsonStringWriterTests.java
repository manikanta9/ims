package com.clifton.core.converter.json;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.migrate.execution.CalendarDay;
import com.clifton.core.util.dataaccess.PagingArrayList;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>MapToJsonStringWriterTests</code> class tests MapToJsonStringWriter.
 *
 * @author vgomelsky
 */
public class MapToJsonStringWriterTests {

	@Test
	public void testWrite() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();
		Assertions.assertEquals("{}", converter.write(new StringBuilder(), null).toString());
		Assertions.assertEquals("{}", converter.write(new StringBuilder(), map).toString());

		map.put("success", "'true\"");
		Assertions.assertEquals("{\"success\":\"'true\\\"\"}", converter.write(new StringBuilder(), map).toString());

		CalendarDay day = new CalendarDay();
		map.put("day", day);
		Assertions.assertEquals("{\"success\":\"'true\\\"\",\"day\":{\"monthEnd\":false,\"createUserId\":0,\"calendarYearId\":0,\"newBean\":true,\"updateUserId\":0,\"yearEnd\":false,\"calendarMonthId\":0,\"quarterEnd\":false}}",
				converter.write(new StringBuilder(), map).toString());

		day.setId(777L);
		day.setRv(new byte[0]);
		day.setCreateDate(new GregorianCalendar(2009, Calendar.MARCH, 26).getTime());
		Assertions.assertEquals(
				"{\"success\":\"'true\\\"\",\"day\":{\"monthEnd\":false,\"createUserId\":0,\"calendarYearId\":0,\"newBean\":false,\"rv\":[],\"updateUserId\":0,\"yearEnd\":false,\"calendarMonthId\":0,\"quarterEnd\":false,\"identity\":777,\"id\":777,\"createDate\":\"2009-03-26 00:00:00\"}}",
				converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteSpecialChars() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		map.put("success", "string with ' and \" and \n new line or \n\r or \r\n or \r and a backslash \\");
		Assertions.assertEquals("{\"success\":\"string with ' and \\\" and \\n new line or \\n or \\n or \\n and a backslash \\\\\"}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteArray() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		TestObj1 obj = new TestObj1();
		obj.setName("test");
		obj.setIntArray(new int[]{1, 2, 3});
		map.put("obj", obj);
		Assertions.assertEquals("{\"obj\":{\"name\":\"test\",\"intArray\":[1,2,3]}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteFile() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		TestObj1 obj = new TestObj1();
		// File class is a "strange" property: when the bean is described it returns new instances of the File
		obj.setFile(new File(""));
		map.put("obj", obj);
		Assertions.assertTrue(converter.write(new StringBuilder(), map).toString().startsWith("{\"obj\":{\"file\":{\"hidden\":false,\"freeSpace\":0,\"totalSpace\":0,\"usableSpace\":0,\"canonicalFile\":{\"parent\":"));
	}


	@Test
	public void testWriteEnum() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		TestObj1 obj = new TestObj1();
		obj.setName("test");
		obj.setEnumValue(TestEnum.VALUE1);
		map.put("obj", obj);
		Assertions.assertEquals("{\"obj\":{\"enumValue\":\"VALUE1\",\"name\":\"test\"}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteCollection() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		ArrayList<Object> list = new ArrayList<>();
		map.put("list", list);
		Assertions.assertEquals("{\"list\":[]}", converter.write(new StringBuilder(), map).toString());

		list.add("test1");
		Assertions.assertEquals("{\"list\":[\"test1\"]}", converter.write(new StringBuilder(), map).toString());

		list.add("test2");
		Assertions.assertEquals("{\"list\":[\"test1\",\"test2\"]}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteDataTableSingleColumn() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		DataColumn[] columnList = new DataColumn[1];
		columnList[0] = new DataColumnImpl("ID", 4);
		DataTable dataTable = new PagingDataTableImpl(columnList, 0, 0);
		map.put("dataTable", dataTable);

		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":0,\"columns\":[\"ID\"],\"rows\":[]}}", converter.write(new StringBuilder(), map).toString());

		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{123}));
		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":1,\"columns\":[\"ID\"],\"rows\":[[123]]}}", converter.write(new StringBuilder(), map).toString());

		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{45}));
		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":2,\"columns\":[\"ID\"],\"rows\":[[123],[45]]}}", converter.write(new StringBuilder(), map).toString());

		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{null}));
		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":3,\"columns\":[\"ID\"],\"rows\":[[123],[45],[null]]}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteDataTableMultiColumn() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		DataColumn[] columnList = new DataColumn[4];
		columnList[0] = new DataColumnImpl("ID", 4);
		columnList[1] = new DataColumnImpl("Boolean", -7);
		columnList[2] = new DataColumnImpl("Date", 91);
		columnList[3] = new DataColumnImpl("String", 12);
		DataTable dataTable = new PagingDataTableImpl(columnList, 0, 0);
		map.put("dataTable", dataTable);

		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":0,\"columns\":[\"ID\",\"Boolean\",\"Date\",\"String\"],\"rows\":[]}}", converter.write(new StringBuilder(), map).toString());

		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{123, true, new GregorianCalendar(2009, Calendar.MARCH, 26).getTime(), "test"}));
		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":1,\"columns\":[\"ID\",\"Boolean\",\"Date\",\"String\"],\"rows\":[[123,true,\"2009-03-26 00:00:00\",\"test\"]]}}", converter.write(new StringBuilder(), map).toString());

		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{45, false, null, "{a=b}"}));
		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":2,\"columns\":[\"ID\",\"Boolean\",\"Date\",\"String\"],\"rows\":[[123,true,\"2009-03-26 00:00:00\",\"test\"],[45,false,null,\"{a=b}\"]]}}", converter.write(new StringBuilder(), map).toString());

		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{15, null, null, null}));
		Assertions.assertEquals("{\"dataTable\":{\"firstIndex\":0,\"totalRows\":3,\"columns\":[\"ID\",\"Boolean\",\"Date\",\"String\"],\"rows\":[[123,true,\"2009-03-26 00:00:00\",\"test\"],[45,false,null,\"{a=b}\"],[15,null,null,null]]}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWritePagingArrayList() {
		MapToJsonStringWriter writer = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		PagingArrayList<String> list = new PagingArrayList<>();
		list.add("one");
		list.add("two");
		list.add(null);
		map.put("list", list);

		Assertions.assertEquals("{\"list\":{\"rows\":[\"one\",\"two\"],\"start\":0,\"total\":2}}", writer.write(new StringBuilder(), map).toString());

		writer.getJsonConfig(null).setSkipNullFields(false);
		Assertions.assertEquals("{\"list\":{\"rows\":[\"one\",\"two\",null],\"start\":0,\"total\":3}}", writer.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteSameObject() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		CalendarDay day = new CalendarDay();
		map.put("day1", day);
		map.put("day2", day);
		String dayJson = "{\"monthEnd\":false,\"createUserId\":0,\"calendarYearId\":0,\"newBean\":true,\"updateUserId\":0,\"yearEnd\":false,\"calendarMonthId\":0,\"quarterEnd\":false}";
		Assertions.assertEquals("{\"day2\":" + dayJson + ",\"day1\":" + dayJson + "}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteCircularReferences() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		TestObj1 o1 = new TestObj1();
		o1.setName("111");
		TestObj2 o2 = new TestObj2();
		o2.setName("222");
		o2.setTest(o1);
		o1.setRef1(o2);
		map.put("obj1", o1);

		Assertions.assertEquals("{\"obj1\":{\"name\":\"111\",\"ref1\":{\"name\":\"222\"}}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteCircularReferencesFromArray() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		TestObj1 o1 = new TestObj1();
		o1.setName("111");
		TestObj2 o2 = new TestObj2();
		o2.setName("222");
		o1.setObjectArray(new Object[]{o1, o2});
		map.put("obj1", o1);

		Assertions.assertEquals("{\"obj1\":{\"objectArray\":[{\"name\":\"222\"}],\"name\":\"111\"}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteClassArrayElement() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		map.put("classArray", new Class<?>[]{Integer.class, Boolean.class});

		Assertions.assertEquals("{\"classArray\":[\"java.lang.Integer\",\"java.lang.Boolean\"]}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteStringWithParenthesis() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();

		map.put("data", new NamedEntity<Integer>("Entity # 1", "Entity # 1 (Description)"));
		Assertions.assertEquals("{\"data\":{\"createUserId\":0,\"newBean\":true,\"updateUserId\":0,\"name\":\"Entity # 1\",\"description\":\"Entity # 1 (Description)\",\"label\":\"Entity # 1\"}}", converter.write(new StringBuilder(), map).toString());
	}


	@Test
	public void testWriteTime() {
		MapToJsonStringWriter converter = new MapToJsonStringWriter();
		Map<String, Object> map = new HashMap<>();
		Assertions.assertEquals("{}", converter.write(new StringBuilder(), null).toString());
		Assertions.assertEquals("{}", converter.write(new StringBuilder(), map).toString());

		map.put("success", "'true\"");
		Assertions.assertEquals("{\"success\":\"'true\\\"\"}", converter.write(new StringBuilder(), map).toString());

		TestTime day = new TestTime();
		map.put("time", day);
		Assertions.assertEquals("{\"success\":\"'true\\\"\",\"time\":{\"time2\":\"12:45 PM\",\"time3\":\"3:00 PM\",\"time\":\"2:15 AM\"}}", converter.write(new StringBuilder(), map).toString());
	}
}
