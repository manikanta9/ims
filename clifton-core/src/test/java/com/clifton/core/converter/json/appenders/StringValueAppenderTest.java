package com.clifton.core.converter.json.appenders;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * Test class to verify {@link StringValueAppender} serialization results.
 *
 * @author MikeH
 */
public class StringValueAppenderTest {

	private static final String NO_SPECIAL_CHARS_STRING = "There are no special characters in this string.";
	private static final String ALL_SPECIAL_CHARS_STRING = ""
			+ "nothing: ||;"
			+ "backslash: |\\|;"
			+ "backspace: |\b|;"
			+ "formfeed: |\f|;"
			+ "double-quote: |\"|;"
			+ "solidus: |/|;"
			+ "tab: |\t|;"
			+ "CR: |\r|;"
			+ "double CR: |\r\r|;"
			+ "CR NL: |\r\n|;"
			+ "CR CR NL: |\r\r\n|;"
			+ "CR NL CR: |\r\n\r|;"
			+ "CR CR CR: |\r\r\r|;"
			+ "NL: |\n|;"
			+ "NL CR: |\n\r|;"
			+ "NL NL: |\n\n|;"
			+ "NL NL CR: |\n\n\r|;"
			+ "NL CR NL: |\n\r\n|;"
			+ "NL NL NL: |\n\n\n|;";
	private static final String ALL_SPECIAL_CHARS_STRING_ESCAPED = ""
			+ "nothing: ||;"
			+ "backslash: |\\\\|;"
			+ "backspace: |\\b|;"
			+ "formfeed: |\\f|;"
			+ "double-quote: |\\\"|;"
			+ "solidus: |\\/|;"
			+ "tab: |\\t|;"
			+ "CR: |\\n|;"
			+ "double CR: |\\n\\n|;"
			+ "CR NL: |\\n|;"
			+ "CR CR NL: |\\n\\n|;"
			+ "CR NL CR: |\\n\\n|;"
			+ "CR CR CR: |\\n\\n\\n|;"
			+ "NL: |\\n|;"
			+ "NL CR: |\\n|;"
			+ "NL NL: |\\n\\n|;"
			+ "NL NL CR: |\\n\\n|;"
			+ "NL CR NL: |\\n\\n|;"
			+ "NL NL NL: |\\n\\n\\n|;";

	private StringValueAppender stringValueAppender = new StringValueAppender();


	@Test
	public void testNoEscapedCharacters() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testNoEscapedCharactersNoWrap() {
		String expected = getWrappedString();
		String actual = getSanitizedString();
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedBackslash() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\\\", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\\", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedBackslashNoWrap() {
		String expected = getWrappedString("\\\\");
		String actual = getSanitizedString("\\");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedBackspace() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\b", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\b", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedBackspaceNoWrap() {
		String expected = getWrappedString("\\b");
		String actual = getSanitizedString("\b");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedFormfeed() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\f", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\f", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedFormfeedNoWrap() {
		String expected = getWrappedString("\\f");
		String actual = getSanitizedString("\f");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedDoubleQuote() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\\"", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\"", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedDoubleQuoteNoWrap() {
		String expected = getWrappedString("\\\"");
		String actual = getSanitizedString("\"");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedSolidus() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\/", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "/", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedSolidusNoWrap() {
		String expected = getWrappedString("\\/");
		String actual = getSanitizedString("/");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedTab() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\t", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\t", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedTabNoWrap() {
		String expected = getWrappedString("\\t");
		String actual = getSanitizedString("\t");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCR() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\r", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRNoWrap() {
		String expected = getWrappedString("\\n");
		String actual = getSanitizedString("\r");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedDoubleCR() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\r\r", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedDoubleCRNoWrap() {
		String expected = getWrappedString("\\n\\n");
		String actual = getSanitizedString("\r\r");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRNL() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\r\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRNLNoWrap() {
		String expected = getWrappedString("\\n");
		String actual = getSanitizedString("\r\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRCRNL() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\r\r\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRCRNLNoWrap() {
		String expected = getWrappedString("\\n\\n");
		String actual = getSanitizedString("\r\r\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRNLCR() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\r\n\r", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRNLCRNoWrap() {
		String expected = getWrappedString("\\n\\n");
		String actual = getSanitizedString("\r\n\r");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRCRCR() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\r\r\r", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedCRCRCRNoWrap() {
		String expected = getWrappedString("\\n\\n\\n");
		String actual = getSanitizedString("\r\r\r");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNL() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNoWrap() {
		String expected = getWrappedString("\\n");
		String actual = getSanitizedString("\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLCR() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLCRNoWrap() {
		String expected = getWrappedString("\\n");
		String actual = getSanitizedString("\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNL() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\n\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNLNoWrap() {
		String expected = getWrappedString("\\n\\n");
		String actual = getSanitizedString("\n\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNLCR() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\n\n\r", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNLCRNoWrap() {
		String expected = getWrappedString("\\n\\n");
		String actual = getSanitizedString("\n\n\r");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLCRNL() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\n\r\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLCRNLNoWrap() {
		String expected = getWrappedString("\\n\\n");
		String actual = getSanitizedString("\n\r\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNLNL() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, "\\n\\n\\n", NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, "\n\n\n", NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedNLNLNLNoWrap() {
		String expected = getWrappedString("\\n\\n\\n");
		String actual = getSanitizedString("\n\n\n");
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedAll() {
		String expected = getWrappedString(NO_SPECIAL_CHARS_STRING, ALL_SPECIAL_CHARS_STRING_ESCAPED, NO_SPECIAL_CHARS_STRING);
		String actual = getSanitizedString(NO_SPECIAL_CHARS_STRING, ALL_SPECIAL_CHARS_STRING, NO_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	@Test
	public void testEscapedAllNoWrap() {
		String expected = getWrappedString(ALL_SPECIAL_CHARS_STRING_ESCAPED);
		String actual = getSanitizedString(ALL_SPECIAL_CHARS_STRING);
		Assertions.assertEquals(expected, actual);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getSanitizedString(String... values) {
		StringBuilder stringBuilder = new StringBuilder();
		try {
			this.stringValueAppender.append(stringBuilder, null, String.join("", values), null, null);
		}
		catch (Exception e) {
			throw new RuntimeException("An exception occurred during serialization.", e);
		}
		return stringBuilder.toString();
	}


	private String getWrappedString(String... values) {
		return "\"" + String.join("", values) + "\"";
	}
}
