package com.clifton.core.converter.reversable;

import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class BooleanReversableConverterTests {

	@Test
	public void testDefaultConverter() {
		BooleanReversableConverter converter = new BooleanReversableConverter();

		Assertions.assertFalse(converter.convert("false"));
		Assertions.assertTrue(converter.convert("true"));

		Assertions.assertEquals("TRUE", converter.reverseConvert(true));
		Assertions.assertEquals("FALSE", converter.reverseConvert(false));

		try {
			converter.convert("unmatched");
		}
		catch (ValidationException e) {
			Assertions.assertEquals("Unable to convert unmatched to a boolean value because it is not set up as a true or false value.  Valid True Values [TRUE], Valid False Values [FALSE].",
					e.getMessage());

			// Set to return false if unmatched
			converter.setFalseIfValueUnmatched(true);
			Assertions.assertFalse(converter.convert("unmatched"));
			return;
		}
		Assertions.fail("Conversion should have thrown a ValidationException because not 'isFalseIfUnmatched' and not explicit for 'unmatched' value");
	}


	@Test
	public void testConverterWithListValues() {
		BooleanReversableConverter converter = new BooleanReversableConverter();
		List<String> trueValues = new ArrayList<>();
		List<String> falseValues = new ArrayList<>();

		trueValues.add("Buy");
		trueValues.add("B");
		trueValues.add("true");
		falseValues.add("Sell");
		falseValues.add("S");
		falseValues.add("false");

		converter.setTrueValueList(trueValues);
		converter.setFalseValueList(falseValues);
		converter.setDefaultFalseValue("S");
		converter.setDefaultTrueValue("B");

		Assertions.assertFalse(converter.convert("Sell"));
		Assertions.assertFalse(converter.convert("SELL"));
		Assertions.assertFalse(converter.convert("S"));
		Assertions.assertFalse(converter.convert("false"));

		Assertions.assertTrue(converter.convert("true"));
		Assertions.assertTrue(converter.convert("BUY"));
		Assertions.assertTrue(converter.convert("buy"));
		Assertions.assertTrue(converter.convert("b"));

		Assertions.assertEquals("B", converter.reverseConvert(true));
		Assertions.assertEquals("S", converter.reverseConvert(false));

		try {
			converter.convert("selling");
		}
		catch (ValidationException e) {
			Assertions.assertEquals(
					"Unable to convert selling to a boolean value because it is not set up as a true or false value.  Valid True Values [Buy, B, true], Valid False Values [Sell, S, false].",
					e.getMessage());

			// Set to return false if unmatched
			converter.setFalseIfValueUnmatched(true);
			Assertions.assertFalse(converter.convert("selling"));
			return;
		}
		Assertions.fail("Conversion should have thrown a ValidationException because not 'isFalseIfUnmatched' and not explicit for 'selling' value");
	}
}
