package com.clifton.core.converter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>SimpleConverterTests</code> class tests {@link SimpleConverter}
 *
 * @author vgomelsky
 */
public class SimpleConverterTests {

	@Test
	public void testConvert() {
		SimpleConverter<String> converter = new SimpleConverter<>();
		Assertions.assertNull(converter.convert(null));
		Assertions.assertEquals("test", converter.convert("test"));
	}
}
