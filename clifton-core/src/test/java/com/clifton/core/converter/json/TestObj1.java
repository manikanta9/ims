package com.clifton.core.converter.json;


import java.io.File;


public class TestObj1 {

	private String name;
	private String description;
	private TestEnum enumValue;
	private TestObj2 ref1;
	private TestObj2 ref2;
	private int[] intArray;
	private Object[] objectArray;
	private File file;
	private Boolean flag;


	@Override
	public String toString() {
		return "{Object1 = " + this.name + "}";
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the enumValue
	 */
	public TestEnum getEnumValue() {
		return this.enumValue;
	}


	/**
	 * @param enumValue the enumValue to set
	 */
	public void setEnumValue(TestEnum enumValue) {
		this.enumValue = enumValue;
	}


	/**
	 * @return the intArray
	 */
	public int[] getIntArray() {
		return this.intArray;
	}


	/**
	 * @param intArray the intArray to set
	 */
	public void setIntArray(int[] intArray) {
		this.intArray = intArray;
	}


	/**
	 * @return the objectArray
	 */
	public Object[] getObjectArray() {
		return this.objectArray;
	}


	/**
	 * @param objectArray the objectArray to set
	 */
	public void setObjectArray(Object[] objectArray) {
		this.objectArray = objectArray;
	}


	public File getFile() {
		return this.file;
	}


	public void setFile(File file) {
		this.file = file;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Boolean getFlag() {
		return this.flag;
	}


	public void setFlag(Boolean flag) {
		this.flag = flag;
	}


	public TestObj2 getRef1() {
		return this.ref1;
	}


	public void setRef1(TestObj2 ref1) {
		this.ref1 = ref1;
	}


	public TestObj2 getRef2() {
		return this.ref2;
	}


	public void setRef2(TestObj2 ref2) {
		this.ref2 = ref2;
	}
}
