package com.clifton.core.cache.specificity;

import com.clifton.core.cache.specificity.key.SimpleOrderingSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
public class SimpleOrderingSpecificityKeyGeneratorTest {

	private static List<String> SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS;
	private static List<String> SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS;


	static {
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS = new ArrayList<>();
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;0-T;0-ST2;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;0-T;0-ST2;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;0-T;1-null;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;0-T;1-null;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;1-null;0-ST2;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;1-null;0-ST2;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;1-null;1-null;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("0-H;1-null;1-null;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;0-T;0-ST2;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;0-T;0-ST2;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;0-T;1-null;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;0-T;1-null;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;1-null;0-ST2;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;1-null;0-ST2;1-null;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;1-null;1-null;0-ST;InvestmentSecurity");
		SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.add("1-null;1-null;1-null;1-null;InvestmentSecurity");


		SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS = new ArrayList<>();
		SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS.add("0-H;InvestmentSecurity");
		SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS.add("1-ST2;InvestmentSecurity");
		SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS.add("2-ST;InvestmentSecurity");
		SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS.add("3-T;InvestmentSecurity");
		SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS.add("4-null;InvestmentSecurity");
	}


	@Test
	public void simpleOrderingSpecificityKeyGeneratorMultipleHierarchyTest() {
		SimpleOrderingSpecificityKeyGenerator generator = new SimpleOrderingSpecificityKeyGenerator();


		Object[] hierarchy = {"H", null};
		Object[] investmentTypeIdHierarchy = {"T", null};
		Object[] investmentTypeSubType2IdHierarchy = {"ST2", null};
		Object[] investmentTypeSubTypeIdHierarchy = {"ST", null};
		Object[] valuesToAppend = {"InvestmentSecurity"};


		SpecificityKeySource keySource = SpecificityKeySource.builder(hierarchy).addHierarchy(investmentTypeIdHierarchy)
				.addHierarchy(investmentTypeSubType2IdHierarchy)
				.addHierarchy(investmentTypeSubTypeIdHierarchy).setValuesToAppend(valuesToAppend).build();

		for (int i = 0; i < keySource.getNumPossibleKeys(); i++) {
			String key = generator.generateKeyForSource(i, keySource);
			Assertions.assertEquals(SIMPLE_ORDERING_MULTIPLE_HIERARCHIES_GENERATOR_RESULTS.get(i), key);
		}
	}


	@Test
	public void simpleOrderingSpecificityKeyGeneratorHierarchyTest() {
		SimpleOrderingSpecificityKeyGenerator generator = new SimpleOrderingSpecificityKeyGenerator();


		Object[] hierarchy = {"H", "ST2", "ST", "T", null};
		Object[] valuesToAppend = {"InvestmentSecurity"};


		SpecificityKeySource keySource = SpecificityKeySource.builder(hierarchy).setValuesToAppend(valuesToAppend).build();

		for (int i = 0; i < keySource.getNumPossibleKeys(); i++) {
			String key = generator.generateKeyForSource(i, keySource);
			Assertions.assertEquals(SIMPLE_ORDERING_SINGLE_HIERARCHY_GENERATOR_RESULTS.get(i), key);
		}
	}
}
