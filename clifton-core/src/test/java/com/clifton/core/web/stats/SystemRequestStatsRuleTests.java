package com.clifton.core.web.stats;


import com.clifton.core.web.stats.SystemRequestStatsUriRule.RuleEffect;
import com.clifton.core.web.stats.SystemRequestStatsUriRule.RulePatternType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpStatus;
import org.springframework.mock.web.MockHttpServletResponse;

import javax.servlet.http.HttpServletResponse;
import java.util.Collections;


/**
 * @author MikeH
 */
public class SystemRequestStatsRuleTests {

	@Test
	public void testMatches() {
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/private/", RulePatternType.PREFIX, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches("/private/"));
			Assertions.assertTrue(rule.matches("/private/mapping"));
			Assertions.assertTrue(rule.matches("/private//mapping"));
			Assertions.assertFalse(rule.matches(""));
			Assertions.assertFalse(rule.matches("/"));
			Assertions.assertFalse(rule.matches("/private"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/", RulePatternType.PREFIX, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches("/"));
			Assertions.assertTrue(rule.matches("/mapping"));
			Assertions.assertTrue(rule.matches("//mapping"));
			Assertions.assertFalse(rule.matches(""));
			Assertions.assertFalse(rule.matches("mapping"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("", RulePatternType.PREFIX, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches("/"));
			Assertions.assertTrue(rule.matches("/mapping"));
			Assertions.assertTrue(rule.matches("//mapping"));
			Assertions.assertTrue(rule.matches(""));
			Assertions.assertTrue(rule.matches("mapping"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule(".json", RulePatternType.SUFFIX, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches(".json"));
			Assertions.assertTrue(rule.matches("/.json"));
			Assertions.assertTrue(rule.matches("/mapping/.json"));
			Assertions.assertTrue(rule.matches("//mapping//path.json"));
			Assertions.assertFalse(rule.matches(""));
			Assertions.assertFalse(rule.matches("/"));
			Assertions.assertFalse(rule.matches("/json"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("", RulePatternType.SUFFIX, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches(".json"));
			Assertions.assertTrue(rule.matches("/.json"));
			Assertions.assertTrue(rule.matches("/mapping/.json"));
			Assertions.assertTrue(rule.matches("//mapping//path.json"));
			Assertions.assertTrue(rule.matches(""));
			Assertions.assertTrue(rule.matches("/"));
			Assertions.assertTrue(rule.matches("/json"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/path/", RulePatternType.EXACT, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches("/path/"));
			Assertions.assertFalse(rule.matches("/mapping/"));
			Assertions.assertFalse(rule.matches("//mapping//path/"));
			Assertions.assertFalse(rule.matches(""));
			Assertions.assertFalse(rule.matches("/"));
			Assertions.assertFalse(rule.matches("/json"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("", RulePatternType.EXACT, RuleEffect.HIDE);
			Assertions.assertTrue(rule.matches(""));
			Assertions.assertFalse(rule.matches("/mapping/"));
			Assertions.assertFalse(rule.matches("//mapping//path/"));
			Assertions.assertFalse(rule.matches("/"));
			Assertions.assertFalse(rule.matches("/json"));
		}
	}


	@Test
	public void testApply() {
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/private/", RulePatternType.PREFIX, RuleEffect.HIDE);
			Assertions.assertNull(rule.apply("/private/"));
			Assertions.assertNull(rule.apply("/private/mapping"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule(".json", RulePatternType.SUFFIX, RuleEffect.HIDE);
			Assertions.assertNull(rule.apply("/mapping.json"));
			Assertions.assertNull(rule.apply("/private/mapping.json"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/path/", RulePatternType.EXACT, RuleEffect.HIDE);
			Assertions.assertNull(rule.apply("/path/"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/private/", RulePatternType.PREFIX, RuleEffect.PATTERN_ONLY);
			Assertions.assertEquals("/private/", rule.apply("/private/"));
			Assertions.assertEquals("/private/", rule.apply("/private/mapping"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/private/", RulePatternType.SUFFIX, RuleEffect.PATTERN_ONLY);
			Assertions.assertEquals("/private/", rule.apply("/private/"));
			Assertions.assertEquals("/private/", rule.apply("/mapping/private/"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/path/", RulePatternType.EXACT, RuleEffect.PATTERN_ONLY);
			Assertions.assertEquals("/path/", rule.apply("/path/"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/private/", RulePatternType.PREFIX, RuleEffect.TRIMMED);
			Assertions.assertEquals("", rule.apply("/private/"));
			Assertions.assertEquals("mapping", rule.apply("/private/mapping"));
			Assertions.assertEquals("other-mapping", rule.apply("/private/other-mapping"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/private/", RulePatternType.SUFFIX, RuleEffect.TRIMMED);
			Assertions.assertEquals("", rule.apply("/private/"));
			Assertions.assertEquals("/mapping", rule.apply("/mapping/private/"));
			Assertions.assertEquals("/other-mapping", rule.apply("/other-mapping/private/"));
		}
		{
			SystemRequestStatsUriRule rule = new SystemRequestStatsUriRule("/path/", RulePatternType.EXACT, RuleEffect.TRIMMED);
			Assertions.assertEquals("", rule.apply("/path/"));
		}
	}


	@Test
	public void testExcludeStatusCode() {
		HttpServletResponse responseNotFound = new MockHttpServletResponse();
		responseNotFound.setStatus(HttpStatus.NOT_FOUND.value());

		HttpServletResponse responseOk = new MockHttpServletResponse();
		responseOk.setStatus(HttpStatus.OK.value());

		{
			SystemRequestStatsStatusExcludeRule rule = new SystemRequestStatsStatusExcludeRule(Collections.singletonList(HttpStatus.NOT_FOUND.value()));
			Assertions.assertFalse(rule.matches(responseNotFound));
			Assertions.assertTrue(rule.matches(responseOk));
		}
	}
}
