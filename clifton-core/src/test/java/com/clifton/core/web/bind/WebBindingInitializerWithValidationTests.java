package com.clifton.core.web.bind;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.web.WebTestService;
import com.clifton.core.web.mvc.HttpServletResponseImpl;
import com.clifton.core.web.mvc.ValidatingRequestMappingHandlerAdapter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.beans.PropertyEditor;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Hashtable;


/**
 * The <code>WebBindingInitializerWithValidationTests</code> class tests bean binding.
 *
 * @author vgomelsky
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class WebBindingInitializerWithValidationTests {

	@Resource
	private ValidatingRequestMappingHandlerAdapter webMethodHandlerAdapter;
	@Resource
	private WebTestService webTestService;


	@Test
	public void testInitBinder() throws Exception {
		Assertions.assertNotNull(this.webMethodHandlerAdapter);

		HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
		Mockito.when(request.getContextPath()).thenReturn("/");
		Mockito.when(request.getServletPath()).thenReturn("/namedEntityTestObjectSave.json");
		Mockito.when(request.getRequestURI()).thenReturn("/namedEntityTestObjectSave.json");

		// insert a new bean (id = 1)
		Mockito.when(request.getParameterValues("name")).thenReturn(new String[]{"initial name"});
		Mockito.when(request.getParameterValues("label")).thenReturn(new String[]{"initial label"});
		@SuppressWarnings("UseOfObsoleteCollectionType")
		Hashtable<String, String> names = new Hashtable<>();
		names.put("name", "name");
		names.put("label", "label");
		Mockito.when(request.getParameterNames()).thenReturn(names.keys());

		HandlerMethod saveMethod = new HandlerMethod(this.webTestService, MethodUtils.getMethod(this.webTestService.getClass(), "saveNamedEntityTestObject", true));
		ModelAndView result = this.webMethodHandlerAdapter.handle(request, new HttpServletResponseImpl(request), saveMethod);
		Assertions.assertNotNull(result);
		NamedEntityTestObject bean = (NamedEntityTestObject) result.getModel().get("namedEntityTestObject");
		Assertions.assertNotNull(bean);
		Assertions.assertEquals(1, (int) bean.getId());
		Assertions.assertEquals("initial name", bean.getName());
		Assertions.assertEquals("initial label", bean.getLabel());
		Assertions.assertNull(bean.getDescription());

		// update the name of the bean inserted in previous step
		Mockito.when(request.getParameterValues("id")).thenReturn(new String[]{"1"});
		Mockito.when(request.getParameterValues("name")).thenReturn(new String[]{"new value for name"});
		Mockito.when(request.getParameterValues("description")).thenReturn(new String[]{""});
		Mockito.when(request.getParameterValues("rv")).thenReturn(new String[]{""});
		names.put("id", "id");
		names.put("description", "description");
		names.put("rv", "rv");
		Mockito.when(request.getParameterNames()).thenReturn(names.keys());

		result = this.webMethodHandlerAdapter.handle(request, new HttpServletResponseImpl(request), saveMethod);
		Assertions.assertNotNull(result);
		bean = (NamedEntityTestObject) result.getModel().get("namedEntityTestObject");
		Assertions.assertNotNull(bean);
		Assertions.assertEquals(1, (int) bean.getId());
		Assertions.assertEquals("new value for name", bean.getName());
		Assertions.assertEquals("initial label", bean.getLabel());
		Assertions.assertNull(bean.getDescription());

		// now read the bean and make sure it's still the same
		bean = this.webTestService.getNamedEntityTestObject(1);
		Assertions.assertNotNull(bean);
		Assertions.assertEquals(1, (int) bean.getId());
		Assertions.assertEquals("new value for name", bean.getName());
		Assertions.assertEquals("initial label", bean.getLabel());
		Assertions.assertNull(bean.getDescription());
	}


	@Test
	public void testCustomEditors() {
		PropertyEditor propertyEditor = new CustomNumberEditor(BigDecimal.class, NumberFormat.getInstance(), true);

		propertyEditor.setAsText("100");
		Assertions.assertEquals(new BigDecimal("100"), propertyEditor.getValue());

		propertyEditor.setAsText("100,200");
		Assertions.assertEquals(new BigDecimal("100200"), propertyEditor.getValue());

		propertyEditor.setAsText("10.12");
		Assertions.assertEquals(new BigDecimal("10.12"), propertyEditor.getValue());
	}
}
