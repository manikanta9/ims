package com.clifton.core.web.view;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The test class for {@link SizeLimitedAppender} operations.
 *
 * @author MikeH
 */
public class SizeLimitedAppenderTests {

	private static final String STRING_100 = new String(new char[100]).replace('\0', '0');
	private static final String STRING_500 = new String(new char[500]).replace('\0', '0');


	@Test
	public void testSizeLimitedAppender() throws Exception {
		Assertions.assertEquals(STRING_100,
				new SizeLimitedAppender(new StringBuilder(), 100)
						.append(STRING_100)
						.getOut().toString());
		Assertions.assertEquals(STRING_100.substring(0, 99) + 'a',
				new SizeLimitedAppender(new StringBuilder(), 100)
						.append(STRING_100, 0, 99)
						.append('a')
						.getOut().toString());
		Assertions.assertEquals("prefix" + STRING_100 + STRING_100 + "suffix",
				new SizeLimitedAppender(new StringBuilder(), 300)
						.append("prefix")
						.append(STRING_100)
						.append(STRING_100)
						.append("suffix")
						.getOut().toString());
		Assertions.assertEquals("llo",
				new SizeLimitedAppender(new StringBuilder(), 300)
						.append("hello world", 2, 5)
						.getOut().toString());
		Assertions.assertEquals(STRING_100 + STRING_100 + STRING_100,
				new SizeLimitedAppender(new StringBuilder(), 300)
						.append(STRING_500, 0, 300)
						.getOut().toString());
	}


	@Test
	public void testSizeLimitedAppenderSizeExceededSingleCharacter() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			new SizeLimitedAppender(new StringBuilder(), 100)
					.append(STRING_100, 0, 99)
					.append('a')
					.append('b');
			Assertions.fail();
		});
		Assertions.assertEquals("The current attempted written length [101] exceeds the maximum allowed written length [100].", runtimeException.getMessage());
	}


	@Test
	public void testSizeLimitedAppenderSizeExceededSingleCharacterFromMax() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			new SizeLimitedAppender(new StringBuilder(), 100)
					.append(STRING_100)
					.append('a');
			Assertions.fail();
		});
		Assertions.assertEquals("The current attempted written length [101] exceeds the maximum allowed written length [100].", runtimeException.getMessage());
	}


	@Test
	public void testSizeLimitedAppenderSizeExceededString() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			new SizeLimitedAppender(new StringBuilder(), 100)
					.append(STRING_100, 0, 99)
					.append(STRING_100);
			Assertions.fail();
		});
		Assertions.assertEquals("The current attempted written length [199] exceeds the maximum allowed written length [100].", runtimeException.getMessage());
	}


	@Test
	public void testSizeLimitedAppenderSizeExceededStringFromMax() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			new SizeLimitedAppender(new StringBuilder(), 100)
					.append(STRING_100)
					.append(STRING_100);
			Assertions.fail();
		});
		Assertions.assertEquals("The current attempted written length [200] exceeds the maximum allowed written length [100].", runtimeException.getMessage());
	}


	@Test
	public void testSizeLimitedAppenderSizeExceededBulk() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			new SizeLimitedAppender(new StringBuilder(), 100)
					.append(STRING_500);
			Assertions.fail();
		});
		Assertions.assertEquals("The current attempted written length [500] exceeds the maximum allowed written length [100].", runtimeException.getMessage());
	}
}
