package com.clifton.core.web.bind;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntityTestObject;

import java.io.Serializable;


public class TestBean implements IdentityObject {

	private String name;
	private NamedEntityTestObject namedEntity;

	private NamedEntityTestObject namedEntitySecond;


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the namedEntity
	 */
	public NamedEntityTestObject getNamedEntity() {
		return this.namedEntity;
	}


	/**
	 * @param namedEntity the namedEntity to set
	 */
	public void setNamedEntity(NamedEntityTestObject namedEntity) {
		this.namedEntity = namedEntity;
	}


	@Override
	public Serializable getIdentity() {
		return null;
	}


	@Override
	public boolean isNewBean() {
		return false;
	}


	public NamedEntityTestObject getNamedEntitySecond() {
		return this.namedEntitySecond;
	}


	public void setNamedEntitySecond(NamedEntityTestObject namedEntitySecond) {
		this.namedEntitySecond = namedEntitySecond;
	}
}
