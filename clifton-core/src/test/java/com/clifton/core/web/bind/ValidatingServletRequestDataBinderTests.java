package com.clifton.core.web.bind;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;

import java.util.ListIterator;
import java.util.function.Consumer;


public class ValidatingServletRequestDataBinderTests {

	@Test
	public void testPrepareBeanWithCompositeProperties() {
		MutablePropertyValues mpvs = new MutablePropertyValues();
		mpvs.addPropertyValue("name", "Clifton");
		mpvs.addPropertyValue("namedEntity.name", "Parametric Clifton");

		testPrepareBeanWithCompositePropertiesBothOrders(mpvs, new TestBean(),
				testBean -> Assertions.assertNull(testBean.getNamedEntity(), "Nested entity should be null before prepare"),
				testBean -> Assertions.assertNotNull(testBean.getNamedEntity(), "Nested entity should NOT be null after prepare"));
	}


	@Test
	public void testPrepareBeanWithCompositePropertiesSamePrefix() {
		MutablePropertyValues mpvs = new MutablePropertyValues();

		mpvs.add("namedEntity.id", null);
		mpvs.add("namedEntitySecond.id", null);

		ValidatingServletRequestDataBinder binder = new ValidatingServletRequestDataBinder(null, null, false);
		TestBean testBean = new TestBean();

		testPrepareBeanWithCompositePropertiesBothOrders(mpvs, testBean,
				b -> {
					Assertions.assertNull(b.getNamedEntity(), "Nested entity should be null before prepare");
					Assertions.assertNull(b.getNamedEntitySecond(), "Nested entity second should be null before prepare");
				},
				b -> {
					Assertions.assertNull(b.getNamedEntity(), "Nested entity should be null after prepare");
					Assertions.assertNull(b.getNamedEntitySecond(), "Nested entity second should be null after prepare");
				});

		// Set the second entity and then bind - should clear it now
		testBean = new TestBean();
		NamedEntityTestObject ne = new NamedEntityTestObject("Test Clear", "Test");
		testBean.setNamedEntitySecond(ne);

		mpvs = new MutablePropertyValues();
		mpvs.add("namedEntity.id", null);
		mpvs.add("namedEntitySecond.id", null); // new bean of this object should remain during binding

		testPrepareBeanWithCompositePropertiesBothOrders(mpvs, testBean,
				b -> {
					Assertions.assertNull(b.getNamedEntity(), "Nested entity should be null before prepare");
					Assertions.assertNotNull(b.getNamedEntitySecond(), "Nested entity second should NOT be null before prepare");
				},
				b -> {
					Assertions.assertNull(b.getNamedEntity(), "Nested entity should be null after prepare");
					Assertions.assertNotNull(b.getNamedEntitySecond(), "Nested entity second should be NOT null after prepare");
				});

		ne.setId(5);
		testBean.setNamedEntitySecond(ne);

		mpvs = new MutablePropertyValues();
		mpvs.add("namedEntity.id", null);
		mpvs.add("namedEntitySecond.id", null); // non-new bean should be cleared during binding

		testPrepareBeanWithCompositePropertiesBothOrders(mpvs, testBean,
				b -> {
					Assertions.assertNull(b.getNamedEntity(), "Nested entity should be null before prepare");
					Assertions.assertNotNull(b.getNamedEntitySecond(), "Nested entity second should NOT be null before prepare");
				},
				b -> {
					Assertions.assertNull(b.getNamedEntity(), "Nested entity should be null after prepare");
					Assertions.assertNull(b.getNamedEntitySecond(), "Nested entity second should be null after prepare");
				});
	}


	@Test
	public void testPrepareBeanWithCompositePropertiesNested() {
		MutablePropertyValues mpvs = new MutablePropertyValues();
		mpvs.add("parent.id", null);
		mpvs.add("parent.parent.id", null);
		@SuppressWarnings("rawtypes")
		NamedHierarchicalEntity testBean = new NamedHierarchicalEntity();
		testPrepareBeanWithCompositePropertiesBothOrders(mpvs, testBean,
				b -> Assertions.assertNull(b.getParent(), "Entity should be null before prepare"),
				b -> Assertions.assertNull(b.getParent(), "Entity should be null after prepare"));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private <T> void testPrepareBeanWithCompositePropertiesBothOrders(MutablePropertyValues propertyValues, T testBean, Consumer<T> prePrepareAssertion, Consumer<T> postPrepareAssertion) {
		MutablePropertyValues reverseOrderProperties = new MutablePropertyValues();
		ListIterator<PropertyValue> providedPropertiesListIterator = propertyValues.getPropertyValueList().listIterator(propertyValues.size());
		while (providedPropertiesListIterator.hasPrevious()) {
			reverseOrderProperties.addPropertyValue(providedPropertiesListIterator.previous());
		}

		T reverseTestObject = BeanUtils.newInstance(testBean);
		BeanUtils.copyProperties(testBean, reverseTestObject);

		ValidatingServletRequestDataBinder binder = new ValidatingServletRequestDataBinder(null, null, false);
		prePrepareAssertion.accept(testBean);
		binder.prepareBean(testBean, propertyValues);
		postPrepareAssertion.accept(testBean);

		prePrepareAssertion.accept(reverseTestObject);
		binder.prepareBean(reverseTestObject, reverseOrderProperties);
		postPrepareAssertion.accept(reverseTestObject);
	}
}
