package com.clifton.core.web.view;


import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.AnnotationUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class BaseViewTests {

	@Test
	public void testDeepAnnotationReads() {
		Assertions.assertTrue(AnnotationUtils.isAnnotationPresent(BaseAuditableEntityDateRangeWithoutTimeSearchForm.class, NonPersistentObject.class));
		Assertions.assertFalse(AnnotationUtils.isAnnotationPresent(BaseAuditableEntityDateRangeWithoutTimeSearchForm.class, SecureMethod.class));
	}
}
