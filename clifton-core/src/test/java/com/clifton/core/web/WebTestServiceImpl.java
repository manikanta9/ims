package com.clifton.core.web;


import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>TestServiceImpl</code> class is a test service to be used in varies tests.
 *
 * @author vgomelsky
 */
@Service
public class WebTestServiceImpl implements WebTestService {

	private UpdatableDAO<NamedEntityTestObject> namedEntityTestObjectDAO;


	@Override
	public List<NamedEntityTestObject> getNamedEntityTestObjectList() {
		NamedEntityTestObject testObject1 = new NamedEntityTestObject("test1", "desc1");
		testObject1.setId(300);
		NamedEntityTestObject testObject2 = new NamedEntityTestObject("test2", "desc2");
		testObject2.setId(400);

		List<NamedEntityTestObject> result = new ArrayList<>();
		result.add(testObject1);
		result.add(testObject2);
		return result;
	}


	@Override
	public NamedEntityTestObject getNamedEntityTestObject(int id) {
		return getNamedEntityTestObjectDAO().findByPrimaryKey(id);
	}


	@Override
	public void saveNamedEntityTestObject(NamedEntityTestObject entity) {
		getNamedEntityTestObjectDAO().save(entity);
	}


	/**
	 * @return the namedEntityDAO
	 */
	public UpdatableDAO<NamedEntityTestObject> getNamedEntityTestObjectDAO() {
		return this.namedEntityTestObjectDAO;
	}


	/**
	 * @param namedEntityDAO the namedEntityDAO to set
	 */
	public void setNamedEntityTestObjectDAO(UpdatableDAO<NamedEntityTestObject> namedEntityDAO) {
		this.namedEntityTestObjectDAO = namedEntityDAO;
	}
}
