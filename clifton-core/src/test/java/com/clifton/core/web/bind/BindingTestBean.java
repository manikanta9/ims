package com.clifton.core.web.bind;


import com.clifton.core.beans.NamedEntity;

import java.math.BigDecimal;


public class BindingTestBean extends NamedEntity<Integer> {

	private int intField;
	private BigDecimal bigDecimalField;


	public int getIntField() {
		return this.intField;
	}


	public void setIntField(int intField) {
		this.intField = intField;
	}


	public BigDecimal getBigDecimalField() {
		return this.bigDecimalField;
	}


	public void setBigDecimalField(BigDecimal bigDecimalField) {
		this.bigDecimalField = bigDecimalField;
	}
}
