package com.clifton.core.web;


import com.clifton.core.beans.NamedEntityTestObject;

import java.util.List;


public interface WebTestService {

	public List<NamedEntityTestObject> getNamedEntityTestObjectList();


	public NamedEntityTestObject getNamedEntityTestObject(int id);


	public void saveNamedEntityTestObject(NamedEntityTestObject entity);
}
