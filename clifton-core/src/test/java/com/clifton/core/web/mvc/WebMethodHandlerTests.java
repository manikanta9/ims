package com.clifton.core.web.mvc;


import com.clifton.core.beans.NamedEntityTestObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class WebMethodHandlerTests {

	@Resource
	private WebMethodHandler webMethodHandler;


	@Test
	public void testMethodCall() {
		Map<String, String> params = new HashMap<>();
		params.put("label", "initial label");

		NamedEntityTestObject bean = this.webMethodHandler.executeServiceCall("namedEntityTestObjectSave.json?name=initial name", params);
		Assertions.assertNotNull(bean);
		Assertions.assertEquals(1, (int) bean.getId());
		Assertions.assertEquals("initial name", bean.getName());
		Assertions.assertEquals("initial label", bean.getLabel());
		Assertions.assertNull(bean.getDescription());

		params = new HashMap<>();
		params.put("id", bean.getId().toString());
		bean = this.webMethodHandler.executeServiceCall("namedEntityTestObject.json", params);

		Assertions.assertNotNull(bean);
		Assertions.assertEquals(1, (int) bean.getId());
		Assertions.assertEquals("initial name", bean.getName());
		Assertions.assertEquals("initial label", bean.getLabel());
		Assertions.assertNull(bean.getDescription());
	}


	@Test
	public void testMethodListCall() {
		List<NamedEntityTestObject> data = this.webMethodHandler.executeServiceCall("namedEntityTestObjectList.json", null);
		Assertions.assertEquals(2, data.size());
	}
}
