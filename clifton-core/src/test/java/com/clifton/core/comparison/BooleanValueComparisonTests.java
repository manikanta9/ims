package com.clifton.core.comparison;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>BooleanValueComparisonTests</code> ...
 *
 * @author manderson
 */
public class BooleanValueComparisonTests {

	@Test
	public void testBooleanValueTrueTests() {
		BooleanValueComparison comparison = new BooleanValueComparison();
		comparison.setBooleanValue(true);
		Assertions.assertTrue(comparison.evaluate(new NamedEntity<Integer>(), null), "Expected TRUE to be returned by comparison evaluation");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(new NamedEntity<Integer>(), context);
		Assertions.assertEquals("TRUE", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testBooleanValueFalseTests() {
		BooleanValueComparison comparison = new BooleanValueComparison();
		comparison.setBooleanValue(false);
		Assertions.assertFalse(comparison.evaluate(new NamedEntity<Integer>(), null), "Expected FALSE to be returned by comparison evaluation");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(new NamedEntity<Integer>(), context);
		Assertions.assertEquals("FALSE", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}
}
