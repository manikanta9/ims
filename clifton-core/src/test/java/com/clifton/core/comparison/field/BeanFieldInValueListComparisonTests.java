package com.clifton.core.comparison.field;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class BeanFieldInValueListComparisonTests {

	@Test
	public void testCompareNumbersIn() {
		BeanFieldInValueListComparison comparison = new BeanFieldInValueListComparison();
		comparison.setValueList(CollectionUtils.createList("100", "150"));
		comparison.setBeanFieldName("bigDecimal");
		TestBean bean = new TestBean();

		bean.setBigDecimal(new BigDecimal("100"));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "100 in list");
		Assertions.assertEquals("(bigDecimal: 100 IN 100, 150)", context.getTrueMessage());

		comparison.setValueList(CollectionUtils.createList("100.0", "150.0"));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "100.0 in list");
		Assertions.assertEquals("(bigDecimal: 100 IN 100.0, 150.0)", context.getTrueMessage());
	}


	@Test
	public void testCompareNumbersNotIn() {
		BeanFieldNotInValueListComparison comparison = new BeanFieldNotInValueListComparison();
		comparison.setValueList(CollectionUtils.createList("100", "170"));
		comparison.setBeanFieldName("bigDecimal");
		TestBean bean = new TestBean();

		bean.setBigDecimal(new BigDecimal("100"));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context), "100 in list");
		Assertions.assertEquals("(bigDecimal: 100 IN 100, 170)", context.getFalseMessage());

		comparison.setValueList(CollectionUtils.createList("110.0", "170.0"));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "100.0 not in list");
		Assertions.assertEquals("(bigDecimal: 100 NOT IN 110.0, 170.0)", context.getTrueMessage());
	}


	@Test
	public void testCompareStringsIn() {
		BeanFieldInValueListComparison comparison = new BeanFieldInValueListComparison();
		comparison.setValueList(CollectionUtils.createList("My test string", "My other test string"));
		comparison.setBeanFieldName("testString");
		TestBean bean = new TestBean();

		bean.setTestString("My test string");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString: My test string IN My test string, My other test string)", context.getTrueMessage());

		comparison.setValueList(CollectionUtils.createList("Another Test String"));
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString: My test string NOT IN Another Test String)", context.getFalseMessage());

		bean.setTestString(null);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString: null NOT IN Another Test String)", context.getFalseMessage());
	}


	@Test
	public void testCompareStringsNotIn() {
		BeanFieldNotInValueListComparison comparison = new BeanFieldNotInValueListComparison();
		comparison.setValueList(CollectionUtils.createList("My test string", "My other test string"));
		comparison.setBeanFieldName("testString");
		TestBean bean = new TestBean();

		bean.setTestString("My test string");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString: My test string IN My test string, My other test string)", context.getFalseMessage());

		comparison.setValueList(CollectionUtils.createList("Another Test String", "And Another"));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString: My test string NOT IN Another Test String, And Another)", context.getTrueMessage());

		bean.setTestString(null);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString: null NOT IN Another Test String, And Another)", context.getTrueMessage());
	}


	public class TestBean extends NamedEntity<Integer> {

		private BigDecimal bigDecimal;
		private String testString;


		public BigDecimal getBigDecimal() {
			return this.bigDecimal;
		}


		public void setBigDecimal(BigDecimal bigDecimal) {
			this.bigDecimal = bigDecimal;
		}


		public String getTestString() {
			return this.testString;
		}


		public void setTestString(String testString) {
			this.testString = testString;
		}
	}
}
