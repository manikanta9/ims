package com.clifton.core.comparison;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.field.BeanFieldEqualsToValueComparison;
import com.clifton.core.comparison.grouping.AndComparison;
import com.clifton.core.comparison.grouping.OrComparison;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ComparisonTests {

	@Test
	public void testAndWithNoChildren() {
		Assertions.assertThrows(RuntimeException.class, () -> {
			GroupingComparison<NamedEntity<Integer>> comparison = new AndComparison<>();
			NamedEntity<Integer> bean = new NamedEntity<>();
			Assertions.assertTrue(comparison.evaluate(bean, null));
		});
	}


	@Test
	public void testAndWithOneChild() {
		GroupingComparison<NamedEntity<Integer>> comparison = new AndComparison<>();
		BeanFieldEqualsToValueComparison<NamedEntity<Integer>> childComparison = new BeanFieldEqualsToValueComparison<>();
		childComparison.setBeanFieldName("name");
		childComparison.setValue("TEST");
		comparison.addChildComparison(childComparison);

		NamedEntity<Integer> bean = new NamedEntity<>();
		Assertions.assertFalse(comparison.evaluate(bean, null));

		bean.setName("TEST");
		Assertions.assertTrue(comparison.evaluate(bean, null));

		bean.setName("WRONG TEST");
		Assertions.assertFalse(comparison.evaluate(bean, null));
	}


	@Test
	public void testAndWithTwoChildren() {
		GroupingComparison<NamedEntity<Integer>> comparison = new AndComparison<>();
		BeanFieldEqualsToValueComparison<NamedEntity<Integer>> childComparison = new BeanFieldEqualsToValueComparison<>();
		childComparison.setBeanFieldName("name");
		childComparison.setValue("TEST");
		comparison.addChildComparison(childComparison);

		childComparison = new BeanFieldEqualsToValueComparison<>();
		childComparison.setBeanFieldName("createUserId");
		childComparison.setValue("555");
		comparison.addChildComparison(childComparison);

		NamedEntity<Integer> bean = new NamedEntity<>();
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
		Assertions.assertEquals("(name = null instead of TEST)", context.getFalseMessage());

		bean.setName("TEST");
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("COMPARISON EVALUATED TO FALSE", context.getTrueMessage());
		Assertions.assertEquals("(createUserId = 0 instead of 555)", context.getFalseMessage());

		bean.setCreateUserId(new Integer(555).shortValue());
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
		Assertions.assertEquals("(name = TEST) AND (createUserId = 555)", context.getTrueMessage());
	}


	@Test
	public void testOrWithNoChildren() {
		Assertions.assertThrows(RuntimeException.class, () -> {
			GroupingComparison<NamedEntity<Integer>> comparison = new OrComparison<>();
			NamedEntity<Integer> bean = new NamedEntity<>();
			Assertions.assertTrue(comparison.evaluate(bean, null));
		});
	}


	@Test
	public void testOrWithOneChild() {
		GroupingComparison<NamedEntity<Integer>> comparison = new OrComparison<>();
		BeanFieldEqualsToValueComparison<NamedEntity<Integer>> childComparison = new BeanFieldEqualsToValueComparison<>();
		childComparison.setBeanFieldName("name");
		childComparison.setValue("TEST");
		comparison.addChildComparison(childComparison);

		NamedEntity<Integer> bean = new NamedEntity<>();
		Assertions.assertFalse(comparison.evaluate(bean, null));

		bean.setName("TEST");
		Assertions.assertTrue(comparison.evaluate(bean, null));
	}


	@Test
	public void testOrWithTwoChildren() {
		GroupingComparison<NamedEntity<Integer>> comparison = new OrComparison<>();
		BeanFieldEqualsToValueComparison<NamedEntity<Integer>> childComparison = new BeanFieldEqualsToValueComparison<>();
		childComparison.setBeanFieldName("name");
		childComparison.setValue("TEST");
		comparison.addChildComparison(childComparison);

		childComparison = new BeanFieldEqualsToValueComparison<>();
		childComparison.setBeanFieldName("createUserId");
		childComparison.setValue("555");
		comparison.addChildComparison(childComparison);

		NamedEntity<Integer> bean = new NamedEntity<>();
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(name = null instead of TEST) OR (createUserId = 0 instead of 555)", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		bean.setName("TEST");
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(name = TEST)", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		bean.setCreateUserId(new Integer(555).shortValue());
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(name = TEST)", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}
}
