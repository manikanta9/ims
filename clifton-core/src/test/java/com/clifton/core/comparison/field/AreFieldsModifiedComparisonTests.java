package com.clifton.core.comparison.field;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.xml.CoreTestGroupEntity;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * The <code>AreFieldsModifiedComparisonTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("BeanFieldComparisonTests-context.xml")
public class AreFieldsModifiedComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private UpdatableDAO<CoreTestGroupEntity> coreTestGroupEntityDAO;


	private AreFieldsModifiedComparison getComparisonBean() {
		AreFieldsModifiedComparison comparison = new AreFieldsModifiedComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testAreFieldsModifiedComparison_SpecificFields_True() {
		AreFieldsModifiedComparison comparison = getComparisonBean();

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		comparison.setBeanFieldNames("name");
		entity1.setName("Test Name 1 - Updated");
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [name] has been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		comparison.setBeanFieldNames("name,description");
		entity2.setName("Test Name 2 - Updated");
		entity2.setDescription("Description");
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [name,description] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreFieldsModifiedComparison_AllFields_True() {
		AreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);

		CoreTestGroupEntity newEntity = new CoreTestGroupEntity();
		newEntity.setName("Test Insert");
		Assertions.assertTrue(comparison.evaluate(newEntity, null), "Comparison should evaluate to true");

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		entity1.setName("Test Name 1 - Updated");
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Fields [label,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		entity2.setName("Test Name 2 - Updated");
		entity2.setDescription("Description");
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [description,label,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreFieldsModifiedComparison_AllFields_WithExceptions_True() {
		AreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		comparison.setExceptionBeanFieldNames("label");

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		entity1.setName("Test Name 1 - Updated");
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [name] has been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		entity2.setName("Test Name 2 - Updated");
		entity2.setDescription("Description");
		entity2.setBigDecimalField(BigDecimal.TEN);
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [bigDecimalField,description,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreBigDecimalFieldsModifiedComparison() {
		AreFieldsModifiedComparison comparison = getComparisonBean();

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		comparison.setBeanFieldNames("bigDecimalField");
		entity1.setBigDecimalField(new BigDecimal("5.25"));
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [bigDecimalField] has been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		this.coreTestGroupEntityDAO.save(entity1);

		entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		entity1.setBigDecimalField(new BigDecimal("5.2500000"));
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [bigDecimalField] has not been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testAreFieldsModifiedComparison_SpecificFields_False() {
		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		AreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setBeanFieldNames("name");
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [name] has not been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		comparison.setBeanFieldNames("name,description");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("None of the Fields [name,description] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		entity2.setName("Test Name 2 - Updated");
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Field [name] has been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreFieldsModifiedComparison_AllFields_False() {
		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		AreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("None of the Fields [bigDecimalField,description,label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testAreFieldsModifiedComparison_AllFields_WithExceptions_False() {
		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		AreFieldsModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		comparison.setExceptionBeanFieldNames("label");
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("None of the Fields [bigDecimalField,description,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
