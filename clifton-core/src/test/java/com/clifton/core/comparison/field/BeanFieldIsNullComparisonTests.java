package com.clifton.core.comparison.field;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.SimpleComparisonContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>BeanFieldIsNullComparisonTests</code> ...
 *
 * @author manderson
 */
public class BeanFieldIsNullComparisonTests {

	@Test
	public void testCompareNull() {
		BeanFieldIsNullComparison comparison = new BeanFieldIsNullComparison();
		comparison.setBeanFieldName("name");

		NamedEntity<Integer> bean = new NamedEntity<>();
		Assertions.assertTrue(comparison.evaluate(bean, null), "(name is not populated)");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "(name is not populated)");
		Assertions.assertEquals("(name is not populated)", context.getTrueMessage());

		bean.setName("Test");
		Assertions.assertFalse(comparison.evaluate(bean, null), "(name is not populated)");

		context = new SimpleComparisonContext();
		comparison.setFalseMessageOverride("Name is missing");
		comparison.evaluate(bean, context);
		Assertions.assertEquals("Name is missing", context.getFalseMessage());

		context = new SimpleComparisonContext();
		comparison.setFalseMessageOverride(null);
		Assertions.assertFalse(comparison.evaluate(bean, context), "(name is not populated)");
		Assertions.assertEquals("(name is populated)", context.getFalseMessage());
	}


	@Test
	public void testCompareNotNull() {
		BeanFieldIsNotNullComparison comparison = new BeanFieldIsNotNullComparison();
		comparison.setBeanFieldName("name");

		NamedEntity<Integer> bean = new NamedEntity<>();
		bean.setName("Test");
		Assertions.assertTrue(comparison.evaluate(bean, null), "(name is populated)");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "(name is populated)");
		Assertions.assertEquals("(name is populated)", context.getTrueMessage());

		context = new SimpleComparisonContext();
		comparison.setTrueMessageOverride("Name is not missing");
		comparison.evaluate(bean, context);
		Assertions.assertEquals("Name is not missing", context.getTrueMessage());
		comparison.setTrueMessageOverride(null);

		bean.setName(null);
		Assertions.assertFalse(comparison.evaluate(bean, null), "(name is populated)");

		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context), "(name is populated)");
		Assertions.assertEquals("(name is required)", context.getFalseMessage());
	}
}
