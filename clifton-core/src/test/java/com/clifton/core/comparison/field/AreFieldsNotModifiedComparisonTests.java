package com.clifton.core.comparison.field;

import com.clifton.core.comparison.SimpleComparisonContext;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.xml.CoreTestGroupEntity;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * The <code>AreFieldsNotModifiedComparisonTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration("BeanFieldComparisonTests-context.xml")
public class AreFieldsNotModifiedComparisonTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private UpdatableDAO<CoreTestGroupEntity> coreTestGroupEntityDAO;


	private AreFieldsNotModifiedComparison getComparisonBean() {
		AreFieldsNotModifiedComparison comparison = new AreFieldsNotModifiedComparison();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(comparison, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		return comparison;
	}


	@Test
	public void testAreReadOnlyFieldsNotModifiedComparisonTrue() {
		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		AreFieldsNotModifiedComparison comparison = getComparisonBean();
		comparison.setBeanFieldNames("name");
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [name] has not been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		comparison.setBeanFieldNames("name,description");
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("None of the Fields [name,description] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreReadOnlyFieldsNotModifiedComparisonFalse() {
		AreFieldsNotModifiedComparison comparison = getComparisonBean();

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		comparison.setBeanFieldNames("name");
		entity1.setName("Test Name 1 - Updated");
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Field [name] has been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		comparison.setBeanFieldNames("name,description");
		entity2.setName("Test Name 2 - Updated");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Field [name] has been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		entity2.setDescription("Description");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [name,description] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testAreReadOnlyFieldsNotModifiedAllFieldsComparisonTrue() {
		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		AreFieldsNotModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("None of the Fields [bigDecimalField,description,label,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("None of the Fields [bigDecimalField,description,label,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreReadOnlyFieldsNotModifiedAllFieldsComparisonFalse() {
		AreFieldsNotModifiedComparison comparison = getComparisonBean();

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		comparison.setAllFields(true);
		entity1.setName("Test Name 1 - Updated");
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Fields [label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		entity2.setName("Test Name 2 - Updated");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		entity2.setDescription("Description");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [description,label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testAreReadOnlyFieldsNotModifiedAllFieldsWithExceptionsComparisonTrue() {
		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		AreFieldsNotModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		comparison.setExceptionBeanFieldNames("description");
		Assertions.assertTrue(comparison.evaluate(entity1, null), "Comparison should evaluate to true");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("None of the Fields [bigDecimalField,label,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		entity2.setDescription("TEST UPDATE!");
		Assertions.assertTrue(comparison.evaluate(entity2, null), "Comparison should evaluate to true");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("None of the Fields [bigDecimalField,label,name] have been modified.", context.getTrueMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getFalseMessage()));
	}


	@Test
	public void testAreReadOnlyFieldsNotModifiedAllFieldsWithExceptionsComparisonFalse() {
		AreFieldsNotModifiedComparison comparison = getComparisonBean();

		CoreTestGroupEntity entity1 = this.coreTestGroupEntityDAO.findByPrimaryKey(1);
		comparison.setAllFields(true);
		comparison.setExceptionBeanFieldNames("description");
		entity1.setName("Test Name 1 - Updated");
		Assertions.assertFalse(comparison.evaluate(entity1, null), "Comparison should evaluate to false");

		SimpleComparisonContext context = new SimpleComparisonContext();
		comparison.evaluate(entity1, context);
		Assertions.assertEquals("Fields [label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		CoreTestGroupEntity entity2 = this.coreTestGroupEntityDAO.findByPrimaryKey(2);
		entity2.setName("Test Name 2 - Updated");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [label,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));

		entity2.setDescription("Description");
		comparison.setExceptionBeanFieldNames("label");
		Assertions.assertFalse(comparison.evaluate(entity2, null), "Comparison should evaluate to false");
		context = new SimpleComparisonContext();
		comparison.evaluate(entity2, context);
		Assertions.assertEquals("Fields [description,name] have been modified.", context.getFalseMessage());
		Assertions.assertTrue(StringUtils.isEmpty(context.getTrueMessage()));
	}


	@Test
	public void testAreFieldsNotModified_BigDecimalScale() {
		CoreTestGroupEntity entity = new CoreTestGroupEntity();
		entity.setName("Test Big Decimal");
		entity.setBigDecimalField(new BigDecimal("7.45678845"));
		this.coreTestGroupEntityDAO.save(entity);

		// Same Value, but bigger precision
		entity.setBigDecimalField(new BigDecimal("7.45678844987"));
		AreFieldsNotModifiedComparison comparison = getComparisonBean();
		comparison.setAllFields(true);
		Assertions.assertTrue(comparison.evaluate(entity, null), "Comparison should evaluate to true");

		// Different Value, bigger precision
		entity.setBigDecimalField(new BigDecimal("7.456788570456"));
		Assertions.assertFalse(comparison.evaluate(entity, null), "Comparison should evaluate to true");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
