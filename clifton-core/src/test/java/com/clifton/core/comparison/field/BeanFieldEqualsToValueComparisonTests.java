package com.clifton.core.comparison.field;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.SimpleComparisonContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class BeanFieldEqualsToValueComparisonTests {

	@Test
	public void testCompareNumbersEqual() {
		BeanFieldEqualsToValueComparison comparison = new BeanFieldEqualsToValueComparison();
		comparison.setValue("100");
		comparison.setBeanFieldName("bigDecimal");
		TestBean bean = new TestBean();

		bean.setBigDecimal(new BigDecimal("100"));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "100 must be equal to 100");
		Assertions.assertEquals("(bigDecimal = 100)", context.getTrueMessage());

		comparison.setValue("100.0");
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "100.0 must be equal to 100");
		Assertions.assertEquals("(bigDecimal = 100.0)", context.getTrueMessage());

		comparison.setValue(null);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context), "100.0 must NOT be equal to null");
		Assertions.assertEquals("(bigDecimal = 100 instead of null)", context.getFalseMessage());
	}


	@Test
	public void testCompareNumbersNotEqual() {
		BeanFieldNotEqualsToValueComparison comparison = new BeanFieldNotEqualsToValueComparison();
		comparison.setValue("100");
		comparison.setBeanFieldName("bigDecimal");
		TestBean bean = new TestBean();

		bean.setBigDecimal(new BigDecimal("100"));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context), "100 must be equal to 100");
		Assertions.assertEquals("(bigDecimal = 100)", context.getFalseMessage());

		comparison.setValue("100.0");
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context), "100.0 must be equal to 100");
		Assertions.assertEquals("(bigDecimal = 100.0)", context.getFalseMessage());

		comparison.setValue(null);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context), "100.0 must NOT be equal to null");
		Assertions.assertEquals("(bigDecimal = 100 instead of null)", context.getTrueMessage());
	}


	@Test
	public void testCompareStringsEqual() {
		BeanFieldEqualsToValueComparison comparison = new BeanFieldEqualsToValueComparison();
		comparison.setValue("My test string");
		comparison.setBeanFieldName("testString");
		TestBean bean = new TestBean();

		bean.setTestString("My test string");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString = My test string)", context.getTrueMessage());

		comparison.setValue("Another Test String");
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString = My test string instead of Another Test String)", context.getFalseMessage());

		bean.setTestString(null);
		context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString = null instead of Another Test String)", context.getFalseMessage());
	}


	@Test
	public void testCompareStringsNotEqual() {
		BeanFieldNotEqualsToValueComparison comparison = new BeanFieldNotEqualsToValueComparison();
		comparison.setValue("My test string");
		comparison.setBeanFieldName("testString");
		TestBean bean = new TestBean();

		bean.setTestString("My test string");

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString = My test string)", context.getFalseMessage());

		comparison.setValue("Another Test String");
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString = My test string instead of Another Test String)", context.getTrueMessage());

		bean.setTestString(null);
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(testString = null instead of Another Test String)", context.getTrueMessage());
	}


	@Test
	public void testCompareFieldToFieldNumericEqual() {
		BeanFieldEqualsToValueComparison comparison = new BeanFieldEqualsToValueComparison();
		comparison.setValueIsBeanProperty(true);
		comparison.setValue("nestedBean.bigDecimal");
		comparison.setBeanFieldName("bigDecimal");

		TestBean bean = new TestBean();
		bean.setBigDecimal(new BigDecimal("100"));
		bean.setNestedBean(new TestBean());
		bean.getNestedBean().setBigDecimal(new BigDecimal("200"));

		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertFalse(comparison.evaluate(bean, context));
		Assertions.assertEquals("(bigDecimal = 100 instead of 200)", context.getFalseMessage());

		bean.setBigDecimal(new BigDecimal("200"));
		context = new SimpleComparisonContext();
		Assertions.assertTrue(comparison.evaluate(bean, context));
		Assertions.assertEquals("(bigDecimal = 200)", context.getTrueMessage());
	}


	public class TestBean extends NamedEntity<Integer> {

		private BigDecimal bigDecimal;
		private String testString;
		private TestBean nestedBean;


		public BigDecimal getBigDecimal() {
			return this.bigDecimal;
		}


		public void setBigDecimal(BigDecimal bigDecimal) {
			this.bigDecimal = bigDecimal;
		}


		public String getTestString() {
			return this.testString;
		}


		public void setTestString(String testString) {
			this.testString = testString;
		}


		public TestBean getNestedBean() {
			return this.nestedBean;
		}


		public void setNestedBean(TestBean nestedBean) {
			this.nestedBean = nestedBean;
		}
	}
}
