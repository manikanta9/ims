package com.clifton.core.comparison.field.number;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.comparison.SimpleComparisonContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * The <code>BeanFieldNumberComparisonTests</code> ...
 *
 * @author manderson
 */
public class BeanFieldNumberComparisonTests {

	private BeanFieldGreaterThanValueComparison greater = new BeanFieldGreaterThanValueComparison();
	private BeanFieldGreaterThanOrEqualsValueComparison greaterEquals = new BeanFieldGreaterThanOrEqualsValueComparison();
	private BeanFieldLessThanValueComparison less = new BeanFieldLessThanValueComparison();
	private BeanFieldLessThanOrEqualsValueComparison lessEquals = new BeanFieldLessThanOrEqualsValueComparison();


	private void setBeanFieldNameAndValues(String beanFieldName, String value) {
		this.greater.setBeanFieldName(beanFieldName);
		this.greaterEquals.setBeanFieldName(beanFieldName);
		this.less.setBeanFieldName(beanFieldName);
		this.lessEquals.setBeanFieldName(beanFieldName);

		this.greater.setValue(value);
		this.greaterEquals.setValue(value);
		this.less.setValue(value);
		this.lessEquals.setValue(value);
	}


	@Test
	public void testCompareBeanFieldNotNumber() {
		Assertions.assertThrows(Exception.class, () -> {
			setBeanFieldNameAndValues("name", "100");

			TestBean bean = new TestBean();
			bean.setName("Test");
			this.greater.evaluate(bean, null);
		});
	}


	@Test
	public void testCompareBigDecimals() {
		setBeanFieldNameAndValues("bigDecimalField", "100");

		TestBean bean = new TestBean();

		bean.setBigDecimalField(new BigDecimal("105"));
		Assertions.assertTrue(this.greater.evaluate(bean, null), "105 > 100");
		Assertions.assertTrue(this.greaterEquals.evaluate(bean, null), "105 >= 100");
		Assertions.assertFalse(this.less.evaluate(bean, null), "105 !< 100");
		Assertions.assertFalse(this.lessEquals.evaluate(bean, null), "105 !<= 100");

		bean.setBigDecimalField(new BigDecimal("100.0"));
		Assertions.assertFalse(this.greater.evaluate(bean, null), "105 !> 100");
		Assertions.assertTrue(this.greaterEquals.evaluate(bean, null), "105 >= 100");
		Assertions.assertFalse(this.less.evaluate(bean, null), "105 !< 100");
		Assertions.assertTrue(this.lessEquals.evaluate(bean, null), "105 <= 100");

		bean.setBigDecimalField(new BigDecimal("95"));
		Assertions.assertFalse(this.greater.evaluate(bean, null), "95 !> 100");
		Assertions.assertFalse(this.greaterEquals.evaluate(bean, null), "95 !>= 100");
		Assertions.assertTrue(this.less.evaluate(bean, null), "95 < 100");
		Assertions.assertTrue(this.lessEquals.evaluate(bean, null), "95 <= 100");
	}


	@Test
	public void testCompareIntegers() {
		setBeanFieldNameAndValues("intField", "-15");

		TestBean bean = new TestBean();

		bean.setIntField(0);
		Assertions.assertTrue(this.greater.evaluate(bean, null), "0 > -15");
		Assertions.assertTrue(this.greaterEquals.evaluate(bean, null), "0 >= -15");
		Assertions.assertFalse(this.less.evaluate(bean, null), "0 !< -15");
		Assertions.assertFalse(this.lessEquals.evaluate(bean, null), "0 !<= -15");

		bean.setIntField(-15);
		Assertions.assertFalse(this.greater.evaluate(bean, null), "-15 !> -15");
		Assertions.assertTrue(this.greaterEquals.evaluate(bean, null), "-15 >= -15");
		Assertions.assertFalse(this.less.evaluate(bean, null), "-15 !< -15");
		Assertions.assertTrue(this.lessEquals.evaluate(bean, null), "-15 <= -15");

		bean.setIntField(-16);
		Assertions.assertFalse(this.greater.evaluate(bean, null), "-16 !> -15");
		Assertions.assertFalse(this.greaterEquals.evaluate(bean, null), "-16 !>= -15");
		Assertions.assertTrue(this.less.evaluate(bean, null), "-16 < -15");
		Assertions.assertTrue(this.lessEquals.evaluate(bean, null), "-16 <= -15");
	}


	@Test
	public void testCompareDoubles() {
		setBeanFieldNameAndValues("doubleField", "0.5");

		TestBean bean = new TestBean();

		bean.setDoubleField(0.55);
		Assertions.assertTrue(this.greater.evaluate(bean, null), "0.55 > 0.5");
		Assertions.assertTrue(this.greaterEquals.evaluate(bean, null), "0.55 >= 0.5");
		Assertions.assertFalse(this.less.evaluate(bean, null), "0.55 !< 0.5");
		Assertions.assertFalse(this.lessEquals.evaluate(bean, null), "0.55 !<= 0.5");

		bean.setDoubleField(0.5);
		Assertions.assertFalse(this.greater.evaluate(bean, null), "0.5 !> 0.5");
		Assertions.assertTrue(this.greaterEquals.evaluate(bean, null), "0.5 >= 0.5");
		Assertions.assertFalse(this.less.evaluate(bean, null), "0.5 !< 0.5");
		Assertions.assertTrue(this.lessEquals.evaluate(bean, null), "0.5 <= 0.5");

		bean.setDoubleField(0.423);
		Assertions.assertFalse(this.greater.evaluate(bean, null), "0.423 !> 0.5");
		Assertions.assertFalse(this.greaterEquals.evaluate(bean, null), "0.423 !>= 0.5");
		Assertions.assertTrue(this.less.evaluate(bean, null), "0.423 < 0.5");
		Assertions.assertTrue(this.lessEquals.evaluate(bean, null), "0.423 <= 0.5");
	}


	@Test
	public void testCompareTrueFalseMessages() {
		setBeanFieldNameAndValues("bigDecimalField", "100");

		TestBean bean1 = new TestBean();
		bean1.setBigDecimalField(new BigDecimal("105"));

		TestBean bean2 = new TestBean();
		bean2.setBigDecimalField(new BigDecimal("95"));

		// Greater Than True
		SimpleComparisonContext context = new SimpleComparisonContext();
		Assertions.assertTrue(this.greater.evaluate(bean1, context));
		Assertions.assertEquals("(bigDecimalField: 105 > 100)", context.getTrueMessage());

		// Greater Than False
		context = new SimpleComparisonContext();
		Assertions.assertFalse(this.greater.evaluate(bean2, context));
		Assertions.assertEquals("(bigDecimalField: 95 is not > 100)", context.getFalseMessage());

		// Greater Than Equals True
		context = new SimpleComparisonContext();
		Assertions.assertTrue(this.greaterEquals.evaluate(bean1, context));
		Assertions.assertEquals("(bigDecimalField: 105 >= 100)", context.getTrueMessage());

		// Greater Than Equals False
		context = new SimpleComparisonContext();
		Assertions.assertFalse(this.greaterEquals.evaluate(bean2, context));
		Assertions.assertEquals("(bigDecimalField: 95 is not >= 100)", context.getFalseMessage());

		// Less Than True
		context = new SimpleComparisonContext();
		Assertions.assertTrue(this.less.evaluate(bean2, context));
		Assertions.assertEquals("(bigDecimalField: 95 < 100)", context.getTrueMessage());

		// Less Than False
		context = new SimpleComparisonContext();
		Assertions.assertFalse(this.less.evaluate(bean1, context));
		Assertions.assertEquals("(bigDecimalField: 105 is not < 100)", context.getFalseMessage());

		// Less Than Equals True
		context = new SimpleComparisonContext();
		Assertions.assertTrue(this.lessEquals.evaluate(bean2, context));
		Assertions.assertEquals("(bigDecimalField: 95 <= 100)", context.getTrueMessage());

		// Less Than Equals False
		context = new SimpleComparisonContext();
		Assertions.assertFalse(this.lessEquals.evaluate(bean1, context));
		Assertions.assertEquals("(bigDecimalField: 105 is not <= 100)", context.getFalseMessage());
	}


	public class TestBean extends NamedEntity<Integer> {

		private BigDecimal bigDecimalField;
		private int intField;
		private double doubleField;


		public BigDecimal getBigDecimalField() {
			return this.bigDecimalField;
		}


		public void setBigDecimalField(BigDecimal bigDecimalField) {
			this.bigDecimalField = bigDecimalField;
		}


		public int getIntField() {
			return this.intField;
		}


		public void setIntField(int intField) {
			this.intField = intField;
		}


		public double getDoubleField() {
			return this.doubleField;
		}


		public void setDoubleField(double doubleField) {
			this.doubleField = doubleField;
		}
	}
}
