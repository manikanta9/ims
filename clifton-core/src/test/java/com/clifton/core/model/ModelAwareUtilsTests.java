package com.clifton.core.model;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.http.HttpMethod;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class ModelAwareUtilsTests {


	@Test
	public void testToModelObjectList() {

		List<ModelAwareTestEntity> list = null;
		List<TestModel> modelList = null;

		modelList = ModelAwareUtils.getModelObjectList(list);
		Assertions.assertNull(modelList);

		list = new ArrayList<>();
		modelList = ModelAwareUtils.getModelObjectList(list);
		Assertions.assertNotNull(modelList);
		Assertions.assertTrue(CollectionUtils.isEmpty(ModelAwareUtils.getModelObjectList(list)));

		list.add(new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null));
		list.add(new ModelAwareTestEntity(MathUtils.SHORT_TWO, "Test 2", "2nd Test"));
		list.add(new ModelAwareTestEntity(MathUtils.SHORT_THREE, "Test 3", "3rd Test"));

		modelList = ModelAwareUtils.getModelObjectList(list);
		Assertions.assertEquals(3, CollectionUtils.getSize(modelList));
	}


	@Test
	public void testCopyEnumProperties() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		modelAwareTestEntity.setHttpMethod(ModelAwareTestEntity.TestHttpMethod.GET);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals(HttpMethod.GET, testModel.getHttpMethod());
	}


	@Test
	public void testCopyLocalDateProperties() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = new Date();
		modelAwareTestEntity.setDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals(date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), testModel.getDate());
	}


	@Test
	public void testCopyLocalDateProperties_TimezoneMinus12() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = DateUtils.toDate("2021-10-21T12:20:30-1200", DateUtils.DATE_FORMAT_WITH_TIMEZONE);
		modelAwareTestEntity.setDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals("2021-10-21", testModel.getDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_ISO)));
	}


	@Test
	public void testCopyLocalDateProperties_TimezoneMinus7() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = DateUtils.toDate("2021-10-21T12:20:30-0700", DateUtils.DATE_FORMAT_WITH_TIMEZONE);
		modelAwareTestEntity.setDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals("2021-10-21", testModel.getDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_ISO)));
	}


	@Test
	public void testCopyLocalDateProperties_TimezonePlus12() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = DateUtils.toDate("2021-10-21T12:20:30+1200", DateUtils.DATE_FORMAT_WITH_TIMEZONE);
		modelAwareTestEntity.setDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals("2021-10-20", testModel.getDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_ISO)));
	}


	@Test
	public void testCopyOffsetDateTimeProperties() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = new Date();
		modelAwareTestEntity.setOffsetDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals(date.toInstant().atOffset(ZoneOffset.UTC), testModel.getOffsetDate());
	}


	@Test
	public void testCopyOffsetDateTimeProperties_TimezoneMinus12() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = DateUtils.toDate("2021-10-21T12:20:30-1200", DateUtils.DATE_FORMAT_WITH_TIMEZONE);
		modelAwareTestEntity.setOffsetDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals("2021-10-22T00:20:30GMT", testModel.getOffsetDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_WITH_TIMEZONE)));
	}


	@Test
	public void testCopyOffsetDateTimeProperties_TimezoneMinus7() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = DateUtils.toDate("2021-10-21T12:20:30-0700", DateUtils.DATE_FORMAT_WITH_TIMEZONE);
		modelAwareTestEntity.setOffsetDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals("2021-10-21T19:20:30GMT", testModel.getOffsetDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_WITH_TIMEZONE)));
	}


	@Test
	public void testCopyOffsetDateTimeProperties_TimezonePlus12() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		Date date = DateUtils.toDate("2021-10-21T11:20:30+1200", DateUtils.DATE_FORMAT_WITH_TIMEZONE);
		modelAwareTestEntity.setOffsetDate(date);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals("2021-10-20T23:20:30GMT", testModel.getOffsetDate().format(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_WITH_TIMEZONE)));
	}


	@Test
	public void testCopyGeneralProperties() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		modelAwareTestEntity.setNumber(new BigDecimal(1));
		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);
		Assertions.assertEquals(Integer.valueOf(1), testModel.getId());
		Assertions.assertEquals("Test 1", testModel.getName());
		Assertions.assertEquals(new BigDecimal(1), testModel.getNumber());
	}


	@Test
	public void testIgnoreProperties() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);
		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel, "name");
		Assertions.assertEquals(Integer.valueOf(1), testModel.getId());
		Assertions.assertNull(testModel.getName());
	}


	@Test
	public void testNumericConversions() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);

		modelAwareTestEntity.setBoxedIntToPrimitiveShort(2);
		modelAwareTestEntity.setPrimitiveIntToBoxedShort(3);
		modelAwareTestEntity.setBoxedShortToPrimitiveInt((short) 4);
		modelAwareTestEntity.setPrimitiveShortToBoxedInt((short) 5);
		modelAwareTestEntity.setBoxedFloatToPrimitiveDouble(6.1f);
		modelAwareTestEntity.setPrimitiveFloatToBoxedDouble(7.2f);
		modelAwareTestEntity.setBoxedDoubleToPrimitiveFloat(8.3d);
		modelAwareTestEntity.setPrimitiveDoubleToBoxedFloat(9.4d);
		modelAwareTestEntity.setBoxedByteToPrimitiveLong((byte) 10);
		modelAwareTestEntity.setPrimitiveByteToBoxedLong((byte) 11);
		modelAwareTestEntity.setBoxedLongToPrimitiveByte((long) 12);
		modelAwareTestEntity.setPrimitiveLongToBoxedByte(13);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);

		Assertions.assertEquals(2, testModel.getBoxedIntToPrimitiveShort());
		Assertions.assertEquals((short) 3, testModel.getPrimitiveIntToBoxedShort());
		Assertions.assertEquals(4, testModel.getBoxedShortToPrimitiveInt());
		Assertions.assertEquals(5, testModel.getPrimitiveShortToBoxedInt());
		Assertions.assertEquals(6.1f, testModel.getBoxedFloatToPrimitiveDouble());
		Assertions.assertEquals(7.2d, testModel.getPrimitiveFloatToBoxedDouble());
		Assertions.assertEquals(8.3f, testModel.getBoxedDoubleToPrimitiveFloat());
		Assertions.assertEquals(9.4f, testModel.getPrimitiveDoubleToBoxedFloat());
		Assertions.assertEquals(10, testModel.getBoxedByteToPrimitiveLong());
		Assertions.assertEquals(11, testModel.getPrimitiveByteToBoxedLong());
		Assertions.assertEquals(12, testModel.getBoxedLongToPrimitiveByte());
		Assertions.assertEquals((byte) 13, testModel.getPrimitiveLongToBoxedByte());
	}


	@Test
	public void testCopyListProperty() {
		ModelAwareTestEntity modelAwareTestEntity = new ModelAwareTestEntity(MathUtils.SHORT_ONE, "Test 1", null);

		List<ModelAwareTestEntity> modelList = new ArrayList<>();
		modelList.add(new ModelAwareTestEntity(MathUtils.SHORT_TWO, "Test 2", null));
		modelList.add(new ModelAwareTestEntity(MathUtils.SHORT_THREE, "Test 3", null));
		modelAwareTestEntity.setModelList(modelList);

		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(modelAwareTestEntity, testModel);

		Assertions.assertEquals(2, testModel.getModelList().size());
		Assertions.assertEquals(Integer.valueOf(2), testModel.getModelList().get(0).getId());
		Assertions.assertEquals("Test 2", testModel.getModelList().get(0).getName());
		Assertions.assertEquals(Integer.valueOf(3), testModel.getModelList().get(1).getId());
		Assertions.assertEquals("Test 3", testModel.getModelList().get(1).getName());
	}
}
