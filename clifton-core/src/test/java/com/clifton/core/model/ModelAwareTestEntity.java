package com.clifton.core.model;

import com.clifton.core.beans.NamedEntity;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author manderson
 */
public class ModelAwareTestEntity extends NamedEntity<Short> implements ModelAware<TestModel> {

	private TestHttpMethod httpMethod;

	private BigDecimal number;

	private Date date;

	private Date offsetDate;

	private List<ModelAwareTestEntity> modelList;

	private Integer boxedIntToPrimitiveShort;
	private int primitiveIntToBoxedShort;

	private Short boxedShortToPrimitiveInt;
	private short primitiveShortToBoxedInt;

	private Float boxedFloatToPrimitiveDouble;
	private float primitiveFloatToBoxedDouble;

	private Double boxedDoubleToPrimitiveFloat;
	private double primitiveDoubleToBoxedFloat;

	private Byte boxedByteToPrimitiveLong;
	private byte primitiveByteToBoxedLong;

	private Long boxedLongToPrimitiveByte;
	private long primitiveLongToBoxedByte;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ModelAwareTestEntity(short id, String name, String description) {
		super();
		setId(id);
		setName(name);
		setDescription(description);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TestModel toModelObject() {
		TestModel testModel = new TestModel();
		ModelAwareUtils.copyPropertiesToModelObject(this, testModel);
		return testModel;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TestHttpMethod getHttpMethod() {
		return this.httpMethod;
	}


	public void setHttpMethod(TestHttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}


	public BigDecimal getNumber() {
		return this.number;
	}


	public void setNumber(BigDecimal number) {
		this.number = number;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Date getOffsetDate() {
		return this.offsetDate;
	}


	public void setOffsetDate(Date offsetDate) {
		this.offsetDate = offsetDate;
	}


	public List<ModelAwareTestEntity> getModelList() {
		return this.modelList;
	}


	public void setModelList(List<ModelAwareTestEntity> modelList) {
		this.modelList = modelList;
	}


	public Integer getBoxedIntToPrimitiveShort() {
		return this.boxedIntToPrimitiveShort;
	}


	public void setBoxedIntToPrimitiveShort(Integer boxedIntToPrimitiveShort) {
		this.boxedIntToPrimitiveShort = boxedIntToPrimitiveShort;
	}


	public int getPrimitiveIntToBoxedShort() {
		return this.primitiveIntToBoxedShort;
	}


	public void setPrimitiveIntToBoxedShort(int primitiveIntToBoxedShort) {
		this.primitiveIntToBoxedShort = primitiveIntToBoxedShort;
	}


	public Short getBoxedShortToPrimitiveInt() {
		return this.boxedShortToPrimitiveInt;
	}


	public void setBoxedShortToPrimitiveInt(Short boxedShortToPrimitiveInt) {
		this.boxedShortToPrimitiveInt = boxedShortToPrimitiveInt;
	}


	public short getPrimitiveShortToBoxedInt() {
		return this.primitiveShortToBoxedInt;
	}


	public void setPrimitiveShortToBoxedInt(short primitiveShortToBoxedInt) {
		this.primitiveShortToBoxedInt = primitiveShortToBoxedInt;
	}


	public Float getBoxedFloatToPrimitiveDouble() {
		return this.boxedFloatToPrimitiveDouble;
	}


	public void setBoxedFloatToPrimitiveDouble(Float boxedFloatToPrimitiveDouble) {
		this.boxedFloatToPrimitiveDouble = boxedFloatToPrimitiveDouble;
	}


	public float getPrimitiveFloatToBoxedDouble() {
		return this.primitiveFloatToBoxedDouble;
	}


	public void setPrimitiveFloatToBoxedDouble(float primitiveFloatToBoxedDouble) {
		this.primitiveFloatToBoxedDouble = primitiveFloatToBoxedDouble;
	}


	public Double getBoxedDoubleToPrimitiveFloat() {
		return this.boxedDoubleToPrimitiveFloat;
	}


	public void setBoxedDoubleToPrimitiveFloat(Double boxedDoubleToPrimitiveFloat) {
		this.boxedDoubleToPrimitiveFloat = boxedDoubleToPrimitiveFloat;
	}


	public double getPrimitiveDoubleToBoxedFloat() {
		return this.primitiveDoubleToBoxedFloat;
	}


	public void setPrimitiveDoubleToBoxedFloat(double primitiveDoubleToBoxedFloat) {
		this.primitiveDoubleToBoxedFloat = primitiveDoubleToBoxedFloat;
	}


	public Byte getBoxedByteToPrimitiveLong() {
		return this.boxedByteToPrimitiveLong;
	}


	public void setBoxedByteToPrimitiveLong(Byte boxedByteToPrimitiveLong) {
		this.boxedByteToPrimitiveLong = boxedByteToPrimitiveLong;
	}


	public byte getPrimitiveByteToBoxedLong() {
		return this.primitiveByteToBoxedLong;
	}


	public void setPrimitiveByteToBoxedLong(byte primitiveByteToBoxedLong) {
		this.primitiveByteToBoxedLong = primitiveByteToBoxedLong;
	}


	public Long getBoxedLongToPrimitiveByte() {
		return this.boxedLongToPrimitiveByte;
	}


	public void setBoxedLongToPrimitiveByte(Long boxedLongToPrimitiveByte) {
		this.boxedLongToPrimitiveByte = boxedLongToPrimitiveByte;
	}


	public long getPrimitiveLongToBoxedByte() {
		return this.primitiveLongToBoxedByte;
	}


	public void setPrimitiveLongToBoxedByte(long primitiveLongToBoxedByte) {
		this.primitiveLongToBoxedByte = primitiveLongToBoxedByte;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Test enum to test copying enum properties with the same name but different classes.
	 */
	public enum TestHttpMethod {

		GET,
		POST,
		PUT,
		DELETE,
		HEAD,
		PATCH
	}
}
