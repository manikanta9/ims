package com.clifton.core.model;

import org.springframework.http.HttpMethod;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.List;


/**
 * @author manderson
 */
public class TestModel implements Serializable {

	private Integer id;

	private String name;

	private String description;

	private HttpMethod httpMethod;

	private BigDecimal number;

	private LocalDate date;

	private OffsetDateTime offsetDate;

	private List<TestModel> modelList;

	private short boxedIntToPrimitiveShort;
	private Short primitiveIntToBoxedShort;

	private int boxedShortToPrimitiveInt;
	private Integer primitiveShortToBoxedInt;

	private double boxedFloatToPrimitiveDouble;
	private Double primitiveFloatToBoxedDouble;

	private float boxedDoubleToPrimitiveFloat;
	private Float primitiveDoubleToBoxedFloat;

	private long boxedByteToPrimitiveLong;
	private Long primitiveByteToBoxedLong;

	private byte boxedLongToPrimitiveByte;
	private Byte primitiveLongToBoxedByte;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public HttpMethod getHttpMethod() {
		return this.httpMethod;
	}


	public void setHttpMethod(HttpMethod httpMethod) {
		this.httpMethod = httpMethod;
	}


	public BigDecimal getNumber() {
		return this.number;
	}


	public void setNumber(BigDecimal number) {
		this.number = number;
	}


	public LocalDate getDate() {
		return this.date;
	}


	public void setDate(LocalDate date) {
		this.date = date;
	}


	public OffsetDateTime getOffsetDate() {
		return this.offsetDate;
	}


	public void setOffsetDate(OffsetDateTime offsetDate) {
		this.offsetDate = offsetDate;
	}


	public List<TestModel> getModelList() {
		return this.modelList;
	}


	public void setModelList(List<TestModel> modelList) {
		this.modelList = modelList;
	}


	public short getBoxedIntToPrimitiveShort() {
		return this.boxedIntToPrimitiveShort;
	}


	public void setBoxedIntToPrimitiveShort(short boxedIntToPrimitiveShort) {
		this.boxedIntToPrimitiveShort = boxedIntToPrimitiveShort;
	}


	public Short getPrimitiveIntToBoxedShort() {
		return this.primitiveIntToBoxedShort;
	}


	public void setPrimitiveIntToBoxedShort(Short primitiveIntToBoxedShort) {
		this.primitiveIntToBoxedShort = primitiveIntToBoxedShort;
	}


	public int getBoxedShortToPrimitiveInt() {
		return this.boxedShortToPrimitiveInt;
	}


	public void setBoxedShortToPrimitiveInt(int boxedShortToPrimitiveInt) {
		this.boxedShortToPrimitiveInt = boxedShortToPrimitiveInt;
	}


	public Integer getPrimitiveShortToBoxedInt() {
		return this.primitiveShortToBoxedInt;
	}


	public void setPrimitiveShortToBoxedInt(Integer primitiveShortToBoxedInt) {
		this.primitiveShortToBoxedInt = primitiveShortToBoxedInt;
	}


	public double getBoxedFloatToPrimitiveDouble() {
		return this.boxedFloatToPrimitiveDouble;
	}


	public void setBoxedFloatToPrimitiveDouble(double boxedFloatToPrimitiveDouble) {
		this.boxedFloatToPrimitiveDouble = boxedFloatToPrimitiveDouble;
	}


	public Double getPrimitiveFloatToBoxedDouble() {
		return this.primitiveFloatToBoxedDouble;
	}


	public void setPrimitiveFloatToBoxedDouble(Double primitiveFloatToBoxedDouble) {
		this.primitiveFloatToBoxedDouble = primitiveFloatToBoxedDouble;
	}


	public float getBoxedDoubleToPrimitiveFloat() {
		return this.boxedDoubleToPrimitiveFloat;
	}


	public void setBoxedDoubleToPrimitiveFloat(float boxedDoubleToPrimitiveFloat) {
		this.boxedDoubleToPrimitiveFloat = boxedDoubleToPrimitiveFloat;
	}


	public Float getPrimitiveDoubleToBoxedFloat() {
		return this.primitiveDoubleToBoxedFloat;
	}


	public void setPrimitiveDoubleToBoxedFloat(Float primitiveDoubleToBoxedFloat) {
		this.primitiveDoubleToBoxedFloat = primitiveDoubleToBoxedFloat;
	}


	public long getBoxedByteToPrimitiveLong() {
		return this.boxedByteToPrimitiveLong;
	}


	public void setBoxedByteToPrimitiveLong(long boxedByteToPrimitiveLong) {
		this.boxedByteToPrimitiveLong = boxedByteToPrimitiveLong;
	}


	public Long getPrimitiveByteToBoxedLong() {
		return this.primitiveByteToBoxedLong;
	}


	public void setPrimitiveByteToBoxedLong(Long primitiveByteToBoxedLong) {
		this.primitiveByteToBoxedLong = primitiveByteToBoxedLong;
	}


	public byte getBoxedLongToPrimitiveByte() {
		return this.boxedLongToPrimitiveByte;
	}


	public void setBoxedLongToPrimitiveByte(byte boxedLongToPrimitiveByte) {
		this.boxedLongToPrimitiveByte = boxedLongToPrimitiveByte;
	}


	public Byte getPrimitiveLongToBoxedByte() {
		return this.primitiveLongToBoxedByte;
	}


	public void setPrimitiveLongToBoxedByte(Byte primitiveLongToBoxedByte) {
		this.primitiveLongToBoxedByte = primitiveLongToBoxedByte;
	}
}
