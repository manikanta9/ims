package com.clifton.core.validation.hierarchy;

import com.clifton.core.beans.hierarchy.NamedHierarchicalEntityTestObject;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class HierarchicalObjectObserverImplTests {

	@Resource
	private UpdatableDAO<NamedHierarchicalEntityTestObject> namedHierarchicalEntityTestObjectDAO;


	@Test
	public void testSimpleValidate() {
		NamedHierarchicalEntityTestObject bean1 = this.namedHierarchicalEntityTestObjectDAO.findByPrimaryKey(1);
		NamedHierarchicalEntityTestObject bean2 = this.namedHierarchicalEntityTestObjectDAO.findByPrimaryKey(2);
		this.namedHierarchicalEntityTestObjectDAO.save(bean1);
		this.namedHierarchicalEntityTestObjectDAO.save(bean2);

		Assertions.assertEquals("Root", bean1.getNameExpanded());
		Assertions.assertEquals("Root / First Child of Root", bean2.getNameExpanded());

		Assertions.assertEquals("Root", bean1.getLabelExpanded());
		Assertions.assertEquals("Root / First Child of Root", bean2.getLabelExpanded());
	}


	@Test
	public void testValidateParentIsSelf() {
		Assertions.assertThrows(SameHierarchyParentException.class, () -> {
			NamedHierarchicalEntityTestObject bean = this.namedHierarchicalEntityTestObjectDAO.findByPrimaryKey(2);
			bean.setParent(bean);
			this.namedHierarchicalEntityTestObjectDAO.save(bean);
		});
	}


	@Test
	public void testValidateParentIsItsOwnChild() {
		Assertions.assertThrows(ParentIsChildException.class, () -> {
			NamedHierarchicalEntityTestObject parent = this.namedHierarchicalEntityTestObjectDAO.findByPrimaryKey(1);
			NamedHierarchicalEntityTestObject child = this.namedHierarchicalEntityTestObjectDAO.findByPrimaryKey(2);
			parent.setParent(child);
			this.namedHierarchicalEntityTestObjectDAO.save(parent);
		});
	}
}
