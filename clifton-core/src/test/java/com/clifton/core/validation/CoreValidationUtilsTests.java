package com.clifton.core.validation;


import com.clifton.core.beans.BeanUtilsTestBean;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;


public class CoreValidationUtilsTests {


	@Test
	public void testAssertAllowedModifiedFieldsNoChange() {
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setId(1);
		b1.setName("name");
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setLabel("label");
		b1.setRv(new byte[]{1, 2, 3, 4});
		b1.setUpdateUserId((short) 0);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setId(1);
		b2.setName("name");
		b2.setBigDecimal(BigDecimal.ONE);
		b2.setLabel("label");
		b2.setRv(new byte[]{1, 2, 3, 4});
		b2.setUpdateUserId((short) 0);
		CoreValidationUtils.assertAllowedModifiedFields(b1, b2);
	}


	@Test
	public void testAssertAllowedModifiedFieldsSuccess() {
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setId(1);
		b1.setName("name1");
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setLabel("label1");
		b1.setRv(new byte[]{1, 2, 3, 4});
		b1.setUpdateUserId((short) 0);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setId(2);
		b2.setName("name2");
		b2.setBigDecimal(BigDecimal.TEN);
		b2.setLabel("label2");
		b2.setRv(new byte[]{5, 6, 7, 8});
		b2.setUpdateUserId((short) 1);
		CoreValidationUtils.assertAllowedModifiedFields(b1, b2, "id", "name", "bigDecimal", "label", "rv", "updateUserId");
	}


	@Test
	public void testAssertAllowedModifiedFieldsFailure() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			BeanUtilsTestBean b1 = new BeanUtilsTestBean();
			b1.setId(1);
			b1.setName("name1");
			b1.setBigDecimal(BigDecimal.ONE);
			b1.setLabel("label1");
			b1.setRv(new byte[]{1, 2, 3, 4});
			b1.setUpdateUserId((short) 1);
			BeanUtilsTestBean b2 = new BeanUtilsTestBean();
			b2.setId(2);
			b2.setName("name2");
			b2.setBigDecimal(BigDecimal.TEN);
			b2.setLabel("label2");
			b2.setRv(new byte[]{5, 6, 7, 8}); // System field (not validated)
			b2.setUpdateUserId((short) 1); // System field (not validated)
			CoreValidationUtils.assertAllowedModifiedFields(b1, b2, "name");
		});
		Assertions.assertEquals("Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("label", "id", "bigDecimal"), 10), validationException.getMessage());
	}


	@Test
	public void testAssertDisallowedModifiedFieldsNoChange() {
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setId(1);
		b1.setName("name");
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setLabel("label");
		b1.setRv(new byte[]{1, 2, 3, 4});
		b1.setUpdateUserId((short) 0);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setId(1);
		b2.setName("name");
		b2.setBigDecimal(BigDecimal.ONE);
		b2.setLabel("label");
		b2.setRv(new byte[]{1, 2, 3, 4});
		b2.setUpdateUserId((short) 0);
		CoreValidationUtils.assertDisallowedModifiedFields(b1, b2, "id", "name", "bigDecimal", "label", "rv", "updateUserId");
	}


	@Test
	public void testAssertDisallowedModifiedFieldsSuccess() {
		BeanUtilsTestBean b1 = new BeanUtilsTestBean();
		b1.setId(1);
		b1.setName("name1");
		b1.setBigDecimal(BigDecimal.ONE);
		b1.setLabel("label1");
		b1.setRv(new byte[]{1, 2, 3, 4});
		b1.setUpdateUserId((short) 0);
		BeanUtilsTestBean b2 = new BeanUtilsTestBean();
		b2.setId(1);
		b2.setName("name2");
		b2.setBigDecimal(BigDecimal.TEN);
		b2.setLabel("label2");
		b2.setRv(new byte[]{5, 6, 7, 8});
		b2.setUpdateUserId((short) 1);
		CoreValidationUtils.assertDisallowedModifiedFields(b1, b2, "id");
	}


	@Test
	public void testAssertDisallowedModifiedFieldsFailure() {
		ValidationException validationException = Assertions.assertThrows(FieldValidationException.class, () -> {
			BeanUtilsTestBean b1 = new BeanUtilsTestBean();
			b1.setId(1);
			b1.setName("name1");
			b1.setBigDecimal(BigDecimal.ONE);
			b1.setLabel("label1");
			b1.setRv(new byte[]{1, 2, 3, 4});
			b1.setUpdateUserId((short) 1);
			BeanUtilsTestBean b2 = new BeanUtilsTestBean();
			b2.setId(1);
			b2.setName("name2");
			b2.setBigDecimal(BigDecimal.TEN);
			b2.setLabel("label2");
			b2.setRv(new byte[]{5, 6, 7, 8});
			b2.setUpdateUserId((short) 1);
			CoreValidationUtils.assertDisallowedModifiedFields(b1, b2, "id", "name", "bigDecimal", "label");
		});
		Assertions.assertEquals("Modifications to the given fields are not allowed: " + CollectionUtils.toString(Arrays.asList("name", "bigDecimal", "label"), 10), validationException.getMessage());
	}
}
