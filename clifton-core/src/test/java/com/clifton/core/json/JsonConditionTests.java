package com.clifton.core.json;

import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.json.condition.JsonConditionBuilder;
import com.clifton.core.json.condition.JsonConditionOperatorTypes;
import com.clifton.core.json.condition.evaluator.util.JsonConditionEvaluatorUtils;
import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
@ContextConfiguration(locations = "../CoreProjectBasicTests-context.xml")
@ExtendWith(SpringExtension.class)
public class JsonConditionTests {


	private static final Map<String, Object> conditionData;


	static {
		Map<String, Object> map = new HashMap<>();
		map.put("ACC NO", "25848000");
		map.put("TICKER", "SPY");
		map.put("SEC TYPE", "MUTUAL FUNDS");
		map.put("QUANTITY", 50000);
		map.put("DATE", DateUtils.toDate("04/03/81"));
		map.put("OPTION ID", null);
		conditionData = Collections.unmodifiableMap(map);
	}


	@Test
	public void testFreeMarkerEvaluator() {
		JsonConditionBuilder ruleBuilder = new JsonConditionBuilder();
		JsonCondition conditionRule = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("QUANTITY").withValue("<#if fieldValue lt 51000>true<#else>false</#if>").ofOperatorType(JsonConditionOperatorTypes.FREE_MARKER).build();
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(conditionRule, conditionData));

		conditionRule = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("QUANTITY").withValue("<#if fieldValue gt 51000>true<#else>false</#if>").ofOperatorType(JsonConditionOperatorTypes.FREE_MARKER).build();
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(conditionRule, conditionData));
	}


	@Test
	public void testConditionEqualsEvaluator() {
		//Equal True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.EQUAL).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUND", JsonConditionOperatorTypes.EQUAL).build(), conditionData));

		//Not Equal True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUND", JsonConditionOperatorTypes.NOT_EQUAL).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.NOT_EQUAL).build(), conditionData));

		//CASE SENSITIVE - Equal True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.EQUAL_CS).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDs", JsonConditionOperatorTypes.EQUAL_CS).build(), conditionData));

		//CASE SENSITIVE - Not Equal True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDs", JsonConditionOperatorTypes.NOT_EQUAL_CS).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.NOT_EQUAL_CS).build(), conditionData));


		//For the empty/null value tests the values are always pulled from the valueMap, not the ruleValue passed to the condition
		//So we pass new maps here with the values set rather than using the static map.
		//Is Empty True/False
		Map<String, Object> emptyValueMap = new HashMap<>();
		emptyValueMap.put("SEC TYPE", "");
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "", JsonConditionOperatorTypes.IS_EMPTY).build(), emptyValueMap));
		emptyValueMap.put("SEC TYPE", "MUTUAL FUNDS");
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.IS_EMPTY).build(), emptyValueMap));

		//Is Not Empty True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.IS_NOT_EMPTY).build(), emptyValueMap));
		emptyValueMap.put("SEC TYPE", "");
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "", JsonConditionOperatorTypes.IS_NOT_EMPTY).build(), emptyValueMap));

		Map<String, Object> nullValueMap = new HashMap<>();
		nullValueMap.put("SEC TYPE", null);
		//Is Null True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.IS_NULL).build(), nullValueMap));
		nullValueMap.put("SEC TYPE", "MUTUAL FUNDS");
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.IS_NULL).build(), nullValueMap));

		//Is Not Null True/False
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.IS_NOT_NULL).build(), nullValueMap));
		nullValueMap.put("SEC TYPE", null);
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.IS_NOT_NULL).build(), nullValueMap));
	}


	@Test
	public void testConditionContainsEvaluator() {
		//Contains
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.CONTAINS).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.CONTAINS).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "LIABILITY", JsonConditionOperatorTypes.CONTAINS).build(), conditionData));

		//Not Contains
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.NOT_CONTAINS).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "LIABILITY", JsonConditionOperatorTypes.NOT_CONTAINS).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.NOT_CONTAINS).build(), conditionData));

		//Begins With
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.BEGINS_WITH).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL", JsonConditionOperatorTypes.BEGINS_WITH).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.BEGINS_WITH).build(), conditionData));

		//Not Begins With
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.NOT_BEGINS_WITH).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.NOT_BEGINS_WITH).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL", JsonConditionOperatorTypes.NOT_BEGINS_WITH).build(), conditionData));

		//Ends With
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.ENDS_WITH).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.ENDS_WITH).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL", JsonConditionOperatorTypes.ENDS_WITH).build(), conditionData));

		//Not Ends With
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.NOT_ENDS_WITH).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "MUTUAL", JsonConditionOperatorTypes.NOT_ENDS_WITH).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.NOT_ENDS_WITH).build(), conditionData));
	}


	@Test
	public void testConditionInEvaluator() {
		//In
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "LIABILITY,FUNDS,MUTUAL FUNDS", JsonConditionOperatorTypes.IN).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "LIABILITY,FUNDS", JsonConditionOperatorTypes.IN).build(), conditionData));

		//Not In
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "LIABILITY,FUNDS", JsonConditionOperatorTypes.NOT_IN).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", "LIABILITY,FUNDS,MUTUAL FUNDS", JsonConditionOperatorTypes.NOT_IN).build(), conditionData));

		//In/NotIn null value expected exceptions
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.IN).build(), conditionData));
			Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringTextBuilder("SEC TYPE", null, JsonConditionOperatorTypes.NOT_IN).build(), conditionData));
		});
		Assertions.assertEquals("Operation [in] supports only one non-null comma-separated parameter!", validationException.getMessage());
	}


	@Test
	public void testConditionRangeEvaluator() {
		//Less
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", null, JsonConditionOperatorTypes.LESS).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "51000", JsonConditionOperatorTypes.LESS).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "50000", JsonConditionOperatorTypes.LESS).build(), conditionData));

		//Less Or Equal
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", null, JsonConditionOperatorTypes.LESS_OR_EQUAL).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "50000", JsonConditionOperatorTypes.LESS_OR_EQUAL).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "49000", JsonConditionOperatorTypes.LESS_OR_EQUAL).build(), conditionData));

		//Greater
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", null, JsonConditionOperatorTypes.GREATER).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "49000", JsonConditionOperatorTypes.GREATER).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "50000", JsonConditionOperatorTypes.GREATER).build(), conditionData));

		//Greater Or Equal
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", null, JsonConditionOperatorTypes.GREATER_OR_EQUAL).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "50000", JsonConditionOperatorTypes.GREATER_OR_EQUAL).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", "51000", JsonConditionOperatorTypes.GREATER_OR_EQUAL).build(), conditionData));

		//Between
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", null, JsonConditionOperatorTypes.BETWEEN).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", Arrays.asList("49000", "51000"), JsonConditionOperatorTypes.BETWEEN).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", Arrays.asList("47000", "49000"), JsonConditionOperatorTypes.BETWEEN).build(), conditionData));

		//Not Between
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", null, JsonConditionOperatorTypes.NOT_BETWEEN).build(), conditionData));
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", Arrays.asList("47000", "49000"), JsonConditionOperatorTypes.NOT_BETWEEN).build(), conditionData));
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(getStringNumberBuilder("QUANTITY", Arrays.asList("9000", "51000"), JsonConditionOperatorTypes.NOT_BETWEEN).build(), conditionData));
	}


	@Test
	public void testAndConditions() {
		List<JsonCondition> andConditions = new ArrayList<>();
		andConditions.add(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.EQUAL).build());
		andConditions.add(getStringTextBuilder("ACC NO", "25848000", JsonConditionOperatorTypes.EQUAL).build());

		JsonConditionBuilder jsonConditionBuilder = new JsonConditionBuilder();
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(jsonConditionBuilder.andCondition(andConditions).build(), conditionData));

		andConditions.add(getStringTextBuilder("TICKER", "I SPY!", JsonConditionOperatorTypes.EQUAL).build());
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(jsonConditionBuilder.andCondition(andConditions).build(), conditionData));
	}


	@Test
	public void testOrConditions() {
		List<JsonCondition> orConditions = new ArrayList<>();
		orConditions.add(getStringTextBuilder("SEC TYPE", "LIABILITY", JsonConditionOperatorTypes.EQUAL).build());
		orConditions.add(getStringTextBuilder("SEC TYPE", "MUTUAL FUNDS", JsonConditionOperatorTypes.EQUAL).build());

		JsonConditionBuilder ruleBuilder = new JsonConditionBuilder();
		Assertions.assertTrue(JsonConditionEvaluatorUtils.evaluateCondition(ruleBuilder.orCondition(orConditions).build(), conditionData));

		orConditions.remove(1);
		orConditions.add(getStringTextBuilder("SEC TYPE", "FUNDS", JsonConditionOperatorTypes.EQUAL).build());
		Assertions.assertFalse(JsonConditionEvaluatorUtils.evaluateCondition(ruleBuilder.orCondition(orConditions).build(), conditionData));
	}


	@Test
	public void testAndOrConditions() {
		JsonConditionBuilder typeMatches = getStringTextBuilder("SEC TYPE", "LIABILITY", JsonConditionOperatorTypes.EQUAL);
		JsonConditionBuilder accountMatches = getStringTextBuilder("ACC NO", "25848000", JsonConditionOperatorTypes.EQUAL);
		JsonCondition tickerMatches = getStringTextBuilder("TICKER", "SPY", JsonConditionOperatorTypes.EQUAL).build();
		JsonCondition tickerMaybeMatches = getStringTextBuilder("TICKER", "I SPY!", JsonConditionOperatorTypes.EQUAL).build();

		//Line breaks and tabs for clarity
		Assertions.assertTrue(
				JsonConditionEvaluatorUtils.evaluateCondition(
						typeMatches.andCondition(
								Collections.singletonList(
										accountMatches.orCondition(
												Arrays.asList(tickerMatches, tickerMaybeMatches)
										).build()
								)
						).build(), conditionData
				)
		);
	}


	private JsonConditionBuilder getStringTextBuilder(String field, Object value, JsonConditionOperatorTypes operator) {
		return new JsonConditionBuilder()
				.withField(field).withValue(value)
				.ofDataType(JsonDataTypes.STRING)
				.ofInputType(JsonInputTypes.TEXT)
				.ofOperatorType(operator);
	}


	private JsonConditionBuilder getStringNumberBuilder(String field, Object value, JsonConditionOperatorTypes operator) {
		return new JsonConditionBuilder()
				.withField(field).withValue(value)
				.ofDataType(JsonDataTypes.DECIMAL)
				.ofInputType(JsonInputTypes.NUMBER)
				.ofOperatorType(operator);
	}
}
