package com.clifton.core.json;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.json.function.JsonFunctionBuilder;
import com.clifton.core.json.function.JsonFunctionOperatorTypes;
import com.clifton.core.json.function.evaluator.BaseJsonFunctionNumberEvaluator;
import com.clifton.core.json.function.evaluator.util.JsonFunctionEvaluatorUtils;
import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * @author StevenF
 */
@ContextConfiguration(locations = "../CoreProjectBasicTests-context.xml")
@ExtendWith(SpringExtension.class)
public class JsonFunctionTests {

	private static final Map<String, Object> functionData;


	static {
		Map<String, Object> map = new HashMap<>();
		map.put("ACC NO", "25848000");
		map.put("TICKER", "SPY");
		map.put("SEC TYPE", "MUTUAL FUNDS");
		map.put("PRICE", 10);
		map.put("QUANTITY", 50000);
		map.put("NEG_QUANTITY", -503430);
		map.put("DATE", "04/03/81");
		map.put("REAL_DATE", DateUtils.toDate("04/03/1981"));
		map.put("GOOD_DATE_BAD_FORMAT", "1981/04/03");
		map.put("OPTION ID", null);
		map.put("TRIM TEST", " VALUE ");
		map.put("UPPER TEST", "upper");
		map.put("LOWER TEST", "LOWER");
		map.put("HELLO", "Hello");
		map.put("THERE", "World!");
		map.put("TRIM_ME", "I wish I was thinner.       Can you make me thinner?");
		map.put("TRASH", "c11301800051000orcl");
		map.put("GARBAGE", "c11161800049000bhp");
		functionData = Collections.unmodifiableMap(map);
	}


	@Test
	public void testJsonFunctionNumberPattern() {
		List<String> numberList = Arrays.asList("1", "-1", ".1", "-0.1", "0.1", "2352135623", "315135.16437");
		List<String> stringList = Arrays.asList("QUANTITY", "PRICE INDEX", "453.RGdserE", "-G", "0.E", "meh");
		Pattern pattern = BaseJsonFunctionNumberEvaluator.NUMBER_PATTERN;
		for (String number : numberList) {
			Assertions.assertTrue(pattern.matcher(number).matches(), number + " is not a number!");
		}
		for (String string : stringList) {
			Assertions.assertFalse(pattern.matcher(string).matches(), string + " is a number!");
		}
	}


	@Test
	public void testFreeMarkerEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();
		JsonFunction dateFieldRule = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("GOOD_DATE_BAD_FORMAT").withValue("${fieldValue?date(\"yyyy/MM/dd\")?string[\"MM/dd/yyyy\"]}").ofOperatorType(JsonFunctionOperatorTypes.FREE_MARKER).build();
		Assertions.assertEquals("04/03/1981", JsonFunctionEvaluatorUtils.evaluateFunction(dateFieldRule, functionData));

		ruleBuilder = new JsonFunctionBuilder();
		JsonFunction trashOccSymbolFieldRule = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("TRASH").withValue("${fieldValue[15..]?upper_case}<#if fieldValue[15..]?length == 3>   <#else>  </#if>${fieldValue[1..6]?date(\"MMddyy\")?string[\"yyMMdd\"]}${fieldValue[0..<1]?upper_case}${fieldValue[7..14]}").ofOperatorType(JsonFunctionOperatorTypes.FREE_MARKER).build();
		Assertions.assertEquals("ORCL  181130C00051000", JsonFunctionEvaluatorUtils.evaluateFunction(trashOccSymbolFieldRule, functionData));

		ruleBuilder = new JsonFunctionBuilder();
		JsonFunction garbageOccSymbolFieldRule = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("GARBAGE").withValue("${fieldValue[15..]?upper_case}<#if fieldValue[15..]?length == 3>   <#else>  </#if>${fieldValue[1..6]?date(\"MMddyy\")?string[\"yyMMdd\"]}${fieldValue[0..<1]?upper_case}${fieldValue[7..14]}").ofOperatorType(JsonFunctionOperatorTypes.FREE_MARKER).build();
		Assertions.assertEquals("BHP   181116C00049000", JsonFunctionEvaluatorUtils.evaluateFunction(garbageOccSymbolFieldRule, functionData));
	}


	@Test
	public void testFreeMarkerStrikePrice() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();
		JsonFunction strikePriceNormalizer = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("StrikePrice").withValue("<#if fieldValue?length != 0>${fieldValue?replace(\",\", \"\")?number?string[\"#0.000000\"]}</#if>").ofOperatorType(JsonFunctionOperatorTypes.FREE_MARKER).build();

		Map<String, Object> strikePrices = new HashMap<>();

		strikePrices.put("StrikePrice", "1.2");
		Assertions.assertEquals("1.200000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "2");
		Assertions.assertEquals("2.000000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", ".2");
		Assertions.assertEquals("0.200000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "");
		Assertions.assertEquals("", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "123.123");
		Assertions.assertEquals("123.123000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "1,123.123");
		Assertions.assertEquals("1123.123000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "222,1,123.123");
		Assertions.assertEquals("2221123.123000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));
	}


	@Test
	public void testFreeMarkerAccountAndStrikePrice() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();
		JsonFunction strikePriceNormalizer = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("StrikePrice").withValue("<#if fieldValue?? && fieldValue?length != 0 && fieldValue?matches(\"^\\\\w+[ ]{1}[0-9,]*\\\\.?[0-9]*$\")><#assign sequence = fieldValue?split(\" \")>${sequence[0]} ${sequence[1]?replace(\",\", \"\")?number?string[\"#0.000000\"]}</#if>").ofOperatorType(JsonFunctionOperatorTypes.FREE_MARKER).build();

		Map<String, Object> strikePrices = new HashMap<>();

		strikePrices.put("StrikePrice", "CLG9P 1.2");
		Assertions.assertEquals("CLG9P 1.200000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "CLG9P .2");
		Assertions.assertEquals("CLG9P 0.200000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "");
		Assertions.assertEquals("", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "CLG9P 123.123");
		Assertions.assertEquals("CLG9P 123.123000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "CLG9P 123");
		Assertions.assertEquals("CLG9P 123.000000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "CLG9P 1,123.123");
		Assertions.assertEquals("CLG9P 1123.123000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));

		strikePrices.put("StrikePrice", "CLG9P 222,1,123.123");
		Assertions.assertEquals("CLG9P 2221123.123000", JsonFunctionEvaluatorUtils.evaluateFunction(strikePriceNormalizer, strikePrices));
	}


	@Test
	public void testFunctionConcatenateEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction rule1 = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("HELLO").ofOperatorType(JsonFunctionOperatorTypes.FIELD).build();
		JsonFunction rule2 = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withValue(" ").ofOperatorType(JsonFunctionOperatorTypes.VALUE).build();
		JsonFunction rule3 = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("THERE").ofOperatorType(JsonFunctionOperatorTypes.FIELD).build();

		JsonFunction function = ruleBuilder.concatCondition(Arrays.asList(rule1, rule2, rule3)).build();
		Assertions.assertEquals("Hello World!", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionFieldEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("HELLO").ofOperatorType(JsonFunctionOperatorTypes.FIELD).build();
		Assertions.assertEquals("Hello", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionValueEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withValue(" ").ofOperatorType(JsonFunctionOperatorTypes.VALUE).build();
		Assertions.assertEquals(" ", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	//	{"valid":true,"condition":"Evaluate","rules":[{"id":"Bloomberg","field":"Bloomberg","value":[" Comdty",""],"type":"string","input":"text","operator":"replace"},{"id":"Bloomberg","field":"Bloomberg","value":[" Index",""],"type":"string","input":"text","operator":"replace"},{"id":"Bloomberg","field":"Bloomberg","value":[" Curncy",""],"type":"string","input":"text","operator":"replace"}]}
	@Test
	public void testFunctionSubstringEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("ACC NO").withValue(Arrays.asList("2", "3")).ofOperatorType(JsonFunctionOperatorTypes.SUBSTRING).build();

		Assertions.assertEquals("8", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("5", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionLeftEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("ACC NO").withValue("3").ofOperatorType(JsonFunctionOperatorTypes.LEFT).build();

		Assertions.assertEquals("258", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("2584", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionRightEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("ACC NO").withValue("2").ofOperatorType(JsonFunctionOperatorTypes.RIGHT).build();

		Assertions.assertEquals("00", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("25", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionReplaceEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("ACC NO").withValue(Arrays.asList("25", "13")).ofOperatorType(JsonFunctionOperatorTypes.REPLACE).build();

		Assertions.assertEquals("13848000", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("25848000", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionNegateEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("QUANTITY").ofOperatorType(JsonFunctionOperatorTypes.NEGATE).build();

		Assertions.assertEquals(-50000, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(50000, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionAbsoluteValueEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("NEG_QUANTITY").ofOperatorType(JsonFunctionOperatorTypes.NEGATE).build();

		Assertions.assertEquals(503430, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(-503430, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionAddEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("QUANTITY").withValue("1").ofOperatorType(JsonFunctionOperatorTypes.ADD).build();

		Assertions.assertEquals(50001, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(50000, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			JsonFunction f = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
					.withField("QUANTITY").withValue(null).ofOperatorType(JsonFunctionOperatorTypes.ADD).build();
			JsonFunctionEvaluatorUtils.evaluateFunction(f, functionData);
		});
		Assertions.assertEquals("Value for field [QUANTITY] or params [null] was null!", validationException.getMessage());
	}


	@Test
	public void testFunctionAddFieldEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("QUANTITY").withValue("QUANTITY").ofOperatorType(JsonFunctionOperatorTypes.ADD_FIELD).build();

		Assertions.assertEquals(100000, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(50000, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			JsonFunction f = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
					.withField("QUANTITY").withValue("QUANTIT").ofOperatorType(JsonFunctionOperatorTypes.ADD_FIELD).build();
			JsonFunctionEvaluatorUtils.evaluateFunction(f, functionData);
		});
		Assertions.assertEquals("Value for rule [add_field] was null after retrieving from functionData for field [QUANTIT]!", validationException.getMessage());
	}


	@Test
	public void testFunctionMultiplyEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("PRICE").withValue("20").ofOperatorType(JsonFunctionOperatorTypes.MULTIPLY).build();

		Assertions.assertEquals(200, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(10, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			JsonFunction f = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
					.withField("PRICE").withValue(null).ofOperatorType(JsonFunctionOperatorTypes.MULTIPLY).build();
			JsonFunctionEvaluatorUtils.evaluateFunction(f, functionData);
		});
		Assertions.assertEquals("Value for field [PRICE] or params [null] was null!", validationException.getMessage());
	}


	@Test
	public void testFunctionMultiplyFieldEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("PRICE").withValue("PRICE").ofOperatorType(JsonFunctionOperatorTypes.MULTIPLY_FIELD).build();

		Assertions.assertEquals(100, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(10, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			JsonFunction f = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
					.withField("PRICE").withValue("PRIC").ofOperatorType(JsonFunctionOperatorTypes.MULTIPLY_FIELD).build();
			JsonFunctionEvaluatorUtils.evaluateFunction(f, functionData);
		});
		Assertions.assertEquals("Value for rule [multiply_field] was null after retrieving from functionData for field [PRIC]!", validationException.getMessage());
	}


	@Test
	public void testFunctionDivideEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("PRICE").withValue("5").ofOperatorType(JsonFunctionOperatorTypes.DIVIDE).build();

		Assertions.assertEquals(2, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(10, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			JsonFunction f = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
					.withField("PRICE").withValue("0").ofOperatorType(JsonFunctionOperatorTypes.DIVIDE).build();
			JsonFunctionEvaluatorUtils.evaluateFunction(f, functionData);
		});
		Assertions.assertEquals("Cannot divide by zero!", validationException.getMessage());
	}


	@Test
	public void testFunctionDivideFieldEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
				.withField("PRICE").withValue("PRICE").ofOperatorType(JsonFunctionOperatorTypes.DIVIDE_FIELD).build();

		Assertions.assertEquals(1, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(10, JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));

		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			JsonFunction f = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.INTEGER)
					.withField("PRICE").withValue("PRIC").ofOperatorType(JsonFunctionOperatorTypes.DIVIDE_FIELD).build();
			JsonFunctionEvaluatorUtils.evaluateFunction(f, functionData);
		});
		Assertions.assertEquals("Value for rule [divide_field] was null after retrieving from functionData for field [PRIC]!", validationException.getMessage());
	}


	@Test
	public void testFunctionDateToStringEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.DATE)
				.withField("REAL_DATE").withValue("MM/dd/yy").ofOperatorType(JsonFunctionOperatorTypes.DATE_TO_STRING).build();

		Assertions.assertEquals("04/03/81", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("04/03/1981", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionTrimEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("TRIM TEST").ofOperatorType(JsonFunctionOperatorTypes.TRIM).build();

		Assertions.assertEquals("VALUE", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals(" VALUE ", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionTrimAllEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction trimRule = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("TRIM_ME").ofOperatorType(JsonFunctionOperatorTypes.TRIM_ALL).build();

		Assertions.assertEquals("IwishIwasthinner.Canyoumakemethinner?", JsonFunctionEvaluatorUtils.evaluateFunction(trimRule, functionData));
		Assertions.assertNotEquals("I wish I was thinner.       Can you make me thinner?", JsonFunctionEvaluatorUtils.evaluateFunction(trimRule, functionData));
	}


	@Test
	public void testFunctionUpperEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("UPPER TEST").ofOperatorType(JsonFunctionOperatorTypes.UPPER).build();

		Assertions.assertEquals("UPPER", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("upper", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}


	@Test
	public void testFunctionLowerEvaluator() {
		JsonFunctionBuilder ruleBuilder = new JsonFunctionBuilder();

		JsonFunction function = ruleBuilder.ofInputType(JsonInputTypes.TEXT).ofDataType(JsonDataTypes.STRING)
				.withField("LOWER TEST").ofOperatorType(JsonFunctionOperatorTypes.LOWER).build();

		Assertions.assertEquals("lower", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
		Assertions.assertNotEquals("LOWER", JsonFunctionEvaluatorUtils.evaluateFunction(function, functionData));
	}
}
