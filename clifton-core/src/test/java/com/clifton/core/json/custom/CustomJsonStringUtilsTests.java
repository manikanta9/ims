package com.clifton.core.json.custom;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class CustomJsonStringUtilsTests {

	private static final String STRING_COLUMN = "Test String";
	private static final String SHORT_COLUMN = "Test Short";
	private static final String INTEGER_COLUMN = "Test Integer";
	private static final String BOOLEAN_COLUMN = "Test Boolean";
	private static final String OBJECT_COLUMN = "Test Object";


	@Test
	public void testGetColumnValue_Null() {
		CustomJsonString customJsonString = null;

		Assertions.assertNull(CustomJsonStringUtils.getColumnValue(customJsonString, STRING_COLUMN));
		Assertions.assertNull(CustomJsonStringUtils.getColumnValueAsInteger(customJsonString, INTEGER_COLUMN));

		customJsonString = new CustomJsonString();
		Assertions.assertNull(CustomJsonStringUtils.getColumnValue(customJsonString, STRING_COLUMN));
		Assertions.assertNull(CustomJsonStringUtils.getColumnValueAsInteger(customJsonString, INTEGER_COLUMN));
		Assertions.assertNull(CustomJsonStringUtils.getColumnValueAsString(customJsonString, STRING_COLUMN));
		Assertions.assertNull(CustomJsonStringUtils.getColumnValueAsString(customJsonString, OBJECT_COLUMN));

		// Booleans will always default to False if null
		Assertions.assertFalse(CustomJsonStringUtils.getColumnValueAsBoolean(customJsonString, BOOLEAN_COLUMN));

		// set a value for the String
		customJsonString.setColumnValue(STRING_COLUMN, "Test 1", "Test 1");
		// Pull value for short - null
		Assertions.assertNull(CustomJsonStringUtils.getColumnValueAsShort(customJsonString, SHORT_COLUMN));
	}


	@Test
	public void testGetColumnValue_EmptyObject() {
		CustomJsonString customJsonString = new CustomJsonString("{}");
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(customJsonString);
		Assertions.assertNull(customJsonString.getJsonValue());
		Assertions.assertTrue(CollectionUtils.isEmpty(customJsonString.jsonValueMap()));

		// ensure toString() returns nothing so that an object reference isn't returned to the UI when both jsonValue and jsonValueMap are null
		Assertions.assertTrue(StringUtils.isEmpty(customJsonString.toString()));
	}


	@Test
	public void testGetColumnValueAsString() {
		// String can be used for any column regardless of the actual data type
		CustomJsonString customJsonString = buildCustomJsonString("test string", "123", "1", "true");
		Assertions.assertEquals("test string", CustomJsonStringUtils.getColumnValueAsString(customJsonString, STRING_COLUMN));
		Assertions.assertEquals("123", CustomJsonStringUtils.getColumnValueAsString(customJsonString, INTEGER_COLUMN));
		Assertions.assertEquals("1", CustomJsonStringUtils.getColumnValueAsString(customJsonString, SHORT_COLUMN));
		Assertions.assertEquals("true", CustomJsonStringUtils.getColumnValueAsString(customJsonString, BOOLEAN_COLUMN));
	}


	@Test
	public void testGetColumnValueAsInteger() {
		CustomJsonString customJsonString = buildCustomJsonString("test string", "123", "1", "true");
		Assertions.assertEquals(123, CustomJsonStringUtils.getColumnValueAsInteger(customJsonString, INTEGER_COLUMN));
		Assertions.assertEquals(1, CustomJsonStringUtils.getColumnValueAsInteger(customJsonString, SHORT_COLUMN));

		TestUtils.expectException(NumberFormatException.class, () -> {
			CustomJsonStringUtils.getColumnValueAsInteger(customJsonString, STRING_COLUMN);
		}, "For input string: \"test string\"");

		TestUtils.expectException(NumberFormatException.class, () -> {
			CustomJsonStringUtils.getColumnValueAsInteger(customJsonString, BOOLEAN_COLUMN);
		}, "For input string: \"true\"");
	}


	@Test
	public void testGetColumnValueAsShort() {
		CustomJsonString customJsonString = buildCustomJsonString("test string", "123", "1", "true");
		Assertions.assertEquals((short) 123, CustomJsonStringUtils.getColumnValueAsShort(customJsonString, INTEGER_COLUMN));
		Assertions.assertEquals((short) 1, CustomJsonStringUtils.getColumnValueAsShort(customJsonString, SHORT_COLUMN));

		TestUtils.expectException(NumberFormatException.class, () -> {
			CustomJsonStringUtils.getColumnValueAsShort(customJsonString, STRING_COLUMN);
		}, "For input string: \"test string\"");

		TestUtils.expectException(NumberFormatException.class, () -> {
			CustomJsonStringUtils.getColumnValueAsShort(customJsonString, BOOLEAN_COLUMN);
		}, "For input string: \"true\"");
	}


	@Test
	public void testGetColumnValueAsBoolean() {
		CustomJsonString customJsonString = buildCustomJsonString("test string", "123", "1", "true");
		Assertions.assertEquals(false, CustomJsonStringUtils.getColumnValueAsBoolean(customJsonString, INTEGER_COLUMN));
		// Note: This returns true because 1 is considered to be a true value
		Assertions.assertEquals(true, CustomJsonStringUtils.getColumnValueAsBoolean(customJsonString, SHORT_COLUMN));
		Assertions.assertEquals(false, CustomJsonStringUtils.getColumnValueAsBoolean(customJsonString, STRING_COLUMN));
		Assertions.assertEquals(true, CustomJsonStringUtils.getColumnValueAsBoolean(customJsonString, BOOLEAN_COLUMN));
	}

	@Test
	@SuppressWarnings("unchecked")
	public void getCustomJsonStringAsMapRecursion() {
		CustomJsonString customJsonString = new CustomJsonString("{\n" +
				"   \"map1\":\"{\\\"map1Key1\\\":[{\\\"listValue\\\":\\\"A\\\"},{\\\"listValue\\\":\\\"B\\\"}], \\\"map1Key2\\\":{\\\"map1Key2Key1\\\":\\\"C\\\"}}\",\n" +
				"   \"map2\":\"{\\\"map2Key1\\\":[{\\\"listValue\\\":\\\"D\\\"},{\\\"listValue\\\":\\\"E\\\"}]}\",\n" +
				"   \"map3\":\"{\\\"map3Key1\\\":[{\\\"listValue\\\":\\\"F\\\"},{\\\"listValue\\\":\\\"G\\\"}], \\\"map3Key2\\\":{\\\"map3Key2Key1\\\":\\\"H\\\"}, \\\"map3Key3\\\":\\\"I\\\"}\"\n" +
				"}");
		Map<String, Object> jsonMap = CustomJsonStringUtils.getCustomJsonStringAsMap(customJsonString, true);

		Map<String, Object> map1 = (Map<String, Object>) jsonMap.get("map1");
		List<Map<String, Object>> map1List = (List<Map<String, Object>>) map1.get("map1Key1");
		Assertions.assertEquals(2, map1List.size());
		Map<String, Object> map1ListValue1 = map1List.get(0);
		Map<String, Object> map1ListValue2 = map1List.get(1);
		Assertions.assertEquals("A", map1ListValue1.get("listValue"));
		Assertions.assertEquals("B", map1ListValue2.get("listValue"));

		Map<String, Object> map1Key2 = (Map<String, Object>) map1.get("map1Key2");
		Assertions.assertEquals("C", map1Key2.get("map1Key2Key1"));

		Map<String, Object> map2 = (Map<String, Object>) jsonMap.get("map2");
		List<Map<String, Object>> map2List = (List<Map<String, Object>>) map2.get("map2Key1");
		Assertions.assertEquals(2, map2List.size());
		Map<String, Object> map2ListValue1 = map2List.get(0);
		Map<String, Object> map2ListValue2 = map2List.get(1);
		Assertions.assertEquals("D", map2ListValue1.get("listValue"));
		Assertions.assertEquals("E", map2ListValue2.get("listValue"));

		Map<String, Object> map3 = (Map<String, Object>) jsonMap.get("map3");
		List<Map<String, Object>> map3List = (List<Map<String, Object>>) map3.get("map3Key1");
		Assertions.assertEquals(2, map3List.size());
		Map<String, Object> map3ListValue1 = map3List.get(0);
		Map<String, Object> map3ListValue2 = map3List.get(1);
		Assertions.assertEquals("F", map3ListValue1.get("listValue"));
		Assertions.assertEquals("G", map3ListValue2.get("listValue"));

		Map<String, Object> map3Key2 = (Map<String, Object>) map3.get("map3Key2");
		Assertions.assertEquals("H", map3Key2.get("map3Key2Key1"));

		Assertions.assertEquals("I", map3.get("map3Key3"));
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testGetCustomJsonStringAsMapReturnArray() {
		CustomJsonString customJsonString = new CustomJsonString("{\n" +
				"   \"map1\":\"[\\\"A\\\", \\\"B\\\", \\\"C\\\"]\",\n" +
				"   \"map2\":\"[{\\\"listValue\\\":\\\"D\\\"},{\\\"listValue\\\":\\\"E\\\"}]\"\n" +
				"}");

		Map<String, Object> jsonMap = CustomJsonStringUtils.getCustomJsonStringAsMap(customJsonString, true);

		List<String> map1List = (List<String>) jsonMap.get("map1");
		Assertions.assertEquals(3, map1List.size());
		Assertions.assertEquals("A", map1List.get(0));
		Assertions.assertEquals("B", map1List.get(1));
		Assertions.assertEquals("C", map1List.get(2));

		List<Map<String, Object>> map2List = (List<Map<String, Object>>) jsonMap.get("map2");
		Assertions.assertEquals(2, map2List.size());
		Assertions.assertEquals("D", map2List.get(0).get("listValue"));
		Assertions.assertEquals("E", map2List.get(1).get("listValue"));
	}


	@Test
	public void testGetColumn() {
		CustomJsonString customJsonString = new CustomJsonString("{\n" +
				"   \"map1\":\"{\\\"map1Key1\\\":{\\\"map1Key1Key2\\\":\\\"A\\\"}}\",\n" +
				"   \"map2\":\"{\\\"map2Key1\\\":[{\\\"listValue\\\":\\\"D\\\"},{\\\"listValue\\\":[{\\\"innerListValue\\\":\\\"E\\\"},{\\\"innerListValue\\\":\\\"F\\\"}]}]}\",\n" +
				"   \"map3\":\"{\\\"map3Key1\\\":[{\\\"listValue\\\":\\\"G\\\"},[{\\\"innerListValue\\\":\\\"H\\\"},{\\\"innerListValue\\\":\\\"I\\\"}]]}\"\n" +
				"}");
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map2.map2Key1[0].listValue.DNE"));
		Assertions.assertEquals("A", CustomJsonStringUtils.getProperty(customJsonString, "map1.map1Key1.map1Key1Key2"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "DNE"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map1.DNE"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map1.map1Key1.DNE"));

		Assertions.assertEquals("D", CustomJsonStringUtils.getProperty(customJsonString, "map2.map2Key1[0].listValue"));
		Assertions.assertEquals("E", CustomJsonStringUtils.getProperty(customJsonString, "map2.map2Key1[1].listValue[0].innerListValue"));
		Assertions.assertEquals("I", CustomJsonStringUtils.getProperty(customJsonString, "map3.map3Key1[1][1].innerListValue"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map2.map2Key1[0].DNE"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map2.map2Key1[0].DNE.DNE"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map2.map2Key1[0].listValue.DNE"));
		Assertions.assertNull(CustomJsonStringUtils.getProperty(customJsonString, "map3.map3Key1[1][1].DNE"));
	}

	@Test
	public void testGetColumnValueAsObject() {
		CustomJsonString customJsonString = buildCustomJsonString("test string", "123", "1", "true", buildJsonTestObjectList());
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(customJsonString);
		List<JsonTestObject> testObjectList = Arrays.asList(CustomJsonStringUtils.getColumnValueAsObject(customJsonString, OBJECT_COLUMN, JsonTestObject[].class));
		ValidationUtils.assertEquals(CollectionUtils.getSize(testObjectList), 2, "Expected two test objects to be deserialized using " + CustomJsonStringUtils.class);
		String expectedJsonValue = "\"Test Object\":[{\"bigDecimal\":1,\"date\":\"2021-09-21T00:00:00.000-05:00\",\"nestedTestObject\":{\"name\":\"Test Object Name\",\"bool\":true}},{\"bigDecimal\":2,\"date\":\"2021-09-22T00:00:00.000-05:00\",\"nestedTestObject\":{\"name\":\"Test Object Name\",\"bool\":true}}]";
		ValidationUtils.assertTrue(customJsonString.getJsonValue().contains(expectedJsonValue),
				String.format("JSON value for %s:\n\n\t%s\n\ndoes not contain the expected expected JSON value:\n\n\t%s", OBJECT_COLUMN, customJsonString.getJsonValue(), expectedJsonValue));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<JsonTestObject> buildJsonTestObjectList() {
		List<JsonTestObject> testObjectList = new ArrayList<>();
		JsonNestedTestObject nestedTestObject = new JsonNestedTestObject(true, "Test Object Name");
		JsonTestObject testObject1 = new JsonTestObject(new BigDecimal(1), new Date("09/21/2021"), nestedTestObject);
		JsonTestObject testObject2 = new JsonTestObject(new BigDecimal(2), DateUtils.addDays(new Date("09/21/2021"), 1), nestedTestObject);
		return Arrays.asList(testObject1, testObject2);
	}


	private CustomJsonString buildCustomJsonString(String stringValue, String integerValue, String shortValue, String booleanValue) {
		return buildCustomJsonString(stringValue, integerValue, shortValue, booleanValue, null);
	}


	private CustomJsonString buildCustomJsonString(String stringValue, String integerValue, String shortValue, String booleanValue, Object objectValue) {
		CustomJsonString customJsonString = new CustomJsonString();
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(customJsonString);
		customJsonString.setColumnValue(STRING_COLUMN, stringValue, stringValue);
		customJsonString.setColumnValue(INTEGER_COLUMN, integerValue, "Test Integer");
		customJsonString.setColumnValue(SHORT_COLUMN, shortValue, shortValue);
		customJsonString.setColumnValue(BOOLEAN_COLUMN, booleanValue, booleanValue);
		customJsonString.setColumnValue(OBJECT_COLUMN, objectValue, null);
		return customJsonString;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected static class JsonTestObject {

		private BigDecimal bigDecimal;
		private Date date;
		private JsonNestedTestObject nestedTestObject;


		public JsonTestObject() {
		}


		public JsonTestObject(BigDecimal testBigDecimal, Date testDate, JsonNestedTestObject nestedTestObject) {
			this.bigDecimal = testBigDecimal;
			this.date = testDate;
			this.nestedTestObject = nestedTestObject;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public BigDecimal getBigDecimal() {
			return this.bigDecimal;
		}


		public void setBigDecimal(BigDecimal bigDecimal) {
			this.bigDecimal = bigDecimal;
		}


		public Date getDate() {
			return this.date;
		}


		public void setDate(Date date) {
			this.date = date;
		}


		public JsonNestedTestObject getNestedTestObject() {
			return this.nestedTestObject;
		}


		public void setNestedTestObject(JsonNestedTestObject nestedTestObject) {
			this.nestedTestObject = nestedTestObject;
		}
	}

	protected static class JsonNestedTestObject {

		private String name;
		private boolean bool;


		public JsonNestedTestObject() {
		}


		public JsonNestedTestObject(boolean testBoolean, String testName) {
			this.bool = testBoolean;
			this.name = testName;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		public String getName() {
			return this.name;
		}


		public void setName(String name) {
			this.name = name;
		}


		public boolean isBool() {
			return this.bool;
		}


		public void setBool(boolean bool) {
			this.bool = bool;
		}
	}
}
