package com.clifton.core.security.authorization;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>SecurityPermissionTests</code> class tests {@link SecurityPermission}.
 *
 * @author vgomelsky
 */
public class SecurityPermissionTests {

	private SecurityPermission readOnly = new SecurityPermission(SecurityPermission.PERMISSION_READ);
	private SecurityPermission writeOnly = new SecurityPermission(SecurityPermission.PERMISSION_WRITE);
	private SecurityPermission createOnly = new SecurityPermission(SecurityPermission.PERMISSION_CREATE);
	private SecurityPermission deleteOnly = new SecurityPermission(SecurityPermission.PERMISSION_DELETE);
	private SecurityPermission fullControlOnly = new SecurityPermission(SecurityPermission.PERMISSION_FULL_CONTROL);
	private SecurityPermission readAndWrite = new SecurityPermission(SecurityPermission.PERMISSION_READ | SecurityPermission.PERMISSION_WRITE);


	@Test
	public void testIsReadAllowed() {
		Assertions.assertTrue(this.readOnly.isReadAllowed());
		Assertions.assertTrue(this.fullControlOnly.isReadAllowed());
		Assertions.assertTrue(this.readAndWrite.isReadAllowed());
		Assertions.assertFalse(this.writeOnly.isReadAllowed());
	}


	@Test
	public void testIsWriteAllowed() {
		Assertions.assertTrue(this.writeOnly.isWriteAllowed());
		Assertions.assertTrue(this.fullControlOnly.isWriteAllowed());
		Assertions.assertTrue(this.readAndWrite.isWriteAllowed());
		Assertions.assertFalse(this.readOnly.isWriteAllowed());
	}


	@Test
	public void testIsCreateAllowed() {
		Assertions.assertTrue(this.createOnly.isCreateAllowed());
		Assertions.assertTrue(this.fullControlOnly.isCreateAllowed());
		Assertions.assertFalse(this.writeOnly.isCreateAllowed());
		Assertions.assertFalse(this.readAndWrite.isCreateAllowed());
	}


	@Test
	public void testIsDeleteAllowed() {
		Assertions.assertTrue(this.deleteOnly.isDeleteAllowed());
		Assertions.assertTrue(this.fullControlOnly.isDeleteAllowed());
		Assertions.assertFalse(this.writeOnly.isDeleteAllowed());
		Assertions.assertFalse(this.readAndWrite.isDeleteAllowed());
	}


	@Test
	public void testIsFullControlAllowed() {
		Assertions.assertTrue(this.fullControlOnly.isFullControlAllowed());
		Assertions.assertFalse(this.readOnly.isFullControlAllowed());
		Assertions.assertFalse(this.writeOnly.isFullControlAllowed());
		Assertions.assertFalse(this.createOnly.isFullControlAllowed());
		Assertions.assertFalse(this.deleteOnly.isFullControlAllowed());
		Assertions.assertFalse(this.readAndWrite.isFullControlAllowed());
	}


	@Test
	public void testIsMaskAllowed() {
		Assertions.assertTrue(this.fullControlOnly.isMaskAllowed(SecurityPermission.MASK_RW));
		Assertions.assertTrue(this.writeOnly.isMaskAllowed(SecurityPermission.PERMISSION_WRITE));
		Assertions.assertFalse(this.writeOnly.isMaskAllowed(SecurityPermission.MASK_RW));
		Assertions.assertTrue(this.readOnly.isMaskAllowed(SecurityPermission.PERMISSION_READ));
		Assertions.assertFalse(this.readOnly.isMaskAllowed(SecurityPermission.MASK_RW));
		Assertions.assertFalse(this.readOnly.isMaskAllowed(SecurityPermission.PERMISSION_DELETE));
		Assertions.assertTrue(this.createOnly.isMaskAllowed(SecurityPermission.PERMISSION_CREATE));
		Assertions.assertFalse(this.createOnly.isMaskAllowed(SecurityPermission.PERMISSION_READ));
		Assertions.assertFalse(this.createOnly.isMaskAllowed(SecurityPermission.MASK_RW));
		Assertions.assertTrue(this.deleteOnly.isMaskAllowed(SecurityPermission.PERMISSION_DELETE));
		Assertions.assertFalse(this.deleteOnly.isMaskAllowed(SecurityPermission.MASK_RW));
	}
}
