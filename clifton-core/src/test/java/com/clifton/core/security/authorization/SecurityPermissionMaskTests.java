package com.clifton.core.security.authorization;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>SecurityPermissionMaskTests</code> class tests {@link SecurityPermissionMask} methods.
 *
 * @author vgomelsky
 */
public class SecurityPermissionMaskTests {

	@Test
	public void testGetSecurityPermissionMask() {
		Assertions.assertEquals(SecurityPermissionMask.MASK_R, SecurityPermissionMask.getSecurityPermissionMask(1));
		Assertions.assertEquals(SecurityPermissionMask.MASK_RW, SecurityPermissionMask.getSecurityPermissionMask(3));
	}
}
