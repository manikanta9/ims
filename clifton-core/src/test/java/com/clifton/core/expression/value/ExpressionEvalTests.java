package com.clifton.core.expression.value;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.util.math.CoreMathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;


public class ExpressionEvalTests {

	@Test
	public void parseFieldNames() {
		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();

		String valueExpression = "(${SW_VAL_PREMIUM} + ${SW_PAY_NOTL_AMT}) / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.instrument.priceMultiplier}";

		Set<String> fields = ftc.extractAllDataFields(valueExpression);

		Assertions.assertTrue(fields.contains("SW_VAL_PREMIUM"));
		Assertions.assertTrue(fields.contains("SW_PAY_NOTL_AMT"));
		Assertions.assertTrue(fields.contains("SW_PAY_NOTL_AMT"));
		Assertions.assertTrue(fields.contains("investmentSecurity.instrument.priceMultiplier"));
	}


	@Test
	public void parseExternalFieldNames() {
		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();

		String valueExpression = "(${SW_VAL_PREMIUM} + ${SW_PAY_NOTL_AMT}) / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.instrument.priceMultiplier}";

		Set<String> fields = ftc.extractDataFields(valueExpression, null);

		Assertions.assertEquals(3, fields.size());
		Assertions.assertTrue(fields.contains("SW_VAL_PREMIUM"));
		Assertions.assertTrue(fields.contains("SW_PAY_NOTL_AMT"));
		Assertions.assertTrue(fields.contains("investmentSecurity.instrument.priceMultiplier"));

		fields = ftc.extractDataFields(valueExpression, new String[]{"investmentSecurity"});

		Assertions.assertEquals(2, fields.size());
		Assertions.assertTrue(fields.contains("SW_VAL_PREMIUM"));
		Assertions.assertTrue(fields.contains("SW_PAY_NOTL_AMT"));
	}


	@Test
	public void testBigDecimalPrecision() {
		Assertions.assertTrue(CoreMathUtils.evaluateExpression("10*.09").doubleValue() == 0.90);
	}


	@Test
	public void testAbsFunction() {
		String valueExpression = "abs(${field})";
		Map<String, Object> variables = new HashMap<>();

		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();
		variables.put("field", "-10");

		BigDecimal val = CoreMathUtils.evaluateExpression(ftc.convertExpression(valueExpression, variables));

		Assertions.assertTrue(val.doubleValue() == 10);
	}


	@Test
	public void testDeltaMid() {
		String valueExp = "<#if OPT_DELTA_MID??>abs(${OPT_DELTA_MID})<#else>abs(${OPT_DELTA})</#if>";
		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();

		Map<String, Object> variables = new HashMap<>();

		//variables.put("OPT_DELTA_MID", "-50");
		variables.put("OPT_DELTA", "-15");

		String exp = ftc.convertExpression(valueExp, variables);

		Assertions.assertEquals((double) 15, CoreMathUtils.evaluateExpression(exp).doubleValue(), 0d);
	}


	@Test
	public void testUpFrontLastPrecision() {
		//Expression with limited specified precision followed by TEST variable with unbounded precision
		String valueExp = "${UPFRONT_LAST?string('0.#')} + ${TEST}";
		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();

		Map<String, Object> variables = new HashMap<>();

		variables.put("UPFRONT_LAST", -2.076695187045388);
		variables.put("TEST", -5.076691212128);

		String exp = ftc.convertExpression(valueExp, variables);

		Assertions.assertTrue(exp.equals("-2.1 + -5.076691212128"));
	}


	@Test
	public void parseComplexExpressionTest() {
		String valueExp = "<#if PX_BID??>(${PX_ASK}+${PX_BID})/2<#else>${PX_ASK}</#if>";

		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();
		Set<String> fields = ftc.extractAllDataFields(valueExp);

		Assertions.assertTrue(fields.contains("PX_ASK"));
		Assertions.assertTrue(fields.contains("PX_BID"));
	}


	@Test
	public void parseComplexExpressionQuestionMarkTest() {
		String valueExp = "<#if 0 < MTG_TRANCHE_TYP_LONG?split(',')?size>${MTG_TRANCHE_TYP_LONG?split(',')[0]}</#if>";

		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();
		Set<String> fields = ftc.extractAllDataFields(valueExp);

		Assertions.assertTrue(fields.contains("MTG_TRANCHE_TYP_LONG"));
	}


	@Test
	public void testSwaptionExpression() {
		String valueExp = "<#if SW_CS_POSITION == 'Short'>-1*</#if>${SW_VAL_PREMIUM} / ${SW_PAY_NOTL_AMT} / ${investmentSecurity.instrument.priceMultiplier}";

		FreemarkerTemplateConverter ftc = new FreemarkerTemplateConverter();
		Set<String> fields = ftc.extractAllDataFields(valueExp);

		Assertions.assertEquals(3, fields.size());
		Assertions.assertTrue(fields.contains("SW_VAL_PREMIUM"));
		Assertions.assertTrue(fields.contains("SW_PAY_NOTL_AMT"));
		Assertions.assertTrue(fields.contains("investmentSecurity.instrument.priceMultiplier"));
	}
}
