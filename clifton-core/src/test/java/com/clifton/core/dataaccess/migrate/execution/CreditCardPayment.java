package com.clifton.core.dataaccess.migrate.execution;


/**
 * The <code>CreditCardPayment</code> ...
 *
 * @author manderson
 */
public class CreditCardPayment extends Payment {

	private Frequencies requiredFrequency;
	private String creditCardType;
	private CalendarDay creditCardExpirationCalendarDay;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Frequencies getRequiredFrequency() {
		return this.requiredFrequency;
	}


	public void setRequiredFrequency(Frequencies requiredFrequency) {
		this.requiredFrequency = requiredFrequency;
	}


	public String getCreditCardType() {
		return this.creditCardType;
	}


	public void setCreditCardType(String creditCardType) {
		this.creditCardType = creditCardType;
	}


	public CalendarDay getCreditCardExpirationCalendarDay() {
		return this.creditCardExpirationCalendarDay;
	}


	public void setCreditCardExpirationCalendarDay(CalendarDay creditCardExpirationCalendarDay) {
		this.creditCardExpirationCalendarDay = creditCardExpirationCalendarDay;
	}
}
