package com.clifton.core.dataaccess.dao.xml;


import com.clifton.core.beans.NamedEntity;


/**
 * The <code>CoreTestEntity</code> ...
 *
 * @author manderson
 */
public class CoreTestEntity extends NamedEntity<Integer> {

	private CoreTestGroupEntity group;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CoreTestGroupEntity getGroup() {
		return this.group;
	}


	public void setGroup(CoreTestGroupEntity group) {
		this.group = group;
	}
}
