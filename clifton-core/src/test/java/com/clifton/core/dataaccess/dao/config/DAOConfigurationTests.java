package com.clifton.core.dataaccess.dao.config;


import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.search.OrderByDirections;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class DAOConfigurationTests {

	private Table table;


	@BeforeEach
	public void setUp() {
		this.table = new Table();
		this.table.setDtoClass("com.clifton.core.dataaccess.dao.config.TestEntity");
		this.table.setName("Test");
		Column userColumn = new Column();
		userColumn.setBeanPropertyName("user");
		userColumn.setName("User");
		Column descriptionColumn = new Column();
		descriptionColumn.setName("Description");
		descriptionColumn.setBeanPropertyName("description");
		this.table.addColumn(descriptionColumn);
		this.table.addColumn(userColumn);
	}


	@Test
	public void testTableWithNoDefaultsConfigured() {
		Assertions.assertThrows(IllegalStateException.class, () -> {
			DAOConfiguration<TestEntity> configuration = new DAOConfiguration<>(this.table);
			Assertions.assertEquals(0, configuration.getOrderByList().size());
		});
	}


	@Test
	public void testTableWithSortOrder() {
		Column userColumn = this.table.getColumn("User");
		userColumn.setSortOrder(1);
		userColumn.setSortDirection(OrderByDirections.ASC);
		DAOConfiguration<TestEntity> configuration = new DAOConfiguration<>(this.table);
		Assertions.assertEquals(1, configuration.getOrderByList().size());
	}


	@Test
	public void testTableWithIdDefault() {
		Column idColumn = new Column();
		idColumn.setBeanPropertyName("id");
		idColumn.setName("ID");
		this.table.addColumn(idColumn);
		DAOConfiguration<TestEntity> configuration = new DAOConfiguration<>(this.table);
		Assertions.assertEquals(1, configuration.getOrderByList().size());
		Assertions.assertEquals("id", configuration.getOrderByList().get(0).getName());
	}


	@Test
	public void testTableWithLabelDefault() {
		Column labelColumn = new Column();
		labelColumn.setBeanPropertyName("label");
		labelColumn.setName("Label");
		this.table.addColumn(labelColumn);
		DAOConfiguration<TestEntity> configuration = new DAOConfiguration<>(this.table);
		Assertions.assertEquals(1, configuration.getOrderByList().size());
		Assertions.assertEquals("label", configuration.getOrderByList().get(0).getName());
	}


	@Test
	public void testTableWithNameDefault() {
		Column nameColumn = new Column();
		nameColumn.setBeanPropertyName("name");
		nameColumn.setName("Name");
		this.table.addColumn(nameColumn);
		DAOConfiguration<TestEntity> configuration = new DAOConfiguration<>(this.table);
		Assertions.assertEquals(1, configuration.getOrderByList().size());
		Assertions.assertEquals("name", configuration.getOrderByList().get(0).getName());
	}
}
