package com.clifton.core.dataaccess.dao.hibernate;

import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverter;
import com.clifton.core.dataaccess.migrate.reader.MigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>HibernateSchemaConverterTests</code> class tests schema conversion to hibernate format.
 *
 * @author vgomelsky
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class HibernateSchemaConverterTests {

	@Resource
	private MigrationDefinitionReader migrationDefinitionReader;
	@Resource
	private MigrationSchemaConverter hibernateSchemaConverter;
	@Resource
	private String hibernateSchemaXML;
	@Resource
	private String hibernateSchemaWithInheritanceXML;
	@Resource
	private String hibernateSchemaEnum;
	@Resource
	private String hibernateSchemaCalculated;
	@Resource
	private String hibernateSchemaWithVersionXML;
	@Resource
	private String hibernateSchemaXMLCompressionData;


	@Test
	public void testSchemaToXMLConversion() {
		Schema schema = this.migrationDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		String xml = this.hibernateSchemaConverter.convert(schema);
		Assertions.assertEquals(this.hibernateSchemaXML.trim(), xml.trim());
	}


	@Test
	public void testSchemaWithInheritanceToXMLConversion() {
		Schema schema = this.migrationDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration5.xml");
		String xml = this.hibernateSchemaConverter.convert(schema);
		Assertions.assertEquals(this.hibernateSchemaWithInheritanceXML.trim(), xml.trim());
	}


	@Test
	public void testSchemaToXMLConversionEnum() {
		Schema schema = this.migrationDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/dao/hibernate/migration-enum.xml");
		String xml = this.hibernateSchemaConverter.convert(schema);
		Assertions.assertEquals(this.hibernateSchemaEnum.trim(), xml.trim());
	}


	@Test
	public void testSchemaToXMLConversionCalculatedColumns() {
		Schema schema = this.migrationDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-calculated-columns.xml");
		String xml = this.hibernateSchemaConverter.convert(schema);
		Assertions.assertEquals(this.hibernateSchemaCalculated.trim(), xml.trim());
	}


	@Test
	public void testSchemaToXMLConversionWithVersionColumn() {
		Schema schema = this.migrationDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-version.xml");
		String xml = this.hibernateSchemaConverter.convert(schema);
		Assertions.assertEquals(this.hibernateSchemaWithVersionXML.trim(), xml.trim());
	}


	@Test
	public void testSchemaToXMLConversionWithCompressedData() {
		Schema schema = this.migrationDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-compressed-data.xml");
		String xml = this.hibernateSchemaConverter.convert(schema);
		Assertions.assertEquals(this.hibernateSchemaXMLCompressionData.trim(), xml.trim());
	}
}
