package com.clifton.core.dataaccess.migrate.execution;


import com.clifton.core.beans.BaseEntity;


/**
 * The <code>CheckDataCompression</code> test class to check that compress text type generates correct migration.
 *
 * @author mwacker
 */
public class CheckDataCompression extends BaseEntity<Integer> {

	private String jsonData;


	public String getJsonData() {
		return this.jsonData;
	}


	public void setJsonData(String jsonData) {
		this.jsonData = jsonData;
	}
}
