package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import org.springframework.stereotype.Component;


/**
 * The <code>NamedEntityObserver</code> ...
 *
 * @author manderson
 */
@Component
public class NamedEntityTestObjectObserver<T extends NamedEntityTestObject> extends BaseDaoEventObserver<T> {

	@Override
	@SuppressWarnings("unused")
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		bean.setLabel("TEST LABEL");
	}


	@Override
	@SuppressWarnings("unused")
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// DO NOTHING
	}
}
