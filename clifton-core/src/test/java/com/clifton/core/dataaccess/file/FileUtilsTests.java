package com.clifton.core.dataaccess.file;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;


/**
 * The <code>FileUtilsTests</code> tests the utility methods in {@link FileUtils} class
 *
 * @author manderson
 */
public class FileUtilsTests {

	private static final String COPY_TARGET_FILE_PATH = "src/test/java/com/clifton/core/dataaccess/file/file_copy_test_target.txt";


	@AfterEach
	public void cleanUp() {
		deleteCopyTarget();
	}


	@BeforeEach
	public void startUp() {
		deleteCopyTarget();
	}


	private void deleteCopyTarget() {
		File copyTarget = new File(COPY_TARGET_FILE_PATH);
		if (copyTarget.exists()) {
			FileUtils.delete(copyTarget);
		}
	}


	@Test
	public void testFileCopy() {
		File testFile1 = new File("src/test/java/com/clifton/core/dataaccess/file/file_copy_test1.txt");
		File testFile2 = new File("src/test/java/com/clifton/core/dataaccess/file/file_copy_test2.txt");
		File copyTarget = new File(COPY_TARGET_FILE_PATH);

		try {
			FileUtils.copyFileCreatePath(testFile1, copyTarget);
		}
		catch (IOException e) {
			throw new RuntimeException("File copy failed.", e);
		}
		Assertions.assertTrue(copyTarget.exists());
		Assertions.assertEquals("This is the first test file for file copy utility.", readFirstLineFromFile(copyTarget.getAbsolutePath()));

		Exception error = null;
		try {
			FileUtils.copyFileCreatePath(testFile2, copyTarget);
		}
		catch (Exception e) {
			error = e;
		}
		Assertions.assertNotNull(error);
		Assertions.assertTrue(error.getMessage().startsWith("Failed to copy file"));

		try {
			FileUtils.copyFileCreatePathAndOverwrite(testFile2, copyTarget);
		}
		catch (IOException e) {
			throw new RuntimeException("File copy failed.", e);
		}
		Assertions.assertTrue(copyTarget.exists());
		Assertions.assertEquals("This is the second test file for file copy utility.", readFirstLineFromFile(copyTarget.getAbsolutePath()));
	}


	private String readFirstLineFromFile(String path) {
		try {
			List<String> lines = Files.readAllLines(Paths.get(path), Charset.defaultCharset());
			return lines.get(0);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to read the first line from [" + path + "].", e);
		}
	}


	@Test
	public void testReplaceInvalidCharacters() {
		validateReplaceInvalidCharactersFileName(null, null, "test");
		validateReplaceInvalidCharactersFileName(null, null, "");
		validateReplaceInvalidCharactersFileName(null, null, null);

		validateReplaceInvalidCharactersFileName("/Test?File:", "TestFile", null);
		validateReplaceInvalidCharactersFileName("/Test?File:", "TestFile", "");
		validateReplaceInvalidCharactersFileName("\\\\??//TestFile*", "TestFile", "");
		validateReplaceInvalidCharactersFileName("<TestFile>", "TestFile", "");
		validateReplaceInvalidCharactersFileName("<<>Test|File|", "TestFile", "");
		validateReplaceInvalidCharactersFileName("/Test?File:", "TestFile", "");
		validateReplaceInvalidCharactersFileName("\\TestFile*", "TestFile", null);
		validateReplaceInvalidCharactersFileName("<TestFile>", "TestFile", "");
		validateReplaceInvalidCharactersFileName("/Test?File:", "TestFile", null);
		validateReplaceInvalidCharactersFileName("<<>Test|File|", "TestFile", "");

		validateReplaceInvalidCharactersFileName("/Test?File:", "_Test_File_", "_");
		validateReplaceInvalidCharactersFileName("\\Test\\File*", "_Test_File_", "_");
		validateReplaceInvalidCharactersFileName("<Test|File>", "_Test_File_", "_");

		validateReplaceInvalidCharactersFileName("\\\\??//TestFile*", "_TestFile_", "_");

		validateReplaceInvalidCharactersFileName("<<>Test|File|", "-Test-File-", "-");
		validateReplaceInvalidCharactersFileName("<<>Test|File|", "-Test-File-", "-");
		validateReplaceInvalidCharactersFileName("‘TestFile’", "TestFile", "" );
		validateReplaceInvalidCharactersFileName("“TestFile”", "TestFile" ,"");
	}


	private void validateReplaceInvalidCharactersFileName(String fileName, String expectedFileName, String replacement) {
		Assertions.assertEquals(expectedFileName, FileUtils.replaceInvalidCharacters(fileName, replacement));
	}
}
