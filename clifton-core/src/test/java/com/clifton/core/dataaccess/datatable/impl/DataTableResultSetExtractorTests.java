package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.converter.SimpleConverter;
import com.clifton.core.dataaccess.datatable.PagingDataTable;
import com.clifton.core.util.converter.Converter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.dao.DataAccessException;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>DataTableResultSetExtractorTests</code> class contains methods for testing the
 * <code>DataTableResultSetExtractor</code> class.
 *
 * @author vgomelsky
 */
public class DataTableResultSetExtractorTests {

	private ResultSet oneColumnResultSet;


	@BeforeEach
	public void init() throws SQLException {
		// mock ResultSet that has 1 INTEGER column
		ResultSetMetaData metaData = Mockito.mock(ResultSetMetaData.class);
		Mockito.when(metaData.getColumnCount()).thenReturn(1);
		Mockito.when(metaData.getColumnLabel(1)).thenReturn("TEST");
		Mockito.when(metaData.getColumnType(1)).thenReturn(java.sql.Types.INTEGER);

		this.oneColumnResultSet = Mockito.mock(ResultSet.class);
		Mockito.when(this.oneColumnResultSet.getMetaData()).thenReturn(metaData);
	}


	@Test
	public void extractData_no_paging() throws DataAccessException, SQLException {
		// mock ResultSet that has 1 INTEGER column and 5 rows with values 777
		Mockito.when(this.oneColumnResultSet.next()).thenReturn(true, true, true, true, true, false);
		Mockito.when(this.oneColumnResultSet.getObject(1)).thenReturn(777);

		DataTableResultSetExtractor extractor = new DataTableResultSetExtractor(0, 100, null);
		PagingDataTable table = (PagingDataTable) extractor.extractData(this.oneColumnResultSet);
		Assertions.assertNotNull(table);
		Assertions.assertEquals(5, table.getTotalRowCount(), "check total number of rows");
		Assertions.assertEquals(5, table.getCurrentPageElementCount(), "check current page number of rows");
		Assertions.assertEquals(0, table.getFirstElementIndex(), "check first row index");
		Assertions.assertEquals(777, table.getRow(0).getValue(0), "check first row value");
		Assertions.assertEquals(777, table.getRow(4).getValue(0), "check last row value");

		Mockito.verify(this.oneColumnResultSet, Mockito.times(5)).getObject(1);
	}


	@Test
	public void extractData_middle_page() throws DataAccessException, SQLException {
		// mock ResultSet that has 1 INTEGER column and returns rows 11 to 15 out of 25 with values 777
		Mockito.when(this.oneColumnResultSet.absolute(11)).thenReturn(true);
		Mockito.when(this.oneColumnResultSet.last()).thenReturn(true);
		Mockito.when(this.oneColumnResultSet.getRow()).thenReturn(11).thenReturn(25);
		Mockito.when(this.oneColumnResultSet.next()).thenReturn(true, true, true, true);
		Mockito.when(this.oneColumnResultSet.getObject(1)).thenReturn(777);

		DataTableResultSetExtractor extractor = new DataTableResultSetExtractor(10, 5, null);
		PagingDataTable table = (PagingDataTable) extractor.extractData(this.oneColumnResultSet);
		Assertions.assertNotNull(table);
		Assertions.assertEquals(25, table.getTotalRowCount(), "check total number of rows");
		Assertions.assertEquals(5, table.getCurrentPageElementCount(), "check current page number of rows");
		Assertions.assertEquals(10, table.getFirstElementIndex(), "check first row index");
		Assertions.assertEquals(777, table.getRow(0).getValue(0), "check first row value");
		Assertions.assertEquals(777, table.getRow(4).getValue(0), "check last row value");

		Mockito.verify(this.oneColumnResultSet, Mockito.times(4)).next();
		Mockito.verify(this.oneColumnResultSet, Mockito.times(5)).getObject(1);
	}


	@Test
	public void extractData_test_converter() throws DataAccessException, SQLException {
		// mock ResultSet that has 1 INTEGER column and returns rows 11 to 15 out of 25 with values 777
		Mockito.when(this.oneColumnResultSet.absolute(11)).thenReturn(true);
		Mockito.when(this.oneColumnResultSet.last()).thenReturn(true);
		Mockito.when(this.oneColumnResultSet.getRow()).thenReturn(11).thenReturn(25);
		Mockito.when(this.oneColumnResultSet.next()).thenReturn(true, true, true, true);
		Mockito.when(this.oneColumnResultSet.getObject(1)).thenReturn(777);

		Map<String, Converter<Object, Object>> converterMap = new HashMap<>();
		converterMap.put("TEST", new TestNumberConverter());

		DataTableResultSetExtractor extractor = new DataTableResultSetExtractor(10, 5, converterMap);
		PagingDataTable table = (PagingDataTable) extractor.extractData(this.oneColumnResultSet);
		Assertions.assertNotNull(table);
		Assertions.assertEquals(25, table.getTotalRowCount(), "check total number of rows");
		Assertions.assertEquals(5, table.getCurrentPageElementCount(), "check current page number of rows");
		Assertions.assertEquals(10, table.getFirstElementIndex(), "check first row index");
		//Converter should change value from 777 to 19136
		Assertions.assertEquals(19136, table.getRow(0).getValue(0), "check first row value");
		Assertions.assertEquals(19136, table.getRow(4).getValue(0), "check last row value");

		Mockito.verify(this.oneColumnResultSet, Mockito.times(4)).next();
		Mockito.verify(this.oneColumnResultSet, Mockito.times(5)).getObject(1);
	}


	class TestNumberConverter extends SimpleConverter<Object> {

		@Override
		public Object convert(Object from) {
			return 19136;
		}
	}
}
