package com.clifton.core.dataaccess.migrate.schema;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.List;


public class TableTests {

	Table theTable;


	@BeforeEach
	public void setUp() {
		Table table = new Table();
		Column userColumn = new Column();
		userColumn.setBeanPropertyName("user");
		userColumn.setName("User");
		Column descriptionColumn = new Column();
		descriptionColumn.setName("Description");
		descriptionColumn.setBeanPropertyName("description");
		table.addColumn(descriptionColumn);
		table.addColumn(userColumn);
		this.theTable = table;
	}


	@Test
	public void testTablewithNoSortedColumns() {
		Assertions.assertEquals(0, this.theTable.getSortOrderColumnList(true).size());
	}


	@Test
	public void testTableWithOneSortedColumns() {
		this.theTable.getColumn("User").setSortOrder(1);
		List<Column> columns = this.theTable.getSortOrderColumnList(true);
		Assertions.assertEquals(1, columns.size());
		Assertions.assertEquals("User", columns.get(0).getName());
	}


	@Test
	public void testTableWithManySortedColumns() {
		Column idColumn = new Column();
		idColumn.setBeanPropertyName("id");
		idColumn.setName("id");
		idColumn.setSortOrder(2);
		this.theTable.addColumn(idColumn);
		this.theTable.getColumn("Description").setSortOrder(1);
		List<Column> columns = this.theTable.getSortOrderColumnList(true);
		Assertions.assertEquals(2, columns.size());
		Assertions.assertEquals("Description", columns.get(0).getName());
		Assertions.assertEquals("id", columns.get(1).getName());
	}
}
