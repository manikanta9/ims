package com.clifton.core.dataaccess.dao.config;


import com.clifton.core.beans.BaseEntity;


public class TestEntity extends BaseEntity<Integer> {

	private String user;
	private String description;


	public String getUser() {
		return this.user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
