package com.clifton.core.dataaccess.function;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.dialect.SessionFactoryBuilderConfigurer;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


/**
 * @author dillonm
 */
public class SupportedDialectsTests {

	private static final Reflections REFLECTIONS = new Reflections(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION, new SubTypesScanner());


	/**
	 * tests current module to ensure that an implementation for each sql function added exists for each dialect.
	 */
	@Test
	public void allDialectsSupportSameFunctionsTest() throws InstantiationException, IllegalAccessException {
		List<String> errorMessageList = new ArrayList<>();
		Set<Class<? extends SessionFactoryBuilderConfigurer>> sqlFunctionConfigurerClassSet = REFLECTIONS.getSubTypesOf(SessionFactoryBuilderConfigurer.class);
		HashSet<String> allFunctionsSet = new HashSet<>();
		// Determine union of all added functions
		for (Class<?> sqlFunctionConfigurerClass : sqlFunctionConfigurerClassSet) {
			SessionFactoryBuilderConfigurer sessionFactoryBuilderConfigurer = (SessionFactoryBuilderConfigurer) sqlFunctionConfigurerClass.newInstance();
			LocalSessionFactoryBuilder localSessionFactoryBuilder = new LocalSessionFactoryBuilder(null);
			sessionFactoryBuilderConfigurer.configure(localSessionFactoryBuilder);
			allFunctionsSet.addAll(localSessionFactoryBuilder.getSqlFunctions().keySet());
		}
		// Identify missing functions
		for (Class<?> sqlFunctionConfigurerClass : sqlFunctionConfigurerClassSet) {
			SessionFactoryBuilderConfigurer sessionFactoryBuilderConfigurer = (SessionFactoryBuilderConfigurer) sqlFunctionConfigurerClass.newInstance();
			LocalSessionFactoryBuilder localSessionFactoryBuilder = new LocalSessionFactoryBuilder(null);
			sessionFactoryBuilderConfigurer.configure(localSessionFactoryBuilder);
			Set<String> sqlFunctionConfigurerImplementedFunctionSet = localSessionFactoryBuilder.getSqlFunctions().keySet();
			for (String functionName : allFunctionsSet) {
				if (!sqlFunctionConfigurerImplementedFunctionSet.contains(functionName)) {
					errorMessageList.add(sessionFactoryBuilderConfigurer.getClass().getName() + " does not contain an implementation for sql custom function " + functionName);
				}
			}
		}
		// Report errors
		if (!errorMessageList.isEmpty()) {
			Assertions.fail(String.join(StringUtils.NEW_LINE, errorMessageList));
		}
	}
}
