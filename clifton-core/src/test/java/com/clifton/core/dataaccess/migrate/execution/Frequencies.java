package com.clifton.core.dataaccess.migrate.execution;


public enum Frequencies {
	ONCE, HOURLY, DAILY, WEEKLY, MONTHLY, YEARLY
}
