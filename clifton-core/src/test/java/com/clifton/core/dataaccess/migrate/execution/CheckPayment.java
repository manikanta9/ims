package com.clifton.core.dataaccess.migrate.execution;


/**
 * The <code>CheckPayment</code> ...
 *
 * @author manderson
 */
public class CheckPayment extends Payment {

	private String checkNumber;


	/**
	 * @return the checkNumber
	 */
	public String getCheckNumber() {
		return this.checkNumber;
	}


	/**
	 * @param checkNumber the checkNumber to set
	 */
	public void setCheckNumber(String checkNumber) {
		this.checkNumber = checkNumber;
	}
}
