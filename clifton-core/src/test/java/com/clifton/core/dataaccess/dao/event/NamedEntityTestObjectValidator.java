package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;


@Component
public class NamedEntityTestObjectValidator extends SelfRegisteringDaoValidator<NamedEntityTestObject> {

	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	public void validate(NamedEntityTestObject bean, DaoEventTypes config) throws ValidationException {
		if (!"TEST DESCRIPTION".equals(bean.getDescription())) {
			throw new ValidationException("Invalid description: '" + bean.getDescription() + "'. Expected 'TEST DESCRIPTION' for event: " + config);
		}
	}
}
