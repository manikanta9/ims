package com.clifton.core.dataaccess;

import com.clifton.core.util.observer.Observer;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


public class DaoUtilsTest {

	private List<TestObserver> availableObservers = Arrays.asList(
			() -> "one",
			() -> "two",
			() -> "three",
			() -> "four",
			() -> "five",
			() -> "six"
	);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testExecuteWithObserversEnabled_EmptyList() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			DaoUtils.executeWithSpecificObserversEnabled(() -> {
			});
		});
	}


	@Test
	public void testExecuteWithObserversEnabled() {
		DaoUtils.executeWithSpecificObserversEnabled(() -> {
			Assertions.assertNull(DaoUtils.getDisabledObserverSet());
			Assertions.assertNotNull(DaoUtils.getEnabledObserverSet());
			List<TestObserver> filtered = this.availableObservers.stream().filter(DaoUtils.observerPredicate).collect(Collectors.toList());
			MatcherAssert.assertThat("Only 1 observer should be enabled.", filtered.size(), IsEqual.equalTo(1));
			MatcherAssert.assertThat("Only observer 'four' should be enabled.", filtered.get(0).getName(), IsEqual.equalTo("four"));
		}, this.availableObservers.get(3).getClass());
		Assertions.assertNull(DaoUtils.getEnabledObserverSet());
		Assertions.assertNull(DaoUtils.getDisabledObserverSet());

		DaoUtils.executeWithSpecificObserversEnabled(() -> {
			Assertions.assertNull(DaoUtils.getDisabledObserverSet());
			Assertions.assertNotNull(DaoUtils.getEnabledObserverSet());
			List<TestObserver> filtered = this.availableObservers.stream().filter(DaoUtils.observerPredicate).collect(Collectors.toList());
			MatcherAssert.assertThat("Only 2 observers should be enabled.", filtered.size(), IsEqual.equalTo(2));
			MatcherAssert.assertThat("observer 'four' should be enabled.", filtered.get(0).getName(), IsEqual.equalTo("four"));
			MatcherAssert.assertThat("observer 'five' should be enabled.", filtered.get(1).getName(), IsEqual.equalTo("five"));
		}, this.availableObservers.get(3).getClass(), this.availableObservers.get(4).getClass());
		Assertions.assertNull(DaoUtils.getEnabledObserverSet());
		Assertions.assertNull(DaoUtils.getDisabledObserverSet());
	}


	@Test
	public void testExecuteWithObserversDisabled_EmptyList() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			DaoUtils.executeWithSpecificObserversDisabled(() -> {
			});
		});
	}


	@Test
	public void testExecuteWithObserversDisabled() {
		DaoUtils.executeWithSpecificObserversDisabled(() -> {
			Assertions.assertNotNull(DaoUtils.getDisabledObserverSet());
			Assertions.assertNull(DaoUtils.getEnabledObserverSet());
			List<TestObserver> filtered = this.availableObservers.stream().filter(DaoUtils.observerPredicate).collect(Collectors.toList());
			MatcherAssert.assertThat("Only 1 observer should be disabled.", filtered.size(), IsEqual.equalTo(this.availableObservers.size() - 1));
			MatcherAssert.assertThat("Only observer 'three' should be disabled.", filtered.stream().filter(o -> Objects.equals(o.getName(), "three")).count()
					, IsEqual.equalTo(0L));
		}, this.availableObservers.get(2).getClass());
		Assertions.assertNull(DaoUtils.getEnabledObserverSet());
		Assertions.assertNull(DaoUtils.getDisabledObserverSet());

		DaoUtils.executeWithSpecificObserversDisabled(() -> {
			Assertions.assertNotNull(DaoUtils.getDisabledObserverSet());
			Assertions.assertNull(DaoUtils.getEnabledObserverSet());
			List<TestObserver> filtered = this.availableObservers.stream().filter(DaoUtils.observerPredicate).collect(Collectors.toList());
			MatcherAssert.assertThat("Only 2 observer should be disabled.", filtered.size(), IsEqual.equalTo(this.availableObservers.size() - 2));
			MatcherAssert.assertThat("Only observer 'three' should be disabled."
					, filtered.stream().filter(o -> Objects.equals(o.getName(), "three") || Objects.equals(o.getName(), "four")).count(), IsEqual.equalTo(0L));
		}, this.availableObservers.get(2).getClass(), this.availableObservers.get(3).getClass());
		Assertions.assertNull(DaoUtils.getEnabledObserverSet());
		Assertions.assertNull(DaoUtils.getDisabledObserverSet());
	}


	@Test
	public void testExecuteWithAllObserversDisabled() {
		DaoUtils.executeWithAllObserversDisabled(() -> {
			Assertions.assertNull(DaoUtils.getDisabledObserverSet());
			Assertions.assertNotNull(DaoUtils.getEnabledObserverSet());
			MatcherAssert.assertThat("All observers should be disabled.", this.availableObservers.stream().filter(DaoUtils.observerPredicate).count(), IsEqual.equalTo(0L));
		});
		Assertions.assertNull(DaoUtils.getEnabledObserverSet());
		Assertions.assertNull(DaoUtils.getDisabledObserverSet());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@FunctionalInterface
	private interface TestObserver extends Observer {

		public String getName();
	}
}
