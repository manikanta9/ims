package com.clifton.core.dataaccess;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.shared.dataaccess.DataTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.beans.PropertyDescriptor;


public class PropertyDataTypeConverterTests {

	@Test
	public void testConvertManyToManyEntity() {
		PropertyDataTypeConverter converter = new PropertyDataTypeConverter();
		PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(ManyToManyEntity.class, "referenceOne");
		Assertions.assertEquals(DataTypes.INT, converter.convert(descriptor));
	}


	@Test
	public void testConvertProxyEntity() {
		PropertyDataTypeConverter converter = new PropertyDataTypeConverter();
		PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(TestEntityImpl.class, "parent");
		Assertions.assertEquals(DataTypes.INT, converter.convert(descriptor));
	}
}
