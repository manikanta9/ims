package com.clifton.core.dataaccess.file;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanDataTable;
import com.itextpdf.text.pdf.PdfReader;
import org.junit.jupiter.api.Assertions;

import java.io.File;
import java.io.FileInputStream;


/**
 * The <code>DataTableToPdfFileConverterTests</code> tests the {@link DataTableToPdfFileConverter}
 *
 * @author manderson
 */
public class DataTableToPdfFileConverterTests extends DataTableToFileConverterTestBase {

	private DataTableToFileConverter dataTableToFileConverter = new DataTableToPdfFileConverter();


	@Override
	public DataTableToFileConverter getDataTableToFileConverter() {
		return this.dataTableToFileConverter;
	}


	@Override
	public void validateFile(DataTable dataTable, File file) {
		Assertions.assertNotNull(file);
		if (dataTable == null) {
			Assertions.assertEquals(0, file.length());
			return;
		}
		FileInputStream is = null;
		try {
			is = new FileInputStream(file);
			PdfReader reader = new PdfReader(is);
			byte[] contents = reader.getPageContent(1);
			String fileAsStr = getPdfFileAsString(contents);
			if (dataTable instanceof BeanDataTable<?>) {
				Assertions.assertEquals("NameDescriptionCreate DateTest 1 NameTest 1 DescriptionTest 2 NameTest 3 NameTest 3 DescriptionTest 4 NameTest 5 Name", fileAsStr);
			}
			else if (dataTable.getTotalRowCount() == 0) {
				Assertions.assertEquals("NameDescriptionCreate DateAmountQuantityNO DATA", fileAsStr);
			}
			else {
				Assertions.assertEquals(
						"NameDescriptionCreate DateAmountQuantityTest OneTest for Row 101/01/20105.5Test Two02/01/201055Test ThreeTest for Row 317.7987Test FourTest for Row 403/30/201010", fileAsStr);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			FileUtils.close(is);
		}
	}


	private String getPdfFileAsString(byte[] input) {
		boolean blnStartWrite = false;

		if (input == null || input.length == 0) {
			return "";
		}
		String resultString = "";
		try {
			for (byte nextByte : input) {
				char c = (char) nextByte;
				if (c == '(') {
					blnStartWrite = true;
					continue;
				}
				if (c == ')') {
					blnStartWrite = false;
				}
				if (blnStartWrite) {
					resultString += c;
				}
			}
			return resultString;
		}

		catch (Exception e) {
			throw new RuntimeException("Error getting contents of file", e);
		}
	}
}
