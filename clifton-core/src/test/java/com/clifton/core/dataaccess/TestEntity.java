package com.clifton.core.dataaccess;


import com.clifton.core.beans.Entity;
import com.clifton.core.beans.LabeledObject;


public interface TestEntity extends LabeledObject, Entity<Integer> {
	//
}
