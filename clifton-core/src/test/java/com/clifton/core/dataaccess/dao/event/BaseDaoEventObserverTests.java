package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.hibernate.DirtyCheckingStrategy;
import com.clifton.core.dataaccess.dao.hibernate.HibernateUpdatableDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;


/**
 * The <code>BaseDaoEventObserverTests</code> class tests {@link BaseDaoEventObserver} methods.
 *
 * @author vgomelsky
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class BaseDaoEventObserverTests {

	@Resource
	private ContextHandler contextHandler;
	@Resource
	private HibernateUpdatableDAO<IdentityObject> testDAO;
	@Resource
	private TestDaoEventObserver<IdentityObject> testDaoEventObserver1;
	@Resource
	private TestDaoEventObserver<IdentityObject> testDaoEventObserver2;
	@Resource
	private TestDaoEventObserver<IdentityObject> testDaoEventObserver3;
	@Resource
	private TestDaoEventObserver<IdentityObject> testDaoEventObserver7;
	@Resource
	private TestRepeatSaveDaoEventObserver<IdentityObject> testRepeatSaveDaoEventObserver;


	// Reset mocks and context changes, even if an exception was thrown during test execution
	@AfterEach
	public void teardown() {
		// Unregister all added observers
		List<TestDaoEventObserver<IdentityObject>> observerList = Arrays.asList(
				this.testDaoEventObserver1,
				this.testDaoEventObserver2,
				this.testDaoEventObserver3,
				this.testDaoEventObserver7,
				this.testRepeatSaveDaoEventObserver
		);
		for (DaoEventObserver<IdentityObject> observer : observerList) {
			this.testDAO.unregisterEventObserver(DaoEventTypes.INSERT, observer);
			this.testDAO.unregisterEventObserver(DaoEventTypes.UPDATE, observer);
			this.testDAO.unregisterEventObserver(DaoEventTypes.DELETE, observer);
			this.testDAO.unregisterEventObserver(DaoEventTypes.READ, observer);
		}
		// Reset mocks
		Object[] mocks = new Object[]{this.testDAO};
		Mockito.reset(mocks);
	}


	@Test
	public void testUpdateGetsOriginalBeanOnlyOnce() {
		User user = new User();
		user.setId(new Integer(7).shortValue());
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		Session session = Mockito.mock(Session.class);
		SessionFactory factory = Mockito.mock(SessionFactory.class);
		SessionFactoryOptions options = Mockito.mock(SessionFactoryOptions.class);
		DirtyCheckingStrategy dirtyCheckingStrategy = Mockito.mock(DirtyCheckingStrategy.class);
		Mockito.when(options.getCustomEntityDirtinessStrategy()).thenReturn(dirtyCheckingStrategy);
		Mockito.when(factory.getSessionFactoryOptions()).thenReturn(options);
		Mockito.when(factory.getCurrentSession()).thenReturn(session);
		Mockito.when(factory.openSession()).thenReturn(session);
		Mockito.when(session.save(ArgumentMatchers.any(IdentityObject.class))).thenReturn(null);
		NamedEntity<Integer> originalBean = newNamedEntityBean(5);
		this.testDAO.setSessionFactory(factory);

		Assertions.assertNotNull(this.testDAO, "DAO cannot be null");
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver2);
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver3);
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver1);

		this.testDAO.findByPrimaryKey(originalBean.getIdentity());
		Mockito.verify(session, Mockito.times(1)).get(NamedEntity.class, originalBean.getIdentity());

		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context is null before");
		NamedEntity<Integer> bean = newNamedEntityBean(5);
		Mockito.doReturn(bean).when(session).merge(ArgumentMatchers.any(IdentityObject.class));
		Mockito.doReturn(bean).when(this.testDAO).findByPrimaryKey(bean.getId());
		Mockito.verify(this.testDAO, Mockito.times(1)).findByPrimaryKey(bean.getId());
		Assertions.assertEquals(0, bean.getUpdateUserId());

		this.testDAO.save(bean);

		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context is null after");
		Assertions.assertEquals(7, bean.getUpdateUserId());
		Mockito.verify(session, Mockito.times(1)).get(NamedEntity.class, originalBean.getIdentity());
		Mockito.verify(this.testDAO, Mockito.times(2)).findByPrimaryKey(bean.getId());
	}


	private NamedEntity<Integer> newNamedEntityBean(int id) {
		NamedEntity<Integer> bean = new NamedEntity<>();
		bean.setId(id);
		bean.setRv(new byte[0]);
		return bean;
	}


	@Test
	public void testUpdateClearsOriginalBeanOnException() {
		User user = new User();
		user.setId(new Integer(7).shortValue());
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		Assertions.assertNotNull(this.testDAO, "DAO cannot be null");
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver1);

		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context must be null before method calls");
		NamedEntity<Integer> bean = newNamedEntityBean(5);

		Mockito.doReturn(bean).when(this.testDAO).findByPrimaryKey(bean.getId());

		// cache the bean
		this.testDaoEventObserver1.getOriginalBean(this.testDAO, bean);
		Assertions.assertNotNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context is set before");
		boolean exception = false;
		try {
			this.testDAO.save(bean);
		}
		catch (Exception e) {
			exception = true;
		}
		Assertions.assertTrue(exception, "Exception is expected");
		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context must be null after");
	}


	@Test
	public void testUpdateClearsCurrentBeanWhenBeforeException() {
		User user = new User();
		user.setId(new Integer(7).shortValue());
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		Session session = Mockito.mock(Session.class);
		SessionFactory factory = Mockito.mock(SessionFactory.class);
		Mockito.when(factory.getCurrentSession()).thenReturn(session);
		Mockito.when(factory.openSession()).thenReturn(session);
		Mockito.when(session.save(ArgumentMatchers.any(IdentityObject.class))).thenReturn(null);
		this.testDAO.setSessionFactory(factory);

		Assertions.assertNotNull(this.testDAO, "DAO cannot be null");
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver1);
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver7);

		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context is null before");
		NamedEntity<Integer> bean = newNamedEntityBean(5);
		Mockito.doReturn(bean).when(this.testDAO).findByPrimaryKey(bean.getId());
		boolean exception = false;
		try {
			Mockito.verify(this.testDAO, Mockito.times(0)).findByPrimaryKey(bean.getId());
			this.testDAO.save(bean);
		}
		catch (Exception e) {
			exception = true;
		}

		Assertions.assertTrue(exception, "Exception is expected");
		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context must be null after");
		Mockito.verify(this.testDAO, Mockito.times(1)).findByPrimaryKey(bean.getId());
	}


	@Test
	public void testNestedDaoSaveDaoActionCount() {
		User user = new User();
		user.setId(new Integer(7).shortValue());
		this.contextHandler.setBean(Context.USER_BEAN_NAME, user);

		Session session = Mockito.mock(Session.class);
		SessionFactory factory = Mockito.mock(SessionFactory.class);
		SessionFactoryOptions options = Mockito.mock(SessionFactoryOptions.class);
		DirtyCheckingStrategy dirtyCheckingStrategy = Mockito.mock(DirtyCheckingStrategy.class);
		Mockito.when(options.getCustomEntityDirtinessStrategy()).thenReturn(dirtyCheckingStrategy);
		Mockito.when(factory.getSessionFactoryOptions()).thenReturn(options);
		Mockito.when(factory.getCurrentSession()).thenReturn(session);
		Mockito.when(factory.openSession()).thenReturn(session);
		Mockito.when(session.save(ArgumentMatchers.any(IdentityObject.class))).thenReturn(null);
		NamedEntity<Integer> originalBean = newNamedEntityBean(5);
		this.testDAO.setSessionFactory(factory);

		Assertions.assertNotNull(this.testDAO, "DAO cannot be null");
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testDaoEventObserver1);
		this.testDAO.registerEventObserver(DaoEventTypes.UPDATE, this.testRepeatSaveDaoEventObserver);

		this.testDAO.findByPrimaryKey(originalBean.getIdentity());
		Mockito.verify(session, Mockito.times(1)).get(NamedEntity.class, originalBean.getIdentity());

		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context is null before");
		NamedEntity<Integer> bean = newNamedEntityBean(5);
		Mockito.doReturn(bean).when(session).merge(ArgumentMatchers.any(IdentityObject.class));
		Mockito.doReturn(bean).when(this.testDAO).findByPrimaryKey(bean.getId());
		Mockito.verify(this.testDAO, Mockito.times(1)).findByPrimaryKey(bean.getId());
		Assertions.assertEquals(0, bean.getUpdateUserId());

		this.testDAO.save(bean);

		Assertions.assertNull(this.contextHandler.getBean(BaseDaoEventObserver.DAO_EVENT_CONTEXT_KEY), "event context is null after");
		Assertions.assertEquals(7, bean.getUpdateUserId());
		Mockito.verify(session, Mockito.times(1)).get(NamedEntity.class, originalBean.getIdentity());
		Mockito.verify(this.testDAO, Mockito.times(2)).findByPrimaryKey(bean.getId());
	}
}
