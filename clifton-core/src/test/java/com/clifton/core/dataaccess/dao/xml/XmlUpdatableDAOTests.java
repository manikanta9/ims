package com.clifton.core.dataaccess.dao.xml;

import com.clifton.core.dataaccess.dao.UpdatableDAO;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class XmlUpdatableDAOTests {

	@Resource
	private UpdatableDAO<CoreTestEntity> coreTestEntityDAO;
	@Resource
	private UpdatableDAO<CoreTestGroupEntity> coreTestGroupEntityDAO;


	@Test
	public void testUpdatedBean() {

		CoreTestEntity bean = this.coreTestEntityDAO.findByPrimaryKey(100);
		bean.setName("Updated Name");

		CoreTestEntity bean2 = this.coreTestEntityDAO.findByPrimaryKey(100);

		// Make sure the bean isn't updated in the "DB" even though we changed the name field value
		Assertions.assertFalse(bean2.getName().equals(bean.getName()));

		bean.setName("Updated Name Again");
		this.coreTestEntityDAO.save(bean);

		// After save find should return the updated bean
		bean2 = this.coreTestEntityDAO.findByPrimaryKey(100);
		Assertions.assertEquals("Updated Name Again", bean2.getName());
		Assertions.assertEquals(bean2.getName(), bean.getName());
	}


	@Test
	public void testUpdatedReferencedBean() {

		CoreTestEntity bean = this.coreTestEntityDAO.findByPrimaryKey(100);
		CoreTestGroupEntity groupBean = this.coreTestGroupEntityDAO.findByPrimaryKey(bean.getGroup().getId());

		groupBean.setName("Updated Group Name");
		Assertions.assertFalse(bean.getGroup().getName().equals(groupBean.getName()));
		bean = this.coreTestEntityDAO.findByPrimaryKey(100);
		Assertions.assertFalse(bean.getGroup().getName().equals(groupBean.getName()));

		this.coreTestGroupEntityDAO.save(groupBean);
		groupBean = this.coreTestGroupEntityDAO.findByPrimaryKey(groupBean.getId());
		Assertions.assertEquals("Updated Group Name", groupBean.getName());

		bean = this.coreTestEntityDAO.findByPrimaryKey(100);
		// After saving the group (with the udpated name field value) the bean should be updated with the new group name
		Assertions.assertEquals(groupBean.getName(), bean.getGroup().getName());
	}
}
