package com.clifton.core.dataaccess.dao.xml;

import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class XmlBasedCriteriaTests {

	@Resource
	private AdvancedUpdatableDAO<CoreTestEntity, Criteria> coreTestEntityDAO;

	@Resource
	private AdvancedUpdatableDAO<CoreTestGroupEntity, Criteria> coreTestGroupEntityDAO;


	@Test
	public void testSorting() {

		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.addOrder(Order.asc("name"));

		List<CoreTestEntity> sortedList = this.coreTestEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertEquals("[102][100][101]", getSortOrderAsString(sortedList));

		searchConfigurer = criteria -> criteria.addOrder(Order.desc("name"));
		sortedList = this.coreTestEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertEquals("[101][100][102]", getSortOrderAsString(sortedList));
	}


	@Test
	public void testRestrictions() {
		// test not null
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.add(Restrictions.isNotNull("description"));
		Assertions.assertEquals(2, CollectionUtils.getSize(this.coreTestEntityDAO.findBySearchCriteria(searchConfigurer)), "expecting 2 entities with NOT NULL description");

		searchConfigurer = criteria -> criteria.add(Restrictions.eq("name", "3rd Test Entity"));
		List<CoreTestEntity> list = this.coreTestEntityDAO.findBySearchCriteria(searchConfigurer);
		// Make sure only 1 result
		CoreTestEntity result = CollectionUtils.getOnlyElementStrict(list);
		Assertions.assertEquals(new Integer(102), result.getId());
		Assertions.assertEquals("3rd Test Entity", result.getName());

		// Delete it from the database
		this.coreTestEntityDAO.delete(result);

		// Do a search again - should have no results
		list = this.coreTestEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertTrue(CollectionUtils.isEmpty(list));
	}


	@Test
	public void testAlias() {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("group", "g").add(Restrictions.eq("g.name", "Group 1"));
		List<CoreTestEntity> list = this.coreTestEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertEquals(2, CollectionUtils.getSize(list));
	}


	@Test
	public void testLink() {
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.createAlias("entityList", "t").add(Restrictions.isNull("t.description"));
		List<CoreTestGroupEntity> list = this.coreTestGroupEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertEquals(1, CollectionUtils.getSize(list));
	}


	@Test
	public void testMaxResults() {
		List<CoreTestGroupEntity> fullList = this.coreTestGroupEntityDAO.findAll();
		Assertions.assertEquals(3, fullList.size());

		// Without any other search criteria, asking for max 2 results
		HibernateSearchConfigurer searchConfigurer = criteria -> criteria.setMaxResults(2);
		List<CoreTestGroupEntity> list = this.coreTestGroupEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertEquals(2, list.size());

		// Adding a restriction of Description being null, asking for max 2 results
		// Should only return 1, since there is only one that matches this.
		searchConfigurer = criteria -> {
			criteria.add(Restrictions.isNull("description"));
			criteria.setMaxResults(2);
		};

		list = this.coreTestGroupEntityDAO.findBySearchCriteria(searchConfigurer);
		Assertions.assertEquals(1, list.size());

		Assertions.assertEquals(1, CollectionUtils.getSize(list));
	}


	private String getSortOrderAsString(List<CoreTestEntity> sortedList) {
		StringBuilder sb = new StringBuilder();
		for (NamedEntity<Integer> entity : sortedList) {
			sb.append("[").append(entity.getId()).append("]");
		}
		return sb.toString();
	}
}
