package com.clifton.core.dataaccess.datatable.impl;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverterWithDynamicColumnCreation;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * {@link BeanCollectionToDataTableConverterTests} verify the functionality of {@link BeanCollectionToDataTableConverter}s.
 *
 * @author manderson
 * @author michaelm
 */
@ContextConfiguration()
@ExtendWith(SpringExtension.class)
public class BeanCollectionToDataTableConverterTests {

	public static final List<NamedEntityTestObject> namedEntityList = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	static {
		namedEntityList.add(new NamedEntityTestObject("Test 1 Name", "Test 1 Description"));
		namedEntityList.add(new NamedEntityTestObject("Test 2 Name", null));
		namedEntityList.add(new NamedEntityTestObject("Test 3 Name", "Test 3 Description"));
		namedEntityList.add(new NamedEntityTestObject("Test 4 Name", null));
		namedEntityList.add(new NamedEntityTestObject("Test 5 Name", null));
	}


	@Resource
	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testBaseBeanCollectionToDataTableConverter() {
		BeanCollectionToDataTableConverterWithDynamicColumnCreation<NamedEntityTestObject> converter = new BeanCollectionToDataTableConverterWithDynamicColumnCreation<>(new DataTableConverterConfiguration());
		DataTable dataTable = converter.convert(namedEntityList);

		// Data Table should only have 2 columns, Name, Description, Label, CreateDate, UpdateDate
		Assertions.assertEquals(5, dataTable.getColumnCount());

		for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
			DataRow row = dataTable.getRow(i);
			int id = i + 1;
			Assertions.assertEquals("Test " + id + " Name", row.getValue("Name"));
			if (id == 1 || id == 3) {
				Assertions.assertEquals("Test " + id + " Description", row.getValue("Description"));
			}
			else {
				Assertions.assertTrue(StringUtils.isEmpty((String) row.getValue("Description")));
			}
			Assertions.assertNull(row.getValue("Create Date"));
			Assertions.assertNull(row.getValue("Update Date"));
			Assertions.assertEquals(row.getValue("Name"), row.getValue("Label"));
		}
	}


	@Test
	public void testBeanCollectionToDataTableConverter() {
		String columns = "Name:name:fontColor=red|Description:description:width=200|Create Date:createDate:align=center,width=50";

		BeanCollectionToDataTableConverter<NamedEntityTestObject> converter = new BeanCollectionToDataTableConverter<NamedEntityTestObject>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(this.applicationContextService, NamedEntityTestObject.class, columns));
		DataTable dataTable = converter.convert(namedEntityList);

		// Data Table should only have 2 columns, Name, Description, Label, CreateDate, UpdateDate
		Assertions.assertEquals(3, dataTable.getColumnCount());

		for (DataColumn dc : dataTable.getColumnList()) {
			DataColumnStyle dcs = new DataColumnStyle(dc.getStyle());
			if ("Name".equals(dc.getColumnName())) {
				Assertions.assertEquals(100, dcs.getWidth());
				Assertions.assertEquals("left", dcs.getAlign());
				Assertions.assertEquals("red", dcs.getFontColor());
			}
			else if ("Description".equals(dc.getColumnName())) {
				Assertions.assertEquals(200, dcs.getWidth());
				Assertions.assertEquals("left", dcs.getAlign());
			}
			else {
				Assertions.assertEquals("Create Date", dc.getColumnName());
				Assertions.assertEquals(50, dcs.getWidth());
				Assertions.assertEquals("center", dcs.getAlign());
			}
		}

		for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
			DataRow row = dataTable.getRow(i);
			int id = i + 1;
			Assertions.assertEquals("Test " + id + " Name", row.getValue("Name"));
			if (id == 1 || id == 3) {
				Assertions.assertEquals("Test " + id + " Description", row.getValue("Description"));
			}
			else {
				Assertions.assertTrue(StringUtils.isEmpty((String) row.getValue("Description")));
			}
			Assertions.assertNull(row.getValue("Create Date"));
		}
	}


	@Test
	public void testBeanCollectionToDataTableConverter_WithCustomJsonString() {
		String columns = "Name:name:fontColor=red|Description:description:width=200|Test Date:CUSTOM_JSON_STRING_customColumns.TestDate:align=center,width=50|Test Identity Object:CUSTOM_JSON_STRING_customColumns.TestIdentityObject:width=50|Create Date:createDate:align=center,width=50";
		List<DataTableBeanTestObject> testList = new ArrayList<>();
		testList.add(populateDataTableBeanTestObject(1, "{\"TestDate\":\"04/01/2020\",\"TestIdentityObject\":{\"value\": \"401\", \"text\": \"Four Hundred and One\"}}}"));
		testList.add(populateDataTableBeanTestObject(2, "{\"TestDate\":\"06/01/2020\"}"));
		testList.add(populateDataTableBeanTestObject(3, null));

		BeanCollectionToDataTableConverter<DataTableBeanTestObject> converter = new BeanCollectionToDataTableConverter<DataTableBeanTestObject>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(this.applicationContextService, DataTableBeanTestObject.class, columns));
		DataTable dataTable = converter.convert(testList);

		// Data Table should only have 5 Columns
		Assertions.assertEquals(5, dataTable.getColumnCount());

		for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
			DataRow row = dataTable.getRow(i);
			int id = i + 1;
			Assertions.assertEquals("Test " + id, row.getValue("Name"));
			Assertions.assertEquals("Test " + id + " Description", row.getValue("Description"));
			if (id == 1) {
				Assertions.assertEquals("04/01/2020", row.getValue("Test Date"));
				Assertions.assertEquals("Four Hundred and One", row.getValue("Test Identity Object"));
			}
			else if (id == 2) {
				Assertions.assertEquals("06/01/2020", row.getValue("Test Date"));
				Assertions.assertNull(row.getValue("Test Identity Object"));
			}
			else {
				Assertions.assertNull(row.getValue("Test Identity Object"));
				Assertions.assertNull(row.getValue("Test Date"));
			}
			Assertions.assertNull(row.getValue("Create Date"));
		}
	}


	@Test
	public void testBeanCollectionToDataTableConverter_InitializationWithNoArgConstructor() {
		BeanCollectionToDataTableConverter<DataTableBeanTestObject> converter = new DataTableBeanTestObjectDataTableConverter();
		String columns = "Name:name:fontColor=red|Description:description:width=200|Create Date:createDate:align=center,width=50";
		converter.initialize(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(this.applicationContextService, DataTableBeanTestObject.class, columns));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataTableBeanTestObject populateDataTableBeanTestObject(int id, String jsonValueString) {
		DataTableBeanTestObject testObject = new DataTableBeanTestObject();
		testObject.setId(id);
		testObject.setName("Test " + id);
		testObject.setDescription(testObject.getName() + " Description");
		testObject.setCustomColumns(new CustomJsonString(jsonValueString));
		return testObject;
	}
}
