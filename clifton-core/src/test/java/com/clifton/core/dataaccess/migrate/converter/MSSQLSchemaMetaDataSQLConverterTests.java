package com.clifton.core.dataaccess.migrate.converter;

import com.clifton.core.dataaccess.migrate.reader.MigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * The <code>MSSQLSchemaMetaDataSQLConverterTests</code> class tests conversion of Schema objects to corresponding
 * meta data SQL.
 *
 * @author vgomelsky
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class MSSQLSchemaMetaDataSQLConverterTests {

	@Resource
	private MigrationDefinitionReader migrationXmlDefinitionReader;
	@Resource
	private MigrationSchemaConverter migrationSchemaMetaDataConverter;

	@Resource
	private String metaDataSQL;

	@Resource
	private String metaDataSQL2;

	@Resource
	private String metaDataSQL5;


	@Test
	public void testConvertSchemaToMetaDataSQL() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");

		String sql = this.migrationSchemaMetaDataConverter.convert(schema);
		Assertions.assertEquals(this.metaDataSQL.trim(), sql.trim());

		// now make sure changes are properly applied (no duplicates for the table, etc.)
		schema = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2.xml");
		sql = this.migrationSchemaMetaDataConverter.convert(schema);
		Assertions.assertEquals(this.metaDataSQL2.trim(), sql.trim());
	}


	@Test
	public void testConvertSchemaToMetaDataSQL5() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration5.xml");

		String sql = this.migrationSchemaMetaDataConverter.convert(schema);
		Assertions.assertEquals(this.metaDataSQL5.trim(), sql.trim());
	}
}
