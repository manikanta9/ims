package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.json.custom.CustomJsonString;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>DataTableBeanTestObject</code> ...
 *
 * @author manderson
 */
public class DataTableBeanTestObject extends NamedEntityTestObject {

	private BigDecimal amount;
	private Integer quantity;
	private Date tradeDate;

	private CustomJsonString customColumns;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public Integer getQuantity() {
		return this.quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public CustomJsonString getCustomColumns() {
		return this.customColumns;
	}


	public void setCustomColumns(CustomJsonString customColumns) {
		this.customColumns = customColumns;
	}
}
