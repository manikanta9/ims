package com.clifton.core.dataaccess.dao.hibernate;


import org.hibernate.dialect.function.SQLFunctionRegistry;
import org.hibernate.sql.Template;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class SQLServer2012DialectTests {

	@Test
	public void test() {
		String test = "CAST(DECOMPRESS(EntityJson) AS NVARCHAR(MAX))";
		SQLServer2012Dialect dialect = new SQLServer2012Dialect();
		String result = Template.renderWhereStringTemplate(test, dialect, new SQLFunctionRegistry(dialect, java.util.Collections.emptyMap()));
		Assertions.assertEquals("CAST(DECOMPRESS($PlaceHolder$.EntityJson) AS NVARCHAR(MAX))", result);
	}
}
