package com.clifton.core.dataaccess.sql;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.restrictions.BooleanRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ForeignKeyRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRestrictionDefinition;
import com.clifton.core.dataaccess.search.sql.SqlInlineParameterSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SqlInlineParameterSearchFormConfigurerTests</code> ...
 *
 * @author manderson
 */
public class SqlInlineParameterSearchFormConfigurerTests {

	@Test
	public void testConfigureCriteriaInlineParameters() {
		BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
		List<SearchRestriction> restrictionList = new ArrayList<>();
		SearchRestriction restriction = new SearchRestriction();
		restriction.setField("field");
		restriction.setComparison(ComparisonConditions.GREATER_THAN);
		restriction.setValue(2);
		restrictionList.add(restriction);

		SearchRestriction restriction2 = new SearchRestriction();
		restriction2.setField("fieldTwo");
		restriction2.setComparison(ComparisonConditions.EQUALS);
		restriction2.setValue(true);
		restrictionList.add(restriction2);
		searchForm.setRestrictionList(restrictionList);

		SqlInlineParameterSearchFormConfigurer configurer = new SqlInlineParameterSearchFormConfigurer(searchForm);
		configurer.setSelectClause("*");
		configurer.setFromClause("Table");
		configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<>(new IntegerRestrictionDefinition("field", "searchField", "sortField"), new Integer[]{1, 2, 3}));
		configurer.addSearchFieldDefinition(new BooleanRestrictionDefinition("fieldTwo", "searchFieldTwo"));

		SqlSelectCommand command = new SqlSelectCommand();
		configurer.configureCriteria(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nWHERE searchField > 2 AND searchFieldTwo = 1", command.getSql());
		Assertions.assertTrue(CollectionUtils.isEmpty(command.getSqlParameterValues()));
	}
}
