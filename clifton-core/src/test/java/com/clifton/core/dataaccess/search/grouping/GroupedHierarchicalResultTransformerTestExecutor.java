package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.grouping.transformer.GroupedHierarchicalResultTransformer;
import com.clifton.core.util.CollectionUtils;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("rawtypes")
public class GroupedHierarchicalResultTransformerTestExecutor extends BaseGroupedResultTransformerTestExecutor<GroupedHierarchicalResult> {


	/**
	 * Runs results for a specific retriever and entity
	 */
	public static GroupedHierarchicalResultTransformerTestExecutor forGroupingPropertyCountWithMocking(int groupingPropertyCount, List<Object[]> tuples) {
		return new GroupedHierarchicalResultTransformerTestExecutor(buildGroupingSearchForm(buildGroupingPropertyList(groupingPropertyCount), null), Mockito.mock(DaoLocator.class), Mockito.mock(DAOConfiguration.class), tuples);
	}


	public static GroupedHierarchicalResultTransformerTestExecutor forGroupingPropertyAndAggregatesWithMocking(int groupingPropertyCount, GroupingAggregateTypes[] aggregateTypes, List<Object[]> tuples) {
		return new GroupedHierarchicalResultTransformerTestExecutor(buildGroupingSearchForm(buildGroupingPropertyList(groupingPropertyCount), buildGroupingAggregatePropertyList(aggregateTypes)), Mockito.mock(DaoLocator.class), Mockito.mock(DAOConfiguration.class), tuples);
	}


	private GroupedHierarchicalResultTransformerTestExecutor(GroupingSearchForm groupingSearchForm, DaoLocator daoLocator, DAOConfiguration daoConfiguration, List<Object[]> tuples) {
		super(groupingSearchForm, daoLocator, daoConfiguration, tuples);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Grouped Result";
	}


	@SuppressWarnings("unchecked")
	@Override
	protected List<GroupedHierarchicalResult> executeTest() {
		GroupedHierarchicalResultTransformer transformer = new GroupedHierarchicalResultTransformer(getGroupingSearchForm(), getDaoLocator(), getDaoConfiguration());
		List<Object> transformedTuples = new ArrayList<>();

		if (!CollectionUtils.isEmpty(getTuples())) {
			getTuples().forEach(tuple -> transformedTuples.add(transformer.transformTuple(tuple, null)));
		}
		return (List<GroupedHierarchicalResult>) transformer.transformList(transformedTuples);
	}


	@Override
	protected String[] getStringValuesForResultEntity(GroupedHierarchicalResult resultEntity) {
		String resultEntityString = resultEntity.toStringFormatted("");
		return new String[]{resultEntityString};
	}
}
