package com.clifton.core.dataaccess.migrate.execution;

import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverter;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import com.clifton.core.dataaccess.migrate.reader.MigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.action.Argument;
import com.clifton.core.dataaccess.migrate.schema.dml.Data;
import com.clifton.core.dataaccess.migrate.schema.dml.Value;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class MSSQLMigrationExecutionHandlerTests {

	@Resource
	private MigrationDefinitionReader migrationXmlDefinitionReader;
	@Resource
	private MigrationSchemaConverter migrationSchemaPreDDLConverter;
	@Resource
	private MigrationSchemaConverter migrationSchemaDDLConverter;
	@Resource
	private MigrationSchemaConverter migrationSchemaDataConverter;
	@Resource
	private MigrationSchemaConverter migrationSchemaMetaDataConverter;
	@Resource
	private MigrationSchemaConverter migrationSchemaSQLConverter;

	@Resource
	private String migrationSQLOutputOriginalDDL;
	@Resource
	private String migrationSQLOutputOriginalData;
	@Resource
	private String migrationSQLOutputChange1DDL;
	@Resource
	private String migrationSQLOutputChange1Data;
	@Resource
	private String migrationSQLOutputAfterChange1DDL;
	@Resource
	private String migrationSQLOutputAfterChange1Data;
	@Resource
	private String migrationSQLOutputChange2;
	@Resource
	private String migrationSQLOutputAfterChange2;
	@Resource
	private String migrationSQLOutputChange3;
	@Resource
	private String migrationSQLOutputAfterChange3;
	@Resource
	private String migrationSQLOutputOriginalInheritance;
	@Resource
	private String migrationSQLOutputOriginalInheritanceUtf8;
	@Resource
	private String migrationSQLOutputCircularFKDependency;
	@Resource
	private String migrationSQLOutputWithActionsDDL;
	@Resource
	private String migrationSQLOutputWithActionsData;
	@Resource
	private String migrationSQLOutputWithActionsMetaData;
	@Resource
	private String migrationSQLOutputWithActionsSql;
	@Resource
	private String migrationSQLOutputExcludeTableDDL;
	@Resource
	private String migrationSQLOutputExcludeTableMetaData;
	@Resource
	private String migrationSQLOutputExcludeTableData;

	@Resource
	private String migrationSQLOutputColumnAuditTypes_META_DATA;

	@Resource
	private String migrationSQLOutputCompressedData;


	@Test
	public void testCompressedText() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-compressed-data.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputCompressedData.trim(), ddlSql.trim());
	}


	@Test
	public void testLoadAndGenerate() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputOriginalDDL.trim(), ddlSql.trim());
		String dataSql = this.migrationSchemaDataConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputOriginalData.trim(), dataSql.trim());
	}


	@Test
	public void testDefaultOrderByField() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		OrderByDirections isAsc = schema.getTable("CalendarDay").getColumn("CalendarDate").getSortDirection();
		Assertions.assertEquals(OrderByDirections.ASC, isAsc);
	}


	@Test
	public void testSetOrderByField() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		OrderByDirections isAsc = schema.getTable("CalendarMonth").getColumn("MonthName").getSortDirection();
		Assertions.assertEquals(OrderByDirections.DESC, isAsc);
	}


	@Test
	public void testLoadAndGenerateWithInheritance() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration5.xml");

		String sql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputOriginalInheritance.trim(), sql.trim());
	}


	@Test
	public void testLoadAndGenerateWithInheritance_utf8() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration5-utf8.xml");

		String sql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputOriginalInheritanceUtf8.trim(), sql.trim());
	}


	@Test
	public void testUpdatesToSchema() {
		// load original schema
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");

		// apply update 1 and verify updates SQL
		Schema schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2.xml");
		this.migrationXmlDefinitionReader.updateSchema(schema, schemaUpdates);
		String ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		Assertions.assertEquals(this.migrationSQLOutputChange1DDL.trim(), ddlSql.trim());

		String dataSql = this.migrationSchemaDataConverter.convert(schemaUpdates);
		Assertions.assertEquals(this.migrationSQLOutputChange1Data.trim(), dataSql.trim());

		// verify schema after update 1
		ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputAfterChange1DDL.trim(), ddlSql.trim());

		dataSql = this.migrationSchemaDataConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputAfterChange1Data.trim(), dataSql.trim());

		// apply update 2 and verify updates SQL
		schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration3.xml");
		this.migrationXmlDefinitionReader.updateSchema(schema, schemaUpdates);
		ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		Assertions.assertEquals(this.migrationSQLOutputChange2.trim(), ddlSql.trim());

		// verify schema after update 2
		ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputAfterChange2.trim(), ddlSql.trim());

		// apply update 3 and verify updates SQL
		schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration4.xml");
		this.migrationXmlDefinitionReader.updateSchema(schema, schemaUpdates);
		ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		Assertions.assertEquals(this.migrationSQLOutputChange3.trim(), ddlSql.trim());

		// verify schema after update 3
		ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputAfterChange3.trim(), ddlSql.trim());
	}


	@Test
	public void testMigrationsAndDefaultIndex() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration6.xml");

		Table tableWithDefaultYearIndex = schema.getTable("CalendarDayWithDefaultYearIndex");
		Assertions.assertEquals(3, tableWithDefaultYearIndex.getIndexList().size());
		Assertions.assertNotNull(tableWithDefaultYearIndex.getIndex("ix_CalendarDayWithDefaultYearIndex_CalendarYearID"));

		Table tableWithoutDefaultYearIndex = schema.getTable("CalendarDayWithoutDefaultYearIndex");
		Assertions.assertEquals(3, tableWithoutDefaultYearIndex.getIndexList().size());
		Assertions.assertNull(tableWithoutDefaultYearIndex.getIndex("ix_CalendarDayWithoutDefaultYearIndex_CalendarYearID"));
	}


	@Test
	public void testMigrationsAndCircularFKConstraint() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration7.xml");

		Table monthTable = schema.getTable("CalendarMonth");
		// Verify the FK Constraint exists, and is marked as delayed
		Assertions.assertNotNull(monthTable.getConstraint("FK_CalendarMonth_CalendarDay_FirstCalendarDayID"));
		Assertions.assertTrue(monthTable.getConstraint("FK_CalendarMonth_CalendarDay_FirstCalendarDayID").isDelayed());

		// Verify that FK Field Index is removed since it's part of a Unique Index
		Assertions.assertNull(monthTable.getIndex("ix_CalendarMonth_FirstCalendarDayID"));
		Assertions.assertNotNull(monthTable.getIndex("ux_CalendarMonth_FirstCalendarDayID_MonthName"));

		Table dayTable = schema.getTable("CalendarDay");
		Assertions.assertNotNull(dayTable.getConstraint("FK_CalendarDay_CalendarMonth_CalendarMonthID"));
		Assertions.assertFalse(dayTable.getConstraint("FK_CalendarDay_CalendarMonth_CalendarMonthID").isDelayed());

		// Verify SQL 
		String sql = this.migrationSchemaDDLConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputCircularFKDependency.trim(), sql.trim());
	}


	@Test
	public void testMigrationsColumnRequiredForValidation() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration8.xml");

		// Validate Required for Validation property
		Table calendarMonthTable = schema.getTable("CalendarMonth");
		for (Column column : calendarMonthTable.getColumnList()) {
			Assertions.assertTrue(column.getColumnRequired());
			if ("MonthAbbreviation".equals(column.getName())) {
				Assertions.assertFalse(column.getColumnRequiredForValidation(), "Column: " + column.getName() + " should not be required for validation");
			}
			else {
				Assertions.assertTrue(column.getColumnRequiredForValidation(), "Column: " + column.getName() + " should be required for validation");
			}
		}
	}


	@Test
	public void testMigrationsWithActions() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration8.xml");
		List<Action> actionList = schema.getActionList();
		Assertions.assertEquals(2, actionList.size());
		for (Action action : actionList) {
			Assertions.assertEquals("calendarService", action.getBean());
			if ("addCalendarYear".equals(action.getMethod())) {
				Assertions.assertEquals(1, action.getArgumentList().size());
				Argument arg = action.getArgumentList().get(0);
				Assertions.assertFalse(arg.isSelect());
				Assertions.assertEquals("2010", arg.getValue());
			}
			else {
				Assertions.assertEquals("deleteCalendarDay", action.getMethod());
				Assertions.assertEquals(1, action.getArgumentList().size());
				Argument arg = action.getArgumentList().get(0);
				Assertions.assertTrue(arg.isSelect());
				Assertions.assertEquals("(SELECT CalendarDayID FROM CalendarDay WHERE CalendarDate = '01/15/2010')", arg.getValue());
			}
		}

		// Verify Natural Keys
		for (Table table : schema.getTableList()) {
			for (Column column : table.getColumnList()) {
				if ("CalendarYear".equals(table.getName())) {
					Assertions.assertEquals("YearName".equals(column.getName()), column.isNaturalKey());
				}
				else if ("CalendarMonth".equals(table.getName())) {
					Assertions.assertEquals("MonthName".equals(column.getName()), column.isNaturalKey());
				}
				else if ("CalendarDay".equals(table.getName())) {
					Assertions.assertEquals("CalendarDate".equals(column.getName()), column.isNaturalKey());
				}
			}
		}

		// Verify Ignore Upload Columns
		for (Table table : schema.getTableList()) {
			for (Column column : table.getColumnList()) {
				if ("CalendarMonth".equals(table.getName()) && "MonthAbbreviation".equals(column.getName())) {
					Assertions.assertTrue(column.getIgnoreUpload());
				}
				else {
					Assertions.assertTrue(column.getIgnoreUpload() == null || !column.getIgnoreUpload());
				}
			}
		}

		// Verify Data
		Assertions.assertEquals(5, CollectionUtils.getSize(schema.getDataList()));
		Assertions.assertEquals(1, CollectionUtils.getSize(schema.getDataList(MigrationSchemaConverterTypes.DDL.name())));
		Assertions.assertEquals(1, CollectionUtils.getSize(schema.getDataList(MigrationSchemaConverterTypes.META_DATA.name())));
		Assertions.assertEquals(3, CollectionUtils.getSize(schema.getDataList(MigrationSchemaConverterTypes.DATA.name())));

		for (Data data : schema.getDataList()) {
			for (Value val : data.getValueList()) {
				Assertions.assertNotNull(val.getDataType(), "Column [" + val.getColumn() + "] is missing a data type");
			}
		}

		// Verify SQL Tags
		Assertions.assertEquals(4, CollectionUtils.getSize(schema.getSqlList()));
		Assertions.assertEquals(1, CollectionUtils.getSize(schema.getSqlList(MigrationSchemaConverterTypes.DDL.name())));
		Assertions.assertEquals(1, CollectionUtils.getSize(schema.getSqlList(MigrationSchemaConverterTypes.SQL.name())));
		Assertions.assertEquals(1, CollectionUtils.getSize(schema.getSqlList(MigrationSchemaConverterTypes.META_DATA.name())));

		// Verify SQL - SQL Itself should not have any actions in them.
		String preDdlSql = this.migrationSchemaPreDDLConverter.convert(schema);
		preDdlSql = preDdlSql.replaceAll("\\r*", "");
		Assertions.assertEquals("SELECT * FROM CalendarYear WHERE CalendarYearID = 2013;", preDdlSql.trim());

		String ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		ddlSql = ddlSql.replaceAll("\\r*", "");
		Assertions.assertEquals(this.migrationSQLOutputWithActionsDDL.trim(), ddlSql.trim());

		String dataSql = this.migrationSchemaDataConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputWithActionsData.trim(), dataSql.trim());

		String metaSql = this.migrationSchemaMetaDataConverter.convert(schema);
		metaSql = metaSql.replaceAll("\\r*", "");
		Assertions.assertEquals(this.migrationSQLOutputWithActionsMetaData.trim(), metaSql.trim());

		String sqlActual = this.migrationSchemaSQLConverter.convert(schema).trim();
		// Need to remove /r so the strings can be compared properly
		sqlActual = sqlActual.replaceAll("\\r*", "");
		Assertions.assertEquals(this.migrationSQLOutputWithActionsSql.trim(), sqlActual);
	}


	@Test
	public void testConvertSchemaWithBadData() {
		Assertions.assertThrows(RuntimeException.class, () -> this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration9.xml"));
	}


	@Test
	public void testFileExcludeFromSql() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-excludeSQL.xml");
		Assertions.assertTrue(schema.getExcludeFromSql());

		// Verify SQL  
		Assertions.assertEquals("", this.migrationSchemaDDLConverter.convert(schema).trim());
		Assertions.assertEquals("", this.migrationSchemaMetaDataConverter.convert(schema).trim());
		Assertions.assertEquals("", this.migrationSchemaDataConverter.convert(schema).trim());
		Assertions.assertEquals("", this.migrationSchemaSQLConverter.convert(schema).trim());
	}


	@Test
	public void testTableExcludeFromSql() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-excludeTableSQL.xml");
		Assertions.assertTrue(schema.getExcludeFromSql() == null || !schema.getExcludeFromSql());

		Table table = schema.getTable("CalendarDay");
		Assertions.assertTrue(table.getExcludeFromSql());

		// Verify SQL  
		String ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		ddlSql = ddlSql.replaceAll("\\r*", "");
		Assertions.assertEquals(this.migrationSQLOutputExcludeTableDDL.trim(), ddlSql.trim());

		String metaSql = this.migrationSchemaMetaDataConverter.convert(schema);
		metaSql = metaSql.replaceAll("\\r*", "");
		Assertions.assertEquals(this.migrationSQLOutputExcludeTableMetaData.trim(), metaSql.trim());

		String dataSql = this.migrationSchemaDataConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputExcludeTableData.trim(), dataSql.trim());

		String sqlActual = this.migrationSchemaSQLConverter.convert(schema).trim();
		// Need to remove /r so the strings can be compared properly
		sqlActual = sqlActual.replaceAll("\\r*", "");
		Assertions.assertEquals("", sqlActual);
	}


	@Test
	public void testSqlInvalidRunWith() {
		Assertions.assertThrows(RuntimeException.class, () -> this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-runWithInvalid.xml"));
	}


	@Test
	public void testNullablePrimitiveFields() {
		TestUtils.expectException(Exception.class,
				() -> this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration10.xml"),
				"Table 'CalendarDay' Column 'CalendarMonthID' is nullable, but the bean property is primitive type 'int'.  Either change the field to required or change the bean property to it's Object equivalent."
		);
	}


	@Test
	public void testColumnAuditTypes() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration11.xml");

		String sql = this.migrationSchemaMetaDataConverter.convert(schema);
		Assertions.assertEquals(this.migrationSQLOutputColumnAuditTypes_META_DATA.trim(), sql.trim());
	}


	@Test
	public void testCalculatedColumns() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-calculated-columns.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schema);
		validateEqualWithTrimmedWhiteSpace("CREATE TABLE CalendarDay ("//
						+ " CalendarDayID BIGINT IDENTITY (1, 1) NOT NULL"//
						+ " , CalendarDate DATE NOT NULL"//
						+ " , DateFullString  AS DATENAME(WEEKDAY, CalendarDate) + ' ' + DATENAME(MONTH, CalendarDate + ' ' + DATENAME(DAY, CalendarDate) + ', ' + DATENAME(YEAR, CalendarDate)"//
						+ " );"//
						+ " ALTER TABLE CalendarDay ADD CONSTRAINT [PK_CalendarDay]" + "	PRIMARY KEY CLUSTERED (CalendarDayID);"//
						+ " CREATE UNIQUE INDEX [ux_CalendarDay_CalendarDate] ON CalendarDay (CalendarDate);"//
						+ " CREATE TABLE CalendarDay2 ("//
						+ " CalendarDay2ID BIGINT IDENTITY (1, 1) NOT NULL"//
						+ " , CalendarDate DATE NOT NULL"//
						+ " , DateFullString  AS DATENAME(WEEKDAY, CalendarDate) + ' ' + DATENAME(MONTH, CalendarDate + ' ' + DATENAME(DAY, CalendarDate) + ', ' + DATENAME(YEAR, CalendarDate)"//
						+ " );"//
						+ " ALTER TABLE CalendarDay2 ADD CONSTRAINT [PK_CalendarDay2]" + "	PRIMARY KEY CLUSTERED (CalendarDay2ID);"//
						+ " CREATE UNIQUE INDEX [ux_CalendarDay2_CalendarDate] ON CalendarDay2 (CalendarDate);"//
				, ddlSql);

		String metaDataSql = this.migrationSchemaMetaDataConverter.convert(schema);
		validateEqualWithTrimmedWhiteSpace(
				"INSERT INTO SystemTable(SystemDataSourceID, TableName, TableLabel, TableAlias, TableDescription, IsUploadAllowed, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')" //
						+ ", 'CalendarDay', 'Calendar Day', NULL, 'Calendar Day', 0, 0, GETDATE(), 0, GETDATE(); " //
						+ " INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))" //
						+ ", (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER')" //
						+ ", 'CalendarDayID', 'Calendar Day ID', 'Calendar Day ID', 'id', 1, 0, GETDATE(), 0, GETDATE();" //
						+ " INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))" //
						+ ", (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'DATE')" //
						+ ", 'CalendarDate', 'Calendar Date', 'Calendar Date', 'date', 1, 0, GETDATE(), 0, GETDATE(); " //
						+ " INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, SystemAuditTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))" //
						+ ", (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING')" //
						+ ", (SELECT COALESCE((SELECT CAST(SystemAuditTypeID AS VARCHAR) FROM SystemAuditType WHERE AuditTypeName = 'NO AUDIT'), 'Invalid Audit Type: NO AUDIT'))" //
						+ ", 'DateFullString', 'Date Full String', 'Date Full String', 'dateFullString', 1, 0, GETDATE(), 0, GETDATE();" //

						+ " INSERT INTO SystemTable(SystemDataSourceID, TableName, TableLabel, TableAlias, TableDescription, IsUploadAllowed, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')" //
						+ ", 'CalendarDay2', 'Calendar Day 2', NULL, 'Calendar Day 2', 0, 0, GETDATE(), 0, GETDATE(); " //
						+ " INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay2' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))" //
						+ ", (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER')" //
						+ ", 'CalendarDay2ID', 'Calendar Day 2ID', 'Calendar Day 2ID', 'id', 1, 0, GETDATE(), 0, GETDATE();" //
						+ " INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay2' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))" //
						+ ", (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'DATE')" //
						+ ", 'CalendarDate', 'Calendar Date', 'Calendar Date', 'date', 1, 0, GETDATE(), 0, GETDATE(); " //
						+ " INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, SystemAuditTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)" //
						+ " SELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay2' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))" //
						+ ", (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING')" //
						+ ", (SELECT COALESCE((SELECT CAST(SystemAuditTypeID AS VARCHAR) FROM SystemAuditType WHERE AuditTypeName = 'NO AUDIT'), 'Invalid Audit Type: NO AUDIT'))" //
						+ ", 'DateFullString', 'Date Full String', 'Date Full String', 'dateFullString', 1, 0, GETDATE(), 0, GETDATE();", metaDataSql);
	}


	@Test
	public void testCalculatedColumnFailOnInsert() {
		// Should fail when trying to insert value into calculated column
		TestUtils.expectException(Exception.class,
				() -> this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration-calculated-column-fail-insert.xml"),
				"Column [DateFullString] for table [CalendarDay] is a calculated column and cannot be explicitly inserted with value [Thursday January 1, 2014]."
		);
	}


	@Test
	public void testUpdateSqlOnly_BitFields() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		Schema schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2-updatesqlonly.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("ALTER TABLE CalendarDay ADD\n" +
				"\tIsMonthEnd BIT NOT NULL CONSTRAINT DF_CalendarDay_IsMonthEnd DEFAULT(0)\n" +
				"\t, IsQuarterEnd BIT NOT NULL CONSTRAINT DF_CalendarDay_IsQuarterEnd DEFAULT(0)\n" +
				"\t, IsYearEnd BIT NOT NULL CONSTRAINT DF_CalendarDay_IsYearEnd DEFAULT(0);", ddlSql);
		String metaDataSql = this.migrationSchemaMetaDataConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'BOOLEAN'), 'IsMonthEnd', 'Is Month End', 'Is Month End', 'monthEnd', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'BOOLEAN'), 'IsQuarterEnd', 'Is Quarter End', 'Is Quarter End', 'quarterEnd', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarDay' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'BOOLEAN'), 'IsYearEnd', 'Is Year End', 'Is Year End', 'yearEnd', 1, 0, GETDATE(), 0, GETDATE();", metaDataSql);
	}


	@Test
	public void testUpdateSqlOnly_MultipleTablesAndFields() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration2b.xml");
		Schema schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2b-update-sql-only.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("ALTER TABLE CalendarYear ADD\n" +
				"\tFirstCalendarMonthID INT NULL\n" +
				"\t, LastCalendarMonthID INT NULL;\n" +
				"ALTER TABLE CalendarYear ADD CONSTRAINT [FK_CalendarYear_CalendarMonth_FirstCalendarMonthID] FOREIGN KEY (FirstCalendarMonthID)\n" +
				"\tREFERENCES CalendarMonth(CalendarMonthID);\n" +
				"ALTER TABLE CalendarYear ADD CONSTRAINT [FK_CalendarYear_CalendarMonth_LastCalendarMonthID] FOREIGN KEY (LastCalendarMonthID)\n" +
				"\tREFERENCES CalendarMonth(CalendarMonthID);\n" +
				"ALTER TABLE CalendarMonth ADD\n" +
				"\tMonthAbbreviation NVARCHAR(50) NULL;", ddlSql);
		String metaDataSql = this.migrationSchemaMetaDataConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarYear' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'FirstCalendarMonthID', 'First Calendar Month ID', 'First Calendar Month ID', 'firstMonth', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarYear' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'LastCalendarMonthID', 'Last Calendar Month ID', 'Last Calendar Month ID', 'lastMonth', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'MonthAbbreviation', 'Month Abbreviation', 'Month Abbreviation', 'abbreviation', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"INSERT INTO SystemRelationship(SystemColumnID, ParentSystemColumnID, RelationshipName, RelationshipLabel, RelationshipDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'FirstCalendarMonthID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarYear' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))), (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'CalendarMonthID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))), 'FK_CalendarYear_CalendarMonth_FirstCalendarMonthID', NULL, NULL, 0, GETDATE(), 0, GETDATE();\n" +
				"INSERT INTO SystemRelationship(SystemColumnID, ParentSystemColumnID, RelationshipName, RelationshipLabel, RelationshipDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'LastCalendarMonthID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarYear' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))), (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = 'CalendarMonthID' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'))), 'FK_CalendarYear_CalendarMonth_LastCalendarMonthID', NULL, NULL, 0, GETDATE(), 0, GETDATE();\n" +
				"\n", metaDataSql);
		String sqlSql = this.migrationSchemaSQLConverter.convert(schemaUpdates);
		// Marking as Required and UX should be added at the end
		validateEqualWithTrimmedWhiteSpace("ALTER TABLE CalendarYear ALTER COLUMN LastCalendarMonthID INT NOT NULL;\n" +
				"CREATE INDEX [ix_CalendarYear_FirstCalendarMonthID] ON CalendarYear (FirstCalendarMonthID);\n" +
				"CREATE INDEX [ix_CalendarYear_LastCalendarMonthID] ON CalendarYear (LastCalendarMonthID);\n" +
				"ALTER TABLE CalendarMonth ALTER COLUMN MonthAbbreviation NVARCHAR(50) NOT NULL;\n" +
				"CREATE UNIQUE INDEX [ux_CalendarMonth_MonthAbbreviation] ON CalendarMonth (MonthAbbreviation);", sqlSql);
	}


	@Test
	public void testUpdateSqlOnly_FailMissingFieldInOriginalMigration() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		TestUtils.expectException(Exception.class,
				() -> this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2-updatesqlonly-fail.xml"),
				"Cannot run UPDATE_SQL_ONLY migration for Table 'CalendarYear' because original migration was not updated to include new column 'TestField'"
		);
	}


	@Test
	public void testUpdateSqlOnly_RequiredStringField() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		Schema schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2-updatesqlonly-requiredfield.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("ALTER TABLE CalendarMonth ADD\n" +
				"\tMonthAbbreviation NVARCHAR(50) NULL;\n", ddlSql);
		String metaDataSql = this.migrationSchemaMetaDataConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'MonthAbbreviation', 'Month Abbreviation', 'Month Abbreviation', 'abbreviation', 1, 0, GETDATE(), 0, GETDATE();\n", metaDataSql);

		String sqlSql = this.migrationSchemaSQLConverter.convert(schemaUpdates);
		// Marking as Required and UX should be added at the end
		validateEqualWithTrimmedWhiteSpace("\t\t\tUPDATE CalendarMonth SET MonthAbbreviation = SUBSTRING(MonthName,0,3);\n" +
				"\t\t\n" +
				"GO\tALTER TABLE CalendarMonth ALTER COLUMN MonthAbbreviation NVARCHAR(50) NOT NULL;\n" +
				"CREATE UNIQUE INDEX [ux_CalendarMonth_MonthAbbreviation] ON CalendarMonth (MonthAbbreviation);", sqlSql);
	}


	@Test
	public void testCreateSqlOnly_FailMissingTableInOriginalMigration() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		TestUtils.expectException(Exception.class,
				() -> this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2-create-sql-only-fail.xml"),
				"Cannot find table 'Payment' in current schema."
		);
	}


	@Test
	public void testCreateSqlOnly_Success() {
		Schema schema = this.migrationXmlDefinitionReader.loadMigration(null, "com/clifton/core/dataaccess/migrate/execution/migration.xml");
		Schema schemaUpdates = this.migrationXmlDefinitionReader.loadMigration(schema, "com/clifton/core/dataaccess/migrate/execution/migration2-create-sql-only.xml");

		String ddlSql = this.migrationSchemaDDLConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("CREATE TABLE CalendarMonth (\n" +
				"\tCalendarMonthID INT IDENTITY (1, 1) NOT NULL\n" +
				"\t, MonthName NVARCHAR(50) NOT NULL\n" +
				"\t, MonthAbbreviation NVARCHAR(50) NOT NULL\n" +
				"\t, IsYearEnd BIT NOT NULL CONSTRAINT DF_CalendarMonth_IsYearEnd DEFAULT(0)\n" +
				"\t, IsYearStart BIT NOT NULL CONSTRAINT DF_CalendarMonth_IsYearStart DEFAULT(0)\n" +
				"\t, CreateUserID SMALLINT NOT NULL\n" +
				"\t, CreateDate DATETIME NOT NULL\n" +
				"\t, UpdateUserID SMALLINT NOT NULL\n" +
				"\t, UpdateDate DATETIME NOT NULL\n" +
				"\t, rv ROWVERSION NOT NULL\n" +
				");\n" +
				"\n" +
				"ALTER TABLE CalendarMonth ADD CONSTRAINT [PK_CalendarMonth]\n" +
				"\tPRIMARY KEY CLUSTERED (CalendarMonthID);\n" +
				"\n" +
				"CREATE UNIQUE INDEX [ux_CalendarMonth_MonthName] ON CalendarMonth (MonthName);\n" +
				"\n" +
				"CREATE UNIQUE INDEX [ux_CalendarMonth_MonthAbbreviation] ON CalendarMonth (MonthAbbreviation);", ddlSql);
		String metaDataSql = this.migrationSchemaMetaDataConverter.convert(schemaUpdates);
		validateEqualWithTrimmedWhiteSpace("INSERT INTO SystemTable(SystemDataSourceID, TableName, TableLabel, TableAlias, TableDescription, IsUploadAllowed, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource'), 'CalendarMonth', 'Calendar Month', NULL, 'Calendar Month', 0, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'CalendarMonthID', 'Calendar Month ID', 'Calendar Month ID', 'id', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'MonthName', 'Month Name', 'Month Name', 'name', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'MonthAbbreviation', 'Month Abbreviation', 'Month Abbreviation', 'abbreviation', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'BOOLEAN'), 'IsYearEnd', 'Is Year End', 'Is Year End', 'yearEnd', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'BOOLEAN'), 'IsYearStart', 'Is Year Start', 'Is Year Start', 'yearStart', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'CreateUserID', 'Create User ID', 'Create User ID', 'createUserId', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'DATE'), 'CreateDate', 'Create Date', 'Create Date', 'createDate', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'INTEGER'), 'UpdateUserID', 'Update User ID', 'Update User ID', 'updateUserId', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'DATE'), 'UpdateDate', 'Update Date', 'Update Date', 'updateDate', 1, 0, GETDATE(), 0, GETDATE();\n" +
				"\n" +
				"INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)\n" +
				"\tSELECT (SELECT SystemTableID FROM SystemTable WHERE TableName = 'CalendarMonth' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = 'dataSource')), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = 'STRING'), 'rv', 'rv', 'rv', 'rv', 1, 0, GETDATE(), 0, GETDATE();", metaDataSql);

		String sqlSql = this.migrationSchemaSQLConverter.convert(schemaUpdates);
		// no additional sql
		validateEqualWithTrimmedWhiteSpace("", sqlSql);
	}


	private void validateEqualWithTrimmedWhiteSpace(String expected, String actual) {
		Assertions.assertEquals(expected.replaceAll("\\s+", " ").trim(), actual.replaceAll("\\s+", " ").trim());
	}
}
