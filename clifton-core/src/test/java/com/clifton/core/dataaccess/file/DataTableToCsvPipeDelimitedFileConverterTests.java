package com.clifton.core.dataaccess.file;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanDataTable;
import org.junit.jupiter.api.Assertions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;


/**
 * The <code>DataTableToPdfFileConverterTests</code> tests the {@link DataTableToPdfFileConverter}
 *
 * @author manderson
 */
public class DataTableToCsvPipeDelimitedFileConverterTests extends DataTableToFileConverterTestBase {

	private DataTableToFileConverter dataTableToFileConverter = FileFormats.CSV_PIPE.getDataTableConverter();


	@Override
	public DataTableToFileConverter getDataTableToFileConverter() {
		return this.dataTableToFileConverter;
	}


	@Override
	public void validateFile(DataTable dataTable, File file) {
		Assertions.assertNotNull(file);
		if (dataTable == null) {
			Assertions.assertEquals(0, file.length());
			return;
		}
		FileInputStream is = null;
		try {
			is = new FileInputStream(file);
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line;
			int lineCount = 0;
			while ((line = reader.readLine()) != null) {
				String expectedResult = getExpectedResult(dataTable, lineCount);
				Assertions.assertEquals(expectedResult, line);
				lineCount++;
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		finally {
			FileUtils.close(is);
		}
	}


	private String getExpectedResult(DataTable dataTable, int lineCount) {
		if (dataTable instanceof BeanDataTable<?>) {
			if (lineCount == 0) {
				return "Name|Description|Create Date";
			}
			else if (lineCount == 1 || lineCount == 3) {
				return "Test " + lineCount + " Name|Test " + lineCount + " Description|";
			}
			else {
				return "Test " + lineCount + " Name||";
			}
		}
		else if (dataTable.getTotalRowCount() == 0 || lineCount == 0) {
			return "Name|Description|Create Date|Amount|Quantity";
		}
		else {
			if (lineCount == 1) {
				return "Test One|Test for Row 1|2010-01-01 00:00:00|5.5|";
			}
			if (lineCount == 2) {
				return "Test Two||2010-02-01 00:00:00|5|5";
			}
			if (lineCount == 3) {
				return "Test Three|Test for Row 3||17.798|7";
			}
			if (lineCount == 4) {
				return "Test Four|Test for Row 4|2010-03-30 00:00:00||10";
			}
		}
		return null;
	}
}
