-- Insert May Into Database
INSERT INTO CalendarMonth(CalendarMonthID, MonthName, MonthAbbreviation)
VALUES (5, 'May', 'MAY');

-- Fix May's Abbreviation
UPDATE CalendarMonth
SET MonthAbbreviation = 'May'
WHERE MonthName = 'May';
