package com.clifton.core.dataaccess.migrate.execution;


import com.clifton.core.beans.BaseEntity;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>Payment</code>
 * <p/>
 * Includes Workflow properties and Document Properties so can test
 * No Audit on these by column name
 *
 * @author manderson
 */
public class Payment extends BaseEntity<Integer> {

	private Frequencies frequency;
	private BigDecimal amount;

	private CalendarDay paymentCalendarDay;

	private Short workflowStateId;
	private Short workflowStatusId;
	private Date workflowStateEffectiveStartDate;

	private Date documentUpdateDate;
	private String documentUpdateUser;
	private String documentFileType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Frequencies getFrequency() {
		return this.frequency;
	}


	public void setFrequency(Frequencies frequency) {
		this.frequency = frequency;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public CalendarDay getPaymentCalendarDay() {
		return this.paymentCalendarDay;
	}


	public void setPaymentCalendarDay(CalendarDay paymentCalendarDay) {
		this.paymentCalendarDay = paymentCalendarDay;
	}


	public Short getWorkflowStateId() {
		return this.workflowStateId;
	}


	public void setWorkflowStateId(Short workflowStateId) {
		this.workflowStateId = workflowStateId;
	}


	public Short getWorkflowStatusId() {
		return this.workflowStatusId;
	}


	public void setWorkflowStatusId(Short workflowStatusId) {
		this.workflowStatusId = workflowStatusId;
	}


	public Date getWorkflowStateEffectiveStartDate() {
		return this.workflowStateEffectiveStartDate;
	}


	public void setWorkflowStateEffectiveStartDate(Date workflowStateEffectiveStartDate) {
		this.workflowStateEffectiveStartDate = workflowStateEffectiveStartDate;
	}


	public Date getDocumentUpdateDate() {
		return this.documentUpdateDate;
	}


	public void setDocumentUpdateDate(Date documentUpdateDate) {
		this.documentUpdateDate = documentUpdateDate;
	}


	public String getDocumentUpdateUser() {
		return this.documentUpdateUser;
	}


	public void setDocumentUpdateUser(String documentUpdateUser) {
		this.documentUpdateUser = documentUpdateUser;
	}


	public String getDocumentFileType() {
		return this.documentFileType;
	}


	public void setDocumentFileType(String documentFileType) {
		this.documentFileType = documentFileType;
	}
}
