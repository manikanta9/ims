package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.context.ThreadLocalContextHandler;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import org.junit.jupiter.api.Assertions;


/**
 * The <code>TestRepeatSaveDaoEventObserver</code> class is a test class that verifies proper nested DAO action counts.
 *
 * @author NickK
 */
public class TestRepeatSaveDaoEventObserver<T extends IdentityObject> extends TestDaoEventObserver<T> {

	private static final ContextHandler CONTEXT_HANDLER = new ThreadLocalContextHandler();
	private static final String BEFORE_TRANSACTION_COUNT = "nestedTestObserverBefore";
	private static final String AFTER_TRANSACTION_COUNT = "nestedTestObserverAfter";
	private static final int NESTED_SAVE_COUNT = 2;


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		Integer beforeCount = (Integer) CONTEXT_HANDLER.getBean(BEFORE_TRANSACTION_COUNT);
		CONTEXT_HANDLER.setBean(BEFORE_TRANSACTION_COUNT, beforeCount == null ? 1 : beforeCount + 1);
	}


	@Override
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		super.afterMethodCallImpl(dao, event, bean, e);
		Integer beforeCount = (Integer) CONTEXT_HANDLER.getBean(BEFORE_TRANSACTION_COUNT);
		Assertions.assertNotNull(beforeCount, "DAO nested before action count cannot be null in after method call.");
		Integer afterCount = (Integer) CONTEXT_HANDLER.getBean(AFTER_TRANSACTION_COUNT);
		afterCount = afterCount == null ? 1 : afterCount + 1;
		CONTEXT_HANDLER.setBean(AFTER_TRANSACTION_COUNT, afterCount);

		if ((event.isInsert() || event.isUpdate()) && beforeCount <= NESTED_SAVE_COUNT) {
			// nested saves up to NESTED_SAVE_COUNT
			((UpdatableDAO<T>) dao).save(bean);
		}

		if (getDaoEventContext().isLastIntermediateAfterCall()) {
			CONTEXT_HANDLER.setBean(BEFORE_TRANSACTION_COUNT, beforeCount - 1);
			CONTEXT_HANDLER.setBean(AFTER_TRANSACTION_COUNT, afterCount - 1);
		}
	}


	@Override
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		Integer beforeCount = (Integer) CONTEXT_HANDLER.getBean(BEFORE_TRANSACTION_COUNT);
		Assertions.assertNotNull(beforeCount, "DAO nested before action count cannot be null in after transaction method call.");
		Integer afterCount = (Integer) CONTEXT_HANDLER.getBean(AFTER_TRANSACTION_COUNT);
		Assertions.assertNotNull(afterCount, "DAO nested after action count cannot be null in after transaction method call.");
		Assertions.assertTrue(beforeCount.equals(afterCount), "Before and after action count should be the same.");

		if (getDaoEventContext().isLastAfterCall()) {
			Assertions.assertTrue(afterCount.equals(0), "An incorrect number of nested transactions completed: " + afterCount);
			CONTEXT_HANDLER.clear();
		}
	}
}
