package com.clifton.core.dataaccess.dao.event;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class DaoCacheTests {

	@Resource
	private CacheHandler<String, ObjectWrapper<Integer>> cacheHandler;

	@Resource
	private UpdatableDAO<NamedEntityTestObject> namedEntityTestObjectDAO;

	@Resource
	private NamedEntityCache<NamedEntityTestObject> namedEntityTestObjectCache;


	@Resource
	private SelfRegisteringSingleKeyDaoCache<NamedEntityTestObject, String> namedEntityTestObjectByNameWithClearCache;


	@Test
	public void testCacheUpdatesAfterChanges() {
		// This cache will immediately set the set after inserts and updates
		String defaultCacheName = this.namedEntityTestObjectCache.getCacheName();
		// This cache never sets after inserts and updates and always clears the cache
		String clearCacheName = this.namedEntityTestObjectByNameWithClearCache.getCacheName();

		NamedEntityTestObject entity = new NamedEntityTestObject();
		entity.setName("Test Bean 1");
		entity.setDescription("TEST DESCRIPTION");

		entity = this.namedEntityTestObjectDAO.save(entity);
		// Default Cache should NOT be empty after insert and should reference the inserted entity
		Assertions.assertNotNull(this.cacheHandler.get(defaultCacheName, entity.getName()), defaultCacheName + " Cache should NOT be empty for this key " + entity.getName());
		Assertions.assertEquals(entity.getId(), this.cacheHandler.get(defaultCacheName, entity.getName()).getObject());

		// Clear Cache should be empty after insert
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, entity.getName()), clearCacheName + " Cache should be empty for this key " + entity.getName());

		List<NamedEntityTestObject> list = this.namedEntityTestObjectDAO.findAll();
		Assertions.assertEquals(1, CollectionUtils.getSize(list));

		// Change the Name and save - cache should have been cleared for the original name, and set for the new name
		String originalName = entity.getName();
		String newName = "Test Bean 1 - Updated";
		entity.setName(newName);
		this.namedEntityTestObjectDAO.save(entity);

		// both caches should have been cleared for the original key
		Assertions.assertNull(this.cacheHandler.get(defaultCacheName, originalName), defaultCacheName + " Cache should be empty for this key " + originalName);
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, originalName), clearCacheName + " Cache should be empty for this key " + originalName);

		// default  cache should NOT be empty for the new key
		Assertions.assertNotNull(this.cacheHandler.get(defaultCacheName, entity.getName()), defaultCacheName + " Cache should not be empty for this key " + entity.getName());
		Assertions.assertEquals(entity.getId(), this.cacheHandler.get(defaultCacheName, entity.getName()).getObject());

		// cleared cache should be empty for the new key
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, entity.getName()), clearCacheName + " Cache should not be empty for this key " + entity.getName());

		// Delete the bean
		this.namedEntityTestObjectDAO.delete(entity);

		// both caches should have been cleared
		Assertions.assertNull(this.cacheHandler.get(defaultCacheName, entity.getName()), defaultCacheName + " Cache should be empty for this key " + entity.getName());
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, entity.getName()), clearCacheName + " Cache should be empty for this key " + entity.getName());
	}


	@Test
	public void testCache_Missing() {
		// This cache will immediately set the set after inserts and updates
		String defaultCacheName = this.namedEntityTestObjectCache.getCacheName();
		// This cache never sets after inserts and updates and always clears the cache
		String clearCacheName = this.namedEntityTestObjectByNameWithClearCache.getCacheName();

		String name = "Test Bean ABC";

		// Caches should be empty
		Assertions.assertNull(this.cacheHandler.get(defaultCacheName, name), defaultCacheName + " Cache should be empty for this key " + name);
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, name), clearCacheName + " Cache should be empty for this key " + name);

		// Look up the entity by name
		NamedEntityTestObject testEntity = this.namedEntityTestObjectCache.getBeanForKeyValue(this.namedEntityTestObjectDAO, name);
		Assertions.assertNull(testEntity, "Test entity with name " + name + " should be NULL");

		// Caches should NOT be empty, but wrapped a null value
		Assertions.assertNotNull(this.cacheHandler.get(defaultCacheName, name), defaultCacheName + " Cache should NOT be empty for this key " + name);
		Assertions.assertNull(this.cacheHandler.get(defaultCacheName, name).getObject(), defaultCacheName + " Cache wrapped object should be null for this key " + name);

		// Look up the entity by name on the other cache
		testEntity = this.namedEntityTestObjectByNameWithClearCache.getBeanForKeyValue(this.namedEntityTestObjectDAO, name);
		Assertions.assertNull(testEntity, "Test entity with name " + name + " should be NULL");

		// Caches should NOT be empty, but wrapped a null value
		Assertions.assertNotNull(this.cacheHandler.get(clearCacheName, name), clearCacheName + " Cache should NOT be empty for this key " + name);
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, name).getObject(), clearCacheName + " Cache wrapped object should be null for this key " + name);

		// Insert the object
		testEntity = new NamedEntityTestObject();
		testEntity.setName(name);
		testEntity.setDescription("TEST DESCRIPTION");
		testEntity = this.namedEntityTestObjectDAO.save(testEntity);

		// Default Cache should NOT be empty after insert and should reference the inserted entity
		Assertions.assertNotNull(this.cacheHandler.get(defaultCacheName, name), defaultCacheName + " Cache should NOT be empty for this key " + name);
		Assertions.assertNotNull(this.cacheHandler.get(defaultCacheName, name).getObject(), defaultCacheName + " Cache wrapped object should NOT be empty for this key " + name);

		// Clear Cache should be empty after insert
		Assertions.assertNull(this.cacheHandler.get(clearCacheName, name), clearCacheName + " Cache should be empty for this key " + name);
	}
}
