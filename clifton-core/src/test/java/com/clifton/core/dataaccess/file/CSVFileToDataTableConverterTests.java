package com.clifton.core.dataaccess.file;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.AssertUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.File;


/**
 * @author manderson
 */
public class CSVFileToDataTableConverterTests {

	private FileToDataTableConverter fileToDataTableConverter = new CSVFileToDataTableConverter();
	private FileToDataTableConverter fileToDataTableConverter2 = new CSVFileToDataTableConverter(',', false);


	@Test
	public void testCSVFileConverter() {
		File testFile = new File(("src/test/java/com/clifton/core/dataaccess/file/CSV File Test.csv"));
		DataTable result = this.fileToDataTableConverter.convert(testFile);
		DataTable resultWithBlankLines = this.fileToDataTableConverter2.convert(testFile);

		AssertUtils.assertTrue(8 == result.getTotalRowCount(), "Expected a DataTable with 8 rows of data");
		AssertUtils.assertTrue(10 == resultWithBlankLines.getTotalRowCount(), "Expected a DataTable with 9 rows of data");

		validateRow(result.getRow(0), new Object[]{"Client Relationship", "123456", "Salesforce", "Client Relationship (Organization)", "2178"});
		validateRow(result.getRow(1), new Object[]{"Client Relationship", "The Gifford Foundation", "Salesforce", "Client Relationship (Organization)", ""});
		validateRow(result.getRow(2), new Object[]{"Client Relationship", "Research Affiliates \"RA\"", "Salesforce", "Client Relationship (Organization)", "2246"});
		validateRow(result.getRow(3), new Object[]{"Client Relationship", "New Mexico Educational Retirement Board", "Salesforce", "Client Relationship (Organization)", "3413"});
		validateRow(result.getRow(4), new Object[]{"Client Relationship", "Electrical Workers Southern California IBEW/NECA , Locals #11, #440, #441, #477", "Salesforce", "Client Relationship (Organization)", "3445"});
		validateRow(result.getRow(5), new Object[]{"Client Relationship", "Macy's, Inc.", "Salesforce", "Client Relationship (Organization)", "3440"});
		validateRow(result.getRow(6), new Object[]{"Client Relationship", "Gwinnett County Board of Education Retirement System", "Salesforce", "Client Relationship (Organization)", "2968"});
		validateRow(result.getRow(7), new Object[]{"Client Relationship", "", "Salesforce", "Client Relationship (Organization)", "3768"});
		validateRow(resultWithBlankLines.getRow(8), new Object[]{"", " ", " ", "   ", ""});
		validateRow(resultWithBlankLines.getRow(9), new Object[]{"", "", "", "", ""});
	}


	@Test
	public void testEmptyFileConversion() {
		File testFile = new File(("src/test/java/com/clifton/core/dataaccess/file/EmptyFileTest.csv"));
		DataTable result = this.fileToDataTableConverter.convert(testFile);
		AssertUtils.assertTrue(0 == result.getTotalRowCount(), "Expected a DataTable with 0 rows of data");
	}


	private void validateRow(DataRow row, Object[] expectedValues) {
		for (int i = 0; i < expectedValues.length; i++) {
			Object actualValue = row.getValue(i);
			Object expectedValue = expectedValues[i];
			Assertions.assertEquals(expectedValue, actualValue);
		}
	}
}
