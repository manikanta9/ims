package com.clifton.core.dataaccess.db;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.migrate.execution.CalendarMonth;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>PropertyNameConverterTests</code> class defines tests for testing {@link PropertyNameConverter} methods.
 *
 * @author vgomelsky
 */
public class PropertyNameConverterTests {

	private PropertyNameConverter monthConverter = new PropertyNameConverter(BeanUtils.getPropertyDataTypes(CalendarMonth.class), "CalendarMonth");


	@Test
	public void testConvert() {
		Assertions.assertEquals("id", this.monthConverter.convert("CalendarMonthID"));
		Assertions.assertEquals("name", this.monthConverter.convert("CalendarName"));
		Assertions.assertEquals("name", this.monthConverter.convert("NAME"));
	}


	@Test
	public void testConvertFieldException() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			this.monthConverter.convert("TotallyInvalidField");
		});
	}


	@Test
	public void testConvertIdFieldException() {
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			this.monthConverter.convert("TotallyInvalidFieldID");
		});
	}
}
