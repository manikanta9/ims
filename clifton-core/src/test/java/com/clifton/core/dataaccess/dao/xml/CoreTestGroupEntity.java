package com.clifton.core.dataaccess.dao.xml;


import com.clifton.core.beans.NamedEntity;

import java.math.BigDecimal;


/**
 * The <code>CoreTestGroupEntity</code> ...
 *
 * @author manderson
 */
public class CoreTestGroupEntity extends NamedEntity<Integer> {

	private BigDecimal bigDecimalField;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigDecimal getBigDecimalField() {
		return this.bigDecimalField;
	}


	public void setBigDecimalField(BigDecimal bigDecimalField) {
		this.bigDecimalField = bigDecimalField;
	}
}
