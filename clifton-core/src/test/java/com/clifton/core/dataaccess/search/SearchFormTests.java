package com.clifton.core.dataaccess.search;

import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;
import java.util.List;


public class SearchFormTests {

	@Test
	public void testGetSearchRestrictions() {
		Date startDate = new Date();
		String updateStartDate = DateUtils.fromDateShort(startDate);
		String updateEndDate = DateUtils.fromDateShort(DateUtils.addDays(startDate, 2));

		BaseEntitySearchForm searchForm = setupBaseSearchForm(null, null, (short) 0, (short) 0);
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.LESS_THAN, updateEndDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.LESS_THAN, updateEndDate));

		List<SearchRestriction> restrictions = searchForm.getSearchRestrictions("updateDate");
		AssertUtils.assertNotEmpty(restrictions, "No restrictions were found for field: updateDate");
		Assertions.assertEquals(2, restrictions.size());

		SearchRestriction restriction1 = restrictions.get(0);
		Assertions.assertEquals("updateDate", restriction1.getField());
		Assertions.assertEquals(ComparisonConditions.GREATER_THAN, restriction1.getComparison());
		Assertions.assertEquals(updateStartDate, restriction1.getValue());

		SearchRestriction restriction2 = restrictions.get(1);
		Assertions.assertEquals("updateDate", restriction2.getField());
		Assertions.assertEquals(ComparisonConditions.LESS_THAN, restriction2.getComparison());
		Assertions.assertEquals(updateEndDate, restriction2.getValue());
	}


	@Test
	public void testGetSearchRestrictionsMultiple() {
		Date startDate = new Date();
		String updateStartDate = DateUtils.fromDateShort(startDate);
		String updateEndDate = DateUtils.fromDateShort(DateUtils.addDays(startDate, 2));

		BaseEntitySearchForm searchForm = setupBaseSearchForm(null, null, (short) 0, (short) 0);
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.LESS_THAN, updateEndDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.LESS_THAN, updateEndDate));

		List<SearchRestriction> restrictions = searchForm.getSearchRestrictions("updateDate", "createDate");
		AssertUtils.assertNotEmpty(restrictions, "No restrictions were found for fields: {updateDate, createDate}");
		Assertions.assertEquals(4, restrictions.size());

		SearchRestriction restriction1 = restrictions.get(0);
		Assertions.assertEquals("updateDate", restriction1.getField());
		Assertions.assertEquals(ComparisonConditions.GREATER_THAN, restriction1.getComparison());
		Assertions.assertEquals(updateStartDate, restriction1.getValue());

		SearchRestriction restriction2 = restrictions.get(1);
		Assertions.assertEquals("updateDate", restriction2.getField());
		Assertions.assertEquals(ComparisonConditions.LESS_THAN, restriction2.getComparison());
		Assertions.assertEquals(updateEndDate, restriction2.getValue());

		SearchRestriction restriction3 = restrictions.get(2);
		Assertions.assertEquals("createDate", restriction3.getField());
		Assertions.assertEquals(ComparisonConditions.GREATER_THAN, restriction3.getComparison());
		Assertions.assertEquals(updateStartDate, restriction3.getValue());

		SearchRestriction restriction4 = restrictions.get(3);
		Assertions.assertEquals("createDate", restriction4.getField());
		Assertions.assertEquals(ComparisonConditions.LESS_THAN, restriction4.getComparison());
		Assertions.assertEquals(updateEndDate, restriction4.getValue());
	}


	@Test
	public void testGetSearchRestrictionsNoMatch() {
		Date startDate = new Date();
		String updateStartDate = DateUtils.fromDateShort(startDate);
		String updateEndDate = DateUtils.fromDateShort(DateUtils.addDays(startDate, 2));

		BaseEntitySearchForm searchForm = setupBaseSearchForm(null, null, (short) 0, (short) 0);
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.LESS_THAN, updateEndDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("createDate", ComparisonConditions.LESS_THAN, updateEndDate));

		List<SearchRestriction> restrictions = searchForm.getSearchRestrictions("createUserId");
		Assertions.assertEquals(0, restrictions.size());
	}


	@Test
	public void testIsSearchRestrictionSetAny() {
		Date startDate = new Date();
		String updateStartDate = DateUtils.fromDateShort(startDate);
		String updateEndDate = DateUtils.fromDateShort(DateUtils.addDays(startDate, 2));

		BaseEntitySearchForm searchForm = setupBaseSearchForm(startDate, null, (short) 0, null);
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.LESS_THAN, updateEndDate));

		Assertions.assertTrue(searchForm.isSearchRestrictionSetAny("updateDate"));
		Assertions.assertTrue(searchForm.isSearchRestrictionSetAny("createDate"));
		Assertions.assertTrue(searchForm.isSearchRestrictionSetAny("createDate", "updateDate", "createUserId"));
		Assertions.assertTrue(searchForm.isSearchRestrictionSetAny("createDate", "updateUserId"));
		Assertions.assertFalse(searchForm.isSearchRestrictionSetAny("updateUserId"));
	}


	@Test
	public void testIsSearchRestrictionSetAll() {
		Date startDate = new Date();
		String updateStartDate = DateUtils.fromDateShort(startDate);
		String updateEndDate = DateUtils.fromDateShort(DateUtils.addDays(startDate, 2));

		BaseEntitySearchForm searchForm = setupBaseSearchForm(startDate, null, (short) 0, null);
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.GREATER_THAN, updateStartDate));
		searchForm.addSearchRestriction(new SearchRestriction("updateDate", ComparisonConditions.LESS_THAN, updateEndDate));

		Assertions.assertTrue(searchForm.isSearchRestrictionSetAll("updateDate"));
		Assertions.assertTrue(searchForm.isSearchRestrictionSetAll("createDate"));
		Assertions.assertTrue(searchForm.isSearchRestrictionSetAll("createDate", "updateDate", "createUserId"));
		Assertions.assertFalse(searchForm.isSearchRestrictionSetAll("updateUserId"));
		Assertions.assertFalse(searchForm.isSearchRestrictionSetAll("createUserId", "updateUserId"));
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	private BaseEntitySearchForm setupBaseSearchForm(Date createDate, Date updateDate, Short createUserId, Short updateUserId) {
		BaseAuditableEntitySearchForm searchForm = new AuditableEntitySearchForm();
		searchForm.setCreateDate(createDate);
		searchForm.setUpdateDate(updateDate);
		searchForm.setCreateUserId(createUserId);
		searchForm.setUpdateUserId(updateUserId);
		return searchForm;
	}
}
