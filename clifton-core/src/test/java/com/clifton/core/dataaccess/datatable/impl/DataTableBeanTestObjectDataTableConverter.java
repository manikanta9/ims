package com.clifton.core.dataaccess.datatable.impl;

import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.BeanDataTable;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.shared.dataaccess.DataTypes;

import java.beans.PropertyDescriptor;
import java.util.Collection;
import java.util.List;


/**
 * {@link DataTableBeanTestObjectDataTableConverter} is a custom converter used to test initialization of converters from context.
 *
 * @author michaelm
 */
public class DataTableBeanTestObjectDataTableConverter extends BeanCollectionToDataTableConverter<DataTableBeanTestObject> {


	@Override
	public void validateConverterConfiguration() {
		// no custom validation for this converter
	}


	@Override
	public BeanDataTable<DataTableBeanTestObject> convert(Collection<DataTableBeanTestObject> runObjectDetailCollection) {
		BeanDataTable<DataTableBeanTestObject> table = new BeanDataTable<>(getDataColumnArrayForBeanCollection(runObjectDetailCollection));
		return table;
	}


	@Override
	protected DataTypes getDataType(String beanPropertyName) {
		PropertyDataTypeConverter dataTypeConverter = new PropertyDataTypeConverter();
		getPropertyDescriptor(beanPropertyName); // this is used to test NPE
		return null;
	}


	@Override
	protected Integer getSqlType(String beanPropertyName) {
		return null;
	}
}
