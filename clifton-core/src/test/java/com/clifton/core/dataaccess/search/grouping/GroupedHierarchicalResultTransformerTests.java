package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.dataaccess.search.grouping.transformer.GroupedHierarchicalResultTransformer;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


public class GroupedHierarchicalResultTransformerTests {


	@Test
	public void test_MissingGroupingPropertyList() {
		Exception e = Assertions.assertThrows(ValidationException.class, () -> new GroupedHierarchicalResultTransformer(null, null, null));
		Assertions.assertEquals("Grouping Search Form is required.", e.getMessage());

		e = Assertions.assertThrows(ValidationException.class, () -> new GroupedHierarchicalResultTransformer(new GroupingSearchForm() {
			@Override
			public List<GroupingProperty> getGroupingPropertyList() {
				return null;
			}


			@Override
			public List<GroupingAggregateProperty> getGroupingAggregatePropertyList() {
				return null;
			}
		}, null, null));
		Assertions.assertEquals("At least one property to group on is required.", e.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOneResult_OneLevel() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", 3});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(1, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 3\t"
				})
				.execute();
	}


	@Test
	public void testTwoResults_OneLevel() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", 3});
		tuples.add(new Object[]{"Value 2", 13});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(1, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 3\t",
						"Grouping Property: Property 1\tValue: Value 2\tLabel: null\tCount: 13\t"
				})
				.execute();
	}


	@Test
	public void testTwoResults_OneLevel_NullValue() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", 3});
		tuples.add(new Object[]{null, 13});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(1, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 3\t",
						"Grouping Property: Property 1	Value: null	Label: None	Count: 13\t"
				})
				.execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOneResult_TwoLevels() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", "Category 4", 7});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(2, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 7\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 4\tLabel: null\tCount: 7\t"
				})
				.execute();
	}


	@Test
	public void testThreeResults_TwoLevels() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", "Category 4", 7});
		tuples.add(new Object[]{"Value 1", "Category 5", 18});
		tuples.add(new Object[]{"Value 2", "Category 5", 2});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(2, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 25\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 4\tLabel: null\tCount: 7\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 5\tLabel: null\tCount: 18\t",
						"Grouping Property: Property 1\tValue: Value 2\tLabel: null\tCount: 2\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 5\tLabel: null\tCount: 2\t"
				})
				.execute();
	}


	@Test
	public void testThreeResults_TwoLevels_NullValue() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", "Category 4", 7});
		tuples.add(new Object[]{"Value 1", "Category 5", 18});
		tuples.add(new Object[]{"Value 2", null, 2});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(2, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 25\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 4\tLabel: null\tCount: 7\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 5\tLabel: null\tCount: 18\t",
						"Grouping Property: Property 1\tValue: Value 2\tLabel: null\tCount: 2\t\r\n" +
								"\tGrouping Property: Property 2\tValue: null\tLabel: None\tCount: 2\t"
				})
				.execute();
	}


	@Test
	public void testThreeResults_TwoLevels_CountAndSum() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", "Category 4", 7, 99.99});
		tuples.add(new Object[]{"Value 1", "Category 5", 18, 110.784});
		tuples.add(new Object[]{"Value 2", null, 2, 5.145});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyAndAggregatesWithMocking(2, new GroupingAggregateTypes[]{GroupingAggregateTypes.SUM}, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 25\tSUM Property 100: 210.774\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 4\tLabel: null\tCount: 7\tSUM Property 100: 99.99\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 5\tLabel: null\tCount: 18\tSUM Property 100: 110.784\t",
						"Grouping Property: Property 1\tValue: Value 2\tLabel: null\tCount: 2\tSUM Property 100: 5.145\t\r\n" +
								"\tGrouping Property: Property 2\tValue: null\tLabel: None\tCount: 2\tSUM Property 100: 5.145\t"
				})
				.execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testFourResults_ThreeLevels_NullValue() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"Value 1", "Category 4", "Sub-Category 10", 7});
		tuples.add(new Object[]{"Value 1", "Category 5", "Sub-Category 11", 18});
		tuples.add(new Object[]{"Value 2", null, "Sub-Category 10", 2});
		tuples.add(new Object[]{"Value 2", null, "Sub-Category 11", 4});
		GroupedHierarchicalResultTransformerTestExecutor.forGroupingPropertyCountWithMocking(3, tuples)
				.withExpectedResults(new String[]{
						"Grouping Property: Property 1\tValue: Value 1\tLabel: null\tCount: 25\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 4\tLabel: null\tCount: 7\t\r\n" +
								"\t\tGrouping Property: Property 3\tValue: Sub-Category 10\tLabel: null\tCount: 7\t\r\n" +
								"\tGrouping Property: Property 2\tValue: Category 5\tLabel: null\tCount: 18\t\r\n" +
								"\t\tGrouping Property: Property 3\tValue: Sub-Category 11\tLabel: null\tCount: 18\t",
						"Grouping Property: Property 1\tValue: Value 2\tLabel: null\tCount: 6\t\r\n" +
								"\tGrouping Property: Property 2\tValue: null\tLabel: None\tCount: 6\t\r\n" +
								"\t\tGrouping Property: Property 3\tValue: Sub-Category 10\tLabel: null\tCount: 2\t\r\n" +
								"\t\tGrouping Property: Property 3\tValue: Sub-Category 11\tLabel: null\tCount: 4\t"
				})
				.execute();
	}
}
