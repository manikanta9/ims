package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import org.junit.jupiter.api.Assertions;


/**
 * The <code>TestDaoEventObserver</code> class is a test class that verifies proper order of observers.
 *
 * @param <T>
 * @author vgomelsky
 */
public class TestDaoEventObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {

	@Override
	@SuppressWarnings("unused")
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		Assertions.assertEquals(getOrder(), getDaoEventContext().getBeforeCallCount(), "The order must be the same as before currentCallDepth");
		T originalBean = getOriginalBean(dao, bean);
		Assertions.assertNotNull(originalBean, "original bean cannot be null");
	}


	@Override
	@SuppressWarnings("unused")
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// The order will match the afterMethodCallImpl because the after call count is incremented at prior to this being invoked in BaseDaoEventObserver.
		Assertions.assertEquals(getOrder(), getDaoEventContext().getAfterCallCount(), "The order must be the same as after currentCallDepth");
		T originalBean = getOriginalBean(dao, bean);
		Assertions.assertNotNull(originalBean, "original bean cannot be null");
	}
}
