package com.clifton.core.dataaccess.db;


import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>MSSQLDataTypeConverterTests</code> class contains tests for testing <code>MSSQLDataTypeConverter</code>
 *
 * @author vgomelsky
 */
public class MSSQLDataTypeConverterTests {

	@Test
	public void testAll() {
		MSSQLDataTypeConverter converter = new MSSQLDataTypeConverter();
		for (DataTypes dataType : DataTypes.values()) {
			DataTypeNames dataTypeName = dataType.getTypeName();
			if (dataTypeName != null) {
				if (dataTypeName.isSupportEncoding()) {
					for (DataTypeEncodingTypeNames encodingTypeName : DataTypeEncodingTypeNames.values()) {
						validateSqlTypeConversion(converter, dataType, encodingTypeName);
					}
				}
				else {
					validateSqlTypeConversion(converter, dataType, null);
				}
			}
		}
	}


	@Test
	public void testStringEncoding() {
		MSSQLDataTypeConverter converter = new MSSQLDataTypeConverter();
		String result = validateSqlTypeConversion(converter, DataTypes.NAME, DataTypeEncodingTypeNames.UCS_2);
		Assertions.assertEquals(result, "NVARCHAR(50)");

		result = validateSqlTypeConversion(converter, DataTypes.NAME, DataTypeEncodingTypeNames.UTF_8);
		Assertions.assertEquals(result, "VARCHAR(50) COLLATE LATIN1_GENERAL_100_CI_AS_SC_UTF8");
	}


	@Test
	public void testStringEncoding_Missing() {
		MSSQLDataTypeConverter converter = new MSSQLDataTypeConverter();
		Assertions.assertThrows(IllegalArgumentException.class, () -> {
			validateSqlTypeConversion(converter, DataTypes.NAME, null);
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String validateSqlTypeConversion(MSSQLDataTypeConverter converter, DataTypes dataType, DataTypeEncodingTypeNames encodingTypeName) {
		String sqlType = converter.convert(dataType, encodingTypeName);
		Assertions.assertNotNull(sqlType, "Can't convert data type: " + dataType + (encodingTypeName != null ? " with encoding type " + encodingTypeName : ""));
		return sqlType;
	}
}
