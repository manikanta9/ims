package com.clifton.core.dataaccess.file;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.datatable.impl.BeanCollectionToDataTableConverterTests;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.io.File;
import java.sql.Types;


/**
 * The <code>DataTableConverterTests</code> ...
 *
 * @author manderson
 */
public abstract class DataTableToFileConverterTestBase {

	public static final DataTable NULL_DATA_TABLE = null;
	public static final DataColumn[] DATA_COLUMN_LIST = new DataColumn[5];
	public static final DataTable EMPTY_DATA_TABLE;
	public static final DataTable DATA_TABLE;


	static {
		DATA_COLUMN_LIST[0] = new DataColumnImpl("Name", Types.NVARCHAR);
		DATA_COLUMN_LIST[1] = new DataColumnImpl("Description", Types.NVARCHAR);
		DATA_COLUMN_LIST[2] = new DataColumnImpl("Create Date", Types.DATE);
		DATA_COLUMN_LIST[3] = new DataColumnImpl("Amount", Types.DECIMAL);
		DATA_COLUMN_LIST[4] = new DataColumnImpl("Quantity", Types.INTEGER);
	}


	static {
		EMPTY_DATA_TABLE = new PagingDataTableImpl(DATA_COLUMN_LIST);
	}


	static {
		DATA_TABLE = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(DATA_TABLE, new Object[]{"Test One", "Test for Row 1", DateUtils.toDate("01/01/2010"), 5.50, null});
		DATA_TABLE.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Two", null, DateUtils.toDate("02/01/2010"), 5, 5});
		DATA_TABLE.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Three", "Test for Row 3", null, 17.798, 7});
		DATA_TABLE.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Four", "Test for Row 4", DateUtils.toDate("03/30/2010"), null, 10});
		DATA_TABLE.addRow(row);
	}


	@Resource
	private ApplicationContextService applicationContextService;


	public abstract DataTableToFileConverter getDataTableToFileConverter();


	public abstract void validateFile(DataTable dataTable, File file);


	@Test
	public void testDataTableConversion() {
		convert(NULL_DATA_TABLE);
		convert(EMPTY_DATA_TABLE);
		convert(DATA_TABLE);

		String columns = "Name:name:fontColor=red|Description:description:width=200|Create Date:createDate:align=center,width=50";
		BeanCollectionToDataTableConverter<NamedEntityTestObject> converter = new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(this.applicationContextService, NamedEntityTestObject.class, columns));
		DataTable dataTable = converter.convert(BeanCollectionToDataTableConverterTests.namedEntityList);
		convert(dataTable);
	}


	private void convert(DataTable dataTable) {
		File file = null;
		try {
			DataTableFileConfig config = new DataTableFileConfig(dataTable);
			file = getDataTableToFileConverter().convert(config);
			if (config.getDataTable() == null || config.isBlank()) {
				Assertions.assertNull(file);
				return;
			}
			validateFile(dataTable, file);
		}
		finally {
			if (file != null) {
				file.delete();
			}
		}
	}
}
