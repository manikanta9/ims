package com.clifton.core.dataaccess.file;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>DataTableToExcelFileConverterTests</code> ...
 *
 * @author manderson
 */
public class DataTableToExcelFileConverterTests extends DataTableToFileConverterTestBase {

	private DataTableToFileConverter dataTableToFileConverter = new DataTableToExcelFileConverter();
	private FileToDataTableConverter fileToDataTableConverter = new ExcelFileToDataTableConverter();


	@Override
	public DataTableToFileConverter getDataTableToFileConverter() {
		return this.dataTableToFileConverter;
	}


	@Override
	public void validateFile(DataTable dataTable, File file) {
		DataTable convertedDataTable = this.fileToDataTableConverter.convert(file);
		// If both are NULL then OK
		Assertions.assertNotNull(convertedDataTable);
		if (dataTable == null) {
			Assertions.assertEquals(0, convertedDataTable.getColumnCount());
			Assertions.assertEquals(0, convertedDataTable.getTotalRowCount());
			return;
		}

		Assertions.assertEquals(dataTable.getColumnCount(), convertedDataTable.getColumnCount());
		Assertions.assertEquals(dataTable.getTotalRowCount(), convertedDataTable.getTotalRowCount());

		for (int i = 0; i < dataTable.getColumnCount(); i++) {
			DataColumn originalColumn = dataTable.getColumnList()[i];
			DataColumn convertedColumn = convertedDataTable.getColumnList()[i];
			Assertions.assertEquals(originalColumn.getColumnName(), convertedColumn.getColumnName());
		}

		for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
			DataRow originalRow = dataTable.getRow(i);
			DataRow convertedRow = convertedDataTable.getRow(i);

			for (int j = 0; j < dataTable.getColumnCount(); j++) {
				Object originalValue = originalRow.getValue(j);
				Object convertedValue = convertedRow.getValue(j);
				if (originalValue != null && convertedValue != null && !originalValue.getClass().equals(convertedValue.getClass())) {
					if (String.class.equals(convertedValue.getClass())) {
						if (Date.class.equals(originalValue.getClass())) {
							convertedValue = DateUtils.toDate((String) convertedValue);
						}
						else if (Integer.class.equals(originalValue.getClass())) {
							convertedValue = new Double(((String) convertedValue)).intValue();
						}
						else if (Double.class.equals(originalValue.getClass())) {
							convertedValue = new Double(((String) convertedValue));
						}
						else if (BigDecimal.class.equals(originalValue.getClass())) {
							convertedValue = new BigDecimal(((String) convertedValue));
						}
						else {
							originalValue = "" + originalValue;
						}
					}
				}
				Assertions.assertTrue(CompareUtils.compare(originalValue, convertedValue) == 0, "These objects should be equal, but weren't: [" + originalValue + "] [" + convertedValue + "]");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Can be used to test the performance the file conversion to find issues with auto sizing columns to determine when auto sizing affects performance too much
	 * Can run this on demand to check performance - but it's not enabled by default
	 * Anything over 2 seconds will be printed in the console with the results
	 */
	@Disabled
	@Test
	public void testPerformanceOfFileGeneration() {
		convertDataTableToExcel(10000, 10);
		convertDataTableToExcel(7000, 25);
		convertDataTableToExcel(7000, 10);
		convertDataTableToExcel(5000, 10);
		convertDataTableToExcel(5000, 25);
		convertDataTableToExcel(3000, 10);
		convertDataTableToExcel(3000, 50);
		convertDataTableToExcel(1000, 10);
		convertDataTableToExcel(1000, 35);
		convertDataTableToExcel(500, 50);
		convertDataTableToExcel(50, 10);
		convertDataTableToExcel(50, 100);
	}


	private void convertDataTableToExcel(int rowCount, int columnCount) {
		DataTable dataTable = generateTestDataTable(rowCount, columnCount);
		DataTableFileConfig config = new DataTableFileConfig(dataTable);

		long start = System.currentTimeMillis();
		getDataTableToFileConverter().convert(config);
		long end = System.currentTimeMillis();
		if (((end - start) / 1000) > 2) {
			System.out.println("Time to convert data table with " + rowCount + " rows and " + columnCount + " columns: " + DateUtils.getTimeDifference(end - start, true));
		}
	}


	private DataTable generateTestDataTable(int rowCount, int columnCount) {
		List<DataColumn> columnList = new ArrayList<>();
		for (int i = 0; i < columnCount; i += 5) {
			columnList.add(new DataColumnImpl("Name" + i, Types.NVARCHAR));
			columnList.add(new DataColumnImpl("Description" + i, Types.NVARCHAR));
			columnList.add(new DataColumnImpl("Create Date" + i, Types.DATE));
			columnList.add(new DataColumnImpl("Amount" + i, Types.DECIMAL));
			columnList.add(new DataColumnImpl("Quantity" + i, Types.INTEGER));
		}

		DataTable dataTable = new PagingDataTableImpl(columnList.toArray(new DataColumn[columnCount]));

		for (int i = 0; i < rowCount; i++) {
			List<Object> valueList = new ArrayList<>();
			for (int c = 0; c < columnCount; c += 5) {
				valueList.add("Test " + i);
				valueList.add("Test for Row " + i);
				valueList.add(DateUtils.addDays(DateUtils.toDate("01/01/2010"), i));
				valueList.add(10.75 + i);
				valueList.add(i);
			}
			dataTable.addRow(new DataRowImpl(dataTable, valueList.toArray()));
		}
		return dataTable;
	}
}
