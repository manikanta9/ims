package com.clifton.core.dataaccess.sql;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;


/**
 * @author nickk
 */
public class SqlCommandTests {

	@Test
	public void testSelectCommandWithSqlRestriction() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE SomeID IN (1, 2)", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandWithShortRestriction() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addShortRestriction((short) 1, "SomeID = ?");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE SomeID = ?\nWITH PARAMS [[1]]", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandWithIntegerRestriction() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addIntegerRestriction(1, "SomeID = ?");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE SomeID = ?\nWITH PARAMS [[1]]", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandWithLongRestriction() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addLongRestriction(1L, "SomeID = ?");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE SomeID = ?\nWITH PARAMS [[1]]", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandDateRestriction() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addDateRestriction(DateUtils.toDate("01/27/2019"), "SomeDate >= ?");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE SomeDate >= ?\nWITH PARAMS [[2019-01-27]]", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandBooleanRestriction() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addBooleanRestriction(Boolean.TRUE, "Delete = ?");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE Delete = ?\nWITH PARAMS [[true]]", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandWithMultipleWhereRestrictions() {
		SqlSelectCommand command = new SqlSelectCommand();
		command.setSelectClause(" * FROM SomeTable");
		command.addBooleanRestriction(Boolean.TRUE, "Delete = ?");
		command.addIntegerRestriction(1, "SomeID = ?");
		command.addDateRestriction(DateUtils.toDate("01/27/2019"), " AND SomeDate >= ?");
		command.addDateRestriction(DateUtils.toDate("01/29/2019"), "SomeOtherDate <= ?");
		Assertions.assertEquals("SELECT  * FROM SomeTable\nWHERE Delete = ? AND SomeID = ? AND SomeDate >= ? AND SomeOtherDate <= ?\nWITH PARAMS [[true, 1, 2019-01-27, 2019-01-29]]", command.getSqlWithParameters());
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_multiple_constructor_args() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA2\nWHERE SomeID IN (3, 4)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_list_constructor_arg() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		List<SqlSelectCommand> sqlSelectCommandList = CollectionUtils.createList(command1, command2, command3);

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(sqlSelectCommandList);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA2\nWHERE SomeID IN (3, 4)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_getSqlSelectCommand() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA2\nWHERE SomeID IN (3, 4)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		SqlSelectCommand sqlSelectCommand = sqlSelectCommandWithUnionAll.getSqlSelectCommand();
		Assertions.assertEquals(expectedSql, sqlSelectCommand.getSql());
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_removeSqlSelectCommand_index_in_range() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);
		sqlSelectCommandWithUnionAll.removeSqlSelectCommand(1);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect__removeSqlSelectCommand_index_out_of_range_positive_value() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);
		sqlSelectCommandWithUnionAll.removeSqlSelectCommand(4);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA2\nWHERE SomeID IN (3, 4)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect__removeSqlSelectCommand_index_out_of_range_negative_value() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);
		sqlSelectCommandWithUnionAll.removeSqlSelectCommand(-4);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA2\nWHERE SomeID IN (3, 4)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect__addSqlSelectCommand() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		List<SqlSelectCommand> sqlSelectCommandList = CollectionUtils.createList(command1, command2);

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(sqlSelectCommandList);
		sqlSelectCommandWithUnionAll.addSqlSelectCommand(command3);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)\nUNION ALL\nSELECT  * FROM tableA2\nWHERE SomeID IN (3, 4)\nUNION ALL\nSELECT  * FROM tableA3\nWHERE SomeID IN (5, 6)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAll_singleCommand() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		List<SqlSelectCommand> sqlSelectCommandList = CollectionUtils.createList(command1);

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(sqlSelectCommandList);

		String expectedSql = "SELECT  * FROM tableA1\nWHERE SomeID IN (1, 2)";
		String actualSql = sqlSelectCommandWithUnionAll.getSql();
		Assertions.assertEquals(expectedSql, actualSql);
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_getSize() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);
		Assertions.assertEquals(3, sqlSelectCommandWithUnionAll.getSize());
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_combined_SqlParameterValues() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");
		command1.addIntegerRestriction(100, "ID1 = ?");
		command1.addIntegerRestriction(110, "ID2 = ?");

		SqlSelectCommand command2 = new SqlSelectCommand();
		command2.setSelectClause(" * FROM tableA2");
		command2.addSqlRestriction("SomeID IN (", String.join(", ", "3", "4"), ")");
		command2.addIntegerRestriction(200, "ID1 = ?");
		command2.addIntegerRestriction(210, "ID2 = ?");

		SqlSelectCommand command3 = new SqlSelectCommand();
		command3.setSelectClause(" * FROM tableA3");
		command3.addSqlRestriction("SomeID IN (", String.join(", ", "5", "6"), ")");
		command2.addIntegerRestriction(300, "ID1 = ?");
		command2.addIntegerRestriction(310, "ID2 = ?");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1, command2, command3);
		List<SqlParameterValue> expectedSqlParameterValueList = CollectionUtils.combineCollections(command1.getSqlParameterValues(), command2.getSqlParameterValues(), command3.getSqlParameterValues());

		Assertions.assertArrayEquals(expectedSqlParameterValueList.toArray(), sqlSelectCommandWithUnionAll.getSqlSelectCommand().getSqlParameterValues().toArray());
	}


	@Test
	public void testSelectCommandUnionAllWithMultipleSqlSelect_combined_SqlParameterValues_single_command() {
		SqlSelectCommand command1 = new SqlSelectCommand();
		command1.setSelectClause(" * FROM tableA1");
		command1.addSqlRestriction("SomeID IN (", String.join(", ", "1", "2"), ")");
		command1.addIntegerRestriction(100, "ID1 = ?");
		command1.addIntegerRestriction(110, "ID2 = ?");

		SqlSelectCommandUnionAll sqlSelectCommandWithUnionAll = SqlSelectCommandUnionAll.of(command1);
		List<SqlParameterValue> expectedSqlParameterValueList = CollectionUtils.combineCollections(command1.getSqlParameterValues());

		Assertions.assertArrayEquals(expectedSqlParameterValueList.toArray(), sqlSelectCommandWithUnionAll.getSqlSelectCommand().getSqlParameterValues().toArray());
	}
}
