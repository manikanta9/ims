package com.clifton.core.dataaccess.migrate.execution;


import com.clifton.core.beans.BaseEntity;

import java.util.Date;


/**
 * The <code>CalendarDay</code> is a test DTO with Long primary key that should result in BIGINT DB data type.
 *
 * @author vgomelsky
 */
public class CalendarDay extends BaseEntity<Long> {

	private int calendarYearId;
	private int calendarMonthId;
	private Date date;
	private boolean monthEnd;
	private boolean quarterEnd;
	private boolean yearEnd;

	private CalendarYear year;
	private CalendarMonth month;

	/**
	 * Used to test calculated column in migrations
	 * = DATENAME(WEEKDAY, date) + ' ' + DATENAME(MONTH, date) + ' ' + DATENAME(DAY, date) + ', ' + DATENAME(YEAR, date)
	 */
	private String dateFullString;


	/**
	 * @return the monthEnd
	 */
	public boolean isMonthEnd() {
		return this.monthEnd;
	}


	/**
	 * @param monthEnd the monthEnd to set
	 */
	public void setMonthEnd(boolean monthEnd) {
		this.monthEnd = monthEnd;
	}


	/**
	 * @return the quarterEnd
	 */
	public boolean isQuarterEnd() {
		return this.quarterEnd;
	}


	/**
	 * @param quarterEnd the quarterEnd to set
	 */
	public void setQuarterEnd(boolean quarterEnd) {
		this.quarterEnd = quarterEnd;
	}


	/**
	 * @return the yearEnd
	 */
	public boolean isYearEnd() {
		return this.yearEnd;
	}


	/**
	 * @param yearEnd the yearEnd to set
	 */
	public void setYearEnd(boolean yearEnd) {
		this.yearEnd = yearEnd;
	}


	/**
	 * @return the calendarYearId
	 */
	public int getCalendarYearId() {
		return this.calendarYearId;
	}


	/**
	 * @param calendarYearId the calendarYearId to set
	 */
	public void setCalendarYearId(int calendarYearId) {
		this.calendarYearId = calendarYearId;
	}


	/**
	 * @return the calendarMonthId
	 */
	public int getCalendarMonthId() {
		return this.calendarMonthId;
	}


	/**
	 * @param calendarMonthId the calendarMonthId to set
	 */
	public void setCalendarMonthId(int calendarMonthId) {
		this.calendarMonthId = calendarMonthId;
	}


	/**
	 * @return the date
	 */
	public Date getDate() {
		return this.date;
	}


	/**
	 * @param date the date to set
	 */
	public void setDate(Date date) {
		this.date = date;
	}


	public CalendarYear getYear() {
		return this.year;
	}


	public void setYear(CalendarYear year) {
		this.year = year;
	}


	public CalendarMonth getMonth() {
		return this.month;
	}


	public void setMonth(CalendarMonth month) {
		this.month = month;
	}


	public String getDateFullString() {
		return this.dateFullString;
	}


	public void setDateFullString(String dateFullString) {
		this.dateFullString = dateFullString;
	}
}
