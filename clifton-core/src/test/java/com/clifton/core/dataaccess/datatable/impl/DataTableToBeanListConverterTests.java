package com.clifton.core.dataaccess.datatable.impl;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.DataTableToBeanListConverter;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.List;


public class DataTableToBeanListConverterTests {

	public static final DataTable NULL_DATA_TABLE = null;

	public static final DataColumn[] DATA_COLUMN_LIST = new DataColumn[5];


	static {
		DATA_COLUMN_LIST[0] = new DataColumnImpl("Name", java.sql.Types.NVARCHAR);
		DATA_COLUMN_LIST[1] = new DataColumnImpl("Description", java.sql.Types.NVARCHAR);
		DATA_COLUMN_LIST[2] = new DataColumnImpl("CreateDate", java.sql.Types.DATE);
		DATA_COLUMN_LIST[3] = new DataColumnImpl("Amount", java.sql.Types.DECIMAL);
		DATA_COLUMN_LIST[4] = new DataColumnImpl("Quantity", java.sql.Types.INTEGER);
	}


	public static final DataTable EMPTY_DATA_TABLE;


	static {
		EMPTY_DATA_TABLE = new PagingDataTableImpl(DATA_COLUMN_LIST);
	}


	public static final DataTable DATA_TABLE;


	static {
		DATA_TABLE = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(DATA_TABLE, new Object[]{"Test One", "Test for Row 1", DateUtils.toDate("01/01/2010"), 5.50, null});
		DATA_TABLE.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Two", null, DateUtils.toDate("02/01/2010"), 5, 5});
		DATA_TABLE.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Three", "Test for Row 3", null, 17.798, 7});
		DATA_TABLE.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Four", "Test for Row 4", DateUtils.toDate("03/30/2010"), null, 10});
		DATA_TABLE.addRow(row);
	}


	public static final DataColumn[] DATA_COLUMN_LIST_2 = new DataColumn[5];


	static {
		DATA_COLUMN_LIST[0] = new DataColumnImpl("Name", java.sql.Types.NVARCHAR);
		DATA_COLUMN_LIST[1] = new DataColumnImpl("Description", java.sql.Types.NVARCHAR);
		DATA_COLUMN_LIST[2] = new DataColumnImpl("TradeDate", java.sql.Types.DATE);
		DATA_COLUMN_LIST[3] = new DataColumnImpl("Amount", java.sql.Types.DECIMAL);
		DATA_COLUMN_LIST[4] = new DataColumnImpl("Quantity", java.sql.Types.INTEGER);
	}


	public static final DataTable DATA_TABLE_2;


	static {
		DATA_TABLE_2 = new PagingDataTableImpl(DATA_COLUMN_LIST);
		DataRow row = new DataRowImpl(DATA_TABLE, new Object[]{"Test One", "Test for Row 1", DateUtils.toDate("01/01/2010"), 5.50, null});
		DATA_TABLE_2.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Two", null, DateUtils.toDate("02/01/2010"), 5, 5});
		DATA_TABLE_2.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Three", "Test for Row 3", null, 17.798, 7});
		DATA_TABLE_2.addRow(row);
		row = new DataRowImpl(DATA_TABLE, new Object[]{"Test Four", "Test for Row 4", DateUtils.toDate("03/30/2010"), null, 10});
		DATA_TABLE_2.addRow(row);
	}


	/////////////////////////////////////////////////////
	/////////////////////////////////////////////////////


	@Test
	public void testEmptyDataTable() {
		DataTableToBeanListConverter<NamedEntityTestObject> converter = new DataTableToBeanListConverter<>(NamedEntityTestObject.class);
		List<NamedEntityTestObject> list = converter.convert(NULL_DATA_TABLE);
		AssertUtils.assertTrue(CollectionUtils.getSize(list) == 0, "Expected nothing returned for null DataTable");

		list = converter.convert(EMPTY_DATA_TABLE);
		AssertUtils.assertTrue(CollectionUtils.getSize(list) == 0, "Expected nothing returned for empty DataTable");
	}


	@Test
	public void testDataTable_MissingRequiredColumns() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			DataTableToBeanListConverter<NamedEntityTestObject> converter = new DataTableToBeanListConverter<>(NamedEntityTestObject.class, new String[]{"name", "label"});
			converter.convert(DATA_TABLE);
		});
		Assertions.assertEquals("Missing Required Columns: Label", validationException.getMessage());
	}


	@Test
	public void testDataTable_InvalidRequiredColumns() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			DataTableToBeanListConverter<NamedEntityTestObject> converter = new DataTableToBeanListConverter<>(NamedEntityTestObject.class, new String[]{"name", "label", "address", "phoneNumber"});
			converter.convert(DATA_TABLE);
		});
		Assertions.assertEquals("Invalid Setup. The following properties are required but are not available: [address, phoneNumber], Available Properties [name, description, label]", runtimeException.getMessage());
	}


	@Test
	public void testDataTable() {
		DataTableToBeanListConverter<NamedEntityTestObject> converter = new DataTableToBeanListConverter<>(NamedEntityTestObject.class);

		// Validate Data Returned (Skips create date, amount, quantity as they are system managed or don't exist on the bean)
		List<NamedEntityTestObject> list = converter.convert(DATA_TABLE);
		AssertUtils.assertTrue(CollectionUtils.getSize(list) == 4, "Expected 4 beans returned 4 rows in the DataTable");

		for (NamedEntityTestObject bean : CollectionUtils.getIterable(list)) {
			if ("Test One".equals(bean.getName())) {
				Assertions.assertEquals("Test for Row 1", bean.getDescription());
				Assertions.assertNull(bean.getCreateDate());
				// Create Date is skipped as system managed field
				// No Amount or Quantity Property
			}
			else if ("Test Two".equals(bean.getName())) {
				Assertions.assertNull(bean.getDescription());
				Assertions.assertNull(bean.getCreateDate());
				// Create Date is skipped as sytem managed field
				// No Amount or Quantity Property

			}
			else if ("Test Three".equals(bean.getName())) {
				Assertions.assertEquals("Test for Row 3", bean.getDescription());
				Assertions.assertNull(bean.getCreateDate());
				// Create Date is skipped as sytem managed field
				// No Amount or Quantity Property

			}
			else if ("Test Four".equals(bean.getName())) {
				Assertions.assertEquals("Test for Row 4", bean.getDescription());
				Assertions.assertNull(bean.getCreateDate());
				// No Amount or Quantity Property
			}
			else {
				throw new RuntimeException("Unexpected Bean with name [" + bean.getName() + "].");
			}
		}
	}


	@Test
	public void testDataTable_MissingRequiredData() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			DataTableToBeanListConverter<NamedEntityTestObject> converter = new DataTableToBeanListConverter<>(NamedEntityTestObject.class, new String[]{"name", "description"});
			converter.convert(DATA_TABLE);
		});
		Assertions.assertEquals("Missing Required Value for row # [2] and Column [Description]", validationException.getMessage());
	}


	@Test
	public void testDataTable2() {
		DataTableToBeanListConverter<DataTableBeanTestObject> converter = new DataTableToBeanListConverter<>(DataTableBeanTestObject.class);

		// Validate Data Returned (Skips create date, amount, quantity as they are system managed or don't exist on the bean)
		List<DataTableBeanTestObject> list = converter.convert(DATA_TABLE_2);
		AssertUtils.assertTrue(CollectionUtils.getSize(list) == 4, "Expected 4 beans returned 4 rows in the DataTable");

		for (DataTableBeanTestObject bean : CollectionUtils.getIterable(list)) {
			if ("Test One".equals(bean.getName())) {
				Assertions.assertEquals("Test for Row 1", bean.getDescription());
				Assertions.assertEquals(DateUtils.toDate("01/01/2010"), bean.getTradeDate());
				Assertions.assertEquals(BigDecimal.valueOf(5.50), bean.getAmount());
				Assertions.assertNull(bean.getQuantity());
			}
			else if ("Test Two".equals(bean.getName())) {
				Assertions.assertNull(bean.getDescription());
				Assertions.assertEquals(DateUtils.toDate("02/01/2010"), bean.getTradeDate());
				Assertions.assertEquals(BigDecimal.valueOf(5), bean.getAmount());
				Assertions.assertEquals((Integer) 5, bean.getQuantity());
			}
			else if ("Test Three".equals(bean.getName())) {
				Assertions.assertEquals("Test for Row 3", bean.getDescription());
				Assertions.assertNull(bean.getTradeDate());
				Assertions.assertEquals(BigDecimal.valueOf(17.798), bean.getAmount());
				Assertions.assertEquals((Integer) 7, bean.getQuantity());
			}
			else if ("Test Four".equals(bean.getName())) {
				Assertions.assertEquals("Test for Row 4", bean.getDescription());
				Assertions.assertEquals(DateUtils.toDate("03/30/2010"), bean.getTradeDate());
				Assertions.assertNull(bean.getAmount());
				Assertions.assertEquals((Integer) 10, bean.getQuantity());
			}
			else {
				throw new RuntimeException("Unexpected Bean with name [" + bean.getName() + "].");
			}
		}
	}
}
