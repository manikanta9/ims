package com.clifton.core.dataaccess.search;

import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author vgomelsky
 */
public class SearchUtilsTests {


	@Test
	public void testValidateRequiredFilter() {
		BaseAuditableEntitySearchForm searchForm = new AuditableEntitySearchForm();
		searchForm.setUpdateUserId((short) 1);
		SearchUtils.validateRequiredFilter(searchForm, "createUserId");

		searchForm = new AuditableEntitySearchForm();
		searchForm.addSearchRestriction(new SearchRestriction("updateUserId", ComparisonConditions.EQUALS, 1));
		SearchUtils.validateRequiredFilter(searchForm, "createUserId");
	}


	@Test
	public void testValidateRequiredFilter_Nothing() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BaseAuditableEntitySearchForm searchForm = new AuditableEntitySearchForm();
			SearchUtils.validateRequiredFilter(searchForm, "createUserId");
		});
	}


	@Test
	public void testValidateRequiredFilter_OnlyIgnore() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BaseAuditableEntitySearchForm searchForm = new AuditableEntitySearchForm();
			searchForm.setCreateUserId((short) 1);
			SearchUtils.validateRequiredFilter(searchForm, "createUserId");
		});
	}
}
