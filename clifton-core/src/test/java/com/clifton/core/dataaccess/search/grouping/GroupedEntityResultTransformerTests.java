package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.dao.xml.CoreTestEntity;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.dataaccess.search.grouping.transformer.GroupedEntityResultTransformer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class GroupedEntityResultTransformerTests {

	@Resource
	private DaoLocator daoLocator;

	/**
	 * Since we are mocking the tuple results it's not exactly pulling the data from that table into the results, however the dao config for FK lookups are used.
	 */

	@Resource
	private XmlReadOnlyDAO<CoreTestEntity> coreTestEntityDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void test_MissingGroupingPropertyList() {
		Exception e = Assertions.assertThrows(ValidationException.class, () -> new GroupedEntityResultTransformer<>(null, null, null));
		Assertions.assertEquals("Grouping Search Form is required.", e.getMessage());

		e = Assertions.assertThrows(ValidationException.class, () -> new GroupedEntityResultTransformer<>(new GroupingSearchForm() {
			@Override
			public List<GroupingProperty> getGroupingPropertyList() {
				return null;
			}


			@Override
			public List<GroupingAggregateProperty> getGroupingAggregatePropertyList() {
				return null;
			}
		}, null, null));
		Assertions.assertEquals("At least one property to group on is required.", e.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOneResult_OneLevel() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{2, 1});

		GroupedEntityResultTransformerTestExecutor.forGroupingPropertyList(CollectionUtils.createList(buildGroupingProperty_GroupId()), tuples, this.daoLocator, this.coreTestEntityDAO.getConfiguration())
				.withExpectedResults("Group: Group 2\tCount: 1\t")
				.execute();
	}


	@Test
	public void testTwoResults_OneLevel() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{1, 3});
		tuples.add(new Object[]{2, 1});

		GroupedEntityResultTransformerTestExecutor.forGroupingPropertyList(CollectionUtils.createList(buildGroupingProperty_GroupId()), tuples, this.daoLocator, this.coreTestEntityDAO.getConfiguration())
				.withExpectedResults("Group: Group 1\tCount: 3\t",
						"Group: Group 2\tCount: 1\t")
				.execute();
	}


	@Test
	public void testTwoResults_OneLevel_NullValue() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{"test description", 3});
		tuples.add(new Object[]{null, 1});

		GroupedEntityResultTransformerTestExecutor.forGroupingPropertyList(CollectionUtils.createList(buildGroupingProperty_Description()), tuples, this.daoLocator, this.coreTestEntityDAO.getConfiguration())
				.withExpectedResults("Description: test description\tCount: 3\t",
						"Description: null\tCount: 1\t"
				)
				.execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testThreeResults_TwoLevels() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{1, null, 1});
		tuples.add(new Object[]{1, "Test Description", 2});
		tuples.add(new Object[]{2, "test description", 1});
		GroupedEntityResultTransformerTestExecutor.forGroupingPropertyList(CollectionUtils.createList(buildGroupingProperty_GroupId(), buildGroupingProperty_Description()), tuples, this.daoLocator, this.coreTestEntityDAO.getConfiguration())
				.withExpectedResults("Group: Group 1\tDescription: null\tCount: 1\t",
						"Group: Group 1\tDescription: Test Description\tCount: 2\t",
						"Group: Group 2\tDescription: test description\tCount: 1\t")
				.execute();
	}


	@Test
	public void testThreeResults_TwoLevels_CountAndSum() {
		List<Object[]> tuples = new ArrayList<>();
		tuples.add(new Object[]{1, null, 1, 99.99});
		tuples.add(new Object[]{1, "Test Description", 2, 110.784});
		tuples.add(new Object[]{2, "test Description", 1, 5.145});

		GroupingAggregateProperty groupingAggregateProperty = new GroupingAggregateProperty();
		groupingAggregateProperty.setAggregateLabel("Sum");
		groupingAggregateProperty.setAggregateType(GroupingAggregateTypes.SUM);

		GroupedEntityResultTransformerTestExecutor.forGroupingPropertyListAndAggregateList(CollectionUtils.createList(buildGroupingProperty_GroupId(), buildGroupingProperty_Description()), CollectionUtils.createList(groupingAggregateProperty), tuples, this.daoLocator, this.coreTestEntityDAO.getConfiguration())
				.withExpectedResults("Group: Group 1\tDescription: null\tCount: 1\tSum: 99.99\t",
						"Group: Group 1\tDescription: Test Description\tCount: 2\tSum: 110.784\t",
						"Group: Group 2\tDescription: test Description\tCount: 1\tSum: 5.145\t")
				.execute();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private GroupingProperty buildGroupingProperty_GroupId() {
		return buildGroupingProperty("group.id", "Group");
	}


	private GroupingProperty buildGroupingProperty_Description() {
		return buildGroupingProperty("description", "Description");
	}


	private GroupingProperty buildGroupingProperty(String beanPropertyName, String label) {
		GroupingProperty groupingProperty = new GroupingProperty();
		groupingProperty.setBeanPropertyName(beanPropertyName);
		groupingProperty.setLabel(label);
		return groupingProperty;
	}
}
