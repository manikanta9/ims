package com.clifton.core.dataaccess.dao.event;

import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import org.springframework.stereotype.Component;


/**
 * The <code>NamedEntityWithClearCache</code> is a test cache used to test
 *
 * @author manderson
 */
@Component
public class NamedEntityTestObjectByNameWithClearCache extends SelfRegisteringSingleKeyDaoCache<NamedEntityTestObject, String> {


	@Override
	protected String getBeanKeyProperty() {
		return "name";
	}


	@Override
	protected String getBeanKeyValue(NamedEntityTestObject bean) {
		return bean.getName();
	}


	@Override
	protected boolean isClearOnAllKeyChanges() {
		return true;
	}
}
