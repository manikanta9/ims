package com.clifton.core.dataaccess.migrate.execution;


import com.clifton.core.beans.BaseEntity;


public class CalendarMonth extends BaseEntity<Integer> {

	private String name;
	private String abbreviation;
	private boolean yearEnd;
	private boolean yearStart;

	private CalendarDay firstCalendarDay;


	/**
	 * @return the yearEnd
	 */
	public boolean isYearEnd() {
		return this.yearEnd;
	}


	/**
	 * @param yearEnd the yearEnd to set
	 */
	public void setYearEnd(boolean yearEnd) {
		this.yearEnd = yearEnd;
	}


	/**
	 * @return the yearStart
	 */
	public boolean isYearStart() {
		return this.yearStart;
	}


	/**
	 * @param yearStart the yearStart to set
	 */
	public void setYearStart(boolean yearStart) {
		this.yearStart = yearStart;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the abbreviation
	 */
	public String getAbbreviation() {
		return this.abbreviation;
	}


	/**
	 * @param abbreviation the abbreviation to set
	 */
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation;
	}


	public CalendarDay getFirstCalendarDay() {
		return this.firstCalendarDay;
	}


	public void setFirstCalendarDay(CalendarDay firstCalendarDay) {
		this.firstCalendarDay = firstCalendarDay;
	}
}
