package com.clifton.core.dataaccess.sql;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.restrictions.DateRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ForeignKeyRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRangeRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRestrictionDefinition;
import com.clifton.core.dataaccess.search.sql.SqlSearchFormConfigurer;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SqlSearchFormConfigurerTests</code> class tests {@link SqlSearchFormConfigurer} methods.
 *
 * @author vgomelsky
 */
public class SqlSearchFormConfigurerTests {

	@Test
	public void testConfigureCriteriaForEmptyIN() {
		BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
		List<SearchRestriction> restrictionList = new ArrayList<>();
		SearchRestriction restriction = new SearchRestriction();
		restriction.setField("field");
		restriction.setComparison(ComparisonConditions.IN);
		restrictionList.add(restriction);
		searchForm.setRestrictionList(restrictionList);

		SqlSearchFormConfigurer configurer = new SqlSearchFormConfigurer(searchForm);
		configurer.setSelectClause("*");
		configurer.setFromClause("Table");
		configurer.addSearchFieldDefinition(new IntegerRangeRestrictionDefinition("field", "searchField", "sortField", new int[]{1, 2, 3}));

		SqlSelectCommand command = new SqlSelectCommand();
		configurer.configureCriteria(command);
		Assertions.assertEquals("SELECT *\nFROM Table", command.getSql());

		restriction.setValue(new ArrayList<>());
		configurer.configureCriteria(command);
		Assertions.assertEquals("SELECT *\nFROM Table", command.getSql());
	}


	@Test
	public void testConfigureCriteriaForEquals() {
		BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
		List<SearchRestriction> restrictionList = new ArrayList<>();
		SearchRestriction restriction = new SearchRestriction();
		restriction.setField("field");
		restriction.setComparison(ComparisonConditions.EQUALS);
		restriction.setValue(2);
		restrictionList.add(restriction);
		searchForm.setRestrictionList(restrictionList);

		SqlSearchFormConfigurer configurer = new SqlSearchFormConfigurer(searchForm);
		configurer.setSelectClause("*");
		configurer.setFromClause("Table");
		configurer.addSearchFieldDefinition(new ForeignKeyRestrictionDefinition<>(new IntegerRestrictionDefinition("field", "searchField", "sortField"), new Integer[]{1, 2, 3}));

		SqlSelectCommand command = new SqlSelectCommand();
		configurer.configureCriteria(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nWHERE searchField = ?", command.getSql());
		Assertions.assertEquals(2, command.getSqlParameterValues().get(0).getValue());

		searchForm.setOrderBy("field");
		configurer.configureOrderBy(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nWHERE searchField = ?\nORDER BY sortField ASC", command.getSql());
	}


	@Test
	public void testConfigureCriteriaForEqualsOrIsNull() {
		BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
		List<SearchRestriction> restrictionList = new ArrayList<>();
		SearchRestriction restriction = new SearchRestriction();
		restriction.setField("field");
		restriction.setComparison(ComparisonConditions.EQUALS_OR_IS_NULL);
		restriction.setValue(DateUtils.toDate("04/25/2012"));
		restrictionList.add(restriction);
		searchForm.setRestrictionList(restrictionList);

		SqlSearchFormConfigurer configurer = new SqlSearchFormConfigurer(searchForm);
		configurer.setSelectClause("*");
		configurer.setFromClause("Table");
		configurer.addSearchFieldDefinition(new DateRestrictionDefinition("field", "searchField", null, false, "sortField", false, new ComparisonConditions[]{ComparisonConditions.EQUALS,
				ComparisonConditions.GREATER_THAN, ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS,
				ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_IS_NULL}));

		SqlSelectCommand command = new SqlSelectCommand();
		configurer.configureCriteria(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nWHERE (searchField BETWEEN ? AND ? OR searchField IS NULL)", command.getSql());
		Assertions.assertEquals(DateUtils.toDate("04/25/2012"), command.getSqlParameterValues().get(0).getValue());

		searchForm.setOrderBy("field");
		configurer.configureOrderBy(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nWHERE (searchField BETWEEN ? AND ? OR searchField IS NULL)\nORDER BY sortField ASC", command.getSql());
	}


	@Test
	public void testConfigureOrderBy() {
		BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
		searchForm.setStart(0);
		searchForm.setLimit(10);

		SqlSearchFormConfigurer configurer = new SqlSearchFormConfigurer(searchForm);
		configurer.setSelectClause("*");
		configurer.setFromClause("Table");

		SqlSelectCommand command = new SqlSelectCommand();
		configurer.configureCriteria(command);
		configurer.configureOrderBy(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nORDER BY 1 ASC", command.getSql());

		configurer.setOrderByClause("2, 3 DESC");
		configurer.configureOrderBy(command);
		Assertions.assertEquals("SELECT *\nFROM Table\nORDER BY 2, 3 DESC", command.getSql());
	}
}
