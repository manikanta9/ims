package com.clifton.core.dataaccess.search.form.command;


import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseUpdatableEntitySearchForm;


/**
 * The <code>BaseEntitySearchCommand</code> is used to test the url convention utils.
 * Previously the urlMappings were based on using the method.toGenericString function and check if the resulting string
 * contained the words 'SearchForm', now the url convention is based on the class being annotated with 'SearchForm'
 * Since this command extends a search form, it will contain the annotation and therefor is subject to the url mapping logic.
 * <p>
 * <p>
 * If the searched entity includes audit fields, then the {@link BaseUpdatableEntitySearchForm} or {@link BaseAuditableEntitySearchForm} should be used instead.
 */
public abstract class BaseEntitySearchCommand extends BaseEntitySearchForm {

}
