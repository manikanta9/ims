package com.clifton.core.dataaccess.transactions;

import org.springframework.transaction.annotation.Transactional;


/**
 * The interface for tests on the functionality of the {@link Transactional} annotation.
 * <p>
 * This interface exists to prevent the implementation from being eagerly loaded before weaving is enabled. Weaving is only enabled once the context configuration has been loaded,
 * and weaving must be enabled during class loading, but class loading for all fields, method parameters, and return types takes place before context configuration. By using an
 * interface, we are able to defer class loading for the implementation until after context configuration. {@link Transactional} annotations on the implementation can then be woven
 * as intended.
 *
 * @author MikeH
 */
public interface TransactionTestService {

	public void simpleMethod();


	public void simpleWithExceptionMethod();


	public void simpleMethodCallingTransactionalMethod();


	public void transactionalMethod();


	public void transactionalWithExceptionMethod();


	public void transactionalMethodCallingTransactionalMethod();


	public void transactionalMethodCallingSimpleMethod();


	public void transactionalMethodCallingSimpleWithExceptionMethod();
}
