package com.clifton.core.dataaccess.migrate.execution;


import com.clifton.core.beans.BaseEntity;


public class CalendarYear extends BaseEntity<Short> {

	private String name;
	private String testField;
	private boolean leapYear;

	private CalendarMonth firstMonth;

	private CalendarMonth lastMonth;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * @return the leapYear
	 */
	public boolean isLeapYear() {
		return this.leapYear;
	}


	/**
	 * @param leapYear the leapYear to set
	 */
	public void setLeapYear(boolean leapYear) {
		this.leapYear = leapYear;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the testField
	 */
	public String getTestField() {
		return this.testField;
	}


	/**
	 * @param testField the testField to set
	 */
	public void setTestField(String testField) {
		this.testField = testField;
	}


	public CalendarMonth getFirstMonth() {
		return this.firstMonth;
	}


	public void setFirstMonth(CalendarMonth firstMonth) {
		this.firstMonth = firstMonth;
	}


	public CalendarMonth getLastMonth() {
		return this.lastMonth;
	}


	public void setLastMonth(CalendarMonth lastMonth) {
		this.lastMonth = lastMonth;
	}
}
