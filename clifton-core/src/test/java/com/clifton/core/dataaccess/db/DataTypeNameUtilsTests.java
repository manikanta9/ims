package com.clifton.core.dataaccess.db;

import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author stevenf
 */
public class DataTypeNameUtilsTests {

	@Test
	public void testDataTypeNameIntegerConversion() {
		Object o = DataTypeNameUtils.convertObjectToDataTypeName("04", DataTypeNames.INTEGER);
		Assertions.assertTrue(o instanceof Integer);
		Assertions.assertEquals(4, (int) ((Integer) o));
		o = DataTypeNameUtils.convertObjectToDataTypeName(Short.valueOf("3"), DataTypeNames.INTEGER);
		Assertions.assertTrue(o instanceof Integer);
		Assertions.assertEquals(3, (int) ((Integer) o));
		NumberFormatException numberFormatException = Assertions.assertThrows(NumberFormatException.class, () -> DataTypeNameUtils.convertObjectToDataTypeName("4.4", DataTypeNames.INTEGER));
		Assertions.assertEquals("For input string: \"4.4\"", numberFormatException.getMessage());
	}


	@Test
	public void testDataTypeNameIntegerExplicitConversion() {
		Integer o = DataTypeNameUtils.convertObjectToDataTypeName("04", DataTypeNames.INTEGER, Integer.class);
		Assertions.assertEquals(4, o.intValue());
		o = DataTypeNameUtils.convertObjectToDataTypeName(Short.valueOf("3"), DataTypeNames.INTEGER, Integer.class);
		Assertions.assertEquals(3, o.intValue());
		NumberFormatException numberFormatException = Assertions.assertThrows(NumberFormatException.class, () -> DataTypeNameUtils.convertObjectToDataTypeName("4.4", DataTypeNames.INTEGER));
		Assertions.assertEquals("For input string: \"4.4\"", numberFormatException.getMessage());
	}


	@Test
	public void testDataTypeNameDecimalConversion() {
		Object o = DataTypeNameUtils.convertObjectToDataTypeName("12351.2345254", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("12351.2345254", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("0.234e8", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("23400000", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("0E-10", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("0.0000000000", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("1,245,234.234", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("1245234.234", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(1245234234, DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("1245234234", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(124523423.32424d, DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("124523423.32424", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("-12351.2345254", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("-12351.2345254", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("(12351.2345254)", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("-12351.2345254", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("-0.234e8", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("-23400000", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("(0.234e8)", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("-23400000", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("-1,245,234.234", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("-1245234.234", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("(1,245,234.234)", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("-1245234.234", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(" 43 ", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("43", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("@99.45", DataTypeNames.DECIMAL);
		Assertions.assertTrue(o instanceof BigDecimal);
		Assertions.assertEquals("99.45", ((BigDecimal) o).toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(" - ", DataTypeNames.DECIMAL);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName(".", DataTypeNames.DECIMAL);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName(" .", DataTypeNames.DECIMAL);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName("  ", DataTypeNames.DECIMAL);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName("", DataTypeNames.DECIMAL);
		Assertions.assertNull(o);
	}


	@Test
	public void testDataTypeNameDecimalExplicitConversion() {
		BigDecimal o = DataTypeNameUtils.convertObjectToDataTypeName("12351.2345254", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("12351.2345254", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("0.234e8", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("23400000", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("0E-10", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("0.0000000000", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("1,245,234.234", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("1245234.234", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(1245234234, DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("1245234234", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(124523423.32424d, DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("124523423.32424", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("-12351.2345254", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("-12351.2345254", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("(12351.2345254)", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("-12351.2345254", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("-0.234e8", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("-23400000", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("(0.234e8)", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("-23400000", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("-1,245,234.234", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("-1245234.234", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName("(1,245,234.234)", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("-1245234.234", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(" 43 ", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertEquals("43", o.toPlainString());
		o = DataTypeNameUtils.convertObjectToDataTypeName(" - ", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName(".", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName(" .", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName("  ", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertNull(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName("", DataTypeNames.DECIMAL, BigDecimal.class);
		Assertions.assertNull(o);
	}


	@Test
	public void testDataTypeNameDateConversion() {
		Date targetDate = DateUtils.toDate("04/03/1981");
		Object o = DataTypeNameUtils.convertObjectToDataTypeName("04/03/1981", DataTypeNames.DATE);
		Assertions.assertTrue(o instanceof Date);
		Assertions.assertEquals(o, targetDate);
		o = DataTypeNameUtils.convertObjectToDataTypeName("04-03-1981", DataTypeNames.DATE);
		Assertions.assertTrue(o instanceof Date);
		Assertions.assertEquals(o, targetDate);
		o = DataTypeNameUtils.convertObjectToDataTypeName("04031981", DataTypeNames.DATE);
		Assertions.assertTrue(o instanceof Date);
		Assertions.assertEquals(o, targetDate);
		targetDate = DateUtils.toDate("04/03/2081");
		o = DataTypeNameUtils.convertObjectToDataTypeName("04/03/81", DataTypeNames.DATE);
		Assertions.assertEquals(o, targetDate);
		o = DataTypeNameUtils.convertObjectToDataTypeName("040381", DataTypeNames.DATE);
		Assertions.assertEquals(o, targetDate);
	}


	@Test
	public void testDataTypeNameDateExplicitConversion() {
		Date targetDate = DateUtils.toDate("04/03/1981");
		Date o = DataTypeNameUtils.convertObjectToDataTypeName("04/03/1981", DataTypeNames.DATE, Date.class);
		Assertions.assertEquals(o, targetDate);
		o = DataTypeNameUtils.convertObjectToDataTypeName("04-03-1981", DataTypeNames.DATE, Date.class);
		Assertions.assertEquals(o, targetDate);
		o = DataTypeNameUtils.convertObjectToDataTypeName("04031981", DataTypeNames.DATE, Date.class);
		Assertions.assertEquals(o, targetDate);
		targetDate = DateUtils.toDate("04/03/2081");
		o = DataTypeNameUtils.convertObjectToDataTypeName("04/03/81", DataTypeNames.DATE, Date.class);
		Assertions.assertEquals(o, targetDate);
		o = DataTypeNameUtils.convertObjectToDataTypeName("040381", DataTypeNames.DATE, Date.class);
		Assertions.assertEquals(o, targetDate);
	}


	@Test
	public void testDataTypeNameBooleanConversion() {
		Object o = DataTypeNameUtils.convertObjectToDataTypeName("1", DataTypeNames.BOOLEAN);
		Assertions.assertTrue(o instanceof Boolean);
		o = DataTypeNameUtils.convertObjectToDataTypeName(1, DataTypeNames.BOOLEAN);
		Assertions.assertTrue(o instanceof Boolean);
	}


	@Test
	public void testDataTypeNameBooleanExplicitConversion() {
		Boolean o = DataTypeNameUtils.convertObjectToDataTypeName("1", DataTypeNames.BOOLEAN, Boolean.class);
		Assertions.assertTrue(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName(1, DataTypeNames.BOOLEAN, Boolean.class);
		Assertions.assertTrue(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName("true", DataTypeNames.BOOLEAN, Boolean.class);
		Assertions.assertTrue(o);
		o = DataTypeNameUtils.convertObjectToDataTypeName("false", DataTypeNames.BOOLEAN, Boolean.class);
		Assertions.assertFalse(o);
	}
}
