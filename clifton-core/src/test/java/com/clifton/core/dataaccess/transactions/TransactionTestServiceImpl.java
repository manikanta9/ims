package com.clifton.core.dataaccess.transactions;


import org.springframework.transaction.annotation.Transactional;


/**
 * The <code>TransactionTestService</code> class is used for testing application of transactional aspect.
 *
 * @author vgomelsky
 */
public class TransactionTestServiceImpl implements TransactionTestService {

	@Override
	public void simpleMethod() {
		// do something
	}


	@Override
	public void simpleWithExceptionMethod() {
		throw new RuntimeException("simple method that throws an exception");
	}


	@Override
	public void simpleMethodCallingTransactionalMethod() {
		// do something
		transactionalMethod();
		// do something after
	}


	@Override
	@Transactional
	public void transactionalMethod() {
		// do something
	}


	@Override
	@Transactional
	public void transactionalWithExceptionMethod() {
		throw new RuntimeException("simple method that throws an exception");
	}


	@Override
	@Transactional
	public void transactionalMethodCallingTransactionalMethod() {
		// do something
		transactionalMethod();
		// do something after
	}


	@Override
	@Transactional
	public void transactionalMethodCallingSimpleMethod() {
		// do something
		simpleMethod();
		// do something after
	}


	@Override
	@Transactional
	public void transactionalMethodCallingSimpleWithExceptionMethod() {
		// do something
		simpleWithExceptionMethod();
		// do something after
	}
}
