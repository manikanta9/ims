package com.clifton.core.dataaccess.dao.locator;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntity;
import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.xml.CoreTestEntity;
import com.clifton.core.dataaccess.dao.xml.CoreTestGroupEntity;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.web.bind.TestBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Collection;


/**
 * The <code>SpringContextDaoLocatorTests</code> ...
 *
 * @author manderson
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class SpringContextDaoLocatorTests {

	@Resource
	private DaoLocator daoLocator;


	@Test
	public void locateByDTO_Exists() {
		ReadOnlyDAO<IdentityObject> dao = this.daoLocator.locate(new CoreTestGroupEntity());
		Assertions.assertNotNull(dao);
		Assertions.assertEquals("CoreTestGroupEntity", dao.getConfiguration().getTableName());
		Assertions.assertEquals(3, CollectionUtils.getSize(dao.findAll()));

		dao = this.daoLocator.locate(new CoreTestEntity());
		Assertions.assertNotNull(dao);
		Assertions.assertEquals("CoreTestEntity", dao.getConfiguration().getTableName());
		Assertions.assertEquals(0, CollectionUtils.getSize(dao.findAll()));
	}


	@Test
	public void locateByDTO_NotExists() {
		IllegalArgumentException illegalArgumentException = Assertions.assertThrows(IllegalArgumentException.class, () -> this.daoLocator.locate(new TestBean()));
		Assertions.assertEquals("Cannot locate DAO for class com.clifton.core.web.bind.TestBean and no superclass with DAO found.", illegalArgumentException.getMessage());
	}


	@Test
	public void testLocateByTableName_Exists() {
		ReadOnlyDAO<IdentityObject> dao = this.daoLocator.locate("CoreTestGroupEntity");
		Assertions.assertNotNull(dao);
		Assertions.assertEquals("CoreTestGroupEntity", dao.getConfiguration().getTableName());
		Assertions.assertEquals(3, CollectionUtils.getSize(dao.findAll()));

		dao = this.daoLocator.locate("CoreTestEntity");
		Assertions.assertNotNull(dao);
		Assertions.assertEquals("CoreTestEntity", dao.getConfiguration().getTableName());
		Assertions.assertEquals(0, CollectionUtils.getSize(dao.findAll()));
	}


	@Test
	public void testLocateByTableName_NotExists() {
		NoSuchBeanDefinitionException noSuchBeanDefinitionException = Assertions.assertThrows(NoSuchBeanDefinitionException.class, () -> this.daoLocator.locate("InvalidTableName"));
		Assertions.assertEquals("No bean named 'invalidTableNameDAO' available", noSuchBeanDefinitionException.getMessage());
	}


	@Test
	public void testLocateAll() {
		Collection<ReadOnlyDAO<?>> daoList = this.daoLocator.locateAll();
		Assertions.assertEquals(2, CollectionUtils.getSize(daoList));
	}


	@Test
	public void testLocateAllInstanceOf_Exists() {
		@SuppressWarnings("unchecked")
		Collection<ReadOnlyDAO<? extends NamedEntity<?>>> daoList = (Collection<ReadOnlyDAO<? extends NamedEntity<?>>>) (Collection<?>) this.daoLocator.locateAll(NamedEntity.class);
		Assertions.assertEquals(2, CollectionUtils.getSize(daoList));
	}


	@Test
	public void testLocateAllInstanceOf_NotExists() {
		Collection<ReadOnlyDAO<? extends DocumentAware>> daoList = this.daoLocator.locateAll(DocumentAware.class);
		Assertions.assertEquals(0, CollectionUtils.getSize(daoList));
	}
}
