package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.test.executor.BaseTestExecutor;

import java.util.ArrayList;
import java.util.List;


public abstract class BaseGroupedResultTransformerTestExecutor<T> extends BaseTestExecutor<T> {


	private final GroupingSearchForm groupingSearchForm;
	private final DaoLocator daoLocator;
	private final DAOConfiguration daoConfiguration;
	JsonHandler<?> jsonHandler = new JacksonHandlerImpl();


	/**
	 * List of Object[] of results (as expected to be returned from the query execution)
	 */
	private final List<Object[]> tuples;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected BaseGroupedResultTransformerTestExecutor(GroupingSearchForm groupingSearchForm, DaoLocator daoLocator, DAOConfiguration daoConfiguration, List<Object[]> tuples) {
		this.groupingSearchForm = groupingSearchForm;
		this.daoLocator = daoLocator;
		this.daoConfiguration = daoConfiguration;
		this.tuples = tuples;
	}


	protected static GroupingSearchForm buildGroupingSearchForm(List<GroupingProperty> groupingPropertyList, List<GroupingAggregateProperty> groupingAggregatePropertyList) {
		GroupingSearchForm searchForm = new GroupingSearchForm() {
			@Override
			public List<GroupingProperty> getGroupingPropertyList() {
				return groupingPropertyList;
			}


			@Override
			public List<GroupingAggregateProperty> getGroupingAggregatePropertyList() {
				return groupingAggregatePropertyList;
			}
		};
		return searchForm;
	}


	protected static List<GroupingProperty> buildGroupingPropertyList(int groupPropertyCount) {
		List<GroupingProperty> groupingPropertyList = new ArrayList<>();
		for (int i = 1; i <= groupPropertyCount; i++) {
			GroupingProperty groupingProperty = new GroupingProperty();
			groupingProperty.setBeanPropertyName("property" + i);
			groupingProperty.setLabel("Property " + i);
			groupingProperty.setEmptyValueLabel("None");
			groupingProperty.setSearchFieldName(groupingProperty.getBeanPropertyName());
			groupingPropertyList.add(groupingProperty);
		}
		return groupingPropertyList;
	}


	protected static List<GroupingAggregateProperty> buildGroupingAggregatePropertyList(GroupingAggregateTypes[] aggregateTypes) {
		List<GroupingAggregateProperty> groupingAggregatePropertyList = new ArrayList<>();
		for (int i = 0; i < aggregateTypes.length; i++) {
			GroupingAggregateProperty groupingAggregateProperty = new GroupingAggregateProperty();
			groupingAggregateProperty.setAggregateType(aggregateTypes[i]);
			groupingAggregateProperty.setBeanPropertyName("property" + (i + 100));
			groupingAggregateProperty.setAggregateLabel(aggregateTypes[i].name() + " Property " + (i + 100));
			groupingAggregatePropertyList.add(groupingAggregateProperty);
		}
		return groupingAggregatePropertyList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public GroupingSearchForm getGroupingSearchForm() {
		return this.groupingSearchForm;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public DAOConfiguration getDaoConfiguration() {
		return this.daoConfiguration;
	}


	public List<Object[]> getTuples() {
		return this.tuples;
	}
}
