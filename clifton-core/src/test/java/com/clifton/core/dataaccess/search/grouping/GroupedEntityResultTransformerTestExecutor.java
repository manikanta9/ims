package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.grouping.transformer.GroupedEntityResultTransformer;
import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;


@SuppressWarnings("rawtypes")
public class GroupedEntityResultTransformerTestExecutor<T extends IdentityObject> extends BaseGroupedResultTransformerTestExecutor<GroupedEntityResult<T>> {


	public static GroupedEntityResultTransformerTestExecutor forGroupingPropertyList(List<GroupingProperty> groupingPropertyList, List<Object[]> tuples, DaoLocator daoLocator, DAOConfiguration daoConfiguration) {
		return new GroupedEntityResultTransformerTestExecutor<>(buildGroupingSearchForm(groupingPropertyList, null), daoLocator, daoConfiguration, tuples);
	}


	public static GroupedEntityResultTransformerTestExecutor forGroupingPropertyListAndAggregateList(List<GroupingProperty> groupingPropertyList, List<GroupingAggregateProperty> groupingAggregatePropertyList, List<Object[]> tuples, DaoLocator daoLocator, DAOConfiguration daoConfiguration) {
		return new GroupedEntityResultTransformerTestExecutor<>(buildGroupingSearchForm(groupingPropertyList, groupingAggregatePropertyList), daoLocator, daoConfiguration, tuples);
	}


	private GroupedEntityResultTransformerTestExecutor(GroupingSearchForm groupingSearchForm, DaoLocator daoLocator, DAOConfiguration daoConfiguration, List<Object[]> tuples) {
		super(groupingSearchForm, daoLocator, daoConfiguration, tuples);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getResultEntityTypeName() {
		return "Grouped Entity Result";
	}


	@SuppressWarnings("unchecked")
	@Override
	protected List<GroupedEntityResult<T>> executeTest() {
		GroupedEntityResultTransformer<T> transformer = new GroupedEntityResultTransformer<T>(getGroupingSearchForm(), getDaoLocator(), getDaoConfiguration());
		List<Object> transformedTuples = new ArrayList<>();

		if (!CollectionUtils.isEmpty(getTuples())) {
			getTuples().forEach(tuple -> transformedTuples.add(transformer.transformTuple(tuple, null)));
		}
		return (List<GroupedEntityResult<T>>) transformer.transformList(transformedTuples);
	}


	@Override
	protected String[] getStringValuesForResultEntity(GroupedEntityResult resultEntity) {
		String resultEntityString = resultEntity.toStringFormatted(getGroupingSearchForm().getGroupingPropertyList());
		return new String[]{resultEntityString};
	}
}
