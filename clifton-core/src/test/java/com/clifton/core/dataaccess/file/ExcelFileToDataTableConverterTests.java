package com.clifton.core.dataaccess.file;


import com.clifton.core.converter.string.StringToBigDecimalConverter;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.math.CoreMathUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ExcelFileToDataTableConverterTests</code> ...
 *
 * @author manderson
 */
public class ExcelFileToDataTableConverterTests {

	private FileToDataTableConverter fileToDataTableConverter = new ExcelFileToDataTableConverter();

	private static final Map<String, Double> PRICE_RESULTS_MAP = new HashMap<>();


	static {
		PRICE_RESULTS_MAP.put("TEST-SECURITY-1", 65.786);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-2", 63.008257);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-3", 96.663787000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-4", 112.289000000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-5", 96.663787000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-6", 60.000000000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-7", 104.210000000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-8", 65.786000000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-9", 91.610321000000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-10", 62.731200);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-11", 107.08000);
		PRICE_RESULTS_MAP.put("TEST-SECURITY-12", 107.6900);
	}


	@Test
	public void testExcelFileConverter_Decimals() {
		DataTable result = this.fileToDataTableConverter.convert(new File("src/test/java/com/clifton/core/dataaccess/file/Pricing Upload Example.xls"));

		AssertUtils.assertTrue(12 == result.getTotalRowCount(), "Expected a DataTable with 12 rows of data");

		for (int i = 0; i < result.getTotalRowCount(); i++) {
			DataRow row = result.getRow(i);

			String symbol = (String) row.getValue("InvestmentSecurity-Symbol");
			Double expectedPriceValue = PRICE_RESULTS_MAP.get(symbol);

			validateValue(row, "MeasureValue-Text", expectedPriceValue);
			// validateValue(row, "MeasureValue", expectedPriceValue);
			// validateValue(row, "MeasureValue-Formula", expectedPriceValue);
		}
	}


	private void validateValue(DataRow row, String columnName, Double expectedValue) {
		String symbol = (String) row.getValue("InvestmentSecurity-Symbol");
		// Round all to 15 decimals so have same precision
		BigDecimal priceValue = MathUtils.round(new StringToBigDecimalConverter().convert((String) row.getValue(columnName)), 15);
		BigDecimal expectedPriceValue = MathUtils.round(BigDecimal.valueOf(expectedValue), 15);

		AssertUtils.assertTrue(MathUtils.isEqual(expectedPriceValue, priceValue), "Expected " + columnName + " [" + CoreMathUtils.formatNumber(expectedPriceValue, "#,###.###############") + "] for "
				+ symbol + ", but instead got [" + CoreMathUtils.formatNumber(priceValue, "#,###.###############") + "].");
	}
}
