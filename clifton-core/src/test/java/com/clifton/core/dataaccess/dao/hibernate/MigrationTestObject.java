package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.migrate.execution.Frequencies;
import com.clifton.core.util.date.Time;


public class MigrationTestObject extends NamedEntity<Integer> {

	private Frequencies frequency;
	private Frequencies frequencyTest;
	private Frequencies frequencyTest2;
	private Time startTime;


	public Frequencies getFrequency() {
		return this.frequency;
	}


	public void setFrequency(Frequencies frequency) {
		this.frequency = frequency;
	}


	public Frequencies getFrequencyTest() {
		return this.frequencyTest;
	}


	public void setFrequencyTest(Frequencies frequencyTest) {
		this.frequencyTest = frequencyTest;
	}


	public Frequencies getFrequencyTest2() {
		return this.frequencyTest2;
	}


	public void setFrequencyTest2(Frequencies frequencyTest2) {
		this.frequencyTest2 = frequencyTest2;
	}


	public Time getStartTime() {
		return this.startTime;
	}


	public void setStartTime(Time startTime) {
		this.startTime = startTime;
	}
}
