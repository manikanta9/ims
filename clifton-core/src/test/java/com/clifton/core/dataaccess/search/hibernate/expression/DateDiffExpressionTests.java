package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.type.Type;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author davidi
 */
public class DateDiffExpressionTests {

	/**
	 * An instance of the Criteria implementation for testing
	 */
	private Criteria getCriteria() {
		return new CriteriaImpl("SystemQueryRunHistory", null);
	}


	/**
	 * An instance of the CriteriaQuery implementation for testing
	 */
	private CriteriaQuery getCriteriaQuery() {
		class TestCriteriaQuery implements CriteriaQuery {

			@Override
			public SessionFactoryImplementor getFactory() {
				return null;
			}


			@Override
			public String getColumn(Criteria criteria, String propertyPath) throws HibernateException {
				return StringUtils.capitalize(propertyPath);
			}


			@Override
			public String[] getColumns(String propertyPath, Criteria criteria) throws HibernateException {
				return new String[0];
			}


			@Override
			public String[] findColumns(String propertyPath, Criteria criteria) throws HibernateException {
				return new String[0];
			}


			@Override
			public Type getType(Criteria criteria, String propertyPath) throws HibernateException {
				return null;
			}


			@Override
			public String[] getColumnsUsingProjection(Criteria criteria, String propertyPath) throws HibernateException {
				return new String[0];
			}


			@Override
			public Type getTypeUsingProjection(Criteria criteria, String propertyPath) throws HibernateException {
				return null;
			}


			@Override
			public TypedValue getTypedValue(Criteria criteria, String propertyPath, Object value) throws HibernateException {
				return null;
			}


			@Override
			public String getEntityName(Criteria criteria) {
				return null;
			}


			@Override
			public String getEntityName(Criteria criteria, String propertyPath) {
				return null;
			}


			@Override
			public String getSQLAlias(Criteria criteria) {
				return null;
			}


			@Override
			public String getSQLAlias(Criteria criteria, String propertyPath) {
				return null;
			}


			@Override
			public String getPropertyName(String propertyName) {
				return null;
			}


			@Override
			public String[] getIdentifierColumns(Criteria criteria) {
				return new String[0];
			}


			@Override
			public Type getIdentifierType(Criteria criteria) {
				return null;
			}


			@Override
			public TypedValue getTypedIdentifierValue(Criteria criteria, Object value) {
				return null;
			}


			@Override
			public String generateSQLAlias() {
				return null;
			}
		}
		;

		return new TestCriteriaQuery();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testDateDiffExpression_toSQLString() {
		SearchFieldCustomTypes dateDiffSearchFieldType = SearchFieldCustomTypes.DATE_DIFFERENCE_SECONDS;
		String dateDiffFunction = dateDiffSearchFieldType.getDateDiffFunction();
		String[] propertyNames = new String[]{"startDate", "endDate"};

		DateDiffExpression dateDiffExpression = new DateDiffExpression(dateDiffFunction, ">", propertyNames, 15);
		Criteria criteria = getCriteria();
		CriteriaQuery criteriaQuery = getCriteriaQuery();

		Assertions.assertEquals("(DATEDIFF(MILLISECOND, StartDate, EndDate) / 1000) > ?", dateDiffExpression.toSqlString(criteria, criteriaQuery));
	}
}
