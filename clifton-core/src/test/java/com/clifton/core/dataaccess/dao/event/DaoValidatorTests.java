package com.clifton.core.dataaccess.dao.event;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedEntityTestObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.hibernate.DirtyCheckingStrategy;
import com.clifton.core.dataaccess.dao.hibernate.HibernateUpdatableDAO;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class DaoValidatorTests {

	@Resource
	private ContextHandler contextHandler;
	@Resource
	private HibernateUpdatableDAO<NamedEntityTestObject> namedEntityTestObjectDAO;


	@BeforeEach
	public void configureTest() {
		User user = new User();
		user.setId(new Integer(7).shortValue());
		getContextHandler().setBean(Context.USER_BEAN_NAME, user);

		Session session = Mockito.mock(Session.class);
		SessionFactory factory = Mockito.mock(SessionFactory.class);
		Mockito.when(factory.getCurrentSession()).thenReturn(session);
		Mockito.when(factory.openSession()).thenReturn(session);
		Mockito.when(session.save(ArgumentMatchers.any(IdentityObject.class))).thenReturn(null);
		SessionFactoryOptions options = Mockito.mock(SessionFactoryOptions.class);
		DirtyCheckingStrategy dirtyCheckingStrategy = Mockito.mock(DirtyCheckingStrategy.class);
		Mockito.when(options.getCustomEntityDirtinessStrategy()).thenReturn(dirtyCheckingStrategy);
		Mockito.when(factory.getSessionFactoryOptions()).thenReturn(options);
		getNamedEntityTestObjectDAO().setSessionFactory(factory);
	}


	@Test
	public void testValid() {
		NamedEntityTestObject bean = new NamedEntityTestObject();
		bean.setName("TEST NAME");
		bean.setDescription("TEST DESCRIPTION");
		getNamedEntityTestObjectDAO().save(bean);
	}


	@Test
	public void testInValid() {
		Assertions.assertThrows(Exception.class, () -> {
			NamedEntityTestObject bean = new NamedEntityTestObject();
			bean.setName("TEST NAME");
			bean.setDescription("Wrong Description");
			getNamedEntityTestObjectDAO().save(bean);
		});
	}


	@Test
	public void testObserverCall() {
		NamedEntityTestObject bean = new NamedEntityTestObject();
		bean.setName("TEST NAME");
		bean.setDescription("TEST DESCRIPTION");
		Assertions.assertEquals("TEST NAME", bean.getLabel());
		getNamedEntityTestObjectDAO().save(bean);
		Assertions.assertEquals("TEST LABEL", bean.getLabel());
	}


	/**
	 * @return the contextHandler
	 */
	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	/**
	 * @param contextHandler the contextHandler to set
	 */
	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public HibernateUpdatableDAO<NamedEntityTestObject> getNamedEntityTestObjectDAO() {
		return this.namedEntityTestObjectDAO;
	}


	public void setNamedEntityTestObjectDAO(HibernateUpdatableDAO<NamedEntityTestObject> namedEntityTestObjectDAO) {
		this.namedEntityTestObjectDAO = namedEntityTestObjectDAO;
	}
}
