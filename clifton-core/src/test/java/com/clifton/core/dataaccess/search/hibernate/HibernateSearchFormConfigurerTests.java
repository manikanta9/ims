package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.validation.FieldValidationException;
import org.hibernate.Criteria;
import org.hibernate.sql.JoinType;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatcher;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.clifton.core.util.date.DateUtils.toDate;
import static org.hibernate.criterion.MatchMode.ANYWHERE;
import static org.hibernate.criterion.MatchMode.START;
import static org.hibernate.criterion.Order.asc;
import static org.hibernate.criterion.Order.desc;
import static org.hibernate.criterion.Restrictions.eq;
import static org.hibernate.criterion.Restrictions.gt;
import static org.hibernate.criterion.Restrictions.like;
import static org.hibernate.criterion.Restrictions.lt;
import static org.mockito.ArgumentMatchers.argThat;


/**
 * The <code>HibernateSearchFormConfigurerTests</code> class defines test cases for testing the HibernateSearchFormConfigurer class.
 *
 * @author vgomelsky
 */
public class HibernateSearchFormConfigurerTests {

	@Test
	public void testEmptySearchForm() {
		TestSearchForm form = new TestSearchForm();
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(form);

		Criteria criteria = Mockito.mock(Criteria.class);
		configurer.configureCriteria(criteria);
		configurer.configureOrderBy(criteria);
		Mockito.verifyZeroInteractions(criteria);
	}


	@SuppressWarnings("unused")
	@Test
	public void testRequiredFieldValidation() {
		Assertions.assertThrows(FieldValidationException.class, () -> {
			RequiredSearchForm form = new RequiredSearchForm();
			new HibernateSearchFormConfigurer(form);
		});
	}


	@Test
	public void testRequiredFieldSet() {
		RequiredSearchForm form = new RequiredSearchForm();
		form.setForeignKeyId(123L);
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(form);

		Criteria criteria = Mockito.mock(Criteria.class);
		configurer.configureCriteria(criteria);
		Mockito.verify(criteria).add(ArgumentMatchers.argThat(new ToStringMatcher<>(eq("foreignKey.id", form.getForeignKeyId()))));
		Mockito.verifyNoMoreInteractions(criteria);
	}


	@SuppressWarnings("unused")
	@Test
	public void testFormWithInvalidSearchType() {
		Assertions.assertThrows(FieldValidationException.class, () -> {
			InvalidSearchTypeSearchForm form = new InvalidSearchTypeSearchForm();
			new HibernateSearchFormConfigurer(form);
		});
	}


	@Test
	public void testDateRangeSearch() {
		TestSearchForm form = new TestSearchForm();
		List<SearchRestriction> restrictionList = new ArrayList<>();

		SearchRestriction restriction = new SearchRestriction();
		Date ltDate = new Date();
		restriction.setField("dateField");
		restriction.setComparison(ComparisonConditions.LESS_THAN);
		restriction.setValue(ltDate);
		restrictionList.add(restriction);

		String gtDate = "01/01/2005";
		restriction = new SearchRestriction();
		restriction.setField("dateField");
		restriction.setComparison(ComparisonConditions.GREATER_THAN);
		restriction.setValue(gtDate);
		restrictionList.add(restriction);

		form.setRestrictionList(restrictionList);
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(form);

		Criteria criteria = Mockito.mock(Criteria.class);
		configurer.configureCriteria(criteria);
		Mockito.verify(criteria).add(argThat(new ToStringMatcher<>(lt("dateField", ltDate))));
		Mockito.verify(criteria).add(argThat(new ToStringMatcher<>(gt("dateField", toDate(gtDate)))));
		Mockito.verifyNoMoreInteractions(criteria);
	}


	@Test
	public void testStringLikeAndBeginsWith() {
		TestSearchForm form = new TestSearchForm();
		form.setBeginsWith("start");
		form.setStringLike("inside");
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(form);

		Criteria criteria = Mockito.mock(Criteria.class);
		configurer.configureCriteria(criteria);
		// Hibernate's SimpleExpression object doesn't implement equals method so have to use a custom matcher.
		Mockito.verify(criteria).add(argThat(new ToStringMatcher<>(like("beginsWith", "start", START))));
		Mockito.verify(criteria).add(argThat(new ToStringMatcher<>(like("stringLike", "inside", ANYWHERE))));
		Mockito.verifyNoMoreInteractions(criteria);
	}


	@Test
	public void testOrderBy() {
		TestSearchForm form = new TestSearchForm();
		form.setOrderBy("intAmount:asc#dateField:desc#stringLike");
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(form);

		Criteria criteria = Mockito.mock(Criteria.class);
		configurer.configureOrderBy(criteria);
		// Hibernate's Order object doesn't implement equals method so have to use a custom matcher.
		Mockito.verify(criteria).addOrder(argThat(new ToStringMatcher<>(asc("intAmount"))));
		Mockito.verify(criteria).addOrder(argThat(new ToStringMatcher<>(desc("dateField"))));
		Mockito.verify(criteria).addOrder(argThat(new ToStringMatcher<>(asc("stringLike"))));
		Mockito.verifyNoMoreInteractions(criteria);
	}


	@Test
	public void testGetPathAlias() {
		TestSearchForm form = new TestSearchForm();
		HibernateSearchFormConfigurer configurer = new HibernateSearchFormConfigurer(form);

		Criteria criteria = Mockito.mock(Criteria.class);
		Criteria instrumentCriteria = Mockito.mock(Criteria.class);
		Mockito.when(criteria.createCriteria("instrument", "a0", JoinType.INNER_JOIN)).thenReturn(instrumentCriteria);
		Mockito.when(instrumentCriteria.getAlias()).thenReturn("a0");

		configurer.getPathAlias("instrument", criteria);
		Mockito.verify(criteria).createCriteria("instrument", "a0", JoinType.INNER_JOIN);
		Assertions.assertEquals(1, configurer.getProcessedPathToCriteria().size());

		// should reference first "instrument" alias "a0" instead of adding a new one
		Criteria hierarchyCriteria = Mockito.mock(Criteria.class);
		Mockito.when(instrumentCriteria.createCriteria("hierarchy", "a1", JoinType.INNER_JOIN)).thenReturn(hierarchyCriteria);
		Mockito.when(hierarchyCriteria.getAlias()).thenReturn("a1");

		configurer.getPathAlias("instrument.hierarchy", criteria);
		Assertions.assertEquals(2, configurer.getProcessedPathToCriteria().size());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The matcher that matches objects of the same class with the same toString values.
	 */
	class ToStringMatcher<T> implements ArgumentMatcher<T> {

		private T match;


		ToStringMatcher(T match) {
			this.match = match;
		}


		@Override
		public boolean matches(Object argument) {
			if (argument.getClass().equals(this.match.getClass())) {
				if (argument.toString().equals(this.match.toString())) {
					return true;
				}
			}
			return false;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////////////      Test Form Definitions      ////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@SearchForm(hasOrmDtoClass = false)
	public class RequiredSearchForm extends BaseAuditableEntitySearchForm {

		@SearchField(required = true, searchField = "foreignKey.id")
		private Long foreignKeyId;


		public Long getForeignKeyId() {
			return this.foreignKeyId;
		}


		public void setForeignKeyId(Long foreignKeyId) {
			this.foreignKeyId = foreignKeyId;
		}
	}

	@SearchForm(hasOrmDtoClass = false)
	public class TestSearchForm extends BaseAuditableEntitySearchForm {

		@SearchField(searchField = "foreignKey.id")
		private Long foreignKeyId;

		@SearchField
		private Integer intAmount;

		@SearchField
		private BigDecimal bigDecimalAmount;

		@SearchField
		private Date dateField;

		@SearchField(comparisonConditions = {ComparisonConditions.BEGINS_WITH})
		private String beginsWith;

		@SearchField
		private String stringLike;


		public Long getForeignKeyId() {
			return this.foreignKeyId;
		}


		public void setForeignKeyId(Long foreignKeyId) {
			this.foreignKeyId = foreignKeyId;
		}


		public Integer getIntAmount() {
			return this.intAmount;
		}


		public void setIntAmount(Integer intAmount) {
			this.intAmount = intAmount;
		}


		public BigDecimal getBigDecimalAmount() {
			return this.bigDecimalAmount;
		}


		public void setBigDecimalAmount(BigDecimal bigDecimalAmount) {
			this.bigDecimalAmount = bigDecimalAmount;
		}


		public Date getDateField() {
			return this.dateField;
		}


		public void setDateField(Date dateField) {
			this.dateField = dateField;
		}


		public String getBeginsWith() {
			return this.beginsWith;
		}


		public void setBeginsWith(String beginsWith) {
			this.beginsWith = beginsWith;
		}


		public String getStringLike() {
			return this.stringLike;
		}


		public void setStringLike(String stringLike) {
			this.stringLike = stringLike;
		}
	}

	@SearchForm(hasOrmDtoClass = false)
	public class InvalidSearchTypeSearchForm extends BaseAuditableEntitySearchForm {

		@SearchField
		private Object invalidTypeField;


		public Object getInvalidTypeField() {
			return this.invalidTypeField;
		}


		public void setInvalidTypeField(Object invalidTypeField) {
			this.invalidTypeField = invalidTypeField;
		}
	}
}
