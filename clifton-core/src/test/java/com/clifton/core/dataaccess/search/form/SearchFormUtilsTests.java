package com.clifton.core.dataaccess.search.form;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


public class SearchFormUtilsTests {

	@Test
	public void testValidDateRange() {
		BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
		Date startDate = new Date();
		setupDateRangeSearchRestrictions(searchForm, "createDate", startDate, DateUtils.addDays(startDate, 7));
		SearchFormUtils.validateDateRangeLength(searchForm, "createDate", 7, null);
	}


	@Test
	public void testInvalidDateRange() {
		Assertions.assertThrows(ValidationException.class, () -> {
			BaseEntitySearchForm searchForm = new AuditableEntitySearchForm();
			Date startDate = new Date();
			setupDateRangeSearchRestrictions(searchForm, "createDate", startDate, DateUtils.addDays(startDate, 8));
			SearchFormUtils.validateDateRangeLength(searchForm, "createDate", 7, null);
		});
	}


	@Test
	public void testConvertSearchRestrictionList() {
		AuditableEntitySearchForm searchForm = new AuditableEntitySearchForm();
		searchForm.setUpdateUserId((short) 1);
		searchForm.addSearchRestriction(new SearchRestriction("createUserId", ComparisonConditions.EQUALS, 2));
		searchForm.addSearchRestriction(new SearchRestriction("updateUserId", ComparisonConditions.EQUALS, 3));
		Assertions.assertEquals(2, searchForm.getRestrictionList().size());

		// all restrictions in the list should be converted to search form properties if the properties are not defined
		SearchFormUtils.convertSearchRestrictionList(searchForm);
		Assertions.assertEquals(Short.valueOf((short) 1), searchForm.getUpdateUserId());
		Assertions.assertEquals(1, searchForm.getRestrictionList().size());

		// update updateUserId value to be the same to ensure the restriction is removed from the list
		CollectionUtils.getFirstElementStrict(searchForm.getRestrictionList()).setValue(searchForm.getUpdateUserId());
		SearchFormUtils.convertSearchRestrictionList(searchForm);
		Assertions.assertEquals(Short.valueOf((short) 1), searchForm.getUpdateUserId());
		Assertions.assertEquals(0, searchForm.getRestrictionList().size());
	}


	private void setupDateRangeSearchRestrictions(BaseEntitySearchForm searchForm, String dateFieldName, Date startDate, Date endDate) {
		searchForm.addSearchRestriction(new SearchRestriction(dateFieldName, ComparisonConditions.GREATER_THAN, DateUtils.fromDateShort(startDate)));
		searchForm.addSearchRestriction(new SearchRestriction(dateFieldName, ComparisonConditions.LESS_THAN, DateUtils.fromDateShort(endDate)));
	}


	@Test
	public void testConvertSqlFormulaToStringFormatCompatible() {
		Assertions.assertEquals("CONVERT(DECIMAL(19, 6), %1$s / 1000000000.0))", SearchFormUtils.convertSqlFormulaToStringFormatCompatible("CONVERT(DECIMAL(19, 6), $1 / 1000000000.0))"));
		Assertions.assertEquals("CONVERT(DECIMAL(19, 6), %1$s / 1000000000.0))", SearchFormUtils.convertSqlFormulaToStringFormatCompatible("CONVERT(DECIMAL(19, 6), %1$s / 1000000000.0))"));
		Assertions.assertEquals("%1$s %2$s %1$s", SearchFormUtils.convertSqlFormulaToStringFormatCompatible("$1 $2 $1"));
		Assertions.assertEquals("$hello %1$s $ $", SearchFormUtils.convertSqlFormulaToStringFormatCompatible("$hello $1 $ $"));
		Assertions.assertEquals("%1$s %3$s %4$s", SearchFormUtils.convertSqlFormulaToStringFormatCompatible("$1 $3 $4"));
		Assertions.assertEquals("%2$s %3$s", SearchFormUtils.convertSqlFormulaToStringFormatCompatible("$2 $3"));
	}


	@Test
	public void testSqlFormulaMaximumNumberOfParameters() {
		Assertions.assertEquals(1, SearchFormUtils.getSqlFormulaMaximumNumberOfParameters("CONVERT(DECIMAL(19, 6), $1 / 1000000000.0))"));
		Assertions.assertEquals(0, SearchFormUtils.getSqlFormulaMaximumNumberOfParameters("CONVERT(DECIMAL(19, 6), durationNano / 1000000000.0))"));
		Assertions.assertEquals(2, SearchFormUtils.getSqlFormulaMaximumNumberOfParameters("$1 $2 $1"));
		Assertions.assertEquals(1, SearchFormUtils.getSqlFormulaMaximumNumberOfParameters("$hello $1 $ $"));
		Assertions.assertEquals(4, SearchFormUtils.getSqlFormulaMaximumNumberOfParameters("$1 $3 $4"));
		Assertions.assertEquals(3, SearchFormUtils.getSqlFormulaMaximumNumberOfParameters("$2 $3"));
	}
}
