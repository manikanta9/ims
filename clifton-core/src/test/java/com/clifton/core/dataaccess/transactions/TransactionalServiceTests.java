package com.clifton.core.dataaccess.transactions;


import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;

import javax.annotation.Resource;


/**
 * The <code>TransactionalServiceTests</code> class tests application of transactional aspects.
 * TODO: complete proper testing of transactions including commit and rollback
 *
 * @author vgomelsky
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
@Tag("memory-db") // Run with in-memory database tests so that load-time weaving for transactions is enabled; weaving is disabled during standard unit tests
public class TransactionalServiceTests {

	@Resource
	private TransactionTestService testTransactionsService;
	@Resource
	private PlatformTransactionManager transactionManager;


	@BeforeEach
	@AfterEach
	public void cleanupTransactionManager() {
		Mockito.reset(this.transactionManager);
	}


	@Test
	public void testSimpleMethod() {
		this.testTransactionsService.simpleMethod();
		Mockito.verifyZeroInteractions(this.transactionManager);
		Mockito.verify(this.transactionManager, Mockito.never()).getTransaction(ArgumentMatchers.any(TransactionDefinition.class));
	}


	@Test
	public void testTransactionalMethod() {
		this.testTransactionsService.transactionalMethod();
		Mockito.verify(this.transactionManager, Mockito.times(1)).getTransaction(ArgumentMatchers.any(TransactionDefinition.class));
		//Mockito.verify(this.transactionManager, Mockito.times(1)).commit(Matchers.any(TransactionStatus.class));
	}


	@Test
	public void testSimpleMethodCallingTransactionalMethod() {
		this.testTransactionsService.simpleMethodCallingTransactionalMethod();
		Mockito.verify(this.transactionManager, Mockito.times(1)).getTransaction(ArgumentMatchers.any(TransactionDefinition.class));
	}


	@Test
	public void testTransactionalWithExceptionMethod() {
		try {
			this.testTransactionsService.transactionalWithExceptionMethod();
			throw new RuntimeException("this code should never be reached");
		}
		catch (Exception e) {
			Mockito.verify(this.transactionManager, Mockito.times(1)).getTransaction(ArgumentMatchers.any(TransactionDefinition.class));
		}
	}
}
