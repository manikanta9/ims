package com.clifton.core.dataaccess.dao.event;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.StartEndDateEntityTestObject;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringFlexibleDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyFlexibleDaoCache;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Objects;


/**
 * The test class for base {@link SelfRegisteringFlexibleDaoCache} implementation tests.
 *
 * @author MikeH
 */
@ContextConfiguration()
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class FlexibleDaoCacheTests {

	private static final String ENTITY_KEY_PROPERTY = "name";
	private static final String ENTITY_KEY_PREFIX = "Key ";

	// Test data constants
	private static final Date BASE_DATE = DateUtils.toDate("01/01/2018");
	private static final Date DATE_RANGE_BEFORE = DateUtils.addMonths(BASE_DATE, -100);
	private static final Date DATE_RANGE_AFTER = DateUtils.addMonths(BASE_DATE, 100);
	private static final Date DATE_RANGE_1_START = DateUtils.addMonths(BASE_DATE, 0);
	private static final Date DATE_RANGE_1_END = DateUtils.addMonths(BASE_DATE, 1);
	private static final Date DATE_RANGE_1_AFTER = DateUtils.addMonths(BASE_DATE, 2);
	private static final Date DATE_RANGE_2_START = DateUtils.addMonths(BASE_DATE, 3);
	private static final Date DATE_RANGE_2_END = DateUtils.addMonths(BASE_DATE, 4);
	private static final Date DATE_RANGE_2_AFTER = DateUtils.addMonths(BASE_DATE, 5);
	private static final Date DATE_RANGE_3_START = DateUtils.addMonths(BASE_DATE, 6);
	private static final Date DATE_RANGE_3_END = DateUtils.addMonths(BASE_DATE, 7);
	private static final Date DATE_RANGE_3_AFTER = DateUtils.addMonths(BASE_DATE, 8);
	private static final Date DATE_RANGE_4_START = DateUtils.addMonths(BASE_DATE, 9);
	private static final Date DATE_RANGE_4_END = DateUtils.addMonths(BASE_DATE, 10);
	private static final Date DATE_RANGE_4_AFTER = DateUtils.addMonths(BASE_DATE, 11);

	// Test parameters
	private final List<Integer> expectedFindByFieldsKeyList = new ArrayList<>();
	private final List<Integer> expectedFindByPrimaryKeyList = new ArrayList<>();
	private final List<Integer> expectedSaveIdList = new ArrayList<>();

	// Resources
	@Resource
	@InjectMocks
	private StartEndDateEntityFlexibleDaoStartDateCache startEndDateEntityFlexibleDaoStartDateCache;
	@Resource
	@InjectMocks
	private StartEndDateEntityFlexibleDaoStartAndEndDateCache startEndDateEntityFlexibleDaoStartAndEndDateCache;
	@Mock
	private UpdatableDAO<StartEndDateEntityTestObject> startEndDateEntityTestObjectDaoMock;

	@Resource
	private UpdatableDAO<StartEndDateEntityTestObject> startEndDateEntityTestObjectDAO;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setUp() {
		MockitoAnnotations.initMocks(this);
		this.startEndDateEntityFlexibleDaoStartDateCache.getCacheHandler().clearAll();
		this.startEndDateEntityFlexibleDaoStartAndEndDateCache.getCacheHandler().clearAll();

		// Pass through executions from mock to real implementation; use mock for analysis only
		Mockito.when(this.startEndDateEntityTestObjectDaoMock.findByFields(ArgumentMatchers.any(), ArgumentMatchers.any()))
				.thenAnswer(args -> args.getMethod().invoke(this.startEndDateEntityTestObjectDAO, args.getArguments()));
		Mockito.when(this.startEndDateEntityTestObjectDaoMock.findByPrimaryKey(ArgumentMatchers.any()))
				.thenAnswer(args -> args.getMethod().invoke(this.startEndDateEntityTestObjectDAO, args.getArguments()));
		Mockito.when(this.startEndDateEntityTestObjectDaoMock.save(ArgumentMatchers.any()))
				.thenAnswer(args -> args.getMethod().invoke(this.startEndDateEntityTestObjectDAO, args.getArguments()));
		Mockito.when(this.startEndDateEntityTestObjectDaoMock.getConfiguration())
				.thenAnswer(args -> args.getMethod().invoke(this.startEndDateEntityTestObjectDAO, args.getArguments()));
	}


	@AfterEach
	public void tearDown() {
		// Validate method invocations
		InOrder findByFieldsInOrder = Mockito.inOrder(this.startEndDateEntityTestObjectDaoMock);
		InOrder findByPrimaryKeyInOrder = Mockito.inOrder(this.startEndDateEntityTestObjectDaoMock);
		InOrder saveInOrder = Mockito.inOrder(this.startEndDateEntityTestObjectDaoMock);
		this.expectedFindByFieldsKeyList.forEach(entityKey -> findByFieldsInOrder.verify(this.startEndDateEntityTestObjectDaoMock, Mockito.calls(1))
				.findByFields(
						ArgumentMatchers.eq(new String[]{ENTITY_KEY_PROPERTY}),
						ArgumentMatchers.eq(new String[]{getObjectKey(entityKey)})
				));
		this.expectedFindByPrimaryKeyList.forEach(primaryKey -> findByPrimaryKeyInOrder.verify(this.startEndDateEntityTestObjectDaoMock, Mockito.calls(1))
				.findByPrimaryKey(ArgumentMatchers.eq(primaryKey)));
		this.expectedSaveIdList.forEach(entityId -> saveInOrder.verify(this.startEndDateEntityTestObjectDaoMock, Mockito.calls(1))
				.save(ArgumentMatchers.argThat(arg -> Objects.equals(entityId, arg.getId()))));

		// Ignore all getConfiguration calls
		Mockito.verify(this.startEndDateEntityTestObjectDaoMock, Mockito.atLeast(0)).getConfiguration();
		Mockito.verifyNoMoreInteractions(this.startEndDateEntityTestObjectDaoMock);
		Mockito.validateMockitoUsage();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Start Date Cache Tests                          ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testStartDateCacheStandardOrder() {
		expectFindByFieldsUsage(1);
		expectFindByPrimaryKeyUsage(1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_AFTER);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_END);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_AFTER);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_END);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_AFTER);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_AFTER);
	}


	@Test
	public void testStartDateCacheReverseOrder() {
		expectFindByFieldsUsage(1);
		expectFindByPrimaryKeyUsage(4, 4, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_AFTER);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_END);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_AFTER);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_END);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_AFTER);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
	}


	@Test
	public void testStartDateCacheScatteredOrder() {
		expectFindByFieldsUsage(1);
		expectFindByPrimaryKeyUsage(3, 4, 1, 2, 4, 4, 1, 2, 3, 1, 4, 2, 3);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_AFTER);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_END);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_AFTER);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_END);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartDate(null, 1, DATE_RANGE_BEFORE);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_AFTER);
	}


	@Test
	public void testStartDateCacheMixedKeys() {
		expectFindByFieldsUsage(2, 1);
		expectFindByPrimaryKeyUsage(5, 1, 6, 2, 7, 3, 8, 4, 5, 1, 8, 4);
		assertObjectIdForStartDate(null, 2, DATE_RANGE_BEFORE);
		assertObjectIdForStartDate(null, 1, DATE_RANGE_BEFORE);
		assertObjectIdForStartDate(5, 2, DATE_RANGE_1_START);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartDate(6, 2, DATE_RANGE_2_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartDate(7, 2, DATE_RANGE_3_START);
		assertObjectIdForStartDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartDate(8, 2, DATE_RANGE_4_START);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartDate(5, 2, DATE_RANGE_1_START);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartDate(8, 2, DATE_RANGE_AFTER);
		assertObjectIdForStartDate(4, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartDate(null, 2, DATE_RANGE_BEFORE);
		assertObjectIdForStartDate(null, 1, DATE_RANGE_BEFORE);
	}


	@Test
	public void testStartDateCacheMissing() {
		expectFindByFieldsUsage(1);
		assertObjectIdForStartDate(null, 1, DATE_RANGE_BEFORE);
	}


	@Test
	public void testStartDateCacheMissingStrict() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			expectFindByFieldsUsage(1);
			getEntityForStartDateStrict(1, DATE_RANGE_BEFORE);
		});
		Assertions.assertEquals(String.format("Unable to find Start End Date Entity Test Object with properties [name] and value(s) [Key 1] for order value [%s].", DATE_RANGE_BEFORE), validationException.getMessage());
	}


	@Test
	public void testStartDateCacheEviction() {
		expectFindByFieldsUsage(1, 1);
		expectFindByPrimaryKeyUsage(1, 1, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2);
		expectSaveUsage(2);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_AFTER);
		StartEndDateEntityTestObject modifiedEntity = assertObjectIdForStartDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_AFTER);
		updateAndSaveStartDate(modifiedEntity, DATE_RANGE_1_END);
		assertObjectIdForStartDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_1_END);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartDate(2, 1, DATE_RANGE_2_AFTER);
	}


	@Test
	public void testStartDateCacheMissingStartDate() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			expectFindByFieldsUsage(3);
			getEntityForStartDate(3, DATE_RANGE_1_START);
		});
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("Entities were found with missing order property [startDate]. This field is required for self-registering flexible cache entries. Invalid entity list: "));
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("{id=9,label=Key 3}]."));
	}


	@Test
	public void testStartDateCacheMissingEndDate() {
		expectFindByFieldsUsage(4);
		expectFindByPrimaryKeyUsage(10, 10, 11);
		assertObjectIdForStartDate(null, 4, DATE_RANGE_BEFORE);
		assertObjectIdForStartDate(10, 4, DATE_RANGE_1_START);
		assertObjectIdForStartDate(10, 4, DATE_RANGE_1_AFTER);
		assertObjectIdForStartDate(11, 4, DATE_RANGE_AFTER);
	}


	@Test
	public void testStartDateCacheMissingStartAndEndDate() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			expectFindByFieldsUsage(5);
			getEntityForStartDate(5, DATE_RANGE_1_START);
		});
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("Entities were found with missing order property [startDate]. This field is required for self-registering flexible cache entries. Invalid entity list: "));
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("{id=12,label=Key 5}]."));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Start And End Date Cache Tests                  ////////
	////////////////////////////////////////////////////////////////////////////


	/*
	 * Note: Calls to post-max-end-date cache retrievals also make calls to findByPrimaryKey for the latest entity so that the end date can be validated, even though the cache
	 * query produces no actual entity.
	 */


	@Test
	public void testStartAndEndDateCacheStandardOrder() {
		expectFindByFieldsUsage(1);
		expectFindByPrimaryKeyUsage(1, 1, 1, 2, 2, 2, 3, 3, 3, 4, 4, 4, 4);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_2_AFTER);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_3_AFTER);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_4_AFTER);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_AFTER);
	}


	@Test
	public void testStartAndEndDateCacheReverseOrder() {
		expectFindByFieldsUsage(1);
		expectFindByPrimaryKeyUsage(4, 4, 4, 4, 3, 3, 3, 2, 2, 2, 1, 1, 1);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_4_AFTER);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_END);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_3_AFTER);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_END);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_2_AFTER);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
	}


	@Test
	public void testStartAndEndDateCacheScatteredOrder() {
		expectFindByFieldsUsage(1);
		expectFindByPrimaryKeyUsage(3, 4, 1, 2, 4, 4, 1, 2, 3, 1, 4, 2, 3);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_4_AFTER);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_2_AFTER);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_3_AFTER);
	}


	@Test
	public void testStartAndEndDateCacheMixedKeys() {
		expectFindByFieldsUsage(2, 1);
		expectFindByPrimaryKeyUsage(5, 1, 6, 2, 7, 3, 8, 4, 5, 1, 8, 4);
		assertObjectIdForStartAndEndDate(null, 2, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(5, 2, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(6, 2, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(7, 2, DATE_RANGE_3_START);
		assertObjectIdForStartAndEndDate(3, 1, DATE_RANGE_3_START);
		assertObjectIdForStartAndEndDate(8, 2, DATE_RANGE_4_START);
		assertObjectIdForStartAndEndDate(4, 1, DATE_RANGE_4_START);
		assertObjectIdForStartAndEndDate(5, 2, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(null, 2, DATE_RANGE_AFTER);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartAndEndDate(null, 2, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_BEFORE);
	}


	@Test
	public void testStartAndEndDateCacheMissing() {
		expectFindByFieldsUsage(1, 2);
		expectFindByPrimaryKeyUsage(4, 8);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_AFTER);
		assertObjectIdForStartAndEndDate(null, 2, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(null, 2, DATE_RANGE_AFTER);
	}


	@Test
	public void testStartAndEndDateCacheMissingStrictBefore() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			expectFindByFieldsUsage(1);
			getEntityForStartAndEndDateStrict(1, DATE_RANGE_BEFORE);
		});
		Assertions.assertEquals(String.format("Unable to find Start End Date Entity Test Object with properties [name] and value(s) [Key 1] for order value [%s].", DATE_RANGE_BEFORE), validationException.getMessage());
	}


	@Test
	public void testStartAndEndDateCacheMissingStrictAfter() {
		ValidationException validationException = Assertions.assertThrows(ValidationException.class, () -> {
			expectFindByFieldsUsage(1);
			expectFindByPrimaryKeyUsage(4);
			getEntityForStartAndEndDateStrict(1, DATE_RANGE_AFTER);
		});
		Assertions.assertEquals(String.format("Unable to find Start End Date Entity Test Object with properties [name] and value(s) [Key 1] for order value [%s].", DATE_RANGE_AFTER), validationException.getMessage());
	}


	@Test
	public void testStartAndEndDateCacheEviction() {
		expectFindByFieldsUsage(1, 1);
		expectFindByPrimaryKeyUsage(1, 1, 1, 2, 2, 2, 1, 2, 2, 2, 2, 2);
		expectSaveUsage(2);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_1_AFTER);
		StartEndDateEntityTestObject modifiedEntity = assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_2_AFTER);
		updateAndSaveStartDate(modifiedEntity, DATE_RANGE_1_END);
		assertObjectIdForStartAndEndDate(1, 1, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_1_END);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_1_AFTER);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_START);
		assertObjectIdForStartAndEndDate(2, 1, DATE_RANGE_2_END);
		assertObjectIdForStartAndEndDate(null, 1, DATE_RANGE_2_AFTER);
	}


	@Test
	public void testStartAndEndDateCacheMissingStartDate() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			expectFindByFieldsUsage(3);
			getEntityForStartAndEndDate(3, DATE_RANGE_1_START);
		});
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("Entities were found with missing order property [startDate]. This field is required for self-registering flexible cache entries. Invalid entity list: "));
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("{id=9,label=Key 3}]."));
	}


	@Test
	public void testStartAndEndDateCacheMissingEndDate() {
		expectFindByFieldsUsage(4);
		expectFindByPrimaryKeyUsage(10, 10, 11);
		assertObjectIdForStartAndEndDate(null, 4, DATE_RANGE_BEFORE);
		assertObjectIdForStartAndEndDate(10, 4, DATE_RANGE_1_START);
		assertObjectIdForStartAndEndDate(null, 4, DATE_RANGE_1_AFTER);
		assertObjectIdForStartAndEndDate(11, 4, DATE_RANGE_AFTER);
	}


	@Test
	public void testStartAndEndDateCacheMissingStartAndEndDate() {
		RuntimeException runtimeException = Assertions.assertThrows(RuntimeException.class, () -> {
			expectFindByFieldsUsage(5);
			getEntityForStartAndEndDate(5, DATE_RANGE_1_START);
		});
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("Entities were found with missing order property [startDate]. This field is required for self-registering flexible cache entries. Invalid entity list: "));
		MatcherAssert.assertThat(runtimeException.getMessage(), StringContains.containsString("{id=12,label=Key 5}]."));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getObjectKey(int objectKey) {
		return ENTITY_KEY_PREFIX + objectKey;
	}


	private StartEndDateEntityTestObject getEntityForStartDate(int objectKey, Date date) {
		return this.startEndDateEntityFlexibleDaoStartDateCache.getBeanForKeyValue(this.startEndDateEntityTestObjectDaoMock, date, getObjectKey(objectKey));
	}


	private StartEndDateEntityTestObject getEntityForStartDateStrict(int objectKey, Date date) {
		return this.startEndDateEntityFlexibleDaoStartDateCache.getBeanForKeyValueStrict(this.startEndDateEntityTestObjectDaoMock, date, getObjectKey(objectKey));
	}


	private StartEndDateEntityTestObject getEntityForStartAndEndDate(int objectKey, Date date) {
		return this.startEndDateEntityFlexibleDaoStartAndEndDateCache.getBeanForKeyValue(this.startEndDateEntityTestObjectDaoMock, date, getObjectKey(objectKey));
	}


	private StartEndDateEntityTestObject getEntityForStartAndEndDateStrict(int objectKey, Date date) {
		return this.startEndDateEntityFlexibleDaoStartAndEndDateCache.getBeanForKeyValueStrict(this.startEndDateEntityTestObjectDaoMock, date, getObjectKey(objectKey));
	}


	private void updateAndSaveStartDate(StartEndDateEntityTestObject entity, Date date) {
		entity.setStartDate(date);
		this.startEndDateEntityTestObjectDaoMock.save(entity);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private StartEndDateEntityTestObject assertObjectIdForStartDate(Integer expectedId, int objectKey, Date date) {
		StartEndDateEntityTestObject bean = getEntityForStartDate(objectKey, date);
		Assertions.assertEquals(expectedId, BeanUtils.getBeanIdentity(bean), String.format("Expected bean with ID [%d]. Actual: [%s].", expectedId, bean));
		return bean;
	}


	private StartEndDateEntityTestObject assertObjectIdForStartAndEndDate(Integer expectedId, int objectKey, Date date) {
		StartEndDateEntityTestObject bean = getEntityForStartAndEndDate(objectKey, date);
		Assertions.assertEquals(expectedId, BeanUtils.getBeanIdentity(bean), String.format("Expected bean with ID [%d]. Actual: [%s].", expectedId, bean));
		return bean;
	}


	private void expectFindByFieldsUsage(Integer... entityKeys) {
		this.expectedFindByFieldsKeyList.addAll(Arrays.asList(entityKeys));
	}


	private void expectFindByPrimaryKeyUsage(Integer... primaryKeys) {
		this.expectedFindByPrimaryKeyList.addAll(Arrays.asList(primaryKeys));
	}


	private void expectSaveUsage(Integer... entityIds) {
		this.expectedSaveIdList.addAll(Arrays.asList(entityIds));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class StartEndDateEntityFlexibleDaoStartDateCache extends SelfRegisteringSingleKeyFlexibleDaoCache<StartEndDateEntityTestObject, Date, String> {

		@Override
		protected String getBeanKeyProperty() {
			return ENTITY_KEY_PROPERTY;
		}


		@Override
		protected String getBeanKeyValue(StartEndDateEntityTestObject bean) {
			return bean.getName();
		}


		@Override
		protected String getBeanOrderProperty() {
			return "startDate";
		}


		@Override
		protected Date getBeanOrderValue(StartEndDateEntityTestObject bean) {
			return bean.getStartDate();
		}
	}


	private static class StartEndDateEntityFlexibleDaoStartAndEndDateCache extends SelfRegisteringSingleKeyFlexibleDaoCache<StartEndDateEntityTestObject, Date, String> {

		@Override
		protected String getBeanKeyProperty() {
			return ENTITY_KEY_PROPERTY;
		}


		@Override
		protected String getBeanKeyValue(StartEndDateEntityTestObject bean) {
			return bean.getName();
		}


		@Override
		protected String getBeanOrderProperty() {
			return "startDate";
		}


		@Override
		protected Date getBeanOrderValue(StartEndDateEntityTestObject bean) {
			return bean.getStartDate();
		}


		@Override
		protected String getBeanEndOrderProperty() {
			return "endDate";
		}


		@Override
		protected Date getBeanEndOrderValue(StartEndDateEntityTestObject bean) {
			return bean.getEndDate();
		}
	}
}
