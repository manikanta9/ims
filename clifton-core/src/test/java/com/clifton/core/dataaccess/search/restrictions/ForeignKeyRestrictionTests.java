package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.FieldValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Field;


public class ForeignKeyRestrictionTests {

	@Test
	public void testLongRestriction() {
		SearchRestrictionDefinition definition = getRestrictionDefinitionForField("foreignKeyId");
		SearchRestriction restriction = new SearchRestriction();
		restriction.setField("foreignKeyId");
		assert definition != null;
		restriction.setComparison(CollectionUtils.getFirstElementStrict(definition.getSearchConditionList()));
		restriction.setValue("1234");
		Object value = definition.getValue(restriction.getValue());
		Assertions.assertEquals(Long.class, value.getClass());
		Assertions.assertEquals(Long.parseLong("1234"), ((Long) value).longValue());
	}


	@Test
	public void testIntegerRestriction() {
		SearchRestrictionDefinition definition = getRestrictionDefinitionForField("intAmount");
		SearchRestriction restriction = new SearchRestriction();
		restriction.setField("intAmount");
		assert definition != null;
		restriction.setComparison(CollectionUtils.getFirstElementStrict(definition.getSearchConditionList()));
		restriction.setValue("1");
		Object value = definition.getValue(restriction.getValue());
		Assertions.assertEquals(Integer.class, value.getClass());
		Assertions.assertEquals(Integer.parseInt("1"), ((Integer) value).intValue());
	}


	@Test
	public void testInvalidValue() {
		Assertions.assertThrows(FieldValidationException.class, () -> {
			SearchRestrictionDefinition definition = getRestrictionDefinitionForField("intAmount");
			SearchRestriction restriction = new SearchRestriction();
			restriction.setField("intAmount");
			assert definition != null;
			restriction.setComparison(CollectionUtils.getFirstElementStrict(definition.getSearchConditionList()));
			restriction.setValue("5");
			definition.validate(restriction);
		});
	}


	private SearchRestrictionDefinition getRestrictionDefinitionForField(String fieldName) {
		try {
			TestSearchForm searchForm = new TestSearchForm();
			Field field = searchForm.getClass().getField(fieldName);
			SearchField searchField = AnnotationUtils.getAnnotation(field, SearchField.class);
			String searchFieldName = SearchField.SAME_AS_MARKED_FIELD.equals(searchField.searchField()) ? field.getName() : searchField.searchField();
			String searchFieldPath = SearchField.SAME_TABLE.equals(searchField.searchFieldPath()) ? null : searchField.searchFieldPath();
			String sortFieldName = searchField.sortField();
			if (SearchField.SORT_NOT_ALLOWED.equals(sortFieldName)) {
				sortFieldName = null;
			}
			else if (SearchField.SORT_BY_SEARCH_FIELD.equals(sortFieldName)) {
				sortFieldName = searchFieldName;
			}
			if (field.getType().equals(Integer.class)) {
				return new ForeignKeyRestrictionDefinition<>(new IntegerRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, false, sortFieldName, searchField.required()),
						new Integer[]{1, 2, 3});
			}
			else if (field.getType().equals(Long.class)) {
				return new ForeignKeyRestrictionDefinition<Long>(new LongRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, false, sortFieldName, searchField.required()), null);
			}
		}
		catch (SecurityException | NoSuchFieldException e) {
			e.printStackTrace();
			Assertions.fail();
		}
		return null;
	}


	@SearchForm(hasOrmDtoClass = false)
	public class TestSearchForm extends BaseAuditableEntitySearchForm {

		@SearchField(searchField = "foreignKey.id")
		public Long foreignKeyId;

		@SearchField
		public Integer intAmount;
	}
}
