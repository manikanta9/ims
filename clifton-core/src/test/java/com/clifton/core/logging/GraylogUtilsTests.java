package com.clifton.core.logging;

import com.clifton.core.logging.message.graylog.GraylogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Date;


/**
 * @author theodorez
 */
public class GraylogUtilsTests {


	@Test
	public void testGetDateFromGraylogTime() {
		String testString = "2017-09-28T16:34:48.475Z";
		Date graylogDate = GraylogUtils.getDateFromGraylogTime(testString);
		Assertions.assertTrue(DateUtils.getMonthOfYear(graylogDate) == 9);
		Assertions.assertTrue(DateUtils.getDayOfMonth(graylogDate) == 28);
		Assertions.assertTrue(DateUtils.getYear(graylogDate) == 2017);
		Assertions.assertTrue(DateUtils.getHours(graylogDate) == 11);
		Assertions.assertTrue(DateUtils.getMinutes(graylogDate) == 34);
		Assertions.assertTrue(DateUtils.getSeconds(graylogDate) == 48);
		Assertions.assertTrue(DateUtils.getMilliseconds(graylogDate) == 475);
	}


	@Test
	public void testGetDateStringInGraylogTime() {
		String expectedString = "2017-09-28T16:34:48.475Z";
		Date graylogDate = GraylogUtils.getDateFromGraylogTime(expectedString);
		String result = GraylogUtils.getDateStringInGraylogTime(graylogDate);
		Assertions.assertTrue(StringUtils.isEqual(expectedString, result));
	}
}
