package com.clifton.core.beans;

import com.clifton.core.beans.common.AddressObject;
import com.clifton.core.beans.document.DocumentAware;
import com.clifton.core.beans.hierarchy.HierarchicalEntity;
import com.clifton.core.beans.hierarchy.HierarchicalObject;
import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;
import java.util.function.Function;


/**
 * The <code>BeanUtilsTests</code> class defines unit tests for {@link BeanUtils}.
 *
 * @author vgomelsky
 */
public class BeanUtilsTests {

	@Test
	public void testSetPropertyValue() throws Exception {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		BeanUtils.setPropertyValue(bean, "name", "test");
		Assertions.assertEquals("test", bean.getName());

		BeanUtils.setPropertyValue(bean, "createUserId", 7);
		Assertions.assertEquals(7, bean.getCreateUserId());

		BeanUtils.setPropertyValue(bean, "createUserId", "888");
		Assertions.assertEquals(888, bean.getCreateUserId());

		BeanUtils.setPropertyValue(bean, "testBoolean", "true");
		Assertions.assertTrue(bean.isTestBoolean());

		BeanUtils.setPropertyValue(bean, "testBoolean", "false");
		Assertions.assertFalse(bean.isTestBoolean());

		BeanUtils.setPropertyValue(bean, "items[0].name", "items[0].name");
		List<BeanUtilsTestBean> items = bean.getItems();
		Assertions.assertEquals("items[0].name", items.get(0).getName());

		BeanUtils.setPropertyValue(bean, "enumValue", "HOT");
		Assertions.assertEquals(BeanUtilsTestEnum.HOT, bean.getEnumValue());

		BeanUtils.setPropertyValue(bean, "bigDecimal", "3.14");
		Assertions.assertEquals(new BigDecimal("3.14"), bean.getBigDecimal());

		BeanUtils.setPropertyValue(bean, "date", "1/1/2000");
		SimpleDateFormat formatter = new SimpleDateFormat("mm/dd/yyyy");
		Date parsedDate = formatter.parse("1/1/2000");
		Assertions.assertEquals(DateUtils.clearTime(parsedDate), bean.getDate());

		BeanUtils.setPropertyValue(bean, "numbers", "1,2");
		Assertions.assertEquals(1, bean.getNumbers()[0]);
		Assertions.assertEquals(2, bean.getNumbers()[1]);
	}


	@Test
	public void testSetPropertyValueIfCurrentlyNull() throws Exception {
		// Set properties
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		BeanUtils.setPropertyValueIfNotSet(bean, "name", "test", true);
		Assertions.assertEquals("test", bean.getName());

		BeanUtils.setPropertyValueIfNotSet(bean, "createUserId", 7, true);
		Assertions.assertEquals(7, bean.getCreateUserId());

		BeanUtils.setPropertyValueIfNotSet(bean, "createUserId", "888", true);
		Assertions.assertEquals(7, bean.getCreateUserId());

		BeanUtils.setPropertyValueIfNotSet(bean, "testBoolean", "true", true);
		Assertions.assertTrue(bean.isTestBoolean());

		BeanUtils.setPropertyValueIfNotSet(bean, "testBoolean", "false", true);
		Assertions.assertTrue(bean.isTestBoolean());

		BeanUtils.setPropertyValueIfNotSet(bean, "items[0].name", "items[0].name", true);
		List<BeanUtilsTestBean> items = bean.getItems();
		Assertions.assertEquals("items[0].name", items.get(0).getName());

		BeanUtils.setPropertyValueIfNotSet(bean, "enumValue", "HOT", true);
		Assertions.assertEquals(BeanUtilsTestEnum.HOT, bean.getEnumValue());

		BeanUtils.setPropertyValueIfNotSet(bean, "bigDecimal", "3.14", true);
		Assertions.assertEquals(new BigDecimal("3.14"), bean.getBigDecimal());

		BeanUtils.setPropertyValueIfNotSet(bean, "date", "1/1/2000", true);
		SimpleDateFormat formatter = new SimpleDateFormat("mm/dd/yyyy");
		Date parsedDate = formatter.parse("1/1/2000");
		Assertions.assertEquals(DateUtils.clearTime(parsedDate), bean.getDate());

		BeanUtils.setPropertyValueIfNotSet(bean, "numbers", "1,2", true);
		Assertions.assertEquals(1, bean.getNumbers()[0]);
		Assertions.assertEquals(2, bean.getNumbers()[1]);

		// validate no properties are updated
		BeanUtils.setPropertyValueIfNotSet(bean, "name", "blah", true);
		Assertions.assertEquals("test", bean.getName());

		BeanUtils.setPropertyValueIfNotSet(bean, "items[0].name", "items[0].description", true);
		items = bean.getItems();
		Assertions.assertEquals("items[0].name", items.get(0).getName());

		BeanUtils.setPropertyValueIfNotSet(bean, "enumValue", "COLD", true);
		Assertions.assertEquals(BeanUtilsTestEnum.HOT, bean.getEnumValue());

		BeanUtils.setPropertyValueIfNotSet(bean, "bigDecimal", "3.15", true);
		Assertions.assertEquals(new BigDecimal("3.14"), bean.getBigDecimal());

		BeanUtils.setPropertyValueIfNotSet(bean, "date", "1/2/2000", true);
		Assertions.assertEquals(DateUtils.clearTime(parsedDate), bean.getDate());

		BeanUtils.setPropertyValueIfNotSet(bean, "numbers", "2,1", true);
		Assertions.assertEquals(1, bean.getNumbers()[0]);
		Assertions.assertEquals(2, bean.getNumbers()[1]);
	}


	@Test
	public void testIsPropertyValueDefault() {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		List<String> propertyNameList = BeanUtils.getPropertyNames(bean.getClass(), true, true);
		propertyNameList.forEach(name -> Assertions.assertTrue(BeanUtils.isPropertyValueDefault(bean, name, true), "Property is not default: " + name));
		bean.setTestBoolean(true);
		bean.setTestGetterSetterFieldSetter(true);
		bean.setTestJsonExclusionFilterOnField(true);
		bean.setTestJsonExclusionFilterOnGetter(true);
		bean.setItems(new ArrayList<>());
		bean.setParent(new BeanUtilsTestBean());
		bean.setEnumValue(BeanUtilsTestEnum.COLD);
		bean.setBigDecimal(BigDecimal.ZERO);
		bean.setDate(new Date());
		bean.setNumbers(new int[0]);
		bean.setCreateDate(bean.getDate());
		bean.setCreateUserId((short) 5);
		bean.setUpdateDate(bean.getDate());
		bean.setUpdateUserId(bean.getCreateUserId());
		bean.setId(1);
		bean.setName("test");
		bean.setDescription(bean.getName());
		bean.setRv(new byte[0]);
		propertyNameList.forEach(name -> Assertions.assertFalse(BeanUtils.isPropertyValueDefault(bean, name, true), String.format("Property is default: %s", name)));
	}


	@Test
	public void testIsPropertyPresent() {
		Assertions.assertTrue(BeanUtils.isPropertyPresent(BeanUtilsTestBean.class, "parent.parent.testBoolean"));
		Assertions.assertTrue(BeanUtils.isPropertyPresent(BeanUtilsTestBean.class, "testBoolean"));
		Assertions.assertFalse(BeanUtils.isPropertyPresent(BeanUtilsTestBean.class, "parent.parent.nope"));
		Assertions.assertFalse(BeanUtils.isPropertyPresent(BeanUtilsTestBean.class, "nope"));
	}


	@Test
	public void testGetBeanPropertyType() {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		Assertions.assertEquals("com.clifton.core.beans.BeanUtilsTestBean", BeanUtils.getPropertyType(bean, "items[0]").getName());

		Assertions.assertEquals("java.lang.String", BeanUtils.getPropertyType(bean, "items[0].name").getName());

		Assertions.assertEquals("java.lang.String", BeanUtils.getPropertyType(bean, "items[0].items[0].name").getName());
	}


	@Test
	public void testSetPropertyValueIllegalFormat() {
		Assertions.assertThrows(Exception.class, () -> BeanUtils.setPropertyValue(new NamedEntity<Integer>(), "createUserId", "abc"));
	}


	/**
	 * Should still throw an exception, because it's not a writable exception
	 */
	@Test
	public void testSetPropertyValueIllegalFormat_IgnoreWritableException() {
		Assertions.assertThrows(Exception.class, () -> BeanUtils.setPropertyValue(new NamedEntity<Integer>(), "createUserId", "abc"));
	}


	@Test
	public void testSetPropertyValue_NotWritableException() {
		Assertions.assertThrows(Exception.class, () -> BeanUtils.setPropertyValue(new BeanUtilsTestBean(), "itemCount", 5, null, false));
	}


	@Test
	public void testSetPropertyValue_IgnoreNotWritableException() {
		BeanUtilsTestBean testBean = new BeanUtilsTestBean();

		// No exception - and value should be zero
		BeanUtils.setPropertyValue(testBean, "itemCount", 5, null, true);
		Assertions.assertEquals(new Integer(0), testBean.getItemCount());
	}


	@Test
	public void testGetSetPropertyNestedValue() {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();

		// Verify nulls remain as null
		Assertions.assertNull(BeanUtils.getPropertyValue(bean, "name"));
		Assertions.assertNull(BeanUtils.getPropertyValue(bean, "parent.name"));
		Assertions.assertNull(BeanUtils.getPropertyValue(bean, "parent"));

		// Set values
		BeanUtils.setPropertyValue(bean, "name", "test");
		BeanUtils.setPropertyValue(bean, "parent.name", "test's parent");

		// Verify set values are now returned
		Assertions.assertEquals("test", BeanUtils.getPropertyValue(bean, "name"));
		Assertions.assertEquals("test's parent", BeanUtils.getPropertyValue(bean, "parent.name"));
	}


	@Test
	public void testGetPropertyValues() {
		NamedEntity<Integer> entity = new NamedEntity<>();
		entity.setName("Test");
		List<NamedEntity<Integer>> list = new ArrayList<>();

		list.add(entity);
		Assertions.assertEquals("'Test'", BeanUtils.getPropertyValues(list, "name", ", ", "'", "'", ", ...", 2));

		list.add(entity);
		Assertions.assertEquals("'Test', 'Test'", BeanUtils.getPropertyValues(list, "name", ", ", "'", "'", ", ...", 2));

		list.add(entity);
		Assertions.assertEquals("'Test', 'Test', ...", BeanUtils.getPropertyValues(list, "name", ", ", "'", "'", ", ...", 2));
	}


	@Test
	public void testGetPropertyNames() {
		Collection<String> propertyNames;
		Collection<String> expected;

		propertyNames = new HashSet<>(BeanUtils.getPropertyNames(BeanUtilsTestBean.class, false, false));
		expected = getBeanUtilsTestBeanPropertyNames(false, false, false);
		Assertions.assertEquals(expected, propertyNames);

		propertyNames = new HashSet<>(BeanUtils.getPropertyNames(BeanUtilsTestBean.class, true, false));
		expected = getBeanUtilsTestBeanPropertyNames(true, false, false);
		Assertions.assertEquals(expected, propertyNames);

		propertyNames = new HashSet<>(BeanUtils.getPropertyNames(BeanUtilsTestBean.class, false, true));
		expected = getBeanUtilsTestBeanPropertyNames(false, true, false);
		Assertions.assertEquals(expected, propertyNames);

		propertyNames = new HashSet<>(BeanUtils.getPropertyNames(BeanUtilsTestBean.class, true, true));
		expected = getBeanUtilsTestBeanPropertyNames(true, true, false);
		Assertions.assertEquals(expected, propertyNames);
	}


	@Test
	public void testGetPropertyValuesExcludeNullObjectsByFunction() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			NamedEntity<Integer> entity = new NamedEntity<>();
			if (i % 2 == 0) {
				entity.setName("Test" + i);
				entity.setId(i);
			}
			else {
				entity.setName(null);
				entity.setId(null);
			}
			list.add(entity);
		}
		Object[] nameList = BeanUtils.getPropertyValuesExcludeNull(list, NamedEntity::getName);
		Assertions.assertEquals(5, nameList.length);
		for (int i = 0; i < nameList.length; i++) {
			Assertions.assertEquals("Test" + i * 2, nameList[i]);
		}

		Object[] idList = BeanUtils.getPropertyValuesExcludeNull(list, NamedEntity::getId);
		Assertions.assertEquals(5, idList.length);
		for (int i = 0; i < idList.length; i++) {
			Assertions.assertEquals(i * 2, idList[i]);
		}

		list = new ArrayList<>();
		nameList = BeanUtils.getPropertyValuesExcludeNull(list, NamedEntity::getName);
		Assertions.assertEquals(0, nameList.length);

		list = null;
		nameList = BeanUtils.getPropertyValuesExcludeNull(list, NamedEntity::getName);
		Assertions.assertEquals(0, nameList.length);
	}


	@Test
	public void testGetPropertyValuesObjectsByFunction() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			NamedEntity<Integer> entity = new NamedEntity<>();
			entity.setName("Test" + i);
			entity.setId(i);
			list.add(entity);
		}
		Object[] nameList = BeanUtils.getPropertyValues(list, NamedEntity::getName);
		Assertions.assertEquals(10, nameList.length);
		for (int i = 0; i < nameList.length; i++) {
			Assertions.assertEquals("Test" + i, nameList[i]);
		}

		Object[] idList = BeanUtils.getPropertyValues(list, NamedEntity::getId);
		Assertions.assertEquals(10, idList.length);
		for (int i = 0; i < idList.length; i++) {
			Assertions.assertEquals(i, idList[i]);
		}

		list = new ArrayList<>();
		nameList = BeanUtils.getPropertyValues(list, NamedEntity::getName);
		Assertions.assertEquals(0, nameList.length);

		list = null;
		nameList = BeanUtils.getPropertyValues(list, NamedEntity::getName);
		Assertions.assertEquals(0, nameList.length);
	}


	@Test
	public void testGetPropertyValuesArray() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 10; i++) {
			NamedEntity<Integer> entity = new NamedEntity<>();
			entity.setName("Test" + i);
			entity.setId(i);
			list.add(entity);
		}
		String[] nameList = BeanUtils.getPropertyValues(list, NamedEntity::getName, String.class);
		Assertions.assertEquals(10, nameList.length);
		for (int i = 0; i < nameList.length; i++) {
			Assertions.assertEquals("Test" + i, nameList[i]);
		}

		Integer[] idList = BeanUtils.getPropertyValues(list, NamedEntity::getId, Integer.class);
		Assertions.assertEquals(10, idList.length);
		for (int i = 0; i < idList.length; i++) {
			Assertions.assertEquals(i, idList[i]);
		}

		list = new ArrayList<>();
		nameList = BeanUtils.getPropertyValues(list, NamedEntity::getName, String.class);
		Assertions.assertEquals(0, nameList.length);

		list = null;
		nameList = BeanUtils.getPropertyValues(list, NamedEntity::getName, String.class);
		Assertions.assertEquals(0, nameList.length);
	}


	@Test
	public void testGetPropertyValuesUniqueExcludeNull() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			NamedEntity<Integer> entity = new NamedEntity<>();
			entity.setName("Test" + (i < 10 ? i : i - 10));
			entity.setId(i);
			list.add(entity);
		}
		String[] nameList = BeanUtils.getPropertyValuesUniqueExcludeNull(list, NamedEntity::getName, String.class);
		Assertions.assertEquals(10, nameList.length);
		for (int i = 0; i < nameList.length; i++) {
			Assertions.assertEquals("Test" + i, nameList[i]);
		}

		list = new ArrayList<>();
		for (int i = 0; i < 20; i++) {
			NamedEntity<Integer> entity = new NamedEntity<>();
			if ((i % 2) == 0) {
				entity.setName("Test" + i);
			}
			entity.setId(i);
			list.add(entity);
		}
		nameList = BeanUtils.getPropertyValuesUniqueExcludeNull(list, NamedEntity::getName, String.class);
		Assertions.assertEquals(10, nameList.length);
		for (int i = 0; i < nameList.length; i++) {
			Assertions.assertEquals("Test" + i * 2, nameList[i]);
		}
	}


	@Test
	public void testCopyProperty() {
		NamedEntity<Integer> from = new NamedEntity<>();
		NamedEntity<Integer> to = new NamedEntity<>();

		from.setName("TEST");
		Assertions.assertNull(to.getName());
		Assertions.assertTrue(BeanUtils.copyProperty(from, to, NamedEntity::getName, NamedEntity::setName, false));
		Assertions.assertEquals("TEST", to.getName());

		Assertions.assertFalse(BeanUtils.copyProperty(from, to, NamedEntity::getName, NamedEntity::setName, false));

		from.setName("TEST2");
		Assertions.assertFalse(BeanUtils.copyProperty(from, to, NamedEntity::getName, NamedEntity::setName, false));
		Assertions.assertEquals("TEST", to.getName());
		Assertions.assertTrue(BeanUtils.copyProperty(from, to, NamedEntity::getName, NamedEntity::setName, true));
		Assertions.assertEquals("TEST2", to.getName());
	}


	@Test
	public void testFilter_Java8Streams() {
		List<NamedEntity<Integer>> list = getSortList();

		Assertions.assertEquals("[{id=7,label=7th entity}]", BeanUtils.filter(list, NamedEntity::getName, "7th entity").toString());
		Assertions.assertEquals("[{id=1,label=entity-1}, {id=2,label=entity-1}]", BeanUtils.filter(list, NamedEntity::getName, "entity-1").toString());

		Assertions.assertEquals("[{id=1,label=entity-1}]", BeanUtils.filter(list, NamedEntity::getId, 1).toString());

		Assertions.assertEquals("[]", BeanUtils.filter(new ArrayList<NamedEntity<Integer>>(), NamedEntity::getId, 1).toString());
	}


	@Test
	public void testFilter() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		NamedEntity<Integer> entity = new NamedEntity<>();
		entity.setName("entity1");
		list.add(entity);
		entity = new NamedEntity<>();
		entity.setName("entity2");
		list.add(entity);
		list.add(new NamedEntity<>());
		list.add(new NamedEntity<>());

		Assertions.assertEquals(4, list.size(), "4 entities in the list");
		Assertions.assertNull(BeanUtils.filterByPropertyName(list, "name", null, false, true), "no entities for null list or values");
		Assertions.assertNull(BeanUtils.filterByPropertyName(list, "name", new Object[]{"fake entity"}, false, true), "no entities named 'fake entity");
		Assertions.assertNull(BeanUtils.filterByPropertyName(list, "description", new Object[]{"fake entity"}, false, true), "no entities named 'fake entity");
		Assertions.assertEquals(1, CollectionUtils.getSize(BeanUtils.filterByPropertyName(list, "name", new Object[]{"entity1"}, false, true)), "only 1 entity with name 'entity1'");
		Assertions.assertEquals(2, CollectionUtils.getSize(BeanUtils.filterByPropertyName(list, "name", new Object[]{null}, false, true)), "2 entities with name 'null'");
		Assertions.assertEquals(4, CollectionUtils.getSize(BeanUtils.filterByPropertyName(list, "description", new Object[]{null}, false, true)), "2 entities with name 'null'");
	}


	@Test
	public void testFilterNotNull() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		NamedEntity<Integer> entity = new NamedEntity<>();
		entity.setName("entity1");
		list.add(entity);
		entity = new NamedEntity<>();
		entity.setName("entity2");
		list.add(entity);
		list.add(new NamedEntity<>());
		list.add(new NamedEntity<>());

		Assertions.assertEquals(4, list.size());
		Assertions.assertEquals(2, BeanUtils.filterNotNull(list, NamedEntity::getName).size());
		Assertions.assertEquals(0, BeanUtils.filterNotNull(list, NamedEntity::getDescription).size());
	}


	@Test
	public void testSortSimple() {
		List<NamedEntity<Integer>> list = getSortList();

		List<String> propertyNames = new ArrayList<>();
		List<Boolean> ascending = new ArrayList<>();

		// Order By ID
		propertyNames.add("id");
		ascending.add(true);
		Assertions.assertEquals("[1][2][7]", getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending)));

		// Test sorting a null/empty lists
		Assertions.assertTrue(CollectionUtils.isEmpty(BeanUtils.sortWithPropertyNames(null, propertyNames, ascending)));
		Assertions.assertTrue(CollectionUtils.isEmpty(BeanUtils.sortWithPropertyNames(new ArrayList<>(), propertyNames, ascending)));

		ascending.set(0, false);
		Assertions.assertEquals("[7][2][1]", getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending)));

		// Order By Name
		propertyNames.set(0, "name");
		ascending.set(0, true);
		String result = getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending));
		Assertions.assertTrue(StringUtils.compare("[7][1][2]", result) == 0 || StringUtils.compare("[7][2][1]", result) == 0);
		ascending.set(0, false);
		result = getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending));
		Assertions.assertTrue(StringUtils.compare("[1][2][7]", result) == 0 || StringUtils.compare("[2][1][7]", result) == 0);

		// Order By Create Date
		propertyNames.set(0, "createDate");
		ascending.set(0, true);
		result = getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending));
		Assertions.assertTrue(StringUtils.compare("[2][1][7]", result) == 0 || StringUtils.compare("[2][7][1]", result) == 0);
		ascending.set(0, false);
		result = getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending));
		Assertions.assertTrue(StringUtils.compare("[7][1][2]", result) == 0 || StringUtils.compare("[1][7][2]", result) == 0);
	}


	@Test
	public void testSortAdvanced() {
		List<NamedEntity<Integer>> list = getSortList();

		List<String> propertyNames = new ArrayList<>();
		List<Boolean> ascending = new ArrayList<>();

		// Order By Name & Create Date both Ascending
		propertyNames.add("name");
		ascending.add(true);
		propertyNames.add("createDate");
		ascending.add(true);

		Assertions.assertEquals("[7][2][1]", getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending)));

		// Order By Name Asc & Create Date Desc
		ascending.set(1, false);
		Assertions.assertEquals("[7][1][2]", getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending)));

		// Order By Create Date Desc & Name Asc
		//  ** NOTE CHANGING TO FIRST SORT BY CREATE DATE, THEN NAME
		propertyNames.set(0, "createDate");
		ascending.set(0, false);
		propertyNames.set(1, "name");
		ascending.set(1, true);
		Assertions.assertEquals("[7][1][2]", getSortOrderAsString(BeanUtils.sortWithPropertyNames(list, propertyNames, ascending)));
	}


	@Test
	public void testSortWithFunctionsSimple() {
		List<NamedEntity<Integer>> list = getSortList();

		List<Function<NamedEntity<Integer>, Object>> functionList = new ArrayList<>();
		List<Boolean> ascending = new ArrayList<>();

		// Order By ID
		functionList.add(NamedEntity::getId);
		ascending.add(true);
		Assertions.assertEquals("[1][2][7]", getSortOrderAsString(BeanUtils.sortWithFunction(list, NamedEntity::getId, true)));
		Assertions.assertEquals("[1][2][7]", getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending)));

		// Test sorting a null/empty lists
		Assertions.assertTrue(CollectionUtils.isEmpty(BeanUtils.sortWithFunctions(null, functionList, ascending)));
		Assertions.assertTrue(CollectionUtils.isEmpty(BeanUtils.sortWithFunctions(new ArrayList<>(), functionList, ascending)));

		ascending.set(0, false);
		Assertions.assertEquals("[7][2][1]", getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending)));

		// Order By Name
		functionList.set(0, NamedEntity::getName);
		ascending.set(0, true);
		String result = getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending));
		Assertions.assertTrue(StringUtils.compare("[7][1][2]", result) == 0 || StringUtils.compare("[7][2][1]", result) == 0);
		ascending.set(0, false);
		result = getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending));
		Assertions.assertTrue(StringUtils.compare("[1][2][7]", result) == 0 || StringUtils.compare("[2][1][7]", result) == 0);

		// Order By Create Date
		functionList.set(0, NamedEntity::getCreateDate);
		ascending.set(0, true);
		result = getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending));
		Assertions.assertTrue(StringUtils.compare("[2][1][7]", result) == 0 || StringUtils.compare("[2][7][1]", result) == 0);
		ascending.set(0, false);
		result = getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending));
		Assertions.assertTrue(StringUtils.compare("[7][1][2]", result) == 0 || StringUtils.compare("[1][7][2]", result) == 0);
	}


	@Test
	public void testSortWithFunctionsAdvanced() {
		List<NamedEntity<Integer>> list = getSortList();

		List<Function<NamedEntity<Integer>, Object>> functionList = new ArrayList<>();
		List<Boolean> ascending = new ArrayList<>();

		// Order By Name & Create Date both Ascending
		functionList.add(NamedEntity::getName);
		ascending.add(true);
		functionList.add(NamedEntity::getCreateDate);
		ascending.add(true);

		Assertions.assertEquals("[7][2][1]", getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending)));

		// Order By Name Asc & Create Date Desc
		ascending.set(1, false);
		Assertions.assertEquals("[7][1][2]", getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending)));

		// Order By Create Date Desc & Name Asc
		//  ** NOTE CHANGING TO FIRST SORT BY CREATE DATE, THEN NAME
		functionList.set(0, NamedEntity::getCreateDate);
		ascending.set(0, false);
		functionList.set(1, NamedEntity::getName);
		ascending.set(1, true);
		Assertions.assertEquals("[7][1][2]", getSortOrderAsString(BeanUtils.sortWithFunctions(list, functionList, ascending)));
	}


	@Test
	public void testGetBeansMap() {
		List<NamedEntity<Integer>> list = getSortList();
		Map<String, List<NamedEntity<Integer>>> map = BeanUtils.getBeansMap(list, NamedEntity::getName);
		Assertions.assertEquals("{7th entity=[{id=7,label=7th entity}], entity-1=[{id=1,label=entity-1}, {id=2,label=entity-1}]}", map.toString());

		Map<Integer, List<NamedEntity<Integer>>> map2 = BeanUtils.getBeansMap(list, NamedEntity::getId);
		Assertions.assertEquals("{1=[{id=1,label=entity-1}], 2=[{id=2,label=entity-1}], 7=[{id=7,label=7th entity}]}", map2.toString());

		list.get(0).setId(null); // null keys are valid
		Map<Integer, List<NamedEntity<Integer>>> map3 = BeanUtils.getBeansMap(list, NamedEntity::getId);
		Assertions.assertEquals("{null=[{id=null,label=entity-1}], 2=[{id=2,label=entity-1}], 7=[{id=7,label=7th entity}]}", map3.toString());

		map = BeanUtils.getBeansMap(null, NamedEntity::getName);
		Assertions.assertEquals("{}", map.toString());

		map = BeanUtils.getBeansMap(new ArrayList<>(), NamedEntity::getName);
		Assertions.assertEquals("{}", map.toString());
	}


	@Test
	public void testSimpleCloneBean() {
		NamedEntity<Integer> originalBean = new NamedEntity<>();
		originalBean.setId(1);
		originalBean.setName("Original");
		originalBean.setDescription("Original Description");
		originalBean.setCreateDate(new Date());

		NamedEntity<Integer> clonedBean = BeanUtils.cloneBean(originalBean, false, false);
		Assertions.assertNotSame(originalBean, clonedBean);
		Assertions.assertNull(clonedBean.getId());
		Assertions.assertNull(clonedBean.getCreateDate());
		Assertions.assertEquals(originalBean.getName(), clonedBean.getName());
		Assertions.assertEquals(originalBean.getDescription(), clonedBean.getDescription());

		clonedBean = BeanUtils.cloneBean(originalBean, false, true);
		Assertions.assertNotSame(originalBean, clonedBean);
		Assertions.assertEquals(originalBean.getId(), clonedBean.getId());
		Assertions.assertEquals(originalBean.getCreateDate(), clonedBean.getCreateDate());
		Assertions.assertEquals(originalBean.getName(), clonedBean.getName());
		Assertions.assertEquals(originalBean.getDescription(), clonedBean.getDescription());

		clonedBean.setName("Clone");
		Assertions.assertNotEquals(originalBean.getName(), clonedBean.getName());
		Assertions.assertEquals(originalBean.getDescription(), clonedBean.getDescription());
	}


	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	public void testAdvancedShallowCloneBean() {
		NamedHierarchicalEntity hierarchy1Bean = new NamedHierarchicalEntity();
		NamedHierarchicalEntity hierarchy2Bean = new NamedHierarchicalEntity();

		hierarchy1Bean.setName("Parent Bean");
		hierarchy2Bean.setDescription("Parent Bean Description");

		hierarchy2Bean.setName("Child Bean");
		hierarchy2Bean.setDescription("Child Bean Description");
		hierarchy2Bean.setParent(hierarchy1Bean);

		// Shallow Cloning
		NamedHierarchicalEntity cloned2Bean = BeanUtils.cloneBean(hierarchy2Bean, false, false);
		Assertions.assertNotSame(cloned2Bean, hierarchy2Bean);
		Assertions.assertSame(cloned2Bean.getParent(), hierarchy2Bean.getParent());
		Assertions.assertEquals(hierarchy2Bean.getName(), cloned2Bean.getName());

		// Updated Parent referenced bean should update both cases
		hierarchy1Bean.setName("Updated Parent Bean");
		Assertions.assertEquals("Updated Parent Bean", hierarchy2Bean.getParent().getName());
		Assertions.assertEquals("Updated Parent Bean", cloned2Bean.getParent().getName());
	}


	@SuppressWarnings({"unchecked", "rawtypes"})
	@Test
	public void testAdvancedDeepCloneBean() {
		NamedHierarchicalEntity hierarchy1Bean = new NamedHierarchicalEntity();
		NamedHierarchicalEntity hierarchy2Bean = new NamedHierarchicalEntity();

		hierarchy1Bean.setName("Parent Bean");
		hierarchy1Bean.setDescription("Parent Bean Description");
		hierarchy1Bean.setRv(new byte[0]);

		hierarchy2Bean.setName("Child Bean");
		hierarchy2Bean.setDescription("Child Bean Description");
		hierarchy2Bean.setParent(hierarchy1Bean);

		// Deep Cloning
		NamedHierarchicalEntity cloned2Bean = BeanUtils.cloneBean(hierarchy2Bean, true, false);
		Assertions.assertNotSame(cloned2Bean, hierarchy2Bean);
		Assertions.assertNotSame(cloned2Bean.getParent(), hierarchy2Bean.getParent());
		Assertions.assertEquals(hierarchy2Bean.getName(), cloned2Bean.getName());
		Assertions.assertEquals(hierarchy2Bean.getParent().getName(), cloned2Bean.getParent().getName());

		// Updated Parent referenced bean should NOT update both cases
		hierarchy1Bean.setName("Updated Parent Bean");
		Assertions.assertEquals("Updated Parent Bean", hierarchy2Bean.getParent().getName());
		Assertions.assertNotEquals("Updated Parent Bean", cloned2Bean.getParent().getName());

		// Updated Original Parent Name should NOT update actual parent bean nor the clone's parent
		hierarchy2Bean.getParent().setName("Updated Parent by Child");
		Assertions.assertEquals("Updated Parent by Child", hierarchy1Bean.getName());
		Assertions.assertNotEquals("Updated Parent by Child", cloned2Bean.getParent().getName());
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	@Test
	public void testCircularDependencyClone() {
		NamedHierarchicalEntity hierarchy1Bean = new NamedHierarchicalEntity();
		NamedHierarchicalEntity hierarchy2Bean = new NamedHierarchicalEntity();

		hierarchy1Bean.setName("Parent Bean");
		hierarchy1Bean.setDescription("Parent Bean Description");
		hierarchy1Bean.setParent(hierarchy1Bean);

		hierarchy2Bean.setName("Child Bean");
		hierarchy2Bean.setDescription("Child Bean Description");
		hierarchy2Bean.setParent(hierarchy1Bean);
		Assertions.assertEquals(hierarchy1Bean, hierarchy2Bean.getParent());
		Assertions.assertEquals(hierarchy2Bean, hierarchy1Bean.getParent());

		NamedHierarchicalEntity hierarchy1Clone = BeanUtils.cloneBean(hierarchy1Bean, false, false);
		NamedHierarchicalEntity hierarchy2Clone = BeanUtils.cloneBean(hierarchy2Bean, false, false);
		Assertions.assertNotSame(hierarchy1Bean, hierarchy1Clone);
		Assertions.assertNotSame(hierarchy2Bean, hierarchy2Clone);
		Assertions.assertSame(hierarchy1Bean, hierarchy2Clone.getParent());

		hierarchy1Clone = BeanUtils.cloneBean(hierarchy1Bean, true, false);
		hierarchy2Clone = BeanUtils.cloneBean(hierarchy2Bean, true, false);
		Assertions.assertNotSame(hierarchy1Bean, hierarchy1Clone);
		Assertions.assertNotSame(hierarchy2Bean, hierarchy2Clone);
		Assertions.assertNotSame(hierarchy1Bean, hierarchy2Clone.getParent());
		Assertions.assertNotSame(hierarchy2Bean, hierarchy1Clone.getParent());
	}


	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testBeanWithCollectionsClone() {

		BeanUtilsTestEntity testBean = new BeanUtilsTestEntity();
		testBean.setName("Test Bean");

		NamedEntity<Integer> bean1 = new NamedEntity<>();
		bean1.setName("Bean 1");

		NamedEntity<Integer> bean2 = new NamedEntity<>();
		bean2.setName("Bean 2");

		List<NamedEntity<Integer>> entityList = new ArrayList<>();
		entityList.add(bean1);
		entityList.add(bean2);
		testBean.setEntityList(entityList);

		NamedEntity[] entityArray = new NamedEntity[]{bean1, bean2};
		testBean.setEntityArray(entityArray);

		Map<String, NamedEntity<Integer>> entityMap = new HashMap<>();
		entityMap.put(bean1.getName(), bean1);
		entityMap.put(bean2.getName(), bean2);
		testBean.setEntityMap(entityMap);

		BeanUtilsTestEntity clonedBean = BeanUtils.cloneBean(testBean, false, false);
		Assertions.assertEquals(testBean.getName(), clonedBean.getName());

		Assertions.assertSame(testBean.getEntityList(), clonedBean.getEntityList());
		Assertions.assertSame(testBean.getEntityArray(), clonedBean.getEntityArray());
		Assertions.assertSame(testBean.getEntityMap(), clonedBean.getEntityMap());

		Assertions.assertEquals(2, CollectionUtils.getSize(clonedBean.getEntityList()));
		Assertions.assertEquals(2, clonedBean.getEntityArray().length);
		Assertions.assertEquals(2, clonedBean.getEntityMap().size());

		Assertions.assertSame(bean1, clonedBean.getEntityList().get(0));
		Assertions.assertSame(bean2, clonedBean.getEntityList().get(1));
		Assertions.assertSame(bean1, clonedBean.getEntityArray()[0]);
		Assertions.assertSame(bean2, clonedBean.getEntityArray()[1]);
		Assertions.assertSame(bean1, clonedBean.getEntityMap().get(bean1.getName()));
		Assertions.assertSame(bean2, clonedBean.getEntityMap().get(bean2.getName()));

		clonedBean = BeanUtils.cloneBean(testBean, true, false);
		Assertions.assertEquals(testBean.getName(), clonedBean.getName());

		Assertions.assertNotSame(testBean.getEntityList(), clonedBean.getEntityList());
		Assertions.assertNotSame(testBean.getEntityArray(), clonedBean.getEntityArray());
		Assertions.assertNotSame(testBean.getEntityMap(), clonedBean.getEntityMap());

		Assertions.assertEquals(2, CollectionUtils.getSize(clonedBean.getEntityList()));
		Assertions.assertEquals(2, clonedBean.getEntityArray().length);
		Assertions.assertEquals(2, clonedBean.getEntityMap().size());

		Assertions.assertNotSame(bean1, clonedBean.getEntityList().get(0));
		Assertions.assertEquals(bean1.getName(), clonedBean.getEntityList().get(0).getName());
		Assertions.assertNotSame(bean2, clonedBean.getEntityList().get(1));
		Assertions.assertEquals(bean2.getName(), clonedBean.getEntityList().get(1).getName());

		Assertions.assertNotSame(bean1, clonedBean.getEntityArray()[0]);
		Assertions.assertEquals(bean1.getName(), clonedBean.getEntityArray()[0].getName());
		Assertions.assertNotSame(bean2, clonedBean.getEntityArray()[1]);
		Assertions.assertEquals(bean2.getName(), clonedBean.getEntityArray()[1].getName());

		Assertions.assertNotSame(bean1, clonedBean.getEntityMap().get(bean1.getName()));
		Assertions.assertEquals(bean1.getName(), clonedBean.getEntityMap().get(bean1.getName()).getName());
		Assertions.assertNotSame(bean2, clonedBean.getEntityMap().get(bean2.getName()));
		Assertions.assertEquals(bean2.getName(), clonedBean.getEntityMap().get(bean2.getName()).getName());
	}


	@Test
	public void testGetPropertyType() {

		Class<?> clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "testBoolean");
		Assertions.assertEquals(boolean.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "items");
		Assertions.assertEquals(List.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "parent");
		Assertions.assertEquals(BeanUtilsTestBean.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "parent");
		Assertions.assertEquals(BeanUtilsTestBean.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "name");
		Assertions.assertEquals(String.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "label");
		Assertions.assertEquals(String.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "newBean");
		Assertions.assertEquals(boolean.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "identity");
		Assertions.assertEquals(Integer.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "parent.date");
		Assertions.assertEquals(Date.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "enumValue");
		Assertions.assertEquals(BeanUtilsTestEnum.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "bigDecimal");
		Assertions.assertEquals(BigDecimal.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "date");
		Assertions.assertEquals(Date.class, clz);

		clz = BeanUtils.getPropertyType(BeanUtilsTestBean.class, "numbers");
		Assertions.assertEquals(int[].class, clz);

		clz = BeanUtils.getPropertyType(DocumentAware.class, "documentFileType");
		Assertions.assertEquals(String.class, clz);

		clz = BeanUtils.getPropertyType(DocumentAware.class, "newBean");
		Assertions.assertEquals(boolean.class, clz);

		clz = BeanUtils.getPropertyType(DocumentAware.class, "identity");
		Assertions.assertEquals(Serializable.class, clz);

		clz = BeanUtils.getPropertyType(AddressObject.class, "cityStatePostalCode");
		Assertions.assertEquals(String.class, clz);

		clz = BeanUtils.getPropertyType(HierarchicalEntity.class, "parent.parent");
		Assertions.assertEquals(HierarchicalEntity.class, clz);

		clz = BeanUtils.getPropertyType(HierarchicalObject.class, "parent.parent.leaf");
		Assertions.assertEquals(boolean.class, clz);
	}


	@Test
	public void testGetPropertyValue() {
		BeanUtilsTestBean beanUtilsTestBean = generateBeanToDescribe();
		Assertions.assertEquals(beanUtilsTestBean.getBigDecimal(), BeanUtils.getPropertyValue(beanUtilsTestBean, "bigDecimal"));
		Assertions.assertEquals(beanUtilsTestBean.getClass(), BeanUtils.getPropertyValue(beanUtilsTestBean, "class"));
		Assertions.assertEquals(beanUtilsTestBean.getCreateDate(), BeanUtils.getPropertyValue(beanUtilsTestBean, "createDate"));
		Assertions.assertEquals(beanUtilsTestBean.getCreateUserId(), BeanUtils.getPropertyValue(beanUtilsTestBean, "createUserId"));
		Assertions.assertEquals(beanUtilsTestBean.getDate(), BeanUtils.getPropertyValue(beanUtilsTestBean, "date"));
		Assertions.assertEquals(beanUtilsTestBean.getDescription(), BeanUtils.getPropertyValue(beanUtilsTestBean, "description"));
		Assertions.assertEquals(beanUtilsTestBean.getEnumValue(), BeanUtils.getPropertyValue(beanUtilsTestBean, "enumValue"));
		Assertions.assertEquals(beanUtilsTestBean.getId(), BeanUtils.getPropertyValue(beanUtilsTestBean, "id"));
		Assertions.assertEquals(beanUtilsTestBean.getIdentity(), BeanUtils.getPropertyValue(beanUtilsTestBean, "identity"));
		Assertions.assertEquals(beanUtilsTestBean.getItemCount(), BeanUtils.getPropertyValue(beanUtilsTestBean, "itemCount"));
		Assertions.assertEquals(beanUtilsTestBean.getItems(), BeanUtils.getPropertyValue(beanUtilsTestBean, "items"));
		Assertions.assertEquals(beanUtilsTestBean.getLabel(), BeanUtils.getPropertyValue(beanUtilsTestBean, "label"));
		Assertions.assertEquals(beanUtilsTestBean.getName(), BeanUtils.getPropertyValue(beanUtilsTestBean, "name"));
		Assertions.assertEquals(beanUtilsTestBean.isNewBean(), BeanUtils.getPropertyValue(beanUtilsTestBean, "newBean"));
		Assertions.assertEquals(beanUtilsTestBean.getNumbers(), BeanUtils.getPropertyValue(beanUtilsTestBean, "numbers"));
		Assertions.assertEquals(beanUtilsTestBean.getParent(), BeanUtils.getPropertyValue(beanUtilsTestBean, "parent"));
		Assertions.assertEquals(beanUtilsTestBean.getRv(), BeanUtils.getPropertyValue(beanUtilsTestBean, "rv"));
		Assertions.assertEquals(beanUtilsTestBean.isTestBoolean(), BeanUtils.getPropertyValue(beanUtilsTestBean, "testBoolean"));
		Assertions.assertEquals(beanUtilsTestBean.getTestGetterSetterFieldGetter(), BeanUtils.getPropertyValue(beanUtilsTestBean, "testGetterSetterFieldGetter"));
		Assertions.assertEquals(beanUtilsTestBean.isTestJsonExclusionFilterOnField(), BeanUtils.getPropertyValue(beanUtilsTestBean, "testJsonExclusionFilterOnField"));
		Assertions.assertEquals(beanUtilsTestBean.isTestJsonExclusionFilterOnGetter(), BeanUtils.getPropertyValue(beanUtilsTestBean, "testJsonExclusionFilterOnGetter"));
		Assertions.assertEquals(beanUtilsTestBean.getUpdateDate(), BeanUtils.getPropertyValue(beanUtilsTestBean, "updateDate"));
		Assertions.assertEquals(beanUtilsTestBean.getUpdateUserId(), BeanUtils.getPropertyValue(beanUtilsTestBean, "updateUserId"));
	}


	@Test
	public void testDescribe() {
		assertBeanDescription(false, false);
	}


	@Test
	public void testDescribeWithSetter() {
		assertBeanDescription(true, false);
	}


	@Test
	public void testDescribeWithSerializationRules() {
		assertBeanDescription(false, true);
	}


	@Test
	public void testDescribeWithGetterAndSetterAndSerializationRules() {
		assertBeanDescription(true, true);
	}


	@Test
	public void testGetBeanIdentityArray() {
		// test null list
		Integer[] idArray = BeanUtils.getBeanIdentityArray(null, Integer.class);
		Assertions.assertNotNull(idArray);
		Assertions.assertEquals(0, idArray.length);
		// test entities in list
		List<NamedEntity<Integer>> entityList = new ArrayList<>();
		entityList.add(null);
		entityList.add(new NamedEntity<>());
		idArray = BeanUtils.getBeanIdentityArray(entityList, Integer.class);
		Assertions.assertNotNull(idArray);
		Assertions.assertEquals(0, idArray.length);
		// test valid list
		entityList.clear();
		for (int i = 1; i <= 10; i++) {
			NamedEntity<Integer> bean = new NamedEntity<>();
			bean.setId(i);
			bean.setName("Bean " + bean.getId());
			entityList.add(bean);
		}
		idArray = BeanUtils.getBeanIdentityArray(entityList, Integer.class);
		Assertions.assertNotNull(idArray);
		Assertions.assertEquals(10, idArray.length);
		Arrays.sort(idArray);
		int i = 1;
		for (Integer id : idArray) {
			Assertions.assertEquals(Integer.valueOf(i), id);
			i++;
		}
	}


	@Test
	public void testGetBeanIdentityList() {
		// test null list
		List<Integer> idList = BeanUtils.getBeanIdentityList(null);
		Assertions.assertNotNull(idList);
		Assertions.assertEquals(0, idList.size());
		// test null entities in list
		List<NamedEntity<Integer>> entityList = new ArrayList<>();
		entityList.add(null);
		entityList.add(new NamedEntity<>());
		idList = BeanUtils.getBeanIdentityList(entityList);
		Assertions.assertNotNull(idList);
		Assertions.assertEquals(0, idList.size());
		// test valid list
		entityList.clear();
		for (int i = 1; i <= 10; i++) {
			NamedEntity<Integer> bean = new NamedEntity<>();
			bean.setId(i);
			bean.setName("Bean " + bean.getId());
			entityList.add(bean);
		}
		idList = BeanUtils.getBeanIdentityList(entityList);
		Assertions.assertNotNull(idList);
		Assertions.assertEquals(10, idList.size());
		Collections.sort(idList);
		int i = 1;
		for (Integer id : idList) {
			Assertions.assertEquals(Integer.valueOf(i), id);
			i++;
		}
	}


	@Test
	public void testCreateKeyFromBeans() {
		Assertions.assertEquals("", BeanUtils.createKeyFromBeans());
		Assertions.assertEquals("", BeanUtils.createKeyFromBeans((Object[]) null));
		Assertions.assertEquals("null", BeanUtils.createKeyFromBeans(new Object[]{null}));
		List<NamedEntity<Integer>> sortedList = getSortList();
		Assertions.assertEquals("1-7-2", BeanUtils.createKeyFromBeans(sortedList.toArray(new Object[0])));
		List<Object> objectList = new ArrayList<>();
		objectList.add(Boolean.TRUE);
		objectList.add(1);
		objectList.add(null);
		objectList.add(sortedList);
		objectList.add(sortedList.get(0));
		Assertions.assertEquals("true-1-null-{id=1,label=entity-1}, {id=7,label=7th entity}, {id=2,label=entity-1}-1", BeanUtils.createKeyFromBeans(objectList.toArray(new Object[0])));
	}


	@Test
	public void testGetNullProperties() {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		bean.setName("Test");
		bean.setDate(new Date());
		List<String> props = BeanUtils.getNullProperties(bean);
		Assertions.assertEquals(12, props.size());
		Assertions.assertFalse(CollectionUtils.contains(props, "name"));
		Assertions.assertFalse(CollectionUtils.contains(props, "date"));
	}


	@Test
	public void testGetNonNullProperties() {
		BeanUtilsTestBean bean = new BeanUtilsTestBean();
		bean.setName("Test");
		bean.setDate(new Date());
		List<String> props = BeanUtils.getNonNullProperties(bean);
		Assertions.assertEquals(11, props.size());
		Assertions.assertTrue(CollectionUtils.contains(props, "name"));
		Assertions.assertTrue(CollectionUtils.contains(props, "date"));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<NamedEntity<Integer>> getSortList() {
		List<NamedEntity<Integer>> list = new ArrayList<>();
		Date date = new Date();
		NamedEntity<Integer> entity = new NamedEntity<>();
		entity.setId(1);
		entity.setName("entity-1");
		entity.setDescription("entity-1-description");
		entity.setCreateDate(date);
		list.add(entity);
		NamedEntity<Integer> entity7 = new NamedEntity<>();
		entity7.setId(7);
		entity7.setName("7th entity");
		entity7.setDescription("entity-7-description");
		entity7.setCreateDate(date);
		list.add(entity7);
		NamedEntity<Integer> entity2 = new NamedEntity<>();
		entity2.setId(2);
		entity2.setName("entity-1");
		entity2.setDescription("my duplicate named entity");
		date = DateUtils.toDate("7/2/2008");
		entity2.setCreateDate(date);
		list.add(entity2);
		return list;
	}


	private String getSortOrderAsString(List<NamedEntity<Integer>> sortedList) {
		StringBuilder sb = new StringBuilder();
		for (NamedEntity<Integer> entity : sortedList) {
			sb.append("[").append(entity.getId()).append("]");
		}
		return sb.toString();
	}


	/**
	 * Generates a bean and asserts that its description matches the expected results when the given arguments are used.
	 */
	private void assertBeanDescription(boolean requireSetter, boolean applyJsonSerializationRules) {
		BeanUtilsTestBean beanUtilsTestBean = generateBeanToDescribe();
		Map<String, Object> beanDescription;
		if (requireSetter && applyJsonSerializationRules) {
			beanDescription = BeanUtils.describeWithRequiredSetterAndAnnotationFilters(beanUtilsTestBean, BeanUtilsTestAnnotation.class);
		}
		else if (requireSetter) {
			beanDescription = BeanUtils.describeWithRequiredSetter(beanUtilsTestBean);
		}
		else if (applyJsonSerializationRules) {
			beanDescription = BeanUtils.describeWithAnnotationFilters(beanUtilsTestBean, BeanUtilsTestAnnotation.class);
		}
		else {
			beanDescription = BeanUtils.describe(beanUtilsTestBean);
		}
		// Fields returned by describe will always have getters by design
		Collection<String> beanPropertyNames = getBeanUtilsTestBeanPropertyNames(true, requireSetter, applyJsonSerializationRules);

		// Verify identical existence of property names
		Collection<String> beanPropertyNamesSorted = new TreeSet<>(beanPropertyNames);
		Collection<String> beanDescriptionNamesSorted = new TreeSet<>(beanDescription.keySet());
		AssertUtils.assertEquals(beanPropertyNamesSorted, beanDescriptionNamesSorted, "Unexpected list of property names retrieved from call to [%s].%nExpected: %s.%nActual:   %s.", "describe", CollectionUtils.toString(beanPropertyNamesSorted, 25), CollectionUtils.toString(beanDescriptionNamesSorted, 25));

		// Verify equal property values
		for (String propertyName : beanPropertyNamesSorted) {
			Assertions.assertEquals(BeanUtils.getPropertyValue(beanUtilsTestBean, propertyName), beanDescription.get(propertyName), String.format("Unexpected value for property [%s].", propertyName));
		}
	}


	/**
	 * Generates a basic bean to use for {@link BeanUtils#describe(Object) describe} tests.
	 */
	private BeanUtilsTestBean generateBeanToDescribe() {
		BeanUtilsTestBean beanUtilsTestBean = new BeanUtilsTestBean();
		beanUtilsTestBean.setBigDecimal(BigDecimal.valueOf(1.0d));
		beanUtilsTestBean.setCreateDate(new Date());
		beanUtilsTestBean.setCreateUserId((short) 0);
		beanUtilsTestBean.setDate(new Date());
		beanUtilsTestBean.setDescription("description");
		beanUtilsTestBean.setEnumValue(BeanUtilsTestEnum.COLD);
		beanUtilsTestBean.setId(0);
		beanUtilsTestBean.setItems(Arrays.asList(new BeanUtilsTestBean(), new BeanUtilsTestBean()));
		beanUtilsTestBean.setLabel("label");
		beanUtilsTestBean.setName("name");
		beanUtilsTestBean.setNumbers(new int[]{1, 2, 3});
		beanUtilsTestBean.setParent(new BeanUtilsTestBean());
		beanUtilsTestBean.setRv(new byte[]{(byte) 0});
		beanUtilsTestBean.setTestBoolean(true);
		beanUtilsTestBean.setTestGetterSetterFieldSetter(true);
		beanUtilsTestBean.setTestJsonExclusionFilterOnField(true);
		beanUtilsTestBean.setTestJsonExclusionFilterOnGetter(true);
		beanUtilsTestBean.setUpdateDate(new Date());
		beanUtilsTestBean.setUpdateUserId((short) 0);
		return beanUtilsTestBean;
	}


	/**
	 * Gets the property names for the {@link BeanUtilsTestBean} class when filtering using the given qualifiers.
	 */
	private Collection<String> getBeanUtilsTestBeanPropertyNames(boolean requireGetter, boolean requireSetter, boolean applyJsonSerializationRules) {
		Collection<String> beanPropertyNames = CollectionUtils.createHashSet("bigDecimal", "createDate", "createUserId", "date", "description", "enumValue", "id", "identity", "itemCount", "items", "label", "name", "newBean", "numbers", "parent", "rv", "testBoolean", "testGetterSetterFieldGetter", "testGetterSetterFieldSetter", "testJsonExclusionFilterOnField", "testJsonExclusionFilterOnGetter", "updateDate", "updateUserId");
		if (requireGetter) {
			beanPropertyNames.remove("testGetterSetterFieldSetter");
		}
		if (requireSetter) {
			beanPropertyNames.remove("class");
			beanPropertyNames.remove("identity");
			beanPropertyNames.remove("itemCount");
			beanPropertyNames.remove("newBean");
			beanPropertyNames.remove("testGetterSetterFieldGetter");
		}
		if (applyJsonSerializationRules) {
			beanPropertyNames.remove("testJsonExclusionFilterOnField");
			beanPropertyNames.remove("testJsonExclusionFilterOnGetter");
		}
		return beanPropertyNames;
	}
}
