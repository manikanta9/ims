package com.clifton.core.beans;


import java.util.List;
import java.util.Map;


/**
 * The <code>BeanUtilsTestEntity</code>
 *
 * @author manderson
 */
public class BeanUtilsTestEntity extends NamedEntity<Integer> {

	private List<NamedEntity<Integer>> entityList;
	private NamedEntity<Integer>[] entityArray;
	private Map<String, NamedEntity<Integer>> entityMap;


	/**
	 * @return the entityList
	 */
	public List<NamedEntity<Integer>> getEntityList() {
		return this.entityList;
	}


	/**
	 * @param entityList the entityList to set
	 */
	public void setEntityList(List<NamedEntity<Integer>> entityList) {
		this.entityList = entityList;
	}


	/**
	 * @return the entityArray
	 */
	public NamedEntity<Integer>[] getEntityArray() {
		return this.entityArray;
	}


	/**
	 * @param entityArray the entityArray to set
	 */
	public void setEntityArray(NamedEntity<Integer>[] entityArray) {
		this.entityArray = entityArray;
	}


	/**
	 * @return the entityMap
	 */
	public Map<String, NamedEntity<Integer>> getEntityMap() {
		return this.entityMap;
	}


	/**
	 * @param entityMap the entityMap to set
	 */
	public void setEntityMap(Map<String, NamedEntity<Integer>> entityMap) {
		this.entityMap = entityMap;
	}
}
