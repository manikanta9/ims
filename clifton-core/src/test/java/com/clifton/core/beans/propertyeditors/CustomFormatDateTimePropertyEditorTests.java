package com.clifton.core.beans.propertyeditors;

import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.Date;


@ExtendWith(SpringExtension.class)
@ContextConfiguration
public class CustomFormatDateTimePropertyEditorTests {

	@Resource
	private CustomFormatDateTimePropertyEditorTestEntity customFormatDateTimePropertyEditorTestEntity;
	@Resource
	private ApplicationContext applicationContext;


	@Test
	public void testSetAsText() {
		Date expectedDate = DateUtils.toDate("10/1/2009 17:00:00", "MM/dd/yyyy HH:mm:ss");
		Assertions.assertEquals(expectedDate, this.customFormatDateTimePropertyEditorTestEntity.getDates().get(0));

		expectedDate = DateUtils.toDate("10/1/2009 00:00:00", "MM/dd/yyyy HH:mm:ss");
		Assertions.assertEquals(expectedDate, this.customFormatDateTimePropertyEditorTestEntity.getDates().get(1));

		expectedDate = DateUtils.toDate("10/1/2009 00:00:00", "MM/dd/yyyy HH:mm:ss");
		Assertions.assertEquals(expectedDate, this.customFormatDateTimePropertyEditorTestEntity.getDates().get(2));

		expectedDate = DateUtils.toDate("5:00 PM", "h:mm aaa");
		Assertions.assertEquals(expectedDate, this.customFormatDateTimePropertyEditorTestEntity.getDates().get(3));

		expectedDate = DateUtils.toDate("8:00 AM", "h:mm aaa");
		Assertions.assertEquals(expectedDate, this.customFormatDateTimePropertyEditorTestEntity.getDates().get(4));

		expectedDate = DateUtils.toDate("10/1/2009 5:00 PM", "MM/dd/yyyy h:mm aaa");
		Assertions.assertEquals(expectedDate, this.customFormatDateTimePropertyEditorTestEntity.getDates().get(5));

		Assertions.assertEquals(17 * 60, this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(0).getMinutes(expectedDate));
		Assertions.assertEquals(17 * 60 * 60, this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(0).getSeconds(expectedDate));
		Assertions.assertEquals("17:00:00", this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(0).toString());
		Assertions.assertEquals("5:00 PM", this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(0).toString("h:mm aaa"));

		Assertions.assertEquals(18 * 60 + 10, this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(1).getMinutes(expectedDate));
		Assertions.assertEquals(18 * 60 * 60 + 10 * 60, this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(1).getSeconds(expectedDate));
		Assertions.assertEquals("18:10:00", this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(1).toString());
		Assertions.assertEquals("6:10 PM", this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(1).toString("h:mm aaa"));

		Assertions.assertEquals(9 * 60 + 23, this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(2).getMinutes(expectedDate));
		Assertions.assertEquals(9 * 60 * 60 + 23 * 60, this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(2).getSeconds(expectedDate));
		Assertions.assertEquals("09:23:00", this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(2).toString());
		Assertions.assertEquals("9:23 AM", this.customFormatDateTimePropertyEditorTestEntity.getTimes().get(2).toString("h:mm aaa"));

		Assertions.assertNull(this.customFormatDateTimePropertyEditorTestEntity.getDates().get(6));

		Exception ex = null;
		try {
			@SuppressWarnings("unused")
			Object bean = this.applicationContext.getBean("customFormatDateTimePropertyEditorTestEntity1");
		}
		catch (Exception e) {
			Assertions.assertTrue(e.getMessage().contains("Cannot convert '10/1/2009 5:00 pm f=MM/dd/yyyy h:mm aaa f=MM/dd/yyyy' to a Date."));
			ex = e;
		}
		Assertions.assertNotNull(ex);

		ex = null;
		try {
			@SuppressWarnings("unused")
			Object bean = this.applicationContext.getBean("customFormatDateTimePropertyEditorTestEntity2");
		}
		catch (Exception e) {
			Assertions.assertTrue(e.getMessage().contains("Could not parse date: "));
			ex = e;
		}
		Assertions.assertNotNull(ex);
	}
}
