package com.clifton.core.beans;


import com.clifton.core.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


public class BeanUtilsTestBean extends NamedEntity<Integer> {

	private boolean testBoolean;
	private boolean testGetterSetterField;
	private boolean testJsonExclusionFilterOnGetter;
	@BeanUtilsTestAnnotation
	private boolean testJsonExclusionFilterOnField;
	private List<BeanUtilsTestBean> items;
	private BeanUtilsTestBean parent;
	private BeanUtilsTestEnum enumValue;
	private BigDecimal bigDecimal;
	private Date date;
	private int[] numbers;


	public Integer getItemCount() {
		return CollectionUtils.getSize(getItems());
	}


	public int[] getNumbers() {
		return this.numbers;
	}


	public void setNumbers(int[] numbers) {
		this.numbers = numbers;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public BigDecimal getBigDecimal() {
		return this.bigDecimal;
	}


	public void setBigDecimal(BigDecimal bigDecimal) {
		this.bigDecimal = bigDecimal;
	}


	public BeanUtilsTestEnum getEnumValue() {
		return this.enumValue;
	}


	public void setEnumValue(BeanUtilsTestEnum enumValue) {
		this.enumValue = enumValue;
	}


	public List<BeanUtilsTestBean> getItems() {
		return this.items;
	}


	public void setItems(List<BeanUtilsTestBean> items) {
		this.items = items;
	}


	public boolean isTestBoolean() {
		return this.testBoolean;
	}


	public void setTestBoolean(boolean testBoolean) {
		this.testBoolean = testBoolean;
	}


	// Getter field with no setter
	public boolean getTestGetterSetterFieldGetter() {
		return this.testGetterSetterField;
	}


	// Setter field with no getter
	public void setTestGetterSetterFieldSetter(boolean testSetterField) {
		this.testGetterSetterField = testSetterField;
	}


	public BeanUtilsTestBean getParent() {
		return this.parent;
	}


	public void setParent(BeanUtilsTestBean parent) {
		this.parent = parent;
	}


	@BeanUtilsTestAnnotation
	public boolean isTestJsonExclusionFilterOnGetter() {
		return this.testJsonExclusionFilterOnGetter;
	}


	public void setTestJsonExclusionFilterOnGetter(boolean testJsonExclusionFilterOnGetter) {
		this.testJsonExclusionFilterOnGetter = testJsonExclusionFilterOnGetter;
	}


	public boolean isTestJsonExclusionFilterOnField() {
		return this.testJsonExclusionFilterOnField;
	}


	public void setTestJsonExclusionFilterOnField(boolean testJsonExclusionFilterOnField) {
		this.testJsonExclusionFilterOnField = testJsonExclusionFilterOnField;
	}
}
