package com.clifton.core.beans.propertyeditors;


import com.clifton.core.util.date.Time;

import java.util.Date;
import java.util.List;


public class CustomFormatDateTimePropertyEditorTestEntity {

	private List<Date> dates;
	private List<Time> times;


	/**
	 * @return the dates
	 */
	public List<Date> getDates() {
		return this.dates;
	}


	/**
	 * @param dates the dates to set
	 */
	public void setDates(List<Date> dates) {
		this.dates = dates;
	}


	public List<Time> getTimes() {
		return this.times;
	}


	public void setTimes(List<Time> times) {
		this.times = times;
	}
}
