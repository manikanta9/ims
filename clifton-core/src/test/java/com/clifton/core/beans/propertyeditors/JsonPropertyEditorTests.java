package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.propertyeditors.CustomNumberEditor;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class JsonPropertyEditorTests {

	private Map<Class<?>, PropertyEditorSupport> propertyEditorMap;


	@Test
	public void testNulls() {
		JsonPropertyEditor editor = new JsonPropertyEditor();

		editor.setAsText(null);
		Assertions.assertNull(editor.getValue());

		editor.setAsText("[]");
		Assertions.assertNull(editor.getValue());
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultArrayObjectClass() {
		JsonPropertyEditor editor = new JsonPropertyEditor();
		editor.setAsText("[{\"comparison\":\"IN\",\"value\":[1],\"field\":\"auditTypeId\"}]");

		List<Object> list = (List<Object>) editor.getValue();
		Assertions.assertEquals(1, list.size());

		Object element = list.get(0);
		Assertions.assertNotNull(element);
		Assertions.assertEquals(SearchRestriction.class, element.getClass(), "If not class property is specified, default class must be SearchRestriction");

		SearchRestriction restriction = (SearchRestriction) element;
		Assertions.assertEquals(ComparisonConditions.IN, restriction.getComparison());
		Assertions.assertEquals("auditTypeId", restriction.getField());
		ArrayList<String> value = new ArrayList<>();
		value.add("1");
		Assertions.assertEquals(value, restriction.getValue());
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testArrayObjectClass() {
		JsonPropertyEditor editor = new JsonPropertyEditor();
		editor.setAsText("[{\"class\":\"com.clifton.core.beans.NamedEntity\",\"name\":\"my name\",\"label\":\"my label\"}]");

		List<Object> list = (List<Object>) editor.getValue();
		Assertions.assertEquals(1, list.size());

		Object element = list.get(0);
		Assertions.assertNotNull(element);
		Assertions.assertEquals(NamedEntity.class, element.getClass());

		NamedEntity<Integer> entity = (NamedEntity<Integer>) element;
		Assertions.assertEquals("my name", entity.getName());
		Assertions.assertEquals("my label", entity.getLabel());
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testStringArray() {
		JsonPropertyEditor editor = new JsonPropertyEditor();
		editor.setAsText("[\"one\",\"two\",\"three\"]");

		List<Object> list = (List<Object>) editor.getValue();
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals("one", list.get(0));
		Assertions.assertEquals("two", list.get(1));
		Assertions.assertEquals("three", list.get(2));
	}


	@Test
	public void testNullsWithPropertyMap() {
		JsonPropertyEditor editor = new JsonPropertyEditor(getPropertyEditorMap());

		editor.setAsText(null);
		Assertions.assertNull(editor.getValue());

		editor.setAsText("[]");
		Assertions.assertNull(editor.getValue());
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testDefaultArrayObjectClassWithPropertyMap() {
		JsonPropertyEditor editor = new JsonPropertyEditor(getPropertyEditorMap());
		editor.setAsText("[{\"comparison\":\"IN\",\"value\":[1],\"field\":\"auditTypeId\"}]");

		List<Object> list = (List<Object>) editor.getValue();
		Assertions.assertEquals(1, list.size());

		Object element = list.get(0);
		Assertions.assertNotNull(element);
		Assertions.assertEquals(SearchRestriction.class, element.getClass(), "If not class property is specified, default class must be SearchRestriction");

		SearchRestriction restriction = (SearchRestriction) element;
		Assertions.assertEquals(ComparisonConditions.IN, restriction.getComparison());
		Assertions.assertEquals("auditTypeId", restriction.getField());
		ArrayList<String> value = new ArrayList<>();
		value.add("1");
		Assertions.assertEquals(value, restriction.getValue());
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testArrayObjectClassWithPropertyMap() {
		JsonPropertyEditor editor = new JsonPropertyEditor(getPropertyEditorMap());
		editor.setAsText("[{\"class\":\"com.clifton.core.beans.NamedEntity\",\"name\":\"my name\",\"label\":\"my label\"}]");

		List<Object> list = (List<Object>) editor.getValue();
		Assertions.assertEquals(1, list.size());

		Object element = list.get(0);
		Assertions.assertNotNull(element);
		Assertions.assertEquals(NamedEntity.class, element.getClass());

		NamedEntity<Integer> entity = (NamedEntity<Integer>) element;
		Assertions.assertEquals("my name", entity.getName());
		Assertions.assertEquals("my label", entity.getLabel());
	}


	@Test
	@SuppressWarnings("unchecked")
	public void testStringArrayWithPropertyMap() {
		JsonPropertyEditor editor = new JsonPropertyEditor(getPropertyEditorMap());
		editor.setAsText("[\"one\",\"two\",\"three\"]");

		List<Object> list = (List<Object>) editor.getValue();
		Assertions.assertEquals(3, list.size());
		Assertions.assertEquals("one", list.get(0));
		Assertions.assertEquals("two", list.get(1));
		Assertions.assertEquals("three", list.get(2));
	}


	@Test
	public void testParseDates() {
		DatePropertyEditor editor = new DatePropertyEditor();

		// DATE_FORMAT_INPUT = "MM/dd/yyyy"
		String dateString = "05/10/2021";
		editor.setAsText(dateString);
		DateUtils.assertDatesEqual((Date) editor.getValue(), DateUtils.toDate(dateString, DateUtils.DATE_FORMAT_INPUT));

		// DATE_FORMAT_SHORT = "MM/dd/yyyy h:mm aaa"
		dateString = "05/10/2021 1:23 AM";
		editor.setAsText(dateString);
		DateUtils.assertDatesEqual((Date) editor.getValue(), DateUtils.toDate(dateString, DateUtils.DATE_FORMAT_SHORT));

		// "MM/dd/yyyy HH:mm:ss"
		dateString = "05/10/2021 12:34:56";
		editor.setAsText(dateString);
		DateUtils.assertDatesEqual((Date) editor.getValue(), DateUtils.toDate("2021-05-10 12:34:56", DateUtils.DATE_FORMAT_FULL));

		// DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss"
		dateString = "2021-05-10 12:34:56";
		editor.setAsText(dateString);
		DateUtils.assertDatesEqual((Date) editor.getValue(), DateUtils.toDate(dateString, DateUtils.DATE_FORMAT_FULL));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<Class<?>, PropertyEditorSupport> getPropertyEditorMap() {
		if (this.propertyEditorMap == null) {
			this.propertyEditorMap = new HashMap<>();
			this.propertyEditorMap.put(Date.class, new DatePropertyEditor());
			this.propertyEditorMap.put(Time.class, new TimePropertyEditor());
			this.propertyEditorMap.put(byte[].class, new ByteArrayPropertyEditor());
			NumberFormat numberFormat = NumberFormat.getInstance(); // support inputs with commas
			this.propertyEditorMap.put(int.class, new CustomNumberEditor(Integer.class, numberFormat, true));
			this.propertyEditorMap.put(Integer.class, new CustomNumberEditor(Integer.class, numberFormat, true));
			this.propertyEditorMap.put(Double.class, new CustomNumberEditor(Double.class, numberFormat, true));
			this.propertyEditorMap.put(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, numberFormat, true));
		}
		return this.propertyEditorMap;
	}
}
