package com.clifton.core.beans.propertyeditors;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


public class ByteArrayPropertyEditorTests {

	@Test
	public void testSetAsText() {
		ByteArrayPropertyEditor editor = new ByteArrayPropertyEditor();

		editor.setAsText(null);
		Assertions.assertNull(editor.getValue());

		editor.setAsText("");
		Assertions.assertNull(editor.getValue());

		editor.setAsText("0,0,10,84,112,-101,11");
		byte[] result = (byte[]) editor.getValue();
		Assertions.assertNotNull(result);
		Assertions.assertEquals(7, result.length);
		Assertions.assertEquals(84, result[3]);
		Assertions.assertEquals(-101, result[5]);
	}
}
