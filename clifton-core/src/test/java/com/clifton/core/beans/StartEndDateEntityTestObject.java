package com.clifton.core.beans;

import java.util.Date;


public class StartEndDateEntityTestObject extends NamedEntity<Integer> {

	private Date startDate;
	private Date endDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "StartEndDateEntityTestObject{" +
				"startDate=" + this.startDate +
				", endDate=" + this.endDate +
				"} " + super.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
