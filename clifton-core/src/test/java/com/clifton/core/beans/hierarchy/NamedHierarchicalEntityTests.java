package com.clifton.core.beans.hierarchy;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * The <code>NamedHierarchicalEntityTests</code> class defines test methods for {@link NamedHierarchicalEntity}.
 *
 * @author vgomelsky
 */
public class NamedHierarchicalEntityTests {

	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testGetParent() {
		NamedHierarchicalEntity entity = new NamedHierarchicalEntity();
		Assertions.assertNull(entity.getParent(), "First level parent must be null.");

		NamedHierarchicalEntity entity2 = new NamedHierarchicalEntity();
		entity2.setName("Level 2a");
		entity2.setParent(entity);
		Assertions.assertNotNull(entity2.getParent(), "Second level parent must not null.");
	}


	@Test
	@SuppressWarnings({"rawtypes", "unchecked"})
	public void testGetLevel() {
		NamedHierarchicalEntity entity = new NamedHierarchicalEntity();
		entity.setName("Parent Level 1");

		NamedHierarchicalEntity entity2 = new NamedHierarchicalEntity();
		entity2.setName("Level 2a");
		entity2.setParent(entity);

		NamedHierarchicalEntity entity3 = new NamedHierarchicalEntity();
		entity3.setName("Level 2b");
		entity3.setParent(entity);

		NamedHierarchicalEntity entity4 = new NamedHierarchicalEntity();
		entity4.setName("Level 3");
		entity4.setParent(entity2);
		Assertions.assertEquals("Level 2a", entity4.getParent().getName());

		Assertions.assertEquals(1, entity.getLevel());
		Assertions.assertEquals(2, entity2.getLevel());
		Assertions.assertEquals(2, entity3.getLevel());
		Assertions.assertEquals(3, entity4.getLevel());

		Assertions.assertFalse(entity.isLeaf());
		Assertions.assertFalse(entity2.isLeaf());
		Assertions.assertFalse(entity3.isLeaf());
		Assertions.assertFalse(entity4.isLeaf());

		// check for self parent
		entity.setParent(entity);
		Assertions.assertEquals(1, entity.getLevel());
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	@Test
	public void testGetLabelExpanded() {
		NamedHierarchicalEntity parent = new NamedHierarchicalEntity();
		parent.setName("Parent");

		NamedHierarchicalEntity child = new NamedHierarchicalEntity();
		child.setName("Child");
		child.setParent(parent);

		Assertions.assertEquals("Parent / Child", child.getLabelExpanded());
		Assertions.assertEquals("Parent / Child", child.getNameExpanded());

		// not try the infinite loop: should break after 20
		parent.setParent(child);
		Assertions.assertEquals(
				"Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child",
				child.getLabelExpanded());
		Assertions.assertEquals(
				"Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child / Parent / Child",
				child.getNameExpanded());
	}
}
