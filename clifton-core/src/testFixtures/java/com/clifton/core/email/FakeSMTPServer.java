package com.clifton.core.email;

import org.subethamail.wiser.Wiser;


/**
 * Adds the clear messages method to dump the messages from the wiser server without restarting it
 */
public class FakeSMTPServer extends Wiser {

	public FakeSMTPServer(Integer port) {
		super(port);
	}


	public void clearMessages() {
		this.messages.clear();
	}
}
