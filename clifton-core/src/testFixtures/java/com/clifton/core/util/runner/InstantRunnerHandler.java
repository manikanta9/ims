package com.clifton.core.util.runner;


import com.clifton.core.util.CollectionUtils;

import java.util.Date;
import java.util.List;


/**
 * The <code>InstantRunnerHandler</code> class implements a simple runner controller that, when possible,
 * immediately runs the Runner.
 * <p/>
 * This implementation can be useful inside of unit tests to run jobs synchronously.
 *
 * @author vgomelsky
 */
public class InstantRunnerHandler implements RunnerHandler {

	@Override
	public void addRunner(Runner runner) {
		runner.run();
	}


	@Override
	public void addRunnerProvider(@SuppressWarnings("unused") RunnerProvider runnerProvider) {
		// do nothing

	}


	@Override
	public Date getNextScheduleDate() {
		return null;
	}


	@Override
	public List<Runner> getScheduledRunnerList(@SuppressWarnings("unused") String type) {
		return null;
	}


	@Override
	public List<Runner> getScheduledRunnerList(@SuppressWarnings("unused") String type, @SuppressWarnings("unused") String typeId) {
		return null;
	}


	@Override
	public List<Runner> getScheduledRunnerList() {
		return null;
	}


	@Override
	public boolean isRunning() {
		return false;
	}


	@Override
	public void runNow(Runner runner) {
		runner.run();
	}


	@Override
	public void rescheduleRunnersForProviders() {
		//do nothing
	}


	@Override
	public void rescheduleRunner(Runner runner) {
		// SINCE EVERYTHING IS INSTANT - NO ABILITY TO RESCHEDULE
		runNow(runner);
	}


	@Override
	public void rescheduleRunners(List<Runner> runners) {
		for (Runner runner : CollectionUtils.getIterable(runners)) {
			runNow(runner);
		}
	}


	@Override
	public void start() {
		// do nothing
	}


	@Override
	public void start(@SuppressWarnings("unused") long delayMilliseconds) {
		// do nothing
	}


	@Override
	public void stop() {
		// do nothing
	}


	@Override
	public void stop(@SuppressWarnings("unused") boolean softStop) {
		// do nothing
	}


	@Override
	public void clearCompletedRunners() {
		// do nothing
	}


	@Override
	public void cancelRunners(String runnerType) {
		//do nothing
	}


	@Override
	public void cancelRunners(String runnerType, String runnerTypeId) {
		//do nothing
	}

	@Override
	public void terminateRunners(String runnerType, String runnerTypeId) {
		//do nothing
	}


	@Override
	public boolean isRunnerWithTypeAndIdActive(String runnerType, String runnerTypeId) {
		return false;
	}


	/**
	 * This <code>RunnerHandler</code> is not intended for production use because it runs a {@link Runner}
	 * immediately in the thread that invokes {@link InstantRunnerHandler#addRunner(Runner)},
	 * {@link InstantRunnerHandler#runNow(Runner)} or {@link InstantRunnerHandler#rescheduleRunner(Runner)}.
	 */
	@Override
	public RunnerHandlerStatus getStatus(RunnerHandlerStatusCommand command) {
		return new RunnerHandlerStatus.RunnerHandlerStatusBuilder().setStarted(true).build();
	}


	@Override
	public void registerExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		// do nothing
	}


	@Override
	public void deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		// do nothing
	}
}
