package com.clifton.core.util.runner;

import java.util.Date;
import java.util.List;


/**
 * The {@link NoopRunnerHandler} is a simple mocked implementation of {@link RunnerHandler} for no-op processing. This allows triggered runners to be ignored during tests.
 *
 * @author MikeH
 */
public class NoopRunnerHandler implements RunnerHandler {

	@Override
	public void start() {
		// Do nothing
	}


	@Override
	public void start(long delayMilliseconds) {
		// Do nothing
	}


	@Override
	public void stop(boolean softStop) {
		// Do nothing
	}


	@Override
	public void stop() {
		// Do nothing
	}


	@Override
	public boolean isRunning() {
		// Do nothing
		return false;
	}


	@Override
	public void runNow(Runner runner) {
		// Do nothing
	}


	@Override
	public void rescheduleRunnersForProviders() {
		// Do nothing
	}


	@Override
	public void rescheduleRunner(Runner runner) {
		// Do nothing
	}


	@Override
	public void rescheduleRunners(List<Runner> runners) {
		// Do nothing
	}


	@Override
	public void addRunnerProvider(RunnerProvider<?> runnerProvider) {
		// Do nothing
	}


	@Override
	public void addRunner(Runner runner) {
		// Do nothing
	}


	@Override
	public boolean isRunnerWithTypeAndIdActive(String runnerType, String runnerTypeId) {
		// Do nothing
		return false;
	}


	@Override
	public void cancelRunners(String runnerType) {
		// Do nothing
	}


	@Override
	public void cancelRunners(String runnerType, String runnerTypeId) {
		// Do nothing
	}


	@Override
	public void terminateRunners(String runnerType, String runnerTypeId) {
		//do nothing
	}


	@Override
	public void clearCompletedRunners() {
		// Do nothing
	}


	@Override
	public List<Runner> getScheduledRunnerList(String type) {
		// Do nothing
		return null;
	}


	@Override
	public List<Runner> getScheduledRunnerList(String type, String typeId) {
		// Do nothing
		return null;
	}


	@Override
	public List<Runner> getScheduledRunnerList() {
		// Do nothing
		return null;
	}


	@Override
	public Date getNextScheduleDate() {
		// Do nothing
		return null;
	}


	@Override
	public RunnerHandlerStatus getStatus(RunnerHandlerStatusCommand command) {
		return new RunnerHandlerStatus.RunnerHandlerStatusBuilder().setStarted(true).build();
	}


	@Override
	public void registerExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		// Do nothing
	}


	@Override
	public void deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		// Do nothing
	}
}
