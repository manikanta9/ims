package com.clifton.core.context.spring;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.FunctionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.validation.UserIgnorableValidation;
import org.aspectj.lang.annotation.Aspect;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.reflections.Reflections;
import org.reflections.scanners.MethodAnnotationsScanner;
import org.reflections.scanners.SubTypesScanner;
import org.reflections.scanners.TypeAnnotationsScanner;
import org.springframework.context.ApplicationContext;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The {@link AbstractBeanWeavingTests} class provides tests for validating weaving behavior for all existing class path resources. These tests verify that all of the expected
 * types are woven after start-up of the configured Spring context. Given that weaving is typically attached to the class loader during context initialization, it can be difficult
 * to ensure that conditions prevent classes from being loaded before the appropriate instrumentation is registered. This test class attempts to validate that classes are woven
 * properly under existing conditions.
 * <p>
 * Context XML usage:
 * <pre><code>{@literal
 * @ExtendWith(SpringExtension.class)
 * @ContextConfiguration("classpath:/META-INF/spring/app-clifton-ims-context.xml") // A context which enables weaving during context initialization is declared here
 * @Tag("memory-db") // The "memory-db" tag is selected since all tests under this tag use weaving, meaning that weaving will not produce class loader race conditions
 * public class MyWeavingTests extends AbstractBaseWeavingTests {
 *     public MyWeavingTests(ApplicationContext applicationContext) {
 *         super(applicationContext);
 *     }
 * }
 * }</code></pre>
 * <p>
 * Spring Boot context usage:
 * <pre><code>{@literal
 * @SpringBootTest
 * @ContextHierarchy(value = {
 *     // Evaluate context hierarchy as done in the application class
 *     @ContextConfiguration(name = "root", classes = SpringLoadTimeWeavingConfiguration.class),
 *     @ContextConfiguration(name = "child", classes = ReconciliationApplication.class)
 * })
 * @Tag("memory-db") // Run with in-memory database tests so that load-time weaving for transactions is enabled; weaving is disabled during standard unit tests
 * public class ReconciliationBeanWeavingTests extends AbstractBeanWeavingTests {
 *     public ReconciliationBeanWeavingTests(ApplicationContext applicationContext) {
 *         super(applicationContext);
 *     }
 * }
 * }</code></pre>
 *
 * @author MikeH
 */
public abstract class AbstractBeanWeavingTests {

	private static final String ERROR_MSG_DELIMITER = "\n\t";
	private static final String ERROR_MSG_PREFIX = "Unwoven types were found in context beans. This may be indicative of a failure with the weaving system, such as an issue caused by premature class loading. This will prevent aspect functionality such as transaction management and user-ignorable validation exception handling. Violating classes:" + ERROR_MSG_DELIMITER;

	private static final Reflections REFLECTIONS = new Reflections(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION, new TypeAnnotationsScanner(), new MethodAnnotationsScanner(), new SubTypesScanner());

	private final ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractBeanWeavingTests(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates that all eligible types are woven by AspectJ.
	 */
	@Test
	public void testEligibleTypesWoven() {
		// Find all concrete types with applicable annotations
		List<String> errorList = new ArrayList<>();
		Collection<Class<?>> ignoredTypeList = getIgnoredTypeList();
		Collection<String> ignoredPathSubstringList = getIgnoredPathSubstringList();
		Collection<Class<?>> annotatedTypeList = getPointcutConfigurationList().stream()
				.flatMap(pointcutConfiguration -> Stream.concat(
						getAnnotatedTypeList(pointcutConfiguration).stream(),
						getAnnotatedMethodTypeList(pointcutConfiguration, errorList).stream()
				))
				.filter(annotatedType -> !ignoredTypeList.contains(annotatedType))
				.distinct()
				.filter(FunctionUtils.uncheckedPredicate(annotatedType -> {
					String classPathLocation = annotatedType.getProtectionDomain().getCodeSource().getLocation().toString().replace(File.separatorChar, '/');
					return !CollectionUtils.anyMatch(ignoredPathSubstringList, classPathLocation::contains);
				}))
				.collect(Collectors.toSet());
		if (isValidateDiscoverySuccess()) {
			Assertions.assertFalse(annotatedTypeList.isEmpty(), "No types with eligible annotations were discovered. This may be indicative of a test class path or configuration issue.");
		}

		// Validate all classes
		Collection<Class<?>> unwovenBeanClassList = CollectionUtils.getFiltered(annotatedTypeList, annotatedType -> !CoreClassUtils.isAspectJWoven(annotatedType));
		errorList.addAll(CollectionUtils.getConverted(unwovenBeanClassList, Class::getName));

		// Display violations
		if (!errorList.isEmpty()) {
			Assertions.fail(generateViolationMessage(errorList));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Collection<Class<?>> getAnnotatedTypeList(PointcutConfiguration pointcutConfiguration) {
		return REFLECTIONS.getTypesAnnotatedWith(pointcutConfiguration.annotationType).stream()
				.flatMap(annotatedType -> {
					if (annotatedType.isAnnotation() && pointcutConfiguration.allowMetaAnnotations) {
						// Recursively discover types to which meta-annotations are applied
						@SuppressWarnings("unchecked")
						Class<? extends Annotation> metaAnnotatedType = (Class<? extends Annotation>) annotatedType;
						PointcutConfiguration annotatedTypePointcutConfiguration = new PointcutConfiguration(metaAnnotatedType, pointcutConfiguration.allowInterfaceInheritance, pointcutConfiguration.allowMetaAnnotations);
						return getAnnotatedTypeList(annotatedTypePointcutConfiguration).stream();
					}
					else if (annotatedType.isAnnotation()) {
						return Stream.empty();
					}
					else {
						return Stream.of(annotatedType);
					}
				})
				.collect(Collectors.toSet());
	}


	private Collection<Class<?>> getAnnotatedMethodTypeList(PointcutConfiguration pointcut, List<String> errorList) {
		Collection<Method> annotatedMethodList = REFLECTIONS.getMethodsAnnotatedWith(pointcut.annotationType);
		Collection<Class<?>> applicableTypeList = new ArrayList<>();
		for (Method method : annotatedMethodList) {
			Class<?> methodClass = method.getDeclaringClass();
			if (Modifier.isAbstract(method.getModifiers()) && pointcut.allowInterfaceInheritance) {
				if (!methodClass.isInterface()) {
					applicableTypeList.add(methodClass);
				}
				/*
				 * Verify all sub-types implementing this method are woven. This is not a built-in feature for AspectJ and has a unique implementation for annotations such as
				 * UserIgnorableValidation. This incorporates all non-interface sub-types, which may be an overly-conservative approach as some non-interface types in the class
				 * hierarchy may use super- or sub-class implementations for the woven methods and do not need to be woven themselves.
				 */
				List<Class<?>> methodSubTypeList = REFLECTIONS.getSubTypesOf(method.getDeclaringClass()).stream()
						.filter(subType -> !subType.isInterface())
						.collect(Collectors.toList());
				applicableTypeList.addAll(methodSubTypeList);
			}
			else if (Modifier.isAbstract(method.getModifiers())) {
				// Handle disallowed abstract methods
				errorList.add(String.format("Abstract method [%s#%s] is annotated with [%s], but annotation pointcuts do not apply to abstract methods.", methodClass.getName(), method.getName(), pointcut.annotationType.getSimpleName()));
			}
			else {
				// Handle method implementations
				applicableTypeList.add(methodClass);
			}
		}
		return applicableTypeList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of {@link Annotation} types which, when present on a type or method, indicate that the target should be affected by existing {@link Aspect aspects}.
	 */
	protected Collection<PointcutConfiguration> getPointcutConfigurationList() {
		return CollectionUtils.createList(
				new PointcutConfiguration(Transactional.class, false, true),
				new PointcutConfiguration(UserIgnorableValidation.class, true, false)
		);
	}


	/**
	 * Returns the list of types ignored during weaving tests.
	 */
	protected Collection<Class<?>> getIgnoredTypeList() {
		return Collections.emptyList();
	}


	/**
	 * Returns the list of class-file location substrings for which all matching classes will be ignored.
	 */
	protected Collection<String> getIgnoredPathSubstringList() {
		return CollectionUtils.createList(
				"/java/test/" // Test classes should not be woven; @Transactional annotations are handled with test execution listeners
		);
	}


	/**
	 * If {@code true}, then the test will validate that at least one annotated type or method is discovered. In this case, the absence of annotated types will automatically cause
	 * the test to fail. No discoverable annotated types may be indicative of a test class path or configuration issue.
	 */
	protected boolean isValidateDiscoverySuccess() {
		return true;
	}


	/**
	 * Generates the violation message from the given list of errors.
	 */
	private String generateViolationMessage(Collection<String> errorList) {
		return errorList.stream()
				.sorted()
				.collect(Collectors.joining(ERROR_MSG_DELIMITER, ERROR_MSG_PREFIX, StringUtils.EMPTY_STRING));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class PointcutConfiguration {

		private final Class<? extends Annotation> annotationType;
		private final boolean allowInterfaceInheritance;
		private final boolean allowMetaAnnotations;


		public PointcutConfiguration(Class<? extends Annotation> annotationType, boolean allowInterfaceInheritance, boolean allowMetaAnnotations) {
			this.annotationType = annotationType;
			this.allowInterfaceInheritance = allowInterfaceInheritance;
			this.allowMetaAnnotations = allowMetaAnnotations;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
