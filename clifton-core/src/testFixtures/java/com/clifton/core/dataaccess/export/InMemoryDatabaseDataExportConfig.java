package com.clifton.core.dataaccess.export;

import com.clifton.core.context.config.AppConfig;
import com.clifton.core.context.config.dataaccess.BaseHibernateDatabaseConfig;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.dao.locator.DaoLocatorInSpringContext;
import com.clifton.core.json.custom.CustomJsonStringStaticContextInitializer;
import com.clifton.core.json.custom.util.CustomJsonObjectUtilsStaticContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;

import java.util.ArrayList;
import java.util.List;


/**
 * @author StevenF
 */
public class InMemoryDatabaseDataExportConfig extends BaseHibernateDatabaseConfig implements AppConfig {


	@Bean
	@Override
	public DaoLocator daoLocator() {
		return new DaoLocatorInSpringContext();
	}


	@Bean
	public JsonHandler<?> jsonHandler() {
		return new JacksonHandlerImpl();
	}


	@Bean
	public CustomJsonObjectUtilsStaticContextInitializer customJsonObjectUtilsStaticContextInitializer() {
		return new CustomJsonObjectUtilsStaticContextInitializer();
	}


	@Bean
	public CustomJsonStringStaticContextInitializer customJsonStringStaticContextInitializer() {
		return new CustomJsonStringStaticContextInitializer();
	}


	@Bean
	public InMemoryDatabaseDataExportHandler inMemoryDatabaseDataExportHandler(DaoLocator daoLocator, JdbcTemplate jdbcTemplate) {
		return new InMemoryDatabaseDataExportHandler(daoLocator, jdbcTemplate);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Resource[] getMappingJarLocations() {
//		File dir = new File("C:/work/workspace/app-clifton-ims/build/dist/app-clifton-ims/WEB-INF/lib/");
//		File[] jarFiles = dir.listFiles(new FilenameFilter() {
//			@Override
//			public boolean accept(File dir, String name) {
//				return name.startsWith("clifton-");
//			}
//		});
//		if (jarFiles != null) {
//			Resource[] jarResources = new FileSystemResource[jarFiles.length];
//			for (int i = 0; i < jarFiles.length; i++) {
//				jarResources[i] = new FileSystemResource(jarFiles[i]);
//			}
//			return jarResources;
//		}
		return new Resource[]{};
	}


	@Override
	public String[] getMappingResources() {
		List<String> schemaMappingResourceList = new ArrayList<>();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		for (String project : getProjectNameList()) {
			String resourceName = "META-INF/schema/schema-hibernate-" + project + ".xml";
			if (classLoader.getResource(resourceName) != null) {
				schemaMappingResourceList.add(resourceName);
			}
		}
		return schemaMappingResourceList.toArray(new String[0]);
	}


	@Override
	public String[] getProjectNameList() {
		List<String> projectNameList = new ArrayList<>();
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		PathMatchingResourcePatternResolver schemaResourceResolver = new PathMatchingResourcePatternResolver(classLoader);
		try {
			Resource[] schemaResources = schemaResourceResolver.getResources("classpath*:META-INF/schema/schema-hibernate-*.xml");
			for (Resource resource : schemaResources) {
				if (resource.getURL().getPath() != null) {
					String path = resource.getURL().getPath();
					if (!(path.contains("memory") || path.contains("queries"))) {
						projectNameList.add(path.substring(path.indexOf("META-INF")).replace("META-INF/schema/schema-hibernate-", "").replace(".xml", ""));
					}
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred scanning for Hibernate schema resources.", e);
		}
		return projectNameList.toArray(new String[0]);
	}
}
