package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.security.SecurityUser;


/**
 * The <code>User</code> class defines a user in the system. Used for tests in Core because actual users
 * are in Security project
 *
 * @author manderson
 */
public class User extends BaseEntity<Short> implements LabeledObject, SecurityUser {

	private String firstName;
	private String lastName;


	@Override
	public String getLabel() {
		return this.firstName + " " + this.lastName;
	}


	@Override
	public String getUserName() {
		return getLabel();
	}


	@Override
	public String getEmailAddress() {
		return "";
	}


	public String getFirstName() {
		return this.firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return this.lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
}
