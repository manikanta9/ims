package com.clifton.core.dataaccess.export;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.converter.json.jackson.strategy.JacksonHibernateStrategy;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compression.GzipCompressionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringEscapeUtils;
import org.hibernate.HibernateException;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.tool.hbm2ddl.MultipleLinesSqlCommandExtractor;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.sql.ResultSet;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;


/**
 * The {@link InMemoryDatabaseDataExportHandler} class is used to export SQL data used for in-memory database tests. See {@link InMemoryDatabaseDataExportRunner} for more details.
 *
 * @author stevenf
 * @author MikeH
 */
public class InMemoryDatabaseDataExportHandler {

	private static final String EXPORT_SQL_FILE_SUFFIX = "DataExport.sql";
	private static final String DATA_FILE_SUFFIX = "Data.xml";
	private static final String EXPORT_RESOURCE_PATH_REPLACEMENT_FROM = "build" + File.separator + "resources" + File.separator + "test";
	private static final String EXPORT_RESOURCE_PATH_REPLACEMENT_TO = "src" + File.separator + "test" + File.separator + "java";
	private static final String TABLE_COLUMN_ORDINAL_POSITIONS = "SELECT COLUMN_NAME, ORDINAL_POSITION FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = '%s'";

	private static final char ENTITY_KEY_SEPARATOR = '#';
	private static final String COLUMN_SEPARATOR = ", ";
	private static final String NULL_VALUE = "NULL";
	private static final String ENTITY_TABLE_COLUMN_NAME = "entityTableName";
	private static final String ENTITY_ID_COLUMN_NAME = "entityId";
	private static final String RV_COLUMN_NAME = "rv";
	private static final String HEXES = "0123456789ABCDEF";

	private final ObjectMapper mapper = new JacksonObjectMapper(new JacksonHibernateStrategy());

	private final DaoLocator daoLocator;
	private final JdbcTemplate jdbcTemplate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public InMemoryDatabaseDataExportHandler(DaoLocator daoLocator, JdbcTemplate jdbcTemplate) {
		this.daoLocator = daoLocator;
		this.jdbcTemplate = jdbcTemplate;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void generateData(String inputFileName, String outputFileName) {
		try {
			PathMatchingResourcePatternResolver extractorFileResolver = new PathMatchingResourcePatternResolver(Thread.currentThread().getContextClassLoader());
			Resource[] extractorResources = inputFileName != null
					? new Resource[]{new FileSystemResource(inputFileName)}
					: extractorFileResolver.getResources("classpath*:com/clifton/**/data/*" + EXPORT_SQL_FILE_SUFFIX);

			ArrayUtils.getStream(extractorResources).parallel().forEach(extractorResource -> {
				try {
					// Only process those export resources that are local to this project - dependencies will be in a jar
					if ("file".equals(extractorResource.getURL().getProtocol())) {
						long start = System.nanoTime();

						File inputFile = extractorResource.getFile();
						File outputFile = outputFileName != null
								? new File(outputFileName)
								: new File(inputFile.getParentFile(), inputFile.getName().replace(EXPORT_SQL_FILE_SUFFIX, DATA_FILE_SUFFIX));

						// Append semi-colon to export script to address terminating semi-colon parser requirement
						String exportSql = FileUtils.readFileToString(inputFile);
						if (!exportSql.matches("(?s).*;\\s*")) {
							exportSql += ';';
						}
						MultipleLinesSqlCommandExtractor sqlCommandExtractor = new MultipleLinesSqlCommandExtractor();
						String[] sql = sqlCommandExtractor.extractCommands(new StringReader(exportSql));
						String exportedData = generateMigrationStatementsBySql(sql);
						FileUtils.writeStringToFile(outputFile, exportedData);

						if (outputFileName == null) {
							// The outputFile was in the test resource output and was updated so in memory database tests will succeed.
							// Copy the of the outputFile to the test source outputFile as well so changes can be seen and committed.
							FileUtils.copyFileOverwrite(outputFile,
									new File(outputFile.getParentFile().getPath().replace(EXPORT_RESOURCE_PATH_REPLACEMENT_FROM, EXPORT_RESOURCE_PATH_REPLACEMENT_TO),
											outputFile.getName()));
						}
						LogUtils.info(InMemoryDatabaseDataExportHandler.class, "Took " + TimeUnit.MILLISECONDS.convert((System.nanoTime() - start), TimeUnit.NANOSECONDS) + " milliseconds to process export for: " + extractorResource.getFilename());
					}
				}
				catch (Exception e) {
					LogUtils.error(InMemoryDatabaseDataExportHandler.class, "Error occurred when processing data export for: " + extractorResource.getFilename(), e);
				}
			});
		}
		catch (IOException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Generates the SQL inserts needed for InMemoryDatabaseTests based on tableNames and ids returned by SQL
	 * The queries generated at this point are only appropriate for the HSQL
	 * database we use (e.g. we don't include the 'rv' field and date objects
	 * are formatted specifically in order to utilize the CAST function.
	 */
	@Transactional
	protected String generateMigrationStatementsBySql(String[] sqlCommands) {
		Map<String, String> queryMap = new TreeMap<>(); // use TreeMap for sorting
		Set<String> walkedObjects = new HashSet<>();
		for (String sql : sqlCommands) {
			PreparedStatementCreator statementCreator = con -> con.prepareStatement(sql, ResultSet.TYPE_FORWARD_ONLY, ResultSet.CONCUR_READ_ONLY);
			getJdbcTemplate().query(statementCreator, (rs -> {
				String entityTableName = rs.getString(ENTITY_TABLE_COLUMN_NAME);
				Serializable entityId = (Serializable) rs.getObject(ENTITY_ID_COLUMN_NAME);
				generateSqlForEntity(getDaoLocator(), entityTableName, entityId, queryMap, walkedObjects);
			}));
		}
		return createInMemoryDatabaseMigrationStatement(queryMap);
	}


	private void generateSqlForEntity(DaoLocator daoLocator, String entityTableName, Serializable entityId, Map<String, String> queryMap, Set<String> walkedObjects) {
		ReadOnlyDAO<IdentityObject> entityDao = daoLocator.locate(entityTableName);
		IdentityObject entity = entityDao.findByPrimaryKey(entityId);

		boolean firstColumn = true;
		StringBuilder queryColumns = new StringBuilder();
		StringBuilder queryValues = new StringBuilder();
		Collection<Column> fullColumnCollection = getFullColumnCollection(entityDao.getConfiguration());
		Map<String, Integer> ordinals = getTableColumnOrdinals(entityTableName);
		fullColumnCollection = fullColumnCollection.stream()
				.sorted((o1, o2) -> MathUtils.compare(ordinals.get(o1.getName()), ordinals.get(o2.getName())))
				.collect(Collectors.toList());
		for (Column column : CollectionUtils.getIterable(fullColumnCollection)) {
			//Skip the rv field - we don't use it in the in memory database
			if (RV_COLUMN_NAME.equals(column.getName())) {
				continue;
			}
			if (!firstColumn) {
				queryColumns.append(COLUMN_SEPARATOR);
				queryValues.append(COLUMN_SEPARATOR);
			}
			if (column.getFkTable() != null) {
				String fkTableName = column.getFkTable();
				Serializable fkFieldId = getFkFieldId(entity, column.getBeanPropertyName());
				if (fkFieldId != null) {
					//if this entity has not been walked - walk it
					if (walkedObjects.add(fkTableName + ENTITY_KEY_SEPARATOR + fkFieldId)) {
						generateSqlForEntity(daoLocator, fkTableName, fkFieldId, queryMap, walkedObjects);
					}
				}
			}
			queryColumns.append(StringEscapeUtils.escapeSql(column.getName()));
			queryValues.append(getSQlValueForColumn(column, entity));
			firstColumn = false;
		}

		StringBuilder query = new StringBuilder("INSERT INTO ").append(entityTableName)
				.append(" (").append(queryColumns).append(") VALUES (").append(queryValues).append(");");
		queryMap.put(entityTableName + ENTITY_KEY_SEPARATOR + entityId, query.toString());
	}


	@Transactional(propagation = Propagation.REQUIRES_NEW)
	protected Map<String, Integer> getTableColumnOrdinals(String tableName) {
		final Map<String, Integer> ordinals = new HashMap<>();
		getJdbcTemplate().query(String.format(TABLE_COLUMN_ORDINAL_POSITIONS, tableName), (ResultSet rs) -> {
			ordinals.put(rs.getString(1), rs.getInt(2));
		});
		return ordinals;
	}


	private Collection<Column> getFullColumnCollection(DAOConfiguration<?> daoConfiguration) {
		Set<Column> columnSet = new LinkedHashSet<>(daoConfiguration.getColumnList());
		for (Subclass subclass : CollectionUtils.getIterable(daoConfiguration.getTable().getSubclassList())) {
			columnSet.addAll(subclass.getColumnList());
		}
		return columnSet;
	}


	private Serializable getFkFieldId(IdentityObject entity, String fkFieldName) {
		Object object = BeanUtils.getPropertyValue(entity, fkFieldName);
		if (object != null) {
			if (object instanceof IdentityObject) {
				return ((IdentityObject) object).getIdentity();
			}
			else if (object instanceof HibernateProxy) {
				return ((HibernateProxy) object).getHibernateLazyInitializer().getIdentifier();
			}
		}
		return null;
	}


	private String getSQlValueForColumn(Column column, IdentityObject entity) {
		StringBuilder sqlValue = new StringBuilder();
		Object value = BeanUtils.getPropertyValue(entity, column.getBeanPropertyName(), true);
		if (value == null) {
			sqlValue.append(NULL_VALUE);
		}
		else if (value instanceof IdentityObject) {
			sqlValue.append(((IdentityObject) value).getIdentity());
		}
		else if (value instanceof HibernateProxy) {
			sqlValue.append(((HibernateProxy) value).getHibernateLazyInitializer().getIdentifier());
		}
		else if (DataTypeNames.DATE == column.getDataType().getTypeName()) {
			sqlValue.append("CAST('").append(DateUtils.fromDate((Date) value, DateUtils.DATE_FORMAT_SQL_PRECISE)).append("' AS DateTime)");
		}
		else if (DataTypeNames.BOOLEAN == column.getDataType().getTypeName() || DataTypeNames.INTEGER == column.getDataType().getTypeName() || DataTypeNames.DECIMAL == column.getDataType().getTypeName()) {
			if (value instanceof Time) {
				value = ((Time) value).getMilliseconds();
			}
			sqlValue.append(value);
		}
		else if (DataTypes.COMPRESSED_TEXT == column.getDataType()) {
			try {
				sqlValue.append("'");
				byte[] compressed = GzipCompressionUtils.compress(getMapper().writeValueAsBytes(value));
				for (final byte b : compressed) {
					// https://stackoverflow.com/questions/2817752/java-code-to-convert-byte-to-hexadecimal#2817883
					sqlValue.append(HEXES.charAt((b & 0xF0) >> 4)).append(HEXES.charAt((b & 0x0F)));
				}
				sqlValue.append("'");
			}
			catch (IOException ioe) {
				throw new HibernateException("unable to set object to result set", ioe);
			}
		}
		else {
			sqlValue.append("'").append(StringEscapeUtils.escapeSql(String.valueOf(value))).append("'");
		}
		return sqlValue.toString();
	}


	//TODO - externalize this - or create 'XmlMigrationDefinitionWriter'
	private String createInMemoryDatabaseMigrationStatement(Map<String, String> queryMap) {
		StringBuilder queryStringBuilder = new StringBuilder()
				.append("<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n")
				.append("<migrations xmlns=\"https://nexus.paraport.com/repository/schema/migrations\"\n")
				.append("            xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"\n")
				.append("            xsi:schemaLocation=\"https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd\">\n")
				.append("\t<sql>\n")
				.append("\t\t<statement>\n")
				.append("\t\t\t<![CDATA[\n")
				.append("\t\t\t\tSET DATABASE REFERENTIAL INTEGRITY FALSE;\n");

		// re-map output so each table has its own section where the inserts occur
		Map<String, Map<String, String>> queryTableMap = convertQueryMapToQueryTableMap(queryMap);
		for (Map.Entry<String, Map<String, String>> queryTableMapEntry : queryTableMap.entrySet()) {
			queryStringBuilder.append("\n\t\t\t-- BEGIN TABLE: ").append(queryTableMapEntry.getKey()).append("\n");
			queryTableMapEntry.getValue().values().forEach(query -> queryStringBuilder.append("\t\t\t\t").append(query).append("\n"));
			queryStringBuilder.append("\t\t\t-- END TABLE: ").append(queryTableMapEntry.getKey()).append("\n");
		}

		return queryStringBuilder.append("\n\t\t\t\tSET DATABASE REFERENTIAL INTEGRITY TRUE;\n")
				.append("\t\t\t]]>\n")
				.append("\t\t</statement>\n")
				.append("\t</sql>\n")
				.append("</migrations>\n")
				.toString();
	}


	private Map<String, Map<String, String>> convertQueryMapToQueryTableMap(Map<String, String> queryMap) {
		Map<String, Map<String, String>> queryTableMap = new TreeMap<>();
		for (Map.Entry<String, String> queryMapEntry : queryMap.entrySet()) {
			String tableName = StringUtils.substringBeforeFirst(queryMapEntry.getKey(), ENTITY_KEY_SEPARATOR + "");
			queryTableMap.putIfAbsent(tableName, new TreeMap());
			queryTableMap.get(tableName).put(queryMapEntry.getKey(), queryMapEntry.getValue());
		}
		return queryTableMap;
	}


	private String createSqlStatementElement(String sql) {
		return new StringBuilder("\t\t<statement>\n")
				.append("\t\t\t<![CDATA[\n")
				.append("\t\t\t\t").append(sql).append('\n')
				.append("\t\t\t]]>\n")
				.append("\t\t</statement>\n").toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ObjectMapper getMapper() {
		return this.mapper;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}
}
