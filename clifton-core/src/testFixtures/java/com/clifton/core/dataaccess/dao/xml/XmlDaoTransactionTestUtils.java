package com.clifton.core.dataaccess.dao.xml;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.context.ApplicationContext;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>XmlDaoTransactionTestUtils</code> contains the utility methods for enabling, cleaning up, and tracking xml dao bean data updates for use in "transactional" type xml dao data testing.
 *
 * @author manderson
 */
public class XmlDaoTransactionTestUtils {


	private static final String TRANSACTIONAL_TEST_ENABLED_KEY = "TRANSACTIONAL_TEST_ENABLED";

	private static final String TRANSACTIONAL_TEST_UPDATED_BEAN_MAP_KEY = "TRANSACTIONAL_TEST_UPDATED_BEAN_MAP";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Turns on the Transactional Test property in context handler, and also creates an empty map to store affected dao data bean names and original beans (for updates only)
	 * For inserts/deletes we just track the dao dataBeanName that is affected.  Updates we track the original properties before the first update.  We must do this because if that
	 * bean is referenced by another bean in the context (i.e. FK field) then cloning the bean breaks the link.
	 */
	public static void setupContextBeforeTest(ContextHandler contextHandler) {
		contextHandler.setBean(TRANSACTIONAL_TEST_ENABLED_KEY, true);
		contextHandler.setBean(TRANSACTIONAL_TEST_UPDATED_BEAN_MAP_KEY, new HashMap<>());
	}


	/**
	 * Cleans up the context after the test completes.  All original beans that were updated are restored in the application context
	 */
	@SuppressWarnings("unchecked")
	public static void cleanUpContextAfterTest(ContextHandler contextHandler, ApplicationContext applicationContext) {
		try {
			Map<String, Map<Serializable, IdentityObject>> updatedDaoDataBeanMap = getTransactionalTestUpdatedBeanMap(contextHandler, true);

			if (updatedDaoDataBeanMap != null) {
				for (Map.Entry<String, Map<Serializable, IdentityObject>> updatedDaoDataBeanMapEntry : updatedDaoDataBeanMap.entrySet()) {
					String dataBeanName = updatedDaoDataBeanMapEntry.getKey();
					// Remove Local Copy
					contextHandler.removeBean(dataBeanName);

					Map<Serializable, IdentityObject> updatedBeanMap = updatedDaoDataBeanMapEntry.getValue();
					if (!CollectionUtils.isEmpty(updatedBeanMap) && applicationContext.containsBean(dataBeanName)) {
						List<IdentityObject> entityList = (List<IdentityObject>) applicationContext.getBean(dataBeanName);
						for (IdentityObject entity : CollectionUtils.getIterable(entityList)) {
							IdentityObject originalEntity = updatedBeanMap.get(entity.getIdentity());
							if (originalEntity != null) {
								BeanUtils.copyProperties(originalEntity, entity);
							}
						}
					}
				}
			}
		}
		finally {
			contextHandler.setBean(TRANSACTIONAL_TEST_ENABLED_KEY, false);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static boolean isTransactionTestEnabled(ContextHandler contextHandler) {
		return contextHandler != null && BooleanUtils.isTrue(contextHandler.getBean(TRANSACTIONAL_TEST_ENABLED_KEY));
	}


	public static boolean isDataBeanNameRegistered(ContextHandler contextHandler, String dataBeanName) {
		if (isTransactionTestEnabled(contextHandler)) {
			return getTransactionalTestUpdatedBeanMap(contextHandler, false).containsKey(dataBeanName);
		}
		return false;
	}


	public static Map<Serializable, IdentityObject> registerDataBeanNameForTransactionalTest(ContextHandler contextHandler, String dataBeanName) {
		if (isTransactionTestEnabled(contextHandler)) {
			return getTransactionalTestUpdatedBeanMap(contextHandler, false).computeIfAbsent(dataBeanName, key -> new HashMap<>());
		}
		return null;
	}


	public static void registerOriginalBeanForTransactionalTest(ContextHandler contextHandler, String dataBeanName, IdentityObject originalBean) {
		if (isTransactionTestEnabled(contextHandler)) {
			Map<Serializable, IdentityObject> map = registerDataBeanNameForTransactionalTest(contextHandler, dataBeanName);
			if (map != null && originalBean != null) {
				map.computeIfAbsent(originalBean.getIdentity(), key -> BeanUtils.cloneBean(originalBean, false, true));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	private static Map<String, Map<Serializable, IdentityObject>> getTransactionalTestUpdatedBeanMap(ContextHandler contextHandler, boolean removeBean) {
		return (Map<String, Map<Serializable, IdentityObject>>) (removeBean ? contextHandler.removeBean(TRANSACTIONAL_TEST_UPDATED_BEAN_MAP_KEY) : contextHandler.getBean(TRANSACTIONAL_TEST_UPDATED_BEAN_MAP_KEY));
	}
}
