package com.clifton.core.dataaccess.dao.xml;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.BaseObserverableDAOImpl;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.ExistsSubqueryExpression;
import org.hibernate.criterion.LogicalExpression;

import java.beans.PropertyDescriptor;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>XmlReadOnlyDAO</code> class implements a DAO object that uses Spring's context as its data store (not persistent).
 * It retrieves, updates, creates, deletes DTO objects inside of Spring's context.
 * <p>
 * The class uses the following naming convention for its data store: if DAO bean name is "testDAO" then data bean name is "testDAO_data".
 * The data bean is a List.
 *
 * @param <T>
 * @author vgomelsky
 */
public class XmlReadOnlyDAO<T extends IdentityObject> extends BaseObserverableDAOImpl<T> implements AdvancedReadOnlyDAO<T, Criteria> {

	/**
	 * Set the following properties if configuration is not injected.
	 */
	private String dtoClassName;
	/**
	 * Specifies whether instances of {@link ExistsSubqueryExpression} should evaluate to true or false.
	 */
	private boolean existsEvaluatesToTrue = true;

	/**
	 * Specifies whether instances of {@link LogicalExpression} should evaluate to true or false
	 */
	private boolean logicalEvaluatesToTrue = false;

	private boolean ignoreJoins = false;

	private ContextHandler contextHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<T> findAll() {
		return findAll(true);
	}


	@SuppressWarnings("unchecked")
	protected List<T> findAll(boolean cloned) {
		List<T> list = null;
		String dataBeanName = getDataBeanName();
		if (XmlDaoTransactionTestUtils.isDataBeanNameRegistered(this.contextHandler, dataBeanName)) {
			list = findAllForTransactionalTest();
		}
		else {
			if (getApplicationContext().containsBean(dataBeanName)) {
				list = (List<T>) getApplicationContext().getBean(dataBeanName);
			}
		}
		if (!cloned || list == null) {
			return list;
		}
		return cloneList(list);
	}


	@SuppressWarnings("unchecked")
	private List<T> findAllForTransactionalTest() {
		String dataBeanName = getDataBeanName();
		List<T> list = (List<T>) getContextHandler().getBean(dataBeanName);
		if (list == null) {
			if (getApplicationContext().containsBean(dataBeanName)) {
				list = (List<T>) getApplicationContext().getBean(dataBeanName);
				List<T> copyOfList = new ArrayList<>(list);
				this.contextHandler.setBean(dataBeanName, copyOfList);
				list = copyOfList;
			}
		}
		return list;
	}


	@Override
	public List<T> findByField(String beanFieldName, Object value) {
		return findByField(beanFieldName, new Object[]{value});
	}


	@Override
	public List<T> findByField(String beanFieldName, Object[] values) {
		return cloneList(BeanUtils.filterByPropertyName(findAll(false), beanFieldName, values, false, true));
	}


	@Override
	public List<T> findByFields(String[] beanFieldNames, Object[] values) {
		int restrictionCount = beanFieldNames.length;
		if (restrictionCount != values.length) {
			throw new IllegalArgumentException("The number of beanFieldNames must be the same as the number of values: " + restrictionCount + " <> " + values.length);
		}
		List<T> result = findAll(false);
		for (int i = 0; i < restrictionCount; i++) {
			result = BeanUtils.filterByPropertyName(result, beanFieldNames[i], new Object[]{values[i]}, false, true);
		}
		return cloneList(result);
	}


	@Override
	@SuppressWarnings("unused")
	public List<T> findByNamedQuery(String queryName, Object... input) {
		throw new UnsupportedOperationException("Named Query not supported.");
	}


	@Override
	public T findByNaturalKey(T searchObject) {
		return findByNaturalKey(searchObject, true);
	}


	public T findByNaturalKey(T searchObject, boolean cloned) {
		Set<String> naturalKeys = getConfiguration().getNaturalKeys((DaoLocator) getApplicationContext().getBean("daoLocator"));
		List<T> list = findAll(false);
		if (list != null) {
			for (T bean : list) {
				boolean match = !naturalKeys.isEmpty();
				for (String naturalKey : naturalKeys) {
					Object beanValue = BeanUtils.getPropertyValue(bean, naturalKey);
					Object identityObjectValue = BeanUtils.getPropertyValue(searchObject, naturalKey);
					if (!beanValue.equals(identityObjectValue)) {
						match = false;
					}
				}
				if (match) {
					return cloned ? BeanUtils.cloneBean(bean, true, true) : bean;
				}
			}
		}
		return null;
	}


	@Override
	public T findByPrimaryKey(Serializable primaryKey) {
		return findByPrimaryKey(primaryKey, true);
	}


	protected T findByPrimaryKey(Serializable primaryKey, boolean cloned) {
		List<T> list = findAll(false);
		if (list != null) {
			for (T bean : list) {
				// Support for Long - Int - Short comparisons
				if (bean.getIdentity().equals(primaryKey) || (primaryKey instanceof Number && MathUtils.isEqual((Number) bean.getIdentity(), (Number) primaryKey))) {
					return cloned ? BeanUtils.cloneBean(bean, true, true) : bean;
				}
			}
		}
		return null;
	}


	@Override
	public T findOneByField(String beanFieldName, Object value) {
		return findOneByFields(new String[]{beanFieldName}, new Object[]{value});
	}


	@Override
	public T findOneByFields(String[] beanFieldNames, Object[] values) {

		List<T> list = findByFields(beanFieldNames, values);

		int count = CollectionUtils.getSize(list);
		if (count == 0) {
			return null;
		}
		if (count > 1) {
			throw new IllegalStateException("The query for " + getConfiguration().getTableName() + " with parameters (" + ArrayUtils.toString(beanFieldNames) + ") and values ("
					+ ArrayUtils.toString(values) + ") cannot return more than 1 row; count = " + count);
		}

		return list.get(0);
	}


	@Override
	public List<T> findByPrimaryKeys(Serializable[] ids) {
		// No Special Logic here - code filtering, so just use regular findByField logic
		return findByField("id", ids);
	}


	@Override
	@SuppressWarnings("unchecked")
	public List<T> findBySearchCriteria(SearchConfigurer<Criteria> searchConfigurer) {
		if (searchConfigurer != null) {
			Criteria criteria = new XmlBasedCriteria<>(getConfiguration(), getApplicationContext(), this.existsEvaluatesToTrue, this.logicalEvaluatesToTrue, this.ignoreJoins);
			searchConfigurer.configureCriteria(criteria);
			searchConfigurer.configureOrderBy(criteria);

			int limit = searchConfigurer.getLimit();
			// check if it's a simple case with no paging
			if (searchConfigurer.getStart() == 0 && (limit == Integer.MAX_VALUE || limit < 10)) {
				if (limit < 10) {
					// if limit is less than 10 then we are not using pagination (doesn't make sense to page with such small pages)
					// instead, one is likely trying to limit the number of rows returned with TOP clause (usually TOP 1)
					criteria.setMaxResults(limit);
				}
			}

			return criteria.list();
		}
		return null;
	}


	@Override
	public DAOConfiguration<T> getConfiguration() {
		if (super.getConfiguration() == null) {
			// if not injected, try to create what's possible using reflection
			Table table = new Table();
			table.setName(StringUtils.capitalize(getBeanName().substring(0, getBeanName().length() - 3)));
			table.setDtoClass(getDtoClassName());
			try {
				List<Column> columnList = new ArrayList<>();
				Class<?> dtoClass = Class.forName(getDtoClassName());
				Map<String, PropertyDescriptor> propertyMap = BeanUtils.getPropertyDataTypes(dtoClass);
				for (String propertyName : propertyMap.keySet()) {
					Column column = new Column();
					column.setBeanPropertyName(propertyName);
					column.setRequired(false);
					columnList.add(column);
				}
				table.setColumnList(columnList);
			}
			catch (ClassNotFoundException e) {
				throw new RuntimeException("Cannot load columns for DTO class: " + getDtoClassName(), e);
			}
			setConfiguration(new DAOConfiguration<>(table));
		}
		return super.getConfiguration();
	}


	/**
	 * Creates a deep clone of the specified list.
	 *
	 * @param list
	 */
	@SuppressWarnings("rawtypes")
	private List<T> cloneList(List<T> list) {
		if (list == null) {
			return null;
		}
		List<T> clonedList = new ArrayList<>();
		for (T bean : list) {
			if (bean instanceof BaseEntity && ((BaseEntity) bean).getRv() == null) {
				((BaseEntity) bean).setRv(new byte[0]);
			}
			T clonedBean = BeanUtils.cloneBean(bean, true, true);
			clonedList.add(clonedBean);
		}
		return clonedList;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDataBeanName() {
		AssertUtils.assertNotNull(getBeanName(), "Bean name is not set");
		return getBeanName() + "_data";
	}


	public String getDtoClassName() {
		return this.dtoClassName;
	}


	public void setDtoClassName(String dtoClassName) {
		this.dtoClassName = dtoClassName;
	}


	public boolean isExistsEvaluatesToTrue() {
		return this.existsEvaluatesToTrue;
	}


	public void setExistsEvaluatesToTrue(boolean existsEvaluatesToTrue) {
		this.existsEvaluatesToTrue = existsEvaluatesToTrue;
	}


	public boolean isLogicalEvaluatesToTrue() {
		return this.logicalEvaluatesToTrue;
	}


	public void setLogicalEvaluatesToTrue(boolean logicalEvaluatesToTrue) {
		this.logicalEvaluatesToTrue = logicalEvaluatesToTrue;
	}


	public boolean isIgnoreJoins() {
		return this.ignoreJoins;
	}


	public void setIgnoreJoins(boolean ignoreJoins) {
		this.ignoreJoins = ignoreJoins;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
