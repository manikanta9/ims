package com.clifton.core.dataaccess.export;


import com.clifton.core.context.spring.SpringLoadTimeWeavingConfiguration;
import com.clifton.core.dataaccess.migrate.MigrationCommands;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import java.util.Map;
import java.util.Objects;


/**
 * The <code>InMemoryDatabaseDataExportRunner</code> class is used to export SQL data used for in memory database tests.
 * It supports a variety of configuration flags documented here:
 * <p>
 * The intent is to allow for exporting static data for use in unit tests for complex tests that are too difficult
 * to mock efficiently.
 * <p>
 * It is preferred to use the Gradle task to execute this class because Gradle properly sets the classpath and JVM system
 * properties needed to execute it. Gradle can execute this class for each module, or subproject, scanning the module's test
 * resources for data export files using the pattern, classpath*:com/clifton/**&#47;data/*DataExport.sql. For each export file,
 * a test data file will be created in the same directory with the same name with the DataExport.sql suffix replaced with
 * Data.xml.
 * <p>
 * To run this class manually, the module classpath must be defined with all dependencies, libraries and other modules, and
 * the following JVM system properties are required:
 * <br/>- jdbc.driverClassName
 * <br/>- jdbc.url
 * <br/>- jdbc.username
 * <br/>- jdbc.password
 * <br/>- hibernate.dialect
 * <br/>- hibernate.show_sql
 * <br/>- hibernate.format_sql
 * <br/>- hibernate.generate_statistics
 * <p>
 * You can optionally provide arguments for the input and output files rather than scanning the classpath:
 * <br/>{@code java -cp ... com.clifton.core.dataaccess.export.InMemoryDatabaseDataExportRunner C:\work\export.sql
 * C:\work\Workspace\clifton-collateral\test\java\com\clifton\collateral\balance\booking\data\CollateralBalanceBookingRuleTestsData.xml}
 * <p>
 * A number of future enhancements can be made such as allowing the query export functionality to utilize the UsedBy
 * functionality to better infer the data needed for a test from a base entity, or additional program arguments could
 * be used to specify exporting supporting data such as marketData and calendar data.
 *
 * @author stevenf
 */
public class InMemoryDatabaseDataExportRunner {

	/**
	 * Executes command specified by the first argument.
	 * <p>
	 * First argument is one of {@link MigrationCommands}:
	 * Second argument specifies path to the spring context file that has "migrationService" bean defined.
	 */
	public static void main(String[] args) {
		try {
			// Determine export parameters
			String inputFileName;
			String outputFileName;
			if (!ArrayUtils.isEmpty(args)) {
				inputFileName = Objects.requireNonNull(args[0], "You must specify a SQL input file.");
				outputFileName = Objects.requireNonNull(args[1], "You must specify a SQL output file.");
				ValidationUtils.assertTrue(inputFileName.endsWith(".sql"), "Input file must end with '.sql'!");
				ValidationUtils.assertTrue(outputFileName.endsWith(".xml"), "Output file must end with '.xml'!");
			}
			else {
				inputFileName = null;
				outputFileName = null;
			}

			// Initialize context, using a separate root context to enable weaving
			ApplicationContext rootContext = new AnnotationConfigApplicationContext(SpringLoadTimeWeavingConfiguration.class);
			GenericApplicationContext childContext = new AnnotationConfigApplicationContext();
			childContext.setParent(rootContext);
			childContext.registerBean(InMemoryDatabaseDataExportConfig.class);
			childContext.refresh();

			// Execute exports
			Map<String, InMemoryDatabaseDataExportHandler> exportHandlerMap = childContext.getBeansOfType(InMemoryDatabaseDataExportHandler.class);
			exportHandlerMap.forEach((handlerName, handler) -> {
				try {
					handler.generateData(inputFileName, outputFileName);
				}
				catch (Exception e) {
					LogUtils.error(InMemoryDatabaseDataExportRunner.class, "An error occurred during data generation for handler [%s].", e);
				}
			});
		}
		catch (Exception e) {
			//Left the stack trace in because logging does not show and I haven't looked into it.
			//e.printStackTrace();
			LogUtils.error(InMemoryDatabaseDataExportRunner.class, "An error occurred during data generation.", e);
			System.exit(1);
		}
		System.exit(0);
	}
}
