package com.clifton.core.dataaccess.dao.xml;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapperWithBooleanResult;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.context.Context;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import org.hibernate.Criteria;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;


/**
 * The <code>XmlUpdatableDAO</code> class implements a DAO object that uses Spring's context as its data store (not persistent).
 * It retrieves, updates, creates, deletes DTO objects inside of Spring's context.
 * <p>
 * The class uses the following naming convention for its data store: if DAO bean name is "testDAO" then data bean name is "testDAO_data".
 * The data bean is a List.
 *
 * @param <T>
 * @author vgomelsky
 */
public class XmlUpdatableDAO<T extends IdentityObject> extends XmlReadOnlyDAO<T> implements AdvancedUpdatableDAO<T, Criteria> {

	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private T delete(T bean, boolean ignoreDAOEvents) {
		XmlDaoTransactionTestUtils.registerDataBeanNameForTransactionalTest(getContextHandler(), getDataBeanName());

		if (!ignoreDAOEvents && super.isDeleteObserverRegistered()) {
			super.getEventObserver(DaoEventTypes.DELETE).beforeTransactionMethodCall(this, DaoEventTypes.DELETE, bean);
			super.getEventObserver(DaoEventTypes.DELETE).beforeMethodCall(this, DaoEventTypes.DELETE, bean);
		}

		try {
			AssertUtils.assertNotNull(bean, "Bean to delete cannot be null.");
			List<T> allBeans = findAll(false);
			for (T existingBean : CollectionUtils.getIterable(allBeans)) {
				if (existingBean.getIdentity() != null && existingBean.getIdentity().equals(bean.getIdentity())) {
					allBeans.remove(existingBean);
					break;
				}
			}
		}
		catch (Throwable e) {
			if (super.isDeleteObserverRegistered()) {
				super.getEventObserver(DaoEventTypes.DELETE).afterMethodCall(this, DaoEventTypes.DELETE, bean, e);
				super.getEventObserver(DaoEventTypes.DELETE).afterTransactionMethodCall(this, DaoEventTypes.DELETE, bean, e);
			}
			throw new RuntimeException("Error in delete(" + bean + ", " + ignoreDAOEvents + ")", e);
		}

		if (!ignoreDAOEvents && super.isDeleteObserverRegistered()) {
			super.getEventObserver(DaoEventTypes.DELETE).afterMethodCall(this, DaoEventTypes.DELETE, bean, null);
			super.getEventObserver(DaoEventTypes.DELETE).afterTransactionMethodCall(this, DaoEventTypes.DELETE, bean, null);
		}
		return bean;
	}


	@Override
	public void delete(T bean) {
		delete(bean, false);
	}


	@Override
	public void delete(Serializable primaryKey) {
		delete(primaryKey, false);
	}


	private void delete(Serializable primaryKey, boolean ignoreDAOEvents) {
		T bean = findByPrimaryKey(primaryKey, false);
		AssertUtils.assertNotNull(bean, "Cannot find bean to delete with primary key: " + primaryKey);
		delete(bean, ignoreDAOEvents);
	}


	@Override
	public void deleteList(List<T> deleteList) {
		if (CollectionUtils.isEmpty(deleteList)) {
			return;
		}
		for (T deleteBean : deleteList) {
			delete(deleteBean);
		}
	}


	@Override
	public void saveList(List<T> beanList) {
		for (T bean : CollectionUtils.getIterable(beanList)) {
			save(bean);
		}
	}


	@Override
	public void saveList(List<T> newList, List<T> oldList) {
		saveList(newList);
		for (T old : CollectionUtils.getIterable(oldList)) {
			if (newList == null || !newList.contains(old)) {
				delete(old);
			}
		}
	}


	@Override
	@SuppressWarnings("rawtypes")
	public T save(T bean) {

		boolean insert = bean.isNewBean();

		// Actual Reference to Bean Stored in the "Database"
		T dtoInstance = (insert ? BeanUtils.cloneBean(bean, true, true) : findByPrimaryKey(bean.getIdentity(), false));
		XmlDaoTransactionTestUtils.registerOriginalBeanForTransactionalTest(getContextHandler(), getDataBeanName(), insert ? null : dtoInstance);

		if (insert && super.isInsertObserverRegistered()) {
			super.getEventObserver(DaoEventTypes.INSERT).beforeTransactionMethodCall(this, DaoEventTypes.INSERT, bean);
			super.getEventObserver(DaoEventTypes.INSERT).beforeMethodCall(this, DaoEventTypes.INSERT, bean);
		}
		else if (!insert && super.isUpdateObserverRegistered()) {
			super.getEventObserver(DaoEventTypes.UPDATE).beforeTransactionMethodCall(this, DaoEventTypes.UPDATE, bean);
			super.getEventObserver(DaoEventTypes.UPDATE).beforeMethodCall(this, DaoEventTypes.UPDATE, bean);
		}

		try {
			List<T> list = findAll(false);
			// create a new List for storing DTO objects if one doesn't already exist
			// empty list means it's already registered and is just empty, null means it's never been registered/no data ever
			if (list == null) {
				list = new ArrayList<>();
				if (XmlDaoTransactionTestUtils.isTransactionTestEnabled(getContextHandler())) {
					getContextHandler().setBean(getDataBeanName(), list);
				}
				else {
					ConfigurableListableBeanFactory beanFactory = (ConfigurableListableBeanFactory) getApplicationContext().getAutowireCapableBeanFactory();
					beanFactory.registerSingleton(getDataBeanName(), list);
				}
			}

			if (insert) {
				if (bean.getIdentity() == null) {
					/*
					 * Some entities have defined/calculated IDs and thus cannot be auto-generated.
					 * If the ID is not defined, generate it.
					 */
					Class idType = BeanUtils.getPropertyType(bean, "id");
					AtomicLong idGenerator = getIdGenerator();
					if (idGenerator == null) {
						idGenerator = registerIdGenerator(getHighestEntityId(list));
					}
					Long nextId = idGenerator.incrementAndGet();
					if ((bean instanceof BaseEntity) && Long.class.isAssignableFrom(idType)) {
						@SuppressWarnings("unchecked")
						BaseEntity<Long> baseBean = (BaseEntity<Long>) bean;
						baseBean.setId(nextId);
					}
					else if ((bean instanceof BaseSimpleEntity)) {
						if (Integer.class.isAssignableFrom(idType)) {
							@SuppressWarnings("unchecked")
							BaseSimpleEntity<Integer> baseBean = (BaseSimpleEntity<Integer>) bean;
							baseBean.setId(nextId.intValue());
						}
						else if (Short.class.isAssignableFrom(idType)) {
							@SuppressWarnings("unchecked")
							BaseSimpleEntity<Short> baseBean = (BaseSimpleEntity<Short>) bean;
							baseBean.setId(nextId.shortValue());
						}
						else if (Long.class.isAssignableFrom(idType)) {
							@SuppressWarnings("unchecked")
							BaseSimpleEntity<Long> baseBean = (BaseSimpleEntity<Long>) bean;
							baseBean.setId(nextId);
						}
					}
				}
				copyBeanProperties(bean, dtoInstance);
				list.add(dtoInstance);
			}
			else {
				copyBeanProperties(bean, dtoInstance);
			}
		}
		catch (Throwable e) {
			if (insert && super.isInsertObserverRegistered()) {
				super.getEventObserver(DaoEventTypes.INSERT).afterMethodCall(this, DaoEventTypes.INSERT, bean, e);
				super.getEventObserver(DaoEventTypes.INSERT).afterTransactionMethodCall(this, DaoEventTypes.INSERT, bean, e);
			}
			else if (!insert && super.isUpdateObserverRegistered()) {
				super.getEventObserver(DaoEventTypes.UPDATE).afterMethodCall(this, DaoEventTypes.UPDATE, bean, e);
				super.getEventObserver(DaoEventTypes.UPDATE).afterTransactionMethodCall(this, DaoEventTypes.UPDATE, bean, e);
			}
			throw new RuntimeException("Error in save(" + bean + ")", e);
		}

		if (insert && super.isInsertObserverRegistered()) {
			super.getEventObserver(DaoEventTypes.INSERT).afterMethodCall(this, DaoEventTypes.INSERT, bean, null);
			super.getEventObserver(DaoEventTypes.INSERT).afterTransactionMethodCall(this, DaoEventTypes.INSERT, bean, null);
		}
		else if (!insert && super.isUpdateObserverRegistered()) {
			super.getEventObserver(DaoEventTypes.UPDATE).afterMethodCall(this, DaoEventTypes.UPDATE, bean, null);
			super.getEventObserver(DaoEventTypes.UPDATE).afterTransactionMethodCall(this, DaoEventTypes.UPDATE, bean, null);
		}

		return bean;
	}


	@Override
	public ObjectWrapperWithBooleanResult<T> saveSmart(T bean) {
		return ObjectWrapperWithBooleanResult.success(save(bean));
	}


	private void copyBeanProperties(T bean, T dtoInstance) {
		boolean insert = (dtoInstance.isNewBean());
		populateRecordStamp(bean, insert);

		Field[] fields = ClassUtils.getClassFields(bean.getClass(), true, false);
		if (fields == null || fields.length == 0) {
			return;
		}
		try {
			for (Field field : fields) {
				if (!field.isAccessible()) {
					field.setAccessible(true);
				}
				Object beanValue = field.get(bean);
				Object dtoInstanceValue = field.get(dtoInstance);
				if (beanValue instanceof IdentityObject) {
					Serializable beanValueIdentity = ((IdentityObject) beanValue).getIdentity();
					Serializable dtoInstanceValueIdentity = (dtoInstanceValue == null ? null : ((IdentityObject) dtoInstanceValue).getIdentity());
					// If an update and no changes - no need to update the actual reference
					if (!insert && CompareUtils.isEqual(beanValueIdentity, dtoInstanceValueIdentity)) {
						continue;
					}
					// Otherwise replace the value with actual reference from the database
					IdentityObject identityObject = (IdentityObject) beanValue;
					XmlReadOnlyDAO<?> valueDAO = (XmlReadOnlyDAO<?>) getDaoLocator().locate(identityObject.getClass());
					field.set(dtoInstance, valueDAO.findByPrimaryKey(identityObject.getIdentity(), false));
				}
				else {
					field.set(dtoInstance, beanValue);
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Exception encountered while trying to copy properties & update references.", e);
		}
	}


	/**
	 * Populates record stamp fields (create/update user/date) for the specified DTO.
	 *
	 * @param bean
	 */
	private void populateRecordStamp(T bean, boolean newBean) {
		if (bean instanceof UpdatableOnlyEntity) {
			UpdatableOnlyEntity updatableOnlyEntity = (UpdatableOnlyEntity) bean;
			Date now = new Date();

			IdentityObject currentUser = (getContextHandler() == null) ? null : (IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
			short currentUserID = (currentUser == null) ? 0 : ((Short) currentUser.getIdentity());

			if (newBean && bean instanceof UpdatableEntity) {
				UpdatableEntity updatableEntity = (UpdatableEntity) bean;
				updatableEntity.setCreateUserId(currentUserID);
				updatableEntity.setCreateDate(now);
			}
			updatableOnlyEntity.setUpdateUserId(currentUserID);
			updatableOnlyEntity.setUpdateDate(now);
			// Set RV value here to mirror what the database would have done
			if (updatableOnlyEntity.getRv() == null) {
				updatableOnlyEntity.setRv(new byte[0]);
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void executeNamedQuery(String queryName, Object... parameterValues) {
		throw new UnsupportedOperationException("Named Query not supported.");
	}


	private String getIdGeneratorName() {
		return this.getDataBeanName() + "_idGenerator";
	}


	/**
	 * Get the currently registered ID generator for this DAO.
	 *
	 * @return The registered AtomicLong or null if not yet registered.
	 */
	private AtomicLong getIdGenerator() {
		String idGeneratorName = this.getIdGeneratorName();
		if (this.getApplicationContext().containsBean(idGeneratorName)) {
			return (AtomicLong) this.getApplicationContext().getBean(idGeneratorName);
		}
		return null;
	}


	private long getHighestEntityId(List<T> knownEntities) {
		long id = 0;
		for (T entity : knownEntities) {
			Serializable entityId = entity.getIdentity();
			if (entityId instanceof Number) {
				long entityLongId = ((Number) entityId).longValue();
				if (entityLongId > id) {
					id = entityLongId;
				}
			}
		}
		return id;
	}


	/**
	 * Adding an ID generator for new entities. Previously, we were incrementing the max ID of known entities.
	 * This causes issues with related entities that are not cleaned up when the dependent entity is deleted.
	 * <p>
	 * Example: Trade and TradeFill - When a Trade is deleted the fills are not; the next Trade with fills would inherit the old.
	 *
	 * @param initialId ID to start incrementing from
	 * @return The AtomicLong generator
	 */
	private AtomicLong registerIdGenerator(long initialId) {
		ConfigurableListableBeanFactory beanFactory = (ConfigurableListableBeanFactory) getApplicationContext().getAutowireCapableBeanFactory();
		AtomicLong idGenerator = new AtomicLong(initialId);
		beanFactory.registerSingleton(getIdGeneratorName(), idGenerator);
		return idGenerator;
	}


	/**
	 * @return the daoLocator
	 */
	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	/**
	 * @param daoLocator the daoLocator to set
	 */
	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
