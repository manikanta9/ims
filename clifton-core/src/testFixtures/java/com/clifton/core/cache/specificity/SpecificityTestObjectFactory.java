package com.clifton.core.cache.specificity;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.SimpleCacheHandler;


public class SpecificityTestObjectFactory {

	public static <T extends SpecificityTargetHolder> CacheHandler<String, SpecificityCacheStructure<T>> getCacheHandlerForSpecificityCache(@SuppressWarnings("unused") Class<T> clazz) {
		return new SimpleCacheHandler<>();
	}
}
