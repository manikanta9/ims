package com.clifton.core.security.authorization;

public class SecurityResourceBuilder {

	private SecurityResource securityResource;


	private SecurityResourceBuilder(SecurityResource securityResource) {
		this.securityResource = securityResource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static SecurityResourceBuilder createAccounting() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(1);
		securityResource.setName("Accounting");
		securityResource.setLabel("Accounting");
		securityResource.setDescription("Controls access to all accounting module entities");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createInvestment() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(31);
		securityResource.setName("Investment");
		securityResource.setLabel("Investment");
		securityResource.setDescription("Controls access to all investment module entities");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createInvestmentSecurity() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(42);
		securityResource.setParent(createInvestment().toSecurityResource());
		securityResource.setName("InvestmentSecurity");
		securityResource.setLabel("Securities");
		securityResource.setDescription("Controls access to investment securities");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createInvestmentInstrumentHierarchy() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(43);
		securityResource.setParent(createInvestment().toSecurityResource());
		securityResource.setName("InvestmentInstrumentHierarchy");
		securityResource.setLabel("Investment Instrument Hierarchy");
		securityResource.setDescription("Investment Instrument Hierarchy");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createTrading() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(50);
		securityResource.setName("Trading");
		securityResource.setLabel("Trading");
		securityResource.setDescription("Controls access to all trading module entities");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createTrade() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(51);
		securityResource.setParent(SecurityResourceBuilder.createTrading().toSecurityResource());
		securityResource.setName("Trade");
		securityResource.setLabel("Trades");
		securityResource.setDescription("Controls access to trades");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createPortfolio() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(67);
		securityResource.setName("Portfolio");
		securityResource.setLabel("Portfolio");
		securityResource.setDescription("Controls access to all portfolio security resources");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createPortfolioRuns() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(68);
		securityResource.setParent(SecurityResourceBuilder.createPortfolio().toSecurityResource());
		securityResource.setName("Portfolio Runs");
		securityResource.setLabel("Runs");
		securityResource.setDescription("Controls access to portfolio run security resources");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createAccountingPositionTransfer() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(70);
		securityResource.setParent(SecurityResourceBuilder.createAccounting().toSecurityResource());
		securityResource.setName("Position Transfer");
		securityResource.setLabel("Position Transfer");
		securityResource.setDescription("Controls access to accounting Position Transfers");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createOrder() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(42);
		securityResource.setParent(SecurityResourceBuilder.createAccounting().toSecurityResource());
		securityResource.setName("Order");
		securityResource.setLabel("Order");
		securityResource.setDescription("Controls access to  Order resources");
		securityResource.setAllowedPermissionMask(31);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createCollateral() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(73);
		securityResource.setName("Collateral");
		securityResource.setLabel("Collateral");
		securityResource.setDescription("Collateral management including haircuts");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createMarkToMarket() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(82);
		securityResource.setName("MarkToMarket");
		securityResource.setLabel("Mark To Market");
		securityResource.setDescription("Controls access to mark to market section");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createLending() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(103);
		securityResource.setName("Lending");
		securityResource.setLabel("Lending");
		securityResource.setDescription("Controls access to \"Lending\" project resources: REPO's, etc.");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createLendingRepo() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(104);
		securityResource.setParent(SecurityResourceBuilder.createLending().toSecurityResource());
		securityResource.setName("LendingREPO");
		securityResource.setLabel("Lending REPO");
		securityResource.setDescription("Controls access to REPO (Repurchase Agreements) section");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createInvestmentInstructions() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(125);
		securityResource.setParent(createInvestment().toSecurityResource());
		securityResource.setName("InvestmentInstructions");
		securityResource.setLabel("Instructions");
		securityResource.setDescription("Controls access to investment instructions");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createBusiness() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(15);
		securityResource.setName("Business");
		securityResource.setLabel("Business");
		securityResource.setDescription("Controls access to all business module entities");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createBusinessContract() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(23);
		securityResource.setParent(SecurityResourceBuilder.createBusiness().toSecurityResource());
		securityResource.setName("BusinessContract");
		securityResource.setLabel("Contracts");
		securityResource.setDescription("Controls access to contracts");
		securityResource.setAllowedPermissionMask(47);

		return new SecurityResourceBuilder(securityResource);
	}


	public static SecurityResourceBuilder createClientDocuments() {
		SecurityResource securityResource = new SecurityResource();

		securityResource.setId(121);
		securityResource.setParent(SecurityResourceBuilder.createBusinessContract().toSecurityResource());
		securityResource.setName("ClientDocuments");
		securityResource.setLabel("Client Documents");
		securityResource.setDescription("Controls access to client related contracts/documents");
		securityResource.setAllowedPermissionMask(31);

		return new SecurityResourceBuilder(securityResource);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityResourceBuilder withId(int id) {
		getSecurityResource().setId(id);
		return this;
	}


	public SecurityResourceBuilder withName(String name) {
		getSecurityResource().setName(name);
		return this;
	}


	public SecurityResourceBuilder withDescription(String description) {
		getSecurityResource().setDescription(description);
		return this;
	}


	public SecurityResource toSecurityResource() {
		return this.securityResource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SecurityResource getSecurityResource() {
		return this.securityResource;
	}
}
