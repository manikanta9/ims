package com.clifton.core.folder;

import com.clifton.core.util.AssertUtils;

import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.StandardWatchEventKinds;
import java.nio.file.WatchEvent;
import java.nio.file.WatchKey;
import java.nio.file.WatchService;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;


/**
 * <code>TestFolderWatcher</code> return a list of files triggered by the provided events on the target folder.
 */
public class TestFolderWatcher {

	public static List<Path> watchForFolderEvents(Path folder, Runnable folderOperation, int expectedEventCount, Duration maxDuration, Predicate<Path> fileAcceptance, WatchEvent.Kind<?>... events) {
		AssertUtils.assertTrue(maxDuration.toMillis() > 0, "The maximum duration must be greater than zero.");
		long endingMillis = System.currentTimeMillis() + maxDuration.toMillis();
		List<Path> eventFiles = new ArrayList<>();
		try (WatchService watcher = FileSystems.getDefault().newWatchService()) {
			WatchKey folderKey = folder.register(watcher, events);
			try {
				folderOperation.run();
				while (eventFiles.size() < expectedEventCount) {
					if (System.currentTimeMillis() > endingMillis) {
						throw new RuntimeException("The maxDuration time has been exceeded " + maxDuration.toMillis() + " milliseconds.");
					}
					WatchKey key = watcher.poll(1, TimeUnit.SECONDS);
					if (key != null) {
						for (WatchEvent<?> event : key.pollEvents()) {
							WatchEvent.Kind<?> kind = event.kind();
							if (kind != StandardWatchEventKinds.OVERFLOW && event.context() instanceof Path) {
								Path filename = (Path) event.context();
								if (fileAcceptance.test(filename)) {
									eventFiles.add(filename);
								}
							}
						}
						boolean valid = key.reset();
						if (!valid) {
							break;
						}
					}
				}
			}
			finally {
				folderKey.cancel();
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		return eventFiles;
	}
}
