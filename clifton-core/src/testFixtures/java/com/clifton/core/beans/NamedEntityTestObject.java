package com.clifton.core.beans;


import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;


@CacheByName
public class NamedEntityTestObject extends NamedEntity<Integer> {

	public NamedEntityTestObject() {
		super();
	}


	public NamedEntityTestObject(String name, String description) {
		super(name, description);
	}
}
