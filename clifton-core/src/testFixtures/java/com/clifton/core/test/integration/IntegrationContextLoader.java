package com.clifton.core.test.integration;


import com.clifton.core.util.ArrayUtils;
import org.springframework.test.annotation.ProfileValueSource;
import org.springframework.test.annotation.ProfileValueUtils;
import org.springframework.test.context.support.GenericXmlContextLoader;


/**
 * The <code>IntegrationContextLoader</code> ...
 *
 * @author manderson
 */
public class IntegrationContextLoader extends GenericXmlContextLoader {

	@Override
	protected String[] generateDefaultLocations(Class<?> clazz) {
		return modifyLocations(clazz, super.generateDefaultLocations(clazz));
	}


	@Override
	protected String[] modifyLocations(Class<?> clazz, String... locations) {
		ProfileValueSource source = ProfileValueUtils.retrieveProfileValueSource(clazz);
		if ("true".equals(source.get("db-integration-test"))) {
			String integrationContext = "classpath:/com/clifton/core/test/integration/IntegrationTest-context.xml";
			return super.modifyLocations(clazz, ArrayUtils.add(locations, integrationContext));
		}
		return super.modifyLocations(clazz, locations);
	}
}
