package com.clifton.core.test.util.email;

import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;


/**
 * The <code>TestEmailHandlerImpl</code> is an empty implementation of the email handler.  it does not actually do anything and can be used by tests that want to mimic the actual send part of the email processing
 *
 * @author manderson
 */
public class TestEmailHandlerImpl implements EmailHandler {

	@Override
	public void send(Email email) {
		// DO NOTHING
	}


	@Override
	public String getDefaultFromAddress() {
		return "test@paraport.com";
	}


	@Override
	public void closeService() {
		// DO NOTHING
	}


	@Override
	public String getSmtpHost() {
		return "doesnotexist.paraport.com";
	}
}
