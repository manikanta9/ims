package com.clifton.core.test.matcher;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import org.mockito.ArgumentMatcher;

import java.util.ArrayList;
import java.util.List;


/**
 * @author mwacker
 */
public class PropertyMatcher<T> implements ArgumentMatcher<T> {

	private List<PropertyValue> propertyValueList = new ArrayList<>();


	public static <T> PropertyMatcher<T> hasProperty(String propertyName, Object value) {
		PropertyMatcher<T> result = new PropertyMatcher<>();
		return result.andProperty(propertyName, value);
	}


	public PropertyMatcher<T> andProperty(String propertyName, Object value) {
		this.propertyValueList.add(new PropertyValue(propertyName, value));
		return this;
	}


	@Override
	public boolean matches(Object item) {
		boolean result = true;
		for (PropertyValue propertyValue : CollectionUtils.getIterable(this.propertyValueList)) {
			Object objValue = BeanUtils.getPropertyValue(item, propertyValue.getPropertyName(), true);
			if (objValue == null || propertyValue.getValue() == null) {
				result = objValue == propertyValue.getValue();
			}
			else {
				result = objValue.equals(propertyValue.getValue());
			}
			if (!result) {
				break;
			}
		}
		return result;
	}


	private class PropertyValue {

		private final String propertyName;
		private final Object value;


		private PropertyValue(String propertyName, Object value) {
			this.propertyName = propertyName;
			this.value = value;
		}


		private String getPropertyName() {
			return this.propertyName;
		}


		private Object getValue() {
			return this.value;
		}
	}
}
