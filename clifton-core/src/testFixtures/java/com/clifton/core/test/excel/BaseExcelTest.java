package com.clifton.core.test.excel;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.jupiter.api.Assertions;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseExcelTest</code> contains the common logic for
 * running tests that use Excel Files for test and validation data.
 *
 * @author Mary Anderson
 */
public abstract class BaseExcelTest {

	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	// Test Specific Methods - To Be Overridden
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	/**
	 * To Be Overridden
	 * Needs to call SystemUploadService upload methods
	 */
	public abstract void uploadData(BaseExcelTestConfig config);


	/**
	 * To Be Overridden.
	 * Call service methods to manipulate the loaded test data from here
	 */
	public abstract void processData(BaseExcelTestConfig config);


	/**
	 * To Be Overridden.
	 * Call service methods to return data that needs to be verified
	 */
	public abstract List<?> getExistingRecords(String tableName);


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	// Processing Methods
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public void processFile(BaseExcelTestConfig config) {
		Workbook workbook = ExcelFileUtils.createWorkbook(config.getFile());
		config.setWorkbook(workbook);

		uploadData(config);

		Map<String, DataTable> expectedResults = ExcelFileUtils.convertWorkbookToDataTableMap(workbook, config.getTabTableNameResultMap());
		config.setExpectedResults(expectedResults);
		validateResultsPopulated(config);

		processData(config);
		validateResults(config);
	}


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	// Validate Methods
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public void validateResults(BaseExcelTestConfig config) {
		for (String tableName : config.getExpectedResults().keySet()) {
			TableConfig tableConfig = config.getTableConfig(tableName);
			DataTable expectedRecords = config.getExpectedResults(tableName);
			String[] uniqueBeanLabelFields = tableConfig.getUniqueLabelBeanFields();
			String[] uniqueDataRowLabelFields = tableConfig.getUniqueLabelExcelColumns();
			List<?> existingRecords = getExistingRecords(tableName);
			validateResultSizes(expectedRecords, existingRecords, tableName);
			for (int i = 0; i < expectedRecords.getTotalRowCount(); i++) {
				DataRow row = expectedRecords.getRow(i);
				String expectedLabel = getDataRowUniqueLabel(row, uniqueDataRowLabelFields);
				Object matching = null;
				for (Object bean : existingRecords) {
					if (expectedLabel.equalsIgnoreCase(getBeanUniqueLabel(bean, uniqueBeanLabelFields))) {
						matching = bean;
						break;
					}
				}

				String msg = "Excel Row [" + (i + 1) + "] " + tableName + "[" + expectedLabel + "]";
				if (matching == null) {
					throw new RuntimeException(msg + " is missing in the database.");
				}

				Map<String, String> validateValuesMap = tableConfig.getValidateBeanToExcelFields();
				for (Map.Entry<String, String> stringStringEntry : validateValuesMap.entrySet()) {
					Object actual = BeanUtils.getPropertyValue(matching, stringStringEntry.getKey(), false);
					Object expected = null;
					if (actual instanceof BigDecimal) {
						actual = MathUtils.round((BigDecimal) actual, 2);
						BigDecimal val = (BigDecimal) row.getValue(stringStringEntry.getValue());
						if (val != null) {
							expected = MathUtils.round(val, 2);
						}
					}
					else if (actual instanceof Integer) {
						String str = (String) row.getValue(stringStringEntry.getValue());
						if (str != null) {
							expected = new Double(str).intValue();
						}
					}
					else {
						expected = row.getValue(stringStringEntry.getValue());
					}
					Assertions.assertEquals(expected, actual, msg + ": Field [" + stringStringEntry.getValue() + "]");
				}
			}
		}
	}


	public void validateResultSizes(DataTable dataTable, List<?> beanList, String type) {
		int dataTableSize = (dataTable == null ? 0 : dataTable.getTotalRowCount());
		AssertUtils.assertTrue(dataTableSize == CollectionUtils.getSize(beanList), "Expected [" + dataTableSize + "] " + type + " records, but there are [" + CollectionUtils.getSize(beanList) + "]");
	}


	public void validateResultsPopulated(BaseExcelTestConfig config) {
		for (String key : config.getTabTableNameResultMap().values()) {
			AssertUtils.assertNotNull(config.getExpectedResults().get(key), "Missing results to compare to for [" + key + "]");
		}
	}


	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////
	// Helper Methods
	/////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////


	public String getBeanUniqueLabel(Object bean, String[] uniqueLabelFields) {
		String label = "";
		for (int i = 0; i < uniqueLabelFields.length; i++) {
			label += BeanUtils.getPropertyValue(bean, uniqueLabelFields[i], true);
			if (i < uniqueLabelFields.length - 1) {
				label += "-";
			}
		}
		return label;
	}


	public String getDataRowUniqueLabel(DataRow row, String[] uniqueLabelColumns) {
		String label = "";

		for (int i = 0; i < uniqueLabelColumns.length; i++) {
			label += row.getValue(uniqueLabelColumns[i]);
			if (i < uniqueLabelColumns.length - 1) {
				label += "-";
			}
		}
		return label;
	}
}
