package com.clifton.core.test.integration;


import org.springframework.test.annotation.ProfileValueSource;


/**
 * The <code>IntegrationProfileValueSource</code> ...
 *
 * @author manderson
 */
public class IntegrationProfileValueSource implements ProfileValueSource {

	public static final IntegrationProfileValueSource INSTANCE = new IntegrationProfileValueSource();


	/**
	 * Obtain the canonical instance of this ProfileValueSource.
	 */
	public static IntegrationProfileValueSource getInstance() {
		return INSTANCE;
	}


	/**
	 * Private constructor, enforcing the singleton pattern.
	 */
	public IntegrationProfileValueSource() {
		// nothing
	}


	@Override
	public String get(String key) {
		if (key.equalsIgnoreCase("integration-test")) {
			return "true";
		}
		return System.getProperty(key);
	}
}
