package com.clifton.core.test.dataaccess;

import com.clifton.core.dataaccess.migrate.schema.action.Action;
import org.springframework.core.io.Resource;

import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public interface InMemoryDatabaseTests {

	public boolean sqlFirst();


	/**
	 * Hook called before data is loaded.
	 */
	public void beforeActions();


	/**
	 * Hook called after data has been loaded and before executing a test.
	 */
	public void beforeTest();


	/**
	 * Hook called after test runs.
	 */
	public void afterTest();


	/**
	 * By default no overrides are necessary.  There are times in which a custom service/method may need to be called when
	 * saving an object, for the most part these are automatically discovered, or, in some cases, they are defined
	 * on the DTO definitions in the schema migration files via migrationServiceName and migrationServiceMethod params,
	 * however, those can be overridden further here if custom functionality is required.
	 */
	public Map<String, Action> getActionMapOverrides();


	/**
	 * By default these tests will look for a single migration file in the 'data' directory relative to the current
	 * test class location named as '<className>Data.xml' this method may be overridden to provide multiple
	 * action data files or those with alternate naming conventions/locations.
	 */
	public List<Resource> getMigrationData();


	/**
	 * By default these tests will look for a single migration file in the 'data' directory relative to the current
	 * test class location named as '<className>DataFunctions.xml' this method may be overridden to provide multiple
	 * action data files or those with alternate naming conventions/locations.
	 * <p>
	 * The functions migration is an optional file to import any SQL user defined functions for calculated columns.
	 */
	public List<Resource> getFunctionsData();


	/**
	 * By default these tests will look for a single migration file in the 'data' directory relative to the current
	 * test class location named as '<className>DataAdditional.xml' this method may be overridden to provide multiple
	 * action data files or those with alternate naming conventions/locations.
	 * <p>
	 * The additional migration file can be used to perform sql actions after data has been loaded.
	 */
	public List<Resource> getAdditionalData();
}
