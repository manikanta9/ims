package com.clifton.core.test.excel;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.web.multipart.MultipartFileImpl;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseExcelTestConfig</code> ...
 *
 * @author Mary Anderson
 */
public class BaseExcelTestConfig {

	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	// Populated during setup

	private MultipartFile file;

	/**
	 * Map of Excel Tab names (Key) to System Table Names (Value)
	 * Used to determine which tabs to upload, and which tables to upload it for
	 */
	private Map<String, String> tabTableNameUploadMap = new HashMap<>();

	/**
	 * Map of Excel Tab names (Key) to System Table Names (Value)
	 * Used to determine which tabs are considered results and should be
	 * processed and which tables the tab contains the results for
	 */
	private Map<String, String> tabTableNameResultMap = new HashMap<>();

	private List<TableConfig> tableConfigList = new ArrayList<>();

	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	// Populated during processing

	private Workbook workbook;

	private Map<String, DataTable> expectedResults;


	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////


	public BaseExcelTestConfig(String filePath) {
		if (filePath.endsWith(".xlsx")) {
			System.out.println("WARNING: Please use .xls files instead of .xlsx files.  .xlsx files are inefficient and will slow down test cases considerably.");
		}
		this.file = new MultipartFileImpl(filePath);
	}


	public void addTableConfig(TableConfig tbConfig) {
		this.tableConfigList.add(tbConfig);
	}


	public TableConfig getTableConfig(String tableName) {
		for (TableConfig conf : this.tableConfigList) {
			if (conf.getTableName().equals(tableName)) {
				return conf;
			}
		}
		throw new RuntimeException("No Table Config set up for table [" + tableName + "]");
	}


	public DataTable getExpectedResults(String tableName) {
		return this.expectedResults.get(tableName);
	}


	public void addTabTableNameUpload(String key, String value) {
		this.tabTableNameUploadMap.put(key, value);
	}


	public void addTabTableNameResult(String key, String value) {
		this.tabTableNameResultMap.put(key, value);
	}


	/////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////
	// Getters & Setters


	public Workbook getWorkbook() {
		return this.workbook;
	}


	public void setWorkbook(Workbook workbook) {
		this.workbook = workbook;
	}


	public Map<String, DataTable> getExpectedResults() {
		return this.expectedResults;
	}


	public void setExpectedResults(Map<String, DataTable> expectedResults) {
		this.expectedResults = expectedResults;
	}


	public MultipartFile getFile() {
		return this.file;
	}


	public Map<String, String> getTabTableNameUploadMap() {
		return this.tabTableNameUploadMap;
	}


	public Map<String, String> getTabTableNameResultMap() {
		return this.tabTableNameResultMap;
	}


	public List<TableConfig> getTableConfigList() {
		return this.tableConfigList;
	}
}
