package com.clifton.core.test.excel;


import java.util.HashMap;
import java.util.Map;


/**
 * The <code>TableConfig</code> ...
 *
 * @author Mary Anderson
 */
public class TableConfig {

	private String tableName;

	/**
	 * Bean Fields that can be parsed together to result in a uniqueLabel
	 */
	private String[] uniqueLabelBeanFields;

	/**
	 * Excel Columns that can be parsed together to result in a uniqueLabel
	 */
	private String[] uniqueLabelExcelColumns;

	/**
	 * Bean Fields mapped to Excel Columns that should be compared to verify results
	 */
	private Map<String, String> validateBeanToExcelFields = new HashMap<>();


	public TableConfig(String tableName, String[] uniqueLabelBeanFields, String[] uniqueLabelExcelColumns) {
		this.tableName = tableName;
		this.uniqueLabelBeanFields = uniqueLabelBeanFields;
		this.uniqueLabelExcelColumns = uniqueLabelExcelColumns;
	}


	public void addValidateField(String beanFieldName, String excelFieldName) {
		this.validateBeanToExcelFields.put(beanFieldName, excelFieldName);
	}


	public String getTableName() {
		return this.tableName;
	}


	public String[] getUniqueLabelBeanFields() {
		return this.uniqueLabelBeanFields;
	}


	public String[] getUniqueLabelExcelColumns() {
		return this.uniqueLabelExcelColumns;
	}


	public Map<String, String> getValidateBeanToExcelFields() {
		return this.validateBeanToExcelFields;
	}
}
