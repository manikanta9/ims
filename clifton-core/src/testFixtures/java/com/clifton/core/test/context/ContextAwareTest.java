package com.clifton.core.test.context;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>ContextAwareTest</code> annotation should be used to mark context aware test cases. These tests can use
 * context managed objects that take advantage of code generation, autowiring, etc. They can overwrite default location
 * of XML context file used by the test.
 *
 * @author vgomelsky
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface ContextAwareTest {

	public static final String NULL = "REQUIRED ATTRIBUTE: YOU MUST SPECIFY THE VALUE OF THIS ATTRIBUTE";


	/**
	 * Path to XML file that defines application context to be used for this test. If not defined, assumes an XML file
	 * with same name and location as the test class.
	 */
	String contextPath() default NULL;


	/**
	 * Optional test bean name inside of application context. If one is not defined, a new bean will be created and
	 * autowired.
	 */
	String testBeanName() default NULL;


	/**
	 * Set to false to avoid dependency injection check in test cases that use this annotation.
	 */
	boolean checkDependency() default true;
}
