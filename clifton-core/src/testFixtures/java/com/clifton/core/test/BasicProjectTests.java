package com.clifton.core.test;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.cache.CacheTypes;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.convention.util.CoreConventionUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyDaoListCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringCompositeKeyFlexibleDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringDaoListCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoListCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringThreeKeyDaoCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringThreeKeyDaoListCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityFilterableCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.MigrationSchemaService;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.migrate.schema.search.TableSearchForm;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.BaseSearchForm;
import com.clifton.core.dataaccess.search.form.BaseSortableSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.observer.Observer;
import com.clifton.core.validation.UserIgnorableValidation;
import com.clifton.core.validation.UserIgnorableValidationAspect;
import com.clifton.core.validation.ValidationService;
import org.aspectj.lang.annotation.Pointcut;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.reflections.Reflections;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.messaging.simp.annotation.SubscribeMapping;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.annotation.Resource;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.beans.BeanInfo;
import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringReader;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Parameter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;

import static com.clifton.core.util.ClassUtils.getClassFields;


/**
 * The <code>BasicProjectTests</code> class executes basic generic tests that can and should apply to most projects.
 *
 * @author vgomelsky
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration
public abstract class BasicProjectTests {

	@Resource
	private ApplicationContext applicationContext;

	@Resource
	private DaoLocator daoLocator;

	@Resource
	private MigrationSchemaService migrationSchemaService;

	@Resource
	private ValidationService validationService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * A set of annotation classes that are restricted from being defined on implementation methods,
	 * but should instead be defined on the interface methods.
	 */
	private static final List<Class<?>> restrictedServiceMethodAnnotationList = Arrays.asList(
			RequestBody.class,
			RequestMapping.class,
			SecureMethod.class,
			DoNotAddRequestMapping.class,
			ModelAttribute.class,
			SubscribeMapping.class,
			MessageMapping.class,
			SendTo.class,
			SendToUser.class
	);

	/**
	 * A set of annotation classes that are restricted from being defined on interface methods,
	 * but should instead be defined on the implementation methods.
	 */
	private static final List<Class<?>> restrictedInterfaceMethodAnnotationList = Collections.singletonList(
			Transactional.class
	);


	/**
	 * A set of annotation classes that conflict with the annotation class DoNotAddRequestMapping.
	 */
	private static final Set<Class<?>> CONFLICTING_ANNOTATIONS;

	private static final List<Class<?>> ANNOTATIONS_REQUIRING_DO_NOT_ADD_REQUEST_MAPPING = Arrays.asList(
			SubscribeMapping.class,
			MessageMapping.class,
			SendTo.class,
			SendToUser.class
	);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	static {
		CONFLICTING_ANNOTATIONS = CollectionUtils.createHashSet(SecureMethod.class);
		Reflections reflections = new Reflections("org.springframework.web.bind.annotation");
		Set<Class<? extends Annotation>> allClasses =
				reflections.getSubTypesOf(Annotation.class);
		for (Class<?> cls : CollectionUtils.getIterable(allClasses)) {
			CONFLICTING_ANNOTATIONS.add(cls);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the name of the project to be tested.  This name will be used as a prefix for project services.
	 */
	public abstract String getProjectPrefix();


	/**
	 * Returns the string directory for this project (e.g. com/clifton/accounting). The result will be
	 * {@link #getProjectPackage()} with all '.'s replaced with {@link File#separatorChar}.
	 */
	public String getProjectPackageAsDirectory() {
		return getProjectPackage().replace('.', File.separatorChar);
	}


	/**
	 * Returns the path for the Java directory of the given source set relative to the project path.
	 */
	protected String getProjectSourceSetJavaDirectory(String sourceSet) {
		return "src/" + sourceSet + "/java/";
	}


	/**
	 * Returns the list of source sets which are used in production. Files under these source sets will be included when validating main source conventions and excluded when
	 * validating test source conventions.
	 */
	protected List<String> getProjectProductionSourceSets() {
		List<String> excludedSourceSetList = CollectionUtils.combineCollections(getProjectTestSourceSets(), getProjectIgnoredSourceSets());
		return CollectionUtils.getFiltered(getAllProjectSourceSets(), sourceSet -> !excludedSourceSetList.contains(sourceSet));
	}


	/**
	 * Returns the list of source sets which are used for testing. Files under these source sets will be excluded when validating main source conventions and included when
	 * validating test source conventions.
	 */
	protected List<String> getProjectTestSourceSets() {
		return Arrays.asList("test", "testFixtures");
	}


	/**
	 * Returns the list of source sets which should be ignored by all tests. These source sets may be used for development-only utilities, such as benchmarks.
	 */
	protected List<String> getProjectIgnoredSourceSets() {
		return Arrays.asList("jmh", "integTest");
	}


	/**
	 * Returns a list of all of the source sets for the current project.
	 */
	protected List<String> getAllProjectSourceSets() {
		File srcDir = new File("src");
		return ArrayUtils.getStream(srcDir.listFiles())
				.filter(File::isDirectory)
				.map(File::getName)
				.collect(Collectors.toList());
	}


	/**
	 * Returns a List of all classes of the specified type located in this project.
	 */
	protected <T> List<Class<T>> getProjectClassFiles(Class<T> typeClass) {
		return CollectionUtils.getConvertedFlattened(getProjectProductionSourceSets(), sourceSet -> getProjectSourceSetClassFiles(sourceSet, typeClass));
	}


	/**
	 * Returns a list of all classes of the specified type within the specified source set for the current project.
	 */
	protected <T> List<Class<T>> getProjectSourceSetClassFiles(String sourceSet, Class<T> typeClass) {
		return ClassUtils.getClassFiles(getProjectSourceSetJavaDirectory(sourceSet) + getProjectPackageAsDirectory(), typeClass);
	}


	/**
	 * Can be overridden to skip automated testing for certain methods.
	 */
	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	protected boolean isMethodSkipped(@SuppressWarnings("unused") Method method) {
		return false;
	}


	/**
	 * Can be overridden to skip automated testing for certain beans.
	 */
	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	protected boolean isBeanSkipped(@SuppressWarnings("unused") String beanName) {
		return false;
	}


	/**
	 * Can be overridden to skip automated testing for public methods on certain beans.
	 */
	@SuppressWarnings("BooleanMethodIsAlwaysInverted")
	protected boolean isServiceSkipped(@SuppressWarnings("unused") String beanName) {
		return false;
	}


	/**
	 * Can be overridden to skip automated testing for certain classes.
	 */
	protected boolean isClassSkipped(@SuppressWarnings("unused") String className) {
		return false;
	}


	/**
	 * Can be overridden to skip automated testing for certain fields.
	 */
	@SuppressWarnings("SameReturnValue")
	protected boolean isFieldIgnored(@SuppressWarnings("unused") String fieldName) {
		return false;
	}


	/**
	 * Can be overridden to skip testBooleanGetterMethodsAreValid testing for certain classes.
	 */
	protected boolean isBooleanGetterMethodsAreValidSkipped(@SuppressWarnings("unused") String className) {
		return false;
	}


	//This tests to ensure there are no null pointers during this method due to Column configuration
	@SuppressWarnings("rawtypes")
	@Test
	public void validateBeans() {
		Map<String, ReadOnlyDAO> beanMap = this.applicationContext.getBeansOfType(ReadOnlyDAO.class);
		for (String beanName : beanMap.keySet()) {
			try {
				Assertions.assertNotNull(this.validationService.getValidationMetaData(beanName.replace("DAO", "")));
			}
			catch (IllegalStateException ise) {
				//ignore - thrown by 'NoTableDAOConfig'
			}
		}
	}


	@Test
	public void testAllBeansCanBeInstantiated() {
		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		for (String name : beanNames) {
			if (!name.toLowerCase().endsWith("testservice") && !isBeanSkipped(name)) {
				// skip bean factories: not real beans that cannot be instantiated
				Class<?> beanClass = this.applicationContext.getType(name);
				String factoryBeanName = ((GenericApplicationContext) this.applicationContext).getBeanDefinition(name).getFactoryBeanName();
				if (factoryBeanName != null) {
					// TODO: should we try to instantiate factory beans?
				}
				else {
					Assertions.assertNotNull(beanClass, "Cannot retrieve bean type for bean with name '" + name + "'.");
					if (!beanClass.equals(void.class)) {
						Assertions.assertNotNull(this.applicationContext.getBean(name), "The bean '" + name + "' cannot be null.");
					}
				}
			}
		}
	}


	@Test
	public void testBooleanGetterMethodsAreValid() {
		StringBuilder invalidGetterMethods = new StringBuilder();
		List<Class<Object>> classes = getProjectClassFiles(Object.class);
		for (Class<Object> clazz : CollectionUtils.getIterable(classes)) {
			if (isBooleanGetterMethodsAreValidSkipped(clazz.getName())) {
				continue;
			}
			for (Method method : MethodUtils.getAllDeclaredMethods(clazz)) {
				if ((method.getReturnType().equals(boolean.class) || method.getReturnType().equals(Boolean.class)) &&
						(method.getName().startsWith("is") || method.getName().startsWith("get"))) {

					String methodName = method.getName();
					boolean invalid = method.getReturnType().equals(boolean.class) ? methodName.startsWith("get") : methodName.startsWith("is");
					if (invalid) {
						// Skip cases where the method is defined by code outside of our control
						if (!method.getDeclaringClass().getName().startsWith("com.clifton")) {
							System.out.println("Skipping method " + method);
							continue;
						}

						String validMethodName = method.getReturnType().equals(boolean.class) ? methodName.replace("get", "is") : methodName.replace("is", "get");
						Method validMethod = MethodUtils.getMethod(clazz, validMethodName);
						if (validMethod != null) {
							invalidGetterMethods.append("Method [").append(validMethodName).append("] in class [")
									.append(clazz.getSimpleName()).append("] uses both a valid and invalid getter name it should not use [").append(methodName).append("]\n");
						}
						else {
							invalidGetterMethods.append("Method [").append(methodName).append("] in class [")
									.append(clazz.getSimpleName()).append("] should use [").append(validMethodName).append("]\n");
						}
					}
					else {
						String invalidMethodName = method.getReturnType().equals(boolean.class) ? methodName.replace("is", "get") : methodName.replace("get", "is");
						Method invalidMethod = MethodUtils.getMethod(clazz, invalidMethodName);
						if (invalidMethod != null) {
							invalidGetterMethods.append("Method [").append(invalidMethodName).append("] in class [")
									.append(clazz.getSimpleName()).append("] uses both a valid and invalid getter name it should use [").append(methodName).append("]\n");
						}
					}
					//Now lets check for a corresponding field and ensure the method return type matches the field return type
					String fieldName = methodName.startsWith("get") ? StringUtils.deCapitalize(methodName.replace("get", "")) : StringUtils.deCapitalize(methodName.replace("is", ""));
					Field field = ClassUtils.getClassField(clazz, fieldName, false, false);
					if (field != null && (field.getType().equals(boolean.class) || field.getType().equals(Boolean.class)) && !field.getType().equals(method.getReturnType())) {
						invalidGetterMethods.append("Field [").append(fieldName).append("] in class [")
								.append(clazz.getSimpleName()).append("] is of type [").append(field.getType().getSimpleName())
								.append("] with a getter that returns type [").append(method.getReturnType().getSimpleName())
								.append("] it should return [").append(field.getType().getSimpleName()).append("]\n");
					}
				}
			}
		}
		Assertions.assertEquals(0, invalidGetterMethods.length(), invalidGetterMethods.toString());
	}


	@Test
	public void testServiceAnnotationsProperlyDefined() {
		List<String> errorList = new ArrayList<>();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			if (!isServiceSkipped(stringObjectEntry.getKey())) {
				Object service = stringObjectEntry.getValue();
				for (Method serviceMethod : service.getClass().getMethods()) {
					// public and non-abstract methods only
					if (Modifier.isPublic(serviceMethod.getModifiers()) && !Modifier.isAbstract(serviceMethod.getModifiers())) {
						// ignore Object methods
						if (MethodUtils.isMethodInClass(Object.class, serviceMethod)) {
							continue;
						}

						Method interfaceMethod = null;
						Class<?> currentClass = service.getClass();
						while (currentClass != null && interfaceMethod == null) {
							for (Class<?> classInterface : currentClass.getInterfaces()) {
								interfaceMethod = MethodUtils.getMatchingMethodInClass(classInterface, serviceMethod);
								if (interfaceMethod != null) {
									break;
								}
							}
							currentClass = currentClass.getSuperclass();
						}

						//Skip if null; could be a getter/setter - 'testAllPublicMethodsAreDeclaredOnInterfaces' will catch this
						if (interfaceMethod != null) {
							List<Class<?>> serviceMethodAnnotationClassList = Arrays.stream(AnnotationUtils.getDeclaredAnnotations(serviceMethod)).map(Annotation::annotationType).collect(Collectors.toList());
							if (serviceMethodAnnotationClassList.stream().anyMatch(getRestrictedServiceMethodAnnotationList()::contains)) {
								errorList.add(String.format("Service method [%s#%s] contains a URL specific annotation that should instead be defined on its interface. URL annotations are [%s]", service.getClass().getSimpleName(), serviceMethod.getName(), CollectionUtils.getConverted(getRestrictedServiceMethodAnnotationList(), Class::getSimpleName)));
							}
							List<Class<?>> interfaceMethodAnnotationClassList = Arrays.stream(AnnotationUtils.getDeclaredAnnotations(interfaceMethod)).map(Annotation::annotationType).collect(Collectors.toList());
							if (interfaceMethodAnnotationClassList.stream().anyMatch(getRestrictedInterfaceMethodAnnotationList()::contains)) {
								errorList.add(String.format("Service method [%s#%s] contains an implementation specific annotation that should instead be defined on its service. Implementation annotations are [%s]", service.getClass().getSimpleName(), serviceMethod.getName(), CollectionUtils.getConverted(getRestrictedInterfaceMethodAnnotationList(), Class::getSimpleName)));
							}
						}
					}
				}
			}
		}
		errorList.sort(String::compareTo);
		AssertUtils.assertTrue(errorList.isEmpty(), StringUtils.join(errorList, "\n"));
	}


	protected Set<String> getIgnoreVoidSaveMethodSet() {
		return null;
	}


	/**
	 * Validates that all service methods have valid return types.
	 */
	@Test
	public void testServiceMethodValidReturnTypes() throws Exception {
		Map<String, Object> controllerMap = this.applicationContext.getBeansWithAnnotation(Controller.class);
		controllerMap.putAll(this.applicationContext.getBeansWithAnnotation(Service.class));

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectPrefix())) {
				Object controller = stringObjectEntry.getValue();

				Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());

				// public and non-abstract methods only
				BeanInfo beanInfo = Introspector.getBeanInfo(clz);

				for (Method method : clz.getMethods()) {
					// ignore Object methods
					if (MethodUtils.isMethodInClass(Object.class, method)) {
						continue;
					}
					if (processMethod(beanInfo, method)) {
						Method interfaceMethod = MethodUtils.getInterfaceMethod(method);
						if (interfaceMethod != null) {
							//If this service is not called by the UI; then a returnValue does not need to be enforced (but still should be returned when possible/appropriate)
							if (!AnnotationUtils.isAnnotationPresent(method, DoNotAddRequestMapping.class)) {
								// By default, strings returned from handlers are interpreted as view names; an appropriate annotation must be added to include the string in the response body
								if (method.getReturnType().equals(String.class)
										&& !AnnotationUtils.isAnnotationPresent(method, ModelAttribute.class)
										&& !AnnotationUtils.isAnnotationPresent(method, ResponseBody.class)) {
									results.append(interfaceMethod).append(": Controller methods which return strings must be annotated with @ModelAttribute or @ResponseBody").append(StringUtils.NEW_LINE);
								}
								// Require return values for all "save" methods; each save method should return the saved entity
								if (method.getName().startsWith("save") && !method.getName().endsWith("List") && Void.TYPE.equals(method.getReturnType())) {
									if (!CollectionUtils.contains(getIgnoreVoidSaveMethodSet(), (method.getName()))) {
										results.append(interfaceMethod).append(": Controller save methods must return a value").append(StringUtils.NEW_LINE);
									}
								}
							}
						}
					}
				}
			}
		}
		if (results.length() != 0) {
			Assertions.fail("Controller methods found with invalid return types:" + StringUtils.NEW_LINE + results);
		}
	}


	private List<Class<?>> getRestrictedServiceMethodAnnotationList() {
		List<Class<?>> restrictedList = new ArrayList<>();
		restrictedList.addAll(restrictedServiceMethodAnnotationList);
		restrictedList.addAll(getAdditionalRestrictedServiceMethodAnnotationList());
		return restrictedList;
	}


	public List<Class<?>> getAdditionalRestrictedServiceMethodAnnotationList() {
		return new ArrayList<>();
	}


	private List<Class<?>> getRestrictedInterfaceMethodAnnotationList() {
		List<Class<?>> restrictedList = new ArrayList<>();
		restrictedList.addAll(restrictedInterfaceMethodAnnotationList);
		restrictedList.addAll(getAdditionalRestrictedInterfaceMethodAnnotationList());
		return restrictedList;
	}


	public List<Class<?>> getAdditionalRestrictedInterfaceMethodAnnotationList() {
		return new ArrayList<>();
	}


	@Test
	public void testNonPublicMethodAnnotationsNotPresent() {
		List<Class<Object>> classes = getProjectClassFiles(Object.class);
		StringBuilder builder = new StringBuilder();
		for (Class<Object> clazz : CollectionUtils.getIterable(classes)) {
			for (Method method : clazz.getDeclaredMethods()) {
				if (!Modifier.isPublic(method.getModifiers()) && !isMethodSkipped(method)) {
					for (Annotation annotation : AnnotationUtils.getAnnotations(method)) {
						if (!(annotation instanceof Transactional)
								&& !(annotation instanceof Pointcut)
								&& !(annotation instanceof SafeVarargs)) {
							builder.append("Invalid Method Annotation Defined On NON Public Method for Class[").append(clazz.getSimpleName()).append("], Method [").append(method.getName()).append("], Method Annotation [").append(annotation.annotationType().getSimpleName()).append("]\n");
						}
					}
				}
			}
		}
		String result = builder.toString();
		AssertUtils.assertTrue(StringUtils.isEmpty(result), result);
	}


	@Test
	public void testAllPublicMethodsAreDeclaredOnInterfaces() throws Exception {
		List<String> errorMessageList = new ArrayList<>();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			if (!isServiceSkipped(stringObjectEntry.getKey())) {
				Object service = stringObjectEntry.getValue();
				BeanInfo beanInfo = Introspector.getBeanInfo(service.getClass());
				for (Method method : service.getClass().getMethods()) {
					// public and non-abstract methods only
					if (Modifier.isPublic(method.getModifiers()) && !Modifier.isAbstract(method.getModifiers())) {
						// ignore Object methods
						if (MethodUtils.isMethodInClass(Object.class, method)) {
							continue;
						}

						// Ignore lifecycle methods
						if (AnnotationUtils.isAnnotationPresent(method, PreDestroy.class) || AnnotationUtils.isAnnotationPresent(method, PostConstruct.class)) {
							continue;
						}

						// check interfaces on all super classes as well
						boolean found = false;
						Class<?> currentClass = service.getClass();
						while (currentClass != null && !found) {
							for (Class<?> classInterface : currentClass.getInterfaces()) {
								if (MethodUtils.isMethodInClass(classInterface, method)) {
									found = true;
								}
							}
							currentClass = currentClass.getSuperclass();
						}

						// Ignore property descriptors with getters and setters or backing fields
						for (PropertyDescriptor descriptor : beanInfo.getPropertyDescriptors()) {
							if (found) {
								break;
							}
							if (descriptor.getReadMethod() != null) {
								if (descriptor.getWriteMethod() != null) {
									// Read and write methods exist; current method must match one of these
									found = method.equals(descriptor.getReadMethod()) || method.equals(descriptor.getWriteMethod());
								}
								else if (method.equals(descriptor.getReadMethod())) {
									// Read method exists, but not write method; this method must match a final field declared on this class (not an inherited field)
									Field field = ClassUtils.getClassField(method.getDeclaringClass(), descriptor.getName(), false, true);
									found = Modifier.isFinal(field.getModifiers());
								}
							}
						}

						if (!found) {
							errorMessageList.add("Bean '" + stringObjectEntry.getKey() + "' has public method '" + method.getName() + "' that is not declared on any of its interfaces");
						}
					}
				}
			}
		}
		if (!CollectionUtils.isEmpty(errorMessageList)) {
			Assertions.fail(errorMessageList.stream().collect(Collectors.joining("\n- ", "- ", "")));
		}
	}


	@Test
	public void testNoDuplicateRequestMappingsAreDeclaredOnServiceInterfaces() {
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		StringBuilder sb = new StringBuilder();
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			if (!isServiceSkipped(stringObjectEntry.getKey())) {
				Object service = stringObjectEntry.getValue();

				if (AnnotationUtils.isAnnotationPresent(service.getClass(), Service.class)) {
					Map<String, List<Method>> methodMap = new HashMap<>();
					Set<String> requestMappingUrl = new HashSet<>();

					for (Method method : service.getClass().getMethods()) {
						// public, non-bridged, and non-abstract methods only
						if (Modifier.isPublic(method.getModifiers()) && !method.isBridge() && !Modifier.isAbstract(method.getModifiers())) {
							// ignore Object methods and method annotated with DoNotAddRequestMapping
							if (!MethodUtils.isMethodInClass(Object.class, method) && !AnnotationUtils.isAnnotationPresent(method, DoNotAddRequestMapping.class)) {
								if (AnnotationUtils.isAnnotationPresent(method, RequestMapping.class) || ContextConventionUtils.isUrlFromMethodAllowed(method, service.getClass())) {
									// check methods annotated with RequestMapping for duplicates
									RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
									String[] values = (rm != null) ? rm.value() : new String[]{ContextConventionUtils.getUrlFromMethod(method)};
									if (!requestMappingUrl.add(values[0])) {
										sb.append("Bean [").append(stringObjectEntry.getKey()).append("] has duplicate RequestMapping URL [").append(values[0]).append("] with method name [").append(method.getName()).append("].\n");
									}
								}
								else {
									methodMap.computeIfAbsent(method.getName(), methodName -> new ArrayList<>()).add(method);
								}
							}
						}
					}
					// Test for methods with duplicate names. Methods with name get*List may have duplicates if they generate different urls (SearchForm).
					for (Map.Entry<String, List<Method>> methodEntry : methodMap.entrySet()) {
						String methodName = methodEntry.getKey();
						List<Method> methodList = methodEntry.getValue();
						if (methodList.size() > 1) {
							if (methodName.contains("List")) {
								// Test that the duplicate methods result in different URLs.
								for (Method method : methodList) {
									String url = ContextConventionUtils.getUrlFromMethod(method);
									if (!requestMappingUrl.add(url)) {
										sb.append("Bean [").append(stringObjectEntry.getKey()).append("] has duplicate RequestMapping URL [").append(url).append("] with method name [").append(methodName).append("].\n");
									}
								}
							}
							else {
								// Test if the methods contain interface arguments. If they do, they can be ignored.
								boolean methodArgumentInterface = true;
								for (Method method : methodList) {
									if (!MethodUtils.isMethodWithInterfaceParameters(method)) {
										if (!methodArgumentInterface) {
											sb.append("Bean '").append(stringObjectEntry.getKey()).append("' has duplicate public methods '").append(methodName).append("'.\n");
											break;
										}
										else {
											methodArgumentInterface = false;
										}
									}
								}
							}
						}
					}
				}
			}
		}

		Assertions.assertTrue(StringUtils.isEmpty(sb.toString()), sb.toString());
	}


	@Test
	public void testNamedEntitySearchFormsImplementSearchPattern() {
		List<Class<? extends BaseSearchForm>> searchFormClasses = getProjectClassFiles(BaseSearchForm.class).stream()
				.filter(clazz -> AnnotationUtils.getAnnotation(clazz, SearchForm.class).hasOrmDtoClass())
				.filter(clazz -> !(Modifier.isAbstract(clazz.getModifiers())))
				.collect(Collectors.toList());

		Map<String, Class<?>> projectClassesByName = getProjectClassFiles(Object.class).stream()
				.collect(Collectors.toMap(Class::getName, Function.identity()));

		List<String> errorList = new ArrayList<>();
		for (Class<? extends BaseSearchForm> searchFormClass : searchFormClasses) {
			Class<?> dtoClass = TestUtils.getDtoClassFromSearchForm(searchFormClass, projectClassesByName);
			if (NamedObject.class.isAssignableFrom(dtoClass)) {
				if (ClassUtils.getClassField(searchFormClass, "searchPattern", true, false) == null) {
					errorList.add("Search form [" + searchFormClass.getSimpleName() + "] supports a NamedObject DTO [" + dtoClass.getSimpleName() + "] but does not implement 'searchPattern' on the searchForm.\n");
				}
			}
		}
		AssertUtils.assertEmpty(errorList, "Search forms supporting NamedObject DTOs must define 'searchPattern' as a search field at minimum supporting the 'name' field, e.g., @SearchField(searchField = \"name\")\n\n" + errorList);
	}


	@Test
	public void testFieldsAreNotShadowed() {
		List<Class<Object>> classList = getProjectClassFiles(Object.class);
		for (Class<Object> clazz : classList) {
			if (!isClassSkipped(clazz.getName())) {
				Field[] fields = getClassFields(clazz, true, true);
				findDuplicateField(fields, clazz.getName());
			}
		}
	}


	private void findDuplicateField(Field[] fields, String serviceName) {
		List<String> shadowedFields = new ArrayList<>();
		for (int i = 0; i < fields.length; i++) {
			String fieldName = fields[i].getName();
			//Ignoring fields with $ in them - hibernate may sometimes shadow fields with this naming convention
			if (!fieldName.contains("$") && !isFieldIgnored(fieldName)) {
				for (int j = i + 1; j < fields.length; j++) {
					if (fieldName.equals(fields[j].getName())) {
						shadowedFields.add(fieldName);
					}
				}
			}
		}
		if (!shadowedFields.isEmpty()) {
			AssertUtils.fail("%s has shadowed fields: %s", serviceName, CollectionUtils.toString(shadowedFields, 20));
		}
	}


	@Test
	public void testInterfaceAndImplementingMethodParameterNamesMatch() throws Exception {
		Map<String, String> mismatchedParameterNames = new LinkedHashMap<>();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			if (!isServiceSkipped(stringObjectEntry.getKey())) {
				Object service = stringObjectEntry.getValue();
				List<Method> interfaceMethodList = new ArrayList<>();
				Class<?> currentClass = service.getClass();
				for (Class<?> classInterface : currentClass.getInterfaces()) {
					//Parameter names cannot be inferred from interfaces of external libraries
					if (classInterface.getName().startsWith("com.clifton")) {
						interfaceMethodList.addAll(Arrays.asList(classInterface.getDeclaredMethods()));
						for (Method method : classInterface.getDeclaredMethods()) {
							method.getParameters();
						}
					}
				}

				// public and non-abstract methods only
				BeanInfo beanInfo = Introspector.getBeanInfo(currentClass);

				for (Method method : currentClass.getMethods()) {
					if (processMethod(beanInfo, method)) {
						// check interfaces on all super classes as well
						if (!MethodUtils.isMethodUsingSameParamNames(method, interfaceMethodList.toArray(new Method[0]))) {
							mismatchedParameterNames.put(stringObjectEntry.getKey() + "." + method.getName(),
									"Method '" + stringObjectEntry.getKey() + "." + method.getName() + "' has mismatched parameter names.\n");
						}
					}
				}
			}
		}
		if (!mismatchedParameterNames.isEmpty()) {
			String assertionMessage = CollectionUtils.getStream(mismatchedParameterNames.values()).collect(Collectors.joining("\n"));
			Assertions.fail(assertionMessage);
		}
	}


	protected boolean processMethod(BeanInfo beanInfo, Method method) {
		return Modifier.isPublic(method.getModifiers()) && !Modifier.isAbstract(method.getModifiers()) && !MethodUtils.isGetterOrSetter(beanInfo, method);
	}


	/**
	 * Can be overridden to support special enums that either don't end in s even though they are
	 * plural or are skipped
	 */
	public Set<String> getEnumAllowedNamesOverride() {
		return null;
	}


	@SuppressWarnings("rawtypes")
	@Test
	public void testEnumsNamingConventionIsPlural() {
		// for each class in this project
		List<Class<Enum>> projectClassList = getProjectClassFiles(Enum.class);
		Set<String> overrides = getEnumAllowedNamesOverride();

		for (Class<Enum> projectClass : projectClassList) {
			if (overrides != null && overrides.contains(projectClass.getName())) {
				continue;
			}
			// Special Cases
			if (projectClass.getName().endsWith("Status")) {
				Assertions.fail("Enum [" + projectClass.getName() + "] is not plural.");
			}
			Assertions.assertTrue(projectClass.getName().endsWith("s"), "Enum [" + projectClass.getName() + "] is not plural.");
		}
	}


	@Test
	public void testServicesFollowOurNamingConvention() {
		Map<String, Object> serviceBeans = this.applicationContext.getBeansWithAnnotation(Service.class);
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			// ignore services from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				Assertions.assertTrue((stringObjectEntry.getKey()).endsWith("Service"), "@Service bean '" + stringObjectEntry.getKey() + "' for " + stringObjectEntry.getValue().getClass()
						+ " must have name that ends with 'Service': change bean/class name or stereotype annotation.");
			}
		}
	}


	@Test
	public void testControllersFollowOurNamingConvention() {
		if ("core".equals(getProjectPrefix())) { // skip this project
			return;
		}

		StringBuilder errors = new StringBuilder();
		Map<String, Object> serviceBeans = this.applicationContext.getBeansWithAnnotation(Controller.class);
		serviceBeans.putAll(this.applicationContext.getBeansWithAnnotation(Service.class));
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			// ignore controllers from other projects
			String serviceBeanName = stringObjectEntry.getKey();
			if (!isServiceSkipped(serviceBeanName) && serviceBeanName.startsWith(getProjectClassNamePrefix())) {
				Object controller = stringObjectEntry.getValue();
				if (controller.getClass().isAnnotationPresent(RestController.class)) {
					Assertions.assertTrue(serviceBeanName.endsWith("Service") || serviceBeanName.endsWith("Api"), "Controller bean name '" + serviceBeanName + "' for " + controller.getClass() + " must end with 'Service' or 'Api'");
				}
				else {
					Assertions.assertTrue(serviceBeanName.endsWith("Service"), "Controller bean name '" + serviceBeanName + "' for " + controller.getClass() + " must end with 'Service'");
				}
				for (Method method : controller.getClass().getMethods()) {
					if (AnnotationUtils.isAnnotationPresent(method, RequestMapping.class) || ContextConventionUtils.isUrlFromMethodAllowed(method, controller.getClass())) {
						RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
						String[] urls = (rm != null) ? rm.value() : new String[]{ContextConventionUtils.getUrlFromMethod(method)};
						for (String url : urls) {
							// check if same prefix as project prefix
							if (url == null) {
								errors.append("Cannot create URL for method: ").append(method).append("\n");
							}
							else {
								if (url.charAt(0) == '/') {
									url = url.substring(1);
								}
								if (!url.startsWith(getProjectClassNamePrefix()) && !url.startsWith("openapi")) {
									errors.append("URL: ").append(url).append(" for ").append(controller.getClass().getName())
											.append(" must start with either 'openapi' or the project prefix: ").append(getProjectClassNamePrefix()).append("\n");
								}
							}
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, errors.length(), errors.toString());
	}


	@Test
	public void testContextManagedBeanSuffixName() {
		List<String> allowedSuffixes = getAllowedContextManagedBeanSuffixNames();

		// allowed exceptions
		HashSet<String> exclusionsSet = new HashSet<>();
		exclusionsSet.add("loadTimeWeaver"); // defined by Spring
		exclusionsSet.add("storedProcedureExecutor"); // allow?
		exclusionsSet.add("templateBuilder"); // how to rename it?  Generator?
		exclusionsSet.add("xmlMigrationDefinitionReader"); // allow?
		exclusionsSet.add("logUtils");
		exclusionsSet.add("fileUtils");
		exclusionsSet.add("fileDownloadView");
		exclusionsSet.add("fileUploadView");
		exclusionsSet.add("jsonView");
		exclusionsSet.add("restTemplate"); // used for OpenAPI

		String[] beanNames = this.applicationContext.getBeanDefinitionNames();
		StringBuilder violations = new StringBuilder();
		int count = 0;
		List<String> violatingBeanNames = ArrayUtils.getStream(beanNames)
				.filter(beanName -> !CollectionUtils.anyMatch(allowedSuffixes, beanName::endsWith))
				.collect(Collectors.toList());
		for (String beanName : violatingBeanNames) {
			if (!isBeanSkipped(beanName)) {
				// XML DAO and DTO for testing are not violations
				Class<?> beanClass = this.applicationContext.getType(beanName);
				String factoryBeanName = ((GenericApplicationContext) this.applicationContext).getBeanDefinition(beanName).getFactoryBeanName();
				if (factoryBeanName != null) {
					// factory beans don't have the class defined
					if (!beanName.endsWith("Session") || !factoryBeanName.endsWith("Factory")) {
						violations.append("  name = '").append(beanName).append("', factory bean name = '").append(factoryBeanName).append("'\n");
						count++;
					}
				}
				else {
					Assertions.assertNotNull(beanClass, "Cannot retrieve bean type for bean with name '" + beanName + "'.");
					if (List.class.isAssignableFrom(beanClass) && beanName.endsWith("_data")) {
						continue;
					}
					if (this.applicationContext.containsBean(DaoUtils.getDaoName(beanClass) + "_data")) {
						List<?> dtoList = this.applicationContext.getBean(DaoUtils.getDaoName(beanClass) + "_data", List.class);
						if (dtoList.contains(this.applicationContext.getBean(beanName))) {
							continue;
						}
					}

					if (beanClass.isAnnotationPresent(RestController.class) && beanName.endsWith("Api")) {
						continue;
					}

					if (beanClass.getName().contains(".api") && (beanName.endsWith("Api") || beanName.endsWith("ApiClient"))) {
						continue;
					}

					if (!exclusionsSet.contains(beanName)) {
						violations.append("  name = '").append(beanName).append("', class = '").append(beanClass.getName()).append("'\n");
						count++;
					}
				}
			}
		}
		if (count > 0) {
			Assertions.fail("Found " + count + " bean name suffix violation(s) of our naming convention):\n" + violations);
		}
	}


	/**
	 * Returns a List of allowed Context Managed Bean Name suffixes.
	 * Note, this method can be overridden to add project specific bean name suffixes.
	 */
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		return CollectionUtils.createList("Action",
				"Adapter",
				"Aspect",
				"Cache",
				"Calculator",
				"Configuration",
				"Configurer",
				"Converter",
				"DAO",
				"DaoConfig",
				"Evaluator",
				"EventListener",
				"Factory",
				"Filter",
				"Generator",
				"Handler",
				"Initializer",
				"List",
				"Locator",
				"Manager",
				"Map",
				"Marshaller",
				"ObjectMapper",
				"Observer",
				"Populator",
				"Processor",
				"Provider",
				"Registrator",
				"Replicator",
				"Resolver",
				"Retriever",
				"Rounder",
				"Rule",
				"Scheduler",
				"Service",
				"Unmarshaller",
				"Validator"
		);
	}


	/**
	 * For every @Service, @Controller, or @Component annotation, if there is an
	 * override for the bean name, verifies that it doesn't match the default.  If it does
	 * throws an exception because the override is not necessary
	 */
	@Test
	public void testUnnecessaryOverrideSpringBeanName() {
		Map<String, Object> controllerBeans = this.applicationContext.getBeansWithAnnotation(Controller.class);
		for (Map.Entry<String, Object> stringObjectEntry : controllerBeans.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				Object controller = stringObjectEntry.getValue();
				// Validate Override on the Controller Annotation
				Controller controllerAnnotation = (AnnotationUtils.getAnnotation(controller.getClass(), Controller.class));
				if (controllerAnnotation != null) {
					validateOverrideBeanName(controller.getClass().getName(), Controller.class, controllerAnnotation.value());
				}
			}
		}

		Map<String, Object> serviceBeans = this.applicationContext.getBeansWithAnnotation(Service.class);
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			// ignore services from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				Object service = stringObjectEntry.getValue();
				// Validate Override on the Service Annotation
				Service serviceAnnotation = (AnnotationUtils.getAnnotation(service.getClass(), Service.class));
				if (serviceAnnotation != null) {
					validateOverrideBeanName(service.getClass().getName(), Service.class, serviceAnnotation.value());
				}
			}
		}

		Map<String, Object> componentBeans = this.applicationContext.getBeansWithAnnotation(Component.class);
		for (Map.Entry<String, Object> stringObjectEntry : componentBeans.entrySet()) {
			// ignore services from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				Object bean = stringObjectEntry.getValue();
				// Validate Override on the Service Annotation
				Component componentAnnotation = (AnnotationUtils.getAnnotation(bean.getClass(), Component.class));
				if (componentAnnotation != null) {
					validateOverrideBeanName(bean.getClass().getName(), Component.class, componentAnnotation.value());
				}
			}
		}
	}


	private void validateOverrideBeanName(String className, Class<?> annotationClass, String overrideName) {
		if (!StringUtils.isEmpty(overrideName)) {
			String defaultName = ContextConventionUtils.getBeanNameFromClassName(className);
			Assertions.assertFalse(
					StringUtils.isEqual(overrideName, defaultName), "Default Bean Name Matches " + StringUtils.substringAfterLast(annotationClass.getName(), ".") + " value override [" + overrideName + "] for Class ["
							+ StringUtils.substringAfterLast(className, ".") + "].  Please remove override.");
		}
	}


	/**
	 * For every @Service, @Controller, or @Component annotation, if there is a property that ends in "Cache" and is injected from a spring managed bean this
	 * validates that the interface is declared, not the implementation
	 */
	@Test
	public void testCacheDeclaredAsInterface() throws Exception {
		Map<String, Object> controllerBeans = this.applicationContext.getBeansWithAnnotation(Controller.class);
		controllerBeans.putAll(this.applicationContext.getBeansWithAnnotation(Service.class));
		controllerBeans.putAll(this.applicationContext.getBeansWithAnnotation(Component.class));
		for (Map.Entry<String, Object> stringObjectEntry : controllerBeans.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				Object controller = stringObjectEntry.getValue();

				BeanInfo beanInfo = Introspector.getBeanInfo(controller.getClass());
				// for each property: if it's spring managed insure it's declared as interface
				for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
					if (propertyDescriptor.getName().endsWith("Cache") && this.applicationContext.containsBean(propertyDescriptor.getName())) {
						Assertions.assertTrue(propertyDescriptor.getPropertyType().isInterface(), "Bean " + stringObjectEntry.getKey() + " property for [" + propertyDescriptor.getName() + "] is not declared as an interface.");
					}
				}
			}
		}
	}


	/**
	 * For every @RequestMapping method annotation, if there is an
	 * override for the url, verifies that it doesn't match the default we would generate.  If it does
	 * throws an exception because the override is not necessary
	 */
	@Test
	public void testUnnecessaryRequestMappingURL() {
		Map<String, Object> serviceBeans = this.applicationContext.getBeansWithAnnotation(Controller.class);
		serviceBeans.putAll(this.applicationContext.getBeansWithAnnotation(Service.class));
		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				// These objects already have Request Mapping Annotation that we add by default
				// So we want only what's manually entered - so instead get the class file
				// and check annotations there
				Object controller = stringObjectEntry.getValue();

				Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());
				for (Method method : clz.getMethods()) {
					if (AnnotationUtils.isAnnotationPresent(method, RequestMapping.class) || ContextConventionUtils.isUrlFromMethodAllowed(method, controller.getClass())) {
						RequestMapping rm = AnnotationUtils.getAnnotation(method, RequestMapping.class);
						String[] urls = (rm != null) ? rm.value() : new String[]{ContextConventionUtils.getUrlFromMethod(method)};
						for (String url : urls) {
							// if the URL doesn't begin with the slash or end with .json then it's manually overridden
							if (url == null) {
								throw new IllegalStateException("Cannot create URL for method: " + method);
							}
							if (url.charAt(0) != '/' || !url.endsWith(ContextConventionUtils.URL_DEFAULT_EXTENSION)) {
								// DON'T Check this if ContextConventionUtils.isUrlFromMethodAllowed returns false
								// That means that the system can't auto generate the RM url (i.e. has interface parameter)
								if (ContextConventionUtils.isUrlFromMethodAllowed(method, controller.getClass())) {
									String defaultUrl = ContextConventionUtils.getUrlFromMethod(method);
									validateOverrideRequestMappingUrl(results, controller.getClass().getName(), method, url, defaultUrl);
								}
							}
						}
						// If explicit request mapping and also has explicit on the interface method (api usage) make sure url is the same
						if (rm != null) {
							Method interfaceMethod = MethodUtils.getInterfaceMethod(method);
							if (interfaceMethod != null) {
								RequestMapping irm = AnnotationUtils.getAnnotation(interfaceMethod, RequestMapping.class);
								if (irm != null) {
									String[] interfaceUrls = irm.value();
									if (!CompareUtils.isEqual(urls, interfaceUrls)) {
										Assertions.fail(clz.getName() + " " + method.getName() + " has request mapping annotation on both the implementation and interface but they don't match.");
									}
								}
							}
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "Default Request Mapping URL overrides match default for the following method(s).  Please remove overrides:" + StringUtils.NEW_LINE + results);
	}


	private void validateOverrideRequestMappingUrl(StringBuilder results, String className, Method method, String overrideUrl, String defaultUrl) {
		if (!StringUtils.isEmpty(overrideUrl)) {
			// We determine an override really is an override if it doesn't start with / or end with .json

			if (StringUtils.isEqual(stripUrl(overrideUrl), stripUrl(defaultUrl))) {
				results.append(StringUtils.NEW_LINE).append(className).append(".").append(method.getName()).append(": ").append(overrideUrl);
			}
		}
	}


	/**
	 * Finds each method with annotation @DoNotAddRequestMapping and verifies that there are no additional
	 * Spring MVC annotations and / or the @SecureMethod() annotation.
	 */
	@Test
	public void testForConflictingServiceMethodAnnotations() {
		List<String> errorList = new ArrayList<>();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Object service : serviceBeans.entrySet()) {
			Class<?> cls = service.getClass();
			for (Method method : cls.getMethods()) {
				Set<Class<?>> annotationClassSet = ArrayUtils.getStream(AnnotationUtils.getAnnotations(method)).map(Annotation::annotationType).collect(Collectors.toSet());
				if (CollectionUtils.contains(annotationClassSet, DoNotAddRequestMapping.class)) {
					for (Class<?> annotationClass : annotationClassSet) {
						if (CollectionUtils.contains(CONFLICTING_ANNOTATIONS, annotationClass)) {
							errorList.add(String.format("Method [%s#%s]: Annotation [%s] conflicts with the presence of annotation [%s]. Both annotations cannot be assigned to the same method.", cls.getName(), method.getName(), annotationClass.getName(), DoNotAddRequestMapping.class.getName()));
						}
					}
				}
			}
		}
		if (!errorList.isEmpty()) {
			Assertions.fail(errorList.stream()
					.sorted()
					.collect(Collectors.joining("\n")));
		}
	}


	@Test
	public void testForRequiredServiceMethodAnnotations() {
		List<String> errorList = new ArrayList<>();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Object service : serviceBeans.values()) {
			Class<?> cls = service.getClass();
			for (Method method : cls.getMethods()) {
				Set<Class<?>> annotationClassSet = ArrayUtils.getStream(AnnotationUtils.getAnnotations(method)).map(Annotation::annotationType).collect(Collectors.toSet());
				List<Class<?>> matchingAnnotationList = CollectionUtils.getIntersection(annotationClassSet, ANNOTATIONS_REQUIRING_DO_NOT_ADD_REQUEST_MAPPING);
				if (!matchingAnnotationList.isEmpty() && !CollectionUtils.contains(annotationClassSet, DoNotAddRequestMapping.class)) {
					for (Class<?> annotationClass : matchingAnnotationList) {
						errorList.add(String.format("Method [%s#%s]: Annotation [%s] requires the presence of the sibling annotation [%s].", cls.getName(), method.getName(), annotationClass.getName(), DoNotAddRequestMapping.class.getName()));
					}
				}
			}
		}
		if (!errorList.isEmpty()) {
			Assertions.fail(errorList.stream()
					.sorted()
					.collect(Collectors.joining("\n")));
		}
	}


	/**
	 * For every @SecureMethod annotation, if permissions() is not 0 will compare to default permission level based on method name If it matches default, throws
	 * an exception because the override is not necessary
	 */
	@Test
	public void testUnnecessarySecureMethodAnnotation() {
		Map<String, Object> controllerMap = this.applicationContext.getBeansWithAnnotation(Controller.class);
		controllerMap.putAll(this.applicationContext.getBeansWithAnnotation(Service.class));

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				// These objects already have Request Mapping Annotation that we add by default
				// So we want only what's manually entered - so instead get the class file
				// and check annotations there
				Object controller = stringObjectEntry.getValue();

				Class<?> clz = controller.getClass();
				for (Method method : clz.getMethods()) {
					if (AnnotationUtils.isAnnotationPresent(method, SecureMethod.class)) {
						SecureMethod secureAnnotation = AnnotationUtils.getAnnotation(method, SecureMethod.class);
						if (secureAnnotation.permissions() != 0) {
							int defaultPermission = ContextConventionUtils.getRequiredPermissionFromMethodName(method.getName(), null);
							if (defaultPermission == secureAnnotation.permissions()) {
								results.append(generateServiceMethodExceptionMessage(controller, method, "Unnecessary \"permission\" value override."));
							}
							//else {
							// results.append(StringUtils.NEW_LINE + "Different Permission Override: " + controller.getClass().getName() + "." + method.getName());
							//}
						}
						// Check if Not explicit Security Resource and Not Explicit Resolver
						// And Table Name or DTO Class populated
						if (SecureMethod.DEFAULT_STRING.equals(secureAnnotation.securityResource()) && SecureMethod.DEFAULT_STRING.equals(secureAnnotation.securityResourceResolverBeanName())
								&& SecureMethod.DEFAULT_STRING.equals(secureAnnotation.tableResolverBeanName())) {
							String tableName = (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.table())) ? secureAnnotation.table() : null;
							@SuppressWarnings("unchecked")
							Class<IdentityObject> dtoClass = (!IdentityObject.class.equals(secureAnnotation.dtoClass()) ? (Class<IdentityObject>) secureAnnotation.dtoClass() : null);

							if (tableName != null || dtoClass != null) {
								// skip explicitly defined naming convention violations
								if (!secureAnnotation.namingConventionViolation()) {
									// try to determine table name
									ObjectWrapper<Class<IdentityObject>> dtoClassHolder = new ObjectWrapper<>(null);

									try {
										String defaultTableName = ContextConventionUtils.getTableNameFromMethod(method, method.getName(), dtoClassHolder, this.daoLocator);
										if (tableName != null && StringUtils.isEqual(tableName, defaultTableName)) {
											results.append(generateServiceMethodExceptionMessage(controller, method, "Unnecessary \"table\" value override."));
										}
										else if (dtoClassHolder.getObject() != null && dtoClassHolder.getObject().equals(dtoClass)) {
											results.append(generateServiceMethodExceptionMessage(controller, method, "Unnecessary \"dtoClass\" value override."));
										}
									}
									catch (Exception e) {
										results.append(generateServiceMethodExceptionMessage(controller, method, e.getMessage()));
									}
								}
							}
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "SecureMethod annotation violations found for the following method(s):" + StringUtils.NEW_LINE + results);
	}


	/**
	 * For every @SecureMethod annotation, if dynamic properties are present will validate url/properties are set up correctly and types of properties match
	 * what is expected
	 */
	@Test
	public void testInvalidDynamicSecureMethodAnnotationProperties() {
		Map<String, Object> controllerMap = this.applicationContext.getBeansWithAnnotation(Controller.class);
		controllerMap.putAll(this.applicationContext.getBeansWithAnnotation(Service.class));

		StringBuilder results = new StringBuilder(16);
		for (Map.Entry<String, Object> stringObjectEntry : controllerMap.entrySet()) {
			// ignore controllers from other projects
			if ((stringObjectEntry.getKey()).startsWith(getProjectClassNamePrefix())) {
				// These objects already have Request Mapping Annotation that we add by default
				// So we want only what's manually entered - so instead get the class file
				// and check annotations there
				Object controller = stringObjectEntry.getValue();

				Class<?> clz = CoreClassUtils.getClass(controller.getClass().getName());
				for (Method method : clz.getMethods()) {
					if (AnnotationUtils.isAnnotationPresent(method, SecureMethod.class)) {
						SecureMethod secureAnnotation = AnnotationUtils.getAnnotation(method, SecureMethod.class);

						if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameUrlParameter())) {
							// Validate Table Name Parameter
							validateUrlParameterPresent(controller, method, "Table Name", secureAnnotation.dynamicTableNameUrlParameter(), String.class, results);
						}
						if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameBeanPath()) || !SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicSecurityResourceBeanPath())) {
							try {
								String tableName = (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.table())) ? secureAnnotation.table() : null;
								@SuppressWarnings("unchecked")
								Class<IdentityObject> dtoClass = (!IdentityObject.class.equals(secureAnnotation.dtoClass()) ? (Class<IdentityObject>) secureAnnotation.dtoClass() : null);
								ObjectWrapper<Class<IdentityObject>> dtoClassHolder = new ObjectWrapper<>(null);
								String defaultTableName = (!secureAnnotation.namingConventionViolation()) ? ContextConventionUtils.getTableNameFromMethod(method, method.getName(), dtoClassHolder, this.daoLocator) : null;

								dtoClass = dtoClass == null ? dtoClassHolder.getObject() : dtoClass;
								tableName = ObjectUtils.coalesce(tableName, defaultTableName);
								if (dtoClass == null) {
									if (tableName != null) {
										dtoClass = this.daoLocator.locate(tableName).getConfiguration().getBeanClass();
									}
								}

								if (dtoClass == null) {
									results.append(generateServiceMethodExceptionMessage(controller, method, "Missing DTO Class TO evaluate dynamic bean path (" + secureAnnotation.dynamicTableNameBeanPath() + ")"));
								}
								else {
									if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicTableNameBeanPath())) {
										validateStringPropertyPresent(controller, method, "table name", secureAnnotation.dynamicTableNameBeanPath(), dtoClass, results);
									}

									if (!SecureMethod.DEFAULT_STRING.equals(secureAnnotation.dynamicSecurityResourceBeanPath())) {
										validateStringPropertyPresent(controller, method, "security resource", secureAnnotation.dynamicSecurityResourceBeanPath(), dtoClass, results);
									}
								}
								// Validate ID Parameter
								// No ID property - then just expect it to be a Number?
								validateUrlParameterPresent(controller, method, "ID", secureAnnotation.dynamicIdUrlParameter(), dtoClass != null ? BeanUtils.getPropertyType(dtoClass, "id") : Number.class, results);
							}
							catch (Exception e) {
								results.append(generateServiceMethodExceptionMessage(controller, method, ExceptionUtils.getOriginalMessage(e)));
							}
						}
					}
				}
			}
		}
		Assertions.assertEquals(0, results.length(), "SecureMethod annotation dynamic property violations found for the following method(s):" + StringUtils.NEW_LINE + results);
	}


	private void validateUrlParameterPresent(Object controller, Method method, String parameterLabel, String parameterName, Class<?> expectedType, StringBuilder results) {
		Parameter[] parameters = method.getParameters();
		boolean found = false;

		for (Parameter param : parameters) {
			if (param.getName().equals(parameterName)) {
				found = true;
				if (!expectedType.isAssignableFrom(param.getType())) {
					if (!(expectedType.equals(Integer.class) && param.getType().equals(int.class)) && !(expectedType.equals(Short.class) && param.getType().equals(short.class)) && !(expectedType.equals(Long.class) && param.getType().equals(long.class))) {
						results.append(generateServiceMethodExceptionMessage(controller, method, "Invalid Parameter for " + parameterLabel + "[" + parameterName + "]. Expected " + expectedType.getSimpleName() + " property but was " + param.getType().getName()));
					}
				}
				break;
			}
			if (!param.getType().isPrimitive() && BeanUtils.isPropertyPresent(param.getType(), parameterName)) {
				Class<?> type = BeanUtils.getPropertyType(param.getType(), parameterName);
				if (type != null) {
					found = true;
					if (!expectedType.isAssignableFrom(type)) {
						results.append(generateServiceMethodExceptionMessage(controller, method, "Invalid Parameter for " + parameterLabel + " [" + parameterName + "]. Expected " + expectedType.getSimpleName() + " property but was " + type.getName()));
					}
					break;
				}
			}
		}
		if (!found) {
			results.append(generateServiceMethodExceptionMessage(controller, method, "Missing Parameter for " + parameterLabel + " [" + parameterName + "]"));
		}
	}


	private void validateStringPropertyPresent(Object controller, Method method, String label, String beanPropertyPath, Class<?> dtoClass, StringBuilder results) {
		Class<?> type = BeanUtils.getPropertyType(dtoClass, beanPropertyPath);
		if (type != null) {
			if (!StringUtils.isEqual(String.class.getName(), type.getName())) {
				results.append(generateServiceMethodExceptionMessage(controller, method, "Invalid dynamic " + label + " bean path (" + beanPropertyPath + ") on DTO Class " + dtoClass.getSimpleName() + ". Expected String property but was " + type.getName()));
			}
		}
		else {
			results.append(generateServiceMethodExceptionMessage(controller, method, "Missing dynamic " + label + " bean path (" + beanPropertyPath + ") on DTO Class " + dtoClass.getSimpleName()));
		}
	}


	private String generateServiceMethodExceptionMessage(Object controller, Method method, String message) {
		return StringUtils.NEW_LINE + controller.getClass().getName() + "." + method.getName() + ": " + message;
	}


	private String stripUrl(String url) {
		if (url != null) {
			if (url.charAt(0) == '/') {
				url = url.substring(1);
			}
			if (url.endsWith(".json")) {
				url = url.substring(0, url.length() - 5);
			}
		}
		return url;
	}


	protected void configureApprovedPackageNames(@SuppressWarnings("unused") Set<String> approvedList) {
		// do nothing
	}


	/**
	 * Ensure that projects follow our package naming convention.
	 * <p>
	 * This also makes it explicit if a project uses non-standard packages. See "configureApprovedPackageNames"
	 */
	@Test
	public void testApprovedPackageNamesUsed() {
		Set<String> approvedProjects = new HashSet<>();
		CoreConventionUtils.configureApprovedPackageNames(approvedProjects);
		configureApprovedPackageNames(approvedProjects);

		// Collect full class names
		List<String> projectClassNames = getProjectClassFiles(Object.class).stream()
				.map(Class::getName)
				.collect(Collectors.toList());

		// Find any packages which do not follow the basic project-level package name conventions
		String projectPackagePrefix = getProjectPackage() + '.';
		String packageNamesInViolationOfBasicConvention = projectClassNames.stream()
				.filter(className -> !className.startsWith(projectPackagePrefix))
				.collect(Collectors.joining(", "));
		Assertions.assertTrue(StringUtils.isEmpty(packageNamesInViolationOfBasicConvention), "Packages in violation of basic project convention found : [" + packageNamesInViolationOfBasicConvention + "]");

		// Find all sub-package names within this project that are not on the approved list
		String packageNamesNotApproved = projectClassNames.stream()
				.map(className -> className.substring(projectPackagePrefix.length()))
				.map(classNameWithinProject -> StringUtils.substringBeforeLast(classNameWithinProject, "."))
				.map(packageName -> packageName.split("\\.")[0])
				.filter(subPackageName -> !subPackageName.isEmpty())
				.filter(subPackageName -> !approvedProjects.contains(subPackageName))
				.distinct()
				.collect(Collectors.joining(", "));

		Assertions.assertTrue(StringUtils.isEmpty(packageNamesNotApproved), "Packages in violation of convention found : [" + packageNamesNotApproved + "]");
	}


	/**
	 * If a project uses non-standard API, add corresponding packages/classes that are ok to be used
	 * to the specified imports List.  If an import starts with what's in this list, it will be allowed.
	 *
	 * @param imports the list to which the allowed import patterns will be added
	 */
	protected void configureApprovedClassImports(@SuppressWarnings("unused") List<String> imports) {
		// do nothing
	}


	/**
	 * Adds approved import patterns for test classes to the given list. This method should be overridden
	 * (calling <tt>super</tt>) when additional import patterns need to be allowed for a particular module.
	 *
	 * @param imports the list to which the allowed import patterns will be added
	 */
	protected void configureApprovedTestClassImports(@SuppressWarnings("unused") List<String> imports) {
		configureApprovedClassImports(imports);
	}


	/**
	 * Validates imports within source classes against a provided package name white-list.
	 * <p>
	 * The imports white-list prevents accidental mis-selected imports when they, for example,
	 * conflict with naming of our classes.
	 * <p>
	 * White-listed patterns are treated as prefixes for package/class-names. E.g., <tt>"com.jcraft.jsch."</tt>
	 * is sufficient to allow all classes in <tt>jsch</tt> and all sub-packages.
	 *
	 * @see #configureApprovedClassImports(List)
	 * @see CoreConventionUtils#configureApprovedClassImports(List)
	 */
	@Test
	public void testApprovedClassImportsUsed() {
		// the following imports are allowed
		List<String> approvedList = new ArrayList<>();
		CoreConventionUtils.configureApprovedClassImports(approvedList);
		configureApprovedClassImports(approvedList);
		for (String sourceSet : getProjectProductionSourceSets()) {
			validateApprovedClassImportsUsed(getProjectSourceSetJavaDirectory(sourceSet), getProjectSourceSetClassFiles(sourceSet, Object.class), approvedList);
		}
	}


	/**
	 * Validates imports within test source classes against a provided package name white-list. This white-list
	 * includes additional import patterns that are only allowed within test classes.
	 *
	 * @see #testApprovedClassImportsUsed()
	 * @see #configureApprovedTestClassImports(List)
	 * @see CoreConventionUtils#configureApprovedTestClassImports(List)
	 */
	@Test
	public void testApprovedTestClassImportsUsed() {
		List<String> approvedList = new ArrayList<>();
		CoreConventionUtils.configureApprovedTestClassImports(approvedList);
		configureApprovedTestClassImports(approvedList);
		for (String sourceSet : getProjectTestSourceSets()) {
			validateApprovedClassImportsUsed(getProjectSourceSetJavaDirectory(sourceSet), getProjectSourceSetClassFiles(sourceSet, Object.class), approvedList);
		}
	}


	/**
	 * Validates the import patterns used within the class scope provided. If any import patterns exist which do not
	 * satisfy the requirements, then a failed assertion will be raised.
	 *
	 * @param rootDirectory the root directory for the source files which will be validated
	 * @param classList     the list of classes which will be validated
	 * @param approvedList  the list of approved import pattern prefixes
	 */
	private void validateApprovedClassImportsUsed(String rootDirectory, List<Class<Object>> classList, List<String> approvedList) {
		// for each class in this project
		JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
		StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
		List<String> unapprovedImportList = new ArrayList<>();
		for (Class<Object> projectClass : classList) {
			Iterable<? extends JavaFileObject> files = fileManager.getJavaFileObjects(rootDirectory + projectClass.getName().replaceAll("\\.", "/") + ".java");
			for (JavaFileObject file : files) {
				try {
					String javaFile = file.getCharContent(true).toString();
					BufferedReader bufferedReader = new BufferedReader(new StringReader(javaFile));
					String sourceLine;
					// read each source line of the file
					while ((sourceLine = bufferedReader.readLine()) != null) {
						if (sourceLine.startsWith("public ")) {
							// got to class definition: skip the rest to improve performance
							break;
						}
						if (sourceLine.startsWith("import ")) {
							String ref = sourceLine.substring(7, sourceLine.length() - 1);
							if (ref.startsWith("static ")) {
								// allow static imports for approved packages
								ref = ref.substring(7, ref.length() - 1);
							}
							boolean approved = false;
							for (String item : approvedList) {
								if (ref.startsWith(item)) {
									approved = true;
									break;
								}
							}
							if (!approved) {
								unapprovedImportList.add(projectClass.getName() + " class has NOT approved import: " + ref);
							}
						}
					}
				}
				catch (IOException e) {
					String stackTrace = ExceptionUtils.getFullStackTrace(e);
					if (stackTrace != null) {
						stackTrace = stackTrace.replaceAll("\n", "\n\t");
					}
					unapprovedImportList.add(String.format("An exception occurred while processing class [%s]: %s", projectClass.getName(), stackTrace));
				}
			}
		}
		if (!CollectionUtils.isEmpty(unapprovedImportList)) {
			String message = unapprovedImportList.stream().collect(Collectors.joining("\n\t", "Unapproved imports were found:\n\t", ""));
			Assertions.fail(message);
		}
	}


	/**
	 * If a DTO contains a member property that is calculated or dependent on other properties,
	 * it can be added to the Set of skipPropertyNames to avoid validation issues. The initial
	 * skipPropertyNames contains the "label" property.
	 *
	 * @param skipPropertyNames the set of property names to skip validation
	 * @see #testDTOGetSet()
	 */
	protected void configureDTOSkipPropertyNames(@SuppressWarnings("unused") Set<String> skipPropertyNames) {
		// do nothing
	}


	@Test
	public void testDTOGetSet() throws Exception {
		List<Class<IdentityObject>> beanClasses = getProjectClassFiles(IdentityObject.class);
		Set<String> skipProperties = new HashSet<>();
		skipProperties.add("label");
		configureDTOSkipPropertyNames(skipProperties);
		testBeanGetSet(beanClasses, skipProperties);
	}


	/**
	 * Use {@link ValueChangingSetter} or {@link ValueIgnoringGetter} annotations to exclude non true get/setter methods from this test.
	 */
	@Test
	public void testOtherClassesGetSet() throws Exception {
		List<Class<Object>> classesToTest = getProjectClassFiles(Object.class);
		// filter out classes tested by other methods
		List<Class<?>> classesToSkip = getSkipGetSetTestClasses();
		classesToTest = CollectionUtils.getFiltered(classesToTest, clazz -> !(IdentityObject.class.isAssignableFrom(clazz) || BaseSearchForm.class.isAssignableFrom(clazz) || classesToSkip.contains(clazz)));
		testBeanGetSet(classesToTest, new HashSet<>());
	}


	/**
	 * Override to return a List of classes that should be skipped for get/set testing.
	 */
	protected List<Class<?>> getSkipGetSetTestClasses() {
		return Collections.emptyList();
	}


	@Test
	public void testSearchFormConfiguration() {
		StringBuilder errorMessage = new StringBuilder();
		List<Class<BaseSearchForm>> beanClasses = getProjectClassFiles(BaseSearchForm.class);
		for (Class<BaseSearchForm> clazz : CollectionUtils.getIterable(beanClasses)) {
			for (Field field : ClassUtils.getClassFields(clazz, true, false)) {
				SearchField searchConfig = AnnotationUtils.getAnnotation(field, SearchField.class);
				if (searchConfig != null && "id".equals(searchConfig.searchField())) {
					String searchPath = searchConfig.searchFieldPath();
					// skip cases with no path or paths ending on many-to-many link
					if (!SearchField.SAME_TABLE.equals(searchPath) && !searchPath.endsWith("List")) {
						errorMessage.append("    ").append(field).append("\n");
					}
				}
			}
		}

		if (errorMessage.length() > 0) {
			Assertions.fail("The following Search Form fields have unnecessary extra join:\n\n" + errorMessage);
		}
	}


	@Test
	public void testSearchFormGetSet() throws Exception {
		List<Class<BaseSearchForm>> beanClasses = getProjectClassFiles(BaseSearchForm.class);
		Set<String> skipProperties = new HashSet<>();

		// Validates for each field annotated with SearchField the getter/setter is present
		StringBuilder invalidString = new StringBuilder(50);
		for (Class<BaseSearchForm> beanClass : CollectionUtils.getIterable(beanClasses)) {
			Field[] fields = ClassUtils.getClassFields(beanClass, false, true);
			for (Field field : fields) {
				if (AnnotationUtils.isAnnotationPresent(field, SearchField.class)) {
					PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(beanClass, field.getName());
					String missing = null;
					if (descriptor == null) {
						missing = "Getter/Setter";
					}
					else if (descriptor.getReadMethod() == null) {
						missing = "Getter";
					}
					else if (descriptor.getWriteMethod() == null) {
						missing = "Setter";
					}
					if (!StringUtils.isEmpty(missing)) {
						invalidString.append("Missing ").append(missing).append(" for property ").append(field.getName()).append(" on ").append(beanClass).append(StringUtils.NEW_LINE);
					}
				}
			}
		}
		Assertions.assertEquals(0, invalidString.length(), invalidString.toString());

		// If there is a getter/setter then confirms setting the value, then reading the value
		testBeanGetSet(beanClasses, skipProperties);
	}


	protected <T> void testBeanGetSet(List<Class<T>> beanClasses, Set<String> skipProperties) throws Exception {
		for (Class<T> beanClass : beanClasses) {
			int classModifier = beanClass.getModifiers();
			boolean mockBean = (Modifier.isAbstract(classModifier) || !ClassUtils.isDefaultConstructorPresent(beanClass));
			if (Modifier.isInterface(classModifier) || (mockBean && Modifier.isFinal(classModifier))) {
				continue;
			}
			// try to mock using real methods when instantiation is not possible
			Object bean = mockBean ? Mockito.mock(beanClass, Mockito.CALLS_REAL_METHODS) : BeanUtils.newInstance(beanClass);
			BeanInfo beanInfo = Introspector.getBeanInfo(beanClass);
			// for each DTO property: set value, get value, verify that it's the same
			for (PropertyDescriptor propertyDescriptor : beanInfo.getPropertyDescriptors()) {
				// Skip get/set for label property as we often override getLabel method with additional information.
				if (skipProperties.contains(propertyDescriptor.getName()) || skipProperties.contains(beanClass.getSimpleName() + "." + propertyDescriptor.getName())) {
					continue;
				}

				// check naming convention first
				Class<?> propertyType = propertyDescriptor.getPropertyType();
				if (Integer.class.equals(propertyType) || int.class.equals(propertyType) || Long.class.equals(propertyType) || long.class.equals(propertyType)) {
					// make sure int properties don't end on ID
					String name = propertyDescriptor.getName();
					Assertions.assertFalse((name.length() > 2 && name.endsWith("ID")), "int/long property '" + name + "' on '" + beanClass + "' cannot end with ID suffix. Use 'Id' instead.");
				}

				Method readMethod = propertyDescriptor.getReadMethod();
				Method writeMethod = propertyDescriptor.getWriteMethod();
				if (readMethod != null && writeMethod != null && !Modifier.isAbstract(readMethod.getModifiers()) && !Modifier.isAbstract(writeMethod.getModifiers())) {
					if (AnnotationUtils.getAnnotation(writeMethod, ValueChangingSetter.class) != null || AnnotationUtils.getAnnotation(readMethod, ValueIgnoringGetter.class) != null) {
						// skip get/set when set property is marked with the ValueChangingSetter annotation
						continue;
					}
					if (!readMethod.getDeclaringClass().getName().startsWith(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION) || !writeMethod.getDeclaringClass().getName().startsWith(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION)) {
						// skip get/set that are not part of our code base
						continue;
					}

					if (int.class.equals(propertyType) || Integer.class.equals(propertyType)) {
						MethodUtils.invoke(writeMethod, bean, 123);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(123, result, "Read different int value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (short.class.equals(propertyType) || Short.class.equals(propertyType)) {
						MethodUtils.invoke(writeMethod, bean, (short) 321);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals((short) 321, result, "Read different short value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (long.class.equals(propertyType) || Long.class.equals(propertyType)) {
						MethodUtils.invoke(writeMethod, bean, 1_234_567_890_123L);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(1_234_567_890_123L, result, "Read different long value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (String.class.equals(propertyType)) {
						MethodUtils.invoke(writeMethod, bean, "test string");
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals("test string", result, "Read different String value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (boolean.class.equals(propertyType) || Boolean.class.equals(propertyType)) {
						MethodUtils.invoke(writeMethod, bean, true);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(true, result, "Read different boolean value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");

						MethodUtils.invoke(writeMethod, bean, false);
						result = readMethod.invoke(bean);
						Assertions.assertEquals(false, result, "Read different boolean value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (Date.class.equals(propertyType)) {
						Date value = new Date();
						MethodUtils.invoke(writeMethod, bean, value);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(value, result, "Read different Date value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (BigDecimal.class.equals(propertyType)) {
						BigDecimal value = new BigDecimal("153.6");
						MethodUtils.invoke(writeMethod, bean, value);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(value, result, "Read different BigDecimal value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (IdentityObject.class.isAssignableFrom(propertyType)) {
						int mod = propertyType.getModifiers();
						Object value = (Modifier.isAbstract(mod) || Modifier.isInterface(mod) || !ClassUtils.isDefaultConstructorPresent(propertyType)) ? null : BeanUtils.newInstance(propertyType);
						MethodUtils.invoke(writeMethod, bean, value);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(value, result, "Read different IdentityObject value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (CustomJsonString.class.equals(propertyType)) {
						MethodUtils.invoke(writeMethod, bean, new CustomJsonString("{id:123}"));
						CustomJsonString result = (CustomJsonString) readMethod.invoke(bean);
						Assertions.assertEquals("{id:123}", result.getJsonValue(), "Read different CustomJsonString value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (List.class.isAssignableFrom(propertyType)) {
						List<?> value = new ArrayList<>();
						MethodUtils.invoke(writeMethod, bean, value);
						Object result = readMethod.invoke(bean);
						Assertions.assertEquals(value, result, "Read different List value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
					}
					else if (propertyType.isArray()) {
						Class<?> componentTypeClass = propertyType.getComponentType();
						Object value;
						if (int.class.equals(componentTypeClass)) {
							value = new int[]{1, 2, 3};
						}
						else if (Integer.class.equals(componentTypeClass)) {
							value = new Integer[]{1, 2, 3};
						}
						else if (short.class.equals(componentTypeClass)) {
							value = new short[]{(short) 10, (short) 20, (short) 30};
						}
						else if (Short.class.equals(componentTypeClass)) {
							value = new Short[]{(short) 10, (short) 20, (short) 30};
						}
						else if (long.class.equals(componentTypeClass)) {
							value = new long[]{100L, 200L, 300L};
						}
						else if (Long.class.equals(componentTypeClass)) {
							value = new Long[]{100L, 200L, 300L};
						}
						else if (boolean.class.equals(componentTypeClass)) {
							value = new boolean[]{true, false};
						}
						else if (Boolean.class.equals(componentTypeClass)) {
							value = new Boolean[]{true, false};
						}
						else if (byte.class.equals(componentTypeClass)) {
							value = new byte[]{1, 2};
						}
						else if (String.class.equals(componentTypeClass)) {
							value = new String[]{"test string 1", "test string 2"};
						}
						else if (componentTypeClass.isEnum()) {
							value = componentTypeClass.getEnumConstants();
						}
						else {
							// do not test unexpected data types: enhance based on test coverage gaps if appropriate
							continue;
						}
						MethodUtils.invoke(writeMethod, bean, value);
						Object result = readMethod.invoke(bean);
						Assertions.assertTrue(CompareUtils.isEqual(value, result), "Read different Array value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "' for elements type: " + componentTypeClass);
					}
					else if (propertyType.isEnum()) {
						for (Object value : propertyType.getEnumConstants()) {
							MethodUtils.invoke(writeMethod, bean, value);
							Object result = readMethod.invoke(bean);
							Assertions.assertEquals(value, result, "Read different Enum value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");
						}
					}
					else {
						Object value = null;
						try {
							// support interfaces too through mocking
							value = Modifier.isFinal(propertyType.getModifiers()) ? Mockito.spy(BeanUtils.newInstance(propertyType)) : Mockito.spy(propertyType);
						}
						catch (Exception e) {
							// can't test if the type cannot be instantiated
						}
						if (value != null) {
							MethodUtils.invoke(writeMethod, bean, new Object[]{null});
							Object result = readMethod.invoke(bean);
							Assertions.assertNull(result, "Read different Object value than null for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");

							MethodUtils.invoke(writeMethod, bean, value);
							result = readMethod.invoke(bean);
							Assertions.assertSame(value, result, "Read different Object value than wrote for property '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.");

							try {
								Mockito.verifyNoMoreInteractions(value);
							}
							catch (Exception e) {
								throw new RuntimeException("Error verifying object interactions for '" + propertyDescriptor.getName() + "' on '" + beanClass + "'.", e);
							}
						}
					}
				}
			}
		}
	}


	/**
	 * Validates {@link BaseSearchForm} parameters within service methods.
	 * <p>
	 * Since these parameters are automatically instantiated by the system, it is necessary to verify that the parameter types refer to a concrete class with an
	 * accessible nullary constructor. This check is performed by this method.
	 */
	@Test
	public void testServiceValidSearchForms() {
		// Get service classes
		Collection<Class<?>> serviceClasses = getProjectClassFiles(Object.class).stream()
				.filter(clazz -> AnnotationUtils.isAnnotationPresent(clazz, Service.class))
				.collect(Collectors.toList());

		// Validate search form parameters
		MultiValueMap<String, String> invalidSearchFormParameters = new MultiValueHashMap<>(true);
		for (Class<?> serviceClass : serviceClasses) {
			// Get all search form service methods
			Method[] serviceClassMethods = serviceClass.getDeclaredMethods();
			List<Method> exposedServiceMethods = ArrayUtils.getStream(serviceClassMethods)
					.filter(method -> Modifier.isPublic(method.getModifiers()))
					.filter(method -> !AnnotationUtils.isAnnotationPresent(method, DoNotAddRequestMapping.class))
					.collect(Collectors.toList());

			// Verify that all search form parameters are instantiable
			for (Method method : exposedServiceMethods) {
				for (Class<?> parameterType : method.getParameterTypes()) {
					if (BaseSearchForm.class.isAssignableFrom(parameterType)) {
						try {
							parameterType.newInstance();
						}
						catch (Exception e) {
							// Failed to instantiate search form
							invalidSearchFormParameters.put(serviceClass.getName() + '#' + method.getName(), parameterType.getName());
						}
					}
				}
			}
		}

		// Report invalid search form parameters
		if (!invalidSearchFormParameters.isEmpty()) {
			StringBuilder errorMessageSb = new StringBuilder("Errors were found while validating search form usages.");
			StringJoiner invalidParameterJoiner = new StringJoiner(StringUtils.NEW_LINE + StringUtils.TAB, StringUtils.TAB, StringUtils.EMPTY_STRING);
			for (String classAndMethodName : invalidSearchFormParameters.keySet().stream().sorted().collect(Collectors.toList())) {
				for (String parameterClassName : invalidSearchFormParameters.get(classAndMethodName)) {
					invalidParameterJoiner.add(String.format("%s: Cannot instantiate search form [%s]", classAndMethodName, parameterClassName));
				}
			}
			errorMessageSb.append(StringUtils.NEW_LINE).append(invalidParameterJoiner);
			Assertions.fail(errorMessageSb.toString());
		}
	}


	/**
	 * Currently only Validates there is no parameter within service methods that are exposed via URL that use Serializable type.  Binding cannot support this.
	 * If additional types are necessary for checking, can be added here.  In general, cannot check for interface or abstract classes as in those cases binding can use
	 * class specific overrides to define how to bind.
	 * <p>
	 */
	@Test
	public void testInvalidServicePropertyType() {
		// Get service classes
		Collection<Class<?>> serviceClasses = getProjectClassFiles(Object.class).stream()
				.filter(clazz -> AnnotationUtils.isAnnotationPresent(clazz, Service.class))
				.collect(Collectors.toList());

		// Validate invalid parameters
		MultiValueMap<String, String> invalidParameters = new MultiValueHashMap<>(true);
		for (Class<?> serviceClass : serviceClasses) {
			// Get all search form service methods
			Method[] serviceClassMethods = serviceClass.getDeclaredMethods();
			List<Method> exposedServiceMethods = ArrayUtils.getStream(serviceClassMethods)
					.filter(method -> Modifier.isPublic(method.getModifiers()))
					.filter(method -> {
						Method interfaceMethod = MethodUtils.getInterfaceMethod(method);
						return !(interfaceMethod != null && AnnotationUtils.isAnnotationPresent(interfaceMethod, DoNotAddRequestMapping.class));
					})
					.collect(Collectors.toList());

			// Verify that all search form parameters are instantiable
			for (Method method : exposedServiceMethods) {
				for (Class<?> parameterType : method.getParameterTypes()) {
					if (Serializable.class.equals(parameterType)) {
						// Failed to instantiate search form
						invalidParameters.put(serviceClass.getName() + '#' + method.getName(), parameterType.getName());
					}
				}
			}
		}

		// Report invalid parameters
		if (!invalidParameters.isEmpty()) {
			StringBuilder errorMessageSb = new StringBuilder("Errors were found while validating parameters.");
			StringJoiner invalidParameterJoiner = new StringJoiner(StringUtils.NEW_LINE + StringUtils.TAB, StringUtils.TAB, StringUtils.EMPTY_STRING);
			for (String classAndMethodName : invalidParameters.keySet().stream().sorted().collect(Collectors.toList())) {
				for (String parameterClassName : invalidParameters.get(classAndMethodName)) {
					invalidParameterJoiner.add(String.format("%s: Cannot instantiate parameter [%s]", classAndMethodName, parameterClassName));
				}
			}
			errorMessageSb.append(StringUtils.NEW_LINE).append(invalidParameterJoiner);
			Assertions.fail(errorMessageSb.toString());
		}
	}


	@SuppressWarnings("rawtypes")
	@Test
	public void testBasicServiceMethods() throws Exception {
		List<String> invalidServiceMethodsList = new ArrayList<>();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			Object service = stringObjectEntry.getValue();

			// loop through service methods to find DAO's based on getter method pattern
			for (Method method : service.getClass().getMethods()) {
				String methodName = method.getName();
				if (methodName.startsWith("get") && methodName.endsWith("DAO")) {
					// found DAO - test basic methods
					String dtoName = StringUtils.capitalize(methodName.substring(3, methodName.length() - 3));
					ReadOnlyDAO<IdentityObject> dao = this.daoLocator.locate(dtoName);
					// If the DAO is directly tied to an abstract class don't continue with basic DAO tests.
					if (dao.getConfiguration() == null || Modifier.isAbstract(dao.getConfiguration().getBeanClass().getModifiers())) {
						continue;
					}

					IdentityObject bean = dao.newBean();

					// 1. test find by primary key
					Number primaryKey = 1;
					Method methodFindByPK = MethodUtils.getMethod(service.getClass(), "get" + dtoName, int.class);
					if (methodFindByPK == null) {
						//look for an implementation using short
						methodFindByPK = MethodUtils.getMethod(service.getClass(), "get" + dtoName, short.class);
						primaryKey = methodFindByPK != null ? (short) 1 : primaryKey;
					}
					if (methodFindByPK == null) {
						//look for an implementation using long
						methodFindByPK = MethodUtils.getMethod(service.getClass(), "get" + dtoName, long.class);
						primaryKey = methodFindByPK != null ? (long) 1 : primaryKey;
					}
					if (methodFindByPK != null && !isMethodSkipped(methodFindByPK)) {
						Object result = methodFindByPK.invoke(service, primaryKey);
						if (result != null) {
							invalidServiceMethodsList.add(String.format("%s.%s: Call to find primary key (1) must return null.", stringObjectEntry.getKey(), methodFindByPK.getName()));
						}
						if (!BeanUtils.getPropertyType(bean, "id").equals(primaryKey.getClass())) {
							invalidServiceMethodsList.add(String.format("%s: %s bean identity data type must be the same as the one on service method.", methodFindByPK, dtoName));
						}
					}

					// 2. test find all
					Method methodFindAll = MethodUtils.getMethod(service.getClass(), "get" + dtoName + "List");
					if (methodFindAll != null && !isMethodSkipped(methodFindAll)) {
						Object result = methodFindAll.invoke(service);
						if (result instanceof Collection) {
							if (!CollectionUtils.isEmpty((Collection) result)) {
								invalidServiceMethodsList.add(String.format("%s.%s: Expected empty collection.", stringObjectEntry.getKey(), methodFindAll.getName()));
							}
						}
						else if (result != null) {
							invalidServiceMethodsList.add(String.format("%s.%s: Must return null.", stringObjectEntry.getKey(), methodFindAll.getName()));
						}
					}

					// 3. test find by <? extends BaseSortableSearchForm>
					Method methodFindBySearchForm = MethodUtils.getMethodByAssignableFromParameters(service.getClass(), "get" + dtoName + "List", BaseSortableSearchForm.class);
					if (methodFindBySearchForm != null && !isMethodSkipped(methodFindBySearchForm)) {
						// Validate that instantiation is possible
						BaseSortableSearchForm searchForm = (BaseSortableSearchForm) BeanUtils.newInstance(methodFindBySearchForm.getParameterTypes()[0]);

						// Validate null results when restrictions are passed such that no results should be returned
						if (!searchForm.isFilterRequired() || searchForm instanceof BaseAuditableEntitySearchForm) {
							if (searchForm instanceof BaseAuditableEntitySearchForm) {
								((BaseAuditableEntitySearchForm) searchForm).addSearchRestriction(new SearchRestriction("createUserId", ComparisonConditions.EQUALS, 777));
							}
							else {
								// attempt to set id (if present) to avoid validation errors
								BeanUtils.setPropertyValue(searchForm, "id", 1, true);
							}
							try {
								Object result = methodFindBySearchForm.invoke(service, searchForm);
								if (result instanceof Collection) {
									if (!CollectionUtils.isEmpty((Collection) result)) {
										invalidServiceMethodsList.add(String.format("%s.%s: Expected empty collection.", stringObjectEntry.getKey(), methodFindBySearchForm.getName()));
									}
								}
								else if (result != null) {
									invalidServiceMethodsList.add(String.format("%s.%s: Must return null.", stringObjectEntry.getKey(), methodFindBySearchForm.getName()));
								}
							}
							catch (Exception e) {
								String stackTrace = ExceptionUtils.getFullStackTrace(e);
								if (stackTrace != null) {
									stackTrace = stackTrace.replaceAll("\n", "\n\t");
								}
								invalidServiceMethodsList.add(String.format("%s.%s: Invocation error: %s", stringObjectEntry.getKey(), methodFindBySearchForm.getName(), stackTrace));
							}
						}
					}

					// 3.1. test find by name
					Method methodFindByName = MethodUtils.getMethodByAssignableFromParameters(service.getClass(), "get" + dtoName + "ByName", String.class);
					if (methodFindByName != null && !isMethodSkipped(methodFindByName)) {
						Object result = null;
						try {
							result = methodFindByName.invoke(service, TestUtils.DEFAULT_STRING_VALUE);
						}
						catch (InvocationTargetException e) {
							// "strict" implementation will throw an exception if not found: could use @Contract(_ -> !null) annotation for strict methods but it's not runtime
							String expectedMessage = "Unable to find " + StringUtils.splitWords(dtoName) + " with properties [name] and value(s) [TEST_VALUE]";
							String actualMessage = e.getTargetException().getMessage();
							if (!expectedMessage.equals(actualMessage)) {
								invalidServiceMethodsList.add(String.format("%s.%s: Must throw a ValidationException with message '%s' instead of '" + expectedMessage + "'", stringObjectEntry.getKey(), methodFindByName.getName(), actualMessage));
							}
						}
						if (result != null) {
							invalidServiceMethodsList.add(String.format("%s.%s: Must return null instead of %s", stringObjectEntry.getKey(), methodFindByName.getName(), result));
						}
					}

					// 4. test save bean
					// Unregister all NON cache by name observers, and keeps track of them so we can re-register them after test is completed
					Map<DaoEventTypes, Set<DaoEventObserver>> disabledObserverMap = getAndDisableNonCacheByNameObservers(((ObserverableDAO) dao));
					Method methodSave = MethodUtils.getMethod(service.getClass(), "save" + dtoName, bean.getClass());
					boolean methodSaveFailed = false;
					boolean populateDefaultPropertyValues = false;
					boolean updatedAfterInsert = false;
					if (methodSave != null && !isMethodSkipped(methodSave)) {
						if (bean.getIdentity() != null) {
							invalidServiceMethodsList.add(String.format("%s.%s: Identity must be null for empty bean: %s", stringObjectEntry.getKey(), methodSave.getName(), bean.getClass()));
						}
						try {
							if (BeanUtils.isPropertyPresent(bean.getClass(), "name")) {
								// Populate the name property for ByName method lookups. Populating other properties may result in validation exceptions.
								BeanUtils.setPropertyValue(bean, "name", TestUtils.DEFAULT_STRING_VALUE);
							}
							methodSave.invoke(service, bean);
						}
						catch (Exception e) {
							populateDefaultPropertyValues = true;
							TestUtils.populateDefaultPropertyValues(bean, true);
							try {
								methodSave.invoke(service, bean);
							}
							catch (Exception e2) {
								String stackTrace = ExceptionUtils.getFullStackTrace(e2);
								if (stackTrace != null) {
									stackTrace = stackTrace.replaceAll("\n", "\n\t");
								}
								invalidServiceMethodsList.add(String.format("%s.%s: Invocation error: %s", stringObjectEntry.getKey(), methodSave.getName(), stackTrace));
								methodSaveFailed = true;
							}
						}
						if (bean.getIdentity() == null && !methodSaveFailed) {
							invalidServiceMethodsList.add(String.format("%s.%s: Identity cannot be null for created bean: %s", stringObjectEntry.getKey(), methodSave.getName(), bean.getClass()));
							methodSaveFailed = true;
						}
						else if (methodFindByPK != null && !isMethodSkipped(methodFindByPK) && !methodSaveFailed) {
							// 4.1 now make sure the bean is there
							Object result = methodFindByPK.invoke(service, bean.getIdentity());
							if (result == null) {
								invalidServiceMethodsList.add(String.format("%s.%s: Call to find primary key (%s) cannot return null.", stringObjectEntry.getKey(), methodFindByPK.getName(), bean.getIdentity()));
								methodSaveFailed = true;
							}
							else if (BeanUtils.isPropertyPresent(result.getClass(), "name")) {
								// now test update vs insert
								BeanUtils.setPropertyValue(result, "name", TestUtils.DEFAULT_STRING_VALUE_2);
								if (populateDefaultPropertyValues) {
									TestUtils.populateDefaultPropertyValues(result, true);
								}
								try {
									methodSave.invoke(service, result);
									updatedAfterInsert = true;
								}
								catch (Exception e) {
									if (!CoreExceptionUtils.isValidationException(e)) {
										// ignore validation exceptions: some entities do not allow updates
										throw e;
									}
								}
							}
						}
						if (methodFindByName != null && !isMethodSkipped(methodFindByName) && !methodSaveFailed) {
							// now that the first object was created: it should be returned by name
							Object result = methodFindByName.invoke(service, updatedAfterInsert ? TestUtils.DEFAULT_STRING_VALUE_2 : TestUtils.DEFAULT_STRING_VALUE);
							if (result == null) {
								invalidServiceMethodsList.add(String.format("%s.%s: returned null instead of expected object.", stringObjectEntry.getKey(), methodFindByName.getName()));
							}
						}
					}
					// 5. test delete bean
					primaryKey = 1;
					Method methodDelete = MethodUtils.getMethod(service.getClass(), "delete" + dtoName, int.class);
					if (methodDelete == null) {
						//look for an implementation using short
						methodDelete = MethodUtils.getMethod(service.getClass(), "delete" + dtoName, short.class);
						primaryKey = methodDelete != null ? (short) 1 : primaryKey;
					}
					if (methodDelete == null) {
						//look for an implementation using long
						methodDelete = MethodUtils.getMethod(service.getClass(), "delete" + dtoName, long.class);
						primaryKey = methodDelete != null ? (long) 1 : primaryKey;
					}
					if (methodDelete != null && !isMethodSkipped(methodDelete)) {
						if (!BeanUtils.getPropertyType(bean, "id").equals(primaryKey.getClass())) {
							invalidServiceMethodsList.add(String.format("%s: %s bean identity data type must be the same as the one on service method.", methodDelete, dtoName));
						}
						if (methodSave != null && !isMethodSkipped(methodSave) && !methodSaveFailed) {
							// a row was inserted by save - try to delete it
							try {
								methodDelete.invoke(service, primaryKey);
								if (methodFindByPK != null && !isMethodSkipped(methodFindByPK)) {
									// 5.1 now make sure the bean is no longer there
									Object result = methodFindByPK.invoke(service, bean.getIdentity());
									if (result != null) {
										invalidServiceMethodsList.add(String.format("%s.%s: Call to find primary key (1) must return null.", stringObjectEntry.getKey(), methodFindByPK.getName()));
									}
								}
							}
							catch (Exception e) {
								String stackTrace = ExceptionUtils.getFullStackTrace(e);
								if (stackTrace != null) {
									stackTrace = stackTrace.replaceAll("\n", "\n\t");
								}
								invalidServiceMethodsList.add(String.format("%s.%s: Invocation error: %s", stringObjectEntry.getKey(), methodDelete.getName(), stackTrace));
							}
						}
						else {
							// there was no row inserted by save - should get an error
							try {
								methodDelete.invoke(service, primaryKey);
								invalidServiceMethodsList.add(String.format("%s: Deleting a row that doesn't exist should error out but it didn't.", methodDelete));
							}
							catch (Exception e) {
								// could be one of the following two messages
								String message = ExceptionUtils.getOriginalMessage(e);
								if (message == null) {
									message = methodDelete + " : " + e.getMessage();
								}
								if (!"Cannot find bean to delete with primary key: 1".equals(message) && !"Bean to delete cannot be null.".equals(message)) {
									invalidServiceMethodsList.add(String.format("%s.%s: Expected 'Bean to delete cannot be null.' Message was: %s", stringObjectEntry.getKey(), methodDelete.getName(), message));
								}
							}
						}
					}
					enableDisabledObservers(((ObserverableDAO) dao), disabledObserverMap);
				}
			}
		}
		if (!CollectionUtils.isEmpty(invalidServiceMethodsList)) {
			String message = invalidServiceMethodsList.stream().collect(Collectors.joining("\n\n-", "Service method errors were encountered:\n\n-", ""));
			Assertions.fail(message);
		}
	}


	/**
	 * This method unregisters all NON cache by name observers, and keeps track of them so we can re-register them after test is completed
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	protected Map<DaoEventTypes, Set<DaoEventObserver>> getAndDisableNonCacheByNameObservers(ObserverableDAO dao) {
		Map<DaoEventTypes, Set<DaoEventObserver>> disableObserversMap = new HashMap<>();
		disableObserversMap.put(DaoEventTypes.INSERT, getAndDisableNonCacheByNameObserversForEventType(dao, DaoEventTypes.INSERT, dao.getInsertObserversRegistered()));
		disableObserversMap.put(DaoEventTypes.UPDATE, getAndDisableNonCacheByNameObserversForEventType(dao, DaoEventTypes.UPDATE, dao.getUpdateObserversRegistered()));
		disableObserversMap.put(DaoEventTypes.DELETE, getAndDisableNonCacheByNameObserversForEventType(dao, DaoEventTypes.DELETE, dao.getDeleteObserversRegistered()));
		return disableObserversMap;
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private Set<DaoEventObserver> getAndDisableNonCacheByNameObserversForEventType(ObserverableDAO dao, DaoEventTypes eventType, List<Observer> observerList) {
		Set<DaoEventObserver> disabledObserverSet = new HashSet<>();
		for (Observer observer : CollectionUtils.getIterable(observerList)) {
			if (observer instanceof DaoEventObserver && !(observer instanceof DaoNamedEntityCache)) {
				disabledObserverSet.add((DaoEventObserver) observer);
				dao.unregisterEventObserver(eventType, (DaoEventObserver) observer);
			}
		}
		return disabledObserverSet;
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	protected void enableDisabledObservers(ObserverableDAO dao, Map<DaoEventTypes, Set<DaoEventObserver>> disabledObserversMap) {
		for (Map.Entry<DaoEventTypes, Set<DaoEventObserver>> mapEntry : disabledObserversMap.entrySet()) {
			for (DaoEventObserver eventObserver : CollectionUtils.getIterable(mapEntry.getValue())) {
				dao.registerEventObserver(mapEntry.getKey(), eventObserver);
			}
		}
	}


	@Test
	public void testServiceUserIgnorableValidationAnnotations() {
		StringBuilder errors = new StringBuilder();
		Map<String, Object> serviceBeans = getContextBeans(getProjectClassNamePrefix(), "Service");
		for (Map.Entry<String, Object> stringObjectEntry : serviceBeans.entrySet()) {
			Object service = stringObjectEntry.getValue();
			for (Method serviceMethod : service.getClass().getMethods()) {
				// public and non-abstract methods only
				if (Modifier.isPublic(serviceMethod.getModifiers()) && !Modifier.isAbstract(serviceMethod.getModifiers())) {
					// ignore Object methods
					if (MethodUtils.isMethodInClass(Object.class, serviceMethod)) {
						continue;
					}
					// Test for UserIgnorableValidation annotation
					if (AnnotationUtils.isAnnotationPresent(serviceMethod, UserIgnorableValidation.class)) {
						if (AnnotationUtils.isAnnotationPresent(serviceMethod, DoNotAddRequestMapping.class)) {
							errors.append("Service method [").append(serviceMethod.getName()).append("] in class [")
									.append(service.getClass().getSimpleName()).append("] is annotated with ")
									.append(UserIgnorableValidation.class.getSimpleName()).append(" and also ")
									.append(DoNotAddRequestMapping.class.getSimpleName()).append(".\n");
						}
						else {
							boolean found = false;
							for (Parameter parameter : serviceMethod.getParameters()) {
								if (UserIgnorableValidationAspect.IGNORE_VALIDATION_URL_KEY.equals(parameter.getName())
										&& (boolean.class.isAssignableFrom(parameter.getType()) || Boolean.class.isAssignableFrom(parameter.getType()))) {
									found = true;
								}
							}
							if (!found) {
								errors.append("Service method [").append(serviceMethod.getName()).append("] in class [")
										.append(service.getClass().getSimpleName()).append("] is annotated with ")
										.append(UserIgnorableValidation.class.getSimpleName()).append(" and is missing the ")
										.append(UserIgnorableValidationAspect.IGNORE_VALIDATION_URL_KEY).append(" boolean argument.\n");
							}
						}
					}
				}
			}
		}
		AssertUtils.assertTrue(errors.length() == 0, errors.toString());
	}


	@SuppressWarnings("UnusedReturnValue")
	protected Set<String> configureIgnoreCaches(Set<String> ignoreCaches) {
		return ignoreCaches;
	}


	/**
	 * For each cache that caches by "id" - we use findByPrimaryKey on the dao to get the actual
	 * object.  Any cache that utilizes this feature should have hibernate cache used
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Test
	public void testCustomCacheByIdUtilizesHibernateCaching() {
		Map<String, Object> cacheBeans = getContextBeans(getProjectClassNamePrefix(), "Cache");

		if (!CollectionUtils.isEmpty(cacheBeans)) {
			Set<String> ignoreCaches = new HashSet<>();
			configureIgnoreCaches(ignoreCaches);

			for (String cacheName : cacheBeans.keySet()) {
				Object cache = this.applicationContext.getBean(cacheName);
				if (cache instanceof SelfRegisteringDaoCache<?> || cache instanceof SelfRegisteringDaoListCache<?>) {
					// Figure out the DAO Bean
					Class dtoClass = DaoUtils.getDtoClass((SelfRegisteringDaoObserver<?>) cache);
					ReadOnlyDAO<?> dao = this.daoLocator.locate(dtoClass);
					CacheTypes cacheType = dao.getConfiguration().getTable().getCacheType();
					if (ignoreCaches.contains(cacheName)) {
						Assertions.assertTrue(
								cacheType == null || CacheTypes.NONE == cacheType,
								"Cache [" + cacheName + "] utilizes ID look ups, and hibernate caching is enabled on the table.  Please remove the cache from ignore override.");
					}
					else {
						Assertions.assertTrue(
								cacheType != null && CacheTypes.NONE != cacheType,
								"Cache [" + cacheName + "] utilizes ID look ups, and hibernate caching is not enabled on the table.  Please enable hibernate caching on the table xml definition.");
					}
				}
			}
		}
	}


	/**
	 * Rare - but some caches for entities taht use subclasses can only apply to a specific subclass
	 * use this to override (see compliance tests for an example)
	 */
	protected Map<String, IdentityObject> configureCacheDtoInstanceOverrideMap() {
		return new HashMap<>();
	}


	/**
	 * For each cache that extends are SelfRegistering....
	 */
	@SuppressWarnings({"rawtypes", "unchecked"})
	@Test
	public void testCacheGetBeanPropertyAndValueMethods() {
		Map<String, Object> cacheBeans = getContextBeans(getProjectClassNamePrefix(), "Cache");
		Map<String, IdentityObject> cacheDtoInstanceOverrideMap = configureCacheDtoInstanceOverrideMap();
		if (!CollectionUtils.isEmpty(cacheBeans)) {
			StringBuilder errors = new StringBuilder("");
			for (String cacheName : cacheBeans.keySet()) {
				Object cache = this.applicationContext.getBean(cacheName);
				if (!(cache instanceof SelfRegisteringDaoObserver)) {
					continue;
				}
				if (cacheDtoInstanceOverrideMap.containsKey(cacheName)) {
					testCacheGetBeanPropertyAndValueMethodsForBean(cacheDtoInstanceOverrideMap.get(cacheName), cache, cacheName, errors);
				}
				else {
					Class dtoClass = DaoUtils.getDtoClass((SelfRegisteringDaoObserver<?>) cache);
					ReadOnlyDAO<?> dao = this.daoLocator.locate(dtoClass);
					if (!CollectionUtils.isEmpty(dao.getConfiguration().getTable().getSubclassList())) {
						for (Subclass subclass : CollectionUtils.getIterable(dao.getConfiguration().getTable().getSubclassList())) {
							Class<IdentityObject> beanClass = (Class<IdentityObject>) CoreClassUtils.getClass(subclass.getDtoClass());
							IdentityObject bean = BeanUtils.newInstance(beanClass);
							testCacheGetBeanPropertyAndValueMethodsForBean(bean, cache, cacheName, errors);
						}
					}
					else {
						IdentityObject bean = dao.newBean();
						testCacheGetBeanPropertyAndValueMethodsForBean(bean, cache, cacheName, errors);
					}
				}
			}
			if (errors.length() > 0) {
				Assertions.fail("Error Evaluating Cache Getter Methods: " + errors);
			}
		}
	}


	private void testCacheGetBeanPropertyAndValueMethodsForBean(IdentityObject bean, Object cache, String cacheName, StringBuilder errors) {
		try {
			if ((cache instanceof SelfRegisteringSingleKeyDaoCache || cache instanceof SelfRegisteringSingleKeyDaoListCache) && !(cache instanceof NamedEntityFilterableCache)) {
				testCacheGetBeanPropertyAndValueMethodsForBeanImpl(bean, cache, cacheName, "getBeanKeyProperty", "getBeanKeyValue", errors);
			}
			else if (cache instanceof SelfRegisteringCompositeKeyDaoCache || cache instanceof SelfRegisteringCompositeKeyDaoListCache || cache instanceof SelfRegisteringCompositeKeyFlexibleDaoCache) {
				testCacheGetBeanPropertyAndValueMethodsForBeanImpl(bean, cache, cacheName, "getBeanKey1Property", "getBeanKey1Value", errors);
				testCacheGetBeanPropertyAndValueMethodsForBeanImpl(bean, cache, cacheName, "getBeanKey2Property", "getBeanKey2Value", errors);
			}
			else if (cache instanceof SelfRegisteringThreeKeyDaoCache || cache instanceof SelfRegisteringThreeKeyDaoListCache) {
				testCacheGetBeanPropertyAndValueMethodsForBeanImpl(bean, cache, cacheName, "getBeanKey1Property", "getBeanKey1Value", errors);
				testCacheGetBeanPropertyAndValueMethodsForBeanImpl(bean, cache, cacheName, "getBeanKey2Property", "getBeanKey2Value", errors);
				testCacheGetBeanPropertyAndValueMethodsForBeanImpl(bean, cache, cacheName, "getBeanKey3Property", "getBeanKey3Value", errors);
			}
		}
		catch (Exception e) {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": ").append(ExceptionUtils.getDetailedMessage(e));
		}
	}


	private void testCacheGetBeanPropertyAndValueMethodsForBeanImpl(IdentityObject bean, Object cache, String cacheName, String propertyMethodName, String propertyValueMethodName, StringBuilder errors) {
		Method getBeanKeyPropertyMethod = MethodUtils.getMethod(cache.getClass(), propertyMethodName, true);
		if (getBeanKeyPropertyMethod == null) {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Missing " + propertyMethodName + " method (?)");
			return;
		}

		String propertyName = (String) MethodUtils.invoke(getBeanKeyPropertyMethod, cache);
		if (StringUtils.isEmpty(propertyName)) {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": " + propertyMethodName + " method returned an empty string.");
			return;
		}

		Method valueMethod = null;
		for (Method method : MethodUtils.getAllDeclaredMethods(cache.getClass())) {
			if (StringUtils.isEqual(propertyValueMethodName, method.getName())) {
				if (!StringUtils.startsWith(MethodUtils.getMethodParametersAsString(method, true), "IdentityObject")) {
					valueMethod = method;
					valueMethod.setAccessible(true);
					break;
				}
			}
		}

		if (valueMethod == null) {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Unable to find a valid " + propertyValueMethodName + " method.");
			return;
		}

		// Passing in a null does not work, so we'll try an empty bean and a bean with the value set
		Class<?> valueType = valueMethod.getReturnType();
		try {
			Object result = MethodUtils.invoke(valueMethod, cache, bean);
			if (boolean.class.equals(valueType) || Boolean.class.equals(valueType)) {
				if (BooleanUtils.isTrue(result)) {
					errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Expected false value returned for " + propertyValueMethodName + " on new bean for boolean type.");
				}
			}
			else if (result != null && !result.getClass().isPrimitive()) {
				// Primitives can't be null, so just leave it as OK
				if (Number.class.isAssignableFrom(valueType)) {
					if (!MathUtils.isNullOrZero((Number) result)) {
						errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Expected null or zero value returned for " + propertyValueMethodName + " on new bean, but received " + result + ".");
					}
				}
				else {
					errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Expected null value returned for " + propertyValueMethodName + " on new bean, but received " + result + ".");
				}
			}
		}
		catch (Exception e) {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Error encountered when calling " + propertyValueMethodName + " method on an empty bean (check for NPE) - " + ExceptionUtils.getDetailedMessage(e));
		}

		Object testPropertyValue = null;
		if (String.class.equals(valueType)) {
			testPropertyValue = "Test String";
		}
		else if (Boolean.class.equals(valueType) || boolean.class.equals(valueType)) {
			testPropertyValue = true;
		}
		else if (Long.class.isAssignableFrom(valueType)) {
			testPropertyValue = MathUtils.getNumberAsLong(50);
		}
		else if (Integer.class.isAssignableFrom(valueType)) {
			testPropertyValue = 15;
		}
		else if (Short.class.isAssignableFrom(valueType)) {
			testPropertyValue = MathUtils.getNumberAsShort(5);
		}
		else if (valueType.isEnum()) {
			testPropertyValue = valueType.getEnumConstants()[0];
		}
		else {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Test incomplete.  Please Add a default value for  - " + valueMethod.getReturnType());
		}

		try {
			BeanUtils.setPropertyValue(bean, propertyName, testPropertyValue);
		}
		catch (Exception e) {
			// abstract class - just ignore for now - not sure how to automate this
			return;
		}
		try {
			Object result = MethodUtils.invoke(valueMethod, cache, bean);
			if (!CompareUtils.isEqual(testPropertyValue, result)) {
				errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Expected " + testPropertyValue + " returned for " + propertyValueMethodName + " on bean with property set.");
			}
		}
		catch (Exception e) {
			errors.append(StringUtils.NEW_LINE).append(cacheName).append(": Error encountered when calling " + propertyValueMethodName + " method on a bean with test value " + testPropertyValue + " - " + ExceptionUtils.getOriginalMessage(e));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected boolean isRowVersionRequired() {
		return false;
	}


	/**
	 * Confirms (if turned on) that all tables that support versioning have it turned on.  This test is to prevent new tables from being added
	 * to a project that have already all be changed to use versioning so snapshot isolation can be turned on on a database
	 */
	@Test
	public void testRowVersioningEnabledForAllApplicableTables() {
		// Would be better to disable or enable if than this check
		if (isRowVersionRequired()) {
			TableSearchForm searchForm = new TableSearchForm();
			searchForm.setVersionSupported(true);
			searchForm.setVersion(false);
			searchForm.setName(StringUtils.removeAll(getProjectPrefix(), "-"));

			List<Table> tableList = this.migrationSchemaService.getTableList(searchForm);
			Assertions.assertEquals(0, CollectionUtils.getSize(tableList), "Found [" + CollectionUtils.getSize(tableList) + "] tables that should, but do not have versioning enabled: " + StringUtils.collectionToCommaDelimitedString(tableList, Table::getName));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRequiredApplicationContextBeansLoaded() {
		Map<String, Object> applicationContextBeanMap = new HashMap<>();
		configureRequiredApplicationContextBeans(applicationContextBeanMap);
		for (String beanName : applicationContextBeanMap.keySet()) {
			Assertions.assertNotNull(applicationContextBeanMap.get(beanName), String.format("Application context was not initialized correctly. [%s] is null", beanName));
		}
	}


	protected void configureRequiredApplicationContextBeans(Map<String, Object> beanMap) {
		beanMap.put("validationService", this.validationService);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("rawtypes")
	@Test
	public void testCacheNames() {
		Map<String, String> cacheNameToClassNameMap = new HashMap<>();

		Map<String, CustomCache> cacheBeanMap = getApplicationContext().getBeansOfType(CustomCache.class);
		for (Map.Entry<String, CustomCache> stringCustomCacheEntry : cacheBeanMap.entrySet()) {
			CustomCache cacheBean = stringCustomCacheEntry.getValue();
			String thisClassName = cacheBean.getClass().getName();
			String existingClassName = cacheNameToClassNameMap.get(cacheBean.getCacheName());
			Assertions.assertTrue(StringUtils.isEmpty(existingClassName),
					"Cache Name [" + cacheBean.getCacheName() + "] is already used by class [" + existingClassName + "].  Cannot also be used by class [" + thisClassName + "].");
			cacheNameToClassNameMap.put(cacheBean.getCacheName(), thisClassName);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a Map of context managed bean name to corresponding bean mappings for names start with the specified prefix and end with the specified suffix.
	 * For example, use "Service" suffix to get services and "DAO" suffix to get data access objects.
	 */
	private Map<String, Object> getContextBeans(String beanNamePrefix, String beanNameSuffix) {
		Map<String, Object> result = new HashMap<>();
		String[] names = this.applicationContext.getBeanDefinitionNames();
		String prefix = beanNamePrefix.toLowerCase();
		for (String name : names) {
			if (name.toLowerCase().startsWith(prefix) && name.endsWith(beanNameSuffix)) {
				result.put(name, this.applicationContext.getBean(name));
			}
		}
		return result;
	}


	/**
	 * Gets the package name for this project.
	 *
	 * @return the package name for this project
	 */
	private String getProjectPackage() {
		return ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION + getProjectPrefix().toLowerCase().replace('-', '.');
	}


	private String getProjectClassNamePrefix() {
		int index = getProjectPrefix().indexOf('-');
		if (index > 0) {
			return getProjectPrefix().substring(0, index) + StringUtils.capitalize(getProjectPrefix().substring(index + 1));
		}
		return getProjectPrefix();
	}


	protected ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
