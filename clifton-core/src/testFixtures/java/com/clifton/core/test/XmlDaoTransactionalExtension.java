package com.clifton.core.test;

import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.xml.XmlDaoTransactionTestUtils;
import org.junit.jupiter.api.extension.AfterEachCallback;
import org.junit.jupiter.api.extension.BeforeEachCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * The <code>XmlDaoTransactionalExtension</code> can be used by any test class
 * that utilizes XML dao for testing data and want the test to run "transactionally" - i.e. all data updates after the test
 * completes wipes and goes back to what it was before the test started.  The methods run before and after each test METHOD in the class.
 * <p>
 * Used as an extension, i.e. @ExtendWith(SpringExtension.class, XmlDaoTransactionExtension.class)
 *
 * @author manderson
 */
public class XmlDaoTransactionalExtension implements BeforeEachCallback, AfterEachCallback {


	@Override
	public void beforeEach(ExtensionContext context) throws Exception {
		XmlDaoTransactionTestUtils.setupContextBeforeTest((ContextHandler) SpringExtension.getApplicationContext(context).getBean("contextHandler"));
	}


	@Override
	public void afterEach(ExtensionContext context) {
		ApplicationContext applicationContext = SpringExtension.getApplicationContext(context);
		XmlDaoTransactionTestUtils.cleanUpContextAfterTest((ContextHandler) SpringExtension.getApplicationContext(context).getBean("contextHandler"), applicationContext);
	}
}
