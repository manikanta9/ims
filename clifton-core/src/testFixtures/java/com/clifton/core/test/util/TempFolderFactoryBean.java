package com.clifton.core.test.util;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.FactoryBean;

import java.io.File;
import java.nio.file.Files;


/**
 * @author danielh
 */
public class TempFolderFactoryBean implements FactoryBean<File> {


	private String filePathName;
	private File temp;


	@Override
	public File getObject() throws Exception {

		this.temp = Files.createTempDirectory(this.filePathName).toFile();
		return this.temp;
	}


	@Override
	public Class<?> getObjectType() {
		return File.class;
	}


	public String getFilePathName() {
		return this.filePathName;
	}


	public void setFilePathName(String filePathName) {
		this.filePathName = filePathName;
	}


	public void destroy() {
		FileUtils.deleteQuietly(temp);
	}
}
