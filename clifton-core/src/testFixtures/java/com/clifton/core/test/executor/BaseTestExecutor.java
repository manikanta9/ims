package com.clifton.core.test.executor;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>BaseTestExecutor</code> provides basic functionality and structure for executing integration tests and verifying the results.
 *
 * @author michaelm
 */
public abstract class BaseTestExecutor<T> {

	protected String[] expectedResultStrings;
	protected Date startBalanceDate;
	protected Date endBalanceDate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseTestExecutor<T> withBalanceStartAndEndDates(String startBalanceDate, String endBalanceDate) {
		this.startBalanceDate = DateUtils.toDate(startBalanceDate);
		this.endBalanceDate = DateUtils.toDate(endBalanceDate);
		return this;
	}


	public BaseTestExecutor<T> withExpectedResults(String... expectedResultStrings) {
		this.expectedResultStrings = expectedResultStrings;
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Abstract method used for printing out the type of entity for execution results.
	 */
	protected abstract String getResultEntityTypeName();


	/**
	 * Abstract method that must be implemented to execute the test and return the results so they can be validated.
	 */
	protected abstract List<T> executeTest();


	/**
	 * Abstract method that should return an array of strings to match the fields in the {@link #expectedResultStrings}.
	 */
	protected abstract String[] getStringValuesForResultEntity(T resultEntity);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Verifies that the {@link #expectedResultStrings} match the actual results after execution.
	 * If an assertion fails, that result is printed prior to throwing the assertion error.
	 * Returns the results that were validated upon assertion.
	 */
	public List<T> execute() {
		return execute(false);
	}


	/**
	 * Verifies that the {@link #expectedResultStrings} match the actual results after execution.
	 * If an assertion fails, that result is printed prior to throwing the assertion error.
	 * Prints all results whether an assertion failure occurs or not.
	 * Returns the results that were validated upon assertion.
	 */
	public List<T> executeAndPrintResults() {
		return execute(true);
	}


	/**
	 * @see #execute(boolean)
	 * @see #executeAndPrintResults()
	 */
	protected List<T> execute(boolean printResults) {
		return validateResultList(executeTest(), printResults);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected List<T> validateResultList(List<T> resultEntityList, boolean printResults) {
		int expectedCount = ArrayUtils.getLength(this.expectedResultStrings);
		int actualCount = CollectionUtils.getSize(resultEntityList);
		if (expectedCount != actualCount) {
			failValidationWithMessage(resultEntityList, String.format("%s count mismatch. Expected: %d, Actual: %d", getResultEntityTypeName(), expectedCount, actualCount));
		}

		List<String> failedResultStringList = new ArrayList<>();
		for (int i = 0; i < ArrayUtils.getLength(this.expectedResultStrings); i++) {
			String[] expectedValueStrings = expectedResultStrings[i].split("     ");
			String[] actualValueStrings = getStringValuesForResultEntity(resultEntityList.get(i));
			if (printResults) {
				System.out.print("\n");
				System.out.println(joinResultStrings(actualValueStrings));
				System.out.print("\n");
			}
			if (!isExpectedMatchesActual(expectedValueStrings, actualValueStrings)) {
				failedResultStringList.add(String.format("%s %d:\n\tExpect: %s\n\tActual: %s", getResultEntityTypeName(), i + 1, joinResultStrings(expectedValueStrings), joinResultStrings(actualValueStrings)));
			}
		}
		if (!CollectionUtils.isEmpty(failedResultStringList)) {
			failValidationWithMessage(resultEntityList, String.format("%s validation failed! Records with mismatch include:\n%s", getResultEntityTypeName(), String.join("\n", failedResultStringList)));
		}
		return resultEntityList;
	}


	protected void failValidationWithMessage(List<T> resultEntityList, String message) {
		System.out.println("All Results upon error:\n" + String.join("\n",
				CollectionUtils.getStream(resultEntityList).map(resultEntity -> joinResultStrings(getStringValuesForResultEntity(resultEntity))).collect(Collectors.toList())) + "\n");
		Assertions.fail(message);
	}


	protected static boolean isExpectedMatchesActual(String[] expectedValueStrings, String[] actualValueStrings) {
		for (int i = 0; i < expectedValueStrings.length; i++) {
			if (!expectedValueStrings[i].equals(actualValueStrings[i])) {
				return false;
			}
		}
		return true;
	}


	protected static String joinResultStrings(String[] resultsStringArray) {
		return String.join("     ", resultsStringArray);
	}
}
