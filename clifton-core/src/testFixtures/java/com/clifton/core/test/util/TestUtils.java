package com.clifton.core.test.util;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.SimpleEntity;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.search.form.BaseSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.function.Executable;
import org.junit.platform.commons.util.BlacklistedExceptions;

import java.beans.PropertyDescriptor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;


/**
 * Utility class for test execution.
 */
public class TestUtils {

	public static final String DEFAULT_STRING_VALUE = "TEST_VALUE";
	public static final String DEFAULT_STRING_VALUE_2 = "TEST_VALUE_2";


	private static final AtomicLong ID_COUNTER = new AtomicLong(1);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates properties of the specified bean with default values to help with automatic testing.
	 */
	public static void populateDefaultPropertyValues(Object bean, boolean populateIdentityObjects) throws InvocationTargetException, IllegalAccessException, InstantiationException {
		// save method may use nested bean properties for validation via configuration attributes: try to hydrate them with empty objects before retrying
		for (PropertyDescriptor propertyDescriptor : BeanUtils.getPropertyDescriptors(bean.getClass())) {
			Method writeMethod = propertyDescriptor.getWriteMethod();
			if (writeMethod != null) {
				if (populateIdentityObjects && IdentityObject.class.isAssignableFrom(propertyDescriptor.getPropertyType()) && !Modifier.isAbstract(propertyDescriptor.getPropertyType().getModifiers())) {
					Object dependentObject = propertyDescriptor.getPropertyType().newInstance();
					populateDefaultPropertyValues(dependentObject, false); // don't do recursively: one level deep
					writeMethod.invoke(bean, dependentObject);
				}
				else if (propertyDescriptor.getPropertyType().isEnum()) {
					Object[] enumArray = propertyDescriptor.getPropertyType().getEnumConstants();
					if (enumArray != null && enumArray.length > 0) {
						writeMethod.invoke(bean, enumArray[0]);
					}
				}
				else if (String.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
					writeMethod.invoke(bean, DEFAULT_STRING_VALUE);
				}
				else if (Date.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
					Date value = new Date();
					if (propertyDescriptor.getName().toLowerCase().contains("end")) {
						// add an extra day to "endDate" properties to avoid validation error with endDate not being after startDate
						value = DateUtils.addDays(value, 1);
					}
					writeMethod.invoke(bean, value);
				}
				else if (BigDecimal.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
					writeMethod.invoke(bean, new BigDecimal("1"));
				}
				else if (!"id".equals(propertyDescriptor.getName()) && Long.class.isAssignableFrom(propertyDescriptor.getPropertyType())) {
					writeMethod.invoke(bean, 1L);
				}
			}
		}
	}


	/**
	 * Gets the DTO class associated with a given search form. If a DTO class is not explicitly designated via the {@link SearchForm} annotation, the current
	 * project package will be searched for a valid DTO class based on the search form class name.
	 *
	 * @param searchFormClass the search form for which to find a corresponding DTO class
	 * @return the corresponding DTO class
	 */
	public static Class<? extends IdentityObject> getDtoClassFromSearchForm(Class<? extends BaseSearchForm> searchFormClass, Map<String, Class<?>> projectClassesByName) {
		// Find the search form class which will most closely relate to the DTO (by name or annotation)
		Class<? extends BaseSearchForm> searchFormClassForDto = searchFormClass;
		while (!searchFormClassForDto.getSimpleName().endsWith("SearchForm")
				&& AnnotationUtils.getAnnotation(searchFormClassForDto, SearchForm.class).ormDtoClass() == IdentityObject.class) {
			boolean noSearchFormClassesRemaining = !BaseSearchForm.class.isAssignableFrom(searchFormClassForDto.getSuperclass());
			AssertUtils.assertFalse(noSearchFormClassesRemaining, "The search form [%s] is not suffixed with \"SearchForm\" and an explicit DTO class has not been assigned via the SearchForm annotation. Traversing super classes for a conforming class produced no results. The DTO class cannot be automatically determined.", searchFormClass.getName());

			@SuppressWarnings("unchecked")
			Class<? extends BaseSearchForm> searchFormSuperClass = (Class<? extends BaseSearchForm>) searchFormClassForDto.getSuperclass();
			searchFormClassForDto = searchFormSuperClass;
		}

		Class<?> dtoClass;
		Class<?> annotationClass = AnnotationUtils.getAnnotation(searchFormClass, SearchForm.class).ormDtoClass();
		if (annotationClass != IdentityObject.class) {
			// Trivial case: DTO specified by annotation
			dtoClass = annotationClass;
		}
		else {
			/*
			 * Search for DTO class by conventions.
			 *
			 * The following seek constraints are used in the order given until a valid DTO is found:
			 * - Location: Parent package. Name: Matches search form class name with "SearchForm" stripped.
			 * - Location: Parent package and all sub-packages. Name: Matches search form class name with "SearchForm" stripped.
			 * - Location: Parent package and all sub-packages. Name: Ends in search form class name with "SearchForm" stripped.
			 * - Location: Project package and all sub-packages. Name: Matches search form class name with "SearchForm" stripped.
			 * - Location: Project package and all sub-packages. Name: Ends in search form class name with "SearchForm" stripped.
			 */
			String dtoClassName = StringUtils.substringBeforeLast(searchFormClassForDto.getSimpleName(), "SearchForm");
			Pattern dtoClassPattern = Pattern.compile(".*" + Pattern.quote(dtoClassName));

			String searchFormPackageName = StringUtils.substringBeforeLast(searchFormClass.getName(), ".");
			String parentPackagePrefix = StringUtils.substringBeforeLast(searchFormPackageName, ".") + '.';
			String projectPackagePrefix = ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION;

			// Attempt to retrieve class from parent package
			List<String> dtoClassList = new ArrayList<>();
			if (projectClassesByName.containsKey(parentPackagePrefix + dtoClassName)) {
				dtoClassList.add(parentPackagePrefix + dtoClassName);
			}

			// Attempt to retrieve class from parent package or sub-packages
			if (CollectionUtils.isEmpty(dtoClassList)) {
				projectClassesByName.keySet().stream()
						.filter(className -> className.startsWith(parentPackagePrefix))
						.filter(className -> StringUtils.isEqual(StringUtils.substringAfterLast(className, "."), dtoClassName))
						.forEachOrdered(dtoClassList::add);
			}

			// Attempt to retrieve class by matching suffix from parent package or sub-packages
			if (CollectionUtils.isEmpty(dtoClassList)) {
				projectClassesByName.keySet().stream()
						.filter(className -> className.startsWith(parentPackagePrefix))
						.filter(className -> dtoClassPattern.matcher(StringUtils.substringAfterLast(className, ".")).matches())
						.forEachOrdered(dtoClassList::add);
			}

			// Attempt to retrieve class from project package or sub-packages
			if (CollectionUtils.isEmpty(dtoClassList)) {
				projectClassesByName.keySet().stream()
						.filter(className -> className.startsWith(projectPackagePrefix))
						.filter(className -> StringUtils.isEqual(StringUtils.substringAfterLast(className, "."), dtoClassName))
						.forEachOrdered(dtoClassList::add);
			}

			// Attempt to retrieve class by matching suffix from project package or sub-packages
			if (CollectionUtils.isEmpty(dtoClassList)) {
				projectClassesByName.keySet().stream()
						.filter(className -> className.startsWith(projectPackagePrefix))
						.filter(className -> dtoClassPattern.matcher(StringUtils.substringAfterLast(className, ".")).matches())
						.forEachOrdered(dtoClassList::add);
			}

			AssertUtils.assertTrue(CollectionUtils.getSize(dtoClassList) == 1, "Unable to find a singular valid DTO class for search form [%s]. To resolve this, follow conventions for search form class names and package structures, manually specify a DTO class using the SearchForm annotation, or declare the search form as abstract with a qualifier or by using the SearchForm annotation. Discovered classes: [%s].", searchFormClass.getName(), CollectionUtils.toString(dtoClassList, 5));
			dtoClass = projectClassesByName.get(CollectionUtils.getOnlyElementStrict(dtoClassList));
		}

		// Verify valid DTO class found
		AssertUtils.assertTrue(IdentityObject.class.isAssignableFrom(dtoClass), "The corresponding DTO class [%s] for search form [%s] is not an instance of IdentityObject.", dtoClass.getName(), searchFormClass.getName());

		@SuppressWarnings("unchecked")
		Class<? extends IdentityObject> dtoIdentityObjectClass = (Class<? extends IdentityObject>) dtoClass;
		return dtoIdentityObjectClass;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a new unique ID.
	 */
	public static int generateId() {
		return generateId(Integer.class);
	}


	/**
	 * Generates a unique ID for the given type.
	 *
	 * @param <T>    the type of the entity for which an ID should be generated
	 * @param idType the entity ID type
	 * @return the generated ID
	 */
	public static <T extends Number> T generateId(Class<T> idType) {
		// Get properly adapted ID for ID field type
		final T newId;
		long newIdRaw = ID_COUNTER.getAndIncrement();
		if (idType == Long.class) {
			@SuppressWarnings("unchecked")
			T newIdLong = (T) Long.valueOf(newIdRaw);
			newId = newIdLong;
		}
		else if (idType == Integer.class) {
			@SuppressWarnings("unchecked")
			T newIdInteger = (T) Integer.valueOf((int) newIdRaw);
			newId = newIdInteger;
		}
		else if (idType == Short.class) {
			@SuppressWarnings("unchecked")
			T newIdShort = (T) Short.valueOf((short) newIdRaw);
			newId = newIdShort;
		}
		else {
			throw new RuntimeException("Unsupported ID type: " + idType);
		}

		return newId;
	}


	/**
	 * Generates an entity of the given type with an auto-incremented integer ID.
	 *
	 * @param type the class object for the type of the entity to generate
	 * @param <T>  the type of the entity to generate
	 * @return the generated entity
	 */
	public static <T extends SimpleEntity<Integer>> T generateEntity(Class<T> type) {
		return generateEntity(type, Integer.class);
	}


	/**
	 * Generates an entity of the given type with an auto-incremented ID.
	 *
	 * @param type   the class object for the type of the entity to generate
	 * @param idType the entity ID type
	 * @param <T>    the type of the entity to generate
	 * @param <S>    the type of the entity ID
	 * @return the generated entity
	 */
	public static <T extends SimpleEntity<S>, S extends Number> T generateEntity(Class<T> type, Class<S> idType) {
		final T entity;
		try {
			entity = type.getConstructor().newInstance();
			entity.setId(TestUtils.generateId(idType));
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while generating an entity of the given type: " + type, e);
		}
		return entity;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes the given command, asserting that an exception of the provided type is thrown. If any <code>messages</code> are provided, the exception will be analyzed to verify
	 * that each element in <code>messages</code> is contained in the exception message.
	 * <p>
	 * <b>Note: This is applicable to JUnit 5 tests only.</b>
	 *
	 * @param type       the type of exception expected for the current test execution
	 * @param executable the executable to run which should throw an exception of the given type
	 * @param messages   the expected substrings to be found in the exception message
	 */
	@SuppressWarnings("ThrowableNotThrown")
	public static void expectException(Class<? extends Throwable> type, Executable executable, Object... messages) {
		TestUtils.expectExceptionWithResult(type, executable, messages);
	}


	/**
	 * Executes the given command, asserting that an exception of the provided type is thrown. If any <code>messages</code> are provided, the exception will be analyzed to verify
	 * that each element in <code>messages</code> is contained in the exception message.
	 * <p>
	 * The resulting exception is returned.
	 * <p>
	 * <b>Note: This is applicable to JUnit 5 tests only.</b>
	 *
	 * @param type       the type of exception expected for the current test execution
	 * @param executable the executable to run which should throw an exception of the given type
	 * @param messages   the expected substrings to be found in the exception message
	 */
	public static Throwable expectExceptionWithResult(Class<? extends Throwable> type, Executable executable, Object... messages) {

		Throwable throwable = expectExceptionOnExecution(type, executable);
		List<String> missingMessageList = new ArrayList<>();
		String exceptionMessage = ExceptionUtils.getDetailedMessage(throwable);
		for (Object message : messages) {
			String messageStr = String.valueOf(message);
			if (!exceptionMessage.contains(messageStr)) {
				missingMessageList.add(messageStr);
			}
		}
		if (!missingMessageList.isEmpty()) {
			Assertions.fail("The thrown exception did not contain one or more of the expected message components. Missing components:\n\t- " + StringUtils.join(missingMessageList, "\n\t- ") + "\n\nReceived:\n" + exceptionMessage);
		}
		return throwable;
	}


	/**
	 * Executes the executable and compares the thrown {@link Throwable} to the expected type using {@link ExceptionUtils#getCauseException(Throwable, Class)}.
	 * Fails assertion with {@link Assertions#fail(String)} if no throwable is thrown or no throwable of the expected type is thrown.
	 * Overrides JUnit 5 implementation of executing {@link Executable} to look at causes of a thrown exception.
	 */
	public static Throwable expectExceptionOnExecution(Class<? extends Throwable> type, Executable executable) {
		try {
			executable.execute();
		}
		catch (Throwable actual) {
			Throwable actualExpectedThrowable = ExceptionUtils.getCauseException(actual, type);
			if (actualExpectedThrowable != null) {
				return actualExpectedThrowable;
			}
			else {
				BlacklistedExceptions.rethrowIfBlacklisted(actual);
				Assertions.fail(String.format("Expected %s to be thrown, but received %s instead.", type.getCanonicalName(), actual.getClass().getCanonicalName()), actual);
			}
		}

		Assertions.fail(String.format("Expected %s to be thrown, but nothing was thrown.", type.getCanonicalName()));
		return null;
	}


	/**
	 * Validates the lines of the expected and actual upload results, ignoring order.
	 */
	public static void validateUploadResultStringsIgnoringOrder(String expected, String actual) {
		String[] expectedLines = expected.split(StringUtils.NEW_LINE);
		Set<String> actualLinesSet = CollectionUtils.createLinkedHashSet(actual.split(StringUtils.NEW_LINE));
		Assertions.assertEquals(expectedLines.length, actualLinesSet.size());
		for (String expectedLine : expectedLines) {
			actualLinesSet.contains(expectedLine);
		}
	}
}
