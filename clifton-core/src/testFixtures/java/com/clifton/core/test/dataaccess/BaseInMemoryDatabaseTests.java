package com.clifton.core.test.dataaccess;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonMigrationStrategy;
import com.clifton.core.dataaccess.dao.event.User;
import com.clifton.core.dataaccess.migrate.MigrationJdbcTemplate;
import com.clifton.core.dataaccess.migrate.MigrationUtils;
import com.clifton.core.dataaccess.migrate.execution.action.MigrationExecutionActionHandlerImpl;
import com.clifton.core.dataaccess.migrate.execution.database.MigrationExecutionMSSQLDatabaseHandlerImpl;
import com.clifton.core.dataaccess.migrate.reader.MigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.sql.Sql;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.TestDataUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.core.io.ClassPathResource;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;


/**
 * This class is the base implementation for all in memory database tests that utilize JSON migration actions to insert the test data prior to test execution.  A context file and a
 * list of action files are used to load the necessary project info, create the appropriate in memory tables, and create the test data.
 * <p>
 *
 * @author StevenF on 10/5/2016.
 */
@ExtendWith(SpringExtension.class)
@Tag("memory-db")
public abstract class BaseInMemoryDatabaseTests implements InMemoryDatabaseTests, ApplicationContextAware {

	private static final String FUNCTIONS_MIGRATION_FILE_SUFFIX = "DataFunctions.xml";
	private static final String DATA_MIGRATION_FILE_SUFFIX = "Data.xml";
	private static final String ADDITIONAL_MIGRATION_FILE_SUFFIX = "DataAdditional.xml";

	public ApplicationContext applicationContext;
	public Map<String, Action> actionMap;

	@Resource
	private CacheHandler<?, ?> cacheHandler;
	@Resource
	private TestDataUtils inMemoryDatabaseTestsUtils;
	@Resource
	public JsonHandler<JacksonMigrationStrategy> migrationJsonHandler;
	@Resource
	public MigrationJdbcTemplate migrationJdbcTemplate;
	@Resource
	public MigrationDefinitionReader migrationXmlDefinitionReader;


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	@Override
	public boolean sqlFirst() {
		return true;
	}


	@Override
	public Map<String, Action> getActionMapOverrides() {
		return new HashMap<>();
	}


	@Override
	public List<org.springframework.core.io.Resource> getMigrationData() {
		return Collections.singletonList(getClasspathResourcesWithFileSuffix(DATA_MIGRATION_FILE_SUFFIX));
	}


	@Override
	public List<org.springframework.core.io.Resource> getFunctionsData() {
		return Collections.singletonList(getClasspathResourcesWithFileSuffix(FUNCTIONS_MIGRATION_FILE_SUFFIX));
	}


	@Override
	public List<org.springframework.core.io.Resource> getAdditionalData() {
		return Collections.singletonList(getClasspathResourcesWithFileSuffix(ADDITIONAL_MIGRATION_FILE_SUFFIX));
	}


	protected Class<?> getTestClass() {
		return this.getClass();
	}


	private org.springframework.core.io.Resource getClasspathResourcesWithFileSuffix(String suffix) {
		return new ClassPathResource(getClass().getPackage().getName().replaceAll("\\.", "/") + "/data/" + getTestClass().getSimpleName() + suffix);
	}


	@BeforeEach
	public void baseBeforeTest() {
		//Database data is cleared between tests, so need to execute actions each time to insert again.
		this.actionMap = MigrationUtils.populateActionMap(this.applicationContext);
		this.actionMap.putAll(getActionMapOverrides());
		beforeActions();
		executeActions();
		beforeTest();
	}


	/**
	 * https://stackoverflow.com/questions/4990410/how-can-i-wipe-data-from-my-hsqldb-after-every-test
	 * <p>
	 * Maybe the better option:
	 * <p>
	 * Alternatively, if you need the table and schema object definitions, you can create a file: database containing the objects but no data, and add the property below to the
	 * .properties file. Using this type of database for tests, the changes to data are not persisted files_read_only=true
	 */
	@AfterEach
	public void shutDown() {
		//Ensures database is cleared before running next test
		getMigrationJdbcTemplate().execute("TRUNCATE SCHEMA PUBLIC RESTART IDENTITY AND COMMIT NO CHECK;");
		// Clear Caches
		ArrayUtils.getStream(this.cacheHandler.getCacheNames()).forEach(this.cacheHandler::clear);
		afterTest();
	}


	@Override
	public void beforeActions() {
		//Do nothing; allows for override.
	}


	@Override
	public void beforeTest() {
		//Do nothing; allows for override.
	}


	@Override
	public void afterTest() {
		//Do nothing; allows for override.
	}


	protected IdentityObject getUser() {
		User user = new User();
		user.setId((short) 0);
		return user;
	}


	private void executeActions() {
		List<Sql> sqlList = new ArrayList<>();
		List<Action> actionList = new ArrayList<>();
		Consumer<org.springframework.core.io.Resource> resourceConsumer = resource -> {
			if (resource.exists()) {
				Schema schema = getMigrationXmlDefinitionReader().loadMigration(null, resource);
				sqlList.addAll(schema.getSqlList());
				actionList.addAll(schema.getActionList());
			}
		};

		CollectionUtils.getIterable(getFunctionsData()).forEach(resourceConsumer);
		CollectionUtils.getIterable(getMigrationData()).forEach(resourceConsumer);
		CollectionUtils.getIterable(getAdditionalData()).forEach(resourceConsumer);

		if (sqlFirst()) {
			executeSql(sqlList);
			executeActions(actionList);
		}
		else {
			executeActions(actionList);
			executeSql(sqlList);
		}
	}


	private void executeSql(List<Sql> sqlList) {
		MigrationExecutionMSSQLDatabaseHandlerImpl executionDatabaseHandler = new MigrationExecutionMSSQLDatabaseHandlerImpl();
		executionDatabaseHandler.setMigrationJdbcTemplate(getMigrationJdbcTemplate());
		for (Sql sql : CollectionUtils.getIterable(sqlList)) {
			for (String sqlString : CollectionUtils.getIterable(sql.getStatementList())) {
				executionDatabaseHandler.execute(sqlString);
			}
		}
	}


	private void executeActions(List<Action> actionList) {
		MigrationExecutionActionHandlerImpl executionActionHandler = new MigrationExecutionActionHandlerImpl();
		executionActionHandler.setJsonHandler(getMigrationJsonHandler());
		executionActionHandler.setMigrationUser(getUser());
		executionActionHandler.executeActions(actionList, this.actionMap, this.applicationContext);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public TestDataUtils getInMemoryDatabaseTestsUtils() {
		return this.inMemoryDatabaseTestsUtils;
	}


	public void setInMemoryDatabaseTestsUtils(TestDataUtils inMemoryDatabaseTestsUtils) {
		this.inMemoryDatabaseTestsUtils = inMemoryDatabaseTestsUtils;
	}


	public JsonHandler<JacksonMigrationStrategy> getMigrationJsonHandler() {
		return this.migrationJsonHandler;
	}


	public void setMigrationJsonHandler(JsonHandler<JacksonMigrationStrategy> migrationJsonHandler) {
		this.migrationJsonHandler = migrationJsonHandler;
	}


	public MigrationJdbcTemplate getMigrationJdbcTemplate() {
		return this.migrationJdbcTemplate;
	}


	public void setMigrationJdbcTemplate(MigrationJdbcTemplate migrationJdbcTemplate) {
		this.migrationJdbcTemplate = migrationJdbcTemplate;
	}


	public MigrationDefinitionReader getMigrationXmlDefinitionReader() {
		return this.migrationXmlDefinitionReader;
	}


	public void setMigrationXmlDefinitionReader(MigrationDefinitionReader migrationXmlDefinitionReader) {
		this.migrationXmlDefinitionReader = migrationXmlDefinitionReader;
	}
}
