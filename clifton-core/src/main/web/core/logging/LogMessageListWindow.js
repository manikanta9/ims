Clifton.core.logging.LogMessageListWindow = Ext.extend(TCG.app.Window, {
	id: 'logMessageListWindow',
	title: 'Centralized Logging',
	iconCls: 'explorer',
	width: 1500,
	height: 700,
	maximized: true,

	windowOnShow: function(w) {
		this.addAdditionalTabs(w);
	},
	addAdditionalTabs: function(w) {
		const arr = w.additionalTabs || [];
		const tabs = w.items.get(0);
		for (let i = 0; i < arr.length; i++) {
			tabs.add(arr[i]);
		}
	},

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Centralized Logging',
				items: [{
					xtype: 'gridpanel',
					name: 'logMessageListFind',
					remoteSort: true,
					hideToolsMenu: true,
					appendStandardColumns: false,
					instructions: 'The following messages have been logged into Graylog. NOTE: All text filtering is CASE SENSITIVE and BEGINS WITH.',
					wikiPage: 'IT/System+Logging',
					columns: [
						{header: 'ID', width: 50, dataIndex: 'messageId', filter: {comparison: 'EQUALS', searchFieldName: 'id'}, sortable: false, hidden: true},
						{header: 'Index', width: 50, dataIndex: 'index', sortable: false, hidden: true},
						{header: 'Time', width: 75, dataIndex: 'timestamp', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC', filter: false},
						{header: 'Log Group', width: 45, dataIndex: 'logGroup', sortable: false, hidden: true, filter: {type: 'list', options: [['internal', 'Internal'], ['server', 'Server']]}},
						{header: 'Application', width: 45, dataIndex: 'application', filter: {searchFieldName: 'applicationBeginsWith'}},
						{header: 'Environment', width: 50, dataIndex: 'environment', hidden: true},
						{header: 'Server', width: 50, dataIndex: 'server', hidden: true},
						{header: 'Level', width: 35, dataIndex: 'logLevel', filter: {searchFieldName: 'logLevelBeginsWith'}},
						{header: 'Level ID', width: 25, dataIndex: 'logLevelId', type: 'int', hidden: true},
						{header: 'Thread', width: 50, dataIndex: 'threadName'},
						{header: 'Thread ID', width: 50, dataIndex: 'threadId', type: 'int', hidden: true},
						{header: 'URL', width: 100, dataIndex: 'requestUri', hidden: true},
						{header: 'Session ID', width: 50, dataIndex: 'sessionId', hidden: true},
						{header: 'Class', width: 200, dataIndex: 'loggerName'},
						{header: 'User', width: 50, dataIndex: 'currentUserName'},
						{
							header: 'Message', width: 300, dataIndex: 'message', sortable: false,
							renderer: function(v, p, r) {
								//Strip HTML tags out of messages
								return TCG.stripTags(v);
							}
						}
					],
					editor: {
						drillDownOnly: true,
						getDetailPageClass: function(grid, row) {
							if (row.data.logGroup === 'server') {
								return 'Clifton.core.logging.LogMessageServerWindow';
							}
							return 'Clifton.core.logging.LogMessageInternalWindow';
						},
						getDetailPageId: function(gridPanel, row) {
							const data = row.data;
							return data.messageId + ':' + data.index;
						},
						getDefaultData: function(gridPanel) {
							const graylogUrl = this.getWindow().graylogUrl;
							if (graylogUrl) {
								return {
									graylogUrl: graylogUrl
								};
							}
						}
					},
					addToolbarButtons: function(toolBar, gridPanel) {
						toolBar.add({
							text: 'Open Graylog',
							tooltip: 'Opens the Graylog Web Interface',
							iconCls: 'graylog',
							scope: this,
							handler: function() {
								const url = this.getWindow().graylogUrl;
								if (url) {
									window.open(url, '_blank');
								}
							}
						});
						toolBar.add('-');
					},
					getTopToolbarFilters: function(toolbar) {
						//Start the beginning and end at today
						const date = new Date().format('m/d/Y');
						const filters = [];
						filters.push({
							fieldLabel: 'Log Level', xtype: 'toolbar-combo', name: 'logLevel', width: 75, mode: 'local',
							store: {
								xtype: 'arraystore',
								data: [
									['ERROR', 'ERROR', 'ERROR'],
									['WARN ', 'WARN ', 'WARN '],
									['INFO ', 'INFO ', 'INFO '],
									['DEBUG', 'DEBUG', 'DEBUG'],
									['TRACE', 'TRACE', 'TRACE'],
									['', 'All', 'All Log Levels']
								]
							}
						});
						filters.push({
							fieldLabel: 'Log Group', xtype: 'toolbar-combo', name: 'logGroup', width: 75, mode: 'local', value: 'internal',
							store: {
								xtype: 'arraystore',
								data: [
									['internal', 'Internal', 'Logs from internal applications'],
									['server', 'Server', 'Logs from server instances'],
									['', 'All', 'All Log Groups']
								]
							}
						});
						if (!this.getWindow().application) {
							filters.push({
								fieldLabel: 'Application', xtype: 'toolbar-combo', name: 'application', width: 225, mode: 'local',
								store: {
									xtype: 'arraystore',
									data: [
										['Bloomberg', 'Bloomberg', 'Bloomberg'],
										['FIX', 'FIX', 'FIX'],
										['IMS', 'IMS', 'IMS'],
										['Integration', 'Integration', 'Integration'],
										['OMS', 'OMS', 'OMS'],
										['Portal', 'Portal', 'Portal'],
										['Portal Admin', 'Portal Admin', 'Portal Admin'],
										['Reconciliation', 'Reconciliation', 'Reconciliation'],
										['SWIFT', 'SWIFT', 'SWIFT']
									]
								}
							});
						}

						filters.push(
							{fieldLabel: 'Start Date', xtype: 'toolbar-datefield', name: 'startDate', value: date, qtip: 'The search will begin at 00:00:00 on the start date.'},
							{fieldLabel: 'End Date', xtype: 'toolbar-datefield', name: 'endDate', value: date, qtip: 'The search will end at 11:59:59 on the end date.'}
						);

						return filters;
					},
					getLoadParams: function(firstLoad) {
						const lp = {};
						const logLevelFilter = TCG.getChildByName(this.getTopToolbar(), 'logLevel');
						if (firstLoad) {
							logLevelFilter.setValue('ERROR');
						}

						const logLevelFilterValue = logLevelFilter.getValue();
						if (logLevelFilterValue) {
							lp.logLevel = logLevelFilterValue;
						}

						const logGroupValue = TCG.getChildByName(this.getTopToolbar(), 'logGroup').getValue();
						if (logGroupValue) {
							lp.logGroup = logGroupValue;
						}

						const applicationToolbarFilter = TCG.getChildByName(this.getTopToolbar(), 'application');
						if (this.getWindow().application) {
							lp.applicationBeginsWith = this.getWindow().application;
						}
						else {
							if (TCG.isTrue(firstLoad) && this.getWindow().defaultApplicationFilter) {
								applicationToolbarFilter.setValue(this.getWindow().defaultApplicationFilter);
							}
							if (applicationToolbarFilter.getValue()) {
								lp.application = applicationToolbarFilter.getValue();
							}
						}

						//Set the date filter values
						const startDateValue = TCG.getChildByName(this.getTopToolbar(), 'startDate').getValue();
						const endDateValue = TCG.getChildByName(this.getTopToolbar(), 'endDate').getValue();
						if (startDateValue) {
							lp.startDate = startDateValue.format('m/d/Y');
						}
						if (endDateValue) {
							lp.endDate = endDateValue.format('m/d/Y');
						}
						return lp;
					}
				}]
			},


			{
				title: 'Loggers',
				items: [{
					xtype: 'log-configuration-grid'
				}]
			},


			{
				title: 'Appenders',
				items: [{
					xtype: 'log-appender-grid'
				}]
			}
		]
	}]
});
