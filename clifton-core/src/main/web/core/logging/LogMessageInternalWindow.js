Clifton.core.logging.LogMessageInternalWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Log Details',
	width: 1400,
	height: 700,
	setTitle: function(title, iconCls) {
		if (this.params && this.params.id) {
			title = title + ' - ' + this.params.id;
		}
		TCG.Window.superclass.setTitle.call(this, title, iconCls);
	},
	items: [{
		xtype: 'formpanel',
		url: 'logMessage.json',
		readOnly: true,
		loadValidation: false,
		getLoadParams: function(win) {
			return {identifier: win.params.id};
		},
		tbar: [{
			text: 'View in Graylog',
			tooltip: 'View this message in the Graylog interface',
			iconCls: 'graylog',
			handler: function() {
				const graylogUrl = TCG.getParentFormPanel(this).getWindow().defaultData.graylogUrl;
				if (graylogUrl) {
					const idArr = TCG.getParentFormPanel(this).getWindow().params.id.split(':');
					window.open(graylogUrl + '/messages/' + idArr[1] + '/' + idArr[0], '_blank');
				}
			}
		}],
		items: [
			{
				xtype: 'panel',
				layout: 'column',
				defaults: {
					layout: 'form',
					defaults: {xtype: 'textfield', anchor: '-10'}
				},
				items: [
					{
						columnWidth: .40,
						items: [
							{fieldLabel: 'Time', name: 'timestamp'},
							{fieldLabel: 'Log Level', name: 'logLevel'},
							{fieldLabel: 'Log Level ID', name: 'logLevelId'},
							{fieldLabel: 'User', name: 'currentUserName'},
							{fieldLabel: 'URL', name: 'requestUri'}
						]
					},
					{
						columnWidth: .28,
						items: [
							{fieldLabel: 'Application', name: 'application'},
							{fieldLabel: 'Environment', name: 'environment'},
							{fieldLabel: 'Server', name: 'server'},
							{fieldLabel: 'Client', name: 'requestClient'},
							{fieldLabel: 'Session ID', name: 'sessionId'}
						]
					},
					{
						columnWidth: .32,
						items: [
							{fieldLabel: 'Message ID', name: 'messageId'},
							{fieldLabel: 'Index', name: 'index'},
							{fieldLabel: 'Thread', name: 'threadName'},
							{fieldLabel: 'Thread ID', name: 'threadId'}
						]
					}
				]
			},
			{fieldLabel: 'Class', name: 'loggerName'},
			{fieldLabel: 'Message', name: 'message', xtype: 'textarea', height: 100, renderer: TCG.renderText},
			{
				xtype: 'formgrid',
				title: 'Request Parameter Properties',
				storeRoot: 'requestParameterProperties',
				collapsed: true,
				readOnly: true,
				columnsConfig: [
					{header: 'Name', width: 200, dataIndex: 'propertyName', editor: {xtype: 'textfield'}},
					{header: 'Value', width: 800, dataIndex: 'propertyValue', editor: {xtype: 'textfield'}, renderer: TCG.renderText}
				]
			},
			{
				xtype: 'formgrid',
				title: 'Request Attribute Properties',
				storeRoot: 'requestAttributeProperties',
				collapsed: true,
				readOnly: true,
				columnsConfig: [
					{header: 'Name', width: 200, dataIndex: 'propertyName', editor: {xtype: 'textfield'}},
					{header: 'Value', width: 800, dataIndex: 'propertyValue', editor: {xtype: 'textfield'}, renderer: TCG.renderText}
				]
			},
			{
				xtype: 'formgrid',
				title: 'Session Attribute Properties',
				storeRoot: 'sessionAttributeProperties',
				collapsed: true,
				readOnly: true,
				columnsConfig: [
					{header: 'Name', width: 200, dataIndex: 'propertyName', editor: {xtype: 'textfield'}},
					{header: 'Value', width: 800, dataIndex: 'propertyValue', editor: {xtype: 'textfield'}, renderer: TCG.renderText}
				]
			},
			{fieldLabel: 'Stack Trace', name: 'stackTrace', xtype: 'textarea', anchor: '-35 -350'}
		]
	}]
});
