Clifton.core.logging.configuration.LogConfigurationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Log Configuration',
	iconCls: 'hammer',
	width: 750,
	height: 300,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'Logger Configuration',
				items: [{
					xtype: 'formpanel',
					url: 'logConfiguration.json',
					instructions: 'Newly added Loggers initially get Appenders copied from the ROOT logger.',
					dtoClass: 'com.clifton.core.logging.configuration.LogConfiguration',
					loadValidation: false,
					getLoadParams: function(win) {
						const params = win ? win.params : this.getWindow().params;
						if (params) {
							return {name: params.id};
						}
					},
					items: [
						{fieldLabel: 'Logger Name', name: 'name', qtip: 'The fully-qualified class or package the logger should target'},
						{fieldLabel: 'Log Level', name: 'level', xtype: 'combo', mode: 'local', store: {xtype: 'arraystore', data: Clifton.core.logging.configuration.LOG_LEVELS}},
						{fieldLabel: 'Additive', name: 'additive', xtype: 'checkbox', disabled: true, boxLabel: 'Also forward messages from this Logger to the Parent Logger', qtip: 'If true, messages from this logger will forwarded to the parent logger after being handled by the current logger. NOTE: This can result in multiple instances of the same message being logged'},
						{fieldLabel: 'State', name: 'state', readOnly: true, submitValue: false}
					]
				}]
			},


			{

				title: 'Appender Configuration',
				items: [{
					xtype: 'gridpanel',
					name: 'logAppenderListForLogConfiguration',
					appendStandardColumns: false,
					instructions: 'Log messages from this Logger will be sent to the appenders listed. NOTE: If no appenders are listed here and the Logger IS additive, messages will be forwarded to the appenders listed in the ROOT logger.',
					getLoadURL: function() {
						let url = this.loadURL;
						if (!url) {
							url = this.name;
							if (url.indexOf('.json') === -1) {
								url += '.json';
							}
						}
						const w = this.getWindow();
						if (w && w.defaultData && w.defaultData.urlPrefix) {
							url = w.defaultData.urlPrefix + url.substring(0, 1).toUpperCase() + url.substring(1);
						}
						return url;
					},
					getLoadParams: function(firstLoad) {
						return {loggerName: this.getWindow().getMainForm().formValues.name};
					},
					columns: [
						{header: 'Appender Name', width: 65, dataIndex: 'name'},
						{header: 'Implementation Class', width: 125, dataIndex: 'clazz'},
						{header: 'State', width: 30, dataIndex: 'state'}
					],
					editor: {
						detailPageClass: 'Clifton.core.logging.configuration.LogAppenderWindow',
						getDetailPageId: function(gridPanel, row) {
							const data = row.data;
							return data.name;
						},
						getDeleteURL: function() {
							return 'logConfigurationLogAppenderLinkDelete.json';
						},
						getDeleteParams: function(selectionModel) {
							const loggerName = this.getWindow().getMainForm().formValues.name;
							const appenderName = selectionModel.getSelected().data.name;
							return {
								loggerName: loggerName,
								appenderName: appenderName
							};
						},
						addEditButtons: function(t, gp) {
							t.add(new TCG.form.ComboBox({
								name: 'appenderName',
								valueField: 'name',
								url: this.getWindow().urlPrefix ? this.getWindow().urlPrefix + 'LogAppenderList.json' : 'logAppenderList.json',
								emptyText: '< Select an Appender >',
								loadAll: true,
								width: 200,
								listWidth: 250
							}));
							t.add({
								text: 'Add Selected',
								xtype: 'button',
								tooltip: 'Add an appender',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const appender = TCG.getChildByName(t, 'appenderName');
									const appenderName = appender.getValue();
									if (TCG.isBlank(appenderName)) {
										TCG.showError('You must first select an appender from the list.');
									}
									else {
										const loggerName = this.getWindow().getMainForm().formValues.name;
										const loader = new TCG.data.JsonLoader({
											waitTarget: gp,
											waitMsg: 'Adding...',
											params: {loggerName: loggerName, appenderName: appenderName},
											onLoad: function(record, conf) {
												gp.reload();
												TCG.getChildByName(t, 'appenderName').reset();
											}
										});
										loader.load(this.getWindow().urlPrefix ? this.getWindow().urlPrefix + 'LogConfigurationLogAppenderLinkSave.json' : 'logConfigurationLogAppenderLinkSave.json');
									}
								}
							});
							t.add('-');
							TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
							t.add('-');
						}
					}
				}]
			}]
	}]
})
;
