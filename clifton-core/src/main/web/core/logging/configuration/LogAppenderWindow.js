Clifton.core.logging.configuration.LogAppenderWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Log Appender',
	iconCls: 'hammer',
	width: 750,
	height: 250,

	items: [{
		xtype: 'formpanel',
		url: 'logAppender.json',
		instructions: 'Appenders are responsible for delivering LogEvents to their destination. Every Appender must implement the Appender interface.',
		readOnly: true,
		loadValidation: false,
		getLoadParams: function(win) {
			return {name: win.params.id};
		},
		getLoadURL: function() {
			const w = this.getWindow();
			if (w && w.defaultData && w.defaultData.urlPrefix && this.url) {
				const url = this.url;
				return w.defaultData.urlPrefix + url.substring(0, 1).toUpperCase() + url.substring(1);
			}
			else if (this.url && w && w.params) {
				return this.url;
			}
			return false;
		},
		items: [
			{fieldLabel: 'Appender Name', name: 'name'},
			{fieldLabel: 'Impl Class', name: 'clazz'},
			{fieldLabel: 'State', name: 'state'}
		]
	}]
});
