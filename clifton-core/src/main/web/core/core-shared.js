Ext.ns('Clifton.core', 'Clifton.core.file', 'Clifton.core.util', 'Clifton.core.util.math', 'Clifton.core.oauth', 'Clifton.core.logging', 'Clifton.core.logging.configuration', 'Clifton.core.errors', 'Clifton.core.stats');

/*
 * Constants
 */

Clifton.core.file.FILE_FORMATS = [
	['PDF', 'PDF', 'PDF file format.'],
	['Excel (*.xlsx)', 'EXCEL_OPEN_XML', 'Excel file format.'],
	['Excel 97-2003 (*.xls)', 'EXCEL', 'Excel 97-2003 file format.'],
	['CSV', 'CSV', 'Comma delimited file format.'],
	['CSV Minimal', 'CSV_MIN', 'Comma delimited file format using quotes only when necessary.'],
	['CSV Pipe', 'CSV_PIPE', 'Pipe delimited csv min file (uses quotes only when necessary).'],
	['XML', 'XML', 'Extensible Markup Language file format.']
];

Clifton.core.util.math.COMPARISON_TYPES = [
	['LESS_THAN', 'Less Than', 'The first value is less than the second.'],
	['LESS_THAN_OR_EQUAL_TO', 'Less Than or Equal To', 'The first value is less than or equal to the second.'],
	['GREATER_THAN', 'Greater Than', 'The first value is greater than the second.'],
	['GREATER_THAN_OR_EQUAL_TO', 'Greater Than or Equal To', 'The first value is greater than or equal to the second.'],
	['EQUAL_TO', 'Equal To', 'The first value is equal to the second.']
];

Clifton.core.util.math.TRANSFORMATION_TYPES = [
	['NONE', 'None', 'Retain the original calculated value.'],
	['NEGATE', 'Negate', 'Multiply the calculated value by -1.'],
	['ABSOLUTE_VALUE', 'Absolute Value', 'Take the absolute value of the evaluated result.']
];

Clifton.core.util.math.AGGREGATION_OPERATIONS = [
	['SUM', 'Sum', 'Aggregate all values to find the sum.'],
	['MEAN', 'Mean', 'Find the mean average.'],
	['MEDIAN', 'Median', 'Find the median average.'],
	['MAX', 'Maximum', 'Find the maximum value.'],
	['MIN', 'Minimum', 'Find the minimum value.'],
	['COUNT', 'Count', 'Retrieve the number of items.'],
	['STDEV_S', 'Sample Standard Deviation', 'Calculate the sample standard deviation of the set.'],
	['STDEV_P', 'Population Standard Deviation', 'Calculate the population standard deviation of the set.'],
	['VAR_S', 'Sample Variance', 'Calculate the sample variance of the set.'],
	['VAR_P', 'Population Variance', 'Calculate the population variance of the set.']
];

Clifton.core.util.math.BINARY_OPERATORS = [
	['ADD', 'Add (+)', 'Adds the left operand and the right operand.'],
	['SUBTRACT', 'Subtract (-)', 'Subtracts the right operand from the left operand.'],
	['MULTIPLY', 'Multiply (*)', 'Multiplies the left operand by the right operand.'],
	['DIVIDE', 'Divide (/)', 'Divides the left operand by the right operand.'],
	['MODULO', 'Modulo (%)', 'Finds the remainder when the left operand is divided by the right operand.'],
	['POWER', 'Power (^)', 'Takes the left operand to the power of the right operand.']
];

/*
 * Classes
 */

Clifton.core.ScheduledRunnerListGridPanel = Ext.extend(TCG.grid.GridPanel, {
	typeName: 'REQUIRED-TYPE-NAME',
	instantRunner: false, // instant runners cannot be rescheduled
	name: 'runnerConfigListFind',
	restartUrl: 'runnerControllerReschedule.json',
	clearCompletedUrl: 'runnerCompletedClear.json',
	terminateRunnerUrl: 'runnerTerminate.json',
	xtype: 'gridpanel',
	limitRequestedProperties: false,
	useBufferView: false, // allow cell wrapping
	instructions: 'The scheduler runs every hour and schedules jobs that will run in the next hour.  The following is a list of all runs of selected type that are currently scheduled for execution in the next hour. Double click on a run to see more details.',

	columns: [
		{header: 'Type ID', width: 30, dataIndex: 'typeId'},
		{header: 'Type', width: 50, dataIndex: 'type'},
		{header: 'User', width: 50, dataIndex: 'runUser.label'},
		{header: 'State', width: 50, dataIndex: 'runnerState', filter: {type: 'list', options: [['SCHEDULED', 'SCHEDULED'], ['RUNNING ', 'RUNNING '], ['COMPLETED ', 'COMPLETED '], ['UNSCHEDULED', 'UNSCHEDULED']]}},
		{header: 'Run Status', width: 200, dataIndex: 'status.messageWithChildStatus', renderer: TCG.renderText},
		{header: 'Runner Class', width: 200, dataIndex: 'className', hidden: true},
		{header: 'Scheduled Time', width: 50, dataIndex: 'runDate', defaultSortColumn: true, defaultSortDirection: 'ASC'}
	],
	getLoadParams: function() {
		return {type: this.typeName};
	},
	isPagingEnabled: function() {
		return false;
	},
	editor: {
		drillDownOnly: true,
		detailPageClass: 'Clifton.core.RunnerStatusWindow',
		getDefaultDataForExisting: function(gridPanel, row) {
			return row.json;
		},
		addEditButtons: function(toolBar, gridPanel) {
			TCG.grid.GridEditor.prototype.addEditButtons.apply(this, arguments);

			if (!gridPanel.instantRunner) {
				toolBar.add(
					{
						text: 'Reschedule',
						tooltip: 'Restart runner controller and reschedule all future tasks.',
						iconCls: 'clock',
						handler: function() {
							const loader = new TCG.data.JsonLoader({
								success: function() {
									gridPanel.reload.defer(300, gridPanel);
								}
							});
							loader.load(gridPanel.restartUrl);
						}
					}, '-',
					{
						text: 'Clear Completed',
						tooltip: 'Clear the completed runners from the grid.',
						iconCls: 'clear',
						handler: function() {
							const loader = new TCG.data.JsonLoader({
								waitTarget: gridPanel,
								waitMsg: 'Clearing...',
								timeout: 5000,
								scope: gridPanel,
								onLoad: function() {
									gridPanel.reload();
								}
							});
							loader.load(gridPanel.clearCompletedUrl);
						}
					}, '-'
				);
			}
			toolBar.add(
				{
					text: 'Stop Runner',
					tooltip: 'Request that selected runner is stopped. Not all runners support this. Stopping will occurs on next status update.',
					iconCls: 'stop',
					handler: function() {
						const sm = gridPanel.grid.getSelectionModel();
						if (sm.getCount() === 0) {
							TCG.showError('Please select a runner to be stopped.', 'No Runner Selected');
						}
						else if (sm.getCount() !== 1) {
							TCG.showError('Multi-selection is not supported.  Please select one row.', 'NOT SUPPORTED');
						}
						else {
							const loader = new TCG.data.JsonLoader({
								waitTarget: gridPanel,
								waitMsg: 'Stopping...',
								params: {
									runnerType: sm.getSelected().json.type,
									runnerTypeId: sm.getSelected().json.typeId
								}
							});
							loader.load(gridPanel.terminateRunnerUrl);
						}
					}
				}, '-'
			);
		}
	}
});
Ext.reg('core-scheduled-runner-grid', Clifton.core.ScheduledRunnerListGridPanel);

Clifton.core.BaseStatusDetailForm = Ext.extend(TCG.form.FormPanel, {
	readOnly: true,
	initComponent: function() {
		const currentItems = [];
		Ext.each(this.statusDetailAdditionalFields, function(f) {
			currentItems.push(f);
		});
		Ext.each(this.statusDetailList, function(f) {
			currentItems.push(f);
		});
		this.items = currentItems;
		Clifton.core.BaseStatusDetailForm.superclass.initComponent.call(this);
	},
	statusDetailAdditionalFields: [],
	statusDetailList: [
		{
			xtype: 'textarea', fieldLabel: 'Message', name: 'status.message', style: 'font-size: 12px', height: 10, autoHeight: true
		},
		{
			xtype: 'formgrid-scroll',
			collapsible: false,
			viewConfig: {
				forceFit: true
			},
			readOnly: true,
			heightResized: true,
			height: 275,
			addToolbarButtons: function(toolBar, gridPanel) {
				toolBar.add(
					{
						text: 'Info',
						tooltip: 'Toggle instructions for this page',
						iconCls: 'info',
						enableToggle: true,
						handler: function() {
							if (this.pressed) {
								gridPanel.insert(0, {
									xtype: 'panel',
									frame: true,
									layout: 'fit',
									bodyStyle: 'padding: 3px 3px 3px 3px',
									html: 'This is the details of the status of the object, it will include warnings, updates and errors.'
								});
							}
							else {
								gridPanel.remove(0);
							}
							gridPanel.doLayout();
						}
					}, '-',
					{text: 'Print', xtype: 'button', iconCls: 'printer', handler: button => Ext.ux.Printer.print(TCG.getParentByClass(button, TCG.grid.FormGridPanel))}
				);
				toolBar.add('-');
				toolBar.add({
					text: 'Export to CSV',
					iconCls: 'export',
					tooltip: 'Export the table to CSV file',
					handler: function() {
						if (gridPanel.getStore().getTotalCount() < 1) {
							TCG.showError('There are no rows to export.');
							return;
						}
						const csvData = TCG.grid.generateGridPanelCsvData(gridPanel);
						TCG.downloadDataAsFile('text/csv', csvData, 'StatusDetails.csv');
					}
				});
			},

			storeRoot: 'status.detailList',
			useDynamicColumns: true,
			getDynamicColumns: data => data.map(entity => entity.properties)
				.filter(properties => properties)
				.flatMap(properties => Object.keys(properties))
				.distinct()
				.map(key => ({header: TCG.camelCaseToTitle(key), dataIndex: `properties.${key}`})),
			columnsConfig: [
				{header: 'Category', width: 100, dataIndex: 'category', defaultSortColumn: true},
				{header: 'Note', width: 800, dataIndex: 'note', renderer: TCG.renderText},
				{header: 'Date', width: 150, dataIndex: 'date'}
			],
			markModified: Ext.emptyFn
		}
	],
	getLoadParams: function(firstLoad) {
		return this.getWindow().defaultData;
	}
});
Ext.reg('status-detail-form', Clifton.core.BaseStatusDetailForm);


Clifton.core.StatusWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Status',
	iconCls: 'run',
	width: 1130,
	height: 410,
	modal: true,
	defaultDataIsReal: true,
	items: [{
		xtype: 'status-detail-form'
	}]
});


Clifton.core.RunnerStatusWindow = Ext.extend(Clifton.core.StatusWindow, {
	title: 'Runner Status',
	width: 1130,
	items: [{
		xtype: 'status-detail-form',
		height: 350,
		statusDetailAdditionalFields: [
			{
				xtype: 'columnpanel',
				columns: [
					{
						rows: [
							{fieldLabel: 'Run Date', name: 'runDate', xtype: 'textfield'},
							{fieldLabel: 'Run User', name: 'runUser.label', xtype: 'textfield'}
						]
					},
					{
						rows: [
							{fieldLabel: 'Run Type', name: 'type', xtype: 'textfield'},
							{fieldLabel: 'Run Type ID', name: 'typeId', xtype: 'textfield'}
						]
					}
				]
			},
			{fieldLabel: 'Class Name', name: 'className'}
		]
	}]
});


Clifton.core.oauth.OAuth2ClientGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'securityOauthClientDetailList',
	xtype: 'gridpanel',
	remoteSort: false,
	forceLocalFiltering: true,
	instructions: 'This screen is for administering Outgoing and Incoming OAuth connections.',
	wikiPage: 'IT/OAuth2',
	columns: [
		{header: 'Client ID', dataIndex: 'clientId'},
		{header: 'Incoming', dataIndex: 'incomingConnection', width: 20, type: 'boolean', tooltip: 'Denotes that this client is registered with to access THIS server via OAuth'},
		{header: 'Client URL', dataIndex: 'additionalInformation.clientUrl', tooltip: 'The URL of the client that will consume the resources of the Resource Host'},
		{header: 'Resource URL', dataIndex: 'additionalInformation.resourceUrl', tooltip: 'The URL of the API to be consumed'},
		{header: 'Grant Types', dataIndex: 'authorizedGrantTypes', hidden: true, tooltip: 'The methods that this client is authorized to use to obtain access tokens'},
		{header: 'Scopes', dataIndex: 'scope', hidden: true, tooltip: 'The authorized permissions of this client'},
		{header: 'Access Token Validity', dataIndex: 'accessTokenValiditySeconds', hidden: true, tooltip: 'The number of seconds that an access token for this client will be valid for'},
		{header: 'Signature Algorithm', dataIndex: 'signatureAlgorithm', hidden: true, tooltip: 'The algorithm that will be used to sign access token requests. (Only applicable to JWT grant type)'},
		{header: 'Signature Key Checksum', dataIndex: 'signatureKeyFileChecksum', hidden: true, tooltip: 'The SHA256 hash of the signature key.'}
	],
	editor: {
		detailPageClass: 'Clifton.core.oauth.OAuth2ClientWindow',
		getDetailPageId: function(gridPanel, row) {
			return row.json.clientId;
		},
		deleteUrl: 'securityOauthClientDetailDelete.json',
		getDeleteParams: function(selectionModel) {
			return {id: selectionModel.getSelected().json.clientId};
		},
		addEditButtons: function(t, gridPanel) {
			t.add({
				text: 'Register OAuth Connection',
				tooltip: 'Re-run the entire import processing for the selected runs.',
				iconCls: 'run',
				handler: function() {
					TCG.createComponent('Clifton.core.oauth.OAuth2RegistrationWindow', {
						openerCt: gridPanel
					});
				}
			});
			t.add('-');
			TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
		}
	}
});
Ext.reg('oauth-client-grid', Clifton.core.oauth.OAuth2ClientGrid);

Clifton.core.logging.configuration.LOG_LEVELS = [
	['OFF', 'OFF', 'No events will be logged.'],
	['FATAL', 'FATAL', 'A severe error that will prevent the application from continuing.'],
	['ERROR', 'ERROR', 'An error in the application, possibly recoverable.'],
	['WARN', 'WARN', 'An event that might possible lead to an error.'],
	['INFO', 'INFO', 'An event for informational purposes.'],
	['DEBUG', 'DEBUG', 'A general debugging event.'],
	['TRACE', 'TRACE', 'A fine-grained debug message, typically capturing the flow through the application.'],
	['ALL', 'ALL', 'All events should be logged.']
];

Clifton.core.logging.LogConfigurationGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'logConfigurationList',
	appendStandardColumns: false,
	instructions: 'A list of all message Loggers defined in system config file. Most specific Logger (based on class path and name) will override less specific logger.  Additive flag, will specify whether less specific Loggers get to process the message too or not. New Loggers can be added and existing Logger attributes can be changed but these changes will be lost after system restart (in-memory only).',
	columns: [
		{header: 'Logger Name', width: 225, dataIndex: 'name', defaultSortColumn: true},
		{
			header: 'Log Level', width: 30, dataIndex: 'level', filter: {
				type: 'combo',
				mode: 'local',
				store: {
					xtype: 'arraystore',
					data: Clifton.core.logging.configuration.LOG_LEVELS
				}
			}
		},
		{header: 'Additive', width: 20, dataIndex: 'additive', type: 'boolean'},
		{header: 'State', width: 30, dataIndex: 'state', hidden: 'true'}
	],
	editor: {
		detailPageClass: 'Clifton.core.logging.configuration.LogConfigurationWindow',
		getDetailPageId: function(gridPanel, row) {
			const data = row.data;
			return data.name;
		},
		getDeleteParams: function(selectionModel) {
			const selection = selectionModel.getSelected();
			if (TCG.isBlank(selection)) {
				TCG.showError('You must select a configuration to delete');
			}
			const data = selection.data;
			return {name: data.name};
		}
	}
});
Ext.reg('log-configuration-grid', Clifton.core.logging.LogConfigurationGrid);

Clifton.core.logging.LogAppenderGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'logAppenderList',
	appendStandardColumns: false,
	instructions: 'This is the list of appenders currently configured in the system. To add a new appender, edit the Configuration file and restart the server.',
	columns: [
		{header: 'Appender Name', width: 65, dataIndex: 'name'},
		{header: 'Impl Class', width: 125, dataIndex: 'clazz'},
		{header: 'State', width: 30, dataIndex: 'state'}
	],
	editor: {
		detailPageClass: 'Clifton.core.logging.configuration.LogAppenderWindow',
		drillDownOnly: true,
		getDetailPageId: function(gridPanel, row) {
			const data = row.data;
			return data.name;
		}
	}
});
Ext.reg('log-appender-grid', Clifton.core.logging.LogAppenderGrid);


/**
 * The list of tabs to be used within the stats window. See {@link Clifton.core.stats.StatsListWindow}.
 *
 * @type {Array}
 */
Clifton.core.stats.StatsListTabs = [];

// override to provide drill down
Clifton.core.stats.createDetailStatsUriWindow = function(uri, source) {
	TCG.showInfo('Override "Clifton.core.stats.createDetailStatsUriWindow" function to provide implementation.<br/><br/>URI: ' + uri + '<br/>Source: ' + source);
};

Clifton.core.stats.PATH = window.location.pathname;

Clifton.core.stats.getNormalizedURI = function(uri, source) {
	if (source === 'WEB' && Clifton.core.stats.PATH && uri.startsWith(Clifton.core.stats.PATH)) {
		// remove app name
		return uri.substring(Clifton.core.stats.PATH.length - 1); // keep the forward slash
	}
	return uri;
};
