Clifton.core.stats.StatsWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Requests for URL',
	iconCls: 'timer',
	width: 1400,
	height: 600,
	defaultDataIsReal: true,

	items: [{
		xtype: 'formpanel',
		labelFieldName: 'requestURL',
		readOnly: true,
		items: [
			{
				fieldLabel: 'URI', name: 'requestURI', xtype: 'linkfield',
				createDetailClass: function() {
					const source = this.getParentForm().getFormValue('requestSource');
					const uri = Clifton.core.stats.getNormalizedURI(this.getValue(), source);
					Clifton.core.stats.createDetailStatsUriWindow(uri, source);
				}
			},
			{
				xtype: 'columnpanel',
				defaults: {xtype: 'floatfield'},
				columns: [
					{
						config: {columnWidth: 0.25},
						rows: [
							{fieldLabel: 'Source', name: 'requestSource', xtype: 'textfield'},
							{fieldLabel: 'Hits', name: 'executeCount'}
						]
					},
					{
						config: {columnWidth: 0.25},
						rows: [
							{fieldLabel: 'Slow Hits', name: 'slowExecuteCount'},
							{fieldLabel: 'Slowest Max Count', name: 'slowestMaxCount'}
						]
					},
					{
						config: {columnWidth: 0.25, labelWidth: 130},
						rows: [
							{fieldLabel: 'Min Time (seconds)', name: 'minTotalTimeSeconds'},
							{fieldLabel: 'Max Time (seconds)', name: 'maxTotalTimeSeconds'}
						]
					},
					{
						config: {columnWidth: 0.25, labelWidth: 130},
						rows: [
							{fieldLabel: 'Total Time (seconds)', name: 'totalTimeSeconds'},
							{fieldLabel: 'Avg Time (seconds)', name: 'avgTimeSeconds'}
						]
					}
				]
			},

			{
				title: 'Slowest Requests',
				xtype: 'formgrid-scroll',
				storeRoot: 'slowestStats',
				readOnly: true,
				height: 400,
				heightResized: true,
				columnsConfig: [
					{header: 'Time', width: 125, dataIndex: 'logTime', type: 'date'},
					{header: 'Time Millis', width: 100, dataIndex: 'logTimeMillis', type: 'int', hidden: true},
					{header: 'Duration', width: 65, dataIndex: 'durationSeconds', type: 'currency'},
					{header: 'First Hit', width: 60, dataIndex: 'firstHit', type: 'boolean'},
					{header: 'Request Parameters', width: 1050, dataIndex: 'parameters', renderer: TCG.renderText, openCellDetailWindowOnDblClick: true}
				],
				markModified: Ext.emptyFn
			}
		]
	}]
});
