// Add default tabs to the stats list window
Clifton.core.stats.StatsListTabs.unshift(
	{
		title: 'Cache Stats',
		items: [{
			xtype: 'gridpanel',
			name: 'systemCacheStatsList',
			instructions: 'The following cache statistics was collected. "statistics" property must be enabled in config file in order to see stats.',
			reloadOnRender: false, // slow request, we often need other tabs
			columns: [
				{header: 'Cache Name', dataIndex: 'cacheName', width: 150, nonPersistentField: true},
				{header: 'Count', dataIndex: 'size', width: 30, type: 'int', summaryType: 'sum', defaultSortColumn: true, defaultSortDirection: 'DESC', nonPersistentField: true},
				{header: 'Memory Size', dataIndex: 'inMemorySize', width: 40, type: 'int', summaryType: 'sum', nonPersistentField: true},
				{header: 'Time to Live', dataIndex: 'timeToLive', width: 35, type: 'int', title: 'Time before cache expires in seconds', nonPersistentField: true},
				{header: 'Stats Enabled', dataIndex: 'statisticsEnabled', width: 40, type: 'boolean', nonPersistentField: true},
				{header: 'Hit Count', dataIndex: 'cacheHitCount', width: 30, type: 'int', summaryType: 'sum', nonPersistentField: true},
				{header: 'Miss Count', dataIndex: 'cacheMissCount', width: 30, type: 'int', summaryType: 'sum', nonPersistentField: true},
				{
					header: 'Hit %', dataIndex: 'cacheHitRate', width: 25, type: 'percent', sortable: true, nonPersistentField: true,
					convert: (value, record) => 100 * record.cacheHitCount / (record.cacheHitCount + record.cacheMissCount)
				}
			],
			getTopToolbarFilters: function(toolbar) {
				return [
					'-',
					{fieldLabel: '', boxLabel: 'Calculate Size', xtype: 'toolbar-checkbox', name: 'calculateSize'}
				];
			},
			getLoadParams: function(firstLoad) {
				const calcSize = TCG.getChildByName(this.getTopToolbar(), 'calculateSize');
				return {calculateSize: calcSize.checked};
			},
			plugins: {ptype: 'gridsummary'},
			// show memory stats in the top toolbar
			updateCount: function() {
				const t = this.getTopToolbar();
				if (t && !TCG.isTrue(this.updatingMemoryState)) {
					// Set a flag for working on memory state asynchronously. Without the flag, the state is updated twice; once by the grid store load and once by the grid filter
					this.updatingMemoryState = true;
					TCG.data.getDataPromise('systemMemoryState.json', this)
						.then(data => {
							try {
								const mb = 1024 * 1024;
								const label = data ? 'Free: ' + Ext.util.Format.number(data.freeMemory / mb, '0,000') + ' MB | Total: ' + Ext.util.Format.number(data.totalMemory / mb, '0,000') + ' MB | Max: ' + Ext.util.Format.number(data.maxMemory / mb, '0,000') + ' MB' : 'N/A';
								const last = t.items.get(t.items.length - 1);
								if (last.text && last.text.indexOf('Free:') === 0) {
									last.setText(label);
								}
								else {
									t.add('->', {xtype: 'tbtext', text: label});
								}
								t.syncSize();
							}
							finally {
								// reset working flag
								this.updatingMemoryState = false;
							}
						});
				}
			},
			editor: {
				addEnabled: false,
				addToolbarDeleteButton: function(toolbar) {
					toolbar.add([
						{
							text: 'Clear',
							iconCls: 'remove',
							menu: [
								{
									text: 'Clear Selected',
									tooltip: 'Remove all items from selected cache',
									iconCls: 'remove',
									scope: this,
									handler: function() {
										const grid = this.grid;
										const sm = grid.getSelectionModel();
										if (sm.getCount() === 0) {
											TCG.showError('Please select a cache to be cleared.', 'No Row(s) Selected');
										}
										else if (sm.getCount() !== 1) {
											TCG.showError('Multi-selection clearing is not supported yet.  Please select one row.', 'NOT SUPPORTED');
										}
										else {
											Ext.Msg.confirm('Clear Selected Cache', 'Would you like to clear all elements from selected cache?', function(a) {
												if (a === 'yes') {
													const loader = new TCG.data.JsonLoader({
														waitTarget: grid.ownerCt,
														waitMsg: 'Clearing...',
														params: {cacheName: sm.getSelected().data.cacheName}
													});
													loader.load('systemCacheDelete.json');
												}
											});
										}
									}
								},
								{
									text: 'Clear All',
									tooltip: 'Remove all cached items from the system',
									iconCls: 'remove',
									scope: this,
									handler: function() {
										const grid = this.grid;
										Ext.Msg.confirm('Clear All Caches', 'This will remove <b>all cached items from the system.</b> Would you like to clear all caches?', function(a) {
											if (a === 'yes') {
												new Promise(function(resolve, reject) {
													const loader = new TCG.data.JsonLoader({
														waitTarget: grid.ownerCt,
														waitMsg: 'Clearing...',
														onLoad: resolve
													});
													loader.load('systemCacheAllDelete.json');
												}).then(function(result) {
													grid.ownerCt.reload();
												});
											}
										});
									}
								}
							]
						},
						'-',
						{
							text: 'Enable Stats',
							tooltip: 'Enabled collection of Cache statistics for selected cache (all caches if none is selected): slight performance overhead',
							iconCls: 'checked',
							scope: this,
							handler: function() {
								const sm = this.grid.getSelectionModel();
								const cacheName = (sm.getCount() === 1) ? sm.getSelected().data.cacheName : '';
								TCG.getResponseText('systemCacheStatsEnabled.json?enableStatistics=true&cacheName=' + cacheName, this);
								this.grid.ownerCt.reload();
							}
						},
						'-',
						{
							text: 'Disable Stats',
							tooltip: 'Disable collection of Cache statistics for selected cache (all caches if none is selected): slight performance improvement',
							iconCls: 'unchecked',
							scope: this,
							handler: function() {
								const sm = this.grid.getSelectionModel();
								const cacheName = (sm.getCount() === 1) ? sm.getSelected().data.cacheName : '';
								TCG.getResponseText('systemCacheStatsEnabled.json?enableStatistics=false&cacheName=' + cacheName, this);
								this.grid.ownerCt.reload();
							}
						},
						'-'
					]);
				}
			}
		}]
	},


	{
		title: 'Database Stats',
		items: [
			{
				xtype: 'formpanel',
				loadValidation: false,
				labelWidth: 80,
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				defaults: {flex: 0},
				url: 'databaseStatistics.json',

				listeners: {
					afterload: function() {
						const configurationGrid = TCG.getChildByName(this, 'configuration-grid');
						configurationGrid.store.loadData(this.getForm().formValues);
						configurationGrid.store.rejectChanges(); // reset modified
					}
				},

				getLoadURL: function() {
					return this.url;
				},

				getLoadParams: function() {
					return {
						dataSourceName: TCG.getChildByName(this.toolbars[0], 'dataSourcePoolName').getValue(),
						requestedPropertiesRoot: 'databasePropertyList',
						requestedPropertiesToExclude: 'databaseStatisticsCommand|dataSourceName'
					};
				},

				reload: function() {
					const fp = this;
					fp.load(Ext.applyIf({
						waitMsg: 'Loading...'
					}, fp.getSubmitDefaults()));
				},

				getSubmitDefaults: function() {
					const fp = this;
					return Ext.applyIf({
						url: encodeURI(fp.getLoadURL()),
						params: fp.getLoadParams(),
						success: function(form, action) {
							fp.loadJsonResult(action.result, true);
							fp.fireEvent('afterload', fp);
						}
					}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults));
				},

				updatePropertyList: function(properties) {
					const fp = this;
					const params = {
						beanList: Ext.util.JSON.encode(properties),
						dataSourceName: TCG.getChildByName(this.toolbars[0], 'dataSourcePoolName').getValue()
					};
					fp.load(Ext.applyIf({
						url: encodeURI('databasePropertyListSave.json'),
						params: params,
						waitMsg: 'Saving...'
					}, fp.getSubmitDefaults()));
				},

				processDatabaseStatisticsCommand: function(paramsArg) {
					const fp = this;
					const params = TCG.isBlank(paramsArg) ? {} : paramsArg;
					const confirmWindow = new TCG.app.Window({
						title: 'Process Database Statistics Command',
						iconCls: 'clear',
						width: 400,
						height: 150,
						modal: true,
						minimizable: false,
						maximizable: false,
						resizable: false,
						border: true,
						bodyStyle: 'padding: 20px 20px 0;',
						openerCt: this,
						items: [
							{xtype: 'label', html: 'Are you sure you want to process the following database statistics command?<br/>' + JSON.stringify(params)}
						],
						buttons: [
							{
								text: 'Process', width: 100, tooltip: 'Process command and reload the grid closing this window.',
								handler: function() {
									confirmWindow.win.closeWindow();
									fp.load(Ext.applyIf({
										url: encodeURI('databaseStatisticsCommandProcess.json'),
										params: Ext.applyIf(params, fp.getLoadParams()),
										waitMsg: 'Processing...'
									}, fp.getSubmitDefaults()));
								}
							},
							{
								text: 'Reload', width: 100, tooltip: 'Do not process but reload the grid closing this window.',
								handler: function() {
									confirmWindow.win.closeWindow();
									fp.reload();
								}
							},
							{
								text: 'Cancel', width: 100, tooltip: 'Do not perform any action and close the window.',
								handler: function() {
									confirmWindow.win.closeWindow();
								}
							}
						],
						defaultButton: 0,
						windowOnShow: function() {
							this.focus(); // allow keyboard navigation selection: unfortunately visual highlight only happens after the user hits tab once (anywhere/anytime after our app is loaded)
						}
					});
				},

				resetStatistics: function() {
					this.processDatabaseStatisticsCommand({clearStatistics: true});
				},

				restoreDatabasePropertyList: function() {
					this.processDatabaseStatisticsCommand({restorePropertyList: true});
				},

				resetDatabaseConnections: function() {
					this.processDatabaseStatisticsCommand({resetConnections: true});
				},

				tbar: [
					{
						text: 'Reload',
						xtype: 'button',
						iconCls: 'table-refresh',
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							fp.reload();
						}
					},
					'-',
					{
						text: 'Reset Statistics',
						tooltip: 'Administrative action to reset database statistics.',
						xtype: 'button',
						iconCls: 'clear',
						handler: function() {
							TCG.getParentFormPanel(this).resetStatistics();
						}
					},
					'-',
					{
						text: 'Reset Connections',
						tooltip: 'Administrative action to purge all idle connections and active connections upon return for the pool.',
						xtype: 'button',
						iconCls: 'clear',
						handler: function() {
							TCG.getParentFormPanel(this).resetDatabaseConnections();
						}
					},
					'->',
					{xtype: 'tbtext', text: '&nbsp;Data Source:&nbsp;'},
					{
						tooltip: 'Selects a database pool name for which the stats will be displayed.',
						xtype: 'toolbar-combo',
						value: 'dataSource',
						url: 'databasePoolNameList.json',
						loadAll: true,
						mode: 'remote',
						name: 'dataSourcePoolName',
						description: 'description',

						listeners: {
							select: function(combo, record, index) {
								const fp = TCG.getParentFormPanel(combo);
								fp.reload();
							}
						}
					}
				],

				items: [
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {
							columnWidth: .22,
							layout: 'form',
							defaults: {xtype: 'integerfield', readOnly: true, anchor: '0'}
						},

						items: [
							{
								items: [
									{fieldLabel: 'Created', name: 'createdCount', qtip: 'Connections created to the database since either the start of the process or last time statistics were reset'},
									{fieldLabel: 'Reconnected', name: 'reconnectedCount', qtip: 'Connections reconnected to the database since either the start of the process or last time statistics were reset'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								items: [
									{fieldLabel: 'Size', name: 'size', qtip: 'Current size of the connection pool'},
									{fieldLabel: 'Active', name: 'activeCount', qtip: 'Number of active connections in the pool'},
									{fieldLabel: 'Max Active', name: 'maxActiveCount', qtip: 'Maximum number of active connections created in the pool'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								labelWidth: 140,
								columnWidth: .25,
								items: [
									{fieldLabel: 'Idle', name: 'idleCount', qtip: 'Number of idle connections in the pool'},
									{fieldLabel: 'Borrowed', name: 'borrowedCount', qtip: 'Number of times a connection was borrowed from the connection pool since the start of the process or last time the statistics were reset'},
									{fieldLabel: 'Returned', name: 'returnedCount', qtip: 'Number of times a connection was returned to the connection pool since the start of the process or last time the statistics were reset'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								labelWidth: 140,
								columnWidth: .25,
								items: [
									{fieldLabel: 'Released', name: 'releasedCount', qtip: 'Number of connections that have been released/closed since the start of the process or last time the statistics were reset'},
									{fieldLabel: 'Released Idle', name: 'releasedIdleCount', qtip: 'Number of connections that have been released/closed while idle since the start of the process or last time the statistics were reset'},
									{fieldLabel: 'Released Abandoned', name: 'releaseAbandonedCount', qtip: 'Number of connections that have been released/closed due to being abandoned since the start of the process or last time the statistics were reset'}
								]
							}
						]
					},
					{
						xtype: 'formgrid',
						title: 'Configuration Properties',
						name: 'configuration-grid',
						hideInstructions: true,
						storeRoot: 'databasePropertyList',
						viewConfig: {markDirty: false, forceFit: true},
						collapsible: false,
						autoHeight: false,
						readOnly: true,
						flex: 1,

						addToolbarButtons: function(toolbar, grid) {
							toolbar.add({
								text: 'Help',
								iconCls: 'help',
								handler: function() {
									TCG.openFile('https://tomcat.apache.org/tomcat-8.5-doc/jdbc-pool.html#Common_Attributes');
								}
							});
							toolbar.add('->',
								{
									text: 'Save New Values',
									tooltip: 'Administrative action to update the database connection pool configuration real-time to reflect new values specified in the grid. The updated values are not persisted between application restarts. Refer to the Help documentation for more information on tuning the connection pool. If no new values are specified, this button will do nothing.',
									iconCls: 'run',
									handler: function() {
										const properties = [];
										let propertyNamesString = '';
										const modifiedRecords = grid.getStore().getModifiedRecords();
										let i = 0;
										const len = modifiedRecords.length;
										for (; i < len; i++) {
											const record = modifiedRecords[i].data;
											if (TCG.isNotEquals(record['value'], record['newValue'])) {
												record['class'] = 'com.clifton.core.dataaccess.datasource.stats.DatabaseProperty';
												properties.push(record);
												propertyNamesString += record.name + ', ';
											}
										}
										if (properties.length > 0) {
											const confirmWindow = new TCG.app.Window({
												title: 'Database Property List - Update',
												iconCls: 'clear',
												width: 400,
												height: 150,
												modal: true,
												minimizable: false,
												maximizable: false,
												resizable: false,
												border: true,
												bodyStyle: 'padding: 20px 20px 0;',
												openerCt: grid,
												items: [
													{xtype: 'label', html: 'Are you sure you want to <b>update</b> the following database properties?<br/>' + propertyNamesString.substring(0, propertyNamesString.length - 2) + ''}
												],
												buttons: [
													{
														text: 'Update', width: 100, tooltip: 'Perform update and reload the grid closing this window.',
														handler: function() {
															confirmWindow.win.closeWindow();
															TCG.getParentFormPanel(grid).updatePropertyList(properties);
														}
													},
													{
														text: 'Cancel', width: 100, tooltip: 'Do not perform any action and close the window.',
														handler: function() {
															confirmWindow.win.closeWindow();
														}
													}
												],
												defaultButton: 0,
												windowOnShow: function() {
													this.focus(); // allow keyboard navigation selection: unfortunately visual highlight only happens after the user hits tab once (anywhere/anytime after our app is loaded)
												}
											});
										}
									}
								},
								'-',
								{
									text: 'Restore Values',
									tooltip: 'Administrative action to restore the database connection pool configuration real-time to the original values used at startup.',
									iconCls: 'run',
									handler: function() {
										TCG.getParentFormPanel(grid).restoreDatabasePropertyList();
									}
								});
						},

						columnsConfig: [
							{header: 'Name', dataIndex: 'name'},
							{header: 'Type', dataIndex: 'type'},
							{header: 'Value', dataIndex: 'value'},
							{header: 'New Value', dataIndex: 'newValue', editor: {xtype: 'textfield'}}
						]
					}
				]
			}
		]
	},


	{
		title: 'User Sessions'
	},


	{
		title: 'Scheduled Runners',
		items: [{
			xtype: 'core-scheduled-runner-grid',
			getLoadParams: function() {
				return undefined; // include all
			}
		}]
	},


	{
		title: 'Runner Handler Stats',
		items: [
			{
				xtype: 'formpanel',
				loadValidation: false,
				labelWidth: 80,
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				defaults: {flex: 0},
				getLoadURL: function() {
					return 'runnerHandlerStatus.json';
				},
				getLoadParams: function() {
					return {
						requestedPropertiesRoot: 'data.executorServiceStatusList'
					};
				},
				reload: function() {
					this.reloadWithParams();
				},
				reloadWithParams: function(paramsArg) {
					const fp = this;
					const params = TCG.isBlank(paramsArg) ? fp.getLoadParams(fp.getWindow()) : paramsArg;
					params['includeStatusOfAllThreads'] = fp.isIncludeAllThreads();
					fp.load(Ext.applyIf({
						url: encodeURI(fp.getLoadURL()),
						params: params,
						waitMsg: 'Loading...',
						success: function(form, action) {
							fp.loadJsonResult(action.result, true);
							fp.fireEvent('afterload', fp);
						}
					}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
				},
				isIncludeAllThreads: function() {
					const toolbar = this.getTopToolbar();
					return TCG.getChildByName(toolbar, 'includeAllThreads').getValue();
				},
				resetStatistics: function() {
					const fp = this;
					const params = fp.getLoadParams(fp.getWindow());
					params['clearStatistics'] = true;
					fp.reloadWithParams(params);
				},
				handleRunnerMonitoringRegistrationAction: function(url) {
					const fp = this;
					const toolbar = fp.getTopToolbar();
					const type = TCG.getChildByName(toolbar, 'runnerType').getValue();
					if (!type) {
						TCG.showError('Please enter a Runner Type and optional Type ID.', 'No Runner Type Specified.');
						return;
					}
					const params = {};
					params['type'] = type;
					params['typeId'] = TCG.getChildByName(toolbar, 'runnerTypeId').getValue();
					fp.load(Ext.applyIf({
						url: encodeURI(url),
						params: params,
						waitMsg: 'Registering...',
						success: function(form, action) {
							TCG.getChildByName(toolbar, 'runnerType').reset();
							TCG.getChildByName(toolbar, 'runnerTypeId').reset();
							fp.reload();
						}
					}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
				},
				tbar: [
					{xtype: 'sectionheaderfield', header: 'Runner Stats'},
					'-',
					{
						text: 'Reload',
						xtype: 'button',
						iconCls: 'table-refresh',
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							fp.reload();
						}
					},
					'-',
					{
						text: 'Reset Statistics',
						xtype: 'button',
						iconCls: 'clear',
						handler: function() {
							const fp = TCG.getParentFormPanel(this);
							const confirmWindow = new TCG.app.Window({
								title: 'Runner Handler Statistics - Reset',
								iconCls: 'workflow',
								width: 400,
								height: 120,
								modal: true,
								minimizable: false,
								maximizable: false,
								resizable: false,
								border: true,
								bodyStyle: 'padding: 20px 20px 0;',
								openerCt: this,
								items: [
									{xtype: 'label', html: 'Are you sure you want to <b>reset</b> the runner statistics?'}
								],
								buttons: [
									{
										text: 'Reset',
										tooltip: 'Perform reset and reload the grid closing this window.',
										width: 100,
										handler: function() {
											confirmWindow.win.closeWindow();
											fp.resetStatistics();
										}
									},
									{
										text: 'Reload',
										tooltip: 'Do not reset but reload the grid closing this window.',
										width: 100,
										handler: function() {
											confirmWindow.win.closeWindow();
											fp.reload();
										}
									},
									{
										text: 'Cancel',
										tooltip: 'Do not perform any action and close the window.',
										width: 100,
										handler: function() {
											confirmWindow.win.closeWindow();
										}
									}
								],
								defaultButton: 0,
								windowOnShow: function() {
									this.focus(); // allow keyboard navigation selection: unfortunately visual highlight only happens after the user hits tab once (anywhere/anytime after our app is loaded)
								}
							});
						}
					},
					'-',
					{xtype: 'label', html: '&nbsp;Type:&nbsp;', qtip: 'Runner Type to register monitoring of execution statistics.'},
					{xtype: 'textfield', name: 'runnerType'},
					{xtype: 'label', html: '&nbsp;&nbsp;Type ID:&nbsp;', qtip: 'Runner Type ID to use in conjunction with the Type register monitoring of execution statistics.'},
					{xtype: 'textfield', name: 'runnerTypeId'},
					{
						text: 'Runner Monitoring Actions',
						tooltip: 'Register or Deregister Runners by Type and optional Type ID that the system will monitor run statistics for. Monitored Runners are only registered until the system is restarted; registrations are not saved.',
						iconCls: 'run',
						menu: new Ext.menu.Menu({
							items: [
								{
									text: 'Register Runner Monitoring',
									iconCls: 'add',
									handler: function() {
										const fp = TCG.getParentFormPanel(this);
										fp.handleRunnerMonitoringRegistrationAction.call(fp, 'registerRunnerExecutionMonitoring.json');
									}
								},
								{
									text: 'Deregister Runner Monitoring',
									iconCls: 'remove',
									handler: function() {
										const fp = TCG.getParentFormPanel(this);
										fp.handleRunnerMonitoringRegistrationAction.call(fp, 'deregisterRunnerExecutionMonitoring.json');
									}
								}
							]
						})
					},
					'->',
					{xtype: 'label', html: 'Dump All Threads:&nbsp;', qtip: 'Include all threads in the system in the thread dump.'},
					{xtype: 'checkbox', name: 'includeAllThreads', width: 70}
				],
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {
							columnWidth: .22,
							layout: 'form',
							defaults: {xtype: 'integerfield', readOnly: true, anchor: '0'}
						},
						items: [
							{
								items: [
									{fieldLabel: 'Scheduled', name: 'scheduledRunnerCount'},
									{fieldLabel: 'Completed', name: 'completedRunnerCount'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								items: [
									{fieldLabel: 'Unsuccessful', name: 'unsuccessfulRunnerCount'},
									{fieldLabel: 'Canceled', name: 'canceledRunnerCount'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								labelWidth: 140,
								columnWidth: .25,
								items: [
									{fieldLabel: 'Rescheduled', name: 'rescheduledRunnerCount'},
									{fieldLabel: 'Longest Queue Duration', name: 'longestQueueMilliseconds', qtip: 'The longest time a runner was queued beyond its scheduled run time. Duration is in milliseconds.'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								labelWidth: 140,
								columnWidth: .25,
								items: [
									{fieldLabel: 'Average Queue Duration', name: 'averageQueueMilliseconds', qtip: 'Average time a runner is queued beyond its scheduled run time. Duration is in milliseconds.'},
									{fieldLabel: 'Average Run Duration', name: 'averageRunMilliseconds', qtip: 'Average execution time for all executed runners. Duration is in milliseconds.'}
								]
							}
						]
					},
					{
						xtype: 'formgrid',
						readOnly: true,
						hideInstructions: true,
						storeRoot: 'runnerExecutionStatusList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						height: 120,
						bindToFormLoad: true,
						columnsConfig: [
							{header: 'Monitored Runner Name', width: 225, dataIndex: 'name', tooltip: 'Execution statistics associated with the current monitored runners.'},
							{header: 'Execution Count', width: 120, dataIndex: 'executionCount', type: 'int'},
							{header: 'Next Execution', width: 120, dataIndex: 'nextRunDate', type: 'date', tooltip: 'Approximate date of the next scheduled execution.'},
							{header: 'Last Execution', width: 120, dataIndex: 'lastRunDate', type: 'date', tooltip: 'Date of the last execution.'},
							{header: 'Last Duration', width: 120, dataIndex: 'lastRunMillisecondDuration', type: 'int', tooltip: 'Millisecond duration of the last execution.'},
							{header: 'Longest Duration', width: 120, dataIndex: 'longestRunMillisecondDuration', type: 'int', tooltip: 'Millisecond duration of the longest execution.'},
							{header: 'Average Duration', width: 125, dataIndex: 'averageRunMillisecondDuration', type: 'int', tooltip: 'Average millisecond duration of monitored executions.'}
						]
					},
					{
						xtype: 'formgrid',
						title: '<div qtip="View the details associated with the current thread pools responsible for executing runners.">Thread Pool Executor Stats</div>',
						collapsible: false,
						readOnly: true,
						hideInstructions: true,
						storeRoot: 'executorServiceStatusList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						height: 125,
						bindToFormLoad: true,
						columnsConfig: [
							{header: 'Thread Pool Name', width: 225, dataIndex: 'name'},
							{header: 'Thread Prefix', width: 225, dataIndex: 'threadPrefix', hidden: true}, //hidden since it is likely the same as the thread pool name
							{header: 'Running', width: 65, dataIndex: 'running', type: 'boolean'},
							{header: 'Scheduled Tasks', width: 130, dataIndex: 'taskCount', type: 'int'},
							{header: 'Completed Tasks', width: 130, dataIndex: 'completedTaskCount', type: 'int'},
							{header: 'Active Tasks', width: 100, dataIndex: 'activeTaskCount', type: 'int'},
							{header: 'Queued Tasks', width: 110, dataIndex: 'queuedCount', type: 'int'},
							{header: 'Pool Size', width: 90, dataIndex: 'poolSize', type: 'int'},
							{header: 'Core Pool Size', width: 110, dataIndex: 'corePoolSize', type: 'int'},
							{header: 'Max Pool Size', width: 100, dataIndex: 'maxPoolSize', type: 'int', hidden: true},
							{header: 'Largest Pool Size', width: 130, dataIndex: 'largestPoolSize', type: 'int'},
							{header: 'Keep Alive Seconds', width: 135, dataIndex: 'keepAliveSeconds', type: 'int', hidden: true}
						]
					},
					{
						xtype: 'panel',
						title: 'Threads',
						layout: 'fit',
						flex: 1,
						items: [
							{fieldLabel: 'Threads', name: 'threadDump', xtype: 'textarea', readOnly: true}
						]
					}
				]
			}
		]
	},


	{
		title: 'Request Processor Stats',
		items: [
			{
				xtype: 'formpanel',
				loadValidation: false,
				labelWidth: 80,
				layout: 'vbox',
				layoutConfig: {align: 'stretch'},
				defaults: {flex: 0},
				getLoadURL: () => 'requestProcessorStatus.json',
				reload: function(params) {
					this.load({
						url: encodeURI(this.getLoadURL()),
						params: params || this.getLoadParams(this.getWindow()),
						waitMsg: 'Loading...',
						success: (form, action) => {
							this.loadJsonResult(action.result, true);
							this.fireEvent('afterload', this);
						},
						...TCG.form.submitDefaults
					});
				},
				saveResponseParameters: function() {
					const loader = new TCG.data.JsonLoader({
						waitTarget: this,
						params: {
							responseBufferSize: TCG.getChildByName(this.getTopToolbar(), 'responseBufferSize').getValue(),
							responseLimitSize: TCG.getChildByName(this.getTopToolbar(), 'responseLimitSize').getValue()
						},
						onLoad: () => this.reload()
					});
					loader.load('requestProcessorParametersSave.json');
				},
				tbar: [
					{xtype: 'sectionheaderfield', header: 'Request Processor Stats'},
					'-',
					{
						text: 'Reload',
						xtype: 'button',
						iconCls: 'table-refresh',
						handler: button => TCG.getParentFormPanel(button).reload()
					},
					'-',
					{xtype: 'label', html: '&nbsp;Response Buffer Size:&nbsp;', qtip: 'The buffer size to use for responses in bytes. This is a performance configuration. While larger buffers will tend to increase speed, they also increase memory usage per request.'},
					{xtype: 'integerfield', name: 'responseBufferSize', listeners: {specialkey: (field, e) => e.getKey() === e.ENTER && TCG.getParentFormPanel(field).saveResponseParameters()}},
					{xtype: 'label', html: '&nbsp;&nbsp;Response Limit Size:&nbsp;', qtip: 'The limit size for responses. This is an fail-safe in order to terminate serialization for large responses. This can be especially useful to terminate serialization for redundant large responses, such as when a user believes a request may succeed if retried, and, as a result, he or she attempts to retry the request several times, consuming a large amount of system resources.'},
					{xtype: 'integerfield', name: 'responseLimitSize', listeners: {specialkey: (field, e) => e.getKey() === e.ENTER && TCG.getParentFormPanel(field).saveResponseParameters()}},
					{
						text: 'Apply',
						tooltip: 'Save the specified response serialization parameters.',
						iconCls: 'run',
						handler: button => TCG.getParentFormPanel(button).saveResponseParameters()
					}
				],
				listeners: {
					afterload: formPanel => {
						TCG.getChildByName(formPanel.getTopToolbar(), 'responseBufferSize').setValue(formPanel.getFormValue('responseBufferSize'));
						TCG.getChildByName(formPanel.getTopToolbar(), 'responseLimitSize').setValue(formPanel.getFormValue('responseLimitSize'));
					}
				},
				items: [
					{
						xtype: 'panel',
						layout: 'column',
						defaults: {
							columnWidth: .32,
							layout: 'form',
							defaults: {xtype: 'integerfield', readOnly: true, anchor: '0'}
						},
						items: [
							{
								columnWidth: .23,
								labelWidth: 150,
								items: [
									{fieldLabel: 'Total Requests', name: 'totalRequestCount', qtip: 'The total number of requests received.'},
									{fieldLabel: 'Total Errors', name: 'totalErrorCount', qtip: 'The total number of requests which resulted in errors (HTTP status greater than or equal to 400).'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								columnWidth: .23,
								labelWidth: 150,
								items: [
									{fieldLabel: 'Total Processing Time', name: 'totalProcessingTime', qtip: 'The total processing time for all requests, in milliseconds.'},
									{fieldLabel: 'Average Processing Time', name: 'averageProcessingTime', qtip: 'The average processing time for all requests, in milliseconds.'}
								]
							},
							{columnWidth: .02, items: [{html: '&nbsp;'}]},
							{
								columnWidth: .50,
								labelWidth: 120,
								items: [
									{fieldLabel: 'Max Request URI', name: 'maxRequestUri', qtip: 'The request URI which took the longest time to process of any individual request.', xtype: 'textfield'},
									{fieldLabel: 'Max Request Time', name: 'maxRequestTime', qtip: 'The longest time taken for any individual request, in milliseconds.'}
								]
							}
						]
					},
					{
						xtype: 'formgrid',
						title: '<div qtip="View the details associated with the current thread pools responsible for executing runners.">Thread Pool Executor Stats</div>',
						collapsible: false,
						readOnly: true,
						hideInstructions: true,
						storeRoot: 'executorServiceStatusList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						height: 150,
						bindToFormLoad: true,
						columnsConfig: [
							{header: 'Thread Pool Name', width: 225, dataIndex: 'name'},
							{header: 'Thread Prefix', width: 225, dataIndex: 'threadPrefix', hidden: true}, //hidden since it is likely the same as the thread pool name
							{header: 'Running', width: 65, dataIndex: 'running', type: 'boolean'},
							{header: 'Scheduled Tasks', width: 130, dataIndex: 'taskCount', type: 'int'},
							{header: 'Completed Tasks', width: 130, dataIndex: 'completedTaskCount', type: 'int'},
							{header: 'Active Tasks', width: 100, dataIndex: 'activeTaskCount', type: 'int'},
							{header: 'Queued Tasks', width: 110, dataIndex: 'queuedCount', type: 'int'},
							{header: 'Pool Size', width: 90, dataIndex: 'poolSize', type: 'int'},
							{header: 'Core Pool Size', width: 110, dataIndex: 'corePoolSize', type: 'int'},
							{header: 'Max Pool Size', width: 100, dataIndex: 'maxPoolSize', type: 'int', hidden: true},
							{header: 'Largest Pool Size', width: 130, dataIndex: 'largestPoolSize', type: 'int'},
							{header: 'Keep Alive Seconds', width: 135, dataIndex: 'keepAliveSeconds', type: 'int', hidden: true}
						]
					},
					{
						xtype: 'formgrid',
						title: '<div qtip="View the details associated with each registered application server endpoint. See <code>org.apache.tomcat.util.net.AbstractEndpoint</code> for more detailed property descriptions.">Endpoint Stats</div>',
						collapsible: false,
						readOnly: true,
						hideInstructions: true,
						storeRoot: 'endpointList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						height: 150,
						bindToFormLoad: true,
						// Dynamically attach all columns from the attribute map
						useDynamicColumns: true,
						getDynamicColumns: data => data.flatMap(entity => Object.entries(entity.attributeMap))
							.distinct(([fieldName]) => fieldName)
							.map(([fieldName, value]) => [fieldName, `attributeMap.${fieldName}.value`, value.description])
							.map(([fieldName, dataIndex, description]) => ({header: TCG.camelCaseToTitle(fieldName), dataIndex: dataIndex, tooltip: description, hidden: true})),
						columnsConfig: [
							{header: 'Endpoint Name', width: 225, dataIndex: 'attributeMap.name.value', tooltip: 'The name of the endpoint.'},
							{header: 'Port', width: 100, dataIndex: 'attributeMap.port.value', tooltip: 'The port to which this endpoint listens.', type: 'int'},
							{header: 'SSL', width: 100, dataIndex: 'attributeMap.sSLEnabled.value', tooltip: 'True if SSL is enabled for this endpoint, or false otherwise.', type: 'boolean'},
							{header: 'Connections', width: 100, dataIndex: 'attributeMap.connectionCount.value', tooltip: 'The current number of concurrent connections for this endpoint.', type: 'int'},
							{header: 'Max Connections', width: 120, dataIndex: 'attributeMap.maxConnections.value', tooltip: 'The maximum number of concurrent connections for this endpoint.', type: 'int'},
							{header: 'Current Thread Count', width: 150, dataIndex: 'attributeMap.currentThreadCount.value', tooltip: 'The current number of concurrent threads active.', type: 'int'},
							{header: 'Current Threads Busy', width: 150, dataIndex: 'attributeMap.currentThreadsBusy.value', tooltip: 'The current number of concurrent threads which are processing requests.', type: 'int'},
							{header: 'Max Allowed Threads', width: 150, dataIndex: 'attributeMap.maxThreads.value', tooltip: 'The maximum allowed number of concurrent threads.', type: 'int'}
						]
					},
					{
						xtype: 'formgrid',
						title: '<div qtip="View the details associated with each registered application server endpoint. See <code>org.apache.coyote.RequestInfo</code> for more detailed property descriptions.">Request Processor Stats</div>',
						collapsible: false,
						readOnly: true,
						hideInstructions: true,
						storeRoot: 'requestProcessorList',
						viewConfig: {markDirty: false},
						autoHeight: false,
						flex: 1,
						bindToFormLoad: true,
						// Dynamically attach all columns from the attribute map
						useDynamicColumns: true,
						getDynamicColumns: data => data.flatMap(entity => Object.entries(entity.attributeMap))
							.distinct(([fieldName]) => fieldName)
							.map(([fieldName, value]) => [fieldName, `attributeMap.${fieldName}.value`, value.description])
							.map(([fieldName, dataIndex, description]) => ({header: TCG.camelCaseToTitle(fieldName), dataIndex: dataIndex, tooltip: description, hidden: true})),
						columnsConfig: [
							{header: 'Endpoint Name', width: 150, dataIndex: 'keyValueMap.type', tooltip: 'The name of the endpoint associated with this request processor.', hidden: true},
							{header: 'Processor Name', width: 110, dataIndex: 'keyValueMap.name', tooltip: 'The name of the request processor.'},
							{header: 'Worker Thread Name', width: 190, dataIndex: 'attributeMap.workerThreadName.value', tooltip: 'The name of the worker thread being used for this request processor.'},
							{header: 'Max Request URI', width: 300, dataIndex: 'attributeMap.maxRequestUri.value', tooltip: 'The URI of the request which has the longest response time.'},
							{header: 'Max Time', width: 100, dataIndex: 'attributeMap.maxTime.value', tooltip: 'The longest response time for a request in milliseconds.', type: 'int'},
							{header: 'Error Count', width: 80, dataIndex: 'attributeMap.errorCount.value', tooltip: 'The number of response codes of HTTP status 400 or greater.', type: 'int', hidden: true},
							{header: 'Remote Address', width: 100, dataIndex: 'attributeMap.remoteAddr.value', tooltip: 'The remote address for the current request.', hidden: true},
							{header: 'Current URI', width: 300, dataIndex: 'attributeMap.currentUri.value', tooltip: 'The current request URI.'},
							{header: 'Current Query String', width: 300, dataIndex: 'attributeMap.currentQueryString.value', tooltip: 'The current request query string.', hidden: true},
							{header: 'Current Time', width: 110, dataIndex: 'attributeMap.requestProcessingTime.value', tooltip: 'The elapsed processing time for the current request in milliseconds.', type: 'int', defaultSortColumn: true, defaultSortDirection: 'DESC'}
						]
					}
				]
			}
		]
	}
);


/**
 * The administrative window for displaying various system stats. See {@link Clifton.core.stats.StatsListTabs}.
 */
Clifton.core.stats.StatsListWindow = Ext.extend(TCG.app.Window, {
	id: 'coreStatsListWindow',
	title: 'System Ops Views',
	iconCls: 'doctor',
	width: 1200,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: Clifton.core.stats.StatsListTabs
	}]
});
