Clifton.core.oauth.OAuth2RegistrationWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Register OAuth2 Client',
	iconCls: 'run',
	id: 'oauth2RegistrationWindow',
	enableShowInfo: false,

	items: [{
		xtype: 'formpanel',
		instructions: 'Use this window to register a new connection from this client to selected OAuth2 Resource Server.',
		okButtonText: 'Register',
		getSaveURL: function() {
			return 'securityRegisterAsOAuth2ClientWithResource.json';
		},
		labelWidth: 120,
		items: [
			{
				fieldLabel: 'API URL',
				name: 'apiHostname',
				qtip: 'The HTTP Address of the API to be registered with. This list is populated using the values from the Endpoint Package Map in the properties file.',
				allowBlank: false,
				xtype: 'string-combo',
				stringArrayUrl: 'securityResourceRegistrationList.json',
				listeners: {
					change: function(field) {
						const val = field.getValue();
						if (TCG.isNotBlank(val)) {
							const fp = TCG.getParentFormPanel(field);
							const f = fp.getForm();
							f.findField('resourceAuthorizationUrl').setValue(val + '/j_security_check.json');
							f.findField('resourceRegistrationUrl').setValue(val + '/securityRegisterOAuth2Client.json');
						}
					}
				}
			},
			{fieldLabel: 'Authorization URL', name: 'resourceAuthorizationUrl', value: '/j_security_check.json', allowBlank: false, qtip: 'The URL that will be used to authenticate the provided Admin user with the registration server.'},
			{fieldLabel: 'Admin Username', name: 'adminUserName', allowBlank: false, qtip: 'The username of a user that is an Administrator on the server to be registered with.'},
			{fieldLabel: 'Admin Password', name: 'adminPassword', inputType: 'password', allowBlank: false, qtip: 'The password for the Admin user'},
			{fieldLabel: 'Registration URL', name: 'resourceRegistrationUrl', value: '/securityRegisterOAuth2Client.json', allowBlank: false, qtip: 'The URL that will be used to register this server with the remote API.'},
			{fieldLabel: 'Signature Algorithm', name: 'signatureAlgorithm', value: 'HMAC256', allowBlank: false, qtip: 'The algorithm that will be used to sign the access token requests. (Only applicable for JWT grant type)'},
			{fieldLabel: 'Scope', name: 'scope', value: 'ACT_AS_USER', qtip: 'Defines permissions that this server will be authorized on the remote API. The default is ACT_AS_USER which allows this server to impersonate users on the API.'},
			{fieldLabel: 'Grant Type', name: 'grantType', value: 'urn:ietf:params:oauth:grant-type:jwt-bearer', allowBlank: false, qtip: 'Defines the method that this server will use to obtain an access token from the remote API.'}
		]
	}]
});
