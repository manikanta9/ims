Clifton.core.oauth.OAuth2ClientWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'OAuth Client Details',
	iconCls: 'lock',
	id: 'oauth2ClientWindow',
	enableShowInfo: false,
	closeWindowTrail: true,
	hideOKButton: true,
	cancelButtonText: 'Exit',
	width: 700,
	setTitle: function(title, iconCls) {
		if (this.params && this.params.titlePrefix) {
			title = this.params.titlePrefix + ' - ' + title;
		}
		TCG.Window.superclass.setTitle.call(this, title, iconCls);
	},

	items: [{
		xtype: 'formpanel',
		readOnly: true,
		loadValidation: false,
		labelWidth: 160,
		url: 'securityOauthClientDetail.json',
		getLoadURL: function() {
			const win = this.getWindow();
			if (win.params && win.params.loadUrl) {
				return win.params.loadUrl;
			}
			return this.url;
		},
		items: [
			{fieldLabel: 'Client ID', name: 'clientId'},
			{fieldLabel: 'Incoming', xtype: 'checkbox', name: 'incomingConnection', boxLabel: 'Checked if this is OAuth Server (as opposed to Client) for this Connection'},
			{fieldLabel: 'Client URL', name: 'additionalInformation.clientUrl', qtip: 'The URL of the client that will consume the resources of the Resource Host'},
			{fieldLabel: 'Resource URL', name: 'additionalInformation.resourceUrl', qtip: 'The URL of the API to be consumed'},
			{fieldLabel: 'Scopes', name: 'scope', qtip: 'The authorized permissions of this client'},
			{fieldLabel: 'Grant Types', name: 'authorizedGrantTypes', qtip: 'The methods that this client is authorized to use to obtain access tokens'},
			{fieldLabel: 'Signature Algorithm', name: 'signatureAlgorithm', qtip: 'The algorithm that will be used to sign access token requests. (Only applicable to JWT grant type)'},
			{fieldLabel: 'Signature Key Checksum', name: 'signatureKeyFileChecksum', qtip: 'The SHA256 hash of the signature key'},
			{fieldLabel: 'Signature Key File Location', name: 'signatureKeyFileLocation', qtip: 'The location of the signature key file'}
		]
	}]
});
