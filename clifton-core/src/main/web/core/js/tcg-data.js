Ext.ns('TCG.data');


Ext.data.HttpProxy.prototype.onRead = Ext.data.HttpProxy.prototype.onRead.createInterceptor(function(action, o, response) {
	// store original element on the reader so that we can re-execute this action (after re-login)
	o.reader.parentCt = o;
});


/**
 * Method override to handle server exceptions
 */
Ext.override(Ext.data.JsonReader, {
	// don't error on undefined nested objects; is this too slow?
	createAccessor: function(expr) {
		switch (true) {
			case Ext.isEmpty(expr):
				return Ext.emptyFn;
			case Ext.isFunction(expr):
				return expr;
			case /[\[.]/.test(expr):
				return obj => TCG.getValue(expr, obj);
			default:
				return obj => obj[expr];
		}
	},

	read: function(response) {
		const json = response.responseText;
		const o = Ext.decode(json);
		if (!o) {
			throw {message: 'JsonReader.read: Json object not found'};
		}
		if (!o.success) { // CHANGE: added the if statement to handle server side exceptions
			TCG.data.ErrorHandler.handleFailure(this, o);
			return o;
		}
		return this.readRecords(o);
	},

	readRecords: function(o) {
		this.jsonData = o;
		if (o.metaData) {
			this.onMetaChange(o.metaData);
		}
		const s = this.meta;
		let v;

		// CHANGE BEGIN: changed the following line to support results with no root node (means 0 rows and empty array for root)
		//var root = this.getRoot(o), c = root.length, totalRecords = c, success = true;
		let root = this.getRoot(o);
		const c = root ? root.length : 0;
		let totalRecords = c, success = true;
		if (TCG.isNull(root)) {
			root = [];
		}
		// CHANGE END

		if (s.totalProperty) {
			v = parseInt(this.getTotal(o), 10);
			if (!isNaN(v)) {
				totalRecords = v;
			}
		}
		if (s.successProperty) {
			v = this.getSuccess(o);
			if (v === false || v === 'false') {
				success = false;
			}
		}

		return {
			success: success,
			records: this.extractData(root, true),
			totalRecords: totalRecords
		};
	},

	// allows to re-execute original request (after successful login)
	loadJsonResult: function(result) {
		const o = this.parentCt; // saved during Ext.data.HttpProxy.onRead
		if (o) {
			o.request.callback.call(o.request.scope, this.readRecords(result), o.request.arg, true);
		}
		else {
			TCG.showError('Cannot re-execute original request.<br />Original request is missing.<br /><br />Please retry your last action.', 'Execute Error');
		}
	}
});


// DataTableReader reads array data into JSON objects
TCG.data.DataTableReader = Ext.extend(Ext.data.JsonReader, {
	extractValues: function(data, items, len) {
		let f;
		const values = {};
		for (let j = 0; j < len; j++) {
			f = items[j];
			const v = data[j];
			values[f.name] = f.convert((v !== undefined) ? v : f.defaultValue, data);
		}
		return values;
	}
});


// use our syntax for remoteSort parameter that supports multiple fields/directions
Ext.data.Store.prototype.fixSortingParams = function(store, options) {
	const si = this.sortInfo;
	if (si) {
		let o = si.field;
		// if search field for the filter is defined, use it for sorting
		const field = this.fields.get(o);
		// Apply the filter in orderBy only if the field is not marked with nonPersistentField = true.
		// Non persisted fields are ignored and sort will be client side.
		if (!TCG.isTrue(field.nonPersistentField)) {
			const filter = (field && (field.searchFieldName || field.sortFieldName)) ? field : field.filter;
			if (filter) {
				if (filter.sortFieldName) {
					o = filter.sortFieldName;
				}
				else if (filter.searchFieldName) {
					o = filter.searchFieldName;
				}
			}
			if (o.indexOf('.') !== -1 && TCG.isFalse(o.doNotTruncateBeanPath)) {
				// cannot have . in the sort field name: use the last part
				o = o.substring(o.lastIndexOf('.') + 1);
			}

			const d = si.direction;
			if (TCG.isNotNull(d)) {
				o += ':' + d;
			}
			const pn = this.paramNames;
			if (!options.params) {
				options.params = {};
			}
			options.params[pn.sort] = o;
		}
	}
};
Ext.data.Store.prototype.constructor = Ext.data.Store.prototype.constructor.createSequence(function() {
	this.on('beforeload', this.fixSortingParams, this);
});


TCG.data.DataTableStore = Ext.extend(Ext.data.Store, {
	paramNames: {
		sort: 'orderBy'
	},
	constructor: function(config) {
		const conf = Ext.apply({
			root: 'data.rows',
			totalProperty: 'data.totalRows'
		}, config);
		TCG.data.DataTableStore.superclass.constructor.call(this, Ext.apply(conf, {
			reader: new TCG.data.DataTableReader(conf)
		}));
		this.addListener('exception', TCG.data.ErrorHandler.handleException);
		this.addListener('load', function(store, records, opts) {
			store.remoteSort = store.isPartialData();
		});
	},
	isPartialData: function() {
		return (this.getTotalCount() > this.getCount());
	}
});

TCG.data.DataTableGroupingStore = Ext.extend(Ext.data.GroupingStore, {
	paramNames: {
		sort: 'orderBy'
	},
	constructor: function(config) {
		const conf = Ext.apply({
			root: 'data.rows',
			totalProperty: 'data.totalRows',
			id: 'id',
			autoLoad: false
		}, config);
		TCG.data.DataTableGroupingStore.superclass.constructor.call(this, Ext.apply(conf, {
			reader: new TCG.data.DataTableReader(conf)
		}));
		this.addListener('exception', TCG.data.ErrorHandler.handleException);
		this.addListener('load', function(store, records, opts) {
			store.remoteSort = store.forceRemoteSortDataTable || store.isPartialData();
		});
	},
	isPartialData: function() {
		return (this.getTotalCount() > this.getCount());
	}
});

Ext.Ajax.defaultHeaders = {
	'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
};

/**
 * Loads a single JSON record from the server and passes it to the override of onLoad(record, conf)
 */
TCG.data.JsonLoader = function(conf) {
	this.root = 'data';
	Ext.apply(this, conf);
};

TCG.data.JsonLoader.prototype = {
	load: function(url, componentScope, applicationName, applicationScope) {
		this.doLoad(url, false, componentScope, applicationName, applicationScope);
	},
	// WARNING: avoid using Synchronous calls because they will likely be deprecated in future browser versions
	loadSynchronous: function(url, componentScope, applicationName, applicationScope) {
		this.doLoad(url, true, componentScope, applicationName, applicationScope);
	},
	doLoad: function(url, synchronousCall, componentScope, applicationName, applicationScope) {
		// Create URL based on passed in componentScope, applicationName, or URL prefix, inferring application properties from waitTarget if other arguments are not assigned
		const loaderUrl = TCG.getApplicationUrl(url, componentScope || this.waitTarget, applicationName, applicationScope);
		let params = this.params;
		const jsonData = this.jsonData;
		let formData;
		let paramNames, i, paramKey;
		if (this.isUseWaitMsg()) {
			this.getMsgTarget().mask(this.getWaitMsg());
		}
		if (synchronousCall) {
			// ExtJS doesn't support synchronous calls? our custom impl
			const xhr = new XMLHttpRequest();
			// FF 12+ doesn't support timeout for synchronous requests and errors out during open
			// IE doesn't allow setting this property
			// SKIP completely
			// ALSO: FF doesn't support passing params: use URL for parameters
			//try {
			//	xhr.timeout = this.timeout ? this.timeout : 20000;
			//} catch(e) {}
			try {
				// Convert object to URLSearchParams
				if (typeof params === 'object') {
					formData = new URLSearchParams();
					paramNames = Object.getOwnPropertyNames(params);
					for (i = 0; i < paramNames.length; i++) {
						paramKey = paramNames[i];
						formData.append(paramKey, params[paramKey]);
					}
					params = formData;
				}
				xhr.open('POST', encodeURI(loaderUrl), false); // SYNCHRONOUS
				if (loaderUrl.startsWith('json/')) {
					xhr.setRequestHeader('Content-Type', 'application/json;charset=UTF-8');
				}
				else {
					xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded; charset=UTF-8');
				}
				xhr.send(params || null);
				if (xhr.status === 200 || xhr.status === 0) {
					this.success(xhr);
				}
				else {
					this.failure(xhr, {url: loaderUrl});
				}
			}
			catch (e) {
				TCG.data.ErrorHandler.handleException(null, null, null, {url: loaderUrl}, xhr, e);
			}
		}
		else {
			if (loaderUrl.startsWith('json/')) {
				TCG.Ajax.request({
					success: this.success,
					failure: this.failure,
					headers: {'Content-Type': 'application/json'},
					params: params,
					jsonData: JSON.stringify(jsonData),
					scope: this,
					timeout: this.timeout ? this.timeout : 40000,
					method: 'POST',
					url: encodeURI(loaderUrl)
				});
			}
			else {
				Ext.Ajax.request({
					success: this.success,
					failure: this.failure,
					params: params,
					scope: this,
					timeout: this.timeout ? this.timeout : 40000,
					method: 'POST',
					url: encodeURI(loaderUrl)
				});
			}
		}
	},

	// allows to re-execute original request (after successful login)
	loadJsonResult: function(result) {
		if (this.conf && this.conf.root) {
			this.root = this.conf.root;
		}
		const record = this.getNode(result, this.root);
		this.onLoad.call(this.scope || this, record, this.conf);
	},
	getNode: function(root, propertyName) {
		return this.getNodeRetrievalFunction(propertyName)(root);
	},
	getNodeRetrievalFunction: function(propertyName) {
		return root => root[propertyName];
	},
	isUseWaitMsg: function() {
		return !!(this.waitMsg || this.waitTarget);
	},
	getWaitMsg: function() {
		return this.waitMsg || 'Loading...';
	},
	getMsgTarget: function() {
		let t = this.waitTarget;
		if (t) {
			if (t.getEl) {
				t = t.getEl();
			}
		}
		else {
			t = TCG.data.LoadMaskTarget;
		}
		return t;
	},

	onLoad: function(record, conf) {
		// empty
	},
	onFailure: function(obj, result) {
		return false; // return true to cancel standard processing/error message
	},
	success: function(response, opts) {
		if (this.isUseWaitMsg()) {
			this.getMsgTarget().unmask();
		}
		const result = Ext.decode(response.responseText);
		if (result.success) {
			if (this.conf && this.conf.root) {
				this.root = this.conf.root;
			}
			let record = result;
			if (this.root !== '') {
				record = this.getNode(result, this.root);
			}
			this.onLoad.call(this.scope || this, record, this.conf);
		}
		else {
			if (this.onFailure(this, result) !== true) {
				TCG.data.ErrorHandler.handleFailure(this, result);
			}
		}
	},
	failure: function(response, opts) {
		const f = this.onFailure(this, Ext.decode(response.responseText));
		if (this.isUseWaitMsg()) {
			this.getMsgTarget().unmask();
		}
		if (f !== true) {
			TCG.data.ErrorHandler.handleException(null, null, null, opts, response);
		}
	}
};


TCG.data.JsonStore = function(conf) {
	const newConf = {root: 'data', id: 'id', autoLoad: false};
	Ext.apply(newConf, conf);
	TCG.data.JsonStore.superclass.constructor.call(this, newConf);
	this.addListener('exception', TCG.data.ErrorHandler.handleException);
	this.addListener('load', function(store, records, opts) {
		store.remoteSort = store.forceRemoteSort || store.isPartialData();
	});
};

Ext.extend(TCG.data.JsonStore, Ext.data.JsonStore, {
	paramNames: {
		sort: 'orderBy'
	},
	isPartialData: function() {
		return (this.getTotalCount() > this.getCount());
	}
});


TCG.data.ArrayStore = Ext.extend(Ext.data.ArrayStore, {
	// Extend with default fields
	constructor: function(config) {
		const cfg = config || {};
		if (TCG.isNull(cfg.fields)) {
			cfg.fields = ['id', 'name', 'description'];
		}
		return TCG.callSuper(this, 'constructor', [cfg]);
	}
});
Ext.reg('arraystore', TCG.data.ArrayStore);


// Applies advanced filtering support when used in a combo - default array store appears to only do starts with
// which works well only for small lists.  For larger lists, contains search is better.
// Also - for these should set typeAhead to false on the combo so that it doesn't try to select the first match automatically
TCG.data.ArrayStoreWithFilter = Ext.extend(Ext.data.ArrayStore, {
	// Extend with default fields
	constructor: function(config) {
		const cfg = config || {};
		if (TCG.isNull(cfg.fields)) {
			cfg.fields = ['id', 'name', 'description'];
		}
		return TCG.callSuper(this, 'constructor', [cfg]);
	},
	filter: function(property, value, anyMatch, caseSensitive, exactMatch) {
		// Need to set anyMatch = true, caseSensitive = false, and exactMatch = false
		return TCG.callSuper(this, 'filter', [property, value, true, false, false]);
	}
});
Ext.reg('arraystore-with-filter', TCG.data.ArrayStoreWithFilter);


TCG.data.GroupingStore = function(conf) {
	const newConf = {root: 'data', id: 'id', autoLoad: false};
	Ext.apply(newConf, conf);
	newConf.reader = new Ext.data.JsonReader(newConf);
	TCG.data.GroupingStore.superclass.constructor.call(this, newConf);
	this.addListener('exception', TCG.data.ErrorHandler.handleException);
};

Ext.extend(TCG.data.GroupingStore, Ext.data.GroupingStore, {
	paramNames: {
		sort: 'orderBy'
	},
	isPartialData: function() {
		return (this.getTotalCount() > this.getCount());
	}
});


//gets requested data from DATA_CACHE stored under specified dataKey;
//if dataKey is undefined then doesn't do caching
//if not cached, executes synchronous request for the specified url;
//optionally can pass config for JsonLoader
// WARNING: avoid using Synchronous calls because they will likely be deprecated in future browser versions
TCG.data.getData = function(url, componentScope, dataKey, config) {
	let data;
	let cacheDataKey = dataKey;
	const cache = cacheDataKey != null;
	if (cache) {
		cacheDataKey += `_Application=${TCG.getApplicationName(url, componentScope) || 'Internal'}`;
		data = TCG.CacheUtils.get('DATA_CACHE', cacheDataKey);
	}
	if (TCG.isNull(data)) {
		// execute a synchronous request and cache result
		const loader = new TCG.data.JsonLoader(Ext.apply({
			onLoad: function(record, conf) {
				data = record;
				if (cache) {
					TCG.CacheUtils.put('DATA_CACHE', record, cacheDataKey);
				}
			}
		}, config));
		loader.loadSynchronous(url, componentScope);
	}
	return data;
};


// Asynchronously retrieves JSON data from the specified url. Optional config parameters for Ext.Ajax.request
TCG.data.getDataPromise = function(url, componentScope, config) {
	return TCG.data.getDataPromiseUsingCaching(url, componentScope, null, config);
};
// First looks up the data in the cache using the specified cacheKey. If not found, asynchronously retrieves JSON data from the specified url
// and stores it in cache for future calls. Optional config parameters for Ext.Ajax.request
TCG.data.getDataPromiseUsingCaching = function(url, componentScope, cacheKey, config) {
	return new Promise(function(resolve, reject) {
		let data;
		let cacheDataKey = cacheKey;
		const cache = cacheDataKey != null;
		if (cache) {
			cacheDataKey += `_Application=${TCG.getApplicationName(url, componentScope) || 'Internal'}`;
			data = TCG.CacheUtils.get('DATA_CACHE', cacheDataKey);
		}
		if (TCG.isNull(data)) {
			// execute asynchronous request and cache result
			const loader = new TCG.data.JsonLoader(Ext.apply({
				onLoad: function(record, conf) {
					data = record;
					if (cache) {
						TCG.CacheUtils.put('DATA_CACHE', record, cacheDataKey);
					}
					resolve(data);
				},
				onFailure: function(obj, result) {
					reject(result);
				}
			}, config));
			loader.load(url, componentScope);
		}
		else {
			resolve(data);
		}
	});
};


/**
 * Asynchronously calls the specified URL and passes "responseText" of response object back to the callBackFunction on success.
 * This method is intended for use with services that return a single value: number, string, date, etc.
 */
TCG.data.getDataValue = function(url, componentScope, callBackFunction, scope, params, timeout) {
	const applicationUrl = TCG.getApplicationUrl(url, componentScope);
	Ext.Ajax.request({
		success: function(response, opts) {
			const result = response.responseText;
			if (!TCG.data.isErrorResponse(result, url)) {
				callBackFunction.call(scope || this, result);
			}
		},
		failure: function(response, opts) {
			TCG.data.ErrorHandler.handleException(null, null, null, opts, response);
		},
		params: params,
		scope: scope,
		timeout: timeout || 40000,
		method: 'POST',
		url: encodeURI(applicationUrl)
	});
};
TCG.data.getDataValuePromise = function(url, componentScope, scope, params, timeout) {
	const applicationUrl = TCG.getApplicationUrl(url, componentScope);
	return new Promise(function(resolveCallback) {
		Ext.Ajax.request({
			success: function(response, opts) {
				const result = response.responseText;
				if (!TCG.data.isErrorResponse(result, applicationUrl)) {
					resolveCallback.call(scope || this, result);
				}
			},
			failure: function(response, opts) {
				TCG.data.ErrorHandler.handleException(null, null, null, opts, response);
			},
			params: params,
			scope: scope,
			timeout: timeout || 40000,
			method: 'POST',
			url: encodeURI(applicationUrl)
		});
	});
};
TCG.data.getDataValuePromiseUsingCaching = function(url, componentScope, cacheKey, scope, params, timeout) {
	const applicationUrl = TCG.getApplicationUrl(url, componentScope);
	return new Promise(function(resolveCallback) {
		let data;
		let cacheDataKey = cacheKey;
		const cache = cacheDataKey != null;
		if (cache) {
			cacheDataKey += `_Application=${TCG.getApplicationName(url, componentScope) || 'Internal'}`;
			data = TCG.CacheUtils.get('DATA_CACHE', cacheDataKey);
		}
		if (TCG.isNull(data)) {
			Ext.Ajax.request({
						success: function(response, opts) {
							const result = response.responseText;
							if (cache) {
								TCG.CacheUtils.put('DATA_CACHE', result, cacheDataKey);
							}
							if (!TCG.data.isErrorResponse(result, applicationUrl)) {
								resolveCallback.call(scope || this, result);
							}
						},
						failure: function(response, opts) {
							TCG.data.ErrorHandler.handleException(null, null, null, opts, response);
						},
						params: params,
						scope: scope,
						timeout: timeout || 40000,
						method: 'POST',
						url: encodeURI(applicationUrl)
					}
			);
		}
		else {
			resolveCallback.call(scope || this, data);
		}
	});
};


// allows to register custom windows for specific error types
TCG.data.ErrorWindows = [];

TCG.data.isErrorResponse = function(result, url) {
	// check if response is a JSON error (relatively efficient impl)
	if (result.length > 10 && '{' === result.substr(0, 1) && result.indexOf('"success":false') !== -1) {
		const objResult = Ext.util.JSON.decode(result);
		if (objResult.validationError === true) {
			const classOverride = TCG.data.ErrorWindows[objResult.errorType];
			if (classOverride) {
				TCG.createComponent(classOverride, {data: objResult});
			}
			else {
				TCG.showError(objResult.message, 'Validation Error');
			}
		}
		else {
			TCG.showError('Error sending server request to:<br /><br />' + (url ? url : 'UNKNOWN URL') + '<br /><br />' + objResult.message, 'Server Error');
		}
		return true;
	}
	return false;
};


TCG.data.ErrorHandler = function() {
	// new class
};
TCG.data.ErrorHandler.handleException = function(proxy, source, action, o, response, e) {
	const status = response != null ? response.status : 'CANNOT READ STATUS';
	if (TCG.isNotNull(status)) {
		let msg = 'Error sending server request to:<br /><br />' + (o ? o.url : 'UNKNOWN URL') + '<br /><br />';
		let title = 'Server Error';
		if (status === 403) {
			msg += 'You do not have permission to access requested resource.';
			title = 'Access Denied';
		}
		else if (status === 404) {
			msg += 'Cannot locate requested resource.';
			title = 'Page Not Found';
		}
		else if (status === 500) {
			msg += 'The server encountered an internal error that prevented it from fulfilling this request.';
		}
		else {
			if (e) {
				msg += e;
				title = 'Client Side Script Error';
			}
			else {
				msg += 'Response status: ' + (response != null ? response.statusText : 'NO RESPONSE');
			}
		}
		TCG.showError(msg, title);
	}
	// skip other errors: handled by Ext.data.JsonReader.read
};


// obj - original object that issued request before requiring relogin; should implement 'loadJsonResult(result)' method to populate itself after successful login
TCG.data.ErrorHandler.handleFailure = function(obj, result, msg) {
	if (result.authenticationRequired) {
		if (TCG.data.AuthHandler.isLoginWindowActive()) {
			// TODO: there could be multiple requests/originalCt's and all of them need to be reloaded on success: validation, detail, list (interest rate detail)
			return;
		}
		TCG.data.AuthHandler.promptLogin(obj);
	}
	else if (result.response && result.response.status === 403) {
		TCG.showError('You do not have permission to access requested resource.', 'Access Denied');
	}
	else if (result.response && result.response.status === 404) {
		TCG.showError('Resource not Found. The requested resource is not available.', 'Page not Found');
	}
	else {
		if (result.validationError === true) {
			const classOverride = TCG.data.ErrorWindows[result.errorType];
			if (classOverride) {
				TCG.createComponent(classOverride, {data: result});
				return;
			}
			if (result.errorType === 'UserIgnorableValidationException' && !TCG.isBlank(result.ignoreExceptionUrl) && obj.load) {
				Ext.Msg.confirm('Validation Error(s)', `${result.message}<br/><br/><b>${result.confirmationMessage}</b>`, function(a) {
					if (a === 'yes') {
						// The ignoreExceptionUrl will not contain any params defined in the initial object url. Adding the url params at this level
						// would be difficult because this function is called from multiple different components. TCG.form.submitDefaults for an example of
						// original load params being added to the ignoreExceptionUrl.
						obj.load(result.ignoreExceptionUrl);
					}
				});
				return;
			}
		}
		if (!msg) {
			if (result.message) {
				msg = result.message;
				if (msg && msg.replace) {
					msg = msg.replace(/(\r\n|\n|\r)/gm, '<br />');
				}
			}
			else if (result.errors) {
				msg = 'Please correct the following error(s):<br/><br/>';
				for (let i = 0; i < result.errors.length; i++) {
					msg += '<li>' + result.errors[i].msg;
				}
			}
			else {
				msg = 'UNKNOWN RESULT FORMAT: ' + result;
			}
		}
		TCG.showError(msg, 'Validation Error(s)');
	}
};


/**
 * The login window component.
 */
TCG.data.LoginWindow = Ext.extend(function(config) {
	const conf = Object.assign(
			{
				id: 'loginWindow',
				title: 'Login',
				iconCls: 'key',
				modal: true,
				closable: false,
				minimizable: false,
				maximizable: false,
				centerAlign: true,
				width: 400,
				height: 200,

				originalCt: null,
				callback: Ext.emptyFn,

				items: [{
					xtype: 'formpanel',
					defaults: {width: 200},
					instructions: 'Please enter your \'User Name\', \'Password\' and click \'Login\' to continue.',
					loadValidation: false,
					items: [
						{fieldLabel: 'User Name', name: 'j_username', allowBlank: false},
						{fieldLabel: 'Password', name: 'j_password', allowBlank: false, inputType: 'password'}
					]
				}],
				buttons: [{
					id: 'login',
					text: 'Login',
					type: 'submit',
					handler: () => {
						const form = this.win.items.get(0).getForm();
						if (form.isValid()) {
							form.submit({
								url: 'j_security_check.json',
								waitMsg: 'Authenticating...',
								success: (submittedForm, action) => {
									// close the login window and process original request
									this.win.close();

									this.callback(action.result);

									// Process original request
									if (this.originalCt && this.originalCt.loadJsonResult) {
										this.originalCt.loadJsonResult(action.result);
									}
								},
								failure: (submittedForm, action) => {
									let msg = `Unknown login failure during authentication against: <b>${action.getUrl()}</b>`;
									if (action.result && action.result.loginFailed) {
										msg = 'Invalid user name and/or password.';
										if (!TCG.isBlank(action.result.errorMessage)) {
											msg = action.result.errorMessage;
										}
										if (action.result.redirectWindow) {
											// close the login window and open new window
											this.win.close();
											TCG.createComponent(action.result.redirectWindow, Ext.apply({}, action.result.redirectWindowConfig));
										}
									}
									else if (action.response.status === 404) {
										msg = `Authentication URL is not found: <b>${action.getUrl()}</b>`;
									}
									else if (action.response.isTimeout) {
										msg = 'Communication failure. The server is not responding.';
									}
									else {
										msg += `<br /><br /><b>Response Text:</b><br /><br />${action.response.responseText}`;
									}
									TCG.showError(msg, 'Login Failed');
								}
							});
						}
						else {
							TCG.showError('Please enter valid user name and password.', 'Validation Error');
						}
					}
				}],
				keys: [{
					key: [10, 13],
					fn: (o, event) => this.buttons[0].handler()
				}],
				windowOnShow: win => TCG.getChildByName(this.win, 'j_username').focus(false, 250)
			},
			config
	);
	TCG.data.LoginWindow.superclass.constructor.call(this, conf);
}, TCG.app.Window);


TCG.data.AuthHandler = new class {

	constructor() {
		this.loginListeners = [];
		this.logoutListeners = [];
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	initLogin(currentUserUri, callback) {
		const userLoader = new TCG.data.JsonLoader({
			onLoad: user => {
				// Store the current user in the context
				TCG.setCurrentUser(user);
				callback(user);
				this.onLogin();
			},
			onFailure: (loader, result) => {
				// Use custom handling for authentication prompt
				if (result.authenticationRequired) {
					this.showLoginPrompt(loader);
					return true;
				}
			}
		});
		userLoader.load(currentUserUri);
	}


	promptLogin(originalCt) {
		return this.showLoginPrompt(originalCt)
				.then(response => this.onLogin());
	}


	logout() {
		const app = TCG.app.app;
		app.closeAllWindows();
		if (app.getWindowCount() === 0) {
			Ext.Ajax.request({
				success: () => {
					this.logoutListeners.forEach(listener => listener());
					Ext.Msg.confirm('Close Window?', 'You were successfully logged out from the system.<br/><br/>Would you like to close the browser window?', answer => {
						if (answer === 'yes') {
							window.close();
						}
					});
				},
				// TODO: 404, etc.
				failure: (response, opts) => TCG.showError('Error during logout.', 'Logout Error'),
				timeout: 10000,
				method: 'POST',
				url: 'logout.json'
			});
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	addLoginListener(callback, insertAtFront) {
		this.loginListeners[insertAtFront ? 'unshift' : 'push'](callback);
	}


	addLogoutListener(callback, insertAtFront) {
		this.logoutListeners[insertAtFront ? 'unshift' : 'push'](callback);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	isLoginWindowActive() {
		return !!TCG.app.app.getWindow('loginWindow');
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @private
	 */
	showLoginPrompt(originalCt) {
		return new Promise((resolve, reject) => new TCG.data.LoginWindow({originalCt, callback: response => resolve(response.data)}));
	}


	/**
	 * @private
	 */
	onLogin() {
		// Run listeners
		this.loginListeners.forEach(listener => listener());
	}
};


TCG.data.StringArrayToArrayStore = function(array) {
	const newArray = [];
	TCG.each(array, function(x) {
		if (typeof x !== 'string') {
			TCG.showError('Illegal Argument: TCG.StringArrayToArrayStore Excepts String Arrays Only');
		}
		newArray.push([x, x]);
	});
	return new Ext.data.ArrayStore({
		fields: ['value', 'name'],
		data: newArray
	});
};

TCG.data.StoreCookie = function(key, value, expiration) {
	Ext.util.Cookies.set(key, value, expiration);
};

TCG.data.GetCookie = function(key) {
	return Ext.util.Cookies.get(key);
};


/**
 * The generic load mask target object.
 *
 * This object proxies calls to the {@link Ext.MessageBox} object to provide full-page modal blocking, even when other floating windows exist.
 */
TCG.data.LoadMaskTarget = new class {
	constructor() {
		this.messageBox = Ext.MessageBox;
	}


	mask(message, title = 'Please Wait...') {
		this.messageBox.wait(message, title);
	}


	unmask() {
		this.messageBox.updateProgress(1);
		this.messageBox.hide();
	}
};

/*
 * Override the form upload function so that it can properly handle JSON responses which are invalid HTML.
 *
 * ExtJS performs form uploads by using standard HTML forms with the attribute 'target="<frame>"', where "<frame>" is some hidden iframe on the page. The browser will render the
 * contents of the response in this frame, whose innerHTML will be read and interpreted by ExtJS as the response. This typically works properly with the standard response
 * Content-Type ("text/html") as long as the response body is not manipulated by the browser's HTML parser. However, it breaks with responses that are interpreted as invalid HTML.
 * One of the most common examples of this is escaped "/" characters in self-closing tags (e.g., "<br\/>") for string values. Given that the tag with the backslash is not
 * recognized as a self-closing tag, the browser DOM handler will automatically insert a separate closing tag at the end of the response, eventually rendering the response as
 * invalid JSON.
 *
 * We fix this issue by changing the response type to "application/json", which causes the browser to wrap the response with a "<pre>" tag and thus not interpret the response as
 * HTML. ExtJS does not handle this "<pre>" tag by default, so we override this function here to unwrap the "<pre>" tag if it exists. This works as of Chrome 74 and Firefox 66.
 */
Ext.override(Ext.data.Connection, {
	doFormUpload: function(o, ps, url) {
		const id = Ext.id();
		const doc = document;
		const frame = doc.createElement('iframe');
		const form = Ext.getDom(o.form);
		const hiddens = [];
		let hd;
		const encoding = 'multipart/form-data';
		const buf = {
			target: form.target,
			method: form.method,
			encoding: form.encoding,
			enctype: form.enctype,
			action: form.action
		};

		/*
		 * Originally this behaviour was modified for Opera 10 to apply the secure URL after
		 * the frame had been added to the document. It seems this has since been corrected in
		 * Opera so the behaviour has been reverted, the URL will be set before being added.
		 */
		Ext.fly(frame).set({
			id: id,
			name: id,
			cls: 'x-hidden',
			src: Ext.SSL_SECURE_URL
		});

		doc.body.appendChild(frame);

		// This is required so that IE doesn't pop the response up in a new window.
		if (Ext.isIE) {
			document.frames[id].name = id;
		}

		Ext.fly(form).set({
			target: id,
			method: POST,
			enctype: encoding,
			encoding: encoding,
			action: url || buf.action
		});

		// add dynamic params
		Ext.iterate(Ext.urlDecode(ps, false), function(k, v) {
			hd = doc.createElement('input');
			Ext.fly(hd).set({
				type: 'hidden',
				value: v,
				name: k
			});
			form.appendChild(hd);
			hiddens.push(hd);
		});

		function cb() {
			const me = this;
			// bogus response object
			const r = {
				responseText: '',
				responseXML: null,
				argument: o.argument
			};
			let doc;
			let firstChild;

			try {
				doc = frame.contentWindow.document || frame.contentDocument || WINDOW.frames[id].document;
				if (doc) {
					if (doc.body) {
						firstChild = doc.body.firstChild || {};
						if (/textarea/i.test(firstChild.tagName)) { // json response wrapped in textarea
							r.responseText = firstChild.value;
						}
						// BEGIN EDIT
						else if (/pre/i.test(firstChild.tagName)) { // json response wrapped in <pre> tag
							r.responseText = firstChild.innerText;
						}
						// END EDIT
						else {
							r.responseText = doc.body.innerHTML;
						}
					}
					//in IE the document may still have a body even if returns XML.
					r.responseXML = doc.XMLDocument || doc;
				}
			}
			catch (e) {
			}

			Ext.EventManager.removeListener(frame, LOAD, cb, me);

			me.fireEvent(REQUESTCOMPLETE, me, r, o);

			function runCallback(fn, scope, args) {
				if (Ext.isFunction(fn)) {
					fn.apply(scope, args);
				}
			}

			runCallback(o.success, o.scope, [r, o]);
			runCallback(o.callback, o.scope, [o, true, r]);

			if (!me.debugUploads) {
				setTimeout(function() {
					Ext.removeNode(frame);
				}, 100);
			}
		}

		Ext.EventManager.on(frame, LOAD, cb, this);
		form.submit();

		Ext.fly(form).set(buf);
		Ext.each(hiddens, function(h) {
			Ext.removeNode(h);
		});
	}
});

/**
 * This function returns a value from a derived cache or by computing it.
 *
 * The cache used is the property of the provided cache scope with the cache name provided.
 * If the cache does not exist, it is created prior to being used. The result of the compute function
 * will be cached after computed if the key, cache scope, and cache name are all provided.
 *
 * @param key - key for the value from cache
 * @param cacheScope - the entity the cache is a property of
 * @param cacheName - the name of the cache property on the cache scope
 * @param computeFunction - the function that can be invoked to compute the function
 * @returns {Promise<*>}
 */
TCG.data.getFromCacheOrCompute = async function(key, cacheScope, cacheName, computeFunction) {
	let cache = void 0;
	if (TCG.isNotBlank(key)) {
		if (cacheScope && cacheName) {
			cache = cacheScope[cacheName];
			if (!cache) {
				cache = {};
				cacheScope[cacheName] = cache;
			}
			if (cache[key]) {
				return cache[key];
			}
		}
	}

	if (computeFunction && typeof computeFunction === 'function') {
		const result = await computeFunction();
		if (result && cache) {
			cache[key] = result;
		}
		return result;
	}

	return void 0;
};
