Ext.ns('TCG.grid');


//'pivotgrid' with our specific support for loading data
TCG.grid.PivotGrid = Ext.extend(TCG.grid.GridPanel, {
	measureField: 'OVERRIDE_ME', // Data Field that is pivoted
	measureFieldRenderer: function(value) { // Data Field Value Renderer - Uses Default Number Formatting
		return TCG.renderAmount(value);
	},
	aggregator: 'sum', // How Data will be transformed (defaults to sum - supports avg, min, max, and count)
	leftAxis: [], // Override for Row Group Columns
	topAxis: [], // Override for Pivot Columns


	initComponent: function() {
		const cols = []; // Use Axis & Measure, Not Columns

		// Auto Create Columns to use
		Ext.each(this.leftAxis, function(f) {
			cols.push(f);
		});
		Ext.each(this.topAxis, function(f) {
			cols.push(f);
		});
		cols.push({dataIndex: this.measureField});
		this.columns = cols;

		TCG.grid.PivotGrid.superclass.initComponent.call(this);
	},


	createGrid: function(filtersPlugin) {
		const gridPanel = this;
		if (TCG.isNotNull(this.editor)) {
			if (!this.editor.ptype) {
				this.editor.ptype = 'pivotgrideditor';
			}
			this.editor = Ext.ComponentMgr.createPlugin(this.editor);
		}
		// create a copy of gridConf and add plugins as necessary
		const gridConf = Ext.apply({}, this.gridConfig);
		gridConf.measure = this.measureField;
		gridConf.renderer = this.measureFieldRenderer;

		this.leftAxis = TCG.clone(this.leftAxis);
		this.topAxis = TCG.clone(this.topAxis);
		TCG.grid.fillColumnMetaData(this.leftAxis, [], []);
		TCG.grid.fillColumnMetaData(this.topAxis, [], []);
		gridConf.leftAxis = this.leftAxis;
		gridConf.topAxis = this.topAxis;

		if (!gridConf.plugins) {
			gridConf.plugins = [];
		}
		if (TCG.isNotNull(this.editor)) {
			gridConf.plugins.push(this.editor);
		}
		if (TCG.isNotNull(filtersPlugin)) {
			gridConf.plugins.push(filtersPlugin);
		}


		const viewClass = TCG.grid.PivotGridView;
		let viewConf = {
			forceFit: this.forceFit, scrollDelay: false,
			// make grid text selectable (can copy to clipboard)
			templates: {
				cell: new Ext.Template(
					'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>',
					'<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>',
					'</td>'
				)
			}
		};
		if (this.viewConfig) {
			viewConf = Ext.apply(this.viewConfig, viewConf);
		}
		return new Ext.grid.PivotGrid(Ext.apply({
			ownerGridPanel: gridPanel,
			border: false,
			flex: 1, // take 100% of all available space
			loadMask: true,
			stripeRows: true,
			ds: this.ds,
			cm: this.getColumnModel(),
			sm: this.getRowSelectionModel(),
			bbar: this.isPagingEnabled() ? new TCG.toolbar.PagingToolbar({store: this.ds, displayInfo: true, pageSize: this.pageSize}) : undefined,
			getTitle: function() {
				return gridPanel.getWindow().title;
			},
			view: new viewClass(viewConf)
		}, gridConf));
	},


	updateCount: function() {
		if (this.getBottomToolbar()) {
			return;
		} // already has counts
		const t = this.getTopToolbar();
		if (t && t.items.length > 0) {
			const label = 'Records: ' + this.grid.extractData().length + ' ';
			const last = t.items.get(t.items.length - 1);
			if (last.text && last.text.indexOf('Records:') === 0) {
				last.setText(label);
			}
			else {
				t.add(this.topToolbarUpdateCountPrefix, {xtype: 'tbtext', text: label});
			}
			t.syncSize();
		}
	}
});
Ext.reg('pivotgrid', TCG.grid.PivotGrid);


TCG.grid.PivotGridEditor = Ext.extend(TCG.grid.GridEditor, {


	startEditing: function(grid, rowIndex, evt) {
		if (evt && TCG.isActionColumn(evt.target)) {
			return; // skip because this is action column
		}
		const gridPanel = this.getGridPanel();

		const row = this.grid.leftAxis.getTuples()[rowIndex];
		const clazz = this.getDetailPageClass(grid, row);
		if (clazz) {
			const id = this.getDetailPageId(gridPanel, row);
			if (id !== undefined) {
				this.openDetailPage(clazz, gridPanel, id, row);
			}
		}
	}


});
Ext.preg('pivotgrideditor', TCG.grid.PivotGridEditor);


TCG.grid.PivotGridView = Ext.extend(Ext.grid.PivotGridView, {

	// TODO NOTHING HERE FOR NOW, BUT MAY WANT TO OVERRIDE


});


Ext.override(Ext.grid.PivotAxis, {
	// TODO - NEED TO GET THIS FROM THE GRID, BUT FOR NOW HARDCODED TO ASSUME IF PIVOTING RESULTS WILL ALWAYS BE IN PREFERRED SORTING
	remoteSort: true,

	getTuples: function() {
		const newStore = new Ext.data.Store({});

		newStore.data = this.store.data.clone();
		newStore.fields = this.store.fields;

		// This is the customized section that removes ExtJs sorting to change order of rows/column groupings
		if (this.remoteSort === false) {
			const sorters = [];
			const dimensions = this.dimensions;

			for (const dimension of dimensions) {
				sorters.push({
					field: dimension.dataIndex,
					direction: dimension.direction || 'ASC'
				});
			}

			newStore.sort(sorters);
		}

		const records = newStore.data.items;
		const hashes = [];
		const tuples = [];
		let hash;
		let info;
		let data;
		let key;

		for (const record of records) {
			info = this.getRecordInfo(record);
			data = info.data;
			hash = '';

			for (key of Object.keys(data)) {
				hash += data[key] + '---';
			}

			if (hashes.indexOf(hash) === -1) {
				hashes.push(hash);
				tuples.push(info);
			}
		}

		newStore.destroy();

		return tuples;
	},


	/**
	 * OVERRIDDEN TO SUPPORT FORMATTING and HIDDEN COLUMNS
	 * @private
	 * Uses the calculated set of tuples to build an array of headers that can be rendered into a table using rowspan or
	 * colspan. Basically this takes the set of tuples and spans any cells that run into one another, so if we had dimensions
	 * of Person and Product and several tuples containing different Products for the same Person, those Products would be
	 * spanned.
	 * @return {Array} The headers
	 */
	buildHeaders: function() {
		const tuples = this.getTuples(),
			rowCount = tuples.length,
			dimensions = this.dimensions,
			colCount = dimensions.length,
			headers = [];
		let tuple, rows, currentHeader, previousHeader, span, start, isLast, changed, i, j;

		for (i = 0; i < colCount; i++) {
			const dimension = dimensions[i];
			rows = [];
			span = 0;
			start = 0;

			//  Hidden Columns we don't display, but we include them for drill down support at this time (i.e. id columns)
			if (dimension.hidden !== true) {
				for (j = 0; j < rowCount; j++) {
					tuple = tuples[j];
					isLast = j === (rowCount - 1);
					// Customized Rendering - Useful especially for number values
					if (dimension.renderer) {
						currentHeader = dimension.renderer(tuple.data[dimension.dataIndex]);
					}
					else {
						currentHeader = tuple.data[dimension.dataIndex];
					}

					/*
					 * 'changed' indicates that we need to create a new cell. This should be true whenever the cell
					 * above (previousHeader) is different from this cell, or when the cell on the previous dimension
					 * changed (e.g. if the current dimension is Product and the previous was Person, we need to start
					 * a new cell if Product is the same but Person changed, so we check the previous dimension and tuple)
					 */
					changed = TCG.isNotNull(previousHeader) && previousHeader !== currentHeader;
					if (i > 0 && j > 0) {
						changed = changed || tuple.data[dimensions[i - 1].dataIndex] !== tuples[j - 1].data[dimensions[i - 1].dataIndex];
					}

					if (changed) {
						rows.push({
							header: previousHeader,
							span: span,
							start: start
						});

						start += span;
						span = 0;
					}

					if (isLast) {
						rows.push({
							header: currentHeader,
							span: span + 1,
							start: start
						});

						start += span;
						span = 0;
					}

					previousHeader = currentHeader;
					span++;
				}
			}

			headers.push({
				items: rows,
				width: dimension.width || this.defaultHeaderWidth
			});

			previousHeader = undefined;
		}
		return headers;
	}

});
