Ext.ns('TCG.Cache');


TCG.Cache = function() {
	this.nextId = 1;
	this.cache = {};
};

TCG.Cache.prototype.getNextId = function() {
	return this.nextId++;
};
TCG.Cache.prototype.remove = function(key) {
	delete this.cache[key];
};

TCG.Cache.prototype.put = function(key, item, validTimeSec) {
	if (TCG.isNotNull(this.cache[key])) {
		this.remove(key);
	}
	this.cache[key] = new TCG.CacheItem(item, validTimeSec);
};

TCG.Cache.prototype.get = function(key) {
	const i = this.cache[key];
	if (TCG.isNull(i)) {
		return i;
	}
	const v = i.get();
	if (i.isExpired()) {
		this.remove(key);
		return i;
	}
	return v;
};

TCG.Cache.prototype.keys = function() {
	const keys = [];
	for (const p in this.cache) {
		if (p !== 'remove') {
			keys.push(p);
		}
	}
	return keys;
};


TCG.CacheItem = function(item, validTimeSec) {
	this.item = item;
	this.createDate = validTimeSec ? new Date() : null;
	this.validTimeSec = validTimeSec ? 1000 * validTimeSec : null;
};

TCG.CacheItem.prototype.get = function() {
	if (this.isExpired()) {
		return null;
	}
	return this.item;
};

TCG.CacheItem.prototype.isExpired = function() {
	if (TCG.isNull(this.createDate)) {
		return false;
	}
	const life = new Date() - this.createDate;
	return (life > this.validTimeSec);
};


const globalCache = {
	nextId: 1,

	getNextId: function() {
		return this.nextId++;
	},

	remove: function(key) {
		delete this[key];
	}
};

TCG.CacheUtils = function() {
	// new class
};

TCG.CacheUtils.nextId = function() {
	return globalCache.getNextId();
};

TCG.CacheUtils.put = function(cacheName, item, key, validTimeSec) {
	let c = globalCache[cacheName];
	if (TCG.isNull(c)) {
		c = new TCG.Cache();
		globalCache[cacheName] = c;
	}
	if (!key) {
		key = c.getNextId();
	}
	c.put(key, item, validTimeSec);
	return key;
};

TCG.CacheUtils.get = function(cacheName, key) {
	const c = globalCache[cacheName];
	if (TCG.isNull(c)) {
		return c;
	}
	return c.get(key);
};

TCG.CacheUtils.getCache = function(cacheName) {
	return globalCache[cacheName];
};

TCG.CacheUtils.remove = function(cacheName, key) {
	const c = globalCache[cacheName];
	if (TCG.isNull(c)) {
		return;
	}
	if (key) {
		c.remove(key);
	}
	else {
		globalCache.remove(cacheName);
	}
};
