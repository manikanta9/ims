Ext.ns('TCG.toolbar');

TCG.toolbar.Toolbar = Ext.extend(Ext.Toolbar, {
	applicationName: ''
});
Ext.reg('toolbar', TCG.toolbar.Toolbar);

TCG.toolbar.PagingToolbar = Ext.extend(Ext.PagingToolbar, {
	applicationName: ''
});
Ext.reg('paging-toolbar', TCG.toolbar.PagingToolbar);
