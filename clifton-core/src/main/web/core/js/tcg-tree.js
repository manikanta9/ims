Ext.ns('TCG.tree');


/*
 * Modify the TreeNode "expand" function to propagate the "anim" property when doing deep expansions. This reduces the cost of expansions for large nested trees.
 */
Ext.tree.TreeNode.prototype.expand = function(deep, anim, callback, scope) {
	if (!this.expanded) {
		if (this.fireEvent('beforeexpand', this, deep, anim) === false) {
			return;
		}
		if (!this.childrenRendered) {
			this.renderChildren();
		}
		this.expanded = true;
		if (!this.isHiddenRoot() && (this.getOwnerTree().animate && anim !== false) || anim) {
			this.ui.animExpand(() => {
				this.fireEvent('expand', this);
				this.runCallback(callback, scope || this, [this]);
				if (deep === true) {
					this.expandChildNodes(true, true);
				}
			});
			return;
		}
		else {
			this.ui.expand();
			this.fireEvent('expand', this);
			this.runCallback(callback, scope || this, [this]);
		}
	}
	else {
		this.runCallback(callback, scope || this, [this]);
	}
	if (deep === true) {
		// Begin changes
		this.expandChildNodes(true, anim !== false);
		// End changes
	}
};


// 'treepanel' with context menu for drill down/edits
TCG.tree.AdvancedTreePanel = Ext.extend(Ext.tree.TreePanel, {
	split: true,
	width: 220,
	animate: true,
	autoScroll: true,
	detailPageClass: 'CONTEXT MENU DETAIL PAGE CLASS FOR TREE NODES',
	deleteURL: 'CONTEXT MENU URL FOR NODE DELETION',
	openDetailWindowAsModal: true, // Defines if Adds/Drill Down window on selected folders should be modal or not

	onContextMenu: function(node, e) {
		node.select();
		const tree = node.getOwnerTree();
		if (!(this.contextMenu instanceof Ext.Component)) { // lazy initialization
			this.contextMenu = tree.createComponent(this.contextMenu, 'menu');
		}
		const c = this.contextMenu;
		if (c) {
			c.contextNode = node;
			// disable info/delete for the root node
			const root = !Ext.isNumber(node.id);
			c.items.get(1).setDisabled(root);
			c.items.get(3).setDisabled(root);
			c.showAt(e.getXY());
		}
	},

	contextMenu: {
		items: [
			{value: 'rf-add', text: 'Add Sub Folder', iconCls: 'add'},
			{value: 'rf-remove', text: 'Delete Selected Folder', iconCls: 'remove'},
			{value: 'rf-reload', text: 'Reload Sub Folders', iconCls: 'table-refresh'},
			{value: 'rf-open', text: 'View Selected Folder', iconCls: 'info'},
			{value: 'rf-expand', text: 'Fully Expand', iconCls: 'expand-all'},
			{value: 'rf-collapse', text: 'Fully Collapse', iconCls: 'collapse-all'}
		],
		listeners: {
			itemclick: function(item) {
				const node = item.parentMenu.contextNode;
				const tree = node.getOwnerTree();
				tree.contextNode = node; // save so that can reload after changes
				const className = tree.detailPageClass;
				const id = Ext.isNumber(node.id) ? node.id : null;
				if (item.value === 'rf-open') {
					TCG.createComponent(className, {
						id: TCG.getComponentId(className, id),
						params: {id: id},
						modal: tree.openDetailWindowAsModal,
						allowOpenFromModal: true,
						openerCt: tree
					});
				}
				else if (item.value === 'rf-expand') {
					node.expand(true, false);
				}
				else if (item.value === 'rf-collapse') {
					node.collapse(true, false);
				}
				else if (item.value === 'rf-add') {
					const dd = {
						parent: {
							id: id,
							name: node.attributes.text
						},
						tableName: tree.tableName // in case parent is root
					};
					tree.configureDefaultDataForAdd(dd);
					TCG.createComponent(className, {
						id: TCG.getComponentId(className),
						defaultData: dd,
						modal: tree.openDetailWindowAsModal,
						openerCt: tree
					});
				}
				else if (item.value === 'rf-reload') {
					node.reload();
				}
				else if (item.value === 'rf-remove') {
					Ext.Msg.show({
						title: 'Delete Folder?',
						msg: 'Would you like to delete "' + node.text + '"folder?',
						buttons: Ext.Msg.YESNO,
						fn: function(buttonId) {
							if (buttonId === 'yes') {
								const loader = new TCG.data.JsonLoader({
									waitTarget: tree,
									params: {id: id},
									onLoad: function(record, conf) {
										node.remove(true);
									}
								});
								loader.load(tree.deleteURL);
							}
						}
					});
				}
			}
		}
	},

	configureDefaultDataForAdd: function(dd) {
		// Override if needed
	},

	// add listeners separately to allow additional 'listeners' on child classes
	afterRender: function() {
		TCG.tree.AdvancedTreePanel.superclass.afterRender.apply(this, arguments);

		// load first level children
		this.getRootNode().expand();

		// refresh the grid on node select
		const sm = this.getSelectionModel();
		sm.on('selectionchange', function(sm, node) {
			if (TCG.isNotNull(node)) {
				const id = Ext.isNumber(node.id) ? node.id : null;
				this.onNodeSelected(id, node);
			}
		}, this, {buffer: 400});

		this.on('dblclick', function(node, e) {
			const tree = node.getOwnerTree();
			if (tree.openDetailOnDblClick(node, e)) {
				const className = tree.detailPageClass;
				const id = Ext.isNumber(node.id) ? node.id : null;
				TCG.createComponent(className, {
					id: TCG.getComponentId(className, id),
					params: {id: id},
					modal: tree.openDetailWindowAsModal,
					openerCt: tree
				});
			}
		});

		this.on('contextmenu', this.onContextMenu);

		const dd = this.getWindow().defaultData;
		if (dd && dd.defaultNodePath) {
			this.selectPath('/' + dd.defaultNodePath, 'text');
		}
	},

	reload: function(win) {
		// called after a folder was added/updated
		if (this.contextNode && this.contextNode.reload) {
			this.contextNode.reload();
		}
	},

	onDestroy: function() {
		Ext.destroy(this.contextMenu);
		TCG.tree.AdvancedTreePanel.superclass.onDestroy.apply(this, arguments);
	},

	// called when a node is selected (nodeId argument): can reload grid, etc.
	onNodeSelected: Ext.emptyFn,

	openDetailOnDblClick: function(node, e) {
		return false;
	},

	getWindow: function() {
		let result = this.findParentByType(Ext.Window);
		if (TCG.isNull(result)) {
			result = this.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			});
		}
		return result;
	}
});
Ext.reg('treepanel-advanced', TCG.tree.AdvancedTreePanel);


// smart loader that supports custom JSON
TCG.tree.JsonTreeLoader = Ext.extend(Ext.tree.TreeLoader, {
	rootNode: 'data',
	useExtSyntax: true,
	idNode: 'id',
	textNode: 'label',
	leafNode: 'leaf',

	nodeParameter: 'parentId',

	// EXT JS OVERRIDES
	processResponse: function(response, node, callback, scope) {
		try {
			// save so that can re-execute on re-login
			this.lastProcessResponse = [response, node, callback, scope];
			const data = Ext.decode(response.responseText);
			if (data.success) {
				response.responseData = this.getResultData(data);
				TCG.tree.JsonTreeLoader.superclass.processResponse.apply(this, arguments);
			}
			else {
				TCG.data.ErrorHandler.handleFailure(this, data);
			}
		}
		catch (e) {
			this.handleFailure(response, null, e);
		}
	},

	handleFailure: function(response, opts, e) {
		TCG.tree.JsonTreeLoader.superclass.handleFailure.apply(this, arguments);
		// show our error message
		TCG.data.ErrorHandler.handleException(null, null, null, opts, response, e);
	},

	getParams: function(node) {
		const result = TCG.tree.JsonTreeLoader.superclass.getParams.apply(this, arguments);
		if (!Ext.isNumber(node.id)) {
			delete result[this.nodeParameter];
			Ext.applyIf(result, this.getRootParams());
		}
		return result;
	},

	// save the data so that other attributes could be used
	createNode: function(attr) {
		const result = TCG.tree.JsonTreeLoader.superclass.createNode.apply(this, arguments);
		result.nodeData = attr.nodeData;
		return result;
	},
	// OUR HELPER METHODS

	// optional parameters to be passed to get root level nodes (rootNodesOnly = true, etc.)
	getRootParams: function() {
		return {};
	},

	// allows to re-execute original request (after successful login)
	loadJsonResult: function(result) {
		this.lastProcessResponse[0] = {responseData: this.getResultData(result)};
		TCG.tree.JsonTreeLoader.superclass.processResponse.apply(this, this.lastProcessResponse);
	},

	// converts custom JSON to ExtJS node syntax
	getResultData: function(result) {
		const o = TCG.getValue(this.rootNode, result);
		const data = [];
		const fn = (typeof this.leafNode === 'function');
		let i = 0;
		const len = o.length;
		for (; i < len; i++) {
			const n = o[i];
			data[i] = this.useExtSyntax ? {
				id: TCG.getValue(this.idNode, n),
				text: TCG.getValue(this.textNode, n),
				qtip: (this.qtipNode ? TCG.getValue(this.qtipNode, n) : undefined),
				leaf: fn ? this.leafNode(n) : TCG.getValue(this.leafNode, n),
				nodeData: n // save all data just in case it's needed
			} : this.prepareResultData(n);
		}
		return data;
	},

	prepareResultData: function(v) {
		return v;
	}
});


/**
 * And extension of the ExtJS TreeGrid with additional configuration to support our basic functionality
 * we use on grids, i.e. Add, Remove, Print, etc.
 *
 * TODO: refactor to extract common functionality into a plugin (grid & treegrid)
 */
TCG.tree.TreeGrid = Ext.extend(Ext.ux.tree.TreeGrid, {
	loader: null,
	rootVisible: false,
	editor: null,
	lines: true,
	useArrows: false,
	dataTable: false, // TODO IF THERE IS AN ACTUAL USE CASE FOR THIS, NEEDS TO BE TESTED
	enableSort: false,
	showExpandCollapseShortcut: false, // the expand all collapse all is hidden in the menu - if true will add it to the beginning of the toolbar buttons (icons only)

	standardColumns: [ // these columns will be added to the end of the grid if corresponding dataIndex fields are defined
		{header: 'Created', width: 80, dataIndex: 'createDate', hidden: true},
		{header: 'Updated On', width: 80, dataIndex: 'updateDate', hidden: true}
	],
	appendStandardColumns: true,

	/**
	 * By default, our tree grid passes the current item's selected node identity as the parentId in
	 * the request.  To change the parentId parameter name, override this parameter.  To pass nothing,
	 * change the parameter name to null.
	 */
	loadIDParameterName: 'parentId',
	loadIDNoteAttributeName: 'identity',
	useExtSyntax: false,
	idNode: 'id',
	textNode: 'label',
	leafNode: 'leaf',
	nodeParameter: 'parentId',

	listeners: {
		contextmenu: function(node, eventObj) {
			// Show context menu
			const menuItems = this.getContextMenuItems(node, eventObj);
			if (menuItems.length > 0) {
				eventObj.stopEvent();
				const mainMenu = new Ext.menu.Menu({items: menuItems});
				mainMenu.showAt(eventObj.getXY());
			}
		}
	},

	prepareResultData: function(v) {
		return v;
	},

	initComponent: function() {
		this.columns = TCG.clone(this.columns); // avoid conflicts when multiple instances are open at the same time

		if (this.isEditEnabled() || this.instructions) {
			Ext.apply(this, {tbar: new Ext.Toolbar()});
		}

		// add standard columns if they don't already exist
		if (this.dataTable) {
			this.appendStandardColumns = false;
		}
		if (this.appendStandardColumns) {
			for (let i = 0; i < this.standardColumns.length; i++) {
				let found = false;
				const c = TCG.clone(this.standardColumns[i]);
				for (let j = 0; j < this.columns.length; j++) {
					if (this.columns[j] && this.columns[j].dataIndex === c.dataIndex) {
						found = true;
						break;
					}
				}
				if (!found) {
					this.columns[this.columns.length] = c;
				}
			}
		}

		const fields = [];
		const filters = [];
		TCG.grid.fillColumnMetaData(this.columns, fields, filters);

		for (const col of this.columns) {
			/*
			 * Normalize data index; standard template does not handle paths correctly (attempts to access path without null-safe navigation operators and does not use record
			 * object)
			 */
			const valueJs = `TCG.getValue('${col.dataIndex}', values)`;
			if (col.renderer) {
				// Use custom renderer on data index value
				col.tpl = new Ext.XTemplate(`{[TCG.coalesce(this.call('customRenderer', ${valueJs}, values), '')]}`, {customRenderer: col.renderer});
			}
			else {
				// Display data index
				col.tpl = new Ext.XTemplate(`{[TCG.coalesce(${valueJs}, '')]}`);
			}
		}

		TCG.tree.TreeGrid.superclass.initComponent.call(this);

		// Initialize editor
		if (this.editor) {
			this.editor.ptype = this.editor.ptype || 'treegrideditor';
			this.editor = Ext.ComponentMgr.createPlugin(this.editor);
			this.editor.init(this);
		}
	},

	onRender: function() {
		TCG.tree.TreeGrid.superclass.onRender.apply(this, arguments);

		// generate toolbar
		const tree = this;
		const toolbar = this.getTopToolbar();

		if (TCG.isTrue(this.showExpandCollapseShortcut)) {
			toolbar.add({
				iconCls: 'expand-all',
				tooltip: 'Expand All Nodes',
				handler: () => this.root.expand(true, false)
			});
			toolbar.add('-');
			toolbar.add(
				{
					iconCls: 'collapse-all',
					tooltip: 'Collapse all Nodes',
					handler: () => this.root.collapse(true, false)
				});
			toolbar.add('-');
		}

		if (this.isEditEnabled()) {
			this.editor.addEditButtons(toolbar);
		}
		if (toolbar) {
			toolbar.add({
				text: 'Reload',
				tooltip: 'Reload latest grid data',
				iconCls: 'table-refresh',
				handler: () => tree.reload()
			});
			if (this.instructions) {
				toolbar.add('-', {
					text: 'Info',
					tooltip: 'Toggle instructions for this page',
					iconCls: 'info',
					enableToggle: true,
					handler: function() {
						if (this.pressed) {
							tree.insert(0, {
								xtype: 'panel',
								frame: true,
								layout: 'fit',
								bodyStyle: 'padding: 3px 3px 3px 3px',
								html: tree.instructions
							});
						}
						else {
							tree.remove(0);
						}
						tree.doLayout();
					}
				});
			}

			const toolsMenuItems = [
				{
					text: 'Show All Columns',
					handler: () => {
						this.columns.forEach(col => col.hidden = false);
						this.updateColumnWidths();
					}
				},
				'-',
				{
					text: 'Expand All Nodes',
					iconCls: 'expand-all',
					handler: () => this.root.expand(true, false)
				},
				{
					text: 'Collapse All Nodes',
					iconCls: 'collapse-all',
					handler: () => this.root.collapse(true, false)
				},
				'-'
			];
			const importItems = tree.getToolsImportItems(tree);
			if (importItems && importItems.length > 0) {
				toolsMenuItems.push(...importItems);
			}
			toolsMenuItems.push({
				text: 'Print',
				iconCls: 'printer',
				handler: () => Ext.ux.Printer.print(this)
			});

			toolbar.add('-', {
				text: 'Tools',
				iconCls: 'config',
				menu: {items: toolsMenuItems}
			});

			// Execute hook for toolbar modifications
			this.addTopToolbarItems(toolbar);
		}

		this.loader = new TCG.tree.JsonTreeLoader({
			prepareResultData: this.prepareResultData,
			useExtSyntax: this.useExtSyntax,
			idNode: this.idNode,
			textNode: this.textNode,
			leafNode: this.leafNode,
			nodeParameter: this.nodeParameter,
			requestData: this.requestData
		});
		this.loader.dataUrl = encodeURI(TCG.getApplicationUrl(this.getLoadURL(), this));
		if (this.getRootParams) {
			this.loader.getRootParams = this.getRootParams;
		}

		if (this.loadIDNoteAttributeName !== 'identity') {
			this.loader.nodeParameter = undefined;
		}

		if (this.loadIDParameterName) {
			this.loader.on('beforeload', function(treeLoader, node) {
				if (node.isRoot) {
					this.body.mask('Loading...');
				}
				const idVal = (this.loadIDNoteAttributeName === 'identity') ? node.attributes.identity : TCG.getValue(this.loadIDNoteAttributeName, node.attributes);
				if (!node || !node.attributes || !idVal) {
					treeLoader.baseParams[this.loadIDParameterName] = null;
					const params = this.getLoadParams(this.firstLoadProcessed !== true);
					this.firstLoadProcessed = true;
					if (params === false) {
						this.body.unmask();
						return false;
					}
					// Note: This is put into it's own property so if it's a promise it can't be properly resolved before apply it to the base params. In the above we check for return of false - we are assuming that the promise will not evaluate to false but will just return false if the load should be canceled
					treeLoader.loadParams = params;
				}
				else {
					treeLoader.baseParams[this.loadIDParameterName] = idVal;
				}
			}, this);

			this.loader.on('load', function(treeLoader, node) {
				node.getOwnerTree().body.unmask();
			});
		}
		else {
			this.loader.baseParams = this.getLoadParams(true);
		}
	},

	// Overridden from ExtJs to handle Promise (loadParams - see method above where these are set)
	requestData: function(node, callback, scope) {
		Promise.resolve(this.fireEvent('beforeload', this, node, callback)).then(result => {
			if (result !== false) {
				Promise.resolve(this.loadParams).then(lp => {
					// Resolve the load params and then apply them to the base params
					Ext.apply(this.baseParams, lp);
					this.loadParams = undefined; // Reset lp for next call
					if (this.directFn) {
						const args = this.getParams(node);
						args.push(this.processDirectResponse.createDelegate(this, [{callback: callback, node: node, scope: scope}], true));
						this.directFn.apply(window, args);
					}
					else {
						this.transId = Ext.Ajax.request({
							method: this.requestMethod,
							url: this.dataUrl || this.url,
							success: this.handleResponse,
							failure: this.handleFailure,
							scope: this,
							argument: {callback: callback, node: node, scope: scope},
							params: this.getParams(node)
						});
					}
				});
			}
			else {
				// if the load is cancelled, make sure we notify
				// the node that we are done
				this.runCallback(callback, scope || node, []);
			}
		});
	},

	updateColumnWidths: function() {
		const cols = this.columns,
			colCount = cols.length;
		let groups = this.outerCt.query('colgroup');
		const groupCount = groups.length;
		let c, g, i, j;

		for (i = 0; i < colCount; i++) {
			c = cols[i];
			for (j = 0; j < groupCount; j++) {
				g = groups[j];
				g.childNodes[i].style.width = (c.hidden ? 0 : c.width) + 'px';
				g.childNodes[i].style.visibility = c.hidden ? 'collapse' : null;
			}
		}

		groups = this.innerHd.query('td');
		const len = groups.length;
		for (i = 0; i < len; i++) {
			c = Ext.fly(groups[i]);
			if (cols[i] && cols[i].hidden) {
				c.addClass('x-treegrid-hd-hidden');
			}
			else {
				c.removeClass('x-treegrid-hd-hidden');
			}
		}

		const tcw = this.getTotalColumnWidth();
		Ext.fly(this.innerHd.dom.firstChild).setWidth(tcw + (this.scrollOffset || 0));
		this.outerCt.select('table').setWidth(tcw);
		this.syncHeaderScroll();
	},

	reload: function() {
		this.getLoader().abort();
		this.getLoader().load(this.root);
	},

	getWindow: function() {
		let result = this.findParentByType(Ext.Window);
		if (TCG.isNull(result)) {
			result = this.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			});
		}
		return result;
	},


	isEditEnabled: function() {
		return !!this.editor;
	},

	getLoadURL: function() {
		if (!this.loadURL) {
			return this.name + '.json';
		}
		return this.loadURL;
	},

	// return false to cancel reload
	getLoadParams: function(firstLoad) {
		return {};
	},

	addTopToolbarItems: function(toolbar) {
		// Do nothing (override only)
	},

	/**
	 * Gets the list of context menu items to display for a selected node.
	 */
	getContextMenuItems: function(node, eventObj) {
		const menuItems = [];
		if (!node.leaf) {
			const childCount = getChildCount(node.attributes);
			const childThreshold = 500;
			menuItems.push({
				text: 'Fully Expand',
				iconCls: 'expand-all',
				...childCount > childThreshold ? {
					tooltip: `The number of child nodes (${childCount}) is too great to fully expand this node. The maximum number of children for full expansion is ${childThreshold}.`,
					disabled: true
				} : {
					tooltip: `Full expand all ${childCount} descendants of this node.`
				},
				handler: () => node.expand(true, false)
			}, {
				text: 'Fully Collapse',
				iconCls: 'collapse-all',
				handler: () => node.collapse(true, false)
			});
		}
		return menuItems;


		function getChildCount(entity) {
			const children = entity.children || [];
			return children.length + children.reduce((count, child) => count + getChildCount(child), 0);
		}
	},

	getToolsImportItems: function(tree) {
		const importItems = [];
		if (tree.importTableName) {
			let imps = [];
			if (typeof this.importTableName == 'string') {
				imps.push({table: this.importTableName, importComponentName: this.importComponentName});
			}
			else {
				imps = this.importTableName;
			}

			for (let i = 0; i < imps.length; i++) {
				const label = imps[i].label;
				importItems.push({
					text: 'Import ' + (label ? label + ' ' : '') + 'from Excel',
					iconCls: 'import',
					importIndex: i,
					handler: function() {
						const imp = imps[this.importIndex];
						const componentName = (imp.importComponentName ? imp.importComponentName : 'Clifton.system.upload.UploadWindow');
						let defaultData = {tableName: imp.table};
						if (tree.getImportComponentDefaultData) {
							defaultData = Ext.apply(defaultData, tree.getImportComponentDefaultData(imp.table));
						}

						TCG.createComponent(componentName, {
							defaultData: defaultData,
							openerCt: tree
						});
					}
				});
			}
		}
		return importItems;
	}
});
Ext.reg('treegrid', TCG.tree.TreeGrid);


TCG.tree.TreeGridEditor = Ext.extend(Ext.util.Observable, {
	// detailPageClass: use to defined the detail page for each grid row; for multiple classes can be an object: {getItemText: function(gridItem) {...}, items: [{text: 'Item1', className: 'Class1'}, ...]})
	// deleteURL:       defines delete URL (default impl derives it from GridPanel name)
	drillDownOnly: false,
	addEnabled: true,
	deleteEnabled: true,

	constructor: function(config) {
		Ext.apply(this, config);
	},

	init: function(tree) {
		this.tree = tree;
		this.tree.addListener('dblclick', this.startEditing, this);
		if (tree.getWindow()) {
			this.detailWindowIconClass = tree.getWindow().iconCls;
		}
	},

	isAddEnabled: function() {
		if (this.drillDownOnly) {
			return false;
		}
		return this.addEnabled;
	},

	isDeleteEnabled: function() {
		if (this.drillDownOnly) {
			return false;
		}
		return this.deleteEnabled;
	},

	getWindow: function() {
		return this.tree.getWindow();
	},

	addEditButtons: function(t) {
		if (this.isAddEnabled()) {
			this.addToolbarAddButton(t, this);
		}
		if (this.isDeleteEnabled()) {
			this.addToolbarDeleteButton(t, this);
		}
	},

	startEditing: function(node, evt) {
		const id = node.attributes.identity;
		this.openDetailPage(this.detailPageClass, this.tree, id);
	},

	getDetailPageClass: function(tree, row) {
		let result = undefined;
		const type = Ext.type(this.detailPageClass);
		if (type === 'string') {
			result = this.detailPageClass;
		}
		else if (type === 'object') {
			TCG.showError('Not Supported', 'Multiple classes per table not supported');
		}
		return result;
	},

	openDetailPage: function(className, tree, id) {
		const defaultData = id ? undefined : this.getDefaultData(tree);
		const params = id ? {id: id} : undefined;
		const cmpId = className + '-' + (id || (new Date() - 1)); // unique id for new row
		TCG.createComponent(className, {
			id: cmpId,
			defaultData: defaultData,
			params: params,
			openerCt: tree,
			defaultIconCls: tree.detailWindowIconClass
		});
	},

	// returns JSON representation of defaults for detail page
	getDefaultData: function(tree) {
		return undefined;
	},

	addToolbarAddButton: function(toolBar) {
		if (Ext.type(this.detailPageClass) === 'object') {
			TCG.showError('Not Supported', 'Multiple classes not supported.');
		}
		else {
			toolBar.add({
				text: 'Add',
				tooltip: 'Add a new item',
				iconCls: 'add',
				scope: this,
				handler: function() {
					this.openDetailPage(this.getDetailPageClass(), this.tree);
				}
			});
		}
		toolBar.add('-');
	},

	addToolbarDeleteButton: function(toolBar) {
		toolBar.add({
			text: 'Remove',
			tooltip: 'Remove selected item(s)',
			iconCls: 'remove',
			scope: this,
			handler: function() {
				const editor = this;
				const tree = this.tree;
				const sm = tree.getSelectionModel();
				const selected = sm.getSelectedNode();
				if (!selected) {
					TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
				}
				else {
					Ext.Msg.confirm('Delete Selected Row', 'Would you like to delete selected row?', function(a) {
						if (a === 'yes') {
							const loader = new TCG.data.JsonLoader({
								waitTarget: tree,
								waitMsg: 'Deleting...',
								params: editor.getDeleteParams(sm),
								conf: {rowId: selected.attributes.identity},
								onLoad: function(record, conf) {
									tree.reload(selected.parentNode);
								}
							});
							loader.load(editor.getDeleteURL());
						}
					});
				}
			}
		});
		toolBar.add('-');
	},

	getDeleteURL: function() {
		if (!this.deleteURL) {
			let url = this.tree.name;
			if (url.length > 4 && url.substring(url.length - 4) === 'List') {
				url = url.substring(0, url.length - 4);
			}
			return url + 'Delete.json';
		}
		return this.deleteURL;
	},

	getDeleteParams: function(selectionModel) {
		return {id: selectionModel.getSelectedNode().attributes.identity};
	}
});
Ext.preg('treegrideditor', TCG.tree.TreeGridEditor);


// Configure tree grid printing
Ext.override(Ext.ux.Printer.ColumnTreeRenderer, {

	indentPadding: 0, // Indent padding is no longer used in lieu of tree-grid based spacing

	bodyTpl: new Ext.XTemplate(TCG.trimWhitespace(`
		<tr class="x-tree-node-el">
			<tpl for=".">
				<td align="{[values.align || 'left']}"
						class="\\{\\[values['{dataIndex}'].metadata.css || ''\\]\\}">
					\\{\\[values['{dataIndex}'].value\\]\\}
				</td>
			</tpl>
		</tr>
	`)),

	// language=CSS
	styleOverrides: [
		...Ext.ux.Printer.ColumnTreeRenderer.superclass.styleOverrides,
		TCG.trimWhitespace(`
			* {
				/* Remove "clickable" indicators */
				cursor: inherit !important;
			}

			table td {
				/* Override default stylesheet to remove padding since replication of treegrid styling is used */
				padding: 0 3px;
			}

			/*noinspection CssUnusedSymbol*/
			.x-tree-node-icon {
				/* Adjust icons for wrapped displays */
				background-position: top !important;
			}

			/*noinspection CssUnusedSymbol*/
			.x-tree-elbow-line {
				background-repeat: repeat-y;
			}

			/* Repeat elbow lines even when wrapping occurs */
			/*noinspection CssUnusedSymbol*/
			.x-tree-elbow-line-include {
				position: relative;
			}

			/*noinspection CssUnusedSymbol*/
			.x-tree-elbow-line-include:before {
				content: "";
				/*noinspection CssUnknownTarget*/
				background-image: url(${TCG.ExtRootUrl}/resources/images/default/tree/elbow-line.gif);
				background-repeat: repeat-y;
				position: absolute;
				height: 100%;
				width: 100%;
				z-index: -1;
			}
		`)
	],

	prepareData: function(tree) {
		// Prepare row values for visible rows
		const data = [];
		const columns = this.getColumns(tree);
		const useLineAtLevels = [];
		tree.root.cascade(node => {
			// Guard-clause: Ignore hidden nodes
			if (node.hidden === true || node.isHiddenRoot() === true) {
				return;
			}

			// Track "useLineAtLevels" property depth
			const depth = node.getDepth() - 1;
			useLineAtLevels[depth] = !node.isLast();

			// Populate row data
			const row = {};
			for (let i = 0; i < columns.length; i++) {
				const column = columns[i];
				const value = column.tpl.apply(node.attributes);
				const metadata = {};
				let valueHtml;
				if (i === 0) {
					// Display tree structure in first column
					metadata.css = ` x-treegrid-col x-tree-node-${node.isLeaf() ? 'leaf' : node.isExpanded() ? 'expanded' : 'collapsed'}`;
					valueHtml = TCG.trimWhitespace(`
						<div style="display: table-row;">
							${Array.from({length: depth})
						.map((value, curDepth) => this.generateTableCell({classes: [`x-tree-${useLineAtLevels[curDepth] ? 'elbow-line' : 'icon'}`]}))
						.join('')}
							${this.generateTableCell({classes: ['x-tree-ec-icon', `x-tree-elbow${node.isLast() ? '-end' : ''}`, !node.isLast() ? 'x-tree-elbow-line-include' : '']})}
							${this.generateTableCell({classes: ['x-tree-node-icon', (node.isExpanded() && node.hasChildNodes()) ? 'x-tree-elbow-line-include' : '']})}
							${this.generateTableCell({styles: ['white-space: initial', 'padding: 0 3px'], content: value})}
						</div>
					`);
				}
				else {
					metadata.css = ' x-treegrid-col';
					valueHtml = value;
				}
				row[column.dataIndex] = {
					value: valueHtml,
					metadata: metadata
				};
			}
			data.push(row);

			// Only cascade into children if expanded
			return node.hasChildNodes() && node.isExpanded();
		});
		return data;
	},

	getColumns: function(tree) {
		return tree.getVisibleColumns();
	},

	generateBody: function(tree) {
		const columns = this.getColumns(tree);
		return TCG.trimWhitespace(`
			<table class="x-tree-lines">
				<thead>${this.headerTpl.apply(columns)}</thead>
				<tpl for=".">${this.bodyTpl.apply(columns)}</tpl>
			</table>
		`);
	},

	generateTableCell: function({content = '', styles = [], classes = []}) {
		return `<div style="display: table-cell; ${styles.join('; ') || ''}" class="${classes.join(' ')}">${content}</div>`;
	}
});
Ext.ux.Printer.registerRenderer('treegrid', Ext.ux.Printer.ColumnTreeRenderer);

/**
 * Override MultiSelectionModel to allow for shift selection for multiple selections.
 * Onley ctrl select and single select are supported in the base implementation.
 */
Ext.override(Ext.tree.MultiSelectionModel, {
	firstSelectedNode: void 0,

	onNodeClick: function(node, e) {
		if (e.shiftKey && TCG.isNotNull(this.firstSelectedNode)) {
			if (this.firstSelectedNode.parentNode !== node.parentNode) {
				TCG.showError('Multi-selections using Shift must have the same parent element.', 'Tree Multi-Select');
				return;
			}

			this.clearSelections();

			const siblingNodes = node.parentNode.childNodes;
			const firstSelectionIndex = siblingNodes.findIndex(i => i === this.firstSelectedNode);
			const currentSelectionIndex = siblingNodes.findIndex(i => i === node);
			let iteratingIndex;
			let stoppingIndex;
			if (firstSelectionIndex < currentSelectionIndex) {
				iteratingIndex = firstSelectionIndex;
				stoppingIndex = currentSelectionIndex;
			}
			else {
				iteratingIndex = currentSelectionIndex;
				stoppingIndex = firstSelectionIndex;
			}
			while (iteratingIndex <= stoppingIndex) {
				this.select(siblingNodes[iteratingIndex], e, e.shiftKey);
				iteratingIndex++;
			}
		}
		else if (e.ctrlKey && this.isSelected(node)) {
			this.unselect(node);
		}
		else {
			this.select(node, e, e.ctrlKey);
			this.firstSelectedNode = node;
		}
	}
});
