Ext.ns('TCG.app');


TCG.app.Application = function(conf) {
	Ext.apply(this, conf);
	this.addEvents({
		'afterload': true,
		'beforeunload': true,
		'createWindow': true,
		'closeWindow': true,
		'titlechange': true,
		'modified': true
	});
	Ext.onReady(this.initApp, this, {delay: 200});
};

Ext.extend(TCG.app.Application, Ext.util.Observable, {
	menubar: null,
	northAutoHeight: false,

	initApp: function() {
		this.menubar = this.getMenuBar();
		this.viewport = new Ext.Viewport({
			layout: 'border',
			hideBorders: true,
			renderTo: Ext.getBody(),
			items: [
				{
					region: 'north',
					height: 55,
					autoHeight: this.northAutoHeight, // Note: This is needed for Portal becaue the top menu bar is 4 rows
					xtype: 'panel',
					hideBorders: true,
					items: [this.getTopBar(), this.menubar]
				}, {
					region: 'center',
					xtype: 'panel',
					hideBorders: true,
					id: 'desktop',
					items: [this.getBody()]
				}
			]
		});
		this.desktopEl = Ext.get('desktop');

		Ext.WindowMgr.zseed = 99999; // alerts should cover our window group(s)
		this.windows = new Ext.WindowGroup();
		this.activeWindow = null;

		Ext.EventManager.onWindowResize(this.layout);

		this.init();

		Ext.EventManager.on(window, 'beforeunload', this.onBeforeUnload, this);
		Ext.EventManager.on(window, 'unload', this.onUnload, this);
		this.fireEvent('afterload', this);
	},

	getTopBar: function() {
		return {
			html: 'Override TCG.app.Application.getTopBar()'
		};
	},
	getMenuBar: Ext.emptyFn,
	getBody: function() {
		return {
			// empty
		};
	},

	init: function() {
		// define default behavior for our applications
		Ext.QuickTips.init();
		Ext.apply(Ext.QuickTips.getQuickTip(), {
			maxWidth: 500,
			dismissDelay: 30000 // 30 seconds
		});
		Ext.MessageBox.minWidth = 380;
		Ext.data.Field.prototype.dateFormat = 'Y-m-d H:i:s';
		Ext.form.Field.prototype.msgTarget = 'side';
		Ext.form.NumberField.prototype.decimalPrecision = 20; // default is 2: too little for grid filters
		Ext.form.NumberField.prototype.style = 'text-align: right';
		Ext.form.TextArea.prototype.height = 90;
		Ext.form.TextArea.prototype.growMin = 40;
		Ext.form.TextArea.prototype.growMax = 200;
		Ext.form.Checkbox.prototype.autoWidth = true;
		Ext.chart.Chart.CHART_URL = `${TCG.ExtRootUrl}/resources/charts.swf`;
	},

	getWindow: function(id) {
		return this.windows.get(id);
	},

	onBeforeUnload: function(e) {
		if (this.fireEvent('beforeunload', this) === false) {
			e.stopEvent();
		}
	},

	onUnload: function(e) {
		// Destroy any active windows to allow each to cleanup resources when necessary.
		// For example, any active subscriptions should be unsubscribed on reload and/or close, which can be done by destroying the window
		this.windows.each(function(window) {
			if (window.destroy) {
				try {
					window.destroy();
				}
				catch (e) {
					console.warn(e);
				}
			}
		});
	},

	createWindow: function(config, cls) { // defaults cls to Ext.Window class
		const w = this.windows.getActive();
		const win = new (cls || Ext.Window)(
			Ext.applyIf(config || {}, {
				app: this,
				manager: this.windows,
				minimizable: true,
				maximizable: true
			})
		);
		if (!win.iconCls && win.defaultIconCls) {
			win.setIconClass(win.defaultIconCls);
		}
		win.render(this.desktopEl);

		// position new window next to the currently active window
		if (win.centerAlign) {
			// do nothing
		}
		else if (win.maximized !== true) {
			if (TCG.isNotNull(w)) {
				const pos = w.getPosition();
				if (pos[0] > 400 || pos[1] > 400) {
					win.setPosition(20, 20); // keep the window visible
				}
				else {
					win.alignTo(w.id, 'tl', [20, 35]);
				}
			}
			else {
				win.setPosition(10, 10); // first window
			}
		}

		win.on({
			'activate': {fn: this.markActive},
			'beforeshow': {fn: this.markActive},
			'deactivate': {fn: this.markInactive},
			'minimize': {fn: this.minimizeWin},
			'close': {fn: this.removeWin},
			'titlechange': function(win, title) {
				win.app.fireEvent('titlechange', win, title);
			}
		});

		this.layout();
		this.fireEvent('createWindow', win);
		return win;
	},

	minimizeWin: function(win) {
		win.minimized = true;
		win.hide();
	},

	markActive: function(win) {
		const app = win.app;
		if (app.activeWindow && app.activeWindow !== win) {
			app.markInactive(app.activeWindow);
		}
		app.activeWindow = win;
		win.minimized = false;
	},

	markInactive: function(win) {
		if (win === win.app.activeWindow) {
			win.app.activeWindow = null;
		}
	},

	removeWin: function(win) {
		win.app.fireEvent('closeWindow', win);
		win.app.layout();
	},

	closeAllWindows: function() {
		this.windows.each(function(w) {
			w.close();
		});
	},

	getWindowCount: function() {
		let count = 0;
		this.windows.each(function() {
			count++;
		});
		return count;
	},

	layout: function() {
		const d = Ext.get('desktop');
		d.setHeight(Ext.lib.Dom.getViewHeight() - d.getTop(true));
		d.setWidth(Ext.lib.Dom.getViewWidth());
	}
});


TCG.app.Application.APPLICATION_EVENT_REGISTRY_CACHE_NAME = 'APPLICATION_EVENT_REGISTRY';

TCG.app.Application.registerApplicationEventListener = function(eventName, processingFunction) {
	let processors = TCG.CacheUtils.get(TCG.app.Application.APPLICATION_EVENT_REGISTRY_CACHE_NAME, eventName);
	if (TCG.isNull(processors)) {
		processors = [];
		TCG.CacheUtils.put(TCG.app.Application.APPLICATION_EVENT_REGISTRY_CACHE_NAME, processors, eventName);
	}
	processors.push(processingFunction);
};

TCG.app.Application.processApplicationEvent = function(eventName, eventData, scope, errorIfNone) {
	const processors = TCG.CacheUtils.get(TCG.app.Application.APPLICATION_EVENT_REGISTRY_CACHE_NAME, eventName);
	if (TCG.isNull(processors)) {
		if (errorIfNone) {
			TCG.showInfo('No event handler registered for [' + eventName + '] application event. Event Data: ' + eventData, 'Application Event: ' + eventName);
		}
	}
	else {
		for (let i = 0; i < processors.length; i++) {
			processors[i].call(scope || this, eventData);
		}
	}
};

// allow overriding methods
TCG.Window = Ext.extend(Ext.Window, {});


TCG.app.Window = function(conf) {
	Ext.apply(this, conf);
	Ext.applyIf(this, {
		title: 'UNDEFINED',
		titlePrefixSeparator: ' - ',
		layout: 'fit',
		modal: false,
		constrainHeader: true,
		border: false,
		events: {} // Short-hand for this.addEvents(...); required for adding listeners
	});
	TCG.app.Window.superclass.constructor.call(this);
	this.init();
};

Ext.extend(TCG.app.Window, Ext.util.Observable, {
	closeWindowTrail: false,
	enableRefreshWindow: false, // Allows clicking to refresh the main form panel
	enableShowInfo: false,
	init: function() {
		const desktop = TCG.app.app; // better way to set this?
		this.win = desktop.getWindow(this.id);
		if (!this.win) {
			let config = Ext.apply({
				width: 800,
				height: 600,
				setTitle: function(title, iconCls) {
					title = this.titlePrefix ? this.titlePrefix + this.titlePrefixSeparator + title : title;
					TCG.Window.superclass.setTitle.call(this, title, iconCls);
				},
				closeWindow: function() {
					this.close();
				}
			}, this);
			config = Ext.apply(config, {
				initComponent: function() {
					const w = this;
					if (w.enableRefreshWindow) {
						w.addTool({
							id: 'refresh',
							qtip: 'Refreshes all components of this window with the latest data.',
							handler: w.doRefreshWindow.createDelegate(w, [])
						});
					}
					if (this.enableShowInfo) {
						this.addTool({
							id: 'help',
							handler: this.showInfo.createDelegate(this, [])
						});
					}
					if (w.closeWindowTrail) {
						w.addTool({
							id: 'left',
							qtip: 'Close window trail: go back to first detail window',
							handler: w.doCloseWindowTrail.createDelegate(w, [true])
						});
					}
					TCG.Window.superclass.initComponent.apply(w, arguments);

					if (w.defaultActiveTabName) { // set the default active tab if specified
						const o = w.items.get(0);
						for (let i = 0; i < o.items.length; i++) {
							const tab = o.items.get(i);
							if (tab.title === w.defaultActiveTabName) {
								if (tab.setActiveTab) {
									tab.setActiveTab(i);
									break;
								}
							}
						}
					}
				}
			});
			this.win = desktop.createWindow(config);
			this.win.addListener('show', this.windowOnShow);
		}
		else if (this.defaultData) {
			this.win.defaultData = this.defaultData; // pass new default data if specified
		}

		// If set by index
		let activeTab = this.defaultActiveTab;
		// otherwise find the index based on the tab title
		if (this.defaultActiveTabName) {
			const o = this.win.items.get(0);
			for (let i = 0; i < o.items.length; i++) {
				if (o.items.get(i).title === this.defaultActiveTabName) {
					activeTab = i;
					break;
				}
			}
		}
		if (activeTab !== undefined) {
			const o = this.win.items.get(0);
			if (o && o.setActiveTab) {
				o.setActiveTab(activeTab);
				const dd = this.win.defaultData;
				if (dd && dd.forceReload) {
					TCG.getChildrenByFunctionName(o.get(activeTab), 'reload').forEach(
						function(child) {
							child.firstLoadProcessed = false;
							child.reload();
						}
					);
					dd.forceReload = false;
				}
			}
		}
		this.win.show();
		this.doUpdateTabCounts(this.win);
	},

	doCloseWindowTrail: function(force) {
		if (this.openerCt) {
			let w = this.openerCt;
			if (w.getWindow) {
				w = w.getWindow();
			}
			if (TCG.isNotNull(w) && w.closeWindowTrail) {
				force = false;
				this.closeWindow();
				w.doCloseWindowTrail();
			}
		}
		if (force) {
			this.closeWindow();
		}
	},

	doRefreshWindow: function() {
		TCG.getChildrenByFunctionName(this, 'reload').forEach(
			function(o) {
				if (o instanceof Ext.Panel) {
					o.reload();
				}
			});
	},

	windowOnShow: Ext.emptyFn,

	showInfo: function() {
		const fp = this.getMainFormPanel();
		const f = fp.getForm();
		const infoWindowClass = this.getInfoWindowClass();
		const data = {
			windowId: this.id,
			url: f.url || fp.url,
			table: fp.table,
			childTables: fp.childTables,
			data: f.formValues
		};
		if (fp.getInfoWindowAuditTrailLoadParams) {
			data.getInfoWindowAuditTrailLoadParams = function() {
				return fp.getInfoWindowAuditTrailLoadParams(fp);
			};
		}
		new infoWindowClass({
			data: data,
			openerCt: fp
		});
	},

	// Can be overridden to show a custom info window - example Portal where we have different user base and audit trail support
	getInfoWindowClass: function() {
		return TCG.app.InfoWindow;
	},

	getTabPosition: function(tabTitle) {
		const tabs = this.items.get(0);
		if (tabs) {
			for (let i = 0; i < tabs.items.length; i++) {
				if (tabs.items.get(i).title === tabTitle) {
					return i;
				}
			}
		}
		return 0;
	},

	doUpdateTabCounts: function(win) {
		if (win.gridTabWithCount) {
			// when specified, dynamically includes row count from corresponding grid in the title of the tab
			const fp = win.getMainFormPanel();
			fp.addListener('afterload', function(fp) {
				if (!fp.gridTabWithCountActivated) {
					fp.gridTabWithCountActivated = true;
					const w = fp.getWindow();
					const tabs = w.items.get(0);
					const gridTabsWithCount = Ext.isArray(w.gridTabWithCount) ? w.gridTabWithCount : [w.gridTabWithCount];
					for (let i = 0; i < gridTabsWithCount.length; i++) {
						let tabToUpdate;
						for (let j = 0; j < tabs.items.length; j++) {
							const tab = tabs.items.get(j);
							if (tab.title === gridTabsWithCount[i]) {
								tabToUpdate = tab;
								break;
							}
						}
						if (tabToUpdate) {
							// If the tab has it's own way of calculating - i.e. a tab that has 2 grids - show total count across both
							if (tabToUpdate.updateTabCount) {
								tabToUpdate.updateTabCount();
							}
							else {
								const gp = tabToUpdate.items.get(0);
								gp.reloadOnRender = false;
								if (gp.ds) {
									gp.ds.addListener('load', function(store, records, opts) {
										const count = store.getCount();
										tabToUpdate.setTitle(gridTabsWithCount[i] + '<b> (' + count + ')</b>');
									});
									gp.reload();
								}
							}
						}
					}
				}
			});
		}
	}
});


TCG.app.URLWindow = Ext.extend(TCG.app.Window, {
	url: 'REQUIRED-URL-OF-THIS-WINDOW',
	enableRefreshWindow: true,

	init: function() {
		Ext.apply(this, {
			items: [{
				html: '<iframe src="' + this.url + '" frameborder="0" width="100%" height="100%" />'
			}]
		});
		TCG.app.URLWindow.superclass.init.apply(this, arguments);
	},
	getIFrame: function() {
		return this.items.items[0].getEl().dom.getElementsByTagName('iframe')[0];
	},
	getURL: function() {
		return this.url;
	},
	setURL: function(url) {
		this.url = url;
		this.getIFrame().src = url;
	},
	doRefreshWindow: function() {
		this.setURL(this.url);
	}
});


TCG.app.DetailWindow = Ext.extend(TCG.app.Window, {
	hideApplyButton: false, // use true if only want OK & Cancel Buttons
	hideOKButton: false, // use true if only want Apply & Cancel Buttons
	hideCancelButton: false, // use true to hide Cancel button
	okButtonText: 'OK',
	okButtonTooltip: 'Save your changes (if any) and close the window',
	cancelButtonText: 'Cancel',
	cancelButtonTooltip: 'Close this window without saving',
	applyButtonText: 'Apply',
	applyButtonTooltip: 'Save your changes and leave the window open',
	applyDisabled: true,
	forceModified: false, // always considers all forms as modified
	enableShowInfo: true,
	enableRefreshWindow: false, // Allows clicking to refresh the main form panel
	doNotWarnOnCloseModified: false, // use true if don't want to warn when closing a modified window
	saveTimeout: 40,
	closeWindowTrail: true,

	init: function() {
		const desktop = TCG.app.app; // better way to set this?
		this.win = desktop.getWindow(this.id);
		if (this.win) {
			if (this.windowOnToFront) {
				// allow hook for window to refresh if previously opened and config data (e.g. params and defaultData) changes
				// pass the window being brought to the front and the window config
				this.windowOnToFront(this.win, this);
			}
		}
		else {
			this.win = desktop.createWindow(Ext.apply({
				width: 600,
				height: 400,
				buttons: [
					{
						text: this.okButtonText,
						tooltip: this.okButtonTooltip,
						width: this.okButtonWidth, // Only if defined will it override the width
						hidden: this.hideOKButton,
						handler: function() {
							this.ownerCt.ownerCt.saveWindow(true);
						}
					}, {
						text: this.cancelButtonText,
						tooltip: this.cancelButtonTooltip,
						hidden: this.hideCancelButton,
						handler: function() {
							this.ownerCt.ownerCt.closeWindow();
						}
					}, {
						text: this.applyButtonText,
						tooltip: this.applyButtonTooltip,
						disabled: this.applyDisabled,
						hidden: this.hideApplyButton,
						handler: function() {
							this.ownerCt.ownerCt.saveWindow(false);
						}
					}
				],
				initComponent: function() {
					if (this.enableRefreshWindow) {
						this.addTool({
							id: 'refresh',
							qtip: 'Refreshes the main form of this window.  If you have any unsaved changes they will be lost.',
							handler: this.refreshWindow.createDelegate(this, [])
						});
					}
					if (this.enableShowInfo) {
						this.addTool({
							id: 'help',
							handler: this.showInfo.createDelegate(this, [])
						});
						this.addTool({
							id: 'left',
							qtip: 'Close window trail: go back to first detail window',
							handler: this.doCloseWindowTrail.createDelegate(this, [true])
						});
					}
					TCG.Window.superclass.initComponent.apply(this, arguments);
				},
				render: function() {
					TCG.Window.superclass.render.apply(this, arguments);
					// handle window closing event
					this.on('beforeclose', function() {
						if (!this.closing) {
							this.closeWindow();
							return false;
						}
						return true;
					});
				},


				refreshWindow: function() {
					const win = this;

					// Window is in the process of saving...
					if (win.windowSaveInProgress === true) {
						TCG.showMessage('Cannot refresh the window.  Window save is in progress.', 'Save In Progress');
						return;
					}

					const fp = this.getMainFormPanel();
					if (fp) {
						// New Entity - Nothing to refresh
						if (TCG.isNull(fp.getForm().idFieldValue)) {
							TCG.showMessage('Cannot refresh window.  Entity has not been created yet, nothing to refresh.', 'Entity Not Created');
							return;
						}
						else {
							// If there is a specific reload method on the formpanel - use that
							if (fp.reload) {
								if (win.isModified()) {
									Ext.Msg.confirm('Discard Changes', 'You are refreshing a window that has unsaved changes.<br/>Would you like to refresh the window without saving?', function(a) {
										if (a === 'yes') {
											fp.reload();
										}
									});
								}
								else {
									fp.reload();
								}
								return;
							}
							// Otherwise call the doLoad method
							else if (fp.doLoad) {
								if (win.isModified()) {
									Ext.Msg.confirm('Discard Changes', 'You are refreshing a window that has unsaved changes.<br/>Would you like to refresh the window without saving?', function(a) {
										if (a === 'yes') {
											fp.doLoad(encodeURI(fp.getLoadURL()));
										}
									});
								}
								else {
									fp.doLoad(encodeURI(fp.getLoadURL()));
								}
								return;
							}
						}
					}
					// Unknown reason...
					TCG.showError('Unable to refresh the window.  Please close and reopen manually.', 'Unable to Refresh');
				},


				closeWindow: function() {
					const win = this;
					win.closing = true;
					if (win.isModified() && !win.doNotWarnOnCloseModified) {
						Ext.Msg.confirm('Discard Changes', 'You are closing a window that has unsaved changes.<br/>Would you like to close the window without saving?', function(a) {
							if (a === 'yes') {
								win.close();
							}
							else {
								win.closing = false;
							}
						}, this.app.activeWindow);
						return false;
					}
					if (win.savedSinceOpen && win.openerCt && !win.openerCt.isDestroyed) {
						if (win.reloadOpener) {
							win.reloadOpener();
						}
						else if (win.openerCt.reload) {
							win.openerCt.reload(win);
						}
					}
					win.close();
				},

				saveWindow: function(closeOnSuccess, forceSubmit) {
					const win = this;
					// avoid double submit
					if (win.windowSaveInProgress === true) {
						window.setTimeout(function() {
							win.windowSaveInProgress = false;
						}, 2000);
						return;
					}
					win.windowSaveInProgress = true;
					window.setTimeout(function() {
						win.windowSaveInProgress = false;
					}, 2000);

					const forms = this.getFormPanels(!this.forceModified && (forceSubmit !== true));
					if (forms.length === 0) {
						if (closeOnSuccess) {
							win.closeWindow();
						}
						return;
					}
					// TODO: if at least one form is invalid, show an error and don't submit anything
					// start with the first form and process others on success
					const panel = forms[0];
					if (panel.readOnly && (forceSubmit !== true)) {
						TCG.showError('Not allowed action: cannot save a read only form.', 'Action Error');
						return;
					}
					const form = panel.getForm();
					const o = panel.getFirstInValidField();

					// get the submit parameters
					let params = {};
					if (panel.getSubmitParams) {
						const returnParams = panel.getSubmitParams();
						if (returnParams) {
							params = returnParams;
						}
					}
					if (form.idFieldValue) {
						params.id = form.idFieldValue;
					}
					const rvField = panel.rowVersionFieldName;
					if (TCG.isNotBlank(rvField)) {
						const rvValue = TCG.getValue('formValues.' + rvField, form);
						if (rvValue != null) {
							params[rvField] = rvValue;
						}
					}

					if (TCG.isNull(o)) {
						// Static confirm message, or function that returns the confirm message if certain conditions apply
						let confirmBeforeSave = null;
						if (panel.getConfirmBeforeSaveMsg) {
							confirmBeforeSave = panel.getConfirmBeforeSaveMsg();
						}
						if (!confirmBeforeSave) {
							confirmBeforeSave = panel.confirmBeforeSaveMsg;
						}

						if (confirmBeforeSave) {
							Ext.Msg.confirm(panel.confirmBeforeSaveMsgTitle, confirmBeforeSave, function(a) {
								if (a === 'yes') {
									win.saveForm(closeOnSuccess, forms, form, panel, params);
								}
							});
						}
						else {
							win.saveForm(closeOnSuccess, forms, form, panel, params);
						}
					}
					else if (o !== false) { // custom error reporting when false
						// make sure the field is visible (active tab)
						let oTab = null;
						const tabPanel = o.findParentBy(function(p) {
							if (p instanceof Ext.TabPanel) {
								return true;
							}
							oTab = p;
							return false;
						});
						if (tabPanel && tabPanel.getActiveTab() !== oTab) {
							tabPanel.setActiveTab(oTab);
						}
						o.focus(o.el.dom.select ? true : false);
						TCG.showError('Please correct validation error(s) before submitting.', 'Validation Error(s)');
					}
				},

				saveForm: function(closeOnSuccess, forms, form, panel, params) {
					const win = this;
					win.closeOnSuccess = closeOnSuccess;
					win.modifiedFormCount = forms.length;
					// TODO: data binding for partial field updates
					// TODO: concurrent updates validation
					form.trackResetOnLoad = true;
					form.submit(Ext.applyIf({
						url: encodeURI(panel.getSaveURL()),
						params: panel.getSaveParams ? panel.getSaveParams() : params,
						waitMsg: 'Saving...',
						submitEmptyText: panel.submitEmptyText,
						success: function(form, action) {
							win.windowSaveInProgress = false;
							win.response = action;
							const events = action.result.APPLICATION_EVENTS_KEY;
							if (events) {
								for (const e in events) {
									if (events.hasOwnProperty(e)) {
										TCG.app.Application.processApplicationEvent(e, events[e], win, true);
									}
								}
							}

							// TODO: move the code to win.loadJsonResult (need to save the form and the panel)
							const createdEntity = TCG.isNull(form.idFieldValue);
							const data = panel.getDataAfterSave(action.result.data);
							if (data !== false) {
								form.idFieldValue = data ? data.id : undefined;
								form.clearInvalid();
								form.setValues(data, true);
								win.savedSinceOpen = true;
								panel.updateTitle();
								panel.fireEvent('afterload', panel, win.closeOnSuccess);
								if (createdEntity) {
									if (TCG.isTrue(win.enableRefreshWindow)) {
										// If not set - set the ID value so reloads can occur without having to close and re-open the window (i.e. enableRefreshWindow)
										if (!win.params) {
											win.params = {};
										}
										if (!win.params.id) {
											win.params.id = data ? data.id : undefined;
										}
									}
									panel.fireEvent('aftercreate', panel, win.closeOnSuccess);
								}
								if (win.modifiedFormCount > 1) {
									win.saveWindow(win.closeOnSuccess);
								}
								else if (win.closeOnSuccess) {
									win.closeWindow();
								}
								else {
									win.setModified(false);
								}
							}
						}
					}, Ext.applyIf({timeout: this.saveTimeout}, TCG.form.submitDefaults)));
				},

				loadJsonResult: function(result) {
					// TODO:
				},

				isModified: function() {
					return (this.getFormPanels(!this.forceModified).length > 0);
				},

				setModified: function(mod) {
					const b = this.buttons[2];
					if (b) {
						if (mod) {
							b.enable();
						}
						else {
							b.disable();
						}
					}
					// Fires Event "modified" and passes the window and whether or not modified is set or cleared
					this.fireEvent('modified', this, mod);
				},

				setTitle: function(title, iconCls) {
					title = this.titlePrefix ? this.titlePrefix + this.titlePrefixSeparator + title : title;
					TCG.Window.superclass.setTitle.call(this, title, iconCls);
				},

				getFormPanels: function(modified) {
					let items = this.items;
					if (items.length === 1 && items.get(0).xtype === 'tabpanel') {
						items = items.get(0).items;
					}
					const result = [];
					items.each(function(i) {
						if (i.getForm && (!modified || (i.controlWindowModified === true && i.getForm().isDirty()))) {
							result[result.length] = i;
						}
						else if (i.items) {
							i.items.each(function(o) {
								if (o.getForm && (!modified || (o.controlWindowModified === true && o.getForm().isDirty()))) {
									result[result.length] = o;
								}
							});
						}
					});
					return result;
				},

				getMainFormPanel: function() {
					return this.getFormPanels()[0];
				},

				getMainForm: function() {
					return this.getMainFormPanel().getForm();
				},

				getMainFormId: function() {
					return this.getMainForm().idFieldValue;
				},

				isMainFormSaved: function() {
					const idValue = '' + this.getMainFormId();
					return !(idValue === '' || idValue === 'undefined');
				}

			}, this), TCG.Window);
			this.win.addListener('show', this.windowOnShow);
		}
		this.win.show();

		this.doUpdateTabCounts(this.win);

		if (TCG.isNotBlank(this.defaultActiveTab) || TCG.isNotBlank(this.defaultActiveTabName)) {
			// Detail Window - Don't switch tab until after main form panel is loaded
			// perform active tab lookup by name after load as well, so any dynamic tabs are present
			const o = this.win.items.get(0);
			const setActiveTabFunction = () => {
				if (this.defaultActiveTab) {
					// set by index
					o.setActiveTab(this.defaultActiveTab);
					// Clear the default active tab - if the main tab is reloadOnChange will continue to send back to the default active tab
					this.defaultActiveTab = undefined;
					return;
				}
				if (this.defaultActiveTabName) {
					// set by name
					for (let i = 0; i < o.items.length; i++) {
						if (o.items.get(i).title === this.defaultActiveTabName) {
							o.setActiveTab(i);
							// Clear the default active tab - if the main tab is reloadOnChange will continue to send back to the default active tab
							this.defaultActiveTabName = undefined;
							return;
						}
					}
				}
			};
			// If setting active tab on a detail window - i.e. main form panel is set
			// Don't switch tabs until main fp is loaded so needed information is loaded
			if (o && o.setActiveTab) {
				const fp = this.win.getMainFormPanel();
				if (fp) {
					fp.addListener('afterload', function(fp) {
						setActiveTabFunction();
					});
				}
				else {
					setActiveTabFunction();
				}
			}
		}
	}
});
Ext.reg('detailwindow', TCG.app.DetailWindow);


// same as DetailWindow but hides Apply button and always forces a Save/Submit (even when there are no changes)
TCG.app.OKCancelWindow = Ext.extend(TCG.app.DetailWindow, {
	hideApplyButton: true,
	forceModified: true,
	doNotWarnOnCloseModified: true
});
Ext.reg('okcancelwindow', TCG.app.OKCancelWindow);

//same as DetailWindow but hides Apply button and always forces a Save/Submit (even when there are no changes)
TCG.app.CloseWindow = Ext.extend(TCG.app.OKCancelWindow, {
	cancelButtonText: 'Close',
	hideOKButton: true
});
Ext.reg('closewindow', TCG.app.CloseWindow);


// Overridden in system project
// if > 0 (asynch server calls); audit trail (capitalize first letter of url and strip up to .)
TCG.app.InfoWindow = Ext.extend(TCG.app.CloseWindow, {
	title: 'Info Window',
	iconCls: 'info',
	modal: true,
	allowOpenFromModal: true,
	minimizable: false,
	maximizable: false,
	enableShowInfo: false,
	closeWindowTrail: true,
	width: 770,

	items: [{
		xtype: 'tabpanel',
		items: [{
			title: 'Info',
			items: [{
				xtype: 'formpanel'
			}]
		}]
	}],

	init: function() {
		const d = this.data;
		let f = this.items[0].items[0].items[0];
		f.items = [];
		f = f.items;
		f.push({fieldLabel: 'Window ID', xtype: 'displayfield', value: d.windowId});
		f.push({fieldLabel: 'URL', xtype: 'displayfield', value: d.url});
		// show table info
		let table = '';
		if (d.url) {
			table = d.url.substring(0, d.url.indexOf('.'));
			table = table.charAt(0).toUpperCase() + table.substr(1);
			f.push({fieldLabel: 'Table Name', xtype: 'displayfield', value: table});
		}
		if (d.childTables) {
			this.items[0].items[1].items[0].childTables = d.childTables;
		}
		// show entity info
		if (d.data) {
			f.push({xtype: 'label', html: '<hr />'});
			this.idFieldValue = d.data.id;
			if (d.data.label) {
				f.push({fieldLabel: 'Label', xtype: 'displayfield', value: d.data.label});
			}
			f.push({fieldLabel: 'Created By', xtype: 'displayfield', value: this.getUserLabel(d.data.createUserId)});
			f.push({fieldLabel: 'Create Date', xtype: 'displayfield', value: d.data.createDate, type: 'date'});
			f.push({fieldLabel: 'Updated By', xtype: 'displayfield', value: this.getUserLabel(d.data.updateUserId)});
			f.push({fieldLabel: 'Update Date', xtype: 'displayfield', value: d.data.updateDate, type: 'date'});
		}

		const label = TCG.getValue('data.label', this.data);
		this.title = 'Info Window for ' + table + ' ' + this.idFieldValue + (TCG.isNull(label) ? '' : ' (' + label + ')');
		TCG.app.InfoWindow.superclass.init.apply(this, arguments);
	},

	getUserLabel: function(userId) {
		return userId;
	}
});


TCG.app.SelectWindow = Ext.extend(TCG.app.DetailWindow, {
	applyButtonText: 'Select',
	applyButtonTooltip: 'Select highlighted rows and close this window',
	applyDisabled: false,
	hideOKButton: true,
	forceModified: true,
	doNotWarnOnCloseModified: true,

	saveWindow: function() {
		const sm = this.getGrid().getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select at least one row or click Cancel.', 'No Row(s) Selected');
		}
		else {
			if (this.openerCt && this.openerCt.processSelection) {
				this.openerCt.processSelection.call(this.openerCt, sm.getSelections());
				this.close();
			}
			else {
				TCG.showError('Opener component must be defined and must have processSelection(records) method.', 'Callback Missing');
			}
		}
	},

	getGrid: function() {
		return this.items.get(0).grid;
	}
});
Ext.reg('selectwindow', TCG.app.SelectWindow);


// extend when there are more than one detail windows based on entities type/etc.
TCG.app.DetailWindowSelector = Ext.extend(Ext.util.Observable, {
	url: 'CHANGE-ME-TO-REAL-URL',

	// can return an object with className property and getEntity(config, entity) function that can replace the entity
	getClassName: function(config, entity) {
		return 'CHANGE-ME-TO-REAL-CLASS-NAME';
	},

	constructor: function(config) {
		TCG.app.DetailWindowSelector.superclass.constructor.call(this, config);
		Ext.apply(this, config);
		const win = this;
		if (!config.params) { // NEW entity
			win.openEntityWindow(config);
		}
		else { // EXISTING entity
			const loader = new TCG.data.JsonLoader({
				params: this.getLoadParams(config),
				timeout: this.loaderTimeout, // set to override default timeout
				onLoad: async (record, conf) => {
					if (record) {
						const preparedRecord = await win.prepareEntity(record);
						win.openEntityWindow(config, preparedRecord);
					}
				}
			});
			loader.load(this.url, this);
		}
	},

	// Can be overridden to add to request to limit results
	requestedProperties: undefined,
	requestedPropertiesRoot: undefined,
	getLoadParams: function(config) {
		const params = {id: config.params.id};
		if (this.requestedPropertiesRoot) {
			params.requestedPropertiesRoot = this.requestedPropertiesRoot;
		}
		if (this.requestedProperties) {
			params.requestedProperties = this.requestedProperties;
		}
		return params;
	},

	prepareEntity: function(entity) {
		return entity;
	},

	openEntityWindow: function(config, entity) {
		const className = this.getClassName(config, entity);
		this.doOpenEntityWindow(config, entity, className);
	},

	doOpenEntityWindow: function(config, entity, className) {
		if (className !== false) {
			if (typeof className === 'object') {
				if (className.getEntity) {
					entity = className.getEntity(config, entity);
				}
				if (className.className) {
					className = className.className;
				}
			}
			if (this.useEntityAsDefaultData(className, config, entity)) {
				// the entity was already retrieved: pass it to the window and instruct not to get it again
				config = Ext.apply(config, {
					defaultDataIsReal: true,
					defaultData: entity
				});
			}
			TCG.createComponent(className, config);
		}
	},

	/**
	 * Function to determine if the window should be launched with the entity as the default data.
	 * If true, the configuration will have the defaultDataIsReal flag set. This can be overridden
	 * by the selector window to use alternative paths for initial loading, such as with form panels
	 * using different urls than the selector window.
	 *
	 * @param className The window to open
	 * @param config The configuration to pass to the window
	 * @param entity The entity to set as default data with defaultDataIsReal in the config
	 * @returns {boolean}
	 */
	useEntityAsDefaultData: function(className, config, entity) {
		return TCG.isNotNull(entity);
	}
});


/**
 * Menu item with sub-menus for managing open windows.
 * Adds/removes Window objects as sub-items when new windows are created/closed.
 */
TCG.app.WindowMenu = function(app) {
	this.app = app;
	this.initComponent();
};

Ext.extend(TCG.app.WindowMenu, Ext.Toolbar.Button, {
	text: 'Window',
	menu: new Ext.menu.Menu({
		items: [
			{
				text: 'Close Active Window',
				handler: function() {
					const w = this.parentMenu.app.activeWindow;
					if (TCG.isNotNull(w)) {
						w.close();
					}
				}
			},
			{
				text: 'Close All Windows',
				handler: function() {
					this.parentMenu.app.closeAllWindows();
				}
			},
			{
				text: 'Cascade Windows',
				handler: function() {
					const app = this.parentMenu.app;
					let winIndex = 0;
					app.windows.each(function(w) {
						if (w.maximized) {
							w.restore();
						}
						else if (w.minimized) {
							w.show();
						}
						w.toFront();
						w.setPosition(10 + 20 * winIndex, 10 + 35 * winIndex);
						winIndex++;
					});
				}
			},
			{
				text: 'Color Theme',
				iconCls: 'colors',
				menu: new Ext.menu.Menu({
					applyTheme: function(themeName) {
						this.ownerCt.parentMenu.ownerCt.applyColorTheme(themeName);
					},
					items: [
						new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Accessibility Theme',
							handler: function() {
								this.parentMenu.applyTheme('Accessibility Theme');
							}
						})
						, new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Black Theme',
							handler: function() {
								this.parentMenu.applyTheme('Black Theme');
							}
						})
						, new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Blue Theme',
							checked: true,
							handler: function() {
								this.parentMenu.applyTheme('Blue Theme');
							}
						})
						, new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Gray Theme',
							handler: function() {
								this.parentMenu.applyTheme('Gray Theme');
							}
						})
						, new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Olive Theme',
							handler: function() {
								this.parentMenu.applyTheme('Olive Theme');
							}
						})
						, new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Slate Theme',
							handler: function() {
								this.parentMenu.applyTheme('Slate Theme');
							}
						})
						, new Ext.menu.CheckItem({
							group: 'theme',
							text: 'Target Process',
							handler: function() {
								this.parentMenu.applyTheme('Target Process');
							}
						})
					]
				})
			},

			'-'
		]
	}),

	initComponent: function() {
		TCG.app.WindowMenu.superclass.initComponent.apply(this, arguments);
		const app = this.getApp();
		this.menu.app = app;
		app.on('createWindow', this.onCreateWindow, this);
		app.on('closeWindow', this.onCloseWindow, this);
		app.on('titlechange', this.onTitleChange, this);
		app.on('afterload', this.trackLastClickedMenu, this);
		this.applyStoredColorTheme();

	},

	applyStoredColorTheme: function() {
		const themeName = TCG.data.GetCookie('IMSColorTheme');

		if (themeName) {
			const submenu = this.menu.items.find(function(item) {
				if (item.text === 'Color Theme') {
					return true;
				}
				return false;
			});

			const menuItem = submenu.menu.items.find(function(item) {
				if (item.text === themeName) {
					return true;
				}
				return false;
			});

			menuItem.setChecked(true);
			this.applyColorTheme(themeName);
		}
	},

	applyColorTheme: function(themeName) {
		let stylesheet = '';
		switch (themeName) {
			case 'Accessibility Theme':
				stylesheet = `${TCG.ExtRootUrl}/resources/css/xtheme-access.css`;
				break;
			case 'Black Theme':
				stylesheet = `${TCG.ExtRootUrl}/resources/css/xtheme-black.css`;
				break;
			case 'Gray Theme':
				stylesheet = `${TCG.ExtRootUrl}/resources/css/xtheme-gray.css`;
				break;
			case 'Olive Theme':
				stylesheet = `${TCG.ExtRootUrl}/resources/css/xtheme-olive.css`;
				break;
			case 'Slate Theme':
				stylesheet = `${TCG.ExtRootUrl}/resources/css/xtheme-slate.css`;
				break;
			case 'Target Process':
				stylesheet = `${TCG.ExtRootUrl}/resources/css/xtheme-tp.css`;
				break;
			default:
		}
		Ext.util.CSS.swapStyleSheet('theme', stylesheet);
		TCG.data.StoreCookie('IMSColorTheme', themeName, new Date().add(Date.YEAR, 10));
	},

	getApp: function() {
		return this.app;
	},
	getMenuFromWindowId: function(win) {
		return 'menu-' + win.id;
	},
	getWindowFromMenuId: function(menu) {
		return menu.id.substring(5);
	},

	onCreateWindow: function(win) {
		// set window icon to menu icon if the window doesn't have an icon
		let iconCls = win.iconCls;
		const lastClicked = this.lastClickedMenu;
		if (lastClicked && !iconCls) {
			iconCls = lastClicked.iconCls;
			win.setIconClass(iconCls);
		}
		// add a new menu item for the newly created window
		const winMenu = this;
		winMenu.menu.add({
			id: winMenu.getMenuFromWindowId(win),
			iconCls: iconCls,
			text: win.title,
			handler: function(m) {
				winMenu.getApp().getWindow(winMenu.getWindowFromMenuId(m)).show();
			}
		});
	},

	onCloseWindow: function(win) {
		// remove menu item corresponding to the window being closed
		this.menu.remove(this.menu.items.get(this.getMenuFromWindowId(win)));
	},

	onTitleChange: function(win, title) {
		// update menu title with this of the window
		const menu = this.menu.items.get(this.getMenuFromWindowId(win));
		menu.setText(title);
	},

	trackLastClickedMenu: function() {
		// save last clicked menu item
		const winMenu = this;
		Ext.menu.Item.prototype.onClick = Ext.menu.Item.prototype.onClick.createInterceptor(function() {
			winMenu.lastClickedMenu = this;
			// do not allow menu execution if active window is modal
			const w = winMenu.getApp().windows.getActive();
			if (TCG.isNotNull(w) && w.modal && !w.allowOpenFromModal) {
				TCG.showError('You must follow instructions in the modal window that is currently active.', 'Menu Disabled');
				return false;
			}
		});
	}
});


TCG.app.ContactSupportMenu = {
	text: 'Contact Support',
	iconCls: 'email',
	handler: function() {
		new TCG.app.OKCancelWindow({
			title: 'Contact Support',
			iconCls: 'email',
			modal: true,
			minimizable: false,
			maximizable: false,
			enableShowInfo: false,
			okButtonText: 'Send',
			okButtonTooltip: 'Generate an email with your support request and open it in Outlook.  You will still need to click Send button in Outlook.',
			width: 750,
			height: 450,

			items: [{
				xtype: 'formpanel',
				instructions: 'Enter information below and be as specific as possible including exact steps to recreate the problem. When you click the "Send" button, the email using information entered will be open in Outlook. You can further edit your description and include screen shots and attachments if appropriate before actually sending the email.<br/><br/><b>Note: do not use this to ask for new features or enhancements to existing functionality.</b>',
				labelWidth: 70,
				items: [
					{
						fieldLabel: 'Priority', name: 'requestPriority', xtype: 'combo', value: 'Medium', mode: 'local',
						store: {
							xtype: 'arraystore',
							data: [
								['Urgent', 'Urgent (<15 minutes 24/7)', 'Production Critical. Multiple users impacted. System wide issue. Need immediate support. < 15 minute response.  Will page support 24/7.'],
								['High', 'High (<1 hour 7am to 5pm PT)', 'High impact event. Single user impacted. Need support as soon as possible. < 1 hour response between 7am and 5pm PT.'],
								['Medium', 'Medium (<3 hours 7am to 5pm PT)', 'Minimal impact. Workaround available or non-urgent. < 3 hour response between 7am and 5pm PT.'],
								['Low', 'Low (<24 hours 7am to 5pm PT)', 'Minor impact. Non urgent. < 24 hour response between 7am and 5pm PT.']
							]
						}, allowBlank: false
					},
					{fieldLabel: 'Subject', name: 'emailSubject', xtype: 'textfield', allowBlank: false},
					{fieldLabel: 'Description', name: 'emailDescription', xtype: 'htmleditor', height: 200, allowBlank: false, plugins: [new TCG.form.HtmlEditorFreemarker()]}
				]
			}],
			saveWindow: function() {
				const fp = this.getMainFormPanel();
				if (fp.getForm().isValid()) {
					const emailSubject = fp.getFormValue('requestPriority') + ': ' + fp.getFormValue('emailSubject');
					const emailBody = '<b>' + TCG.ApplicationName.toUpperCase() + ' Application</b> from ' + window.location.href + '<br/><br/>' + fp.getFormValue('emailDescription');
					TCG.generateEmailFile(emailSubject, 'text/html', emailBody, 'ApplicationSupportRequest.eml', 'support@paraport.com');
					this.close();
				}
				else {
					TCG.showError('Please enter required information before clicking Send.', 'Invalid Input');
				}
			}
		});
	}
};
