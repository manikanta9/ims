Ext.ns('TCG.form');

Ext.Element.prototype.getValue = function(asNumber) {
	const val = this.dom ? this.dom.value : undefined; // CHANGE: check if dom exists to avoid errors
	return asNumber ? parseInt(val, 10) : val;
};

// CHANGE: send 0 to server for unchecked checkboxes; send values for disabled elements (unchecked mutuallyExclusive checkbox)
// If the form values should not be encoded, pass in true for the disableEncoding attribute.
Ext.lib.Ajax.serializeForm = function(form, disableEncoding) {
	const fElements = form.elements || (document.forms[form] || Ext.getDom(form)).elements;
	let hasSubmit = false;
	const encoder = encodeURIComponent;
	let name,
		data = '',
		type;

	Ext.each(fElements, function(element) {
		name = element.name;
		type = element.type;

		// CHANGE: include disabled
		if (name && element.doNotSubmit !== true) {
			if (/select-(one|multiple)/i.test(type)) {
				Ext.each(element.options, function(opt) {
					if (opt.selected) {
						data += String.format('{0}={1}&',
							(TCG.isTrue(disableEncoding) ? name : encoder(name)),
							(TCG.isTrue(disableEncoding) ? (opt.hasAttribute ? opt.hasAttribute('value') : TCG.isNotNull(opt.getAttribute('value'))) ? opt.value : opt.text : encoder((opt.hasAttribute ? opt.hasAttribute('value') : TCG.isNotNull(opt.getAttribute('value'))) ? opt.value : opt.text)));
					}
				});
			}
			else if (!/file|undefined|reset|button/i.test(type)) {
				if (!(/radio/i.test(type) && !element.checked) && !(type === 'submit' && hasSubmit)) {
					// CHANGE: set to 0 if unchecked
					const value = (type === 'checkbox' && !element.checked) ? '0' : element.value;
					data += (TCG.isTrue(disableEncoding) ? name : encoder(name)) + '=' + (TCG.isTrue(disableEncoding) ? value : encoder(value)) + '&';
					hasSubmit = /submit/i.test(type);
				}
			}
		}
	});
	return data.substr(0, data.length - 1);
};

/*
 * Fix library getErrors method to treat nulls as missing values. Direct references to undefined have been removed and replaced with null-compatible references.
 */
Ext.form.NumberField.prototype.getErrors = function(value) {
	const errors = Ext.form.NumberField.superclass.getErrors.apply(this, arguments);

	const normalizedValue = value != null ? value : this.processValue(this.getRawValue());
	if (normalizedValue.length != null && normalizedValue.length < 1) {
		return errors;
	}

	const stringValue = `${normalizedValue}`.replace(this.decimalSeparator, '.');
	const numericValue = this.parseValue(stringValue);

	if (isNaN(Number(value))) {
		errors.push(String.format(this.nanText, value));
	}

	if (this.minValue != null && numericValue < this.minValue) {
		errors.push(String.format(this.minText, this.minValue));
	}

	if (this.maxValue != null && numericValue > this.maxValue) {
		errors.push(String.format(this.maxText, this.maxValue));
	}

	return errors;
};

// enable formatting for number fields
TCG.form.IntegerField = Ext.extend(Ext.form.NumberField, {
	allowDecimals: false,
	decimalPrecision: 0,
	variablePrecision: false, // keeps the format but shows any number of decimals (ignores decimal precision)
	baseChars: '0123456789,', // also allow commas

	initComponent: function() {
		if (this.variablePrecision) {
			this.allowDecimals = true;
		}
		TCG.form.IntegerField.superclass.initComponent.apply(this, arguments);
		if (this.onChangeHandler) {
			this.on('change', this.onChangeHandler);
		}

		// this will make enter behave like tab when field is in a form
		this.on('specialkey', function(field, e) {
			if (e.getKey() === e.ENTER) {
				const next = field.nextSibling();
				if (next) {
					next.focus();
				}
			}
		});

	},

	getParentFormPanel: function() {
		const parent = TCG.getParentFormPanel(this);
		return parent || this.ownerCt;
	},

	initEvents: function() {
		const field = this;
		this.mon(this.el, Ext.EventManager.getKeyEvent(), function(e) {
			// this will make enter behave like tab when field is in a grid
			if (e.keyCode === Ext.EventObjectImpl.prototype.ENTER && !field.nextSibling()) {
				e.keyCode = Ext.EventObjectImpl.prototype.TAB;
				if (e.browserEvent) {
					e.browserEvent.keyCode = Ext.EventObjectImpl.prototype.TAB;
				}
			}
			field.fireKey(e);
		}, this);

		this.mon(this.el, 'focus', this.onFocus, this);

		// standardise buffer across all browsers + OS-es for consistent event order.
		// (the 10ms buffer for Editors fixes a weird FF/Win editor issue when changing OS window focus)
		this.mon(this.el, 'blur', this.onBlur, this, this.inEditor ? {buffer: 10} : null);

	},

	getValue: function() { // raw value is already formatted
		return this.getRawValue();
	},

	// format values
	setValue: function(v) {
		v = Ext.isNumber(v) ? v : parseFloat(this.parseValue(v));
		let format = '0,000';
		if (this.variablePrecision === false && this.decimalPrecision > 0) {
			format += '.';
			for (let i = 0; i < this.decimalPrecision; i++) {
				format += '0';
			}
		}
		v = TCG.numberFormat(v, format, this.variablePrecision, this.decimalPrecisionMax, this.decimalPrecisionMin).replace('.', this.decimalSeparator);
		return Ext.form.NumberField.superclass.setValue.call(this, v);
	},
	getNumericValue: function() {
		return parseFloat(this.parseValue(this.getValue()));
	},
	// remove formatting commas
	parseValue: function(value) {
		value = parseFloat(this.removeFormattingChars(String(value).replace(this.decimalSeparator, '.')));
		return isNaN(value) ? '' : value;
	},
	processValue: function(value) {
		return this.removeFormattingChars(Ext.form.NumberField.superclass.processValue.call(this, value));
	},
	removeFormattingChars: function(value) {
		return value.replace(/,/g, '');
	}
});
Ext.reg('integerfield', TCG.form.IntegerField);

TCG.form.FloatField = Ext.extend(TCG.form.IntegerField, {
	allowDecimals: true,
	variablePrecision: true
});
Ext.reg('floatfield', TCG.form.FloatField);

TCG.form.CurrencyField = Ext.extend(TCG.form.IntegerField, {
	allowDecimals: true,
	decimalPrecision: 2
});
Ext.reg('currencyfield', TCG.form.CurrencyField);

TCG.form.CurrencyField4 = Ext.extend(TCG.form.IntegerField, {
	allowDecimals: true,
	variablePrecision: true,
	decimalPrecisionMin: 2,
	decimalPrecisionMax: 4
});
Ext.reg('currencyfield4', TCG.form.CurrencyField4);

// Supports 32nds: "98-3" is "98 + 3/32"
TCG.form.PriceField = Ext.extend(TCG.form.FloatField, {
	// remove formatting commas and support 32nds: "98-3" is "98 + 3/32"
	parseValue: function(value) {
		let strValue = this.removeFormattingChars(String(value).replace(this.decimalSeparator, '.'));
		let suffix32 = 0;
		if (strValue.length > 3) {
			const index32 = strValue.indexOf('-', 1);
			if (index32 !== -1) {
				suffix32 = parseFloat(strValue.substring(index32 + 1)) / 32;
				strValue = strValue.substring(0, index32);
			}
		}
		value = parseFloat(strValue);
		return isNaN(value) ? '' : value + suffix32;
	}
});
Ext.reg('pricefield', TCG.form.PriceField);

// all form fields support the following additional properties:
//   - mutuallyExclusiveFields: an array of field names to be disabled when this field is populated
//   - mutuallyExclusiveRemoteFields: An array of remote field names (for dynamic forms) which shall be disabled when this field is populated
//   - requiredFields: an array of field names that must be populated before this field is enabled
//   - requiredRemoteFields: An array of remote field names (for dynamic forms) which must be populated before this field is enabled
//	When using requiredFields, can also use: doNotClearIfRequiredChanges so the dependent value won't reset if required field has a value, can
// 	also use: setVisibilityOnRequired - which instead of enabling/disabling the field, will make the field visible/hidden.
Ext.form.Field.prototype.afterRender = Ext.form.Field.prototype.afterRender.createSequence(function() {
	const field = this;
	const formPanel = TCG.getParentFormPanel(field);
	const form = formPanel && formPanel.getForm();
	let dynamicFieldNames;
	const doNotClearIfRequiredChanges = field.doNotClearIfRequiredChanges;

	//Mutually Exclusive Fields By Label is used on formwithdynamicfields since the field names are created dynamically
	if (this.mutuallyExclusiveFieldsByLabel) {
		dynamicFieldNames = [];
		Ext.each(field.mutuallyExclusiveFieldsByLabel, function(label) {
			dynamicFieldNames.push(formPanel.getDynamicFieldNameByFieldLabel(label));
		});
		field.mutuallyExclusiveFields = dynamicFieldNames;
	}

	// Add remote fields to list of mutually-exclusive fields
	if (field.mutuallyExclusiveRemoteFields && field.mutuallyExclusiveRemoteFields.length) {
		field.mutuallyExclusiveFields = field.mutuallyExclusiveFields || [];
		Ext.each(field.mutuallyExclusiveRemoteFields, function(mutuallyExclusiveFieldName) {
			const mutuallyExclusiveField = form.findRemoteField(mutuallyExclusiveFieldName);
			if (!mutuallyExclusiveField) {
				console.warn('Unable to find mutually exclusive remote field [' + mutuallyExclusiveFieldName + '] for field [' + field.name + '].');
				return;
			}
			field.mutuallyExclusiveFields.push(mutuallyExclusiveField.id);
			mutuallyExclusiveField.on('destroy', () => field.mutuallyExclusiveFields.remove(mutuallyExclusiveField.id));
		});
	}

	// Apply mutually-exclusive fields
	if (field.mutuallyExclusiveFields && field.mutuallyExclusiveFields.length) {
		if (field.setValue) {
			field.setValue = field.setValue.createSequence(field.handleMutuallyExclusiveFields);
		}
		if (field.clearValue) {
			field.clearValue = field.clearValue.createSequence(field.handleMutuallyExclusiveFields);
		}
		else {
			field.on('blur', field.handleMutuallyExclusiveFields);
		}
	}

	// Add remote fields to list of required fields
	if (field.requiredRemoteFields && field.requiredRemoteFields.length) {
		field.requiredFields = field.requiredFields || [];
		Ext.each(field.requiredRemoteFields, function(requiredFieldName) {
			const requiredField = form.findRemoteField(requiredFieldName);
			if (!requiredField) {
				console.warn('Unable to find required remote field [' + requiredFieldName + '] for field [' + field.name + '].');
				return;
			}
			field.requiredFields.push(requiredField.id);
			requiredField.on('destroy', () => field.requiredFields.remove(requiredField.id));
		});
	}

	// Apply dependencies on required fields
	if (field.requiredFields && field.requiredFields.length) {
		Ext.each(field.requiredFields, function(requiredFieldName) {
			const requiredField = form.findField(requiredFieldName);
			if (!requiredField) {
				console.warn('Unable to find required field [' + requiredFieldName + '] for field [' + field.name + '].');
				return;
			}
			applyRequiredField(field, requiredField);
		});
		// disable the field if at least one required field is not set
		field.updateEnabled(field, field.getValue());
	}

	/**
	 * Applies a dependency from the given field to the given required field.
	 *
	 * @param {object} field the field which is dependent upon another field
	 * @param {string} requiredField the field which is depended upon
	 */
	function applyRequiredField(field, requiredField) {
		if (doNotClearIfRequiredChanges === true) {
			requiredField.doNotClearIfRequiredChanges = doNotClearIfRequiredChanges;
		}
		// set dependentFields property on required fields and start monitoring changes
		if (!requiredField.dependentFields) {
			requiredField.dependentFields = [];
		}
		requiredField.dependentFields.push(field.id);
		field.on('destroy', () => requiredField.dependentFields.remove(field.id));
		if (requiredField.setValue) {
			requiredField.setValue = requiredField.setValue.createInterceptor(Ext.form.Field.handleDependentFields, requiredField);
		}
		if (requiredField.clearValue) {
			requiredField.clearValue = requiredField.clearValue.createInterceptor(Ext.form.Field.handleDependentFields, requiredField);
		}
		else {
			requiredField.on('change', function(triggerField, newValue, oldValue) {
				Ext.form.Field.handleDependentFields.call(triggerField, newValue, oldValue);
			}, requiredField);
		}
	}
});
//the method must be invoked in form field scope and pass new field value
// setValue/clearValue will pass new field value only, but change event will pass both
Ext.form.Field.handleDependentFields = function(value, oldValue) {
	// No old value, set it to this.value (used for setValue/clearValue)
	if (TCG.isNull(oldValue)) {
		oldValue = this.value;
	}
	let doNotClear = false;
	// Value is Cleared - Always Validate
	const valueCleared = (TCG.isFalse(value) || TCG.isBlank(value));
	if (oldValue !== value || valueCleared === true) {
		if (this.doNotClearIfRequiredChanges === true && valueCleared === false) {
			// Do not trigger clearing dependent field if value is currently populated with something
			doNotClear = true;
		}
		const field = this;
		const form = TCG.getParentFormPanel(this).getForm();
		Ext.each(this.dependentFields, function(name) {
			const dependentField = form.findField(name);
			if (doNotClear !== true && dependentField.doNotClearIfRequiredSet !== true) {
				// clear dependent field value
				if (dependentField.clearAndReset && (dependentField.mode === 'remote' || dependentField.loadAll === true)) {
					dependentField.clearAndReset(); // force requery for combo
				}
				else if (dependentField.clearValue) {
					dependentField.clearValue();
					dependentField.lastQuery = null;
				}
				else {
					dependentField.setValue(undefined);
				}
			}
			field.updateEnabled(dependentField, value);
		});
	}
	return true;
};

Ext.override(Ext.form.Field, {
//Slightly modified from:
// https://www.sencha.com/forum/showthread.php?65136-2-2-Ext-Element-setAttribute-(and-ability-to-update-qtip-on-the-fly)
	setAttributeNS: function(ns, att, value) {
		const fieldEl = this.el;
		if (fieldEl.dom.setAttributeNS) {
			fieldEl.dom.setAttributeNS(ns, att, value);
		}
		else if (fieldEl.dom.setAttribute) {
			fieldEl.dom.setAttribute(ns + ':' + att, value);
		}
	},
	setFieldLabel: function(text) {
		if (this.rendered) {
			this.el.up('.x-form-item', 10, true).child('.x-form-item-label').update(text);
		}
		this.fieldLabel = text;
	},
	updateEnabled: function(targetField, value) {
		const eventField = this;
		let useVisibility = false;
		let enable;
		let form;
		let requiredField, requiredFieldValue;
		// Evaluate mutual-exclusion rule
		enable = !(targetField.mutuallyExclusiveBy && targetField.mutuallyExclusiveBy.length > 0);
		// Evaluate required-fields rule
		if (targetField.requiredFields) {
			// Option for dependencies on other fields to enable/disable the field, or set visible/hidden
			// Useful for long chains of fields to reduce clutter on the screen when most cases the additional fields aren't used
			useVisibility = !!targetField.setVisibilityOnRequired;
			form = TCG.getParentFormPanel(targetField).getForm();
			Ext.each(targetField.requiredFields, function(requiredFieldName, index, array) {
				requiredField = form.findField(requiredFieldName);
				// This method may be called as an interceptor to setValue, so prefer using the passed value for the own field
				requiredFieldValue = (requiredField === eventField) ? value : requiredField.getValue();
				if (requiredField.xtype === 'checkbox') {
					enable = TCG.isTrue(requiredFieldValue);
				}
				else if (!requiredFieldValue) {
					enable = false;
				}
				return enable;
			});
		}
		// Apply visibility or enabled/disabled status
		if (useVisibility && targetField.setVisible) {
			targetField.setVisible(enable);
		}
		else {
			targetField[enable ? 'enable' : 'disable']();
		}
	},
	/**
	 * Applies mutual-exclusion rules to all associated fields. If this field has no value, then associated fields with no remaining exclusion-triggering fields
	 * will be enabled. Otherwise, all associated fields will be disabled.
	 */
	handleMutuallyExclusiveFields: function() {
		const field = this;
		// Get field value, using the value assigned via the container if present
		const value = field.listField ? field.listField.getValue() : field.getValue();
		const enable = (TCG.isBlank(value) || TCG.isFalse(value));

		// Guard-clause: Skip initial processing for disabled checkboxes
		if (!this.mutuallyExclusiveInitCompleted && enable && this.isXType('checkbox')) {
			this.mutuallyExclusiveInitCompleted = true;
			return;
		}
		// Apply mutual-exclusion values
		const form = TCG.getParentFormPanel(this).getForm();
		Ext.each(this.mutuallyExclusiveFields, function(name) {
			const otherField = form.findField(name);
			otherField.mutuallyExclusiveBy = otherField.mutuallyExclusiveBy || [];
			// Toggle excluded-by-mutual-exclusion flag based on whether this field is populated
			if (enable) {
				otherField.mutuallyExclusiveBy.remove(field.name);
			}
			else if (otherField.mutuallyExclusiveBy.indexOf(field.name) < 0) {
				otherField.mutuallyExclusiveBy.push(field.name);
			}
			field.updateEnabled(otherField, value);
		});
	}
});

Ext.override(Ext.form.Checkbox, {
	setBoxLabel: function(boxLabel) {
		this.boxLabel = boxLabel;
		if (this.rendered) {
			this.wrap.child('.x-form-cb-label').update(boxLabel);
		}
	}
});

// Fix owner container property not being propagated to children by default
Ext.override(Ext.form.CompositeField, {
	initComponent: Ext.util.Functions.createSequence(Ext.form.CompositeField.prototype.initComponent, function() {
		if (this.innerCt) {
			this.innerCt.ownerCt = this;
		}
	})
});

// Extension of Checkbox with Additional Listener to Reload Grid on check
TCG.form.ToolbarCheckbox = Ext.extend(Ext.form.Checkbox, {
	/**
	 * The value which shall be used for the field when its checkbox is checked. If this is a boolean, then its opposite value will be used when the checkbox is unchecked.
	 * Otherwise, <tt>undefined</tt> will be used when the checkbox is unchecked.
	 */
	checkedValue: true,
	listeners: {
		check: function(field) {
			TCG.getParentByClass(field, Ext.Panel).reload();
		}
	},
	getValue: function() {
		const checked = TCG.callSuper(this, 'getValue', arguments);
		let result;
		if (typeof this.checkedValue === 'boolean') {
			// Use boolean true/false result
			result = checked && this.checkedValue;
		}
		else {
			// Use specified value or undefined
			result = checked ? this.checkedValue : void 0;
		}
		return result;
	}
});
Ext.reg('toolbar-checkbox', TCG.form.ToolbarCheckbox);

//custom Vtype for email (adds support for ' - copied from ExtJs 3.4 which has the correct syntax
const emailTest = /(?:[A-Za-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[A-Za-z0-9!#$%&'*+\/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(\w[\-\w]*\.){1,5}([A-Za-z]){2,6}$/;

Ext.apply(Ext.form.VTypes, {
	//  vtype validation function
	email: function(val, field) {
		return emailTest.test(val);
	},
	emailText: 'This field should be an e-mail address in the format "user@example.com"',
	emailMask: /[A-Za-z0-9_.\-+'#@]/i
});

TCG.form.DisplayField = Ext.extend(Ext.form.DisplayField, {
	setValue: function(v) {
		let args = arguments;
		if (this.type === 'date') {
			args = [TCG.renderDate(v)];
		}
		else if (this.type === 'currency') {
			args = [Ext.util.Format.number(v, '0,000.00')];
		}
		else if (this.type === 'float') {
			args = [TCG.numberFormat(v, '0,000.00', true)];
		}
		if (this.valuePrefix && args[0]) {
			args[0] = this.valuePrefix + args[0];
		}
		TCG.form.DisplayField.superclass.setValue.apply(this, args);
	}
});
Ext.reg('displayfield', TCG.form.DisplayField);

TCG.form.SectionHeaderField = Ext.extend(Ext.form.Label, {
	header: 'SECTION HEADER',
	onRender: function(ct, position) {
		if (!this.el) {
			this.el = document.createElement('label');
			this.el.id = this.getId();
			this.el.innerHTML = '<div class="sectionHeader"><span class="x-grid3-hd-text">' + this.header + '</span></div>';
			if (this.forId) {
				this.el.setAttribute('for', this.forId);
			}
		}
		Ext.form.Label.superclass.onRender.call(this, ct, position);
	}
});
Ext.reg('sectionheaderfield', TCG.form.SectionHeaderField);

Ext.override(Ext.ux.form.SpinnerField, {
	allowDecimals: false,
	minValue: 1,
	maxValue: 100000,

	afterRender: function() {
		Ext.ux.form.SpinnerField.superclass.afterRender.apply(this, arguments);
		// the form must set/clear modified on spin
		this.on('spin', () => this.fireEvent('valid', this));
		/*
		 * ExtJS bug fix (related to https://www.sencha.com/forum/showthread.php?74556-SpinnerField-no-change-event):
		 * Allow the spinner to manage on-blur event handling. The spinner creates a wrapper field around the element so that blur events are not triggered until both the spinner
		 * and the field lose focus. The original implementation goes half-way to have the spinner create the wrapper field but does not correctly process the blur events.
		 */
		// Remove DOM blur handler
		this.mun(this.el, 'blur', this.onBlur, this);
		// Initiate focus on spinner click
		this.spinner.trigger.addListener('mousedown', () => this.spinner.doFocus());
	},

	// Re-enable standard blur handler for event (erroneously disabled by framework implementation)
	onBlur: Ext.ux.form.SpinnerField.superclass.onBlur
});

// delegate is needed to set the field on the spinner so the spin event has a reference to it
Ext.ux.form.SpinnerField.prototype.constructor = Ext.ux.form.SpinnerField.prototype.constructor.createDelegate(function() {
	this.spinner.field = this;
});

// Override form actions (loads, saves, etc.) to use proxied application URIs
Ext.override(Ext.form.Action, {
	getUrl: (origFn => function(appendParams) {
		const uri = origFn.apply(this, arguments);
		return TCG.getApplicationUrl(uri, this.form);
	})(Ext.form.Action.prototype.getUrl)
});

// also find checkboxes inside of checkboxgroup
Ext.form.BasicForm.prototype.findField = function(id, searchNested) {
	let field = this.items.get(id);
	if (!field) {
		this.items.each(function(f) {
			// Guard-clause: Apply to form fields only
			if (!f.isFormField || TCG.isTrue(f.isDestroyed)) {
				return;
			}
			if (f.dataIndex === id || f.id === id || f.getName() === id || f.name === id) {
				field = f;
				return false;
			}
			if (searchNested && f.getName() && f.getName().indexOf(id + '.') === 0) {
				field = f;
				return false;
			}
			if (f.items) {
				const items = f.items;
				for (let i = 0; i < items.length; i++) {
					const o = items.get ? items.get(i) : items[i];
					if (o.name === id) {
						field = o;
						return false;
					}
				}
			}
		});
	}
	return field || null;
};
/**
 * Finds a remote field with the given name, if one exists. Remote fields are generated by dynamic forms when retrieving fields from the host.
 *
 * @param remoteName the remote field name
 * @param searchNested <tt>true</tt> if fields pointing to children of the given name should match, or <tt>false</tt> otherwise
 * @return {*|null} the field corresponding to the remote field name, if found
 */
Ext.form.BasicForm.prototype.findRemoteField = function(remoteName, searchNested) {
	let field = null;
	this.items.each(function(item) {
		let child, i;

		// Guard-clause: Apply to remote form fields only
		if (!item.isFormField || !item.remoteName) {
			return;
		}

		// Check if current field matches
		if (item.remoteName === remoteName || searchNested && item.remoteName.indexOf(remoteName + '.') === 0) {
			field = item;
		}
		// If the item has children, check if a child field matches
		else if (item.items) {
			for (i = 0; i < item.items.length; i++) {
				child = item.items.get ? item.items.get(i) : item.items[i];
				if (child.remoteName === remoteName) {
					field = item;
					break;
				}
			}
		}

		// Terminate when a field is found
		return !field;
	});
	return field;
};
Ext.form.BasicForm.prototype.setValues = function(values, resetAll) {
	if (Ext.isArray(values)) {
		for (let i = 0, len = values.length; i < len; i++) {
			const v = values[i];
			const f = this.findField(v.id);
			if (f) {
				f.setValue(v.value);
				if (this.trackResetOnLoad) {
					f.originalValue = f.getValue();
				}
			}
		}
	}
	else {
		// CHANGE: loop through form fields and set corresponding data instead of looping through data
		this.formValues = values; // store form data so that it can be used later (could do a better solution)
		const form = this;
		this.items.each(function(f) {
			// Guard-clause: Apply to non-destroyed form fields only
			if (TCG.isTrue(f.isDestroyed)) {
				return;
			}

			let value;
			if (f.isFormField && f.items) {
				const items = f.items;
				for (let i = 0; i < items.length; i++) {
					const o = items.get ? items.get(i) : items[i];
					if (o.getName) {
						// Works correctly for combo boxes that should return hiddenName, not name - same as below
						value = TCG.getValue(o.getName(), values);
					}
					else {
						value = TCG.getValue(o.name, values);
					}
					if (o.xtype === 'datefield') {
						value = Date.parseDate(value, Ext.data.Field.prototype.dateFormat);
					}
					else if (f.xtype === 'radiogroup' && (TCG.isNull(f.name) || f.name === o.name)) { // only set radio that matches the value
						if (o.inputValue !== value) {
							value = null;
							if (form && form.trackResetOnLoad) {
								o.originalValue = o.getValue ? o.getValue() : o.value;
							}
						}
						else {
							if (TCG.isNotNull(f.getValue()) && TCG.isNotEquals(f.getValue(), o)) {
								f.getValue().originalValue = false;
							}
							o.checked = true;
							value = true;
						}
					}
					else if (f.xtype === 'secretfield' || f.xtype === 'secrettextarea') {
						if (form && form.trackResetOnLoad) {
							o.originalValue = o.getValue ? o.getValue() : o.value;
						}
					}
					if (TCG.isNotNull(value)) {
						if (o.setValue) {
							o.setValue(value);
						}
						else {
							o.value = value;
						}
						if (form && form.trackResetOnLoad || resetAll) {
							o.originalValue = o.getValue ? o.getValue() : o.value;
						}
					}
				}
			}
			else {
				value = TCG.getValue(f.getName(), values);
				if (f.xtype === 'datefield' && !(value instanceof Date)) {
					value = Date.parseDate(value, Ext.data.Field.prototype.dateFormat);
				}

				if (TCG.isNotNull(value)) {
					f.setValue(value);
					if (form.trackResetOnLoad || resetAll) {
						f.originalValue = f.getValue();
					}
				}
			}
		});
	}
	if (resetAll) {
		this.items.each(function(f) {
			// Guard-clause: Apply to non-destroyed form fields only
			if (TCG.isTrue(f.isDestroyed)) {
				return;
			}
			f.originalValue = f.getValue();
			if (f.items && f.items.each) { // also reset nested fields: 'compositefield'
				f.items.each(function(f1) {
					f1.originalValue = f1.getValue();
				});
			}
		});
	}
	return this;
};

TCG.form.TextFieldLookup = Ext.extend(Ext.form.TextField, {
	root: 'data.rows',
	idField: 'id',
	textField: 'name',
	tooltipField: 'description',
	disabled: true,

	setValue: function(v) {
		this.realValue = v;
		const loader = new TCG.data.JsonLoader({
			conf: {field: this},
			onLoad: function(record, conf) {
				const o = conf.field;
				o.setRawValue(this.getNode(record, o.textField));
				/* FF doesn't display QuickTips for disabled fields
				 Ext.QuickTips.register({
				 target:  o.getEl(),
				 text: this.getNode(record, o.tooltipField)
				 });*/
				o.getEl().set({'title': this.getNode(record, o.tooltipField)});
			}
		});
		loader.load(this.getURL(), this);
	},
	// TODO: on form submit get 'realValue' when getting state (override getValue???)

	getURL: function() {
		return this.url + '?' + this.idField + '=' + this.realValue;
	}
});
Ext.reg('textfield-lookup', TCG.form.TextFieldLookup);

TCG.form.ComboBox = Ext.extend(Ext.form.ComboBox, {
	// detailPageClass: 'detail.page.class.string for drill down into details',
	disableAddNewItem: false, // applies only when detailPageClass is specified and can disable addition of new items
	minListWidth: 270,
	maxHeight: 350,
	selectOnFocus: true,
	forceSelection: true,
	typeAhead: true,
	queryDelay: 100,
	mode: 'remote',
	loadAll: false, // set true to load all elements (small lists)
	requeryOnFormUpdate: false, // reset combo state and force requery after successful form update
	disableUntilTheFormIsCreated: false, // disable combo until the form is successfully loaded/created
	displayField: 'name',
	comboDisplayField: undefined, // optional override to how the combo is displayed vs. the displayField which is displayed when selected
	displayIconClsField: undefined, // optional field with CSS class for icon to put in prefix display field with
	valueField: 'id',
	tooltipField: 'description',
	tooltipPrefix: '',
	triggerAction: 'all',
	emptyText: '< Select >',
	msgTarget: 'side',
	listClass: 'x-combo-list-small',
	root: 'data.rows',
	queryParam: 'searchPattern',
	requestedProps: undefined, // optional additional properties to request
	limitRequestedProperties: true, // set to true to minimize the size of data returned to only value/text/description fields
	minChars: 0,
	doNotTrimStrings: true, // server setting to allow spaces in searches
	doNotAddContextMenu: false,

	// Used for Combos in Grids to copy values - defaults to enabled for everything
	addGridCopyContextMenu: true,
	copyMenuItemName: 'Copy To All Rows',

	// Used to reset mode between queries
	originalMode: undefined,

	initComponent: function() {
		this.originalMode = this.mode;
		const remote = (this.mode === 'remote');
		if (remote) {
			this.typeAhead = false; // true to populate and autoselect the remainder of the text being typed after a configurable delay (typeAheadDelay) if it matches a known value (defaults to false)
			if (!this.pageSize && !this.loadAll) {
				this.pageSize = 15;
			}
		}

		// If the submit value is set to false and not undefined or true, set a flag that
		// will remove the name attribute from the hiddenField after render so nothing is submitted.
		// We need to do this because the Ext.form.ComboBox sets submitValue to false any time
		// a hiddenName is set, it does that to avoid submitting the combo box because it
		// will instead submit the hiddenField that it creates to hold the combo value.
		if (this.submitValue === false && this.hiddenName) {
			this.doNotSubmitValue = true;
		}

		let requestedProps = this.valueField + '|' + this.displayField + '|' + this.tooltipField;
		if (this.requestedProps) {
			requestedProps += '|' + this.requestedProps;
		}
		if (this.comboDisplayField) {
			requestedProps += '|' + this.comboDisplayField;
		}
		this.urlFields = [this.valueField, {name: 'label', mapping: this.displayField}, {name: 'tooltip', mapping: this.tooltipField}, {name: 'comboLabel', mapping: (this.comboDisplayField ? this.comboDisplayField : this.displayField)}];
		if (!this.tpl) {
			if (this.url) {
				this.displayField = 'label';
				this.tooltipField = 'tooltip';
				this.comboDisplayField = (this.comboDisplayField ? 'comboLabel' : 'label');
			}
			else if (!this.comboDisplayField) {
				this.comboDisplayField = this.displayField;
			}
			const item = this.displayIconClsField ? '<table><tbody><tr><td><div class="{' + this.displayIconClsField + '} x-icon-combo-icon" /></td><td>{' + this.comboDisplayField + '}</td></tr></tbody></table>' : '{' + this.comboDisplayField + '}';
			this.tpl = '<tpl for="."><div class="x-combo-list-item" ext:qtip="' + this.tooltipPrefix + '{' + this.tooltipField + ':htmlEncode}">' + item + '</div></tpl>';
		}

		if (!remote || this.loadAll) {
			this.root = 'data';
		}
		if (this.store) {
			// so that it works on second try
			this.store = Ext.apply({}, this.store);
		}
		TCG.form.ComboBox.superclass.initComponent.apply(this, arguments);
		if (this.url) { // TODO: what if the list is loaded before the form?
			if (this.beforequery) {
				this.on('beforequery', this.beforequery);
			}
			if (this.selectHandler) { // avoid naming conflicts with extjs
				this.on('select', this.selectHandler);
			}
			if (this.blur) {
				this.on('blur', this.blur);
			}
			const applicationUrl = TCG.getApplicationUrl(this.url, this);
			this.store = new TCG.data.JsonStore({
				root: this.root,
				totalProperty: (remote && !this.loadAll) ? 'data.total' : 'totalRowCount',
				fields: this.fields ? this.fields : this.urlFields,
				url: encodeURI(applicationUrl),
				autoLoad: !remote
			});
			if (this.limitRequestedProperties) {
				this.store.setBaseParam('requestedPropertiesRoot', this.root);
				this.store.setBaseParam('requestedProperties', requestedProps);
			}

			if (this.doNotTrimStrings && remote) {
				this.store.setBaseParam('doNotTrimStrings', true);
			}
			if (this.loadAll) {
				const box = this;
				this.store.on('beforeload', function() {
					box.mode = 'local';
				});
			}
		}
	},

	initEvents: function() {
		TCG.form.ComboBox.superclass.initEvents.call(this);
		delete this.keyNav.tab;
		this.keyNav.tab = function(e) {
			this.onViewClick();
			return true;
		};
	},

	afterRender: function() {
		TCG.form.ComboBox.superclass.afterRender.apply(this, arguments);

		// remove the name attribute from the form element if doNotSubmitValue is true
		if (this.doNotSubmitValue === true) {
			if (this.hiddenField) {
				this.hiddenField.doNotSubmit = true;
			}
			else if (this.el.dom && this.el.dom.name) {
				this.el.dom.doNotSubmit = true;
			}
		}
		if (this.displayText) {
			this.lastSelectionText = this.displayText;
			Ext.form.ComboBox.superclass.setRawValue.call(this, this.displayText);
		}

		if (this.requeryOnFormUpdate) {
			this.getParentForm().on('afterload', function() {
				this.store.removeAll();
				this.lastQuery = null;
				if (this.loadAll) {
					this.mode = this.originalMode;
				}
			}, this);
		}

		if (this.disableUntilTheFormIsCreated) {
			this.disable();
			this.getParentForm().on('afterload', function() {
				this.enable();
			}, this);
		}

		if (!this.doNotAddContextMenu) {
			let createDrillDownMenu = true;
			if (this.detailPageClass) {
				if (!this.drillDownMenu) {
					this.drillDownMenu = new Ext.menu.Menu({
						items: [{
							text: 'Open Detail Window', iconCls: 'info',
							handler: function() {
								this.createDetailClass(false);
							}, scope: this
						}, {
							text: 'Add New Item', iconCls: 'add',
							handler: function() {
								this.createDetailClass(true);
							}, scope: this
						}]
					});
				}
				else {
					createDrillDownMenu = false;
				}
			}
			if (this.gridEditor && this.addGridCopyContextMenu && createDrillDownMenu) {
				if (!this.drillDownMenu) {
					this.drillDownMenu = new Ext.menu.Menu({
						items: [{
							text: this.copyMenuItemName, iconCls: 'copy', handler: function() {
								this.copyHandler();
							}, scope: this
						}]
					});
				}
				else {
					this.drillDownMenu.addItem({
						text: this.copyMenuItemName, iconCls: 'copy', handler: function() {
							this.copyHandler();
						}, scope: this
					});
				}
			}
			if (this.drillDownMenu) {
				const el = this.getEl();
				el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
				el.on('contextmenu', function(e, target) {
					e.preventDefault();
					// disable detail menu if no selection was made
					const valueExists = (this.getValue() === '') ? false : true;

					for (let i = 0; i < this.drillDownMenu.items.length; i++) {
						const item = this.drillDownMenu.items.get(i);
						if (item.text === 'Open Detail Window' || item.text === this.copyMenuItemName) {
							if (valueExists === true) {
								item.enable();
							}
							else {
								item.disable();
							}
						}
						else if (item.text === 'Add New Item') {
							if (this.disableAddNewItem) {
								item.disable();
							}
						}
					}
					this.drillDownMenu.showAt(e.getXY());
				}, this);
			}
		}
	},

	copyHandler: function() {
		const editor = this.gridEditor;
		const grid = editor.containerGrid;
		const colNum = editor.col;

		const cm = grid.getColumnModel();
		const c = cm.getColumnById(colNum);

		const valueFieldName = c.idDataIndex;
		const displayFieldName = c.dataIndex;

		let i = 0;
		const store = grid.grid ? grid.grid.store : grid.store;
		const items = store.data.items;
		let row = items[i];

		while (row) {
			// Some grid combo editors for local store values and system list do not require an idDataIndex.
			// Thus, set dataIndex and idDataIndex values if both are present.
			// Otherwise, just set dataIndex value to prevent an 'undefined' value mapping on a record, which can prevent binding.
			if (valueFieldName) {
				if (row.data[valueFieldName] !== this.getValue()) {
					row.set(valueFieldName, this.getValue());
					row.set(displayFieldName, this.getRawValue());
				}
			}
			else if (row.data[displayFieldName] !== this.getValue()) {
				row.set(displayFieldName, this.getValue());
			}
			row = items[i++];
		}
		// Refresh Grid So Updates Are Visible
		const view = grid.grid ? grid.grid.getView() : grid.getView();
		view.refresh(false);
		if (grid.markModified) {
			grid.markModified();
		}
	},

	onDestroy: function() {
		Ext.destroy(this.drillDownMenu);
		TCG.form.ComboBox.superclass.onDestroy.apply(this, arguments);
	},

	getWindow: function(w) {
		let o = w ? w : this;
		o = o.ownerCt;
		if (o.getWindow) {
			return o.getWindow();
		}
		if (o.ownerCt) {
			return this.getWindow(o);
		}
		return null;
	},

	getParentForm: function() {
		const parent = TCG.getParentFormPanel(this);
		return parent || this.ownerCt;
	},

	setUrl: function(url) {
		this.store.proxy.setUrl(url, true);
	},

	setValue: function(v) {
		let text = v;
		if (TCG.isNotNull(v) && typeof v === 'object') {
			text = v.text;
			v = v.value;
		}
		else {
			if (this.valueField) {
				const r = this.findRecord(this.valueField, v);
				if (r) {
					text = r.data[this.displayField];
					// CHANGE: value passed may have been text
					v = r.data[this.valueField];
				}
				else if (Ext.isDefined(this.valueNotFoundText)) {
					text = this.valueNotFoundText;
				}
			}
			if (this.value === v && v === text && this.lastSelectionText !== undefined) {
				text = this.lastSelectionText;
			}
		}
		this.lastSelectionText = text;
		if (this.hiddenField) {
			this.hiddenField.value = Ext.value(v, '');
		}
		Ext.form.ComboBox.superclass.setValue.call(this, text);
		this.value = v;
		return this;
	},

	// returns fully populated object for current value or null
	getValueObject: function() {
		const id = this.getValue();
		if (id === '') {
			return null;
		}
		let result = this.getStore().getById(id);
		if (result) {
			return result.json;
		}
		result = {};
		result[this.valueField] = id;
		result[this.displayField] = this.lastSelectionText;
		return result;
	},

	findRecord: function(prop, value) {
		let record = TCG.form.ComboBox.superclass.findRecord.apply(this, arguments);
		// initially the mode is remote and no data is loaded but we have the data about selected row
		if (typeof record == 'undefined') {
			if (!(this.blurInProgres && value === '')) { // allow clearing values
				const fp = this.getParentForm();
				if (fp && fp.getForm) {
					let text = fp.getFormValue(this.name);
					if (TCG.isNull(text)) {
						text = this.lastSelectionText;
					}
					if (TCG.isNotNull(text)) {
						const r = {};
						r[this.valueField] = value;
						r[this.displayField] = text;
						record = new Ext.data.Record(r);
					}
				}
			}
			if ((typeof record == 'undefined') && this.gridEditor) {
				// inside of EditorGridPanel
				const editRow = this.gridEditor.record.data;
				// find field name by value
				let textFieldName = undefined;
				for (const n in editRow) {
					if (editRow[n] === value) {
						textFieldName = n;
						break;
					}
				}
				// find field definition for the name
				if (textFieldName) {
					const textField = this.gridEditor.record.fields.get(textFieldName);
					// check for matching id valueFieldName
					if (textField && textField.valueFieldName) {
						// if found, get its value and create a record
						const comboValue = editRow[textField.valueFieldName];
						if (comboValue) {
							const r = {};
							r[this.valueField] = comboValue;
							r[this.displayField] = value;
							record = new Ext.data.Record(r);
						}
					}
				}
			}
		}
		return record;
	},

	beforeBlur: function() {
		this.blurInProgres = true;
		const text = this.getRawValue();
		if (this.forceSelection && !this.findRecord(this.displayField, text)) {
			// the store is empty but the field maybe dirty
			let dirty = this.isDirty();
			if (!dirty) {
				if (text !== this.lastSelectionText) {
					this.reset();
					dirty = true;
				}
			}
			if (dirty) {
				this.clearValue();
				if (text.length > 0 && text !== this.emptyText) {
					TCG.showError('"' + (this.errorFieldLabel || this.fieldLabel) + '" field requires a valid selection from the drop down list', 'Invalid Combo Selection');
					this.focus();
				}
			}
		}
		// skip if the store hasn't been initialized
		else if (this.store.getCount() > 0) {
			TCG.form.ComboBox.superclass.beforeBlur.apply(this, arguments);
		}
		this.blurInProgres = false;
	},

	createDetailClass: function(newInstance) {
		const combo = this;
		const fp = TCG.getParentFormPanel(combo);
		const w = TCG.isNull(fp) ? {} : fp.getWindow();
		const className = this.getDetailPageClass(newInstance);
		if (className === false) {
			return;
		}
		let id = this.getValue();
		if (id === '' || newInstance) {
			id = undefined;
		}
		const params = this.getDetailPageParams(id);
		const defaultData = this.getDefaultData(TCG.isNull(fp) ? {} : fp.getForm(), newInstance);
		const cmpId = TCG.getComponentId(className, id);
		Promise.all([params, defaultData])
			.then(function(values) {
				const p = values[0],
					dd = values[1];
				// If default Data returns false, then cannot create new item
				if (dd === false) {
					return;
				}
				TCG.createComponent(className, {
					modal: TCG.isNull(id),
					id: cmpId,
					defaultData: dd,
					params: p,
					openerCt: combo, // for modal new item window will call reload() on close
					defaultIconCls: w.iconCls
				});
			});
	},

	getDetailPageClass: function(newInstance) {
		return this.detailPageClass;
	},

	getDetailPageParams: function(id) {
		return id ? {id: id} : void 0;
	},

	// returns JSON representation of defaults for detail page
	getDefaultData: function(form) {
		return undefined;
	},

	clearAndReset: function() {
		this.clearValue();
		this.resetStore();
	},

	resetStore: function() {
		this.store.removeAll();
		this.lastQuery = null;
		this.mode = this.originalMode;
	},

	// Used to dynamically change empty text and set new empty value
	setEmptyText: function(emptyTextValue) {
		this.emptyText = emptyTextValue;
		this.setRawValue('');
		this.applyEmptyText();
	},

	reload: function(win, reset) {
		if (reset) {
			this.clearAndReset();
		}
		else {
			const currentValue = this.getValue();
			// populate value/text from the window passed (not in store)
			// note: we just change the value, we don't clear and then set it, so dependencies for doNotClearIfRequiredChanges = true these aren't affected
			const v = win.getMainFormId();
			const text = win.getMainFormPanel().getFormLabel();
			this.setValue({value: v, text: text});
			// reset the store so will re-query for newly created entries via combo context menu are listed
			// Do NOT call reload on the combo as filters applied in beforequery will not get called again
			this.resetStore();

			if (currentValue !== v) {
				this.fireEvent('change', this, v, currentValue);
			}
		}
	}
});
Ext.reg('combo', TCG.form.ComboBox);

// Extension of Combo with Listeners to Reload Grid on Selections
TCG.form.ToolbarComboBox = Ext.extend(TCG.form.ComboBox, {
	linkedFilterCancelReload: true,

	reloadGridPanel: field => {
		const gridPanel = TCG.getParentByClass(field, TCG.grid.GridPanel);
		if (gridPanel && gridPanel.reload) {
			gridPanel.reload();
		}
	},

	listeners: {
		select: function(field) {
			this.reloadGridPanel(field);
		},
		specialkey: function(field, e) {
			if (e.getKey() === e.ENTER) {
				// If value is cleared, need to really clear the value before trying to reload
				if (field.triggerBlur) {
					field.triggerBlur();
				}
				this.reloadGridPanel(field);
			}
		}
	}
});
Ext.reg('toolbar-combo', TCG.form.ToolbarComboBox);

// Used for local combos where all we have is an array of Strings
TCG.form.StringComboBox = Ext.extend(TCG.form.ComboBox, {
	stringArray: undefined, // REQUIRED ARRAY OF STRINGS unless using url
	stringArrayUrl: undefined, // Unless using stringArray, can use stringArrayUrl to query for the list of Strings
	mode: 'local',
	displayField: 'name',
	valueField: 'value',
	tooltipField: 'name',

	initComponent: function() {
		if (TCG.isBlank(this.stringArrayUrl)) {
			this.store = TCG.data.StringArrayToArrayStore(this.stringArray);
		}
		else {
			this.store = TCG.data.StringArrayToArrayStore(TCG.data.getData(this.stringArrayUrl, this));
		}
		TCG.form.StringComboBox.superclass.initComponent.apply(this, arguments);
	}
});
Ext.reg('string-combo', TCG.form.StringComboBox);

// Extension of DateField with Listeners to Reload Grid on Selections
TCG.form.ToolbarDateField = Ext.extend(Ext.form.DateField, {
	width: 80,
	listeners: {
		select: function(field) {
			TCG.getParentByClass(field, TCG.grid.GridPanel).reload();
		},
		specialkey: function(field, e) {
			if (e.getKey() === e.ENTER) {
				TCG.getParentByClass(field, TCG.grid.GridPanel).reload();
			}
		}
	}
});
Ext.reg('toolbar-datefield', TCG.form.ToolbarDateField);

// Extension of Text with Listeners to Reload Grid on Enter
TCG.form.ToolbarTextField = Ext.extend(Ext.form.TextField, {
	listeners: {
		specialkey: function(field, e) {
			if (e.getKey() === e.ENTER) {
				TCG.getParentByClass(field, TCG.grid.GridPanel).reload();
			}
		}
	}
});
Ext.reg('toolbar-textfield', TCG.form.ToolbarTextField);

TCG.form.TextArea = Ext.extend(Ext.form.TextArea, {
	listeners: {
		specialkey: (field, keyEvent) => {
			// Submit on ctrl + enter when in a detail window
			if (keyEvent.ctrlKey && keyEvent.getKey() === keyEvent.ENTER) {
				const parentWindow = field.findParentBy(cmp => typeof cmp.saveWindow === 'function');
				if (parentWindow) {
					parentWindow.saveWindow(true);
				}
			}
		}
	}
});
Ext.reg('textarea', TCG.form.TextArea);

TCG.form.ScriptTextArea = Ext.extend(TCG.form.TextArea, {
	style: {'font-family': 'monospace'}
});
/**
 * A <tt>textarea</tt> field providing user-friendly formatting for writing script.
 */
Ext.reg('script-textarea', TCG.form.ScriptTextArea);

TCG.form.LinkField = Ext.extend(Ext.form.DisplayField, {
	linkName: undefined,
	submitValue: false,
	submitDetailField: true,
	detailPageWhenNoId: false, // default to: do not open detail page when id == ''
	useNameFieldValueForDetailIdFieldValue: false, //  can use this to use the name field value, usually means detailIdField is not used (or the same as the name field)
	fieldClass: 'x-form-link-field',
	/**
	 * A true value for this attribute will render a tooltip over the display field of thie field with the value.
	 * It is useful when the width of this field is defined, which allows the value to be truncated with ellipsis.
	 *
	 * The default value is undefined to allow it to be set to true when the width attribute is provided.
	 * To override this behavior, set the value to anything else (e.g. false).
	 */
	renderDataTip: undefined,

	initComponent: function() {
		if (this.type === 'currency' && !this.style) {
			this.style = 'text-align: right; padding-right: 5px';
		}
		if (this.width || this.flex) {
			// If the width is limitted, allow the field to be truncated with ellipsis.
			// If flex is used, width and height must be defined since the initial value of the field is ''.
			this.style = (this.style || '') + 'white-space: nowrap; overflow: hidden; text-overflow: ellipsis; width: '
				+ (this.flex ? '100%; minHeight: 80%; maxHeight: 90%' : this.width + 'px');
			if (TCG.isNull(this.renderDataTip)) {
				this.renderDataTip = true;
			}
		}
		TCG.form.LinkField.superclass.initComponent.apply(this, arguments);

		//this will rename the original linkfield to use the specified linkName, and will create a new hidden field with the original
		//name, this allows both the original field and the detail field to be submit with the form
		const p = this.getParentForm();
		let fieldAdded = false;
		//table layout breaks the intended layout and functionality. See Portfolio Run window.
		const ownerCt = this.findParentBy(o => (o instanceof Ext.Container) && o.layout !== 'table');
		if (this.submitValue) {
			if (TCG.isNull(p.getForm().findField(this.name, true))) {
				ownerCt.add({name: this.name, xtype: 'hidden', value: p.getFormValue(this.name), originalValue: p.getFormValue(this.name), submitValue: this.submitValue});
				fieldAdded = true;
				if (this.linkName) {
					this.name = this.linkName;
				}
			}
		}

		// add hidden detail id if it doesn't exist
		if (this.detailIdField && !(this.detailIdField === this.name && fieldAdded)) {
			if (TCG.isNull(p.getForm().findField(this.detailIdField, true))) {
				ownerCt.add({name: this.detailIdField, xtype: 'hidden', value: p.getFormValue(this.detailIdField), originalValue: p.getFormValue(this.detailIdField), submitValue: this.submitDetailField});
			}
		}

		this.on('afterrender', function() {
			this.mon(this.el, 'click', this.createDetailClass, this, {preventDefault: true});
		}, this);
	},

	setValue: function(v) {
		let args = arguments;
		if (this.type === 'date') {
			args = [TCG.renderDate(v)];
		}
		else if (this.type === 'currency') {
			args = [Ext.util.Format.number(v, '0,000.00')];
		}
		if (this.valuePrefix && args[0]) {
			args[0] = this.valuePrefix + args[0];
		}
		if (TCG.isTrue(this.renderDataTip) && this.rendered) {
			this.el.set({'ext:qtip': v});
		}
		TCG.form.LinkField.superclass.setValue.apply(this, args);
	},

	getNumericValue: function() {
		// remove formatting commas
		const value = parseFloat(String(this.getValue()).replace(this.decimalSeparator, '.').replace(/,/g, ''));
		return parseFloat(isNaN(value) ? '' : value);
	},

	createDetailClass: function() {
		const p = this.getParentForm();
		const w = p.getWindow();
		if (this.baseURL) {
			const url = this.getURL();
			const win = new TCG.app.URLWindow({
				title: this.title,
				id: this.windowId,
				defaultIconCls: w.iconCls,
				url: url,
				openerCt: this
			});
			win.setURL(url);
		}
		else {
			const className = this.getDetailPageClass(p);
			if (className === false) {
				// cancelled
			}
			else if (className) {
				const id = this.getDetailIdFieldValue(p);
				if (id === false || (this.detailPageWhenNoId === false && TCG.isBlank(id))) {
					// cancelled
				}
				else if (TCG.isNull(id)) {
					TCG.showError('Cannot find field "' + this.detailIdField + '"', 'Client Side Error');
				}
				else {
					const cmpId = TCG.getComponentId(className, id);
					const dd = this.getDefaultData(p);
					const args = {
						id: cmpId,
						params: {id: id},
						defaultData: dd,
						defaultIconCls: w.iconCls,
						openerCt: p // for modal new item window will call reload() on close
					};
					if (dd) {
						if (dd.defaultActiveTabName) {
							args.defaultActiveTabName = dd.defaultActiveTabName;
						}
						if (dd.additionalComponentConfig) {
							args.additionalComponentConfig = {...dd.additionalComponentConfig};
						}

					}

					TCG.createComponent(className, args);
				}
			}
			else {
				TCG.showError('Missing required property: detailPageClass');
			}
		}
	},

	isDirty: function() {
		const v = this.getValue();
		let o = this.originalValue;
		if (v !== o) {
			o = Ext.util.Format.htmlEncode(o);
			if (v === o) {
				this.originalValue = o;
			}
		}
		return TCG.form.LinkField.superclass.isDirty.apply(this, arguments);
	},

	getParentForm: function() {
		const parent = TCG.getParentFormPanel(this);
		return parent || this.ownerCt;
	},

	// returns JSON representation of defaults for NEW detail pages; return false to cancel detail page open
	getDefaultData: function(formPanel) {
		return undefined;
	},

	getDetailIdFieldValue: function(formPanel) {
		const f = (this.useNameFieldValueForDetailIdFieldValue) ? formPanel.getForm().findField(this.nameField, true) : formPanel.getForm().findField(this.detailIdField, true);
		return f ? f.getValue() : null;
	},

	getDetailPageClass: function(fp) {
		return this.detailPageClass;
	},

	getURL: function() {
		return this.baseURL + this.getValue();
	}
});
Ext.reg('linkfield', TCG.form.LinkField);

TCG.form.SecretField = Ext.extend(Ext.form.CompositeField, {
	fieldLabel: 'Secret',
	fieldPath: 'secretString',
	fieldPathHiddenName: 'id',
	tableName: 'OVERRIDE-ME', //The table name that the secret is an FK in
	valuePath: undefined, //The path to the field the SecretID is stored in, if not specified will use the fieldPathHiddenName
	secretId: undefined,
	listeners: {
		afterrender: function(panel) {
			panel.setupSecretField();
		}
	},
	initComponent: function() {
		Ext.form.CompositeField.prototype.initComponent.call(this, arguments);

		this.secretField = this.items.items[2];
		if (this.height) {
			this.secretField.height = this.height;
		}

		if (this.name) {
			this.fieldPath = this.name + '.' + this.fieldPath;
			this.fieldPathHiddenName = this.name + '.' + this.fieldPathHiddenName;
		}

		this.secretField.name = this.fieldPath;
		this.secretField.hiddenName = this.fieldPathHiddenName;

		this.secretField.submitValue = this.submitValue;
	},
	getValuePath: function() {
		const fp = this.getParentForm();
		let vp = this.valuePath;

		if (fp.getDynamicPropertyFromField && fp.getDynamicPropertyFromField(this) && fp.getDynamicPropertyFromField(this).parameter) {
			const systemPropertyName = fp.getDynamicPropertyFromField(this).parameter.systemPropertyName;
			if (TCG.isNotNull(vp)) {
				vp = vp + '.' + systemPropertyName;
			}
			else {
				vp = systemPropertyName;
			}
		}
		else if (TCG.isNull(vp)) {
			vp = this.name;
		}
		return vp;
	},
	setupSecretField: function() {
		const parent = this;
		const cs = this.innerCt.items.items;
		let i = 0;
		const len = cs.length;
		for (; i < len; i++) {
			const fld = cs[i];
			fld.parentField = parent;
			if (fld.menu) {
				const csMenu = fld.menu.items.items;
				for (let j = 0; j < csMenu.length; j++) {
					csMenu[j].parentField = parent;
				}
			}
		}
	},
	decryptField: function() {
		const fp = this.getParentForm();
		if (this.getValue && TCG.isNotBlank(this.getValue())) {
			if ((TCG.isNull(this.tableName) || this.tableName === 'OVERRIDE-ME') && fp.childTables) {
				this.tableName = fp.childTables;
			}
			this.openDecryptionPage();
		}
		else {
			TCG.showError('There is no value to decrypt.', 'Decryption Error');
		}
	},
	openDecryptionPage: function() {
		const fp = this.getParentForm();
		const className = 'Clifton.security.system.SecretWindow';
		const params = {};
		params.secretId = this.getValue();
		params.entityId = fp.form.idFieldValue;
		params.fieldPath = this.getValuePath();
		params.tableName = this.tableName;
		const cmpId = TCG.getComponentId(className, this.secretId);
		TCG.createComponent(className, {
			id: cmpId,
			params: params,
			openerCt: this
		});
	},
	getValue: function() {
		const fp = this.getParentForm();
		let val = this.secretId ? this.secretId : TCG.getValue(this.fieldPathHiddenName, fp.form.formValues);
		if (TCG.isNull(val) && fp.getDynamicPropertyFromField && fp.getDynamicPropertyFromField(this)) {
			val = fp.getDynamicPropertyFromField(this).value;
		}
		return val;
	},
	getText: function() {
		return this.secretField.getValue();
	},
	setValue: function(v) {
		if (TCG.isNotBlank(v)) {
			this.secretId = v;
			this.getParentForm().setFormValue(this.fieldPath, '[ENCRYPTED]', true);
		}
	},
	getParentForm: function() {
		const parent = TCG.getParentFormPanel(this);
		return parent || this.ownerCt;
	},
	items: [
		{
			//Field that chrome will attempt to autofill
			fieldLabel: 'Username Trap', xtype: 'hidden', submitValue: false, submitDetailField: false,
			getValue: function() {
				return null;
			}
		},
		{
			//Field that chrome will attempt to autofill
			fieldLabel: 'Password Trap', xtype: 'hidden', inputType: 'password', submitValue: false, submitDetailField: false,
			getValue: function() {
				return null;
			}
		},
		{xtype: 'textfield', flex: 1, height: 22, inputType: 'password'},
		{
			xtype: 'button', iconCls: 'lock', width: 40, tooltip: 'Click to decrypt this secret.',
			handler: function(btn) {
				return btn.parentField.decryptField();
			}
		}
	]
});
Ext.reg('secretfield', TCG.form.SecretField);

TCG.form.SecretTextArea = Ext.extend(TCG.form.SecretField, {
	items: [
		{
			//Field that chrome will attempt to autofill
			fieldLabel: 'Username Trap', xtype: 'hidden', submitValue: false, submitDetailField: false,
			getValue: function() {
				return null;
			}
		},
		{
			//Field that chrome will attempt to autofill
			fieldLabel: 'Password Trap', xtype: 'hidden', inputType: 'password', submitValue: false, submitDetailField: false,
			getValue: function() {
				return null;
			}
		},
		{xtype: 'textarea', flex: 1},
		{
			xtype: 'button', iconCls: 'lock', width: 40, tooltip: 'Click to decrypt this secret.',
			handler: function(btn) {
				return btn.parentField.decryptField();
			}
		}
	]
});
Ext.reg('secrettextarea', TCG.form.SecretTextArea);

TCG.form.FormPanel = Ext.extend(Ext.form.FormPanel, {
	controlWindowModified: true, // call win.setModified when the form is modified
	readOnly: false,
	loadValidation: true,
	loadDefaultDataAfterRender: false,
	//if set to true, formValues will be reset with defaultData (meaning the form will be 'clean' and apply button will not enable)
	updateFormWithDefaultData: false,
	autoScroll: true,
	border: false,
	frame: true,
	bodyStyle: 'padding: 5px 5px 0',
	labelWidth: 110,
	defaults: {
		anchor: '-35' // leave room for error icon
	},
	defaultType: 'textfield',
	attachedEvents: {},
	submitEmptyText: false,
	applicationName: '',
	rowVersionFieldName: 'rv', // if specified, this parameter with the corresponding value will be submitted to the server and added to SQL UPDATE statements

	initComponent: function() {
		if (TCG.isNull(this.tbar) && this.addToolbarButtons) {
			Ext.apply(this, {tbar: new Ext.Toolbar()});
		}
		TCG.form.FormPanel.superclass.initComponent.call(this);

		// Apply field overrides prior to filling metadata
		const fieldOverrides = this.getFieldOverrides();
		if (fieldOverrides) {
			for (let i = 0; i < fieldOverrides.length; i++) {
				const field = fieldOverrides[i];
				this.overrideField(field.name, field);
			}
		}
	},

	getFieldOverrides: function() {
		const overrides = [];

		if (this.fieldOverrides) {
			if (Array.isArray(this.fieldOverrides)) {
				overrides.push(...this.fieldOverrides);
			}
			else {
				overrides.push(this.fieldOverrides);
			}
		}

		// Allow fieldOverrides to be injected from window launching
		if (this.getWindow() && this.getWindow().fieldOverrides) {
			if (Array.isArray(this.getWindow().fieldOverrides)) {
				overrides.push(...this.getWindow().fieldOverrides);
			}
			else {
				overrides.push(this.getWindow().fieldOverrides);
			}
		}

		return overrides;
	},

	onRender: function() {
		this.on('afterrender', function() {
			// even though this is called 'afterrender' for the formpanel, form elements are still not rendered and can't be highlighted => wait 100 ms
			if (this.readOnly === false) {
				this.loadValidationMetaData.defer(100, this);
			}
		}, this);

		TCG.form.FormPanel.superclass.onRender.apply(this, arguments);
		this.addEvents({
			'afterload': true, // fired after the data was loaded into this form
			'aftercreate': true // fired after a new entity was created
		});

		const url = this.getLoadURL();
		const panel = this;
		const f = this.getForm();
		const win = this.getWindow();
		if (this.readOnly) {
			this.setReadOnly(true);
		}
		if (win.defaultDataIsReal) {
			// the default data specified is the real data: no need to do a server request
			const result = {};
			result.data = this.prepareDefaultData(this.getDefaultData(win));
			f.trackResetOnLoad = true;
			if (this.loadDefaultDataAfterRender) {
				this.on('afterrender', function() { // so that it's not re-initialized
					panel.loadJsonResult(result);
					panel.fireEvent.defer(10, panel, ['afterload', panel]); // wait until panel elements are really loaded
				}, this, {delay: 100});
			}
			else {
				panel.loadJsonResult(result);
				panel.fireEvent.defer(10, panel, ['afterload', panel]); // wait until panel elements are really loaded
			}
			// sync field submit values
			panel.afterRenderFieldSubmitValueSyncWithElement();
		}
		else if (url) {
			f.trackResetOnLoad = true;
			f.waitMsgTarget = this.getEl();
			f.idFieldValue = win.params ? win.params.id : undefined; // set id before loading the data so that dependent components can start using it
			this.doLoad(url);
		}
		else {
			const applyDefaultDataFunction = () => {
				Promise.resolve(panel.getDefaultData(win))
					.then(dd => dd ? panel.prepareDefaultData(dd) : dd)
					.then(function(dd) {
						if (dd) {
							//Set defaultData and resetValues so originalData reflects the default
							f.setValues(dd, panel.updateFormWithDefaultData);
						}
						f.loaded = true;

						//used when the afterrender event is dependent on the formValues data being loaded first.
						//by removing the listener and defining instead as 'afterRenderPromise' it will ensure the data is ready.
						//takes the formPanel returned from the original promise as the parameter
						if (TCG.isNotNull(panel.afterRenderPromise)) {
							panel.afterRenderPromise(panel); // see long comment below
							// Fire a special event signalling afterRenderPromise has completed. Data is now loaded and dynamic fields are processed.
							panel.fireEvent('afterRenderPromise', panel);
						}
						// sync field submit values
						panel.afterRenderFieldSubmitValueSyncWithElement();
					});
			};

			if (panel.loadDefaultDataAfterRender) {
				panel.on('afterrender', applyDefaultDataFunction, panel, {delay: 100});
			}
			else {
				applyDefaultDataFunction();
			}
		}

		if (this.controlWindowModified && win.isModified) {
			this.on({
				'change': function() {
					if (f.loaded) {
						win.setModified(f.isValid() && win.isModified());
					}
				},
				'invalid': function() {
					if (f.loaded) {
						win.setModified(false);
					}
				},
				'valid': function() {
					if (f.loaded) {
						win.setModified(win.isModified());
					}
				}
			});
		}

		// generate toolbar
		const t = this.getTopToolbar();
		if (TCG.isNotNull(t) && this.addToolbarButtons) {
			this.addToolbarButtons(t, this);
		}

		this.focusOnFirstField();
	},

	/**
	 * The submitValue of the field is read during render of the form. If we update this value of a field on the form
	 * in prepareDefaultData, onRender, or in an afterRenderPromise the submitValue value must be applied to the field's
	 * already created element. The element's doNotSubmit value is used when sending data to the server.
	 *
	 * @see Ext.lib.Ajax.serializeForm
	 */
	afterRenderFieldSubmitValueSyncWithElement: function() {
		this.getForm().items.each(function(field) {
			if (TCG.isFalse(field.submitValue) && TCG.isNotNull(field.el) && TCG.isNotNull(field.el.dom)) {
				field.el.dom.doNotSubmit = TCG.isFalse(field.submitValue);
				if (field.isXType('radiogroup')) {
					field.items.each(radio => radio.el.dom.doNotSubmit = !field.submitValue);
				}
			}
		});
	},

	doLoad: function(url, params) {
		const panel = this;
		this.load({
			url: encodeURI(url || this.getLoadURL()),
			method: 'POST',
			params: params || this.getLoadParams(this.getWindow()),
			waitMsg: 'Loading...',
			success: function(form, action) {
				panel.loadJsonResult(action.result, true);
				panel.fireEvent('afterload', panel);
			},
			failure: function(form, action) {
				let msg = 'Error loading form data from: <b>' + action.getUrl() + '</b><br /><br />';
				msg += (action.result && action.result.message) ? action.result.message : 'Failure Type: ' + action.failureType;
				console.warn(`An error occurred during form load: [${msg}]. Form:`, form, 'Action:', action);
				TCG.data.ErrorHandler.handleFailure(panel, action.result ? action.result : action, msg);
				form.loaded = true;
			}
		});
	},

	getDefaultData: function(win) {
		return win.defaultData;
	},

	getLoadParams: function(win) {
		return win.params;
	},

	// can be overridden to optionally change default data before it's set
	prepareDefaultData: function(defaultData) {
		return defaultData;
	},

	focusOnFirstField: function() {
		// set focus to first non read-only field
		const f = this.getForm();
		for (let i = 0; i < f.items.length; i++) {
			const o = f.items.get(i);
			if (!o.disabled && !o.readOnly && o instanceof Ext.form.TextField) {
				o.focus(false, 500);
				break;
			}
		}
	},

	loadJsonResult: function(result, doNotUpdate) {
		const form = this.getForm();
		if (!doNotUpdate) {
			form.clearInvalid();
			form.setValues(result.data);
		}
		form.idFieldValue = result.data.id;
		form.loaded = true;
		this.updateTitle();
		this.fireEvent('change', this);
		this.focusOnFirstField();
	},

	getFormValuesFormatted: function(dirty) {
		const result = this.getForm().getFieldValues(dirty);
		if (result) {
			for (const k in result) {
				if (result[k] instanceof Date) {
					result[k] = result[k].format('m/d/Y');
				}
				// Radio Groups getValue returns the Radio object that is selected so use that ones input value
				if (result[k] instanceof Object && result[k].inputValue) {
					result[k] = result[k].inputValue;
				}
			}
		}
		return result;
	},

	setFormValue: function(fieldName, v, original) {
		const o = this.getForm().findField(fieldName);
		if (o) {
			o.setValue(v);
			if (original) {
				if (o.xtype === 'radiogroup') {
					o.eachItem(function(item) {
						item.originalValue = item.getValue();
					});
				}
				else {
					o.originalValue = o.getValue();
				}
			}
		}
	},

	setFieldLabel: function(fieldName, v) {
		let o = this.getForm().findField(fieldName);
		if (!o) {
			o = TCG.getChildByName(this, fieldName);
		}
		if (o) {
			if (o.setFieldLabel) {
				o.setFieldLabel(v);
			}
			else if (o.setText) {
				o.setText(v);
			}
		}
	},
	setFieldSubmitValue: function(fieldName, v) {
		const o = this.getForm().findField(fieldName);
		if (o) {
			if (v) {
				o.getEl().dom.setAttribute('name', fieldName);
			}
			else {
				o.getEl().dom.removeAttribute('name');
			}
		}
	},
	setFieldQtip: function(fieldName, qtip) {
		const o = this.getForm().findField(fieldName);
		if (o && o.getTipTarget) {
			if (o.rendered) {
				const target = o.getTipTarget();
				target.dom.qtip = qtip;
			}
			else {
				o.qtip = qtip;
			}
		}
	},
	setReadOnly: function(ro, excludeFields) {
		if (excludeFields == null) {
			excludeFields = [];
		}
		this.getForm().items.each(function(o) {
			const xtype = o.getXType();
			if (!(o instanceof TCG.form.LinkField) && xtype !== 'displayfield') {
				if (o instanceof Ext.form.TextField || xtype === 'datefield' || xtype === 'textarea' || xtype === 'compositefield' || xtype === 'htmleditor') {
					if (!excludeFields.includes(o.getName())) {
						o.setReadOnly(ro);
					}
				}
				else if (o.setDisabled) {
					if (!excludeFields.includes(o.getName())) {
						o.setDisabled(ro);
					}
				} // setDisabled as opposed to setReadOnly also works for checkboxes and changes visual appearance
			}
		});
	},

	getWindow: function() {
		let result = this.findParentByType(Ext.Window);
		if (TCG.isNull(result)) {
			result = this.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			});
		}
		return result;
	},

	getLoadURL: function() {
		const w = this.getWindow();
		if (this.url && w && w.params) {
			return this.url;
		}
		return false;
	},

	getSaveURL: function() {
		let url = this.url;
		if (url) {
			const i = url.indexOf('.');
			url = url.substring(0, i) + 'Save' + url.substring(i);
		}
		return url;
	},
	// uses this data to reload the form after save with new data; return false to cancel
	getDataAfterSave: function(data) {
		return data;
	},

	updateTitle: function() {
		let l = this.getFormLabel();
		if (TCG.isNotNull(l)) {
			const w = this.getWindow();
			if (w && w.setTitle) {
				if (TCG.isNumber(this.maxWindowTitleLength)) {
					if (l.length > this.maxWindowTitleLength) {
						l = l.substring(0, this.maxWindowTitleLength) + '...';
					}
				}
				w.setTitle(l);
			}
		}
	},

	getFormLabel: function() {
		if (this.labelFieldName) {
			return this.getFormValue(this.labelFieldName);
		}
		let l = this.getFormValue('label');
		if (TCG.isNull(l)) {
			l = this.getFormValue('name');
		}
		return l;
	},

	clearFields: function(items) {
		for (let i = 0; i < items.length; i++) {
			const o = items.get(i);
			if (o.items) {
				this.clearFields(o.items);
			}
			else {
				o.setValue('');
			}
		}
	},

	removeField: function(field) {
		if (field.isFormField) {
			const el = field.el.up('.x-form-item');
			this.items.remove(field);
			this.getForm().remove(field);
			if (el) {
				el.remove();
			}
			this.doLayout();
		}
	},

	replaceField: function(fieldName, newField) {
		const field = this.getForm().findField(fieldName);
		let insertedNewField = this.replaceComponentField(this, field, newField);
		if (TCG.isNull(insertedNewField)) {
			// field was not found for replacement on the form panel, add it
			insertedNewField = this.insert(-1, newField);
		}
		this.doLayout();
		return insertedNewField;
	},

	replaceComponentField: function(component, field, newField) {
		let insertedNewField = void 0;
		const formPanel = this;
		if (component.items) {
			const fieldIndex = component.items.indexOf(field);
			if (fieldIndex > -1) {
				// field was found in the component items, replace it
				component.remove(field, true);
				if (formPanel.getForm().items.indexOf(field) > -1) {
					// if field was removed from a child/nexted container, remove the item from the from if it still exists to prevent duplicate
					formPanel.getForm().items.remove(field);
				}
				insertedNewField = component.insert(fieldIndex, newField);
			}
			else {
				// Field was not found in the direct items of the component. Look at nested items of a child (e.g. form fragments, composite fields, etc.)
				// to replace in order to prevent a duplicate form field from being added.
				if (component.items.each) {
					component.items.each(item => {
						if (item.items) {
							const childNewField = formPanel.replaceComponentField(item, field, newField);
							if (childNewField) {
								// child item contained the field set the field replaced
								insertedNewField = childNewField;
							}
						}
					});
				}
			}
		}
		return insertedNewField;
	},

	// overrides existing field with the given fieldName
	// inserts new field with given overrideProperties if there is no existing field with fieldName
	overrideField: function(fieldName, overrideProperties) {
		const field = this.getForm().findField(fieldName);
		const newFieldName = overrideProperties.name || fieldName;
		const formValuesAndOverrideProps = {
			value: TCG.getValue(newFieldName, this.getForm().formValues),
			...overrideProperties
		};
		if (TCG.isBlank(field)) {
			return this.replaceField(fieldName, formValuesAndOverrideProps);
		}
		return this.replaceField(fieldName, {
			...field.initialConfig,
			...formValuesAndOverrideProps
		});
	},

	convertComboToLinkField: function(fieldName, additionalProperties) {
		const field = this.getForm().findField(fieldName);
		return this.overrideField(fieldName, {
			xtype: 'linkfield',
			hiddenName: null,
			detailIdField: field.initialConfig.hiddenName,
			...additionalProperties
		});
	},

	setHidden: function(field, hide) {
		if (TCG.isTrue(hide)) {
			field.hide();
		}
		else {
			field.show();
		}
	},

	hideField: function(fieldName) {
		const o = this.getForm().findField(fieldName);
		if (TCG.isNotNull(o)) {
			o.hide();
		}
	},

	hideFieldIfBlank: function(fieldName) {
		if (TCG.isBlank(this.getFormValue(fieldName))) {
			this.hideField(fieldName);
		}
	},

	showField: function(fieldName) {
		const o = this.getForm().findField(fieldName);
		if (TCG.isNotNull(o)) {
			o.show();
		}
	},

	enableField: function(fieldName, show) {
		const o = this.getForm().findField(fieldName);
		if (o) {
			o.enable();
			if (show) {
				o.show();
			}
		}
	},

	disableField: function(fieldName, hide) {
		const o = this.getForm().findField(fieldName);
		if (o) {
			o.disable();
			if (hide) {
				o.hide();
			}
		}
	},

	setReadOnlyField: function(fieldName, readOnly) {
		const o = this.getForm().findField(fieldName);
		if (o) {
			const xtype = o.getXType();
			if (!(o instanceof TCG.form.LinkField) && xtype !== 'displayfield') {
				if (o instanceof Ext.form.TextField || xtype === 'datefield' || xtype === 'textarea' || xtype === 'compositefield' || xtype === 'htmleditor') {
					o.setReadOnly(readOnly);
				}
				else if (o.setDisabled) {
					o.setDisabled(readOnly);
				} // setDisabled as opposed to setReadOnly also works for checkboxes and changes visual appearance
			}
		}
	},

	getFormValue: function(fieldName, lookupFormFieldFirst, lookupFromFieldOnly) {
		if (lookupFormFieldFirst) {
			const o = this.getForm().findField(fieldName);
			if (o) {
				const v = o.getValue();
				if (v !== undefined) {
					return v;
				}
			}
		}
		if (lookupFromFieldOnly) {
			return null;
		}
		let v = TCG.getValue(fieldName, this.getForm().formValues);
		if (TCG.isNull(v) && !lookupFormFieldFirst) {
			const o = this.getForm().findField(fieldName);
			if (o) {
				v = o.getValue();
				if (v === '') {
					v = null;
				}
			}
		}
		return v;
	},

	getSubmitParams: function() {
		return {};
	},

	loadValidationMetaData: function(attachEvents) {
		const panel = this;
		if (this.loadValidation && panel.url) {
			const beanName = panel.getValidationBeanName(panel.url);
			TCG.getValidationMetaData(beanName, panel.setValidationMetaData, panel, panel);
		}
		if (attachEvents) {
			this.validationEventsAttached = false; // force the events to be attached
		}
		panel.attachValidationEvents();
		if (!this.getLoadURL() && !this.validatedOnLoad) {
			this.validatedOnLoad = true;
			this.getForm().isValid();
		}
	},

	getValidationBeanName: function(url) {
		return url.substring(0, url.indexOf('.'));
	},

	getFirstInValidFieldOverride: function() {
		return null;
	},
	getFirstInValidField: function(validateAll) {
		let result = this.getFirstInValidFieldOverride();
		if (result === false) {
			return result;
		}
		const f = this.getForm();
		// use the base form for the list of fields
		f.items.each(function(o) {
			if (o.isValid && !o.isValid(false)) {
				if (TCG.isNull(result)) {
					result = o;
				}
				if (!validateAll) {
					return false; // return false to break loop
				}
			}
			let el = o.getEl();
			if (!el) {
				el = o.el;
			}
			if (el && el.hasClass(o.invalidClass)) {
				if (TCG.isNull(result)) {
					result = o;
				}
				if (!validateAll) {
					return false;
				}
			}
		});
		return result;
	},

	setValidationMetaData: function(data) {
		const f = this.getForm();
		for (let i = 0; i < data.length; i++) {
			const d = data[i];
			let field = f.findField(d.name);
			if (!field) {
				field = f.findField(d.name + '.id');
			}
			if (field && field.skipValidation !== true) {
				if (d.required === true) {
					field.allowBlank = false;
				}
				else if (d.required === false) {
					field.allowBlank = true;
				}
				// Allow explicitly setting field maxLength differently than loaded from the server
				// See Company Abbreviation - Using 4 character limit, but db supports up to 10
				if (d.maxLength && field.maxLength === Number.MAX_VALUE && field.xtype !== 'combo') {
					field.maxLength = d.maxLength;
				}
			}
		}
		if (!f.trackResetOnLoad) {
			f.isValid();
		}
	},

	attachValidationEvents: function() {
		// if items is empty the call is from local form and not recursive call below
		if (this.validationEventsAttached) {
			return;
		}
		this.validationEventsAttached = true;
		this.addEvents({
			'change': true,
			'fieldchange': true, // event on the form for field and value changes
			'valid': true,
			'invalid': true
		});
		const fp = this;
		const f = this.getForm();
		// use the base form for the list of fields
		f.items.each(function(o) {
			fp.attachValidationEventsToField(fp, o);
		});
	},

	attachValidationEventsToField: function(fp, o) {
		if (!fp.validationDelay) {
			fp.validationDelay = new Ext.util.DelayedTask(fp.validateFields, fp);
		}

		// prevent duplicate events by registering the controls that have been attached
		if (fp.attachedEvents[o.id]) {
			return;
		}
		fp.attachedEvents[o.id] = true;

		const xtype = o.getXType ? o.getXType() : undefined;
		if (xtype === 'checkbox') {
			o.addListener('check', function(field, checked) {
				fp.fireEvent('change', fp, fp);
				if (field.isDirty()) {
					fp.fireEvent('fieldchange', field, checked);
				}
			});
		}
		else if (xtype === 'checkboxgroup' || xtype === 'radiogroup') {
			o.addListener('change', function(field, checked) {
				fp.fireEvent('change', fp, fp);
				if (field.isDirty()) {
					fp.fireEvent('fieldchange', field, checked);
				}
			});
			// other form elements maybe present too
			TCG.each(o.items, function(o1) {
				fp.attachValidationEventsToField(fp, o1);
			});
		}
		else if (xtype === 'compositefield') {
			TCG.each(o.items, function(o1) {
				fp.attachValidationEventsToField(fp, o1);
			});
		}
		else if (xtype) {
			o.on({
				'change': function(field, newValue, oldValue) {
					fp.fireEvent('change', fp, fp);
					if (field.isDirty()) {
						fp.fireEvent('fieldchange', field, newValue);
					}
				},
				'valid': function(field) {
					if (fp.validationInProgress || !fp.getForm().loaded) {
						return;
					}
					fp.validationDelay.delay(200, this.validateFields, fp, [field]);
				},
				'invalid': function(field) {
					fp.fireEvent('invalid', fp, fp);
				}
			});
		}
	},

	validateFields: function(field) {
		this.validationInProgress = true;
		let valid = true;
		// use the base form for the list of fields
		this.getForm().items.each(function(f) {
			if (f !== field && f.isValid && !f.isValid(true)) {
				valid = false;
				return false; // return false to break loop
			}
		});
		this.validationInProgress = false;
		if (valid) {
			this.fireEvent('valid', this);
		}
	},

	getIdFieldValue: function() {
		return this.getForm().idFieldValue;
	}
});
Ext.reg('formpanel', TCG.form.FormPanel);

TCG.form.FormPanelWithTabs = Ext.extend(TCG.form.FormPanel, {
	layout: 'fit',
	frame: false,
	bodyStyle: 'padding: 0px 0px 0'
});
Ext.reg('formwithtabs', TCG.form.FormPanelWithTabs);

TCG.form.FormPanelFragment = Ext.extend(Ext.Panel, {
	layout: 'form',
	bodyStyle: 'padding: 5px 5px 0',
	labelWidth: 110,
	defaults: {
		anchor: '-20' // leave room for error icon
	},
	defaultType: 'textfield',
	autoScroll: true,
	frame: true
});
Ext.reg('formfragment', TCG.form.FormPanelFragment);

TCG.form.FormPanelColumn = Ext.extend(Ext.Panel, {
	toolbars: [],
	layout: 'column',
	initComponent: function() {
		const formPanelColumn = this;
		this.anchor = '0'; // take full width and leave room for errors inside of inner column panels
		TCG.form.FormPanelColumn.superclass.initComponent.call(this);
		Ext.each(this.columns, function(item, i) {
			this.add(Ext.apply(new Ext.Panel({
				layout: 'form',
				columnWidth: '.5',
				items: item.rows,
				ownerCt: formPanelColumn,
				defaults: Ext.apply({anchor: '-35'}, this.defaults || {})
			}), item.config));
		}, this);
	}
});
Ext.reg('columnpanel', TCG.form.FormPanelColumn);

TCG.form.FormPanelTable = Ext.extend(Ext.Panel, {
	columns: 3, // required number of columns in this table
	layout: 'table',
	layoutConfig: {
		extraCls: 'x-form-item x-form-item-label',
		tableAttrs: {style: {width: '100%'}}
	},
	defaults: {
		xtype: 'label',
		width: '95%'
	},
	initComponent: function() {
		this.layoutConfig = TCG.clone(this.layoutConfig);
		this.layoutConfig.columns = this.columns;
		TCG.form.FormPanelTable.superclass.initComponent.call(this);
	}
});
Ext.reg('formtable', TCG.form.FormPanelTable);

TCG.form.FormPanelWithDynamicFields = Ext.extend(TCG.form.FormPanel, {
	// The name of the field on which dynamic fields depend
	dynamicTriggerFieldName: 'FORM-FIELD-NAME-THAN-DEFINES-DYNAMIC-FIELDS',
	// The value of the dynamicTriggerFieldName field which will be used to retrieve dynamic field types
	dynamicTriggerValueFieldName: 'FORM-FIELD-NAME-THAT-DEFINES-VALUE', // different than previous for linkfield
	// The field on the property value which defines the property type
	dynamicFieldTypePropertyName: 'VALUE-PROPERTY-NAME-THAT-DEFINES-TYPE',
	// The entity field which provides the list of property objects
	dynamicFieldListPropertyName: 'FIELD-VALUE-LIST-PROPERTY-NAME',
	// The URL which shall be queried to retrieve the list of dynamic field property types
	dynamicFieldsUrl: 'URL-TO-GET-DYNAMIC-FIELDS.json',
	// The parameter which shall be populated with the dynamicTriggerValueFieldName value when querying the dynamicFieldsUrl endpoint
	dynamicFieldsUrlParameterName: 'id',
	// The field on the property which contains the actual property value
	dynamicFieldValuePropertyName: 'value',
	// The field on the property which contains the textual representation of the property value
	dynamicFieldTextPropertyName: 'text',
	dynamicFieldClass: undefined,
	dynamicFieldFormFragment: undefined,

	// sub property trigger fields
	dynamicFieldsUrlListParameterName: undefined, // defines the property name when submitting parameters to lookup field types
	dynamicFieldsUrlIdFieldName: 'FORM-FIELD-NAME-THAT-DEFINES-ID-OF-THE-DYNAMIC-OBJECT',

	// when true the dynamic field will not submit it's type, this is used for FixTagModifiers where the type is not a DTO
	doNotSubmitDynamicFieldTypeId: false,

	onRender: function() {
		this.on('afterload', function(fp, isClosing) {
			// on first bean load we get bean type and need to reset corresponding bean properties
			if (!isClosing) {
				const triggerField = this.getForm().findField(this.dynamicTriggerFieldName);
				this.resetDynamicFields(triggerField, true);
			}
		}, this);

		TCG.form.FormPanelWithDynamicFields.superclass.onRender.apply(this, arguments);
	},

	afterRenderPromise: function(fp) {
		// new entity with defaulted type (triggerField): reset corresponding properties
		const triggerField = fp.getForm().findField(fp.dynamicTriggerFieldName);
		if (triggerField && triggerField.getValue() !== '') {
			fp.resetDynamicFields(triggerField, true);
		}
	},

	// add dynamic field values separately
	getSubmitParams: function() {
		let result = {};
		if (this.dynamicFields) {
			let submitCount = 0;
			for (let i = 0; i < this.dynamicFields.length; i++) {
				const field = this.dynamicFields[i];
				// do not submit fields with no values
				if (TCG.isNotNull(field) && field.isFormField && field.getValue() !== '' && field.submitDetailField !== false) {
					// handle update existing value vs create new one
					const fieldValue = this.getDynamicPropertyFromField(field);
					if (TCG.isNotNull(fieldValue) && TCG.isNotNull(fieldValue.id)) {
						result[this.dynamicFieldListPropertyName + '[' + submitCount + '].id'] = fieldValue.id;
					}
					else if (TCG.isNull(field.targetField) && !this.doNotSubmitDynamicFieldTypeId) {
						result[this.dynamicFieldListPropertyName + '[' + submitCount + '].' + this.dynamicFieldTypePropertyName + '.id'] = field.fieldTypeId;
					}
					const value = this.getValueFromField(field);
					const text = this.getTextFromField(field, value);
					if (this.dynamicFieldClass) {
						result[this.dynamicFieldListPropertyName + '[' + submitCount + '].class'] = this.dynamicFieldClass;
					}
					result[this.dynamicFieldListPropertyName + '[' + submitCount + '].' + this.dynamicFieldValuePropertyName] = value;
					result[this.dynamicFieldListPropertyName + '[' + submitCount + '].' + this.dynamicFieldTextPropertyName] = text;
					if (field.targetField) {
						const typeFieldPropertyName = this.dynamicFieldListPropertyName + '[' + submitCount + '].' + this.dynamicFieldTypePropertyName;
						result[typeFieldPropertyName + '.virtualTypeId'] = field.fieldVirtualTypeId;
						result[typeFieldPropertyName + '.targetSystemUserInterfaceAware.id'] = field.targetField.id;
					}
					submitCount++;
				}
			}
		}
		if (this.appendSubmitParams) {
			result = this.appendSubmitParams(result);
		}
		return result;
	},

	getDynamicTriggerFieldValue: function() {
		const f = this.getForm().findField(this.dynamicTriggerValueFieldName);
		return f ? f.getValue() : '';
	},

	// remove previously added and replace them with the new fields
	resetDynamicFields: function(triggerField, cancelIfLoaded, keepCurrentFieldValues) {
		if ((triggerField && !triggerField.isValid()) || !this.getDynamicFields) {
			return;
		}
		if (cancelIfLoaded && this.dynamicFieldsLoaded) {
			return;
		}
		this.dynamicFieldsLoaded = true; // to avoid unnecessary reloads
		this.previousDynamicFieldValues = undefined;
		if (keepCurrentFieldValues) {
			this.previousDynamicFieldValues = this.getCurrentDynamicFieldValues(); // keep the current field value to reset the field if needed
		}
		// remove existing dynamic fields first
		if (this.dynamicFields) {
			// Dynamic Form Fragment
			if (this.dynamicFieldFormFragment) {
				const formFragment = TCG.getChildByName(this, this.dynamicFieldFormFragment);
				formFragment.removeAll(true);
			}
			else {
				for (let i = 0; i < this.dynamicFields.length; i++) {
					const item = this.dynamicFields[i];
					this.remove(item, true);
				}
			}
			this.dynamicFields = null;
		}
		// add new dynamic fields for the specified triggerField
		this.getDynamicFields(triggerField, this.addDynamicFields);
	},

	getCurrentDynamicFieldValues: function() {
		const result = [];
		if (this.dynamicFields) {
			for (let i = 0; i < this.dynamicFields.length; i++) {
				const field = this.dynamicFields[i];
				// do not submit fields with no values
				if (TCG.isNotNull(field) && TCG.isNotBlank(field.getValue())
					&& field.submitDetailField !== false
					&& !field.targetField) {
					const valueObject = {};
					valueObject.value = this.getValueFromField(field);
					valueObject.text = this.getTextFromField(field, valueObject.value);
					valueObject.type = {id: field.fieldTypeId};
					valueObject.targetField = field.targetField;
					valueObject.dirty = field.isDirty();
					result.push(valueObject);
				}
			}
		}
		return result;
	},

	getDynamicFieldsParams: function() {
		const fp = this;
		const params = {};
		if (this.dynamicFieldsUrlListParameterName) {
			params['requestedMaxDepth'] = 3;
			params[fp.dynamicFieldsUrlParameterName] = this.getDynamicTriggerFieldValue();
			if (this.previousDynamicFieldValues) {
				let submitCount = 0;
				for (let i = 0; i < this.previousDynamicFieldValues.length; i++) {
					const parameter = this.previousDynamicFieldValues[i];
					if (TCG.isNull(parameter.targetField)) {
						params[this.dynamicFieldsUrlListParameterName + '[' + submitCount + '].' + fp.dynamicFieldsUrlParameterName] = parameter.type.id;
						params[this.dynamicFieldsUrlListParameterName + '[' + submitCount + '].text'] = parameter.text;
						params[this.dynamicFieldsUrlListParameterName + '[' + submitCount + '].value'] = parameter.value;
						submitCount++;
					}
				}
			}
			else {
				// only submit the bean id if a new bean has bean selected
				params['id'] = fp.getFormValue(this.dynamicFieldsUrlIdFieldName);
			}
		}
		else {
			params[fp.dynamicFieldsUrlParameterName] = this.getDynamicTriggerFieldValue();
			params['requestedMaxDepth'] = 5;
		}
		return params;
	},

	getDynamicFieldNameByFieldLabel: function(fieldLabel) {
		if (this.dynamicFields) {
			for (let i = 0; i < this.dynamicFields.length; i++) {
				const field = this.dynamicFields[i];
				if (TCG.isNotNull(field) && TCG.isEquals(field.fieldLabel, fieldLabel)) {
					return field.name;
				}
			}
		}
		return undefined;
	},

	getDynamicFieldNameByRemoteName: function(remoteName) {
		if (this.dynamicFields) {
			for (let i = 0; i < this.dynamicFields.length; i++) {
				const field = this.dynamicFields[i];
				if (field && TCG.isEquals(field.remoteName, remoteName)) {
					return field.name;
				}
			}
		}
		return undefined;
	},

	// call-back because get is asynchronous
	getDynamicFields: function(triggerField, callback) {
		const fp = this;
		const params = this.getDynamicFieldsParams();
		const loader = new TCG.data.JsonLoader({
			waitTarget: fp,
			waitMsg: 'Loading fields...',
			params: params,
			onLoad: function(records, conf) {
				const fields = [];
				for (let i = 0; i < records.length; i++) {
					const record = records[i];
					const field = fp.getDynamicFieldFromRecord(fp, record, i);
					if (field) {
						if (Ext.type(field) === 'array') {
							for (let j = 0; j < field.length; j++) {
								if (field[j]) {
									fields.push(field[j]);
								}
							}
						}
						else {
							fields.push(field);
						}
					}
				}
				callback.apply(fp, [fields]);
				// clear the previous values
				this.previousDynamicFieldValues = undefined;
			}
		});
		loader.load(this.dynamicFieldsUrl);
	},

	addDynamicFields: function(fields) {
		const f = this.getForm();
		this.dynamicFields = [];

		// Dynamic Form Fragment
		let ff = this;
		if (this.dynamicFieldFormFragment) {
			ff = TCG.getChildByName(this, this.dynamicFieldFormFragment);
		}

		for (let i = 0; i < fields.length; i++) {
			const field = fields[i];
			ff.add(field);
			Array.prototype.push.apply(this.dynamicFields, ff.find('name', field.name));

			// hide and do not submit target fields that store the json for other fields
			if (field.targetField) {
				const targetFieldName = this.getFieldNameFromType(field.targetField);
				f.findField(targetFieldName).submitDetailField = false;
				f.findField(targetFieldName).hidden = true;
			}
		}
		ff.doLayout();
		this.setDynamicFieldsValues();
		this.loadValidationMetaData(true);
		this.getForm().isValid();
	},

	setDynamicFieldsValues: function() {
		const formValues = this.getForm().formValues;
		// if loaded, then trigger field value is the same as corresponding loaded value
		if ((formValues && TCG.isEquals(TCG.getValue(this.dynamicTriggerValueFieldName, formValues), this.getDynamicTriggerFieldValue())) || this.previousDynamicFieldValues) {
			for (let i = 0; i < this.dynamicFields.length; i++) {
				const field = this.dynamicFields[i];
				this.setDynamicFieldValue(field);
			}
		}
	},

	setDynamicFieldValue(field) {
		if (TCG.isNotNull(field) && field.isFormField) {
			const fieldValue = this.getDynamicPropertyFromField(field);
			if (field.xtype === 'listfield' && fieldValue) {
				//split the field value and text and build an array of objects that are [{label:"",id:},..]
				const texts = fieldValue.text.split('::');
				const values = fieldValue.value.split('::');
				const objValues = [];

				for (let j = 0; j < values.length; j++) {
					const idVal = (values[j] === '' || isNaN(values[j])) ? values[j] : parseInt(values[j]); //pull off the ""
					const listObj = {
						[field.controlField.displayField || 'label']: texts[j],
						[field.controlField.valueField || 'id']: idVal
					};

					objValues.push(listObj);
				}

				field.setValue(objValues);
				if (!fieldValue.dirty || fieldValue.dirty !== true) {
					field.originalValue = field.getValue(); // mark as non-dirty
				}

			}
			else {
				field.setValue(this.getDynamicFieldValue(field, fieldValue, this.dynamicFieldValuePropertyName));
				if (TCG.isNull(fieldValue) || !fieldValue.dirty || fieldValue.dirty !== true) {
					field.originalValue = field.getValue(); // mark as non-dirty
				}
				// Can be a combo, or a custom version of a combo - i.e. system-condition-combo
				if (TCG.endsWith(field.xtype, 'combo')) {
					field.lastSelectionText = this.getDynamicFieldValue(field, fieldValue, this.dynamicFieldTextPropertyName);
					field.setRawValue(field.lastSelectionText);
				}
			}
		}
	},
	getDynamicFieldValue: function(field, fieldValue, dynamicFieldPropertyName) {
		let value = TCG.getValue(dynamicFieldPropertyName, fieldValue);
		if (TCG.isBlank(value) && field.getDefaultValue) {
			value = field.getDefaultValue();
		}
		if (TCG.isBlank(value)) {
			value = '';
		}
		return value;
	},
	getDynamicPropertyFromField: function(field) {
		const typeId = field.name.substring(field.name.lastIndexOf('_') + 1);
		let fieldValue = null;
		const f = this.getForm();
		const idPropertyName = this.dynamicFieldTypePropertyName + '.' + (field.fieldVirtualTypeId ? 'virtualTypeId' : 'id');
		if (this.previousDynamicFieldValues) {
			for (let j = 0; j < this.previousDynamicFieldValues.length; j++) {
				const p = this.previousDynamicFieldValues[j];
				if (TCG.isEquals(TCG.getValue(idPropertyName, p), typeId)) {
					fieldValue = p;
					break;
				}
			}
		}
		if (TCG.isNull(fieldValue)) {
			const savedProps = f.formValues ? TCG.getValue(this.dynamicFieldListPropertyName, f.formValues) : undefined;
			if (savedProps) {
				for (let j = 0; j < savedProps.length; j++) {
					const p = savedProps[j];
					if (TCG.isEquals(TCG.getValue(idPropertyName, p), typeId)) {
						fieldValue = p;
						break;
					}
				}
			}
		}
		return fieldValue;
	},

	// returns form field for the specified field type
	getDynamicFieldFromRecord: function(fp, fieldType, index) {
		let controlConfig;
		let field = {
			xtype: 'textfield',
			name: this.getFieldNameFromType(fieldType),
			listeners: {},
			fieldLabel: fieldType.label || fieldType.name,
			targetField: fieldType.targetSystemUserInterfaceAware,
			triggerField: fieldType.resetDynamicFieldsOnChange,
			submitValue: false,
			allowBlank: !fieldType.required,
			fieldVirtualTypeId: fieldType.virtualTypeId,
			fieldTypeId: fieldType.id, // used during bean property creation to identify field type
			remoteName: fieldType.systemPropertyName
		};
		if (fieldType.description) {
			field.qtip = fieldType.description;
		}
		if (fieldType.resetDynamicFieldsOnChange) {
			field.listeners[fieldType.valueListUrl ? 'select' : 'change'] = function(combo) {
				combo.ownerCt.resetDynamicFields(combo, false, true);
			};
		}

		const dt = fieldType.dataType.name;
		if (fieldType.valueGroup) {
			Ext.apply(field, {
				xtype: 'combo',
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				url: 'systemBeanListFind.json?groupId=' + fieldType.valueGroup.id,
				fieldGroupId: fieldType.valueGroup.id,
				fieldGroupName: fieldType.valueGroup.name,
				getDefaultData: function(form) {
					return {type: {group: {id: this.fieldGroupId, name: this.fieldGroupName}}};
				}
			});
		}
		else if (fieldType.valueListUrl) {
			field.xtype = 'combo';
			field.url = fieldType.valueListUrl;
			if (fieldType.valueTable && fieldType.valueTable.detailScreenClass) {
				field.detailPageClass = fieldType.valueTable.detailScreenClass;
			}
		}
		else if (dt === 'DATE') {
			field.xtype = 'datefield';
		}
		else if (dt === 'INTEGER') {
			field.xtype = 'integerfield';
		}
		else if (dt === 'DECIMAL') {
			field.xtype = 'currencyfield';
		}
		else if (dt === 'BOOLEAN') {
			field.xtype = 'checkbox';
		}
		else if (dt === 'SECRET') {
			field.xtype = 'secretfield';
			field.submitDetailField = true;
		}
		else if (dt === 'TIME') {
			field.xtype = 'timefield';
			field.format = 'H:i:s';
		}
		else if (dt === 'TABLE') {
			field.xtype = 'grid-field';
		}

		// apply custom configuration if any
		if (fieldType.userInterfaceConfig) {
			Ext.apply(field, Ext.decode(fieldType.userInterfaceConfig));
		}

		if (fieldType.multipleValuesAllowed) {
			controlConfig = field;
			field = {
				xtype: 'listfield',
				allowBlank: true,
				name: controlConfig.name,
				remoteName: controlConfig.remoteName,
				detailPageClass: controlConfig.detailPageClass,
				allowDuplicates: controlConfig.allowDuplicates,
				fieldGroupId: controlConfig.fieldGroupId,
				fieldGroupName: controlConfig.fieldGroupName,
				getDefaultData: controlConfig.getDefaultData,
				mutuallyExclusiveFields: controlConfig.mutuallyExclusiveFields,
				mutuallyExclusiveRemoteFields: controlConfig.mutuallyExclusiveRemoteFields,
				requiredFields: controlConfig.requiredFields,
				requiredRemoteFields: controlConfig.requiredRemoteFields,
				fieldLabel: fieldType.label || fieldType.name,
				fieldTypeId: fieldType.id
			};

			// Apply control configuration overrides
			if (fieldType.valueListUrl) {
				controlConfig.hiddenName = 'id';
			}
			Ext.apply(controlConfig, {
				allowBlank: true,
				mutuallyExclusiveFields: null,
				mutuallyExclusiveRemoteFields: null,
				requiredFields: null,
				requiredRemoteFields: null
			});
			field.controlConfig = controlConfig;
		}

		return field;
	},

	getFieldNameFromType: function(fieldType) {
		if (fieldType.virtualTypeId) {
			return 'field_virtual_' + fieldType.virtualTypeId;
		}
		return 'field_' + fieldType.id;
	},

	getValueFromField: function(field) {
		let value = field.getValue();
		if (field.xtype === 'datefield') {
			value = field.getRawValue();
		}
		return value;
	},

	getTextFromField: function(field, value) {
		let text = value;
		// Can be a combo, or a custom version of a combo - i.e. system-condition-combo
		if (TCG.endsWith(field.xtype, 'combo')) {
			text = field.lastSelectionText;
		}
		if (field.xtype === 'listfield' || field.xtype === 'secretfield' || field.xtype === 'secrettextarea') {
			text = field.getText();
		}
		if (field.getSubmitText) {
			text = field.getSubmitText();
		}
		return text;
	}

});
Ext.reg('formwithdynamicfields', TCG.form.FormPanelWithDynamicFields);

// default submit configuration to handle errors
TCG.form.submitDefaults = {
	timeout: 40, // seconds
	timeoutErrorMsg: 'Timeout reached while waiting for a response from the server.',
	failure: function(form, action) {
		let msg = 'Error sending data to: <b>' + action.getUrl() + '</b><br /><br />';
		const result = action.result;
		if (result && result.errors) {
			msg = 'Please correct the following highlighted errors:<br /><br /><ul>';
			const err = result.errors;
			for (let i = 0; i < err.length; i++) {
				msg += '<li>' + Ext.util.Format.htmlEncode(err[i].msg) + '</li>';
			}
			msg += '</ul>';
		}
		else {
			if (result && result.validationError) {
				const classOverride = TCG.data.ErrorWindows[result.errorType];
				if (classOverride) {
					TCG.createComponent(classOverride, {data: result});
					return;
				}
				if (result.errorType === 'UserIgnorableValidationException' && !TCG.isBlank(result.ignoreExceptionUrl)) {
					Ext.Msg.confirm('Validation Error(s)', `${result.message}<br/><br/><b>${result.confirmationMessage}</b>`, function(a) {
						if (a === 'yes') {
							if (action.options.url.includes('?')) {
								Ext.applyIf(action.options.params, Ext.urlDecode(action.options.url.split('?').slice(-1)[0], false));
							}
							action.form.submit(Ext.applyIf({url: encodeURI(result.ignoreExceptionUrl)}, action.options));
						}
					});
					return;
				}
				msg = 'Please correct the following validation error:<br /><br />';
			}
			if (result && result.message) {
				msg += result.message;
			}
			else if (action.failureType === 'connect' && action.options.timeoutErrorMsg) {
				msg += action.options.timeoutErrorMsg;
			}
			else {
				msg += 'Failure Type: ' + action.failureType;
			}
		}
		TCG.data.ErrorHandler.handleFailure(form, result ? result : action, msg);
	}
};

Ext.Panel.prototype.warningMessageCls = 'warning-msg';
Ext.Panel.prototype.onRender = Ext.Panel.prototype.onRender.createSequence(function() {
	if ((this.getInstructions || this.instructions) && this.xtype && (this.xtype.startsWith('form') || this.xtype === 'fieldset' || this.isXType('formpanel')) && (this.hideInstructions !== true)) {
		let instructions = this.instructions;
		if (this.getInstructions) {
			instructions = this.getInstructions();
		}
		this.insert(0, {xtype: 'label', html: instructions + '<br /><br />'});
	}
	// on form afterload, if warning message is returned, display it as the first element
	if (this.getWarningMessage) {
		const fp = (this instanceof Ext.form.FormPanel) ? this : TCG.getParentFormPanel(this);
		fp.addListener('afterload', function(fp) {
			const msg = this.getWarningMessage(fp.getForm());
			if (this.warningMsg !== msg) {
				this.updateWarningMessage(msg);
			}
		}, this);
	}
});
Ext.Panel.prototype.updateWarningMessage = function(msg) {
	this.warningMsg = msg;
	const warnId = this.id + '-warn';
	const warn = Ext.get(warnId);
	if (TCG.isNotNull(warn)) {
		this.remove(warnId);
		Ext.destroy(warn);
		this.doLayout();
	}
	let messageItems = msg;
	if (TCG.isNotNull(msg)) {
		if (!Ext.isArray(msg)) {
			messageItems = [{xtype: 'label', html: msg}];
		}
		const warningContainer = {xtype: 'container', layout: 'hbox', id: warnId, autoEl: 'div', cls: this.warningMessageCls, items: messageItems};
		if (TCG.isNotBlank(this.warningMessageHeight)) {
			warningContainer.height = this.warningMessageHeight;
		}
		this.insert(0, warningContainer);
		this.doLayout();
	}
};

TCG.form.SearchForm = Ext.extend(TCG.form.FormPanel, {
	loadValidation: false,
	defaults: {},
	defaultType: 'panel',
	autoWidth: true,

	// add search/clear buttons
	initComponent: function() {
		TCG.form.SearchForm.superclass.initComponent.call(this);
		const o = new Ext.FormPanel({
			items: [{
				layout: 'form',
				defaults: {minWidth: 80, xtype: 'button'},
				searchForm: this,
				items: [{
					text: 'Search',
					name: 'search',
					type: 'submit',
					tooltip: 'Perform search using selected filter(s)',
					cls: 'new-line',
					handler: function() {
						TCG.showInfo('Searching: ' + this.ownerCt.searchForm.url);
					}
				}, {
					text: 'Clear',
					name: 'clear',
					tooltip: 'Clear all search fields',
					handler: function() {
						const f = this.ownerCt.searchForm;
						f.clearFields(f.items);
					}
				}]
			}]
		});

		const firstItem = this.items.get(0);
		if (firstItem.layout === 'column') {
			const b = o.items.get(0);
			b.columnWidth = 0.2;
			firstItem.items.add(b);
		}
	}
});

TCG.form.RadioInputSelect = Ext.extend(Ext.Panel, {
	toolbars: [], // work around for EXTJS 3.1.0
	layout: 'column',
	border: false,
	selectName: '',
	selectColumnWidth: .25,
	selectTitle: '',
	controlTitle: '',
	controlHeight: 200,
	selectGroupClass: Ext.form.RadioGroup,
	defaults: {
		border: false
	},
	inputControls: [],
	getParentForm: function() {
		if (!this.parentForm) {
			this.parentForm = TCG.getParentFormPanel(this);
		}
		return this.parentForm;
	},
	onRender: function() {
		TCG.form.RadioInputSelect.superclass.onRender.apply(this, arguments);

		const control = this;
		const form = this.getParentForm();

		// clear the default or previous control control
		form.on('afterload', function() {
			const value = TCG.getValue(control.selectName, control.getFormValues().data);
			for (let i = 0; i < control.selectGroupControl.items.items.length; i++) {
				const item = control.selectGroupControl.items.items[i];
				if ((item.inputValue !== value) && item.runtimeControl) {
					control.removeInputControl(item);
					item.checked = false;
				}
				else if (item.inputValue === value) {
					control.loadInputControl(item);
					if (TCG.isTrue(control.selectGroupControl.disabled)) {
						item.runtimeControl.setDisabled(true);
					}
				}
			}

			form.loadJsonResult(control.getFormValues(), false);
		});
	},
	initComponent: function() {
		const control = this;

		this.selectPanel = new Ext.Panel({
			columnWidth: this.selectColumnWidth,
			bodyStyle: 'padding-right:5px;',
			layout: 'form'
		});
		this.add(this.selectPanel);
		this.selectFieldSet = new TCG.form.FieldSet({
			title: this.selectTitle,
			labelWidth: 1,
			height: this.controlHeight,
			autoHeight: false,
			collapsible: false
		});
		this.selectPanel.add(this.selectFieldSet);

		const selectGroup = {
			xtype: 'radiogroup',
			name: this.selectName,
			vertical: true,
			columns: 1,
			items: [],
			autoWidth: true
		};

		this.controlPanel = new Ext.Panel({
			columnWidth: .75,
			bodyStyle: 'padding-left:5px;',
			layout: 'form'
		});
		this.add(this.controlPanel);

		this.controlFieldSet = new TCG.form.FieldSet({
			title: this.controlTitle,
			height: this.controlHeight,
			autoHeight: false,
			collapsible: false
		});
		this.controlPanel.add(this.controlFieldSet);

		const radioButtonHandler = function(checkbox, checked) {
			const form = control.getParentForm();
			if (form && form.getForm().loaded) {
				if (checked) {
					control.loadInputControl(checkbox);
				}
				else {
					control.removeInputControl(checkbox);
				}
			}
		};

		for (let i = 0; i < this.inputControls.length; i++) {

			const ctrl = Ext.apply({
				border: false,
				collapsible: false,
				xtype: 'fieldset',
				defaultType: 'panel'
			}, this.inputControls[i].control);

			const radioButton = Ext.apply({
				inputControl: ctrl,
				name: this.selectName,
				handler: radioButtonHandler
			}, this.inputControls[i].selectControl);

			if (radioButton.checked) {
				this.loadInputControl(radioButton, true);
			}

			selectGroup.items.push(radioButton);
		}
		this.selectGroupControl = this.selectFieldSet.add(selectGroup);
	},

	// create the input control
	loadInputControl: function(checkbox, doNotUpdateValidationData) {
		// clear current control first or validation setting do not work
		if (this.activeControl) {
			this.removeInputControl(this.activeControl);
		}
		this.activeControl = checkbox;
		checkbox.runtimeControl = this.controlFieldSet.add(checkbox.inputControl);
		this.controlFieldSet.doLayout();
		const fp = this.getParentForm();
		if (fp && fp.getForm().loaded) {
			// reset the values if editing
			const value = TCG.getValue(this.selectName, this.getFormValues().data);
			if ((fp.getIdFieldValue() > 0) && fp.getForm().formValues && (checkbox.inputValue === value)) {
				fp.loadJsonResult(this.getFormValues());
			}
			// reset the validation
			if (!doNotUpdateValidationData) {
				fp.loadValidationMetaData(true);
			}
		}
	},

	// remove the input control
	removeInputControl: function(checkbox) {
		if (checkbox.runtimeControl) {
			this.removeFields(checkbox.runtimeControl.items);
			this.controlFieldSet.remove(checkbox.runtimeControl);
			checkbox.runtimeControl = null;
		}
		this.activeControl = undefined;
	},

	// remove the fields from the form panel
	removeFields: function(items) {
		const form = this.getParentForm();
		const ctrl = this;
		items.each(function(o) {
			if (o.items && o.items.length > 0) {
				ctrl.removeFields(o.items);
			}
			form.removeField(o);
		});
	},

	// mimic the load data using formValues
	getFormValues: function() {
		const form = this.getParentForm();
		return {data: form.getForm().formValues};
	}
});

Ext.reg('radioinputselect', TCG.form.RadioInputSelect);

TCG.form.FieldSet = Ext.extend(Ext.form.FieldSet, {
	columnWidth: 1,
	collapsible: true,
	autoHeight: true,
	defaults: {
		anchor: '-20' // leave room for error icon
	},
	onDisable: function() {
		this.items.items.forEach(function(item) {
			item.disable();
		});
	},
	defaultType: 'textfield'
});
Ext.reg('fieldset', TCG.form.FieldSet);

TCG.form.FieldSetCheckbox = Ext.extend(Ext.form.FieldSet, {
	columnWidth: 1,
	autoHeight: true,
	defaults: {
		anchor: '-20' // leave room for error icon
	},
	defaultType: 'textfield',
	collapsible: false,
	checkboxToggle: true,
	collapsed: true,

	getParentForm: function() {
		if (!this.parentForm) {
			this.parentForm = TCG.getParentFormPanel(this);
		}
		return this.parentForm;
	},

	onRender: function(ct, position) {
		if (!this.el) {
			this.el = document.createElement('fieldset');
			this.el.id = this.id;
			if (this.title || this.header || this.checkboxToggle) {
				this.el.appendChild(document.createElement('legend')).className = this.baseCls + '-header';
			}
		}

		Ext.form.FieldSet.superclass.onRender.call(this, ct, position);

		if (this.instructions) {
			this.insert(0, {xtype: 'label', html: this.instructions + '<br /><br />'});
		}

		if (this.checkboxToggle) {
			const o = typeof this.checkboxToggle == 'object' ?
				this.checkboxToggle :
				{tag: 'input', type: 'checkbox', name: this.checkboxName || this.id + '-checkbox'};
			this.checkbox = this.header.insertFirst(o);
			this.checkbox.dom.checked = false;
			this.mon(this.checkbox, 'click', this.onCheckClick, this);
		}

		if (this.checkboxName) {
			this.getParentForm().on('afterload', function() {
				this.synchCheckboxValue();
			}, this);
		}

	},

	synchCheckboxValue: function() {
		this.checkbox.dom.checked = TCG.getValue(this.checkboxName, this.getParentForm().form.formValues);
		this.onCheckClick();
	}
});
Ext.reg('fieldset-checkbox', TCG.form.FieldSetCheckbox);

/**
 * Uses radio field & expand name to determine whether or not to expand/collapse
 */
TCG.form.FieldSetRadio = Ext.extend(Ext.form.FieldSet, {
	columnWidth: 1,
	autoHeight: true,
	defaults: {
		anchor: '-20' // leave room for error icon
	},
	defaultType: 'textfield',
	collapsible: false,
	collapsed: true,

	// default to no frame & border since most cases this will be the case
	frame: false,
	border: false,

	groupName: undefined, // If set, will attach change event listener to the group field
	radioName: undefined,
	expandValue: undefined,

	getParentForm: function() {
		if (!this.parentForm) {
			this.parentForm = TCG.getParentFormPanel(this);
		}
		return this.parentForm;
	},

	onRender: function(ct, position) {
		TCG.form.FieldSetRadio.superclass.onRender.call(this, ct, position);

		if (this.radioName) {
			this.getParentForm().on('afterload', function() {
				this.onCheckClick();
			}, this);
		}
		const f = this.getParentForm().getForm();
		const radioField = (this.groupName ? f.findField(this.groupName) : f.findField(this.radioName));
		this.mon(radioField, 'change', this.onCheckClick, this);
	},

	onCheckClick: function() {
		const f = this.getParentForm().getForm();
		const radioField = f.findField(this.radioName);
		const radioValue = (this.groupName ? radioField.getGroupValue() : radioField.getValue());

		const type = Ext.type(this.expandValue);
		let exp = false;
		if (type === 'string') {
			if (this.expandValue === radioValue) {
				exp = true;
			}
		}
		else if (type === 'object' || type === 'array') {
			Ext.each(this.expandValue, function(expVal, index) {
				if (expVal === radioValue) {
					exp = true;
				}
			});
		}
		if (exp === true) {
			this['expand']();
		}
		else {
			this['collapse']();
		}
	},

	getRadioField: function() {
		const f = this.getParentForm().getForm();
		const radioField = (this.groupName ? f.findField(this.groupName) : f.findField(this.radioName));
		return radioField;
	}
});
Ext.reg('fieldset-radio', TCG.form.FieldSetRadio);

TCG.form.MonthYearDateField = Ext.extend(Ext.form.DateField, {
	format: 'F, Y',
	plugins: ['monthPickerPlugin']
});
Ext.reg('datefield-monthYear', TCG.form.MonthYearDateField);

TCG.form.MonthPickerPlugin = function() {
	let picker = undefined;
	let oldDateDefaults = undefined;

	this.init = function(pk) {
		picker = pk;
		picker.onTriggerClick = picker.onTriggerClick.createSequence(onClick);
		picker.getValue = picker.getValue.createInterceptor(setDefaultMonthDay).createSequence(restoreDefaultMonthDay);
		picker.beforeBlur = picker.beforeBlur.createInterceptor(setDefaultMonthDay).createSequence(restoreDefaultMonthDay);
	};

	function setDefaultMonthDay() {
		oldDateDefaults = Date.defaults.d;
		Date.defaults.d = 1;
		return true;
	}

	function restoreDefaultMonthDay(ret) {
		Date.defaults.d = oldDateDefaults;
		return ret;
	}

	function onClick(e, el, opt) {
		const p = picker.menu.picker;
		p.activeDate = p.activeDate.getFirstDateOfMonth();
		if (p.value) {
			p.value = p.value.getFirstDateOfMonth();
		}

		p.showMonthPicker();

		if (!p.disabled) {
			p.monthPicker.stopFx();
			p.monthPicker.show();

			p.mun(p.monthPicker, 'click', p.onMonthClick, p);
			p.mun(p.monthPicker, 'dblclick', p.onMonthDblClick, p);
			p.onMonthClick = p.onMonthClick.createSequence(pickerClick);
			p.onMonthDblClick = p.onMonthDblClick.createSequence(pickerDblclick);
			p.mon(p.monthPicker, 'click', p.onMonthClick, p);
			p.mon(p.monthPicker, 'dblclick', p.onMonthDblClick, p);
		}
	}

	function pickerClick(e, t) {
		const el = new Ext.Element(t);
		if (el.is('button.x-date-mp-cancel')) {
			picker.menu.hide();
		}
		else if (el.is('button.x-date-mp-ok')) {
			const p = picker.menu.picker;
			p.setValue(p.activeDate);
			p.fireEvent('select', p, p.value);
		}
	}

	function pickerDblclick(e, t) {
		const el = new Ext.Element(t);
		if (el.parent()
			&& (el.parent().is('td.x-date-mp-month')
				|| el.parent().is('td.x-date-mp-year'))) {

			const p = picker.menu.picker;
			p.setValue(p.activeDate);
			p.fireEvent('select', p, p.value);
		}
	}
};

Ext.preg('monthPickerPlugin', TCG.form.MonthPickerPlugin);

// A plugin that creates functionality necessary to work with Freemarker
// logic within an HtmlEditor.
TCG.form.HtmlEditorFreemarker = Ext.extend(Ext.util.Observable, {
	hiddenField: undefined,

	// private
	init: function(cmp) {
		this.cmp = cmp;
		if (this.hiddenField) {
			this.cmp.ownerCt.remove(this.hiddenField);
			this.hiddenField = undefined;
		}
		this.createHiddenField();
		this.cmp.ownerCt.items.add(this.hiddenField);
		this.cmp.on('beforesync', this.beforeSync, this);
		this.cmp.on('beforepush', this.beforePush, this);
	},

	getParentForm: function() {
		if (!this.parentForm) {
			this.parentForm = TCG.getParentFormPanel(this);
		}
		return this.parentForm;
	},

	createHiddenField: function() {
		this.hiddenField = new Ext.form.Hidden({
			name: this.cmp.hiddenName,
			submitValue: true,
			ownerPlugin: this,
			autoCreate: true,

			// When "Setting" the hidden field value - set the html editor field properly
			setValue: function(v) {
				this.setValueWithTrigger(v, true);
			},

			getValue: function() {
				if (this.ownerPlugin.cmp.sourceEditMode) {
					this.ownerPlugin.cmp.pushValue();
				}
				else {
					this.ownerPlugin.cmp.syncValue();
				}
				return Ext.form.Hidden.superclass.getValue.call(this);
			},

			setValueWithTrigger: function(v, triggerUpdate) {
				Ext.form.Hidden.superclass.setValue.call(this, v);
				if (this.ownerPlugin.cmp.readOnly === true) {
					this.originalValue = v;
				}

				if (triggerUpdate) {
					if (this.ownerPlugin.cmp.sourceEditMode) {
						this.ownerPlugin.cmp.setValue(v);
					}
					else {
						this.ownerPlugin.cmp.setValue(this.ownerPlugin.convertFromFreemarkerLogic(v));
					}
					if (this.ownerPlugin.cmp.readOnly === true) {
						this.ownerPlugin.cmp.originalValue = this.ownerPlugin.cmp.getValue();
					}
				}
			}
		});
	},

	beforeSync: function(editor, html) {
		this.cmp.el.dom.value = this.convertToFreemarkerLogic(html);
		this.cmp.fireEvent('sync', this.cmp, this.cmp.el.dom.value);
		return false;
	},

	beforePush: function(editor, html) {
		editor.getEditorBody().innerHTML = this.convertFromFreemarkerLogic(html);
		if (Ext.isGecko) {
			// Gecko hack, see: https://bugzilla.mozilla.org/show_bug.cgi?id=232791#c8
			this.cmp.setDesignMode(false);  //toggle off first
			this.cmp.setDesignMode(true);
		}
		this.cmp.fireEvent('push', this.cmp, html);
		return false;
	},

	// Converts the String str to Freemarker syntax or reverse.
	// For Example: If converting to fromStart would be LOGIC-START( toStart would be <#
	convertToFreemarkerLogic: function(str) {
		if (str && str.length !== 0) {
			str = this.convertFreemarkerLogic(str, '<!--LOGIC-START(', '<#', ')-->', '>');
			str = this.convertFreemarkerLogic(str, '<!--LOGIC-END(', '</#', ')-->', '>');
		}
		return str;
	},

	convertFromFreemarkerLogic: function(str) {
		if (str && str.length !== 0) {
			str = this.convertFreemarkerLogic(str, '<#', '<!--LOGIC-START(', '>', ')-->');
			str = this.convertFreemarkerLogic(str, '</#', '<!--LOGIC-END(', '>', ')-->');
		}
		return str;
	},

	convertFreemarkerLogic: function(str, fromStart, toStart, fromEnd, toEnd) {
		str = String(str);
		if (str.length === 0) {
			return str;
		}

		const text = str.split(fromStart);
		// Start at the 2nd index
		for (let i = 1; i < text.length; i++) {
			text[i] = toStart + text[i];
			// Just want to replace the first instance
			const startIndex = text[i].indexOf(fromEnd);
			const endIndex = startIndex + fromEnd.length;
			text[i] = text[i].substring(0, startIndex) + toEnd + text[i].substring(endIndex, text[i].length);
		}
		str = '';
		for (let i = 0; i < text.length; i++) {
			str = str + text[i];
		}
		return str;
	}

});
Ext.preg('htmleditor-freemarker', TCG.form.HtmlEditorFreemarker);

// Copied from Ext js threads: http://www.sencha.com/forum/showthread.php?57571-Adding-tooltip-to-form-fields
(function() {
	Ext.override(Ext.BoxComponent, tooltipOverrideGenerator(Ext.BoxComponent));
	Ext.override(Ext.Container, tooltipOverrideGenerator(Ext.Container));

	function tooltipOverrideGenerator(clazz) {
		return {
			afterRender: clazz.prototype.afterRender.createSequence(function() {
				if (this.qtip) {
					const target = this.getTipTarget();
					if (typeof this.qtip == 'object') {
						Ext.QuickTips.register(Ext.apply({
							target: target
						}, this.qtip));
					}
					else {
						target.dom.qtip = this.qtip;
					}
				}
			}),
			getTipTarget: function() {
				let tipEl;
				let wrapperEl;
				let isToolbar = false;

				// Get wrapper element containing input field and its label
				if (this.ownerCt.isXType('toolbar')) {
					isToolbar = true;
					if (this.isXType('checkbox')) {
						wrapperEl = this.el.up('.x-form-check-wrap', 10);
					}
					else {
						wrapperEl = this.el.up('.x-toolbar-cell', 10).prev('.x-toolbar-cell');
					}
				}
				else {
					// Assume component is part of a form
					wrapperEl = this.el.up('.x-form-item', 10);
				}

				// Get target for quicktip
				tipEl = this.el;
				if (this.boxLabel && this.boxLabel !== '&#160;') {
					tipEl = wrapperEl.child('.x-form-cb-label');
				}
				else if (this.fieldLabel) {
					tipEl = wrapperEl.child(isToolbar ? '.xtb-text' : '.x-form-item-label');
				}
				return tipEl;
			}
		};
	}
})();

TCG.form.ListField = Ext.extend(Ext.form.Field, {
	defaultAutoCreate: {tag: 'div'},
	controlConfig: null,
	selectedValues: [],
	listItems: [],
	defaultItems: [],
	defaultItemsApplied: false,
	deleteClsClass: 'remove',
	minSelections: 0,
	maxSelections: Number.MAX_VALUE,
	allowBlank: false,
	isOriginalValueSet: false,
	allowDuplicates: false,

	initComponent: function() {
		TCG.form.ListField.superclass.initComponent.apply(this, arguments);

		this.addEvents({
			'change': true
		});

		this.selectedValues = [];
		this.listItems = [];
		this.isOriginalValueSet = false;
	},

	onRender: function(ct, position) {
		TCG.form.ListField.superclass.onRender.apply(this, arguments);

		if (this.controlConfig) {
			const parent = this;
			const fs = new Ext.Panel({
				renderTo: parent.el,
				layout: 'fit',
				frame: true
			});
			parent.fs = fs;

			// Apply control field defaults
			Ext.applyIf(this.controlConfig, {
				xtype: 'combo',
				submitValue: false,
				listField: parent
			});

			// Generate control field
			const controlField = fs.add(this.controlConfig);
			this.controlField = controlField;
			controlField.getParentForm = function() {
				return parent.getParentForm();
			};
			fs.doLayout();

			if (!controlField.isXType('combo')) {
				// this is a hack but I can't get the field to maintain it's height
				const originalFieldHeight = this.controlConfig.height || this.controlField.getOuterSize().height;
				this.originalFieldHeight = originalFieldHeight;
				controlField.on({
					'resize': function() {
						controlField.setHeight(originalFieldHeight);
					}
				});
			}

			// remove the name attribute of the control field so it is not submitted
			controlField.el.dom.removeAttribute('name');
			if (controlField.hiddenField) {
				controlField.hiddenField.removeAttribute('name');
			}

			if (TCG.endsWith(this.controlConfig.xtype, 'combo') || TCG.endsWith(this.controlConfig.xtype, 'datefield')) {
				controlField.on('select', function(combo, record, index) {
					parent.addFieldValueToList(combo);
				}, this);
			}
			else {
				controlField.on('specialkey', function(field, e) {
					if (e.getKey() === e.ENTER || e.getKey() === e.TAB) {
						parent.addFieldValueToList(field);
					}
				}, this);
			}

			// register a hidden field to store the list of values
			this.hiddenName = this.hiddenName || this.name || Ext.id();
			const hiddenTag = {tag: 'input', type: 'hidden', value: '', name: this.hiddenName};
			this.hiddenField = this.el.createChild(hiddenTag);
			this.hiddenField.dom.disabled = this.hiddenName !== this.name;

			// prevent the hidden field from being submitted
			if (this.submitValue === false) {
				this.hiddenField.dom.removeAttribute('name');
			}
		}

		if (!this.defaultItemsApplied) {
			const listfield = this;
			this.defaultItems.forEach(function(item) {
				listfield.controlField.setValue(item);
				listfield.addFieldValueToList(listfield.controlField);
			});
			this.defaultItemsApplied = true;
		}
	},

	addFieldValueToList: function(field) {
		let value = field.getValue();

		// check if the dom value contains ';'
		const domValue = field.el && field.el.dom ? field.el.dom.value : undefined;

		if (((value === '') && domValue) || (domValue && domValue.includes(','))) {
			value = domValue;
		}

		if (TCG.isNotBlank(value)) {
			this.setValue(field.getValue());
			field.setRawValue('');
			field.focus(true);
		}

		this.validate();
	},

	addListItem: function(value, selectedRecord, record) {
		// Normalize value to formatted string
		let normalizedValue = value;
		if (this.controlField.xtype === 'datefield') {
			normalizedValue = Ext.util.Format.date(normalizedValue, 'm/d/Y');
		}
		normalizedValue = `${normalizedValue}`;

		// Check for presence and add value to list if necessary
		if (this.allowDuplicates || !this.selectedValues.includes(normalizedValue)) {
			const displayField = this.getItemDisplayField(this.controlField, selectedRecord, record);
			if (displayField) {
				this.selectedValues.push(normalizedValue);
				this.listItems.push(displayField);
				this.fs.add(displayField);
				this.fs.doLayout();

				this.hiddenField.dom.value = this.selectedValues.join('::');

				this.fireEvent('change', this);
			}
		}
	},

	getItemDisplayField: function(field, selectedRecord, record) {
		let displayValue;
		let selectedValue;
		let detailPageClass = undefined;
		let isLink = false;

		if (TCG.endsWith(field.xtype, 'combo')) {
			if (!selectedRecord) {
				selectedRecord = field.findRecord(field.valueField, field.getValue()).data;
			}
			// get the id for the link field
			selectedValue = TCG.getValue(field.valueField ? field.valueField : field.displayField, selectedRecord);
			displayValue = TCG.getValue(field.displayField, selectedRecord);
			detailPageClass = this.getDetailPageClassForItem(selectedRecord);
			isLink = detailPageClass ? true : false;
		}
		else {
			displayValue = (selectedRecord) ? selectedRecord.label : field.getValue();
			selectedValue = displayValue;
		}

		const fieldClass = isLink ? 'x-form-list-link-field' : '.x-form-list-display-field';
		const parentField = this;
		const controlField = this.controlField;
		const w = this.getParentForm().getWindow();
		const originalFieldHeight = this.originalFieldHeight;

		if (field.xtype === 'datefield') {
			displayValue = selectedValue = Ext.util.Format.date(displayValue, 'm/d/Y');
		}

		return new Ext.form.DisplayField({
			defaultAutoCreate: {tag: 'div'},
			submitValue: false,
			fieldClass: fieldClass,
			value: displayValue,
			selectedValue: selectedValue,
			isLink: isLink,
			record: record,
			height: originalFieldHeight,
			detailPageClass: detailPageClass,
			createDetailClass: function(newInstance) {
				const className = this.detailPageClass;
				let id = this.selectedValue;
				if (id === '' || newInstance) {
					id = undefined;
				}
				const params = id ? {id: id} : undefined;
				const cmpId = TCG.getComponentId(className, id);
				TCG.createComponent(className, {
					modal: TCG.isNull(id),
					id: cmpId,
					params: params,
					openerCt: this, // for modal new item window will call reload() on close
					defaultIconCls: w.iconCls
				});
			},
			afterRender: function() {
				Ext.form.DisplayField.prototype.afterRender.apply(this, arguments);
				this.getEl().dom.innerHTML = '';

				const dispEl = this.el.createChild({tag: 'div', style: 'float: left; padding-top: 4px'});
				dispEl.dom.innerText = displayValue;

				// create a div used to float the delete button on the right
				this.deleteBtnEl = this.el.createChild({tag: 'div', style: 'float: right; display: none;'});
				this.downBtnEl = this.el.createChild({tag: 'div', style: 'float: right; display: none;'});
				this.upBtnEl = this.el.createChild({tag: 'div', style: 'float: right; display: none;'});

				this.getEl().dom.style.height = '22px';

				this.deleteButton = new Ext.Button({
					iconCls: parentField.deleteClsClass,
					renderTo: this.deleteBtnEl
				});

				this.downButton = new Ext.Button({
					iconCls: 'arrow-down-green',
					renderTo: this.downBtnEl
				});

				this.upButton = new Ext.Button({
					iconCls: 'arrow-up-green',
					renderTo: this.upBtnEl
				});


				// monitor the click event
				this.mon(this.deleteBtnEl, 'click', function() {
					// call the remove field
					parentField.removeListItem(this);
					controlField.focus(true);
				}, this, {preventDefault: true});

				this.mon(this.upBtnEl, 'click', function() {
					parentField.moveListItemUp(this);
					controlField.focus(true);
				}, this, {preventDefault: true});

				this.mon(this.downBtnEl, 'click', function() {
					parentField.moveListItemDown(this);
					controlField.focus(true);
				}, this, {preventDefault: true});

				this.mon(this.el, 'click', function() {
					if (arguments[1].nodeName !== 'BUTTON' && this.isLink) {
						// show the detail page
						this.createDetailClass.apply(this, [false]);
					}
				}, this, {preventDefault: true});

				// monitor mouse over and out to show delete button
				this.mon(this.el, 'mouseover', this.showButtons, this, {preventDefault: true});
				this.mon(this.el, 'mouseout', this.hideButtons, this, {preventDefault: true});
			},
			showButtons: function() {
				this.upBtnEl.dom.style.display = '';
				this.downBtnEl.dom.style.display = '';
				this.deleteBtnEl.dom.style.display = '';
			},
			hideButtons: function() {
				this.upBtnEl.dom.style.display = 'none';
				this.downBtnEl.dom.style.display = 'none';
				this.deleteBtnEl.dom.style.display = 'none';
			}
		});
	},

	afterRender: function() {
		TCG.form.ListField.superclass.afterRender.apply(this, arguments);
		const listControl = this;
		const el = this.controlField.getEl();
		el.dom.setAttribute('ext:qtip', 'Right click to display the context menu with additional features.');
		el.on('contextmenu', function(e, target) {
			e.preventDefault();
			if (!this.drillDownMenu) {
				this.drillDownMenu = new Ext.menu.Menu({
					items: [{
						text: 'Clear', iconCls: 'clear', handler: function() {
							const listItemValues = [];
							// copy the list item values so we are not looping through the array while removing items from it
							const listItems = listControl.getListItems();
							for (let i = 0; i < listItems.length; i++) {
								listItemValues.push(listItems[i].selectedValue);
							}
							// remove the items
							for (let i = 0; i < listItemValues.length; i++) {
								listControl.removeListItemByValue(listItemValues[i]);
							}
						}, scope: this
					}, {
						text: 'Add New Item', iconCls: 'add', handler: function() {
							listControl.createDetailClass(true);
						}, scope: this
					}]
				});
			}
			if (this.disableAddNewItem || !this.detailPageClass) {
				this.drillDownMenu.items.get(1).disable();
			}
			this.drillDownMenu.showAt(e.getXY());
		}, this);
	},

	createDetailClass: function(newInstance) {
		const fp = TCG.getParentFormPanel(this);
		const w = fp.getWindow();
		const className = this.detailPageClass;
		let id = this.getValue();
		if (id === '' || newInstance) {
			id = undefined;
		}
		const params = this.getDetailPageParams(id);
		const defaultData = this.getDefaultData ? this.getDefaultData(fp.getForm()) : {};
		const cmpId = TCG.getComponentId(className, id);
		TCG.createComponent(className, {
			modal: TCG.isNull(id),
			id: cmpId,
			defaultData: defaultData,
			params: params,
			openerCt: this, // for modal new item window will call reload() on close
			defaultIconCls: w.iconCls
		});
	},

	onResize: function() {
		if (this.rendered) {
			this.fs.doLayout();
		}
	},

	doLayout: function() {
		if (this.rendered) {
			this.fs.doLayout();
		}
	},

	removeListItem: function(item) {
		const value = `${item.selectedValue}`;
		this.selectedValues.remove(value);
		this.listItems.remove(item);
		this.fs.remove(item);
		this.fs.doLayout();
		this.hiddenField.dom.value = this.selectedValues.join('::');
		this.fireEvent('change', this);

		// Clear value on empty; required for external on-clear triggers, such as mutually-excluded/required fields
		if (this.selectedValues.length === 0) {
			this.clearValue();
		}
	},

	moveListItemUp: function(item) {
		const value = `${item.selectedValue}`;
		const idx = this.selectedValues.indexOf(value);
		if (idx > 0) {
			this.selectedValues.remove(value);
			this.selectedValues.splice(idx - 1, 0, value);

			const listItem = this.listItems.find(i => i.selectedValue === item.selectedValue);

			this.listItems.remove(listItem);
			this.listItems.splice(idx - 1, 0, listItem);

			const fsIdx = this.fs.items.indexOf(item);

			this.fs.remove(item);
			this.fs.insert(fsIdx - 1, new item.constructor(item.cloneConfig()));
			this.fs.doLayout();
			this.hiddenField.dom.value = this.selectedValues.join('::');
			this.fireEvent('change', this);

			// Clear value on empty; required for external on-clear triggers, such as mutually-excluded/required fields
			if (this.selectedValues.length === 0) {
				this.clearValue();
			}

		}
	},

	moveListItemDown: function(item) {
		const value = `${item.selectedValue}`;
		const idx = this.selectedValues.indexOf(value);
		if (idx < this.selectedValues.length - 1) {
			this.selectedValues.remove(value);
			this.selectedValues.splice(idx + 1, 0, value);

			this.listItems.remove(item);
			this.listItems.splice(idx + 1, 0, item);

			const fsIdx = this.fs.items.indexOf(item);
			this.fs.remove(item);
			this.fs.insert(fsIdx + 1, new item.constructor(item.cloneConfig()));
			this.fs.doLayout();
			this.hiddenField.dom.value = this.selectedValues.join('::');
			this.fireEvent('change', this);

			// Clear value on empty; required for external on-clear triggers, such as mutually-excluded/required fields
			if (this.selectedValues.length === 0) {
				this.clearValue();
			}

		}
	},

	removeListItemByValue: function(value) {
		const listItems = this.getListItems();
		for (let i = 0; i < listItems.length; i++) {
			if (listItems[i].selectedValue === value) {
				this.removeListItem(listItems[i]);
				break;
			}
		}
	},

	getParentForm: function() {
		const parent = TCG.getParentFormPanel(this);
		return parent || this.ownerCt;
	},

	getValue: function(valueField) {
		return this.hiddenField.dom.value;
	},

	getText: function() {
		return (this.listItems).map(function(obj) {
			return obj.value;
		}).join('::');
	},

	getListItems: function() {
		return this.listItems;
	},

	setValue: function(values) {
		if (Array.isArray(values)) {
			// Assign a list of values
			for (let i = 0; i < values.length; i++) {
				const record = values[i];
				const value = typeof record !== 'object' ? record : TCG.getValue(this.controlField.valueField || 'id', record);
				this.addListItem(value, record);
			}
		}
		else if (values) {
			// Assign an individual value
			this.addListItem(values);
		}
		else {
			// Clear all values
			for (let i = 0; i < this.listItems.length; i++) {
				this.removeListItem(this.listItems[i]);
			}
		}
	},

	getDetailPageClassForItem: function(record) {
		return this.detailPageClass;
	},

	getDetailPageParams: function(id) {
		return id ? {id: id} : void 0;
	},

	getErrors: function(value) {
		if (this.getListItems().length < 1) { // if it has no value
			if (this.allowBlank !== true) {
				return ['This field is required.'];
			}
		}
		if (this.getListItems().length < this.minSelections) {
			return ['Not enough items have been selected.'];
		}
		if (this.getListItems().length > this.maxSelections) {
			return ['Too many items have been selected.'];
		}
		return [];
	},

	// inherit docs
	disable: function() {
		this.disabled = true;
		this.hiddenField.dom.disabled = true;
		this.fs.disable();
	},

	// inherit docs
	enable: function() {
		this.disabled = false;
		this.hiddenField.dom.disabled = false;
		this.fs.enable();
	},

	clearValue: function() {
		this.setValue();
		if (this.controlField.mode === 'remote') {
			this.controlField.store.removeAll();
		}
		this.controlField.lastQuery = null;
	},

	// inherit docs
	destroy: function() {
		for (let i = 0; i < this.listItems.lenth; i++) {
			Ext.destroy(this.listItems[i]);
		}
		Ext.destroy(this.fs, this.controlField);
		TCG.form.ListField.superclass.destroy.call(this);
	}
});

Ext.reg('listfield', TCG.form.ListField);

TCG.form.ExcelExportButton = Ext.extend(Ext.SplitButton, {
	exportMethod: function(format) {
		// must be overwritten
	},
	xtype: 'splitbutton',
	text: 'Excel (*.xlsx)',
	iconCls: 'excel_open_xml',
	handler: function(b) {
		if (!b.hasVisibleMenu()) {
			b.exportMethod('xlsx');
		}
	},
	menu: {
		xtype: 'menu',
		items: [{
			text: 'Excel (97-2003)',
			iconCls: 'excel',
			handler: function(b) {
				b.parentMenu.ownerCt.exportMethod('xls');
			}
		}]
	}
});
Ext.reg('excel_splitbutton', TCG.form.ExcelExportButton);

TCG.form.CSVExportButton = Ext.extend(Ext.SplitButton, {
	exportMethod: function(format) {
		// must be overwritten
	},
	text: 'CSV',
	iconCls: 'csv',
	handler: function(b) {
		if (!b.hasVisibleMenu()) {
			b.exportMethod('csv');
		}
	},
	menu: {
		xtype: 'menu',
		items: [{
			text: 'CSV Minimal',
			iconCls: 'csv',
			handler: function(b) {
				b.parentMenu.ownerCt.exportMethod('csv_min');
			}
		},
			{
				text: 'CSV Pipe',
				iconCls: 'csv',
				handler: function(b) {
					b.parentMenu.ownerCt.exportMethod('csv_pipe');
				}
			}
		]
	}
});
Ext.reg('csv_splitbutton', TCG.form.CSVExportButton);

TCG.form.JsonMigrationFormPanel = Ext.extend(TCG.form.FormPanel, {
	jsonEditor: undefined,
	autoScroll: true,
	loadValidation: false,
	requestedMaxDepth: 5,
	jsonMigrationUrl: 'jsonMigrationAction.json',
	listeners: {
		afterrender: function(fp) {
			const action = TCG.data.getData(TCG.trimWhitespaceAndNewlines(`
					${this.jsonMigrationUrl}
					?entityFkFieldId=${this.getEntityId()}
					&entityTableName=${this.getTableName()}
					&enableOpenSessionInView=true
					&requestedPropertiesToExcludeGlobally=rv|createDate|updateDate|updateUserId|createUserId|id|identity
				`), this);
			const options = {};
			const div = document.createElement('div');
			div.setAttribute('style', 'height: 100%; overflow-y:auto;');
			fp.body.appendChild(div);
			this.jsonEditor = new JSONEditor(div, options, action);
			fp.doLayout();
		}
	},
	getMigrationAction: function(fp) {
		const action = fp.jsonEditor.get();
		let jsonMigrationSyntax = '';
		if (action && action.bean && action.method && action.argumentList) {
			jsonMigrationSyntax = '<action bean="' + action.bean + '" method="' + action.method + '">\n';
			action.argumentList.forEach(function(argument) {
				jsonMigrationSyntax += '\t<arg><![CDATA[\n';
				jsonMigrationSyntax += JSON.stringify(argument.objectValue, null, '\t') + '\n';
				jsonMigrationSyntax += '\t]]></arg>\n';
			});
			jsonMigrationSyntax += '</action>';
		}
		else {
			jsonMigrationSyntax = 'No migration syntax could be produced.';
		}

		const textArea = document.createElement('textarea');
		textArea.setAttribute('style', 'position: fixed; top: 0; left: 0; width:2em; height:2em; padding:0; border:none; outline:none; boxShadow:none; background:transparent;');
		textArea.value = jsonMigrationSyntax;
		document.body.appendChild(textArea);
		textArea.select();

		try {
			document.execCommand('copy');
		}
		catch (err) {
			new TCG.app.CloseWindow({
				modal: true,
				allowOpenFromModal: true,
				height: 100,
				iconCls: 'workflow',
				title: 'Copy to Clipboard Failure',
				openerCt: fp,
				items: [{
					xtype: 'label', html: 'Copying to clipboard failed, or is not supported in your browser.'
				}]
			});
		}
		document.body.removeChild(textArea);
	},
	addToolbarButtons: function(toolBar) {
		const fp = this;

		toolBar.add({
			text: 'Copy Migration Action',
			tooltip: 'Copy XML action with internal JSON syntax to clipboard for use in a migration file.',
			iconCls: 'copy',
			scope: this,
			handler: function() {
				fp.getMigrationAction(this);
			}
		});
	}
});
Ext.reg('json-migration-form', TCG.form.JsonMigrationFormPanel);

/*
 * Override the mark function to prevent errors form elements are destroyed.
 *
 * A listener for the "alignErrorIcon" function is attached to the ownerCt upon form element creation. If the form element is destroyed (for dynamic form fields, for example) then
 * the default behavior does not include removal of its listeners which were attached to its ownerCt. This override forces removal of these listeners.
 */
Ext.form.MessageTargets.side.mark = TCG.concatFn(function(field, msg) {
	if (!field.errorIcon) {
		// Store original ownerCt for closure callbacks
		const ownerCt = field.ownerCt;
		field.on('destroy', function() {
			// If an ownerCt originally existed, remove registered listeners from it
			if (ownerCt) {
				ownerCt.un('afterlayout', field.alignErrorIcon, field);
				ownerCt.un('expand', field.alignErrorIcon, field);
			}
		}, field);
	}
}, Ext.form.MessageTargets.side.mark);

/**
 * A standard panel with a canvas child element for rendering a chart.
 * Refer to the chart.js documentation for custom options: https://www.chartjs.org/docs/latest/
 */
TCG.form.ChartPanel = Ext.extend(Ext.Panel, {
	html: '<canvas></canvas>',
	chartType: void 0, // REQUIRED, refer to chart.js reference: https://www.chartjs.org/docs/latest/charts/
	responsive: true, // resize with panel
	maintainAspectRatio: false, // fit chart in panel
	chartTitle: '', // convenience property; title can also be set in chartOptions
	/**
	 * Object containing chart options such as xAxes, yAxes, title, and other items.
	 * Refer to the chart.js documentation for options pertaining to specific chart types.
	 */
	chartOptions: {},
	listeners: {
		afterrender: function(panel) {
			panel.childCanvas = panel.getEl().dom.getElementsByTagName('CANVAS')[0];
			panel.createChart();
		}
	},
	createChart: function() {
		const panel = this;
		if (!panel.chart) {
			if (TCG.isBlank(panel.chartType)) {
				TCG.showError('A chart type is required. Possible types include: line, bar, radar, doughnut, pie, and scatter.', 'Chart Type Required');
				return;
			}
			const chartOptions = Ext.applyIf(panel.chartOptions || {}, {
				responsive: panel.responsive,
				maintainAspectRatio: panel.maintainAspectRatio
			});
			if (TCG.isNotBlank(panel.chartTitle) && TCG.isNull(chartOptions.title)) {
				chartOptions.title = {display: true, text: panel.chartTitle};
			}
			Promise.resolve(panel.getChartData())
				.then(function(data) {
					panel.chart = new Chart(panel.childCanvas, {
						type: panel.chartType,
						data: data,
						options: Ext.apply({
							onClick: function(e) {
								const activePoints = this.getElementsAtEvent(e);
								if (activePoints && activePoints.length > 0) {
									const selectedIndex = activePoints[0]._index;
									const datasetIndex = activePoints[0]._datasetIndex;
									const dataset = this.data.datasets[datasetIndex];
									panel.onClickEvent(dataset.data[selectedIndex], this.data.labels[selectedIndex], this.data.datasets[datasetIndex].label, dataset.fullData ? dataset.fullData[selectedIndex] : undefined);
								}
							}
						}, chartOptions)
					});
				});
		}
	},
	// when fullData is available, it will contain add data returned for clicked element: may contain additional fields that can be used for drill down
	onClickEvent: function(data, label, datasetLabel, fullData) {
		// to be overridden
	},
	getChartData: function() {
		return {};
	},
	reload: function() {
		this.updateChartData();
	},
	updateChartData: function() {
		const chart = this.chart;
		if (chart) {
			Promise.resolve(this.getChartData())
				.then(function(data) {
					chart.data = data;
					chart.update();
				});
		}
	}
});
Ext.reg('chartpanel', TCG.form.ChartPanel);

Chart.plugins.register({
	afterDraw: function(chart) {
		if (chart.data.datasets.length === 0) {
			// No data is present
			const ctx = chart.chart.ctx;
			const width = chart.chart.width;
			const height = chart.chart.height;
			chart.clear();

			ctx.save();
			ctx.textAlign = 'center';
			ctx.textBaseline = 'middle';
			ctx.font = '16pt Tahoma';
			ctx.fillText('No Data to Display', width / 2, height / 2);
			ctx.restore();
		}
	}
});

Chart.plugins.register({
	id: 'doughnuttotal',
	beforeDatasetDraw: function(chart, args, options) {
		if (options.calculateTotal) {
			let totalCount = 0;

			chart.data.datasets.forEach(function(ds) {
				ds.data.forEach(function(d) {
					totalCount += d;
				});
			});

			totalCount = totalCount.toLocaleString();   // format to include thousands separators
			const fontSize = (chart.height / ((chart.ctx.measureText(totalCount).width / chart.height) > 0.125 ? 120 : 100)).toFixed(2);
			chart.ctx.font = fontSize + 'em Tahoma';
			//chart.ctx.textBaseline = 'middle';

			const centerX = (chart.chartArea.left + chart.chartArea.right - chart.ctx.measureText(totalCount).width) / 2;
			const centerY = chart.height / 2;

			chart.ctx.fillText(totalCount, centerX, centerY);
		}
	}
});
