Ext.ns('TCG.grid');


TCG.grid.GroupSummary = Ext.extend(Ext.ux.grid.GroupSummary, {
	showGrandTotal: true,

	init: function(grid) {
		if (!this.cellTpl) { // allow select for copy/paste
			this.cellTpl = new Ext.Template(
				'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}">',
				'<div class="x-grid3-cell-inner x-grid3-col-{id}" unselectable="on">{value}</div>',
				'</td>'
			);
			this.cellTpl.disableFormats = true;
		}

		TCG.grid.GroupSummary.superclass.init.call(this, grid.grid || grid);

		if (this.showGrandTotal === true) {
			const grd = grid.grid || grid;
			const v = this.view = grd.getView();

			grd.addEvents({
				'grandtotalupdated': true // fired after grand total value is updated: (value, columnDefinition)
			});

			v.onLayout = this.onLayout;

			v.afterMethod('render', this.refreshGrandTotal, this);
			v.afterMethod('refresh', this.refreshGrandTotal, this);
			v.afterMethod('onUpdate', this.refreshGrandTotal, this);
			v.afterMethod('onRemove', this.refreshGrandTotal, this);

			v.afterMethod('syncScroll', this.syncSummaryScroll, this);


			if (!this.lastRowTpl) {
				this.lastRowTpl = new Ext.Template(
					'<div class="x-grid3-summary-row-total x-grid3-gridsummary-row-offset x-selectable">',
					'<table class="x-grid3-summary-table" border="0" cellspacing="0" cellpadding="0" style="{tstyle}">',
					'<tbody><tr>{cells}</tr></tbody>',
					'</table>',
					'</div>'
				);
				this.lastRowTpl.disableFormats = true;
			}
			this.lastRowTpl.compile();
		}
	},

	syncSummaryScroll: function() {
		const mb = this.view.scroller.dom;

		this.view.summaryWrap.dom.scrollLeft = mb.scrollLeft;
		this.view.summaryWrap.dom.scrollLeft = mb.scrollLeft; // second time for IE (1/2 time first fails, other browsers ignore)
	},

	doAllWidths: function(ws, tw) {
		if (!this.view.skipGroupHeaders) {
			TCG.grid.GroupSummary.superclass.doAllWidths.call(this, ws, tw);
		}
		if (this.showGrandTotal === true) {
			this.refreshGrandTotal();
		}
	},

	onLayout: function(vw, vh) {
		if (Ext.type(vh) !== 'number') { // handles grid's height:'auto' config
			return;
		}
		// note: this method is scoped to the GridView
		if (this.summary && !this.grid.getGridEl().hasClass('x-grid-hide-gridsummary')) {
			// readjust gridview's height only if grid summary row is visible
			this.scroller.setHeight(vh - this.summary.getHeight());
		}
	},

	renderSummary: function(o, cs, isGrandTotal) {
		cs = cs || this.view.getColumnData();
		const cfg = this.grid.getColumnModel().config,
			buf = [];
		let c;
		const p = {};
		let cf;
		const last = cs.length - 1;
		let i = 0;
		const len = cs.length;
		for (; i < len; i++) {
			c = cs[i];
			cf = cfg[i];
			p.id = c.id;
			p.style = c.style;
			p.css = i === 0 ? 'x-grid3-cell-first ' : (i === last ? 'x-grid3-cell-last ' : '');
			if (cf.summaryType || cf.summaryRenderer || cf.summaryCalculation) {
				if (((isGrandTotal === true) && (cf.hideGrandTotal === true)) || (!isGrandTotal && (cf.hideGroupTotal === true))) {
					p.value = '';
				}
				else {
					p.isGrandTotal = isGrandTotal;
					// Use ternary clause to retain function scopes
					p.value = cf.summaryRenderer
						? cf.summaryRenderer(o.data[c.name], p, o, cf)
						: c.renderer(o.data[c.name], p, o, cf);
					if (p.value === 0 && cf.useNull === true) {
						p.value = '';
					}
				}
			}
			else {
				p.value = '';
			}
			if (isGrandTotal === true) {
				// This event appears to fire correctly for formgrids, however for grid panels, need to fire the event on the ownerGridPanel
				this.grid.fireEvent('grandtotalupdated', o.data[c.name], cf);
				if (this.grid.ownerGridPanel) {
					this.grid.ownerGridPanel.fireEvent('grandtotalupdated', o.data[c.name], cf);
				}
			}
			if (TCG.isBlank(p.value)) {
				p.value = '&#160;';
			}
			buf[buf.length] = this.cellTpl.apply(p);
		}

		// if a template is passed use it other wise use the default
		const tpl = isGrandTotal === true ? this.lastRowTpl : this.rowTpl;

		return tpl.apply({
			tstyle: 'width:' + this.view.getTotalWidth() + ';',
			cells: buf.join('')
		});
	},

	calculate: function(rs, cs, grandTotal) {
		const data = {};
		let r, c;
		const cfg = this.grid.getColumnModel().config;
		let cf;
		let j = 0;
		const jlen = rs.length;
		for (; j < jlen; j++) {
			r = rs[j];
			let i = 0;
			const len = cs.length;
			for (; i < len; i++) {
				c = cs[i];
				cf = cfg[i];
				if (cf.summaryType || cf.summaryCalculation) {
					if (!grandTotal && cf.summaryCondition && !cf.summaryCondition(r.json)) {
						// skip this row if condition is false
						continue;
					}
					if (grandTotal && cf.summaryTotalCondition && !cf.summaryTotalCondition(r.json, cf)) {
						// skip this row if condition is false
						continue;
					}
					data[c.name] = (cf.summaryCalculation || Ext.ux.grid.GroupSummary.Calculations[cf.summaryType])(data[c.name] || 0, r, c.name, data, cf, this.grid, grandTotal, j, jlen);
				}
			}
		}
		return data;
	},

	refreshGrandTotal: function() {
		const store = this.grid.getStore();
		if (TCG.isNull(store)) {
			return;
		} // not initialized
		const rs = store.getRange();
		const cs = this.view.getColumnData(),
			data = this.calculate(rs, cs, true),
			markup = this.renderSummary({data: data}, cs, true);


		if (!this.view.summaryWrap) {
			this.view.summaryWrap = Ext.DomHelper.insertAfter(this.view.scroller, {
				tag: 'div',
				cls: 'x-grid3-gridsummary-row-inner'
			}, true);
		}

		this.view.summary = this.view.summaryWrap.update(markup).first();
	}
});
Ext.preg('groupsummary', TCG.grid.GroupSummary);
Ext.preg('gridsummary', TCG.grid.GroupSummary);


/*
 * all Calculation methods are called on each Record in the Store
 * with the following 5 parameters:
 *
 * v - cell value
 * record - reference to the current Record
 * colName - column name (i.e. the ColumnModel's dataIndex)
 * data - the cumulative data for the current column + summaryType up to the current Record
 * col - current column
 */
Ext.ux.grid.GroupSummary.Calculations.sum = function(v, record, field, data, col) {
	let d = record.data[field] || 0;
	if (typeof d == 'string') {
		d = parseFloat(d);
	}
	return v + d;
};

Ext.ux.grid.GroupSummary.Calculations.oneFalseAllFalse = function(v, record, field, data) {
	v = record.data[field];
	if ((data[field + 'oneFalseAllFalse'] === undefined) || !v) {
		data[field + 'oneFalseAllFalse'] = v;
	}
	return data[field + 'oneFalseAllFalse'];
};

Ext.ux.grid.GroupSummary.Calculations.percentChange = function(v, record, field, data, column) {
	const previousValue = record.json[column.previousValueField] || 0;
	const currentValue = record.json[column.currentValueField] || 0;

	if (data[field + 'percentChangeDenominator'] === undefined) {
		data[field + 'percentChangeDenominator'] = 0;
	}
	if (data[field + 'percentChangeNumerator'] === undefined) {
		data[field + 'percentChangeNumerator'] = 0;
	}
	if (data[field + 'percentChange'] === undefined) {
		data[field + 'percentChange'] = 0;
	}

	data[field + 'percentChangeDenominator'] += previousValue;
	data[field + 'percentChangeNumerator'] += (currentValue - previousValue) * 100;
	const denom = data[field + 'percentChangeDenominator'];
	if (denom === 0) {
		data[field + 'percentChange'] = 100;
	}
	else {
		data[field + 'percentChange'] = data[field + 'percentChangeNumerator'] / denom;
	}

	return data[field + 'percentChange'];
};

/*
 * Works similar to Excel Function FVSCHEDULE(1, values) - 1
 * Actual Calculation is ((1 + Value1) * (1 + Value2) * ... (1 + ValueN)) - 1
 *
 * This summary calculator will not subtract 1 on the final row calculation. We will want to
 *   enhance calculators to know what row they are out of the total set.
 */
Ext.ux.grid.GroupSummary.Calculations.totalPercentChange = function(v, record, field, data, column) {
	let d = record.data[field] || 0;
	if (typeof d == 'string') {
		d = parseFloat(d);
	}

	if (data[field + 'totalPercentChange'] === undefined) {
		data[field + 'totalPercentChange'] = 1;
	}

	data[field + 'totalPercentChange'] = data[field + 'totalPercentChange'] * (1 + (d / 100));

	return data[field + 'totalPercentChange'];
};

Ext.ux.grid.GroupSummary.Calculations.percent = function(v, record, field, data, column) {
	const denominatorValue = record.json[column.denominatorValueField] || 0;
	const numeratorValue = record.json[column.numeratorValueField] || 0;

	if (data[field + 'percentDenominator'] === undefined) {
		data[field + 'percentDenominator'] = 0;
	}
	if (data[field + 'percentNumerator'] === undefined) {
		data[field + 'percentNumerator'] = 0;
	}
	if (data[field + 'percent'] === undefined) {
		data[field + 'percent'] = 0;
	}

	data[field + 'percentDenominator'] += denominatorValue;
	data[field + 'percentNumerator'] += numeratorValue * 100;
	data[field + 'percent'] = data[field + 'percentNumerator'] / data[field + 'percentDenominator'];

	return data[field + 'percent'];
};
