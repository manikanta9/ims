Ext.ns('TCG.file');

////////////////////////////////////////////////////////////////////////////////////
////////////                  File Drag and Drop                        ////////////
////////////////////////////////////////////////////////////////////////////////////


TCG.file.DragAndDropContainer = Ext.extend(Ext.Container, {
	layout: 'hbox',
	autoEl: 'div',
	iconCls: 'attach',
	cls: 'drag-drop-msg',
	message: 'Drag and Drop Files Here',
	tooltipMessage: undefined,
	saveUrl: 'REPLACE_ME', // unless using popup window
	popupComponentName: undefined, // TCG.file.DragAndDropPopupWindow
	allowMultiple: false,
	maxFiles: undefined,
	enableDDForParentComponent: true, // Defaults to the parent component - i.e. the form panel, if false will enable drag and drop for the message box only
	parentDDComponentName: undefined, // If true checks for parent with given name, instead of just the parent by class of Ext.Panel
	bindToFormLoad: false, // By default will automatically enable DD after rendering, but if true, will get parent form panel and enable after load)

	initComponent: function() {
		const cmp = this;
		this.items = [{xtype: 'label', html: '<div style="width: 16px; height: 16px; float: left;" class="' + cmp.iconCls + '"' + (cmp.tooltipMessage ? ' ext:qtip="' + cmp.tooltipMessage + '"' : '') + '>&nbsp</div>'},
			{xtype: 'label', html: cmp.message}
		];
		Ext.Container.prototype.initComponent.call(this, arguments);
	},

	enableDD: function(fp) {
		const cmp = this;
		// Set the drag and drop element as the container panel if specified, otherwise for just this hbox element
		let ddObj = cmp;
		if (cmp.enableDDForParentComponent === true) {
			if (cmp.parentDDComponentName) {
				ddObj = cmp.findParentBy(function(p) {
					return (p && p.name === cmp.parentDDComponentName);
				});
				if (TCG.isNull(ddObj)) {
					ddObj = fp;
				}
			}
			else {
				ddObj = TCG.getParentByClass(cmp, Ext.Panel);
			}
		}
		TCG.file.enableDD(ddObj, cmp.getParams(fp), cmp.getReloadObject(), cmp.saveUrl, cmp.allowMultiple, cmp.maxFiles, cmp.popupComponentName, cmp.popupComponentConfig);
	},

	listeners: {
		afterrender: function() {
			const cmp = this;
			if (cmp.bindToFormLoad === false) {
				cmp.enableDD();
			}
			else {
				let fp = TCG.getParentFormPanel(cmp);
				if (!fp) {
					const p = TCG.getParentByClass(cmp, Ext.Panel);
					fp = p.getWindow().getMainFormPanel();
				}
				const afterloadHandler = function(record) {
					if (record) {
						cmp.enableDD(fp);
					}
				};
				fp.on('afterload', afterloadHandler, cmp);
				cmp.on('beforedestroy', function() {
					fp.un('afterload', afterloadHandler, cmp);
				}, cmp);
			}
		}
	},

	// Parameters to pass for saving the file
	getParams: function(formPanel) {
		// OVERRIDE ME
		return null;
	},

	// Object to reload - i.e. gridPanel to reload after attaching new file
	getReloadObject: function() {
		// OVERRIDE ME
		return null;
	}
});
Ext.reg('drag-drop-container', TCG.file.DragAndDropContainer);


TCG.file.DragAndDropPopupWindow = Ext.extend(TCG.app.OKCancelWindow, {
	title: 'Document Upload Window',
	iconCls: 'attach',
	height: 250,
	width: 400,

	okButtonText: 'Upload File',
	okButtonTooltip: 'Upload File',

	saveUrl: 'REPLACE_ME',
	formItems: [],

	reload: function() {
		const win = this;
		win.closeWindow();
		if (win.openerCt && win.openerCt.reload) {
			win.openerCt.reload();
		}
	},

	saveCallBackFunction: undefined,

	saveForm: function(closeOnSuccess, forms, form, panel, params) {
		const win = this;
		const files = win.ddFiles;
		TCG.file.enableDD.upload(files, 0, panel.getEl(), panel.getFormValuesFormatted(), win, this.saveUrl, this, this.saveCallBackFunction);
	},

	items: [{
		xtype: 'formpanel',
		instructions: 'Upload your document with the following additional properties',
		getInstructions: function() {
			if (this.getWindow().instructions) {
				return this.getWindow().instructions;
			}
			return this.instructions;
		},
		fileUpload: true,
		getSaveURL: function() {
			return this.getWindow().saveUrl;
		},
		initComponent: function() {
			const win = this.getWindow();
			const currentItems = [];
			Ext.each(win.formItems, function(f) {
				currentItems.push(f);
			});
			this.items = currentItems;
			TCG.form.FormPanel.superclass.initComponent.apply(this, arguments);
		},
		getDefaultData: function(win) {
			if (win.getDefaultData) {
				return win.getDefaultData(win, this);
			}
			return win.defaultData;
		}
	}]
});


TCG.file.enableDD = function(ddObj, params, reloadObj, loadUrl, allowMultiple, maxFiles, popupComponentName, popupComponentConfig) {
	const dropCallback = (event, renderedObject) => {
		const el = ddObj.getEl();
		if (!maxFiles) {
			maxFiles = 5;
		}
		const files = event.dataTransfer.files;
		if (files.length === 0) {
			TCG.showError('You can only drag and drop files from your local computer.', 'File Not Found');
		}
		else if ((files.length === 1) || ((allowMultiple === true) && (files.length <= maxFiles))) {
			if (popupComponentName) {
				TCG.file.enableDD.uploadWithPopupWindow(popupComponentName, popupComponentConfig, el, params, reloadObj, files);
			}
			else {
				const fileCount = 0;
				TCG.file.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl, ddObj);
			}
		}
		else if (allowMultiple === true) {
			TCG.showError('You can only drag and drop up to [' + maxFiles + '] files at a time.', 'Max Files Exceeded');
		}
		else if (allowMultiple === false) {
			TCG.showError('You can only drag and drop one file at a time.', 'Multiple Files Not Supported');
		}
	};
	TCG.enableDragAndDrop(ddObj, dropCallback);
};


TCG.file.enableDD.uploadWithPopupWindow = function(componentName, componentConfig, el, params, reloadObj, files) {
	if (TCG.isNull(componentConfig)) {
		componentConfig = {};
	}

	componentConfig = Ext.apply({
		modal: true,
		defaultData: params,
		ddFiles: files,
		openerCt: reloadObj
	}, componentConfig);

	TCG.createComponent(componentName, componentConfig);
	el.unmask();
};

TCG.file.enableDD.upload = function(files, fileCount, el, params, reloadObj, loadUrl, componentScope, callbackFn) {
	const file = files[fileCount];
	const form = new FormData();
	form.append('file', file);
	for (const p in params) {
		if (params[p] || params[p] === false) {
			form.append(p, params[p]);
		}
	}
	const xhr = (window.XMLHttpRequest) ? new XMLHttpRequest() : new ActiveXObject('Microsoft.XMLHTTP');
	el.mask('Uploading ' + file.name);
	xhr.addEventListener('load', function(obj) {
		el.unmask();
		const json = obj.currentTarget.responseText;
		const o = Ext.decode(json);
		if (o && o.success === false) {
			TCG.showError('Upload Error: ' + o.message, 'Upload Error');
		}
		else {
			fileCount++;
			if (fileCount === files.length) {
				if (callbackFn) {
					callbackFn.call(componentScope || this, o.data);
				}
				if (reloadObj && reloadObj.reload) {
					reloadObj.reload.defer(300, reloadObj);
				}
			}
			else {
				TCG.file.enableDD.upload(files, fileCount, el, params, reloadObj, loadUrl, componentScope);
			}
		}
	}, false);
	xhr.addEventListener('error', function(obj) {
		el.unmask();
		TCG.showError('Upload Failed for: ' + file.name, 'Upload Error');
	}, false);
	xhr.open('POST', TCG.getApplicationUrl(loadUrl, componentScope));
	xhr.send(form);
};

TCG.file.disableDD = function(obj) {
	const el = obj.getEl();
	el.dom.ondragover = null;
	el.dom.ondragleave = null;
	el.dom.ondrop = null;
};
