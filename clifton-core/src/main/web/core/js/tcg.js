Ext.ns('TCG');

TCG.ExternalApplicationNames = TCG.ExternalApplicationNames || [];
TCG.ExternalApplicationRootUri = TCG.ExternalApplicationRootUri || '';
TCG.ExtRootUrl = TCG.ExtRootUrl || 'extjs';
Ext.BLANK_IMAGE_URL = `${TCG.ExtRootUrl}/resources/images/default/s.gif`;

TCG.endsWith = function(searchStr, pattern) {
	if (TCG.isBlank(searchStr)) {
		return false;
	}
	return searchStr.indexOf(pattern, searchStr.length - pattern.length) !== -1;
};

TCG.startsWith = function(searchStr, pattern) {
	if (TCG.isBlank(searchStr)) {
		return false;
	}
	return searchStr.indexOf(pattern) === 0;
};

TCG.contains = function(searchStr, pattern) {
	if (TCG.isBlank(searchStr)) {
		return false;
	}
	return searchStr.indexOf(pattern) >= 0;
};

/**
 * Gets the list of distinct items in the array. If a <code>keyGeneratorFn</code> is included then it will be used to distinguish elements by key.
 *
 * If no <code>keyGeneratorFn</code> is given, then this is implemented using a set. As a result, there is no guarantee on the order of elements in the resulting list. If the original order must be maintained then a sufficient <code>keyGeneratorFn</code> is required.
 *
 * Key-based distinctions will always use the <i>first</i> discovered item for any given key in the resulting list.
 *
 * @param [keyGeneratorFn] the key-generator function for distinguishing elements
 * @return {any[]} the list of distinct items from the original list
 */
Array.prototype.distinct = function(keyGeneratorFn) {
	let result;
	if (keyGeneratorFn) {
		// Use the key generator to distinguish elements
		const valueMap = {};
		result = [];
		for (const item of this) {
			const key = keyGeneratorFn(item);
			if (!valueMap.hasOwnProperty(key)) {
				valueMap[key] = true;
				result.push(item);
			}
		}
	}
	else {
		// Use set for rapid performance
		result = [...new Set(this)];
	}
	return result;
};

/**
 * Partitions the array into two arrays: one of all elements which satisfy the given predicate, and another of all elements which do not.
 *
 * The first element of the resulting tuple is the array of elements which <i>do</i> satisfy the given predicate. The second element is the array of elements which <i>do not</i> satisfy the given predicate.
 *
 * @param predicate the predicate to use to partition the array
 * @return {[[], []]} the tuple of partitioned items where the first element is the matched items and the second element is the unmatched items
 */
Array.prototype.partition = function(predicate) {
	const acceptedEls = [];
	const rejectedEls = [];
	for (const el of this) {
		const arrayForEl = predicate(el) ? acceptedEls : rejectedEls;
		arrayForEl.push(el);
	}
	return [acceptedEls, rejectedEls];
};

// let's try to override and see what happens
Ext.handleError = function(e) {
	alert('CLIENT-SIDE ERROR: ' + e);
	// throw e;
};

TCG.TableLayout = Ext.extend(Ext.layout.TableLayout, {
	getNextCell: function(c) {
		const result = TCG.TableLayout.superclass.getNextCell.apply(this, arguments);
		if (c.cellWidth) {
			result.width = c.cellWidth;
		}
		return result;
	}
});
Ext.Container.LAYOUTS['table'] = TCG.TableLayout;

// creates tooltips for menu item
Ext.menu.Menu.prototype.initComponent = Ext.menu.Menu.prototype.initComponent.createSequence(function() {
	const menu = this;
	if (menu.items) {
		menu.items.each(function(item) {
			if (typeof (item.tooltip) !== 'undefined') {
				item.on('afterrender', function(menuItem) {
					let tooltip = typeof (menuItem.tooltip) === 'object'
						? menuItem.tooltip
						: {text: menuItem.tooltip};
					tooltip = Ext.apply(tooltip, {target: menuItem.getEl().getAttribute('id')});
					Ext.QuickTips.register(tooltip);
				});
			}
		});
	}
});

// create a tooltip for items in a menu
Ext.menu.Item.prototype.initComponent = Ext.menu.Item.prototype.initComponent.createSequence(function() {
	const item = this;
	if (typeof (item.tooltip) !== 'undefined') {
		item.on('afterrender', function(menuItem) {
			let tooltip = typeof (menuItem.tooltip) === 'object'
				? menuItem.tooltip
				: {text: menuItem.tooltip};
			tooltip = Ext.apply(tooltip, {target: menuItem.getEl().getAttribute('id')});
			Ext.QuickTips.register(tooltip);
		});
	}
});

//Clicking on a menu item that is a menu itself should not navigate away but should still capture the click event
Ext.menu.BaseItem.prototype.initComponent = Ext.menu.BaseItem.prototype.initComponent.createSequence(function() {
	const item = this;
	if (item.menu) {
		item.hideOnClick = false;
	}
});

TCG.TabPanel = Ext.extend(Ext.TabPanel, {
	activeTab: 0,
	layoutOnTabChange: true,
	enableTabScroll: true,
	requiredFormIndex: -1, // if specified, disables all other tabs until this form is successfully loaded; can be an array
	defaults: {layout: 'fit'},
	border: false,
	reloadOnChange: false,
	doNotDisableAfterLoad: false, // if true, won't add afterload to enable tabs in case tab enable is done within the specific window

	listeners: {
		afterRender: function() {
			if (this.requiredFormIndex >= 0 || Ext.isArray(this.requiredFormIndex)) {
				this.renderAllRequiredTabs.defer(500, this); // need a delay to avoid Notification Definition activation
			}
		}
	},

	onRender: function(ct, position) {
		if (this.requiredFormIndex >= 0 || Ext.isArray(this.requiredFormIndex)) {
			const tabPanel = this;
			const arr = Ext.isArray(this.requiredFormIndex) ? this.requiredFormIndex : [this.requiredFormIndex];
			if (arr.length > 1) {
				this.deferredRender = false; // render all tabs so that validation can be activated everywhere
			}
			for (let i = 0; i < this.items.length; i++) {
				const tab = this.items.get(i);
				if (arr.indexOf(i) === -1) {
					tab.setDisabled(true);
				}
				else {
					// first child or grand parent
					let form = tab.items.get(0);
					if (!(form instanceof Ext.form.FormPanel)) {
						form = TCG.getParentByClass(this, Ext.form.FormPanel);
					}

					if (TCG.isFalse(this.doNotDisableAfterLoad)) {
						form.on('afterload', () => tabPanel.items.each(t => {
							if (t.disabled) {
								t.setDisabled(false);
							}
						}));
					}
				}
			}
		}

		this.on('tabchange', function(tabPanel, tab) {
			// skip the first reload (grid does it anyway)
			if (tab) {
				if (tab.reloadOnTabChange) {
					const items = tab.items;
					for (let i = 0; i < items.length; i++) {
						const item = items.get(i);
						if (item.reload) {
							item.reload();
						}
					}
				}
				else if (tabPanel.reloadOnChange) {
					tab.reloadOnTabChange = true;
				}
			}
		});
		TCG.TabPanel.superclass.onRender.apply(this, arguments);
	},

	// If a tab is required - make sure that it is rendered so all validation
	// and empty text values are validated and submitted if needed
	renderAllRequiredTabs: function() {
		const activeItem = this.getActiveTab();
		const arr = Ext.isArray(this.requiredFormIndex) ? this.requiredFormIndex : [this.requiredFormIndex];
		if (arr.length > 1) {
			for (let i = 0; i < this.items.length; i++) {
				const tab = this.items.get(i);
				if (arr.indexOf(i) !== -1 && !tab.rendered) {
					this.activate(tab);
				}
			}
		}
		this.activate(activeItem);
	},

	getWindow: function() {
		return this.ownerCt;
	},

	// when switching between tabs that share filters, one can save the value on 'beforetabchange' and restore it on 'tabchange'
	saveToolbarFilter: function(toolBar, filterName) {
		if (toolBar) {
			const o = TCG.getChildByName(toolBar, filterName);
			if (o && o.getValue()) {
				if (!this.savedToolbarFilters) {
					this.savedToolbarFilters = {};
				}
				this.savedToolbarFilters[filterName] = {
					value: o.getValue(),
					text: o.getRawValue()
				};
			}
			else if (this.savedToolbarFilters) {
				this.savedToolbarFilters[filterName] = null;
			}
		}
	},
	restoreToolbarFilter: function(toolBar, filterName) {
		if (toolBar && this.savedToolbarFilters) {
			const o = TCG.getChildByName(toolBar, filterName);
			const s = this.savedToolbarFilters[filterName];

			if (o) {
				if (s) {
					if (TCG.isNotEquals(o.getValue(), s.value)) {
						o.setValue(s);
						o.fireEvent('select', o, s);
					}
				}
				else {
					o.reset();
				}
			}
		}
	}
});
Ext.reg('tabpanel', TCG.TabPanel);

// similar to Ext.each but support items.get(index) method in addition to array[index]
TCG.each = function(array, fn, scope) {
	if (Ext.isEmpty(array, true)) {
		return;
	}
	if (!Ext.isIterable(array) || Ext.isPrimitive(array)) {
		array = [array];
	}
	let i = 0;
	const len = array.length;
	for (; i < len; i++) {
		const el = array.get ? array.get(i) : array[i];
		if (fn.call(scope || el, el, i, array) === false) {
			return i;
		}
	}
};

TCG.clone = function(obj) {
	const seenObjects = new Set();
	const originalToCloneMap = new Map();
	const f = function(simpleObject) {
		if (!seenObjects.has(simpleObject)) {
			seenObjects.add(simpleObject);
			switch (Ext.type(simpleObject)) {
				case 'object':
					const sourcePrototype = Object.getPrototypeOf(simpleObject);
					const newObject = Object.create(sourcePrototype);
					originalToCloneMap.set(simpleObject, newObject);
					for (const p in simpleObject) {
						if (simpleObject.hasOwnProperty(p)) {
							newObject[p] = f(simpleObject[p]);
						}
					}
					return newObject;
				case 'array':
					const newArray = [];
					originalToCloneMap.set(simpleObject, newArray);
					let i = 0;
					const len = simpleObject.length;
					for (; i < len; i++) {
						newArray.push(f(simpleObject[i]));
					}
					return newArray;
				default:
					originalToCloneMap.set(simpleObject, simpleObject);
					return simpleObject;
			}
		}
		return originalToCloneMap.get(simpleObject);
	};
	return f(obj);
};

// take array of arrays, clones the argument array and swaps inner array elements in the specified positions (0 based index)
// for example TCG.swapArrayOfArrayElements([[1,2],[3,4]], 0, 1) => [2,1],[4,3]]
TCG.swapArrayOfArrayElements = function(arr, index1, index2) {
	const result = TCG.clone(arr);
	for (let i = 0; i < result.length; i++) {
		const o = result[i];
		const tmp = o[index1];
		o[index1] = o[index2];
		o[index2] = tmp;
	}
	return result;
};

TCG.getText = function(o) {
	return o.textContent || o.innerText;
};
TCG.setText = function(o, value) {
	if (!o) {
		TCG.showError('Cannot set object\'s text. The object is not defined.', 'Client Side Error');
	}
	else if (typeof o.textContent !== 'undefined') {
		o.textContent = value;
	}
	else if (typeof o.innerText !== 'undefined') {
		o.innerText = value;
	}
	else {
		TCG.showError('Cannot set object\'s text. The object does not support this operation.', 'Client Side Error');
	}
};

// returns property value of the specified JSON object using the specified path (supports composite properties)
TCG.getValue = function(path, obj) {
	if (TCG.isNull(path) || typeof obj !== 'object') {
		return null;
	}
	const index = path.indexOf('.');
	let name = (index === -1) ? path : path.substring(0, index);
	let valueIndex = -1;
	// support arrays
	if (name.indexOf(']') > -1) {
		const nameEndIndex = name.indexOf('[');
		valueIndex = name.substring(nameEndIndex + 1, name.length - 1);
		name = name.substring(0, nameEndIndex);
	}
	for (const v in obj) {
		if (TCG.isEquals(v, name)) {
			let result = obj[v];
			if (result && (valueIndex > -1)) {
				result = result[valueIndex];
			}
			return (index === -1) ? result : TCG.getValue(path.substring(index + 1), result);
		}
	}
	return null;
};
// returns first array element that has the specified property with the specified value
TCG.getArrayElement = function(arr, prop, value) {
	if (arr && arr.length) {
		for (let i = 0; i < arr.length; i++) {
			const v = arr[i];
			if (TCG.isEquals(TCG.getValue(prop, v), value)) {
				return v;
			}
		}
	}
	return null;
};

/**
 * Removes all items in the given array that match the provided predicate.
 */
TCG.removeAll = function(arr, predicate) {
	const matchIndexList = [];
	for (let i = 0; i < arr.length; i++) {
		if (predicate(arr[i])) {
			matchIndexList.push(i);
		}
	}
	for (let i = matchIndexList.length - 1; i > -1; i--) {
		arr.splice(i, 1);
	}
};

TCG.showDifferences = function(oldValue, newValue) {
	const dmp = new diff_match_patch();
	const diffs = dmp.diff_main(oldValue, newValue);
	let html = dmp.diff_prettyHtml(diffs);
	//Used to remove a 'paragraph' character at every line break, and add non-breaking spaces for tabs.
	html = html.replace(/\t/g, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;').replace(/&para;/g, '');
	new TCG.app.Window({
		id: 'differenceWindow',
		title: 'Differences',
		iconCls: 'diff',
		width: 1000,
		height: 600,
		autoScroll: true,
		bodyStyle: 'padding: 10px; background-color: #FFFFFF;',
		items: [{
			xtype: 'label', html: html
		}]
	});
};

/**
 * Displays a message dialog with the given message and title. The returned promise resolves to the button selection result.
 */
TCG.showMessage = async function(message, title, config = {}) {
	return await new Promise(resolve => Ext.Msg.show({
		title: title || '',
		msg: message,
		minWidth: Ext.Msg.minWidth,
		buttons: Ext.Msg.OK,
		modal: true,
		...config,
		fn: config.fn ? (response => config.fn(response, resolve)) : resolve
	}));
};
TCG.showError = async (message, title, config = {}) => await TCG.showMessage(message, title || 'Error', {icon: Ext.MessageBox.ERROR, ...config});
TCG.showInfo = async (message, title, config = {}) => await TCG.showMessage(message, title || 'Info', {icon: Ext.MessageBox.INFO, ...config});
TCG.showConfirm = async (message, title, config = {}) => await TCG.showMessage(message, title || 'Action Needed', {icon: Ext.Msg.QUESTION, buttons: Ext.Msg.YESNO, ...config});

// Asynchronously imports the specified class if not loaded already (import is a reserved word in IE)
TCG.classRegistry = {};

/**
 * Initializes the given class using the given source. This evaluates the given source and validates that the given class was generated.
 *
 * @param {string} className the name of the class that shall be generated
 * @param {string} sourceLocation the location which shall be used to indicate the source URL
 * @param {string} source the source text
 * @return {Function} the initialized class, or <tt>null</tt> if class initialization failed
 */
TCG.initializeClass = function(className, sourceLocation, source) {
	const globalEval = eval;
	const globalScope = globalEval('this');
	let clazz = TCG.getValue(className, globalScope);
	let valueWithSourceUrl, absoluteUrl;

	// Guard-clause: Class already loaded
	if (clazz) {
		console.warn('The class was already loaded: ' + className + ' (' + sourceLocation + ')');
		return clazz;
	}

	try {
		// Load class
		absoluteUrl = window.location.href.substring(0, window.location.href.lastIndexOf('/')) + '/' + sourceLocation;
		valueWithSourceUrl = source + '//# sourceURL=' + absoluteUrl;
		globalEval(valueWithSourceUrl);

		// Verify loaded class
		clazz = TCG.getValue(className, globalScope);
		if (clazz) {
			TCG.registerClass(className, clazz);
		}
		else {
			// noinspection ExceptionCaughtLocallyJS: Allow local throw/catch for consistent error-handling
			throw new Error(`After evaluating the source file ([${sourceLocation}]), the class [${className}] was still not found in the global context. Please ensure that the source file is not malformed, that its location matches the class name, and that it can be executed without errors.`);
		}
	}
	catch (e) {
		const errorMsg = `Cannot load class: ${className}\n\n${e}`;
		console.error(errorMsg);
		alert(errorMsg);
	}
	return clazz;
};

/**
 * Gets the class by class name. If the class has not already been initialized, this will attempt to asynchronously load the class.
 *
 * @param {string} className the class to get
 * @return {Promise} a promise which shall be resolved with the retrieved class, if a class was retrieved successfully
 */
TCG.useAsynchronous = async function(className) {
	// Guard-clause: No classname given
	if (!className) {
		TCG.showError('No class name was found for the given operation.', 'Empty Class Name');
		return;
	}

	// Check for pre-loaded classes
	let clazz = TCG.classRegistry[className];
	if (!clazz) {
		const globalEval = eval;
		const globalScope = globalEval('this');
		clazz = TCG.getValue(className, globalScope);
		if (clazz) {
			TCG.registerClass(className, clazz);
		}
	}

	// Fall-back to class retrieval
	if (!clazz) {
		const url = TCG.classNameToURL(className);
		const source = await TCG.data.getDataValuePromise(url);
		clazz = TCG.initializeClass(className, url, source);
	}
	return clazz;
};

/**
 * Registers the given class in the class registry.
 *
 * @param {string} className the fully-qualified class name
 * @param {object} clazz the class object
 */
TCG.registerClass = function(className, clazz) {
	if (TCG.classRegistry[className]) {
		console.warn(`The class [${className}] has already been registered. Re-registration will be performed. This may cause issues for existing references to the orphaned type.`);
	}
	// Infer the application name from the class module
	const [, classModuleQualifier] = /^Clifton\.([^.]+)\..+/.exec(className) || [];
	const isExternalApplication = TCG.ExternalApplicationNames.includes(classModuleQualifier);
	const inferredApplicationName = (isExternalApplication && classModuleQualifier) || null;
	Ext.override(clazz, {
		applicationName: clazz?.prototype?.applicationName ?? inferredApplicationName ?? null
	});
	TCG.classRegistry[className] = clazz;
};

/**
 * Gets the class by class name. If the class has not already been initialized, this will attempt to synchronously load the class.
 *
 * <b>Note: Avoid using this synchronous method. This is here for backwards compatibility only.</b>
 *
 * @param {string} className the class to get
 * @return {Function} the retrieved class
 */
TCG.use = function(className) {
	const globalEval = eval;
	const globalScope = globalEval('this');
	let clazz;
	let url, source;

	// Check for pre-loaded classes
	clazz = TCG.classRegistry[className];
	if (!clazz) {
		clazz = TCG.getValue(className, globalScope);
		if (clazz) {
			TCG.registerClass(className, clazz);
		}
	}

	// Fall-back to class retrieval
	if (!clazz) {
		url = TCG.classNameToURL(className);
		source = TCG.getResponseText(url);
		clazz = TCG.initializeClass(className, url, source);
	}

	return clazz;
};

TCG.createComponent = async function(className, args) {
	// Resolve class
	const clazz = await TCG.useAsynchronous(className);
	if (!clazz) {
		const classNotFoundErr = `Class not found exception: ${className}`;
		alert(classNotFoundErr);
		throw new Error(classNotFoundErr);
	}

	// Instantiate class
	try {
		return new clazz(args);
	}
	catch (e) {
		const instantiationError = `Cannot instantiate "${className}": ${e}`;
		TCG.showError(instantiationError, 'TCG.createComponent');
		const loggedEx = new Error(instantiationError);
		loggedEx.stack += '\nCaused by:\n' + e.stack;
		throw loggedEx;
	}
};

/**
 * Gets the application name which should be targeted for the given intended request. This function infers the target application for the request using the given URI, application
 * name, and application scope, if provided.
 *
 * @param uri the request URI for which the proxied application URI should be generated
 * @param componentScope the component from whose hierarchy the application information should be obtained if not explicitly provided
 * @param applicationName the explicit application name used for this window, or <code>null</code> to infer the application name automatically
 * @param applicationScope the application name used for this application scope; typically, the parent window application name or scope or <code>null</code> if no parent application name is defined
 * @return {string} the application name inferred from the provided arguments, or <code>null</code> if no application name was inferred
 */
TCG.getApplicationName = function(uri, componentScope, applicationName, applicationScope) {
	const hasValidComponent = componentScope instanceof Ext.util.Observable;
	if (componentScope && !hasValidComponent) {
		console.warn('A invalid component scope was provided for application name inference. The provided object is not a suitable component.', componentScope);
	}
	const inferredApplicationName = applicationName || (hasValidComponent && componentScope.getApplicationName()) || null;
	const inferredApplicationScope = applicationScope || (hasValidComponent && componentScope.getApplicationScope()) || null;
	return inferredApplicationName || this.getApplicationNameForUrlOrBeanName(uri) || inferredApplicationScope || null;
};

/**
 * Gets the application URI which should be used for the given intended request. This function infers the target application for the request using the given URI, application name,
 * and application scope, if provided. If the target application is not the application which hosts the current page, then a URI which routes requests directly to the target
 * application will be generated and returned. Otherwise, the original provided URI will be returned.
 *
 * @param uri the request URI for which the proxied application URI should be generated
 * @param componentScope the component from whose hierarchy the application information should be obtained if not explicitly provided
 * @param applicationName the explicit application name used for this window, or <code>null</code> to infer the application name automatically
 * @param applicationScope the application name used for this application scope; typically, the parent window application name or scope or <code>null</code> if no parent application name is defined
 * @return {string} the URI which should be used for the given intended application request
 */
TCG.getApplicationUrl = function(uri, componentScope, applicationName, applicationScope) {
	// Note: This function could likely be easily memoized if profiling shows a large number of cycles spent here; however, this could cause dynamic application scopes to become more difficult
	const appliedApplicationName = TCG.getApplicationName(uri, componentScope, applicationName, applicationScope);
	const isExternalApplication = TCG.ExternalApplicationNames.includes(appliedApplicationName);
	let applicationUri;
	if (isExternalApplication) {
		const normalizedPrefix = TCG.ExternalApplicationRootUri
			? TCG.ExternalApplicationRootUri.replace(/\/+$/, '') + '/'
			: '';
		applicationUri = `${normalizedPrefix}${appliedApplicationName}/${uri}`;
	}
	else {
		applicationUri = uri;
	}
	return applicationUri;
};

TCG.getApplicationNameForUrlOrBeanName = function(urlOrBeanName) {
	const externalAppNames = TCG.ExternalApplicationNames;
	if (!TCG.isBlank(externalAppNames)) {
		//Split the url or beanName into words
		if (urlOrBeanName) {
			const words = urlOrBeanName.replace(/([a-z])([A-Z])/, '$1 $2').split(/\s/);
			if (!TCG.isBlank(words) && words.length > 0) {
				const word = words[0].toLowerCase();
				//If the first word in the url or beanName is one of the configured external apps then return.
				if (externalAppNames.includes(word)) {
					return word;
				}
			}
		}
	}
	return '';
};

/**
 * Retrieves the application name for the current {@link Ext.util.Observable} type. This typically applies only to {@link Ext.Component} types. However, since many component-like
 * types exist which extend {@link Ext.util.Observable}, this function is applied to the root prototype for compatibility, instead.
 *
 * The application name is the application directly assigned to any component and overrides any parent or implicit application scope.
 *
 * @see Ext.util.Observable#getApplicationScope
 */
Ext.util.Observable.prototype.getApplicationName = function() {
	return this.applicationName;
};

/**
 * Retrieves the application scope for the current {@link Ext.util.Observable} type. This typically applies only to {@link Ext.Component} types. However, since many component-like
 * types exist which extend {@link Ext.util.Observable}, this function is applied to the root prototype for compatibility, instead.
 *
 * The application scope is typically the inherited application name which will be used as the base application for all child component requests. The application scope will be used
 * in the event that an explicit application name is not defined and an application name cannot be inferred from the request URI.
 *
 * @see Ext.util.Observable#getApplicationName
 */
Ext.util.Observable.prototype.getApplicationScope = function() {
	// Traverse ownerCt and openerCt hierarchy until a suitable application name is found
	return this.getApplicationName()
		|| this.openerCt?.getApplicationScope?.()
		|| this.ownerCt?.getApplicationScope?.();
};

TCG.getResponseText = function(url, componentScope) {
	const requestUrl = TCG.getApplicationUrl(url, componentScope);
	const req = new XMLHttpRequest();
	const async = false;
	req.open('GET', encodeURI(requestUrl), async);
	try {
		req.send(null);
		if (req.status === 200 || req.status === 0) {
			let result = req.responseText;
			if (TCG.data.isErrorResponse(result, requestUrl)) {
				result = undefined;
			}
			return result;
		}
	}
	catch (e) {
		alert('File not found: ' + requestUrl);
	}
	return null;
};

// converts specified className to corresponding script URL that contains the class
TCG.classNameToURL = function(className) {
	let url = '';
	const pkg = className.split('.');
	for (let i = 1; i < pkg.length; i++) {
		if (i < pkg.length - 1) {
			url += pkg[i] + '/';
		}
		else {
			url += pkg[i] + '.js';
		}
	}
	url += '?ts=' + (new Date() - 1); // avoid browser caching
	return url;
};

// our naming convention is to end URL's with 'Find.json' when the result supports paging (different format)
TCG.isUrlWithPagination = function(url) {
	return (url && (url.lastIndexOf('Find.json') !== -1 || url.endsWith('Find')));
};

/**
 * Gets an object representing the key-value pairs of the query parameters for the provided URL.
 */
TCG.getQueryParams = function(url = window.location.search) {
	const searchParams = new URLSearchParams(url);
	return [...searchParams.entries()].reduce((entryMap, [key, value]) => {
		entryMap[key] = value;
		return entryMap;
	}, {});
};

/**
 * Gets the function of the given name as defined by the prototype of the given object.
 *
 * @param {object} object the object whose prototype function should be retrieved
 * @param {string} functionName the name of the function to get
 * @return {function|null} the prototype function, or <tt>null</tt> if no such function was found
 */
TCG.getOverridden = function(object, functionName) {
	const prototype = object.constructor.prototype;
	let fn = prototype && prototype[functionName] || null;
	if (fn === object[functionName]) {
		fn = null;
	}
	return fn;
};

/**
 * Gets the function of the given name as defined by the superclass of the given object.
 *
 * @param {object} object the object whose superclass function should be retrieved
 * @param {string} functionName the name of the function to get
 * @return {function|null} the superclass function, or <tt>null</tt> if no such function was found
 */
TCG.getSuper = function(object, functionName) {
	let superClass = object.constructor.superclass;
	// In case the function was called by a subclass, traverse up the tree until a different function is found
	while (TCG.isNotNull(superClass) && superClass[functionName] === object[functionName]) {
		superClass = superClass.constructor.superclass;
	}
	return (superClass && superClass[functionName]) || null;
};

/**
 * Calls the function of the given name as defined by the prototype of the given object. This function called shall use the given object as its context.
 *
 * @param {object} object the object whose overridden function should be called
 * @param {string} functionName the name of the function to call
 * @param {*[]|IArguments} [args] an array of arguments with which to execute the function, taken as for example from the <tt>arguments</tt> keyword
 * @return {*} the result of the overridden function call
 */
TCG.callOverridden = function(object, functionName, args) {
	const fn = TCG.getOverridden(object, functionName);
	if (TCG.isNull(fn)) {
		throw new Error('Overridden function [' + functionName + '] not defined for object.');
	}
	return fn.apply(object, args);
};

/**
 * Calls the function of the given name as defined by the superclass of the given object. This function called shall use the given object as its context.
 *
 * This function should be only be used when attempting to call the function of the given name as defined by a <i>superclass</i>. If attempting to call an
 * overridden function, use {@link TCG.callOverridden} or {@link TCG.callParent} instead.
 *
 * @param {object} object the object whose superclass function should be called
 * @param {string} functionName the name of the function to call
 * @param {*[]|IArguments} [args] an array of arguments with which to execute the function, taken as for example from the <tt>arguments</tt> keyword
 * @return {*} the result of the superclass function call
 */
TCG.callSuper = function(object, functionName, args) {
	const fn = TCG.getSuper(object, functionName);
	if (TCG.isNull(fn)) {
		throw new Error('Superclass function [' + functionName + '] not defined for object.');
	}
	return fn.apply(object, args);
};

/**
 * Calls the function of the given name as last defined by the parent of the given object, whether this parent be an overridden prototype or the object
 * superclass. This function called shall use the given object as its context.
 *
 * @param {object} object the object whose parent function should be called
 * @param {string} functionName the name of the function to call
 * @param {*[]|IArguments} [args] an array of arguments with which to execute the function, taken as for example from the <tt>arguments</tt> keyword
 * @return {*} the result of the parent function call
 */
TCG.callParent = function(object, functionName, args) {
	const fn = TCG.getOverridden(object, functionName) || TCG.getSuper(object, functionName);
	if (TCG.isNull(fn)) {
		throw new Error('Parent function [' + functionName + '] not defined for object.');
	}
	return fn.apply(object, args);
};

// returns component id for the specified class and unique id
TCG.getComponentId = function(className, id) {
	return className + '-' + (id || (new Date() - 1)); // unique id for new row
};

// can be used for longer column values that show full value on hover but we don't want to wrap the data on multiple lines
TCG.renderValueWithTooltip = function(val, additionalStyle, clazz) {
	if (additionalStyle) {
		additionalStyle.attr = TCG.renderQtip(val);
	}
	return val;
};

// wrap text into multiple lines
TCG.renderText = function(val, additionalStyle, clazz) {
	return '<div style="WHITE-SPACE: normal;' + (additionalStyle || '') + '"' + (clazz === undefined ? '' : ' class="' + clazz + '"') + '>' + val + '</div>';
};

/**
 * Renders the provided value with nowrap and ellipsis.
 * @param value the value to render
 * @returns {string} the resulting div wrapped value
 */
TCG.renderTextNowrap = function(value) {
	// strip any HTML content off the value so it can be properly displayed with nowrap and ellipsis
	return '<div class="nowrap-with-ellipsis">' + Ext.util.Format.stripTags(value) + '</div>';
};

/**
 * Renders the provided value with nowrap and ellipsis, and added value tooltip.
 *
 * @param value the value to render
 * @param additionalStyle the cell metadata used when rendering
 * @returns {string} the resulting div wrapped value
 * @see renderTextNowrap
 * @see renderValueWithTooltip
 */
TCG.renderTextNowrapWithTooltip = function(value, additionalStyle) {
	return TCG.renderTextNowrap(TCG.renderValueWithTooltip(value, additionalStyle));
};

/**
 * Renders the given value wrapped in a &lt;pre&gt; tag and with HTML characters encoded. &lt;pre&gt; tags by default use the CSS attribute "<code>white-space: pre</code>" to retain whitespace.
 */
TCG.renderPre = function(val) {
	return `<pre>${Ext.util.Format.htmlEncode(val)}</pre>`;
};

TCG.renderQtip = function(v) {
	return v ? 'qtip="' + Ext.util.Format.htmlEncode(v) + '"' : undefined;
};

TCG.renderBoolean = function(val, qtip) {
	let qt = '';
	if (qtip && qtip.length > 0) {
		qt = ' qtip="' + Ext.util.Format.htmlEncode(qtip) + '"';
	}
	let r = '<div style="text-align:center;height:13px;overflow:visible;"><img style="vertical-align:-3px;" src="core/images/icons/';
	if (!val) {
		r += 'un';
	}
	r += 'checked.png"' + qt + ' /></div>';
	return r;
};

TCG.renderCheck = function(val, imageIcon) {
	if (!val) {
		return '';
	}
	if (typeof imageIcon !== 'string') {
		imageIcon = 'accept.png';
	}
	let r = '<div style="text-align:center;height:13px;overflow:visible;"><img style="vertical-align:-3px;" src="core/images/icons/';
	r += imageIcon + '" /></div>';
	return r;
};

/**
 * Renders an icon for the result of an evaluation.
 *
 * Supported result values:
 * <ul>
 * <li><code>0</code>, <code>false</code>, or <code>"Failure"</code>: Fail
 * <li><code>1</code>, <code>true</code>, or <code>"Success"</code>: Pass
 * <li><code>2</code> or <code>"Ignored"</code>: Ignored
 * </ul>
 *
 * @param evalResult the result code, boolean, or string from the evaluation
 * @return {string} the icon HTML
 */
TCG.renderEvalResultIcon = function(evalResult) {
	let icon;
	let tooltip;
	switch (evalResult) {
		case 0:
		case false:
		case 'Failure':
			icon = 'cancel.png';
			tooltip = 'Failure';
			break;
		case 1:
		case true:
		case 'Success':
			icon = 'accept.png';
			tooltip = 'Success';
			break;
		case 2:
		case 'Ignored':
			icon = 'ignored.png';
			tooltip = 'Ignored';
			break;
		default:
			icon = null;
			tooltip = null;
	}
	return icon != null
		? `<div style="text-align:center;height:13px;overflow:visible;"><img style="vertical-align: middle;" src="core/images/icons/${icon}" title="${tooltip || ''}"/></div>`
		: '';
};

TCG.renderDate = function(v) {
	let result = '';
	if (TCG.isNotBlank(v)) {
		result = TCG.renderDateTime(v);
		const timeIndex = result.indexOf(' 00:00:00');
		if (timeIndex !== -1) {
			result = result.substring(0, timeIndex);
		}
	}
	return result;
};

TCG.renderTime = function(v) {
	let result = '';
	if (TCG.isNotBlank(v)) {
		if (typeof v === 'string') {
			const dateV = TCG.parseDate(v);
			result = TCG.isNull(dateV) ? v : dateV.dateFormat('H:i:s');
		}
		else {
			result = v.dateFormat('H:i:s');
		}
	}
	return result;
};

TCG.renderDateTime = function(v) {
	let result = '';
	if (TCG.isNotBlank(v)) {
		if (typeof v === 'string') {
			const dateV = TCG.parseDate(v);
			result = TCG.isNull(dateV) ? v : dateV.dateFormat('m/d/Y H:i:s');
		}
		else {
			result = v.dateFormat('m/d/Y H:i:s');
		}
	}
	return result;
};


TCG.renderMinSLA = function(renderValue, slaValue, slaMinValue, metaData) {
	return TCG.renderRangeSLA(renderValue, slaValue, slaMinValue, null, metaData);
};

TCG.renderMaxSLA = function(renderValue, slaValue, slaMaxValue, metaData) {
	return TCG.renderRangeSLA(renderValue, slaValue, null, slaMaxValue, metaData);
};

TCG.renderRangeSLA = function(renderValue, slaValue, slaMinValue, slaMaxValue, metaData) {
	if (TCG.isNotBlank(slaMaxValue)) {
		if (slaValue > slaMaxValue) {
			metaData.css = (slaValue > 2 * slaMaxValue) ? 'ruleViolationBig' : 'ruleViolation';
			metaData.attr = TCG.renderQtip('SLA exception: ' + TCG.numberFormat(slaValue, '0,000') + ' exceeds ' + TCG.numberFormat(slaMaxValue, '0,000') + ' by ' + TCG.numberFormat(100 * (slaValue - slaMaxValue) / slaMaxValue, '0,000') + '%');
			return renderValue;
		}
	}
	if (TCG.isNotBlank(slaMinValue)) {
		if (slaValue < slaMinValue) {
			metaData.css = (slaValue < slaMinValue / 2) ? 'minRuleViolationBig' : 'minRuleViolation';
			metaData.attr = TCG.renderQtip('SLA exception: ' + TCG.numberFormat(slaValue, '0,000') + ' is less than ' + TCG.numberFormat(slaMinValue, '0,000') + ' by ' + TCG.numberFormat(100 * (slaMinValue - slaValue) / slaMinValue, '0,000') + '%');
		}
	}
	return renderValue;
};

TCG.stripTags = function(v) {
	return Ext.util.Format.stripTags(v);
};

// copy-pasted from Ext.isNumber
TCG.isNumber = function(v) {
	return typeof v === 'number' && isFinite(v);
};

/**
 * Returns true if o is an Object and it has no keys
 */
TCG.isEmptyObject = function(o) {
	return TCG.isBlank(o) || (Object.keys(o).length === 0 && o.constructor === Object);
};

/**
 * Returns true if the argument is undefined, null, 'null', an empty string, or NaN.
 */
TCG.isBlank = function(v) {
	return TCG.isNull(v) || v === '' || v === 'null' || (typeof v === 'number' && isNaN(v));
};

/**
 * Returns true if teh argument has a value other than '' or NAN
 * Convenience method for !TCG.isBlank(v)
 */
TCG.isNotBlank = function(v) {
	return !TCG.isBlank(v);
};

/**
 * Returns true if the argument is true or 'true' or 1
 */
TCG.isTrue = function(v) {
	return v === true || v === 'true' || v === 1 || v === '1';
};

/**
 * Returns true if the argument is false or 'false' or 0
 * Warning: This is not the same as !TCG.isTrue
 * Example: var params = {}  !TCG.isTrue(params) = true, but TCG.isFalse(params) = false
 */
TCG.isFalse = function(v) {
	return v === false || v === 'false' || v === 0 || v === '0';
};

/**
 * Returns true if
 * <br/>- expected == toCompare
 * <br/>- both arguments are arrays with equal items
 * <br/>- both arguments are objects with equal entries
 */
TCG.isEquals = function(expected, toCompare) {
	if (expected == toCompare) {
		return true;
	}
	else if (expected == null || toCompare == null) {
		return false;
	}

	const typeExpected = typeof expected;
	const typeToCompare = typeof toCompare;
	if (typeExpected === typeToCompare) {
		if (typeExpected === 'object') {
			if (Array.isArray(expected) && Array.isArray(toCompare)) { // array comparison
				if (expected.length === toCompare.length) {
					const expectedSorted = [...expected].sort(); // de-structure and create new array so original is not impacted
					const toCompareSorted = [...toCompare].sort(); // de-structure and create new array so original is not impacted
					for (let i = 0; i < expected.length; i++) {
						if (!TCG.isEquals(expectedSorted[i], toCompareSorted[i])) {
							return false;
						}
					}
				}
			}
			else { // standard object keys and values comparison
				const expectedKeys = Object.keys(expected);
				const toCompareKeys = Object.keys(toCompare);
				if (expectedKeys.length === toCompareKeys.length) {
					return expectedKeys.every(expectedKey => {
						if (toCompareKeys.indexOf(expectedKey) < 0) {
							return false;
						}
						return TCG.isEquals(expected[expectedKey], toCompare[expectedKey]);
					});
				}
			}
		}
	}
	return false;
};

/**
 * Returns true if the arguments are not equal according to !{@link TCG.isEquals}
 */
TCG.isNotEquals = function(expected, toCompare) {
	return !TCG.isEquals(expected, toCompare);
};

/**
 * Returns true if expected === toCompare
 */
TCG.isEqualsStrict = function(expected, toCompare) {
	return expected === toCompare;
};

/**
 * Returns true if expected !== toCompare
 */
TCG.isNotEqualsStrict = function(expected, toCompare) {
	return expected !== toCompare;
};

/**
 * Returns true if the provided value is null or undefined
 */
TCG.isNull = function(value) {
	return TCG.isEquals(value, null);
};

/**
 * Returns true if the provided value is not null or undefined
 */
TCG.isNotNull = function(value) {
	return TCG.isNotEquals(value, null);
};

/**
 * Returns the first argument which is not <code>null</code> or <code>undefined</code>. If no such argument is found, then <code>null</code> is returned.
 */
TCG.coalesce = function(...values) {
	let result = null;
	for (const val of values) {
		if (val != null) {
			result = val;
			break;
		}
	}
	return result;
};

TCG.parseFloat = function(v) {
	if (!TCG.isNumber(v) && v.replace) {
		v = v.replace(/,/g, '');
		return parseFloat(v);
	}
	return v;
};

//returns formatted file size if "size" is not empty
TCG.fileSize = function(size) {
	if (!TCG.isNumber(size)) {
		return '';
		// the following is copy-pasted from Ext.util.Format.fileSize.  The if-statement above was added for convenience.
	}
	else if (size < 1024) {
		return size + ' bytes';
	}
	else if (size < 1048576) {
		return (Math.round(((size * 10) / 1024)) / 10) + ' KB';
	}
	else {
		return (Math.round(((size * 10) / 1048576)) / 10) + ' MB';
	}
};

TCG.numberFormat = function(v, format, variablePrecision, maxSuffixLength, minSuffixLength) {
	if (isNaN(v)) {
		return '';
	}
	if (variablePrecision && TCG.isNotNull(v)) {
		// if variablePrecision, format string should contain no decimal places
		// if variablePrecision, suffix format string should start with a decimal place.
		let i = format.indexOf('.');
		if (i !== -1) {
			format = format.substring(0, i);
		}

		// format prefix only and keep decimals as are
		let prefix = v.indexOf ? v : v + '';
		i = prefix.indexOf('.'); // preserve decimals in strings
		let suffix = (i === -1) ? '' : prefix.substring(i, prefix.length);
		if (maxSuffixLength) {
			suffix = (maxSuffixLength > suffix.length) ? suffix.substring(0, suffix.length) : suffix.substring(0, maxSuffixLength + 1); // +1 for decimal separator
		}
		if (minSuffixLength && suffix.length < (minSuffixLength + 1)) {
			if (suffix.length === 0) {
				suffix = '.';
			}
			const zerosToAdd = minSuffixLength + 1 - suffix.length;
			for (let i = 0; i < zerosToAdd; i++) {
				suffix += '0';
			}
		}
		prefix = (i === -1) ? prefix : prefix.substring(0, i);
		return (TCG.isEquals(prefix, '-0') && TCG.isNotBlank(suffix) ? prefix : Ext.util.Format.number(prefix, format)) + suffix;
	}

	return Ext.util.Format.number(v, format);
};

TCG.calculatePercentChange = function(v1, v2) {
	if (TCG.isNull(v1) || TCG.isNull(v2)) {
		return null;
	}

	if (TCG.isEquals(v1, v2)) {
		return 0;
	}
	if (v1 === 0) {
		return 100;
	}
	if (v2 === 0) {
		return -100;
	}
	return ((v2 * 100) / v1) - 100;
};

TCG.subtractPrecise = function(v1, v2) {
	// Ensure string type
	v1 = v1 + '';
	v2 = v2 + '';

	// Split integer from decimal (index 0 = integer, index 1 = decimal part)
	const v1Parts = v1.split('.');
	const v2Parts = v2.split('.');

	if (v1Parts.length > 2 || v2Parts.length > 2) {
		TCG.showError('Error in TCG.subtractPrecise: Passed values may not have more than one decimal point.');
		return;
	}

	if (v1Parts.length === 1 && v2Parts.length === 1) {
		return parseInt(v1) - parseInt(v2);
	}

	// At least one has decimal places, convert the other decimal part to 0
	v1Parts[1] = (!v1Parts[1]) ? '0' : v1Parts[1];
	v2Parts[1] = (!v2Parts[1]) ? '0' : v2Parts[1];

	// Name the biggest and smallest fractional parts
	let biggest;
	let smallest;
	if (v1Parts[1].length > v2Parts[1].length) {
		biggest = v1Parts[1];
		smallest = v2Parts[1];
	}
	else {
		biggest = v2Parts[1];
		smallest = v1Parts[1];
	}

	// Build divisor proportional to decimal places of biggest and pad smallest with zeros
	// Example:  (3454 - 3) -> (3454 - 3000), divisor == 10000 (four zeros after the 1)
	let divisor = '1';
	for (let i = 0; i < biggest.length; i++) {
		if (smallest.charAt(i) === '') {
			smallest = smallest + '0';
		}
		divisor = divisor + '0';
	}

	// Perform the subtraction of the decimal part
	let decPart;
	if (TCG.isEquals(parseInt(biggest), parseInt(v1Parts[1]))) {  // keep order of subtraction
		biggest = (v1Parts[0].indexOf('-') !== -1) ? '-' + biggest : biggest;
		smallest = (v2Parts[0].indexOf('-') !== -1) ? '-' + smallest : smallest;

		decPart = parseInt(biggest) - parseInt(smallest);
	}
	else {
		biggest = (v2Parts[0].indexOf('-') !== -1) ? '-' + biggest : biggest;
		smallest = (v1Parts[0].indexOf('-') !== -1) ? '-' + smallest : smallest;

		decPart = parseInt(smallest) - parseInt(biggest);
	}

	decPart = decPart / parseInt(divisor);

	// Perform the subtraction of the integral part
	const intPart = parseInt(v1Parts[0]) - parseInt(v2Parts[0]);

	return Number(intPart + decPart).toFixed(biggest.length); // ensure no scientific notation
};

// If not adjusted, but number is negative, will render the value in red
TCG.renderAdjustedAmount = function(v, adjusted, original, numberFormat, adjustmentLabel, colorPositive) {
	if (adjusted) {
		const format = numberFormat || '0,000.00';
		const value = Ext.util.Format.number(v, format);
		if (!adjustmentLabel) {
			adjustmentLabel = 'Adjustment';
		}
		return '<div class="amountAdjusted" qtip=\'<table><tr><td>Original Value: </td><td align="right">' + Ext.util.Format.number(original, format) + '</td></tr><tr><td>' + adjustmentLabel + ': </td><td align="right">' + TCG.renderAmount(v - original, true, format) + '</td></tr></table>\'>' + value + '</div>';
	}
	return TCG.renderAmount(v, colorPositive, numberFormat);
};

// Color Positive means that colors will also be applied to positive numbers, not just negative numbers
TCG.renderAmount = function(v, colorPositive, numberFormat, variablePrecision, valueSuffix, maxSuffixLength, minSuffixLength) {
	if (v === undefined || v === '') {
		return '';
	}
	let value = TCG.numberFormat(v, numberFormat || (numberFormat === false ? false : '0,000.00'), variablePrecision, maxSuffixLength, minSuffixLength);
	if (valueSuffix) {
		value += valueSuffix;
	}
	if (v < 0) {
		return '<div class="amountNegative">' + value + '</div>';
	}
	if (colorPositive && v > 0) {
		return '<div class="amountPositive">' + value + '</div>';
	}
	return value;
};

TCG.renderPercent = function(val) {
	return TCG.renderAmount(val, true, '0,000.0000 %');
};

TCG.renderPercentPrecise = function(val) {
	return TCG.renderAmount(val, true, '0,000.000000', true, ' %');
};

TCG.renderAmountArrow = function(up) {
	return '<span class="arrow-' + (up ? 'up-green' : 'down-red') + '-narrow" style="padding-right: 10px;" >&nbsp;</span>';
};

TCG.renderIncomingOrOutgoing = function(incoming) {
	if (incoming === '') {
		return '';
	}
	return TCG.renderIcon(`arrow-${(incoming ? 'down-red' : 'up-green')}`, 16, {position: 'relative', bottom: '1px'});
};

// Used to render an icon.
// A style like "position:relative;bottom:1px;" may be needed if edges are cut off
TCG.renderIcon = function(iconCls, size, style, qtip) {
	const appliedStyle = {display: 'inline-block', height: `${size}px`, width: `${size}px`, ...style}
		.let(Object.entries)
		.map(propertyValuePair => propertyValuePair.join(':'))
		.join(';');
	return `<div class="${iconCls}" style="${appliedStyle}" ${(qtip ? `qtip="${qtip}"` : '')}" >&nbsp;</div>`;
};

TCG.renderEmailAddress = function(v) {
	if (TCG.isBlank(v)) {
		return '';
	}
	return '<a href="javascript:TCG.mailto(\'' + v + '\')">' + v + '</a>';
};

// disable confirm for 1 second because we are not leaving the window
TCG.mailto = function(toStr, ccStr, bccStr, subject, body) {
	TCG.enableConfirmBeforeLeavingWindow(true);
	window.setTimeout(TCG.enableConfirmBeforeLeavingWindow, 1000);
	const urlString = ('mailto:' + toStr + '?' +
		(TCG.isBlank(ccStr) ? '' : 'cc=' + ccStr + '&') +
		(TCG.isBlank(bccStr) ? '' : 'bcc=' + bccStr + '&') +
		(TCG.isBlank(subject) ? '' : 'subject=' + encodeURIComponent(subject) + '&') +
		(TCG.isBlank(body) ? '' : 'body=' + encodeURIComponent(body) + '&')).slice(0, -1);
	window.open(urlString, '_self');
};


// Generates an eml file download using the specified address, subject, and body content.
// The default bodyMimeType, if not specified, is 'text/plain'
TCG.generateEmailFile = function(subject, bodyMimeType, body, downloadFileName, toAddress, ccAddress, bccAddress) {
	let contentType = TCG.isNotBlank(bodyMimeType) ? bodyMimeType : '';
	if (TCG.isNotBlank(contentType) && !contentType.includes('charset')) {
		contentType += ';charset=UTF-8';
	}
	const message = (!TCG.isBlank(toAddress) ? 'To:' + toAddress + '\n' : '')
		+ (!TCG.isBlank(ccAddress) ? 'Cc:' + ccAddress + '\n' : '')
		+ (!TCG.isBlank(bccAddress) ? 'Bcc:' + bccAddress + '\n' : '')
		+ 'Subject:' + subject
		+ '\nX-Unsent:1' // Used to flag the message as unsent
		+ (!TCG.isBlank(contentType) ? '\nContent-Type:' + contentType : '')
		+ '\n\n' // need an empty line between the header and body
		+ body;
	TCG.downloadDataAsFile('message/rfc822', message, downloadFileName);
};

TCG.getDocumentFileTypeIconClass = function(extension) {
	switch (extension) {
		case 'doc':
		case 'docx':
			return 'word';
		case 'xls':
			return 'excel';
		case 'xlsx':
			return 'excel_open_xml';
		case 'pdf':
			return 'pdf';
		case 'ppt':
		case 'pptx':
			return 'powerpoint';
		case 'msg':
			return 'email';
		case 'png':
		case 'gif':
		case 'jpg':
			return 'image';
		case 'xml':
			return 'xml';
		case 'avi':
		case 'flv':
		case 'mp4':
		case 'mpeg':
		case 'mpg':
		case 'wmv':
			return 'video';
		case 'mp3':
		case 'ogg':
		case 'wav':
		case 'wma':
			return 'music';
		default:
			return 'question';
	}
};

TCG.getDocumentFileTypeDescription = function(extension) {
	if (!extension) {
		return undefined;
	}
	if (extension.indexOf('doc') === 0) {
		return 'Microsoft Word Document';
	}
	if (extension.indexOf('pdf') === 0) {
		return 'Adobe PDF Document';
	}
	if (extension.indexOf('xls') === 0) {
		return 'Microsoft Excel Document';
	}
	if (extension.indexOf('xlsx') === 0) {
		return 'Microsoft Excel Document';
	}
	if (extension.indexOf('ppt') === 0) {
		return 'Microsoft PowerPoint Document';
	}
	if (extension.indexOf('msg') === 0) {
		return 'Email Message';
	}
	if (extension.indexOf('png') === 0 || extension.indexOf('gif') === 0 || extension.indexOf('jpg') === 0) {
		return 'Image';
	}
	if (extension.indexOf('xml') === 0) {
		return 'XML Document';
	}
	return 'Other Document';
};

TCG.renderDocumentFileTypeIcon = function(extension) {
	if (!extension) {
		return '';
	}
	return '<span ext:qtip="' + extension + '" class="' + TCG.getDocumentFileTypeIconClass(extension) + '" style="padding-right: 10px;" >&nbsp;&nbsp;</span>';
};

TCG.renderIconWithTooltip = function(iconCls, tooltip) {
	return '<span ext:qtip="' + tooltip + '"><span style="width: 16px; height: 16px; float: left;" class="' + iconCls + '" ext:qtip="' + tooltip + '">&nbsp;</span> </span>';
};

TCG.renderActionColumn = function(iconCls, text, tooltip, eventName) {
	const tip = tooltip ? ' ext:qtip="' + tooltip + '"' : '';
	const eventTag = eventName ? ' eventName="' + eventName + '"' : '';
	return '<span class="action-column"' + tip + '' + eventTag + '><span style="width: 16px; height: 16px; float: left;" class="' + iconCls + '"' + tip + '>&nbsp;</span> ' + (text || '') + '</span>';
};

TCG.isActionColumn = function(obj) {
	if (obj && obj.tagName === 'SPAN') {
		if (obj.className === 'action-column') {
			return true;
		}
		obj = obj.parentNode;
		if (obj && obj.className === 'action-column') {
			return true;
		}
	}
	return false;
};

TCG.getActionColumnEventName = function(evt) {
	if (evt.target && evt.target.tagName === 'SPAN') {
		if (evt.target.getAttribute('eventName')) {
			return evt.target.getAttribute('eventName');
		}
		if (evt.target.parentNode) {
			return evt.target.parentNode.getAttribute('eventName');
		}
	}
	return null;
};

// Can use tooltipEventName if you need multiple columns that display different data
TCG.renderDynamicTooltipColumn = function(iconCls, tooltipEventName) {
	const eventTag = tooltipEventName ? ' tooltipEventName="' + tooltipEventName + '"' : '';
	return '<div class="tooltip-column"' + eventTag + '><div style="width: 16px; height: 16px; float: left;" class="' + iconCls + '">&nbsp;</div> </div>';
};

TCG.getDynamicTooltipColumnEventName = function(target) {
	if (target && target.tagName === 'DIV') {
		if (target.getAttribute('tooltipEventName')) {
			return target.getAttribute('tooltipEventName');
		}
		if (target.parentNode) {
			return target.parentNode.getAttribute('tooltipEventName');
		}
	}
	return null;
};

TCG.parseDate = function(value, format) {
	if (!value || Ext.isDate(value)) {
		return value;
	}
	return Date.parseDate(value, format ? format : 'Y-m-d H:i:s');
};

// tries multiple common formats
TCG.parseDateSmart = function(value) {
	if (!value || Ext.isDate(value)) {
		return value;
	}
	let result = undefined;
	['m/d/Y', 'm/d/Y H:i:s', 'Y-m-d H:i:s', 'Y-m-d'].forEach(f => {
		const d = Date.parseDate(value, f);
		if (d) {
			result = d;
			return false;
		}
	});
	return result;
};

TCG.dayDifference = function(toDate, fromDate) {
	const dateOneStr = toDate.dateFormat('m/d/Y');
	const dateTwoStr = fromDate.dateFormat('m/d/Y');
	const key = dateOneStr + ':' + dateTwoStr;
	let result = TCG.CacheUtils.get('DATA_CACHE', key);
	if (!result) {
		result = TCG.getResponseText('coreDateUtilDaysDifference.json?dateOne=' + dateOneStr + '&dateTwo=' + dateTwoStr);
		TCG.CacheUtils.put('DATA_CACHE', result, key);
	}
	return result;
};

TCG.getPreviousWeekday = function(date) {
	date = (date === undefined) ? new Date() : date;
	let i = -1;
	if (date.getDay() === 0) { // Sunday
		i = -2;
	}
	else if (date.getDay() === 1) { // Monday
		i = -3;
	}
	return date.add(Date.DAY, i);
};

TCG.getLastDateOfPreviousMonth = function() {
	// returns this month (0-based), which equals last month (1-based)
	const previousMonth = new Date().getMonth().toString();
	const previousMonthDate = Date.parseDate(previousMonth, 'n');

	return previousMonthDate.getLastDateOfMonth();
};

TCG.getLastDateOfPreviousQuarter = function() {
	const date = new Date();
	const month = date.getMonth();
	if (month < 3) {
		// 12/31
		return new Date(date.getFullYear() - 1, 11, 31);
	}
	else if (month < 6) {
		// 3/31
		return new Date(date.getFullYear(), 2, 31);
	}
	else if (month < 9) {
		// 6/30
		return new Date(date.getFullYear(), 5, 30);
	}
	// 9/30
	return new Date(date.getFullYear(), 8, 30);
};

TCG.getFirstDateOfPreviousQuarter = function() {
	const date = new Date();
	const month = date.getMonth();
	if (month < 3) {
		// 10/1
		return new Date(date.getFullYear() - 1, 9, 1);
	}
	else if (month < 6) {
		// 1/1
		return new Date(date.getFullYear(), 0, 1);
	}
	else if (month < 9) {
		// 4/1
		return new Date(date.getFullYear(), 3, 1);
	}
	// 7/1
	return new Date(date.getFullYear(), 6, 1);
};

TCG.getParentByClass = function(o, clazz) {
	return o.findParentBy(function(p) {
		return (p instanceof clazz);
	});
};

TCG.getParentFormPanel = function(o) {
	return TCG.getParentByClass(o, Ext.form.FormPanel);
};
TCG.getParentTabPanel = function(o) {
	return TCG.getParentByClass(o, Ext.TabPanel);
};

TCG.getChildrenByClass = function(container, clazz) {
	const children = [];
	container.findBy(function(i) {
		if (i instanceof clazz) {
			children.push(i);
		}
	});
	return children;
};

TCG.getChildrenByFunctionName = function(container, functionName) {
	const children = [];
	container.findBy(function(i) {
		if (i[functionName] && (typeof i[functionName] === 'function')) {
			children.push(i);
		}
	});
	return children;
};

TCG.getChildByName = function(container, itemName, includeButtons, itemIndex) {
	const result = container.findBy(function(i) {
		if (TCG.isEquals(i.name, itemName)) {
			return true;
		}
		else if (includeButtons) {
			if (i.buttons) {
				for (let j = 0; j < i.buttons.length; j++) {
					if (TCG.isEquals(i.buttons[j].name, itemName)) {
						return true;
					}
				}
			}
		}
		return false;
	});
	if (TCG.isNotNull(result)) {
		if (result.length === 1) {
			const r = result[0];
			if (includeButtons && TCG.isNotEquals(r.name, itemName)) {
				for (let j = 0; j < r.buttons.length; j++) {
					if (TCG.isEquals(r.buttons[j].name, itemName)) {
						return r.buttons[j];
					}
				}
			}
			return r;
		}
		if (result.length > 1) {
			if (TCG.isNotBlank(itemIndex)) {
				return result[itemIndex];
			}
			else {
				alert('Expected one item with name = "' + itemName + '" but found ' + result.length);
			}
		}
	}
	return null;
};

TCG.setCurrentUser = function(user) {
	TCG.currentUser = user;
};
TCG.getCurrentUser = function() {
	return TCG.currentUser;
};
TCG.setCurrentUserIsAdmin = function(userIsAdmin) {
	TCG.currentUserIsAdmin = userIsAdmin;
};
TCG.isCurrentUserAdmin = function() {
	return TCG.currentUserIsAdmin;
};
TCG.setCurrentUserIsDeveloper = function(userIsDeveloper) {
	TCG.currentUserIsDeveloper = userIsDeveloper;
};
TCG.isCurrentUserDeveloper = function() {
	return TCG.currentUserIsDeveloper;
};

// @param beanName: calendar, systemTable, systemAuditType, etc.
// @param callbackFn: takes JSON validation as first argument: [{maxLength:50,name:"name",required:true},{maxLength:500,name:"description",required:true}]
TCG.getValidationMetaData = function(beanName, callbackFn, scope, componentScope) {
	const CACHE_NAME = 'BEAN_VALIDATION';
	const data = TCG.CacheUtils.get(CACHE_NAME, beanName);
	if (data) {
		callbackFn.call(scope || this, data);
	}
	else {
		const loader = new TCG.data.JsonLoader({
			conf: {cacheKey: beanName},
			onLoad: function(record, conf) {
				TCG.CacheUtils.put(CACHE_NAME, record, conf.cacheKey);
				callbackFn.call(scope || this, record);
			}
		});
		loader.load(`validationMetaData.json?beanName=${beanName}`, componentScope);
	}
};

// handles accidental clicking of Browser Back button
TCG.enableConfirmBeforeLeavingWindow = function(disable) {
	if (disable) {
		window.onbeforeunload = undefined;
	}
	else {
		window.onbeforeunload = function(e) {
			return 'Your current IMS session will be lost.';
		};
	}
};

// Used to submit requests that also download a form
TCG.downloadFile = function(url, params, componentScope) {
	if (!params) {
		params = {};
	}
	params.retrieveRowCount = false;
	// avoid double clicking for file downloads: 2 seconds delay before next click
	if (TCG.downloadInProgress === true) {
		TCG.showInfo('You already started file download. Please wait at least 2 seconds before attempting download of another file. See Browser status bar for download progress.<br/><br/>Do not double-click in order to download a file.', 'Fast Clicker?');
		return;
	}
	TCG.downloadInProgress = true;
	window.setTimeout(function() {
		TCG.downloadInProgress = false;
	}, 2000);

	const o = document.body;
	const downloadUrl = TCG.getApplicationUrl(url, componentScope);
	const form = Ext.DomHelper.append(o, {tag: 'form', method: 'post', action: downloadUrl});

	Ext.iterate(params, function(k, v) {
		if (Ext.isArray(v)) {
			Ext.iterate(v, function(vo) {
				const hd = document.createElement('input');
				Ext.fly(hd).set({type: 'hidden', value: vo, name: k});
				form.appendChild(hd);
			});
		}
		else {
			const hd = document.createElement('input');
			Ext.fly(hd).set({type: 'hidden', value: v, name: k});
			form.appendChild(hd);
		}
	});

	TCG.enableConfirmBeforeLeavingWindow(true);
	o.appendChild(form);
	form.submit();
	o.removeChild(form);
	window.setTimeout(TCG.enableConfirmBeforeLeavingWindow, 1000);
};

//Used to directly open a file from a url - no request submitted
TCG.openFile = function(url) {
	window.open(url);
};

// Example: TCG.openWIKI('IT/Editing+CSV+Files', 'Import Run Help')
TCG.openWIKI = function(wikiPage, windowTitle, width, height, maximized, iconCls) {
	return new TCG.app.URLWindow({
		id: 'wikiWindow',
		title: windowTitle || 'WIKI',
		width: width || 1500,
		height: height || 800,
		maximized: maximized,
		iconCls: iconCls || 'help',
		url: 'https://wiki.paraport.com/display/' + wikiPage
	});
};

TCG.clipboardCopy = function(value) {
	if (window.clipboardData) {
		window.clipboardData.setData('text', value);
	}
	else {
		alert('Your browser doesn\'t support copying to clipboard.');
	}
};

// Creates a link to the provided data as a stream to be downloaded.
// Use dataMimeType and fileName (including suffix) to accurately stream and save the data with the proper functionality
// Example: CSV data: TCG.downloadDataAsFile('text/csv', csvData, 'CsvFile.csv')
TCG.downloadDataAsFile = function(dataMimeType, textData, fileName) {
	const blob = new Blob([textData], {type: dataMimeType + ';charset=utf-8'});
	const url = window.URL.createObjectURL(blob);
	const link = document.createElement('a');
	link.setAttribute('href', url);
	link.setAttribute('download', fileName);
	document.body.appendChild(link); // Required for FF
	link.click(); // This will download the data file
	window.URL.revokeObjectURL(url);
	document.body.removeChild(link);
};

/**
 * Generates a new function which concatenates the given functions. The new function executes all of the given functions in order with the provided arguments and returns the result
 * of the last function.
 */
TCG.concatFn = function() {
	// Get non-null list of functions for closure
	const fnList = Array.prototype.filter.call(arguments, function(arg) {
		return arg != null;
	});

	// Generate concatenated function
	return function() {
		let result;
		for (const fn of fnList) {
			result = fn.apply(this, arguments);
		}
		return result;
	};
};

const BEFOREREQUEST = 'beforerequest',
	REQUESTCOMPLETE = 'requestcomplete',
	REQUESTEXCEPTION = 'requestexception',
	UNDEFINED = undefined,
	LOAD = 'load',
	POST = 'POST',
	GET = 'GET',
	WINDOW = window;

TCG.Ajax = new Ext.data.Connection({
	autoAbort: false,
	serializeForm: function(form) {
		return Ext.lib.Ajax.serializeForm(form);
	},
	request: function(o) {
		const me = this;
		if (me.fireEvent(BEFOREREQUEST, me, o)) {
			if (o.el) {
				if (!Ext.isEmpty(o.indicatorText)) {
					me.indicatorText = '<div class="loading-indicator">' + o.indicatorText + '</div>';
				}
				if (me.indicatorText) {
					Ext.getDom(o.el).innerHTML = me.indicatorText;
				}
				o.success = (Ext.isFunction(o.success) ? o.success : Ext.emptyFn).createInterceptor(function(response) {
					Ext.getDom(o.el).innerHTML = response.responseText;
				});
			}

			let p = o.params,
				url = o.url || me.url;
			const cb = {
				success: me.handleResponse,
				failure: me.handleFailure,
				scope: me,
				argument: {options: o},
				timeout: Ext.num(o.timeout, me.timeout)
			};
			let form,
				serForm;

			if (Ext.isFunction(p)) {
				p = p.call(o.scope || WINDOW, o);
			}

			p = Ext.urlEncode(me.extraParams, Ext.isObject(p) ? Ext.urlEncode(p) : p);

			if (Ext.isFunction(url)) {
				url = url.call(o.scope || WINDOW, o);
			}

			if ((form = Ext.getDom(o.form))) {
				url = url || form.action;
				if (o.isUpload || (/multipart\/form-data/i.test(form.getAttribute('enctype')))) {
					return me.doFormUpload.call(me, o, p, url);
				}
				serForm = Ext.lib.Ajax.serializeForm(form);
				p = p ? (p + '&' + serForm) : serForm;
			}

			const method = o.method || me.method || ((p || o.xmlData || o.jsonData) ? POST : GET);

			if (method === GET && (me.disableCaching && o.disableCaching !== false) || o.disableCaching === true) {
				const dcp = o.disableCachingParam || me.disableCachingParam;
				url = Ext.urlAppend(url, dcp + '=' + (new Date().getTime()));
			}

			o.headers = Ext.apply(me.defaultHeaders || {}, o.headers || {});

			if (o.autoAbort === true || me.autoAbort) {
				me.abort();
			}

			if ((method === GET || o.xmlData || o.jsonData) && p) {
				url = Ext.urlAppend(url, p);
				p = '';
			}
			return (me.transId = Ext.lib.Ajax.request(method, url, cb, p, o));
		}
		else {
			return o.callback ? o.callback.apply(o.scope, [o, UNDEFINED, UNDEFINED]) : null;
		}
	}
});

/**
 * Trims whitespace from each line in the given string. This removes whitespace beginning and end of each line in the given string, leaving only the line content and singular
 * newlines in place.
 */
TCG.trimWhitespace = function(string) {
	// Remove all leading/trailing whitespace for each line
	return string.replace(/(\s*(^|[\r\n]+|$)\s*)+/g, '\n');
};

/**
 * Trims whitespace and newlines from the given string. This removes whitespace beginning and end of each line in the given string and then removes all newlines.
 */
TCG.trimWhitespaceAndNewlines = function(string) {
	// Remove all leading/trailing whitespace for each line and all newlines
	return string.replace(/(\s*(^|[\r\n]+|$)\s*)+/g, '');
};

/**
 * Converts the given <i>camelCase</i> string to a title based on generalized camelCase boundaries. Supported boundaries include lowercase-to-uppercase transitions, number-to-
 * letter transitions and vice versa, the transition between one or more uppercase characters and a word beginning with an uppercase letter, and standard regex word boundaries.
 *
 * Example:
 * <code>TCG.camelCaseToTitle('investmentSubType2') === 'Investment Sub Type 2'</code>
 *
 * @param {string} camelCaseString the camelCase string to be converted to a title
 * @return {string} the title string representation of the given camelCase string
 */
TCG.camelCaseToTitle = function(camelCaseString) {
	/**
	 * The regular expression matching all unicode words. This is adapted from Lodash v4.17.11
	 * (https://github.com/lodash/lodash/blob/0843bd46ef805dd03c0c8d804630804f3ba0ca3c/lodash.js#L264).
	 */
	const reUnicodeWord = /[A-Z\xc0-\xd6\xd8-\xde]?[a-z\xdf-\xf6\xf8-\xff]+(?:['’](?:d|ll|m|re|s|t|ve))?(?=[\xac\xb1\xd7\xf7\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\xbf\u2000-\u206f \t\x0b\f\xa0\ufeff\n\r\u2028\u2029\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000]|[A-Z\xc0-\xd6\xd8-\xde]|$)|(?:[A-Z\xc0-\xd6\xd8-\xde]|[^\ud800-\udfff\xac\xb1\xd7\xf7\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\xbf\u2000-\u206f \t\x0b\f\xa0\ufeff\n\r\u2028\u2029\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\d+\u2700-\u27bfa-z\xdf-\xf6\xf8-\xffA-Z\xc0-\xd6\xd8-\xde])+(?:['’](?:D|LL|M|RE|S|T|VE))?(?=[\xac\xb1\xd7\xf7\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\xbf\u2000-\u206f \t\x0b\f\xa0\ufeff\n\r\u2028\u2029\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000]|[A-Z\xc0-\xd6\xd8-\xde](?:[a-z\xdf-\xf6\xf8-\xff]|[^\ud800-\udfff\xac\xb1\xd7\xf7\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\xbf\u2000-\u206f \t\x0b\f\xa0\ufeff\n\r\u2028\u2029\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\d+\u2700-\u27bfa-z\xdf-\xf6\xf8-\xffA-Z\xc0-\xd6\xd8-\xde])|$)|[A-Z\xc0-\xd6\xd8-\xde]?(?:[a-z\xdf-\xf6\xf8-\xff]|[^\ud800-\udfff\xac\xb1\xd7\xf7\x00-\x2f\x3a-\x40\x5b-\x60\x7b-\xbf\u2000-\u206f \t\x0b\f\xa0\ufeff\n\r\u2028\u2029\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000\d+\u2700-\u27bfa-z\xdf-\xf6\xf8-\xffA-Z\xc0-\xd6\xd8-\xde])+(?:['’](?:d|ll|m|re|s|t|ve))?|[A-Z\xc0-\xd6\xd8-\xde]+(?:['’](?:D|LL|M|RE|S|T|VE))?|\d*(?:1ST|2ND|3RD|(?![123])\dTH)(?=\b|[a-z_])|\d*(?:1st|2nd|3rd|(?![123])\dth)(?=\b|[A-Z_])|\d+|(?:[\u2700-\u27bf]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe2f\u20d0-\u20ff]|\ud83c[\udffb-\udfff])?(?:\u200d(?:[^\ud800-\udfff]|(?:\ud83c[\udde6-\uddff]){2}|[\ud800-\udbff][\udc00-\udfff])[\ufe0e\ufe0f]?(?:[\u0300-\u036f\ufe20-\ufe2f\u20d0-\u20ff]|\ud83c[\udffb-\udfff])?)*/g;
	const strComponents = camelCaseString.match(reUnicodeWord);
	const capitalizedStrComponents = strComponents.map(str => (str.length > 1) ? str.charAt(0).toUpperCase() + str.substring(1) : str.toUpperCase());
	return capitalizedStrComponents.join(' ');
};

if (!Array.prototype.flatMap) {
	/**
	 * Maps an array of values according to the mapping function provided and then flattens the resulting values into a single-dimensional array. The mapping function must produce
	 * an array value or <code>null</code> for each entry in the source array.
	 */
	Array.prototype.flatMap = function(mappingFn) {
		return this
			.map(mappingFn)
			.filter(value => !!value)
			.reduce((resultArr, valueList) => resultArr.concat(valueList), []);
	};
}

if (!Array.prototype.flatten) {
	/**
	 * Flattens the sub-array elements of an array into a resulting array. This performs flattening recursively up to the specified depth. This also removes empty elements from the array.
	 *
	 * This is a shim for <code><a href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/flat">Array.prototype.flat</a></code>.
	 */
	Array.prototype.flatten = function(maxDepth = 1) {
		const flattenElements = (array, curDepth = 0) => {
			const result = [];
			for (let i = 0; i < array.length; i++) {
				if (array.hasOwnProperty(i)) {
					const element = array[i];
					if (curDepth < maxDepth && Array.isArray(element)) {
						result.push(...flattenElements(element, curDepth + 1));
					}
					else {
						result.push(element);
					}
				}
			}
			return result;
		};
		return flattenElements(this);
	};
}

if (!Array.prototype.peek) {
	/**
	 * Performs the given function for each element in the array. The original array is returned.
	 */
	Array.prototype.peek = function(fn) {
		this.forEach(el => fn(el));
		return this;
	};
}

// Assign scope-functions
/**
 * Performs the given *let* operation on this value, returning the operation's result. This borrows from the use of `let` in Kotlin, which in turn adapts it from languages like
 * Haskell.
 *
 * Example usage:
 * ```
 * function processValue(value) {
 *     // Convert a new-line delimited list of key-value pairs into an object ('key1=value1\nkey2=value2\n...').
 *     // This is equivalent to Object.entries(value.split('\n').map(el => el.split('='))).
 *     value.split('\n')
 *          .map(el => el.split('='))
 *          .let(Object.fromEntries);
 * }
 * ```
 * @param {function} fn the function to apply on this object
 * @external Object.prototype.let
 * @external String.prototype.let
 * @external Number.prototype.let
 * @external Boolean.prototype.let
 */
const letFn = function(fn) {
	return fn(this);
};
// Use Object.defineProperty to prevent modified prototype property from being enumerable (enumerable is false by default; see https://stackoverflow.com/a/10695535/1941654)
if (!Object.prototype.let) {
	Object.defineProperty(Object.prototype, 'let', {value: letFn});
}
if (!String.prototype.let) {
	Object.defineProperty(String.prototype, 'let', {value: letFn});
}
if (!Number.prototype.let) {
	Object.defineProperty(Number.prototype, 'let', {value: letFn});
}
if (!Boolean.prototype.let) {
	Object.defineProperty(Boolean.prototype, 'let', {value: letFn});
}

/**
 * A class for debouncing a callback using a given <code>period</code>, buffering values until the callback is triggered. This is a rate-limiting operator.
 *
 * Instances of this class will act as buffers with delayed callbacks. After each time that a value is submitted, an internal timer will be reset to the specified <code>period</code>. Once this timer is allowed to expire (i.e., no values are submitted for the specified <code>period</code>) then the callback will be executed with the buffered list of values. The <code>maxDelay</code> value, if specified, will be used as the maximum amount of time that the buffer will use to build up values.
 *
 * This is similar to the RxJS <a href="https://rxjs-dev.firebaseapp.com/api/operators/debounceTime"><code>debounceTime</code></a> and <a href="https://rxjs-dev.firebaseapp.com/api/operators/bufferTime"><code>bufferTime</code></a> operators.
 */
TCG.DebounceBuffer = class {

	/**
	 * @param callback the callback to execute once no events have been received for the specified <code>period</code>
	 * @param period the delay after receiving the last event after which the <code>callback</code> should be executed
	 * @param maxDelay the maximum delay after receiving the first event in the current buffered window after which the <code>callback</code> will be executed
	 */
	constructor(callback, period, maxDelay = -1) {
		this.callback = callback;
		this.debounceDelay = period;
		this.maxDelay = maxDelay;

		this.periodTimeoutId = null;
		this.maxDelayTimeoutId = null;
		this.valueList = [];
	}


	/**
	 * Submits a new event to be buffered for execution.
	 */
	next(...values) {
		clearTimeout(this.periodTimeoutId);
		this.valueList.push(...values);
		this.periodTimeoutId = setTimeout(() => this.execute(), this.debounceDelay);
		if (this.maxDelay >= 0 && !this.maxDelayTimeoutId) {
			this.maxDelayTimeoutId = setTimeout(() => this.execute(), this.maxDelay);
		}
	}


	/**
	 * @private
	 */
	execute() {
		clearTimeout(this.periodTimeoutId);
		clearTimeout(this.maxDelayTimeoutId);
		this.periodTimeoutId = null;
		this.maxDelayTimeoutId = null;
		this.callback(this.valueList);
		this.valueList = [];
	}
};

/**
 * Enables drag and drop event handling for a component based on the provided config.
 *
 * @param renderedOndropObject the rendered object to add dom events to for drag and drop functionality
 * @param ondropCallback the ondrop callback to execute upon drop events, arguments will be the event and rendered object
 * @param renderedOnclickObject the rendered object to add dom onclick event functionality
 * @param onclickCallback the onclick callback to execute upon click events, arguments will be the event and rendered object
 */
TCG.enableDragAndDrop = function(renderedOndropObject, ondropCallback, renderedOnclickObject, onclickCallback) {
	if (renderedOndropObject) {
		const el = renderedOndropObject.getEl();
		el.dom.ondragover = function(event) {
			event.dataTransfer.dropEffect = 'copy';
			el.setStyle('opacity', '0.25');
			return false;
		};
		el.dom.ondragleave = function() {
			el.setStyle('opacity', '');
			return false;
		};
		el.dom.ondrop = function(event) {
			event.preventDefault && event.preventDefault();
			el.setStyle('opacity', '');
			if (ondropCallback && typeof ondropCallback === 'function') {
				ondropCallback(event, renderedOndropObject);
			}
		};
		if (renderedOnclickObject && onclickCallback && typeof onclickCallback === 'function') {
			renderedOnclickObject.getEl().dom.onclick = function(event) {
				onclickCallback(event, renderedOnclickObject);
			};
		}
	}
};

/**
 * Returns the given function with the given bound arguments. This intentionally avoids the use of {@link Function#bind} so that the <code>this</code> context during execution can
 * be determined by the caller of the resulting function.
 *
 * @param func the function to bind to
 * @param args the arguments to bind
 * @return {Function} the function with bound parameters
 */
TCG.partial = function(func, ...args) {
	return function() {
		return func.call(this, ...args, ...arguments);
	};
};

/*
 * Enhance built-in initComponent function to clone the plugins array. This prevents shared state across multiple instances with the same configuration for components which use an
 * array to set multiple plugins.
 */
Ext.Component.prototype.initComponent = TCG.concatFn(
	function() {
		// Clone plugins array during instantiation
		this.plugins = this.plugins && (Array.isArray(this.plugins) ? [...this.plugins] : [this.plugins]) || [];
	},
	Ext.Component.prototype.initComponent
);

/**
 * If date1 is after date2, returns true
 *
 * If date1 is before date2, returns false
 *
 * If date1 is null it is treated as far future and the function returns true. (date1 is farther into the future than date2)
 *
 * If date2 is null it is treated as far future and the function returns false (date2 is farther into the future than date1)
 */
TCG.isDateAfter = function(date1, date2, excludeTime) {
	if (TCG.isBlank(date1)) {
		return true;
	}
	if (TCG.isBlank(date2)) {
		return false;
	}
	let d1, d2;
	if (excludeTime) {
		d1 = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
		d2 = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
	}
	else {
		d1 = date1;
		d2 = date2;
	}
	return d1.getTime() > d2.getTime();
};

/**
 * Decodes the given JSON string. This modifies the original ExtJS function to provide additional error-handling capabilities.
 */
Ext.decode = (fn => json => {
	try {
		// Discover exception messages injected during streaming serialization
		const responseResetDelimiter = '\n// RESET ';
		let deserializeStr = json;
		if (typeof json === 'string') {
			const lastResetIndex = json.lastIndexOf(responseResetDelimiter);
			if (lastResetIndex > -1) {
				deserializeStr = json.substring(lastResetIndex + responseResetDelimiter.length);
			}
		}
		return fn(deserializeStr);
	}
	catch (e) {
		// Display error message on deserialization failure
		const errorMessage = `An error occurred during deserialization: [${e && e.message}]`;
		TCG.showError(errorMessage);
		const newError = new Error(errorMessage);
		if (e.stack) {
			newError.stack += `\nCaused by: ${e.stack}`;
		}
		throw newError;
	}
})(Ext.decode);


/**
 * Override Ext.data.Store.createSortFunction to handle undefined values correctly.
 */
Ext.data.Store.prototype.createSortFunction = function(field, direction) {
	const directionModifier = (direction || 'ASC').toUpperCase() === 'DESC' ? -1 : 1;
	const sortType = this.fields.get(field).sortType;

	return function(r1, r2) {
		const v1 = sortType(r1.data[field]);
		const v2 = sortType(r2.data[field]);

		// CHANGE: convert undefined to null so that comparisons work
		const normalizedV1 = typeof v1 === 'undefined' ? null : v1;
		const normalizedV2 = typeof v2 === 'undefined' ? null : v2;

		return directionModifier * (normalizedV1 > normalizedV2 ? 1 : (normalizedV1 < normalizedV2 ? -1 : 0));
	};
};

/**
 * Appends one or more elements to an array, if the provided array argument is an array.
 *
 * @param array - Array to append one or more elements to.
 * @param elements - An array of elements, or one or more independent elements, to append.
 * @returns {*} - the provided <code>array</code>; if <code>array</code> is an array, elements will have been appended
 */
TCG.appendToArray = (array, ...elements) => {
	if (Array.isArray(array) && elements) {
		if (Array.isArray(elements)) {
			array.push(...elements);
		}
		else {
			array.push(elements);
		}
	}
	return array;
};

/**
 * Appends one or more elements to an array. If the provided <code>array</code> argument undefined, it will be initialized to an empty array.
 *
 * @param array - Array to append one or more elements to.
 * @param elements - An array of elements, or one or more independent elements, to append.
 * @returns {*} - the provided <code>array</code>, or a new array, with elements appended
 */
TCG.appendToArrayWithInitialization = (array = [], ...elements) => {
	if (Array.isArray(elements)) {
		return TCG.appendToArray(array, ...elements);
	}
	return TCG.appendToArray(array, elements);
};

/**
 * Rounds the value the given number of digits, if the number of digits are not provided, the default is 3 digits.
 *
 * @param v - Value to be rounded.
 * @param numDigits - The number of digits that the value will be rounded to.
 * @returns {Number} - The rounded value.
 */
TCG.roundToDecimalDigits = function(v, numDigits) {
	if (TCG.isBlank(v) || !TCG.isNumber(v)) {
		return v;
	}
	const precision = (TCG.isBlank(numDigits) || !TCG.isNumber(numDigits)) ? 3 : numDigits;
	return Math.round(v * 10 ** precision) / 10 ** precision;
};
