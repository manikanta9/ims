// tcg-gridfilters.js file customizes behavior of Ext.ux.grid.GridFilters elements
// and adds new filters: ComboFilter


Ext.override(Ext.ux.grid.GridFilters, {
	updateColumnHeadings: function() {
		const view = this.grid.getView();
		let i, len, filter;
		if (view.mainHd) {
			for (i = 0, len = view.cm.config.length; i < len; i++) {
				// CHANGE: add title
				const conf = view.cm.config[i];
				filter = this.getFilter(conf.dataIndex);
				let cell = view.getHeaderCell(i);
				const active = filter && filter.active;
				cell = Ext.get(cell);
				cell[active ? 'addClass' : 'removeClass'](this.filterCls);
				cell.child('div', true).qtip = active ? this.getFilterQtip(filter, conf) : '';
			}
		}
	},

	getFilterQtip: function(filter, cellConf) {
		let qtip = '<b>' + cellConf.header + ':</b><br />';
		let args = filter.getSerialArgs();
		if (!Ext.isArray(args)) {
			const a = args;
			args = [];
			args[0] = a;
		}
		for (let i = 0; i < args.length; i++) {
			const arg = args[i];
			let comp = arg.comparison;
			if (comp === 'LIKE') {
				comp = 'Contains';
			}
			else if (comp === 'EQUALS') {
				comp = 'Equals';
			}
			else if (comp === 'NOT_EQUALS') {
				comp = 'Not Equals';
			}
			else if (comp === 'LESS_THAN') {
				comp = 'Less than';
			}
			else if (comp === 'LESS_THAN_OR_EQUALS') {
				comp = 'Less than or equals';
			}
			else if (comp === 'GREATER_THAN') {
				comp = 'Greater than';
			}
			else if (comp === 'GREATER_THAN_OR_EQUALS') {
				comp = 'Greater than or equals';
			}
			else if (comp === 'IN') {
				comp = 'In';
			}
			else if (comp === 'BEGINS_WITH') {
				comp = 'Begins with';
			}
			else if (comp === 'ENDS_WITH') {
				comp = 'Ends with';
			}
			else if (comp === 'EQUALS_OR_IS_NULL') {
				comp = 'Equals or empty';
			}
			else if (comp === 'LESS_THAN_OR_IS_NULL') {
				comp = 'Less than or empty';
			}
			else if (comp === 'GREATER_THAN_OR_IS_NULL') {
				comp = 'Greater than or empty';
			}
			else if (comp === 'IS_NULL') {
				comp = 'Is empty';
			}
			if (i > 0) {
				qtip += ' AND';
			}
			qtip += '&nbsp;' + comp + ' "' + filter.getText(arg) + '"';
		}
		return qtip;
	},

	bindStore: function(store, initial) {
		if (!initial && this.store) {
			// CHANGE: unregister both events
			store.un('load', this.onLoad, this);
			store.un('beforeload', this.onBeforeLoad, this);
		}
		if (store) {
			store.on('load', this.onLoad, this);
			store.on('beforeload', this.onBeforeLoad, this);
			// set toolbar if present so that on filter we always load first page
			if (!this.toolbar) {
				const tbar = this.grid.getBottomToolbar();
				if (tbar instanceof TCG.toolbar.PagingToolbar) {
					this.toolbar = tbar;
				}
			}
		}
		this.store = store;
	},

	onLoad: function(store, options) {
		// CHANGE: do not do local filtering for remote stores
		if (this.local || this.grid.localFilteringEnabled) {
			store.filterBy(this.getRecordFilter());
		}
		this.applyNonPersistentFieldFilters(store, true);
	},

	/**
	 * Applies filtering to the provided store with all filters with nonPersistentField=true.
	 *
	 * Filters defined as nonPersistentField will be applied after data from the server is applied. If the data is populated after the server-side
	 * data is loaded, the filter will filter all records out. Thus, the skipFilterOnLoad property equal to true can be used to skip this after load filtering.
	 * The client will have to apply this filter when applicable.
	 *
	 * If there are no applicable filters, nothing will be filtered.
	 *
	 * @param store the grid store holding data for filtering
	 * @param onLoad indicates if the nonPersistentField filters active should be applied as on load or not; applicable with skipFilterOnLoad property of the filter
	 * @returns {boolean} true if the store was updated, or rows were removed, as a result of filtering nonPersistentField filters
	 */
	applyNonPersistentFieldFilters: function(store, onLoad) {
		const f = [];
		let recordFilteredOut = false;
		this.filters.each(function(filter) {
			if (filter.active && TCG.isTrue(filter.nonPersistentField) && (!TCG.isTrue(filter.skipFilterOnLoad) || TCG.isFalse(onLoad))) {
				f.push(filter);
			}
		});

		const len = f.length;
		if (f.length > 0) {
			store.filterBy(function(record) {
				for (let i = 0; i < len; i++) {
					if (!f[i].validateRecord(record)) {
						recordFilteredOut = true;
						return false;
					}
				}
				return true;
			});
		}
		return recordFilteredOut;
	},

	getFilterData: function() {
		const filters = [];
		let i, len;

		this.filters.each(function(f) {
			if (f.active && !TCG.isTrue(f.nonPersistentField)) {
				const d = [].concat(f.serialize());
				for (i = 0, len = d.length; i < len; i++) {
					let searchField = f.searchFieldName ? f.searchFieldName : f.dataIndex;
					if (searchField && searchField.indexOf('.') !== -1 && TCG.isFalse(f.doNotTruncateBeanPath)) {
						// cannot have . in the search field name: use the last part
						searchField = searchField.substring(searchField.lastIndexOf('.') + 1);
					}
					filters.push({
						field: searchField,
						data: d[i]
					});
				}
			}
		});
		// CHANGE: added onBeforeReturnFilterData to allow changes to filters
		this.onBeforeReturnFilterData(filters);
		return filters;
	},

	onBeforeLoad: function(store, options) {
		options.params = options.params || {};
		this.cleanParams(options.params);
		const params = this.buildQuery(this.getFilterData());
		Ext.apply(options.params, params);
		// CHANGE: add hook to populate store.baseParams and merge 2 restriction lists (strings of arrays of objects)
		this.onBeforeQuery(store, options);

	},

	// CHANGE: added onBeforeReturnFilterData to allow changes to filters
	onBeforeReturnFilterData: function(filters) {
		// empty
	},

	onBeforeQuery: function(store, options) {
		TCG.grid.mergeStoreQueryParameter(store, options, this.paramPrefix);
	}
});


Ext.ux.grid.filter.Filter.prototype.getText = function(arg) {
	return arg.value;
};


Ext.override(Ext.ux.grid.filter.DateFilter, {
	orNull: false,
	orEquals: false,
	filterNull: false,
	compareMap: {
		// CHANGE: use different names
		before: 'LESS_THAN',
		after: 'GREATER_THAN',
		on: 'EQUALS'
	},
	/**
	 * Overridden to support null queries.
	 */
	setValue: function(value, preserve) {
		this.nullQuery = false;
		for (const key in this.fields) {
			//Added from merge - necessary?
			if (this.fields.hasOwnProperty(key)) {
				if (value[key]) {
					this.fields[key].menu.picker.setValue(value[key]);
					this.fields[key].setChecked(true);
				}
				else if ('on' === key && TCG.isNull(value[key]) && this.filterNull) {
					this.nullQuery = true;
					this.fields['on'].setChecked(true);
				}
				else if (!preserve) {
					this.fields[key].setChecked(false);
				}
			}
		}
		this.fireEvent('update', this);
	},
	getSerialArgs: function() {
		const args = [];
		if (this.nullQuery) {
			args.push({comparison: 'IS_NULL'});
		}
		else {
			for (const key in this.fields) {
				if (this.fields[key].checked) {
					args.push({
						//type: 'date', // CHANGE: don't need the type
						comparison: this.compareMap[key] + (this.orNull === true ? '_OR_IS_NULL' : '') + ((this.orEquals === true && this.compareMap[key] !== 'EQUALS') ? '_OR_EQUALS' : ''),
						value: this.getFieldValue(key).format(this.dateFormat),
						dataTypeName: 'DATE'
					});
				}
			}
		}
		return args;
	}

});


Ext.override(Ext.ux.grid.filter.ListFilter, {
	matchTextInLocalMode: true,
	orNull: false,

	getSerialArgs: function() {
		// CHANGE: use comparison: 'IN' instead of type: 'list'
		const args = {comparison: 'IN' + (TCG.isTrue(this.orNull) ? '_OR_IS_NULL' : ''), value: this.phpMode ? this.getValue().join(',') : this.getValue()};
		return args;
	},

	validateRecord: function(record) {
		const value = this.getValue();
		// CHANGE: support text matching (instead of value) in local mode
		if (this.matchTextInLocalMode) {
			for (let i = 0; i < value.length; i++) {
				let v = value[i];
				for (let j = 0; j < this.options.length; j++) {
					const o = this.options[j];
					if (o[0] === value[i]) {
						v = o[1];
						break;
					}
				}
				value[i] = v;
			}
		}
		return value.indexOf(record.get(this.dataIndex)) > -1;
	}
});


Ext.override(Ext.ux.grid.filter.NumericFilter, {
	iconCls: {
		gt: 'comparison-greater-than',
		lt: 'comparison-less-than',
		eq: 'comparison-equals',
		ne: 'comparison-not-equals'
	},
	menuItems: ['lt', 'gt', '-', 'eq', 'ne'],

	getSerialArgs: function() {
		const args = [],
			values = this.menu.getValue();
		for (const key of Object.keys(values)) {
			args.push({
				// CHANGE: don't send the type and convert the key
				//type: 'numeric',
				comparison: (key === 'eq') ? 'EQUALS' : (key === 'ne') ? 'NOT_EQUALS' : (((key === 'lt') ? 'LESS_THAN' : 'GREATER_THAN') + ((this.orEquals === true && key !== 'eq') ? '_OR_EQUALS' : '')),
				value: values[key],
				dataTypeName: 'DECIMAL'
			});
		}
		return args;
	},

	validateRecord: function(record) {
		const val = record.get(this.dataIndex),
			values = this.getValue();
		if (values.eq !== undefined && val !== values.eq) {
			return false;
		}
		if (values.ne !== undefined && val === values.ne) {
			return false;
		}
		if (values.lt !== undefined && val >= values.lt) {
			return false;
		}
		if (values.gt !== undefined && val <= values.gt) {
			return false;
		}
		return true;
	}
});


Ext.override(Ext.ux.grid.filter.StringFilter, {
	iconCls: 'ux-gridfilter-text-icon',
	local: false,
	filterConditions: [],
	defaultSelection: 'LIKE',
	showEqualsFilter: false,
	showNotEqualsFilter: false,
	showContainsFilter: false,
	showNotContainsFilter: false,
	showInFilter: false,
	showStartsWithFilter: false,
	showEndsWithFilter: false,
	showNullFilter: false,
	init: function(config) {
		//Reset filter conditions so columns have individual configs
		this.filterConditions = [];
		const enabledFilters = [];
		const gridPanel = this.gridPanel;
		if (this.showContainsFilter) {
			const filter = ['LIKE', 'Contains', 'Filter Contains Cell Value', 'Filters on all selected row values for this column.', 'comparison-contains'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		if (this.showEqualsFilter) {
			const filter = ['EQUALS', 'Equals', 'Filter Equal To Cell Value', 'Filters on all selected row values for this column.', 'comparison-equals'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		if (this.showStartsWithFilter) {
			const filter = ['BEGINS_WITH', 'Starts With', 'Filter Starts With Cell Value', 'Filters on all selected row values for this column.', 'starts-with'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		if (this.showNotContainsFilter) {
			const filter = ['NOT_LIKE', 'Not Contains', 'Filter Not Contains Cell Value', 'Filters on all selected row values for this column.', 'comparison-not-contains'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		if (this.showNotEqualsFilter) {
			const filter = ['NOT_EQUALS', 'Not Equal', 'Filter Not Equal To Cell Value', 'Filters on all selected row values for this column.', 'comparison-not-equals'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		if (this.showEndsWithFilter) {
			const filter = ['ENDS_WITH', 'Ends With', 'Filter Ends With Cell Value', 'Filters on all selected row values for this column.', 'ends-with'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		if (this.showInFilter) {
			//IN filter is only created if the grid allows multiple selection
			if (gridPanel.allowMultiple) {
				const filter = ['IN', 'In', 'Filter In Selected Cell Values', 'Filters on all selected row values for this column.', 'comparison-in'];
				if (enabledFilters.indexOf(filter[0]) === -1) {
					this.filterConditions.push(filter);
					enabledFilters.push(filter[0]);
				}
			}
		}
		if (this.showNullFilter) {
			const filter = ['IS_NULL', 'Is Empty', 'Filter Where Cell Value Is NULL', 'Filters on all selected NULL row values for this column.', 'is-null'];
			if (enabledFilters.indexOf(filter[0]) === -1) {
				this.filterConditions.push(filter);
				enabledFilters.push(filter[0]);
			}
		}
		Ext.applyIf(config, {
			enableKeyEvents: true,
			iconCls: this.iconCls,
			filterConditions: this.filterConditions,
			defaultSelection: this.defaultSelection,
			listeners: {
				scope: this,
				keyup: this.onInputKeyUp
			}
		});
		if (config.filterConditions.length > 0) {
			this.initCombo(config);
		}
		this.inputItem = new Ext.form.TextField(config);
		this.menu.add(this.inputItem);
		this.updateTask = new Ext.util.DelayedTask(this.fireUpdate, this);
	},

	initCombo: function(config) {
		let selection = undefined;
		for (const item of config.filterConditions) {
			selection = selection || item[0];
			if (item[0] === config.defaultSelection) {
				selection = item[0];
				break;
			}
		}
		this.combo = new Ext.form.ComboBox({
			mode: 'local',
			width: 150,
			typeAhead: false,
			triggerAction: 'all',
			enableKeyEvents: true,
			forceSelection: true,
			submitValue: false,
			lazyInit: false,
			style: {
				marginLeft: '25px'
			},
			store: new Ext.data.ArrayStore({
				fields: ['value', 'name'],
				data: config.filterConditions
			}),
			listeners: {
				scope: this,
				select: function(cb, record, index) {
					this.comparison = record.data.value;
					//Ensure the input field is set back to active in case changing from IS_NULL to another filter, and reset value to blank for same reason.
					this.inputItem.setDisabled(false);
					if (TCG.isEqualsStrict(this.inputItem.getValue(), ' ')) {
						this.inputItem.setValue('');
					}
					if (TCG.isEqualsStrict(this.comparison, 'IS_NULL')) {
						this.inputItem.setDisabled(true);
						//Set dummy blank value so filter will fire update.
						this.inputItem.setValue(' ');
					}
					const active = this.isActivatable();
					this.setActive(active);
					if (active) {
						this.fireEvent('update', this);
					}
				},
				afterrender: function(cb) {
					cb.mon(cb.innerList, 'mousedown', function(e) {
						const ev = e.browserEvent || e;
						if (ev.stopPropagation) {
							ev.stopPropagation();
						}
						else {
							ev.cancelBubble = true;
						}
					}, cb);
				}
			},
			value: selection,
			valueField: 'value',
			displayField: 'name'
		});
		this.combo.parentFilter = this;
		this.menu.add(this.combo);
	},

	getSerialArgs: function() {
		let v = this.getValue();
		if (TCG.isNotBlank(this.comparison)) {
			if (TCG.isEqualsStrict(this.comparison, 'IS_NULL')) {
				v = undefined;
			}
			return {comparison: this.comparison, value: v};
		}
		if (this.combo) {
			if (TCG.isEqualsStrict(this.combo.value, 'IS_NULL')) {
				v = undefined;
			}
			return {comparison: this.combo.value, value: v, dataTypeName: 'STRING'};
		}
		// CHANGE: use comparison: 'LIKE' instead of type: 'string'
		return {comparison: 'LIKE', value: v, dataTypeName: 'STRING'};
	},

	getValue: function() { // when copy/pasting from Excel, etc. spaces maybe added
		const v = this.inputItem.getValue();
		return (v && v.trim && !this.doNotTrimStrings) ? v.trim() : v;
	},

	validateRecord: function(record) {
		const val = record.get(this.dataIndex);
		if (typeof val != 'string') {
			return (this.getValue().length === 0);
		}
		if (this.filterConditions.length > 0) {
			if (TCG.isEqualsStrict(this.combo.getValue(), 'EQUALS')) {
				return TCG.isEqualsStrict(val.toLowerCase(), this.getValue().toLowerCase());
			}
			if (TCG.isEqualsStrict(this.combo.getValue(), 'NOT_EQUALS')) {
				return !TCG.isEqualsStrict(val.toLowerCase(), this.getValue().toLowerCase());
			}
			if (TCG.isEqualsStrict(this.combo.getValue(), 'LIKE')) {
				return val.toLowerCase().indexOf(this.getValue().toLowerCase()) > -1;
			}
			if (TCG.isEqualsStrict(this.combo.getValue(), 'NOT_LIKE')) {
				return val.toLowerCase().indexOf(this.getValue().toLowerCase()) < 0;
			}
			if (TCG.isEqualsStrict(this.combo.getValue(), 'IN')) {
				const values = this.getValue().split('<OR>').map(v => v.toLowerCase().trim());
				return values.includes(val.toLowerCase());
			}
			if (TCG.isEqualsStrict(this.combo.getValue(), 'BEGINS_WITH')) {
				return val.toLowerCase().startsWith(this.getValue().toLowerCase());
			}
			if (TCG.isEqualsStrict(this.combo.getValue(), 'ENDS_WITH')) {
				return val.toLowerCase().endsWith(this.getValue().toLowerCase());
			}
		}
		return val.toLowerCase().indexOf(this.getValue().toLowerCase()) > -1;
	},

	isActivatable: function() {
		return this.inputItem.getValue().length > 0 && (!this.combo || this.combo.getValue().length > 0);
	}
});


Ext.override(Ext.ux.grid.filter.BooleanFilter, {
	getSerialArgs: function() {
		// CHANGE: use comparison: 'EQUALS' instead of type: 'string'
		const args = {comparison: 'EQUALS', value: this.getValue(), dataTypeName: 'BOOLEAN'};
		return args;
	},
	getText: function(arg) {
		return arg.value ? this.yesText : this.noText;
	}
});


Ext.ux.grid.filter.ComboFilter = Ext.extend(Ext.ux.grid.filter.Filter, {
	orNull: false,
	showNotEquals: false,
	menuItems: ['equals', 'not_equals'],
	compareMap: {
		equals: 'EQUALS',
		not_equals: 'NOT_EQUALS'
	},
	textMap: {
		equals: 'Equals',
		not_equals: 'Not Equals'
	},

	phpMode: false,
	matchTextInLocalMode: true,
	defaultField: 'equals',

	init: function(config) {
		this.fields = {};
		if (!this.showNotEquals) {
			let combo = Ext.applyIf(config, {
				xtype: 'combo',
				iconCls: 'ux-gridfilter-text-icon', // TODO: find layout that shows the icon
				emptyText: '< No Filter Selected >',
				getListParent: function() {
					return this.el.up('.x-menu');
				}
			});
			Ext.apply(config, {layout: 'fit', width: 200, style: {overflow: 'visible'}});
			this.menu = new Ext.menu.Menu(config);
			combo = this.fields['equals'] = this.menu.add(combo);
			combo.on('select', this.fireUpdate, this);
		}
		else {
			const getListParentFunction = function() {
				return this.el.up('.x-menu');
			};
			const originalConfig = Ext.apply({}, config);
			for (let i = 0; i < this.menuItems.length; i++) {
				let item = this.menuItems[i];
				let menuItem = item;
				if (item !== '-') {
					// get the original config
					config = Ext.apply({}, originalConfig);

					const combo = Ext.applyIf(config, {
						xtype: 'combo',
						iconCls: 'ux-gridfilter-text-icon', // TODO: find layout that shows the icon
						emptyText: '< No Filter Selected >',
						getListParent: getListParentFunction,
						listeners: {
							scope: this,
							select: this.onMenuSelect
						}
					});
					Ext.apply(config, {layout: 'fit', width: 200, style: {overflow: 'visible'}});
					const comboMenu = new Ext.menu.Menu(config);
					item = this.fields[item] = comboMenu.add(combo);
					item.on('select', this.fireUpdate, this);
					const cfg = {
						itemId: 'range-' + menuItem,
						text: this.textMap[menuItem],
						menu: comboMenu,
						listeners: {
							scope: this,
							checkchange: this.onCheckChange
						}
					};
					menuItem = item.menuItem = new Ext.menu.CheckItem(cfg);
				}
				this.menu.add(menuItem);
			}
		}
		// make it backwards compatible
		this.combo = this.fields[this.defaultField];
	},

	onCheckChange: function() {
		this.setActive(this.isActivatable());
		this.fireEvent('update', this);
	},

	onMenuSelect: function(combo, record) {
		const fields = this.fields;
		combo.menuItem.setChecked(true);

		if (combo === fields.equals) {
			fields.not_equals.menuItem.setChecked(false, true);
		}
		else {
			fields.equals.menuItem.setChecked(false, true);
		}
		this.fireEvent('update', this);
	},

	setValue: function(value) {
		if (typeof value === 'object') {
			let found = false;
			for (const key in this.fields) {
				if (value[key]) {
					found = true;
					const val = value[key];
					if (typeof val === 'object') {
						this.fields[key].lastSelectionText = val.text;
						Ext.form.ComboBox.superclass.setValue.call(this.fields[key], val.text);
						this.fields[key].value = val.value;
					}
					else {
						this.fields[key].setValue(val);
					}
					if (this.fields[key] && this.fields[key].menuItem) {
						this.fields[key].menuItem.setChecked(true);
					}
				}
			}
			if (!found) {
				this.fields[this.defaultField].lastSelectionText = value.text;
				Ext.form.ComboBox.superclass.setValue.call(this.fields[this.defaultField], value.text);
				this.fields[this.defaultField].value = value.value;
				if (this.fields[this.defaultField] && this.fields[this.defaultField].menuItem) {
					this.fields[this.defaultField].menuItem.setChecked(true);
				}
			}
		}
		else {
			this.fields[this.defaultField].setValue(value);
			if (this.fields[this.defaultField] && this.fields[this.defaultField].menuItem) {
				this.fields[this.defaultField].menuItem.setChecked(true);
			}
		}
		this.fireUpdate();
	},

	isActivatable: function() {
		for (const key in this.fields) {
			if (this.fields[key].getValue() !== '') {
				return true;
			}
		}
		return false;
	},

	getValue: function() {
		const result = {};
		for (const key in this.fields) {
			if (this.fields[key].checked) {
				result[key] = this.fields[key].getValue();
			}
		}
		return result;
	},

	getText: function(selected) {
		let filterField = this.fields[this.defaultField];
		if (selected && selected.comparison) {
			const selectedField = this.fields[selected.comparison.toLocaleLowerCase()];
			if (TCG.isNotNull(selectedField)) {
				filterField = selectedField;
			}
		}
		return filterField.lastSelectionText;
	},

	getSerialArgs: function() {
		const args = [];
		for (const key in this.fields) {
			if (!this.fields[key].menuItem || this.fields[key].menuItem.checked) {
				args.push({
					comparison: this.compareMap[key] + (this.orNull === true ? '_OR_IS_NULL' : ''),
					value: this.fields[key].getValue()
				});
				if (this.dataTypeName) {
					args[args.length - 1].dataTypeName = this.dataTypeName;
				}
			}
		}
		return args;
	},

	validateRecord: function(record) {
		const rowValue = record.get(this.dataIndex);
		for (const key in this.fields) {
			if (TCG.isFalse(this.showNotEquals) || this.fields[key].checked) {
				const filterValue = this.matchTextInLocalMode ? this.fields[this.defaultField].lastSelectionText : this.fields[this.defaultField].getValue(); // must be a better way to get text
				if ((this.orNull !== true) && TCG.isNull(rowValue)) {
					return false;
				}
				if ((key === 'not_equals') && (rowValue === filterValue)) {
					return false;
				}
				if ((key === 'equals') && (rowValue !== filterValue)) {
					return false;
				}
			}
		}
		return true;
	}
});

