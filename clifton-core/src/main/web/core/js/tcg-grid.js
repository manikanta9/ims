Ext.ns('TCG.grid');


/*
 * Override the column model to fix sizes in Chrome. Without this override, columns within the grid may be a different
 * width than the header columns.
 *
 * Delete this override if the issue is found to be fixed in a future version of Chrome. Last verified in Chrome 52.
 */
Ext.grid.ColumnModel.override({
	getTotalWidth: function(includeHidden) {
		let off = 0;
		if (!Ext.isDefined(Ext.isChromeExtraPadding)) {
			Ext.isChromeExtraPadding = Ext.isChrome;
		}
		if (Ext.isChromeExtraPadding) {
			off = 2;
		}
		if (!this.totalWidth) {
			this.totalWidth = 0;
			let i = 0;
			const len = this.config.length;
			for (; i < len; i++) {
				if (includeHidden || !this.isHidden(i)) {
					this.totalWidth += this.getColumnWidth(i) + off;
				}
			}
		}
		return this.totalWidth;
	}
});


Ext.ux.form.SpinnerField.prototype.constructor = Ext.ux.form.SpinnerField.prototype.constructor.createDelegate(function() {
	this.spinner.field = this;
});

TCG.grid.GridView_onLoad = function() { // keep selected row in view: default behavior is to scroll to top
	const grid = this.grid;
	const sm = grid.getSelectionModel();
	const rowIndex = sm.getSelected ? grid.store.indexOf(sm.getSelected()) : -1;
	//holdPosition can be set to true on the gridView prior to calling reload
	//this will allow the scrollbar position to be maintained rather than calling scrollToTop
	if (this.holdPosition) {
		this.holdPosition = false;
		return false;
	}
	if (rowIndex >= 0) {
		grid.getView().focusRow(rowIndex);
		return false;
	}
	return true;
};
Ext.ux.grid.BufferView.prototype.onLoad = Ext.ux.grid.BufferView.prototype.onLoad.createInterceptor(TCG.grid.GridView_onLoad);
Ext.grid.GridView.prototype.onLoad = Ext.grid.GridView.prototype.onLoad.createInterceptor(TCG.grid.GridView_onLoad);
Ext.grid.GroupingView.prototype.onLoad = Ext.grid.GroupingView.prototype.onLoad.createInterceptor(TCG.grid.GridView_onLoad);

/**
 * Need to override BufferView to not mark dirty cells if the grid view is difined with markDirty = false (true is default)
 * This method is copied from the original with the only change being the addition of "this.markDirty &&" for marking dirty cells in the loop.
 */
Ext.override(Ext.ux.grid.BufferView, {
	doRender: function(cs, rs, ds, startRow, colCount, stripe, onlyBody) {
		const ts = this.templates,
				ct = ts.cell,
				rt = ts.row,
				rb = ts.rowBody,
				last = colCount - 1,
				rh = this.getStyleRowHeight(),
				vr = this.getVisibleRows(),
				tstyle = 'width:' + this.getTotalWidth() + ';height:' + rh + 'px;',
				// buffers
				buf = [];
		let cb,
				c;
		const p = {},
				rp = {tstyle: tstyle};
		let r;
		let j = 0;
		const len = rs.length;
		for (; j < len; j++) {
			r = rs[j];
			cb = [];
			const rowIndex = (j + startRow);
			const visible = rowIndex >= vr.first && rowIndex <= vr.last;
			if (visible) {
				for (let i = 0; i < colCount; i++) {
					c = cs[i];
					p.id = c.id;
					p.css = i === 0 ? 'x-grid3-cell-first ' : (i === last ? 'x-grid3-cell-last ' : '');
					p.attr = p.cellAttr = '';
					p.value = c.renderer(r.data[c.name], p, r, rowIndex, i, ds);
					p.style = c.style;
					if (p.value === undefined || p.value === '') {
						p.value = '&#160;';
					}
					if (this.markDirty && r.dirty && typeof r.modified[c.name] !== 'undefined') {
						p.css += ' x-grid3-dirty-cell';
					}
					cb[cb.length] = ct.apply(p);
				}
			}
			const alt = [];
			if (stripe && ((rowIndex + 1) % 2 === 0)) {
				alt[0] = 'x-grid3-row-alt';
			}
			if (r.dirty) {
				alt[1] = ' x-grid3-dirty-row';
			}
			rp.cols = colCount;
			if (this.getRowClass) {
				alt[2] = this.getRowClass(r, rowIndex, rp, ds);
			}
			rp.alt = alt.join(' ');
			rp.cells = cb.join('');
			buf[buf.length] = !visible ? ts.rowHolder.apply(rp) : (onlyBody ? rb.apply(rp) : rt.apply(rp));
		}
		return buf.join('');
	}
});

/**
 * The grid resizes itself to take full bottom portion of its container window.
 */
TCG.grid.GridPanel = Ext.extend(Ext.Panel, {
	stripeRows: true,
	forceFit: true, // fit columns into view or allow horizontal scrolling
	border: false,
	layout: 'vbox',
	layoutConfig: {align: 'stretch'},
	gridConfig: {},
	pageSize: 100, // number of elements per page when paging is enabled
	standardColumns: [ // these columns will be added to the end of the grid if corresponding dataIndex fields are defined
		{header: 'Created On', width: 40, dataIndex: 'createDate', hidden: true},
		{header: 'Updated On', width: 40, dataIndex: 'updateDate', hidden: true}
	],
	appendStandardColumns: true,
	editor: null,
	viewNames: undefined, // optional array of view names (or objects with text/url properties if each view has different url) for this grid: each column that has viewNames property will be shown/hidden depending on matching view name
	includeAllColumnsView: true, // If view names is populated, add All Columns view (enabled by default)
	elements: 'body, bbar',
	rowSelectionModel: 'single', // supported models: 'single', 'multiple', 'checkbox'
	overrideSelectionRightClick: false, // if true overrides the right click for selection to behave like left click; doing so messes up selections using context menus
	useBufferView: true, // faster but can't wrap rows
	limitRequestedProperties: true, // set to pass 'requestedProperties' parameter with pipe delimited properties
	requestIdDataIndex: false, // if set to true and limitRequestedProperties is true, idDataIndex will be appended to requestedProperties
	additionalPropertiesToRequest: 'id', // pipe delimited list of additional properties that are not part of column model
	reloadOnRender: true,
	hideStandardButtons: false,
	hideToolsMenu: false,
	topToolbarUpdateCountPrefix: '->',
	topToolbarSearchParameter: false, // usually "searchPattern": will add Search box to the toolbar

	// Dynamically Loaded Tooltips
	useDynamicTooltipColumns: false, // True to load the tooltip for the grid on render
	dynamicTooltipTitle: 'Details', // Title for the tooltip
	cacheDynamicTooltip: true, // If true, will get the generated tooltip once and store it so when hovering again will display the same data without another server call
	// getTooltipTextForRow: Make sure when used to override the getTooltipTextForRow function to return the text that is needed for your tooltip
	enableCellContextMenuFilterItems: true, // set to anything other than true to disable grid cell contextmenu filter items

	initComponent: function() {
		this.initColumns();
		const gridPanel = this;
		if (this.isEditEnabled() || this.searchForm || this.instructions || this.wikiPage) {
			// In some cases we may want to start with a toolbar that has been initialized already
			// See DocumentVersionGrid where we want to be able to start with Document toolbar, and add to that.
			if (TCG.isNull(this.tbar)) {
				Ext.apply(this, {tbar: new Ext.Toolbar()});
			}
		}
		TCG.grid.GridPanel.superclass.initComponent.call(this);

		let filtersPlugin = null;
		if (!this.ds) {
			const fields = [];
			const filters = [];

			// add standard columns if they don't already exist
			if (this.dataTable) {
				this.appendStandardColumns = false;
			}
			if (this.appendStandardColumns) {
				for (let i = 0; i < gridPanel.standardColumns.length; i++) {
					let found = false;
					const c = TCG.clone(gridPanel.standardColumns[i]);
					for (let j = 0; j < gridPanel.columns.length; j++) {
						if (gridPanel.columns[j].dataIndex === c.dataIndex) {
							found = true;
							break;
						}
					}
					if (!found) {
						if (gridPanel.standardColumnViewNames && gridPanel.standardColumnViewNames[c.dataIndex]) {
							c.viewNames = gridPanel.standardColumnViewNames[c.dataIndex];
						}
						gridPanel.columns[gridPanel.columns.length] = c;
					}
				}
			}

			// Apply column overrides prior to filling metadata and creating filters
			const columnOverrides = this.getColumnOverrides();
			if (columnOverrides) {
				for (let i = 0; i < columnOverrides.length; i++) {
					const co = columnOverrides[i];
					const column = this.columns.find(c => c.dataIndex === co.dataIndex);
					if (column) {
						Ext.apply(column, co);
					}
					else {
						// column is not override, add it as new
						this.columns.push(co);
					}
				}
			}

			const sortInfo = TCG.grid.fillColumnMetaData(this.columns, fields, filters, this);

			this.ds = this.createStore(fields);
			if (TCG.isNotNull(sortInfo.sortColumn)) {
				this.ds.setDefaultSort(sortInfo.sortColumn, sortInfo.sortDirection);
			}
			if (filters.length > 0) {
				this.onBeforeCreateFilters(filters);
				this.applyDependencyFilters(filters);
				// Define our filter parameter used for sending filters to the server.
				const filterParameterPrefix = 'restrictionList';
				const gridFiltersConfig = Ext.applyIf((this.gridFiltersConfig || {}),
						{local: true, encode: true, filters: filters, paramPrefix: filterParameterPrefix, gridPanel: gridPanel});
				filtersPlugin = new Ext.ux.grid.GridFilters(gridFiltersConfig);
				filtersPlugin.onBeforeReturnFilterData = gridPanel.onBeforeReturnFilterData;
				filtersPlugin.onBeforeQuery = function(store, options) {
					if (store.proxy && gridPanel.reloadIsRunning !== true) {
						// store is performing a load not initiated by the grid e.g. filter selection via the filter plugin
						// populate the options.params for the store the same way the grid does on reload
						gridPanel.reloadIsRunning = true;
						return gridPanel.setStoreBaseParams(options)
								.then(() => {
									TCG.grid.mergeStoreQueryParameter(store, options, filterParameterPrefix);
									(function() {
										gridPanel.reloadIsRunning = false;
									}).defer(750, gridPanel);
								});
					}
					else {
						TCG.grid.mergeStoreQueryParameter(store, options, filterParameterPrefix);
					}
				};
				this.ds.addListener('load', function(store, records, opts) {
					// switch to remote (server side filtering) if local and (yes paging or filers present)
					const remote = ((store.isPartialData && store.isPartialData()) || filtersPlugin.getFilterData().length > 0);
					filtersPlugin.local = gridPanel.forceLocalFiltering ? true : !remote;
					filtersPlugin.bindStore(store);
				});
			}
		}
		this.grid = this.createGrid(filtersPlugin);
		this.add(this.grid);
	},
	// allows for overriding to add dynamic columns
	initColumns: function() {
		this.columns = TCG.clone(this.columns); // avoid conflicts when multiple instances are open at the same time
	},

	getColumnOverrides: function() {
		return this.columnOverrides;
	},

	onDestroy: function() {
		if (this.reloadTask) {
			Ext.TaskMgr.stop(this.reloadTask);
		}
		Ext.destroy(this.rowSelectionModel);
		Ext.destroy(this.columns);
		Ext.destroy(this.ds);
		Ext.destroy(this.grid);
		TCG.grid.GridPanel.superclass.onDestroy.apply(this, arguments);
	},

	createGrid: function(filtersPlugin) {
		if (TCG.isNotNull(this.editor)) {
			if (!this.editor.ptype) {
				this.editor.ptype = 'grideditor';
			}
			this.editor = Ext.ComponentMgr.createPlugin(this.editor);
		}
		// create a copy of gridConf and add plugins as necessary
		const gridConf = {
			plugins: [],
			...this.gridConfig
		};
		if (TCG.isNotNull(this.editor)) {
			if (this.editor.allowToDeleteMultiple && this.rowSelectionModel === 'single') {
				//If delete multiple is allowed default to multiple selection if set to single
				this.rowSelectionModel = 'multiple';
			}
			gridConf.plugins.push(this.editor);
		}
		if (TCG.isNotNull(filtersPlugin)) {
			gridConf.plugins.push(filtersPlugin);
		}

		// Get view configuration
		const viewConf = {
			forceFit: this.forceFit, scrollDelay: false, groupTextTpl: this.groupTextTpl, emptyGroupText: this.emptyGroupText,
			// make grid text selectable (can copy to clipboard)
			templates: {
				cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>',
						'</td>'
				)
			},
			...this.viewConfig || {}
		};

		// Get view class
		let viewClass;
		if (this.viewConfig && this.viewConfig.xtype) {
			viewClass = Ext.ComponentMgr.types[this.viewConfig.xtype];
		}
		else if (this.groupField) {
			viewClass = Ext.grid.GroupingView;
			viewConf.groupTextTpl = this.groupTextTpl || '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})';
			if (this.getGroup) {
				viewConf.getGroup = this.getGroup;
			}
		}
		else if (this.useBufferView) {
			viewClass = Ext.ux.grid.BufferView;
		}
		else {
			viewClass = Ext.grid.GridView;
		}

		// Generate grid panel
		return new Ext.grid.GridPanel({
			ownerGridPanel: this,
			border: false,
			flex: 1, // take 100% of all available space
			loadMask: true,
			stripeRows: this.stripeRows,
			ds: this.ds,
			cm: this.getColumnModel(),
			sm: this.getRowSelectionModel(),
			bbar: this.isPagingEnabled() ? new TCG.toolbar.PagingToolbar({store: this.ds, displayInfo: true, pageSize: this.pageSize}) : undefined,
			getTitle: () => this.getWindow().title,
			view: new viewClass(viewConf),
			...gridConf
		});
	},

	getRowSelectionModel: function() {
		const config = {};
		if (this.overrideSelectionRightClick) {
			Ext.apply(config, this.getRowSelectionModelOverrideConfig());
		}
		if (typeof this.rowSelectionModel != 'object') {
			if (this.rowSelectionModel === 'checkbox') {
				this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel(config);
			}
			else {
				config.singleSelect = this.rowSelectionModel !== 'multiple';
				this.rowSelectionModel = new Ext.grid.RowSelectionModel(config);
			}
		}
		return this.rowSelectionModel;
	},

	getRowSelectionModelOverrideConfig: function() {
		const config = {};
		if (this.overrideSelectionRightClick) {
			//Enable right click to be the same as left click
			config.handleMouseDown = function(g, rowIndex, e) {
				if ((e.button !== 0 && e.button !== 2) || this.isLocked()) {
					return;
				}
				const view = this.grid.getView();
				if (e.shiftKey && !this.singleSelect && this.last !== false) {
					const last = this.last;
					this.selectRange(last, rowIndex, e.ctrlKey);
					this.last = last; // reset the last
					view.focusRow(rowIndex);
				}
				else {
					const isSelected = this.isSelected(rowIndex);
					if (e.ctrlKey && isSelected) {
						this.deselectRow(rowIndex);
					}
					else if (!isSelected || this.getCount() > 1) {
						this.selectRow(rowIndex, e.ctrlKey || e.shiftKey);
						view.focusRow(rowIndex);
					}
				}
			};
		}

		return config;
	},

	getColumnModel: function() {
		if (TCG.isNotNull(this.grid) && TCG.isNotNull(this.grid.getColumnModel())) {
			return this.grid.getColumnModel();
		}

		// copy columns
		const cols = this.columns.slice();
		if (this.rowSelectionModel === 'checkbox') {
			// insert checkbox at index 0
			cols.splice(0, 0, this.getRowSelectionModel());
		}
		return new Ext.grid.ColumnModel(cols);
	},

	createStore: function(fields) {
		const gridPanel = this;
		const storeConf = {
			fields: fields,
			url: encodeURI(TCG.getApplicationUrl(this.getLoadURL(), this)),
			listeners: {
				datachanged: function(store, records, opts) {
					gridPanel.updateCount();
				}
			}
		};
		if (gridPanel.idProperty) {
			storeConf.id = gridPanel.idProperty;
		}
		if (!this.dataTable && this.isPagingEnabled()) {
			storeConf.root = 'data.rows';
			storeConf.totalProperty = 'data.total';
		}
		if (this.storeRoot) {
			storeConf.root = this.storeRoot;
		}
		if (this.remoteSort) {
			storeConf.forceRemoteSort = this.remoteSort;
		}
		let storeClass = TCG.data.JsonStore;
		if (this.dataTable) {
			if (this.groupField) {
				storeClass = TCG.data.DataTableGroupingStore;
				storeConf.groupField = this.groupField;
				storeConf.remoteSort = this.remoteSort || false;
				storeConf.forceRemoteSortDataTable = this.forceRemoteSortDataTable;
			}
			else {
				storeClass = TCG.data.DataTableStore;
			}
		}
		else if (this.groupField) {
			storeClass = TCG.data.GroupingStore;
			storeConf.groupField = this.groupField;
			storeConf.remoteSort = this.remoteSort || false;
		}
		return new storeClass(storeConf);
	},

	onRender: function() {
		TCG.grid.GridPanel.superclass.onRender.apply(this, arguments);

		const gridPanel = this;
		if (this.reloadOnRender) {
			this.on('render', this.reload, this, {delay: 100});
		}

		// generate toolbar
		this.generateToolbar(gridPanel);

		// Columns with listeners do not properly render due to how we implemented the grid panel. This hook registers listeners for
		// a column after the grid panel is rendered by adding listeners using the eventListeners property. The event handlers in
		// eventListeners should look identical to how they would in the listeners property; just a new property name is used.
		this.on('afterrender', function() {
			TCG.grid.registerColumnEventListeners(this.grid);

			// conditionally apply rowcontextmenu if cellcontextmenu is not used.
			let enableRowContextMenu = TCG.isTrue(this.grid.events['rowcontextmenu']) || TCG.isNull(this.grid.events['rowcontextmenu']);

			// After column listeners are defined, add grid cellcontextmenu default filter if not already defined.
			if (TCG.isTrue(this.enableCellContextMenuFilterItems)) {
				if (TCG.isTrue(this.grid.events['cellcontextmenu']) || TCG.isNull(this.grid.events['cellcontextmenu'])) {
					enableRowContextMenu = false;

					this.grid.addListener('cellcontextmenu', async function(grid, rowIndex, columnIndex, event) {
						const column = grid.getColumnModel().getColumnById(columnIndex),
								columnContextMenu = column.events['contextmenu'];
						if (!TCG.isFalse(column.filter) && (TCG.isTrue(columnContextMenu) || TCG.isNull(columnContextMenu))) {
							event.preventDefault();
							const contextMenuItems = await grid.ownerGridPanel.getGridCellContextMenuItems(grid, rowIndex, columnIndex);
							if (contextMenuItems.length > 0) {
								const menu = new Ext.menu.Menu({
									items: contextMenuItems
								});
								menu.showAt(event.getXY());
							}
						}
					});
				}
			}

			// Add rowcontextmenu items if no event present
			if (enableRowContextMenu) {
				this.grid.addListener('rowcontextmenu', async function(grid, rowIndex, event) {
					const record = grid.getStore().getAt(rowIndex);
					event.preventDefault();
					const contextMenuItems = await grid.ownerGridPanel.getGridRowContextMenuItems(grid, rowIndex, record);
					if (contextMenuItems && contextMenuItems.length > 0) {
						const menu = new Ext.menu.Menu({
							items: contextMenuItems
						});
						menu.showAt(event.getXY());
					}
				});
			}

			// Add clear filter header context menu to filterable columns if no event present
			if (TCG.isTrue(this.grid.events['headercontextmenu']) || TCG.isNull(this.grid.events['headercontextmenu'])) {
				this.grid.addListener('headercontextmenu', function(grid, columnIndex, event) {
					const column = grid.getColumnModel().getColumnById(columnIndex);
					if (column && !TCG.isFalse(column.filter)) {
						const menu = new Ext.menu.Menu({
							items: [
								{
									text: 'Clear Filter(s)',
									tooltip: 'Removes any filters that are applied to this column.',
									iconCls: 'clear',
									handler: function() {
										const filter = grid.filters.getFilter(column.dataIndex);
										filter.setActive(false);
									}
								}
							]
						});
						event.preventDefault();
						menu.showAt(event.getXY());
					}
				});
			}
		}, this);
	},

	generateToolbar: function(gridPanel) {
		const t = gridPanel.getTopToolbar();

		// Add First Toolbar Buttons - Before "Add"
		if (this.addFirstToolbarButtons) {
			this.addFirstToolbarButtons(t, gridPanel);
		}

		if (this.isEditEnabled()) {
			this.editor.addEditButtons(t, this);
		}
		if (this.searchForm) {
			const sb = {
				text: 'Find',
				tooltip: 'Toggle search filters',
				iconCls: 'find',
				enableToggle: true,
				pressed: !gridPanel.hideSearchForm,
				handler: function() {
					if (this.pressed) {
						gridPanel.searchForm.show();
					}
					else {
						gridPanel.searchForm.hide();
					}
				}
			};
			t.add(sb);
			t.add('-');
			gridPanel.insert(0, gridPanel.searchForm);
			if (gridPanel.hideSearchForm) {
				gridPanel.searchForm.hide();
			}
		}
		if (TCG.isNotNull(t) && this.viewNames) {
			const menu = new Ext.menu.Menu();
			let i;
			for (i = 0; i < this.viewNames.length; i++) {
				const v = this.viewNames[i];
				menu.add({
					text: v.text || v,
					xtype: 'menucheckitem',
					tooltip: v.tooltip || void 0,
					checked: (i === 0),
					group: 'view' + gridPanel.id,
					handler: function() {
						gridPanel.switchToView(this.text);
					}
				});
			}
			if (TCG.isTrue(this.includeAllColumnsView)) {
				menu.add('-');
				menu.add({
					text: 'All Columns',
					xtype: 'menucheckitem',
					checked: (i === 0),
					group: 'view' + gridPanel.id,
					handler: function() {
						gridPanel.switchToView('All Columns');
					}
				});
			}
			gridPanel.configureViews(menu, gridPanel);
			t.add({
				text: 'Views',
				iconCls: 'views',
				menu: menu
			});
			t.add('-');
		}
		if (TCG.isNotNull(t) && this.addToolbarButtons) {
			this.addToolbarButtons(t, gridPanel);
		}
		if (TCG.isNotNull(t) && gridPanel.ds.proxy && !this.hideStandardButtons) {
			t.add({
				text: 'Reload',
				tooltip: 'Reload latest grid data',
				iconCls: 'table-refresh',
				handler: function() {
					gridPanel.reload();
				}
			});
			t.add('-');
		}
		if (this.instructions && !this.hideStandardButtons) {
			t.add({
				text: 'Info',
				tooltip: 'Toggle instructions for this page',
				iconCls: 'info',
				enableToggle: true,
				handler: function() {
					if (this.pressed) {
						gridPanel.insert(0, {
							xtype: 'panel',
							frame: true,
							layout: 'fit',
							bodyStyle: 'padding: 3px 3px 3px 3px',
							html: gridPanel.instructions
						});
					}
					else {
						gridPanel.remove(0);
					}
					gridPanel.doLayout();
				}
			}, '-');
		}
		if (this.getWikiPage(gridPanel) !== false) {
			t.add({
				text: this.wikiPageIconText || 'Help',
				iconCls: this.wikiPageIconCls || 'help',
				tooltip: this.wikiPageTitle || 'Open WIKI Help Window',
				handler: function() {
					gridPanel.openWikiPage(gridPanel);
				}
			}, '-');
		}
		if (TCG.isNotNull(t) && !this.hideStandardButtons && !this.hideToolsMenu) {
			const menu = new Ext.menu.Menu();
			if (gridPanel.grid.filters) {
				menu.add({
					text: 'Clear All Filters',
					iconCls: 'clear',
					handler: function() {
						gridPanel.clearFilters();
					}
				});
				menu.add('-');
			}

			menu.add({
				text: 'Show All Columns',
				handler: function() {
					gridPanel.switchToView('All Columns');
				}
			});
			menu.add('-');

			menu.add({
				text: 'Export to Excel',
				iconCls: 'excel',
				menu: {
					items: [
						{
							text: 'All Columns (Excel 97-2003)',
							iconCls: 'excel',
							handler: function() {
								gridPanel.exportGrid('xls', true);
							}
						}, {
							text: 'Visible Columns (Excel 97-2003)',
							iconCls: 'excel',
							handler: function() {
								gridPanel.exportGrid('xls');
							}
						}, '-', {
							text: 'All Columns (*.xlsx)',
							iconCls: 'excel_open_xml',
							handler: function() {
								gridPanel.exportGrid('xlsx', true);
							}
						}, {
							text: 'Visible Columns (*.xlsx)',
							iconCls: 'excel_open_xml',
							handler: function() {
								gridPanel.exportGrid('xlsx');
							}
						}

					]
				}
			});
			menu.add({
				text: 'Export to CSV',
				iconCls: 'csv',
				menu: {
					items: [
						{
							text: 'All Columns',
							iconCls: 'csv',
							handler: function() {
								gridPanel.exportGrid('csv', true);
							}
						},
						{
							text: 'Visible Columns',
							iconCls: 'csv',
							handler: function() {
								gridPanel.exportGrid('csv');
							}
						}
					]
				}
			});
			menu.add({
				text: 'Export to PDF',
				iconCls: 'pdf',
				menu: {
					items: [
						{
							text: 'All Columns',
							iconCls: 'pdf',
							handler: function() {
								gridPanel.exportGrid('pdf', true);
							}
						},
						{
							text: 'Visible Columns',
							iconCls: 'pdf',
							handler: function() {
								gridPanel.exportGrid('pdf');
							}
						}
					]
				}
			});
			if (TCG.isCurrentUserDeveloper()) {
				menu.add({
					text: 'Export Migration Actions',
					iconCls: 'tools',
					handler: function() {
						gridPanel.exportGrid('migrationActions', true);
					}
				});
			}
			if (this.queryExportTagName) {
				const dd = {tagName: this.queryExportTagName};
				menu.add({
					text: 'Query Exports',
					iconCls: 'run',
					handler: function() {
						TCG.createComponent('Clifton.system.query.QueryExportListWindow', {
							defaultData: dd,
							openerCt: gridPanel
						});
					}
				});
			}
			if (this.importTableName) {
				let imps = [];
				if (typeof this.importTableName == 'string') {
					imps.push({table: this.importTableName, importComponentName: this.importComponentName});
				}
				else {
					imps = this.importTableName;
				}

				for (let i = 0; i < imps.length; i++) {
					const label = imps[i].label;
					menu.add({
						text: 'Import ' + (label ? label + ' ' : '') + 'from Excel',
						iconCls: 'import',
						importIndex: i,
						handler: function() {
							const imp = imps[this.importIndex];
							const componentName = (imp.importComponentName ? imp.importComponentName : 'Clifton.system.upload.UploadWindow');
							let defaultData = {tableName: imp.table};
							if (gridPanel.getImportComponentDefaultData) {
								defaultData = Ext.apply(defaultData, gridPanel.getImportComponentDefaultData(imp.table));
							}

							TCG.createComponent(componentName, {
								defaultData: defaultData,
								openerCt: gridPanel
							});
						}
					});
				}
			}
			menu.add({
				text: 'Print',
				iconCls: 'printer',
				handler: () => Ext.ux.Printer.print(gridPanel.grid)
			});
			this.configureToolsMenu(menu);
			t.add({
				text: 'Tools',
				iconCls: 'config',
				menu: menu
			});
			t.add('-');
		}

		if (TCG.isNotNull(t) && this.addToolbarFilters) {
			this.addToolbarFilters(t, gridPanel);
		}

		if (TCG.isNotNull(t) && this.addToolbarButtonsAfter) {
			this.addToolbarButtonsAfter(t, gridPanel);
		}

		if (this.useDynamicTooltipColumns) {
			gridPanel.grid.addListener('render', function(grid) {
				const store = grid.getStore();  // Capture the Store.
				const view = grid.getView();    // Capture the GridView.
				gridPanel.addDynamicTooltip(grid, store, view);
			});
		}
	},

	// if false is returned, Help icon will not be added to the toolbar
	getWikiPage: function(gridPanel) {
		return gridPanel.wikiPage || false;
	},

	openWikiPage: function(gridPanel) {
		const url = gridPanel.getWikiPage(gridPanel);
		if (url === false) {
			TCG.showError('Help Page was not defined.', 'No Help Page');
		}
		else {
			TCG.openWIKI(url, gridPanel.wikiPageTitle, gridPanel.wikiPageWidth || 1500, gridPanel.wikiPageHeight || 800, gridPanel.wikiPageMaximized, gridPanel.wikiPageIconCls || 'help');
		}
	},

	configureViews: function(menu, gridPanel) {
		// optional Views menu configuration that can add additional items
	},

	addDynamicTooltip: function(grid, store, view) {
		const gp = this;
		grid.tip = new Ext.ToolTip({
			target: view.mainBody,    // The overall target element.
			delegate: 'div.tooltip-column',// Need the specific column - for it to apply to the row use .x-grid3-row
			//trackMouse: true,         // Moving within the row should not hide the tip
			renderTo: document.body,  // Render immediately so that tip.body can be referenced prior to the first show

			closable: true,
			draggable: true,
			anchor: 'right',
			maxWidth: 500,
			autoWidth: false,
			autoHide: true, // ??? how to skip auto-hide when hovered over the tooltip itself
			dismissDelay: 60000,
			hideDelay: 5000,
			showDelay: 700,
			title: gp.dynamicTooltipTitle,
			tooltipDataCache: [],
			listeners: {              // Change content dynamically depending on which element
				//  triggered the show.
				beforeshow: function(tip) {
					let tipEventName = TCG.getDynamicTooltipColumnEventName(tip.triggerElement);
					tipEventName = tipEventName ? tipEventName : 'GenericTooltipEvent';
					const rowIndex = view.findRowIndex(tip.triggerElement);
					const key = tipEventName + '_' + rowIndex;
					let tipText = undefined;
					if (gp.cacheDynamicTooltip) {
						for (let i = 0; i < tip.tooltipDataCache.length; i++) {
							if (tip.tooltipDataCache[i].key === key) {
								tipText = tip.tooltipDataCache[i].tipText;
							}
						}
					}
					if (TCG.isNull(tipText)) {
						const rw = store.getAt(rowIndex);
						const rwId = rw.id;
						tipText = gp.getTooltipTextForRow(rw, rwId, tipEventName);
						if (gp.cacheDynamicTooltip) {
							tip.tooltipDataCache.push({'key': key, 'tipText': tipText});
						}
					}
					tip.body.dom.innerHTML = tipText;
				},

				'render': function(tip) {
					tip.on('mouseout', function(e) {
						if (!e.within(tip, true)) {
							tip.hide();
						}
					}, this);
				}
			}
		});
	},

	getTooltipTextForRow: function(row, id, tooltipEventName) {
		return ''; // Should be overridden when used
	},

	configureToolsMenu: function(menu) {
		// can override and add additional menu items
	},

	// Can be called after render to default the view to something else before loading the first time
	// Will also update the menu item so it's set as checked
	// But doesn't call "reload" since grid hasn't been loaded yet.
	setDefaultView: function(viewName) {
		this.switchToView(viewName, true);

		const tb = this.getTopToolbar();
		// Increment By two because of the | separators
		for (let i = 0; i < tb.items.length; i = i + 2) {
			const button = tb.items.items[i];
			if (button.text === 'Views') {
				for (let j = 0; j < button.menu.items.length; j++) {
					const mi = button.menu.items.items[j];
					if (mi.text === viewName) {
						// true to suppress checked event so it doesn't try to switch it again
						mi.setChecked(true, true);
						break;
					}
				}
				break;
			}
		}
	},

	// each column may define views that it's included in: viewNames: ['SHORT', 'EXPANDED']
	switchToView: function(viewName, cancelReload) {
		if (this.beforeSwitchToView && typeof this.beforeSwitchToView === 'function') {
			this.beforeSwitchToView(viewName);
		}
		this.switchToViewColumns(viewName);
		// see if url needs to be updated
		if (this.viewNames) {
			// Revert name/url to original if necessary before view change
			if (TCG.isNotBlank(this.nameChangedByView)) {
				this.nameChangedByView = void 0;
				if (TCG.isNotBlank(this.originalName)) {
					this.name = this.originalName;
					this.originalName = void 0;
				}
			}

			for (let i = 0; i < this.viewNames.length; i++) {
				const v = this.viewNames[i];
				if (v.text === viewName && v.url) {
					// Track original name and view that changed it for reverting back on view change
					this.nameChangedByView = viewName;
					this.originalName = this.name;

					this.name = v.url;
					break;
				}
			}
		}
		Promise.resolve(this.switchToViewBeforeReload(viewName))
				.then(cancel => {
					if (!cancelReload && cancel !== true) {
						this.reload();
					}
				});
	},

	switchToViewBeforeReload: function(viewName) {
		// override me if necessary
	},

	// When switching from All Columns view to another view, anything not associated with any views
	// isn't changed - so by showing all, these columns would remain shown after switching views
	// When All Columns is selected - a list of columns that were previously hidden is kept so when we
	// switch to another view these can be hidden again
	allColumnsViewPreviouslyHidden: undefined,
	switchToViewColumns: function(viewName) {
		const cm = this.grid.getColumnModel();
		const l = cm.getColumnCount();
		const grid = this.grid;
		const currentForceFit = grid.view.forceFit;
		let forcefit = this.forceFit;
		if (viewName === 'All Columns') {
			this.allColumnsViewPreviouslyHidden = [];
		}

		// Suspend events to avoid rendering delays; the resume events must follow
		// we can do this because we force width recalc at the end
		cm.suspendEvents();

		try {
			let clearAllColumnsViewPreviouslyHidden = false;
			const offset = (this.rowSelectionModel && this.rowSelectionModel.width) ? 1 : 0; // checkbox selector
			for (let i = offset; i < l; i++) {
				const c = cm.getColumnAt(i);
				if (viewName === 'All Columns') {
					// When showing all - never forceFit
					forcefit = false;
					if (c.hidden === true && !TCG.isTrue(c.exportOnly)) {
						this.allColumnsViewPreviouslyHidden.push(i);
						cm.setHidden(i, false);
					}
				}
				else {
					clearAllColumnsViewPreviouslyHidden = true;
					if (c.viewNames) {
						cm.setHidden(i, (c.viewNames.indexOf(viewName) === -1));
					}
					else if (c.allViews) {
						// Use allViews for columns that apply to everything and don't change column names
						cm.setHidden(i, c.allViews !== true);
					}
					else if (this.allColumnsViewPreviouslyHidden && this.allColumnsViewPreviouslyHidden.indexOf(i) !== -1) {
						cm.setHidden(i, true);
					}
					if (c.viewNameHeaders) {
						let found = false;
						for (let j = 0; j < c.viewNameHeaders.length; j++) {
							const header = c.viewNameHeaders[j];
							if (header.name === viewName) {
								if (header.label) {
									found = true;
									cm.setColumnHeader(i, header.label);
									if (header.widthMultiplier) {
										cm.setColumnWidth(i, cm.getColumnWidth(i) * header.widthMultiplier / (c.lastWidthMultiplier || 1));
										c.lastWidthMultiplier = header.widthMultiplier;
									}
									if (header.exportDataType) {
										c.exportDataType = header.exportDataType;
									}
									if (header.exportColumnValueConverter) {
										c.exportColumnValueConverter = header.exportColumnValueConverter;
									}
								}
							}
						}
						if (!found && c.defaultHeader) {
							// If not found - reset to default column header
							if (typeof c.defaultHeader == 'string') {
								cm.setColumnHeader(i, c.defaultHeader);
							}
							else {
								if (c.defaultHeader.label) {
									cm.setColumnHeader(i, c.defaultHeader.label);
								}
								if (c.defaultHeader.exportDataType) {
									c.exportDataType = c.defaultHeader.exportDataType;
								}
								if (c.defaultHeader.exportColumnValueConverter) {
									c.exportColumnValueConverter = c.defaultHeader.exportColumnValueConverter;
								}
							}
						}
					}
				}
			}
			if (TCG.isTrue(clearAllColumnsViewPreviouslyHidden)) {
				this.allColumnsViewPreviouslyHidden = undefined;
			}
			// update column widths after all column visibility updated
			this.updateAllColumnWidthsFORCE();
			// update view
			if (currentForceFit !== forcefit) {
				grid.view.forceFit = forcefit;
				grid.view.init(grid);
				grid.view.render();
			}
			this.currentViewName = viewName;
		}
		finally {
			// Rusume events
			cm.resumeEvents();
		}
	},

	updateAllColumnWidthsFORCE: function() {
		const v = this.grid.getView();
		if (v.cm) {
			v.cm.totalWidth = null; // force recalc
			v.fitColumns(true, false, 0);
			v.updateAllColumnWidths();
		}
	},

	// invoked before returning filters data; can be used to add additional filters or modify existing
	onBeforeReturnFilterData: function(filters) {
		// empty
	},

	// invoked before returning filters data; can be used to add additional filters or modify existing
	onBeforeCreateFilters: function(filters) {
		// empty
	},

	// adds dependency between filters where used - i.e. filter holding account by client account selected
	applyDependencyFilters: function(filters) {
		const gridPanel = this;
		for (let i = 0; i < filters.length; i++) {
			const filter = filters[i];
			// Note: If needed could change to support an array of dependency filters - not needed at this time
			if (filter.dependencyFilter) {
				const filterName = filter.dependencyFilter.filterName;
				const paramName = filter.dependencyFilter.parameterName;
				if (filterName && paramName) {
					filter.beforequery = function(queryEvent) {
						const combo = queryEvent.combo;
						const dependentFilter = gridPanel.grid.filters.getFilter(filterName);
						combo.resetStore();
						if (dependentFilter && dependentFilter.active === true && dependentFilter.combo && dependentFilter.combo.getValue() !== '') {
							combo.store.setBaseParam(paramName, dependentFilter.combo.getValue());
						}
						else {
							combo.store.setBaseParam(paramName, '');
						}
					};
				}
			}
		}
	},

	isFilterValueSet: function(dataIndex) {
		const currentFilter = this.grid.filters.getFilter(dataIndex);
		const v = currentFilter.getValue();
		if (typeof v == 'object') {
			// at least one property is set
			return Object.keys(v).length > 0;
		}
		return TCG.isNotNull(v);
	},

	setFilterValueWithSerialArgs: function(dataIndex, serialArgs, doNotCancelReload, activate) {
		const currentFilter = this.grid.filters.getFilter(dataIndex);
		if (currentFilter) {
			let customComparison = false;
			if (Array.isArray(serialArgs)) {
				serialArgs.forEach(serialArg => {
					if (currentFilter.filterConditions && currentFilter.filterConditions.length > 0) {
						customComparison = serialArg.comparison;
					}
					let value = serialArg.value;
					if (TCG.isEqualsStrict(serialArg.dataTypeName, 'DATE')) {
						if (TCG.isEqualsStrict(serialArg.comparison, 'EQUALS')) {
							value = {'on': new Date(value)};
						}
						else if (TCG.isEqualsStrict(serialArg.comarison, 'LESS_THAN')) {
							value = {'before': new Date(value)};
						}
						else if (TCG.isEqualsStrict(serialArg.comparison, 'GREATER_THAN')) {
							value = {'after': new Date(value)};
						}
					}
					if (TCG.isEqualsStrict(serialArg.dataTypeName, 'DECIMAL')) {
						if (TCG.isEqualsStrict(serialArg.comparison, 'EQUALS')) {
							value = {'eq': value};
						}
						else if (TCG.isEqualsStrict(serialArg.comarison, 'NOT_EQUALS')) {
							value = {'ne': value};
						}
						else if (TCG.isEqualsStrict(serialArg.comparison, 'GREATER_THAN')) {
							value = {'gt': value};
						}
						else if (TCG.isEqualsStrict(serialArg.comparison, 'LESS_THAN')) {
							value = {'lt': value};
						}
					}
					this.setFilterValue(dataIndex, value, doNotCancelReload, activate, customComparison);
				});
			}
			else {
				if (currentFilter.filterConditions && currentFilter.filterConditions.length > 0) {
					customComparison = serialArgs.comparison;
				}
				this.setFilterValue(dataIndex, serialArgs.value, doNotCancelReload, activate, customComparison);
			}
		}
	},

	setFilterValue: function(dataIndex, value, doNotCancelReload, activate, customComparison) {
		const filters = this.grid.filters;
		const currentFilter = filters.getFilter(dataIndex);

		// if this value is already set, activate the filter
		if (currentFilter.getValue() === value && currentFilter.active === false) {
			currentFilter.setActive(true);
		}
		else {
			currentFilter.setValue(value);
		}
		if (customComparison && currentFilter.combo) {
			currentFilter.combo.setValue(customComparison);
			currentFilter.comparison = customComparison;
		}
		// cancel reloading triggered by setting the filter
		if (!doNotCancelReload) {
			filters.deferredUpdate.cancel();
		}
		if (activate === true) {
			currentFilter.setActive(currentFilter.isActivatable(), true);
		}
	},

	clearFilter: function(dataIndex, suppressEvent) {
		const filters = this.grid.filters;
		const currentFilter = filters.getFilter(dataIndex);
		currentFilter.setActive(false, suppressEvent);
	},

	clearFilters: function(suppressEvent) {
		this.grid.filters.filters.each(function(filter) {
			filter.setActive(false, suppressEvent);
		});
	},

	addToolbarFilters: function(toolbar, gridPanel) {
		const filters = this.getTopToolbarFilters(toolbar);
		if (TCG.isNotNull(toolbar) && TCG.isNotNull(filters)) {
			toolbar.add('->');
			for (let i = 0; i < filters.length; i++) {
				let filter = filters[i];
				// labels are not displayed by default in the toolbar
				if (filter.fieldLabel) {
					const labelSeparator = (filter.labelSeparator !== undefined) ? filter.labelSeparator : ':';
					const tabLabelField = {xtype: 'tbtext', text: '&nbsp;' + filter.fieldLabel + labelSeparator + '&nbsp;', hidden: filter.hidden};
					if (filter.tooltip) {
						tabLabelField.qtip = filter.tooltip;
					}
					toolbar.add(tabLabelField);
				}
				if (filter.linkedFilter) {
					// on toolbar filter change, update menu filter
					let ls = filter.listeners;
					if (!ls) {
						ls = {};
						filter.listeners = ls;
					}
					if (!ls.select) {
						ls.select = function(combo, record) {
							gridPanel.setFilterValue(combo.linkedFilter, {value: combo.getValue(), text: combo.getRawValue()}, !combo.linkedFilterCancelReload);
						};
					}
					if (!ls.blur) {
						ls.blur = function(combo) {
							if (combo.getValue() === '') { // clear on clear
								gridPanel.setFilterValue(combo.linkedFilter, {value: '', text: ''}, !combo.linkedFilterCancelReload);
							}
						};
					}
				}
				filter = toolbar.add(filter);
				if (filter.linkedFilter) {
					const linkedColumnFilter = this.grid.filters.getFilter(filter.linkedFilter);
					if (TCG.isNotNull(linkedColumnFilter) && TCG.isNotNull(linkedColumnFilter.combo)) {
						// on menu filter change update toolbar filter
						const mf = linkedColumnFilter.combo;
						mf.linkedCombo = filter;
						mf.addListener('select', function(combo, record) {
							const filter = combo.linkedCombo;
							filter.lastSelectionText = combo.getRawValue();
							Ext.form.ComboBox.superclass.setValue.call(filter, filter.lastSelectionText);
							filter.value = combo.getValue();
						});
					}
				}
			}
		}
	},

	getTopToolbarFilters: function(toolbar) {
		if (this.topToolbarSearchParameter) {
			return [{fieldLabel: 'Search', width: 130, xtype: 'toolbar-textfield', name: 'searchPattern'}];
		}
		return undefined;
	},

	addToolbarButtonsAfter: undefined,

	resetToolbar: function(toolbar, gridPanel) {
		//Reload toolbar items (to update 'approve/unapprove' button)
		toolbar.removeAll();
		gridPanel.generateToolbar(gridPanel);
	},

	/**
	 * Returns an array of menu items to append to a context menu for a cell/row/grid. Column, row, and grid context menus should be
	 * appended together. The default implementation return an empty array for no items. It can be overridden to add context menu items
	 * particular to the provided row.
	 *
	 * @param grid - the grid provided in the contextmenu event, is not the gridpanel.
	 * @param rowIndex - the row the contextmenu is to be displayed for
	 * @param record - the record of the grid for the rowIndex
	 * @returns {Array} of filter menu items based on column data type
	 */
	getGridRowContextMenuItems: async function(grid, rowIndex, record) {
		return [];
	},

	/**
	 * Returns an array of context menu items composed of the combined arrays from {@link #getGridRowContextMenuItems} and {@link #getGridCellContextMenuFilterItems}.
	 *
	 * @param grid - the grid provided in the contextmenu event, is not the gridpanel.
	 * @param rowIndex - the row the contextmenu is to be displayed for
	 * @param columnIndexOrColumn - the column ID or column object the contextmenu is to be displayed for
	 * @returns {[]} an array of context meny itmes for a cell, including row items
	 */
	getGridCellContextMenuItems: async function(grid, rowIndex, columnIndexOrColumn) {
		const contextMenuItems = [];

		// append row context menu items
		const record = grid.getStore().getAt(rowIndex);
		const rowMenuItems = await grid.ownerGridPanel.getGridRowContextMenuItems(grid, rowIndex, record);
		if (rowMenuItems && rowMenuItems.length > 0) {
			contextMenuItems.push(...rowMenuItems);
		}

		// append column context menu items
		const filterItems = await grid.ownerGridPanel.getGridCellContextMenuFilterItems(grid, rowIndex, columnIndexOrColumn);
		if (filterItems && filterItems.length > 0) {
			if (contextMenuItems.length > 0) {
				contextMenuItems.push('-');
			}
			contextMenuItems.push(...filterItems);
		}

		return contextMenuItems;
	},

	/**
	 * Returns an array of menu items that can be added to context menu for a column or grid cell. The items returned
	 * will vary in length depending on the column data type (e.g. string/default will use =; date will use =, >, and <;
	 * numeric will use =, >, <, and not equal).
	 *
	 * This method is used by the grid for the cellcontextmenu when the gridpanel is created with enableCellContextMenuFilterItems = true.
	 *
	 * For combo filters, the entity ID is attempted to be looked up by the column's defined idDataIndex or dataIndex sibling ID property
	 * (e.g. if dataIndex = investmentAccount.name, idDataIndex = investmentAccount.id). If the ID cannot be found on the record/row, an
	 * attempt to look the entity up from the server using the dataIndex property. If the property is label, and the entity's sibling name
	 * is not present, the entity cannot be looked up. The solution for this, is to add idDataIndex to the column definition.
	 *
	 * @param grid - the grid provided in the contextmenu event, is not the gridpanel.
	 * @param rowIndex - the row the contextmenu is to be displayed for
	 * @param columnIndexOrColumn - the column ID or column object the contextmenu is to be displayed for
	 * @returns {Array} of filter menu items based on column data type
	 */
	getGridCellContextMenuFilterItems: async function(grid, rowIndex, columnIndexOrColumn) {
		const column = Ext.isNumber(columnIndexOrColumn) ? grid.getColumnModel().getColumnById(columnIndexOrColumn) : columnIndexOrColumn,
				filter = grid.filters.getFilter(column.dataIndex),
				filterItems = [];

		if (TCG.isNotNull(filter)) {
			// column supports filtering
			const record = grid.store.getAt(rowIndex);
			const dataIndex = column.dataIndex;
			let value = record.get(dataIndex);
			const valueList = [];
			const rows = grid.getSelectionModel().getSelections();
			if (rows.length > 0) {
				for (let i = 0; i < rows.length; i++) {
					let value = rows[i].get(dataIndex);
					//Reconciliation uses arrays for displaying two values as if they are one row - typically a check that is unnecessary in most cases.
					if (Array.isArray(value)) {
						value = value[0];
					}
					valueList.push(value);
				}
			}
			// Custom Json Fields - the dataIndex can represent the object so if value.text - use that as the "value"
			// May also need to supply idDataIndex to properly pull the id of the record (see system-shared for the GridPanelWithCustomJsonFields as an example)
			if (Ext.isObject(value) && value.text) {
				value = value.text;
			}
			// used to disable the filter if unable to find unique value to filter with
			let canFilter = true;
			// following fields are used specifically for combo filters
			// the combos us an object value to filter (label and id)
			let originalValue = value;
			let idDataIndex = column.idDataIndex;
			let idValue = void 0;
			const dataIndexLastPeriodIndex = dataIndex.lastIndexOf('.');
			let entityProperty = dataIndex.substring(dataIndexLastPeriodIndex + 1);
			// entity property displayed in column
			const entityDisplayProperty = TCG.isNotNull(column.filter) ? column.filter.displayField : void 0;
			// entity property displayed in combo filter
			let entityLookupParams = void 0;

			// Validate combo details; determine and prepare if a lookup is necessary for the filter entity ID
			if (TCG.isEqualsStrict(filter.type, 'combo')) {
				if (TCG.isNotBlank(idDataIndex)) {
					idValue = TCG.getValue(idDataIndex, record.json);
				}
				else if (!TCG.isTrue(filter['nonPersistentField']) && (dataIndexLastPeriodIndex > -1 || dataIndex.endsWith('Id'))) {
					if ((dataIndexLastPeriodIndex > -1)) {
						idDataIndex = dataIndex.substring(0, dataIndexLastPeriodIndex + 1) + 'id';
						idValue = TCG.getValue(idDataIndex, record.json);
					}
					else {
						// set entityProperty to ID for looking up idValue with combo URL
						// an example of this usage is when UI uses updateUserId of BaseUpdatableOnlyEntity
						entityProperty = 'id';
					}
				}
				else {
					// unable to filter without an ID for the combo
					canFilter = false;
				}

				if (TCG.isTrue(canFilter) && TCG.isBlank(idValue) && TCG.isNotNull(column.filter) && TCG.isNotBlank(column.filter.url)) {
					// will try to look up the entity from the server
					let entityLookupProperty = entityProperty;
					entityLookupParams = {limit: 100, requestedPropertiesRoot: 'data.rows', requestedProperties: 'id|' + entityProperty};
					if (TCG.isNotBlank(entityDisplayProperty) && TCG.isNotEqualsStrict(entityDisplayProperty, entityProperty)) {
						entityLookupParams['requestedProperties'] += ('|' + entityDisplayProperty);
					}

					if (TCG.isEqualsStrict('label', entityProperty) || TCG.isEqualsStrict('name', entityProperty)) {
						if (TCG.isEqualsStrict('label', entityProperty)) {
							// check if we have name instead of label
							const nameValue = TCG.getValue(dataIndex.substring(0, dataIndexLastPeriodIndex + 1) + 'name', record.json);
							if (TCG.isNotBlank(nameValue)) {
								// look up by name
								entityLookupProperty = 'name';
								value = nameValue;
							}
							else {
								// convert for looking up by label
								entityLookupProperty = grid.ownerGridPanel.getGridCellComboLabelFilterSearchField(column);
								value = grid.ownerGridPanel.getGridCellComboLabelFilterValue(column, value);
							}
						}
					}
					// use restriction list to ensure equals is used
					entityLookupParams['restrictionList'] = JSON.stringify([{comparison: 'EQUALS', value: value, field: entityLookupProperty}]);
				}
			}

			if (filter.convert) {
				value = filter.convert(value, record);
			}

			if (TCG.isTrue(canFilter)) {
				const filterHandler = async function(comparison) {
					let filterValue = void 0;
					let customComparison;
					if (TCG.isEqualsStrict(filter.type, 'combo')) {
						if (TCG.isNotNull(entityLookupParams)) {
							// create promise for looking up entity
							idValue = await TCG.data.getDataPromise(column.filter.url, grid, {params: entityLookupParams})
									.then(function(entity) {
										let matchingEntity = void 0;
										if (Array.isArray(entity) || (('object' === typeof entity) && TCG.isNotNull(entity['rows']))) {
											// paging array list may be one or more entities
											const entities = Array.isArray(entity) ? entity : entity['rows'];
											if (entities.length === 1) {
												matchingEntity = entities[0];
											}
											else {
												// try to find exact match in returned rows
												const matches = {};
												entities.forEach(e => {
													if (e[entityProperty] === originalValue) {
														matches[e[entityProperty]] = e;
													}
												});
												const matchingObjects = Object.values(matches);
												if (matchingObjects.length === 1) {
													matchingEntity = matchingObjects[0];
												}
											}
										}
										else if (TCG.isNotNull(entity)) {
											matchingEntity = entity;
										}

										if (TCG.isNull(matchingEntity)) {
											// alert the user
											Ext.Msg.alert('Indeterminate Filter Value', 'Unable to determine the exact value to filter by. Please consider adding the \'idDataIndex: '
													+ idDataIndex + '\' property to column: ' + column.header);
											return void 0;
										}

										// update originalValue for combo display
										if (TCG.isNotBlank(entityDisplayProperty) && TCG.isNotEqualsStrict(originalValue, matchingEntity[entityDisplayProperty])) {
											originalValue = matchingEntity[entityDisplayProperty];
										}
										return matchingEntity.id;
									});
						}

						if (TCG.isNotBlank(idValue)) {
							filterValue = {};
							filterValue[(comparison === 'eq' ? 'equals' : 'not_equals')] = {text: originalValue, value: idValue};
						}
					}
					else if (filter.filterConditions && filter.filterConditions.length > 0) {
						if (TCG.isEqualsStrict(comparison, 'IN')) {
							filterValue = (valueList.length > 0) ? valueList.join(' <OR> ') : value;
						}
						else {
							filterValue = (valueList.length > 0) ? valueList[0] : value;
						}
						customComparison = comparison;
					}
					else if (TCG.isNotBlank(value)) {
						if (TCG.isEqualsStrict(filter.type, 'boolean')) {
							// ensures the value is a boolean for conversion
							const booleanValue = TCG.isTrue(value);
							filterValue = (TCG.isEqualsStrict(comparison, 'eq') ? booleanValue : !booleanValue);
						}
						else if (TCG.isEqualsStrict(filter.type, 'string') || TCG.isEqualsStrict(filter.type, 'list') || TCG.isEqualsStrict(filter.type, 'auto')) {
							filterValue = value;
						}
						else {
							filterValue = {};
							filterValue[comparison] = TCG.isEqualsStrict(filter.type, 'date') ? new Date(value) : value;
						}
					}
					else if (TCG.isTrue(filter['filterNull'])) {
						if (TCG.isEqualsStrict(filter.type, 'date')) {
							filterValue = {};
							filterValue['on'] = value;
						}
					}

					if (TCG.isNotNull(filterValue)) {
						grid.ownerGridPanel.setFilterValue(dataIndex, filterValue, true, true, customComparison);
					}
				};

				if (!filter.filterConditions || !filter.filterConditions.length > 0) {
					filterItems.push({
						text: 'Filter ' + (TCG.isEqualsStrict(filter.type, 'date') ? 'On' : 'Equal To') + ' Cell Value',
						iconCls: 'comparison-equals',
						handler: function() {
							filterHandler(TCG.isEqualsStrict(filter.type, 'date') ? 'on' : 'eq');
						}
					});
				}

				const numericFilter = TCG.isEqualsStrict(filter.type, 'int') || TCG.isEqualsStrict(filter.type, 'float');
				if (filter.filterConditions && filter.filterConditions.length > 0) {
					filter.filterConditions.forEach(function(filterCondition) {
						filterItems.push({
							text: filterCondition[2],
							tooltip: filterCondition[3],
							iconCls: filterCondition[4],
							handler: function() {
								filterHandler(filterCondition[0]);
							}
						});
					});
				}
				else if (TCG.isEqualsStrict(filter.type, 'date')) {
					filterItems.push({
						text: 'Filter After Cell Value',
						iconCls: 'comparison-greater-than',
						handler: function() {
							filterHandler('after');
						}
					}, {
						text: 'Filter Before Cell Value',
						iconCls: 'comparison-less-than',
						handler: function() {
							filterHandler('before');
						}
					});
				}
				else if (numericFilter) {
					filterItems.push({
						text: 'Filter Greater Than Cell Value',
						iconCls: 'comparison-greater-than',
						handler: function() {
							filterHandler('gt');
						}
					}, {
						text: 'Filter Less Than Cell Value',
						iconCls: 'comparison-less-than',
						handler: function() {
							filterHandler('lt');
						}
					});
				}

				if (TCG.isEqualsStrict(filter.type, 'boolean') || numericFilter || (TCG.isEqualsStrict(filter.type, 'combo') && TCG.isTrue(filter.showNotEquals))) {
					filterItems.push({
						text: 'Filter Not Equal To Cell Value',
						iconCls: 'comparison-not-equals',
						handler: function() {
							filterHandler('ne');
						}
					});
				}
			}
			// can always clear
			filterItems.push({
				text: 'Clear Filter(s)',
				tooltip: 'Removes any filters that are applied to this column.',
				iconCls: 'clear',
				handler: function() {
					filter.setActive(false);
				}
			});

			return filterItems;
		}

		return [];
	},

	/**
	 * Overridable method for getting the search field name when a column uses an entity label as the dataIndex and a combo filter.
	 *
	 * The default is to use searchPattern with an equals comparison.
	 *
	 * @param column - the column being filtered
	 */
	getGridCellComboLabelFilterSearchField: function(column) {
		return 'searchPattern';
	},

	/**
	 * Overridable method for converting a column value when the column uses an entity label as the dataIndex and a combo filter.
	 *
	 * The default is return a substring from index 0 to the first ': ' or ' (' match. If there is no match, the value argument is returned as is.
	 *
	 * @param column - the column being filtered
	 * @param value - the value to convert for entity lookup
	 */
	getGridCellComboLabelFilterValue: function(column, value) {
		if (value.includes(': ')) {
			return value.substring(0, value.indexOf(': '));
		}
		else if (value.includes(' (')) {
			return value.substring(0, value.indexOf(' ('));
		}
		return value;
	},

	getWindow: function() {
		let result = this.findParentByType(Ext.Window);
		if (TCG.isNull(result)) {
			result = this.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			});
		}
		return result;
	},

	getBottomToolbar: function() {
		return this.grid.getBottomToolbar();
	},

	// Ability to Have the Grid Auto Reload for Specified Duration @ Specified Intervals
	// Defaults to Every 3 Seconds for 30 Seconds
	reloadTask: undefined, // Task for reloading so we can stop it and restart it
	reloadRepeated: function(interval, duration) {
		const grid = this;
		if (!interval) {
			interval = 3000;
		}

		if (!duration) {
			duration = 30000;
		}
		else if (duration <= 0) {
			duration = undefined;
		}

		if (grid.reloadTask) {
			Ext.TaskMgr.stop(grid.reloadTask);
		}
		this.reloadTask = {
			run: grid.reload,
			scope: grid,
			interval: interval,
			duration: duration
		};

		Ext.TaskMgr.start(this.reloadTask);
	},

	reload: function(firstPage) {
		if (this.autoLoad === false) {
			this.autoLoad = true;
		}
		else if (this.ds.proxy && this.reloadIsRunning !== true) {
			// don't allow reloading too quickly: avoid double clicks.
			this.reloadIsRunning = true;
			this.doReload(firstPage);
			(function() {
				this.reloadIsRunning = false;
			}).defer(750, this);
			this.fireEvent('afterReload', this);
		}
	},

	doReload: function(firstPage) {
		const gp = this;
		gp.setStoreBaseParams()
				.then(cancelReload => {
					if (!TCG.isTrue(cancelReload)) {
						gp.ds.proxy.setUrl(encodeURI(TCG.getApplicationUrl(gp.getLoadURL(), gp)));
						const toolBar = gp.grid.getBottomToolbar();
						if (toolBar) {
							toolBar.doLoad(firstPage === true ? 0 : toolBar.cursor);
						}
						else {
							gp.ds.load();
						}
					}
				});
	},

	/**
	 * When reloading the grid, the store's base params should be set based on the result of the grid's {@link #getLoadParams} and column data indices.
	 * When the grid's underlying store is reloaded from underneath the grid, such as with certain filter criteria setting, the same base params should be populated
	 * so the requestedProperties and requestedPropertiesRoot parameters are set.
	 *
	 * @param options an optional object of options for the grid's store event handling and can be used to populate params
	 * @returns {Promise<boolean>} a promise that will resolve to true if the reload should be cancelled
	 */
	setStoreBaseParams: function(options = {}) {
		const gp = this;
		// If initiated by the grid reload, the options will be defaulted. In this case, set the baseParams on the store when making the query.
		const useBaseParams = !(options.params);
		if (useBaseParams) {
			options.params = {};
		}

		/*
		 * Define limit request properties if applicable. It is important that this gets done prior to handling any potential promises below.
		 * Since this function can be handled within event processing (e.g. beforeload on the store), there is no way to ensure the promise
		 * handling gets performed before the actual request is made to the server. At a minimum we will get request property limits applied.
		 * The grid reload functionality expects a promise and will always handle the promise chain.
		 */
		if (gp.limitRequestedProperties) {
			if (gp.grid.store) {
				options.params.requestedPropertiesRoot = gp.grid.store.root;
				const columnProperties = [];
				for (let i = 0; i < gp.grid.getColumnModel().getColumnCount(); i++) {
					const c = gp.grid.getColumnModel().getColumnAt(i);
					if (!TCG.isTrue(c.doNotRequest)) {
						if (c.dataIndex) {
							columnProperties.push(c.dataIndex);
						}
						if (TCG.isTrue(gp.requestIdDataIndex) && TCG.isNotBlank(c.idDataIndex)) {
							columnProperties.push(c.idDataIndex);
						}
					}
				}
				if (TCG.isNotBlank(gp.additionalPropertiesToRequest)) {
					columnProperties.push(gp.additionalPropertiesToRequest);
				}
				options.params.requestedProperties = columnProperties.join('|');
			}
		}

		// supports both static parameters as well a Promise that can be chained for asynchronous parameter retrieval
		const loadParams = gp.getLoadParams(gp.firstLoadProcessed !== true);
		return Promise.resolve(loadParams)
				.then(function(resolvedLoadParams) {
					gp.firstLoadProcessed = true;
					if ((typeof resolvedLoadParams === 'boolean' && !resolvedLoadParams)) {
						return true;
					}
					const resultParams = Ext.applyIf(options.params, resolvedLoadParams);
					if (useBaseParams) {
						gp.ds.baseParams = resultParams;
					}
					return false;
				});
	},

	updateCount: function() {
		if (this.getBottomToolbar()) {
			return;
		} // already has counts
		const t = this.getTopToolbar();
		if (t && t.items && t.items.length > 0) {
			const label = 'Records: ' + this.grid.getStore().getCount() + ' ';
			const last = t.items.get(t.items.length - 1);
			if (last.text && last.text.indexOf('Records:') === 0) {
				last.setText(label);
			}
			else {
				t.add(this.topToolbarUpdateCountPrefix, {xtype: 'tbtext', text: label});
			}
			t.syncSize();
		}
	},

	isEditEnabled: function() {
		return this.editor ? true : false;
	},

	isPagingEnabled: function() {
		return this.dataTable || TCG.isUrlWithPagination(this.name);
	},

	getLoadURL: function() {
		let url = this.loadURL;
		if (!url) {
			url = this.name;
			if (url.indexOf('.json') === -1) {
				url += '.json';
			}
		}
		return url;
	},
	getExportURL: function() {
		let url = this.exportURL;
		if (!url) {
			url = this.getLoadURL();
		}
		else if (url.indexOf('.json') === -1) {
			url += '.json';
		}
		return url;
	},

	// request parameters to be sent during reload: { id: 555 }
	// return false to cancel reload
	getLoadParams: function(firstLoad) {
		if (this.topToolbarSearchParameter) {
			const ttParams = this.getTopToolbarInitialLoadParams(firstLoad);
			if ((typeof ttParams === 'boolean' && !ttParams)) {
				return false; // cancel reload
			}
			const params = ttParams || {};
			const searchPattern = TCG.getChildByName(this.getTopToolbar(), 'searchPattern');
			if (firstLoad) {
				searchPattern.focus(false, 500);
			}
			if (searchPattern.getValue() !== '') {
				params[this.topToolbarSearchParameter] = searchPattern.getValue();
			}
			return params;
		}

		return undefined;
	},

	// return false to cancel reload
	getTopToolbarInitialLoadParams: function(firstLoad) {
		return {};
	},

	exportGrid: function(outputFormat, includeHidden, selectedRows) {
		const gp = this;
		const options = {};
		const params = gp.getExportGridParams(options, selectedRows);
		Promise.resolve(params).then(function(params) {
			if (gp.maxLimit) {
				params.maxLimit = gp.maxLimit;
			}
			if (gp.beanCollectionToDataTableConverter) {
				params.beanCollectionToDataTableConverter = gp.beanCollectionToDataTableConverter;
			}
			params.columns = encodeURI(gp.getColumnsMetaData(includeHidden));
			params.outputFormat = outputFormat;

			const fileName = gp.getExportFileName();
			if (fileName) {
				params.fileName = fileName;
			}
			// options.params does not get defined if there are no filters and Ext.apply will do nothing if options.params is undefined
			if (!options.params) {
				options.params = {};
			}
			Ext.apply(options.params, params);
			TCG.downloadFile(gp.getExportURL(), options.params, gp);
			gp.grid.loadMask.onLoad();
		});
	},

	getExportGridParams(options, selectedRows) {
		const gridPanel = this;
		// additional params (filters, etc.) maybe added before load and mask needs to be hidden after
		// NOTE: Need to do this first before applying getLoadParams(). Appears to clear some filters if done afterwards
		gridPanel.ds.fireEvent('beforeload', gridPanel, options);

		// send fields/order/format info to the server and use it to generate exports
		let params = gridPanel.getLoadParams(gridPanel.firstLoadProcessed !== true);
		params = gridPanel.getAdditionalExportParams(params);
		if (TCG.isNull(params)) {
			params = {};
		}
		//If a param is null it should be submitted as an empty value instead of null
		for (const param in params) {
			if (params.hasOwnProperty(param)) {
				const value = params[param];
				if (TCG.isNull(value)) {
					params[param] = '';
				}
			}
		}
		if (selectedRows) {
			if (gridPanel.rowSelectionModel && !gridPanel.rowSelectionModel.singleSelect) {
				const selectionModel = gridPanel.grid.getSelectionModel();
				params['ids'] = (selectionModel.getSelections() || []).map(v => v.get('id')).distinct();
			}
			else {
				TCG.showError('Unable to export selected rows!  Grid does not support multiple selections!');
			}
		}
		return params;
	},

	getExportFileName: function() {
		if (this.exportFileName) {
			return this.exportFileName;
		}
		const tp = TCG.getParentTabPanel(this);
		if (tp && tp.getActiveTab() && tp.getActiveTab().title) {
			return tp.getActiveTab().title;
		}
		if (this.getWindow().title) {
			return this.getWindow().title;
		}
		return undefined;
	},
	getAdditionalExportParams: function(params) {
		//Override to change or add additional params explicitly for exports.
		return params;
	},
	getColumnsMetaData: function(includeHidden) {
		const gridPanel = this;
		const cols = gridPanel.grid.getColumnModel().config;
		if (TCG.isNotNull(cols)) {
			let columnString = '';
			const gridPanelHasSkipFunction = Ext.isFunction(this.isSkipExport);
			Ext.each(cols, function(f) {
				// Always skip if no data index or export data index
				if ((f.dataIndex && f.dataIndex !== '') || (f.exportDataIndex && f.exportDataIndex !== '')) {
					// Only care about non hidden columns or those not explicitly set to skip
					// exportOnly is hidden, but still always want to include in exports
					if ((!TCG.isTrue(f.hidden) || includeHidden || TCG.isTrue(f.exportOnly)) && TCG.isNotNull(f.header) && !TCG.isTrue(f.skipExport) && (!gridPanelHasSkipFunction || !gridPanel.isSkipExport.call(gridPanel, f))) {
						if (f.exportHeader) {
							columnString += (f.exportHeader + ':');
						}
						else {
							columnString += (f.header + ':');
						}
						columnString += ((f.exportDataIndex && f.exportDataIndex !== '' ? f.exportDataIndex : f.dataIndex) + ':');

						let styleString = '';
						if (TCG.isNotNull(f.align)) {
							styleString += ('align=' + f.align + ',');
						}
						if (TCG.isNotNull(f.width)) {
							styleString += ('width=' + f.width + ',');
						}
						if (TCG.isNotNull(f.exportDataType)) {
							styleString += ('dataType=' + f.exportDataType + ',');
						}
						styleString += ':';
						columnString += styleString;
						if (TCG.isNotNull(f.exportColumnValueConverter)) {
							columnString += f.exportColumnValueConverter;
						}
						columnString += '|';
					}
				}
			});
			return columnString;
		}
	},


	setColumnHidden: function(dataIndex, hidden) {
		if (TCG.isNotBlank(dataIndex)) {
			const columnModel = this.getColumnModel();
			columnModel.setHidden(columnModel.findColumnIndex(dataIndex), TCG.isTrue(hidden));
		}
	}
});
Ext.reg('gridpanel', TCG.grid.GridPanel);


TCG.grid.GridEditor = Ext.extend(Ext.util.Observable, {
	// detailPageClass: use to defined the detail page for each grid row; for multiple classes can be an object: {getItemText: function(gridItem) {...}, items: [{text: 'Item1', className: 'Class1'}, ...]})

	applicationName: '', //Defines the application that will be targeted (proxy to) for the request (e.g. applicationName: 'reconciliation', results in 'reconciliation/<url>.json')
	// deleteURL:       defines delete URL (default impl derives it from GridPanel name)
	// copyURL:         defines copy URL
	copyIdParameter: 'id', // id parameter name used by default copy impl
	copyButtonName: 'Copy', // Used for display for the copy button
	copyButtonTooltip: 'Copy Selected Item',
	drillDownOnly: false, //If true, indicates that Add, Delete, and Copy buttons are not enabled
	addEnabled: true,
	deleteEnabled: true,
	allowToDeleteMultiple: false,
	reloadGridAfterDelete: false, // If true will not remove from store on client side but will reload the grid with a call to the server

	constructor: function(config) {
		Ext.apply(this, config);
	},

	init: function(grid) {
		this.grid = grid;
		this.grid.addListener('rowdblclick', this.startEditing, this);
	},

	isAddEnabled: function() {
		if (this.drillDownOnly) {
			return false;
		}
		return this.addEnabled;
	},

	isDeleteEnabled: function() {
		if (this.drillDownOnly) {
			return false;
		}
		return this.deleteEnabled;
	},

	isCopyEnabled: function() {
		if (this.drillDownOnly) {
			return false;
		}
		return this.copyURL ? true : false;
	},

	getGridPanel: function() {
		return this.grid.ownerCt;
	},

	getWindow: function() {
		return this.getGridPanel().getWindow();
	},

	addEditButtons: function(toolBar, gridPanel) {
		if (this.isAddEnabled()) {
			this.addToolbarAddButton(toolBar, gridPanel);
		}
		if (this.isDeleteEnabled()) {
			this.addToolbarDeleteButton(toolBar, gridPanel);
		}
		if (this.isCopyEnabled()) {
			toolBar.add({
				text: this.copyButtonName,
				tooltip: this.copyButtonTooltip,
				iconCls: 'copy',
				scope: this,
				handler: function() {
					this.copyHandler(gridPanel);
				}
			});
			toolBar.add('-');
		}
	},

	startEditing: function(grid, rowIndex, evt) {
		if (evt && TCG.isActionColumn(evt.target)) {
			return; // skip because this is action column
		}
		const gridPanel = this.getGridPanel();
		const row = grid.store.data.items[rowIndex];
		const clazz = this.getDetailPageClass(grid, row);
		if (clazz) {
			const id = this.getDetailPageId(gridPanel, row);
			if (id !== undefined) {
				this.openDetailPage(clazz, gridPanel, id, row);
			}
		}
	},

	getDetailPageId: function(gridPanel, row) {
		return row.id;
	},
	getDetailPageParams: function(id) {
		return {'id': id};
	},

	getDetailPageClass: function(grid, row) {
		let result = undefined;
		const type = Ext.type(this.detailPageClass);
		if (type === 'string') {
			result = this.detailPageClass;
		}
		else if (type === 'object') {
			const text = this.detailPageClass.getItemText(row);
			result = this.getDetailPageClassByText(text);
		}
		return result;
	},

	getDetailPageClassByText: function(text) {
		let result = undefined;
		Ext.each(this.detailPageClass.items, function(item) {
			if (item.text === text) {
				result = item.className;
				return;
			}
		});
		return result;
	},

	openDetailPage: function(className, gridPanel, id, row, itemText, defaultActiveTabName) {
		const defaultData = id ? this.getDefaultDataForExisting(gridPanel, row, className, itemText) : this.getDefaultData(gridPanel, row, className, itemText);
		if (defaultData === false) {
			return;
		}

		const editor = this;
		Promise.resolve(defaultData).then(function(defaultData) {
			const params = id ? editor.getDetailPageParams(id) : undefined;
			const cmpId = TCG.getComponentId(className, id);
			TCG.createComponent(className, {
				id: cmpId,
				defaultData: defaultData,
				params: params,
				openerCt: gridPanel,
				defaultIconCls: gridPanel.getWindow().iconCls,
				defaultActiveTabName: defaultActiveTabName
			});
		});
	},

	// returns JSON representation of defaults for NEW detail pages; return false to cancel detail page open
	getDefaultData: function(gridPanel, row, detailPageClass, itemText) {
		return undefined;
	},
	// returns JSON representation of defaults for EXISTING detail pages
	getDefaultDataForExisting: function(gridPanel, row, detailPageClass, itemText) {
		return this.getDefaultData(gridPanel, row, detailPageClass, itemText);
	},


	addToolbarAddButton: function(toolBar, gridPanel) {
		if (Ext.type(this.detailPageClass) === 'object') {
			const detailPageItems = [];
			Ext.each(this.detailPageClass.items, function(item) {
				const className = item.className ? item.className : this.getDetailPageClassByText(item.text);
				detailPageItems.push(Ext.apply(item, {
					scope: this,
					handler: function() {
						this.openDetailPage(className, gridPanel, null, null, item.text);
					}
				}));
			}, this);
			toolBar.add({
				text: 'Add',
				tooltip: 'Add a new item',
				iconCls: 'add',
				menu: new Ext.menu.Menu({
					items: detailPageItems
				})
			});
		}
		else if (this.addFromTemplateURL) {
			const editor = this;
			toolBar.add({
				text: 'Add',
				xtype: 'splitbutton',
				tooltip: 'Add a new item',
				iconCls: 'add',
				handler: function() {
					editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
				},
				menu: new Ext.menu.Menu({
					items: [
						{
							text: 'Add',
							tooltip: 'Add a new item',
							iconCls: 'add',
							handler: function() {
								editor.openDetailPage(editor.getDetailPageClass(), gridPanel);
							}
						},
						{
							text: 'Add from Template',
							tooltip: 'Populate new item fields from selected item fields',
							iconCls: 'copy',
							handler: function() {
								const sm = gridPanel.grid.getSelectionModel();
								if (sm.getCount() === 0) {
									TCG.showError('Please select a row to use as the template.', 'No Row(s) Selected');
								}
								else if (sm.getCount() !== 1) {
									TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
								}
								else {
									const loader = new TCG.data.JsonLoader({
										waitTarget: gridPanel,
										params: {id: sm.getSelected().id},
										onLoad: function(record, conf) {
											const pageClass = editor.getDetailPageClass();
											delete record['id'];
											TCG.createComponent(pageClass, {
												id: TCG.getComponentId(pageClass),
												defaultData: record,
												openerCt: gridPanel
											});
										}
									});
									loader.load(editor.addFromTemplateURL);
								}
							}
						}
					]
				})
			});
		}
		else {
			toolBar.add({
				text: 'Add',
				tooltip: 'Add a new item',
				iconCls: 'add',
				scope: this,
				handler: function() {
					this.openDetailPage(this.getDetailPageClass(), gridPanel);
				}
			});
		}
		toolBar.add('-');
	},

	addToolbarDeleteButton: function(toolBar, gridPanel) {
		toolBar.add({
			// Optionally override these on the editor
			text: this.deleteButtonName ? this.deleteButtonName : 'Remove',
			tooltip: this.deleteButtonTooltip ? this.deleteButtonTooltip : 'Remove selected item(s)',
			iconCls: 'remove',
			scope: this,
			handler: function() {
				const editor = this;
				const sm = editor.grid.getSelectionModel();
				if (sm.getCount() === 0) {
					TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
				}
				else if (sm.getCount() !== 1) {
					if (editor.allowToDeleteMultiple) {
						if (editor.deleteDualConfirm) {
							Ext.Msg.prompt('Delete Selected Row(s)', deleteConfirmMessage + '<br /><br />Type \'CONFIRM\' to delete.', function(btn, text) {
								if (btn === 'ok') {
									if (TCG.isEquals('CONFIRM', text)) {
										editor.deleteSelection(sm, gridPanel);
									}
									else {
										Ext.Msg.alert('Object(s) Not Deleted', 'Expected \'CONFIRM\' but user typed \'' + text + '\'');
									}
								}
							});
						}
						else {
							Ext.Msg.confirm('Delete Selected Row(s)', 'Would you like to delete all selected rows(s)?', function(a) {
								if (a === 'yes') {
									editor.deleteSelection(sm, gridPanel);
								}
							});
						}
					}
					else {
						TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
					}
				}
				else {
					const deleteConfirmMessage = editor.deleteConfirmMessage ? editor.deleteConfirmMessage : 'Would you like to delete the selected row?';
					if (editor.deleteDualConfirm) {
						Ext.Msg.prompt('Delete Selected Row', deleteConfirmMessage + '<br /><br />Type \'CONFIRM\' to delete.', function(btn, text) {
							if (btn === 'ok') {
								if (TCG.isEquals('CONFIRM', text)) {
									editor.deleteSelection(sm, gridPanel);
								}
								else {
									Ext.Msg.alert('Object Not Deleted', 'Expected \'CONFIRM\' but user typed \'' + text + '\'');
								}
							}
						});
					}
					else {
						Ext.Msg.confirm('Delete Selected Row', deleteConfirmMessage, function(a) {
							if (a === 'yes') {
								editor.deleteSelection(sm, gridPanel);
							}
						});
					}
				}
			}
		});
		toolBar.add('-');
	},

	deleteSelection: function(selectionModel, gridPanel, deleteUrl) {
		const grid = this.grid;
		const store = grid.store;
		const deletedItemList = [];

		const deleteSelectedItem = () => {
			const selectedItem = selectionModel.getSelected();
			const loader = new TCG.data.JsonLoader({
				waitTarget: grid.ownerCt,
				waitMsg: 'Deleting...',
				params: this.getDeleteParams(selectionModel),
				conf: {rowId: selectedItem.id},
				onLoad: (record, conf) => {
					deletedItemList.push(selectedItem);
					selectionModel.deselectRow(store.indexOfId(selectedItem.id));
					if (selectionModel.getSelected()) {
						// Delete the next selected item
						deleteSelectedItem();
					}
					else {
						// Deletions are complete; refresh changes to the grid
						if (this.reloadGridAfterDelete) {
							gridPanel.reload();
						}
						else {
							deletedItemList.forEach(item => grid.getStore().remove(item));
							grid.ownerCt.updateCount();
						}
					}
				}
			});
			loader.load(deleteUrl || this.getDeleteURL(selectedItem));
		};

		deleteSelectedItem();
	},

	getDeleteURL: function(selectedItem) {
		if (!this.deleteURL) {
			const gridPanel = this.getGridPanel();
			let url = gridPanel.name;
			if (url.length > 4 && url.substring(url.length - 4) === 'List') {
				url = url.substring(0, url.length - 4);
			}
			else if (url.length > 8 && url.substring(url.length - 8) === 'ListFind') {
				url = url.substring(0, url.length - 8);
			}
			else if (url.indexOf('ListBy') !== -1) {
				url = url.substring(0, url.indexOf('ListBy'));
			}
			return url + 'Delete.json';
		}
		return this.deleteURL;
	},

	getDeleteParams: function(selectionModel) {
		return {id: selectionModel.getSelected().id};
	},

	copyHandler: function(gridPanel) {
		const sm = gridPanel.grid.getSelectionModel();
		if (sm.getCount() === 0) {
			TCG.showError('Please select a row to be copied.', 'No Row(s) Selected');
		}
		else if (sm.getCount() !== 1) {
			TCG.showError('Multi-selection copies are not supported.  Please select one row.', 'NOT SUPPORTED');
		}
		else {
			Ext.Msg.prompt('Name', 'Please enter a name for the copied entity:', function(btn, text) {
						if (btn === 'ok') {
							if (text === '') {
								TCG.showError('A name for the copied entity is required.', 'No Name Entered');
								return;
							}
							if (this.copyNameMaxLength && text.length > this.copyNameMaxLength) {
								TCG.showError('The name for the copied entity [' + text + '] is too long.  Please enter a name no longer than [' + this.copyNameMaxLength + '] characters.', 'Name too Long');
								return;
							}
							const params = {name: text};
							params[this.copyIdParameter] = sm.getSelected().id;
							const loader = new TCG.data.JsonLoader({
								waitTarget: gridPanel,
								waitMsg: 'Copying...',
								params: params,
								conf: params,
								onLoad: function(record, conf) {
									gridPanel.reload();
								}
							});
							loader.load(this.copyURL);
						}
					}, this // scope
			);
		}
	}
});
Ext.preg('grideditor', TCG.grid.GridEditor);


// TODO: refactor to GridEditor and remove
TCG.grid.EditorGridPanel = Ext.extend(TCG.grid.GridPanel, {
	clicksToEdit: 1,

	createGrid: function(filtersPlugin) {
		if (TCG.isNotNull(this.editor)) {
			if (!this.editor.ptype) {
				this.editor.ptype = 'grideditor';
			}
			this.editor = Ext.ComponentMgr.createPlugin(this.editor);
		}
		// create a copy of gridConf and add plugins as necessary
		const gridConf = {
			plugins: [],
			...this.gridConfig
		};
		if (TCG.isNotNull(this.editor)) {
			if (this.editor.allowToDeleteMultiple && this.rowSelectionModel === 'single') {
				//If delete multiple is allowed default to multiple selection if set to single
				this.rowSelectionModel = 'multiple';
			}
			gridConf.plugins.push(this.editor);
		}
		if (TCG.isNotNull(filtersPlugin)) {
			gridConf.plugins.push(filtersPlugin);
		}

		// Get view configuration
		const viewConf = {
			forceFit: this.forceFit, scrollDelay: false, groupTextTpl: this.groupTextTpl,
			// make grid text selectable (can copy to clipboard)
			templates: {
				cell: new Ext.Template(
						'<td class="x-grid3-col x-grid3-cell x-grid3-td-{id} x-selectable {css}" style="{style}" tabIndex="0" {cellAttr}>',
						'<div class="x-grid3-cell-inner x-grid3-col-{id}" {attr}>{value}</div>',
						'</td>'
				)
			},
			...this.viewConfig || {}
		};

		// Get view class
		let viewClass;
		if (this.viewConfig && this.viewConfig.xtype) {
			viewClass = Ext.ComponentMgr.types[this.viewConfig.xtype];
		}
		else if (this.groupField) {
			viewClass = Ext.grid.GroupingView;
			viewConf.groupTextTpl = this.groupTextTpl || '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})';
			if (this.getGroup) {
				viewConf.getGroup = this.getGroup;
			}
		}
		else if (this.useBufferView) {
			viewClass = Ext.ux.grid.BufferView;
		}
		else {
			viewClass = Ext.grid.GridView;
		}

		// Generate grid panel
		const gridPanel = this;
		return new Ext.grid.EditorGridPanel({
			ownerGridPanel: this,
			border: false,
			flex: 1, // take 100% of all available space
			loadMask: true,
			stripeRows: true,
			ds: this.ds,
			cm: this.getColumnModel(),
			sm: this.getRowSelectionModel(),
			bbar: this.isPagingEnabled() ? new TCG.toolbar.PagingToolbar({store: this.ds, displayInfo: true, pageSize: this.pageSize}) : undefined,
			clicksToEdit: this.clicksToEdit,
			getTitle: () => this.getWindow().title,
			startEditing: function(row, col) {
				Ext.grid.EditorGridPanel.prototype.startEditing.apply(this, arguments);
				// need a way to get to grid from editable fields: combo, etc. to get form arguments
				const ed = this.colModel.getCellEditor(col, row);
				if (ed) {
					ed.containerGrid = gridPanel;
				}
			},
			view: new viewClass(viewConf),
			...gridConf
		});
	},

	isDirty: function() {
		const rows = this.grid.store.getRange();
		for (let i = 0; i < rows.length; i++) {
			const row = rows[i];
			if (row.dirty === true) {
				return true;
			}
		}
		return false;
	},

	getRowSelectionModel: function() {
		if (typeof this.rowSelectionModel !== 'object') {
			if (this.rowSelectionModel === 'checkbox') {
				this.rowSelectionModel = new Ext.grid.CheckboxSelectionModel({
					getCellEditor: function() {
						return null;
					}
				});
			}
			else {
				this.rowSelectionModel = new Ext.grid.RowSelectionModel({singleSelect: TCG.isNotEqualsStrict(this.rowSelectionModel, 'multiple')});
			}
		}
		return this.rowSelectionModel;
	},

	createStore: function(fields) {
		const gridPanel = this;
		const storeConf = {
			fields: fields,
			url: encodeURI(TCG.getApplicationUrl(this.getLoadURL(), this)),
			listeners: {
				datachanged: function(store, records, opts) {
					gridPanel.updateCount();
				},
				load: function(store, records, opts) { // keep selected row in view
					const rowIndex = gridPanel.grid.store.indexOf(gridPanel.grid.getSelectionModel().getSelected());
					if (rowIndex >= 0) {
						const a = function() {
							return gridPanel.grid.getView().focusRow(rowIndex);
						};
						a.defer(250, this);
					}
				},
				update: function(store, record, operation) {
					if (gridPanel.updateRecord) {
						gridPanel.updateRecord(store, record, operation);
					}
					else if (operation === Ext.data.Record.EDIT) {
						const loader = new TCG.data.JsonLoader({
							waitTarget: gridPanel,
							waitMsg: 'Saving...',
							params: record.data,
							conf: {rowId: record.id},
							onLoad: function(record, conf) {
								store.commitChanges();
							},
							failure: function() {
								store.rejectChanges();
								if (this.isUseWaitMsg()) {
									this.getMsgTarget().unmask();
								}
								TCG.showError('Error sending server request to:<br /><br />' + gridPanel.getSaveURL() + '<br /><br />Response status: ' + response.statusText, 'Server Error');
							}
						});
						loader.load(gridPanel.getSaveURL());
					}
				}
			}
		};
		if (!this.dataTable && this.isPagingEnabled()) {
			storeConf.root = 'data.rows';
			storeConf.totalProperty = 'data.total';
		}
		let storeClass = TCG.data.JsonStore;
		if (this.dataTable) {
			if (this.groupField) {
				storeClass = TCG.data.DataTableGroupingStore;
				storeConf.groupField = this.groupField;
				storeConf.remoteSort = this.remoteSort || false;
			}
			else {
				storeClass = TCG.data.DataTableStore;
			}
		}
		else if (this.groupField) {
			storeClass = TCG.data.GroupingStore;
			storeConf.groupField = this.groupField;
			storeConf.remoteSort = this.remoteSort || false;
		}
		return new storeClass(storeConf);
	},

	getSaveURL: function() {
		if (!this.saveURL) {
			let url = this.name;
			if (url.length > 4 && url.substring(url.length - 4) === 'List') {
				url = url.substring(0, url.length - 4);
			}
			return url + 'Save.json';
		}
		return this.saveURL;
	}
});

Ext.reg('editorgrid', TCG.grid.EditorGridPanel);


TCG.grid.FormGridPanel = Ext.extend(Ext.grid.EditorGridPanel, {
	storeRoot: 'REQUIRED-ROOT-PATH-FOR-STORE',
	dtoClass: 'REQUIRED-CLASS-NAME-OF-DTO-TO-MAP-TO',
	doNotSubmitFields: [], // do not submit to the server fields with the following names
	alwaysSubmitFields: [], // always submit to the server fields with the following names
	clicksToEdit: 1,
	bindToFormLoad: true,
	frame: true,
	collapsible: true,
	autoHeight: true,
	stripeRows: true,
	readOnly: false,
	submitValue: true,
	useFilters: false, // supports local filtering only, must set to true to enable filters
	detailPageClass: undefined, // optional detail page class for drill down when readOnly = true
	contextMenuConfig: undefined, //optional array of context menu items to provide various drill down options
	controlWindowModified: true, // call win.setModified when the form is modified
	useDynamicColumns: false, // If true, columns will be dynamically added to the grid when retrieved

	initComponent: function() {
		const fields = [];
		const filters = [];
		const columns = this.getColumnsWithOverrides();
		const sortInfo = TCG.grid.fillColumnMetaData(columns, fields, filters, this);

		this.plugins = this.plugins && (Array.isArray(this.plugins) ? [...this.plugins] : [this.plugins]) || [];

		// adjust column model where appropriate
		for (let i = 0; i < columns.length; i++) {
			let c = columns[i];
			if (c.type === 'boolean' && c.editor) {
				delete c.editor;
				// Allows for cases where we want to override the renderer to have something special
				// For example, checkboxes that may not apply to each row, we want to show blank instead of un-editable check box
				if (!c.overrideRenderer) {
					delete c.renderer;
				}
				c = new Ext.grid.CheckColumn(c);
				columns[i] = c;

				this.plugins.push(c);
			}
		}

		this.store = this.createStore(fields);
		this.cm = new Ext.grid.ColumnModel({
			columns: columns
		});

		if (!this.readOnly || this.addToolbarButtons) {
			Ext.apply(this, {tbar: new Ext.Toolbar()});
		}
		if (!this.getGridTitle()) {
			this.collapsible = false;
		}

		if (TCG.isNotNull(sortInfo.sortColumn)) {
			this.store.setDefaultSort(sortInfo.sortColumn, sortInfo.sortDirection);
		}

		if (this.useFilters && filters.length > 0) {
			const filtersPlugin = new Ext.ux.grid.GridFilters({local: true, encode: true, filters: filters, gridPanel: this});
			if (this.plugins) {
				this.plugins.push(filtersPlugin);
			}
		}

		// Add warning to topToolbar if no columns.
		if (this.cm.getColumnCount() === 0) {
			const toolbar = this.getTopToolbar();
			const columnWarning = 'Grid is missing column definitions from columnsConfig property!';
			if (toolbar) {
				toolbar.add(columnWarning);
			}
			else {
				this.tbar = {items: [columnWarning]};
			}
		}

		TCG.grid.FormGridPanel.superclass.initComponent.apply(this, arguments);
	},

	getGridTitle: function() {
		return this.title;
	},

	createStore: function(fields) {
		const storeConf = {
			root: this.storeRoot,
			fields: fields
		};
		let storeClass = TCG.data.JsonStore;
		if (this.groupField) {
			storeClass = TCG.data.GroupingStore;
			storeConf.groupField = this.groupField;
			storeConf.remoteSort = this.remoteSort || false;
			this.view = new Ext.grid.GroupingView({
				...this.viewConfig || {},
				forceFit: this.forceFit,
				groupTextTpl: this.groupTextTpl || '{values.text} ({[values.rs.length]} {[values.rs.length > 1 ? "Items" : "Item"]})'
			});
		}
		else if (this.dataTable === true) {
			storeClass = TCG.data.DataTableStore;
		}
		const store = new storeClass(storeConf);
		if (this.useDynamicColumns) {
			this.configureDynamicColumnHandler(store);
		}
		return store;
	},

	configureDynamicColumnHandler: function(store) {
		if (this.useDynamicColumns) {
			// Apply dynamic columns when processing new data
			let originalColumns;
			store.reader.extractData = store.reader.extractData.createInterceptor(root => {
				// Track visible columns
				const visibleColumnDataIndexes = this.getColumnModel().columns
						.filter((column, index) => !this.getColumnModel().isHidden(index))
						.map(column => column.dataIndex);
				// Restore original columns
				if (!originalColumns) {
					originalColumns = [...this.getColumnModel().columns];
				}
				else {
					const dataIndexesToRemove = this.getColumnModel().columns
							.filter(column => !originalColumns.some(originalCol => originalCol.dataIndex === column.dataIndex))
							.map(column => column.dataIndex);
					this.removeColumn(...dataIndexesToRemove);
				}
				// Add new columns
				const dynamicColumns = this.getDynamicColumns(root)
						.filter(column => !originalColumns.some(originalCol => originalCol.dataIndex === column.dataIndex))
						.peek(column => column.header = column.header != null ? column.header : TCG.camelCaseToTitle(column.dataIndex.lastIndexOf('.')))
						.peek(column => column.hidden = column.hidden && !visibleColumnDataIndexes.includes(column.dataIndex))
						.let(columns => this.sortDynamicColumns(columns));
				this.addColumn(...dynamicColumns);
			});
		}
		else {
			// Disable dynamic columns
			store.reader.extractData = store.reader.constructor.prototype.extractData;
		}
	},

	/**
	 * Retrieves dynamic column configurations from the given loaded data.
	 */
	getDynamicColumns: function(data) {
		return data.flatMap(entity => Object.keys(entity))
				.distinct()
				.map(dataIndex => ({header: TCG.camelCaseToTitle(dataIndex), dataIndex: dataIndex, hidden: true}));
	},

	sortDynamicColumns: function(columns) {
		return columns.sort((col1, col2) => col1.dataIndex.localeCompare(col2.dataIndex));
	},

	setReadOnly: function(readOnly) {
		this.readOnly = readOnly;
		this.controlWindowModified = !readOnly;
		if (this.getTopToolbar()) {
			this.getTopToolbar().setVisible(!readOnly);
		}
	},

	afterRender: function() {
		TCG.grid.FormGridPanel.superclass.afterRender.apply(this, arguments);
		this.addEvents({
			'afterload': true // fired after the data was loaded into this formgrid
		});

		const grid = this;
		const fp = TCG.getParentFormPanel(this);
		if (this.bindToFormLoad) {
			fp.on('afterload', async function() {
				if (this.isDestroyed) {
					return;
				}
				const data = fp.getForm().formValues;
				// In some uses the form is reloaded after save occurs and the form is serialized in the reload request.
				// Since this method is now asynchronous, we need to ensure the submit field does not have the array result from save.
				// Clear the value, which will be populated correctly after the loadData and markModified requests below.
				// The raw field value must be analyzed as the element value is a string representation (e.g., '[object Object]').
				if (this.submitField && Array.isArray(this.submitField.value)) {
					this.submitField.setValue('[]');
				}
				try {
					await this.beforeLoadData(fp, this, data);
				}
				catch (error) {
					console.warn(error);
				}
				this.loadData(this.getStore(), data);
				this.markModified(true);
				this.fireEvent('afterload', this);
			}, this);
		}

		this.submitField = new Ext.form.Hidden({
			name: grid.storeRoot,
			submitValue: this.submitValue
		});
		fp.add(this.submitField);
		fp.doLayout();

		TCG.grid.registerColumnEventListeners(grid);

		grid.on({
			'afteredit': function(e) {
				grid.markModified();

				if (grid.onAfterEdit) { // optionally listen to do something AFTER data is updated
					// Skip Check box columns that don't have a field
					if (e.field) {
						const field = e.record.fields.get(e.field);
						grid.onAfterEdit(e, field);
					}
				}
			},
			'validateedit': function(e) {
				// set the value field on record in addition to text (for combos)
				if (String(e.value) !== String(e.startValue)) {
					let value = this.lastActiveEditor.getValue();
					if (value === undefined || value === '') {
						value = -1;
					} // send id = -1 to the server to clear the field - for special cases, like enums, the binder handles the -1
					const field = e.record.fields.get(e.field);
					if (TCG.isNotEquals(value, e.value) && field.valueFieldName) {
						e.record.set(field.valueFieldName, value);
					}
					if (grid.onAfterUpdateFieldValue) { // optionally allow overwriting values, etc.
						grid.onAfterUpdateFieldValue(e, field);
					}
				}
				return true;
			}
		});

		if (this.readOnly) {
			if (this.detailPageClass) {
				grid.addListener('rowdblclick', function(grid, rowIndex, evt) {
					this.rowDoubleClickHandler(grid, rowIndex, evt);
				}, this);
			}

			if (this.contextMenuConfig) {
				const el = grid.getEl();
				el.on('contextmenu', function(e, target) {
					const rowIndex = grid.view.findRowIndex(target);

					if (rowIndex !== false) {
						e.preventDefault();

						grid.contextMenu = new Ext.menu.Menu({
							items: grid.contextMenuConfig,
							openerCt: grid,
							originalTarget: target,
							row: grid.store.data.items[rowIndex]
						});
						grid.contextMenu.showAt(e.getXY());
					}
				}, this);
			}
		}
		else {
			const toolBar = this.getTopToolbar();
			if (this.addToolbarAddButton(toolBar) !== false) {
				toolBar.add('-');
			}
			if (this.addToolbarRemoveButton(toolBar) !== false) {
				toolBar.add('-');
			}
		}
		if (this.addToolbarButtons) {
			this.addToolbarButtons(this.getTopToolbar(), grid);
		}

		// conditionally apply rowcontextmenu if cellcontextmenu is not used.
		let enableRowContextMenu = TCG.isTrue(this.events['rowcontextmenu']) || TCG.isNull(this.events['rowcontextmenu']);

		// After column listeners are defined, add grid cellcontextmenu
		if (TCG.isTrue(this.events['cellcontextmenu']) || TCG.isNull(this.events['cellcontextmenu'])) {
			enableRowContextMenu = false;

			this.addListener('cellcontextmenu', async function(grid, rowIndex, columnIndex, event) {
				const column = grid.getColumnModel().getColumnById(columnIndex),
						columnContextMenu = column.events['contextmenu'];
				if (TCG.isTrue(columnContextMenu) || TCG.isNull(columnContextMenu)) {
					event.preventDefault();
					const contextMenuItems = await grid.getGridCellContextMenuItems(grid, rowIndex, columnIndex);
					if (contextMenuItems.length > 0) {
						const menu = new Ext.menu.Menu({
							items: contextMenuItems
						});

						menu.showAt(event.getXY());
					}
				}
			});
		}

		// Add rowcontextmenu items if no event present
		if (enableRowContextMenu) {
			this.addListener('rowcontextmenu', async function(grid, rowIndex, event) {
				const record = grid.getStore().getAt(rowIndex);
				event.preventDefault();
				const contextMenuItems = await grid.getGridRowContextMenuItems(grid, rowIndex, record);
				if (contextMenuItems && contextMenuItems.length > 0) {
					const menu = new Ext.menu.Menu({
						items: contextMenuItems
					});
					menu.showAt(event.getXY());
				}
			});
		}
	},

	beforeLoadData: function(formPanel, formGrid, formValues) {
		// override for before load hook
	},

	loadData: function(store, data) {
		store.loadData(data);
	},

	rowDoubleClickHandler: function(grid, rowIndex, event) {
		const row = this.store.data.items[rowIndex];
		const id = this.getDetailPageId(grid, row);
		const className = this.getDetailPageClass(grid, row);
		const cmpId = TCG.getComponentId(className, id);
		TCG.createComponent(className, {
			id: cmpId,
			params: this.getDetailPageParams(id),
			defaultData: this.getDefaultData(grid, row),
			openerCt: this,
			defaultIconCls: this.getWindow().iconCls
		});
	},

	addToolbarRemoveButton: function(toolBar) {
		const grid = this;
		toolBar.add({
			text: 'Remove',
			tooltip: 'Remove selected item',
			iconCls: 'remove',
			scope: this,
			disabled: grid.getColumnModel().getColumnCount() === 0,
			handler: function() {
				const index = grid.getSelectionModel().getSelectedCell();
				if (index) {
					const store = grid.getStore();
					const rec = store.getAt(index[0]);
					store.remove(rec);
					grid.markModified();
				}
			}
		});
	},

	addToolbarAddButton: function(toolBar) {
		toolBar.add({
			text: 'Add',
			tooltip: 'Add a new item',
			iconCls: 'add',
			scope: this,
			disabled: this.getColumnModel().getColumnCount() === 0,
			handler: function() {
				const store = this.getStore();
				const RowClass = store.recordType;
				this.stopEditing();
				store.insert(0, new RowClass(this.getNewRowDefaults()));
				this.startEditing(0, 0);
				this.markModified();
			}
		});
	},

	addToolbarButtons: undefined,

	addToolbarButtonsAfter: undefined,

	getColumnsWithOverrides: function() {
		// Use clones for both the original and override column configurations to avoid having the configurations modified.
		const columns = TCG.clone(this.columnsConfig || []);
		let overriddenColumns = [];
		if (this.columnOverrides) {
			const clonedOverrides = TCG.clone(this.columnOverrides);
			for (let i = 0; i < columns.length; i++) {
				let c = columns[i];
				for (let j = 0; j < clonedOverrides.length; j++) {
					const co = clonedOverrides[j];
					if (co.dataIndex === c.dataIndex) {
						c = Ext.apply(c, co);
						// remove override from the clone to avoid iterating and filter to columns to add
						clonedOverrides.splice(j, 1);
						break;
					}
				}
				overriddenColumns.push(c);
			}
			if (clonedOverrides.length > 0) {
				clonedOverrides.forEach(newColumnFromOverrides => overriddenColumns.push(newColumnFromOverrides));
			}
		}
		else {
			overriddenColumns = columns;
		}
		return overriddenColumns;
	},

	getNewRowDefaults: function() {
		return {};
	},

	getDetailPageClass: function(grid, row) {
		return this.detailPageClass;
	},
	getDetailPageId: function(grid, row) {
		return row.id;
	},
	getDetailPageParams: function(id) {
		return {'id': id};
	},
	getDefaultData: function(gridPanel, row) {
		return undefined;
	},

	startEditing: function(row, col) {
		TCG.grid.FormGridPanel.superclass.startEditing.apply(this, arguments);
		// need a way to get to grid from editable fields: combo, etc. to get form arguments
		const ed = this.colModel.getCellEditor(col, row);
		if (ed) {
			ed.containerGrid = this;
		}
	},

	markModified: function(initOnly) {
		const result = [];
		let newId = -10;
		this.store.each(function(record) {
			if (this.isRecordApplicableForDirtyChecking(record)) {
				if (!Ext.isNumber(record.id)) {
					record.id = newId--;
					record.markDirty(); // new records should be fully submitted
				}
				const r = {
					id: record.id,
					'class': this.dtoClass
				};
				for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
					const f = this.alwaysSubmitFields[i];
					let val = TCG.getValue(f, record.json);
					if (TCG.isNotNull(val)) {
						if (typeof val == 'number' && isNaN(val)) {
							val = '';
						}
						else if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
							val = TCG.renderDate(val);
						}
						r[f] = val;
					}
				}
				if (record.dirty) {
					// copy all modified fields
					for (const m of Object.keys(record.modified)) {
						const field = record.fields.get(m);
						// skip 'text' field submission when there's 'value'
						if ((!field || !field.valueFieldName) && this.doNotSubmitFields.indexOf(m) === -1) {
							let v = record.modified[m];
							let skip = false;
							if (typeof v == 'object') {
								if (TCG.isNull(v)) {
									skip = true;
								}
								else if (Ext.isDate(v)) {
									v = v.dateFormat('m/d/Y');
								}
							}
							else if (typeof v == 'number' && isNaN(v)) {
								v = '';
							}
							if (!skip) {
								r[m] = v;
							}
						}
					}
				}

				result.push(r);
			}
		}, this);
		const value = Ext.util.JSON.encode(result);
		this.submitField.setValue(value); // changed value
		if (initOnly) {
			this.submitField.originalValue = value; // initial value
		}
		else {
			const win = this.getWindow();
			/*
			 * TODO On certain windows, win.setModified is not a defined function. I think this function is only
			 * defined for detail windows. For now, perform a check whether this function actually exists before
			 * calling it. Will want to find an alternative solution later. - JG
			 */
			if (typeof (win.setModified) === 'function' && this.controlWindowModified === true) {
				win.setModified(true);
			}
		}
	},

	/*
	 * Tests if the provided record should be tested for modified field values.
	 * The default is to always check for record modifications, this can be overridden
	 * to change the behavior (e.g. check for record's include checkcolumn selection).
	 */
	isRecordApplicableForDirtyChecking: function(record) {
		return true;
	},

	getWindow: function() {
		let result = this.findParentByType(Ext.Window);
		if (TCG.isNull(result)) {
			result = this.findParentBy(function(o) {
				return o.baseCls === 'x-window';
			});
		}
		return result;
	},

	getTitle: function() {
		return this.getWindow().title;
	},

	/**
	 * Returns an array of menu items to append to a context menu for a cell/row/grid. Column, row, and grid context menus should be
	 * appended together. The default implementation return an empty array for no items. It can be overridden to add context menu items
	 * particular to the provided row.
	 *
	 * @param grid - the grid provided in the contextmenu event, is not the gridpanel.
	 * @param rowIndex - the row the contextmenu is to be displayed for
	 * @param record - the record of the grid for the rowIndex
	 * @returns {Array} of filter menu items based on column data type
	 */
	getGridRowContextMenuItems: async function(grid, rowIndex, record) {
		return [];
	},

	/**
	 * Returns an array of context menu items composed of the combined arrays from {@link #getGridRowContextMenuItems} and {@link #getGridCellContextMenuFilterItems}.
	 *
	 * @param grid - the grid provided in the contextmenu event, is not the gridpanel.
	 * @param rowIndex - the row the contextmenu is to be displayed for
	 * @param columnIndexOrColumn - the column ID or column object the contextmenu is to be displayed for
	 * @returns {[]} an array of context meny itmes for a cell, including row items
	 */
	getGridCellContextMenuItems: async function(grid, rowIndex, columnIndexOrColumn) {
		const contextMenuItems = [];

		// append row context menu items
		const record = grid.getStore().getAt(rowIndex);
		const rowMenuItems = await grid.getGridRowContextMenuItems(grid, rowIndex, record);
		if (rowMenuItems && rowMenuItems.length > 0) {
			contextMenuItems.push(...rowMenuItems);
		}

		return contextMenuItems;
	}
});
Ext.reg('formgrid', TCG.grid.FormGridPanel);

/**
 * GridField is a special FormGridPanel that can be used as a field for forms with dynamic fields.
 * The columnsConfig property is the only property required for use.
 */
TCG.form.GridField = Ext.extend(TCG.grid.FormGridPanel, {
	viewConfig: {
		forceFit: true
	},
	isFormField: true, // indicate this is a form field
	dtoClass: 'com.clifton.core.json.custom.CustomJsonStringList',
	storeRoot: 'entityList', // root of the field value to be used for the setValue and getValue functions.
	instructions: void 0, // override via user interface config
	collapsible: false, // override to true in user interface config to allow collapsing

	// override dynamic TABLE fields with the user interface config for the columns with something like the following:
	// {columnsConfig:  [
	// 	{header: 'Manager', dataIndex: 'manager.label', idDataIndex: 'manager.id', editor: {xtype: 'combo', url: 'investmentManagerAccountListFind.json', displayField: 'label'}},
	// 	{header: 'Allocation Percent', dataIndex: 'allocationPercent', nonPersistentField: true, type: 'float', editor: {xtype: 'floatfield'}}
	// ]}

	getGridTitle: function() {
		// overridden to behave on collapsible since it is based on title in parent type
		return TCG.isTrue(this.collapsible) ? this.fieldLabel : '';
	},

	addToolbarButtons: function(toolBar, gridPanel) {
		if (TCG.isNotBlank(this.instructions)) {
			toolBar.add({
				text: 'Info',
				tooltip: 'Toggle instructions for this page',
				iconCls: 'info',
				enableToggle: true,
				handler: function() {
					if (this.pressed) {
						gridPanel.insert(0, {
							xtype: 'panel',
							frame: true,
							layout: 'fit',
							bodyStyle: 'padding: 3px 3px 3px 3px',
							html: gridPanel.instructions
						});
					}
					else {
						gridPanel.remove(0);
					}
					gridPanel.doLayout();
				}
			});
		}
	},

	listeners: {
		collapse: function(panel) {
			// Override collapsed to avoid flex rendering in vbox from hiding the panel on collapse
			panel.overriddenCollapsed = panel.collapsed;
			panel.collapsed = false;
			if (panel.flex && panel.flex !== 0) {
				panel.overriddenFlex = panel.flex;
				panel.flex = 0; // set flex to 0 to use actual height
			}
			panel.ownerCt.doLayout();
		},
		expand: function(panel) {
			// Override to reset state for flex rendering in vbox to expand
			panel.overriddenCollapsed = void 0;
			if (panel.overriddenFlex) {
				panel.flex = panel.overriddenFlex; // revert flex on expand
			}
			panel.ownerCt.doLayout();
		},
		afterrender: function(panel) {
			// initialize custom json string to {}.
			panel.markModified(true);
		}
	},
	toggleCollapse: function(animate) {
		/*
		 * Overridden to reset collapsed to overriddenCollapsed.
		 */
		if (this.overriddenCollapsed) {
			this.collapsed = this.overriddenCollapsed;
		}
		this[this.collapsed ? 'expand' : 'collapse'](animate);
		return this;
	},

	/**
	 * Expects a JSON string defining an objct with a single property for the store root.
	 * The provided JSON string will be parsed and laoded into the grid.
	 */
	setValue: function(value) {
		const store = this.getStore();
		if (value) {
			store.loadData(JSON.parse(value));
			this.markModified(true); // reset original value
			this.loadData(store); // clear modified cells
		}
	},

	/**
	 * Using the default store root, the value will be a JSON object string
	 * with a single property of entityList (e.g. {entityList: [...]}).
	 */
	getValue: function() {
		return this.submitField.getValue();
	},

	loadData: function(store, data) {
		// overridden to avoid loading from form's formValues
		// values are dynamic so just commit them to remove modified indicators
		store.each(record => record.commit());
	},

	markModified: function(initOnly) {
		const resultList = [];
		const result = {entityList: resultList};
		let newId = -10;
		this.store.each(function(record) {
			if (this.isRecordApplicableForDirtyChecking(record)) {
				if (!Ext.isNumber(record.id)) {
					record.id = newId--;
					record.markDirty(); // new records should be fully submitted
				}
				const r = {};
				for (let i = 0; i < this.alwaysSubmitFields.length; i++) {
					const f = this.alwaysSubmitFields[i];
					let val = TCG.getValue(f, record.json);
					if (TCG.isNotNull(val)) {
						if (typeof val == 'number' && isNaN(val)) {
							val = '';
						}
						else if (f.length > 4 && f.substring(f.length - 4) === 'Date') {
							val = TCG.renderDate(val);
						}
						r[f] = val;
					}
				}
				// apply all data to the record each time, not just the modified so we have the entire contents of the dynamic field value
				record.fields.each(field => {
					const fieldName = field.name;
					let value = record.get(fieldName);
					if (TCG.isBlank(value) || (typeof value == 'number' && isNaN(value))) {
						return;
					}
					if (typeof value == 'object' && Ext.isDate(value)) {
						value = value.dateFormat('m/d/Y');
					}

					const fieldSegments = fieldName.split('.');
					const iterations = fieldSegments.length - 1;
					let objectReference = r;
					let segmentIndex = 0;
					while (segmentIndex < iterations) {
						const segment = fieldSegments[segmentIndex];
						if (TCG.isNull(objectReference[segment])) {
							objectReference[segment] = {};
						}
						objectReference = objectReference[segment];
						segmentIndex++;
					}
					objectReference[fieldSegments[segmentIndex]] = value;
				});

				if (Object.keys(r).length > 0) {
					resultList.push(r);
				}
			}
		}, this);

		const value = Ext.util.JSON.encode(result);
		this.submitField.setValue(value); // changed value
		if (initOnly) {
			this.submitField.originalValue = value; // initial value
		}
		else {
			const win = this.getWindow();
			if (typeof (win.setModified) === 'function' && this.controlWindowModified === true) {
				win.setModified(true);
			}
		}
	}
});
Ext.reg('grid-field', TCG.form.GridField);

// columns are not resizable and horizontal scroll will appear if necessary
TCG.grid.FormGridPanelWithScroll = function(conf) {
	const grid = {
		autoHeight: false,
		height: 200,
		plugins: conf.plugins
	};
	Ext.apply(grid, conf);
	grid.xtype = 'formgrid';
	const panel = {
		xtype: 'panel',
		layout: 'fit',
		items: [grid]
	};
	if (conf.heightResized === true) {
		panel.listeners = {
			afterlayout: TCG.grid.registerDynamicHeightResizing
		};
	}
	TCG.grid.FormGridPanelWithScroll.superclass.constructor.apply(this, [panel]);
};
Ext.extend(TCG.grid.FormGridPanelWithScroll, Ext.Panel);
Ext.reg('formgrid-scroll', TCG.grid.FormGridPanelWithScroll);

TCG.grid.registerDynamicHeightResizing = function(p) {
	if (!p.savedOffset) {
		const w = p.findParentBy(function(o) {
			return o.baseCls === 'x-window';
		});
		p.savedOffset = p.getPosition(false)[1] - w.getPosition(false)[1];
		p.savedHeight = p.getHeight();
		p.savedWinHeight = w.getHeight();
		w.on('resize', function() {
			if (p.layoutInProgress === true) {
				return;
			}
			const offset = p.getPosition(false)[1] - w.getPosition(false)[1];
			p.setHeight(p.savedHeight + p.savedOffset - offset - p.savedWinHeight + w.getHeight());
			w.doLayout();
		});
	}
};


TCG.grid.GridPanelWithScroll = function(conf) {
	const grid = {
		autoHeight: false,
		height: 200
	};
	Ext.apply(grid, conf);
	grid.xtype = 'gridpanel';
	const panel = {
		xtype: 'panel',
		layout: 'fit',
		items: [grid]
	};
	if (conf.heightResized === true) {
		panel.listeners = {
			afterlayout: function(p) {
				if (!p.savedOffset) {
					const w = p.findParentBy(function(o) {
						return o.baseCls === 'x-window';
					});
					p.savedOffset = p.getPosition(false)[1] - w.getPosition(false)[1];
					p.savedHeight = p.getHeight();
					p.savedWinHeight = w.getHeight();
					w.on('resize', function() {
						if (p.layoutInProgress === true) {
							return;
						}
						const offset = p.getPosition(false)[1] - w.getPosition(false)[1];
						p.setHeight(p.savedHeight + p.savedOffset - offset - p.savedWinHeight + w.getHeight());
						w.doLayout();
					});

					p.get(0).ds.addListener('load', function(store, records, opts) {
						w.fireEvent('resize'); // set size/scroll bars on reload
					});
				}
			}
		};
	}
	TCG.grid.GridPanelWithScroll.superclass.constructor.apply(this, [panel]);
};
Ext.extend(TCG.grid.GridPanelWithScroll, Ext.Panel);
Ext.reg('gridpanel-scroll', TCG.grid.GridPanelWithScroll);


// populates column data types, align, etc. based on defaults and our naming convention
TCG.grid.fillColumnMetaData = function(columns, storeFields, filters, gridPanel) {
	const sortInfo = {sortDirection: 'ASC'};
	for (const col of columns) {
		col.gridPanel = gridPanel;
		if (col.xtype) {
			Ext.applyIf(col, Ext.grid.Column.types[col.xtype].prototype);
		}

		// Attach data-store renderer
		if (Array.isArray(col.storeData)) {
			col.renderer = value => {
				const [, text, description] = col.storeData.find(storeElement => value === storeElement[0]) || ['', '', ''];
				const displayedText = text || (value != null && value) || '';
				return `<div ext:qtip="${description}">${displayedText}</div>`;
			};
		}

		// try to determine types based on naming convention
		col.dataIndex = col.dataIndex || col.header;
		if (!col.type) {
			if (col.dataIndex && (col.dataIndex.endsWith('Date') || col.dataIndex === 'date')) {
				col.type = 'date';
			}
			else if (col.dataIndex === 'id') {
				col.type = 'int';
				if (col.doNotFormat === undefined) {
					col.doNotFormat = true;
				}
			}
			else {
				col.type = col.renderer ? 'auto' : 'string'; // custom renderer may need an object
			}
		}

		// Format by type
		switch (col.type) {
			case 'date':
				col.renderer = col.renderer || (col.alwaysIncludeTime ? TCG.renderDateTime : TCG.renderDate);
				col.align = col.align || 'center';
				break;
			case 'currency':
			case 'percent':
				let numFormat = col.numberFormat || '0,000.00';
				if (col.type === 'percent') {
					numFormat += ' %';
				}
				let variablePrecision, maxSuffixLength, minSuffixLength;
				if (col.type === 'currency' && TCG.isBlank(col.numberFormat)) {
					variablePrecision = true;  // support both currency and currency4 data types
					minSuffixLength = 2;
					maxSuffixLength = 4;
				}
				col.type = 'float';
				if (col.negativeInRed) {
					col.renderer = col.renderer || (value => TCG.renderAmount(value, !!col.positiveInGreen, numFormat, variablePrecision, undefined, maxSuffixLength, minSuffixLength));
				}
				else {
					col.renderer = col.renderer || (value => (value == null || value === '') ? '' : TCG.numberFormat(value, numFormat, variablePrecision, maxSuffixLength, minSuffixLength));
				}
				// Intentional fall-through
			case 'int':
			case 'float':
				col.align = col.align || 'right';
				if (!col.doNotFormat) {
					const numFormat = col.numberFormat || '0,000';
					if (col.negativeInRed) {
						col.renderer = col.renderer || (value => TCG.renderAmount(value, !!col.positiveInGreen, numFormat, true));
					}
					else {
						col.renderer = col.renderer || (value => TCG.numberFormat(value, numFormat, true));
					}
				}
				break;
			case 'boolean':
				col.renderer = col.renderer || TCG.renderBoolean;
				col.align = col.align || 'center';
				break;
			default:
				// Do nothing
		}

		// make sortable by default
		if (TCG.isNull(col.sortable)) {
			col.sortable = true;
		}
		const index = storeFields.length;
		storeFields[storeFields.length] = {
			name: col.sortFieldName || col.dataIndex,
			type: col.type ? col.type : undefined,
			filter: col.filter,
			useNull: col.useNull,
			nonPersistentField: col.nonPersistentField,
			skipFilterOnLoad: col.skipFilterOnLoad,
			convert: col.convert
		};
		if (col.dateFormat) {
			storeFields[storeFields.length - 1].dateFormat = col.dateFormat;
		}
		if (col.idDataIndex) {
			// define and link value/text combination
			storeFields[storeFields.length - 1].valueFieldName = col.idDataIndex;
			// Some grid combo editors for local store values and system list do not require an idDataIndex.
			// If idDataIndex is present, it may be a type other than int.
			let idType = 'int';
			if (col.idType) {
				idType = col.idType;
			}
			else if (col.dataIndex === col.idDataIndex && col.type) {
				idType = col.type;
			}
			// Otherwise, just set dataIndex value to prevent an 'undefined' value mapping on a record, which can prevent binding.
			storeFields[storeFields.length] = {name: col.idDataIndex, type: idType, textFieldName: col.dataIndex};
		}
		if (col.defaultSortColumn) {
			sortInfo.sortColumn = col.dataIndex;
			if (col.defaultSortDirection) {
				sortInfo.sortDirection = col.defaultSortDirection;
			}
		}
		if (!TCG.isFalse(col.filter)) {
			let f = col.filter;
			if (TCG.isNotEquals(Ext.type(f), 'object')) {
				f = {type: col.type || 'string'};
			}
			else if (!f.type) {
				f.type = col.type || 'string';
			}
			f.dataIndex = col.dataIndex;
			f.gridPanel = gridPanel;
			// The nonPersistentField allows remote filtering with custom local filtering
			// Filter columns marked with nonPersistentField=true, will filter client side
			// see tcg-gridfilters.js - applyNonPersistentFieldFilters
			f.nonPersistentField = col.nonPersistentField;
			// The skipFilterOnLoad property can be used in conjunction with nonPersistentField to avoid
			// post-load filtering of data in the event the column is manually populated client-side.
			f.skipFilterOnLoad = col.skipFilterOnLoad;
			filters[filters.length] = f;
		}
		if (col.dataTypeName) {
			storeFields[index].dataTypeName = col.dataTypeName;
		}
	}
	return sortInfo;
};


Ext.grid.CheckColumn.prototype.processEvent = function(name, e, grid, rowIndex, colIndex) {
	if (name === 'mousedown') {
		const record = grid.store.getAt(rowIndex);
		// Can use isCheckEnabled method to return false if checkbox should apply to specific rows
		// based on other information (See Portfolio Run - Trade Creation Window for example)
		if (!this.isCheckEnabled || this.isCheckEnabled(record) === true) {
			record.set(this.dataIndex, !record.data[this.dataIndex]);
			// CHANGE/ADDITION:
			if (this.fireEditEvent !== false) {
				grid.fireEvent('afteredit', grid, rowIndex, colIndex);
			}
			if (this.onClick) {
				this.onClick(grid, record, rowIndex, colIndex, record.get(this.dataIndex));
			}
			return false; // Cancel row selection.
		}
	}
	return Ext.grid.ActionColumn.superclass.processEvent.apply(this, arguments);
};


Ext.Editor.prototype.startEdit = function(el, value) {
	if (this.editing) {
		this.completeEdit();
	}
	this.boundEl = Ext.get(el);
	const v = value !== undefined ? value : this.boundEl.dom.innerHTML;
	if (!this.rendered) {
		this.render(this.parentEl || document.body);
	}
	if (this.fireEvent('beforestartedit', this, this.boundEl, v) !== false) {
		//this.startValue = v; // CHANGE: set later
		this.field.reset();
		this.field.setValue(v);
		this.startValue = this.field.getRawValue();  // CHANGE: the value may be different than v: combo value vs text
		this.realign(true);
		this.editing = true;
		this.show();
	}
};


Ext.Editor.prototype.completeEdit = function(remainVisible) {
	if (!this.editing) {
		return;
	}
	// Assert combo values first
	if (this.field.assertValue) {
		this.field.assertValue();
	}
	let v = this.getValue();
	if (!this.field.isValid()) {
		if (this.revertInvalid !== false) {
			this.cancelEdit(remainVisible);
		}
		return;
	}
	if (String(v) === String(this.startValue) && this.ignoreNoChange) {
		this.hideEdit(remainVisible);
		return;
	}
	if (this.fireEvent('beforecomplete', this, v, this.startValue) !== false) {
		const combo = (this.field instanceof TCG.form.ComboBox);
		v = combo ? this.field.getRawValue() : this.field.getNumericValue ? this.field.getNumericValue() : this.getValue(); // CHANGE: for combo's get text instead of value: currencyfield and other fields must use getValue()
		if (this.updateEl && this.boundEl) {
			this.boundEl.update(v);
		}
		this.hideEdit(remainVisible);
		this.fireEvent('complete', this, v, this.startValue);
		// also update id for combo boxes
		if (combo && String(v) !== String(this.startValue)) {
			const grid = this.containerGrid;
			const idIndex = grid.getColumnModel().getColumnAt(this.col).idDataIndex;
			if (idIndex) {
				let value = this.getValue();
				if (value === undefined || value === '') {
					value = -1;
				} // send id = -1 to the server to clear the field - for special cases, like enums, the binder handles the -1
				this.record.set(idIndex, value);
			}
		}
	}
};


Ext.data.Record.prototype.set = function(name, value) {
	const encode = Ext.isPrimitive(value) ? String : Ext.encode;
	if (encode(this.data[name]) === encode(value)) {
		return;
	}
	this.dirty = true;
	if (!this.modified) {
		this.modified = {};
	}
	// CHANGE: next 2 lines instead of 4 commented out: new records were getting 'undefined' and modified was not updated with latest value
	this.data[name] = value;
	this.modified[name] = Ext.isDate(value) ? value.dateFormat('m/d/Y') : this.data[name];
	//if(this.modified[name] === undefined){
	//    this.modified[name] = this.data[name];
	//}
	//this.data[name] = value;
	if (!this.editing) {
		this.afterEdit();
	}
};

/**
 * Add functions to Store to support dynamically adding and removing columns on a grid.
 */
Ext.override(Ext.data.Store, {
	addField: function(...fields) {
		// Update store reader for new fields
		this.reader.onMetaChange({
			...this.reader.meta,
			fields: [...this.reader.meta.fields, ...fields]
		});
		// Set field values to defaults for existing records
		for (const field of fields) {
			if (typeof field.defaultValue !== 'undefined') {
				this.each(r => {
					if (typeof r.data[field.name] === 'undefined') {
						r.data[field.name] = field.defaultValue;
					}
				});
			}
		}
	},
	removeField: function(...names) {
		// Update store reader for removed fields
		this.reader.onMetaChange({
			...this.reader.meta,
			fields: this.reader.meta.fields.filter(field => !names.includes(field.dataIndex))
		});
		// Remove modified flags on existing records for removed fields
		this.each(function(r) {
			for (const name of names) {
				delete r.data[name];
				if (r.modified) {
					delete r.modified[name];
				}
			}
		});
	}
});

/**
 * Add functions to ColumnModel to support dynamically adding and removing columns on a grid.
 */
Ext.override(Ext.grid.ColumnModel, {
	addColumn: function(...columns) {
		// Normalize columns
		const normalizedColumns = columns.map(column => typeof column === 'string'
				? {header: TCG.camelCaseToTitle(column.substring(column.lastIndexOf('.'))), dataIndex: column}
				: column);
		// Do not retain existing columns with matching data indexes
		const unmodifiedColumns = this.config.filter(column => !normalizedColumns.some(newColumn => newColumn.dataIndex === column.dataIndex));
		// Must reset the reference of this.config to avoid remaining columns' editors from being removed. The setConfig will reset this.config columns to the provided config.
		const config = this.config;
		config.splice(0, this.config.length, ...unmodifiedColumns, ...normalizedColumns);
		// Column IDs are auto-generated and must start from zero; force regeneration
		config.forEach(col => col.id = null);
		this.config = [];
		this.setConfig(config, false);
	},
	removeColumn: function(...colIndexes) {
		const remainingColumns = this.config.filter((column, index) => !colIndexes.includes(index));
		const removedColumns = this.config.filter((column, index) => colIndexes.includes(index));
		// this.config may have references elsewhere and must be mutated directly instead of recreated
		this.config.splice(0, this.config.length, ...remainingColumns);
		// Temporarily set this.config in order to properly remove old columns
		const config = this.config;
		this.config = removedColumns;
		this.setConfig(config, false);
	}
});

/**
 * Add functions to GridPanel to support dynamically adding and removing columns on a grid.
 * Each function will make the necessary updates to the child ColumnModel and Store.
 */
Ext.override(Ext.grid.GridPanel, {
	addColumn: function(...columns) {
		const storeFields = [];
		// As part of filling column metadata, filters are populated. Ignoring them for now for backward compatibility.
		// They should be added to a possible gridpanel grid filter plugin. Form grids use this api as well and could have different behavior.
		const filters = [];
		TCG.grid.fillColumnMetaData(columns, storeFields, filters, this);
		this.store.addField(...storeFields);
		this.getColumnModel().addColumn(...columns);
	},
	removeColumn: function(...dataIndexes) {
		this.store.removeField(...dataIndexes);
		const colIndexes = dataIndexes
				.map(dataIndex => this.getColumnModel().findColumnIndex(dataIndex))
				.filter(index => index > -1);
		this.getColumnModel().removeColumn(...colIndexes);
	}
});


/**
 * Overrides Column Model getColumnHeader so we can have blank column headers in the grid,
 * but in the menu item it can show the header.
 * Example - Empty column header that displays an action column (like download report)
 * When you click on the menu item to show/hide columns it will display View Report so the user knows what column it is
 * See GridView override below when building the menu columns
 */
Ext.override(Ext.grid.ColumnModel, {
	getColumnHeader: function(col, menu) {
		if (TCG.isTrue(menu) && this.config[col].menuHeader) {
			return this.config[col].menuHeader;
		}
		return this.config[col].header;
	}
});


Ext.override(Ext.grid.GridView, {
	//If set to true will not execute scrollToTop and will leave scrollbar where it is when grid is reloaded.
	holdPosition: false,
	beforeColMenuShow: function() {
		const colModel = this.cm,
				colCount = colModel.getColumnCount(),
				colMenu = this.colMenu;
		let i;

		colMenu.removeAll();

		for (i = 0; i < colCount; i++) {
			if (colModel.config[i].hideable !== false) {
				colMenu.add(new Ext.menu.CheckItem({
					text: colModel.getColumnHeader(i, true),
					itemId: 'col-' + colModel.getColumnId(i),
					checked: !colModel.isHidden(i),
					disabled: colModel.config[i].hideable === false,
					hideOnClick: false
				}));
			}
		}
	}
});


/**
 * Appends filter items to an existing grid contextmenu applicable to a clicked cell. The updated menu will be shown at the location of the event.
 * The filter items are obtained using TCG.grid.GridPanel's getGridCellContextMenuFilterItems(grid, row, column) function.
 *
 * @param gridPanel - the TCG gridpanel to use for obtaining cell filter items
 * @param currentContextMenu - the current contextmenu that should be displayed; is displayed if defined and no filter items are found
 * @param row - the row the grid clicked
 * @param column - the column of the grid clicked
 * @param event - the event of the current contextmenu; used for display
 */
TCG.grid.showGridPanelContextMenuWithAppendedCellFilterItems = async function(gridPanel, currentContextMenu, row, column, event) {
	let menu = currentContextMenu;
	if (TCG.isNotNull(gridPanel) && Ext.isNumber(row) && (gridPanel.getGridCellContextMenuFilterItems || gridPanel.getGridRowContextMenuItems)) {
		const menuContextMenuItems = [];
		if (gridPanel.getGridCellContextMenuItems) {
			const cellItems = await gridPanel.getGridCellContextMenuItems(gridPanel.grid, row, column);
			if (cellItems.length > 0) {
				menuContextMenuItems.push('-', ...cellItems);
			}
		}
		else {
			if (gridPanel.getGridCellContextMenuFilterItems) {
				const filterItems = await gridPanel.getGridCellContextMenuFilterItems(gridPanel.grid, row, column);
				if (filterItems.length > 0) {
					menuContextMenuItems.push('-', ...filterItems);
				}
			}
			if (gridPanel.getGridRowContextMenuItems) {
				const store = gridPanel.grid ? gridPanel.grid.getStore() : gridPanel.getStore();
				let record = void 0;
				if (store) {
					record = store.getAt(row);
				}
				const rowItems = await gridPanel.getGridRowContextMenuItems(gridPanel.grid, row, record);
				if (rowItems && rowItems.length > 0) {
					menuContextMenuItems.push('-', ...rowItems);
				}
			}
		}

		if (TCG.isNotNull(currentContextMenu) && TCG.isNotNull(currentContextMenu.initialConfig) && TCG.isNotNull(currentContextMenu.initialConfig.items)) {
			menuContextMenuItems.splice(0, 0, currentContextMenu.initialConfig.items);
		}
		else {
			menuContextMenuItems.splice(0, 1); // delete '-' at item 0
		}
		menu = new Ext.menu.Menu({items: menuContextMenuItems});
	}
	if (TCG.isNotNull(menu)) {
		menu.showAt(event.getXY());
	}
};


/**
 * A grid view type which allows a combination of some features of both the {@link Ext.ux.grid.BufferView} and the {@link Ext.grid.GroupingView}.
 *
 * This class extends {@link Ext.ux.grid.BufferView} to discriminately apply {@link Ext.grid.GroupingView} functionality as needed.
 */
TCG.grid.BufferedGroupView = class BufferedGroupView extends Ext.ux.grid.BufferView {

	constructor(config = {}) {
		super({
			stripeClass: 'x-grid3-row-alt-darker',
			rowOverCls: 'x-grid3-group-hover',
			...config,

			gridPanel: null,
			view: null,
			groups: [],
			groupsByKey: {},
			// Prevent standard groupField behavior implemented by other classes
			skipGroupHeaders: true
		});
		this.addListener('beforerefresh', () => this.populateGroups());
	}


	/**
	 * Populates the list of groups in the view based on the records in the attached store.
	 */
	populateGroups() {
		this.groups = [];
		this.groupsByKey = {};
		const records = this.grid.getStore().data.items;
		for (let index = 0; index < records.length; index++) {
			// Create or retrieve group for record
			const key = records[index].get(this.getGroupField());
			let group = this.groupsByKey[key];
			if (!group) {
				group = this.groupsByKey[key] = [];
				this.groups.push(group);
			}

			// Add item to group
			group.push(index);
		}
	}


	getGroupField() {
		return this.grid.store.getGroupState();
	}


	getRowClass(record, rowIndex, rowParams, dataStore) {
		return 'x-grid3-row-no-border';
	}


	doRender(columns, records, dataStore, startRow, colCount, stripe, onlyBody, ...restArgs) {
		// Guard-clause: Allow innerHTML adjustments by parent class when re-rendering only the body of pre-existing rows
		if (onlyBody) {
			return super.doRender(columns, records, dataStore, startRow, colCount, false, onlyBody, ...restArgs);
		}

		// Define alternate row style functions
		const rowClassStandard = this.getRowClass;
		const rowClassAlternate = rowClassStandard
				? (...args) => `${rowClassStandard.call(this, ...args)} ${this.stripeClass}`
				: () => this.stripeClass;

		// Render groups
		const renderElements = [];
		let row = 0;
		for (let i = 0; i < this.groups.length; i++) {
			const groupRecords = this.groups[i]
					.map(index => records[index]);
			this.getRowClass = (stripe && (i % 2 === 1)) ? rowClassAlternate : rowClassStandard;
			renderElements.push(super.doRender(columns, groupRecords, dataStore, startRow - row, colCount, false, onlyBody, ...restArgs));
			row += groupRecords.length;
		}
		this.getRowClass = rowClassStandard;

		return renderElements.join('');
	}
};
Ext.reg('buffered-group-view', TCG.grid.BufferedGroupView);

/**
 * Returns an updated JSON representation of the provided {@link Ext.data.Record}. A record has two sets of data: the original json and a data array
 * of potentially updated values. This function will clone the original json and update the clone's values with those from the data array.
 *
 * @param record an Ext.data.Record to get a merged JSON representation of the record's json and data values
 * @returns {*} a JSON object representation of the current record values
 */
TCG.grid.getUpdatedRecordAsJson = function(record) {
	if (TCG.isNull(record) || TCG.isNull(record.data) || TCG.isNull(record.json)) {
		return null;
	}

	// clone the original json for updating with record data
	const returnJson = TCG.clone(record.json);
	const keys = Object.keys(record.data);
	for (const key of keys) {
		const value = record.data[key];
		const keyPathElements = key.split('.');
		const length = keyPathElements.length;
		const lastObjectIndex = length - 1;
		let object = returnJson;
		for (let i = 0; i < length; i++) {
			const property = keyPathElements[i];
			const currentPropertyValue = object[property];
			if (i === lastObjectIndex) {
				// at the last property element, set the value if different
				if (TCG.isNotEquals(currentPropertyValue, value)) {
					object[property] = value;
				}
			}
			else {
				// navigate to the object to update
				let propertyObjectValue = currentPropertyValue;
				if (!propertyObjectValue) {
					// property was not part of the original record json, add it
					propertyObjectValue = {};
					object[property] = propertyObjectValue;
				}
				// update object reference for the child object
				object = propertyObjectValue;
			}
		}
	}
	return returnJson;
};

// Configure grid printing
Ext.override(Ext.ux.Printer.BaseRenderer, {
	stylesheetPath: [
		`${TCG.ExtRootUrl}/resources/css/ext-all.css`,
		`${TCG.ExtRootUrl}/ux/css/ux-all.css`,
		`${TCG.ExtRootUrl}/ux/printer/print.css`,
		'core/css/application.css'
	],
	// language=CSS
	styleOverrides: [TCG.trimWhitespace(`
		/* Encourage browser to print with backgrounds */
		* {
			-webkit-print-color-adjust: exact;
			color-adjust: exact;
		}

		/* Discourage page breaks that divide cells */
		tr, td, div {
			page-break-inside: avoid;
		}

		table {
			border-collapse: collapse;
			border: #a0a0a0 1px solid;
			font-family: arial;
			font-size: 11px;
		}

		thead {
			display: table-header-group;
		}

		th {
			font-weight: bold;
			background: #f0f0f0;
			BORDER: #a0a0a0 1px solid;
			padding: 4px;
		}

		td {
			BORDER: #a0a0a0 1px solid;
			padding: 2px;
		}

	`)],
	print: function(component) {
		const win = window.open('', '', 'toolbar=0,status=1,menubar=0,location=0,directories=0,scrollbars=1,resizable=1');
		win.document.write(this.generateHTML(component));
		win.document.close();
		win.print();
	},
	generateHTML: function(component) {
		const stylesheetsHtml = this.stylesheetPath.map(path => `<link href="${path}" rel="stylesheet" type="text/css" media="screen,print"/>`).join('\n');
		const styleOverridesHtml = this.styleOverrides.map(style => `<style type="text/css" media="screen,print">${style}</style>`).join('\n');
		return new Ext.XTemplate(TCG.trimWhitespace(`
			<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
			<html lang="en">
			<head>
				<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
				${stylesheetsHtml}
				${styleOverridesHtml}
				<title>${this.getTitle(component)}</title>
			</head>
			<body>${this.generateBody(component)}</body>
			</html>
		`)).apply(this.prepareData(component));
	}
});

Ext.override(Ext.ux.Printer.GridPanelRenderer, {
	headerTpl: new Ext.XTemplate(TCG.trimWhitespace(`
		<thead>
			<tr>
			<tpl for=".">
				<th>{header}</th>
			</tpl>
			</tr>
		</thead>
	`)),
	bodyTpl: new Ext.XTemplate(TCG.trimWhitespace(`
		<tr>
			<tpl for=".">
				<td align="{[values.align || 'left']}"
						class="\\{\\[values['{dataIndex}'].metadata.css || ''\\]\\}"
						\\{\\[values['{dataIndex}'].metadata.attr || ''\\]\\}>
					\\\{\[values['{dataIndex}'].value\\]\\}
				</td>
			</tpl>
		</tr>
	`)),
	prepareData: function(grid) {
		// Populate row values
		const data = [];
		const columns = this.getColumns(grid);
		grid.store.data.each(item => {
			const row = {};
			for (const column of columns) {
				const metadata = {};
				const value = item.data[column.dataIndex];
				row[column.dataIndex] = {
					value: column.renderer ? column.renderer(value, metadata, item) : value,
					metadata: metadata
				};
			}
			data.push(row);
		});
		return data;
	}
});

/**
 * Upon querying the server for grid stores, there are cases where the query and store's base params must be merged. One case used in grid filtering is for the restrictionList.
 * This function can be used to merge this parameter upon querying the server. A common use is to merge 2 restriction lists (strings of arrays of objects).
 *
 * @param store the grid store
 * @param queryOptions the query options used for the request
 * @param filterParameter the filter parameter to merge (e.g. restrictionList)
 */
TCG.grid.mergeStoreQueryParameter = function(store, queryOptions, filterParameter) {
	if (store.baseParams && store.baseParams[filterParameter] && queryOptions.params[filterParameter]) {
		queryOptions.params[filterParameter] = queryOptions.params[filterParameter].substring(0, queryOptions.params[filterParameter].length - 1) + ',' + store.baseParams[filterParameter].substring(1);
	}
};

/**
 * Columns with listeners do not properly render due to how we implemented the grid panel. This hook registers listeners for
 * a column after the grid panel is rendered by adding listeners using the eventListeners property. The event handlers in
 * eventListeners should look identical to how they would in the listeners property; just a new property name is used.
 */
TCG.grid.registerColumnEventListeners = function(grid) {
	const columnsWithEventListeners = grid.getColumnModel().getColumnsBy(function(column) {
		if (column.openCellDetailWindowOnDblClick === true) {
			column.addListener('dblclick', function(col, gridPanel, rowIndex, event) {
				new TCG.app.CloseWindow({
					title: 'Details for: ' + col.header,
					iconCls: 'help',
					modal: true,
					height: 300,
					width: 800,
					items: [{
						xtype: 'textarea', readOnly: true, value: gridPanel.getStore().getAt(rowIndex).data[col.dataIndex]
					}]
				});
			});
		}
		return TCG.isNotNull(column.eventListeners);
	});
	if (columnsWithEventListeners && columnsWithEventListeners.length > 0) {
		let listenerColumnCounter = 0;
		const columnLength = columnsWithEventListeners.length;
		for (; listenerColumnCounter < columnLength; listenerColumnCounter++) {
			const column = columnsWithEventListeners[listenerColumnCounter];
			const eventListeners = column.eventListeners;
			if (eventListeners) {
				const eventNames = Object.keys(eventListeners);
				let eventIndex = 0;
				const eventLength = eventNames.length;
				for (; eventIndex < eventLength; eventIndex++) {
					const eventName = eventNames[eventIndex];
					const event = eventListeners[eventName];
					if (eventName !== 'constructor') {
						column.addListener(eventName, event.fn || event, event.scope || eventListeners.scope, event.fn ? event : eventListeners);
					}
				}
			}
		}
	}
};

/**
 * This is a generic utility that uses the provided grid panel's column model and records to generate a comma delimited string.
 * It uses the column headers and data indexes for the values. Headers can be excluded if desired.
 *
 * @param gridPanel
 * @param excludeHeaders
 * @returns {string}
 */
TCG.grid.generateGridPanelCsvData = function(gridPanel, excludeHeaders) {
	const columnModel = gridPanel.getColumnModel();
	const columnHeaders = [];
	const columnDataIndex = [];
	for (let i = 0; i < columnModel.getColumnCount(false); i++) {
		const column = columnModel.getColumnAt(i);
		columnHeaders.push(column.header);
		columnDataIndex.push(column.dataIndex);
	}
	const csvHeaders = columnHeaders.join(',');
	const gridRecordDataStrings = [];

	const objectValueExtractorFunction = (object, field) => {
		if (field) {
			const value = object[field];
			if (!TCG.isBlank(value)) {
				// quote to protect against values with comma
				return `"${value}"`;
			}
		}
		return '';
	};

	gridPanel.getStore().each(record => {
		const recordData = [];
		columnDataIndex.forEach(dataIndex => recordData.push(objectValueExtractorFunction(record.data, dataIndex)));
		gridRecordDataStrings.push(recordData.join(','));
	});
	const csvData = gridRecordDataStrings.join('\n');
	return TCG.isTrue(excludeHeaders) ? csvData : `${csvHeaders}\n${csvData}`;
};

TCG.grid.LocalDateColumn = Ext.extend(Ext.grid.Column, {
		dateFormat: 'Y-m-d',
		renderer: TCG.renderDate
	}
);
Ext.grid.Column.types.localdate = TCG.grid.LocalDateColumn;
