Clifton.example.AnalyticsExampleWindow = Ext.extend(TCG.app.Window, {
	id: 'analyticsExampleWindow',
	iconCls: 'chart-bar',
	title: 'Analytics',
	width: 200,
	height: 100,

	items: [{}]
});


// advanced binding example: http://dev.sencha.com/deploy/ext-4.1.0-gpl/examples/charts/FormDashboard.html
const win = Ext4.createWidget('window', {
	x: 10,
	y: 75,
	width: 800,
	height: 500,
	title: 'Analytics',
	iconCls: 'chart-bar',
	renderTo: Ext4.getBody(),
	maximizable: true,
	layout: 'fit',
	items: [{
		xtype: 'panel',
		layout: 'fit',
		tbar: [{
			text: 'Save Chart',
			handler: function() {
				const chart = this.ownerCt.ownerCt.items.get(0);
				Ext.MessageBox.confirm('Confirm Download', 'Would you like to download the chart as an image?', function(choice) {
					if (choice === 'yes') {
						chart.save({
							type: 'image/png'
						});
					}
				});
			}
		}, '-', {
			text: 'Reload Data',
			handler: function() {
				const s = this.ownerCt.ownerCt.items.get(0).store;
				s.load();
			}
		}, '-', {
			enableToggle: true,
			pressed: false,
			text: 'Donut',
			toggleHandler: function(btn, pressed) {
				const chart = this.ownerCt.ownerCt.items.get(0);
				chart.series.first().donut = pressed ? 35 : false;
				chart.refresh();
			}
		}],
		items: [{
			xtype: 'chart',
			animate: true,
			store: {
				xtype: 'store.json',
				fields: ['id', 'name', 'value'],
				proxy: {
					type: 'ajax',
					url: 'accountingAnalyticsGroupingForAUM.json',
					reader: {
						type: 'json',
						root: 'analyticsGroupingList',
						idProperty: 'id'
					}
				},
				autoLoad: true
			},
			shadow: true,
			legend: {
				position: 'right'
			},
			insetPadding: 20,
			theme: 'Base:gradients',
			series: [{
				type: 'pie',
				field: 'value',
				showInLegend: true,
				donut: false,
				tips: {
					trackMouse: true,
					width: 200,
					//height: 35,
					renderer: function(storeItem, item) {
						//calculate percentage.
						let total = 0;
						storeItem.store.each(function(rec) {
							total += rec.get('value');
						});
						const v = storeItem.get('value');
						this.setTitle(storeItem.get('name') + '<br />' + TCG.numberFormat(v, '0,000') + ' (' + Math.round(v / total * 100) + '%)');
					}
				},
				highlight: {
					segment: {
						margin: 20
					}
				},
				label: {
					field: 'name',
					display: 'rotate',
					contrast: true,
					font: '14px Arial'
				}
			}]
		}]
	}]
});
win.show();
