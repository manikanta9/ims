package com.clifton.core.model;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.beans.MethodUtils;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The <code>ModelAwareUtils</code> class is a utility to help with working with {@link ModelAware} objects
 *
 * @author manderson
 */
public class ModelAwareUtils {


	/**
	 * Converts a list of ModelAware objects to a list of their corresponding model objects
	 */
	public static <T, M extends ModelAware<T>> List<T> getModelObjectList(List<M> modelAwareList) {
		if (modelAwareList == null) {
			return null;
		}
		return modelAwareList.stream().map(ModelAware::toModelObject).collect(Collectors.toList());
	}


	/**
	 * Converts a list of ModelAware objects to a list of their corresponding model objects
	 */
	public static <T, M> List<M> getModelObjectListUsingConverter(List<T> entityList, Function<T, M> modelConverter) {
		if (entityList == null) {
			return null;
		}
		return entityList.stream().map(modelConverter).collect(Collectors.toList());
	}


	/**
	 * Converts a ModelAware object to a model object using the given converter.  Supports null check, which returns null
	 */
	public static <T, M> M getModelObjectUsingConverter(T bean, Function<T, M> modelConverter) {
		if (bean == null) {
			return null;
		}
		return modelConverter.apply(bean);
	}


	/**
	 * Converts a ModelAware object to it's model object.  Supports null check, which returns null
	 */
	public static <T, M extends ModelAware<T>> T getModelObject(M modelAwareBean) {
		if (modelAwareBean == null) {
			return null;
		}
		return modelAwareBean.toModelObject();
	}


	/**
	 * Copies properties from a ModelAware object to it's model object. This function will copy properties with the exact same name and type,
	 * and will additionally:
	 * <ul>
	 *  <li>Copy enum properties that share the same name but different classes </li>
	 *  <li>Convert LocalDate values to Java 8 Date</li>
	 *  <li>Copy properties with the same name but different numeric types (i.e. ids)</li>
	 *  <li>Copy and convert list properties</li>
	 * </ul>
	 */
	public static <T, M extends ModelAware<T>> void copyPropertiesToModelObject(M source, T model, String... propertiesToIgnore) {
		copyPropertiesToTargetObject(source, model, propertiesToIgnore);
	}


	/**
	 * Copies properties from a source to target object; performs the same functionality as <code>copyPropertiesToModelObject</code> but without
	 * the requirement that the source object is ModelAware
	 */
	public static void copyPropertiesToTargetObject(Object source, Object model, String... propertiesToIgnore) {
		Map<String, PropertyDescriptor> propertyMap = BeanUtils.getPropertyDataTypes(model.getClass());
		for (PropertyDescriptor property : CollectionUtils.getFiltered(propertyMap.values(), p -> !CollectionUtils.getAsList(propertiesToIgnore).contains(p.getName()))) {
			if (property.getPropertyType().isEnum()) {
				copyEnumProperty(source, model, property);
			}
			else if (property.getPropertyType().isAssignableFrom(LocalDate.class) || property.getPropertyType().isAssignableFrom(OffsetDateTime.class)) {
				copyDateProperty(source, model, property);
			}
			else if (property.getPropertyType() != null) {
				copyProperty(source, model, property);
			}
			else {
				throw new RuntimeException(String.format("Unable to copy property [%s] to target object; property type [%s] is not recognized.", property.getName(), property.getPropertyType().getName()));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static void copyProperty(Object source, Object model, PropertyDescriptor property) {
		Class<?> propertyType = property.getPropertyType();
		Object getMethodResult = getSourceFieldValue(source, property);
		if (getMethodResult != null) {
			Method setMethod = property.getWriteMethod();
			if (propertyType.isAssignableFrom(List.class)) {
				ArrayList<Object> targetList = new ArrayList<>();
				ParameterizedType listType = (ParameterizedType) setMethod.getGenericParameterTypes()[0];
				Class<?> listClass = CoreClassUtils.getClass(listType.getActualTypeArguments()[0].getTypeName());
				for (Object getMethodResultItem : (List<?>) getMethodResult) {
					try {
						Object targetObject = listClass.getDeclaredConstructor().newInstance();
						copyPropertiesToTargetObject(getMethodResultItem, targetObject);
						targetList.add(targetObject);
					}
					catch (Exception e) {
						throw new RuntimeException(e);
					}
					MethodUtils.invoke(setMethod, model, targetList);
				}
			}
			else if (propertyType.isAssignableFrom(getMethodResult.getClass())) {
				MethodUtils.invoke(setMethod, model, getMethodResult);
			}
			else if (Number.class.isAssignableFrom(propertyType)) {
				try {
					MethodUtils.invoke(setMethod, model, propertyType.getDeclaredConstructor(String.class).newInstance(getMethodResult.toString()));
				}
				catch (Exception e) {
					throw new RuntimeException(e);
				}
			}
			else if (propertyType.isPrimitive() && !propertyType.equals(Character.TYPE) && !propertyType.equals(Boolean.TYPE)) {
				MethodUtils.invoke(setMethod, model, getNumericPrimitiveValue(getMethodResult, propertyType));
			}
		}
	}


	private static Object getNumericPrimitiveValue(Object sourceProperty, Class<?> targetPropertyType) {
		if (Number.class.isAssignableFrom(sourceProperty.getClass())) {
			Number number = (Number) sourceProperty;
			if (Long.TYPE.equals(targetPropertyType)) {
				return number.longValue();
			}
			if (Short.TYPE.equals(targetPropertyType)) {
				return number.shortValue();
			}
			if (Integer.TYPE.equals(targetPropertyType)) {
				return number.intValue();
			}
			if (Float.TYPE.equals(targetPropertyType)) {
				return number.floatValue();
			}
			if (Double.TYPE.equals(targetPropertyType)) {
				return number.doubleValue();
			}
			if (Byte.TYPE.equals(targetPropertyType)) {
				return number.byteValue();
			}
		}
		return null;
	}


	private static void copyEnumProperty(Object source, Object model, PropertyDescriptor property) {
		Object getMethodResult = getSourceFieldValue(source, property);
		if (getMethodResult != null) {
			Method setMethod = property.getWriteMethod();
			Method valueOfMethod = MethodUtils.getMethod(property.getPropertyType(), "valueOf", String.class);
			Object enumObject = MethodUtils.invoke(valueOfMethod, null, getMethodResult.toString());
			MethodUtils.invoke(setMethod, model, enumObject);
		}
	}


	private static void copyDateProperty(Object source, Object model, PropertyDescriptor property) {
		Date getMethodResult = (Date) getSourceFieldValue(source, property);
		if (getMethodResult != null) {
			Method setMethod = property.getWriteMethod();
			if (property.getPropertyType().isAssignableFrom(LocalDate.class)) {
				MethodUtils.invoke(setMethod, model, getMethodResult.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
			}
			else if (property.getPropertyType().isAssignableFrom(OffsetDateTime.class)) {
				MethodUtils.invoke(setMethod, model, getMethodResult.toInstant().atOffset(ZoneOffset.UTC));
			}
		}
	}


	private static Object getSourceFieldValue(Object source, PropertyDescriptor property) {
		PropertyDescriptor sourceProperty = BeanUtils.getPropertyDescriptor(source.getClass(), property.getName());
		if (sourceProperty != null) {
			Method getMethod = sourceProperty.getReadMethod();
			return MethodUtils.invoke(getMethod, source);
		}
		return null;
	}
}
