package com.clifton.core.model;

/**
 * The <code>ModelAware</code> class should be implemented by DTO objects that have an OpenAPI corresponding Model version.
 * This allows use of utility methods to help with converting from the dto object to the model object
 *
 * @author manderson
 */
public interface ModelAware<T> {


	public T toModelObject();
}
