package com.clifton.core.util.concurrent.synchronize;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.Set;


/**
 * <code>AbstractSynchronizable</code> defines the base properties and structure for a {@link Synchronizable}.
 *
 * @author NickK
 */
public abstract class AbstractSynchronizable implements Synchronizable {

	private final String secureAreaName;
	private final Set<String> lockKeySet;
	private final String lockMessage;
	private int millisecondsToWaitIfBusy;


	/**
	 * Creates a {@link Synchronizable} for a single lock key.
	 */
	public AbstractSynchronizable(String secureAreaName, String lockMessage, String lockKey) {
		this(secureAreaName, lockMessage, CollectionUtils.createHashSet(lockKey));
	}


	/**
	 * Creates a {@link Synchronizable} for a multiple lock keys.
	 */
	public AbstractSynchronizable(String secureAreaName, String lockMessage, Set<String> lockKeySet) {
		this.secureAreaName = secureAreaName;
		this.lockMessage = lockMessage;
		this.lockKeySet = lockKeySet;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder("Lock with secure area [").append(getSecureAreaName())
				.append("] message [").append(getLockMessage())
				.append("] and key(s) [").append(StringUtils.join(getLockKeySet(), ",")).append("]");

		if (getMillisecondsToWaitIfBusy() > 0) {
			result.append(", waiting up to ").append(getMillisecondsToWaitIfBusy()).append(" milliseconds");
		}
		return result.toString();
	}


	@Override
	public String getSecureAreaName() {
		return this.secureAreaName;
	}


	@Override
	public Set<String> getLockKeySet() {
		return this.lockKeySet;
	}


	@Override
	public String getLockMessage() {
		return this.lockMessage;
	}


	@Override
	public int getMillisecondsToWaitIfBusy() {
		return this.millisecondsToWaitIfBusy;
	}


	public void setMillisecondsToWaitIfBusy(int millisecondsToWaitIfBusy) {
		this.millisecondsToWaitIfBusy = millisecondsToWaitIfBusy;
	}
}
