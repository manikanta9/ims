package com.clifton.core.util.http.authenticator;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.http.CustomURIBuilderFactory;
import com.clifton.core.util.http.HttpEntityFactory;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.core.util.http.RedirectStatusException;
import com.clifton.core.util.http.SimpleHttpClientFactory;
import com.clifton.core.util.http.SimpleHttpResponse;
import com.clifton.core.util.http.cookie.AuthScopedPersistentCookieStoreManager;
import com.clifton.core.util.http.cookie.ImsCookieStore;
import com.clifton.core.util.http.cookie.PersistentCookieStore;
import org.apache.http.client.CookieStore;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.cookie.BasicClientCookie;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ImsAuthenticator</code> authenticates with IMS. Authentication is persisted for future requests with a {@link PersistentCookieStore}.
 *
 * @author jgommels
 */
public class ImsAuthenticator implements HttpAuthenticator {

	private static final String AUTH_COOKIE_NAME = "crowd.token_key";
	private static final String AUTH_PATH = "j_security_check.json";

	private static final AuthScopedPersistentCookieStoreManager cookieStoreManager = new AuthScopedPersistentCookieStoreManager();

	private final URI authUri;
	private String currentUsername;
	private String currentPassword;

	/**
	 * The context associated with the currently "logged in" user
	 */
	private HttpClientContext currentContext;


	public ImsAuthenticator(CustomURIBuilderFactory uriBuilderFactory) {
		this.authUri = uriBuilderFactory.newBuilder().appendToPath(AUTH_PATH).build();
	}


	public ImsAuthenticator(String url) throws URISyntaxException {
		this(new CustomURIBuilderFactory(new URI(url)));
	}


	@Override
	public HttpClientContext login(String username, String password) {
		LogUtils.info(ImsAuthenticator.class, "Logging in as user [" + username + "]...");

		ImsCookieStore cookieStore = getCookieStore(username);
		clearOldCookies(cookieStore);

		HttpClientContext clientContext = createClientContext(cookieStore);

		if (!cookieStore.containsCookie(AUTH_COOKIE_NAME, this.authUri.getHost())) {
			requestAuthCookie(username, password, clientContext);
		}
		this.currentUsername = username;
		this.currentPassword = password;

		this.currentContext = clientContext;
		return clientContext;
	}


	private void clearOldCookies(ImsCookieStore cookieStore) {
		cookieStore.clearExpired(new Date());
	}


	public void refreshLastLogin() {
		clearCookiesForCurrentUser();
		login(this.currentUsername, this.currentPassword);
	}


	public void clearCookiesForCurrentUser() {
		getCookieStore(this.currentUsername).clear();
	}


	public String getCurrentUsername() {
		return this.currentUsername;
	}


	public HttpClientContext getCurrentHttpClientContext() {
		return this.currentContext;
	}


	private HttpClientContext createClientContext(CookieStore cookieStore) {
		HttpClientContext context = new HttpClientContext();
		context.setCookieStore(cookieStore);
		return context;
	}


	private void requestAuthCookie(String username, String password, HttpClientContext clientContext) {
		Map<String, String> params = new HashMap<>();
		params.put("j_username", username);
		params.put("j_password", password);

		LogUtils.info(ImsAuthenticator.class, "Requesting authentication token for user [" + username + "]...");
		SimpleHttpResponse response = null;
		try {
			response = SimpleHttpClientFactory.getHttpClient().post(this.authUri, HttpEntityFactory.createHttpEntity(params), clientContext);
		}
		catch (Throwable e) {
			//Silently catch exception because a redirect is expected here and we don't need to do anything with it.
			if (!(e.getCause() instanceof RedirectStatusException)) {
				throw e;
			}
		}
		finally {
			HttpUtils.closeResponse(response);
		}

		BasicClientCookie authCookie = (BasicClientCookie) ((ImsCookieStore) clientContext.getCookieStore()).getCookie(AUTH_COOKIE_NAME, this.authUri.getHost());
		if (authCookie.getExpiryDate() == null) {
			authCookie.setExpiryDate(DateUtils.addMinutes(new Date(), 12 * 60));
		}
	}


	private ImsCookieStore getCookieStore(String username) {
		return cookieStoreManager.getCookieStore(this.authUri.getHost(), username);
	}
}
