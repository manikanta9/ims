package com.clifton.core.util.http;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import org.apache.http.NameValuePair;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.StringBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;


/**
 * @author jgommels
 */
public class MultipartEntityBuilderUtils {

	public static void addFile(String parameterName, MultipartFile multipartFile, MultipartEntityBuilder builder) {
		try {
			ContentType contentType = ContentType.create(FileUtils.getMimeContentType(multipartFile.getOriginalFilename()));
			builder.addBinaryBody(parameterName, multipartFile.getInputStream(), contentType, multipartFile.getOriginalFilename());
		}
		catch (IOException e) {
			LogUtils.error(LogCommand.ofThrowable(MultipartEntityBuilderUtils.class, e));
			throw new RuntimeException("Problem getting input stream", e);
		}
	}


	public static void addNameValuePairs(List<NameValuePair> values, MultipartEntityBuilder builder) {
		for (NameValuePair pair : CollectionUtils.getIterable(values)) {
			builder.addPart(pair.getName(), new StringBody(pair.getValue(), ContentType.TEXT_PLAIN));
		}
	}
}
