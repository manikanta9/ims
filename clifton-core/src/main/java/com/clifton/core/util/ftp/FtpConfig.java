package com.clifton.core.util.ftp;


/**
 * The <code>FtpConfig</code> encapsulates data necessary for creating a connection session to a remote server.
 * <ul>
 * <li>url</li>
 * <li>port (optional)</li>
 * <li>username</li>
 * <li>password</li>
 * <li>ftpProtocol (see {@link FtpProtocols})</li>
 * </ul>
 *
 * @author msiddiqui
 */
public class FtpConfig {

	private String url;

	/**
	 * Can be null.
	 */
	private Integer port;

	private FtpProtocols ftpProtocol;

	private String userName;
	private String password;

	/**
	 * The path to the folder on the FTP server where the file should be placed.
	 */
	private String remoteFolder;

	private String pgpPublicKey;
	private Boolean pgpArmor;
	private Boolean pgpIntegrityCheck;


	/**
	 * If true, indicates that Active FTP will be used, rather than Passive FTP.
	 */
	private boolean activeFtp;

	/**
	 * A string to prepend to the filename
	 */
	private String filenamePrefix;


	/**
	 * The SSH Private Key string
	 */
	private String sshPrivateKey;

	/**
	 * The SSH Public Ket string
	 */
	private String sshPublicKey;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FtpConfig() {
		// nothing for now
	}


	public FtpConfig(String url, Integer port, FtpProtocols ftpProtocol, String userName, String password, boolean activeFtp, String remoteFolder, String pgpPublicKey, Boolean pgpArmor, Boolean pgpIntegrityCheck) {
		this.url = FtpUtils.removePortFromUrl(url);
		if (port == null) {
			this.port = FtpUtils.getPortFromUrl(url);
		}
		else {
			this.port = port;
		}
		this.remoteFolder = remoteFolder;
		this.ftpProtocol = ftpProtocol;
		this.userName = userName;
		this.password = password;
		this.activeFtp = activeFtp;
		this.pgpPublicKey = pgpPublicKey;
		this.pgpArmor = pgpArmor;
		this.pgpIntegrityCheck = pgpIntegrityCheck;
	}


	public FtpConfig(String url, Integer port, FtpProtocols ftpProtocol, String userName, String password) {
		this(url, port, ftpProtocol, userName, password, false, null, null, Boolean.FALSE, Boolean.FALSE);
	}


	public FtpConfig(String url, FtpProtocols ftpProtocol, String userName, String password) {
		this(url, null, ftpProtocol, userName, password, false, null, null, Boolean.FALSE, Boolean.FALSE);
	}


	public FtpConfig(String url, FtpProtocols ftpProtocol) {
		this(url, ftpProtocol, null, null);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Placeholder for returning the ID of the secret if the password is stored in one.
	 * <p>
	 * Override on classes that extend FtpConfig
	 */
	public Integer getPasswordSecretId() {
		return null;
	}


	/**
	 * Placeholder for returning the ID of the secret if the password is stored in one.
	 * <p>
	 * Override on classes that extend FtpConfig
	 */
	public Integer getSshPrivateKeySecretId() {
		return null;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("FTP URL: ").append(getUrl()).append((getPort() != null) ? ":" + getPort() : "");
		sb.append("\n");

		sb.append("User Name: ").append(getUserName());
		sb.append("\n");

		sb.append("Protocol: ").append(getFtpProtocol());
		sb.append("\n");

		return sb.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUrl() {
		return this.url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public Integer getPort() {
		return this.port;
	}


	public void setPort(Integer port) {
		this.port = port;
	}


	public String getUserName() {
		return this.userName;
	}


	public void setUserName(String userName) {
		this.userName = userName;
	}


	public String getPassword() {
		return this.password;
	}


	public void setPassword(String password) {
		this.password = password;
	}


	public FtpProtocols getFtpProtocol() {
		return this.ftpProtocol;
	}


	public void setFtpProtocol(FtpProtocols ftpProtocol) {
		this.ftpProtocol = ftpProtocol;
	}


	public boolean isActiveFtp() {
		return this.activeFtp;
	}


	public void setActiveFtp(boolean activeFtp) {
		this.activeFtp = activeFtp;
	}


	public String getRemoteFolder() {
		return this.remoteFolder;
	}


	public void setRemoteFolder(String remoteFolder) {
		this.remoteFolder = remoteFolder;
	}


	public String getPgpPublicKey() {
		return this.pgpPublicKey;
	}


	public void setPgpPublicKey(String pgpPublicKey) {
		this.pgpPublicKey = pgpPublicKey;
	}


	public Boolean getPgpArmor() {
		return this.pgpArmor;
	}


	public void setPgpArmor(Boolean pgpArmor) {
		this.pgpArmor = pgpArmor;
	}


	public Boolean getPgpIntegrityCheck() {
		return this.pgpIntegrityCheck;
	}


	public void setPgpIntegrityCheck(Boolean pgpIntegrityCheck) {
		this.pgpIntegrityCheck = pgpIntegrityCheck;
	}


	public String getFilenamePrefix() {
		return this.filenamePrefix;
	}


	public void setFilenamePrefix(String filenamePrefix) {
		this.filenamePrefix = filenamePrefix;
	}


	public String getSshPrivateKey() {
		return this.sshPrivateKey;
	}


	public void setSshPrivateKey(String sshPrivateKey) {
		this.sshPrivateKey = sshPrivateKey;
	}


	public String getSshPublicKey() {
		return this.sshPublicKey;
	}


	public void setSshPublicKey(String sshPublicKey) {
		this.sshPublicKey = sshPublicKey;
	}
}
