package com.clifton.core.util.aws.s3;

import com.amazonaws.services.s3.AmazonS3URI;


/**
 * The {@link AWSS3Utils} class provided a collection of static utility methods for working with AWS S3 objects
 */
public class AWSS3Utils {
	private AWSS3Utils() {
		// Private constructor
	}

	public static AmazonS3URI getAmazonS3Uri(String s3DestinationPath) {
		AmazonS3URI s3Uri;

		if(s3DestinationPath.matches("^\\w+://.*")) {
			s3Uri = new AmazonS3URI(s3DestinationPath);
		} else {
			s3Uri = new AmazonS3URI("s3://" + s3DestinationPath);
		}

		return s3Uri;
	}

	public static String getS3FileKey(String path, String fileName) {
		if(path == null) {
			return fileName;
		}

		if(path.endsWith("/")) {
			return String.format("%s%s", path, fileName);
		} else {
			return String.format("%s/%s", path, fileName);
		}
	}
}
