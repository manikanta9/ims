package com.clifton.core.util.email;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;

import java.io.Serializable;


/**
 * The <code>Email</code> encapsulates all data required to send an email.
 * <ul>
 * <li>fromAddress (optional). If null/empty the underlying service should populate it with a default address.</li>
 * <li>toAddresses (required)</li>
 * <li>subject (required)</li>
 * <li>content (required)</li>
 * <li>contentType. (required) @see {@link MimeContentTypes}</li>
 * <li>attachments. Fully qualified file name and locations. (optional)</li>
 * </ul>
 *
 * @author msiddiqui
 */
public class Email implements Serializable {

	private String fromAddress;
	/**
	 * The "name" of the from contact
	 */
	private String fromName;

	//Used to send email on behalf of the from address
	private String senderAddress;

	private String senderName;

	private String[] toAddresses;

	private String[] ccAddresses;

	private String[] bccAddresses;

	private String subject;

	private Object content;

	private MimeContentTypes contentType;

	/**
	 * Representing fully qualified file name and locations to attach to the
	 * email.
	 */
	private FileWrapper[] attachments;


	/**
	 * Constructor for creating this object with its <b>required</b> attributes.
	 *
	 * @param fromAddress Email address from which the email is to be sent. If null/empty
	 *                    the underlying service will populate it with a default address.
	 * @param toAddresses Recipient's TO email addresses.
	 * @param subject     Subject of the email.
	 * @param content     contents of the email.
	 * @param contentType Type of content. Either "text/plain" or "text/html". See {@link MimeContentTypes}.
	 * @param attachments fully qualified file name and locations.
	 */
	public Email(String fromAddress, String[] toAddresses, String subject, Object content, MimeContentTypes contentType, FileWrapper... attachments) {
		this.fromAddress = fromAddress;
		this.toAddresses = toAddresses;
		this.subject = subject;
		this.content = content;
		this.contentType = contentType;
		this.attachments = attachments;
	}


	/**
	 * Constructor for creating this object with additional cc addresses.
	 *
	 * @param fromAddress Email address from which the email is to be sent. If null/empty
	 *                    the underlying service will populate it with a default address.
	 * @param toAddresses Recipient's TO email addresses.
	 * @param ccAddresses Recipient's CC email addresses.
	 * @param subject     Subject of the email.
	 * @param content     contents of the email.
	 * @param contentType Type of content. Either "text/plain" or "text/html". See {@link MimeContentTypes}.
	 * @param attachments fully qualified file name and locations.
	 */
	public Email(String fromAddress, String[] toAddresses, String[] ccAddresses, String subject, Object content, MimeContentTypes contentType, FileWrapper... attachments) {
		this.fromAddress = fromAddress;
		this.toAddresses = toAddresses;
		this.ccAddresses = ccAddresses;
		this.subject = subject;
		this.content = content;
		this.contentType = contentType;
		this.attachments = attachments;
	}


	/**
	 * Constructor for creating this object with all its attributes.
	 *
	 * @param fromAddress  Email address from which the email is to be sent. If null/empty
	 *                     the underlying service will populate it with a default address.
	 * @param toAddresses  Recipient's TO email addresses.
	 * @param ccAddresses  Recipient's CC email addresses. Can be null/empty.
	 * @param bccAddresses Recipient's BCC email addresses. Can be null/empty.
	 * @param subject      Subject of the email.
	 * @param content      contents of the email.
	 * @param contentType  Type of content. Either "text/plain" or "text/html". See {@link MimeContentTypes}.
	 * @param attachments  fully qualified file name and locations.
	 */
	public Email(String fromAddress, String[] toAddresses, String[] ccAddresses, String[] bccAddresses, String subject, Object content, MimeContentTypes contentType, FileWrapper... attachments) {
		this.fromAddress = fromAddress;
		this.toAddresses = toAddresses;
		this.ccAddresses = ccAddresses;
		this.bccAddresses = bccAddresses;
		this.subject = subject;
		this.content = content;
		this.contentType = contentType;
		this.attachments = attachments;
	}


	/**
	 * Constructor for creating this object with all its attributes.
	 *
	 * @param fromAddress   Email address from which the email is to be sent. If null/empty
	 *                      the underlying service will populate it with a default address.
	 * @param senderAddress Email address which will be used to send email "on behalf of" the from address
	 * @param toAddresses   Recipient's TO email addresses.
	 * @param ccAddresses   Recipient's CC email addresses. Can be null/empty.
	 * @param bccAddresses  Recipient's BCC email addresses. Can be null/empty.
	 * @param subject       Subject of the email.
	 * @param content       contents of the email.
	 * @param contentType   Type of content. Either "text/plain" or "text/html". See {@link MimeContentTypes}.
	 * @param attachments   fully qualified file name and locations.
	 */
	public Email(String fromAddress, String senderAddress, String[] toAddresses, String[] ccAddresses, String[] bccAddresses, String subject, Object content, MimeContentTypes contentType, FileWrapper... attachments) {
		this.fromAddress = fromAddress;
		this.senderAddress = senderAddress;
		this.toAddresses = toAddresses;
		this.ccAddresses = ccAddresses;
		this.bccAddresses = bccAddresses;
		this.subject = subject;
		this.content = content;
		this.contentType = contentType;
		this.attachments = attachments;
	}


	public Email(String fromName, String fromAddress, String senderName, String senderAddress, String[] toAddresses, String[] ccAddresses, String[] bccAddresses, String subject, Object content, MimeContentTypes contentType, FileWrapper... attachments) {
		this.fromAddress = fromAddress;
		this.fromName = fromName;
		this.senderAddress = senderAddress;
		this.senderName = senderName;
		this.toAddresses = toAddresses;
		this.ccAddresses = ccAddresses;
		this.bccAddresses = bccAddresses;
		this.subject = subject;
		this.content = content;
		this.contentType = contentType;
		this.attachments = attachments;
	}


	public String getFromName() {
		return this.fromName;
	}


	public void setFromName(String fromName) {
		this.fromName = fromName;
	}


	/**
	 * @return the email address from which the email is being sent. This is an optional field, so a null can be returned.
	 */
	public String getFromAddress() {
		return this.fromAddress;
	}


	/**
	 * @param fromAddress the fromAddress to set
	 */
	public void setFromAddress(String fromAddress) {
		this.fromAddress = fromAddress;
	}


	public String getSenderAddress() {
		return this.senderAddress;
	}


	public void setSenderAddress(String senderAddress) {
		this.senderAddress = senderAddress;
	}


	public String getSenderName() {
		return this.senderName;
	}


	public void setSenderName(String senderName) {
		this.senderName = senderName;
	}


	/**
	 * @return the toAddresses
	 */
	public String[] getToAddresses() {
		return this.toAddresses;
	}


	/**
	 * @param toAddresses the ToAddresses to set
	 */
	public void setToAddresses(String[] toAddresses) {
		this.toAddresses = toAddresses;
	}


	/**
	 * @return the ccAddresses
	 */
	public String[] getCcAddresses() {
		return this.ccAddresses;
	}


	/**
	 * @param ccAddresses the ccAddresses to set
	 */
	public void setCcAddresses(String[] ccAddresses) {
		this.ccAddresses = ccAddresses;
	}


	/**
	 * @return the bccAddresses
	 */
	public String[] getBccAddresses() {
		return this.bccAddresses;
	}


	/**
	 * @param bccAddresses the bccAddresses to set
	 */
	public void setBccAddresses(String[] bccAddresses) {
		this.bccAddresses = bccAddresses;
	}


	/**
	 * @return the subject
	 */
	public String getSubject() {
		return this.subject;
	}


	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}


	/**
	 * @return the content
	 */
	public Object getContent() {
		return this.content;
	}


	/**
	 * @param content the content to set
	 */
	public void setContent(Object content) {
		this.content = content;
	}


	/**
	 * @return the contentType
	 */
	public MimeContentTypes getContentType() {
		return this.contentType;
	}


	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(MimeContentTypes contentType) {
		this.contentType = contentType;
	}


	public FileWrapper[] getAttachments() {
		return this.attachments;
	}


	public void setAttachments(FileWrapper... attachments) {
		this.attachments = attachments;
	}


	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();

		sb.append("From: ").append(this.fromAddress).append("\n");

		sb.append("To: ").append(StringUtils.join(((!ArrayUtils.isEmpty(this.toAddresses)) ? this.toAddresses : null), "; ")).append("\n");

		if (!ArrayUtils.isEmpty(this.ccAddresses)) {
			sb.append("CC: ").append(StringUtils.join(this.ccAddresses, "; ")).append("\n");
		}

		if (!ArrayUtils.isEmpty(this.bccAddresses)) {
			sb.append("BCC: ").append(StringUtils.join(this.bccAddresses, "; ")).append("\n");
		}

		sb.append("Subject: ").append(this.subject).append("\n");

		if (!ArrayUtils.isEmpty(this.attachments)) {
			sb.append("Attachments:\n");

			for (FileWrapper attachment : this.attachments) {
				sb.append(attachment.getFileName());
				sb.append("\n");
			}
		}

		return sb.toString();
	}
}
