package com.clifton.core.util.email;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CoreStringUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileTypeMap;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * Provides email through an explicit email server instance.
 */
public class EmailHandlerImpl implements EmailHandler {

	/**
	 * Created in the constructor based on the subsequent properties
	 */
	private Session session;

	/**
	 * SMTP host name to be used.
	 */
	private String smtpHost;

	/**
	 * SMTP port to be used.
	 */
	private int smtpPort;

	/**
	 * Store Protocol to be used. Either {@code smtp} or {@code imap}
	 */
	private String storeProtocol;

	/**
	 * Transport Protocol to be used. Either {@code smtp} or {@code imap}
	 */
	private String transportProtocol;

	/**
	 * Default sender's email address.
	 */
	private String defaultFromAddress;

	/**
	 * If populated, all emails sent out would also BCC this address
	 * This is used for Compliance so a copy of all outgoing emails are copied to internal box for auditing
	 */
	private String bccAddress;

	/**
	 * If true, apply recipient filtering. Ex: if the environment is not production.
	 */
	private boolean allowedRecipientFilterEnabled;

	/**
	 * Pattern used to determine if the email recipients are valid recipients.
	 */
	private Pattern allowedRecipientFilter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public EmailHandlerImpl(String smtpHost, int smtpPort, String storeProtocol, String transportProtocol, String defaultFromAddress) {
		this(smtpHost, smtpPort, storeProtocol, transportProtocol, defaultFromAddress, null);
	}


	public EmailHandlerImpl(String smtpHost, int smtpPort, String storeProtocol, String transportProtocol, String defaultFromAddress, String bccAddress) {
		this.smtpHost = smtpHost;
		this.smtpPort = smtpPort;
		this.storeProtocol = storeProtocol;
		this.transportProtocol = transportProtocol;
		this.defaultFromAddress = defaultFromAddress;

		if (!StringUtils.isEmpty(bccAddress) && !CoreStringUtils.isEmailValid(bccAddress)) {
			throw new IllegalStateException("Provided BCC Address that will be included on all emails [" + bccAddress + "] is not a valid email address.");
		}
		this.bccAddress = bccAddress;


		Properties properties = new Properties();
		properties.put("mail.smtp.host", getSmtpHost());
		properties.put("mail.smtp.port", Integer.toString(getSmtpPort()));
		properties.put("mail.store.protocol", getStoreProtocol());
		properties.put("mail.transport.protocol", getTransportProtocol());

		// Since no user authentication is required we will create the session with Authenticator value as null.
		this.session = Session.getInstance(properties);
	}


	/**
	 * Creates a default {@code javax.mail.Session} to be used by all client
	 * code by acquiring the JNDI configured session specified by
	 * {@code jndiEMailSession}.
	 *
	 * @param jndiEMailSession e.g. java:comp/env/mail/SendGrid
	 */
	public EmailHandlerImpl(String jndiEMailSession) throws NamingException {
		super();
		Context initCtx = new InitialContext();
		this.session = (Session) initCtx.lookup(jndiEMailSession);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "Email Service: [" + this.smtpHost + ": " + this.smtpPort + "]";
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void send(Email email) {
		// Construct the message
		Message message = initializeMessage(email);

		if (isAllowedRecipientFilterEnabled()) {
			List<String> invalidRecipients = getInvalidEmailRecipients(message);
			if (!invalidRecipients.isEmpty()) {
				LogUtils.warn(LogCommand.ofMessage(getClass(), "Email with subject '" + email.getSubject() + "' not sent. Recipient emails do not match the recipient filter: ", invalidRecipients));
				return;
			}
		}

		try {
			// Email without attachments
			if (ArrayUtils.isEmpty(email.getAttachments())) {
				message.setContent(email.getContent(), email.getContentType().getValue());
			}
			// Email with attachments
			else {
				populateAttachments(email, message);
			}
			// Send the message
			Transport.send(message);
		}
		catch (MessagingException e) {
			throw new RuntimeException("Failed to send email:\n" + email, e);
		}
		LogUtils.info(LogCommand.ofMessage(getClass(), "Email sent:" + email));

	}


	@Override
	public void closeService() {
		try {
			this.session.getTransport(this.transportProtocol).close();
			LogUtils.info(getClass(), this + " closed!");
		}
		catch (MessagingException me) {
			LogUtils.error(getClass(), "Failed to close email service:" + this + "!", me);
			throw new RuntimeException(me);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////                  Helper Methods                  ///////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Populates the specified {@code message}'s recipients with email addresses.
	 *
	 * @param recipientType e.g. {@code Message.RecipientType.TO}
	 * @throws IllegalArgumentException if the specified {@code emailAddresses} are null or empty
	 */
	private void populateAddresses(Message.RecipientType recipientType, Message message, String[] emailAddresses) throws MessagingException {
		if (message == null) {
			throw new IllegalArgumentException("Message must be provided!");
		}
		for (String emailAddress : emailAddresses) {
			message.addRecipient(recipientType, new InternetAddress(emailAddress));
		}
	}


	/**
	 * Validates specified {@code email} and ensures all required attributes are
	 * present, and in the right format.
	 */
	private void validateEmail(Email email) {
		if (StringUtils.isEmpty(email.getSubject())) {
			throw new ValidationException("Email subject must be provided!");
		}

		if (email.getContent() == null) {
			throw new ValidationException("Email content must be provided!");
		}

		if (email.getContentType() == null) {
			throw new ValidationException("Email content type must be provided!");
		}

		// Validate FROM Address if provided
		if (!StringUtils.isEmpty(email.getFromAddress())) {
			validateEmailAddresses(email.getFromAddress());
		}

		// Validate TO addresses
		validateEmailAddresses(email.getToAddresses());

		// CC addresses are optional so validate only if provided
		if (!ArrayUtils.isEmpty(email.getCcAddresses())) {
			validateEmailAddresses(email.getCcAddresses());
		}

		// BCC addresses are optional so validate only if provided
		if (!ArrayUtils.isEmpty(email.getBccAddresses())) {
			validateEmailAddresses(email.getBccAddresses());
		}
	}


	/**
	 * Validates email addresses. At encountering the first invalid email address, exception is thrown and rest are ignored.
	 *
	 * @param emailAddresses to validate
	 * @throws IllegalArgumentException if specified {@code emailAddresses} is null/empty
	 * @throws ValidationException      thrown at encountering first invalid email address, ignoring others
	 */
	private void validateEmailAddresses(String... emailAddresses) {
		if (ArrayUtils.isEmpty(emailAddresses)) {
			throw new IllegalArgumentException("At least one email address must be provided!");
		}
		for (String emailAddress : emailAddresses) {
			if (!CoreStringUtils.isEmailValid(emailAddress)) {
				throw new ValidationException("Email address is not valid: " + emailAddress);
			}
		}
	}


	/**
	 * Gets email address recipients that do not match the recipient filter.
	 *
	 * @param message to validate
	 */
	private List<String> getInvalidEmailRecipients(Message message) {
		try {
			return ArrayUtils.getStream(message.getAllRecipients())
					.map(recipient -> ((InternetAddress) recipient).getAddress())
					.filter(address -> !getAllowedRecipientFilter()
							.matcher(address)
							.matches())
					.collect(Collectors.toList());
		}
		catch (Exception e) {
			throw new RuntimeException("An exception occurred while validating email recipients.", e);
		}
	}


	/**
	 * Validates the specified {@code email}, then initializes the Message and
	 * populates the addresses, and subject.
	 *
	 * @param email with all its required attributes set
	 * @return the {@link Message} object with its to/cc/bcc addresses, subject and sent date populated from the specified {@code email}
	 * @throws IllegalArgumentException if any of the required stuff is missing
	 */
	private Message initializeMessage(Email email) {
		// Ensure everything required for email is there
		validateEmail(email);
		MimeMessage message = new MimeMessage(this.session);

		try {
			//If the from address is specified in the email then use that
			if (!StringUtils.isEmpty(email.getFromAddress())) {
				InternetAddress fromAddress = new InternetAddress(email.getFromAddress());
				if (!StringUtils.isEmpty(email.getFromName())) {
					fromAddress.setPersonal(email.getFromName());
				}
				message.setFrom(fromAddress);
			}
			//Otherwise use the pre-configured default from address
			else {
				LogUtils.warn(getClass(), "Email does not have its fromAddress set! Setting it to default value: " + this.defaultFromAddress);
				message.setFrom(new InternetAddress(this.defaultFromAddress));
				//Also set it in the email
				email.setFromAddress(this.defaultFromAddress);
			}
			message.setSentDate(new Date());

			//Populate TO addresses
			populateAddresses(Message.RecipientType.TO, message, email.getToAddresses());

			//Populate only if available
			if (!ArrayUtils.isEmpty(email.getCcAddresses())) {
				populateAddresses(Message.RecipientType.CC, message, email.getCcAddresses());
			}

			//Populate only if available
			if (!ArrayUtils.isEmpty(email.getBccAddresses())) {
				populateAddresses(Message.RecipientType.BCC, message, email.getBccAddresses());
			}

			// If bcc address is supplied
			if (!StringUtils.isEmpty(this.bccAddress)) {
				populateAddresses(Message.RecipientType.BCC, message, new String[]{this.bccAddress});
			}

			message.setSubject(email.getSubject());

			//Set the "sender" that will send on behalf of the from address
			if (!StringUtils.isEmpty(email.getSenderAddress())) {
				InternetAddress senderAddress = new InternetAddress(email.getSenderAddress());
				if (!StringUtils.isEmpty(email.getSenderName())) {
					senderAddress.setPersonal(email.getSenderName());
				}
				message.setSender(senderAddress);
			}
		}
		catch (AddressException e) {
			throw new ValidationException(e.getMessage(), e);
		}
		catch (MessagingException e) {
			throw new RuntimeException("Failed to initialize email:\n" + email, e);
		}
		catch (UnsupportedEncodingException e) {
			throw new RuntimeException("Failed to initialize email with proper encoding: " + email, e);
		}

		return message;
	}


	/**
	 * Attaches files to the specified {@code message} and removing any time-stamps from file names if present.
	 *
	 * @param email   must have its attachments populated
	 * @param message
	 * @throws IllegalArgumentException if the specified {@code email} does not specify attachments
	 */
	private void populateAttachments(Email email, Message message) {

		if (ArrayUtils.isEmpty(email.getAttachments())) {
			throw new IllegalArgumentException("Attachments must be provided!");
		}

		try {
			// Set the message part for attachments
			BodyPart messageBody = new MimeBodyPart();

			// Fill the message body contents
			messageBody.setContent(email.getContent(), email.getContentType().getValue());

			Multipart multipart = new MimeMultipart();
			// Add the body part containing the message but not yet the attachment.
			multipart.addBodyPart(messageBody);
			// Now to attach the attachments
			for (int i = 0; i < email.getAttachments().length; i++) {
				if (!StringUtils.isEmpty(email.getAttachments()[i].getFileName())) {
					BodyPart attachment = new MimeBodyPart();
					// Get the attachments
					FileWrapper fileWrapper = email.getAttachments()[i];
					DataSource source = new FileContainerDataSource(fileWrapper.getFile().toFileContainer());
					// Set the data handler to the attachment
					attachment.setDataHandler(new DataHandler(source));
					// Set the filename
					attachment.setFileName(fileWrapper.getFileName());
					// Add the attachment
					multipart.addBodyPart(attachment);
				}
			}

			// Put the message part and the attachment part in the message
			message.setContent(multipart);
		}
		catch (MessagingException e) {
			throw new RuntimeException("Failed to populate attachments for email:\n" + email, e);
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////              Getter and Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isAllowedRecipientFilterEnabled() {
		return this.allowedRecipientFilterEnabled;
	}


	public void setAllowedRecipientFilterEnabled(boolean allowedRecipientFilterEnabled) {
		this.allowedRecipientFilterEnabled = allowedRecipientFilterEnabled;
	}


	public Pattern getAllowedRecipientFilter() {
		return this.allowedRecipientFilter;
	}


	public void setAllowedRecipientFilter(Pattern allowedRecipientFilter) {
		this.allowedRecipientFilter = allowedRecipientFilter;
	}


	public Session getSession() {
		return this.session;
	}


	@Override
	public String getSmtpHost() {
		return this.smtpHost;
	}


	public int getSmtpPort() {
		return this.smtpPort;
	}


	public String getStoreProtocol() {
		return this.storeProtocol;
	}


	public String getTransportProtocol() {
		return this.transportProtocol;
	}


	@Override
	public String getDefaultFromAddress() {
		return this.defaultFromAddress;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private class FileContainerDataSource implements DataSource {

		private FileContainer file;
		private FileTypeMap typeMap = null;


		public FileContainerDataSource(FileContainer file) {
			this.file = file;
		}


		@Override
		public InputStream getInputStream() throws IOException {
			return this.file.getInputStream();
		}


		@Override
		public OutputStream getOutputStream() throws IOException {
			return this.file.getOutputStream();
		}


		@Override
		public String getContentType() {
			// check to see if the type map is null?
			if (this.typeMap == null) {
				return FileTypeMap.getDefaultFileTypeMap().getContentType(this.file.getName());
			}
			else {
				return this.typeMap.getContentType(this.file.getName());
			}
		}


		@Override
		public String getName() {
			return this.file.getName();
		}
	}
}
