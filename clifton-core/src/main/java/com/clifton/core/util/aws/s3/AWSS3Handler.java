package com.clifton.core.util.aws.s3;

import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;


/**
 * Service interface for handling AWS S3 functionality.
 *
 * @author rtschumper
 */
public interface AWSS3Handler {

	/**
	 * Sends the file to the specified S3 bucket using system credentials
	 *
	 * @param region
	 * @param s3DestinationPath
	 * @param fileWrapper
	 * @param endpointOverride Used for testing purposes only to send to a specified MockS3 endpoint
	 */
	public void send(final String region, final String s3DestinationPath, FileWrapper fileWrapper, String endpointOverride);

	/**
	 * Sends the file to the specified S3 bucket using system credentials
	 *
	 * @param region
	 * @param s3DestinationPath
	 * @param fileWrapper
	 * @param accessKey Used for manual override of the access key set in the properties file
	 * @param secretKey Used for manual override of the secret key set in the properties file
	 */
	public void send(String region, String s3DestinationPath, FileWrapper fileWrapper, String accessKey, String secretKey);

	public void send(String region, String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength, String endpointOverride);

	public void send(String region, String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength, String accessKey, String secretKey);

	public void send(String region, String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength);

	/**
	 * Retrieves a file from the specified S3 bucket
	 *
	 * @param bucketName
	 * @param region
	 * @param fileName Unique filename as it is stored in S3, otherwise know as the "key"
	 * @param endpointOverride Used for testing purposes only to retrieve from a specified MockS3 endpoint
	 * @return The path of the file that is locally saved
	 */
	public String retrieve(final String region, final String bucketName, final String fileName, String endpointOverride);


	/**
	 * Uses a regex expression to ensure that the bucket name is valid based on a number of guidelines from AWS Docs
	 *
	 * @param bucket
	 */
	public void validateBucketName(String bucket);
}
