package com.clifton.core.util;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import org.bouncycastle.bcpg.ArmoredOutputStream;
import org.bouncycastle.jce.provider.BouncyCastleProvider;
import org.bouncycastle.openpgp.PGPCompressedData;
import org.bouncycastle.openpgp.PGPCompressedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedData;
import org.bouncycastle.openpgp.PGPEncryptedDataGenerator;
import org.bouncycastle.openpgp.PGPEncryptedDataList;
import org.bouncycastle.openpgp.PGPException;
import org.bouncycastle.openpgp.PGPLiteralData;
import org.bouncycastle.openpgp.PGPLiteralDataGenerator;
import org.bouncycastle.openpgp.PGPObjectFactory;
import org.bouncycastle.openpgp.PGPOnePassSignatureList;
import org.bouncycastle.openpgp.PGPPrivateKey;
import org.bouncycastle.openpgp.PGPPublicKey;
import org.bouncycastle.openpgp.PGPPublicKeyEncryptedData;
import org.bouncycastle.openpgp.PGPPublicKeyRing;
import org.bouncycastle.openpgp.PGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.PGPSecretKey;
import org.bouncycastle.openpgp.PGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.PGPSignatureList;
import org.bouncycastle.openpgp.PGPUtil;
import org.bouncycastle.openpgp.bc.BcPGPObjectFactory;
import org.bouncycastle.openpgp.bc.BcPGPPublicKeyRingCollection;
import org.bouncycastle.openpgp.bc.BcPGPSecretKeyRingCollection;
import org.bouncycastle.openpgp.operator.bc.BcPGPDataEncryptorBuilder;
import org.bouncycastle.openpgp.operator.bc.BcPublicKeyKeyEncryptionMethodGenerator;
import org.bouncycastle.openpgp.operator.jcajce.JcaPGPDigestCalculatorProviderBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePBESecretKeyDecryptorBuilder;
import org.bouncycastle.openpgp.operator.jcajce.JcePublicKeyDataDecryptorFactoryBuilder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.SecureRandom;
import java.security.Security;
import java.util.Date;
import java.util.Iterator;


/**
 * The <code>PGPFileUtils</code> pgp decryption/encryption utils
 * <p>
 * NOTE: To use this, lib/bouncycastle/jce_policy-x.zip must extracted to C:\Java\jdk-XXX\jre\lib\security in order to support larger key sizes.
 * See http://www.ngs.ac.uk/tools/jcepolicyfiles
 *
 * @author mwacker
 */
public class PGPFileUtils {

	private static final String PROVIDER = "BC";


	private static PGPPrivateKey findSecretKey(String key, long keyID, char[] pass) throws IOException, PGPException {
		try (ByteArrayInputStream keyIn = new ByteArrayInputStream(key.getBytes())) {
			PGPSecretKeyRingCollection pgpSec = new BcPGPSecretKeyRingCollection(PGPUtil.getDecoderStream(keyIn));
			PGPSecretKey pgpSecKey = pgpSec.getSecretKey(keyID);

			if (pgpSecKey == null) {
				return null;
			}

			return pgpSecKey.extractPrivateKey(new JcePBESecretKeyDecryptorBuilder(new JcaPGPDigestCalculatorProviderBuilder().setProvider(PROVIDER).build()).setProvider(
					PROVIDER).build(pass));
		}
	}


	public static void decrypt(String inputFile, String outputFile, String secretKey, String pw) {
		InputStream in = null;
		OutputStream out = null;
		boolean hasError = false;
		inputFile = FileUtils.normalizeFilePathSeparators(inputFile);
		outputFile = FileUtils.normalizeFilePathSeparators(outputFile);
		try {
			in = new BufferedInputStream(new FileInputStream(inputFile));
			out = new BufferedOutputStream(new FileOutputStream(outputFile));

			decryptFile(in, out, secretKey, pw.toCharArray());
		}
		catch (Throwable e) {
			hasError = true;
			throw new RuntimeException("Failed to decrypt file [" + inputFile + "].", e);
		}
		finally {
			if (in != null) {
				try {
					in.close();
				}
				catch (Throwable e) {
					LogUtils.warn(PGPFileUtils.class, "Failed to close input stream.", e);
				}
			}
			if (out != null) {
				try {
					out.close();
				}
				catch (Throwable e) {
					LogUtils.warn(PGPFileUtils.class, "Failed to close output stream.", e);
				}
			}
			if (hasError) {
				try {
					FileUtils.delete(new File(outputFile));
				}
				catch (Throwable e) {
					LogUtils.warn(PGPFileUtils.class, "Failed to delete file [" + outputFile + "].", e);
				}
			}
		}
	}


	private static void decryptFile(InputStream in, OutputStream out, String key, char[] password) throws Exception {
		Security.addProvider(new BouncyCastleProvider());

		in = org.bouncycastle.openpgp.PGPUtil.getDecoderStream(in);

		PGPObjectFactory pgpF = new BcPGPObjectFactory(in);
		PGPEncryptedDataList enc;

		Object o = pgpF.nextObject();

		// the first object might be a PGP marker packet. 
		if (o instanceof PGPEncryptedDataList) {
			enc = (PGPEncryptedDataList) o;
		}
		else {
			enc = (PGPEncryptedDataList) pgpF.nextObject();
		}

		// find the secret key 
		@SuppressWarnings("unchecked")
		Iterator<PGPPublicKeyEncryptedData> it = enc.getEncryptedDataObjects();
		PGPPrivateKey sKey = null;
		PGPPublicKeyEncryptedData pbe = null;

		while (sKey == null && it.hasNext()) {
			pbe = it.next();
			sKey = findSecretKey(key, pbe.getKeyID(), password);
		}

		if (sKey == null) {
			throw new IllegalArgumentException("Secret key for message not found.");
		}

		InputStream clear = pbe.getDataStream(new JcePublicKeyDataDecryptorFactoryBuilder().setProvider(PROVIDER).setContentProvider(PROVIDER).build(sKey));
		PGPObjectFactory plainFact = new BcPGPObjectFactory(clear);
		Object message = plainFact.nextObject();

		if (message instanceof PGPSignatureList) {
			message = plainFact.nextObject();
		}

		if (message instanceof PGPCompressedData) {

			PGPCompressedData cData = (PGPCompressedData) message;
			PGPObjectFactory pgpFact = new BcPGPObjectFactory(cData.getDataStream());

			message = pgpFact.nextObject();
			// if the first element is the signature, ignore it
			if (message instanceof PGPOnePassSignatureList) {
				message = pgpFact.nextObject();
			}
			if (message instanceof PGPSignatureList) {
				message = pgpFact.nextObject();
			}
		}

		// read the decrypted data
		if (message instanceof PGPLiteralData) {
			PGPLiteralData ld = (PGPLiteralData) message;

			try (InputStream unc = ld.getInputStream()) {
				int ch;
				while ((ch = unc.read()) >= 0) {
					out.write(ch);
				}
			}
		}
		else if (message instanceof PGPOnePassSignatureList) {
			throw new PGPException("Message is not a simple encrypted file - PGPOnePassSignatureList.");
		}
		else {
			throw new PGPException("Message is not a simple encrypted file - type unknown.");
		}

		if (pbe.isIntegrityProtected()) {
			if (!pbe.verify()) {
				throw new PGPException("Message failed integrity check");
			}
		}
	}


	public static void encrypt(String inputFileNameAndPath, String outputFileNameAndPath, String publicKey) {
		//TODO Setting armor to true and integrity check to false by default to ensure backwards compatibility, but may want to revisit this - e.g. armor should perhaps only be true for e-mail
		encrypt(inputFileNameAndPath, outputFileNameAndPath, publicKey, true, false);
	}


	public static void encrypt(String inputFileNameAndPath, String outputFileNameAndPath, String publicKey, boolean armor, boolean withIntegrityCheck) {
		OutputStream out = null;
		boolean hasError = false;
		inputFileNameAndPath = FileUtils.normalizeFilePathSeparators(inputFileNameAndPath);
		outputFileNameAndPath = FileUtils.normalizeFilePathSeparators(outputFileNameAndPath);
		try {
			out = new BufferedOutputStream(new FileOutputStream(outputFileNameAndPath));
			encryptFile(out, new File(inputFileNameAndPath), publicKey, armor, withIntegrityCheck);
		}
		catch (Throwable e) {
			hasError = true;
			throw new RuntimeException("Failed to decrypt file [" + inputFileNameAndPath + "].", e);
		}
		finally {
			closeStream(out, "Failed to close output stream to file [" + outputFileNameAndPath + "].");
			if (hasError) {
				try {
					File outputFile = new File(outputFileNameAndPath);
					if (outputFile.exists()) {
						FileUtils.delete(outputFile);
					}
				}
				catch (Throwable e) {
					LogUtils.warn(PGPFileUtils.class, "Failed to delete file [" + outputFileNameAndPath + "].", e);
				}
			}
		}
	}


	private static void encryptFile(OutputStream out, File inputFile, String publicKey, boolean armor, boolean withIntegrityCheck) throws IOException, PGPException {
		Security.addProvider(new BouncyCastleProvider());

		PGPPublicKey encKey = readPublicKey(publicKey);

		if (armor) {
			out = new ArmoredOutputStream(out);
		}

		// TODO: Write the compressed file to the HDD using a buffered stream (BufferedOutputStream) and then re-read to encrypt.  Can't get this working at the current moment.
		ByteArrayOutputStream bOut = null;
		try {
			bOut = compressInputFileToByteStream(inputFile);

			BcPGPDataEncryptorBuilder dataEncryptor = new BcPGPDataEncryptorBuilder(PGPEncryptedData.TRIPLE_DES);
			dataEncryptor.setWithIntegrityPacket(withIntegrityCheck);
			dataEncryptor.setSecureRandom(new SecureRandom());

			PGPEncryptedDataGenerator encryptedDataGenerator = new PGPEncryptedDataGenerator(dataEncryptor);
			encryptedDataGenerator.addMethod(new BcPublicKeyKeyEncryptionMethodGenerator(encKey));

			OutputStream encryptFileOut = null;
			try {
				byte[] bytes = bOut.toByteArray();
				encryptFileOut = encryptedDataGenerator.open(out, bytes.length);
				encryptFileOut.write(bytes);
			}
			finally {
				if (encryptFileOut != null) {
					FileUtils.close(encryptFileOut);
				}
			}
		}
		finally {
			if (bOut != null) {
				FileUtils.close(bOut);
			}
		}
	}


	private static PGPPublicKey readPublicKey(String publicKey) throws IOException, PGPException {

		ByteArrayInputStream keyIn = null;
		try {
			keyIn = new ByteArrayInputStream(publicKey.getBytes());
			InputStream in = PGPUtil.getDecoderStream(keyIn);

			PGPPublicKeyRingCollection pgpPub = new BcPGPPublicKeyRingCollection(in);
			//
			// iterate through the key rings.
			//
			Iterator<PGPPublicKeyRing> rIt = pgpPub.getKeyRings();

			while (rIt.hasNext()) {
				PGPPublicKeyRing kRing = rIt.next();
				Iterator<PGPPublicKey> kIt = kRing.getPublicKeys();

				while (kIt.hasNext()) {
					PGPPublicKey k = kIt.next();

					if (k.isEncryptionKey()) {
						return k;
					}
				}
			}

			throw new IllegalArgumentException("Can't find encryption key in key ring.");
		}
		finally {
			if (keyIn != null) {
				FileUtils.close(keyIn);
			}
		}
	}


	public static ByteArrayOutputStream compressInputFileToByteStream(File inputFile) {
		ByteArrayOutputStream bOut;
		PGPCompressedDataGenerator comData = null;
		try {
			bOut = new ByteArrayOutputStream();
			comData = new PGPCompressedDataGenerator(PGPCompressedData.BZIP2);

			writeFileToLiteralData(comData.open(bOut), PGPLiteralData.BINARY, inputFile);
			return bOut;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed compress [" + inputFile.getAbsolutePath() + "] before encrypting.", e);
		}
		finally {
			if (comData != null) {
				try {
					comData.close();
				}
				catch (Throwable e) {
					LogUtils.warn(PGPFileUtils.class, "Failed to close compressed output stream.", e);
				}
			}
		}
	}


	/**
	 * Writing our own implementation of PGPUtil.writeFileToLiteralData because they do not use try catch to close the streams
	 * that they create.
	 */
	private static void writeFileToLiteralData(OutputStream out, char fileType, File file) throws IOException {
		PGPLiteralDataGenerator lData = new PGPLiteralDataGenerator();
		OutputStream pOut = null;
		try {
			pOut = lData.open(out, fileType, file.getName(), file.length(), new Date(file.lastModified()));
			pipeFileContents(file, pOut, 4096);
		}
		finally {
			closeStream(pOut, "Failed to close literal data packet stream.");
		}
	}


	/**
	 * Writing our own implementation of PGPUtil.pipeFileContents because they do not use try catch to close the streams
	 * that they create.
	 */
	private static void pipeFileContents(File file, OutputStream pOut, int bufSize) throws IOException {
		FileInputStream in = null;
		try {
			in = new FileInputStream(file);
			byte[] buf = new byte[bufSize];

			int len;
			while ((len = in.read(buf)) > 0) {
				pOut.write(buf, 0, len);
			}
		}
		finally {
			closeStream(in, "Failed to close input stream for [" + file.getAbsolutePath() + "].");
		}
	}


	private static void closeStream(Closeable stream, String failMessage) {
		if (stream != null) {
			try {
				stream.close();
			}
			catch (Throwable e) {
				LogUtils.warn(PGPFileUtils.class, failMessage, e);
			}
		}
	}
}
