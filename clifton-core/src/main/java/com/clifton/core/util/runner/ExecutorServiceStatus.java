package com.clifton.core.util.runner;

import com.clifton.core.concurrent.NamedThreadFactory;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * The <code>ExecutorServiceStatus</code> contains stats and data about an {@link ExecutorService}, particularly for a {@link ThreadPoolExecutor}. If the {@link ExecutorService}
 * is not an instance of {@link ThreadPoolExecutor}, only the name of the executor will be defined.
 */
public class ExecutorServiceStatus {

	private final String name;
	private final Boolean running;
	// Task details
	private final Integer activeTaskCount;
	private final Long completedTaskCount;
	private final Long taskCount;
	private final Integer queuedCount;
	// Pool details
	private final Integer corePoolSize;
	private final Integer poolSize;
	private final Integer maxPoolSize;
	private final Integer largestPoolSize;
	private final Long keepAliveSeconds;
	private final String threadPrefix;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public ExecutorServiceStatus(String name, ExecutorService service) {
		this.name = name;
		if (service instanceof ThreadPoolExecutor) {
			ThreadPoolExecutor threadPoolExecutor = (ThreadPoolExecutor) service;
			this.running = !threadPoolExecutor.isShutdown() && !threadPoolExecutor.isTerminated() && !threadPoolExecutor.isTerminating();
			this.activeTaskCount = threadPoolExecutor.getActiveCount();
			this.completedTaskCount = threadPoolExecutor.getCompletedTaskCount();
			this.taskCount = threadPoolExecutor.getTaskCount();
			this.queuedCount = threadPoolExecutor.getQueue().size();
			this.corePoolSize = threadPoolExecutor.getCorePoolSize();
			this.maxPoolSize = threadPoolExecutor.getMaximumPoolSize();
			this.largestPoolSize = threadPoolExecutor.getLargestPoolSize();
			this.poolSize = threadPoolExecutor.getPoolSize();
			this.keepAliveSeconds = threadPoolExecutor.getKeepAliveTime(TimeUnit.SECONDS);
			if (threadPoolExecutor.getThreadFactory() instanceof NamedThreadFactory) {
				this.threadPrefix = ((NamedThreadFactory) threadPoolExecutor.getThreadFactory()).getPrefix();
			}
			else {
				this.threadPrefix = null;
			}
		}
		else {
			// Unrecognized type; set all values to null
			this.running = null;
			this.activeTaskCount = null;
			this.completedTaskCount = null;
			this.taskCount = null;
			this.queuedCount = null;
			this.corePoolSize = null;
			this.maxPoolSize = null;
			this.largestPoolSize = null;
			this.poolSize = null;
			this.keepAliveSeconds = null;
			this.threadPrefix = null;
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public Boolean getRunning() {
		return this.running;
	}


	/**
	 * Returns the number of tasks actively being executed by a worker thread.
	 */
	public Integer getActiveTaskCount() {
		return this.activeTaskCount;
	}


	/**
	 * Returns the number of tasks that have been completed.
	 */
	public Long getCompletedTaskCount() {
		return this.completedTaskCount;
	}


	/**
	 * Returns the number of tasks that were ever scheduled to run.
	 */
	public Long getTaskCount() {
		return this.taskCount;
	}


	/**
	 * Returns the number of tasks that are queued for execution.
	 */
	public Integer getQueuedCount() {
		return this.queuedCount;
	}


	/**
	 * Returns the number of configured core threads that can be available to handle task execution.
	 */
	public Integer getCorePoolSize() {
		return this.corePoolSize;
	}


	/**
	 * Returns the number of threads that are actively working on or available to work on tasks.
	 */
	public Integer getPoolSize() {
		return this.poolSize;
	}


	/**
	 * Returns the number of configured maximum threads that can be available to handle task execution.
	 */
	public Integer getMaxPoolSize() {
		return this.maxPoolSize;
	}


	/**
	 * Returns the largest number of threads that were actively working on or available to work on tasks at the same time.
	 */
	public Integer getLargestPoolSize() {
		return this.largestPoolSize;
	}


	/**
	 * Returns the number of seconds a create thread should remain idle, if pool size is above core pool size, before it is destroyed.
	 */
	public Long getKeepAliveSeconds() {
		return this.keepAliveSeconds;
	}


	/**
	 * Returns the prefix for threads created by the {@link java.util.concurrent.ThreadFactory} of an ExecutorService.
	 */
	public String getThreadPrefix() {
		return this.threadPrefix;
	}
}
