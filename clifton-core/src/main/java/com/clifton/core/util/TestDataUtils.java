package com.clifton.core.util;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;
import java.util.Date;


/**
 * This class provides helper functions for test data to ensure objects are compared in a consistent manner.
 *
 * @author StevenF
 */
public class TestDataUtils {

	/**
	 * The toStringFormatted function is used when exporting data from a grid in the UI using the new 'Export Test Data'
	 * option.  That data can be pasted into tests as 'expectedData' and also contains an array of the fieldPaths for the
	 * data in the given identity object.  This allows a test to retrieve an object and pass it to this function with the
	 * same fieldPaths used in the export from the UI and will ensure the formatted string returned will match the format
	 * exported from the system (assuming the test returned an object with the same/expected values).
	 */
	public static String toStringFormatted(IdentityObject object, String[] fieldPaths) {
		StringBuilder stringFormatted = new StringBuilder();
		for (String fieldPath : fieldPaths) {
			if (stringFormatted.length() > 0) {
				stringFormatted.append(", ");
			}
			Object fieldValue = BeanUtils.getPropertyValue(object, fieldPath);
			if (fieldValue == null) {
				stringFormatted.append("");
			}
			else if (fieldValue instanceof BigDecimal) {
				stringFormatted.append(CoreMathUtils.formatNumberDecimalMax((BigDecimal) fieldValue));
			}
			else if (fieldValue instanceof Date) {
				stringFormatted.append(DateUtils.fromDate((Date) fieldValue, DateUtils.DATE_FORMAT_FULL));
			}
			else {
				stringFormatted.append(fieldValue);
			}
		}
		return stringFormatted.toString();
	}
}
