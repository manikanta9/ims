package com.clifton.core.util;

import com.clifton.core.dataaccess.file.ExcelFileUtils;
import com.clifton.core.dataaccess.file.FileUtils;


/**
 * @author jgommels
 */
public class SecurityUtils {

	public static void secureFile(String sourceFileNameWithPath, String destinationFileNameWithPath, String password) {
		String ext = FileUtils.getFileExtension(sourceFileNameWithPath);

		if (ext == null) {
			throw new IllegalArgumentException("The file name [" + FileUtils.getFileName(sourceFileNameWithPath) + "] does not have a file extension specified.");
		}

		if (StringUtils.isEqualIgnoreCase("pdf", ext)) {
			PDFSecurityUtils.securePdf(sourceFileNameWithPath, destinationFileNameWithPath, password);
		}
		else if (StringUtils.isEqualIgnoreCase("xls", ext) || StringUtils.isEqualIgnoreCase("xlsx", ext)) {
			ExcelFileUtils.encryptExcelFile(sourceFileNameWithPath, destinationFileNameWithPath, password);
		}
		else {
			throw new IllegalArgumentException("The file type [" + ext + "] is not a supported type for encryption.");
		}
	}
}
