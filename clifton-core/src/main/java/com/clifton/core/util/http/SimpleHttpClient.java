package com.clifton.core.util.http;


import org.apache.http.HttpEntity;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;

import java.net.URI;


/**
 * A simple wrapper for {@link org.apache.http.client.HttpClient} that executes HTTP requests.
 */
public interface SimpleHttpClient {


	public SimpleHttpResponse execute(HttpUriRequest request);


	public SimpleHttpResponse execute(HttpUriRequest request, HttpClientContext context);


	public SimpleHttpResponse execute(HttpUriRequest request, HttpClientContext context, boolean checkStatus);


	public SimpleHttpResponse get(URI uri);


	public SimpleHttpResponse get(URI uri, HttpClientContext context);


	public SimpleHttpResponse patch(URI uri, HttpEntity httpEntity);


	public SimpleHttpResponse patch(URI uri, HttpEntity httpEntity, HttpClientContext context);


	public SimpleHttpResponse post(URI uri, HttpEntity httpEntity);


	public SimpleHttpResponse post(URI uri, HttpEntity httpEntity, HttpClientContext context);


	public SimpleHttpResponse put(URI uri, HttpEntity httpEntity);


	public SimpleHttpResponse put(URI uri, HttpEntity httpEntity, HttpClientContext context);


	public SimpleHttpResponse delete(URI uri);


	public SimpleHttpResponse delete(URI uri, HttpClientContext context);


	public void close();
}
