package com.clifton.core.util.event;


/**
 * The <code>BaseEventListener</code> class defines common behavior for EventListeners.
 *
 * @author rbrooks
 */
public abstract class BaseEventListener<E extends Event<?, ?>> implements EventListener<E> {

	/**
	 * The relative order of this EventListeners in the sequence of listeners registered for the same event.
	 * Defaults to 100.
	 */
	private int order = 100;


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
