package com.clifton.core.util.http.authenticator;

import org.apache.http.client.protocol.HttpClientContext;


public interface HttpAuthenticator {

	public HttpClientContext login(String username, String password);
}
