package com.clifton.core.util.concurrent.synchronize;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.concurrent.CallableAction;
import com.clifton.core.util.concurrent.ExecutableAction;

import java.util.Set;


/**
 * <code>SynchronizableBuilder</code> is a builder that assists in constructing {@link Synchronizable}
 * objects for a variety of use cases.
 *
 * @author NickK
 */
public class SynchronizableBuilder {

	private final String secureAreaName;
	private final Set<String> lockKeySet;
	private final String lockMessage;
	private int millisecondsToWaitIfBusy = 0;

	///////////////////////////////////////////////////////////////////////////
	//////////////           Constructor Methods            ///////////////////
	///////////////////////////////////////////////////////////////////////////


	private SynchronizableBuilder(String secureAreaName, String lockMessage, Set<String> lockKeySet) {
		this.secureAreaName = secureAreaName;
		this.lockMessage = lockMessage;
		this.lockKeySet = lockKeySet;
	}


	/**
	 * Returns a new builder for the provided required {@link Synchronizable} fields, using a single
	 * lock key.
	 */
	public static SynchronizableBuilder forLocking(String secureAreaName, String lockMessage, String lockKey) {
		return forLocking(secureAreaName, lockMessage, CollectionUtils.createHashSet(lockKey));
	}


	/**
	 * Returns a new builder for the provided required {@link Synchronizable} fields, using multiple
	 * lock keys.
	 */
	public static SynchronizableBuilder forLocking(String secureAreaName, String lockMessage, Set<String> lockKeySet) {
		return new SynchronizableBuilder(secureAreaName, lockMessage, lockKeySet);
	}


	///////////////////////////////////////////////////////////////////////////
	//////////////            Optional Methods              ///////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the millisecond wait duration for the {@link Synchronizable} being built and returns this builder.
	 */
	public SynchronizableBuilder withMillisecondsToWaitIfBusy(int millisecondsToWaitIfBusy) {
		this.millisecondsToWaitIfBusy = millisecondsToWaitIfBusy;
		return this;
	}

	///////////////////////////////////////////////////////////////////////////
	//////////////              Build Methods               ///////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a new {@link SynchronizableExecutableAction} with the builder's defined {@link Synchronizable} properties and the provided {@link ExecutableAction}.
	 */
	public SynchronizableExecutableAction buildWithExecutableAction(ExecutableAction executableAction) {
		return new SynchronizableExecutableActionImpl(this, executableAction);
	}


	/**
	 * Returns a new {@link SynchronizableCallableAction} with the builder's defined {@link Synchronizable} properties and the provided {@link CallableAction}.
	 */
	public <V> SynchronizableCallableAction<V> buildWithCallableAction(CallableAction<V> callableAction) {
		return new SynchronizableCallableActionImpl<>(this, callableAction);
	}

	///////////////////////////////////////////////////////////////////////////
	/////////////           Concrete Build Classes           //////////////////
	///////////////////////////////////////////////////////////////////////////

	public static final class SynchronizableExecutableActionImpl extends AbstractSynchronizable implements SynchronizableExecutableAction {

		private final ExecutableAction executableAction;


		private SynchronizableExecutableActionImpl(SynchronizableBuilder builder, ExecutableAction executableAction) {
			super(builder.secureAreaName, builder.lockMessage, builder.lockKeySet);
			setMillisecondsToWaitIfBusy(builder.millisecondsToWaitIfBusy);
			this.executableAction = executableAction;
		}


		@Override
		public ExecutableAction getExecutableAction() {
			return this.executableAction;
		}
	}

	public static final class SynchronizableCallableActionImpl<V> extends AbstractSynchronizable implements SynchronizableCallableAction<V> {

		private final CallableAction<V> callableAction;


		private SynchronizableCallableActionImpl(SynchronizableBuilder builder, CallableAction<V> callableAction) {
			super(builder.secureAreaName, builder.lockMessage, builder.lockKeySet);
			setMillisecondsToWaitIfBusy(builder.millisecondsToWaitIfBusy);
			this.callableAction = callableAction;
		}


		@Override
		public CallableAction<V> getCallableAction() {
			return this.callableAction;
		}
	}
}
