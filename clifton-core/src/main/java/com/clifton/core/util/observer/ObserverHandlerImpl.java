package com.clifton.core.util.observer;


import com.clifton.core.comparison.Ordered;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;


/**
 * The <code>ObserverHandlerImpl</code> class implements {@link ObserverHandler} interface.
 *
 * @author vgomelsky
 */
@Component
public class ObserverHandlerImpl implements ObserverHandler {

	/**
	 * Holds a Map of observer events to List's of registered observer mappings
	 */
	private final Map<Class<? extends Observer>, List<Observer>> observerMap = new ConcurrentHashMap<>();

	/**
	 * Holds a Map of observer events to corresponding notifier proxies.
	 */
	private final Map<Class<? extends Observer>, Observer> notifierMap = new ConcurrentHashMap<>();

	/**
	 * Used to synchronize on atomic operations involving observerMap and notifierMap.
	 */
	private final Object lock = new Object();


	@Override
	public String toString() {
		return this.observerMap.toString();
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends Observer> T getNotifier(Class<T> eventType, Predicate<Observer> observerFilter) {
		T notifier = (T) this.notifierMap.get(eventType);
		if (notifier == null) {
			// not in the cache: create and add
			synchronized (this.lock) {
				List<Observer> registeredObservers = this.observerMap.computeIfAbsent(eventType, k -> new ArrayList<>());
				Class<T> notifierClass = (Class<T>) Proxy.getProxyClass(eventType.getClassLoader(), eventType, ObserverNotifier.class);
				try {
					notifier = notifierClass.getConstructor(InvocationHandler.class).newInstance(new ObserverNotifierImpl<>(registeredObservers, observerFilter));
				}
				catch (Exception e) {
					throw new RuntimeException("Error creating notifier for: " + eventType, e);
				}
				// cache
				this.notifierMap.put(eventType, notifier);
			}
		}

		return notifier;
	}


	@Override
	public <T extends Observer> boolean register(Class<T> eventType, T observer) {
		synchronized (this.lock) {
			List<Observer> registeredObservers = this.observerMap.computeIfAbsent(eventType, k -> new ArrayList<>());
			if (!registeredObservers.contains(observer)) {
				// soft observers if implement {@link Ordered}
				int insertIndex = registeredObservers.size();
				if (observer instanceof Ordered) {
					Ordered orderedObserver = (Ordered) observer;
					for (int i = 0; i < registeredObservers.size(); i++) {
						Observer existing = registeredObservers.get(i);
						if (existing instanceof Ordered) {
							Ordered orderedExisting = (Ordered) existing;
							if (orderedObserver.getOrder() < orderedExisting.getOrder()) {
								insertIndex = i;
								break;
							}
							else if (orderedObserver.getOrder() == orderedExisting.getOrder()) {
								AssertUtils.assertFalse(orderedObserver.isUniqueOrderingRequired() || orderedExisting.isUniqueOrderingRequired(),
										"Unable to use the same order value (%d) for an observer defined to require a unique order. Observers in conflict include: %s and %s",
										orderedObserver.getOrder(), observer.getClass(), existing.getClass());
								// if the order is the same, the new one is appended in the end
							}
						}
						else {
							// un-ordered can be mixed with ordered by go in the end
							insertIndex = i;
							break;
						}
					}
				}
				registeredObservers.add(insertIndex, observer);
				this.notifierMap.remove(eventType); // clear notifier cache
				return true;
			}
		}
		return false;
	}


	@Override
	public List<Observer> getRegisteredObservers() {
		return CollectionUtils.combineCollectionOfCollections(this.observerMap.values());
	}


	@Override
	public <T extends Observer> boolean isRegistered(Class<T> eventType, T observer) {
		List<Observer> registeredObservers = this.observerMap.computeIfAbsent(eventType, k -> new ArrayList<>());
		return registeredObservers.contains(observer);
	}


	@Override
	public <T extends Observer> T unregister(Class<T> eventType, T observer) {
		T result = null;
		synchronized (this.lock) {
			List<Observer> registeredObservers = this.observerMap.get(eventType);
			if (registeredObservers != null) {
				if (registeredObservers.remove(observer)) {
					result = observer;
					// clean up if last
					if (registeredObservers.isEmpty()) {
						this.observerMap.remove(eventType);
					}
				}
			}

			// clear notifier cache
			this.notifierMap.remove(eventType);
		}

		return result;
	}
}
