package com.clifton.core.util.runner.config;


import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.status.Status;

import java.util.Date;


public class RunnerConfig {

	/**
	 * Gets the date and time when the object will be executed.
	 *
	 * @return
	 */
	private Date runDate;
	/**
	 * Gets the unique type name for a runner.  A runner may have a multiple scheduled instances,
	 * each will have the same type name so that all instances of a specific runner can be found.
	 * For example, for batch jobs the type is 'BATCH' allowing us to find all batch job runners.
	 *
	 * @return
	 */
	private String type;
	/**
	 * Gets the unique typeId for a runner type.  This is used to get all instances of a specific runner.
	 * For example, if we have 2 batch jobs BatchJob1 and BatchJob2 the typeId's would be 1 and 2,
	 * then we can find all schedule runners for BatchJob1 by looking for all jobs with type "BATCH"
	 * and typeId "1" and all runners for BatchJob2 by looking for "BATCH" and typeId "2".
	 *
	 * @return
	 */
	private String typeId;
	/**
	 * Current status of this run. For long running tasks the status string maybe
	 * periodically updated by the runner to indicate current progress: "Processed 5 out of 20", etc.
	 */
	private Status status;
	/**
	 * "Run As" user for the run.
	 */
	private Object runUser;
	/**
	 * The class name of the runner instance.
	 */
	private Class<?> className;
	/**
	 * The state of the runner instance.
	 *
	 * @see com.clifton.core.util.runner.Runner.RunnerStates
	 */
	private Runner.RunnerStates runnerState;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public RunnerConfig() {
		// Empty Constructor needed for binding
	}


	public RunnerConfig(Runner runner) {
		this.runDate = runner.getRunDate();
		this.type = runner.getType();
		this.typeId = runner.getTypeId();
		this.status = runner.getStatus();
		this.runUser = runner.getRunUser();
		this.className = runner.getClass();
		this.runnerState = runner.getRunnerState();
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public Date getRunDate() {
		return this.runDate;
	}


	public void setRunDate(Date runDate) {
		this.runDate = runDate;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTypeId() {
		return this.typeId;
	}


	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}


	public Class<?> getClassName() {
		return this.className;
	}


	public void setClassName(Class<?> className) {
		this.className = className;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}


	public Object getRunUser() {
		return this.runUser;
	}


	public void setRunUser(Object runUser) {
		this.runUser = runUser;
	}


	public Runner.RunnerStates getRunnerState() {
		return this.runnerState;
	}


	public void setRunnerState(Runner.RunnerStates runnerState) {
		this.runnerState = runnerState;
	}
}
