package com.clifton.core.util.http.cookie;

import org.apache.http.cookie.Cookie;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Date;
import java.util.List;


/**
 * The <code>PersistentCookieStore</code> is an implementation of {@link ImsCookieStore} that persists cookies in a file.
 *
 * @author jgommels
 */
public class PersistentCookieStore implements ImsCookieStore {

	private static final String TEMP_DIR = System.getProperty("java.io.tmpdir");

	private final ImsCookieStore ramCookieStore;
	private final CookieFileReaderWriter fileReaderWriter;


	public PersistentCookieStore(String name) {
		this.ramCookieStore = new InMemoryCookieStore();
		this.fileReaderWriter = new CookieFileReaderWriter(generateCookiePath(name));
	}


	private Path generateCookiePath(String name) {
		return Paths.get(TEMP_DIR, "ims_cookies", name + ".tmp");
	}


	@Override
	public void addCookie(Cookie cookie) {
		this.ramCookieStore.addCookie(cookie);
		this.fileReaderWriter.save(this.ramCookieStore.getCookies());
	}


	@Override
	public void setCookieList(List<Cookie> cookieList) {
		this.ramCookieStore.setCookieList(cookieList);
		this.fileReaderWriter.save(cookieList);
	}


	@Override
	public List<Cookie> getCookies() {
		List<Cookie> cookieList = this.ramCookieStore.getCookies();
		if (cookieList.isEmpty()) {
			cookieList = this.fileReaderWriter.load();
			this.ramCookieStore.setCookieList(cookieList);
		}
		return cookieList;
	}


	@Override
	public boolean containsCookie(String name, String host) {
		getCookies(); // ensure loaded
		return this.ramCookieStore.containsCookie(name, host);
	}


	@Override
	public Cookie getCookie(String cookieName, String host) {
		return this.ramCookieStore.getCookie(cookieName, host);
	}


	@Override
	public boolean clearExpired(Date date) {
		boolean expired = this.ramCookieStore.clearExpired(date);

		//If a cookie expired, update the cookie store on disk
		if (expired) {
			this.fileReaderWriter.save(this.ramCookieStore.getCookies());
		}

		return expired;
	}


	@Override
	public void clear() {
		this.ramCookieStore.clear();
		this.fileReaderWriter.save(this.ramCookieStore.getCookies());
	}
}
