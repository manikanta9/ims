package com.clifton.core.util.runner.execution;

import com.clifton.core.util.runner.Runner;

import java.util.concurrent.TimeUnit;


/**
 * <code>BaseRunnerCompletionResult</code> is the base, default, RunnerCompletionResult implementation.
 *
 * @author NickK
 */
public class BaseRunnerCompletionResult implements RunnerCompletionResult {

	private final Runner runner;
	private final long queueDuration;
	private long runDuration;
	private TimeUnit runDurationTimeUnit;
	private boolean success;
	private Throwable error;


	/**
	 * Creates a new <code>RunnerCompletionResult</code> populated with {@link Runner}
	 * and queued duration calculated from the current system time and the Runner's defined
	 * run time.
	 */
	public BaseRunnerCompletionResult(Runner runner) {
		this.runner = runner;
		this.queueDuration = System.currentTimeMillis() - runner.getRunDate().getTime();
	}


	@Override
	public Runner getRunner() {
		return this.runner;
	}


	@Override
	public long getQueuedDuration(TimeUnit timeUnit) {
		return timeUnit.convert(this.queueDuration, TimeUnit.MILLISECONDS);
	}


	@Override
	public long getRunDuration(TimeUnit timeUnit) {
		return (this.runDurationTimeUnit == null) ? -1 : timeUnit.convert(this.runDuration, this.runDurationTimeUnit);
	}


	@Override
	public void complete() {
		this.success = true;
	}


	@Override
	public void completeWithError(Throwable throwable) {
		this.success = false;
		this.error = throwable;
	}


	@Override
	public boolean isSuccessful() {
		return this.success;
	}


	@Override
	public Throwable getError() {
		return this.error;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void setRunDuration(long duration, TimeUnit timeUnit) {
		this.runDuration = duration;
		this.runDurationTimeUnit = timeUnit;
	}
}
