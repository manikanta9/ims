package com.clifton.core.util.timer;


import org.springframework.stereotype.Component;

import java.util.Set;


/**
 * The <code>ThreadLocalTimerHandler</code> class implements Timer functionality by storing the Timer as ThreadLocal object.
 *
 * @author vgomelsky
 */
@Component("timerHandler")
public class ThreadLocalTimerHandler implements TimerHandler {

	private final ThreadLocal<Timer> timerHolder = ThreadLocal.withInitial(Timer::new);


	@Override
	public void startTimer() {
		getTimer().start();
	}


	@Override
	public void stopTimer() {
		getTimer().stop();
	}


	@Override
	public void startTimerTask(String taskName) {
		getTimer().startTask(taskName);
	}


	@Override
	public void stopTimerTask(String taskName) {
		getTimer().stopTask(taskName);
	}


	@Override
	public void reset() {
		getTimer().reset();
	}


	@Override
	public long getDurationNano() {
		return getTimer().getDurationNano();
	}


	@Override
	public TimerTask getTimerTask(String taskName) {
		return getTimer().getTimerTask(taskName);
	}


	@Override
	public Set<String> getTaskNames() {
		return getTimer().getTaskNames();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Timer getTimer() {
		return this.timerHolder.get();
	}
}
