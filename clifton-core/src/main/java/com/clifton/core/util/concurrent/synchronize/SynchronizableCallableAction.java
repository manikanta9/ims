package com.clifton.core.util.concurrent.synchronize;

import com.clifton.core.util.concurrent.CallableAction;


/**
 * <code>SynchronizableCallableAction</code> defines an action that is to be called within the scope of an obtained lock.
 * This object defines the required locking attributes ({@link #getSecureAreaName()}, {@link #getLockKeySet()},
 * and {@link #getLockMessage()}) and the action to be called to get a response ({@link #getCallableAction()}).
 *
 * @author NickK
 * @see SynchronizableBuilder
 */
public interface SynchronizableCallableAction<V> extends Synchronizable {

	/**
	 * Returns the action to be executed for producing a result after the lock is successfully obtained: usually a method call () -> return myMethod(arg1, arg2, ...).
	 * <p>
	 * This property is required in order to obtain a lock and execute an action.
	 */
	public CallableAction<V> getCallableAction();
}
