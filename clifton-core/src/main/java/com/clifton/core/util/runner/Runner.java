package com.clifton.core.util.runner;


import com.clifton.core.util.status.Status;

import java.util.Date;


/**
 * The <code>Runner</code> interface defines methods that need to be implemented by various
 * tasks that require scheduling or asynchronous execution.
 * <p/>
 * Different system modules (batch jobs, notifications, etc.) can take advantage of this functionality.
 * Each module needs to implement and register its own RunnerProvider that will return module
 * specific runner instances.
 * <p/>
 * System defined RunnerHandler will use registered RunnerProvider objects to schedule corresponding Runner instances.
 *
 * @author mwacker
 */
public interface Runner extends Runnable {

	/**
	 * Returns the date and time when the object should be executed.
	 */
	public Date getRunDate();


	/**
	 * Returns the unique type name for a runner.  A runner may have multiple scheduled instances,
	 * each will have the same type name so that all instances of a specific runner can be found.
	 * <p/>
	 * For example, for batch jobs the type is 'BATCH' allowing us to find all batch job runners.
	 */
	public String getType();


	/**
	 * Returns the unique typeId for a runner type.  This is used to get all instances of a specific runner.
	 * <p/>
	 * For example, if we have 2 batch jobs BatchJob1 and BatchJob2 the typeId's would be 1 and 2,
	 * then we can find all schedule runners for BatchJob1 by looking for all jobs with type "BATCH"
	 * and typeId "1" and all runners for BatchJob2 by looking for "BATCH" and typeId "2".
	 */
	public String getTypeId();


	/**
	 * Returns current status of this runner. For long running tasks the status string maybe
	 * periodically updated by the runner to indicate current progress: "Processed 5 out of 20", etc.
	 */
	public Status getStatus();


	/**
	 * Used to determine if concurrent runs for the same type are supported.  Most cases this is true - i.e. same with with different type ids will run
	 * at the same time.  Use false to use a single thread executor for the type, and each new runner will start after the previous one finished.
	 */
	public boolean isConcurrentSupported();


	/**
	 * Returns the user for the "Run As" user for this run.
	 */
	public Object getRunUser();


	/**
	 * Sets the run as user for this run. The user must be set before the "run" method is invoked.
	 */
	public void setRunUser(Object runUser);


	/**
	 * Returns the current state for this run.
	 *
	 * @see RunnerStates
	 */
	public RunnerStates getRunnerState();


	/**
	 * Updates this run's active state.
	 *
	 * @see RunnerStates
	 */
	public void setRunnerState(RunnerStates runnerStates);


	/**
	 * Possible states a Runner can be in within the system.
	 */
	public enum RunnerStates {
		SCHEDULED, // queued or scheduled for execution
		RUNNING, // actively executing
		COMPLETED, // has finished execution (may be with error)
		UNSCHEDULED // unknown - not yet submitted to be executed
	}
}
