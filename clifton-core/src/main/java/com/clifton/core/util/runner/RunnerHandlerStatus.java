package com.clifton.core.util.runner;

import com.clifton.core.util.ThreadUtils;

import java.lang.management.ThreadInfo;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;


/**
 * <code>RunnerHandlerStatus</code> contains statistics and data about an active
 * {@link RunnerHandler} to help monitor and troubleshoot Runner processing.
 *
 * @author NickK
 */
public class RunnerHandlerStatus {

	private final boolean started;
	private final int scheduledRunnerCount;
	private final long canceledRunnerCount;
	private final long completedRunnerCount;
	private final long unsuccessfulRunnerCount;
	private final long rescheduledRunnerCount;
	private final long longestQueueMilliseconds;
	private final double averageQueueMilliseconds;
	private final double averageRunMilliseconds;
	private final int executorCount;
	private final List<ExecutorServiceStatus> executorServiceStatusList;
	private final String threadDump;
	private final List<RunnerExecutionStatus> runnerExecutionStatusList;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private RunnerHandlerStatus(RunnerHandlerStatusBuilder builder) {
		this.started = builder.started;
		this.scheduledRunnerCount = builder.scheduledRunnerCount;
		this.completedRunnerCount = builder.completedRunnerCount;
		this.canceledRunnerCount = builder.canceledRunnerCount;
		this.unsuccessfulRunnerCount = builder.unsuccessfulRunnerCount;
		this.rescheduledRunnerCount = builder.rescheduledRunnerCount;
		this.longestQueueMilliseconds = builder.longestQueueMilliseconds;
		this.averageQueueMilliseconds = builder.averageQueueMilliseconds;
		this.averageRunMilliseconds = builder.averageRunMilliseconds;
		this.executorCount = builder.executorCount;
		this.executorServiceStatusList = new ArrayList<>(builder.executorServiceStatusList);
		this.threadDump = builder.threadDumpBuilder.toString();
		this.runnerExecutionStatusList = new ArrayList<>(builder.runnerExecutionStatusList);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the {@link RunnerHandler} is started.
	 */
	public boolean isStarted() {
		return this.started;
	}


	/**
	 * Returns the number of {@link Runner}s processed by a {@link RunnerHandler} that are scheduled for execution.
	 */
	public int getScheduledRunnerCount() {
		return this.scheduledRunnerCount;
	}


	/**
	 * Returns the number of {@link Runner}s processed by a {@link RunnerHandler} that have completed.
	 */
	public long getCompletedRunnerCount() {
		return this.completedRunnerCount;
	}


	/**
	 * Returns the number of {@link Runner}s processed by a {@link RunnerHandler} that were rescheduled.
	 */
	public long getRescheduledRunnerCount() {
		return this.rescheduledRunnerCount;
	}


	/**
	 * Returns the number of {@link Runner}s processed by a {@link RunnerHandler} that were canceled.
	 */
	public long getCanceledRunnerCount() {
		return this.canceledRunnerCount;
	}


	/**
	 * Returns the number of {@link Runner}s processed by a {@link RunnerHandler} that completed with an error.
	 */
	public long getUnsuccessfulRunnerCount() {
		return this.unsuccessfulRunnerCount;
	}


	/**
	 * Returns the longest millisecond duration a {@link Runner} processed by a {@link RunnerHandler}
	 * sat in the queue before starting execution at its scheduled execution time.
	 */
	public long getLongestQueueMilliseconds() {
		return this.longestQueueMilliseconds;
	}


	/**
	 * Returns the average millisecond duration the {@link Runner}s processed by a {@link RunnerHandler}
	 * sat in the queue before starting execution at their scheduled execution time.
	 */
	public double getAverageQueueMilliseconds() {
		return this.averageQueueMilliseconds;
	}


	/**
	 * Returns the average millisecond duration the {@link Runner}s processed by a {@link RunnerHandler}
	 * were in execution.
	 */
	public double getAverageRunMilliseconds() {
		return this.averageRunMilliseconds;
	}


	/**
	 * Returns the number of Executors managed by a {@link RunnerHandler};
	 */
	public int getExecutorCount() {
		return this.executorCount;
	}


	/**
	 * Returns the list of thread pool or executor service statistics used by a {@link RunnerHandler}.
	 */
	public List<ExecutorServiceStatus> getExecutorServiceStatusList() {
		return this.executorServiceStatusList;
	}


	public String getThreadDump() {
		return this.threadDump;
	}


	/**
	 * Returns the list of Runner execution statistics that are monitored by a {@link RunnerHandler}.
	 */
	public List<RunnerExecutionStatus> getRunnerExecutionStatusList() {
		return this.runnerExecutionStatusList;
	}


	/**
	 * <code>RunnerHandlerStatusBuilder</code> is a class that assists in creating {@link RunnerHandlerStatus}
	 */
	public static class RunnerHandlerStatusBuilder {

		private final List<ExecutorServiceStatus> executorServiceStatusList = new ArrayList<>();
		private final List<RunnerExecutionStatus> runnerExecutionStatusList = new ArrayList<>();
		private boolean started;
		private int scheduledRunnerCount;
		private long canceledRunnerCount;
		private long completedRunnerCount;
		private long unsuccessfulRunnerCount;
		private long rescheduledRunnerCount;
		private long longestQueueMilliseconds;
		private double averageQueueMilliseconds;
		private double averageRunMilliseconds;
		private int executorCount;

		private final boolean includeDumpOfAllThreads;
		private ThreadInfo[] threadInfos;
		private final StringBuilder threadDumpBuilder = new StringBuilder();

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		RunnerHandlerStatusBuilder() {
			this(null);
		}


		RunnerHandlerStatusBuilder(RunnerHandlerStatusCommand command) {
			this.includeDumpOfAllThreads = command != null && command.isIncludeStatusOfAllThreads();
		}

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		/**
		 * Appends the provided {@link ExecutorServiceStatus} to the managed collection of this builder if not null. Returns this builder.
		 */
		public RunnerHandlerStatusBuilder addExecutorServiceStatus(ExecutorServiceStatus executorServiceStatus) {
			if (executorServiceStatus != null) {
				this.executorServiceStatusList.add(executorServiceStatus);
				appendThreadInfos(executorServiceStatus);
			}
			return this;
		}


		/**
		 * Looks at an in memory cache of ThreadInfo objects to filter and appends a string status for each
		 * ThreadInfo object that belongs to ExecutorService of the provided ExecutorServiceStatus to this builder.
		 */
		private void appendThreadInfos(ExecutorServiceStatus executorServiceStatus) {
			if (this.threadInfos == null) {
				this.threadInfos = ThreadUtils.getAllThreadInfo(100);
				if (this.includeDumpOfAllThreads) {
					for (ThreadInfo threadInfo : this.threadInfos) {
						this.threadDumpBuilder.append(ThreadUtils.getThreadDump(threadInfo));
						this.threadDumpBuilder.append("\n\n");
					}
				}
			}
			if (!this.includeDumpOfAllThreads && this.threadInfos != null) {
				String executorThreadPrefix = executorServiceStatus.getThreadPrefix() == null ? executorServiceStatus.getName() : executorServiceStatus.getThreadPrefix();
				if (executorThreadPrefix != null) {
					for (ThreadInfo threadInfo : this.threadInfos) {
						if (threadInfo != null && threadInfo.getThreadName() != null && threadInfo.getThreadName().contains(executorThreadPrefix)) {
							this.threadDumpBuilder.append(ThreadUtils.getThreadDump(threadInfo));
							this.threadDumpBuilder.append("\n\n");
						}
					}
				}
			}
		}


		public RunnerHandlerStatusBuilder addRunnerExecutionStatus(RunnerExecutionStatus runnerExecutionStatus) {
			if (runnerExecutionStatus != null) {
				this.runnerExecutionStatusList.add(runnerExecutionStatus);
			}
			return this;
		}


		public RunnerHandlerStatusBuilder setStarted(boolean started) {
			this.started = started;
			return this;
		}


		public RunnerHandlerStatusBuilder setScheduledRunnerCount(int scheduledRunnerCount) {
			this.scheduledRunnerCount = scheduledRunnerCount;
			return this;
		}


		public RunnerHandlerStatusBuilder setCompletedRunnerCount(long completedRunnerCount) {
			this.completedRunnerCount = completedRunnerCount;
			return this;
		}


		public RunnerHandlerStatusBuilder setRescheduledRunnerCount(long rescheduledRunnerCount) {
			this.rescheduledRunnerCount = rescheduledRunnerCount;
			return this;
		}


		public RunnerHandlerStatusBuilder setUnsuccessfulRunnerCount(long unsuccessfulRunnerCount) {
			this.unsuccessfulRunnerCount = unsuccessfulRunnerCount;
			return this;
		}


		public RunnerHandlerStatusBuilder setCanceledRunnerCount(long canceledRunnerCount) {
			this.canceledRunnerCount = canceledRunnerCount;
			return this;
		}


		public RunnerHandlerStatusBuilder setLongestQueueMilliseconds(long longestQueueMilliseconds) {
			this.longestQueueMilliseconds = longestQueueMilliseconds;
			return this;
		}


		public RunnerHandlerStatusBuilder setAverageQueueMilliseconds(double averageQueueDuration) {
			this.averageQueueMilliseconds = averageQueueDuration;
			return this;
		}


		public RunnerHandlerStatusBuilder setAverageRunMilliseconds(double averageRunDuration) {
			this.averageRunMilliseconds = averageRunDuration;
			return this;
		}


		public RunnerHandlerStatusBuilder setExecutorCount(int executorCount) {
			this.executorCount = executorCount;
			return this;
		}


		public RunnerHandlerStatus build() {
			return new RunnerHandlerStatus(this);
		}
	}

	/**
	 * <code>RunnerExecutionStatus</code> is used to track monitored Runner executions over the life
	 * of a RunnerHandler. The creator of this object is responsible for adding execution durations
	 * to this object.
	 */
	public static class RunnerExecutionStatus {

		private final String name;
		private final LongAdder executionCount = new LongAdder();
		private final AtomicLong longestRunDuration = new AtomicLong();
		private final AtomicLong lastRunDuration = new AtomicLong();
		private final LongAdder totalRunDuration = new LongAdder();
		private Date lastRunDate;
		private Date nextRunDate;

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		public RunnerExecutionStatus(String name) {
			this.name = name;
		}

		///////////////////////////////////////////////////////////////////////////
		///////////////////////////////////////////////////////////////////////////


		/**
		 * Resets all internal counter references of this object.
		 */
		public void reset() {
			this.lastRunDate = null;
			this.executionCount.reset();
			this.longestRunDuration.set(0L);
			this.lastRunDuration.set(0L);
			this.totalRunDuration.reset();
		}


		/**
		 * Adds the provided duration to this object and updates any necessary counter references
		 * including: last run date, execution count, last run duration, and long run duration.
		 */
		public void addExecution(long duration, TimeUnit timeUnit) {
			this.lastRunDate = new Date();
			this.executionCount.increment();
			long millisecondDuration = TimeUnit.MILLISECONDS.convert(duration, timeUnit);
			this.totalRunDuration.add(millisecondDuration);
			this.lastRunDuration.set(millisecondDuration);
			updateLongDuration(millisecondDuration);
		}


		private void updateLongDuration(long duration) {
			long currentValue = this.longestRunDuration.longValue();
			if (duration > currentValue) {
				if (!this.longestRunDuration.compareAndSet(currentValue, duration)) {
					updateLongDuration(duration);
				}
			}
		}


		public String getName() {
			return this.name;
		}


		/**
		 * Returns tha date of the last time an execution duration was added.
		 *
		 * @see RunnerExecutionStatus#addExecution(long, TimeUnit)
		 */
		public Date getLastRunDate() {
			return this.lastRunDate;
		}


		/**
		 * Returns the last specified next run date.
		 *
		 * @see RunnerExecutionStatus#setNextRunDate(Date)
		 */
		public Date getNextRunDate() {
			return this.nextRunDate;
		}


		/**
		 * Updates the next run date to the provided date.
		 */
		public void setNextRunDate(Date nextRunDate) {
			this.nextRunDate = nextRunDate;
		}


		/**
		 * Returns the number of executions added to this object.
		 */
		public long getExecutionCount() {
			return this.executionCount.longValue();
		}


		/**
		 * Returns the millisecond duration of the last added execution duration.
		 */
		public long getLastRunMillisecondDuration() {
			return this.lastRunDuration.longValue();
		}


		/**
		 * Returns the longest millisecond duration added to this object.
		 */
		public long getLongestRunMillisecondDuration() {
			return this.longestRunDuration.longValue();
		}


		/**
		 * Returns the average millisecond duration of all added execution durations added to this object.
		 */
		public long getAverageRunMillisecondDuration() {
			long executionCountValue = getExecutionCount();
			return this.totalRunDuration.longValue() / ((executionCountValue == 0) ? 1 : executionCountValue);
		}
	}
}
