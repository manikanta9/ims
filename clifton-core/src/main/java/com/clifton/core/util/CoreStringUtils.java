package com.clifton.core.util;


import org.apache.commons.validator.routines.EmailValidator;


/**
 * The <code>CoreStringUtils</code> provides utilities for working with String objects.
 *
 * @author vgomelsky
 */
public class CoreStringUtils {

	private static final EmailValidator EMAIL_VALIDATOR = EmailValidator.getInstance();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Verifies that the specified {@code email} contains and/or follows the
	 * following rules:
	 * <ul>
	 * <li>Only letters, numbers and email acceptable symbols (+, _, -, .)</li>
	 * <li>No two different symbols may follow each other
	 * <li>Cannot begin with a symbol
	 * <li>Ending domain must be at least 2 letters
	 * <li>Supports sub-domains
	 * <li>Domain must be between 2 and 6 letters (Ex: .ca, .museum)
	 * <li>Only (-) and (.) symbols are allowed in domain, but not
	 * consecutively.</li>
	 * </ul>
	 * For example: g_s+gav@com.com | gav@gav.com | jim@jim.c.dc.ca are
	 * valid</br> For example: gs_.gs@com.com | gav@gav.c | jim@--c.ca are NOT
	 * valid</br>
	 *
	 * @param email to validate
	 * @return true if matches above rules otherwise false
	 */
	public static boolean isEmailValid(String email) {

		if (!StringUtils.isEmpty(email)) {
			return EMAIL_VALIDATOR.isValid(email);
		}

		return false;
	}
}
