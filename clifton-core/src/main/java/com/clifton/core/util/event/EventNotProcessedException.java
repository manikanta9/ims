package com.clifton.core.util.event;

/**
 * The <code>EventNotProcessedException</code> exception is a {@link RuntimeException} that can be used
 * to indicate an event was raised, evaluated by an application, but not processed for any number of reasons.
 * e.g.: Reconciliation receives events from Integration for ImportRuns; however, not all ImportRuns should be
 * processed based on the FileDefinition mappings in reconciliation; if no file definition mapping is found
 * for the file associated with the ImportRun, then we throw this exception so that the response for the event
 * handling can be properly processed.
 *
 * @author StevenF
 */
public class EventNotProcessedException extends RuntimeException {

	/**
	 * Constructs a new runtime exception with the specified detail message.
	 * The cause is not initialized, and may subsequently be initialized by a
	 * call to {@link #initCause}.
	 *
	 * @param message the detail message. The detail message is saved for
	 *                later retrieval by the {@link #getMessage()} method.
	 */
	public EventNotProcessedException(String message) {
		super(message);
	}


	/**
	 * Constructs a new runtime exception with the specified detail message and
	 * cause.  <p>Note that the detail message associated with
	 * <code>cause</code> is <i>not</i> automatically incorporated in
	 * this runtime exception's detail message.
	 *
	 * @param message the detail message (which is saved for later retrieval
	 *                by the {@link #getMessage()} method).
	 * @param cause   the cause (which is saved for later retrieval by the
	 *                {@link #getCause()} method).  (A <tt>null</tt> value is
	 *                permitted, and indicates that the cause is nonexistent or
	 *                unknown.)
	 * @since 1.4
	 */
	public EventNotProcessedException(String message, Throwable cause) {
		super(message, cause);
	}
}
