package com.clifton.core.util;

import com.monitorjbl.json.JsonView;
import com.monitorjbl.json.Match;


/**
 * The utility class for JSON operations.
 *
 * @author MikeH
 */
public class JsonUtils {

	private JsonUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a {@link JsonView} for the given object.
	 * <p>
	 * The <tt>JsonView</tt> will be configured with a special serializer for the class of the provided object. This serializer will only include the given field paths during
	 * serialization.
	 *
	 * @param obj    the object to contain within a <tt>JsonView</tt>
	 * @param fields the field paths to include during serialization (paths use dot-path notation; e.g.: "security.instrument.columnValueList.id")
	 * @param <T>    the type of the contained object
	 * @return the <tt>JsonView</tt>-wrapped object for serialization
	 */
	public static <T> JsonView<T> createView(T obj, String... fields) {
		return createView(obj, obj.getClass(), fields);
	}


	/**
	 * Creates a {@link JsonView} for the given object.
	 * <p>
	 * The <tt>JsonView</tt> will be configured with a special serializer for the given class. This serializer will only include the given field paths during serialization.
	 *
	 * @param obj     the object to contain within a <tt>JsonView</tt>
	 * @param onClass the class for which to use special serialization rules
	 * @param fields  the field paths to include during serialization (paths use dot-path notation; e.g.: "security.instrument.columnValueList.id")
	 * @param <T>     the type of the contained object
	 * @return the <tt>JsonView</tt>-wrapped object for serialization
	 */
	public static <T> JsonView<T> createView(T obj, Class<?> onClass, String... fields) {
		return JsonView.with(obj).onClass(onClass, Match.match()
				.exclude("*")
				.include(fields));
	}
}
