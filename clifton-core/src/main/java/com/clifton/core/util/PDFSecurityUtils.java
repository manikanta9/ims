package com.clifton.core.util;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.exceptions.BadPasswordException;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


/**
 * The <code>PdfSecurityUtil</code> provides functionality to password protect PDF documents.
 *
 * @author msiddiqui
 */
public class PDFSecurityUtils {

	/**
	 * Reads the specified {@code inputStream}, and secures it with the
	 * specified {@code password}, and writes the resulting PDF out on the
	 * specified {@code outputStream}.
	 *
	 * @param inputStream  PDF contents to be secured. Must not be already secured, or RunimeException will be thrown.
	 * @param outputStream secured PDF content
	 * @param password     to be used
	 * @throws RuntimeException if problems with the specified streams or the PDF content in the specified {@code inputStream} is already secured
	 */
	public static void securePdf(InputStream inputStream, OutputStream outputStream, String password) {
		try {
			PdfReader reader = new PdfReader(new BufferedInputStream(inputStream));
			PdfStamper stamper = new PdfStamper(reader, new BufferedOutputStream(outputStream));
			stamper.setEncryption(password.getBytes(), password.getBytes(), PdfWriter.ALLOW_PRINTING, PdfWriter.ENCRYPTION_AES_128 | PdfWriter.DO_NOT_ENCRYPT_METADATA);
			stamper.close();
			reader.close();
		}
		catch (DocumentException de) {
			throw new RuntimeException("Unable to secure PDF file!", de);
		}
		catch (BadPasswordException bpe) {
			//This will get thrown if the PDF is already secured
			throw new RuntimeException("PDF seems already secured! Cannot secure already secured PDF.", bpe);
		}
		catch (IOException ioe) {
			throw new RuntimeException("Unable to secure PDF file!", ioe);
		}
	}


	/**
	 * Reads the specified {@code sourceFileNameWithPath}, and secures it with the
	 * specified {@code password}, and writes the resulting PDF out on the
	 * specified {@code destinationFileNameWithPath}.
	 *
	 * @param sourceFileNameWithPath      fully qualified file name and location ending in {@code .pdf}. Must not be already secured, or RunimeException will be
	 *                                    thrown.
	 * @param destinationFileNameWithPath fully qualified destination file name. If the directory part of this does not exists, it is created
	 * @param password                    to be used
	 * @return the specified {@code destinationFileNameWithPath}
	 * @throws RuntimeException if problems with the specified streams or the PDF content in the specified {@code inputStream} is already secured
	 */
	public static String securePdf(String sourceFileNameWithPath, String destinationFileNameWithPath, String password) {

		File destinationFile = new File(destinationFileNameWithPath);

		//Create the output file directory if it it does not already exists
		if (FileUtils.createDirectory(FileUtils.getFilePath(destinationFileNameWithPath))) {
			LogUtils.info(PDFSecurityUtils.class, "Directory created: " + FileUtils.getFilePath(destinationFileNameWithPath));
		}

		try (InputStream is = new FileInputStream(new File(sourceFileNameWithPath)); OutputStream os = new FileOutputStream(destinationFile)) {

			securePdf(is, os, password);

			return destinationFileNameWithPath;
		}
		catch (Throwable e) {
			LogUtils.debug(LogCommand.ofThrowableAndMessage(PDFSecurityUtils.class, e, () -> (destinationFile.delete() ? "Deleted file: " : "Could not delete file: ") + destinationFile.getAbsolutePath()));
			throw new RuntimeException("Unable to secure PDF file!", e);
		}
	}
}
