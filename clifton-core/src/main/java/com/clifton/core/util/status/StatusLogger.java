package com.clifton.core.util.status;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * <code>StatusLogger</code> is a convenience class that preserves all the functionality of {@link Status} and uses {@link LogUtils} to log
 * notes any time a note is added. Allowing you to set a logging class and default logging levels.
 *
 * @author jonathanr
 */
public class StatusLogger extends Status {


	public static final String ERROR = "ERROR";
	public static final String WARN = "WARN";
	public static final String INFO = "INFO";
	public static final String DEBUG = "DEBUG";
	public static final String TRACE = "TRACE";
	private String defaultMsgLogLevel = DEBUG;
	private String defaultSkippedLogLevel = DEBUG;
	private String defaultSuccessLogLevel = DEBUG;
	private Class<?> loggingClass = this.getClass();


	public StatusLogger() {
	}


	public StatusLogger(Class<?> loggingClass) {
		this.loggingClass = loggingClass;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                  Error Methods                         //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addError(String note) {
		addError(note, getLoggingClass());
	}


	public void addError(String note, Exception e) {
		addError(note, getLoggingClass(), e);
	}


	public void addError(String note, Class<?> loggingClass) {
		super.addError(note);
		LogUtils.error(loggingClass, note);
	}


	public void addError(String note, Class<?> loggingClass, Exception e) {
		super.addError(note);
		LogUtils.error(loggingClass, note, e);
	}


	@Override
	public void addErrors(List<String> notes) {
		addErrors(notes, getLoggingClass());
	}


	public void addErrors(List<String> notes, Class<?> loggingClass) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addError(note, loggingClass);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                  Warning Methods                       //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addWarning(String note) {
		addWarning(note, getLoggingClass());
	}


	public void addWarning(String note, Class<?> loggingClass) {
		super.addWarning(note);
		LogUtils.warn(loggingClass, note);
	}


	@Override
	public void addWarnings(List<String> notes) {
		addWarnings(notes, getLoggingClass());
	}


	public void addWarnings(List<String> notes, Class<?> loggingClass) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addWarning(note, loggingClass);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////////                    Message Methods                     //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addMessage(String note) {
		addMessage(note, getDefaultMsgLogLevel(), getLoggingClass());
	}


	public void addMessage(String note, String logLevel) {
		addMessage(note, logLevel, getLoggingClass());
	}


	public void addMessage(String note, Class<?> loggingClass) {
		addMessage(note, getDefaultMsgLogLevel(), loggingClass);
	}


	public void addMessage(String note, String logLevel, Class<?> loggingClass) {
		super.addMessage(note);
		log(logLevel, note, loggingClass);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addMessageWithInfoLogging(String note) {
		addMessage(note, INFO, getLoggingClass());
	}


	public void addMessageWithDebugLogging(String note) {
		addMessage(note, DEBUG, getLoggingClass());
	}


	public void addMessageWithTraceLogging(String note) {
		addMessage(note, TRACE, getLoggingClass());
	}


	public void addMessageWithInfoLogging(String note, Class<?> loggingClass) {
		addMessage(note, INFO, loggingClass);
	}


	public void addMessageWithDebugLogging(String note, Class<?> loggingClass) {
		addMessage(note, DEBUG, loggingClass);
	}


	public void addMessageWithTraceLogging(String note, Class<?> loggingClass) {
		addMessage(note, TRACE, loggingClass);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addMessages(List<String> notes) {
		addMessages(notes, getDefaultMsgLogLevel(), getLoggingClass());
	}


	public void addMessages(List<String> notes, String logLevel) {
		addMessages(notes, logLevel, getLoggingClass());
	}


	public void addMessages(List<String> notes, Class<?> loggingClass) {
		addMessages(notes, getDefaultMsgLogLevel(), loggingClass);
	}


	public void addMessages(List<String> notes, String logLevel, Class<?> loggingClass) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addMessage(note, logLevel, loggingClass);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setMessage(String message) {
		setMessage(message, getDefaultMsgLogLevel(), getLoggingClass());
	}


	public void setMessage(String message, String loggingLevel) {
		setMessage(message, loggingLevel, getLoggingClass());
	}


	public void setMessage(String message, Class<?> loggingClass) {
		setMessage(message, getDefaultMsgLogLevel(), loggingClass);
	}


	public void setMessage(String message, String loggingLevel, Class<?> loggingClass) {
		super.setMessage(message);
		log(loggingLevel, getMessage(), loggingClass);
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setMessage(String message, Object... messageArgs) {
		setMessage(message, getDefaultMsgLogLevel(), getLoggingClass(), messageArgs);
	}


	public void setMessage(String message, String loggingLevel, Object... messageArgs) {
		setMessage(message, loggingLevel, getLoggingClass(), messageArgs);
	}


	public void setMessage(String message, Class<?> loggingClass, Object... messageArgs) {
		setMessage(message, getDefaultMsgLogLevel(), loggingClass, messageArgs);
	}


	public void setMessage(String message, String loggingLevel, Class<?> loggingClass, Object... messageArgs) {
		super.setMessage(message, messageArgs);
		log(loggingLevel, getMessage(), loggingClass);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setMessageWithErrors(String messagePrefix, Integer maxErrors) {
		super.setMessageWithErrors(messagePrefix, maxErrors);
		log(getDefaultMsgLogLevel(), getMessage());
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                Skipped Methods                         //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addSkipped(String note) {
		addSkipped(note, getDefaultSkippedLogLevel(), getLoggingClass());
	}


	public void addSkipped(String note, String logLevel) {
		addSkipped(note, logLevel, getLoggingClass());
	}


	public void addSkipped(String note, Class<?> loggingClass) {
		addSkipped(note, getDefaultSkippedLogLevel(), loggingClass);
	}


	public void addSkipped(String note, String logLevel, Class<?> loggingClass) {
		super.addSkipped(note);
		log(logLevel, note, loggingClass);
	}


	public void addSkippedWithInfoLogging(String note) {
		addSkipped(note, INFO, getLoggingClass());
	}


	public void addSkippedWithDebugLogging(String note) {
		addSkipped(note, DEBUG, getLoggingClass());
	}


	public void addSkippedWithTraceLogging(String note) {
		addSkipped(note, TRACE, getLoggingClass());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addSkipped(List<String> notes) {
		addSkipped(notes, getDefaultSkippedLogLevel(), getLoggingClass());
	}


	public void addSkipped(List<String> notes, String logLevel) {
		addSkipped(notes, logLevel, getLoggingClass());
	}


	public void addSkipped(List<String> notes, Class<?> loggingClass) {
		addSkipped(notes, getDefaultSkippedLogLevel(), loggingClass);
	}


	public void addSkipped(List<String> notes, String logLevel, Class<?> loggingClass) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addSkipped(note, logLevel, loggingClass);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////                  Log Methods                           //////////
	////////////////////////////////////////////////////////////////////////////


	private void log(String loggingLevel, String note) {
		log(loggingLevel, note, getLoggingClass());
	}


	private void log(String loggingLevel, String note, Class<?> loggingClass) {
		switch (loggingLevel) {
			case ERROR:
				LogUtils.error(loggingClass, note);
				break;
			case WARN:
				LogUtils.warn(loggingClass, note);
				break;
			case INFO:
				LogUtils.info(loggingClass, note);
				break;
			case DEBUG:
				LogUtils.debug(loggingClass, note);
				break;
			case TRACE:
				LogUtils.trace(loggingClass, note);
				break;
			case StatusDetail.CATEGORY_ERROR:
				LogUtils.error(loggingClass, note);
				break;
			case StatusDetail.CATEGORY_MESSAGE:
				log(getDefaultMsgLogLevel(), note, loggingClass);
				break;
			case StatusDetail.CATEGORY_SKIPPED:
				log(getDefaultSkippedLogLevel(), note, loggingClass);
				break;
			case StatusDetail.CATEGORY_SUCCESS:
				log(getDefaultSuccessLogLevel(), note, loggingClass);
				break;
			case StatusDetail.CATEGORY_WARNING:
				LogUtils.warn(loggingClass, note);
				break;
			default:
				LogUtils.debug(loggingClass, note);
				break;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////               Getters And Setters                     ///////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefaultMsgLogLevel() {
		return this.defaultMsgLogLevel;
	}


	public void setDefaultMsgLogLevel(String defaultMsgLogLevel) {
		this.defaultMsgLogLevel = defaultMsgLogLevel;
	}


	public String getDefaultSkippedLogLevel() {
		return this.defaultSkippedLogLevel;
	}


	public void setDefaultSkippedLogLevel(String defaultSkippedLogLevel) {
		this.defaultSkippedLogLevel = defaultSkippedLogLevel;
	}


	public Class<?> getLoggingClass() {
		return this.loggingClass;
	}


	public void setLoggingClass(Class<?> loggingClass) {
		this.loggingClass = loggingClass;
	}


	public String getDefaultSuccessLogLevel() {
		return this.defaultSuccessLogLevel;
	}


	public void setDefaultSuccessLogLevel(String defaultSuccessLogLevel) {
		this.defaultSuccessLogLevel = defaultSuccessLogLevel;
	}
}
