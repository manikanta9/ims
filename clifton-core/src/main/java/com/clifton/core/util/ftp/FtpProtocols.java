package com.clifton.core.util.ftp;


/**
 * The <code>FtpProtocols</code> specify the secure/non-secure
 * {@code FTP}, {@code SFTP}, and {@code FTPS} protocols.
 *
 * @author msiddiqui
 */
public enum FtpProtocols {
	FTP, SFTP, FTPS
}
