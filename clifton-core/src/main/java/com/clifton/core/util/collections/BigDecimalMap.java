package com.clifton.core.util.collections;


import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>BigDecimalMap</code> is an implementation of {@link NumberMap} that maps a key to a {@link BigDecimal} and provides
 * convenience methods for modifying the values. This implementation is not thread-safe.
 *
 * @param <K> the key type
 * @author jgommels
 */
public class BigDecimalMap<K> implements NumberMap<K, BigDecimal> {

	private final Map<K, BigDecimal> map = new HashMap<>();


	@Override
	public BigDecimal add(K key, BigDecimal value) {
		BigDecimal currentValue = this.get(key);
		BigDecimal newValue = MathUtils.add(value, currentValue);
		this.map.put(key, newValue);

		return newValue;
	}


	@Override
	public BigDecimal subtract(K key, BigDecimal value) {
		BigDecimal currentValue = this.map.get(key);
		BigDecimal newValue = MathUtils.subtract(currentValue, value);
		this.map.put(key, newValue);

		return newValue;
	}


	@Override
	public BigDecimal get(K key) {
		BigDecimal value = this.map.get(key);
		if (value == null) {
			value = BigDecimal.ZERO;
		}

		return value;
	}
}
