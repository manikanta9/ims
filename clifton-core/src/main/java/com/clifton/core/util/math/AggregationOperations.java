package com.clifton.core.util.math;

import com.clifton.core.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.function.Function;


/**
 * A selection of numerical aggregation types. These calculations may be applied to a collection of numeric values to produce a single aggregated value.
 * <p>
 * Aggregation operations produce a single numeric value by performing some operation on a collection of numeric values. For example, {@link #SUM} may be used to find the sum
 * of all of the values in a collection, or {@link #STDEV_P} may be used to find the population standard deviation of all of the values in a collection.
 * <p>
 * Aggregation operations may return <tt>null</tt> when the provided collection of values is empty in cases where the aggregation operator does not logically apply to empty sets.
 * For example, the mean average of an empty set is not defined and the {@link #MEAN} operator will return <tt>null</tt> for empty sets.
 *
 * @author MikeH
 */
public enum AggregationOperations {

	SUM(CoreMathUtils::sum, "Sum"),
	MEAN(CoreMathUtils::meanAverage, "Average (Mean)"),
	MEDIAN(CoreMathUtils::medianAverage, "Average (Median)"),
	MAX(CoreMathUtils::max, "Max"),
	MIN(CoreMathUtils::min, "Min"),
	COUNT(valueList -> BigDecimal.valueOf(CollectionUtils.getSize(valueList)), "Count"),
	/**
	 * Finds the sample standard deviation of given collections.
	 */
	STDEV_S(CoreMathUtils::sampleStandardDeviation, "Standard Deviation (Sample)"),
	/**
	 * Finds the population standard deviation of given collections.
	 */
	STDEV_P(CoreMathUtils::populationStandardDeviation, "Standard Deviation (Population)"),
	/**
	 * Finds the sample variance of given collections.
	 */
	VAR_S(CoreMathUtils::sampleVariance, "Variance (Sample)"),
	/**
	 * Finds the population variance of given collections.
	 */
	VAR_P(CoreMathUtils::populationVariance, "Variance (Population)");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final Function<Collection<BigDecimal>, BigDecimal> operator;
	private final String label;


	AggregationOperations(Function<Collection<BigDecimal>, BigDecimal> operator, String label) {
		this.operator = operator;
		this.label = label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Aggregates the set of values according to the aggregation operation, returning the result.
	 *
	 * @param valueList the set of values to aggregate
	 * @return the aggregation result
	 */
	public BigDecimal aggregate(Collection<BigDecimal> valueList) {
		return getOperator().apply(valueList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Function<Collection<BigDecimal>, BigDecimal> getOperator() {
		return this.operator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getLabel() {
		return this.label;
	}
}
