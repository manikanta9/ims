package com.clifton.core.util.matching;


import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * The <code>MatchingResultItem</code> defines a sub match and a score for the sub match.
 *
 * @param <I>
 * @param <E>
 * @author mwacker
 */
public class MatchingResultItem<I, E> {

	/**
	 * The list of internal items.
	 */
	private List<I> internalList;
	/**
	 * The list of external items.
	 */
	private List<E> externalList;
	/**
	 * The match score.
	 */
	private int score;


	public MatchingResultItem(List<I> internalList, List<E> externalList) {
		this.internalList = internalList;
		this.externalList = externalList;
	}


	public List<I> getInternalList() {
		return this.internalList;
	}


	public void setInternalList(List<I> internalList) {
		this.internalList = internalList;
	}


	public List<E> getExternalList() {
		return this.externalList;
	}


	public void setExternalList(List<E> externalList) {
		this.externalList = externalList;
	}


	public int getScore() {
		return this.score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	@Override
	public boolean equals(Object obj) {
		if ((obj instanceof MatchingResultItem)) {
			@SuppressWarnings("unchecked")
			MatchingResultItem<I, E> item = (MatchingResultItem<I, E>) obj;
			if (item.getInternalList().size() != getInternalList().size()) {
				return false;
			}
			if (item.getExternalList().size() != getExternalList().size()) {
				return false;
			}
			for (I internal : CollectionUtils.getIterable(item.getInternalList())) {
				if (!getInternalList().contains(internal)) {
					return false;
				}
			}
			for (E external : CollectionUtils.getIterable(item.getExternalList())) {
				if (!getExternalList().contains(external)) {
					return false;
				}
			}
			return true;
		}
		return super.equals(obj);
	}


	@Override
	public String toString() {
		return getInternalList() + "::" + getExternalList();
	}


	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
