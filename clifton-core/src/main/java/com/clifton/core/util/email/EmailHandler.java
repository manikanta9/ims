package com.clifton.core.util.email;


/**
 * Service interface for handling email functionality.
 */
public interface EmailHandler {

	/**
	 * This method sends email to the addresses specified in {@code email}.
	 *
	 * @param email to be sent with subject, content, type etc.
	 * @see Email
	 */
	public void send(Email email);


	/**
	 * @return the default from address as configured in the implementation
	 */
	public String getDefaultFromAddress();


	/**
	 * Closes this email service, by closing the Session.
	 * <p/>
	 * This method should explicitly be called to  ensure resource cleanup, if
	 * the encompassing container is still running. In case, the encompassing
	 * container is also shut down, then this method may not be needed to be
	 * called.
	 */
	public void closeService();


	public String getSmtpHost();
}
