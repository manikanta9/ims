package com.clifton.core.util.ftp;

import com.clifton.core.util.StringUtils;


public class FtpResult {

	private Throwable error;
	private String warning;
	private String fileName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FtpResult(String fileName) {
		this.fileName = fileName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isSuccess() {
		return getError() == null;
	}


	public boolean hasWarning() {
		return !StringUtils.isEmpty(this.warning);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Throwable getError() {
		return this.error;
	}


	public void setError(Throwable error) {
		this.error = error;
	}


	public String getWarning() {
		return this.warning;
	}


	public void setWarning(String warning) {
		this.warning = warning;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
}
