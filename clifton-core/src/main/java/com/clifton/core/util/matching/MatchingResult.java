package com.clifton.core.util.matching;


import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * The <code>MatchingResult</code> defines a list of sub match items and to combined score.
 *
 * @param <I>
 * @param <E>
 * @author mwacker
 */
public class MatchingResult<I, E> {

	private List<MatchingResultItem<I, E>> itemList;
	private int score;


	public MatchingResult(List<MatchingResultItem<I, E>> combinationItemList) {
		this.itemList = combinationItemList;
	}


	public List<MatchingResultItem<I, E>> getItemList() {
		return this.itemList;
	}


	public void setItemList(List<MatchingResultItem<I, E>> combinationItemList) {
		this.itemList = combinationItemList;
	}


	public int getScore() {
		return this.score;
	}


	public void setScore(int score) {
		this.score = score;
	}


	@Override
	public boolean equals(Object obj) {
		if ((obj instanceof MatchingResult)) {
			@SuppressWarnings("unchecked")
			MatchingResult<I, E> combination = (MatchingResult<I, E>) obj;
			if (combination.getItemList().size() != getItemList().size()) {
				return false;
			}
			for (MatchingResultItem<I, E> item : CollectionUtils.getIterable(combination.getItemList())) {
				if (!getItemList().contains(item)) {
					return false;
				}
			}
			return true;
		}
		return super.equals(obj);
	}


	@Override
	public int hashCode() {
		return super.hashCode();
	}
}
