package com.clifton.core.util;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.compare.CompareUtils;

import java.util.Collection;


/**
 * The {@link CoreCollectionUtils} class contains helper methods for working with Collection(s).
 *
 * @author vgomelsky
 */
public class CoreCollectionUtils {

	/**
	 * Given a collection, return the index of the first entity in the list whose propertyName value equals
	 * the provided value
	 */
	public static <T> Integer getIndexOfFirstPropertyMatch(Collection<T> collection, String propertyName, Object value) {
		Integer index = 0;
		for (T object : CollectionUtils.getIterable(collection)) {
			if (CompareUtils.isEqual(BeanUtils.getPropertyValue(object, propertyName), value)) {
				return index;
			}
			index++;
		}
		return null;
	}


	/**
	 * Creates a shallow clone of the specified collection.
	 */
	public static <T extends Collection<U>, U> T clone(T collection) {
		if (collection == null) {
			return null;
		}

		T result = BeanUtils.newInstance(collection);
		result.addAll(collection);
		return result;
	}


	/**
	 * Produces a new collection which contains all of the elements in <tt>sourceCollection</tt> which do not exist in <tt>removeCollection</tt>.
	 * <p>
	 * The type of the result will be the same as the type of the source collection.
	 *
	 * @param sourceCollection the source collection
	 * @param removeCollection the collection of items which should be excluded from the resulting collection
	 * @param <T>              the type of the collection elements
	 * @param <U>              the source and result collection type
	 * @return the cloned collection with items excluded
	 */
	public static <T, U extends Collection<T>> U removeAll(U sourceCollection, Collection<T> removeCollection) {
		if (CollectionUtils.isEmpty(sourceCollection) || CollectionUtils.isEmpty(removeCollection)) {
			return sourceCollection;
		}

		U resultList = clone(sourceCollection);
		resultList.removeAll(removeCollection);
		return resultList;
	}
}
