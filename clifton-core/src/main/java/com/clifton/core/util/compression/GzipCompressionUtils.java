package com.clifton.core.util.compression;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.zip.GZIPInputStream;
import java.util.zip.GZIPOutputStream;


/**
 * Taken from:
 * https://stackoverflow.com/questions/14777800/gzip-compression-to-a-byte-array
 *
 * @author stevenf
 */
public class GzipCompressionUtils {

	public static byte[] compress(final String value) throws IOException {
		if ((value == null) || (value.isEmpty())) {
			return null;
		}
		return compress(value.getBytes(StandardCharsets.UTF_8));
	}


	/**
	 * Nested try with resources is intentional here since the gzip output stream must be closed before reading the bytes
	 * for compression to work properly.
	 * <p>
	 * https://stackoverflow.com/questions/24531089/exception-unexpected-end-of-zlib-input-stream
	 */
	public static byte[] compress(final byte[] bytes) throws IOException {
		if (bytes != null) {
			try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
				try (GZIPOutputStream gzip = new GZIPOutputStream(baos)) {
					gzip.write(bytes);
					gzip.flush();
				}
				return baos.toByteArray();
			}
		}
		return null;
	}


	public static String decompress(final byte[] compressed) throws IOException {
		final StringBuilder stringBuilder = new StringBuilder();
		if ((compressed == null) || (compressed.length == 0)) {
			return "";
		}
		if (isCompressed(compressed)) {
			try (ByteArrayInputStream bis = new ByteArrayInputStream(compressed);
			     GZIPInputStream gis = new GZIPInputStream(bis);
			     InputStreamReader isr = new InputStreamReader(gis, StandardCharsets.UTF_8);
			     BufferedReader br = new BufferedReader(isr)) {
				String line;
				while ((line = br.readLine()) != null) {
					stringBuilder.append(line);
				}
				return stringBuilder.toString();
			}
		}
		else {
			return new String(compressed);
		}
	}


	public static boolean isCompressed(final byte[] compressed) {
		return (compressed[0] == (byte) (GZIPInputStream.GZIP_MAGIC)) && (compressed[1] == (byte) (GZIPInputStream.GZIP_MAGIC >> 8));
	}
}
