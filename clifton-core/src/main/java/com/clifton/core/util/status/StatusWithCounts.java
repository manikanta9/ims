package com.clifton.core.util.status;

import com.clifton.core.util.StringUtils;


/**
 * The <code></code>StatusWithCounts</code> object can be used to track specific counts if processed, inserts, updates, deletes, and skips
 * <p>
 * Note: this doesn't track error count independently, but uses the error message count
 *
 * @author manderson
 */
public class StatusWithCounts extends Status {

	private int processCount = 0;

	private int insertCount = 0;

	private int updateCount = 0;

	private int deleteCount = 0;

	private int skipCount = 0;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static StatusWithCounts ofMessage(String message) {
		StatusWithCounts status = new StatusWithCounts();
		status.setMessage(message);
		return status;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getCountMessage(String prefix) {
		StringBuilder sb = new StringBuilder(prefix);
		boolean noResults = true;
		if (getProcessCount() != 0) {
			noResults = false;
			sb.append("Processed: ").append(getProcessCount()).append("; ");
		}
		if (getInsertCount() != 0) {
			noResults = false;
			sb.append("Inserted: ").append(getInsertCount()).append("; ");
		}
		if (getUpdateCount() != 0) {
			noResults = false;
			sb.append("Updated: ").append(getUpdateCount()).append("; ");
		}
		if (getDeleteCount() != 0) {
			noResults = false;
			sb.append("Deleted: ").append(getDeleteCount()).append("; ");
		}
		if (getSkipCount() != 0) {
			noResults = false;
			sb.append("Skipped: ").append(getSkipCount()).append("; ");
		}
		if (getErrorCount() != 0) {
			noResults = false;
			sb.append("Errors: ").append(getErrorCount()).append("; ");
		}
		if (noResults) {
			return "No " + (StringUtils.coalesce(prefix, "data")) + " found to process";
		}
		return sb.toString();
	}


	public void incrementProcessCount() {
		this.processCount++;
	}


	public void incrementProcessCount(int processCount) {
		this.processCount += processCount;
	}


	public void incrementProcessCountWithMessage(String message) {
		incrementProcessCount();
		addMessage(message);
	}


	public void incrementInsertCount() {
		this.insertCount++;
	}


	public void incrementInsertCount(int insertCount) {
		this.insertCount += insertCount;
	}


	public void incrementInsertCountWithMessage(String message) {
		incrementInsertCount();
		addMessage(message);
	}


	public void incrementUpdateCount() {
		this.updateCount++;
	}


	public void incrementUpdateCount(int updateCount) {
		this.updateCount += updateCount;
	}


	public void incrementUpdateCountWithMessage(String message) {
		incrementUpdateCount();
		addMessage(message);
	}


	public void incrementSaveCountWithMessage(boolean insert, String message) {
		if (insert) {
			incrementInsertCountWithMessage(message);
		}
		else {
			incrementUpdateCountWithMessage(message);
		}
	}


	public void incrementDeleteCount() {
		this.deleteCount++;
	}


	public void incrementDeleteCount(int deleteCount) {
		this.deleteCount += deleteCount;
	}


	public void incrementDeleteCountWithMessage(String message) {
		incrementDeleteCount();
		addMessage(message);
	}


	public void incrementSkipCount() {
		this.skipCount++;
	}


	public void incrementSkipCount(int skipCount) {
		this.skipCount += skipCount;
	}


	public void incrementSkipCountWithMessage(String message) {
		incrementSkipCount();
		addSkipped(message);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getProcessCount() {
		return this.processCount;
	}


	public int getInsertCount() {
		return this.insertCount;
	}


	public int getUpdateCount() {
		return this.updateCount;
	}


	public int getDeleteCount() {
		return this.deleteCount;
	}


	public int getSkipCount() {
		return this.skipCount;
	}
}
