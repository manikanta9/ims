package com.clifton.core.util.compare;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The {@link NumberAwareStringComparator} is a {@link String} {@link Comparator} that compares all numeric substrings using numeric comparisons instead of alphanumeric
 * comparisons.
 * <p>
 * This comparator works by splitting each string into separate components based on number/non-number boundaries and then comparing each pair of components in order. For example,
 * the string <code>"there are 10 foxes in the garden"</code> is split into the components <code>"there are "</code>, <code>"10"</code>, and <code>" foxes in the garden"</code>.
 * When compared against the string <code>"there are 9 foxes in the garden"</code>, the first component of each string (<code>"there are "</code>) will be equal, so the second
 * component (<code>"10"</code> and <code>"9"</code>) will be compared. Since this is a numeric component, a numeric comparison will be done and <code>"9"</code> will be evaluated
 * as less than <code>"10"</code> (the opposite result of a standard alphanumeric comparison).
 * <p>
 * This comparator converts numbers to {@link Long} values by default, but can also use {@link BigDecimal} by enabling the {@link #useBigDecimal} flag. When {@link BigDecimal} is
 * used, numbers of any length are supported and the compared numbers will properly include decimal components, rather than treating numbers after the decimal components as
 * separate integers. {@link Long} comparison should only be used when numbers in the string can reasonably be expected to not overflow the {@link Long} data type ({@value
 * Long#MIN_VALUE} to {@value Long#MAX_VALUE}). In the event of an overflow, an exception will be thrown.
 *
 * @author MikeH
 */
public class NumberAwareStringComparator implements Comparator<String> {

	/**
	 * The {@link Pattern} for matching {@link BigDecimal}-compatible values within strings. This supports signed decimal values of any length. If the sign character (<code>+
	 * </code> or <code>-</code>) is prepended by an alphanumeric character then it is treated as a string delimiter instead of part of the number.
	 */
	private static final Pattern BIG_DECIMAL_NUMERIC_PATTERN = Pattern.compile("(?:(?<![a-zA-Z0-9])[+-])?(?:\\d[\\d,]*)?(?:\\d\\.?|\\.\\d+)");

	/**
	 * If <code>true</code>, {@link BigDecimal} will be used when parsing and comparing numeric values. Otherwise, {@link Long} will be used.
	 */
	private boolean useBigDecimal;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public NumberAwareStringComparator() {
		this(false);
	}


	public NumberAwareStringComparator(boolean useBigDecimal) {
		this.useBigDecimal = useBigDecimal;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int compare(String str1, String str2) {
		// Guard-clauses: Address trivial cases
		if (Objects.equals(str1, str2)) {
			return 0;
		}
		if (str1 == null) {
			return -1;
		}
		if (str2 == null) {
			return 1;
		}

		// Evaluate comparison, returning -1, 0, or 1
		return Integer.compare(compareNumberAware(str1, str2), 0);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int compareNumberAware(String str1, String str2) {
		return isUseBigDecimal()
				? compareBigDecimalNumberAware(str1, str2)
				: compareLongNumberAware(str1, str2);
	}


	/**
	 * Compares the two strings parsing all numbers as {@link Long} integer values.
	 */
	private int compareLongNumberAware(String str1, String str2) {
		// Iterate over strings, comparing by part
		int str1Index = 0;
		int str2Index = 0;
		while (str1Index < str1.length() && str2Index < str2.length()) {
			String s1NextNumber = getIntegerStringAtIndex(str1, str1Index);
			String s2NextNumber = getIntegerStringAtIndex(str2, str2Index);
			if (s1NextNumber != null || s2NextNumber != null) {
				// Evaluate non-same types
				if (s1NextNumber == null || s2NextNumber == null) {
					return str1.substring(str1Index).compareTo(str2.substring(str2Index));
				}
				// Evaluate numeric comparison
				long num1 = Long.parseLong(s1NextNumber);
				long num2 = Long.parseLong(s2NextNumber);
				int compareResult = Long.compare(num1, num2);
				if (compareResult != 0) {
					return compareResult;
				}
				str1Index += s1NextNumber.length();
				str2Index += s2NextNumber.length();
			}
			else {
				// Evaluate non-numeric comparison
				int compareResult = Character.compare(str1.charAt(str1Index), str2.charAt(str2Index));
				if (compareResult != 0) {
					return compareResult;
				}
				++str1Index;
				++str2Index;
			}
		}

		// One or both indexes have reached end of string; Check end-of-string criteria
		if (str1.length() > str1Index) {
			return 1;
		}
		if (str2.length() > str2Index) {
			return -1;
		}
		return 0;
	}


	/**
	 * Compares the two strings parsing all numbers as {@link BigDecimal} decimal values.
	 */
	private int compareBigDecimalNumberAware(String str1, String str2) {
		// Guard-clause: Avoid pattern-matching when empty strings exist
		if (str1.isEmpty() || str2.isEmpty()) {
			return Objects.compare(str1, str2, String::compareTo);
		}

		// Compare the strings, one part (numeric or non-numeric) at a time
		Matcher str1Matcher = BIG_DECIMAL_NUMERIC_PATTERN.matcher(str1);
		Matcher str2Matcher = BIG_DECIMAL_NUMERIC_PATTERN.matcher(str2);
		int str1Index = 0;
		int str2Index = 0;
		while (str1Matcher.find(str1Index) && str2Matcher.find(str2Index)) {
			// Compare string up to next number
			String intermediateStr1 = str1.substring(str1Index, str1Matcher.start());
			String intermediateStr2 = str2.substring(str2Index, str2Matcher.start());
			int strCompareResult = intermediateStr1.compareTo(intermediateStr2);
			if (strCompareResult != 0) {
				return strCompareResult;
			}
			// Perform numeric comparison
			String str1NumberStr = str1Matcher.group();
			String str2NumberStr = str2Matcher.group();
			BigDecimal str1Number = new BigDecimal(str1NumberStr);
			BigDecimal str2Number = new BigDecimal(str2NumberStr);
			int numCompareResult = str1Number.compareTo(str2Number);
			if (numCompareResult != 0) {
				return numCompareResult;
			}
			// Update indices
			str1Index = str1Matcher.end();
			str2Index = str2Matcher.end();
		}

		// Compare remainder of string
		return str1.substring(str1Index).compareTo(str2.substring(str2Index));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the integer string represented at the provided index in the given string. This handles signed integers (<code>+</code> or <code>-</code>) of any length. Sign characters
	 * which are likely delimiters (that is, they are prepended by a letter or number) are not treated as components of integer strings.
	 * <p>
	 * If the string does not contain an integer representation at the provided index then <code>null</code> is returned.
	 */
	private String getIntegerStringAtIndex(String str, int index) {
		final String nextNumber;
		if (str.length() > index
				&& (Character.isDigit(str.charAt(index)) || isNumericAtIndex(str, index))) {
			// Find numeric string
			int endIndex = index;
			while (++endIndex < str.length() && Character.isDigit(str.charAt(endIndex))) {
				// Do nothing; incrementing endIndex
			}
			nextNumber = str.substring(index, endIndex);
		}
		else {
			// Non-numeric start
			nextNumber = null;
		}
		return nextNumber;
	}


	private boolean isNumericAtIndex(String str, int index) {
		return (str.charAt(index) == '-' || str.charAt(index) == '+') // Current character is a sign
				&& str.length() > index + 1 && Character.isDigit(str.charAt(index + 1)) // Next character is a number
				&& (index < 1 || !Character.isLetterOrDigit(str.charAt(index - 1))); // Do not include sign when it is more likely a delimiter
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isUseBigDecimal() {
		return this.useBigDecimal;
	}


	public void setUseBigDecimal(boolean useBigDecimal) {
		this.useBigDecimal = useBigDecimal;
	}
}
