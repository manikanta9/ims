package com.clifton.core.util.concurrent;

/**
 * The ExecutableAction interface can be used to execute any custom code (usually a method call) that does not return any values.
 *
 * @author vgomelsky
 */
public interface ExecutableAction {

	public void execute();
}
