package com.clifton.core.util.queue;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;


public class PersistentBlockingQueue<E> implements Queue<E> {

	/**
	 * The queue used to store the FIX log entries that need to be processed.
	 */
	private LinkedBlockingQueue<E> queue;

	private String fileNameAndPath;

	/**
	 * Lock held by take, poll, etc
	 */
	private final ReentrantLock lock = new ReentrantLock();


	@Override
	@SuppressWarnings("unchecked")
	public List<E> getElementList() {
		return (List<E>) Arrays.asList(getQueue().toArray());
	}


	@Override
	public void put(E e) throws InterruptedException {
		getQueue().put(e);
		saveQueue();
	}


	@Override
	public E take() throws InterruptedException {
		E result = getQueue().take();
		saveQueue();
		return result;
	}


	@Override
	public boolean add(E e) {
		boolean result = getQueue().add(e);
		if (result) {
			saveQueue();
		}
		return result;
	}


	@Override
	public boolean offer(E e) {
		boolean result = getQueue().offer(e);
		if (result) {
			saveQueue();
		}
		return result;
	}


	@Override
	public E poll(long timeout, TimeUnit unit) throws InterruptedException {
		E result = getQueue().poll(timeout, unit);
		if (result != null) {
			saveQueue();
		}
		return result;
	}


	@Override
	public E poll() {
		E result = getQueue().poll();
		if (result != null) {
			saveQueue();
		}
		return result;
	}


	@Override
	public boolean remove(Object o) {
		boolean result = getQueue().remove(o);
		if (result) {
			saveQueue();
		}
		return result;
	}


	@Override
	public int drainTo(Collection<? super E> c) {
		int result = getQueue().drainTo(c);
		if (result > 0) {
			saveQueue();
		}
		return result;
	}


	@Override
	public int drainTo(Collection<? super E> c, int maxElements) {
		int result = getQueue().drainTo(c, maxElements);
		if (result > 0) {
			saveQueue();
		}
		return result;
	}


	@Override
	public E peek() {
		return getQueue().peek();
	}


	@Override
	public E remove() {
		E result = getQueue().remove();
		if (result != null) {
			saveQueue();
		}
		return result;
	}


	@Override
	public int remainingCapacity() {
		return getQueue().remainingCapacity();
	}


	@Override
	public boolean contains(Object o) {
		return getQueue().contains(o);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private void lock() {
		this.lock.lock();
	}


	private void unlock() {
		this.lock.unlock();
	}


	private LinkedBlockingQueue<E> getQueue() {
		if (this.queue == null) {
			this.queue = loadQueue();
		}
		// if the file doesn't exists, create an empty queue and save it
		if (this.queue == null) {
			this.queue = new LinkedBlockingQueue<>();
			saveQueue();
		}
		return this.queue;
	}


	private void saveQueue() {
		// only need to lock the file because the serialization method of the LinkedBlockingQueue will lock the queue
		// this may not be needed, but it's here to be safe
		lock();
		try {
			ObjectOutputStream stream = null;
			try {
				stream = new ObjectOutputStream(new FileOutputStream(new File(getFileNameAndPath())));
				stream.writeObject(this.queue);
			}
			catch (Throwable e) {
				throw new RuntimeException("Failed to persist queue to file system.", e);
			}
			finally {
				if (stream != null) {
					try {
						stream.close();
					}
					catch (IOException e) {
						LogUtils.error(getClass(), "Failed to close Queue stream.", e);
					}
				}
			}
		}
		finally {
			unlock();
		}
	}


	@SuppressWarnings("unchecked")
	private LinkedBlockingQueue<E> loadQueue() {
		// only need to lock the file because the serialization method of the LinkedBlockingQueue will lock the queue
		// this may not be needed, but it's here to be safe
		lock();
		try {
			File file = new File(getFileNameAndPath());
			if (StringUtils.isEmpty(getFileNameAndPath()) || !file.exists()) {
				return null;
			}

			ObjectInputStream stream = null;
			try {
				stream = new ObjectInputStream(new FileInputStream(file));
				return (LinkedBlockingQueue<E>) stream.readObject();
			}
			catch (Throwable e) {
				throw new RuntimeException("Failed to load queue from file system.", e);
			}
			finally {
				if (stream != null) {
					try {
						stream.close();
					}
					catch (IOException e) {
						LogUtils.error(getClass(), "Failed to close Queue stream.", e);
					}
				}
			}
		}
		finally {
			unlock();
		}
	}


	public String getFileNameAndPath() {
		return this.fileNameAndPath;
	}


	public void setFileNameAndPath(String fileNameAndPath) {
		this.fileNameAndPath = fileNameAndPath;
	}
}
