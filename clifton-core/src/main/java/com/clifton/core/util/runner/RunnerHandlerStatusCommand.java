package com.clifton.core.util.runner;

/**
 * <code>RunnerHandlerStatusCommand</code> is used to customize the behavior and response data
 * when fetching a {@link RunnerHandlerStatus}.
 *
 * @author NickK
 */
public class RunnerHandlerStatusCommand {

	// flag to signal a reset of statistics
	private boolean clearStatistics;

	// flag to return the status of all threads in the current Java process
	private boolean includeStatusOfAllThreads;

	// Runner criteria used for registering execution monitoring
	private Runner runner;
	private String type;
	private String typeId;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isClearStatistics() {
		return this.clearStatistics;
	}


	public void setClearStatistics(boolean clearStatistics) {
		this.clearStatistics = clearStatistics;
	}


	public boolean isIncludeStatusOfAllThreads() {
		return this.includeStatusOfAllThreads;
	}


	public void setIncludeStatusOfAllThreads(boolean includeStatusOfAllThreads) {
		this.includeStatusOfAllThreads = includeStatusOfAllThreads;
	}


	public Runner getRunner() {
		return this.runner;
	}


	public void setRunner(Runner runner) {
		this.runner = runner;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTypeId() {
		return this.typeId;
	}


	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}
}
