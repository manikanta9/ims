package com.clifton.core.util.runner.execution;

import com.clifton.core.util.runner.Runner;

import java.util.concurrent.TimeUnit;


/**
 * <code>RunnerCompletionResult</code> can be used to return execution details of a {@link Runner}.
 *
 * @author NickK
 */
public interface RunnerCompletionResult {

	public Runner getRunner();


	public void complete();


	public void completeWithError(Throwable throwable);


	public long getQueuedDuration(TimeUnit timeUnit);


	public long getRunDuration(TimeUnit timeUnit);


	public boolean isSuccessful();


	public Throwable getError();
}
