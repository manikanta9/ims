package com.clifton.core.util.concurrent.synchronize;

import com.clifton.core.util.concurrent.ExecutableAction;


/**
 * <code>SynchronizableExecutableAction</code> defines an action that is to be executed within the scope of an obtained lock.
 * This object defines the required locking attributes ({@link #getSecureAreaName()}, {@link #getLockKeySet()},
 * and {@link #getLockMessage()}) and the action to be executed ({@link #getExecutableAction()}).
 *
 * @author NickK
 * @see SynchronizableBuilder
 */
public interface SynchronizableExecutableAction extends Synchronizable {

	/**
	 * Returns the action to be executed after the lock is successfully obtained: usually a method call () -> myMethod(arg1, arg2, ...).
	 * <p>
	 * This property is required in order to obtain a lock and execute an action.
	 */
	public ExecutableAction getExecutableAction();
}
