package com.clifton.core.util.compare;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiFunction;


/**
 * The {@link CoreCompareUtils} class contains helper utility methods for comparing various objects.
 *
 * @author vgomelsky
 */
public class CoreCompareUtils {


	/**
	 * Returns true if all of the specified compareProperties of both objects are equal.
	 * Note: null is equal to null.
	 */
	public static boolean isEqual(Object o1, Object o2, String[] compareProperties) {
		for (String property : compareProperties) {
			if (!CompareUtils.isEqual(BeanUtils.getPropertyValue(o1, property, false), BeanUtils.getPropertyValue(o2, property, false))) {
				return false;
			}
		}

		return true;
	}


	/**
	 * Compares not null values of the first objects to these of the second object and throws a ValidationException
	 * if it fines a property that is not the same. Comparison is limited to only those properties that are specified
	 * by the 'properties' argument. The method supports nested properties.
	 */
	public static void validateNonNullPropertiesEqual(Object o1, Object o2, String... properties) {
		Map<String, Object> keyValueMap1 = BeanUtils.describe(o1);
		Map<String, Object> keyValueMap2 = BeanUtils.describe(o2);

		// keys are first level property names and values are remaining properties if any
		Map<String, List<String>> propertyMap = new HashMap<>();
		for (String property : properties) {
			String rootProperty = property;
			int nestedStart = property.indexOf('.');
			if (nestedStart != -1) {
				rootProperty = property.substring(0, nestedStart);
			}
			if (!propertyMap.containsKey(rootProperty)) {
				propertyMap.put(rootProperty, new ArrayList<>());
			}
			if (nestedStart != -1) {
				propertyMap.get(rootProperty).add(property.substring(nestedStart + 1));
			}
		}

		for (Map.Entry<String, List<String>> stringListEntry : propertyMap.entrySet()) {
			Object value1 = keyValueMap1.get(stringListEntry.getKey());
			if (value1 != null) {
				Object value2 = keyValueMap2.get(stringListEntry.getKey());
				List<String> nestedProperties = stringListEntry.getValue();
				if (!CollectionUtils.isEmpty(nestedProperties)) {
					// validate nested properties
					validateNonNullPropertiesEqual(value1, value2, nestedProperties.toArray(new String[nestedProperties.size()]));
				}
				else if (!CompareUtils.isEqualWithSmartLogic(value1, value2)) {
					throw new ValidationException("Expected property '" + stringListEntry.getKey() + "' to be " + value1 + " but was " + value2);
				}
			}
		}
	}


	/**
	 * Returns true if all non-null property values of <code>o1</code> are equal to <code>o2</code>. For {@link Number} comparisons,
	 * {@link MathUtils#isEqual(Number, Number)} is used.
	 *
	 * @param o1                       the Object to compare against. Properties with non-null values will be compared.
	 * @param o2                       the Object to compare with <code>o1</code> for equality.
	 * @param throwExceptionIfNotEqual if true, causes a {@link ValidationException} to be thrown if the two objects are not equal.
	 * @param recursiveClasses         an array of classes such that for any non-null property <code>p</code> of <code>o1</code>, non-null
	 *                                 comparisons will recursively continue for <code>p</code> if <code>p.getClass().isAssignableFrom(c)</code> returns true,
	 *                                 where c is any class in this array. If recursive comparisons are not necessary, then set this value to <code>null</code>.
	 * @param excludedProperties       the names of properties to be excluded from the comparison
	 * @return true if the two objects or equal, otherwise returns false.
	 */
	public static boolean isNonNullPropertiesEqual(Object o1, Object o2, boolean throwExceptionIfNotEqual, Class<?>[] recursiveClasses, String... excludedProperties) {
		return isNonNullPropertiesEqual(o1, o2, throwExceptionIfNotEqual, recursiveClasses, false, excludedProperties);
	}


	/**
	 * See {@link #isNonNullPropertiesEqual(Object, Object, boolean, Class[], String...)}. This method allows for defining if the properties specified are
	 * allowed to be included or excluded.
	 *
	 * @param includeGivenProperties if {@code true}, only the given {@code properties} will be compared; otherwise, the given {@code properties} will be
	 *                               excluded from comparison
	 */
	public static boolean isNonNullPropertiesEqual(Object o1, Object o2, boolean throwExceptionIfNotEqual, Class<?>[] recursiveClasses, boolean includeGivenProperties, String... properties) {
		Map<String, Object> d1 = BeanUtils.describe(o1);
		Map<String, Object> d2 = BeanUtils.describe(o2);

		for (Map.Entry<String, Object> d1Entry : d1.entrySet()) {
			String prop = d1Entry.getKey();
			Object val = d1Entry.getValue();
			if (val != null && ArrayUtils.contains(properties, prop) == includeGivenProperties) {
				Object val2 = d2.get(prop);
				boolean isRecursiveProperty = recursiveClasses != null && ClassUtils.isAssignableFromAny(val.getClass(), recursiveClasses);
				if ((isRecursiveProperty && !isNonNullPropertiesEqual(val, val2, throwExceptionIfNotEqual, recursiveClasses, includeGivenProperties, properties)) || (!isRecursiveProperty && !CompareUtils.isEqualWithSmartLogic(val, val2))) {
					if (throwExceptionIfNotEqual) {
						throw new ValidationException("Expected property '" + prop + "' to be " + val + " but was " + val2);
					}
					return false;
				}
			}
		}

		return true;
	}


	/**
	 * Uses the same logic as {@link #isNonNullPropertiesEqual}, but applied to lists. That is, this method returns true if
	 * every element at index 'i' in <code>list1</code> equals element at index 'i' <code>list2</code> for which equality is determined by comparing the non-null
	 * properties of the element in <code>list1</code> to the same properties of the element in <code>list2</code>.
	 * <p>
	 * <p>This is a convenience method and calls {@link #isNonNullPropertiesEqualForLists(List, List, boolean, boolean, Class[], String...)} where recursiveClasses is null
	 *
	 * @param list1                    the list to compare against
	 * @param list2                    the list to compare against <code>list1</code>
	 * @param sorted                   specify true if the lists' items are in the same order (item at index 1 of each list is equal, item at index 2 of each list is equal, etc.)
	 * @param throwExceptionIfNotEqual if true, causes a {@link ValidationException} to be thrown if the two lists do not match
	 * @param excludedProperties       the names of properties to be excluded from the comparison
	 */
	public static boolean isNonNullPropertiesEqualForLists(List<?> list1, List<?> list2, boolean sorted, boolean throwExceptionIfNotEqual, String... excludedProperties) {
		return isNonNullPropertiesEqualForLists(list1, list2, sorted, throwExceptionIfNotEqual, null, excludedProperties);
	}


	/**
	 * Uses the same logic as {@link #isNonNullPropertiesEqual}, but applied to lists. That is, this method returns true if
	 * every element at index 'i' in <code>list1</code> equals element at index 'i' <code>list2</code> for which equality is determined by comparing the non-null
	 * properties of the element in <code>list1</code> to the same properties of the element in <code>list2</code>.
	 *
	 * @param list1                    the list to compare against
	 * @param list2                    the list to compare against <code>list1</code>
	 * @param sorted                   specify true if the lists' items are in the same order (item at index 1 of each list is equal, item at index 2 of each list is equal, etc.)
	 * @param throwExceptionIfNotEqual if true, causes a {@link ValidationException} to be thrown if the two lists do not match
	 * @param recursiveClasses         an array of classes such that for any non-null property <code>p</code> of <code>o1</code>, non-null
	 *                                 comparisons will recursively continue for <code>p</code> if <code>p.getClass().isAssignableFrom(c)</code> returns true,
	 *                                 where c is any class in this array. If recursive comparisons are not necessary, then set this value to <code>null</code>.
	 * @param excludedProperties       the names of properties to be excluded from the comparison
	 */
	public static boolean isNonNullPropertiesEqualForLists(List<?> list1, List<?> list2, boolean sorted, boolean throwExceptionIfNotEqual, Class<?>[] recursiveClasses, String... excludedProperties) {
		if (CollectionUtils.isEmpty(list1) && CollectionUtils.isEmpty(list2)) {
			return true;
		}

		if (CollectionUtils.getSize(list1) != CollectionUtils.getSize(list2)) {
			if (throwExceptionIfNotEqual) {
				throw new ValidationException("List sizes do not match.");
			}

			return false;
		}

		List<?> list2Clone = new ArrayList<>(list2);
		for (int i = 0; i < list1.size(); i++) {
			Object obj1 = list1.get(i);
			if (sorted) {
				Object obj2 = list2.get(i);
				try {
					if (!isNonNullPropertiesEqual(obj1, obj2, throwExceptionIfNotEqual, recursiveClasses, excludedProperties)) {
						return false;
					}
				}
				catch (ValidationException e) {
					throw new ValidationException("Error occurred at list index [" + i + "] while comparing lists for equality. Object from first list was: "
							+ obj1 + "\nObject from second list was: " + obj2, e);
				}
			}
			else {
				Iterator<?> iterator2 = list2Clone.iterator();
				boolean foundMatch = false;
				while (iterator2.hasNext()) {
					if (isNonNullPropertiesEqual(obj1, iterator2.next(), false, recursiveClasses, excludedProperties)) {
						iterator2.remove(); // remove match to cut down on iterations
						foundMatch = true;
						break;
					}
				}
				if (!foundMatch) {
					if (throwExceptionIfNotEqual) {
						throw new ValidationException("Error occurred while comparing lists for equality. Object from first list [" + obj1 + "] did not find a match.");
					}
					return false;
				}
			}
		}

		return true;
	}


	/**
	 * Gets the list of properties which are not equal between the two given objects. This method uses "smart" type-dependent comparisons via the {@link
	 * CompareUtils#compare(Object, Object)} method.
	 *
	 * @see #getUniquePropertyKeys(Map, Map, boolean, BiFunction, String[])
	 */
	public static List<String> getNoEqualProperties(Object o1, Object o2, boolean includeSystemManagedFields) {
		return getUniquePropertyKeys(BeanUtils.describe(o1), BeanUtils.describe(o2), includeSystemManagedFields, null);
	}


	/**
	 * Gets the list of properties which are not equal between the two given objects. This method uses "smart" type-dependent comparisons via the {@link
	 * CompareUtils#compare(Object, Object)} method.
	 *
	 * @see #getUniquePropertyKeys(Map, Map, boolean, BiFunction, String[])
	 */
	public static List<String> getNoEqualProperties(Object o1, Object o2, boolean includeSystemManagedFields, String... excludedProperties) {
		return getUniquePropertyKeys(BeanUtils.describe(o1), BeanUtils.describe(o2), includeSystemManagedFields, null, excludedProperties);
	}


	/**
	 * Gets the list of properties which are not equal between the two given objects. This method uses "smart" type-dependent comparisons via the {@link
	 * CompareUtils#compare(Object, Object)} method. <b>This method only compares properties for which both getters and setters exist.</b>
	 *
	 * @param o1                         the first object
	 * @param o2                         the second object
	 * @param includeSystemManagedFields {@code true} if {@link BeanUtils#SYSTEM_MANAGED_FIELDS system managed fields} should be compared, or {@code false}
	 *                                   otherwise
	 * @param excludedProperties         the list of properties which should be excluded from the comparison
	 * @return the list of property names for the object properties which are not equal
	 */
	public static List<String> getNoEqualPropertiesWithSetters(Object o1, Object o2, boolean includeSystemManagedFields, String... excludedProperties) {
		return getUniquePropertyKeys(BeanUtils.describeWithRequiredSetter(o1), BeanUtils.describeWithRequiredSetter(o2), includeSystemManagedFields, null, excludedProperties);
	}


	/**
	 * Gets the list of properties which are not equal between the two given objects. This method uses "smart" type-dependent comparisons via the {@link
	 * CompareUtils#compare(Object, Object)} method. <b>This method only compares properties for which both getters and setters exist.</b>
	 *
	 * @param o1                         the first object
	 * @param o2                         the second object
	 * @param includeSystemManagedFields {@code true} if {@link BeanUtils#SYSTEM_MANAGED_FIELDS system managed fields} should be compared, or {@code false}
	 *                                   otherwise
	 * @param excludedProperties         the list of properties which should be excluded from the comparison
	 * @return the list of property names for the object properties which are not equal
	 */
	public static List<String> getNoEqualPropertiesWithSettersUsingCustomComparison(Object o1, Object o2, boolean includeSystemManagedFields, BiFunction<Object, Object, Integer> customComparisonFunction, String... excludedProperties) {
		return getUniquePropertyKeys(BeanUtils.describeWithRequiredSetter(o1), BeanUtils.describeWithRequiredSetter(o2), includeSystemManagedFields, customComparisonFunction, excludedProperties);
	}


	/**
	 * Gets the list of unique property keys which are not equal between the two given object property description maps. This method uses "smart" type-dependent
	 * comparisons via the {@link CompareUtils#compare(Object, Object)} method.
	 *
	 * @param objectOneProperties        the first property description map
	 * @param objectTwoProperties        the second property description map
	 * @param includeSystemManagedFields {@code true} if {@link BeanUtils#SYSTEM_MANAGED_FIELDS system managed fields} should be compared, or {@code false}
	 *                                   otherwise
	 * @param excludedProperties         the list of properties which should be excluded from the comparison
	 * @return the list of property names for the object properties which are not equal
	 */
	private static List<String> getUniquePropertyKeys(Map<String, Object> objectOneProperties, Map<String, Object> objectTwoProperties, boolean includeSystemManagedFields, BiFunction<Object, Object, Integer> customComparisonFunction, String... excludedProperties) {
		final Set<String> excludedPropertiesSet = CollectionUtils.createHashSet(excludedProperties);
		List<String> result = new ArrayList<>();
		Map<String, Object> objectOnePropertiesClone = new HashMap<>(objectOneProperties);
		Map<String, Object> objectTwoPropertiesClone = new HashMap<>(objectTwoProperties);
		// Get the unique properties from the first object
		for (Map.Entry<String, Object> objectOneEntry : objectOnePropertiesClone.entrySet()) {
			String prop = objectOneEntry.getKey();
			if ((includeSystemManagedFields || !BeanUtils.isSystemManagedField(prop)) && !excludedPropertiesSet.contains(prop)) {
				if (customComparisonFunction != null) {
					if (customComparisonFunction.apply(objectOneEntry.getValue(), objectTwoPropertiesClone.get(prop)) != 0) {
						result.add(prop);
					}
				}
				else {
					if (CompareUtils.compare(objectOneEntry.getValue(), objectTwoPropertiesClone.get(prop)) != 0) {
						result.add(prop);
					}
				}
			}
			objectTwoPropertiesClone.remove(prop);
		}

		// Add the remaining properties from the second object
		for (Map.Entry<String, Object> objectTwoEntry : objectTwoPropertiesClone.entrySet()) {
			String prop = objectTwoEntry.getKey();
			if ((includeSystemManagedFields || !BeanUtils.isSystemManagedField(prop)) && !excludedPropertiesSet.contains(prop)) {
				if (objectTwoEntry.getValue() != null) {
					result.add(prop);
				}
			}
		}
		return result;
	}


	/**
	 * Returns true if both collections have the same number of elements and all property
	 * values of elements in each Collection are equal.  The order of elements doesn't have to be the same.
	 */
	public static <T> boolean isEqualProperties(List<T> c1, List<T> c2) {
		if (CollectionUtils.isEmpty(c1)) {
			return CollectionUtils.isEmpty(c2);
		}
		if (CollectionUtils.getSize(c1) != CollectionUtils.getSize(c2)) {
			return false;
		}

		Object firstBean = c1.iterator().next();
		Set<String> propertyNames = BeanUtils.describe(firstBean).keySet();
		String[] beanProperties = propertyNames.toArray(new String[propertyNames.size()]);
		for (T c : c1) {
			int index = c2.indexOf(c);
			if (index == -1) {
				return false;
			}
			if (!isEqual(c, c2.get(index), beanProperties)) {
				return false;
			}
		}

		return true;
	}
}
