package com.clifton.core.util.status;

import java.io.Serializable;
import java.util.Date;
import java.util.Map;
import java.util.Objects;


/**
 * The StatusDetail class contains a single details record for the corresponding {@link Status} run.
 *
 * @author vgomelsky
 */
public class StatusDetail implements Serializable {

	public final static String CATEGORY_ERROR = "Error";
	public final static String CATEGORY_WARNING = "Warning";
	public final static String CATEGORY_MESSAGE = "Message";
	public final static String CATEGORY_SKIPPED = "Skipped";
	public final static String CATEGORY_SUCCESS = "Success";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private Date date;
	private String category;
	private String note;
	private Map<String, String> properties;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	//Necessary constructor declaration for serialization/deserialization
	public StatusDetail() {

	}


	public StatusDetail(Date date, String category, String note) {
		this(date, category, note, null);
	}


	public StatusDetail(Date date, String category, String note, Map<String, String> properties) {
		this.date = date;
		this.category = category;
		this.note = note;
		this.properties = properties;
	}


	@Override
	public String toString() {
		return this.category + ": " + this.note;
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		StatusDetail that = (StatusDetail) o;
		return Objects.equals(this.category, that.category) &&
				Objects.equals(this.note, that.note);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.category, this.note);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getCategory() {
		return this.category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public String getNote() {
		return this.note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public Map<String, String> getProperties() {
		return this.properties;
	}


	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
}
