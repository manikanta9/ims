package com.clifton.core.util.retry;


import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


/**
 * A Future implementation to handle a list of completable futures
 *
 * @author theodorez
 */
public class RetryFuture<T> implements Future<List<T>> {

	private final List<CompletableFuture<T>> completableFutureList = new ArrayList<>();


	public RetryFuture(List<CompletableFuture<T>> completableFutureList) {
		if (completableFutureList != null) {
			this.completableFutureList.addAll(completableFutureList);
		}
	}


	/**
	 * Attempts to cancel all of the futures in the list
	 * Returns false if at least one cannot be canceled, otherwise true if all were canceled
	 */
	@Override
	public boolean cancel(boolean mayInterruptIfRunning) {
		for (CompletableFuture<T> future : CollectionUtils.getIterable(this.completableFutureList)) {
			if (!future.cancel(mayInterruptIfRunning)) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Returns true if all futures in the list are canceled
	 * <p>
	 * Returns false if at least one future in the list is not canceled
	 */
	@Override
	public boolean isCancelled() {
		for (CompletableFuture<T> future : CollectionUtils.getIterable(this.completableFutureList)) {
			if (!future.isCancelled()) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Returns true if all futures in the list are done
	 * <p>
	 * Returns false if at least one future in the list is not done
	 */
	@Override
	public boolean isDone() {
		for (CompletableFuture<T> future : CollectionUtils.getIterable(this.completableFutureList)) {
			if (!future.isDone()) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Gets the results from all of the futures as a list
	 */
	@Override
	public List<T> get() throws InterruptedException, ExecutionException {
		List<T> results = new ArrayList<>();
		for (CompletableFuture<T> future : CollectionUtils.getIterable(this.completableFutureList)) {
			results.add(future.get());
		}
		return results;
	}


	/**
	 * Gets the results from all of the futures as a list.
	 * <p>
	 * NOTE: The timeout parameter specifies the timeout for retrieving EACH future, not the timeout for retrieving all futures
	 */
	@Override
	public List<T> get(long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
		List<T> results = new ArrayList<>();
		for (CompletableFuture<T> future : CollectionUtils.getIterable(this.completableFutureList)) {
			results.add(future.get(timeout, unit));
		}
		return results;
	}
}
