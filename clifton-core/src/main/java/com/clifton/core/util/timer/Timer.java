package com.clifton.core.util.timer;


import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * The <code>Timer</code> class can be used to time something. It measures duration between stop and start method invocations.
 * It also allows starting and stopping tasks within the main run.
 *
 * @author vgomelsky
 */
public class Timer {

	private final Map<String, TimerTask> taskMap = new HashMap<>();

	private long startTimeNano;
	private long endTimeNano;


	public void start() {
		if (this.startTimeNano != 0) {
			throw new IllegalStateException("Cannot start the Timer that was already started and hasn't been reset");
		}
		this.startTimeNano = System.nanoTime();
	}


	public void stop() {
		if (this.endTimeNano != 0) {
			throw new IllegalStateException("Cannot stop the Timer that was already stopped and hasn't been reset");
		}
		this.endTimeNano = System.nanoTime();
	}


	public void startTask(String taskName) {
		if (this.startTimeNano == 0 || this.endTimeNano != 0) {
			throw new IllegalStateException("Cannot start task '" + taskName + "' before the main timer was started or after it was stopped");
		}
		TimerTask task = this.taskMap.computeIfAbsent(taskName, key -> new TimerTask());
		task.start();
	}


	public void stopTask(String taskName) {
		if (this.startTimeNano == 0 || this.endTimeNano != 0) {
			throw new IllegalStateException("Cannot stop task '" + taskName + "' before the main timer was started or after it was stopped");
		}
		TimerTask task = getTimerTask(taskName);
		if (task == null) {
			throw new IllegalStateException("Cannot find task: " + taskName);
		}
		task.stop();
	}


	public void reset() {
		this.startTimeNano = 0;
		this.endTimeNano = 0;
		this.taskMap.clear();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TimerTask getTimerTask(String taskName) {
		return this.taskMap.get(taskName);
	}


	public long getDurationNano() {
		long result = (this.endTimeNano - this.startTimeNano);
		if (result < 0) {
			throw new IllegalStateException("Cannot get duration of timer that was started but hasn't been stopped.");
		}
		return result;
	}


	public Set<String> getTaskNames() {
		return this.taskMap.keySet();
	}
}
