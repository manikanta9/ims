package com.clifton.core.util.runner;


import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>AbstractRunner</code> is the default implementation of the Runner interface.
 *
 * @author mwacker
 */
public abstract class AbstractRunner implements Runner {

	private final Date runDate;
	private final String type;
	private final String typeId;
	/**
	 * Most cases (default) this is true - i.e. multiple runners of the same type run concurrently,
	 * however to run one at a time, set to false, as one finishes the next will be ran, etc. in order
	 */
	private final boolean concurrentSupported;
	private Object runUser;
	private RunnerStates runnerState;


	public AbstractRunner(String type, String typeId, Date runDate) {
		this(type, typeId, runDate, true);
	}


	public AbstractRunner(String type, String typeId, Date runDate, boolean concurrentSupported) {
		this.type = type;
		this.typeId = typeId;
		this.runDate = runDate;
		this.concurrentSupported = concurrentSupported;
		this.runnerState = RunnerStates.UNSCHEDULED;
	}


	@Override
	public String getType() {
		return this.type;
	}


	@Override
	public String getTypeId() {
		return this.typeId;
	}


	@Override
	public Date getRunDate() {
		return this.runDate;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(getType());
		result.append('-');
		result.append(getTypeId());
		if (getRunDate() != null) {
			result.append(" at ");
			result.append(DateUtils.fromDate(getRunDate()));
		}
		if (getRunUser() != null) {
			result.append(" running as ");
			result.append(getRunUser());
		}
		return result.toString();
	}


	@Override
	public boolean equals(Object obj) {
		if (obj instanceof Runner) {
			Runner object = (Runner) obj;
			return this.getType().equals(object.getType()) && this.getTypeId().equals(object.getTypeId()) && DateUtils.compare(this.getRunDate(), object.getRunDate(), true) == 0;
		}
		return super.equals(obj);
	}


	@Override
	public int hashCode() {
		return super.hashCode();
	}


	@Override
	public Object getRunUser() {
		return this.runUser;
	}


	@Override
	public void setRunUser(Object runUser) {
		this.runUser = runUser;
	}


	@Override
	public boolean isConcurrentSupported() {
		return this.concurrentSupported;
	}


	@Override
	public RunnerStates getRunnerState() {
		return this.runnerState;
	}


	@Override
	public void setRunnerState(RunnerStates runnerState) {
		this.runnerState = runnerState;
	}
}
