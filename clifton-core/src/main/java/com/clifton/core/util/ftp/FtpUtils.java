package com.clifton.core.util.ftp;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.jcraft.jsch.Channel;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.ChannelSftp.LsEntry;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPFileFilter;
import org.apache.commons.net.ftp.FTPSClient;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * The <code>FtpUtils</code> provides FTP functionality of PUT/GET and Listing of files to and from remote locations.
 *
 * @author mwacker/msiddiqui
 */
public class FtpUtils {

	private static final String WRITE_FILE_EXTENSION = "write";


	public static Integer getPortFromUrl(String url) {
		Integer port = null;
		if (!StringUtils.isEmpty(url) && url.contains(":")) {
			String[] urlParts = url.split(":");
			if (urlParts.length == 2) {
				port = Integer.valueOf(urlParts[1]);
			}
		}
		return port;
	}


	public static String removePortFromUrl(String url) {
		if (!StringUtils.isEmpty(url) && url.contains(":")) {
			String[] urlParts = url.split(":");
			return urlParts[0];
		}
		return url;
	}


	/**
	 * Puts/uploads a single file to a remote server over FTP.
	 *
	 * @param ftpConfig    specifying data necessary to initiate session with remote host
	 * @param remoteFolder the remote folder destination for the file
	 * @param file         the file to send
	 * @throws Exception if anything goes wrong, throws a {@link RuntimeException}
	 */
	public static void put(FtpConfig ftpConfig, String remoteFolder, FileWrapper file) {
		List<FileWrapper> fileList = new ArrayList<>();
		fileList.add(file);

		List<FtpFileResult> results = put(ftpConfig, remoteFolder, fileList);

		FtpFileResult firstFtpFileResult = CollectionUtils.getOnlyElement(results);
		if (firstFtpFileResult != null) {
			Throwable e = firstFtpFileResult.getError();
			if (e != null) {
				throw new RuntimeException("Failed to upload file through FTP", e);
			}
		}
	}


	/**
	 * Puts/uploads files to a remote server.
	 *
	 * @param ftpConfig    specifying data necessary to initiate session with remote host
	 * @param remoteFolder where the files are going to go
	 * @param fileList     specifying files to be sent/put
	 * @return List of {@link FtpFileResult} objects specifying success/failure of the upload per file.
	 */
	public static List<FtpFileResult> put(FtpConfig ftpConfig, String remoteFolder, List<FileWrapper> fileList) {
		LogUtils.debug(LogCommand.ofMessageSupplier(FtpUtils.class, () -> StringUtils.join(fileList, fileWrapper -> "FTP File: " + fileWrapper.getFileName(), "\n")));

		switch (ftpConfig.getFtpProtocol()) {
			default:
			case FTP:
			case FTPS:
				return putFiles(ftpConfig, remoteFolder, fileList);

			case SFTP:
				return putSftpFiles(ftpConfig, remoteFolder, fileList, ftpConfig.getSshPrivateKey(), ftpConfig.getSshPublicKey());
		}
	}


	/**
	 * Lists the files in the specified {@code remoteFolder}.
	 *
	 * @param ftpConfig       specifying data necessary to initiate session with remote host
	 * @param remoteFolder    where the files are to be listed from
	 * @param fileNamePattern regular expression pattern to match
	 * @return File names from the remote folder matching the specified {@code fileNamePattern}
	 */
	public static List<String> list(FtpConfig ftpConfig, String remoteFolder, String fileNamePattern) {
		List<FtpResult> results;

		switch (ftpConfig.getFtpProtocol()) {
			default:
			case FTP:
			case FTPS:
				results = listOrGetFtpFiles(ftpConfig, remoteFolder, fileNamePattern, null, false, null);
				break;
			case SFTP:
				results = listOrGetSftpFiles(ftpConfig, remoteFolder, fileNamePattern, null, false, null, ftpConfig.getSshPrivateKey());
				break;
		}
		return CollectionUtils.getStream(results).map(FtpResult::getFileName).collect(Collectors.toList());
	}


	/**
	 * Gets/downloads files from the remote server.
	 *
	 * @param ftpConfig         specifying data necessary to initiate session with remote host
	 * @param remoteFolder      from where the files are to be acquired
	 * @param fileNamePattern   to match the file names to get
	 * @param localFolder       where the downloaded files are going to go
	 * @param fileNameConverter
	 * @return List of {@link FtpFileResult} objects specifying success/failure of the get per file.
	 */
	public static List<FtpResult> get(FtpConfig ftpConfig, String remoteFolder, final String fileNamePattern, String localFolder, boolean writeExtension, Converter<String, String> fileNameConverter) {
		switch (ftpConfig.getFtpProtocol()) {
			default:
			case FTP:
			case FTPS:
				return listOrGetFtpFiles(ftpConfig, remoteFolder, fileNamePattern, localFolder, writeExtension, fileNameConverter);
			case SFTP:
				return listOrGetSftpFiles(ftpConfig, remoteFolder, fileNamePattern, localFolder, writeExtension, fileNameConverter, ftpConfig.getSshPrivateKey());
		}
	}


	public static boolean delete(FtpConfig ftpConfig, String remoteFolder, final String fileName, String privateKey) {
		switch (ftpConfig.getFtpProtocol()) {
			default:
			case FTP:
			case FTPS:
				return deleteFtpFile(ftpConfig, remoteFolder, fileName);
			case SFTP:
				deleteSftpFile(ftpConfig, remoteFolder, fileName, privateKey);
				return true;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////////                 private helper methods               ///////////
	////////////////////////////////////////////////////////////////////////////


	private static List<FtpFileResult> putFiles(FtpConfig ftpConfig, String remoteFolder, List<FileWrapper> fileList) {
		FTPClient client = FtpProtocols.FTPS == ftpConfig.getFtpProtocol() ? new FTPSClient() : new FTPClient();

		try {
			connectAndInitFtp(client, ftpConfig, remoteFolder);
		}
		catch (Throwable e) {
			logoutAndDisconnectFtp(client);
			throw new RuntimeException("Failed to establish FTP connection with [" + ftpConfig.getUrl() + "].", e);
		}

		try {
			List<FtpFileResult> resultList = new ArrayList<>();
			for (FileWrapper fileWrapper : CollectionUtils.getIterable(fileList)) {
				FileContainer file = fileWrapper.getFile().toFileContainer();
				FtpFileResult result = new FtpFileResult(fileWrapper);
				resultList.add(result);
				try (
						InputStream inputStream = file.getInputStream();
						BufferedInputStream fis = new BufferedInputStream(inputStream)
				) {
					//To make sure there are no encoding issues
					client.setFileType(FTP.BINARY_FILE_TYPE);

					client.storeFile(fileWrapper.getFileName(), fis);
				}
				catch (Throwable e) {
					result.setError(new RuntimeException("Failed to ftp [" + file.getName() + "].", e));
				}
				finally {
					if (fileWrapper.isTempFile()) {
						FileUtils.delete(file);
					}
				}
			}

			return resultList;
		}
		finally {
			logoutAndDisconnectFtp(client);
		}
	}


	private static void connectAndInitFtp(FTPClient client, FtpConfig ftpConfig, String remoteFolder) throws IOException {

		if (ftpConfig.getPort() != null) {
			client.connect(ftpConfig.getUrl(), ftpConfig.getPort());
		}
		else {
			client.connect(ftpConfig.getUrl());
		}

		//Note: Setting connection mode must be done after connecting, not before.
		if (ftpConfig.isActiveFtp()) {
			//Active mode is default in the Apache FTP client, but still setting it here in case that ever changes.
			client.enterLocalActiveMode();
		}
		else {
			client.enterLocalPassiveMode();
		}

		if (!client.login(ftpConfig.getUserName(), ftpConfig.getPassword())) {
			throw new RuntimeException("Failed to log into ftp server [" + ftpConfig.getUrl() + "].");
		}

		if (!StringUtils.isEmpty(remoteFolder)) {
			client.changeWorkingDirectory(remoteFolder);
		}
	}


	private static void logoutAndDisconnectFtp(FTPClient client) {
		try {
			client.logout();
		}
		catch (IOException e) {
			LogUtils.error(FtpUtils.class, "Failed to logout FTP session.", e);
		}
		try {
			client.disconnect();
		}
		catch (IOException e) {
			LogUtils.error(FtpUtils.class, "Failed to disconnect FTP session.", e);
		}
	}


	private static List<FtpFileResult> putSftpFiles(FtpConfig ftpConfig, String remoteFolder, List<FileWrapper> fileList, String privateKey, String publicKey) {
		List<FtpFileResult> resultList = new ArrayList<>();

		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp sftpChannel = null;

		try {
			if (!StringUtils.isEmpty(privateKey) || !StringUtils.isEmpty(publicKey)) {
				jsch.addIdentity(ftpConfig.getUserName(), !StringUtils.isEmpty(privateKey) ? privateKey.getBytes() : null, !StringUtils.isEmpty(publicKey) ? publicKey.getBytes() : null, new byte[0]);
			}

			if (ftpConfig.getPort() != null) {
				session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getUrl(), ftpConfig.getPort());
			}
			else {
				session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getUrl());
			}
			session.setConfig("StrictHostKeyChecking", "no");
			session.setPassword(ftpConfig.getPassword());
			session.setTimeout(60000);
			session.connect(30000);

			channel = session.openChannel(FtpProtocols.SFTP.name().toLowerCase());
			channel.connect();
			sftpChannel = (ChannelSftp) channel;
			if (!StringUtils.isEmpty(remoteFolder)) {
				sftpChannel.cd(remoteFolder);
			}

			for (FileWrapper file : CollectionUtils.getIterable(fileList)) {
				FtpFileResult result = new FtpFileResult(file);
				resultList.add(result);

				uploadFileToServer(file, sftpChannel, result);
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed ftp files to [" + ftpConfig.getUrl() + "].", e);
		}
		finally {
			// close the sftp channel - probably closes the underlying channel as well but we'll try to close both
			closeSftpChannel(sftpChannel, channel, session);
		}
		return resultList;
	}


	/**
	 * @param ftpConfig         specifying data necessary to initiate session with remote host
	 * @param remoteFolder      from where the files are to be acquired
	 * @param fileNamePattern   to match the file names to get
	 * @param localFolder       where the downloaded files are going to go. If null/empty files are not downloaded, and only listed.
	 * @param fileNameConverter
	 * @return File names from the remote folder matching the specified {@code fileNamePattern}
	 */
	private static List<FtpResult> listOrGetFtpFiles(FtpConfig ftpConfig, String remoteFolder, final String fileNamePattern, String localFolder, boolean writeExtension, Converter<String, String> fileNameConverter) {
		FTPClient client = FtpProtocols.FTPS == ftpConfig.getFtpProtocol() ? new FTPSClient() : new FTPClient();

		try {
			if (ftpConfig.getPort() != null) {
				client.connect(ftpConfig.getUrl(), ftpConfig.getPort());
			}
			else {
				client.connect(ftpConfig.getUrl());
			}
			boolean successful = client.login(ftpConfig.getUserName(), ftpConfig.getPassword());
			if (!successful) {
				throw new RuntimeException("Could not login to FTP server");
			}

			Pattern p = Pattern.compile(fileNamePattern);
			FTPFileFilter filter = file -> {
				if (file.isFile()) {
					Matcher m = p.matcher(file.getName());
					if (m.find()) {
						return true;
					}
				}
				return false;
			};

			FTPFile[] fileList = client.listFiles(remoteFolder, filter);

			if (!StringUtils.isEmpty(localFolder)) {
				return download(client, fileList, localFolder, writeExtension, remoteFolder, fileNameConverter);
			}

			List<FtpResult> results = new ArrayList<>();

			for (FTPFile file : fileList) {
				//Add the file name to be returned in the list
				results.add(new FtpResult(file.getName()));
			}

			return results;
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed ftp files to [" + ftpConfig.getUrl() + "].", e);
		}
		finally {
			try {
				client.disconnect();
			}
			catch (IOException e) {
				LogUtils.error(FtpUtils.class, "Failed to disconnect FTP session.", e);
			}
		}
	}


	private static List<FtpResult> download(FTPClient client, FTPFile[] fileList, String localFolder, boolean writeExtension, String remoteFolder,
	                                        Converter<String, String> fileNameConverter) {
		List<FtpResult> results = new ArrayList<>();

		for (FTPFile file : fileList) {

			FtpResult ftpResult = new FtpResult(file.getName());
			String fileName = file.getName();
			if (fileNameConverter != null) {
				fileName = fileNameConverter.convert(fileName);
			}
			if (writeExtension) {
				fileName = fileName + "." + writeExtension;
			}

			FileContainer outputFileContainer = FileContainerFactory.getFileContainer(FileUtils.combinePath(localFolder, fileName));
			if (outputFileContainer.exists()) {
				ftpResult.setWarning(String.format("Replacing destination file [%s].", outputFileContainer.getAbsolutePath()));
			}
			try (
					OutputStream outputStream = outputFileContainer.getOutputStream();
					BufferedOutputStream bos = new BufferedOutputStream(outputStream)
			) {
				//
				// change directory if necessary
				//
				if (!StringUtils.isEmpty(remoteFolder)) {
					client.changeWorkingDirectory(remoteFolder);
				}

				//
				// Store file to server
				//
				boolean successful = client.retrieveFile(file.getName(), bos);
				if (!successful) {
					throw new RuntimeException(String.format("Could not retrieve file [%s] using path[%s]", file.getName(), remoteFolder));
				}
			}
			catch (Throwable e) {
				ftpResult.setError(e);
			}
			finally {
				removeWritingExtension(localFolder, writeExtension, fileName, ftpResult);
			}

			//Add the file name to be returned in the list
			results.add(ftpResult);
		}

		return results;
	}


	/**
	 * @param ftpConfig         specifying data necessary to initiate session with remote host
	 * @param remoteFolder      from where the files are to be acquired
	 * @param fileNamePattern   to match the file names to get
	 * @param localFolder       where the downloaded files are going to go. If null/empty files are not downloaded, and only listed.
	 * @param fileNameConverter Can be null/empty.
	 * @param privateKey        for security. Can be null/empty.
	 * @return File names in the remote folder matching the specified {@code fileNamePattern}
	 */
	private static List<FtpResult> listOrGetSftpFiles(FtpConfig ftpConfig, String remoteFolder, final String fileNamePattern, String localFolder, boolean writeFileExtension, Converter<String, String> fileNameConverter,
	                                                  String privateKey) {
		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp sftpChannel = null;

		if (!StringUtils.isEmpty(localFolder)) {
			FileUtils.createDirectory(localFolder);
		}

		List<FtpResult> results = new ArrayList<>();

		try {
			if (!StringUtils.isEmpty(privateKey)) {
				jsch.addIdentity(ftpConfig.getUserName(), privateKey.getBytes(), null, new byte[0]);
			}

			if (ftpConfig.getPort() != null) {
				session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getUrl(), ftpConfig.getPort());
			}
			else {
				session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getUrl());
			}

			session.setConfig("StrictHostKeyChecking", "no");

			if (!StringUtils.isEmpty(ftpConfig.getPassword())) {
				session.setPassword(ftpConfig.getPassword());
			}
			else {
				session.setPassword("");
			}

			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;

			if (!StringUtils.isEmpty(remoteFolder)) {
				sftpChannel.cd(remoteFolder);
			}

			@SuppressWarnings({"unchecked", "rawtypes"})
			List<ChannelSftp.LsEntry> vector = new ArrayList<>(sftpChannel.ls("*.*"));

			Pattern p = Pattern.compile(fileNamePattern);
			for (LsEntry entry : CollectionUtils.getIterable(vector)) {
				Matcher m = p.matcher(entry.getFilename());
				if (!m.find()) {
					continue;
				}
				if (!StringUtils.isEmpty(localFolder)) {
					// get
					results.add(queryServerForFiles(entry, fileNameConverter, sftpChannel, localFolder, writeFileExtension));
				}
				else {
					// list
					results.add(new FtpResult(entry.getFilename()));
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed download files from ftp server [" + ftpConfig.getUrl() + "].", e);
		}
		finally {
			// close the sftp channel - probably closes the underlying channel as well but we'll try to close both
			closeSftpChannel(sftpChannel, channel, session);
		}
		return results;
	}


	private static FtpResult queryServerForFiles(LsEntry entry, Converter<String, String> fileNameConverter, ChannelSftp sftpChannel, String localFolder, boolean writeExtension) {
		FtpResult result = new FtpResult(entry.getFilename());

		String fileName = entry.getFilename();
		if (fileNameConverter != null) {
			fileName = fileNameConverter.convert(fileName);
		}
		if (writeExtension) {
			fileName = fileName + "." + WRITE_FILE_EXTENSION;
		}
		FileContainer outputFileContainer = FileContainerFactory.getFileContainer(FileUtils.combinePath(localFolder, fileName));
		if (outputFileContainer.exists()) {
			result.setWarning(String.format("Replacing destination file [%s].", outputFileContainer.getAbsolutePath()));
		}
		try (
				OutputStream outputStream = outputFileContainer.getOutputStream();
				BufferedOutputStream bos = new BufferedOutputStream(outputStream)
		) {
			sftpChannel.get(entry.getFilename(), bos);
		}
		catch (Exception e) {
			result.setError(e);
		}
		finally {
			removeWritingExtension(localFolder, writeExtension, fileName, result);
		}
		return result;
	}


	private static void removeWritingExtension(String localFolder, boolean writeExtension, String fileName, FtpResult result) {
		if (result.isSuccess() && writeExtension) {
			String writeExtensionFileName = FileUtils.combinePath(localFolder, fileName);
			FileContainer writeFileContainer = FileContainerFactory.getFileContainer(writeExtensionFileName);
			FileContainer renamedFileContainer = FileContainerFactory.getFileContainer(FileUtils.getFileNameWithoutExtension(writeExtensionFileName));
			if (renamedFileContainer.exists()) {
				result.setWarning(String.format("Deleted existing file [%s] to enable renaming [%s].", renamedFileContainer.getName(), writeExtensionFileName));
				try {
					renamedFileContainer.deleteFile();
				}
				catch (IOException e) {
					result.setError(e);
				}
			}
			if (result.isSuccess()) {
				boolean success = writeFileContainer.renameToFile(renamedFileContainer);
				if (!success) {
					result.setError(new RuntimeException(String.format("Could not rename file [%s] to [%s].", writeExtensionFileName, renamedFileContainer.getName())));
				}
			}
		}
	}


	/**
	 * Deletes a specified file from the an FTP server.
	 *
	 * @param ftpConfig    - a class specifying data necessary to initiate session with remote host
	 * @param remoteFolder - the folder to wherein the file is located (optional -- leave null if file is in current FTP working directory)
	 * @param filename     - filename to delete.
	 * @return true if successful, false on failure to delete file
	 */
	private static boolean deleteFtpFile(FtpConfig ftpConfig, String remoteFolder, final String filename) {
		FTPClient client = FtpProtocols.FTPS == ftpConfig.getFtpProtocol() ? new FTPSClient() : new FTPClient();

		try {
			if (ftpConfig.getPort() != null) {
				client.connect(ftpConfig.getUrl(), ftpConfig.getPort());
			}
			else {
				client.connect(ftpConfig.getUrl());
			}
			client.login(ftpConfig.getUserName(), ftpConfig.getPassword());

			if (!StringUtils.isEmpty(remoteFolder)) {
				client.changeWorkingDirectory(remoteFolder);
			}

			return client.deleteFile(filename);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed ftp files to [" + ftpConfig.getUrl() + "].", e);
		}
		finally {
			try {
				client.disconnect();
			}
			catch (IOException e) {
				LogUtils.error(FtpUtils.class, "Failed to disconnect FTP session.", e);
			}
		}
	}


	/**
	 * Deletes a specified file from an SFTP server.
	 *
	 * @param ftpConfig    - a class specifying data necessary to initiate session with remote host
	 * @param remoteFolder - the folder to wherein the file is located (optional -- leave null if file is in current FTP working directory)
	 * @param filename     - filename to delete.
	 * @param privateKey   - an optional key used to for authentication when establishing the SFTP connection.
	 */
	private static void deleteSftpFile(FtpConfig ftpConfig, String remoteFolder, String filename, String privateKey) {
		JSch jsch = new JSch();
		Session session = null;
		Channel channel = null;
		ChannelSftp sftpChannel = null;

		try {
			if (!StringUtils.isEmpty(privateKey)) {
				jsch.addIdentity(ftpConfig.getUserName(), privateKey.getBytes(), null, new byte[0]);
			}

			if (ftpConfig.getPort() != null) {
				session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getUrl(), ftpConfig.getPort());
			}
			else {
				session = jsch.getSession(ftpConfig.getUserName(), ftpConfig.getUrl());
			}

			session.setConfig("StrictHostKeyChecking", "no");

			if (!StringUtils.isEmpty(ftpConfig.getPassword())) {
				session.setPassword(ftpConfig.getPassword());
			}
			else {
				session.setPassword("");
			}

			session.connect();
			channel = session.openChannel("sftp");
			channel.connect();
			sftpChannel = (ChannelSftp) channel;

			if (!StringUtils.isEmpty(remoteFolder)) {
				sftpChannel.cd(remoteFolder);
			}

			sftpChannel.rm(filename);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed delete file from ftp server [" + ftpConfig.getUrl() + "].", e);
		}
		finally {
			// close the sftp channel - probably closes the underlying channel as well but we'll try to close both
			closeSftpChannel(sftpChannel, channel, session);
		}
	}


	private static void closeSftpChannel(ChannelSftp sftpChannel, Channel channel, Session session) {
		try {
			if (sftpChannel != null) {
				sftpChannel.exit();
				sftpChannel.disconnect();
			}
		}
		catch (Throwable e) {
			LogUtils.error(FtpUtils.class, "Failed exit SFTP channel.", e);
		}
		// close the channel
		try {
			if (channel != null) {
				channel.disconnect();
			}
		}
		catch (Throwable e) {
			LogUtils.error(FtpUtils.class, "Failed to disconnect SFTP session.", e);
		}
		// close the session
		try {
			if (session != null) {
				session.disconnect();
			}
		}
		catch (Throwable e) {
			LogUtils.error(FtpUtils.class, "Failed to disconnect SFTP session.", e);
		}
	}


	private static void uploadFileToServer(FileWrapper file, ChannelSftp sftpChannel, FtpFileResult result) {
		FileContainer fileContainer = file.getFile().toFileContainer();
		try (
				InputStream inputStream = fileContainer.getInputStream();
				BufferedInputStream fis = new BufferedInputStream(inputStream)
		) {
			//
			// Store file to server
			//
			sftpChannel.put(fis, file.getFileName());
		}
		catch (Throwable e) {
			result.setError(new RuntimeException("Failed to ftp [" + fileContainer.getName() + "].", e));
		}
		finally {
			if (file.isTempFile()) {
				FileUtils.delete(file.getFile());
			}
		}
	}
}
