package com.clifton.core.util.status;

/**
 * A runtime exception that should be throw when a request is received to stop/terminate/interrupt a long running process.
 * The caller can catch this exception and provide special handling: clean up resources, etc.
 */
public class ProcessTerminationException extends RuntimeException {

	public ProcessTerminationException(String message) {
		super(message);
	}

}
