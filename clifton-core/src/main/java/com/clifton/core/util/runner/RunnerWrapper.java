package com.clifton.core.util.runner;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.runner.execution.BaseRunnerCompletionResult;
import com.clifton.core.util.runner.execution.RunnerCompletionResult;
import com.clifton.core.util.status.ProcessTerminationException;
import com.clifton.core.util.timer.TimerHandler;
import com.clifton.core.web.stats.SystemRequestStatsService;

import java.util.concurrent.Callable;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


/**
 * The <code>RunnerWrapper</code> is a wrapper around Runner.
 * It saves scheduling user and at run time, if there's no current user, uses the scheduling user as run as user.
 * It also is responsible for error logging and execution timing.
 *
 * @author mwacker
 */
public class RunnerWrapper implements Callable<RunnerCompletionResult> {

	private ContextHandler contextHandler;

	private SystemRequestStatsService systemRequestStatsService;
	private TimerHandler timerHandler;

	private final Runner runner;
	private final Object schedulingUser;
	private final Consumer<RunnerCompletionResult> resultCallback;


	public RunnerWrapper(Runner runner, Object schedulingUser, Consumer<RunnerCompletionResult> resultCallback) {
		this.runner = runner;
		this.runner.setRunnerState(Runner.RunnerStates.SCHEDULED);
		this.schedulingUser = schedulingUser;
		this.resultCallback = resultCallback;
	}


	@Override
	public RunnerCompletionResult call() {
		if (getTimerHandler() == null) {
			LogUtils.error(getClass(), "RunnerWrapper has not been properly initialized: timerHandler is null");
		}
		this.runner.setRunnerState(Runner.RunnerStates.RUNNING);
		BaseRunnerCompletionResult runResult = new BaseRunnerCompletionResult(this.runner);
		getTimerHandler().startTimer();

		// save current user for restoring after job execution
		Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);

		// for Run As User, use COALESCE(user from the Runner, Scheduling User, Current User)
		Object runAsUser = this.runner.getRunUser();
		if (runAsUser == null) {
			runAsUser = this.schedulingUser;
			if (runAsUser == null) {
				runAsUser = currentUser;
			}
			this.runner.setRunUser(runAsUser);
		}
		setContextHandlerUserBean(runAsUser);

		try {
			// run the job
			this.runner.run();
			runResult.complete();
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error running " + this.runner, e);
			runResult.completeWithError(e);
			if (e instanceof ProcessTerminationException && this.runner.getStatus() != null) {
				// clear termination request to enable proper Status object serialization
				this.runner.getStatus().setTerminationRequested(false);
			}
		}
		finally {
			// restore current user state to be the same a before the run
			setContextHandlerUserBean(currentUser);

			// stop the timer and record stats
			getTimerHandler().stopTimer();
			long nanoDuration = getTimerHandler().getDurationNano();
			runResult.setRunDuration(nanoDuration, TimeUnit.NANOSECONDS);
			getSystemRequestStatsService().saveSystemRequestStats(this.runner.getType(), this.runner.getTypeId(), runAsUser, nanoDuration, 0, 0);
			getTimerHandler().reset();
			if (this.resultCallback != null) {
				this.resultCallback.accept(runResult);
			}
			this.runner.setRunnerState(Runner.RunnerStates.COMPLETED);
		}
		return runResult;
	}


	private void setContextHandlerUserBean(Object user) {
		if (user == null) {
			getContextHandler().removeBean(Context.USER_BEAN_NAME);
		}
		else {
			getContextHandler().setBean(Context.USER_BEAN_NAME, user);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SystemRequestStatsService getSystemRequestStatsService() {
		return this.systemRequestStatsService;
	}


	public void setSystemRequestStatsService(SystemRequestStatsService systemRequestStatsService) {
		this.systemRequestStatsService = systemRequestStatsService;
	}


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}
}
