package com.clifton.core.util;


import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


/**
 * Implements a "rolling list". A rolling list has a maximum capacity, and
 * removes the oldest elements from the list to maintain this capacity.
 *
 * @param <T> The type if items that this list contains
 */
public class RollingList<T> implements List<T> {

	/**
	 * The items in this rolling list.
	 */
	private final List<T> items = Collections.synchronizedList(new ArrayList<>());

	/**
	 * The maximum capacity of this list.
	 */
	private final int capacity;
	/**
	 * Whether or not to add a fake empty item to the end of this list.
	 */
	private final boolean addEmpty;
	/**
	 * This list's position pointer.
	 */
	private int position = 0;
	/**
	 * The "empty" item to be added.
	 */
	private final T empty;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a new RollingList of the specified capacity.
	 *
	 * @param capacity The capacity of this list.
	 */
	public RollingList(final int capacity) {
		this.capacity = capacity;
		this.addEmpty = false;
		this.empty = null;
	}


	/**
	 * Creates a new RollingList of the specified capacity, with the specified
	 * "empty" element appended to the end.
	 *
	 * @param capacity The capacity of this list.
	 * @param empty    The "empty" element to be added
	 */
	public RollingList(final int capacity, final T empty) {
		this.capacity = capacity;
		this.addEmpty = true;
		this.empty = empty;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isEmpty() {
		return this.items.isEmpty();
	}


	@Override
	public int size() {
		return this.items.size();
	}


	@Override
	public T get(final int index) {
		return this.items.get(index);
	}


	@Override
	public boolean contains(final Object o) {
		return this.items.contains(o);
	}


	@Override
	public boolean containsAll(Collection<?> c) {
		return this.items.containsAll(c);
	}


	@Override
	public int indexOf(Object o) {
		return this.items.indexOf(o);
	}


	@Override
	public int lastIndexOf(Object o) {
		return this.items.lastIndexOf(o);
	}


	/**
	 * Adds the specified item to this list. If the list has reached its
	 * maximum capacity, this method will remove elements from the start of the
	 * list until there is sufficient room for the new element.
	 */
	@Override
	public boolean add(T e) {
		boolean result = this.items.add(e);
		ensureCapacity();
		return result;
	}


	@Override
	public void add(int index, T element) {
		this.items.add(index, element);
		ensureCapacity();
	}


	@Override
	public boolean addAll(Collection<? extends T> c) {
		boolean result = this.items.addAll(c);
		ensureCapacity();
		return result;
	}


	@Override
	public boolean addAll(int index, Collection<? extends T> c) {
		boolean result = this.items.addAll(index, c);
		ensureCapacity();
		return result;
	}


	@Override
	public T set(int index, T element) {
		return this.items.set(index, element);
	}


	@Override
	public boolean remove(final Object o) {
		boolean result = this.items.remove(o);
		ensurePosition();
		return result;
	}


	@Override
	public T remove(int index) {
		T result = this.items.remove(index);
		ensurePosition();
		return result;
	}


	@Override
	public boolean removeAll(Collection<?> c) {
		boolean result = this.items.removeAll(c);
		ensurePosition();
		return result;
	}


	@Override
	public boolean retainAll(Collection<?> c) {
		boolean result = this.items.retainAll(c);
		ensurePosition();
		return result;
	}


	@Override
	public void clear() {
		this.items.clear();
		ensurePosition();
	}


	@Override
	public List<T> subList(int fromIndex, int toIndex) {
		return this.items.subList(fromIndex, toIndex);
	}


	@Override
	public Iterator<T> iterator() {
		return this.items.iterator();
	}


	@Override
	public ListIterator<T> listIterator() {
		// Note: Calling #add on the ListIterator can cause the capacity constraint to be violated
		return this.items.listIterator();
	}


	@Override
	public ListIterator<T> listIterator(int index) {
		// Note: Calling #add on the ListIterator can cause the capacity constraint to be violated
		return this.items.listIterator(index);
	}


	@Override
	public Object[] toArray() {
		return this.items.toArray();
	}


	@SuppressWarnings("SuspiciousToArrayCall")
	@Override
	public <A> A[] toArray(A[] a) {
		return this.items.toArray(a);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Determines if there is an element after the positional pointer of the list.
	 *
	 * @return True if there is an element, false otherwise.
	 */
	public boolean hasNext() {
		return (this.items.size() > this.position + 1) || ((this.items.size() > this.position) && this.addEmpty);
	}


	/**
	 * Retrieves the element after the positional pointer of the list.
	 *
	 * @return The next element in the list
	 */
	public T getNext() {
		if (this.items.size() > this.position + 1 || !this.addEmpty) {
			return get(++this.position);
		}
		this.position++;
		return this.empty;
	}


	/**
	 * Determines if there is an element before the positional pointer of the list.
	 *
	 * @return True if there is an element, false otherwise.
	 */
	public boolean hasPrevious() {
		return 0 < this.position;
	}


	/**
	 * Retrieves the element before the positional pointer of the list.
	 *
	 * @return The previous element in the list
	 */
	public T getPrevious() {
		return get(--this.position);
	}


	/**
	 * Sets the positional pointer of this list to the end.
	 */
	public void seekToEnd() {
		this.position = this.items.size();
	}


	/**
	 * Sets the positional pointer of this list to the start.
	 */
	public void seekToStart() {
		this.position = 0;
	}


	/**
	 * Retrieves a list of items that this rolling list contains.
	 *
	 * @return A list of items in this rolling list.
	 */
	public List<T> getList() {
		return new ArrayList<>(this.items);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Ensures that the list is not larger than the assigned {@link #capacity}, and removes items from the beginning of the list to match the capacity if so.
	 */
	private void ensureCapacity() {
		int excessItems = this.items.size() - this.capacity;
		if (excessItems > 0) {
			// Remove any excess items from the beginning of the backing list and adjust the cursor accordingly
			Iterator<T> listIterator = this.items.iterator();
			for (int remainingCnt = excessItems; remainingCnt > 0 && listIterator.hasNext(); remainingCnt--) {
				listIterator.next();
				listIterator.remove();
			}
			this.position -= excessItems;
			ensurePosition();
		}
	}


	private void ensurePosition() {
		this.position = Math.min(this.items.size(), Math.max(0, this.position));
	}
}
