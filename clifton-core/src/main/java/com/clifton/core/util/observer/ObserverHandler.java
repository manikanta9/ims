package com.clifton.core.util.observer;


import java.util.List;
import java.util.function.Predicate;


/**
 * The <code>ObserverHandler</code> interface defines methods for registering and notifying observers.
 *
 * @author vgomelsky
 */
public interface ObserverHandler {

	/**
	 * Returns the notifier for the specified event type, the notifier observer list will be filtered using the provided
	 * predicate.  Desired event (method) should be invoked on returned notifier.
	 * The notifier will call the specified method on all observers registered to handle this event.
	 */
	public <T extends Observer> T getNotifier(Class<T> eventType, Predicate<Observer> notifierExclusions);


	/**
	 * Registers the specified observer for the specified event.
	 * Returns true if the observer was registered and false if it was already registered (doesn't register twice).
	 * <p/>
	 * The methods supports ordering of observer invocations when {@link org.springframework.core.Ordered} interface is implemented.
	 */
	public <T extends Observer> boolean register(Class<T> eventType, T observer);


	/**
	 * Returns true if the specified observer is registered for the specified event
	 */
	public <T extends Observer> boolean isRegistered(Class<T> eventType, T observer);


	public List<Observer> getRegisteredObservers();

		/**
		 * Unregisters the specified observers for the specified event.  Returns unregistered observer or null.
		 */
		public <T extends Observer > T unregister(Class < T > eventType, T observer);
	}
