package com.clifton.core.util.encryption;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import javax.crypto.Cipher;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.Key;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.RSAPrivateKeySpec;
import java.security.spec.RSAPublicKeySpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;


/**
 * Utility Methods for dealing with RSA encryption/decryption and keys.
 *
 * @author theodorez
 */
public class EncryptionUtilsRSA {

	private static final Integer RSA_KEY_SIZE = 4096;

	private static final String KEY_ALGORITHM = "RSA";
	private static final String CIPHER_ALGORITHM = "RSA";


	/**
	 * Creates the key files if they do not already exist (and the checksums are null). If the files already exist the checksums are verified.
	 * <p>
	 * If successful returns a friendly string with the checksums for each file.
	 */
	public static void setupRSAEncryptionKeys(String publicKeyLocation, String publicKeyChecksum, String privateKeyLocation, String privateKeyChecksum) {
		ValidationUtils.assertNotEmpty(privateKeyLocation, "Cannot setup encryption keys. Private key location is empty.");
		ValidationUtils.assertNotEmpty(publicKeyLocation, "Cannot setup encryption keys. Public key location is empty.");

		File privateKeyFile = new File(privateKeyLocation);
		File publicKeyFile = new File(publicKeyLocation);

		if (!privateKeyFile.exists() || !publicKeyFile.exists()) {
			ValidationUtils.assertFalse(privateKeyFile.exists() && !publicKeyFile.exists(), "The private key file exists but the corresponding public key file cannot be found.");
			ValidationUtils.assertFalse(!privateKeyFile.exists() && publicKeyFile.exists(), "The public key file exists but the corresponding private key file cannot be found.");

			LogUtils.info(EncryptionUtilsRSA.class, "Created new random RSA keypair.");
			KeyPair keyPair = generateRandomRSAKeyPair();
			//Save the private key
			savePrivateKeyToFile(privateKeyFile, keyPair.getPrivate());
			privateKeyChecksum = getKeyFileChecksum(privateKeyFile);
			validateRSAKeyFile(privateKeyFile, privateKeyChecksum);
			//Save the public key
			savePublicKeyToFile(publicKeyFile, keyPair.getPublic());
			publicKeyChecksum = getKeyFileChecksum(publicKeyFile);
			validateRSAKeyFile(publicKeyFile, publicKeyChecksum);
		}
		else if (privateKeyFile.exists() && !StringUtils.isEmpty(privateKeyChecksum) && publicKeyFile.exists() && !StringUtils.isEmpty(publicKeyChecksum)) {
			validateRSAKeyFile(privateKeyFile, privateKeyChecksum);
			validateRSAKeyFile(publicKeyFile, publicKeyChecksum);
		}
		LogUtils.info(LogCommand.ofMessage(EncryptionUtilsRSA.class,
				"Private Key File Path: ", privateKeyFile.getAbsolutePath(), "\n",
				"Private Key File Checksum: ", privateKeyChecksum, "\n",
				"Public Key File Path: ", publicKeyFile.getAbsolutePath(), "\n",
				"Public Key File Checksum: ", publicKeyChecksum)
		);
	}


	/**
	 * Returns a Base64 encoded string of the plaintext encrypted using the public key.
	 */
	public static String encryptValueWithRSAPublicKey(String plainText, String keyLocation, String keyChecksum) {
		ValidationUtils.assertNotEmpty(keyLocation, "Cannot encrypt value, public key location is not specified");
		File keyFile = new File(keyLocation);
		EncryptionUtilsRSA.validateRSAKeyFile(keyFile, keyChecksum);
		PublicKey publicKey = EncryptionUtilsRSA.readFileToRSAPublicKey(keyFile);
		return Base64.getEncoder().encodeToString(EncryptionUtilsRSA.encryptRSA(plainText.getBytes(StandardCharsets.UTF_8), publicKey));
	}


	/**
	 * Decrypts the Base64 encoded ciphertext using the specified private encryption key
	 */
	public static String decryptValueWithRSAPrivateKey(String cipherText, String keyLocation, String keyChecksum) {
		ValidationUtils.assertNotEmpty(keyLocation, "Cannot decrypt value, private key location is not specified");
		File keyFile = new File(keyLocation);
		EncryptionUtilsRSA.validateRSAKeyFile(keyFile, keyChecksum);
		PrivateKey key = EncryptionUtilsRSA.readFileToRSAPrivateKey(keyFile);
		return new String(EncryptionUtilsRSA.decryptRSA(Base64.getDecoder().decode(cipherText), key), StandardCharsets.UTF_8);
	}


	/**
	 * Generates a new RSA key pair
	 */
	public static KeyPair generateRandomRSAKeyPair() {
		try {
			KeyPairGenerator generator = KeyPairGenerator.getInstance(KEY_ALGORITHM);
			generator.initialize(RSA_KEY_SIZE);
			return generator.genKeyPair();
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred while generating RSA keypair", e);
		}
	}


	/**
	 * Returns a Base64 encoded string of the data encrypted with RSA using the specified key.
	 */
	public static byte[] encryptRSA(byte[] data, Key key) {
		ValidationUtils.assertTrue(StringUtils.isEqual(key.getAlgorithm(), KEY_ALGORITHM), "Can only get encrypt with " + KEY_ALGORITHM + " using a key generated for the " + KEY_ALGORITHM + " algorithm. Algorithm of current key: " + key.getAlgorithm());
		try {
			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			cipher.init(Cipher.ENCRYPT_MODE, key);
			return cipher.doFinal(data);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred while encrypting.", e);
		}
		finally {
			EncryptionUtils.cleanupByteArray(data);
		}
	}


	/**
	 * Returns a byte array of the data that was decrypted using the specified key
	 */
	public static byte[] decryptRSA(byte[] data, Key key) {
		ValidationUtils.assertTrue(StringUtils.isEqual(key.getAlgorithm(), KEY_ALGORITHM), "Can only get decrypt with " + KEY_ALGORITHM + " using a key generated for the " + KEY_ALGORITHM + " algorithm. Algorithm of current key: " + key.getAlgorithm());
		try {
			Cipher cipher = Cipher.getInstance(CIPHER_ALGORITHM);
			cipher.init(Cipher.DECRYPT_MODE, key);
			return cipher.doFinal(data);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred while encrypting.", e);
		}
		finally {
			EncryptionUtils.cleanupByteArray(data);
		}
	}


	/**
	 * Gets the checksum for the file as a hex string
	 */
	public static String getKeyFileChecksum(File file) {
		return EncryptionUtils.byteArrayToHexString(getKeyFileChecksumBytes(file));
	}


	/**
	 * Validates that the give key file produces the same checksum as the one specified
	 */
	public static void validateRSAKeyFile(File keyFile, String checksum) {
		if (!StringUtils.isEmpty(checksum)) {
			ValidationUtils.assertTrue(Arrays.equals(getKeyFileChecksumBytes(keyFile), EncryptionUtils.hexStringToByteArray(checksum)), "The given server key is invalid. The specified key does not match the checksum.");
		}
	}


	/**
	 * Gets the checksum for the file as a byte array
	 */
	private static byte[] getKeyFileChecksumBytes(File file) {
		return EncryptionUtilsAES.hashBytes(EncryptionUtils.readKeyFileToBytes(file));
	}


	private static void saveRSAKeyToFile(File file, BigInteger modulus, BigInteger exponent) {
		ObjectOutputStream oout = null;
		try {
			oout = new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(file)));
			oout.writeObject(modulus);
			oout.writeObject(exponent);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred while writing key file", e);
		}
		finally {
			if (oout != null) {
				try {
					oout.close();
				}
				catch (IOException ioe) {
					LogUtils.warn(EncryptionUtilsRSA.class, "Failed to close output stream when writing key file [" + file.getName() + "].", ioe);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts a PublicKey into a Base64 encoded String
	 */
	public static String getPublicKeyRSAAsString(PublicKey key) {
		byte[] encodedPublicKey = key.getEncoded();
		return Base64.getEncoder().encodeToString(encodedPublicKey);
	}


	/**
	 * Converts a given Base64 encoded String into a PublicKey
	 */
	public static PublicKey getPublicKeyRSAFromString(String key) {
		byte[] encodedPublicKey = Base64.getDecoder().decode(key);
		try {
			X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encodedPublicKey);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
			return factory.generatePublic(keySpec);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred getting the RSA public key.", e);
		}
	}


	/**
	 * Saves the given public key to the specified file
	 */
	public static void savePublicKeyToFile(File file, PublicKey key) {
		RSAPublicKeySpec publicKeySpec = getRSAPublicKeySpec(key);
		saveRSAKeyToFile(file, publicKeySpec.getModulus(), publicKeySpec.getPublicExponent());
	}


	/**
	 * Reads the given file into a PublicKey.
	 */
	public static PublicKey readFileToRSAPublicKey(File file) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
			BigInteger modulus = (BigInteger) ois.readObject();
			BigInteger exponent = (BigInteger) ois.readObject();
			return getPublicKeyRSA(modulus, exponent);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred while writing key file", e);
		}
		finally {
			if (ois != null) {
				try {
					ois.close();
				}
				catch (IOException ioe) {
					LogUtils.warn(EncryptionUtilsRSA.class, "Failed to close input stream when reading key file [" + file.getName() + "].", ioe);
				}
			}
		}
	}


	/**
	 * Gets the RSA public key given the modulus and exponent
	 */
	private static PublicKey getPublicKeyRSA(BigInteger modulus, BigInteger exponent) {
		try {
			RSAPublicKeySpec keySpec = new RSAPublicKeySpec(modulus, exponent);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
			return factory.generatePublic(keySpec);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred getting the RSA public key.", e);
		}
	}


	private static RSAPublicKeySpec getRSAPublicKeySpec(PublicKey key) {
		ValidationUtils.assertTrue(StringUtils.isEqual(key.getAlgorithm(), KEY_ALGORITHM), "Can only get RSAPublicKeySpec from a public key generated using the " + KEY_ALGORITHM + " algorithm.");
		try {
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
			return factory.getKeySpec(key, RSAPublicKeySpec.class);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred getting the RSA private key.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Saves the private key to the file
	 */
	public static void savePrivateKeyToFile(File keyFile, PrivateKey privateKey) {
		RSAPrivateKeySpec privateKeySpec = getRSAPrivateKeySpec(privateKey);
		saveRSAKeyToFile(keyFile, privateKeySpec.getModulus(), privateKeySpec.getPrivateExponent());
	}


	public static PrivateKey readFileToRSAPrivateKey(File file) {
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new BufferedInputStream(new FileInputStream(file)));
			BigInteger modulus = (BigInteger) ois.readObject();
			BigInteger exponent = (BigInteger) ois.readObject();
			return getPrivateKeyRSA(modulus, exponent);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred while reading key file: " + file.getAbsolutePath(), e);
		}
		finally {
			if (ois != null) {
				try {
					ois.close();
				}
				catch (IOException ioe) {
					LogUtils.warn(EncryptionUtilsRSA.class, "Failed to close input stream when reading key file [" + file.getName() + "].", ioe);
				}
			}
		}
	}


	/**
	 * Gets the RSA private key given the modulus and exponent
	 */
	private static PrivateKey getPrivateKeyRSA(BigInteger modulus, BigInteger exponent) {
		try {
			RSAPrivateKeySpec keySpec = new RSAPrivateKeySpec(modulus, exponent);
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
			return factory.generatePrivate(keySpec);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred getting the RSA private key.", e);
		}
	}


	private static RSAPrivateKeySpec getRSAPrivateKeySpec(PrivateKey key) {
		ValidationUtils.assertTrue(StringUtils.isEqual(key.getAlgorithm(), KEY_ALGORITHM), "Can only get RSAPrivateKeySpec from a private key generated using the " + KEY_ALGORITHM + " algorithm.");
		try {
			KeyFactory factory = KeyFactory.getInstance(KEY_ALGORITHM);
			return factory.getKeySpec(key, RSAPrivateKeySpec.class);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred getting the RSA private key.", e);
		}
	}
}
