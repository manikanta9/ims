package com.clifton.core.util.event;


/**
 * The <code>EventHandler</code> interface provides a specification for relaying events to appropriate
 * EventListeners.
 *
 * @author rbrooks
 */
public interface EventHandler {

	/**
	 * Notifies appropriate {@link EventListener}s that an event has been raised. Each listener
	 * listening for the event's name, is given a chance to process the event.
	 *
	 * @param event The event being raised.
	 */
	public <E extends Event<?, ?>> boolean raiseEvent(E event);


	/**
	 * Registers {@link EventListener}s for specific events based on the event name(s) the
	 * listener supports.
	 *
	 * @param listener the EventListener to be registered
	 * @return false if the listener was already registered
	 * @see EventListener#getEventNameList()
	 * @see EventListener#getEventName()
	 */
	public <E extends Event<?, ?>> boolean registerEventListener(EventListener<E> listener);
}
