package com.clifton.core.util.http;


import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.concurrent.NamedThreadFactory;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

import java.io.IOException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;


public class SimpleHttpClientFactory {

	// static HTTP client for reuse in the JVM
	private static final SimpleHttpClient HTTP_CLIENT;
	// static executor for cleaning up idle http client connections
	private static final ScheduledExecutorService IDLE_CONNECTION_EXECUTOR;


	static {
		int defaultTimeout = 90 * 1000; // 90 seconds
		RequestConfig config = RequestConfig.custom()
				.setConnectTimeout(defaultTimeout)
				.setConnectionRequestTimeout(defaultTimeout)
				.build();

		PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager();
		// Max total connections
		connectionManager.setMaxTotal(100);
		// Max connections per route (host:port)
		connectionManager.setDefaultMaxPerRoute(100);

		CloseableHttpClient httpClient = HttpClients.custom()
				.setDefaultRequestConfig(config)
				.setConnectionManager(connectionManager)
				.build();
		HTTP_CLIENT = new SimpleHttpClientImpl(httpClient);

		// schedule a task to run every ten minutes that closes connections that have been idle for 5 minutes
		IDLE_CONNECTION_EXECUTOR = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("HTTP Client Idle Connection Scanner"));
		// HttpClient example shows an idle connection thread with these values to clean up expired and idle connections.
		// The server side can close connections if there is nothing to read when using blocking IO. The client must
		// check for a stale connection and avoid using to avoid getting connection errors.
		IDLE_CONNECTION_EXECUTOR.scheduleWithFixedDelay(() -> {
			connectionManager.closeExpiredConnections();
			connectionManager.closeIdleConnections(30, TimeUnit.SECONDS);
		}, 5, 5, TimeUnit.SECONDS);

		// Add shutdown hook to close the connection manager, http client, and shutdown the executor service
		Runtime.getRuntime().addShutdownHook(new Thread(() -> {
			connectionManager.close();
			try {
				httpClient.close();
			}
			catch (IOException e) {
				//Ignore
			}
			ConcurrentUtils.shutdownExecutorService(IDLE_CONNECTION_EXECUTOR);
		}));
	}


	/**
	 * Returns a singleton {@link SimpleHttpClient} for reuse within the current JVM.
	 */
	public static SimpleHttpClient getHttpClient() {
		return HTTP_CLIENT;
	}


	public static void close() {
		getHttpClient().close();
		ConcurrentUtils.shutdownExecutorService(SimpleHttpClientFactory.IDLE_CONNECTION_EXECUTOR);
	}
}
