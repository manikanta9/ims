package com.clifton.core.util.matching;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.NoSuchElementException;


/**
 * The <code>Permutation</code> provides a way to iterate through all possible combinations of a list.
 * <p/>
 * For example, with inputs:
 * list = {a,b,c,d}
 * maxCombinationSize = 3
 * minCombinationSize = 1
 * <p/>
 * The output is:
 * list		BITS	INT
 * {a}		0001	1
 * {b}		0010	2
 * {ab}		0011	3
 * {c}		0100	4
 * {ac}		0101	5
 * {bc}		0110	6
 * {abc}	0111	7
 * {d}		1000	8
 * {ad}		1001	9
 * {bd}		1010	10
 * {abd}	1011	11
 * {cd}		1100	12
 * {acd}	1101	13
 * {bcd}	1110	14
 * <p/>
 * <p/>
 * NOTE: Because this implementation checks each integer index for the number of bits that are set,
 * it can be very slow to find the next combination for lists with more than 20 elements.  So for larger list,
 * try to set it up to hit a correct combination in the first few calls.  If this becomes an issue, we should
 * use the combinationSizeList and instead looping from startIndex to minCombinationSize only loop through
 * the values known to have the correct bit counts.
 *
 * @param <T>
 * @author mwacker
 */
public class Permutation<T> implements Iterable<List<T>> {

	/**
	 * This list of current indices.
	 */
	private int[] currentIndexList;
	/**
	 * The list of items to be combined.
	 */
	private List<T> list;
	/**
	 * Max combination size.
	 */
	private int maxCombinationSize = 0; // 0 to use the size of the list
	/**
	 * Min combination size.
	 */
	private int minCombinationSize = 1;
	/**
	 * If populated it's used instead of maxCombinationSize and minCombinationSize, and provides a list
	 * of combination sizes.
	 */
	private List<Integer> combinationSizeList;
	/**
	 * Stores a list of scores that have been visited.
	 */
	private List<Integer> visitedScores = new ArrayList<>();

	/**
	 * Used to get a score for each permutation.  If implemented, this will prevent more than 1 permutation of
	 * the same score from being processed.  For example, if you have 10 permutations, 5 have unique scores and 5 have the same
	 * then 6 permutations will be processed (the 5 unique ones and 1 or the 5 with the same score.
	 */
	private PermutationScorer<T> permutationScorer;

	/**
	 * The number of possible combinations
	 */
	private BigInteger numberOfPermutations;
	/**
	 * The calculated sum of the indices that represents the last permutation.
	 */
	private Integer maxIndexTotal;


	public Permutation() {
		//
	}


	public Permutation(List<T> values) {
		this(values, values.size(), 1, null);
	}


	public Permutation(List<T> values, PermutationScorer<T> permutationScorer) {
		this(values, values.size(), 1, permutationScorer);
	}


	public Permutation(List<T> values, List<Integer> combinationSizeList) {
		this(values, values.size(), 1, null);
		this.combinationSizeList = combinationSizeList;
	}


	public Permutation(List<T> values, Integer maxSelectionSize) {
		this(values, maxSelectionSize, 1, null);
	}


	public Permutation(List<T> values, Integer maxSelectionSize, Integer minSelectionSize) {
		this(values, maxSelectionSize, minSelectionSize, null);
	}


	public Permutation(List<T> values, Integer maxSelectionSize, Integer minSelectionSize, PermutationScorer<T> permutationScorer) {
		this.list = values;
		this.maxCombinationSize = maxSelectionSize == null ? values.size() : maxSelectionSize;
		this.minCombinationSize = minSelectionSize == null ? 1 : minSelectionSize;

		if (this.maxCombinationSize > values.size()) {
			this.maxCombinationSize = values.size();
		}
		this.permutationScorer = permutationScorer;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BigInteger getCombinationCount() {
		if (this.numberOfPermutations == null) {
			calculateCombinations();
		}
		return this.numberOfPermutations;
	}


	public void reset() {
		this.currentIndexList = null;
		this.visitedScores = new ArrayList<>();
	}


	@Override
	public Iterator<List<T>> iterator() {
		return new Itr();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void calculateCombinations() {
		//get the number of combinations as if minSelectionSize is 1
		BigInteger n = BigInteger.valueOf(getList().size());
		BigInteger factorN = MathUtils.factorial(n);

		// get the actual number of combinations
		BigInteger count = BigInteger.ZERO;
		if (getCombinationSizeList() == null) {
			for (long k = this.minCombinationSize; k <= this.maxCombinationSize; k++) {
				BigInteger bk = BigInteger.valueOf(k);
				BigInteger d = MathUtils.factorial(bk).multiply(MathUtils.factorial(n.subtract(bk)));
				count = count.add(factorN.divide(d));
			}
		}
		else {
			this.minCombinationSize = getCombinationSizeList().get(0);
			for (Integer k : CollectionUtils.getIterable(getCombinationSizeList())) {
				if (this.minCombinationSize > k) {
					this.minCombinationSize = k;
				}
				BigInteger bk = BigInteger.valueOf(k);
				BigInteger d = MathUtils.factorial(bk).multiply(MathUtils.factorial(n.subtract(bk)));
				count = count.add(factorN.divide(d));
			}
		}
		this.numberOfPermutations = count;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private class Itr implements Iterator<List<T>> {

		@Override
		public boolean hasNext() {
			if (CollectionUtils.isEmpty(getList())) {
				return false;
			}
			if (Permutation.this.maxIndexTotal == null) {
				initCombinations();
			}
			if ((Permutation.this.currentIndexList != null) && (Permutation.this.currentIndexList.length == Permutation.this.maxCombinationSize)) {
				int indexTotal = 0;
				for (int index : Permutation.this.currentIndexList) {
					indexTotal += index;
				}
				if (indexTotal >= Permutation.this.maxIndexTotal) {
					return false;
				}
			}
			return true;
		}


		@Override
		public List<T> next() {
			if (!hasNext()) {
				throw new NoSuchElementException();
			}

			if (Permutation.this.maxIndexTotal == null) {
				initCombinations();
			}
			List<T> result = null;
			Permutation.this.currentIndexList = PermutationUtils.getNextIndexList(Permutation.this.currentIndexList, Permutation.this.minCombinationSize, Permutation.this.maxCombinationSize);

			boolean visited = false;
			Integer score = null;
			if (getPermutationScorer() != null) {
				score = getScore(Permutation.this.currentIndexList);
				visited = Permutation.this.visitedScores.contains(score);
			}
			if (!visited) {
				result = getCombinationList(Permutation.this.currentIndexList);
				if (score != null) {
					Permutation.this.visitedScores.add(score);
				}
			}

			return result;
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * Gets the number of combinations
		 */
		private void initCombinations() {
			Permutation.this.maxIndexTotal = 0;
			for (int i = getList().size() - 1; i >= getList().size() - Permutation.this.maxCombinationSize; i--) {
				Permutation.this.maxIndexTotal += i;
			}
		}


		private int getScore(int[] indexList) {
			if (getPermutationScorer() != null) {
				return getPermutationScorer().score(getCombinationList(indexList));
			}
			return 0;
		}


		private List<T> getCombinationList(int[] indexList) {
			List<T> result = new LinkedList<>();
			for (int indexItem : indexList) {
				result.add(getList().get(indexItem));
			}
			return result;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<T> getList() {
		return this.list;
	}


	public void setList(List<T> list) {
		this.list = list;
	}


	public int getMaxCombinationSize() {
		return this.maxCombinationSize;
	}


	public void setMaxCombinationSize(int maxSelectionSize) {
		this.maxCombinationSize = maxSelectionSize;
	}


	public int getMinCombinationSize() {
		return this.minCombinationSize;
	}


	public void setMinCombinationSize(int minSelectionSize) {
		this.minCombinationSize = minSelectionSize;
	}


	public List<Integer> getCombinationSizeList() {
		return this.combinationSizeList;
	}


	public void setCombinationSizeList(List<Integer> selectionSizes) {
		this.combinationSizeList = selectionSizes;
	}


	public PermutationScorer<T> getPermutationScorer() {
		return this.permutationScorer;
	}


	public void setPermutationScorer(PermutationScorer<T> permutationScorer) {
		this.permutationScorer = permutationScorer;
	}
}
