package com.clifton.core.util.timer;


import java.util.Set;


/**
 * The <code>TimerHandler</code> interface defines methods for timing execution
 *
 * @author vgomelsky
 */
public interface TimerHandler {

	/**
	 * Starts the main timer: must be called before stop or task start/stop.
	 */
	public void startTimer();


	/**
	 * Stops the main timer and calculates total duration.  Must be called after stop.
	 */
	public void stopTimer();


	/**
	 * Starts the task with the specified name that is timed within the main timer execution.
	 * A task with the same name can be executed multiple times within a run.
	 * Cumulative duration will be calculated as well as the number of invocations.
	 * Nested executions of tasks with the same name are not permitted.
	 *
	 * @param taskName
	 */
	public void startTimerTask(String taskName);


	/**
	 * Stops the task with the specified name that is time within the main timer execution.
	 *
	 * @param taskName
	 */
	public void stopTimerTask(String taskName);


	/**
	 * Resets the Timer and makes it available for new execution.
	 */
	public void reset();


	/**
	 * Returns duration of current run (end - start) in nanoseconds.
	 */
	public long getDurationNano();


	/**
	 * Returns TimerTask from current run with the specified name. Returns null if not found.
	 *
	 * @param taskName
	 */
	public TimerTask getTimerTask(String taskName);


	/**
	 * Returns the names of TimerTask(s) that were started during current execution.
	 */
	public Set<String> getTaskNames();
}
