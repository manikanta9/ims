package com.clifton.core.util.matching;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>ManyToManyMatcher</code> does a depth first search to find the first complete match with score of 0.
 * <p>
 * The implementation builds 2 permutation objects one for the external list and one for the internal list.  It
 * then loops through those permutations until it finds a valid sub match (score != -1 indicates a valid match).  It then
 * removes the matched items from the internal and external list, at this point if there are no items in one of the lists
 * it creates the final result, otherwise it recursively calls the matching again on the new smaller lists.
 *
 * @param <I>
 * @param <E>
 * @author mwacker
 */
public abstract class AbstractManyToManyMatcher<I, E> implements MatchingMatcher<I, E> {

	/**
	 * Retrieves the score of a match item.
	 *
	 * @param matchItem
	 */
	protected abstract int getMatchResultItemScore(MatchingResultItem<I, E> matchItem);


	/**
	 * Retrieves the score of a completed match set.
	 *
	 * @param match
	 */
	protected abstract int getMatchResultScore(MatchingResult<I, E> match);


	/**
	 * Used to score internal permutations in order to avoid re-visiting a given permutation.
	 */
	protected abstract PermutationScorer<I> getInternalPermutationScorer();


	/**
	 * Used to score external permutations in order to avoid re-visiting a given permutation.
	 */
	protected abstract PermutationScorer<E> getExternalPermutationScorer();


	/**
	 * Cache the matching results to avoid recalculating meta data.
	 *
	 * @param internalList
	 * @param externalList
	 */
	private MatchingResultItem<I, E> getCacheMatchingResultItemCache(List<I> internalList, List<E> externalList) {
		MatchingResultItem<I, E> ci = new MatchingResultItem<>(internalList, externalList);
		ci.setScore(getMatchResultItemScore(ci));
		return ci;
	}


	@Override
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList, List<Integer> externalCombinationSizes) {
		int targetPermutationSize = (externalList.size() >= internalList.size()) ? (externalList.size() / internalList.size()) : (internalList.size() / externalList.size());
		MatchingConfig<I, E> config = new MatchingConfig<>();
		config.setInternalList(internalList);
		config.setExternalList(externalList);
		config.setLevel(0);
		if (CollectionUtils.isEmpty(externalCombinationSizes)) {
			config.setTargetPermutationSize(targetPermutationSize * 2);
		}
		else {
			config.setTargetPermutationSize(externalCombinationSizes.get(0));
		}
		config.setExternalCombinationSizes(externalCombinationSizes);
		return doGetMatch(config);
	}


	@Override
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList) {
		return match(internalList, externalList, false);
	}


	private MatchingResult<I, E> match(List<I> internalList, List<E> externalList, boolean allowPartialMatch) {
		//int targetPermutationSize = externalList.size() >= internalList.size() ? externalList.size() / internalList.size() : internalList.size() / externalList.size();
		MatchingConfig<I, E> config = new MatchingConfig<>();
		config.setInternalList(internalList);
		config.setExternalList(externalList);
		config.setLevel(0);
		//config.setTargetPermutationSize(targetPermutationSize * 2);
		config.setAllowPartialMatch(allowPartialMatch);
		return doGetMatch(config);
	}


	@Override
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList, int combinationSize) {
		return match(internalList, externalList, combinationSize, false);
	}


	@Override
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList, int combinationSize, boolean allowPartialMatch) {
		MatchingConfig<I, E> config = new MatchingConfig<>();
		config.setInternalList(internalList);
		config.setExternalList(externalList);
		config.setLevel(0);
		config.setTargetPermutationSize(combinationSize);
		config.setForceCombinationSize(true);
		config.setAllowPartialMatch(allowPartialMatch);
		return doGetMatch(config);
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	private MatchingResult<I, E> doGetMatch(Permutation<?> p1, Permutation<?> p2, MatchingConfig<I, E> config, boolean p1Internal) {
		// recursively process each permutation, when a child branch is invalid stop processing
		for (List pp1 : p1) {
			for (List pp2 : p2) {
				MatchingResult<I, E> result = p1Internal ? doPermutation(pp1, pp2, config) : doPermutation(pp2, pp1, config);
				if (result != null) {
					return result;
				}
				enforceTimeLimit(config);
			}
			p2.reset();
		}
		return null;
	}


	/**
	 * Depth first search to find the correct matches.
	 */
	private MatchingResult<I, E> doGetMatch(MatchingConfig<I, E> config) {
		MatchingResult<I, E> result;

		Permutation<I> pt;
		Permutation<E> pe;
		Integer maxSize = config.getTargetPermutationSize();
		Integer minSize = config.isForceCombinationSize() ? maxSize : 1;
		// depending on which list is larger, flip the side on which the combination sizes are restricted and the order that they are processing in (smaller list first).
		if (config.getExternalList().size() >= config.getInternalList().size()) {
			if (!config.isForceCombinationSize() && getExternalPermutationScorer() != null && config.getInternalList().size() == 1) {
				maxSize = minSize = config.getExternalList().size();
			}

			// to narrow down the search restrict the permutations of the external on the first pass
			if (config.isForceCombinationSize()) {
				pt = new Permutation<>(config.getInternalList(), maxSize, minSize, getInternalPermutationScorer());
			}
			else {
				pt = new Permutation<>(config.getInternalList(), getInternalPermutationScorer());
			}
			if (config.isForceCombinationSize() || CollectionUtils.isEmpty(config.getExternalCombinationSizes())) {
				pe = new Permutation<>(config.getExternalList(), maxSize, minSize, getExternalPermutationScorer());
			}
			else {
				pe = new Permutation<>(config.getExternalList(), config.getTargetPermutationSize(), config.getTargetPermutationSize(), getExternalPermutationScorer());
			}

			// recursively process each permutation, when a child branch is invalid stop processing
			result = doGetMatch(pt, pe, config, true);
		}
		else {
			if (!config.isForceCombinationSize() && getInternalPermutationScorer() != null && config.getExternalList().size() == 1) {
				maxSize = minSize = config.getInternalList().size();
			}
			// to narrow down the search restrict the permutations of the external on the first pass
			pt = new Permutation<>(config.getInternalList(), maxSize, minSize, getInternalPermutationScorer());
			if (config.isForceCombinationSize() || CollectionUtils.isEmpty(config.getExternalCombinationSizes())) {
				pe = new Permutation<>(config.getExternalList(), maxSize, minSize, getExternalPermutationScorer());
			}
			else {
				pe = new Permutation<>(config.getExternalList(), config.getTargetPermutationSize(), config.getTargetPermutationSize(), getExternalPermutationScorer());
			}

			// recursively process each permutation, when a child branch is invalid stop processing
			result = doGetMatch(pe, pt, config, false);
		}
		return result;
	}


	private MatchingResult<I, E> doPermutation(List<I> internalPermutation, List<E> externalPermutation, MatchingConfig<I, E> config) {
		if ((internalPermutation == null) || (externalPermutation == null)) {
			return null;
		}
		MatchingResultItem<I, E> item = getCacheMatchingResultItemCache(internalPermutation, externalPermutation);
		if (item.getScore() == -1) {
			return null;
		}

		List<I> ip = CoreCollectionUtils.clone(config.getInternalList());
		ip.removeAll(item.getInternalList());

		List<E> ep = CoreCollectionUtils.clone(config.getExternalList());
		ep.removeAll(item.getExternalList());

		// if we are out of permutations in one of the lists, create the new combination and return it if the score equal 0
		if (ip.isEmpty() || ep.isEmpty()) {
			MatchingResult<I, E> combination = generateFinalMatch(item, config);
			if (combination.getScore() == 0) {
				return combination;
			}
		}
		else {
			enforceTimeLimit(config);

			// copy the current configuration to new one, and call recursively to get the remaining combinations
			MatchingConfig<I, E> newConfig = new MatchingConfig<>();
			if (config.getParentItemList() == null) {
				newConfig.setParentItemList(new ArrayList<>());
			}
			else {
				newConfig.setParentItemList(CoreCollectionUtils.clone(config.getParentItemList()));
			}
			newConfig.getParentItemList().add(item);
			newConfig.setLevel(config.getLevel() + 1);
			newConfig.setInternalList(ip);
			newConfig.setExternalList(ep);
			newConfig.setAllowPartialMatch(config.isAllowPartialMatch());
			newConfig.setExecutionStartTime(config.getExecutionStartTime());
			newConfig.setLongRunning(config.isLongRunning());
			if (config.isForceCombinationSize()) {
				newConfig.setForceCombinationSize(true);
				newConfig.setTargetPermutationSize(config.getTargetPermutationSize());
			}
			if (!CollectionUtils.isEmpty(config.getExternalCombinationSizes())) {
				newConfig.setExternalCombinationSizes(CoreCollectionUtils.clone(config.getExternalCombinationSizes()));
				newConfig.getExternalCombinationSizes().remove(config.getTargetPermutationSize());
				if (!CollectionUtils.isEmpty(newConfig.getExternalCombinationSizes())) {
					newConfig.setTargetPermutationSize(newConfig.getExternalCombinationSizes().get(0));
				}
			}
			MatchingResult<I, E> result;
			result = doGetMatch(newConfig);
			if (result != null) {
				return result;
			}
			else if (config.isAllowPartialMatch()) {
				return generateFinalMatch(item, config);
			}
		}
		return null;
	}


	private MatchingResult<I, E> generateFinalMatch(MatchingResultItem<I, E> item, MatchingConfig<I, E> config) {
		List<MatchingResultItem<I, E>> itemList;
		if (config.getParentItemList() != null) {
			itemList = CoreCollectionUtils.clone(config.getParentItemList());
		}
		else {
			itemList = new ArrayList<>();
		}
		itemList.add(item);
		MatchingResult<I, E> combination = new MatchingResult<>(itemList);
		combination.setScore(getMatchResultScore(combination));
		return combination;
	}


	private void enforceTimeLimit(MatchingConfig<I, E> config) {
		long secondsDiff = DateUtils.getSecondsDifference(new Date(), config.getExecutionStartTime());

		if (secondsDiff >= 15) {
			String message = secondsDiff + " seconds have passed for this matching run.\n Matcher Class: [" + this.getClass() + "],\n Execution Start Time: [" + config.getExecutionStartTime()
					+ "],\n Example Internal: [" + BeanUtils.describe(CollectionUtils.getFirstElement(config.getInternalList())) + "],\n Example External: ["
					+ BeanUtils.describe(CollectionUtils.getFirstElement(config.getExternalList())) + "]";

			// Log at first 15 seconds only
			if (!config.isLongRunning()) {
				config.setLongRunning(true);
				LogUtils.warn(getClass(), message);
			}

			if (secondsDiff >= 30) {
				LogUtils.error(getClass(), message);
				throw new ValidationException(message);
			}
		}
	}
}
