package com.clifton.core.util.http.cookie;

import com.clifton.core.util.CollectionUtils;
import org.apache.http.cookie.Cookie;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author NickK
 */
public class InMemoryCookieStore implements ImsCookieStore {

	private final Map<String, Cookie> cookieMap = new ConcurrentHashMap<>();


	@Override
	public void addCookie(Cookie cookie) {
		this.cookieMap.put(createKey(cookie), cookie);
	}


	@Override
	public List<Cookie> getCookies() {
		return new ArrayList<>(this.cookieMap.values());
	}


	@Override
	public boolean clearExpired(Date date) {
		boolean expired = false;
		Iterator<Map.Entry<String, Cookie>> cookieIterator = this.cookieMap.entrySet().iterator();
		while (cookieIterator.hasNext()) {
			Cookie cookie = cookieIterator.next().getValue();
			if (cookie.isExpired(date)) {
				cookieIterator.remove();
				expired = true;
			}
		}
		return expired;
	}


	@Override
	public void setCookieList(List<Cookie> cookieList) {
		this.cookieMap.clear();
		CollectionUtils.getStream(cookieList).forEach(cookie -> this.cookieMap.put(createKey(cookie), cookie));
	}


	@Override
	public void clear() {
		this.cookieMap.clear();
	}


	@Override
	public boolean containsCookie(String cookieName, String host) {
		return this.cookieMap.containsKey(createKey(cookieName, host));
	}


	@Override
	public Cookie getCookie(String cookieName, String host) {
		return this.cookieMap.get(createKey(cookieName, host));
	}


	private String createKey(Cookie cookie) {
		return createKey(cookie.getName(), cookie.getDomain());
	}


	private String createKey(String name, String host) {
		return name + ';' + host;
	}
}
