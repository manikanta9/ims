package com.clifton.core.util.xml;


import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Enumeration;


/**
 * The <code>XmlUtils</code> class contains utility methods for generating XML.
 *
 * @author vgomelsky
 */
public class XmlUtils {

	/**
	 * Appends the start or end XML tag to the specified builder.
	 *
	 * @param builder
	 * @param tagName  qualified tag name
	 * @param startTag
	 */
	public static void appendXmlTag(StringBuilder builder, String tagName, boolean startTag) {
		if (tagName == null || builder == null) {
			return;
		}
		builder.append('<');
		if (!startTag) {
			builder.append('/');
		}
		builder.append(tagName);
		builder.append('>');
	}


	/**
	 * Appends argument value to the argument StringBuilder. The value is CDATA escaped and is enclosed in the argument tag.
	 *
	 * @param builder
	 * @param value
	 * @param tagName
	 */
	public static void appendCData(StringBuilder builder, Object value, String tagName) {
		if (builder == null) {
			return;
		}
		if (tagName != null) {
			appendXmlTag(builder, tagName, true);
		}
		appendCData(builder, value == null ? "" : value.toString());
		if (tagName != null) {
			appendXmlTag(builder, tagName, false);
		}
	}


	/**
	 * Appends the XML data argument to the specified builder as CDATA;
	 * if the data has ]]>, it is split into two CDATA
	 * That is, if data is ]]>, returns <![CDATA[]]]]><![CDATA[>]]>
	 *
	 * @param builder
	 * @param xml
	 */
	public static void appendCData(StringBuilder builder, String xml) {
		if (xml == null || builder == null) {
			return;
		}
		if (!xml.contains("]]>")) {
			builder.append("<![CDATA[");
			builder.append(xml);
			builder.append("]]>");
		}
		else {
			int oldIndex = 0;
			int newIndex;
			while ((newIndex = xml.indexOf("]]>", oldIndex)) != -1) {
				builder.append("<![CDATA[");
				builder.append(xml.substring(oldIndex, newIndex + 2));
				builder.append("]]>");
				oldIndex = newIndex + 2;
			}
			builder.append("<![CDATA[");
			builder.append(xml.substring(oldIndex));
			builder.append("]]>");
		}
	}


	/**
	 * Returns XML representation of the HttpServletRequest object.
	 *
	 * @param request
	 * @param longMode
	 * @return XML representation of the HttpServletRequest.
	 */
	public static String requestToXML(HttpServletRequest request, boolean longMode) {
		String key = null;
		String className;
		Object value;

		try {
			StringBuilder result = new StringBuilder();

			result.append("<request time='");
			result.append(DateUtils.fromDate(new Date(System.currentTimeMillis())));
			result.append("' server='");
			result.append(request.getServerName());
			result.append("' client='");
			result.append(request.getRemoteAddr());
			result.append("' uri='");
			result.append(request.getRequestURI());
			result.append("'>\n");

			// get request parameters
			Enumeration<String> parameters = request.getParameterNames();
			if (parameters != null) {

				while (parameters.hasMoreElements()) {
					key = parameters.nextElement();
					result.append("<parameter name='");
					result.append(key);
					result.append("'>");
					String[] parameterValues = request.getParameterValues(key);
					if (parameterValues != null && parameterValues.length > 0) {
						if (parameterValues.length > 1) {
							for (String parameterValue : parameterValues) {
								result.append("<value>");
								appendCData(result, parameterValue);
								result.append("</value>");
							}
						}
						else {
							appendCData(result, parameterValues[0]);
						}
					}
					result.append("</parameter>\n");
				}
			}

			if (longMode) {
				// get request attributes
				Enumeration<String> attributes = request.getAttributeNames();
				if (attributes != null) {

					while (attributes.hasMoreElements()) {
						key = attributes.nextElement();
						value = request.getAttribute(key);
						if (value != null) {
							className = value.getClass().getName();
						}
						else {
							className = "null";
						}
						result.append("<attribute name='");
						result.append(key);
						result.append("' clazz='");
						result.append(className);
						result.append("'>");
						appendCData(result, value == null ? null : value.toString());
						result.append("</attribute>\n");
					}
				}
			}

			result.append("</request>\n");

			return result.toString();
		}
		catch (Exception e) {
			request.setAttribute("javax.servlet.jsp.jspException", e);
			return "<request>Error serializing request (" + key + "): <![CDATA[" + ExceptionUtils.toString(e) + "]]></request>\n";
		}
	}


	/**
	 * Returns the XML representation of the HttpSession object.
	 *
	 * @param session
	 * @return XML representation of the HttpSession.
	 */
	public static String sessionToXML(HttpSession session) {
		String key;
		String className;
		Object value;

		if (session == null) {
			return "";
		}

		try {
			StringBuilder result = new StringBuilder();
			result.append("<session id='");
			result.append(session.getId());
			result.append("'>\n");

			// have valid session - get an array of keys
			Enumeration<String> keys = session.getAttributeNames();
			if (keys != null) {
				while (keys.hasMoreElements()) {
					key = keys.nextElement();
					value = session.getAttribute(key);
					if (value != null) {
						className = value.getClass().getName();
					}
					else {
						className = "null";
					}
					result.append("<attribute name='");
					result.append(key);
					result.append("' clazz='");
					result.append(className);
					result.append("'>");
					if (value != null) {
						String stringValue = value.toString();
						if (!stringValue.isEmpty()) {
							if (stringValue.charAt(0) == '<' && stringValue.indexOf("<![CDATA[") == 0) {
								result.append(stringValue);
							}
							else {
								appendCData(result, stringValue);
							}
						}
					}
					result.append("</attribute>\n");
				}
			}
			result.append("</session>\n");

			return result.toString();
		}
		catch (Exception e) {
			return "<session>Error serializing session: <![CDATA[" + ExceptionUtils.toString(e) + "]]></session>\n";
		}
	}
}
