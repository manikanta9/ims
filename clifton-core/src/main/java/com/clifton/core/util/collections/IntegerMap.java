package com.clifton.core.util.collections;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;

import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;


public class IntegerMap<K> implements NumberMap<K, Integer> {

	private final Map<K, Integer> map = new LinkedHashMap<>();


	public void initialize(Integer value, K... keys) {
		for (K key : CollectionUtils.createList(keys)) {
			this.map.put(key, value);
		}
	}


	public void add(Integer value, K... keys) {
		for (K key : CollectionUtils.createList(keys)) {
			this.add(key, value);
		}
	}


	@Override
	public Integer add(K key, Integer value) {
		Integer newValue = Integer.sum(ObjectUtils.coalesce(this.get(key), 0), ObjectUtils.coalesce(value, 0));
		this.map.put(key, newValue);
		return newValue;
	}


	@Override
	public Integer subtract(K key, Integer value) {
		Integer newValue = Integer.sum(ObjectUtils.coalesce(this.get(key), 0), -ObjectUtils.coalesce(value, 0));
		this.map.put(key, newValue);
		return newValue;
	}


	@Override
	public Integer get(K key) {
		Integer value = this.map.get(key);
		if (value == null) {
			value = 0;
		}
		return value;
	}


	@Override
	public String toString() {
		return this.map.toString();
	}


	public String toString(Function<Map<K, Integer>, String> formatter) {
		return formatter.apply(Collections.unmodifiableMap(this.map));
	}
}
