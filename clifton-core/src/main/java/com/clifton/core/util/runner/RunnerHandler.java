package com.clifton.core.util.runner;


import java.util.Date;
import java.util.List;


/**
 * The <code>RunnerHandler</code> defines the methods that are used to run a schedule controller.  When the controller is
 * started it will call each of the schedule providers and schedule each instance to run at the appropriate time.
 *
 * @author mwacker
 */
public interface RunnerHandler {

	/**
	 * Start the schedule controller using the configured delay.
	 */
	public void start();


	/**
	 * Start the schedule controller with the specified delay.
	 */
	public void start(long delayMilliseconds);


	/**
	 * Stop the schedule controller. If softStop == true, then leaves running tasks and thread pool unaffected.
	 */
	public void stop(boolean softStop);


	/**
	 * Stop the schedule controller. Used by spring config to shut the runner down calls stop with softStop == false.
	 */
	public void stop();


	/**
	 * Get a value indicating if the controller is running (was started and not stopped).
	 */
	public boolean isRunning();


	/**
	 * Schedules the specified Runner to run immediately.
	 * The run is asynchronous so the method returns immediately as the Runner
	 * is scheduled to run immediately on a different thread.
	 */
	public void runNow(Runner runner);


	/**
	 * Reschedules all runners for all providers specified in the handler
	 */
	public void rescheduleRunnersForProviders();


	/**
	 * Schedules the specified Runner to run on the given run date.
	 * <p/>
	 * The run is asynchronous so the method returns immediately as the Runner
	 * is scheduled to run on a different thread.
	 * <p/>
	 * NOTE: If Runner is already scheduled for the same type & typeId will attempt to cancel all previously scheduled runs
	 * so the one pass is the only scheduled run.
	 */
	public void rescheduleRunner(Runner runner);


	/**
	 * Schedules the specified Runners to run on their given run dates. The list of runners may be intermixed of type and typeId.
	 * <p>
	 * NOTE: If Runners with the same type and typeId are already scheduled they will be canceled
	 */
	public void rescheduleRunners(List<Runner> runners);


	/**
	 * Adds the specified {@link RunnerProvider} to this controller.
	 * It will be used to retrieve {@link Runner}s scheduled for a given time period.
	 */
	public void addRunnerProvider(RunnerProvider<?> runnerProvider);


	/**
	 * Schedules a single runner.
	 */
	public void addRunner(Runner runner);


	/**
	 * Returns true if a {@link Runner} with the specified type and type ID is currently running.
	 */
	public boolean isRunnerWithTypeAndIdActive(String runnerType, String runnerTypeId);


	/**
	 * Cancels all scheduled runners with the given type.
	 * Cancels only canceled or completed tasks. If the task is running, waits for its completion.
	 */
	public void cancelRunners(String runnerType);


	/**
	 * Cancels all scheduled runners with the given type and typeId.
	 * Cancels only canceled or completed tasks. If the task is running, waits for its completion.
	 */
	public void cancelRunners(String runnerType, String runnerTypeId);


	/**
	 * Attempts to terminate all scheduled runners with the given type and typeId.
	 * Updates the Status of the runner to request termination: throw an exception on the next Status update.
	 * Usually works with runners that iterate over a large set of items and update the Status after each item is processed.
	 */
	public void terminateRunners(String runnerType, String runnerTypeId);


	/**
	 * Clears all completed or canceled tasks from the list of scheduled runners.
	 */
	public void clearCompletedRunners();


	/**
	 * Returns a List of currently scheduled {@link Runner}s of the specified type.
	 */
	public List<Runner> getScheduledRunnerList(String type);


	/**
	 * Returns a List of currently scheduled {@link Runner}s of the specified type and type id.
	 */
	public List<Runner> getScheduledRunnerList(String type, String typeId);


	/**
	 * Returns a List of currently scheduled {@link Runner}s.
	 */
	public List<Runner> getScheduledRunnerList();


	/**
	 * Returns the next Date and Time when the runner controller will look for and schedule new runners.
	 */
	public Date getNextScheduleDate();


	/**
	 * Returns the {@link RunnerHandlerStatus} for active {@link RunnerHandler}. This status object
	 * provides statistics and operational information to assist in monitoring and troubleshooting.
	 * <p>
	 * Optionally, a {@link RunnerHandlerStatusCommand} can be passed to rest statistics and return
	 * customized data.
	 */
	public RunnerHandlerStatus getStatus(RunnerHandlerStatusCommand command);


	/**
	 * Registers execution statistics monitoring for a Runner that matches criteria from the provided command.
	 * The command should be configured with a {@link Runner} or type and type ID (optional). If type is
	 * specified with no type ID, all Runners of type will have their execution statistics combined.
	 */
	public void registerExecutionStatusMonitoring(RunnerHandlerStatusCommand command);


	/**
	 * Removes the registration of execution statistics monitoring for a Runner that matches criteria from the
	 * provided command. The same criteria should be used to deregister that was used during registration.
	 */
	public void deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand command);
}
