package com.clifton.core.util.http;


import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.client.utils.URIUtils;

import java.net.URI;
import java.net.URISyntaxException;


public class RedirectStatusException extends HttpException {

	private final SimpleHttpResponse response;
	private URI redirectLocation = null; //lazily set


	public RedirectStatusException(SimpleHttpResponse response) {
		super(createMessage(response));

		this.response = response;
	}


	public URI getRedirectLocation() {

		if (this.redirectLocation == null) {
			HttpClientContext context = this.response.getContext();
			try {
				this.redirectLocation = URIUtils.resolve(this.response.getURI(), context.getTargetHost(), context.getRedirectLocations());
			}
			catch (URISyntaxException e) {
				throw new RuntimeException("A RedirectStatusException was thrown, but a URISyntaxException was thrown while attempting to retrieve the URI location. This should never happen.");
			}
		}

		return this.redirectLocation;
	}


	public int getStatusCode() {
		return this.response.getStatusCode();
	}


	public SimpleHttpResponse getResponse() {
		return this.response;
	}


	private static String createMessage(SimpleHttpResponse response) {
		return "A redirect status of " + response.getStatusCode() + " was received from the server and could not be handled automatically.";
	}
}
