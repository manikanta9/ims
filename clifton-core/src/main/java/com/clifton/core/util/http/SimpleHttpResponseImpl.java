package com.clifton.core.util.http;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.util.EntityUtils;

import java.io.Closeable;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;


class SimpleHttpResponseImpl implements SimpleHttpResponse {

	private final HttpResponse response;
	private final HttpClientContext context;
	private final URI uri;


	SimpleHttpResponseImpl(HttpResponse response, HttpClientContext context, URI uri) {
		this.response = response;
		this.context = context;
		this.uri = uri;
	}


	@Override
	public InputStream getContent() {
		try {
			return this.response.getEntity().getContent();
		}
		catch (IOException e) {
			throw new RuntimeException("Could not create input stream", e);
		}
	}


	@Override
	public HttpClientContext getContext() {
		return this.context;
	}


	@Override
	public HttpEntity getEntity() {
		return this.response.getEntity();
	}


	@Override
	public HttpResponse getHttpResponse() {
		return this.response;
	}


	@Override
	public int getStatusCode() {
		return this.response.getStatusLine().getStatusCode();
	}


	@Override
	public URI getURI() {
		return this.uri;
	}


	@Override
	public void close() throws IOException {
		EntityUtils.consumeQuietly(getEntity());
		if (this.response instanceof Closeable) {
			((Closeable) this.response).close();
		}
	}
}
