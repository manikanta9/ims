package com.clifton.core.util.runner.config;


import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.runner.Runner;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.RunnerHandlerStatus;
import com.clifton.core.util.runner.RunnerHandlerStatusCommand;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>RunnerConfigServiceImpl</code> class provides basic implementation of the RunnerConfigService interface.
 */
@Service
public class RunnerConfigServiceImpl implements RunnerConfigService {

	private RunnerHandler runnerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@ModelAttribute("data")
	@SecureMethod(securityResource = "Runner")
	public List<RunnerConfig> getRunnerConfigList(RunnerConfigSearchForm searchForm) {
		List<RunnerConfig> result = new ArrayList<>();

		List<Runner> runners = getRunnerHandler().getScheduledRunnerList();
		for (Runner runner : runners) {
			if ((searchForm.getType() != null) && !runner.getType().equals(searchForm.getType())) {
				continue;
			}
			if ((searchForm.getTypeId() != null) && !runner.getTypeId().equals(searchForm.getTypeId())) {
				continue;
			}
			if ((searchForm.getClassName() != null) && !runner.getTypeId().equals(searchForm.getTypeId())) {
				continue;
			}

			result.add(new RunnerConfig(runner));
		}

		return result;
	}


	@Override
	@SecureMethod(securityResource = "Runner")
	public Date getRunnerHandlerNextSchedulingDate() {
		return getRunnerHandler().getNextScheduleDate();
	}


	@Override
	@RequestMapping("runnerControllerReschedule")
	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void rescheduleRunnersFromProviders() {
		if (getRunnerHandler().isRunning()) {
			getRunnerHandler().rescheduleRunnersForProviders();
		}
	}


	@Override
	@RequestMapping("runnerControllerRestart")
	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void restartRunnerHandler() {
		if (getRunnerHandler().isRunning()) {
			getRunnerHandler().stop(true);
		}
		getRunnerHandler().start(0);
	}


	@Override
	@RequestMapping("runnerCompletedClear")
	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_READ)
	public void clearCompletedRunners() {
		getRunnerHandler().clearCompletedRunners();
	}


	@Override
	@RequestMapping("runnerTerminate")
	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void terminateRunners(String runnerType, String runnerTypeId) {
		getRunnerHandler().terminateRunners(runnerType, runnerTypeId);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_READ)
	@ModelAttribute("data")
	@Override
	public RunnerHandlerStatus getRunnerHandlerStatus(RunnerHandlerStatusCommand command) {
		return getRunnerHandler().getStatus(command);
	}


	@Override
	@RequestMapping("registerRunnerExecutionMonitoring")
	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void registerExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		getRunnerHandler().registerExecutionStatusMonitoring(command);
	}


	@Override
	@RequestMapping("deregisterRunnerExecutionMonitoring")
	@SecureMethod(securityResource = "Runner", permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		getRunnerHandler().deregisterExecutionStatusMonitoring(command);
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}
}
