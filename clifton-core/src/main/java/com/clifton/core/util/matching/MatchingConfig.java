package com.clifton.core.util.matching;


import java.util.Date;
import java.util.List;


public class MatchingConfig<I, E> {

	/**
	 * The list of internal items to match.
	 */
	private List<I> internalList;
	/**
	 * The list of external items to match.
	 */
	private List<E> externalList;
	private List<MatchingResultItem<I, E>> parentItemList;
	private int level;
	private Integer targetPermutationSize;
	private List<Integer> externalCombinationSizes;
	private boolean forceCombinationSize = false;
	private boolean allowPartialMatch = false;
	private Date executionStartTime = new Date();
	private boolean longRunning = false;


	public List<I> getInternalList() {
		return this.internalList;
	}


	public void setInternalList(List<I> internalList) {
		this.internalList = internalList;
	}


	public List<E> getExternalList() {
		return this.externalList;
	}


	public void setExternalList(List<E> externalList) {
		this.externalList = externalList;
	}


	public List<MatchingResultItem<I, E>> getParentItemList() {
		return this.parentItemList;
	}


	public void setParentItemList(List<MatchingResultItem<I, E>> parentItemList) {
		this.parentItemList = parentItemList;
	}


	public int getLevel() {
		return this.level;
	}


	public void setLevel(int level) {
		this.level = level;
	}


	public Integer getTargetPermutationSize() {
		return this.targetPermutationSize;
	}


	public void setTargetPermutationSize(Integer targetPermutationSize) {
		this.targetPermutationSize = targetPermutationSize;
	}


	public List<Integer> getExternalCombinationSizes() {
		return this.externalCombinationSizes;
	}


	public void setExternalCombinationSizes(List<Integer> externalCombinationSizes) {
		this.externalCombinationSizes = externalCombinationSizes;
	}


	public boolean isForceCombinationSize() {
		return this.forceCombinationSize;
	}


	public void setForceCombinationSize(boolean forceCombinationSize) {
		this.forceCombinationSize = forceCombinationSize;
	}


	public boolean isAllowPartialMatch() {
		return this.allowPartialMatch;
	}


	public void setAllowPartialMatch(boolean allowPartialMatch) {
		this.allowPartialMatch = allowPartialMatch;
	}


	public Date getExecutionStartTime() {
		return this.executionStartTime;
	}


	public void setExecutionStartTime(Date executionStartTime) {
		this.executionStartTime = executionStartTime;
	}


	public boolean isLongRunning() {
		return this.longRunning;
	}


	public void setLongRunning(boolean longRunning) {
		this.longRunning = longRunning;
	}
}
