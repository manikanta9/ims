package com.clifton.core.util.concurrent.synchronize.handler;

import com.clifton.core.util.concurrent.LockBusyException;
import com.clifton.core.util.concurrent.synchronize.Synchronizable;
import com.clifton.core.util.concurrent.synchronize.SynchronizableCallableAction;
import com.clifton.core.util.concurrent.synchronize.SynchronizableExecutableAction;


/**
 * The SynchronizationHandler can be used to execute any custom code (usually a method call) that requires the caller
 * to obtain an exclusive lock before the code can be executed.  Internally it handles release of locks
 * upon completion of execution or error.
 *
 * @author vgomelsky
 */
public interface SynchronizationHandler {

	/**
	 * Queues the specified executable action for execution for up to the specified wait millisecond duration defined on the {@link Synchronizable}.
	 * If the requested lock is available, the action will obtain the lock immediately and execute the action. If the specified lock is taken by
	 * another thread, the calling thread will block until either the lock becomes available or the millisecond wait duration has passed.
	 *
	 * @param lockObtainableExecutableAction the action to execute after the lock is successfully obtained
	 * @throws LockBusyException if the lock is not available within the action's specified millisecond wait duration
	 */
	public void execute(SynchronizableExecutableAction lockObtainableExecutableAction);


	/**
	 * Queues the specified callable action for execution for up to the specified wait millisecond duration defined on the {@link Synchronizable}.
	 * If the requested lock is available, the action will obtain the lock immediately and call the action. If the specified lock is taken by
	 * another thread, the calling thread will block until either the lock becomes available or the millisecond wait duration has passed. The
	 * callable response will be returned after successfully obtaining the lock and calling the action.
	 *
	 * @param lockObtainableCallableAction the action to call after the lock is successfully obtained
	 * @param <V>                          the response type of the callable action
	 * @return the response of the called action
	 * @throws LockBusyException if the lock is not available within the action's specified millisecond wait duration
	 */
	public <V> V call(SynchronizableCallableAction<V> lockObtainableCallableAction);
}
