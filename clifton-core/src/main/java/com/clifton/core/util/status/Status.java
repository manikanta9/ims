package com.clifton.core.util.status;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.runner.Runner;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;


/**
 * The Status class can be used to accumulate run stats and errors for a single {@link Runner} run.
 *
 * @author vgomelsky
 */
public class Status implements Serializable {

	public static final int DEFAULT_MAX_MESSAGE_COUNT = 1000;


	/**
	 * A map that can be used to store values specific to processing and later used
	 * to build a specific message for the overall status.
	 */
	private Map<String, Object> statusMap;

	private String statusTitle;
	private String message;
	/**
	 * The list of message arguments with which to format the {@link #message} using {@link String#format(String, Object...)}, if provided.
	 */
	private Object[] messageArgs;

	/**
	 * Used to specify whether an action was performed by the process that's reporting on its Status.
	 * By default, the action is always performed by the process.  However, one can use this flag to identify
	 * when a process ran but didn't do anything (for example, the data was not available to do certain processing).
	 * <p>
	 * This could be used by batch jobs to specify whether execution completed or completed_with_no_results.
	 */
	private boolean actionPerformed = true;

	/**
	 * NOTE: status details maybe accessed concurrently:
	 * while the runner thread adds to the list, another thread can be used to check status of all details.
	 */
	private List<StatusDetail> detailList = new CopyOnWriteArrayList<>();

	/**
	 * Specifies the maximum number of {@link #detailList} items to avoid very large lists and out of memory.
	 */
	private int maxDetailCount = DEFAULT_MAX_MESSAGE_COUNT;


	/**
	 * Set the value to true in order to termination execution (throw an exception) of the process using this status object.
	 * Can be used to stop long running processes.
	 */
	private boolean terminationRequested;

	/**
	 * In some cases, the process using the Status may have child steps. For example, batch job with multiple steps.
	 * This property can be used to identify the Status of currently running child process.
	 * It can be used to include current's child status update for the user as well as stop the child.
	 */
	private Status currentChildStatus;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static Status ofMessage(String message) {
		Status result = new Status();
		result.setMessage(message);
		return result;
	}


	public static Status ofMessage(String message, boolean actionPerformed) {
		Status result = ofMessage(message);
		result.setActionPerformed(actionPerformed);
		return result;
	}


	public static Status ofEmptyMessage() {
		return new Status();
	}


	public static Status ofTitle(String title) {
		Status result = new Status();
		result.setStatusTitle(title);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private void checkForTerminationRequest() {
		if (isTerminationRequested()) {
			throw new ProcessTerminationException("The process was interrupted: " + this.message);
		}
	}

	/**
	 * Returns the list of {@StatusDetail} objects that match the specified category name.
	 * Returns an empty list if one are found.
	 */
	public List<StatusDetail> getDetailListForCategory(String categoryName) {
		checkForTerminationRequest();
		List<StatusDetail> result = new ArrayList<>();
		for (StatusDetail detail : CollectionUtils.getIterable(getDetailList())) {
			if (detail.getCategory().equals(categoryName)) {
				result.add(detail);
			}
		}
		return result;
	}


	/**
	 * Resets the list of {@StatusDetail} objects that match the specified category name.
	 */
	public void clearDetailListForCategory(String categoryName) {
		checkForTerminationRequest();
		List<Integer> detailIndexList = new ArrayList<>();
		for (int i = 0; i < CollectionUtils.getSize(getDetailList()); i++) {
			StatusDetail detail = getDetailList().get(i);
			if (detail.getCategory().equals(categoryName)) {
				detailIndexList.add(i);
			}
		}
		//Reverse the order so we remove the last first (avoid index out of bounds)
		Collections.reverse(detailIndexList);
		for (Integer index : CollectionUtils.getIterable(detailIndexList)) {
			getDetailList().remove(index.intValue());
		}
	}


	/**
	 * Returns the list of {@StatusDetail} objects that have category of {@link StatusDetail#CATEGORY_ERROR}.
	 */
	public List<StatusDetail> getErrorList() {
		return getDetailListForCategory(StatusDetail.CATEGORY_ERROR);
	}


	/**
	 * Returns the number of {@link StatusDetail} objects that have category of {@link StatusDetail#CATEGORY_ERROR}.
	 */
	public int getErrorCount() {
		return getDetailListForCategory(StatusDetail.CATEGORY_ERROR).size();
	}


	/**
	 * Returns the number of {@link StatusDetail} objects that have category of {@link StatusDetail#CATEGORY_WARNING}.
	 */
	public int getWarningCount() {
		return getDetailListForCategory(StatusDetail.CATEGORY_WARNING).size();
	}


	/**
	 * Returns the number of {@link StatusDetail} objects that have category of {@link StatusDetail#CATEGORY_SKIPPED}.
	 */
	public int getSkippedCount() {
		return getDetailListForCategory(StatusDetail.CATEGORY_SKIPPED).size();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addError(String note) {
		addDetail(StatusDetail.CATEGORY_ERROR, note);
	}


	public void addError(String note, Map<String, String> properties) {
		addDetail(StatusDetail.CATEGORY_ERROR, note, properties);
	}


	public void addErrors(List<String> notes) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addError(note);
		}
	}


	public void addWarning(String note) {
		addDetail(StatusDetail.CATEGORY_WARNING, note);
	}


	public void addWarning(String note, Map<String, String> properties) {
		addDetail(StatusDetail.CATEGORY_WARNING, note, properties);
	}


	public void addWarnings(List<String> notes) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addWarning(note);
		}
	}


	public void addMessage(String note) {
		addDetail(StatusDetail.CATEGORY_MESSAGE, note);
	}


	public void addMessage(String note, Map<String, String> properties) {
		addDetail(StatusDetail.CATEGORY_MESSAGE, note, properties);
	}


	public void addMessages(List<String> notes) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addMessage(note);
		}
	}


	public void addSkipped(String note) {
		addDetail(StatusDetail.CATEGORY_SKIPPED, note);
	}


	public void addSkipped(String note, Map<String, String> properties) {
		addDetail(StatusDetail.CATEGORY_SKIPPED, note, properties);
	}


	public void addSkipped(List<String> notes) {
		for (String note : CollectionUtils.getIterable(notes)) {
			addSkipped(note);
		}
	}


	public void addDetail(String category, String note) {
		checkForTerminationRequest();
		addDetail(category, note, null);
	}


	public void addDetail(String category, String note, Map<String, String> properties) {
		checkForTerminationRequest();
		if (this.detailList.size() >= getMaxDetailCount()) {
			if (this.detailList.size() == getMaxDetailCount()) {
				this.detailList.add(new StatusDetail(new Date(), StatusDetail.CATEGORY_MESSAGE, "Cannot add more non-error details. Exceeded maximum allowed count of " + getMaxDetailCount(), properties));
			}
			if (StatusDetail.CATEGORY_ERROR.equals(category)) {
				// still allow errors to be added
				this.detailList.add(new StatusDetail(new Date(), category, note, properties));
			}
		}
		else {
			this.detailList.add(new StatusDetail(new Date(), category, note, properties));
		}
	}


	public void mergeStatus(Status toMerge) {
		String toMergeTitle = toMerge.getStatusTitle();
		if (!StringUtils.isEmpty(toMergeTitle)) {
			setStatusTitle(toMergeTitle);
		}
		String toMergeMessage = toMerge.getMessage();
		if (!StringUtils.isEmpty(toMergeMessage)) {
			setMessage(toMergeMessage);
		}
		if (toMerge.statusMap != null) {
			// statusMap is lazily initialized, only get it if not null
			getStatusMap().putAll(toMerge.getStatusMap());
		}
		List<StatusDetail> toMergeDetailList = toMerge.getDetailList();
		if (!CollectionUtils.isEmpty(toMergeDetailList)) {
			this.detailList.addAll(toMergeDetailList);
		}
		setActionPerformed(isActionPerformed() || toMerge.isActionPerformed());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getStatusCount(String statusCountType) {
		Integer count = (Integer) getStatusMap().get(statusCountType);
		return count == null ? 0 : count;
	}


	public void setStatusCount(String statusCountType, int count) {
		getStatusMap().put(statusCountType, count);
	}


	public void incrementStatusCount(String statusCountType) {
		Integer count = (Integer) getStatusMap().get(statusCountType);
		getStatusMap().put(statusCountType, count == null ? 1 : count + 1);
	}


	public void decrementStatusCount(String statusCountType) {
		Integer count = (Integer) getStatusMap().get(statusCountType);
		getStatusMap().put(statusCountType, count == null ? -1 : count - 1);
	}


	@ValueIgnoringGetter
	public Map<String, Object> getStatusMap() {
		if (this.statusMap == null) {
			this.statusMap = new HashMap<>();
		}
		return this.statusMap;
	}


	public String getMessage() {
		checkForTerminationRequest();
		if (this.messageArgs != null && this.messageArgs.length > 0) {
			return String.format(this.message, this.messageArgs);
		}
		return this.message;
	}


	public void setMessage(String message, Object... messageArgs) {
		checkForTerminationRequest();
		this.message = message;
		this.messageArgs = messageArgs;
	}


	public String getMessageWithChildStatus() {
		if (getCurrentChildStatus() == null) {
			return getMessage();
		}
		return getMessage() + "; CURRENT CHILD: " + getCurrentChildStatus().getMessage();
	}


	public void setMessageArgs(Object... messageArgs) {
		checkForTerminationRequest();
		this.messageArgs = messageArgs;
	}


	/**
	 * Include errors in the message if present;
	 */
	public String getMessageWithErrors() {
		List<StatusDetail> errors = getErrorList();
		if (CollectionUtils.isEmpty(errors)) {
			return getMessage();
		}

		return getMessage() + "\n\n" + CollectionUtils.toString(errors, 5);
	}


	/**
	 * Appends all error category details to the status Message
	 */
	public void setMessageWithErrors(String messagePrefix, Integer maxErrors) {
		setMessage(getMessageForCategory(StatusDetail.CATEGORY_ERROR, messagePrefix, maxErrors));
	}


	public String getMessageForCategory(String category, String messagePrefix, Integer maxCategoryCount) {
		checkForTerminationRequest();
		messagePrefix = StringUtils.coalesce(messagePrefix, "");
		List<StatusDetail> listForCategory = getDetailListForCategory(category);
		if (CollectionUtils.isEmpty(listForCategory)) {
			// no items
			return messagePrefix;
		}
		StringBuilder result = new StringBuilder(messagePrefix);
		result.append(" ").append(category).append(": ");
		int count = 0;
		for (StatusDetail detail : listForCategory) {
			if (maxCategoryCount != null && count >= maxCategoryCount) {
				result.append("...");
				break;
			}
			result.append(detail.getNote()).append("; ");
			count++;
		}
		return result.toString();
	}


	/**
	 * Appends the specified string to the end of current status message.
	 */
	public void appendToMessage(String additionalMessage) {
		checkForTerminationRequest();
		if (this.message == null) {
			this.message = additionalMessage;
		}
		else {
			this.message += "\n" + additionalMessage;
		}
	}


	@Override
	public String toString() {
		return getMessage();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setStatusMap(Map<String, Object> statusMap) {
		this.statusMap = statusMap;
	}


	public String getStatusTitle() {
		return this.statusTitle;
	}


	public void setStatusTitle(String statusTitle) {
		this.statusTitle = statusTitle;
	}


	public void setMessage(String message) {
		checkForTerminationRequest();
		this.message = message;
	}


	public Object[] getMessageArgs() {
		return this.messageArgs;
	}


	public boolean isActionPerformed() {
		return this.actionPerformed;
	}


	public void setActionPerformed(boolean actionPerformed) {
		this.actionPerformed = actionPerformed;
	}


	public List<StatusDetail> getDetailList() {
		return this.detailList;
	}


	public void setDetailList(List<StatusDetail> detailList) {
		this.detailList = detailList;
	}


	public int getMaxDetailCount() {
		return this.maxDetailCount;
	}


	public void setMaxDetailCount(int maxDetailCount) {
		this.maxDetailCount = maxDetailCount;
	}


	public boolean isTerminationRequested() {
		return this.terminationRequested;
	}


	public void setTerminationRequested(boolean terminationRequested) {
		this.terminationRequested = terminationRequested;
		if (getCurrentChildStatus() != null) {
			getCurrentChildStatus().setTerminationRequested(terminationRequested);
		}
	}


	public Status getCurrentChildStatus() {
		return this.currentChildStatus;
	}


	public void setCurrentChildStatus(Status currentChildStatus) {
		this.currentChildStatus = currentChildStatus;
	}
}
