package com.clifton.core.util.collections;


/**
 * The <code>NumberMap</code> is an interface represents a mapping between a key and a number, and provides convenience methods
 * for performing simple arithmetic on the values.
 *
 * @param <K> the key type
 * @param <V> the value type
 * @author jgommels
 */
public interface NumberMap<K, V extends Number> {

	/**
	 * Adds the value to the existing value mapped from the key (which is assumed to be zero if no mapping exists yet).
	 *
	 * @param key   the key that maps to the value to be added to
	 * @param value the value to add to the existing value
	 * @return the new value
	 */
	public V add(K key, V value);


	/**
	 * Subtracts the value from the existing value mapped from the key (which is assumed to be zero if no mapping exists yet).
	 *
	 * @param key   the key that maps to the value to be subtracted from
	 * @param value the value to subtract from the existing value
	 * @return the new value
	 */
	public V subtract(K key, V value);


	/**
	 * Returns the value that the key maps to. If no mapping exists, then zero is returned, as opposed to <code>null</code>.
	 *
	 * @param key the key to retrieve the value for
	 * @return the value that the key maps to, or zero if no mapping exists
	 */
	public V get(K key);
}
