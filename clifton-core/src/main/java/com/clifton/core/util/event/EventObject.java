package com.clifton.core.util.event;


import com.clifton.core.util.AssertUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * The <code>EventObject</code> is a base implementation of the Event interface.
 *
 * @author rbrooks
 */
public class EventObject<T, R> implements Event<T, R> {

	/**
	 * The identifying name of this event
	 */
	private String eventName;

	/**
	 * A map for relaying event-specific information to the EventListener in key-value pairs
	 */
	private Map<String, Object> context;

	/**
	 * The target of the event processing.
	 */
	private T target;

	/**
	 * The result of the event processing.
	 */
	private R result;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public EventObject() {
		// preserve default constructor
	}


	public EventObject(String eventName) {
		setEventName(eventName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static EventObject<Object, Object> ofEvent(String eventName) {
		return ofEventTargetAndResult(eventName, null, null);
	}


	public static <T> EventObject<T, Object> ofEventTarget(String eventName, T target) {
		return ofEventTargetAndResult(eventName, target, null);
	}


	public static <R> EventObject<Object, R> ofEventResult(String eventName, R result) {
		return ofEventTargetAndResult(eventName, null, result);
	}


	public static <T, R> EventObject<T, R> ofEventTargetAndResult(String eventName, T target, R result) {
		EventObject<T, R> eventObject = new EventObject<>(eventName);
		eventObject.setTarget(target);
		eventObject.setResult(result);
		return eventObject;
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void addContextValue(String key, Object value) {
		AssertUtils.assertNotNull(key, "Cannot insert null key into event context");

		if (getContext() == null) {
			this.context = new HashMap<>();
		}

		getContext().put(key, value);
	}


	@Override
	public Object getContextValue(String key) {

		if (getContext() == null) {
			return null;
		}
		return getContext().get(key);
	}


	@Override
	public Object removeContextValue(String key) {

		if (getContext() == null) {
			return null;
		}
		return getContext().remove(key);
	}


	@Override
	public String getEventName() {
		return this.eventName;
	}


	public void setEventName(String eventName) {
		this.eventName = eventName;
	}


	private Map<String, Object> getContext() {
		return this.context;
	}


	@Override
	public R getResult() {
		return this.result;
	}


	@Override
	public void setResult(R result) {
		this.result = result;
	}


	@Override
	public T getTarget() {
		return this.target;
	}


	public void setTarget(T target) {
		this.target = target;
	}
}
