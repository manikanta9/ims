package com.clifton.core.util.http.cookie;

import org.apache.http.client.CookieStore;
import org.apache.http.cookie.Cookie;

import java.util.List;


/**
 * Cookie Store for IMS integration test requests.
 *
 * @author NickK
 */
public interface ImsCookieStore extends CookieStore {

	public void setCookieList(List<Cookie> cookieList);


	public boolean containsCookie(String cookieName, String host);


	public Cookie getCookie(String cookieName, String host);
}
