package com.clifton.core.util.observer;


/**
 * The <code>Observer</code> interface is a marker interface that must be extended by
 * all observerable interfaces.
 * <p/>
 * Observer interface methods are "events" that are invoked by corresponding subject objects (example: an entity was deleted/updated).
 * Classes implementing Observer interface are listeners that will be notified by the subject whenever an event occurs.
 *
 * @author vgomelsky
 */
public interface Observer {

	// marker interface
}
