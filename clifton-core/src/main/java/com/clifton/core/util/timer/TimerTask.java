package com.clifton.core.util.timer;


/**
 * The <code>TimerTask</code> class represents a timed task that can be started and stopped multiple times.
 * The task collects total duration as well as the number of times it was run.
 *
 * @author vgomelsky
 */
public class TimerTask {

	private long startTimeNano;
	private long durationNano;
	private int runCount;


	public void start() {
		if (this.startTimeNano != 0) {
			throw new IllegalStateException("Cannot start the Timer that is running");
		}
		this.startTimeNano = System.nanoTime();
	}


	public void stop() {
		if (this.startTimeNano == 0) {
			throw new IllegalStateException("Cannot stop the Timer that hasn't been started");
		}
		this.durationNano += (System.nanoTime() - this.startTimeNano);
		this.startTimeNano = 0;
		this.runCount++;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public long getStartTimeNano() {
		return this.startTimeNano;
	}


	public long getDurationNano() {
		return this.durationNano;
	}


	public int getRunCount() {
		return this.runCount;
	}
}
