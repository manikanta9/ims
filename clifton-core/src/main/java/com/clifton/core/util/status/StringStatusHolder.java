package com.clifton.core.util.status;


/**
 * The <code>StringStatusHolder</code> class is a simple String based StatusHolderObject.
 *
 * @author vgomelsky
 */
public class StringStatusHolder implements StatusHolderObject<String> {

	private String status;


	public StringStatusHolder() {
		super();
	}


	public StringStatusHolder(String initialStatus) {
		this.status = initialStatus;
	}


	@Override
	public String getStatus() {
		return this.status;
	}
}
