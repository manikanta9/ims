package com.clifton.core.util;


import com.clifton.core.beans.BeanUtils;

import java.util.Map;


public class CoreMapUtils {


	private CoreMapUtils() {
		// Disable instantiation
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Creates a shallow clone of the specified map.
	 */
	public static <K, V, T extends Map<K, V>> T clone(T map) {
		if (map == null) {
			return null;
		}

		T clone = BeanUtils.newInstance(map);
		clone.putAll(map);
		return clone;
	}
}
