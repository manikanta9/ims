package com.clifton.core.util.http;


import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.protocol.HttpClientContext;

import java.io.Closeable;
import java.io.InputStream;
import java.net.URI;


public interface SimpleHttpResponse extends Closeable {

	public HttpClientContext getContext();


	public InputStream getContent();


	public HttpEntity getEntity();


	public HttpResponse getHttpResponse();


	public int getStatusCode();


	public URI getURI();
}
