package com.clifton.core.util.retry;

import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.function.Supplier;


/**
 * @author theodorez
 */
public class RetryContext<T> {

	//The task to execute
	private final RetryTask<T> retryTask;

	private final int maxRetries;
	private final int retryDelaySeconds;

	private final ScheduledExecutorService scheduler;
	//Counts the number of retries
	private AtomicInteger retryNumber = new AtomicInteger(1);


	protected RetryContext(RetryTask<T> retryTask, ScheduledExecutorService scheduler, int maxRetries, int retryDelaySeconds) {
		this.retryTask = retryTask;
		this.scheduler = scheduler;
		this.maxRetries = maxRetries;
		this.retryDelaySeconds = retryDelaySeconds;
	}


	public RetryTask<T> getRetryTask() {
		return this.retryTask;
	}


	public int getMaxRetries() {
		return this.maxRetries;
	}


	public int getRetryDelaySeconds() {
		return this.retryDelaySeconds;
	}


	public ScheduledExecutorService getScheduler() {
		return this.scheduler;
	}


	public AtomicInteger getRetryNumber() {
		return this.retryNumber;
	}


	public static class RetryContextBuilder<T> {

		private RetryTask<T> retryTask;
		private int maxRetries;
		private int retryDelaySeconds;
		private ScheduledExecutorService scheduler;


		private RetryContextBuilder() {
			//Nothing here
		}


		public static <T> RetryContextBuilder<T> getInstanceForTask(RetryTask<T> retryTask) {
			RetryContextBuilder<T> builder = new RetryContextBuilder<>();
			return builder.withRetryTask(retryTask);
		}


		public static <T> RetryContextBuilder<T> getInstanceForActionAndSuccessCondition(Supplier<T> action, Predicate<T> successCondition) {
			RetryContextBuilder<T> builder = new RetryContextBuilder<>();
			return builder.withActionAndSuccessCondition(action, successCondition);
		}


		public RetryContext<T> build() {
			return new RetryContext<>(this.retryTask, this.scheduler, this.maxRetries, this.retryDelaySeconds);
		}


		public RetryContextBuilder<T> withScheduler(ScheduledExecutorService scheduler) {
			this.scheduler = scheduler;
			return this;
		}


		public RetryContextBuilder<T> withMaxRetries(int maxRetries) {
			this.maxRetries = maxRetries;
			return this;
		}


		public RetryContextBuilder<T> withRetryDelayInSeconds(int retryDelayInSeconds) {
			this.retryDelaySeconds = retryDelayInSeconds;
			return this;
		}


		public RetryContextBuilder<T> withRetryTask(RetryTask<T> retryTask) {
			this.retryTask = retryTask;
			return this;
		}


		public RetryContextBuilder<T> withActionAndSuccessCondition(Supplier<T> action, Predicate<T> successCondition) {
			return this.withRetryTask(new RetryTask<>(action, successCondition));
		}
	}
}
