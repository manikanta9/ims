package com.clifton.core.util.retry;

import com.clifton.core.logging.LogUtils;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;


/**
 * A job that will run an action a given number of times, delaying execution of the next retry a set number of seconds after termination.
 * <p>
 * For each run, the result of the action will be passed to the successCondition that should return TRUE or FALSE if the condition is successful.
 *
 * @author theodorez
 */
public class RetryJob<T> implements Runnable {

	private final RetryContext<T> retryContext;
	private final CompletableFuture<T> completableFuture;


	public RetryJob(RetryContext<T> retryContext) {
		this.retryContext = retryContext;
		this.completableFuture = new CompletableFuture<>();
	}


	@Override
	public void run() {
		try {
			T result = this.retryContext.getRetryTask().doAction();
			//If the result is not successful attempt to reschedule it, otherwise complete immediately with the given result
			if (this.retryContext.getRetryTask().success(result)) {
				this.completableFuture.complete(result);
			}
			else {
				reschedule(result);
			}
		}
		catch (Exception e) {
			this.completableFuture.completeExceptionally(e);
		}
	}


	//Schedules this job to run immediately and returns the completable future
	public CompletableFuture<T> submit() {
		this.retryContext.getScheduler().schedule(this, 0, TimeUnit.SECONDS);
		return this.completableFuture;
	}


	private void reschedule(T result) {
		//Reschedules this job until the retry counter = n+1, at which point the job will complete with whatever the result is
		if (this.retryContext.getRetryNumber().getAndIncrement() < this.retryContext.getMaxRetries()) {
			LogUtils.debug(getClass(), "Rescheduling for attempt number: " + this.retryContext.getRetryNumber().get());
			this.retryContext.getScheduler().schedule(this, this.retryContext.getRetryDelaySeconds(), TimeUnit.SECONDS);
		}
		else {
			this.completableFuture.complete(result);
		}
	}
}
