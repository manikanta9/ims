package com.clifton.core.util.http;


import java.net.URI;


/**
 * The <code>BadStatusException</code> indicates that there was a bad status code returned by the web server.
 *
 * @author jgommels
 */
public class BadStatusException extends HttpException {

	private static final long serialVersionUID = -2483832253566892504L;

	private final URI uri;
	private final int statusCode;
	private final String content;


	public BadStatusException(URI uri, int statusCode, String content) {
		super(createMessage(uri, statusCode, content));
		this.uri = uri;
		this.statusCode = statusCode;
		this.content = content;
	}


	public URI getURI() {
		return this.uri;
	}


	public int getStatusCode() {
		return this.statusCode;
	}


	public String getContent() {
		return this.content;
	}


	private static String createMessage(URI uri, int status, String content) {

		StringBuilder errorMsgBuilder = new StringBuilder();
		errorMsgBuilder.append("A status of ").append(status).append(" was received by the server.\nURI: ").append(uri).append("\nResponse Content:\n").append(content);

		return errorMsgBuilder.toString();
	}
}
