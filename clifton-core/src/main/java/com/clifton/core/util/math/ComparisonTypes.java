package com.clifton.core.util.math;

import com.clifton.core.util.MathUtils;

import java.util.function.BiPredicate;


/**
 * A numerical comparison type. These types may be used to compare two numeric values.
 */
public enum ComparisonTypes {

	LESS_THAN(MathUtils::isLessThan, "<", "is less than"),
	LESS_THAN_OR_EQUAL_TO(MathUtils::isLessThanOrEqual, "<=", "is less than or equal to"),
	GREATER_THAN(MathUtils::isGreaterThan, ">", "is greater than"),
	GREATER_THAN_OR_EQUAL_TO(MathUtils::isGreaterThanOrEqual, ">=", "is greater than or equal to"),
	EQUAL_TO(MathUtils::isEqual, "=", "is equal to");

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private final BiPredicate<Number, Number> comparator;
	private final String comparisonExpression;
	private final String label;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	ComparisonTypes(BiPredicate<Number, Number> comparator, String comparisonExpression, String label) {
		this.comparator = comparator;
		this.comparisonExpression = comparisonExpression;
		this.label = label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Compares the given numeric values.
	 *
	 * @param from the first numeric value
	 * @param to   the second numeric value
	 * @return {@code true} if the comparison is passed, or {@code false} otherwise
	 */
	public boolean compare(Number from, Number to) {
		return this.comparator.test(from, to);
	}


	@Override
	public String toString() {
		return this.label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getComparisonExpression() {
		return this.comparisonExpression;
	}
}
