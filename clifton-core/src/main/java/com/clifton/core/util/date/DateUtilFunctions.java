package com.clifton.core.util.date;

import java.util.Date;


/**
 * The <code>DateUtilsFunctions</code> is an enum that can be used to retrieve a date based on a given date. i.e. quarter end, month end, etc.
 *
 * @author manderson
 */
public enum DateUtilFunctions {

	YEAR_START() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getFirstDayOfYear(date);
		}
	},

	YEAR_END() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getLastDayOfYear(date);
		}
	},
	MONTH_START() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getFirstDayOfMonth(date);
		}
	},
	MONTH_END() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getLastDayOfMonth(date);
		}
	},
	QUARTER_START() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getFirstDayOfQuarter(date);
		}
	},
	QUARTER_END() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getLastDayOfQuarter(date);
		}
	},
	PREVIOUS_WEEKDAY() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getPreviousWeekday(date);
		}
	},
	NEXT_WEEKDAY() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getNextWeekday(date);
		}
	},
	MONTH_WEEKDAY_START() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getFirstWeekdayOfMonth(date);
		}
	},
	MONTH_WEEKDAY_END() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getLastWeekdayOfMonth(date);
		}
	},
	PREVIOUS_MONTH_WEEKDAY_END() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getLastWeekdayOfPreviousMonth(date);
		}
	},
	PREVIOUS_MONTH_END() {
		@Override
		public Date evaluateDate(Date date) {
			return DateUtils.getLastDayOfPreviousMonth(date);
		}
	};


	public abstract Date evaluateDate(Date date);
}
