package com.clifton.core.util.concurrent;

/**
 * <code>CallableAction</code> interface can be used to execute any custom code (usually a method call) that returns a value.
 *
 * @author NickK
 */
public interface CallableAction<V> {

	public V call();
}
