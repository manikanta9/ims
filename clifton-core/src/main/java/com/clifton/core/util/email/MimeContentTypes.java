package com.clifton.core.util.email;


/**
 * Represents MIME Content Type values.
 * <ul>
 * <li>{@code text/plain}</li>
 * <li>{@code text/html}</li>
 * </ul>
 * {@code valueOf(), name()} returns the name of the enum and
 * {@link MimeContentTypes#value} and {@code toString()} gets the actual value.
 * e.g {@code text/plain}.
 */
public enum MimeContentTypes {
	/**
	 * {@code text/plain}
	 */
	TEXT_PLAIN("text/plain"),

	/**
	 * {@code text/html}
	 */
	TEXT_HTML("text/html");


	@Override
	public String toString() {
		return getValue();
	}


	/**
	 * The value itself: e.g. {@code text/plain}
	 */
	private final String value;


	MimeContentTypes(String value) {

		this.value = value;
	}


	/**
	 * @return The value itself: e.g. {@code text/plain}
	 */
	public String getValue() {
		return this.value;
	}
}
