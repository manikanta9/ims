package com.clifton.core.util.status;


/**
 * The <code>StatusHolderObject</code> interface should be implemented by objects that have a status field
 * that can change over time.
 * <p/>
 * For example, a batch job runner maybe a StatusHolderObject and each batch job can periodically update the
 * status to indicate where in processing it is: processed 100 out of 500, etc.
 *
 * @param <T>
 * @author vgomelsky
 */
public interface StatusHolderObject<T> {

	/**
	 * Returns current status of StatusHolderObject.
	 */
	public T getStatus();
}
