package com.clifton.core.util.aws.s3;

import com.amazonaws.AmazonServiceException;
import com.amazonaws.ClientConfiguration;
import com.amazonaws.RequestClientOptions;
import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.BasicSessionCredentials;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.AmazonS3URI;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.transfer.TransferManager;
import com.amazonaws.services.s3.transfer.TransferManagerBuilder;
import com.amazonaws.services.s3.transfer.Upload;
import com.clifton.core.cache.SimpleCacheHandler;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.ssl.SSLContexts;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.regex.Pattern;

import static com.clifton.core.util.aws.s3.AWSS3Utils.getAmazonS3Uri;
import static com.clifton.core.util.aws.s3.AWSS3Utils.getS3FileKey;


public class AWSS3HandlerImpl implements AWSS3Handler {

	private static final Pattern S3_VALID_NAME_PATTERN = Pattern.compile("(?=^.{3,63}$)(?!^(\\d{1,3}\\.){3}\\d{1,3}$)(?!.*\\.(?![a-z0-9]))(^[a-z0-9]([a-z0-9-\\.]*)[a-z0-9]$)");

	private String accessKeyId;
	private String secretAccessKey;
	private String sessionToken;

	// After testing on (12/3/2020), 50MB part sizes produces the fastest upload times
	private static final long MULTIPART_UPLOAD_PART_SIZE = org.apache.commons.io.FileUtils.ONE_MB * 50;
	private static final long MULTIPART_UPLOAD_THRESHOLD = org.apache.commons.io.FileUtils.ONE_MB * 50;

	private SimpleCacheHandler<String, Object> simpleCacheHandler;


	@Override
	public void send(String region, String s3DestinationPath, FileWrapper fileWrapper, String endpointOverride) {
		AmazonS3 amazonS3 = getAmazonS3(region, endpointOverride);
		sendFileMultipart(createPutObjectRequest(s3DestinationPath, fileWrapper), amazonS3);
	}


	@Override
	public void send(String region, String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength, String endpointOverride) {
		AmazonS3 amazonS3 = getAmazonS3(region, endpointOverride);
		sendFileMultipart(createPutObjectRequest(s3DestinationPath, fileContainer, fileName, fileLength), amazonS3);
	}


	@Override
	public void send(String region, String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength) {
		AmazonS3 amazonS3 = getAmazonS3(region, null);
		sendFileMultipart(createPutObjectRequest(s3DestinationPath, fileContainer, fileName, fileLength), amazonS3);
	}


	@Override
	public void send(String region, String s3DestinationPath, FileWrapper fileWrapper, String accessKey, String secretKey) {
		AmazonS3 amazonS3 = getAmazonS3(region, accessKey, secretKey);
		sendFileMultipart(createPutObjectRequest(s3DestinationPath, fileWrapper), amazonS3);
	}


	@Override
	public void send(String region, String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength, String accessKey, String secretKey) {
		AmazonS3 amazonS3 = getAmazonS3(region, accessKey, secretKey);
		sendFileMultipart(createPutObjectRequest(s3DestinationPath, fileContainer, fileName, fileLength), amazonS3);
	}


	@Override
	public String retrieve(String region, String bucketName, String fileName, String endpointOverride) {
		String path = null;

		AmazonS3 s3Client = getAmazonS3(region, endpointOverride);

		validateBucketName(bucketName);

		String strTmp = System.getProperty("java.io.tmpdir");
		File localFile = new File(FileUtils.combinePaths(strTmp, File.separator, fileName));


		try {
			s3Client.getObject(new GetObjectRequest(bucketName, fileName), localFile);
		}
		catch (AmazonServiceException amazonServiceException) {
			try {
				FileUtils.delete(localFile);
				throw new ValidationException(amazonServiceException.getErrorMessage(), amazonServiceException);
			}
			catch (Exception e) {
				throw new ValidationException(amazonServiceException.getErrorMessage() + "\n Error deleting local file", e);
			}
		}
		catch (SdkClientException sdkClientException) {
			try {
				Files.delete(localFile.toPath());
				throw new ValidationException(sdkClientException.getMessage(), sdkClientException);
			}
			catch (FileNotFoundException fileNotFoundException) {
				throw new ValidationException(sdkClientException.getMessage(), fileNotFoundException);
			}
			catch (Exception e) {
				throw new ValidationException(sdkClientException.getMessage() + "\n Error deleting local file", e);
			}
		}

		if (localFile.exists()) {
			path = localFile.getAbsolutePath();
		}
		return path;
	}


	@Override
	public void validateBucketName(String bucket) {
		/*
		 (?=^.{3,63}$), a positive look ahead to ensure that the match is between 3 and 63 characters long.
		 (?!^(\d{1,3}\.){3}\d{1,3}$), a negative lookahead which only matches valid IP addresses. Basically, we try to match 1-3 numbers followed by a period 3 times (\d{1,3}\.){3}) followed by 1-3 numbers (\d{1,3}).
		 (?!.*\.(?![a-z0-9])), asserts that it's not possible to match any string, followed by '.' unless that is followed by an alphanumeric (Invalid example: test.-test)
		 (^[a-z0-9]), start with a lowercase letter or a number
		 ([a-z0-9-]*), followed by lowercase letters, numbers, or hyphens repeated 0 to many times
		 [a-z0-9]$ , can only end with alphanumeric
		 */
		ValidationUtils.assertTrue(S3_VALID_NAME_PATTERN.matcher(bucket).matches(), "Invalid S3 bucket name. Your bucket name must fit the following criteria: between 3 and 63 characters long; consist only of lowercase letters, numbers, dots (.), and hyphens (-); begin and end with a letter or number; not be formatted as an IP address (for example, 192.168.5.4).");
	}

	////////////////////////////////////////////////////////////////////////////
	////////                 Private Helper Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Checks IMS cache for AmazonS3 client. Inputs one if it doesn't exist
	 * SimpleCache is formated in the following way: <"AWS_S3_EXPORT", <"us-east-1", AmazonS3>>
	 *
	 * @param region: AWS bucket region
	 * @return AmazonS3 client in cache
	 */
	private AmazonS3 getAmazonS3(String region, String endpointOverride) {
		AmazonS3 s3Client;

		if (StringUtils.isEmpty(endpointOverride)) {
			s3Client = (AmazonS3) getSimpleCacheHandler().get("AWS_S3_EXPORT", region);

			if (s3Client == null) {
				// Credentials provided in properties file
				if (!getAccessKeyId().isEmpty()) {
					AWSCredentials credentials;
					if (getSessionToken().isEmpty()) {
						credentials = new BasicAWSCredentials(getAccessKeyId(), getSecretAccessKey());
					}
					else {
						credentials = new BasicSessionCredentials(getAccessKeyId(), getSecretAccessKey(), getSessionToken());
					}

					// Get AmazonS3 client and return the s3Client object.
					s3Client = AmazonS3ClientBuilder
							.standard()
							.withRegion(Regions.fromName(region))
							.withCredentials(new AWSStaticCredentialsProvider(credentials))
							.build();
				}
				// No credentials in properties file
				else {
					// Check if AWS is currently running on the system
					// If not, getCredentials() method will throw a SdkClientException
					try {
						DefaultAWSCredentialsProviderChain defaultAWSCredentialsProviderChain = new DefaultAWSCredentialsProviderChain();
						defaultAWSCredentialsProviderChain.getCredentials();
					}
					catch (SdkClientException sdkClientException) {
						throw new ValidationException(sdkClientException.getMessage());
					}
					s3Client = AmazonS3ClientBuilder
							.standard()
							.withRegion(Regions.fromName(region))
							.build();
				}

				getSimpleCacheHandler().put("AWS_S3_EXPORT", region, s3Client);
			}
		}
		// For testing using a custom endpoint
		else {
			s3Client = AmazonS3ClientBuilder.standard()
					.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials("foo", "bar")))
					.withClientConfiguration(configureClientToIgnoreInvalidSslCertificates(new ClientConfiguration()))
					.withEndpointConfiguration(getEndpointConfiguration(region, endpointOverride))
					.enablePathStyleAccess()
					.build();
		}

		return s3Client;
	}


	private AmazonS3 getAmazonS3(String region, String accessKey, String secretKey) {
		// Get AmazonS3 client and return the s3Client object.
		return AmazonS3ClientBuilder.standard()
				.withRegion(Regions.fromName(region))
				.withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey)))
				.build();
	}


	private PutObjectRequest createPutObjectRequest(String s3DestinationPath, FileWrapper fileWrapper) {
		AmazonS3URI s3Uri = getAmazonS3Uri(s3DestinationPath);
		String bucketName = s3Uri.getBucket();
		String s3FileKey = getS3FileKey(s3Uri.getKey(), fileWrapper.getFileName());
		validateBucketName(bucketName);

		File file = fileWrapper.getFile().toFile();
		return new PutObjectRequest(bucketName, s3FileKey, file);
	}


	private PutObjectRequest createPutObjectRequest(String s3DestinationPath, FileContainer fileContainer, String fileName, long fileLength) {
		AmazonS3URI s3Uri = getAmazonS3Uri(s3DestinationPath);
		String bucketName = s3Uri.getBucket();
		String s3FileKey = getS3FileKey(s3Uri.getKey(), fileName);
		validateBucketName(bucketName);

		ObjectMetadata meta = new ObjectMetadata();
		meta.setContentLength(fileLength);

		try {
			PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, s3FileKey, new BufferedInputStream(fileContainer.getInputStream(), RequestClientOptions.DEFAULT_STREAM_BUFFER_SIZE), meta);
			putObjectRequest.getRequestClientOptions().setReadLimit(RequestClientOptions.DEFAULT_STREAM_BUFFER_SIZE + 1);
			return putObjectRequest;
		}
		catch (IOException ioException) {
			LogUtils.error(getClass(), ioException.getMessage(), ioException);
			throw new ValidationException(ioException.getMessage(), ioException);
		}
	}


	private void sendFileMultipart(PutObjectRequest putObjectRequest, AmazonS3 amazonS3) {
		// Sets the minPartSize and threshold (Default is 5MB and 16MB, respectively).
		TransferManager tm = TransferManagerBuilder.standard()
				.withS3Client(amazonS3)
				.withMinimumUploadPartSize(MULTIPART_UPLOAD_PART_SIZE)
				.withMultipartUploadThreshold(MULTIPART_UPLOAD_THRESHOLD)
				.build();

		try {
			// TransferManager processes all transfers asynchronously, so this call returns immediately.
			Upload upload = tm.upload(putObjectRequest);

			// Many other methods are dependent on this waiting for the upload to finish before continuing or updating a status to complete.
			upload.waitForCompletion();
			tm.shutdownNow();
		}
		catch (AmazonServiceException amazonServiceException) {
			tm.shutdownNow();
			throw new ValidationException(amazonServiceException.getErrorMessage(), amazonServiceException);
		}
		catch (SdkClientException sdkClientException) {
			tm.shutdownNow();
			throw new ValidationException(sdkClientException.getMessage(), sdkClientException);
		}
		catch (InterruptedException e) {
			Thread.currentThread().interrupt();
			tm.shutdownNow();
			throw new ValidationException(e.getMessage(), e);
		}
	}


	private AwsClientBuilder.EndpointConfiguration getEndpointConfiguration(String region, String endpointOverride) {
		return new AwsClientBuilder.EndpointConfiguration(endpointOverride, region);
	}


	/**
	 * Adjusts the given client configuration to allow the communication with the mock server using
	 * HTTPS, although that one uses a self-signed SSL certificate.
	 *
	 * @param clientConfiguration The {@link ClientConfiguration} to adjust.
	 * @return The adjusted instance.
	 */
	private ClientConfiguration configureClientToIgnoreInvalidSslCertificates(final ClientConfiguration clientConfiguration) {
		try {
			clientConfiguration.getApacheHttpClientConfig()
					.withSslSocketFactory(new SSLConnectionSocketFactory(
							SSLContexts.custom().loadTrustMaterial(new TrustSelfSignedStrategy()).build(),
							NoopHostnameVerifier.INSTANCE));
		}
		catch (final NoSuchAlgorithmException | KeyStoreException | KeyManagementException e) {
			throw new RuntimeException("Exception when configuring S3 Mock client", e);
		}
		return clientConfiguration;
	}



	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getAccessKeyId() {
		return this.accessKeyId;
	}


	public void setAccessKeyId(String accessKeyId) {
		this.accessKeyId = accessKeyId;
	}


	public String getSecretAccessKey() {
		return this.secretAccessKey;
	}


	public void setSecretAccessKey(String secretAccessKey) {
		this.secretAccessKey = secretAccessKey;
	}


	public String getSessionToken() {
		return this.sessionToken;
	}


	public void setSessionToken(String sessionToken) {
		this.sessionToken = sessionToken;
	}


	public SimpleCacheHandler<String, Object> getSimpleCacheHandler() {
		return this.simpleCacheHandler;
	}


	public void setSimpleCacheHandler(SimpleCacheHandler<String, Object> simpleCacheHandler) {
		this.simpleCacheHandler = simpleCacheHandler;
	}
}
