package com.clifton.core.util.event;


/**
 * The <code>Event</code> interface provides a specification for passing event messages to an EventListener.
 *
 * @author rbrooks
 */
public interface Event<T, R> {

	/**
	 * Returns the identifying name of the Event being fired.
	 *
	 * @return an eventName identifying this event
	 */
	public String getEventName();


	/**
	 * Allows customizable information to be saved on the Event object,
	 * so that it may be passed to the EventListener
	 *
	 * @param key   They key for accessing the passed value
	 * @param value The value to be passed to the EventListener
	 */
	public void addContextValue(String key, Object value);


	/**
	 * Returns customizable information about the Event, accessible
	 * to the EventListener
	 *
	 * @param key the key that corresponds to the needed value
	 * @return a value corresponding to the provided key or null if no mapping is found
	 * Note: this method will also return null if the key explicitly maps to null
	 */
	public Object getContextValue(String key);


	/**
	 * Removes information (a key-value mapping) from the event context
	 *
	 * @param key the key indicating the mapping to remove
	 * @return the value associated with the specified key or null if no mapping is found.
	 * Note: this method will also return null if the key explicitly maps to null
	 */
	public Object removeContextValue(String key);


	/**
	 * The target of the event processing.
	 */
	public T getTarget();


	/**
	 * The result of the event processing.
	 */
	public R getResult();


	public void setResult(R result);
}
