package com.clifton.core.util.http.cookie;

import org.apache.commons.lang.SerializationUtils;
import org.apache.http.cookie.Cookie;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>CookieFileReaderWriter</code> will write and read {@link Cookie} to/from a given file path.
 *
 * @author jgommels
 */
class CookieFileReaderWriter {


	private final Path cookieFile;


	public CookieFileReaderWriter(Path cookieFile) {
		this.cookieFile = cookieFile;
	}


	public List<Cookie> load() {
		List<Cookie> persistedList = new ArrayList<>();
		if (this.cookieFile.toFile().exists()) {
			try {
				return loadCookies();
			}
			catch (Exception e) {
				// ignored so it can be recreated outside of try/catch so the stream is closed.
			}
		}

		save(persistedList); // recreate saved file

		return persistedList;
	}


	public void save(List<Cookie> cookieList) {
		Path tempFile = writeToTempFile(cookieList);
		replaceWithFile(tempFile);
	}


	private Path writeToTempFile(List<Cookie> cookieList) {
		Path newFile = createNewTempFile();
		try (FileChannel channel = FileChannel.open(newFile, StandardOpenOption.WRITE, StandardOpenOption.CREATE)) {
			ByteBuffer buf = ByteBuffer.wrap(SerializationUtils.serialize((ArrayList<Cookie>) cookieList));
			channel.write(buf);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred while writing to cookie file", e);
		}
		return newFile;
	}


	private Path createNewTempFile() {
		try {
			createCookieFileDirectories();
			return Files.createTempFile(this.cookieFile.getParent(), this.cookieFile.getFileName().toString(), String.valueOf(System.nanoTime()));
		}
		catch (IOException e) {
			throw new RuntimeException("Unable to create a new temp file.");
		}
	}


	private void createCookieFileDirectories() {
		try {
			Files.createDirectories(this.cookieFile.getParent());
		}
		catch (IOException e) {
			throw new RuntimeException("Error occurred while creating cookie file directories", e);
		}
	}


	@SuppressWarnings("unchecked")
	private synchronized List<Cookie> loadCookies() throws IOException {
		try (BufferedInputStream is = new BufferedInputStream(Files.newInputStream(this.cookieFile))) {
			return ((List<Cookie>) SerializationUtils.deserialize(is));
		}
	}


	private synchronized void replaceWithFile(Path newFile) {
		try {
			Files.move(newFile, this.cookieFile, StandardCopyOption.REPLACE_EXISTING);
		}
		catch (IOException e) {
			throw new RuntimeException("Problem moving/replacing file", e);
		}
	}
}
