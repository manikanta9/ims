package com.clifton.core.util;


import com.clifton.core.beans.SimpleEntity;
import com.clifton.core.context.ContextConventionUtils;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.type.TypeFactory;
import org.aspectj.weaver.NameMangler;
import org.reflections.Reflections;
import org.reflections.scanners.SubTypesScanner;
import org.springframework.aop.aspectj.annotation.AbstractAspectJAdvisorFactory;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.Type;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The {@link CoreClassUtils} class contains helper methods for working with Class File(s).
 *
 * @author manderson
 */
public class CoreClassUtils {

	/**
	 * {@link Reflections} index of all project classes.
	 */
	private static final Reflections PROJECT_CLASS_INDEX = new Reflections(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION, new SubTypesScanner(false));
	private static final TypeFactory DEFAULT_TYPE_FACTORY = TypeFactory.defaultInstance();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/***
	 * Load all {@link SimpleEntity} DTO classes that are not abstract into a {@link Set}.
	 * @return
	 */
	public static Set<Class<?>> getSimpleEntityClassSet() {
		return getProjectClassFiles(SimpleEntity.class)
				.stream().filter(clazz -> !(Modifier.isAbstract(clazz.getModifiers())))
				.collect(Collectors.toSet());
	}


	public static <T> Collection<Class<? extends T>> getProjectClassFiles(Class<T> type) {
		return PROJECT_CLASS_INDEX.getSubTypesOf(type);
	}


	/**
	 * Returns true if there exists a class with the given className.
	 * Returns false otherwise.
	 */
	public static boolean isClassPresent(String className) {
		return org.springframework.util.ClassUtils.isPresent(className, null);
	}


	/**
	 * Returns the class for the given class name. Note that this method
	 * supports primitives and returns the wrapper class for that primitive.
	 */
	public static <T> Class<? extends T> getClass(String className) {
		try {
			@SuppressWarnings("unchecked")
			Class<? extends T> clazz = (Class<? extends T>) org.springframework.util.ClassUtils.forName(className, null);
			return clazz;
			//return Class.forName(className);
		}
		catch (Throwable e) {
			throw new RuntimeException("Error getting Class for class name [" + className + "].", e);
		}
	}


	/**
	 * Returns {@code true} if the given class includes AspectJ markers, indicating that its bytecode has been modified by AspectJ.
	 * <p>
	 * Since AspectJ does not expose an API for determining if a class has been woven, we rely on an internal implementation detail: the injection of class members prefixed with
	 * {@value NameMangler#PREFIX}. This is not guaranteed to be present in woven classes, but will cover most cases. This is a similar approach to those used in {@link
	 * AbstractAspectJAdvisorFactory#compiledByAjc(java.lang.Class)} and in this StackOverflow answer: <a href="https://stackoverflow.com/a/57830373/1941654">"Detect weaved class
	 * by aspectJ with byte-buddy agent"</a>.
	 */
	public static boolean isAspectJWoven(Class<?> clazz) {
		return Stream.concat(Stream.of(clazz.getDeclaredFields()), Stream.of(clazz.getDeclaredMethods()))
				.anyMatch(member -> member.getName().startsWith(NameMangler.PREFIX));
	}


	/**
	 * Gets the canonical name for the given type.
	 * <p>
	 * The <code>type</code> parameter is any {@link Type} instance. This can be a {@link Class}, a {@link Field#getGenericType() field type}, a {@link
	 * Method#getGenericReturnType() method return type}, a {@link Method#getGenericParameterTypes() method parameter type}, or any other {@link Type} representation in the system.
	 * These types retain generic type information at runtime and can be used to provide comprehensive information about declared types.
	 * <p>
	 * The resulting name is typically the same as the fully-qualified type declaration in source code. For example, a {@link Map} of {@link String} keys and {@link List} of {@link
	 * Integer} values would be represented as the following:
	 * <pre>
	 * <code>java.util.Map&lt;java.lang.String, java.util.List&lt;java.lang.Integer&gt;&gt;</code>
	 * </pre>
	 * Notable exceptions exist to the canonical name format for arrays and sub-types of {@link Collection}, {@link Map}, and {@link AtomicReference} with predefined generic types.
	 * These exceptions aid in serialization and deserialization when canonical names are used.
	 * <p>
	 * Arrays are represented using the standard Java {@link Class#getName()} representation (e.g., <code>[Ljava.lang.String;</code>). Compile-time type erasure also always applies
	 * to arrays, meaning that a field of type <code>List&lt;String&gt;[]</code> is always known at runtime as <code>[Ljava.util.List;</code>.
	 * <p>
	 * Sub-types of {@link Collection}, {@link Map}, and {@link AtomicReference} always include their generic parameters for the purpose of deserialization, even if these generic
	 * parameters are predefined in the sub-type. For example, <code>class MyStringList implements List&lt;String&gt;</code> will be represented as
	 * <code>com.package.MyStringList&lt;java.lang.String&gt;</code>, even though <code>java.lang.String</code> is not a generic parameter on the <code>MyStringList</code> class.
	 *
	 * @param type the type to represent as a canonical string
	 * @return the canonical type name, including generic parameters if specified (e.g., <code>java.util.List&lt;java.lang.String&gt;</code>)
	 * @see JavaType#toCanonical()
	 */
	public static String getCanonicalName(Type type) {
		JavaType javaType = DEFAULT_TYPE_FACTORY.constructType(type);
		return javaType.toCanonical();
	}
}
