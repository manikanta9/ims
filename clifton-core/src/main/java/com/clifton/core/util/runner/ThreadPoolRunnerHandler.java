package com.clifton.core.util.runner;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.execution.RunnerCompletionResult;
import com.clifton.core.util.validation.ValidationException;

import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.LongAdder;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * The <code>ThreadPoolRunnerHandler</code> gets a lists of ScheduleBeans from each registered provider
 * and schedules them to run in the thread pool.  The providers are checked based on the period (in seconds),
 * each time the period elapses the run method will be executed.  The run method will get the list of
 * ScheduleBeans, check that the bean is not already scheduled, schedule it if it has not been scheduled and
 * lastly remove any ScheduleBeans that have completed.
 *
 * @author mwacker
 */
public class ThreadPoolRunnerHandler implements RunnerHandler, Runnable {

	/**
	 * The Map of single threaded executors defined by Type for isConcurrentSupported = false
	 * Guarantees running one task at a time, and since defined by type will then support running tasks of the same type in order sequentially
	 */
	private final Map<String, ScheduledExecutorService> typeScheduledExecutorMap = new ConcurrentHashMap<>();
	/**
	 * The period in seconds between checking for new runners.
	 */
	private int period = 60 * 60;
	/**
	 * The number of milliseconds to wait on startup before checking for runners.
	 */
	private long delay = 1000;
	/**
	 * The number of threads to use.  NOTE: This is a set number, if you set it to 5 there will always be 5 threads open.
	 */
	private int numberOfThreads = 10;
	/**
	 * The number of seconds till the next scheduled run that an instant runner can be started.
	 */
	private long rescheduleDelay = 30;
	/**
	 * Flag to enable removing Runners from the collection of scheduled Runners upon completion. Default is true.
	 */
	private boolean autoClearCompletedRunners = true;
	/**
	 * value indication if the controller has been started and not stopped
	 */
	private volatile boolean running;
	private Date lastRunTime; // last time runners where scheduled
	/**
	 * The list of runner providers used to get the runner that need to be scheduled.
	 */
	private List<RunnerProvider<?>> runnerProviders = new CopyOnWriteArrayList<>();
	/**
	 * The Map of corresponding ScheduledFuture objects for each scheduled Runner
	 */
	private final Map<Runner, ScheduledFuture<?>> scheduledRunnerFutures = new ConcurrentHashMap<>();
	/**
	 * The task executor that will execute the run method on this class at a scheduled frequency.
	 */
	private ScheduledFuture<?> reschedulingTaskFuture;

	// Counters for stats
	private final LongAdder canceledRunnerCount = new LongAdder();
	private final LongAdder rescheduledRunnerCount = new LongAdder();
	private final LongAdder unsuccessfulRunnerCount = new LongAdder();
	private final LongAdder completedRunnerCount = new LongAdder();
	private final LongAdder autoClearedCompletedRunnerCount = new LongAdder();
	private final AtomicLong longestRunnerQueuedMillisecondDuration = new AtomicLong(0L);
	private final LongAdder totalRunnerQueuedMillisecondDuration = new LongAdder();
	private final LongAdder totalRunnerRunMillisecondDuration = new LongAdder();

	private static final String RUNNER_PROVIDER_SCHEDULER_RUNNABLE_NAME = "Runner Provider Scheduler";
	private static final String CLEAR_COMPLETED_RUNNER_ACTION_NAME = "Clear Completed Runners Action";

	private final Map<String, RunnerHandlerStatus.RunnerExecutionStatus> runnerExecutionStatusMap = ((Supplier<Map<String, RunnerHandlerStatus.RunnerExecutionStatus>>) () -> {
		Map<String, RunnerHandlerStatus.RunnerExecutionStatus> map = new ConcurrentHashMap<>();
		map.put(RUNNER_PROVIDER_SCHEDULER_RUNNABLE_NAME, new RunnerHandlerStatus.RunnerExecutionStatus(RUNNER_PROVIDER_SCHEDULER_RUNNABLE_NAME));
		map.put(CLEAR_COMPLETED_RUNNER_ACTION_NAME, new RunnerHandlerStatus.RunnerExecutionStatus(CLEAR_COMPLETED_RUNNER_ACTION_NAME));
		return map;
	}).get();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private ApplicationContextService applicationContextService;
	private ContextHandler contextHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public synchronized void start() {
		start(getDelay());
	}


	@Override
	public synchronized void start(long delayMilliseconds) {
		if (isRunning()) {
			throw new IllegalStateException("Cannot start RunnerHandler that is currently running.");
		}

		this.reschedulingTaskFuture = getSingleThreadScheduledExecutorServiceByType("Clifton Runner Provider Handler Executor").scheduleAtFixedRate(this, delayMilliseconds, getPeriod() * 1000, TimeUnit.MILLISECONDS);
		this.running = true;
	}


	@Override
	public synchronized void stop() {
		stop(false);
	}


	@Override
	public synchronized void stop(boolean softStop) {
		// cancel the main rescheduling task
		attemptCancel(null, this.reschedulingTaskFuture, false);

		// shutdown scheduled tasks and removed those canceled or complete
		Iterator<Entry<Runner, ScheduledFuture<?>>> scheduledRunnerIterator = this.scheduledRunnerFutures.entrySet().iterator();
		while (scheduledRunnerIterator.hasNext()) {
			Entry<Runner, ScheduledFuture<?>> entry = scheduledRunnerIterator.next();
			if (attemptCancel(entry.getKey(), entry.getValue(), softStop)) {
				this.canceledRunnerCount.increment();
				scheduledRunnerIterator.remove();
			}
		}

		if (!softStop) {
			// stop the thread pool and any single thread executors
			for (Entry<String, ScheduledExecutorService> executorServiceEntry : this.typeScheduledExecutorMap.entrySet()) {
				shutdownScheduledExecutorService(executorServiceEntry.getKey(), executorServiceEntry.getValue());
			}

			// clear all the variables.
			this.scheduledRunnerFutures.clear();
			this.typeScheduledExecutorMap.clear();
			resetCounters();
		}
		this.lastRunTime = null;
		this.running = false;
	}


	private void resetCounters() {
		this.canceledRunnerCount.reset();
		this.rescheduledRunnerCount.reset();
		this.completedRunnerCount.reset();
		this.unsuccessfulRunnerCount.reset();
		this.autoClearedCompletedRunnerCount.reset();
		this.longestRunnerQueuedMillisecondDuration.set(0L);
		this.totalRunnerQueuedMillisecondDuration.reset();
		this.totalRunnerRunMillisecondDuration.reset();
		this.runnerExecutionStatusMap.values().forEach(RunnerHandlerStatus.RunnerExecutionStatus::reset);
	}


	private void shutdownScheduledExecutorService(String type, ScheduledExecutorService service) {
		if (!ConcurrentUtils.shutdownExecutorService(service, 5, 5)) {
			LogUtils.warn(getClass(), "Could not shutdown for runner type [" + type + "] schedule executor.");
		}
	}


	@Override
	public synchronized void run() {
		long startNanoTime = System.nanoTime();
		boolean firstTime = (this.lastRunTime == null);
		if (firstTime) {
			this.lastRunTime = new Date();
		}

		Date startDate = this.lastRunTime;
		Date endDate = getNextHandlerExecutionDate();

		// to account for day light savings time
		if (endDate.equals(startDate) || endDate.before(startDate)) {
			endDate = DateUtils.addSeconds(endDate, 60 * 60); // add an hour to endDate
		}

		for (RunnerProvider<?> runnerProvider : this.runnerProviders) {
			try {
				LogUtils.info(getClass(), "Scheduling new runs for [" + runnerProvider.getClass().getName() + "].");
				List<Runner> scheduleBeansFromProvider = runnerProvider.getOccurrencesBetween(startDate, endDate);
				for (Runner runner : scheduleBeansFromProvider) {
					LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> "Scheduled new run for [" + runner.getType() + "-" + runner.getTypeId() + "] on [" + DateUtils.fromDate(runner.getRunDate(), DateUtils.DATE_FORMAT_SHORT) + "]."));
					scheduleRunner(runner, false);
				}
			}
			catch (Throwable e) {
				Date runEndDate = endDate;
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Failed to retrieve occurrences for [" + runnerProvider.getClass().getName() + "] between [" + DateUtils.fromDate(startDate, DateUtils.DATE_FORMAT_SHORT)
						+ "] and [" + DateUtils.fromDate(runEndDate, DateUtils.DATE_FORMAT_SHORT) + "]."));
			}
		}
		// clear after scheduling to avoid duplicate tasks
		if (!firstTime) {
			doClearCompletedRunners();
		}
		this.lastRunTime = endDate;
		RunnerHandlerStatus.RunnerExecutionStatus executionStatus = this.runnerExecutionStatusMap.get(RUNNER_PROVIDER_SCHEDULER_RUNNABLE_NAME);
		executionStatus.addExecution(System.nanoTime() - startNanoTime, TimeUnit.NANOSECONDS);
		executionStatus.setNextRunDate(endDate);
	}


	@Override
	public void runNow(Runner runner) {
		for (Runner r : CollectionUtils.getIterable(getScheduledRunnerList(runner.getType(), runner.getTypeId()))) {
			ScheduledFuture<?> sf = this.scheduledRunnerFutures.get(r);
			if (sf != null) {
				long timeUntilRun = sf.getDelay(TimeUnit.SECONDS);
				if (!sf.isDone() && (timeUntilRun < getRescheduleDelay())) {
					if (timeUntilRun > 0) {
						throw new ValidationException("An instant runner for [" + r.getTypeId() + ": " + r.getType() + "] is scheduled to start in " + timeUntilRun + " seconds.");
					}
					throw new ValidationException("An instant runner for [" + r.getTypeId() + ": " + r.getType() + "] is already running.");
				}
			}
		}
		scheduleRunner(runner, true);
	}


	@Override
	public void rescheduleRunnersForProviders() {
		Date startDate = new Date();
		for (RunnerProvider<?> runnerProvider : this.runnerProviders) {
			try {
				LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> "Scheduling new runs for [" + runnerProvider.getClass().getName() + "] between [" + DateUtils.fromDate(startDate, DateUtils.DATE_FORMAT_SHORT)
						+ "] and [" + DateUtils.fromDate(getNextScheduleDate(), DateUtils.DATE_FORMAT_SHORT) + "]."));

				List<Runner> scheduleBeansFromProvider = runnerProvider.getOccurrencesBetween(startDate, getNextScheduleDate());
				rescheduleRunners(scheduleBeansFromProvider);
			}
			catch (Throwable e) {
				Date runEndDate = getNextScheduleDate();
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, () -> "Failed to retrieve occurrences for [" + runnerProvider.getClass().getName() + "] between [" + DateUtils.fromDate(startDate, DateUtils.DATE_FORMAT_SHORT)
						+ "] and [" + DateUtils.fromDate(runEndDate, DateUtils.DATE_FORMAT_SHORT) + "]."));
			}
		}
	}


	@Override
	public void rescheduleRunner(Runner runner) {
		// cancel existing scheduled for same type/id so we can reschedule
		cancelRunners(runner.getType(), runner.getTypeId());

		// Now Schedule the latest Runner
		scheduleRunner(runner, false);
	}


	@Override
	public void rescheduleRunners(List<Runner> runners) {
		if (!CollectionUtils.isEmpty(runners)) {
			//Map of runners where the key is the type
			Map<String, List<Runner>> runnerTypeMap = BeanUtils.getBeansMap(runners, Runner::getType);
			runnerTypeMap.forEach((type, typeRunnerList) -> {
				Map<String, List<Runner>> runnerTypeIdMap = BeanUtils.getBeansMap(typeRunnerList, Runner::getTypeId);
				runnerTypeIdMap.forEach((typeId, typeIdRunnerList) -> rescheduleRunnersOfTypeAndTypeId(type, typeId, typeIdRunnerList));
			});
		}
	}


	/**
	 * Cancels all runners of the given type and typeID and then schedules the runners in the given list
	 */
	private void rescheduleRunnersOfTypeAndTypeId(String type, String typeId, List<Runner> runners) {
		if (!CollectionUtils.isEmpty(runners)) {
			cancelRunners(type, typeId);

			for (Runner runner : CollectionUtils.getIterable(runners)) {
				scheduleRunner(runner, false);
			}
		}
	}


	@Override
	public void cancelRunners(String runnerType) {
		List<Runner> equalRunners = getScheduledRunnerList(runnerType);
		cancelRunners(equalRunners);
	}


	@Override
	public void cancelRunners(String runnerType, String runnerTypeId) {
		List<Runner> equalRunners = getScheduledRunnerList(runnerType, runnerTypeId);
		cancelRunners(equalRunners);
	}


	private void cancelRunners(List<Runner> runners) {
		for (Runner runner : CollectionUtils.getIterable(runners)) {
			cancelRunner(runner);
		}
	}


	private void cancelRunner(Runner runner) {
		ScheduledFuture<?> scheduledFuture = this.scheduledRunnerFutures.get(runner);
		if (scheduledFuture != null) {
			if (attemptCancel(runner, scheduledFuture, false)) {
				this.rescheduledRunnerCount.increment();
				this.scheduledRunnerFutures.remove(runner);
			}
		}
	}


	@Override
	public void terminateRunners(String runnerType, String runnerTypeId) {
		List<Runner> matchedRunners = getScheduledRunnerList(runnerType, runnerTypeId);
		for (Runner runner : CollectionUtils.getIterable(matchedRunners)) {
			if (runner.getRunnerState() == Runner.RunnerStates.RUNNING) {
				runner.getStatus().setTerminationRequested(true);
				LogUtils.warn(getClass(), "Termination request for runner: " + runner);
			}
			else {
				cancelRunner(runner);
			}
		}
	}


	@Override
	public void clearCompletedRunners() {
		doClearCompletedRunners();
	}


	@Override
	public List<Runner> getScheduledRunnerList(String type) {
		return filterScheduledRunnerList(runner -> runner.getType().equals(type));
	}


	@Override
	public List<Runner> getScheduledRunnerList(String type, String typeId) {
		return filterScheduledRunnerList(runner -> runner.getType().equals(type) && runner.getTypeId().equals(typeId));
	}


	@Override
	public List<Runner> getScheduledRunnerList() {
		// copy the list so we don't have access issues
		return filterScheduledRunnerList(runner -> true);
	}


	private List<Runner> filterScheduledRunnerList(Predicate<Runner> filterPredicate) {
		return this.scheduledRunnerFutures.keySet().stream().filter(filterPredicate).collect(Collectors.toList());
	}


	//Calculates the next date/time the handler should execute
	private Date getNextHandlerExecutionDate() {
		return DateUtils.addSeconds(this.lastRunTime, this.getPeriod());
	}


	@Override
	public Date getNextScheduleDate() {
		return this.lastRunTime;
	}


	@Override
	public void addRunnerProvider(RunnerProvider<?> runnerProvider) {
		this.runnerProviders.add(runnerProvider);
	}


	@Override
	public void addRunner(Runner runner) {
		scheduleRunner(runner, false);
	}


	@Override
	public boolean isRunnerWithTypeAndIdActive(String runnerType, String runnerTypeId) {
		List<Runner> runnerList = getScheduledRunnerList(runnerType, runnerTypeId);
		if (!CollectionUtils.isEmpty(runnerList)) {
			Date currentTime = new Date();
			for (Runner runner : CollectionUtils.getIterable(runnerList)) {
				if (DateUtils.isDateAfter(currentTime, runner.getRunDate())) {
					// make sure the runner is not complete
					ScheduledFuture<?> future = this.scheduledRunnerFutures.get(runner);
					if (future != null && !future.isDone()) {
						return true;
					}
				}
			}
		}
		return false;
	}


	/**
	 * Returns a {@link RunnerHandlerStatus} containing statistics of {@link Runner} activity processed through
	 * this handler. The statuses of all {@link java.util.concurrent.ExecutorService}s used to handle the Runners
	 * are included in the response.
	 */
	@Override
	public RunnerHandlerStatus getStatus(RunnerHandlerStatusCommand command) {
		if (command.isClearStatistics()) {
			resetCounters();
		}
		// get a snapshot of reused counters
		long completedRunnerCountValue = this.completedRunnerCount.longValue();
		RunnerHandlerStatus.RunnerHandlerStatusBuilder statusBuilder = new RunnerHandlerStatus.RunnerHandlerStatusBuilder(command)
				.setStarted(isRunning())
				// runner count stats
				.setScheduledRunnerCount(this.scheduledRunnerFutures.size() - CollectionUtils.getStream(this.scheduledRunnerFutures.values()).mapToInt(sf -> sf.isDone() ? 1 : 0).sum())
				.setCompletedRunnerCount(completedRunnerCountValue)
				.setCanceledRunnerCount(this.canceledRunnerCount.longValue())
				.setUnsuccessfulRunnerCount(this.unsuccessfulRunnerCount.longValue())
				.setRescheduledRunnerCount(this.rescheduledRunnerCount.longValue())
				// runner duration stats
				.setLongestQueueMilliseconds(this.longestRunnerQueuedMillisecondDuration.longValue())
				.setAverageQueueMilliseconds(this.totalRunnerQueuedMillisecondDuration.longValue() / (completedRunnerCountValue == 0 ? 1 : completedRunnerCountValue))
				.setAverageRunMilliseconds(this.totalRunnerRunMillisecondDuration.longValue() / (completedRunnerCountValue == 0 ? 1 : completedRunnerCountValue))
				// Runner Handler Executor stats
				.setExecutorCount(this.typeScheduledExecutorMap.size());
		for (Map.Entry<String, ScheduledExecutorService> executorServiceEntry : this.typeScheduledExecutorMap.entrySet()) {
			statusBuilder.addExecutorServiceStatus(new ExecutorServiceStatus(executorServiceEntry.getKey(), executorServiceEntry.getValue()));
		}
		// RunnerHandler maintenance stats
		for (Map.Entry<String, RunnerHandlerStatus.RunnerExecutionStatus> executionStatusEntry : this.runnerExecutionStatusMap.entrySet()) {
			statusBuilder.addRunnerExecutionStatus(executionStatusEntry.getValue());
		}
		return statusBuilder.build();
	}


	@Override
	public void registerExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		String type;
		String typeId;
		if (command.getRunner() != null) {
			type = command.getRunner().getType();
			typeId = command.getRunner().getTypeId();
		}
		else {
			type = command.getType();
			typeId = command.getTypeId();
		}
		String key = getMonitoredRunnerExecutionStatusKey(type, typeId);
		if (key != null) {
			this.runnerExecutionStatusMap.computeIfAbsent(key, RunnerHandlerStatus.RunnerExecutionStatus::new);
		}
	}


	@Override
	public void deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand command) {
		String type;
		String typeId;
		if (command.getRunner() != null) {
			type = command.getRunner().getType();
			typeId = command.getRunner().getTypeId();
		}
		else {
			type = command.getType();
			typeId = command.getTypeId();
		}
		String key = getMonitoredRunnerExecutionStatusKey(type, typeId);
		if (key != null && !StringUtils.equalsAny(key, RUNNER_PROVIDER_SCHEDULER_RUNNABLE_NAME, CLEAR_COMPLETED_RUNNER_ACTION_NAME)) {
			this.runnerExecutionStatusMap.remove(key);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                     Helper Methods                      ///////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Attempts to cancel the scheduled task 5 times and waits 5 seconds between each attempt.
	 * Cancels only canceled or completed tasks. If the task is running, waits for its completion.
	 */
	private boolean attemptCancel(Runner runner, ScheduledFuture<?> scheduledTask, boolean doNotWait) {
		if (scheduledTask.isDone()) {
			// do not cancel if already completed
			return true;
		}
		// cancel but do NOT interrupt a task that is currently running
		boolean cancelled = scheduledTask.cancel(false);
		// check for cancel or completion - isDone will return true if the future's execution is complete or cancelled
		for (int i = 0; i < 5; i++) {
			if (cancelled || scheduledTask.isDone()) {
				return true;
			}
			if (doNotWait) {
				return false;
			}
			try {
				Thread.sleep(5000);
			}
			catch (InterruptedException e) {
				LogUtils.error(getClass(), "Thread was interrupted while waiting to cancel the runner: " + runner, e);
				Thread.currentThread().interrupt();
			}
		}

		if (runner != null) {
			LogUtils.warn(LogCommand.ofMessageSupplier(getClass(), () -> "Could not stop scheduled runner [" + runner.getType() + "] scheduled for [" + (runner.getRunDate() == null ? "IMMEDIATE" : DateUtils.fromDate(runner.getRunDate())) + ". (" + runner.getClass().getName() + ")"));
		}
		else {
			LogUtils.warn(getClass(), "Could not stop scheduled runner.  (" + this.getClass().getName() + ")");
		}
		return false;
	}


	/**
	 * Schedules the specified Runner.  If runNow == true, ignores RunDate and schedules the Runner to run now.
	 */
	private void scheduleRunner(Runner runner, boolean runNow) {
		this.scheduledRunnerFutures.computeIfAbsent(runner, r -> {
			// get the number of seconds to delay.
			long millisecondsDelay = 0;
			if (!runNow) {
				if (runner.getRunDate() != null) {
					millisecondsDelay = DateUtils.getMillisecondsDifference(runner.getRunDate(), new Date());
				}
				if (millisecondsDelay < 0) {
					millisecondsDelay = 0;
				}
			}

			// schedule the run
			Callable<RunnerCompletionResult> runnerWrapper = new RunnerWrapper(runner, getContextHandler().getBean(Context.USER_BEAN_NAME), this::handleRunnerCompletionResult);
			getApplicationContextService().autowireBean(runnerWrapper);
			ScheduledFuture<RunnerCompletionResult> sf;
			if (runner.isConcurrentSupported()) {
				sf = getScheduledExecutorService().schedule(runnerWrapper, millisecondsDelay, TimeUnit.MILLISECONDS);
			}
			else {
				sf = getSingleThreadScheduledExecutorServiceByType(runner.getType()).schedule(runnerWrapper, millisecondsDelay, TimeUnit.MILLISECONDS);
			}
			return sf;
		});
	}


	/**
	 * Clears all completed tasks from the list of scheduled beans.
	 */
	private void doClearCompletedRunners() {
		long startNanoTime = System.nanoTime();
		Iterator<Entry<Runner, ScheduledFuture<?>>> scheduledRunnerFutureIterator = this.scheduledRunnerFutures.entrySet().iterator();
		while (scheduledRunnerFutureIterator.hasNext()) {
			Entry<Runner, ScheduledFuture<?>> entry = scheduledRunnerFutureIterator.next();
			ScheduledFuture<?> future = entry.getValue();
			if (future == null || future.isDone()) {
				// isDone will return true if the future's execution is complete or cancelled
				scheduledRunnerFutureIterator.remove();
			}
		}
		this.runnerExecutionStatusMap.get(CLEAR_COMPLETED_RUNNER_ACTION_NAME).addExecution(System.nanoTime() - startNanoTime, TimeUnit.NANOSECONDS);
	}


	private void handleRunnerCompletionResult(RunnerCompletionResult result) {
		this.completedRunnerCount.increment();
		updateLongestDuration(this.longestRunnerQueuedMillisecondDuration, result.getQueuedDuration(TimeUnit.MILLISECONDS));
		this.totalRunnerQueuedMillisecondDuration.add(result.getQueuedDuration(TimeUnit.MILLISECONDS));
		this.totalRunnerRunMillisecondDuration.add(result.getRunDuration(TimeUnit.MILLISECONDS));
		if (!result.isSuccessful()) {
			this.unsuccessfulRunnerCount.increment();
		}
		updateMonitoredRunnerExecutionStatistics(result);
		if (isAutoClearCompletedRunners() && result.getRunner() != null && StringUtils.equalsAny(result.getRunner().getType(), "BATCH", "EXPORT", "NOTIFICATION")) {
			// Clear completed runners/jobs that have saved history. The others will be cleared on schedule.
			this.scheduledRunnerFutures.remove(result.getRunner());
			this.autoClearedCompletedRunnerCount.increment();
		}
	}


	private void updateMonitoredRunnerExecutionStatistics(RunnerCompletionResult result) {
		Runner runner = result.getRunner();
		if (runner != null) {
			// Check if Runner is monitored and registered by Runner Type and Runner Type ID
			RunnerHandlerStatus.RunnerExecutionStatus status = this.runnerExecutionStatusMap.get(getMonitoredRunnerExecutionStatusKey(runner.getType(), runner.getTypeId()));
			if (status == null) {
				// Check if Runner is monitored and registered by only Runner Type
				status = this.runnerExecutionStatusMap.get(getMonitoredRunnerExecutionStatusKey(runner.getType(), null));
			}
			if (status != null) {
				status.addExecution(result.getRunDuration(TimeUnit.MILLISECONDS), TimeUnit.MILLISECONDS);
			}
		}
	}


	/**
	 * Updates the provided long duration statistic with the provided duration if the provided duration
	 * is greater than the current value of the statistic duration.
	 */
	private void updateLongestDuration(AtomicLong stat, long duration) {
		long currentValue = stat.get();
		if (duration > currentValue) {
			if (!stat.compareAndSet(currentValue, duration)) {
				// comparison for current value did not match, try again.
				updateLongestDuration(stat, duration);
			}
		}
	}


	private String getMonitoredRunnerExecutionStatusKey(String type, String typeId) {
		if (type != null) {
			StringBuilder builder = new StringBuilder(type);
			if (typeId != null) {
				builder.append(" (").append(typeId).append(')');
			}
			return builder.toString();
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	private ScheduledExecutorService getScheduledExecutorService() {
		return this.typeScheduledExecutorMap.computeIfAbsent("Clifton Runner Thread Pool Executor", runnerType -> ConcurrentUtils.newScheduledExecutorService(runnerType, getNumberOfThreads()));
	}


	private ScheduledExecutorService getSingleThreadScheduledExecutorServiceByType(String type) {
		// Not using the single thread factory method because it wraps the executor with a delegate,
		// which makes pulling data out of the executor/thread pool impossible.
		return this.typeScheduledExecutorMap.computeIfAbsent(type, ConcurrentUtils::newSingleThreadScheduledExecutorService);
	}


	public int getPeriod() {
		return this.period;
	}


	public void setPeriod(int period) {
		this.period = period;
	}


	public long getDelay() {
		return this.delay;
	}


	public void setDelay(long delay) {
		this.delay = delay;
	}


	public int getNumberOfThreads() {
		return this.numberOfThreads;
	}


	public void setNumberOfThreads(int numberOfThreads) {
		this.numberOfThreads = numberOfThreads;
	}


	@Override
	public boolean isRunning() {
		return this.running;
	}


	public List<RunnerProvider<?>> getScheduleProviders() {
		return this.runnerProviders;
	}


	public void setScheduleProviders(List<RunnerProvider<?>> runnerProviders) {
		this.runnerProviders = runnerProviders;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public long getRescheduleDelay() {
		return this.rescheduleDelay;
	}


	public void setRescheduleDelay(long rescheduleDelay) {
		this.rescheduleDelay = rescheduleDelay;
	}


	public boolean isAutoClearCompletedRunners() {
		return this.autoClearCompletedRunners;
	}


	public void setAutoClearCompletedRunners(boolean autoClearCompletedRunners) {
		this.autoClearCompletedRunners = autoClearCompletedRunners;
	}
}
