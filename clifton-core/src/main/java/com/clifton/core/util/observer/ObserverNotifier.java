package com.clifton.core.util.observer;


/**
 * The <code>ObserverNotifier</code> interface ...
 *
 * @author vgomelsky
 */
public interface ObserverNotifier {

	// empty for now
}
