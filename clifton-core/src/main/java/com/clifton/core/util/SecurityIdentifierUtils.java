package com.clifton.core.util;

import com.clifton.core.util.validation.FieldValidationException;
import org.apache.commons.validator.routines.CodeValidator;
import org.apache.commons.validator.routines.checkdigit.CUSIPCheckDigit;
import org.apache.commons.validator.routines.checkdigit.ISINCheckDigit;
import org.apache.commons.validator.routines.checkdigit.SedolCheckDigit;

import java.time.LocalDate;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * <code>SecurityIdentifierUtils</code> class defines commonly used investment security utility methods.
 * Contains investment security utility methods for use by modules that do not want a dependency on the
 * investment module.
 *
 * @author TerryS
 */
public class SecurityIdentifierUtils {

	/**
	 * According to the OCC Symbology, the symbol is broken down into:
	 * <br/>-Symbol (6 characters for symbol, 5 for OPRA)
	 * <br/>-Year (2 digits)
	 * <br/>-Month (2 digits, may be 1 Put/Call month code (Calls: A-L, Puts: M-X) for OPRA)
	 * <br/>-Day (2 digits)
	 * <br/>-Put/Call (1 character, or 0 when included with month for OPRA)
	 * <br/>-Strike Dollar (5 digits representing left of decimal separator, OPRA uses 6)
	 * <br/>-Strike Decimal (3 digits representing right of decimal separator, OPRA uses 1)
	 * <p>
	 * This regular expression pattern will match the following symbology:
	 * <br/>- OCC default (Symbol(6)Year(2)Month(2)Day(2)PutCall(1)Dollar(5)Decimal(3))
	 * <br/>- OPRA (Symbol(5)Year(2)PutCallMonth(1)Day(2)Dollar(6)Decimal(1))
	 */
	private static final Pattern OCC_SYMBOL_PATTERN = Pattern.compile("^(?<Symbol>[A-Z0-9\\s]{6})(?<Year>[\\d]{2})(?<Month>[\\d]{2})(?<Day>[\\d]{2})(?<PutCall>[C|P])(?<StrikePrice>[\\d]{8})$");
	private static final Pattern OCC_SYMBOL_FLEXIBLE_DATE_PATTERN = Pattern.compile("^(?<Symbol>[A-Z0-9\\s]{3,6})(?<Year>[\\d]{2})(?<Month>[\\d]{2})(?<Day>[\\d]{2})(?<PutCall>[C|P])(?<StrikePrice>.*)$");
	private static final Pattern OCC_OPRA_SYMBOL_PATTERN = Pattern.compile("^(?<Symbol>[A-Z0-9\\s]{5})(?<Year>[\\d]{2})(?<Month>[A-X])(?<Day>[\\d]{2})(?<StrikePrice>[\\d]{7})$");
	private static final Pattern OCC_OPRA_SYMBOL_FLEXIBLE_DATE_PATTERN = Pattern.compile("^(?<Symbol>[A-Z0-9\\s]{3,5})(?<Year>[\\d]{2})(?<Month>[A-X])(?<Day>[\\d]{2})(?<StrikePrice>.*)$");

	//Validation for CINS when we move out to separate field
	// private static final String CINS_REGEX = "([a-zA-Z0-9]{9})";
	// private static final CodeValidator CINS_VALIDATOR = new CodeValidator(CINS_REGEX, 9, CUSIPCheckDigit.CUSIP_CHECK_DIGIT);

	//This is technically the proper regex for cusip - but until we separate out CINS/RedCode we are more lenient on the validation
	// private static final String CUSIP_REGEX = "([0-9]{3}[a-zA-Z0-9]{6})";
	private static final String CUSIP_REGEX = "([a-zA-Z0-9]{9})";
	private static final CodeValidator CUSIP_VALIDATOR = new CodeValidator(CUSIP_REGEX, 9, CUSIPCheckDigit.CUSIP_CHECK_DIGIT);

	private static final String ISIN_REGEX = "([A-Z]{2}[A-Z0-9]{9}[0-9])";
	private static final CodeValidator ISIN_VALIDATOR = new CodeValidator(ISIN_REGEX, 12, ISINCheckDigit.ISIN_CHECK_DIGIT);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if the specified CUSIP is valid.
	 * CUSIP Numbers are 9 character alphanumeric codes used to identify North American Securities.
	 * Throws corresponding ValidationException if exceptionIfNotValid == true and the specified CUSIP is invalid.
	 * Returns false otherwise.
	 */
	public static boolean isValidCUSIP(String cusip, boolean exceptionIfNotValid) {
		boolean isValid = CUSIP_VALIDATOR.isValid(cusip);
		if (!isValid && exceptionIfNotValid) {
			throw new FieldValidationException(cusip + " is not a valid CUSIP.", "cusip");
		}
		return isValid;
	}


	/**
	 * Returns true if the specified ISIN is valid.
	 * ISIN (International Securities Identifying Number) Numbers are 12 character alphanumeric codes used to identify Securities.
	 * Throws corresponding ValidationException if exceptionIfNotValid == true and the specified ISIN is invalid.
	 * Returns false otherwise.
	 */
	public static boolean isValidISIN(String isin, boolean exceptionIfNotValid) {
		boolean isValid = ISIN_VALIDATOR.isValid(isin);
		if (!isValid && exceptionIfNotValid) {
			throw new FieldValidationException(isin + " is not a valid ISIN.", "isin");
		}
		return isValid;
	}


	/**
	 * Returns true if the specified SEDOL is valid.
	 * SEDOLs are seven characters in length, consisting of two parts: a six-place alphanumeric code and a trailing check digit.
	 * The check digit is computed using a weighted sum of the first six characters. Letters have the value of 9 plus their
	 * alphabet position, such that B = 11 and Z = 35.
	 * Throws corresponding ValidationException if exceptionIfNotValid == true and the specified SEDOL is invalid.
	 * Returns false otherwise.
	 */
	public static boolean isValidSedol(String sedol, boolean exceptionIfNotValid) {
		// NOTE: doesn't validate Country Code (first 2 characters).  Once we upgrade to commons-validator:1.7.0 we can refactor to use ISINValidator class.
		boolean isValid = SedolCheckDigit.SEDOL_CHECK_DIGIT.isValid(sedol);
		if (!isValid && exceptionIfNotValid) {
			throw new FieldValidationException(sedol + " is not a valid SEDOL.", "sedol");
		}
		return isValid;
	}


	/**
	 * Returns true if the specified OCC Symbol is valid.
	 * Throws corresponding ValidationException if exceptionIfNotValid == true and the specified OCC Symbol is invalid.
	 * Returns false otherwise.
	 */
	public static boolean isValidOccSymbol(String occSymbol, boolean exceptionIfNotValid) {
		// do not validate empty field
		if (!StringUtils.isEmpty(occSymbol)) {
			occSymbol = occSymbol.toUpperCase();
			Matcher matcher = OCC_SYMBOL_PATTERN.matcher(occSymbol);
			if (!matcher.matches() && !OCC_OPRA_SYMBOL_PATTERN.matcher(occSymbol).matches()) {
				if (!exceptionIfNotValid) {
					return false;
				}
				if (occSymbol.length() > 23) { // should it be 21???
					throw new FieldValidationException(occSymbol + " is not a valid OCC Symbol because it cannot be more than 23 characters long.", "occSymbol");
				}
				if (occSymbol.length() < 17) {
					throw new FieldValidationException(occSymbol + " is not a valid OCC Symbol because it cannot be less than 17 characters long.", "occSymbol");
				}
				char ch = occSymbol.charAt(occSymbol.length() - 9);
				if (ch != 'P' && ch != 'C') {
					throw new FieldValidationException(occSymbol + " is not a valid OCC Symbol because 9th character from the end is not P or C.", "occSymbol");
				}
				String strikePriceString = occSymbol.substring(occSymbol.length() - 8);
				if (strikePriceString.contains(".")) {
					throw new FieldValidationException(occSymbol + " is not a valid OCC Symbol because the strike [" + strikePriceString + "] contains a decimal separator.", "occSymbol");
				}
				throw new FieldValidationException(occSymbol + " is not a valid OCC Symbol of either of these formats: Symbol(6)Year(2)Month(2)Day(2)PutCall(1)Dollar(5)Decimal(3) or Symbol(5)Year(2)PutCallMonth(1)Day(2)Dollar(6)Decimal(1). Examples: \"SPXPM 170120C02315000\" or \"SPXPM17A200023150\"", "occSymbol");
			}
		}
		return true;
	}


	/**
	 * Provided with a valid OCC Symbol, including OPRA-formatted, extract the expiration date.
	 * <p>
	 * OPRA-formatted OCC Symbols combine the PUT/CALl and Month columns using a single letter in the range A-L for CALL months and M-X for PUT months where A and M = January.
	 */
	public static LocalDate getOccSymbolExpirationDate(String occSymbol, boolean exceptionIfNotValid) {
		if (StringUtils.isEmpty(occSymbol)) {
			return null;
		}
		Matcher matcher = OCC_SYMBOL_PATTERN.matcher(occSymbol).matches() ? OCC_SYMBOL_PATTERN.matcher(occSymbol) : OCC_OPRA_SYMBOL_PATTERN.matcher(occSymbol);
		return getOccSymbolExpirationDate(matcher, exceptionIfNotValid);
	}


	public static LocalDate getOccSymbolExpirationDateFlexible(String occSymbol, boolean exceptionIfNotValid) {
		if (StringUtils.isEmpty(occSymbol)) {
			return null;
		}
		Matcher matcher = OCC_SYMBOL_FLEXIBLE_DATE_PATTERN.matcher(occSymbol).matches() ? OCC_SYMBOL_FLEXIBLE_DATE_PATTERN.matcher(occSymbol) : OCC_OPRA_SYMBOL_FLEXIBLE_DATE_PATTERN.matcher(occSymbol);
		return getOccSymbolExpirationDate(matcher, exceptionIfNotValid);
	}


	private static LocalDate getOccSymbolExpirationDate(Matcher matcher, boolean exceptionIfNotValid) {
		LocalDate results = null;
		if (matcher.matches()) {
			try {
				int year = Integer.parseInt(matcher.group("Year"));
				String month = matcher.group("Month");
				int day = Integer.parseInt(matcher.group("Day"));

				// Unicode code point
				int code = month.codePointAt(0);
				if (code > "9".codePointAt(0)) {
					// CALL uses letters A – L and PUT uses letters M – X.
					month = Integer.toString(code - (code <= "L".codePointAt(0) ? "A".codePointAt(0) - 1 : "L".codePointAt(0)));
				}
				return LocalDate.of((LocalDate.now().getYear() / 100) * 100 + year, Integer.parseInt(month), day);
			}
			catch (Exception e) {
				if (exceptionIfNotValid) {
					throw e;
				}
			}
		}
		return results;
	}
}
