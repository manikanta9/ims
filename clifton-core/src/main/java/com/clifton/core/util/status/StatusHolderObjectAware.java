package com.clifton.core.util.status;


/**
 * The <code>StatusHolderObjectAware</code> interface should be implemented by objects that want
 * to update "status" field implemented via StatusHolderObject interface.
 * <p>
 * For example, a batch job may want to implement this interface in order to provide periodic
 * status updates of a long run: processed 100 out of 500, etc.
 *
 * @param <T>
 * @author vgomelsky
 */
public interface StatusHolderObjectAware<T> {

	/**
	 * Sets the StatusHolderObject which can be used later to update the status field.
	 */
	public void setStatusHolderObject(StatusHolderObject<T> statusHolderObject);
}
