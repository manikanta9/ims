package com.clifton.core.util.service;


import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;


/**
 * The <code>CoreUtilService</code> defines methods that will be used by a web application to call specific Util functions.
 *
 * @author mwacker
 */
public interface CoreUtilService {

	@ResponseBody
	@SecureMethod(disableSecurity = true)
	public Integer getCoreDateUtilDaysDifference(Date dateOne, Date dateTwo);
}
