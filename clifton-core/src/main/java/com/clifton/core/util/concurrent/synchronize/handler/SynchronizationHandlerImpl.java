package com.clifton.core.util.concurrent.synchronize.handler;

import com.clifton.core.beans.LabeledObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.concurrent.LockBusyException;
import com.clifton.core.util.concurrent.synchronize.Synchronizable;
import com.clifton.core.util.concurrent.synchronize.SynchronizableCallableAction;
import com.clifton.core.util.concurrent.synchronize.SynchronizableExecutableAction;
import com.clifton.core.util.date.DateUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;
import java.util.function.Consumer;


/**
 * @author vgomelsky
 */
@Component
public class SynchronizationHandlerImpl implements SynchronizationHandler {

	private ContextHandler contextHandler;

	/**
	 * Main key is the unique name of each secure area.
	 * The value is a Map of unique lock names for that specific secure area with the value of user friendly message.
	 */
	private Map<String, Map<String, FairLock>> lockAreaMap = new ConcurrentHashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void execute(SynchronizableExecutableAction synchronizableExecutableAction) {
		if (synchronizableExecutableAction.getExecutableAction() != null) {
			List<FairLock> obtainedLocks = lock(synchronizableExecutableAction);
			try {
				synchronizableExecutableAction.getExecutableAction().execute();
			}
			finally {
				unlock(synchronizableExecutableAction, obtainedLocks);
			}
		}
	}


	@Override
	public <V> V call(SynchronizableCallableAction<V> synchronizableCallableAction) {
		if (synchronizableCallableAction.getCallableAction() != null) {
			List<FairLock> obtainedLocks = lock(synchronizableCallableAction);
			try {
				return synchronizableCallableAction.getCallableAction().call();
			}
			finally {
				unlock(synchronizableCallableAction, obtainedLocks);
			}
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Helper Methods                      /////////////
	////////////////////////////////////////////////////////////////////////////


	private String getActionLockMessage(Synchronizable synchronizable) {
		LabeledObject currentUser = (LabeledObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
		return new StringBuilder(synchronizable.getSecureAreaName()).append(": ").append(((currentUser == null) ? "Unknown User" : currentUser.getLabel()))
				.append(" locked [").append(synchronizable.getLockMessage()).append("] at ").append(DateUtils.fromDate(new Date())).toString();
	}


	private List<FairLock> lock(Synchronizable synchronizable) {
		String lockMessage = getActionLockMessage(synchronizable);
		Map<String, FairLock> secureAreaLockMap = getLockMapForSecureArea(synchronizable.getSecureAreaName());
		List<FairLock> obtainedLocks = new ArrayList<>();
		for (String lockKey : CollectionUtils.getIterable(synchronizable.getLockKeySet())) {
			if (lockKey != null) {
				try {
					FairLock lock = CollectionUtils.getValue(secureAreaLockMap, lockKey, () -> new FairLock(lockKey));
					// try to lock
					lock.tryLock(lockMessage, synchronizable.getMillisecondsToWaitIfBusy());
					obtainedLocks.add(lock);
				}
				catch (Throwable e) {
					// unlock the obtained locks
					unlock(synchronizable, obtainedLocks);
					String message = new StringBuilder("Failed to obtain lock with secure area [").append(synchronizable.getSecureAreaName()).append("] and key [").append(lockKey).append("]")
							.append(" because ").append(ExceptionUtils.getOriginalMessage(e)).toString();
					LogUtils.warn(getClass(), message, e);
					throw new LockBusyException(message);
				}
			}
		}
		return obtainedLocks;
	}


	private void unlock(Synchronizable synchronizable, List<FairLock> locksToRelease) {
		Map<String, FairLock> secureAreaLockMap = getLockMapForSecureArea(synchronizable.getSecureAreaName());
		for (FairLock lock : CollectionUtils.getIterable(locksToRelease)) {
			if (lock != null) {
				try {
					lock.unlock(fairLock -> secureAreaLockMap.remove(fairLock.getLockKey()));
				}
				catch (Throwable t) {
					// Ignore to ensure all locks are unlocked and to avoid masking an exception from this thread's executed action
				}
			}
		}
	}


	private Map<String, FairLock> getLockMapForSecureArea(String secureAreaName) {
		return CollectionUtils.getValue(this.lockAreaMap, secureAreaName, ConcurrentHashMap::new);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	////////////////////////////////////////////////////////////////////////////
	/////////////                 Member Classes                   /////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Reentrant lock implementation that honors fair locking access and allows only a single thread to obtain it.
	 */
	private static final class FairLock {

		private final ReentrantLock reentrantLock = new ReentrantLock(true);
		private final String lockKey;
		private volatile String lockMessage;


		private FairLock(String lockKey) {
			this.lockKey = lockKey;
		}


		public String getLockKey() {
			return this.lockKey;
		}


		/**
		 * Attempts to lock this lock reference within the provided millisecond wait threshold. This method will return
		 * when the lock is obtained, which will be immediately if no other thread has the lock. If the lock
		 * could not be obtained within the provided wait threshold a {@link LockBusyException} will be thrown.
		 *
		 * @param lockMessage              the lock message to use on this lock describing the owner thread
		 * @param millisecondsToWaitIfBusy the duration to wait if the lock is not immediately available, can be 0
		 * @throws LockBusyException if the lock could not be obtained within the provided wait threshold
		 */
		public void tryLock(String lockMessage, int millisecondsToWaitIfBusy) {
			try {
				if (this.reentrantLock.tryLock(millisecondsToWaitIfBusy, TimeUnit.MILLISECONDS)) {
					this.lockMessage = lockMessage;
				}
				else {
					// already locked by someone else: notify the caller with details
					throw new LockBusyException(this.lockMessage);
				}
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
				throw new LockBusyException("Thread was interrupted while attempting to lock [" + lockMessage + "]");
			}
		}


		/**
		 * Unlocks this lock reference if the current thread owns the lock. If there is cleanup activity required
		 * upon the release of the lock, such as removing the lock from cache to avoid excess memory usage, an
		 * optional unlock action {@link Consumer} can be provided. The unlock action will be invoked with this lock
		 * as an argument before releasing the lock only if there is not another thread queued to use this lock.
		 */
		public void unlock(Consumer<FairLock> unlockAction) {
			if (this.reentrantLock.isHeldByCurrentThread()) {
				if (unlockAction != null && this.reentrantLock.getHoldCount() < 2 && !this.reentrantLock.hasQueuedThreads()) {
					unlockAction.accept(this);
				}
				this.reentrantLock.unlock();
			}
		}
	}
}
