package com.clifton.core.util.observer;


import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.factory.InitializingBean;

import java.util.HashSet;
import java.util.Set;


/**
 * The <code>ObserverRegistrator</code> class should be used to register {@link Observer} objects.
 * The best way to register observers is usually from Spring's context.
 * <p/>
 * <p>"eventTypeSet" property is optional. If not set, all interfaces of the specified observer that extend {@link Observer} are registered.</p>
 * <p/>
 * Example (<b>make sure to inject the singleton observer instance</b>):<pre>{@code
 * <bean id="uniqueIdThatStartsWithProjectName" class="com.clifton.core.util.observer.ObserverRegistrator" lazy-init="false">
 *     <property name="observer" ref="springManagedBeanThatImplementsObserverInterface" />
 * </bean>
 * }</pre>
 *
 * @param <T>
 * @author vgomelsky
 */
public class ObserverRegistrator<T extends Observer> implements InitializingBean {

	private T observer;
	private Set<Class<T>> eventTypeSet;

	private ObserverHandler observerHandler;


	/**
	 * Registers an observer for the specified event(s). If not events are specified,
	 * registers for all interfaces of this class that extend {@link Observer}.
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void afterPropertiesSet() {
		AssertUtils.assertNotNull(getObserver(), "Required property 'observer' is null.");
		Set<Class<T>> typeSet = getEventTypeSet();
		if (CollectionUtils.isEmpty(typeSet)) {
			typeSet = new HashSet<>();
			for (Class<?> clazz : getObserver().getClass().getInterfaces()) {
				if (Observer.class.isAssignableFrom(clazz)) {
					typeSet.add((Class<T>) clazz);
				}
			}
		}
		for (Class<T> eventType : typeSet) {
			getObserverHandler().register(eventType, getObserver());
		}
	}


	/**
	 * @return the observer
	 */
	public T getObserver() {
		return this.observer;
	}


	/**
	 * @param observer the observer to set
	 */
	public void setObserver(T observer) {
		this.observer = observer;
	}


	/**
	 * @return the eventTypeSet
	 */
	public Set<Class<T>> getEventTypeSet() {
		return this.eventTypeSet;
	}


	/**
	 * @param eventTypeSet the eventTypeSet to set
	 */
	public void setObserverTypeSet(Set<Class<T>> eventTypeSet) {
		this.eventTypeSet = eventTypeSet;
	}


	/**
	 * @return the observerHandler
	 */
	public ObserverHandler getObserverHandler() {
		return this.observerHandler;
	}


	/**
	 * @param observerHandler the observerHandler to set
	 */
	public void setObserverHandler(ObserverHandler observerHandler) {
		this.observerHandler = observerHandler;
	}
}
