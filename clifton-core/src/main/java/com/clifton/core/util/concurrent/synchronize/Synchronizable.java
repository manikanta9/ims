package com.clifton.core.util.concurrent.synchronize;

import com.clifton.core.util.concurrent.LockBusyException;

import java.util.Set;


/**
 * <code>Synchronizable</code> defines criteria used for obtaining one or more exclusive lock. The required attributes
 * include: {@link #getSecureAreaName()}, {@link #getLockKeySet()}, and {@link #getLockMessage()}.
 *
 * @author NickK
 * @see SynchronizableBuilder
 * @see AbstractSynchronizable
 */
public interface Synchronizable {

	/**
	 * Return the globally unique name for an area (block of code or database table(s)) being secured by locks.
	 * <p>
	 * This property is required in order to obtain a lock.
	 */
	public String getSecureAreaName();


	/**
	 * Returns the set containing one or more lock key(s) identifying the sub-section(s) of the specified secure area that should be secured.
	 * <p>
	 * This property is required in order to obtain a lock.
	 */
	public Set<String> getLockKeySet();


	/**
	 * Returns the user friendly message describing what's been locked. This message will be used to notify other callers when the lock is busy.
	 * <p>
	 * This property is required in order to obtain a lock.
	 */
	public String getLockMessage();


	/**
	 * Returns the duration the calling thread is willing to wait to obtain the lock for executing the action before a {@link LockBusyException} is thrown.
	 * <p>
	 * This property is optional and ignored if the value is 0.
	 */
	public int getMillisecondsToWaitIfBusy();
}
