package com.clifton.core.util.date;

import java.util.function.Supplier;


/**
 * The {@code TimeTrackedOperation} is a wrapper object for operations whose duration is tracked. This object encapsulates the result of the operation ({@link #result}) and the
 * duration elapsed for the operation ({@link #elapsed}).
 *
 * @param <T> the result type for the operation
 * @author MikeH
 * @see TimeUtils#getElapsedTime(Runnable)
 * @see TimeUtils#getElapsedTime(Supplier)
 */
public class TimeTrackedOperation<T> {

	/**
	 * The result from the operation.
	 */
	private final T result;
	/**
	 * The elapsed time for the action in nanoseconds.
	 */
	private final long elapsed;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TimeTrackedOperation(T result, long elapsed) {
		this.result = result;
		this.elapsed = elapsed;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the elapsed time for the operation in nanoseconds.
	 */
	public long getElapsedNanoseconds() {
		return this.elapsed;
	}


	/**
	 * Gets the elapsed time for the operation in milliseconds.
	 */
	public long getElapsedMilliseconds() {
		return TimeUtils.convertNanoToMilli(getElapsedNanoseconds()).longValue();
	}


	/**
	 * Gets the elapsed time for the operation in seconds as a string using the format {@link TimeUtils#NUMBER_FORMAT_SECONDS_PRECISE}.
	 */
	public String getElapsedSeconds() {
		return TimeUtils.NUMBER_FORMAT_SECONDS_PRECISE.format(TimeUtils.convertNanoToSeconds(getElapsedNanoseconds()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public T getResult() {
		return this.result;
	}


	public long getElapsed() {
		return this.elapsed;
	}
}
