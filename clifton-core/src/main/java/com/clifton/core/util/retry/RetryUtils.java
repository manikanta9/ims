package com.clifton.core.util.retry;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;


/**
 * Utilities for retrying things
 *
 * @author theodorez
 */
public class RetryUtils {


	/**
	 * Creates a task using the given scheduledExecutorService that will check the queue at the interval
	 * specified by retryDelaySeconds and timeUnit. (Note that this time is from termination to next execution)
	 * <p>
	 * If the queue has CompletableFutures, a snapshot of the queue at the moment in time will be taken and each
	 * CompletableFuture analyzed. If the future has completed, its result will be passed to the Consumer specified.
	 * <p>
	 * NOTE: This method assumes that each CompletableFuture in the Queue will each return a LIST of results
	 *
	 * @param scheduledExecutorService The ScheduledExecutorService to use
	 * @param queue                    Queue of completable futures
	 * @param resultProcessor          Executes when a future in the queue is completed
	 * @param <T>                      The result type
	 */
	public static <T> ScheduledFuture<?> getFutureMonitoringFuture(ScheduledExecutorService scheduledExecutorService, BlockingQueue<RetryFuture<T>> queue,
	                                                               Consumer<List<T>> resultProcessor, int retryDelaySeconds) {
		return scheduledExecutorService.scheduleWithFixedDelay(new Runnable() {
			@Override
			public void run() {
				LogUtils.debug(RetryUtils.class, "Checking for available futures - " + System.currentTimeMillis());
				try {
					List<RetryFuture<T>> futures = new ArrayList<>();
					//Looks at ALL futures currently in the queue at this moment in time
					queue.drainTo(futures);

					for (RetryFuture<T> future : CollectionUtils.getIterable(futures)) {
						processFuture(future);
					}
				}
				catch (Exception e) {
					//Unexpected exception occurred
					LogUtils.error(RetryUtils.class, "Error occurred in future monitoring thread.", e);
				}
			}


			void processFuture(RetryFuture<T> future) {
				boolean done = false;
				try {
					done = future.isDone();
					if (done) {
						List<T> results = future.get();
						resultProcessor.accept(results);
					}
				}
				catch (InterruptedException e) {
					Thread.currentThread().interrupt();
				}
				catch (Exception e) {
					LogUtils.error(RetryUtils.class, "Error occurred when processing future result", e);
				}
				finally {
					if (!done) {
						queue.add(future);
					}
				}
			}
		}, retryDelaySeconds, retryDelaySeconds, TimeUnit.SECONDS);
	}
}
