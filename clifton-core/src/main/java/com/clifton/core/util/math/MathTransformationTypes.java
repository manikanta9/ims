package com.clifton.core.util.math;

import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.function.UnaryOperator;


/**
 * A selection of numerical transformation types. These transformations may be applied to numeric values.
 * <p>
 * These transformations are unary operators and may be performed on any single numeric value. For example, {@link #NEGATE} may be used to transform a value to its negated value,
 * or {@link #ABSOLUTE_VALUE} may be used to transform a value to its absolute value.
 *
 * @author MikeH
 */
public enum MathTransformationTypes {

	NONE(UnaryOperator.identity(), "do not transform value"),
	NEGATE(MathUtils::negate, "use negated value"),
	ABSOLUTE_VALUE(MathUtils::abs, "use absolute value");


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final UnaryOperator<BigDecimal> operator;
	private final String label;


	MathTransformationTypes(UnaryOperator<BigDecimal> operator, String label) {
		this.operator = operator;
		this.label = label;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Transforms the given value according to the given transformation type.
	 *
	 * @param value the value to transform
	 * @return the transformed value
	 */
	public BigDecimal transform(BigDecimal value) {
		return this.operator.apply(value);
	}


	@Override
	public String toString() {
		return this.label;
	}
}
