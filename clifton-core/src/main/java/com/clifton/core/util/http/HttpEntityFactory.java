package com.clifton.core.util.http;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicNameValuePair;

import java.io.UnsupportedEncodingException;
import java.nio.charset.UnsupportedCharsetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>HttpEntityFactory</code> provides convenient static factory methods for creating {@link HttpEntity} instances. {@HttpEntity} instances
 * can be used as the content for HTTP POSTs and PUTs.
 *
 * @author jgommels
 */
public class HttpEntityFactory {

	public static HttpEntity createHttpEntity(String content) {
		try {
			return new StringEntity(content);
		}
		catch (UnsupportedEncodingException e) {
			throw new HttpException("Problem creating HttpEntity with content [" + content + "]", e);
		}
	}


	public static HttpEntity createHttpEntity(String content, ContentType contentType) {
		try {
			return new StringEntity(content, contentType);
		}
		catch (UnsupportedCharsetException e) {
			throw new HttpException("Problem creating HttpEntity with content [" + content + "] and contentType [" + contentType + "]", e);
		}
	}


	public static HttpEntity createHttpEntity(Map<String, String> values) {
		List<NameValuePair> nameValuePairs = new ArrayList<>();
		for (Map.Entry<String, String> stringStringEntry : values.entrySet()) {
			nameValuePairs.add(new BasicNameValuePair(stringStringEntry.getKey(), stringStringEntry.getValue()));
		}

		return createHttpEntity(nameValuePairs);
	}


	public static HttpEntity createHttpEntity(List<NameValuePair> values) {
		try {
			return new UrlEncodedFormEntity(values);
		}
		catch (UnsupportedEncodingException e) {
			throw new HttpException("Problem creating HttpEntity with values [" + values.toString() + "]", e);
		}
	}
}
