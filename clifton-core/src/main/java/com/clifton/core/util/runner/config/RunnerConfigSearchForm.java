package com.clifton.core.util.runner.config;


import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


@SearchForm(hasOrmDtoClass = false)
public class RunnerConfigSearchForm extends BaseAuditableEntitySearchForm {

	private String type;
	private String typeId;
	private String className;


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getTypeId() {
		return this.typeId;
	}


	public void setTypeId(String typeId) {
		this.typeId = typeId;
	}


	public String getClassName() {
		return this.className;
	}


	public void setClassName(String className) {
		this.className = className;
	}
}
