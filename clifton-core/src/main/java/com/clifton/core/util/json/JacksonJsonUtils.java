package com.clifton.core.util.json;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;

import java.io.StringWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>JacksonJsonUtils</code> allows for manually parsing a json string to an argument or a list of arguments
 * at a specific depth or up to a maximum depth of 10.
 * <p>
 * This is used in specific cases where it is faster to parse the string directly rather than serializing the entire object.
 *
 * @author stevenf
 */
public class JacksonJsonUtils {

	private static final int MAX_DEPTH = 10;


	public static String getJsonArgument(String parameterName, String jsonBody) {
		return CollectionUtils.getFirstElement(getJsonArguments(Collections.singletonList(parameterName), jsonBody, -1, false, false));
	}


	public static List<String> getJsonArguments(List<String> parameterNames, String jsonBody, int expectedDepth, boolean findAll, boolean errorIfNotFound) {
		List<String> arguments = new ArrayList<>();
		try {
			int depth = -1;
			int parameterNameDepth = -1;
			boolean parameterNameFound = false;
			StringWriter stringWriter = new StringWriter();
			JsonFactory factory = new JsonFactory();
			JsonGenerator generator = factory.createGenerator(stringWriter);
			JsonParser parser = factory.createParser(jsonBody);
			while (parser.nextToken() != null) {
				JsonToken token = parser.currentToken();
				if (token == JsonToken.START_OBJECT || token == JsonToken.START_ARRAY) {
					depth++;
					if (depth > MAX_DEPTH) {
						throw new ValidationException("Max depth exceeded while searching for parameter(s) " + parameterNames.toString() + "!");
					}
				}
				else if (token == JsonToken.END_OBJECT || token == JsonToken.END_ARRAY) {
					depth--;
				}
				if (parameterNameFound) {
					if (token == JsonToken.START_ARRAY) {
						generator.writeStartArray();
					}
					else if (token == JsonToken.END_ARRAY) {
						generator.writeEndArray();
					}
					else if (token == JsonToken.START_OBJECT) {
						generator.writeStartObject();
					}
					else if (token == JsonToken.END_OBJECT) {
						generator.writeEndObject();
					}
					else if (token == JsonToken.VALUE_FALSE) {
						generator.writeBoolean(false);
					}
					else if (token == JsonToken.VALUE_TRUE) {
						generator.writeBoolean(true);
					}
					else if (token == JsonToken.VALUE_NULL) {
						generator.writeNull();
					}
					else if (token == JsonToken.VALUE_NUMBER_FLOAT) {
						generator.writeNumber(parser.getDecimalValue());
					}
					else if (token == JsonToken.VALUE_NUMBER_INT) {
						generator.writeNumber(parser.getIntValue());
					}
					else if (token == JsonToken.VALUE_STRING) {
						generator.writeRaw(parser.getText());
					}
					else if (token == JsonToken.FIELD_NAME) {
						generator.writeFieldName(parser.getText());
					}
					else {
						throw new UnsupportedOperationException("Unable to process token: " + token.name());
					}
					if (depth == parameterNameDepth) {
						generator.close();
						arguments.add(stringWriter.toString());
						if (!findAll) {
							break;
						}
						//Reset parameters to find next value
						parameterNameDepth = -1;
						parameterNameFound = false;
						stringWriter = new StringWriter();
						generator = factory.createGenerator(stringWriter);
					}
				}
				else if (token == JsonToken.FIELD_NAME) {
					String tokenFieldName = parser.getText();
					if (parameterNames.contains(tokenFieldName) && (expectedDepth < 0 || (depth == expectedDepth))) {
						parameterNameFound = true;
						parameterNameDepth = depth;
					}
				}
			}
		}
		catch (Exception e) {
			if (errorIfNotFound) {
				throw new ValidationException("Failed to find argument(s) with names: " + parameterNames.toString());
			}
		}
		return arguments;
	}
}
