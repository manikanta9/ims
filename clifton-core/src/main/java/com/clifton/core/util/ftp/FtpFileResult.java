package com.clifton.core.util.ftp;

import com.clifton.core.dataaccess.file.FileWrapper;


/**
 * The <code>FtpFileResult</code> holder for an errors that occurred during FTP for a file.
 *
 * @author mwacker
 */
public class FtpFileResult extends FtpResult {

	private FileWrapper file;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FtpFileResult(FileWrapper file) {
		super(file.getFileName());
		this.file = file;
	}
	
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FileWrapper getFile() {
		return this.file;
	}


	public void setFile(FileWrapper file) {
		this.file = file;
	}
}
