package com.clifton.core.util.encryption;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.Permission;
import java.security.PermissionCollection;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.Map;


/**
 * Encryption Utils for working with Symmetric Encryption using the AES algorithm operating in Galois Counter Mode (GCM)
 *
 * @author theodorez
 */
public class EncryptionUtilsAES {

	/*
	 * Remove the crypto export restrictions the first time the Utils are used
	 */
	static {
		removeCryptographyRestrictions();
	}


	public static final String ENCRYPTED_STRING_PLACEHOLDER = "[ENCRYPTED]";

	private static final String ENCRYPTION_ALGORITHM = "AES";
	private static final int ENCRYPTION_KEY_LENGTH = 256;

	private static final int TAG_LENGTH = 128;

	private static final int INITIALIZATION_VECTOR_LENGTH = 12;

	private static final String CIPHER_INSTANCE = "AES/GCM/NoPadding";

	private static final String MESSAGE_DIGEST_ALGORITHM = "SHA-256";


	/**
	 * Appends the two keys together and hashes them using SHA-256
	 */
	public static byte[] hashKeys(byte[] key1, byte[] key2) {
		byte[] result = new byte[key1.length + key2.length];

		for (int i = 0; i < result.length; ++i) {
			result[i] = i < key1.length ? key1[i] : key2[i - key1.length];
		}

		return hashBytes(result);
	}


	/**
	 * Creates a SHA-256 hash of the given byte array
	 */
	public static byte[] hashBytes(byte[] array) {
		try {
			MessageDigest hash = MessageDigest.getInstance(MESSAGE_DIGEST_ALGORITHM);
			return hash.digest(array);
		}
		catch (Exception e) {
			throw new RuntimeException("Error hashing array", e);
		}
	}


	/**
	 * Validates that the given checksum (hex encoded string) is valid for the given key
	 */
	private static void validateKey(byte[] key, String checkSum) {
		byte[] hash = EncryptionUtils.hexStringToByteArray(checkSum);
		try {
			ValidationUtils.assertTrue(Arrays.equals(hashBytes(key), hash), "The given server key is invalid. The specified key does not match the checksum.");
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred when validating server key.", e);
		}
	}


	/**
	 * Attempt to read the file at the filepath.
	 * If the file does not exist then a new key file is created.
	 */
	public static byte[] readAndOrCreateKeyFile(String filePath, String checksum) {
		File keyFile = new File(filePath);
		if (!keyFile.exists() && StringUtils.isEmpty(checksum)) {
			FileUtils.createDirectoryForFile(keyFile);
			writeKeyToFile(generateKey(), filePath);
		}

		byte[] serverKey = EncryptionUtils.readKeyFileToBytes(keyFile);
		if (!StringUtils.isEmpty(checksum)) {
			validateKey(serverKey, checksum);
		}
		return serverKey;
	}


	/**
	 * Write the key as a base64 encoded String to the specified file
	 * <p>
	 * DOES NOT STORE THE IV WITH THE KEY
	 */
	public static void writeKeyToFile(byte[] key, String filepath) {
		File keyFile = new File(filepath);
		FileOutputStream fos = null;
		try {
			fos = new FileOutputStream(keyFile);
			fos.write(key);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred while writing key file", e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				}
				catch (IOException ioe) {
					LogUtils.warn(EncryptionUtilsAES.class, "Failed to close output stream when writing key file [" + keyFile.getName() + "].", ioe);
				}
			}
		}
	}


	/**
	 * Encrypts the given data with the key and initialization vector
	 */
	public static byte[] encrypt(byte[] data, byte[] key, byte[] iv) {
		try {
			GCMParameterSpec parameterSpec = new GCMParameterSpec(TAG_LENGTH, iv);
			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
			cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, 0, key.length, ENCRYPTION_ALGORITHM), parameterSpec);
			return cipher.doFinal(data);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred encrypting data", e);
		}
	}


	/**
	 * Decrypts the given data with the key and initialization vector
	 */
	public static byte[] decrypt(byte[] ciphertext, byte[] key, byte[] iv) {
		try {
			GCMParameterSpec parameterSpec = new GCMParameterSpec(TAG_LENGTH, iv);
			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
			cipher.init(Cipher.DECRYPT_MODE, new SecretKeySpec(key, 0, key.length, ENCRYPTION_ALGORITHM), parameterSpec);
			return cipher.doFinal(ciphertext);
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred decrypting data", e);
		}
	}


	/**
	 * Generates a random AES-256 key
	 */
	public static byte[] generateKey() {
		try {
			KeyGenerator generator = KeyGenerator.getInstance(ENCRYPTION_ALGORITHM);
			generator.init(ENCRYPTION_KEY_LENGTH);
			SecretKey key = generator.generateKey();
			return key.getEncoded();
		}
		catch (Exception e) {
			throw new RuntimeException("Error occurred generating key", e);
		}
	}


	/**
	 * Generates a random byte value the can be used as an initialization vector in AES
	 */
	public static byte[] generateInitializationVector() {
		byte[] iv = new byte[INITIALIZATION_VECTOR_LENGTH];
		SecureRandom random = new SecureRandom();
		random.nextBytes(iv);
		return iv;
	}


	public static boolean isRestrictedCryptography() throws NoSuchAlgorithmException {
		return Cipher.getMaxAllowedKeyLength("AES") == 128;
	}


	private static void removeCryptographyRestrictions() {
		try {
			if (!isRestrictedCryptography()) {
				return;
			}
			/*
			 * Do the following, but with reflection to bypass access checks:
			 *
			 * JceSecurity.isRestricted = false;
			 * JceSecurity.defaultPolicy.perms.clear();
			 * JceSecurity.defaultPolicy.add(CryptoAllPermission.INSTANCE);
			 */
			final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity");
			final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions");
			final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission");

			final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted");
			isRestrictedField.setAccessible(true);
			final Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(isRestrictedField, isRestrictedField.getModifiers() & ~Modifier.FINAL);
			isRestrictedField.set(null, false);

			final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy");
			defaultPolicyField.setAccessible(true);
			final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null);

			final Field perms = cryptoPermissions.getDeclaredField("perms");
			perms.setAccessible(true);
			((Map<?, ?>) perms.get(defaultPolicy)).clear();

			final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE");
			instance.setAccessible(true);
			defaultPolicy.add((Permission) instance.get(null));
		}
		catch (final Exception e) {
			throw new RuntimeException("Failed to remove cryptography restrictions", e);
		}
	}
}
