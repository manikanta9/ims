package com.clifton.core.util.collections;


import java.util.HashMap;
import java.util.Map;


/**
 * The <code>TwoWayMap</code> defines a 1 to 1 map that can be used to look up corresponding value using
 * either of the 1 to 1 mapped objects.
 *
 * @param <K>
 * @param <V>
 * @author mwacker
 */
public class TwoWayMap<K, V> {

	private final Map<K, V> firstToSecond = new HashMap<>();
	private final Map<V, K> secondToFirst = new HashMap<>();


	public void put(K first, V second) {
		this.firstToSecond.put(first, second);
		this.secondToFirst.put(second, first);
	}


	public V getFirst(K first) {
		return this.firstToSecond.get(first);
	}


	public K getSecond(V second) {
		return this.secondToFirst.get(second);
	}
}
