package com.clifton.core.util.http;


import java.net.URI;


/**
 * A factory used to construct {@link CustomURIBuilder}s from a common starting {@link URI}.
 */
public class CustomURIBuilderFactory {

	private final URI uri;


	public CustomURIBuilderFactory(URI uri) {
		this.uri = uri;
	}


	public CustomURIBuilder newBuilder() {
		return new CustomURIBuilder(this.uri);
	}
}
