package com.clifton.core.util.runner.config;


import com.clifton.core.util.runner.RunnerHandlerStatus;
import com.clifton.core.util.runner.RunnerHandlerStatusCommand;

import java.util.Date;
import java.util.List;


/**
 * The <code>RunnerConfigService</code> interface defines methods for administering runners.
 */
public interface RunnerConfigService {

	public List<RunnerConfig> getRunnerConfigList(RunnerConfigSearchForm searchForm);


	public Date getRunnerHandlerNextSchedulingDate();


	public void rescheduleRunnersFromProviders();


	public void restartRunnerHandler();


	public void clearCompletedRunners();


	/**
	 * Attempts to terminate all scheduled runners with the given type and typeId.
	 * Updates the Status of the runner to request termination: throw an exception on the next Status update.
	 * Usually works with runners that iterate over a large set of items and update the Status after each item is processed.
	 */
	public void terminateRunners(String runnerType, String runnerTypeId);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @see com.clifton.core.util.runner.RunnerHandler#getStatus(RunnerHandlerStatusCommand)
	 */
	public RunnerHandlerStatus getRunnerHandlerStatus(RunnerHandlerStatusCommand command);


	/**
	 * @see com.clifton.core.util.runner.RunnerHandler#registerExecutionStatusMonitoring(RunnerHandlerStatusCommand)
	 */
	public void registerExecutionStatusMonitoring(RunnerHandlerStatusCommand command);


	/**
	 * @see com.clifton.core.util.runner.RunnerHandler#deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand)
	 */
	public void deregisterExecutionStatusMonitoring(RunnerHandlerStatusCommand command);
}
