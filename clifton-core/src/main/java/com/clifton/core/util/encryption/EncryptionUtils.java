package com.clifton.core.util.encryption;

import com.clifton.core.dataaccess.file.FileUtils;

import java.io.File;


/**
 * Basic utils for working with crypto
 *
 * @author theodorez
 */
public class EncryptionUtils {

	/**
	 * Zeroes the array to prevent the original value from persisting in memory
	 */
	public static void cleanupByteArray(byte[] array) {
		if (array != null) {
			//Cleanup data-sources - Leave no traces behind.
			for (int i = 0; i < array.length; i++) {
				array[i] = 0;
			}
		}
	}


	/**
	 * Converts the given hex string to a byte array
	 * <p>
	 * From http://stackoverflow.com/a/11208685
	 */
	public static byte[] hexStringToByteArray(String s) {
		int len = s.length();
		byte[] data = new byte[len / 2];
		for (int i = 0; i < len; i += 2) {
			data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
					+ Character.digit(s.charAt(i + 1), 16));
		}
		return data;
	}


	/**
	 * Converts the given byte array to a hex string
	 * <p>
	 * From http://stackoverflow.com/a/332101
	 */
	public static String byteArrayToHexString(byte[] bytes) {
		StringBuilder hexString = new StringBuilder();
		for (byte bite : bytes) {
			String hex = Integer.toHexString(0xFF & bite);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}


	public static byte[] readKeyFileToBytes(File keyFile) {
		try {
			return FileUtils.readFileBytes(keyFile);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred while reading key file", e);
		}
	}
}
