package com.clifton.core.util.runner;


import com.clifton.core.util.status.Status;

import java.util.Map;


/**
 * The <code>Task</code> is used to execute the method with a provided context.
 *
 * @author mwacker
 */
public interface Task {

	/**
	 * Task logic is executed inside of this method. The specified context can be used to pass parameters
	 * or cache data.  It returns the status of task execution.
	 */
	public Status run(Map<String, Object> context);
}
