package com.clifton.core.util.http.cookie;

import com.clifton.core.util.BooleanUtils;

import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Function;


/**
 * The <code>AuthScopedPersistentCookieStoreManager</code> manages persistent cookie stores
 * by their domain and username.
 */
public class AuthScopedPersistentCookieStoreManager {

	private final ConcurrentHashMap<String, ImsCookieStore> map = new ConcurrentHashMap<>();
	private final boolean usePersistentCookieStore = BooleanUtils.isTrue(System.getProperty("usePersistentCookieStore"));
	// Define cookie store generators to avoid evaluating the above boolean on every use
	private final Function<String, ImsCookieStore> cookieStoreGenerator = this.usePersistentCookieStore ? ImsCookieStoreFactory::newPersistentCookieStore : key -> ImsCookieStoreFactory.newRamCookieStore();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the {@link ImsCookieStore} associated with the <code>host</code> and <code>username</code>; creates
	 * a new store first if it doesn't already exist.
	 *
	 * @param host     the web server host the cookie store is associated with
	 * @param username the username the cookie store is associated with
	 */
	public ImsCookieStore getCookieStore(String host, String username) {
		String key = host + '_' + username;
		return this.map.computeIfAbsent(key, this.cookieStoreGenerator);
	}
}

