package com.clifton.core.util.status;

import com.clifton.core.beans.annotations.ValueChangingSetter;


/**
 * The StatusHolder class hold {@link Status} object with all the details for a given run.
 *
 * @author vgomelsky
 */
public class StatusHolder implements StatusHolderObject<Status> {

	private Status status;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public StatusHolder(String statusMessage) {
		this.status = Status.ofMessage(statusMessage);
	}


	public StatusHolder(Status status) {
		this.status = status;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getStatusMessage() {
		return getStatus().getMessage();
	}


	@ValueChangingSetter
	public void setStatusMessage(String statusMessage) {
		getStatus().setMessage(statusMessage);
	}


	/**
	 * Appends all error category details to the status Message
	 */
	public void setStatusMessageWithErrors(String statusMessage, Integer maxErrors) {
		getStatus().setMessageWithErrors(statusMessage, maxErrors);
	}


	/**
	 * Returns the number of {@link StatusDetail} objects that have category of {@link StatusDetail#CATEGORY_ERROR}.
	 */
	public int getErrorCount() {
		return getStatus().getErrorCount();
	}


	/**
	 * Returns the number of {@link StatusDetail} objects that have category of {@link StatusDetail#CATEGORY_WARNING}.
	 */
	public int getWarningCount() {
		return getStatus().getWarningCount();
	}


	public void addError(String note) {
		getStatus().addError(note);
	}


	public void addWarning(String note) {
		getStatus().addWarning(note);
	}


	public void addMessage(String note) {
		getStatus().addMessage(note);
	}


	public void addSkipped(String note) {
		getStatus().addSkipped(note);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status getStatus() {
		return this.status;
	}
}
