package com.clifton.core.util.event;


import com.clifton.core.comparison.Ordered;
import com.clifton.core.util.CollectionUtils;

import java.util.List;


/**
 * The <code>EventListener</code> interface provides a specification for receiving notification that
 * a particular event has been fired.
 *
 * @author rbrooks
 */
public interface EventListener<E extends Event<?, ?>> extends Ordered {

	/**
	 * The method that will be called when an applicable event has been fired.
	 *
	 * @param event The event that has been fired.
	 */
	public void onEvent(E event);


	/**
	 * Returns a list of identifying names of the event for which this EventListener is registered.
	 */
	default public List<String> getEventNameList() {
		return CollectionUtils.createList(getEventName());
	}


	/**
	 * Returns the identifying name of the event for which this EventListener is registered.
	 *
	 * @return an eventName linking this listener with the corresponding event.
	 */
	default public String getEventName() {
		throw new UnsupportedOperationException("This event listener supports multiple events.");
	}
}
