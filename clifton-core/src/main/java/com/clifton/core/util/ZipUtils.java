package com.clifton.core.util;


import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.zip.GZIPInputStream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;


public class ZipUtils {

	public static List<FileWrapper> unZipFiles(File zippedFile) {
		return unZipFiles(zippedFile, null);
	}


	public static List<FileWrapper> unZipFiles(File zippedFile, String targetPath) {
		List<FileWrapper> result = new ArrayList<>();
		FileContainer zippedFileContainer = FileContainerFactory.getFileContainer(zippedFile);
		try (
				InputStream fileInputStream = zippedFileContainer.getInputStream();
				BufferedInputStream bufferedInputStream = new BufferedInputStream(fileInputStream);
				ZipInputStream zis = new ZipInputStream(bufferedInputStream)
		) {
			if (StringUtils.isEmpty(targetPath)) {
				targetPath = zippedFileContainer.getParent();
			}
			ZipEntry zipEntry;
			while ((zipEntry = zis.getNextEntry()) != null) {
				FileContainer extractedFile = FileContainerFactory.getFileContainer(FileUtils.combinePath(targetPath, zipEntry.getName()));
				FileUtils.createDirectoryForFile(extractedFile);
				try (
						OutputStream outputStream = extractedFile.getOutputStream();
						BufferedOutputStream entryStream = new BufferedOutputStream(outputStream)
				) {
					byte[] buffer = new byte[2048];
					int size;
					while ((size = zis.read(buffer, 0, buffer.length)) != -1) {
						entryStream.write(buffer, 0, size);
					}
				}
				result.add(new FileWrapper(FilePath.forPath(extractedFile.getPath()), null, false));
			}
		}
		catch (Throwable e) {
			for (FileWrapper file : CollectionUtils.getIterable(result)) {
				if (FileUtils.fileExists(file.getFile())) {
					FileUtils.delete(file.getFile());
				}
			}
			throw new RuntimeException("Failed to unzip file [" + zippedFileContainer.getAbsolutePath() + "].", e);
		}
		return result;
	}


	public static FileContainer zipFiles(List<FileWrapper> fileList, FileContainer zipFile) {
		return zipFiles(fileList, zipFile, null);
	}


	public static FileContainer zipFiles(List<FileWrapper> fileList, FileContainer zipFile, FileContainer targetDirectoryPath) {
		// Exception thrown if the same file name is used twice so instead track the names used and append timestamp if duplicates found
		Set<String> fileNames = new HashSet<>();
		try (
				OutputStream outputStream = zipFile.getOutputStream();
				BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(outputStream);
				ZipOutputStream out = new ZipOutputStream(bufferedOutputStream)
		) {
			for (FileWrapper file : CollectionUtils.getIterable(fileList)) {
				String fileName;
				if (targetDirectoryPath == null) {
					fileName = file.getFileName();
				}
				else if (targetDirectoryPath.exists() && targetDirectoryPath.isDirectory()) {
					//need the path delimiter otherwise the file name will contain preceding delimiter
					fileName = FileUtils.getRelativePath(file.getFile().getPath(), targetDirectoryPath.getAbsolutePath() + File.separator);
				}
				else {
					throw new IllegalArgumentException("The rootDirectory must exist and must be a directory");
				}
				if (fileNames.contains(fileName)) {
					fileName = fileName + "_" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE);
				}
				try (
						InputStream inputStream = FileUtils.getInputStream(file.getFile().getPath());
						BufferedInputStream input = new BufferedInputStream(inputStream)
				) {
					ZipEntry e = new ZipEntry(fileName);
					fileNames.add(fileName);
					out.putNextEntry(e);

					int len;
					byte[] buf = new byte[8192]; //copy in chunks
					while ((len = input.read(buf)) > 0) {
						out.write(buf, 0, len);
					}
				}
				finally {
					out.closeEntry();
					// delete the source file
					if (file.isTempFile()) {
						FileUtils.delete(file.getFile());
					}
				}
			}
		}
		catch (Throwable e) {
			// delete any temp files
			for (FileWrapper file : CollectionUtils.getIterable(fileList)) {
				if (file.isTempFile()) {
					FileUtils.delete(file.getFile());
				}
			}
			throw new RuntimeException("Failed to create zip file.", e);
		}
		return zipFile;
	}


	public static FileContainer zipFiles(List<FileWrapper> fileList, String zipFilePath) {
		return zipFiles(fileList, FileContainerFactory.getFileContainer(zipFilePath));
	}


	public static FileContainer zipFiles(List<FileWrapper> fileList, String zipFilePath, String targetPath) {
		return zipFiles(fileList, FileContainerFactory.getFileContainer(zipFilePath), FileContainerFactory.getFileContainer(targetPath));
	}


	public static FileWrapper unGzipFile(File gzippedFile, String targetPath, String extractedFileName) {
		FileWrapper result;
		FileContainer gzippedFileContainer = FileContainerFactory.getFileContainer(gzippedFile);
		try {
			if (StringUtils.isEmpty(targetPath)) {
				targetPath = gzippedFileContainer.getParent();
			}
			if (StringUtils.isEmpty(extractedFileName)) {
				extractedFileName = FileUtils.getFileNameWithoutExtension(gzippedFileContainer.getName());
			}
			FileContainer extractedFile = FileContainerFactory.getFileContainer(FileUtils.combinePath(targetPath, extractedFileName));
			FileUtils.createDirectoryForFile(extractedFile);
			ValidationUtils.assertFalse(StringUtils.isEqual(gzippedFileContainer.getAbsolutePath(), extractedFile.getAbsolutePath()), "Cannot unzip and extract to the same file");
			try (
					InputStream inputStream = gzippedFileContainer.getInputStream();
					BufferedInputStream bufferedInputStream = new BufferedInputStream(inputStream);
					GZIPInputStream gis = new GZIPInputStream(bufferedInputStream);
					OutputStream output = extractedFile.getOutputStream();
					BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(output)
			) {
				byte[] buffer = new byte[2048];
				int bytesRead;
				while ((bytesRead = gis.read(buffer)) > 0) {
					bufferedOutputStream.write(buffer, 0, bytesRead);
				}
			}
			result = new FileWrapper(FilePath.forPath(extractedFile.getPath()), extractedFileName, false);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to unzip file [" + gzippedFileContainer.getAbsolutePath() + "].", e);
		}
		return result;
	}
}
