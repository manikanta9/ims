package com.clifton.core.util.retry;

import java.util.function.Predicate;
import java.util.function.Supplier;


/**
 * @author theodorez
 */
public class RetryTask<T> {

	public final Supplier<T> action;
	public final Predicate<T> successCondition;


	public RetryTask(Supplier<T> action, Predicate<T> successCondition) {
		this.action = action;
		this.successCondition = successCondition;
	}


	public T doAction() {
		return this.action.get();
	}


	public boolean success(T result) {
		if (this.successCondition != null) {
			return this.successCondition.test(result);
		}
		else {
			return result != null;
		}
	}
}
