package com.clifton.core.util.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.encryption.EncryptionUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.token.JsonWebToken;

import java.lang.reflect.Method;
import java.security.InvalidParameterException;
import java.security.interfaces.ECKey;
import java.security.interfaces.RSAKey;
import java.util.Collections;
import java.util.List;


/**
 * @author theodorez
 */
public class JsonWebTokenUtils {

	private JsonWebTokenUtils() {
		throw new IllegalAccessError("Cannot instantiate utils class");
	}


	/**
	 * Decodes the given JWT and validates the signature using the given signaturekey
	 */
	public static JsonWebToken decodeJsonWebTokenWithValidation(String jwt, String signatureKey, String algorithmName) {
		Algorithm algorithm = getAlgorithmForKey(signatureKey, algorithmName);
		JWTVerifier verifier = JWT.require(algorithm).build();
		DecodedJWT decodedJWT = verifier.verify(jwt);
		return new JsonWebToken(algorithmName, decodedJWT.getClaims(), signatureKey, jwt);
	}


	/**
	 * Decodes a JWT, but DOES NOT verify the signature.
	 */
	public static JsonWebToken decodeJsonWebTokenNoValidation(String jwt) {
		JWT unverifiedToken = JWT.decode(jwt);
		return new JsonWebToken(null, unverifiedToken.getClaims(), null, jwt);
	}


	public static void verifyJsonWebToken(JsonWebToken jsonWebToken) {
		Algorithm algorithm = getAlgorithmForKey(jsonWebToken.getSignatureKey(), jsonWebToken.getAlgorithmName());
		JWTVerifier verifier = JWT.require(algorithm).build();
		verifier.verify(jsonWebToken.getEncodedToken());
	}


	public static Class<?> getAlgorithmClass(String algorithmName) {
		Class<?> algorithmParameterClass;
		switch (algorithmName) {
			case "RSA256":
			case "RSA384":
			case "RSA512":
				algorithmParameterClass = RSAKey.class;
				break;
			case "HMAC256":
			case "HMAC384":
			case "HMAC512":
				algorithmParameterClass = String.class;
				break;
			case "ECDSA256":
			case "ECDSA384":
			case "ECDSA512":
				algorithmParameterClass = ECKey.class;
				break;
			default:
				algorithmParameterClass = null;
		}
		return algorithmParameterClass;
	}


	public static Algorithm getAlgorithmForKey(Object signatureKey, String algorithmName) {
		Algorithm algorithm = null;
		Class<?> algorithmParameterClass = getAlgorithmClass(algorithmName);

		if (algorithmParameterClass == null) {
			throw new InvalidParameterException("Algorithm " + algorithmName + " is not currently supported.");
		}

		Exception exception = null;

		try {
			Method method = Algorithm.class.getMethod(algorithmName, algorithmParameterClass);
			algorithm = (Algorithm) method.invoke(Algorithm.class, algorithmParameterClass.cast(signatureKey));
		}
		catch (Exception e) {
			exception = e;
		}

		if (algorithm == null && exception != null) {
			throw new RuntimeException("Error occurred getting algorithm.", exception);
		}
		return algorithm;
	}


	public static String getClaimAsString(String claimName, JsonWebToken token) {
		Object claim = token.getPayloadClaims().get(claimName);
		if (claim instanceof Claim) {
			return ((Claim) claim).asString();
		}
		return null;
	}


	public static List<String> getClaimAsStringList(String claimName, JsonWebToken token) {
		Object claim = token.getPayloadClaims().get(claimName);
		if (claim instanceof Claim) {
			return ((Claim) claim).asList(String.class);
		}
		return Collections.emptyList();
	}


	public static String getJsonWebTokenSignatureKey(String fileLocation, String checksum) {
		if (FileUtils.fileExists(fileLocation)) {
			String key = FileUtils.readFileToString(fileLocation);
			if (!StringUtils.isEmpty(checksum)) {
				String testChecksum = EncryptionUtils.byteArrayToHexString(EncryptionUtilsAES.hashBytes(key.getBytes()));
				ValidationUtils.assertTrue(StringUtils.isEqual(checksum, testChecksum), "Client signature key checksum is not valid for the key the client supplied.");
			}
			return key;
		}
		return null;
	}
}
