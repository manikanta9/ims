package com.clifton.core.util.observer;


import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationException;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * The <code>ObserverNotifier</code> class is a proxy that holds a list of all {@link Observer}
 * objects registered for a given event. When invoked, it iterates through all observers, filters
 * them based on the provided Predicate and invokes the requested method with the specified arguments
 * for the potentially filtered list of observers.
 *
 * @author vgomelsky
 */
public class ObserverNotifierImpl<T extends Observer> implements ObserverNotifier, InvocationHandler {

	private final List<T> observerList;
	private Predicate<Observer> observerFilter;


	public ObserverNotifierImpl(List<T> observerList, Predicate<Observer> observerFilter) {
		this.observerList = observerList;
		this.observerFilter = observerFilter;
	}


	/**
	 * Invokes the specified method with the specified arguments on all or a subset of registered
	 * observers.
	 */
	@Override
	public Object invoke(@SuppressWarnings("unused") Object proxy, Method method, Object[] args) throws Throwable {
		// Handle methods from "Object"
		switch (method.getName()) {
			case "equals":
				return proxy == args[0];
			case "hashCode":
				return System.identityHashCode(proxy);
			case "toString":
				return proxy.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(proxy)) + ", with InvocationHandler " + this;
		}
		if (this.observerList != null) {
			List<T> selectedObserverList = this.observerList
					.stream()
					.filter(o -> this.observerFilter.test(o))
					.collect(Collectors.toList());
			for (T observer : selectedObserverList) {
				try {
					method.invoke(observer, args);
				}
				catch (Exception e) {
					Throwable original = ExceptionUtils.getOriginalException(e);
					if (original instanceof ValidationException) {
						throw original;
					}
					throw new RuntimeException(e);
				}
			}
		}
		return null;
	}
}
