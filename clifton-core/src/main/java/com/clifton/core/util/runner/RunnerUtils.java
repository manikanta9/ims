package com.clifton.core.util.runner;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;
import java.util.List;
import java.util.function.Predicate;


/**
 * Utility methods for working with Runners
 *
 * @author theodorez
 */
public class RunnerUtils {

	/**
	 * This will create a new asynchronous process that will:
	 * <p>
	 * IF the enabled predicate returns True
	 * 1. Get the occurrences of the entity between now and the next time the runner handler is supposed to reschedule everything
	 * 2. Reschedule those occurrences (after first removing all existing runners for the entity)
	 * <p>
	 * IF The enabled predicate returns False, cancel all currently scheduled runners for the entity
	 *
	 * @param entity         The object that runners should be created for
	 * @param enabled        Evaluates the entity to determine whether new runners should be scheduled or not
	 * @param runnerId       The type ID that the asynchronous rescheduling runner should have
	 * @param runnerProvider The appropriate RunnerProvider for the given entity
	 * @param runnerHandler  The Runner Handler
	 */
	public static <T> void rescheduleRunnersForEntity(T entity, Predicate<T> enabled, String runnerId, RunnerProvider<T> runnerProvider, RunnerHandler runnerHandler) {

		Runner runner = new AbstractStatusAwareRunner("RESCHEDULE-" + entity.getClass().getSimpleName(), runnerId, new Date()) {
			@Override
			public void run() {
				List<Runner> newRunnerOccurrences = null;

				if (enabled.test(entity)) {
					newRunnerOccurrences = runnerProvider.getOccurrencesBetweenForEntity(entity, new Date(), runnerHandler.getNextScheduleDate());
				}

				if (CollectionUtils.isEmpty(newRunnerOccurrences)) {
					//If the newRunnerOccurrences list is empty that means the schedule has changed to be outside of the current time interval
					cancelRunners(entity, runnerProvider, runnerHandler);
				}
				else {
					runnerHandler.rescheduleRunners(newRunnerOccurrences);
				}
			}
		};
		runnerHandler.rescheduleRunner(runner);
	}


	/**
	 * Asynchronously reschedules all runners for a given provider that fall within the specified date range.
	 * <p>
	 * If the provider produces any runners with the same type and typeId as a runner that is already schedule, those existing runners will be canceled.
	 */
	public static <T> void rescheduleRunnersBetweenForProvider(Date startDate, Date endDate, RunnerProvider<T> runnerProvider, RunnerHandler runnerHandler) {
		String runId = DateUtils.fromDate(startDate, DateUtils.DATE_FORMAT_FILE) + "-" + DateUtils.fromDate(endDate, DateUtils.DATE_FORMAT_FILE);
		Runner runner = new AbstractStatusAwareRunner("RESCHEDULE-" + runnerProvider.getClass().getSimpleName(), runId, new Date()) {
			@Override
			public void run() {
				try {
					LogUtils.info(runnerHandler.getClass(), "Scheduling new runs for [" + runnerProvider.getClass().getName() + "].");
					List<Runner> scheduleBeansFromProvider = runnerProvider.getOccurrencesBetween(startDate, endDate);
					runnerHandler.rescheduleRunners(scheduleBeansFromProvider);
				}
				catch (Throwable e) {
					LogUtils.error(LogCommand.ofThrowableAndMessage(runnerHandler.getClass(), e, () -> "Failed to retrieve occurrences for [" + runnerProvider.getClass().getName() + "] between [" + DateUtils.fromDate(startDate, DateUtils.DATE_FORMAT_SHORT)
							+ "] and [" + DateUtils.fromDate(endDate, DateUtils.DATE_FORMAT_SHORT) + "]."));
				}
			}
		};
		runnerHandler.rescheduleRunner(runner);
	}


	private static <T> void cancelRunners(T entity, RunnerProvider<T> runnerProvider, RunnerHandler runnerHandler) {
		Runner cancelRunner = runnerProvider.createRunnerForEntityAndDate(entity, new Date());
		runnerHandler.cancelRunners(cancelRunner.getType(), cancelRunner.getTypeId());
	}
}
