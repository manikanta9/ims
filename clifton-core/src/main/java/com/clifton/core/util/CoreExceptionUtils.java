package com.clifton.core.util;


import com.clifton.core.logging.LoggableException;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>CoreExceptionUtils</code> class provides utility methods for working with exceptions.
 *
 * @author vgomelsky
 */
public class CoreExceptionUtils {


	/**
	 * Returns true if the specified exception or its original exception is an instance of a ValidationException.
	 */
	public static boolean isValidationException(Throwable e) {
		if ((e instanceof ValidationException) || (ExceptionUtils.getOriginalException(e) instanceof ValidationException)) {
			return true;
		}
		return false;
	}


	/**
	 * Returns the first LoggableException in the chain of exception causes.
	 * If one is not found, returns original exception (deepest cause).
	 */
	public static Throwable getLoggableOrOriginalException(Throwable e) {
		if (e == null) {
			return null;
		}
		if (e instanceof LoggableException) {
			return e;
		}
		return (e.getCause() == null) ? e : getLoggableOrOriginalException(e.getCause());
	}
}
