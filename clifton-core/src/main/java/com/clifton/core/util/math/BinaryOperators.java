package com.clifton.core.util.math;

import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;
import java.util.function.BinaryOperator;


/**
 * A selection of numerical binary operator types. These operators may be used to calculate a value from two {@link BigDecimal} operands.
 * <p>
 * Binary operators may be used to produce a single {@link BigDecimal} value from distinct <i>left-hand-side</i> (LHS) and <i>right-hand-side</i> (RHS) values. For example, {@link
 * #SUBTRACT} may be applied to <tt>10</tt> and <tt>9</tt> to produce the value <tt>8</tt>.
 *
 * @author MikeH
 */
public enum BinaryOperators {

	ADD(MathUtils::add),
	SUBTRACT(MathUtils::subtract),
	MULTIPLY(MathUtils::multiply),
	DIVIDE(MathUtils::divide),
	MODULO(MathUtils::modulo),
	POWER(MathUtils::pow);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final BinaryOperator<BigDecimal> operator;


	BinaryOperators(BinaryOperator<BigDecimal> operator) {
		this.operator = operator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes the given operator using the provided operands.
	 *
	 * @param lhs the <i>left-hand-side</i> operand
	 * @param rhs the <i>right-hand-side</i> operand
	 * @return the result of the operator applied to the given operands
	 */
	public BigDecimal operate(BigDecimal lhs, BigDecimal rhs) {
		return this.operator.apply(lhs, rhs);
	}
}
