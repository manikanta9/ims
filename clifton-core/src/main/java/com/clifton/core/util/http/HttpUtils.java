package com.clifton.core.util.http;


import org.apache.http.HttpEntity;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.util.EntityUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.List;


public class HttpUtils {

	/**
	 * Returns the base url.  For example, http://localhost:8080/app-clifton-ims will return http://localhost:8080.
	 */
	public static String getBaseUrl(String url) {

		try {
			URL targetUrl = new URL(url);
			if ((targetUrl.getPort() == 80) ||
					(targetUrl.getPort() == 443)) {
				return targetUrl.getProtocol() + "://" +
						targetUrl.getHost();
			}
			else {
				return targetUrl.getProtocol() + "://" +
						targetUrl.getHost() + ":" + targetUrl.getPort();
			}
		}
		catch (MalformedURLException e) {
			throw new RuntimeException("Failed to parse url [" + url + "].", e);
		}
	}

	public static NameValuePair getParam(String paramName, List<NameValuePair> nameValuePairs) {
		for (NameValuePair valuePair : nameValuePairs) {
			if (valuePair.getName().equals(paramName)) {
				return valuePair;
			}
		}

		return null;
	}


	public static void closeResponse(SimpleHttpResponse closeable) {
		if (closeable != null) {
			try {
				EntityUtils.consumeQuietly(closeable.getEntity());
				closeable.close();
			}
			catch (IOException e) {
				throw new RuntimeException("Error closing response", e);
			}
		}
	}


	public static boolean containsParam(String paramName, List<NameValuePair> nameValuePairs) {
		return getParam(paramName, nameValuePairs) != null;
	}


	/**
	 * Returns the response of the provided URL POST request as a string.
	 *
	 * @param url                      the url to GET
	 * @param includeNonOkResponseCode if true, return the string result for any response. If false, return the string result for only OK (200) response codes.
	 * @throws IOException if an issue occurs reading the response
	 */
	public static String post(String url, boolean includeNonOkResponseCode) throws IOException {
		try (SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(new HttpPost(url))) {
			return (includeNonOkResponseCode || response.getStatusCode() == HttpStatus.SC_OK) ? EntityUtils.toString(response.getEntity(), "UTF-8") : "";
		}
	}


	/**
	 * Returns the response of the provided URL GET request as a string.
	 *
	 * @param url                      the url to GET
	 * @param includeNonOkResponseCode if true, return the string result for any response. If false, return the string result for only OK (200) response codes.
	 * @throws IOException if an issue occurs reading the response
	 */
	public static String get(String url, boolean includeNonOkResponseCode) throws IOException {
		try (SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(new HttpGet(url))) {
			return (includeNonOkResponseCode || response.getStatusCode() == HttpStatus.SC_OK) ? EntityUtils.toString(response.getEntity(), "UTF-8") : "";
		}
	}


	/**
	 * Downloads a file from a given URL and write the input stream of the HTTP request directly to the provided output stream.
	 * The provided output stream is not not closed.
	 */
	public static int downloadAndWriteToStream(String url, OutputStream stream, boolean throwErrorOnBadReport) throws IOException {
		int result = 0;
		try (SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(new HttpGet(url))) {
			HttpEntity entity = new BufferedHttpEntity(response.getEntity());
			if (response.getStatusCode() == 500) {
				if (throwErrorOnBadReport) {
					throw new RuntimeException(EntityUtils.toString(response.getEntity(), "UTF-8"));
				}
			}
			else {
				int length;
				byte[] buffer = new byte[4 * 1024];
				try (InputStream is = entity.getContent()) {
					while ((length = is.read(buffer)) != -1) {
						result += length;
						stream.write(buffer, 0, length);
					}
				}
			}
		}
		return result;
	}


	/**
	 * Downloads a file from the specific url to the file name provided
	 */
	public static File downloadToFile(String url, String fileName, String extension, boolean throwErrorOnBadReport) throws IOException {
		File result = File.createTempFile(fileName, "." + extension);
		int bytesRead;
		try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(result))) {
			bytesRead = downloadAndWriteToStream(url, outputStream, throwErrorOnBadReport);
		}
		if (bytesRead == 0) {
			result.delete();
			return null;
		}
		return result;
	}


	/**
	 * Returns the UTF-8 URL decoded parameter value for the requested parameter name.
	 *
	 * @param request       the servlet request
	 * @param parameterName the name of the parameter to get from the request
	 * @return the decoded parameter value or null if a value did not exist
	 */
	public static String getUrlDecodedParameterValue(HttpServletRequest request, String parameterName) {
		if (request == null || parameterName == null) {
			return null;
		}
		String value = request.getParameter(parameterName);
		try {
			return value == null ? null : URLDecoder.decode(value, "UTF-8");
		}
		catch (UnsupportedEncodingException e) {
			// this should never happen
			throw new RuntimeException("UTF-8 is not a supported encoding.", e);
		}
	}


	/**
	 * Returns the UTF-8 URL decoded parameter value for the requested parameter name.
	 *
	 * @param request       the servlet request
	 * @param parameterName the name of the parameter to get from the request
	 * @return the decoded parameter value or "" if a value did not exist
	 */
	public static String getNonNullUrlDecodedParameterValue(HttpServletRequest request, String parameterName) {
		String value = getUrlDecodedParameterValue(request, parameterName);
		return value == null ? "" : value;
	}
}
