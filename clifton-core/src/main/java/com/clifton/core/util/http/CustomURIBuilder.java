package com.clifton.core.util.http;


import org.apache.http.client.utils.URIBuilder;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;


/**
 * An extension of {@link URIBuilder} that provides additional convenience methods.
 */
public class CustomURIBuilder extends URIBuilder {

	/**
	 * Constructs an empty instance.
	 */
	public CustomURIBuilder() {
		super();
	}


	/**
	 * Construct an instance from the string which must be a valid URI.
	 *
	 * @param uri a valid URI in string form
	 * @throws URISyntaxException if the input is not a valid URI
	 */
	public CustomURIBuilder(final String uri) throws URISyntaxException {
		super(uri);
	}


	/**
	 * Construct an instance from the provided URI.
	 *
	 * @param uri
	 */
	public CustomURIBuilder(final URI uri) {
		super(uri);
	}


	@Override
	public CustomURIBuilder setPath(final String path) {
		super.setPath(path);
		return this;
	}


	@Override
	public CustomURIBuilder setParameter(String param, String value) {
		super.setParameter(param, value);
		return this;
	}


	public CustomURIBuilder setParameters(Map<String, String> params) {
		for (Map.Entry<String, String> stringStringEntry : params.entrySet()) {
			String value = stringStringEntry.getValue();
			setParameter(stringStringEntry.getKey(), value);
		}

		return this;
	}


	public CustomURIBuilder appendToPath(String str) {
		setPath(getPath() + (getPath().endsWith("/") ? "" : "/") + str);
		return this;
	}


	@Override
	public URI build() {
		//Normalize slashes to avoid misconstructed URI
		setPath(getPath().replaceAll("//", "/"));
		try {
			return super.build();
		}
		catch (URISyntaxException e) {
			throw new RuntimeException("Exception while building URI", e);
		}
	}


	public static URI uriFromString(String string) {
		try {
			return new URI(string);
		}
		catch (URISyntaxException e) {
			throw new RuntimeException("Could not convert String to URI", e);
		}
	}
}
