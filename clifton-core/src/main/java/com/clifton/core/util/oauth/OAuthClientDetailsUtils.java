package com.clifton.core.util.oauth;

import com.clifton.core.util.token.JsonWebTokenUtils;
import com.clifton.core.web.token.JsonWebToken;
import com.clifton.core.web.token.JsonWebTokenClaimProperties;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.common.exceptions.InsufficientScopeException;
import org.springframework.security.oauth2.common.exceptions.InvalidScopeException;
import org.springframework.security.oauth2.provider.ClientDetails;

import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;


/**
 * @author theodorez
 */
public class OAuthClientDetailsUtils {

	private OAuthClientDetailsUtils() {
		throw new IllegalAccessError("Cannot instantiate a utility class");
	}


	public static void validateClientScopes(ClientDetails clientDetails, JsonWebToken token) {
		Set<String> scopes = clientDetails.getScope();
		List<String> tokenScopes = JsonWebTokenUtils.getClaimAsStringList(JsonWebTokenClaimProperties.SCOPE.getClaimName(), token);
		//A client cannot request scopes that it does not have access to
		if (!scopes.containsAll(tokenScopes)) {
			throw new InvalidScopeException("JWT Contains invalid scopes for this client.");
		}
	}


	public static void validateClientImpersonationScope(ClientDetails clientDetails, JsonWebToken token) {
		validateClientImpersonationScope(clientDetails.getScope(), JsonWebTokenUtils.getClaimAsStringList(JsonWebTokenClaimProperties.SCOPE.getClaimName(), token), token);
	}


	public static void validateClientImpersonationScope(Set<String> clientScopes, List<String> tokenScopes, JsonWebToken token) {
		if (JsonWebTokenUtils.getClaimAsString(JsonWebTokenClaimProperties.USERNAME.getClaimName(), token) != null) {
			if (!clientScopes.contains(JsonWebTokenClaimProperties.IMPERSONATION_USER_SCOPE.getClaimName())) {
				throw new InsufficientScopeException("This client does not have permission to impersonate a user.");
			}

			if (!tokenScopes.contains(JsonWebTokenClaimProperties.IMPERSONATION_USER_SCOPE.getClaimName())) {
				throw new InsufficientScopeException("This client is requesting to impersonate a user but has not requested the appropriate scope.");
			}
		}
	}


	public static List<SimpleGrantedAuthority> getGrantedAuthoritiesList(List<String> authorities) {
		return authorities.stream().map(SimpleGrantedAuthority::new).collect(toList());
	}
}
