package com.clifton.core.util.event;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


@Component
public class EventHandlerImpl implements EventHandler {

	/**
	 * A map that relates specific events to eventListeners.  Note that multiple EventListeners
	 * may be registered for the same event.
	 */
	private final Map<String, List<EventListener<?>>> eventMap = new ConcurrentHashMap<>();


	@Override
	@SuppressWarnings("unchecked")
	public <E extends Event<?, ?>> boolean raiseEvent(E event) {
		AssertUtils.assertNotNull(event, "EventHandler failed to raise event:  Event is null");
		AssertUtils.assertNotNull(event.getEventName(), "EventHandler failed to raise event:  Event name is null");

		boolean eventRaised = false;
		List<EventListener<?>> listeners = this.eventMap.get(event.getEventName());
		if (!CollectionUtils.isEmpty(listeners)) {
			List<Throwable> errors = new ArrayList<>();
			Throwable firstError = null;
			// iterate through listeners registered for this event and call onEvent()
			for (EventListener<?> listener : listeners) {
				try {
					((EventListener<E>) listener).onEvent(event);
					eventRaised = true;
				}
				catch (Throwable err) {
					if (firstError == null) {
						firstError = err;
					}
					else {
						errors.add(err);
					}
				}
			}

			if (firstError != null) {
				StringBuilder sb = new StringBuilder(ExceptionUtils.getDetailedMessage(firstError));
				for (Throwable error : CollectionUtils.getIterable(errors)) {
					sb.append("\nCAUSED BY ");
					sb.append(ExceptionUtils.getDetailedMessage(error));
				}
				throw new RuntimeException(sb.toString(), firstError);
			}
		}
		return eventRaised;
	}


	/**
	 * Will return false if event listener failed to load for any event name
	 */
	@Override
	public synchronized <E extends Event<?, ?>> boolean registerEventListener(EventListener<E> listener) {
		AssertUtils.assertNotNull(listener, "EventHandler failed to register EventListener:  EventListener is null");
		boolean success = true;
		for (String eventName : CollectionUtils.getIterable(listener.getEventNameList())) {
			AssertUtils.assertNotNull(eventName, "EventHandler failed to register EventListener:  getEventName() on EventListener returned null");

			// if a mapping exists for the listener's event name, add the listener to the corresponding list
			if (this.eventMap.containsKey(eventName)) {
				List<EventListener<?>> listeners = this.eventMap.get(eventName);

				if (listeners.contains(listener)) {
					success = false; // Failed to register
				}

				// Add listener if not already registered
				else {
					listeners.add(listener);

					// sort to maintain order
					BeanUtils.sortWithFunction(listeners, EventListener::getOrder, true);
				}
			}
			else {
				List<EventListener<?>> listeners = new ArrayList<>();
				listeners.add(listener);
				this.eventMap.put(eventName, listeners);
			}
		}
		return success;
	}
}
