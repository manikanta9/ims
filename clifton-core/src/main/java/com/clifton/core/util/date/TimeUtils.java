package com.clifton.core.util.date;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.function.Supplier;


/**
 * The <code>DateUtils</code> class provides utility methods that simplify working with {@link Time} objects.
 *
 * @author RogerW
 */
public class TimeUtils {

	public static final NumberFormat NUMBER_FORMAT_SECONDS_PRECISE = new DecimalFormat("#,##0.000");
	public static final long ONE_SECOND_OF_MILLI = 1000;
	public static final long ONE_SECOND_OF_NANO = 1_000_000_000;
	public static final long ONE_MILLI_OF_NANO = 1_000_000;


	private TimeUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts the specified string to corresponding {@link Time} object.
	 * Supports both full and short time formats: "HH:mm:ss", "h:mm aaa".
	 * Returns null if the string is empty.
	 */
	public static Time toTime(String time) {
		if (StringUtils.isEmpty(time)) {
			return null;
		}

		String timeFormat = Time.TIME_FORMAT_FULL;
		if (time.endsWith("AM") || time.endsWith("PM")) {
			timeFormat = Time.TIME_FORMAT_SHORT;
		}

		return Time.parse(time, timeFormat);
	}


	public static BigDecimal convertNanoToSeconds(long nano) {
		return convertNanoToSeconds((double) nano);
	}


	public static BigDecimal convertNanoToSeconds(Long nano) {
		return nano == null ? null : convertNanoToSeconds((long) nano);
	}


	public static BigDecimal convertNanoToSeconds(double nano) {
		return convertNanoToSeconds(BigDecimal.valueOf(nano));
	}


	public static BigDecimal convertNanoToSeconds(BigDecimal nano) {
		return MathUtils.divide(nano, ONE_SECOND_OF_NANO);
	}


	public static BigDecimal convertNanoToMilli(long nano) {
		return convertNanoToMilli(BigDecimal.valueOf(nano));
	}


	public static BigDecimal convertNanoToMilli(Long nano) {
		return nano == null ? null : convertNanoToMilli((long) nano);
	}


	public static BigDecimal convertNanoToMilli(BigDecimal nano) {
		return MathUtils.divide(nano, ONE_MILLI_OF_NANO);
	}


	public static long convertSecondToMilli(BigDecimal seconds) {
		return MathUtils.multiply(seconds, BigDecimal.valueOf(ONE_SECOND_OF_MILLI)).longValue();
	}


	/**
	 * Executes the given operation, tracking and returning its duration.
	 *
	 * @see TimeTrackedOperation
	 */
	public static TimeTrackedOperation<Void> getElapsedTime(Runnable fn) {
		long t0 = System.nanoTime();
		fn.run();
		long t1 = System.nanoTime();
		return new TimeTrackedOperation<>(null, t1 - t0);
	}


	/**
	 * Executes the given operation, tracking and returning its duration and its result.
	 *
	 * @see TimeTrackedOperation
	 */
	public static <T> TimeTrackedOperation<T> getElapsedTime(Supplier<T> fn) {
		long t0 = System.nanoTime();
		T result = fn.get();
		long t1 = System.nanoTime();
		return new TimeTrackedOperation<>(result, t1 - t0);
	}
}
