package com.clifton.core.util.jaxb;

import com.clifton.core.logging.LogUtils;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.StringReader;
import java.io.Writer;


/**
 * The <code>JAXBUtils</code> class provides generic marshalling and unmarshalling functionality for JAXB classes.
 * This was used to provide a JNLP (Java Network Launching Protocol) XML response to replace the
 * functionality that has been disabled due to Chrome removing support for NPAPI (Netscape Plugin Application Programming Interface)
 * However, the same functionality can be used for a variety of validated XML responses using JAXB objects.
 * I've now removed JNLP, not sure if I want to remove this, any needs for JAXB elsewhere?
 *
 * @author stevenf
 */
public class JAXBUtils {

	public <T> T unMarshal(String content, Class<T> clazz) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(clazz);
			Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
			return unmarshaller.unmarshal(new StreamSource(new StringReader(content)), clazz).getValue();
		}
		catch (JAXBException e) {
			LogUtils.error(getClass(), "Failed to unMarshal content: " + content, e);
		}
		return null;
	}


	public <T> void marshal(T object, Writer writer) {
		try {
			JAXBContext jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller marshaller = jaxbContext.createMarshaller();
			marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
			marshaller.marshal(object, writer);
		}
		catch (JAXBException e) {
			LogUtils.error(getClass(), "Failed to marshal object: " + String.valueOf(object), e);
		}
	}
}
