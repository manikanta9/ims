package com.clifton.core.util.http.cookie;

/**
 * Factory for instantiating different IMS CookieStore implementations.
 *
 * @author NickK
 */
public class ImsCookieStoreFactory {

	public static ImsCookieStore newPersistentCookieStore(String name) {
		return new PersistentCookieStore(name);
	}


	public static ImsCookieStore newRamCookieStore() {
		return new InMemoryCookieStore();
	}
}
