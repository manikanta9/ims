package com.clifton.core.util.runner;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.status.StatusHolderObject;

import java.util.Date;


/**
 * The AbstractStatusAwareRunner class should be extended by runner implementations that provide it status updates.
 * The status object can be used to accumulate error details or additional useful information.
 *
 * @author vgomelsky
 */
public abstract class AbstractStatusAwareRunner extends AbstractRunner implements StatusHolderObject<Status> {

	/**
	 * Current execution status of the job that can inform the user where the job is.
	 * For example, "Processed 10 out of 50"
	 */
	private final StatusHolderObject<Status> statusHolder;


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	@Override
	public Status getStatus() {
		return getStatusHolder().getStatus();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractStatusAwareRunner(String type, String typeId, Date runDate) {
		this(type, typeId, runDate, true);
	}


	public AbstractStatusAwareRunner(String type, String typeId, Date runDate, StatusHolderObject<Status> statusHolder) {
		this(type, typeId, runDate, true, statusHolder);
	}


	public AbstractStatusAwareRunner(String type, String typeId, Date runDate, String initialStatus) {
		this(type, typeId, runDate, true, new StatusHolder(initialStatus));
	}


	public AbstractStatusAwareRunner(String type, String typeId, Date runDate, boolean concurrentSupported) {
		this(type, typeId, runDate, concurrentSupported, new StatusHolder(type + "-" + typeId + " scheduled for " + DateUtils.fromDate(runDate)));
	}


	public AbstractStatusAwareRunner(String type, String typeId, Date runDate, boolean concurrentSupported, StatusHolderObject<Status> statusHolder) {
		super(type, typeId, runDate, concurrentSupported);
		this.statusHolder = statusHolder;
	}
}
