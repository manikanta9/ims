package com.clifton.core.util.runner;


import java.util.Date;
import java.util.List;


/**
 * The <code>RunnerProvider</code> interface marks objects that return <link>Runner</link> instances
 * scheduled for the specified time period.
 * <p/>
 * It should be implemented by various modules that would like to use schedule runners: batch jobs, notification, etc.
 * Each RunnerProvider can use its own scheduling logic and return provider specific Runner instances.
 *
 * @author mwacker
 */
public interface RunnerProvider<T> {

	/**
	 * Returns a List of <link>Runner</link> instances schedule for the specified time period.
	 */
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime);


	/**
	 * Creates a runner for the given entity on the given date.
	 */
	public Runner createRunnerForEntityAndDate(T entity, Date runnerDate);


	/**
	 * Gets a list of runners for a given scheduled object that occur between the start and end date/times.
	 */
	public List<Runner> getOccurrencesBetweenForEntity(T entity, Date startDateTime, Date endDateTime);
}
