package com.clifton.core.util.matching;


import java.util.List;


/**
 * The <code>MatchingMatcher</code> defines a matcher used to match to lists.
 *
 * @param <I>
 * @param <E>
 * @author mwacker
 */
public interface MatchingMatcher<I, E> {

	/**
	 * Build the match for the list.
	 */
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList);


	/**
	 * Match using combination sizes.
	 */
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList, List<Integer> externalCombinationSizes);


	/**
	 * Match using a target combination size.
	 */
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList, int combinationSize);


	/**
	 * Match using a target combination size.
	 */
	public MatchingResult<I, E> match(List<I> internalList, List<E> externalList, int combinationSize, boolean allowPartialMatch);
}
