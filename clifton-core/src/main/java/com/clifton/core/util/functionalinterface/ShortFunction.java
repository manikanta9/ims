package com.clifton.core.util.functionalinterface;

import java.util.function.Function;


/**
 * Represents a function that accepts an short-valued argument and produces a
 * result.  This is the {@code int}-consuming primitive specialization for
 * {@link Function}.
 * Note:  created because java.util.function only offers the IntFunction<R> interface
 *
 * @param <R> the type of the result of the function
 * @author davidi
 */
@FunctionalInterface
public interface ShortFunction<R> {

	/**
	 * Applies this function to the given argument.
	 *
	 * @param value the function argument
	 * @return the function result
	 */
	R apply(short value);
}
