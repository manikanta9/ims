package com.clifton.core.util.math;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.FormatUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.apfloat.Apcomplex;
import org.apfloat.Apfloat;
import org.apfloat.calc.ApfloatCalculatorImpl;
import org.apfloat.calc.CalculatorParser;
import org.apfloat.calc.ParseException;

import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;


/**
 * The <code>CoreMathUtils</code> class contains methods used for working with Numbers
 *
 * @author manderson
 */
public class CoreMathUtils {

	private CoreMathUtils() {
		// Disable instantiation
	}

	////////////////////////////////////////////////////////////////////////////
	//////                    Summation Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * For example: BigDecimal localDebitCredit = CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit, true);
	 */
	public static <T> BigDecimal sumProperty(Collection<T> collection, Function<T, BigDecimal> functionReturningValueToSum, boolean useAbsoluteValues) {
		if (CollectionUtils.isEmpty(collection)) {
			return BigDecimal.ZERO;
		}
		if (useAbsoluteValues) {
			return collection.stream().filter(Objects::nonNull).map(functionReturningValueToSum).map(MathUtils::abs).reduce(BigDecimal.ZERO, MathUtils::add);
		}
		return collection.stream().filter(Objects::nonNull).map(functionReturningValueToSum).reduce(BigDecimal.ZERO, MathUtils::add);
	}


	/**
	 * Applies the provided adjustment to the property of the object resolved by the supplier only if the adjustment is a number not equal to zero. Returns  PropertyValueAdjustmentResults result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applyPropertyAdjustment(Supplier<T> applyToObjectSupplier, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal adjustment) {
		return applyPropertyAdjustment(applyToObjectSupplier, valueGetter, valueSetter, adjustment, null);
	}


	/**
	 * Applies the provided adjustment to the property of the object resolved by the supplier only if the adjustment is a number not equal to zero and is less than or equal to the provided threshold. Returns PropertyValueAdjustmentResults result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applyPropertyAdjustment(Supplier<T> applyToObjectSupplier, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal adjustment, BigDecimal threshold) {
		if (MathUtils.isNullOrZero(adjustment)) {
			return MathUtils.PropertyValueAdjustmentResults.NO_ADJUSTMENT;
		}
		if (threshold == null || MathUtils.isLessThanOrEqual(MathUtils.abs(adjustment), threshold)) {
			// Only apply to object if difference is not zero and less than threshold
			T applyToObject = applyToObjectSupplier.get();
			MathUtils.addValueToProperty(applyToObject, valueGetter, valueSetter, adjustment);
			return MathUtils.PropertyValueAdjustmentResults.VALUE_ADJUSTED_TO_MATCH;
		}
		return MathUtils.PropertyValueAdjustmentResults.ADJUSTMENT_TOO_GREAT;
	}


	/**
	 * Given a list of values and a list of associated weights, adjusted value
	 * by it's weight (value * weight) and return the adjusted list
	 */
	public static List<BigDecimal> weightAdjustedValueList(List<BigDecimal> valueList, List<BigDecimal> weightList) {
		if (CollectionUtils.getSize(valueList) != CollectionUtils.getSize(weightList)) {
			throw new RuntimeException("Weighted adjusted value calculation requires a weight per value. Amount of values does not equal amount of weights.");
		}
		List<BigDecimal> weightedValues = new ArrayList<>();
		int size = CollectionUtils.getSize(valueList);
		for (int i = 0; i < size; i++) {
			weightedValues.add(MathUtils.multiply(valueList.get(i), weightList.get(i)));
		}
		return weightedValues;
	}


	/**
	 * Multiply values by their corresponding weight and average the results.
	 * <p>
	 * Value and weight list must be the same size where the corresponding weight is at the same index as its value.
	 */
	public static BigDecimal weightedAverage(List<BigDecimal> valueList, List<BigDecimal> weightList) {
		if (CollectionUtils.getSize(valueList) != CollectionUtils.getSize(weightList)) {
			throw new RuntimeException("Weighted average calculation requires a weight per value. Amount of values does not equal amount of weights.");
		}
		List<BigDecimal> weightedValues = weightAdjustedValueList(valueList, weightList);

		return average(weightedValues);
	}


	/**
	 * Determines the <i>arithmetic mean</i> of the collection, or <tt>0</tt> if no values are found.
	 *
	 * @param valueList the collection of values for which to find the mean
	 * @return the arithmetic mean of the collection, or <tt>0</tt> if no values are found
	 * @apiNote Returning <tt>0</tt> for <tt>null</tt> and empty inputs is kept here for compatibility reasons. This does not fit the definition of an average (the average of an
	 * empty set is undefined). Ideally, usages which depend on this behavior should be refactored and this method should be deleted.
	 * @see #meanAverage(Collection)
	 */
	public static BigDecimal average(Collection<BigDecimal> valueList) {
		if (CollectionUtils.isEmpty(valueList)) {
			return BigDecimal.ZERO;
		}
		return CoreMathUtils.meanAverage(valueList);
	}


	/**
	 * Determines the <b>arithmetic mean</b> of the collection, or <tt>null</tt> if no values are found.
	 *
	 * @param values the collection of values for which to find the mean
	 * @return the arithmetic mean of the collection
	 */
	public static BigDecimal meanAverage(Collection<BigDecimal> values) {
		if (CollectionUtils.isEmpty(values)) {
			return null;
		}

		BigDecimal total = CoreMathUtils.sum(values);
		return MathUtils.divide(total, CollectionUtils.getSize(values));
	}


	/**
	 * Determines of the <b>statistical mode(s)</b> of the collection, or <tt>null</tt> if no values are found.
	 * <p>
	 * More than one mode may exist when multiple data points have the maximum frequency.
	 *
	 * @param values the collection of values for which to find the mode
	 * @return the set of statistical modes of the collection
	 */
	public static Collection<BigDecimal> modeAverage(Collection<BigDecimal> values) {
		// Generate a histogram of existing values
		Map<BigDecimal, Long> histogram = CollectionUtils.getHistogram(values);

		// Determine the highest frequency of any value in the collection
		Optional<Long> modeFrequency = histogram.entrySet().stream()
				.max((val1, val2) -> MathUtils.compare(val1.getValue(), val2.getValue()))
				.map(Map.Entry::getValue);

		// Find all values in the collection which have the given maximum frequency
		return modeFrequency
				.map(maxValue -> histogram.entrySet().stream()
						.filter(entry -> maxValue.equals(entry.getValue()))
						.map(Map.Entry::getKey)
						.collect(Collectors.toSet()))
				.orElse(null);
	}


	/**
	 * Determines the <b>statistical median</b> of the collection, or <tt>null</tt> if no values are found.
	 *
	 * @param values the collection of values for which to find the median
	 * @return the statistical median of the collection
	 */
	public static BigDecimal medianAverage(Collection<BigDecimal> values) {
		if (CollectionUtils.isEmpty(values)) {
			return null;
		}

		// Sort values and find center value
		List<BigDecimal> sortedValues = CollectionUtils.createSorted(values);
		final BigDecimal median;
		int centerIndex = sortedValues.size() / 2;
		if (sortedValues.size() % 2 == 1) {
			// Odd value; median exists in set
			median = sortedValues.get(centerIndex);
		}
		else {
			// Even value; median must be inferred from mean of two center values
			BigDecimal leftMedian = sortedValues.get(centerIndex - 1);
			BigDecimal rightMedian = sortedValues.get(centerIndex);
			median = CoreMathUtils.meanAverage(CollectionUtils.createList(leftMedian, rightMedian));
		}
		return median;
	}


	/**
	 * Determines the <b>standard deviation</b> of the set.
	 * <p>
	 * This uses the <b>sample</b> method of determining the standard deviation rather than the <b>population</b> method. The population method assumes that the provided data set
	 * is a single complete representation and that no other values (e.g., unaccounted-for past or future values) might exist which would skew the result. If no such values exist,
	 * then the {@link #populationStandardDeviation(Collection) <b>population standard deviation</b>} should be used for a more precise calculation.
	 *
	 * @param valueList the set of numeric data
	 * @return the standard deviation of the set
	 * @see #sampleStandardDeviation(Collection)
	 */
	public static BigDecimal standardDeviation(Collection<BigDecimal> valueList) {
		return CoreMathUtils.sampleStandardDeviation(valueList);
	}


	/**
	 * Determines the <b>population standard deviation</b> of the set.
	 * <p>
	 * The standard deviation measures the variation of values in the set with respect to the mean average of the set. Standard deviation is one measure of volatility which is
	 * often interpreted as risk.
	 * <p>
	 * The <i>population</i> standard deviation assumes that the entire data set is known and provided. If only a sample of the data set is known then the
	 * {@link #sampleStandardDeviation(Collection) sample standard deviation} should be used instead.
	 * <p>
	 * The population standard deviation is defined as the square root of the <b>population variance</b>. The formula is as follows:
	 * <pre>
	 * σ = √( Σ((x_i - μ)^2) / n )
	 * </pre>
	 * where the following symbols are used:
	 * <ul>
	 * <li><b>σ:</b> The population standard deviation
	 * <li><b>x_i:</b> The <i>i</i>th data point in the set
	 * <li><b>μ:</b> The mean average of the set
	 * <li><b>n:</b> The number of data points in the set
	 * </ul>
	 * <p>
	 * For empty sets, the standard deviation is not defined and this method will return <tt>null</tt>.
	 *
	 * @param valueList the set of numeric data
	 * @return the population standard deviation of the given set
	 * @see #populationVariance(Collection)
	 */
	public static BigDecimal populationStandardDeviation(Collection<BigDecimal> valueList) {
		BigDecimal variance = CoreMathUtils.populationVariance(valueList);
		return MathUtils.sqrt(variance);
	}


	/**
	 * Determines the <b>sample standard deviation</b> of the set.
	 * <p>
	 * The standard deviation measures the variation of values in the set with respect to the mean average of the set. Standard deviation is one measure of volatility which is
	 * often interpreted as risk.
	 * <p>
	 * The <i>sample</i> standard deviation applies an estimated correction for the assumption that the provided data set is an incomplete representation of a larger data set. If
	 * the provided data is complete then the {@link #populationStandardDeviation(Collection) population standard deviation} should be used instead.
	 * <p>
	 * The sample standard deviation is defined as the square root of the <b>sample variance</b>. The formula is as follows:
	 * <pre>
	 * s = √( Σ((x_i - x_avg)^2) / (n - 1) )
	 * </pre>
	 * where the following symbols are used:
	 * <ul>
	 * <li><b>s:</b> The sample standard deviation
	 * <li><b>x_i:</b> The <i>i</i>th data point in the sample
	 * <li><b>x_avg:</b> The mean average of the sample
	 * <li><b>n:</b> The number of data points in the sample
	 * </ul>
	 * <p>
	 * For empty sets, the standard deviation is not defined and this method will return <tt>null</tt>.
	 *
	 * @param valueList the set of numeric data
	 * @return the sample variance of the given set
	 * @see #sampleVariance(Collection)
	 */
	public static BigDecimal sampleStandardDeviation(Collection<BigDecimal> valueList) {
		BigDecimal variance = CoreMathUtils.sampleVariance(valueList);
		return MathUtils.sqrt(variance);
	}


	/**
	 * Determines the <b>population variance</b> of the set.
	 * <p>
	 * The population variance is defined as the average of the squared errors of the set with respect to the mean (square mean differences). The formula is as follows:
	 * <pre>
	 * σ^2 = Σ((x_i - μ)^2) / n
	 * </pre>
	 * where the following symbols are used:
	 * <ul>
	 * <li><b>σ^2:</b> The population variance
	 * <li><b>σ:</b> The population standard deviation
	 * <li><b>x_i:</b> The <i>i</i>th data point in the set
	 * <li><b>μ:</b> The mean average of the set
	 * <li><b>n:</b> The number of data points in the set
	 * </ul>
	 * <p>
	 * For empty sets, the variance is not defined and this method will return <tt>null</tt>.
	 *
	 * @param valueList the set of numeric data
	 * @return the population variance of the given set
	 */
	public static BigDecimal populationVariance(Collection<BigDecimal> valueList) {
		// Guard-clause: Variance is not defined for empty sets
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		// Generate a list of mean square differences
		BigDecimal mean = CoreMathUtils.meanAverage(valueList);
		List<BigDecimal> squaredMeanDifferenceList = CollectionUtils.getStream(valueList)
				.map(value -> MathUtils.subtract(value, mean))
				.map(MathUtils::squareValue)
				.collect(Collectors.toList());

		// Determine the average of mean square differences
		return CoreMathUtils.meanAverage(squaredMeanDifferenceList);
	}


	/**
	 * Determines the <b>sample variance</b> of the set.
	 * <p>
	 * The sample variance is defined as the sum the squared residuals of the set with respect to the mean (square mean differences) divided by the number of degrees of
	 * freedom, where the number of degrees of freedom is decreased by <tt>1</tt> from the number of degrees of freedom used in the {@link #populationVariance(Collection)
	 * <b>population variance</b>} via <a href="https://en.wikipedia.org/wiki/Bessel%27s_correction">Bessel's correction</a>. The formula is as follows:
	 * <pre>
	 * s^2 = Σ((x_i - x_avg)^2) / (n - 1)
	 * </pre>
	 * where the following symbols are used:
	 * <ul>
	 * <li><b>s^2:</b> The sample variance
	 * <li><b>s:</b> The sample standard deviation
	 * <li><b>x_i:</b> The <i>i</i>th data point in the sample
	 * <li><b>x_avg:</b> The mean average of the sample
	 * <li><b>n:</b> The number of data points in the sample
	 * </ul>
	 * <p>
	 * For empty sets, the variance is not defined and this method will return <tt>null</tt>.
	 *
	 * @param valueList the set of numeric data
	 * @return the sample variance of the given set
	 */
	public static BigDecimal sampleVariance(Collection<BigDecimal> valueList) {
		// Guard-clause: Variance is not defined for empty sets
		if (CollectionUtils.isEmpty(valueList)) {
			return null;
		}

		// Generate a sum of mean square differences
		BigDecimal mean = CoreMathUtils.meanAverage(valueList);
		BigDecimal squareMeanDifferenceSum = CollectionUtils.getStream(valueList)
				.map(value -> MathUtils.subtract(value, mean))
				.map(MathUtils::squareValue)
				.reduce(BigDecimal.ZERO, BigDecimal::add);

		// Divide the sum of mean square difference by the number of degrees of freedom
		BigDecimal degreesOfFreedom = BigDecimal.valueOf(CollectionUtils.getSize(valueList) - 1);
		if (MathUtils.isEqual(degreesOfFreedom, BigDecimal.ZERO)) {
			return BigDecimal.ZERO;
		}
		return MathUtils.divide(squareMeanDifferenceSum, degreesOfFreedom);
	}


	/**
	 * Returns the difference between the sum of the list and the total expected (Total Expected - SUM)
	 */
	public static <T> BigDecimal getSumPropertyDifference(List<T> list, Function<T, BigDecimal> functionReturningValueToSum, BigDecimal totalExpected) {
		BigDecimal sum = sumProperty(list, functionReturningValueToSum);
		return MathUtils.subtract(totalExpected, sum);
	}


	/**
	 * Adds the specified properties from "from" bean to "to" bean and updates them on "to" bean.
	 */
	public static <T> void addProperties(T to, T from, String[] propertyNames) {
		for (String propertyName : propertyNames) {
			BeanUtils.setPropertyValue(to, propertyName, MathUtils.add((BigDecimal) BeanUtils.getPropertyValue(to, propertyName), (BigDecimal) BeanUtils.getPropertyValue(from, propertyName)));
		}
	}


	/**
	 * Subtracts all specified properties retrieved from the "values" bean from the "from" bean. Updates these properties on "from" bean.
	 */
	public static <T> void subtractProperties(T from, T values, String[] propertyNames) {
		for (String propertyName : propertyNames) {
			BeanUtils.setPropertyValue(from, propertyName, MathUtils.subtract((BigDecimal) BeanUtils.getPropertyValue(from, propertyName), (BigDecimal) BeanUtils.getPropertyValue(values, propertyName)));
		}
	}


	/**
	 * Determines the maximum value in the given collection. Returns <tt>null</tt> if no such value is found.
	 *
	 * @param values the collection of values
	 * @return the maximum value in the collection, or <tt>null</tt> if no values were found
	 * @see #maxNumeric(Collection)
	 */
	public static BigDecimal max(Collection<BigDecimal> values) {
		return CollectionUtils.getStream(values)
				.max(BigDecimal::compareTo)
				.orElse(null);
	}


	/**
	 * Determines the maximum value in the given collection. Returns <tt>null</tt> if no such value is found.
	 * <p>
	 * This method differs from {@link #max(Collection)} in that it applies generically for all numbers, including those of mismatched precision. All values will be compared at
	 * {@link Double} precision.
	 *
	 * @param numberList the list of numbers from which to find the maximum value
	 * @param <T>        the type of the list, and thus the resulting type for the method
	 * @see #max(Collection)
	 */
	public static <T extends Number> T maxNumeric(Collection<T> numberList) {
		T currentMax = null;
		for (T number : CollectionUtils.getIterable(numberList)) {
			if (number == null) {
				continue;
			}
			if (currentMax == null || MathUtils.compare(number, currentMax) > 0) {
				currentMax = number;
			}
		}
		return currentMax;
	}


	/**
	 * Returns the bean with the max/min value for the given property
	 * <p>
	 * Sorts the given list by the property (max - desc, min - asc) and returns the first item in the list.
	 *
	 * @param max (if false returns bean is min value for property)
	 */
	public static <T> T getBeanWithMaxProperty(List<T> list, String propertyName, boolean max) {
		return CollectionUtils.getFirstElement(BeanUtils.sortWithPropertyNames(list, CollectionUtils.createList(propertyName), CollectionUtils.createList(!max)));
	}


	public static <T> T getBeanWithMaxProperty(List<T> list, Function<T, Object> functionReturningValue, boolean max, Predicate<T> canApplyFunction) {
		if (canApplyFunction == null) {
			canApplyFunction = t -> true;
		}
		return CollectionUtils.getFirstElement(BeanUtils.sortWithFunction(BeanUtils.filter(list, canApplyFunction), functionReturningValue, !max));
	}


	/**
	 * Retrieves the minimum value out of the given array of numbers. If no numbers exist or all numbers are <code>null</code>, then <code>null</code> will be returned.
	 */
	@SafeVarargs
	public static <T extends Number> T min(T... numbers) {
		T currentMin = null;
		for (T number : numbers) {
			if (number != null
					&& (currentMin == null || MathUtils.compare(number, currentMin) < 0)) {
				currentMin = number;
			}
		}
		return currentMin;
	}


	/**
	 * Determines the minimum value in the given collection. Returns <tt>null</tt> if no such value is found.
	 *
	 * @param values the collection of values
	 * @return the minimum value in the collection, or <tt>null</tt> if no values were found
	 */
	public static BigDecimal min(Collection<BigDecimal> values) {
		return CollectionUtils.getStream(values)
				.min(BigDecimal::compareTo)
				.orElse(null);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                   Percentage Methods                          ///////
	////////////////////////////////////////////////////////////////////////////


	public static <T> BigDecimal getTotalPercentChange(List<T> list, Function<T, BigDecimal> valueGetter, boolean percentageAsHundredsValue) {
		if (CollectionUtils.isEmpty(list)) {
			return BigDecimal.ZERO;
		}
		BigDecimal value = BigDecimal.ONE;
		for (T object : list) {
			BigDecimal v = valueGetter.apply(object);
			if (v == null) {
				v = BigDecimal.ZERO;
			}
			if (percentageAsHundredsValue) {
				v = MathUtils.divide(v, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
			}
			BigDecimal thisV = MathUtils.add(BigDecimal.ONE, v);
			value = MathUtils.multiply(value, thisV);
		}
		value = MathUtils.subtract(value, BigDecimal.ONE);
		if (percentageAsHundredsValue) {
			value = MathUtils.multiply(value, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}

		return value;
	}


	private static void validatePercentValue(BigDecimal value, BigDecimal total, BigDecimal resultPercentage) {
		if (MathUtils.isGreaterThan(MathUtils.abs(resultPercentage), MathUtils.MAX_PERCENT_VALUE)) {
			throw new ValidationException(formatNumberDecimal(value) + " as a percent of " + formatNumberDecimal(total) + " results in a percentage that is too large ["
					+ formatNumberDecimal(resultPercentage) + "].  The maximum percentage value cannot exceed [" + formatNumberDecimal(MathUtils.MAX_PERCENT_VALUE) + "].");
		}
	}


	/**
	 * Takes the total percent change for the given values and annualizes the value
	 * <p>
	 * ((ABS(1 + TOTAL_PERCENT_CHANGE))^(1/(PERCENT_COUNT – 12))) – 1
	 * <p>
	 * Number of months can be different than the count of percentages.  This usually means there may be a gap
	 * in the percentages where a 0% value would have been used.  If number of months value is null, will just use the
	 * percentage count.
	 */
	public static <T> BigDecimal getTotalPercentChangeAnnualizedMonthly(List<T> list, Function<T, BigDecimal> valueGetter, boolean percentageAsHundredsValue, Integer numberOfMonths) {
		return getTotalPercentChangeAnnualizedImpl(list, valueGetter, percentageAsHundredsValue, 12, numberOfMonths);
	}


	/**
	 * Takes the total percent change for the given values and annualizes the value
	 * <p>
	 * ((ABS(1 + TOTAL_PERCENT_CHANGE))^(1/(PERCENT_COUNT – 365))) – 1
	 */
	public static <T> BigDecimal getTotalPercentChangeAnnualizedDaily(List<T> list, Function<T, BigDecimal> valueGetter, boolean percentageAsHundredsValue, Integer numberOfDays) {

		return getTotalPercentChangeAnnualizedImpl(list, valueGetter, percentageAsHundredsValue, 365, numberOfDays);
	}


	private static <T> BigDecimal getTotalPercentChangeAnnualizedImpl(List<T> list, Function<T, BigDecimal> valueGetter, boolean percentageAsHundredsValue, Integer factor, Integer count) {
		if (CollectionUtils.isEmpty(list)) {
			return BigDecimal.ZERO;
		}
		if (count == null) {
			count = CollectionUtils.getSize(list);
		}

		BigDecimal value = getTotalPercentChange(list, valueGetter, percentageAsHundredsValue);
		value = annualizeValue(value, percentageAsHundredsValue, factor, count);
		return value;
	}


	/**
	 * ((ABS(1 + TOTAL_PERCENT_CHANGE))^(1/(PERCENT_COUNT – FACTOR))) – 1
	 */
	private static BigDecimal annualizeValue(BigDecimal value, boolean percentageAsHundredsValue, int factor, int count) {
		if (percentageAsHundredsValue) {
			value = MathUtils.divide(value, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}
		value = MathUtils.abs(MathUtils.add(value, BigDecimal.ONE));
		BigDecimal power = MathUtils.divide(BigDecimal.ONE, MathUtils.divide(BigDecimal.valueOf(count), BigDecimal.valueOf(factor)));
		value = BigDecimal.valueOf(Math.pow(value.doubleValue(), power.doubleValue()));
		value = MathUtils.subtract(value, 1);

		if (percentageAsHundredsValue) {
			value = MathUtils.multiply(value, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		}

		return value;
	}


	/**
	 * Annualize a return that was calculated using daily or monthly values.
	 * <p>
	 * Start date used for beginning return price, End date used for end return price
	 */
	public static BigDecimal annualizeByDateRange(BigDecimal returnValue, Date startDate, Date endDate, boolean dailyNotMonthlyCalculation, boolean percentageAsHundredsValue) {
		int count;
		if (dailyNotMonthlyCalculation) {
			count = DateUtils.getDaysDifference(endDate, startDate);
		}
		else {
			count = DateUtils.getMonthsDifference(endDate, startDate);
		}

		//No need to annualize
		int factor = (dailyNotMonthlyCalculation) ? 365 : 12;
		if (count <= factor) {
			return returnValue;
		}

		return annualizeValue(returnValue, percentageAsHundredsValue, factor, count);
	}


	public static BigDecimal getValueProrated(BigDecimal count, BigDecimal totalCount, BigDecimal value) {
		if (MathUtils.isNullOrZero(value) || MathUtils.isNullOrZero(totalCount)) {
			return value;
		}
		if (MathUtils.isNullOrZero(count)) {
			return BigDecimal.ZERO;
		}
		if (MathUtils.isEqual(count, totalCount)) {
			return value;
		}
		BigDecimal factor = MathUtils.divide(count, totalCount);
		return MathUtils.multiply(value, factor);
	}


	/**
	 * Returns the DV01 of the given value.
	 * DV01 is the Dollar Value of on basis point = (0.01%) or the value divided by 10,000
	 */
	public static BigDecimal getDV01Value(BigDecimal value) {
		return MathUtils.divide(value, BigDecimal.valueOf(10000));
	}


	/**
	 * Returns String representation of the number using the default format #,###.##
	 * Returns empty string for null values
	 */
	public static String formatNumberMoney(BigDecimal d1) {
		return formatNumber(d1, null);
	}


	/**
	 * Returns String representation of the number using the max decimals we currently use
	 * format #,###.################ (currently 16 Decimal places is the max)
	 * Used by Audit Trail to store differences, so we need to support the most allowed
	 * Returns empty string for null values
	 */
	public static String formatNumberDecimalMax(BigDecimal d1) {
		return formatNumber(d1, MathUtils.NUMBER_FORMAT_DECIMAL_MAX);
	}


	/**
	 * EvaluateExpression allows us to evaluate string mathematical expressions
	 * <p>
	 * Ex. "2+3+5" will evaluate to 10
	 * <p>
	 * List of supported functions : http://www.apfloat.org/apfloat_java/applet/calculator-help.html
	 */
	public static BigDecimal evaluateExpression(String expression) {
		Writer out = new StringWriter();

		//ApFloat is a strong math library but their parser is a bit lacking
		//When it parses out each of the decimals from the string it by default uses the precision
		// specified by the input itself (1.0, 1.000, 1.0000, etc) (No way to easily configure)
		// Therefore certain operations can cause a loss of precision depending on what is supplied.
		// To mitigate this, overriding the decimal parsing logic to specify a ApFloat default precision of 50 digits
		//Ex. new Apfloat(value, 50); whereas that was by default before new Apfloat(value), default precision
		ApfloatCalculatorImpl calculator = new ApfloatCalculatorImpl() {

			@Override
			public Number parseDecimal(String value) {
				Number x;
				if (value.endsWith("i") || value.endsWith("I")) {
					x = new Apfloat(value.substring(0, value.length() - 1), 50).multiply(Apcomplex.I);
				}
				else {
					x = new Apfloat(value, 50);
				}
				return x;
			}
		};

		calculator.setFormat(true);

		//Clean expression up by removing any possible commas and force
		CalculatorParser pe = new CalculatorParser(new StringReader(("(" + expression + ")*1.00000000000000000000000000000000000000000000000000").replaceAll(",", "")), out, calculator);

		try {
			pe.parseOneLine();
		}
		catch (ParseException e) {
			throw new RuntimeException("Your expression [" + expression + "] is improperly formatted");
		}

		String val = out.toString();
		return new BigDecimal(val.trim());
	}


	/**
	 * Sums the values of the given property name in the list
	 * NOTE: SHOULD ONLY BE USED IN RARE CASES - USE sumPropertyName with Functions instead
	 */
	public static <T> BigDecimal sumPropertyName(List<T> list, String propertyName) {
		Object[] values = BeanUtils.getPropertyValues(list, propertyName);
		return ArrayUtils.getStream(values)
				.filter(Objects::nonNull)
				.map(value -> {
					if (!(value instanceof BigDecimal)) {
						throw new IllegalArgumentException("Property type must be a BigDecimal in order to sum values.  Property [" + propertyName + "] is of type [" + value.getClass() + "]");
					}
					return (BigDecimal) value;
				})
				.reduce(BigDecimal.ZERO, MathUtils::add);
	}


	/**
	 * Percentage Of:  Returns the percentage of the value passed compared to the total
	 * If returnAsHundreds is true, will return the fraction result * 100.
	 * For example 0.17 fraction value would be returned as 17 if returnAsHundredsValue is true
	 */
	public static BigDecimal getPercentValue(BigDecimal value, BigDecimal total, boolean returnAsHundredsValue) {
		return getPercentValue(value, total, returnAsHundredsValue, false);
	}


	/**
	 * Percentage Of:  Returns the percentage of the value passed compared to the total
	 * If returnAsHundreds is true, will return the fraction result * 100.
	 * For example 0.17 fraction value would be returned as 17 if returnAsHundredsValue is true
	 * <p>
	 * if validateMaxPercentage is true, will validate based on our database convention of 9,5 decimal data type that the absolute value of
	 * the percentage does not exceed 9,999.99999 %  Note this validates on the actual percent value, not the fractional component even if returned as the fraction
	 */
	public static BigDecimal getPercentValue(BigDecimal value, BigDecimal total, boolean returnAsHundredsValue, boolean validateMaxPercentage) {
		if (value == null || total == null) {
			return null;
		}
		if (MathUtils.isEqual(0, value) || MathUtils.isEqual(0, total)) {
			return BigDecimal.ZERO;
		}
		BigDecimal percent = MathUtils.multiply(value, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
		percent = MathUtils.divide(percent, total);

		if (validateMaxPercentage) {
			validatePercentValue(value, total, percent);
		}
		if (returnAsHundredsValue) {
			return percent;
		}
		return MathUtils.divide(percent, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
	}


	public static BigDecimal sum(Collection<BigDecimal> values) {
		BigDecimal total = BigDecimal.ZERO;
		for (BigDecimal v : CollectionUtils.getIterable(values)) {
			total = MathUtils.add(total, v);
		}
		return total;
	}


	public static BigDecimal sum(BigDecimal... values) {
		return ArrayUtils.getStream(values)
				.reduce(BigDecimal.ZERO, MathUtils::add);
	}


	/**
	 * Returns String representation of the number using the given format
	 * Returns empty string for null values.
	 * <p>
	 * Defaults formatting to #,###.## if no formatter is specified
	 */
	public static String formatNumber(Number d1, String decimalFormat) {
		if (d1 == null) {
			return "";
		}
		NumberFormat formatter = FormatUtils.getDecimalFormat((decimalFormat == null) ? MathUtils.NUMBER_FORMAT_MONEY : decimalFormat);
		return formatter.format(d1);
	}


	/**
	 * Returns String representation of the number using the following decimal format #,###.##########
	 * Returns empty string for null values
	 */
	public static String formatNumberDecimal(BigDecimal d1) {
		return formatNumber(d1, MathUtils.NUMBER_FORMAT_DECIMAL);
	}


	/**
	 * Returns String representation of the number using the integer format #,###
	 * Returns empty string for null values
	 */
	public static String formatNumberInteger(Number d1) {
		return formatNumber(d1, MathUtils.NUMBER_FORMAT_INTEGER);
	}


	/**
	 * For example: BigDecimal localDebitCredit = CoreMathUtils.sumProperty(transactionList, AccountingTransaction::getLocalDebitCredit);
	 */
	public static <T> BigDecimal sumProperty(Collection<T> collection, Function<T, BigDecimal> functionReturningValueToSum) {
		return sumProperty(collection, functionReturningValueToSum, false);
	}


	/**
	 * Percentage Of:  Returns the percentage (as a fraction) of the value passed compared to the total
	 */
	public static BigDecimal getPercentValue(BigDecimal value, BigDecimal total) {
		return getPercentValue(value, total, false);
	}


	/**
	 * Applies a sum difference to the property of the object of the list with the min or max value of the value getter function. Returns a {@link MathUtils.PropertyValueAdjustmentResults} result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applySumPropertyDifference(List<T> list, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal totalExpected, boolean applyToMax) {
		return applySumPropertyDifference(list, valueGetter, valueSetter, totalExpected, applyToMax, null, null);
	}


	/**
	 * Applies a sum difference to the property of the object of the list with the min or max value of the value getter function after filtering the collection with the can apply predicate. Returns a {@link MathUtils.PropertyValueAdjustmentResults} result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applySumPropertyDifference(List<T> list, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal totalExpected, boolean applyToMax, Predicate<T> canApplyFunction) {
		return applySumPropertyDifference(list, valueGetter, valueSetter, totalExpected, applyToMax, canApplyFunction, null);
	}


	/**
	 * Applies a sum difference to the property of the object of the list with the min or max value of the max value getter function after filtering the collection with the can apply predicate. Returns a {@link MathUtils.PropertyValueAdjustmentResults} result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applySumPropertyDifference(List<T> list, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal totalExpected, boolean applyToMax,
	                                                                                      Predicate<T> canApplyFunction, Function<T, BigDecimal> maxValueFunction) {
		Supplier<T> applyToObjectSupplier = () -> getBeanWithMaxProperty(list, (maxValueFunction == null ? valueGetter : maxValueFunction)::apply, applyToMax, canApplyFunction);
		return applySumPropertyDifference(list, valueGetter, valueSetter, totalExpected, applyToObjectSupplier, null);
	}


	/**
	 * Applies a sum difference to the property of the object resolved by the supplier. Returns a {@link MathUtils.PropertyValueAdjustmentResults} result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applySumPropertyDifference(List<T> list, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal totalExpected, Supplier<T> applyToObjectSupplier) {
		return applySumPropertyDifference(list, valueGetter, valueSetter, totalExpected, applyToObjectSupplier, null);
	}


	/**
	 * Applies a sum difference to the property of the object resolved by the supplier only if the difference is less than or equal to the provided threshold value. Returns a {@link MathUtils.PropertyValueAdjustmentResults} result for the application of the difference.
	 */
	public static <T> MathUtils.PropertyValueAdjustmentResults applySumPropertyDifference(List<T> list, Function<T, BigDecimal> valueGetter, BiConsumer<T, BigDecimal> valueSetter, BigDecimal totalExpected, Supplier<T> applyToObjectSupplier, BigDecimal threshold) {
		BigDecimal dif = getSumPropertyDifference(list, valueGetter, totalExpected);
		return applyPropertyAdjustment(applyToObjectSupplier, valueGetter, valueSetter, dif, threshold);
	}
}


