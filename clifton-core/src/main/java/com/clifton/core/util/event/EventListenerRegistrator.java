package com.clifton.core.util.event;


import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.util.AssertUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


/**
 * The <code>EventListenerRegistrator</code> class registers {@link EventListener}s that are defined in
 * the ApplicationContext at startup.
 *
 * @author rbrooks
 */
@Component
public class EventListenerRegistrator implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private EventHandler eventHandler;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		AssertUtils.assertNotNull(getEventHandler(), "EventHandler has not been instantiated on the EventListenerRegistrator");

		String[] beanNames = event.getApplicationContext().getBeanNamesForType(EventListener.class);
		for (String beanName : beanNames) {
			EventListener<?> listener = event.getApplicationContext().getBean(beanName, EventListener.class);
			getEventHandler().registerEventListener(listener);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public EventHandler getEventHandler() {
		return this.eventHandler;
	}


	public void setEventHandler(EventHandler eventHandler) {
		this.eventHandler = eventHandler;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
