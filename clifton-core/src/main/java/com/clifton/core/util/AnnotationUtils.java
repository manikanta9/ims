package com.clifton.core.util;

import com.clifton.core.util.beans.MethodUtils;
import org.springframework.util.ConcurrentReferenceHashMap;

import java.lang.annotation.Annotation;
import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Method;
import java.util.Map;
import java.util.function.Predicate;


/**
 * The <code>AnnotationUtils</code> utilizes Spring AnnotationUtils under the covers.
 * It allows for annotations to be discovered directly on elements, or any annotations
 * declared on their superclass interfaces.
 * <p>
 * The Spring annotation utilities support meta-annotations, inherited annotations beyond the standard Java 5 support, and
 * other annotation declarations, such as annotations on <a href="https://docs.oracle.com/javase/tutorial/java/generics/bridgeMethods.html"> bridge methods</a>.
 * See {@link org.springframework.core.annotation.AnnotationUtils} for more information.
 */
public class AnnotationUtils {

	private static final Map<Class<?>, Annotation[]> classDeepScanAnnotationCache = new ConcurrentReferenceHashMap<>(256);
	private static final Map<Method, Annotation[]> methodDeepScanAnnotationCache = new ConcurrentReferenceHashMap<>(256);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AnnotationUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns only annotations specified directly on the method
	 */
	public static Annotation[] getDeclaredAnnotations(Method method) {
		return getAnnotations(method, false);
	}


	/**
	 * Returns all annotations on the method its superclasses and interfaces
	 */
	public static Annotation[] getAnnotations(Method method) {
		return getAnnotations(method, true);
	}


	private static Annotation[] getAnnotations(Method method, boolean deepScan) {
		if (method == null) {
			return new Annotation[0];
		}
		Annotation[] annotations;
		if (deepScan) {
			annotations = methodDeepScanAnnotationCache.get(method);
			if (annotations == null) {
				annotations = methodDeepScanAnnotationCache.computeIfAbsent(method, methodKey -> {
					Annotation[] newAnnotations = getAnnotations((AnnotatedElement) method);
					Class<?> currentClass = method.getDeclaringClass();
					while (currentClass != null) {
						for (Class<?> classInterface : currentClass.getInterfaces()) {
							newAnnotations = ArrayUtils.addAll(newAnnotations, getAnnotations(MethodUtils.getMatchingMethodInClass(classInterface, method), deepScan));
						}
						currentClass = currentClass.getSuperclass();
						if (currentClass != null) {
							newAnnotations = ArrayUtils.addAll(newAnnotations, getAnnotations(MethodUtils.getMatchingMethodInClass(currentClass, method), deepScan));
						}
					}
					return newAnnotations;
				});
			}
		}
		else {
			annotations = getAnnotations((AnnotatedElement) method);
		}
		return annotations;
	}


	/**
	 * Returns all annotations specified directly on the class
	 */
	public static Annotation[] getDeclaredAnnotations(Class<?> clazz) {
		return getAnnotations(clazz, false);
	}


	/**
	 * Returns all annotations on the class its superclasses and interfaces
	 */
	public static Annotation[] getAnnotations(Class<?> clazz) {
		return getAnnotations(clazz, true);
	}


	private static Annotation[] getAnnotations(Class<?> clazz, boolean deepScan) {
		if (clazz == null) {
			return new Annotation[0];
		}
		Annotation[] annotations;
		if (deepScan) {
			annotations = classDeepScanAnnotationCache.get(clazz);
			if (annotations == null) {
				annotations = classDeepScanAnnotationCache.computeIfAbsent(clazz, clazzKey -> {
					Annotation[] newAnnotations = getAnnotations((AnnotatedElement) clazz);
					Class<?> currentClass = clazz;
					while (currentClass != null) {
						for (Class<?> classInterface : currentClass.getInterfaces()) {
							newAnnotations = ArrayUtils.addAll(newAnnotations, getAnnotations(classInterface, deepScan));
						}
						currentClass = currentClass.getSuperclass();
						if (currentClass != null) {
							newAnnotations = ArrayUtils.addAll(newAnnotations, getAnnotations(currentClass, deepScan));
						}
					}
					return newAnnotations;
				});
			}
		}
		else {
			annotations = getAnnotations((AnnotatedElement) clazz);
		}
		return annotations;
	}


	private static Annotation[] getAnnotations(AnnotatedElement element) {
		return org.springframework.core.annotation.AnnotationUtils.getAnnotations(element);
	}


	/**
	 * Return true if the specified annotation exists directly on the method
	 */
	public static <T extends Annotation> boolean isDeclaredAnnotationPresent(Method method, Class<T> annotationClass) {
		return getDeclaredAnnotation(method, annotationClass) != null;
	}


	/**
	 * Return true if the specified annotation exists on the method its superclasses or interfaces
	 */
	public static <T extends Annotation> boolean isAnnotationPresent(Method method, Class<T> annotationClass) {
		return getAnnotation(method, annotationClass) != null;
	}


	/**
	 * Return true if the specified annotation exists directly on the class
	 */
	public static <T extends Annotation> boolean isDeclaredAnnotationPresent(Class<?> clazz, Class<T> annotationClass) {
		return getDeclaredAnnotation(clazz, annotationClass) != null;
	}


	/**
	 * Return true if the specified annotation exists on the class its superclass or interfaces
	 */
	public static <T extends Annotation> boolean isAnnotationPresent(Class<?> clazz, Class<T> annotationClass) {
		return getAnnotation(clazz, annotationClass) != null;
	}


	/**
	 * Return true if the specified annotation exists directly on the AnnotatedElement
	 */
	public static <T extends Annotation> boolean isAnnotationPresent(AnnotatedElement element, Class<T> annotationClass) {
		return getAnnotation(element, annotationClass) != null;
	}


	/**
	 * Returns the annotation object of the requested annotation type on the given method, or {@code null} if no such annotation exists.
	 */
	public static <T extends Annotation> T getDeclaredAnnotation(Method method, Class<T> annotationClass) {
		//casting to annotated element forces the lookup to be limited to only the specified element.
		return getAnnotation((AnnotatedElement) method, annotationClass);
	}


	/**
	 * Returns the annotation object of the requested annotation type on the given method its superclasses or interfaces, or {@code null} if no such annotation exists.
	 */
	public static <T extends Annotation> T getAnnotation(Method method, Class<T> annotationClass) {
		return org.springframework.core.annotation.AnnotationUtils.findAnnotation(method, annotationClass);
	}


	/**
	 * Returns the annotation object of the requested annotation type on the given class, or {@code null} if no such annotation exists.
	 */
	public static <T extends Annotation> T getDeclaredAnnotation(Class<?> clazz, Class<T> annotationClass) {
		//casting to annotated element forces the lookup to be limited to only the specified element.
		return getAnnotation((AnnotatedElement) clazz, annotationClass);
	}


	/**
	 * Returns the annotation object of the requested annotation type on the given class its superclasses or interfaces, or {@code null} if no such annotation exists.
	 */
	public static <T extends Annotation> T getAnnotation(Class<?> clazz, Class<T> annotationClass) {
		return org.springframework.core.annotation.AnnotationUtils.findAnnotation(clazz, annotationClass);
	}


	/**
	 * Returns the annotation object of the requested annotation type on the given element, or {@code null} if no such annotation exists.
	 */
	public static <T extends Annotation> T getAnnotation(AnnotatedElement element, Class<T> annotationClass) {
		return org.springframework.core.annotation.AnnotationUtils.findAnnotation(element, annotationClass);
	}


	/**
	 * Checks for existence of the specified annotation on the element as well as evaluating the given function of the annotation.
	 * Returns true if the annotation exists and the function returns true; otherwise returns false.
	 * <p>
	 * Several signatures are implemented because methods and classes are traversed differently than a generic annotatedElement allowing
	 * for annotations to be found on superclasses or interfaces.
	 */
	public static <T extends Annotation> boolean isTrue(Method method, Class<T> annotationClass, Predicate<T> annotationPredicate) {
		return isAnnotationPresent(method, annotationClass) && annotationPredicate.test(getAnnotation(method, annotationClass));
	}


	public static <T extends Annotation> boolean isTrue(Class<?> clazz, Class<T> annotationClass, Predicate<T> annotationPredicate) {
		return isAnnotationPresent(clazz, annotationClass) && annotationPredicate.test(getAnnotation(clazz, annotationClass));
	}


	public static <T extends Annotation> boolean isTrue(AnnotatedElement element, Class<T> annotationClass, Predicate<T> annotationPredicate) {
		return isAnnotationPresent(element, annotationClass) && annotationPredicate.test(getAnnotation(element, annotationClass));
	}
}
