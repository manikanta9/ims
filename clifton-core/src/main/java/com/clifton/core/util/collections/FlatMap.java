package com.clifton.core.util.collections;

import java.util.HashMap;


/**
 * The <code>FlatMap</code> is purely used for specialized serialization
 * In a flat map key/values are not nested below field name of the map
 * property on the object, but instead are appended to the json at the
 * same level as the property
 *
 * @author stevenfs
 */
public class FlatMap<K, V> extends HashMap<K, V> {}
