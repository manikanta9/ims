package com.clifton.core.util;


import com.clifton.core.beans.common.AddressObject;


/**
 * The <code>AddressUtils</code> class defines utility methods for working with {@link AddressObject}s
 * Includes formatting address fields for display
 * comparing two addresses for equality
 * copying address field values from one object to another
 *
 * @author manderson
 */
public class AddressUtils {

	private AddressUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////
	///////          Comparison Methods          ///////
	////////////////////////////////////////////////////


	public static boolean isEmpty(AddressObject address) {
		return StringUtils.isEmpty(getAddressLabel(address));
	}


	public static boolean isEqual(AddressObject address1, AddressObject address2, boolean caseSensitive) {
		if (isEmpty(address1) && isEmpty(address2)) {
			return true;
		}
		if (caseSensitive) {
			return getAddressLabel(address1).equals(getAddressLabel(address2));
		}
		return getAddressLabel(address1).equalsIgnoreCase(getAddressLabel(address2));
	}


	////////////////////////////////////////////////////
	///////          Copy/Clear Methods           //////
	////////////////////////////////////////////////////


	public static void copyAddress(AddressObject from, AddressObject to) {
		if (isEmpty(from)) {
			clearAddress(to);
		}
		else if (to != null) {
			to.setAddressLine1(from.getAddressLine1());
			to.setAddressLine2(from.getAddressLine2());
			to.setAddressLine3(from.getAddressLine3());
			to.setCity(from.getCity());
			to.setState(from.getState());
			to.setPostalCode(from.getPostalCode());
			to.setCountry(from.getCountry());
		}
	}


	public static void clearAddress(AddressObject address) {
		if (address != null) {
			address.setAddressLine1(null);
			address.setAddressLine2(null);
			address.setAddressLine3(null);
			address.setCity(null);
			address.setState(null);
			address.setPostalCode(null);
			address.setCountry(null);
		}
	}


	////////////////////////////////////////////////////
	///////          Formatting Methods          ///////
	////////////////////////////////////////////////////


	public static String getAddressLabel(AddressObject address) {
		if (address == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isEmpty(address.getAddressLine1())) {
			sb.append(address.getAddressLine1()).append(" ");
		}
		if (!StringUtils.isEmpty(address.getAddressLine2())) {
			sb.append(address.getAddressLine2()).append(" ");
		}
		if (!StringUtils.isEmpty(address.getAddressLine3())) {
			sb.append(address.getAddressLine3()).append(" ");
		}
		sb.append(getCityStatePostalCode(address));
		if (!StringUtils.isEmpty(address.getCountry())) {
			sb.append(" ").append(address.getCountry());
		}
		return sb.toString().trim();
	}


	public static String getCityState(AddressObject address) {
		if (address == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder();
		if (!StringUtils.isEmpty(address.getCity())) {
			sb.append(address.getCity());
			if (!StringUtils.isEmpty(address.getState())) {
				sb.append(", ");
			}
		}
		if (!StringUtils.isEmpty(address.getState())) {
			sb.append(address.getState());
		}
		return sb.toString().trim();
	}


	public static String getCityStatePostalCode(AddressObject address) {
		if (address == null) {
			return "";
		}
		StringBuilder sb = new StringBuilder(getCityState(address));
		if (!StringUtils.isEmpty(address.getPostalCode())) {
			sb.append(" ").append(address.getPostalCode());
		}
		return sb.toString().trim();
	}
}
