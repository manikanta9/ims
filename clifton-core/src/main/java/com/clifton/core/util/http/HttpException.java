package com.clifton.core.util.http;


/**
 * The <code>HttpException</code> indicates that the HTTP request could not be fulfilled successfully.
 *
 * @author jgommels
 */
public class HttpException extends RuntimeException {

	public HttpException(String msg) {
		super(msg);
	}


	public HttpException(String msg, Throwable cause) {
		super(msg, cause);
	}
}
