package com.clifton.core.util.http.authenticator;

import java.net.URISyntaxException;


public class HttpAuthenticatorFactory {

	public static HttpAuthenticator getAuthenticator(String url) {
		// There's only one at this point so just return it.  THIS WILL EVOLVE
		// Do we need a table in Security that will name applications and link the authenticator to it?
		try {
			return new ImsAuthenticator(url);
		}
		catch (URISyntaxException e) {
			throw new RuntimeException("Failed to parse url [" + url + "].", e);
		}
	}
}
