package com.clifton.core.util.encryption;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.apache.commons.lang.ArrayUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import java.io.ByteArrayOutputStream;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;


/**
 * Utilities for working with Symmetric Encryption using the DES algorithm as implemented in des.exe (compiled from libdes)
 * <p>
 * Used primarily to decrypt Bloomberg Backoffice files.
 * <p>
 * NOTE! DES is insecure and these utils should only be used to DECRYPT incoming data.
 *
 * @author theodorez
 */
public class EncryptionUtilsDES {

	private static final String ENCRYPTION_ALGORITHM = "DES";
	private static final String CIPHER_INSTANCE = "DES/CBC/NoPadding";
	private static final byte[] ODD_PARITY_TABLE = getOddParityTable();


	public static byte[] decrypt(String password, byte[] cipherText) {
		byte[] secretKey = passwordToDesKey(password);
		byte[] iv = new byte[8];

		SecretKey key = new SecretKeySpec(secretKey, ENCRYPTION_ALGORITHM);
		try {
			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);
			cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(iv));
			byte[] bytesDecrypted = cipher.doFinal(cipherText);

			//Calculate the number of bytes of padding that have been added. This value is stored in the last byte of the decrypted plaintext.
			int paddingByteCount = bytesDecrypted[bytesDecrypted.length - 1];

			//Calculate the length of the actual content
			int contentLength = bytesDecrypted.length - 8 + paddingByteCount;

			//Remove the padding value
			return ArrayUtils.subarray(bytesDecrypted, 0, contentLength);
		}
		catch (NoSuchAlgorithmException nsae) {
			throw new RuntimeException("Algorithm " + ENCRYPTION_ALGORITHM + " not valid.", nsae);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred when decrypting with DES. ", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static byte[] passwordToDesKey(String password) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(password), "A password must be specified.");

		byte[] key = new byte[8];

		for (int i = 0; i < password.length(); i++) {
			int c = password.charAt(i);
			if ((i % 16) < 8) {
				key[i % 8] ^= (byte) (c << 1);
			}
			else {
				// reverse bits e.g. 11010010 -> 01001011
				c = (((c << 4) & 0xf0) | ((c >> 4) & 0x0f));
				c = (((c << 2) & 0xcc) | ((c >> 2) & 0x33));
				c = (((c << 1) & 0xaa) | ((c >> 1) & 0x55));
				key[7 - (i % 8)] ^= (byte) c;
			}
		}

		addOddParity(key);

		byte[] target = new byte[8];
		try {
			ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
			outputStream.write(password.getBytes());
			outputStream.write(new byte[8]);
			byte[] temp = outputStream.toByteArray();
			outputStream = new ByteArrayOutputStream();
			for (int i = 0; i < (password.length() + (8 - (password.length() % 8)) % 8); ++i) {
				outputStream.write(temp[i]);
			}
			byte[] passwordBuffer = outputStream.toByteArray();

			Cipher cipher = Cipher.getInstance(CIPHER_INSTANCE);

			//The key is also used as the IV
			IvParameterSpec ivspec = new IvParameterSpec(key);
			cipher.init(Cipher.ENCRYPT_MODE, generateSecretKey(key), ivspec);
			for (int x = 0; x < passwordBuffer.length / 8; ++x) {
				cipher.update(passwordBuffer, 8 * x, 8, target, 0);
			}
		}
		catch (InvalidKeyException ike) {
			throw new RuntimeException("The password: " + password + " is invalid for use as a DES key.", ike);
		}
		catch (Exception e) {
			throw new RuntimeException("Exception occurred when converting the password into a DES key.", e);
		}
		addOddParity(target);

		return target;
	}


	private static SecretKey generateSecretKey(byte[] key) throws InvalidKeyException {
		try {
			SecretKeyFactory factory = SecretKeyFactory.getInstance(ENCRYPTION_ALGORITHM);
			KeySpec keySpec = new DESKeySpec(key);
			return factory.generateSecret(keySpec);
		}
		catch (NoSuchAlgorithmException nsae) {
			throw new RuntimeException("Algorithm " + ENCRYPTION_ALGORITHM + " not valid.", nsae);
		}
		catch (InvalidKeySpecException ikse) {
			throw new RuntimeException("KeySpec is invalid. ", ikse);
		}
	}


	private static void addOddParity(byte[] buffer) {
		for (int i = 0; i < buffer.length; ++i) {
			buffer[i] = ODD_PARITY_TABLE[buffer[i] & 0xFF];
		}
	}


	private static byte[] getOddParityTable() {
		int[] table = {
				1, 1, 2, 2, 4, 4, 7, 7, 8, 8, 11, 11, 13, 13, 14, 14,
				16, 16, 19, 19, 21, 21, 22, 22, 25, 25, 26, 26, 28, 28, 31, 31,
				32, 32, 35, 35, 37, 37, 38, 38, 41, 41, 42, 42, 44, 44, 47, 47,
				49, 49, 50, 50, 52, 52, 55, 55, 56, 56, 59, 59, 61, 61, 62, 62,
				64, 64, 67, 67, 69, 69, 70, 70, 73, 73, 74, 74, 76, 76, 79, 79,
				81, 81, 82, 82, 84, 84, 87, 87, 88, 88, 91, 91, 93, 93, 94, 94,
				97, 97, 98, 98, 100, 100, 103, 103, 104, 104, 107, 107, 109, 109, 110, 110,
				112, 112, 115, 115, 117, 117, 118, 118, 121, 121, 122, 122, 124, 124, 127, 127,
				128, 128, 131, 131, 133, 133, 134, 134, 137, 137, 138, 138, 140, 140, 143, 143,
				145, 145, 146, 146, 148, 148, 151, 151, 152, 152, 155, 155, 157, 157, 158, 158,
				161, 161, 162, 162, 164, 164, 167, 167, 168, 168, 171, 171, 173, 173, 174, 174,
				176, 176, 179, 179, 181, 181, 182, 182, 185, 185, 186, 186, 188, 188, 191, 191,
				193, 193, 194, 194, 196, 196, 199, 199, 200, 200, 203, 203, 205, 205, 206, 206,
				208, 208, 211, 211, 213, 213, 214, 214, 217, 217, 218, 218, 220, 220, 223, 223,
				224, 224, 227, 227, 229, 229, 230, 230, 233, 233, 234, 234, 236, 236, 239, 239,
				241, 241, 242, 242, 244, 244, 247, 247, 248, 248, 251, 251, 253, 253, 254, 254};

		byte[] result = new byte[table.length - 1];
		for (int i = 0; i < table.length - 1; i++) {
			result[i] = (byte) table[i];
		}
		return result;
	}
}
