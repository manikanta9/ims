package com.clifton.core.util.date;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.FieldPosition;
import java.text.NumberFormat;
import java.text.ParsePosition;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;


/**
 * A {@link DateFormat} instance which formats and parses dates using the format string {@code "yyyy-MM-dd HH:mm:ss"}.
 *
 * @author MikeH
 */
public class DateFormatFull extends DateFormat {

	// These fields are used primarily for equals and hashCode evaluation
	private static final Calendar DEFAULT_CALENDAR = new GregorianCalendar();
	private static final NumberFormat DEFAULT_NUMBER_FORMAT = new DecimalFormat();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DateFormatFull() {
		this.calendar = DEFAULT_CALENDAR;
		this.numberFormat = DEFAULT_NUMBER_FORMAT;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public StringBuffer format(Date date, StringBuffer toAppendTo, FieldPosition fieldPosition) {
		return toAppendTo.append(DateUtils.fromDate(date, DateUtils.DATE_FORMAT_FULL));
	}


	@Override
	public Date parse(String source, ParsePosition pos) {
		pos.setIndex(source.length());
		return DateUtils.toDate(source, DateUtils.DATE_FORMAT_FULL);
	}
}
