package com.clifton.core.util.http;


import org.apache.http.HttpEntity;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPatch;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.net.URI;


class SimpleHttpClientImpl implements SimpleHttpClient {

	private final HttpClient client;


	SimpleHttpClientImpl(HttpClient client) {
		this.client = client;
	}


	@Override
	public SimpleHttpResponse execute(HttpUriRequest request) {
		return execute(request, HttpClientContext.create());
	}


	@Override
	public SimpleHttpResponse execute(HttpUriRequest request, HttpClientContext context) {
		return execute(request, context, true);
	}


	@Override
	public SimpleHttpResponse execute(HttpUriRequest request, HttpClientContext context, boolean checkStatus) {
		URI uri = request.getURI();
		try {
			SimpleHttpResponse response = new SimpleHttpResponseImpl(this.client.execute(request, context), context, uri);
			if (checkStatus) {
				checkStatus(response);
			}
			return response;
		}
		catch (Throwable e) {
			throw new HttpException("Problem executing HTTP request: " + uri, e);
		}
	}


	@Override
	public SimpleHttpResponse get(URI uri) {
		return execute(new HttpGet(uri));
	}


	@Override
	public SimpleHttpResponse get(URI uri, HttpClientContext context) {
		return execute(new HttpGet(uri), context);
	}


	@Override
	public SimpleHttpResponse patch(URI uri, HttpEntity httpEntity) {
		return patch(uri, httpEntity, HttpClientContext.create());
	}


	@Override
	public SimpleHttpResponse patch(URI uri, HttpEntity httpEntity, HttpClientContext context) {
		HttpPatch patch = new HttpPatch(uri);
		patch.setEntity(httpEntity);
		return execute(patch, context);
	}


	@Override
	public SimpleHttpResponse post(URI uri, HttpEntity httpEntity) {
		return post(uri, httpEntity, HttpClientContext.create());
	}


	@Override
	public SimpleHttpResponse post(URI uri, HttpEntity httpEntity, HttpClientContext context) {
		HttpPost post = new HttpPost(uri);
		post.setEntity(httpEntity);
		return execute(post, context);
	}


	@Override
	public SimpleHttpResponse put(URI uri, HttpEntity httpEntity) {
		return put(uri, httpEntity, HttpClientContext.create());
	}


	@Override
	public SimpleHttpResponse put(URI uri, HttpEntity httpEntity, HttpClientContext context) {
		HttpPut put = new HttpPut(uri);
		put.setEntity(httpEntity);
		return execute(put, context);
	}


	@Override
	public SimpleHttpResponse delete(URI uri) {
		return execute(new HttpDelete(uri));
	}


	@Override
	public SimpleHttpResponse delete(URI uri, HttpClientContext context) {
		return execute(new HttpDelete(uri), context);
	}


	private void checkStatus(SimpleHttpResponse response) {
		int status = response.getStatusCode();

		if (status >= 300 && status < 400) {
			HttpUtils.closeResponse(response);
			throw new RedirectStatusException(response);
		}
		else if (status >= 400) {
			String content;
			try {
				content = EntityUtils.toString(response.getHttpResponse().getEntity());
			}
			catch (Exception e) {
				content = "[Server reply content could not be retrieved]";
			}
			finally {
				HttpUtils.closeResponse(response);
			}
			throw new BadStatusException(response.getURI(), status, content);
		}
	}


	@Override
	public void close() {
		try {
			if (this.client instanceof CloseableHttpClient) {
				((CloseableHttpClient) this.client).close();
			}
		}
		catch (IOException e) {
			//Ignore
		}
	}
}
