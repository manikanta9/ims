package com.clifton.core.util.matching;


import java.util.List;


/**
 * The <code>PermutationScorer</code> returns a score for permutation list.
 *
 * @param <T>
 * @author mwacker
 */
public interface PermutationScorer<T> {

	public int score(List<T> permutation);
}
