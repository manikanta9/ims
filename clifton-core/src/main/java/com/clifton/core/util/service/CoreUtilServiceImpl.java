package com.clifton.core.util.service;

import com.clifton.core.util.date.DateUtils;
import org.springframework.stereotype.Service;

import java.util.Date;


@Service
public class CoreUtilServiceImpl implements CoreUtilService {

	@Override
	public Integer getCoreDateUtilDaysDifference(Date dateOne, Date dateTwo) {
		return DateUtils.getDaysDifference(dateOne, dateTwo);
	}
}
