package com.clifton.core.cache;


/**
 * The <code>CacheEventTypes</code> defines cache events type.
 * <p/>
 * NOTE: No PUT or GET events to avoid adding extra processing.
 *
 * @author mwacker
 */
public enum CacheEventTypes {
	BEFORE_REMOVE, BEFORE_CLEAR, AFTER_REMOVE, AFTER_CLEAR
}
