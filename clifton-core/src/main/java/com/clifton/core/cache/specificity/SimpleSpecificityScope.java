package com.clifton.core.cache.specificity;


/**
 * The <code>SimpleSpecificityScope</code> is, as its name implies, a simple implementation of {@link SpecificityScope}.
 * This implementation simply uses a scope ID of type <code>int</code> and this is used to uniquely identify the
 * scope.
 *
 * @author jgommels
 */
public class SimpleSpecificityScope implements SpecificityScope {

	/**
	 * The ID that identifies this scope.
	 */
	private final int scopeId;
	private final int maxResults;
	private final boolean limitToMaximumSpecificity;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @param scopeId the unique identifier for the scope
	 */
	public SimpleSpecificityScope(int scopeId) {
		this(scopeId, 1);
	}


	/**
	 * @param scopeId    the unique identifier for the scope
	 * @param maxResults the maximum number of results to be returned for which match this scope
	 */
	public SimpleSpecificityScope(int scopeId, int maxResults) {
		this.scopeId = scopeId;
		this.maxResults = maxResults;
		this.limitToMaximumSpecificity = false;
	}


	/**
	 * Constructs an entity with a given {@link #isLimitToMaximumSpecificity() limitToMaximumSpecificity} value.
	 * <p>
	 * If <code>limitToMaximumSpecificity</code> is <code>true</code>, then {@link #maxResults} will be set to {@link Integer#MAX_VALUE}. Otherwise, {@link #maxResults} will be set
	 * to <code>1</code>.
	 *
	 * @param scopeId                   the unique identifier for the scope
	 * @param limitToMaximumSpecificity <code>true</code> to retrieve all results at the most specific level for any given source, or <code>false</code> to return only a single
	 *                                  result
	 */
	public SimpleSpecificityScope(int scopeId, boolean limitToMaximumSpecificity) {
		this.scopeId = scopeId;
		this.maxResults = limitToMaximumSpecificity ? Integer.MAX_VALUE : 1;
		this.limitToMaximumSpecificity = limitToMaximumSpecificity;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "SimpleSpecificityScope [scopeId=" + this.scopeId + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + this.maxResults;
		result = prime * result + this.scopeId;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		SimpleSpecificityScope other = (SimpleSpecificityScope) obj;
		if (this.maxResults != other.maxResults) {
			return false;
		}
		if (this.scopeId != other.scopeId) {
			return false;
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getScopeId() {
		return this.scopeId;
	}


	@Override
	public int getMaxResults() {
		return this.maxResults;
	}


	@Override
	public boolean isLimitToMaximumSpecificity() {
		return this.limitToMaximumSpecificity;
	}
}
