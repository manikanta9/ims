package com.clifton.core.cache.specificity;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.ObserverableDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;


/**
 * The <code>SpecificityCacheUpdater</code> is an abstract class that represents an update strategy for an implementation of {@link AbstractSpecificityCache}. For example, {@link
 * ClearSpecificityCacheUpdater} will clear the cache any time a registered DTO changes. {@link DynamicSpecificityCacheUpdater} dynamically updates the cache when a registered DTO
 * changes.
 *
 * @param <T> the {@link SpecificityTargetHolder} type
 * @author jgommels
 */
public abstract class SpecificityCacheUpdater<T extends SpecificityTargetHolder> {

	private volatile boolean initialized = false;

	protected final Set<Class<? extends IdentityObject>> registeredTypes;

	private AbstractSpecificityCache<?, ?, T> cache;

	/**
	 * Maps a class to its associated observer.
	 */
	private final Map<Class<? extends IdentityObject>, BaseDaoEventObserver<?>> observerMap = new HashMap<>();

	private ContextHandler contextHandler;

	private DaoLocator daoLocator;


	/**
	 * @param registeredTypes the DTO types to register observers on for the cache updater react to events for those DTO types
	 */
	@SafeVarargs
	protected SpecificityCacheUpdater(Class<? extends IdentityObject>... registeredTypes) {
		this.registeredTypes = CollectionUtils.newUnmodifiableSet(registeredTypes);
	}


	protected abstract <U extends IdentityObject> BaseDaoEventObserver<U> createObserverForRegisteredType();


	protected boolean isTypeRegistered(Class<? extends IdentityObject> clazz) {
		return this.observerMap.containsKey(clazz);
	}


	public abstract <V extends IdentityObject> void registerCacheUpdateEvent(V sourceObject, T targetHolder);


	protected AbstractSpecificityCache<?, ?, T> getCache() {
		return this.cache;
	}


	public void initialize(AbstractSpecificityCache<?, ?, T> cache, ContextHandler contextHandler, DaoLocator daoLocator) {
		if (!this.initialized) {
			synchronized (this) {
				this.cache = cache;
				this.contextHandler = contextHandler;
				this.daoLocator = daoLocator;

				for (Class<? extends IdentityObject> clazz : this.registeredTypes) {
					configureObserver(clazz);
				}
				this.initialized = true;
			}
		}
	}


	protected <U extends IdentityObject> void configureObserver(Class<U> clazz) {
		@SuppressWarnings("unchecked")
		ObserverableDAO<U> dao = (ObserverableDAO<U>) this.daoLocator.locate(clazz);
		if (dao != null) {
			BaseDaoEventObserver<U> obs = createObserverForRegisteredType();
			if (obs != null) {
				obs.setContextHandler(this.contextHandler);

				dao.registerEventObserver(DaoEventTypes.INSERT, obs);
				dao.registerEventObserver(DaoEventTypes.UPDATE, obs);
				dao.registerEventObserver(DaoEventTypes.DELETE, obs);

				this.observerMap.put(clazz, obs);
			}
		}
		else {
			throw new RuntimeException("DAO for " + clazz.getName() + " could not be found.");
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Set<Class<? extends IdentityObject>> getRegisteredTypes() {
		return this.registeredTypes;
	}
}
