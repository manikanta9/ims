package com.clifton.core.cache.hibernate;


import com.clifton.core.cache.CacheTypes;
import com.clifton.core.util.converter.Converter;


/**
 * The <code>CacheTypeToHibernateTypeConverter</code> class converts CacheTypes to corresponding Hibernate usage values.
 * <cache usage="read-write|nonstrict-read-write|read-only" />
 *
 * @author vgomelsky
 */
public class CacheTypeToHibernateTypeConverter implements Converter<CacheTypes, String> {

	@Override
	public String convert(CacheTypes from) {
		if (from == CacheTypes.READ_WRITE) {
			return "read-write";
		}
		if (from == CacheTypes.READ_ONLY) {
			return "read-only";
		}
		if (from == CacheTypes.READ_WRITE_NONSTRICT) {
			return "nonstrict-read-write";
		}
		throw new IllegalArgumentException("Invalid cache type: " + from);
	}
}
