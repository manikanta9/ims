package com.clifton.core.cache.specificity.key;


import com.clifton.core.cache.specificity.SpecificityCache;

import java.util.List;


/**
 * This class is used to generate <i>specificity</i> keys for sources and targets. Each specificity key represents an
 * exact combination of hierarchy attributes. For example,
 * <p>
 * Target specificity keys are an exact representation of the least specific hierarchy data
 * <p>
 * The <code>AbstractSpecificityKeyGenerator</code> is a class used with implementations of {@link SpecificityCache} for generating
 * "specificity" keys for sources and targets. A specificity key is represented as a <code>String</code> and contains the following
 * information to define the "specificity" of a given source or target:
 * <ul>
 * <li>The specificity level of each hierarchy (0 is most specific, 1 is second most specific, etc.)</li>
 * <li>The value associated with each hierarchy level</li>
 * </ul>
 *
 * @author jgommels
 */
public abstract class AbstractSpecificityKeyGenerator {

	/**
	 * Generates and returns a specificity key for a source.
	 *
	 * @param specificityValue the specificity value to retrieve a key for. 0 is the most specific, 1 is the second most specific, etc.
	 * @param keySource        the key source to generate the key for
	 */
	public abstract String generateKeyForSource(int specificityValue, SpecificityKeySource keySource);


	/**
	 * Generates a specificity key for a target. This is done by iterating through all the values for each hierarchy
	 * and finding the first non-null value. If no non-null value is found, then the last index of that hierarchy
	 * is used in the key.
	 *
	 * @param keySource the key source that holds information about the target necessary for generating the key
	 */
	public String generateKeyForTarget(SpecificityKeySource keySource) {
		List<Object[]> hierarchies = keySource.getHierarchies();
		byte[] indexes = new byte[hierarchies.size()];

		/*
		 * Iterate through each hierarchy until the first non-null value is found, and use that for the key value in
		 * that hierarchy. If no non-null value can be found, then the last index is used.
		 */
		for (byte i = 0; i < hierarchies.size(); i++) {
			Object[] hierarchy = hierarchies.get(i);
			for (byte j = 0; j < hierarchy.length; j++) {
				if (hierarchy[j] != null || j == hierarchy.length - 1) {
					indexes[i] = j;
					break;
				}
			}
		}

		return generateKey(indexes, hierarchies, keySource.getValuesToAppend());
	}


	protected void throwExceptionIfSpecificityNotValid(int specificityValue, SpecificityKeySource keySource) {
		if (specificityValue < 0) {
			throw new IllegalArgumentException("specificity cannot be negative.");
		}

		int numPossibleKeys = keySource.getNumPossibleKeys();
		if (specificityValue > numPossibleKeys) {
			throw new IllegalArgumentException("The specificity " + specificityValue + " is larger than the max number of possible keys for this SpecificityKeySource, which is " + numPossibleKeys);
		}
	}


	protected String generateKey(byte[] indexes, List<Object[]> hierarchies, Object[] valuesToAppend) {
		if (indexes.length != hierarchies.size()) {
			throw new IllegalArgumentException("indexes.length = " + indexes.length + " does not equal hierarchies.size() = " + hierarchies.size());
		}

		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < indexes.length; i++) {
			int index = indexes[i];
			Object value = hierarchies.get(i)[index];
			sb.append(index).append('-').append(value).append(';');
		}

		if (valuesToAppend != null) {
			for (Object o : valuesToAppend) {
				sb.append(o).append(',');
			}
		}

		// return the string with the trailing ; or , removed
		return sb.toString().substring(0, sb.length() - 1);
	}
}
