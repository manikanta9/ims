package com.clifton.core.cache;


/**
 * The <code>CustomCache</code> interface defines cache methods used for caching.
 *
 * @param <K> the cache key type
 * @param <V> the cache value type
 * @author manderson
 */
public interface CustomCache<K, V> {

	/**
	 * Returns the name of the cache. Cache names need to be globally unique.
	 * We usually use fully qualified name of implementing cache class or cached objects.
	 *
	 * @return the cache name
	 */
	public String getCacheName();


	/**
	 * Returns the {@link CacheHandler} object that exposes methods for working with caches.
	 *
	 * @return the cache handler
	 */
	public CacheHandler<K, V> getCacheHandler();
}
