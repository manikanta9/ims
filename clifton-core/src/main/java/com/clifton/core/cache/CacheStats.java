package com.clifton.core.cache;


/**
 * The <code>CacheStats</code> class represents statistics about a cache.
 *
 * @author vgomelsky
 */
public class CacheStats {

	private final String cacheName;

	private final long size;
	private final long inMemorySize;
	private final long cacheHitCount;
	private final long cacheMissCount;
	private final boolean statisticsEnabled;

	private long timeToLive;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CacheStats() {
		// Empty Constructor - Needed for binding (i.e. Portal Admin getting cache list from Portal)
		this(null, 0, 0, 0, false);
	}


	public CacheStats(String cacheName, long cacheHitCount, long cacheMissCount, long timeToLive, boolean statisticsEnabled) {
		this(cacheName, 0, 0, cacheHitCount, cacheMissCount, timeToLive, statisticsEnabled);
	}


	public CacheStats(String cacheName, long size, long inMemorySize, long cacheHitCount, long cacheMissCount, long timeToLive, boolean statisticsEnabled) {
		this.cacheName = cacheName;
		this.size = size;
		this.inMemorySize = inMemorySize;
		this.cacheHitCount = cacheHitCount;
		this.cacheMissCount = cacheMissCount;
		this.statisticsEnabled = statisticsEnabled;
		this.timeToLive = timeToLive;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCacheName() {
		return this.cacheName;
	}


	public long getSize() {
		return this.size;
	}


	public long getInMemorySize() {
		return this.inMemorySize;
	}


	public long getCacheHitCount() {
		return this.cacheHitCount;
	}


	public long getCacheMissCount() {
		return this.cacheMissCount;
	}


	public boolean isStatisticsEnabled() {
		return this.statisticsEnabled;
	}


	public long getTimeToLive() {
		return this.timeToLive;
	}


	public void setTimeToLive(long timeToLive) {
		this.timeToLive = timeToLive;
	}
}
