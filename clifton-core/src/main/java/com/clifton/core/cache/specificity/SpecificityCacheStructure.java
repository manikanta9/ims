package com.clifton.core.cache.specificity;


import com.clifton.core.util.collections.ConcurrentMultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.concurrent.Lockable;

import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;


class SpecificityCacheStructure<T extends SpecificityTargetHolder> implements Lockable {

	private final SpecificityCacheTypes type;

	/**
	 * Maps a SpecificityScope to a Map that maps a key (String) to a result or list of results.
	 */
	private final Map<SpecificityScope, MultiValueMap<String, T>> cache = new HashMap<>();

	/**
	 * A reentrant read-write lock that allows concurrent reads when there are no writes.
	 * <p>
	 * TODO When we move to Java 8, this should probably be changed to a StampedLock.
	 */
	private final ReadWriteLock lock = new ReentrantReadWriteLock();


	public SpecificityCacheStructure(final SpecificityCacheTypes type) {
		this.type = type;
	}


	@Override
	public void lock() {
		this.lock.writeLock().lock();
	}


	@Override
	public void unlock() {
		this.lock.writeLock().unlock();
	}


	private MultiValueMap<String, T> getKeyMap(SpecificityScope scope) {
		MultiValueMap<String, T> keyMap;

		this.lock.readLock().lock();
		try {
			keyMap = this.cache.get(scope);
		}
		finally {
			this.lock.readLock().unlock();
		}

		return keyMap;
	}


	/**
	 * Returns a collection of results for the scope/key pair.
	 */
	protected Collection<T> getResults(SpecificityScope scope, String key) {
		MultiValueMap<String, T> keyMap = getKeyMap(scope);

		List<T> results = null;
		if (keyMap != null) {
			results = keyMap.get(key);
		}

		return results;
	}


	protected Set<SpecificityScope> getScopes() {
		this.lock.readLock().lock();
		try {
			return new HashSet<>(this.cache.keySet());
		}
		finally {
			this.lock.readLock().unlock();
		}
	}


	@SuppressWarnings("unchecked")
	protected void addTargetHolder(T targetHolder) {
		String key = targetHolder.getKey();
		SpecificityScope scope = targetHolder.getScope();

		this.lock.writeLock().lock();

		try {
			// Get the key map associated with this scope. If there isn't a match, then this is a new scope.
			MultiValueMap<String, T> keyMap = this.cache.computeIfAbsent(scope, k -> new ConcurrentMultiValueHashMap<>(this.type.getCollectionFactory()));

			keyMap.put(key, targetHolder);
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}


	protected void removeTargetHolder(T targetHolder) {
		SpecificityScope scope = targetHolder.getScope();
		String key = targetHolder.getKey();

		this.lock.writeLock().lock();

		try {

			//Get the key map associated with this scope
			MultiValueMap<String, T> keyMap = this.cache.get(scope);

			if (keyMap != null) {
				keyMap.remove(key, targetHolder);
			}
		}
		finally {
			this.lock.writeLock().unlock();
		}
	}
}
