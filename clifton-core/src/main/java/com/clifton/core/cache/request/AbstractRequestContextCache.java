package com.clifton.core.cache.request;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;

import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * The {@link AbstractRequestContextCache} is an {@link CustomCache} implementation for short-lived request-context-based caches. This type may be inherited by Spring-managed beans
 * to automatically provide access to the {@link RequestContextCacheHandler} for caching.
 * <p>
 * This type should generally <i>not</i> be used for caching persisted entities directly. Instead, a unique identifier to the persisted entity should be cached if needed. See
 * {@link RequestContextCacheHandler} for more details.
 *
 * @author mikeh
 * @see RequestContextCacheHandler
 */
public abstract class AbstractRequestContextCache<K, V> implements CustomCache<K, V> {

	/**
	 * The backing {@link CacheHandler} used to manage all cache operations.
	 */
	public CacheHandler<K, V> requestContextCacheHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return getClass().getName();
	}


	@Override
	public CacheHandler<K, V> getCacheHandler() {
		return getRequestContextCacheHandler();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public V get(K key) {
		return getCacheHandler().get(getCacheName(), key);
	}


	public Map<K, V> getAll() {
		return getCacheHandler().getKeys(getCacheName()).stream()
				.collect(Collectors.toMap(Function.identity(), key -> getCacheHandler().get(getCacheName(), key)));
	}


	public boolean contains(K key) {
		return getCacheHandler().getKeys(getCacheName()).contains(key);
	}


	public void put(K key, V value) {
		getCacheHandler().put(getCacheName(), key, value);
	}


	public void remove(K key) {
		getCacheHandler().remove(getCacheName(), key);
	}


	public void clear() {
		getCacheHandler().clear(getCacheName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CacheHandler<K, V> getRequestContextCacheHandler() {
		return this.requestContextCacheHandler;
	}


	public void setRequestContextCacheHandler(CacheHandler<K, V> requestContextCacheHandler) {
		this.requestContextCacheHandler = requestContextCacheHandler;
	}
}
