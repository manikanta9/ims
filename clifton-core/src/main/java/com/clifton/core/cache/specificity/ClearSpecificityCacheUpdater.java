package com.clifton.core.cache.specificity;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;


/**
 * The <code>ClearSpecificityCacheUpdater</code> is an update strategy implementation that will clear a {@link AbstractSpecificityCache} when
 * an instance of a registered type is created, updated, or deleted. This update strategy is not recommended for most cases, as {@link DynamicSpecificityCacheUpdater}
 * is more efficient, however in some cases a dynamic cache update strategy that is thread-safe would be far too complex to develop and maintain.
 *
 * @param <T> the {@link SpecificityTargetHolder} type
 * @author jgommels
 */
public class ClearSpecificityCacheUpdater<T extends SpecificityTargetHolder> extends SpecificityCacheUpdater<T> {

	/**
	 * @param registeredTypes the DTO types to register observers on. When a change occurs to any of these types (INSERT, UPDATE, DELETE), the cache will be cleared.
	 */
	@SafeVarargs
	public ClearSpecificityCacheUpdater(Class<? extends IdentityObject>... registeredTypes) {
		super(registeredTypes);
	}


	@Override
	protected <U extends IdentityObject> ClearCacheObserver<U> createObserverForRegisteredType() {
		return new ClearCacheObserver<>();
	}


	@Override
	public <V extends IdentityObject> void registerCacheUpdateEvent(V sourceObject, T targetHolder) {
		// Nothing needed here because this method doesn't serve a purpose when the cache is cleared on all updates
	}


	class ClearCacheObserver<V extends IdentityObject> extends BaseDaoEventObserver<V> {

		@Override
		@SuppressWarnings("unused")
		public void afterMethodCallImpl(ReadOnlyDAO<V> dao, DaoEventTypes event, V bean, Throwable e) {
			if (e == null) {
				getCache().clearCache();
			}
		}
	}
}
