package com.clifton.core.cache;


import java.util.Collection;
import java.util.List;


/**
 * The <code>CacheHandler</code> interface defines methods for working with cached data.
 *
 * @param <K> cache element key type
 * @param <V> cache element value type
 * @author vgomelsky
 */
public interface CacheHandler<K, V> {

	/**
	 * Returns an element stored under the specified key from the specified cache.
	 */
	public V get(String cacheName, K key);


	/**
	 * Return list of all keys in the given cache
	 */
	public Collection<K> getKeys(String cacheName);


	/**
	 * Stores the specified value in the specified cache under the specified key.
	 */
	public void put(String cacheName, K key, V value);


	/**
	 * Removes the element with the specified key from the specified cache.
	 */
	public void remove(String cacheName, K key);


	/**
	 * Removes all elements from the specified cache.
	 */
	public void clear(String cacheName);


	/**
	 * Removes all caches.
	 */
	public void clearAll();


	/**
	 * Gets the list of cache names from the cache handler.
	 */
	public String[] getCacheNames();


	/**
	 * Enables/disables statistics collection for the specified cache.
	 * If the specified cacheName == null, then does this for ALL caches.
	 *
	 * @return returns the number of caches affected
	 */
	public int setStatisticsEnabled(String cacheName, boolean enableStatistics);


	/**
	 * Returns a List of CacheStats for all caches.
	 */
	public List<CacheStats> getCacheStats(boolean calculateSize);


	/**
	 * Returns CacheStats for the cache with the specified name.
	 */
	public CacheStats getCacheStats(String cacheName);


	/**
	 * Register a cache event listener.
	 */
	public void registerEventListener(String cacheName, CacheEventTypes eventType, CacheEventListener<K, V> listener);
}
