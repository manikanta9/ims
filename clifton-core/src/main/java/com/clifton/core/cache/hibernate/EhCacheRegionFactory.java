package com.clifton.core.cache.hibernate;


import com.clifton.core.util.AssertUtils;
import net.sf.ehcache.CacheManager;
import org.hibernate.boot.spi.SessionFactoryOptions;
import org.hibernate.cache.CacheException;
import org.hibernate.cache.ehcache.internal.SingletonEhcacheRegionFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;


/**
 * The <code>EhCacheRegionFactory</code> class enables to reuse the same Spring managed CacheManager instance
 * for Hibernate as well as our own caching.
 * <p>
 * NOTE: couldn't find any example with latest Spring/Hibernate API online.
 * Unfortunately Hibernate doesn't allow injection of the session (initialized via property file) so we need
 * a static reference to the BeanFactory to get the CacheManager.
 * <p>
 * THIS CLASS NEEDS TO BE INITIALIZED BEFORE HIBERNATE!!!
 *
 * @author vgomelsky
 */
public class EhCacheRegionFactory extends SingletonEhcacheRegionFactory implements ApplicationContextAware, DisposableBean {

	private static final AtomicInteger REFERENCE_COUNT = new AtomicInteger();

	/**
	 * Used to retrieve context managed CacheManager. To avoid setting this value
	 * each time a new instance of this class is initialized, an {@link AtomicReference}
	 * is used to control the value of the application context.
	 */
	private final AtomicReference<ApplicationContext> applicationContext = new AtomicReference<>(null);


	@Override
	public void setApplicationContext(ApplicationContext appContext) throws BeansException {
		ApplicationContext oldContext = this.applicationContext.getAndSet(appContext);
		if (oldContext != null && !oldContext.equals(appContext)) {
			// Clean up the old cache manager upon new application context
			releaseFromUse();
		}
	}


	@Override
	protected CacheManager resolveCacheManager(SessionFactoryOptions settings, Map properties) {
		try {
			String factoryBeanName = (String) properties.get("hibernate.cache.cache_manager_factory_bean_name");
			if (factoryBeanName == null) {
				// default the name if it's not set
				factoryBeanName = "cacheManagerFactory";
			}
			AssertUtils.assertNotNull(this.applicationContext.get(), "Cannot access application context before it was initialized");
			CacheManager manager = (CacheManager) this.applicationContext.get().getBean(factoryBeanName);
			AssertUtils.assertNotNull(manager, "Cannot find CacheManager in context under name: " + factoryBeanName);
			REFERENCE_COUNT.incrementAndGet();
			return manager;
		}
		catch (Exception e) {
			REFERENCE_COUNT.decrementAndGet();
			throw new CacheException(e);
		}
	}


	@Override
	protected void releaseFromUse() {
		if (getCacheManager() != null) {
			try {
				if (REFERENCE_COUNT.decrementAndGet() == 0) {
					getCacheManager().shutdown();
				}
			}
			catch (net.sf.ehcache.CacheException e) {
				throw new CacheException(e);
			}
		}
	}


	@Override
	public void destroy() {
		this.applicationContext.set(null);
	}
}
