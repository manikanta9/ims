package com.clifton.core.cache.specificity;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Designed to be used when there are multiple updating strategies for different registered classes in a {@link SpecificityCache}.
 * It is designed to use a map that pairs each type of registered class with the appropriate updater to use.
 * <p>
 * This class initializes all updaters that are provided in the map, and delegates update event registration to the updaters in the map.
 * By default, this type does not create any sort of observer for the registered classes, but delegates all observer functionality to updaters for each registered type.
 *
 * @author MitchellF
 */
public class CompositeSpecificityCacheUpdater<T extends SpecificityTargetHolder> extends SpecificityCacheUpdater<T> {

	private final Map<Class<? extends IdentityObject>, SpecificityCacheUpdater<T>> updaterMap = new HashMap<>();


	protected CompositeSpecificityCacheUpdater(List<SpecificityCacheUpdater<T>> updaters) {
		super(CollectionUtils.toArray(CollectionUtils.getConvertedFlattened(updaters, SpecificityCacheUpdater::getRegisteredTypes), Class.class));

		updaters.forEach(u ->
				u.getRegisteredTypes().forEach(t -> {
					if (this.updaterMap.containsKey(t)) {
						throw new UnsupportedOperationException("Cannot add multiple updaters for same class.  Check registeredTypes of CompositeSpecificityCacheUpdater constructor argument for duplicates");
					}
					this.updaterMap.put(t, u);
				})
		);
	}


	@Override
	public void initialize(AbstractSpecificityCache<?, ?, T> cache, ContextHandler contextHandler, DaoLocator daoLocator) {
		super.initialize(cache, contextHandler, daoLocator);
		// initialize all updaters
		for (SpecificityCacheUpdater<T> updater : this.updaterMap.values()) {
			updater.initialize(cache, contextHandler, daoLocator);
		}
	}


	// This method returns null because observer creation is delegated to each updater type.  Creating observers here would cause there to be two duplicate observers registered for each type,
	// one registered in this method, and another registered in the method of the same name in each individual updater class
	@Override
	protected BaseDaoEventObserver<? extends IdentityObject> createObserverForRegisteredType() {
		return null;
	}


	@Override
	protected boolean isTypeRegistered(Class<? extends IdentityObject> clazz) {
		SpecificityCacheUpdater<T> updater = this.updaterMap.get(clazz);
		return updater != null && updater.isTypeRegistered(clazz);
	}


	@Override
	public <V extends IdentityObject> void registerCacheUpdateEvent(V sourceObject, T targetHolder) {
		SpecificityCacheUpdater<T> updater = this.updaterMap.get(sourceObject.getClass());
		if (updater != null) {
			updater.registerCacheUpdateEvent(sourceObject, targetHolder);
		}
		else {
			LogUtils.warn(getClass(), "Tried to observed cache updates for non-registered type: " + sourceObject.getClass());
		}
	}
}
