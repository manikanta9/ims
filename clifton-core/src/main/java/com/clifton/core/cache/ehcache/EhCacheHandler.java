package com.clifton.core.cache.ehcache;


import com.clifton.core.cache.CacheEventListener;
import com.clifton.core.cache.CacheEventTypes;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CacheStats;
import com.clifton.core.util.StringUtils;
import net.sf.ehcache.Cache;
import net.sf.ehcache.CacheManager;
import net.sf.ehcache.Element;
import net.sf.ehcache.statistics.StatisticsGateway;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>EhCacheHandler</code> class provides EhCache based implementation of the {@link CacheHandler} interface.
 *
 * @param <K> cache element key type
 * @param <V> cache element value type
 * @author vgomelsky
 */
public class EhCacheHandler<K, V> implements CacheHandler<K, V> {

	private final Map<String, Map<CacheEventTypes, CacheEventListener<K, V>>> eventListenerMap = new ConcurrentHashMap<>();
	private CacheManager cacheManager;


	@Override
	@SuppressWarnings("unchecked")
	public V get(String cacheName, K key) {
		Element result = getCache(cacheName).get(key);
		return (result == null) ? null : (V) result.getObjectValue();
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<K> getKeys(String cacheName) {
		return getCache(cacheName).getKeys();
	}


	@Override
	public void put(String cacheName, K key, V value) {
		getCache(cacheName).put(new Element(key, value));
	}


	@Override
	public void remove(String cacheName, K key) {
		V value = get(cacheName, key);
		executeEvent(cacheName, CacheEventTypes.BEFORE_REMOVE, key, value);
		getCache(cacheName).remove(key);
		executeEvent(cacheName, CacheEventTypes.AFTER_REMOVE, key, value);
	}


	@Override
	public void clear(String cacheName) {
		executeEvent(cacheName, CacheEventTypes.BEFORE_CLEAR, null, null);
		getCache(cacheName).removeAll();
		executeEvent(cacheName, CacheEventTypes.AFTER_CLEAR, null, null);
	}


	@Override
	public void clearAll() {
		for (String cacheName : getCacheNames()) {
			clear(cacheName);
		}
	}


	@Override
	public String[] getCacheNames() {
		return getCacheManager().getCacheNames();
	}


	@Override
	public int setStatisticsEnabled(String cacheName, boolean enableStatistics) {
		int count = 0;
		String[] cacheNames = StringUtils.isEmpty(cacheName) ? getCacheManager().getCacheNames() : new String[]{cacheName};

		for (String name : cacheNames) {
			Cache cache = getCache(name);
			if (cache != null) {
				cache.getCacheConfiguration().statistics(enableStatistics);
				count++;
			}
		}
		return count;
	}


	@Override
	public List<CacheStats> getCacheStats(boolean calculateSize) {
		List<CacheStats> result = new ArrayList<>();
		for (String cacheName : getCacheManager().getCacheNames()) {
			result.add(getCacheStats(cacheName, calculateSize));
		}
		return result;
	}


	@Override
	public CacheStats getCacheStats(String cacheName) {
		// Getting cache states for one - defaults to calculate size
		return getCacheStats(cacheName, true);
	}


	/**
	 * Can optionally not calculateSizes - useful when trying to load all caches in order to select one to clear.  This can be time consuming.
	 *
	 * @param cacheName
	 * @param calculateSize
	 */
	private CacheStats getCacheStats(String cacheName, boolean calculateSize) {
		// NOTE: statistics must be enabled: statistics="true" in ehcache.xml
		Cache cache = getCache(cacheName);
		// To get the count/# of keys don't use getSize() - to get a very fast result, use #getKeysNoDuplicateCheck()}.size()
		if (cache.getCacheConfiguration().getStatistics()) {
			StatisticsGateway stats = cache.getStatistics();
			return new CacheStats(cacheName, stats.getSize(), stats.getLocalHeapSize(), stats.cacheHitCount(), stats.cacheMissCount(), cache.getCacheConfiguration().getTimeToLiveSeconds(), true);
		}
		// limited stats
		return new CacheStats(cacheName, cache.getSize(), calculateSize ? cache.calculateInMemorySize() : 0, -1, -1, cache.getCacheConfiguration().getTimeToLiveSeconds(), false);
	}


	@Override
	public void registerEventListener(String cacheName, CacheEventTypes eventType, CacheEventListener<K, V> listener) {
		Map<CacheEventTypes, CacheEventListener<K, V>> listenerMap;
		if (this.eventListenerMap.containsKey(cacheName)) {
			listenerMap = this.eventListenerMap.get(cacheName);
		}
		else {
			listenerMap = new ConcurrentHashMap<>();
			this.eventListenerMap.put(cacheName, listenerMap);
		}
		listenerMap.put(eventType, listener);
	}


	private void executeEvent(String cacheName, CacheEventTypes eventType, K key, V value) {
		Map<CacheEventTypes, CacheEventListener<K, V>> listenerMap = this.eventListenerMap.get(cacheName);
		if (listenerMap != null) {
			CacheEventListener<K, V> listener = listenerMap.get(eventType);
			if (listener != null) {
				listener.execute(cacheName, key, value);
			}
		}
	}


	/**
	 * Returns the {@link Cache} object for the specified name.
	 *
	 * @param cacheName
	 */
	private Cache getCache(String cacheName) {
		Cache result = getCacheManager().getCache(cacheName);
		if (result == null) {
			// create a new cache for the specified name (first call)
			synchronized (getCacheManager()) {
				result = getCacheManager().getCache(cacheName);
				if (result == null) {
					getCacheManager().addCache(cacheName);
					result = getCacheManager().getCache(cacheName);
				}
			}
		}
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @return the cacheManager
	 */
	public CacheManager getCacheManager() {
		return this.cacheManager;
	}


	/**
	 * @param cacheManager the cacheManager to set
	 */
	public void setCacheManager(CacheManager cacheManager) {
		this.cacheManager = cacheManager;
	}
}
