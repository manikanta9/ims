package com.clifton.core.cache;


/**
 * The <code>CacheTypes</code> enumeration defines different types of caching.
 * See Hibernate and EHCache for more details.
 *
 * @author vgomelsky
 */
public enum CacheTypes {

	NONE, READ_ONLY, READ_WRITE, READ_WRITE_NONSTRICT
}
