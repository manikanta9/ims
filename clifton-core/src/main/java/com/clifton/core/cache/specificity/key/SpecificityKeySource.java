package com.clifton.core.cache.specificity.key;

import com.clifton.core.cache.specificity.SpecificityCache;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SpecificityKeySource</code> contains the information necessary for generating specificity keys. This
 * primarily consists of the hierarchies and their values.
 * <p>
 * Consider our law example introduced in {@link SpecificityCache}. If we wanted to create a key source for the question
 * "What is the most specific law for oranges in Minnesota?", we would use something like:
 * <pre>
 * SpecificityKeySource keySource = SpecificityKeySource
 *      .builder("Minneapolis", "Minnesota", "USA", null)
 *      .addHierarchy("orange", "fruit", null)
 *      .build();
 * </pre>
 * In this example, two tree structures exist: the geographic hierarchy and the food type hierarchy. Depending on the
 * {@link AbstractSpecificityKeyGenerator} used, the geographic hierarchy could be more specific than the food type
 * hierarchy, or the specificity could be evaluated using some combination of the two hierarchies.
 * <p>
 * Another way to think of these hierarchy values is: "What values should be tried to find a matching target, and in
 * what order should they be tried in?" When more than one hierarchy is involved, the order in which combinations are
 * tried is determined by the implementation of {@link AbstractSpecificityKeyGenerator} that is used, which is typically
 * either {@link SimpleOrderingSpecificityKeyGenerator} or {@link ScoredOrderingSpecificityKeyGenerator}. If only one
 * hierarchy is involved, then {@link SimpleOrderingSpecificityKeyGenerator} is sufficient.
 * <p>
 * Notice that the last index in both hierarchies is <tt>null</tt>. The last index is typically treated as a "wildcard"
 * value that means any value holds true. For example, in the "Food" hierarchy, this would mean "any food, regardless of
 * type". We typically represent this using <tt>null</tt>.
 * <p>
 * Consider the following example that builds a key source:
 * <pre>
 * SpecificityKeySource keySource = SpecificityKeySource
 *      .builder(null, "Minnesota", null, null)
 *      .addHierarchy(null, "fruit", null)
 *      .build();
 * </pre>
 * Notice that the number and size of the hierarchies match in both cases. This is required. The existence of the
 * <tt>nulls</tt> in the second example may seem silly, but it is a convenience so that {@link
 * AbstractSpecificityKeyGenerator} can build the correct key for you by finding the first non-null value in each
 * hierarchy and noting its index.
 * <p>
 * <b>An important fact to understand is that a key source for a <i>Target</i> is used to generate a <i>single</i> key
 * representing the specificity of that target. A key source for a <i>Source</i> is used to generate <i>multiple</i>
 * keys (one at a time, in order, until a match is found).</b>
 * <p>
 * Note: No more than {@value #MAX_HIERARCHIES} hierarchies and no more than {@value #MAX_ELEMENTS_PER_HIERARCHY} values
 * in a hierarchy are supported. Determining the most specific match is a non-scalable problem, and supporting more than
 * these limitations would be impractical due to CPU and memory consumption.
 *
 * @author jgommels
 */
public class SpecificityKeySource {

	private static final short MAX_HIERARCHIES = 7;
	private static final short MAX_ELEMENTS_PER_HIERARCHY = 10;

	private final List<Object[]> hierarchies;
	private final Object[] valuesToAppend;

	private final int numPossibleKeys;


	SpecificityKeySource(Builder builder) {
		this.hierarchies = builder.hierarchies;
		this.valuesToAppend = builder.valuesToAppend;

		if (this.hierarchies.size() > MAX_HIERARCHIES) {
			throw new IllegalArgumentException("No more than " + MAX_HIERARCHIES + " hierarchies are supported.");
		}

		int numPossibleKeysVal = 1;
		for (Object[] hierarchy : this.hierarchies) {
			int length = hierarchy.length;
			if (length > MAX_ELEMENTS_PER_HIERARCHY) {
				throw new IllegalArgumentException("No more than " + MAX_ELEMENTS_PER_HIERARCHY + " elements in a hierarchy are supported.");
			}
			numPossibleKeysVal *= length;
		}

		this.numPossibleKeys = numPossibleKeysVal;
	}


	public Object[] getValuesToAppend() {
		return this.valuesToAppend;
	}


	public List<Object[]> getHierarchies() {
		return this.hierarchies;
	}


	public int getNumHierarchies() {
		return this.hierarchies.size();
	}


	public int getNumPossibleKeys() {
		return this.numPossibleKeys;
	}


	public static Builder builder(Object... mostSpecificHierarchy) {
		return new Builder(mostSpecificHierarchy);
	}


	public static class Builder {

		final List<Object[]> hierarchies = new ArrayList<>();
		Object[] valuesToAppend = null;


		/**
		 * @param mostSpecificHierarchy the most specific hierarchy
		 */
		Builder(Object... mostSpecificHierarchy) {
			this.hierarchies.add(mostSpecificHierarchy);
		}


		/**
		 * @param hierarchy the next most specific hierarchy
		 */
		public Builder addHierarchy(Object... hierarchy) {
			this.hierarchies.add(hierarchy);
			return this;
		}


		/**
		 * @param valuesToAppend a set of values that are required to match between a source and a target. Essentially,
		 *                       these are values that are appended to the key(s), and are an additional requirement in
		 *                       terms of <b>equality</b> rather than <b>specificity</b>.
		 */
		public Builder setValuesToAppend(Object... valuesToAppend) {
			this.valuesToAppend = valuesToAppend;
			return this;
		}


		/**
		 * Builds and returns the key source.
		 */
		public SpecificityKeySource build() {
			return new SpecificityKeySource(this);
		}
	}
}
