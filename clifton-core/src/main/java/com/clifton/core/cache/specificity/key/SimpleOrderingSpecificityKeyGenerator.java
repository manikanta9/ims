package com.clifton.core.cache.specificity.key;


import java.util.List;


/**
 * This is a simple implementation of {@link AbstractSpecificityKeyGenerator}. In this implementation, latter
 * hierarchies are <i>always</i> less specific than former hierarchies. If more complex evaluation for specificity
 * ordering is required, see {@link ScoredOrderingSpecificityKeyGenerator}.
 * <p>
 * If the problem requires only one hierarchy then the behavior is obvious: the keys are attempted from most specific to
 * least specific within that hierarchy. When there are multiple hierarchies, the ordering of the keys for a source is
 * determined by effectively computing the cross product across the hierarchies. For example, consider the following two
 * hierarchies:
 * <ul>
 * <li>Hierarchy1 = <tt>{1, 2, null}</tt>
 * <li>Hierarchy2 = <tt>{A, B, null}</tt>
 * </ul>
 * The ordering of keys would be:
 * <ul>
 * <li><tt>(1, A)</tt>
 * <li><tt>(1, B)</tt>
 * <li><tt>(1, null)</tt>
 * <li><tt>(2, A)</tt>
 * <li><tt>(2, B)</tt>
 * <li><tt>(2, null)</tt>
 * <li><tt>(null, null</tt>)
 * </ul>
 * Recall that the last element in a hierarchy array may be considered a "wildcard", often denoted with
 * <tt>null</tt>. However, for this implementation, the last element of the array is not treated differently in the
 * ordering, unlike {@link ScoredOrderingSpecificityKeyGenerator}.
 * <p>
 * If this ordering will not work for your use case (for example, if it doesn't make sense for <tt>(1, null)</tt> to be
 * considered more specific than <tt>(2, A)</tt> given the wildcard), then consider using {@link
 * ScoredOrderingSpecificityKeyGenerator} instead.
 *
 * @author jgommels
 * @see ScoredOrderingSpecificityKeyGenerator
 */
public class SimpleOrderingSpecificityKeyGenerator extends AbstractSpecificityKeyGenerator {

	@Override
	public String generateKeyForSource(int specificityValue, SpecificityKeySource keySource) {
		throwExceptionIfSpecificityNotValid(specificityValue, keySource);

		/*
		 * Get the indexes of the elements to use within each hierarchy. The math used here is similar to an algorithm
		 * that could be used to convert a value from base 10 to binary. For example, take a three-tiered hierarchy
		 * where each tier has the values A, B, and C. For increasing specificity values, the resulting keys would be
		 * as so:
		 *
		 * Value | Indexes | Key
		 * ------+---------+------
		 *   0   |  0 0 0  | A A A
		 *   1   |  0 0 1  | A A B
		 *   2   |  0 0 2  | A A C
		 *   3   |  0 1 0  | A B A
		 *   4   |  0 1 1  | A B B
		 *   5   |  0 1 2  | A B C
		 */
		List<Object[]> hierarchies = keySource.getHierarchies();
		byte[] hierarchyIndexes = new byte[hierarchies.size()];
		int numPossibilitiesPriorToCurrentHierarchy = 1;
		for (int i = hierarchies.size() - 1; i >= 0; i--) {
			Object[] hierarchy = hierarchies.get(i);
			int numElementsAtHierarchy = hierarchy.length;
			int specificityValueAtHierarchy = specificityValue / numPossibilitiesPriorToCurrentHierarchy;
			byte hierarchyIndex = (byte) (specificityValueAtHierarchy % numElementsAtHierarchy);

			hierarchyIndexes[i] = hierarchyIndex;
			numPossibilitiesPriorToCurrentHierarchy *= hierarchies.get(i).length;
		}

		return generateKey(hierarchyIndexes, hierarchies, keySource.getValuesToAppend());
	}
}
