package com.clifton.core.cache.specificity.key;


import com.clifton.core.util.compare.CompareUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * The <code>ScoredOrderingSpecificityKeyGenerator</code> is an implementation of {@link AbstractSpecificityKeyGenerator} that generates keys for sources using a scoring system.
 * While {@link SimpleOrderingSpecificityKeyGenerator} works for many use cases, it does not produce expected results in certain situations. For example, consider the case where
 * there are two hierarchies, Hierarchy1 and Hierarchy2. Both hierarchies have four possible indexes of specificity, where the very last index is a "wildcard" specificity that
 * means any value. (For example, {city, state, country, null}). Now consider the following two "specificities":
 * <ul>
 * <li>A: The most specific value of Hierarchy1 and the least specific (wildcard) value of Hierarchy2
 * <li>B: The second most specific value of Hierarchy1 and the most specific value of Hierarchy2
 * </ul>
 * <p/>
 * If {@link SimpleOrderingSpecificityKeyGenerator} was used, example A would take precedence over B. But is it logical for Hierarchy1 to have such a precedence over the other
 * hierarchies? In many use cases, likely not.
 * <p/>
 * This is what the <code>ScoredOrderingSpecificityKeyGenerator</code> is for. It works by building a sorted list of definitions, where each definition defines what indexes for a
 * given specificity. For example, we can ask "What is the specificity key that represents the fourth most specific (specificity=3) possible target?" The cache will find the
 * definition with index=3 and use the indexes to return back a key.
 * <p/>
 * The formula for determining specificity takes into consideration three things:
 * <ul>
 * <li>The ordering of the hierarchies (the first hierarchy is more specific than the second, which is more specific than the third, etc.)
 * <li>The index of the value in each hierarchy (the first value in a hierarchy is more specific than the second, etc.)
 * <li>Whether or not the index is the last index in the hierarchy (last indexes represent a "wildcard" value)
 * </ul>
 *
 * @author jgommels
 */
public class ScoredOrderingSpecificityKeyGenerator extends AbstractSpecificityKeyGenerator {

	/**
	 * An ordered list of definitions that define which indexes to use on the hierarchies given a specificity value
	 */
	private volatile List<SpecificityDefinition> specificityDefinitions = null;

	/**
	 * Used to synchronize the construction of specificityDefinitions
	 */
	private final Object DEFINITIONS_LOCK = new Object();


	@Override
	public String generateKeyForSource(int specificityValue, SpecificityKeySource keySource) {
		throwExceptionIfSpecificityNotValid(specificityValue, keySource);

		if (this.specificityDefinitions == null) {
			synchronized (this.DEFINITIONS_LOCK) {
				if (this.specificityDefinitions == null) {
					this.specificityDefinitions = generateSpecificityDefinitions(keySource);
				}
			}
		}

		byte[] indexes = this.specificityDefinitions.get(specificityValue).indexes;

		return generateKey(indexes, keySource.getHierarchies(), keySource.getValuesToAppend());
	}


	private List<SpecificityDefinition> generateSpecificityDefinitions(SpecificityKeySource keySource) {
		byte[] indexes = new byte[keySource.getHierarchies().size()];
		List<SpecificityDefinition> definitions = new ArrayList<>();
		generateSpecificityDefinitionsRecurse(definitions, indexes, keySource, 0);
		Collections.sort(definitions);
		return definitions;
	}


	private void generateSpecificityDefinitionsRecurse(List<SpecificityDefinition> definitions, byte[] indexes, SpecificityKeySource keySource, int column) {
		int numRows = keySource.getHierarchies().get(column).length;

		//Whether the current column being processed is the final column
		boolean lastColumn = column == (keySource.getNumHierarchies() - 1);

		for (byte i = 0; i < numRows; i++) {
			indexes[column] = i;
			if (lastColumn) {
				byte[] copy = new byte[keySource.getNumHierarchies()];
				System.arraycopy(indexes, 0, copy, 0, keySource.getNumHierarchies());
				int specificityScore = calculateSpecificityScore(copy, keySource);
				definitions.add(new SpecificityDefinition(specificityScore, copy));
			}
			else {
				generateSpecificityDefinitionsRecurse(definitions, indexes, keySource, column + 1);
			}
		}
	}


	/**
	 * @param indexes   the indexes of the hierarchies
	 * @param keySource the key source containing information about the hierarchies
	 * @return a score representing the specificity based on the indexes and hierarchies (the higher the score, the less
	 * specific)
	 */
	private static int calculateSpecificityScore(byte[] indexes, SpecificityKeySource keySource) {
		int numHierarchies = indexes.length;

		int score = 0;
		for (int i = 0; i < numHierarchies; i++) {
			int index = indexes[i];
			int hierarchyTotal = 10000 * (numHierarchies - i);
			int hierarchyLength = keySource.getHierarchies().get(i).length;

			/*
			 * The value per row is first determined by dividing the hierarchyTotal by hierarchyLength. Since this alone
			 * would result in many different specificities being equal, we give a slight precedence towards the
			 * hierarchies at the front by adding an additional value to valuePerRow based on the hierarchy.
			 */
			int valuePerRow = hierarchyTotal / (hierarchyLength - 1) + (numHierarchies - i - 1);

			score += valuePerRow * index;

			/*
			 * If this is the last index, then treat this as a "wildcard" value for this hierarchy. We add an additional
			 * amount in this case. The more specific the hierarchy, the larger the amount we add for the wildcard. (If
			 * the most specific hierarchy has a "wildcard" index, then that is less specific than an entity that has a
			 * "wildcard" index in the least specific hierarchy).
			 */
			if ((index + 1) == hierarchyLength) {
				score += (numHierarchies - i) * 100;
			}
		}

		return score;
	}


	private static class SpecificityDefinition implements Comparable<SpecificityDefinition> {

		/**
		 * 0 is most specific, 1 is second most specific, etc.
		 */
		final int specificityScore;

		/**
		 * The indexes of the hierarchies for this specificity
		 */
		final byte[] indexes;


		SpecificityDefinition(int specificityScore, byte[] indexes) {
			this.specificityScore = specificityScore;
			this.indexes = indexes;
		}


		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + this.specificityScore;
			result = prime * result + Arrays.hashCode(this.indexes);
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj) {
				return true;
			}
			if (obj == null) {
				return false;
			}
			if (getClass() != obj.getClass()) {
				return false;
			}
			SpecificityDefinition other = (SpecificityDefinition) obj;
			if (this.specificityScore != other.specificityScore) {
				return false;
			}
			return CompareUtils.isEqual(this.indexes, other.indexes);
		}


		@Override
		public int compareTo(SpecificityDefinition other) {
			return this.specificityScore - other.specificityScore;
		}


		@Override
		public String toString() {
			return "SpecificityDefinition [specificityScore=" + this.specificityScore + ", indexes=" + Arrays.toString(this.indexes) + "]";
		}
	}
}
