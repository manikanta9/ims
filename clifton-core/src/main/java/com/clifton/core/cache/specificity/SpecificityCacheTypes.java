package com.clifton.core.cache.specificity;


import com.clifton.core.util.collections.CollectionFactory;
import org.apache.commons.collections.buffer.CircularFifoBuffer;

import java.util.ArrayList;
import java.util.TreeSet;


@SuppressWarnings("rawtypes")
public enum SpecificityCacheTypes {
	/**
	 * A {@link SpecificityCache} type in which a given {@link SpecificityScope} will have at most one result for a particular key. {@link AbstractSpecificityCache#isMatch(Object, SpecificityTargetHolder)} will
	 * be called for the result returned by each {@link SpecificityScope}.
	 */

	ONE_TO_ONE_RESULT(() ->
			//Essentially a collection with a max size of 1. Adding a new value will replace any previously existing value.
			new CircularFifoBuffer(1)
	),


	/**
	 * A {@link SpecificityCache} type in which the cache can return multiple results for a given {@link SpecificityScope} for a particular key. The list of results is iterated through, for which
	 * {@link AbstractSpecificityCache#isMatch(Object, SpecificityTargetHolder)} is called for each until it returns <code>true</code>. One example of where this is useful is in a case where the cache
	 * is searching for the "most specific" rule given some parameters, but each rule is only valid for a certain date range. In this case, there may be multiple rules that match a given level of "specificity",
	 * but further comparison is needed on each rule in the result set to determine which one applies to the current date.
	 */
	MATCHING_RESULT(ArrayList::new),


	/**
	 * Similar to {@link #MATCHING_RESULT}, with the addition that the results are sorted according to their natural ordering. {@link AbstractSpecificityCache#isMatch(Object, SpecificityTargetHolder)} is called
	 * for each until it returns <code>true</code>. <b>Implementations of {@link SpecificityTargetHolder} associated with specificity cache implementations that use this type must implement {@link Comparable}.</b>
	 */
	MATCHING_ORDERED_RESULT(TreeSet::new);


	private final CollectionFactory collectionFactory;


	SpecificityCacheTypes(CollectionFactory collectionFactory) {
		this.collectionFactory = collectionFactory;
	}


	public CollectionFactory getCollectionFactory() {
		return this.collectionFactory;
	}
}
