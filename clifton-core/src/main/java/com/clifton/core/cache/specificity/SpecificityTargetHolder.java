package com.clifton.core.cache.specificity;


/**
 * The <code>SpecificityTargetHolder</code> is used with implementations of {@link AbstractSpecificityCache} and is used to contain
 * the meta data for a target. It is what is actually stored in the specificity cache.
 *
 * @author jgommels
 */
public interface SpecificityTargetHolder {

	/**
	 * Returns a string representing the specificity of the target that this holder maps to.
	 */
	public String getKey();


	/**
	 * Returns the scope for the target that this holder maps to. If scoping is unnecessary, then the implementing
	 * class can simply return <code>null</code>.
	 */
	public SpecificityScope getScope();
}
