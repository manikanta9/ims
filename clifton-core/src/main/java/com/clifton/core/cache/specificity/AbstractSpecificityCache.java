package com.clifton.core.cache.specificity;


import com.clifton.core.cache.LazilyBuiltCache;
import com.clifton.core.cache.specificity.key.AbstractSpecificityKeyGenerator;
import com.clifton.core.cache.specificity.key.SpecificityKeySource;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;


/**
 * The <code>AbstractSpecificityCache</code> is a {@link SpecificityCache} implementation that relies on the use of "specificity keys". A specificity key is basically a
 * <code>String</code> that is used to label how specific something is.
 * <p/>
 * The <code>AbstractSpecificityCache</code> does not cache the target itself, instead it caches a <b>Target Holder</b> that contains the meta data to retrieve the target. The
 * advantage of this is that the "holder" can contain only the minimal set of information needed, thus reducing the memory footprint of the cache. The logic for converting a target
 * holder back to a target depends on the underlying implementation of the class extending <code>AbstractSpecificityCache</code>, but in general it is best for the retrieval to be
 * efficient (e.g. the target itself is cached in the Hibernate cache by its ID).
 * <p/>
 * The structure of the cache and even thus the traversal of the cache is determined by the {@link SpecificityCacheTypes} supplied by the implementing class.
 * <p/>
 * Specificity caches have two main pieces of logic:
 * <ul>
 * <li>How the cache is built</li>
 * <li>How the cache is queried for results</li>
 * </ul>
 * <p/>
 * How the structure of the cache is built depends on the underlying implementation, but in general the entire list of targets are first retrieved, and a target holder is generated
 * from every target. A specificity key is then generated from the target holder, and a mapping between the key and the target holder is placed into the cache.
 * <p/>
 * When a <b>Source</b> is provided to query the cache, a {@link SpecificityKeySource} generated for the source. Using the key source, the cache is then queried using keys in order
 * from most specific to least specific until a match is found. Depending on the {@link SpecificityCacheTypes}, there may be just one result, or there could be multiple results.
 * Refer to {@link SpecificityCacheTypes} for more information on these different types.
 *
 * @param <S> the source type (the data that is sent to the cache in order to get the most specific results associated with it)
 * @param <T> the target type (the data the cache returns)
 * @param <U> the holder type (the cached meta data that can be used to retrieve a target)
 * @author jgommels
 */
public abstract class AbstractSpecificityCache<S, T, U extends SpecificityTargetHolder> extends LazilyBuiltCache<SpecificityCacheStructure<U>> implements SpecificityCache<S, T> {

	private final SpecificityCacheTypes type;
	/**
	 * The key generator this cache uses for generating keys for sources.
	 */
	private final AbstractSpecificityKeyGenerator keyGenerator;

	private volatile boolean initialized = false;

	private ContextHandler contextHandler;
	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @param type         the type of specificity cache, which describes how the cache will store results
	 * @param keyGenerator the key generator for generating keys for sources
	 */
	protected AbstractSpecificityCache(SpecificityCacheTypes type, AbstractSpecificityKeyGenerator keyGenerator) {
		this.type = type;
		this.keyGenerator = keyGenerator;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Whether or not the <code>source</code> is a true match with the <code>holder</code>. When this method is called, it should be assumed that the <code>holder</code> in
	 * question has already been matched with the <code>source</code> as having the same level of "specificity". In some implementations, simply returning <code>true</code> may be
	 * sufficient, but in many cases further comparison is needed.
	 *
	 * @param source the source object in the match comparison
	 * @param holder the {@link SpecificityTargetHolder} in the match comparison
	 * @return a boolean indicating whether or not there is a match between the source and holder
	 */
	protected abstract boolean isMatch(S source, U holder);


	/**
	 * @param targetHolder the {@link SpecificityTargetHolder} to retrieve the corresponding target for
	 * @return the target object that corresponds to <code>targetHolder</code>
	 */
	protected abstract T getTarget(U targetHolder);


	/**
	 * @param source the source to retrieve a key source for
	 * @return the key source used to generate keys for <code>source</code>
	 */
	protected abstract SpecificityKeySource getKeySource(S source);


	/**
	 * @return the complete list of targets holders to cache. This method is called when the cache structure is built.
	 */
	protected abstract Collection<U> getTargetHolders();


	/**
	 * @return the {@link SpecificityCacheUpdater} instance that will handle updates to the cache
	 */
	protected abstract SpecificityCacheUpdater<U> getCacheUpdater();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<T> getMostSpecificResultList(S source) {
		SpecificityCacheStructure<U> cache = getOrBuildCache();

		SpecificityKeySource keySource = getKeySource(source);

		//The actual results (the cached targets)
		List<T> results = new ArrayList<>();

		//The list of scopes in the cache
		Set<SpecificityScope> scopes = cache.getScopes();

		//Iterate through each scope, and get the most specific result(s) for each one
		for (SpecificityScope scope : CollectionUtils.getIterable(scopes)) {
			List<T> scopeResults = getMostSpecificListAtScope(cache, source, keySource, scope);
			if (!CollectionUtils.isEmpty(scopeResults)) {
				results.addAll(scopeResults);
			}
		}
		return results;
	}


	/**
	 * @return the underlying cache structure. May return <code>null</code> is the cache structure has not been built or has been cleared.
	 */
	SpecificityCacheStructure<U> getStructure() {
		return getCacheIfBuilt();
	}


	private List<T> getMostSpecificListAtScope(SpecificityCacheStructure<U> cache, S source, SpecificityKeySource keySource, SpecificityScope scope) {
		int maxResults = scope != null ? scope.getMaxResults() : 1;
		if (maxResults < 1) {
			throw new RuntimeException("getMaxResults() must return an int greater than 0.");
		}
		boolean multipleResultsPerScope = maxResults > 1;
		boolean limitToMaximumSpecificity = scope != null && scope.isLimitToMaximumSpecificity();

		List<T> results = null;
		if (multipleResultsPerScope) {
			results = new ArrayList<>();
		}

		int numResultsForScope = 0;
		boolean doneProcessingScope = false;

		//Iterate through each key and query the cache for this scope/key pair
		for (int i = 0; i < keySource.getNumPossibleKeys(); i++) {
			String key = this.keyGenerator.generateKeyForSource(i, keySource);

			//Get the target holders for this scope/key pair
			Collection<U> targetHolders = cache.getResults(scope, key);

			//We iterate through each targetHolder in the collection to find a match
			for (U targetHolder : CollectionUtils.getIterable(targetHolders)) {
				if (isMatch(source, targetHolder)) {
					T target = getTarget(targetHolder);
					if (multipleResultsPerScope) {
						//If this is a match, then we use the target holder to retrieve the actual target and append it to the list of results
						results.add(target);
						numResultsForScope++;

						// If we are limiting results to a single specificity level, we will complete the scope immediately after the current results are evaluated
						if (limitToMaximumSpecificity) {
							doneProcessingScope = true;
						}

						//If we've reached the maximum number of results for this scope, then we are done processing this scope
						if (numResultsForScope >= maxResults) {
							doneProcessingScope = true;
							break;
						}
					}
					else {
						results = Collections.singletonList(target);
						doneProcessingScope = true;
					}
				}
			}

			//If we are done processing for this scope, then we can move onto the next scope
			if (doneProcessingScope) {
				break;
			}
		}

		if (results == null) {
			results = Collections.emptyList();
		}

		return results;
	}


	@Override
	protected final SpecificityCacheStructure<U> buildCache() {
		//Perform one-time initialization if not done so already
		if (!this.initialized) {
			synchronized (this) {
				if (!this.initialized) {
					SpecificityCacheUpdater<U> updateStrategy = getCacheUpdater();
					if (updateStrategy != null) {
						getCacheUpdater().initialize(this, getContextHandler(), getDaoLocator());
					}

					this.initialized = true;
				}
			}
		}

		//Build the cache
		Collection<U> targetHolders = getTargetHolders();
		SpecificityCacheStructure<U> cache = new SpecificityCacheStructure<>(this.type);

		for (U holder : CollectionUtils.getIterable(targetHolders)) {
			cache.addTargetHolder(holder);
		}

		return cache;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public AbstractSpecificityKeyGenerator getKeyGenerator() {
		return this.keyGenerator;
	}
}
