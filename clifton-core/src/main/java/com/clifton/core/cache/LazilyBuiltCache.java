package com.clifton.core.cache;


/**
 * The <code>LazilyBuiltCache</code> is an abstract class that handles the common logic for a lazily built cache.
 *
 * @param <T> the type of the cache structure
 * @author jgommels
 */
public abstract class LazilyBuiltCache<T> implements CustomCache<String, T> {

	private static final String CACHE_STRUCTURE_NAME = LazilyBuiltCache.class.getName();

	/**
	 * The lock used whenever synchronization on the cache is necessary.
	 */
	private final Object cacheLock = new Object();

	private CacheHandler<String, T> cacheHandler;


	/**
	 * @return the name of this cache. Defaults to the name of implementing class. May be overridden
	 * if a different name is needed.
	 */
	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	protected abstract T buildCache();


	/**
	 * @return the cache if it has been built, otherwise returns <code>null</code>
	 */
	protected T getCacheIfBuilt() {
		return getCacheHandler().get(getCacheName(), CACHE_STRUCTURE_NAME);
	}


	/**
	 * @return the cache if it has already been built, otherwise builds it and then returns it.
	 */
	protected T getOrBuildCache() {
		T cache = getCacheHandler().get(getCacheName(), CACHE_STRUCTURE_NAME);

		//Immediately return the cache if it exists
		if (cache != null) {
			return cache;
		}

		//If the cache doesn't exist, build it. Synchronize on building to ensure it isn't built more than once.
		synchronized (this.cacheLock) {
			cache = getCacheHandler().get(getCacheName(), CACHE_STRUCTURE_NAME);
			if (cache == null) {
				cache = buildCache();
				getCacheHandler().put(getCacheName(), CACHE_STRUCTURE_NAME, cache);
			}

			return cache;
		}
	}


	public void clearCache() {
		synchronized (this.cacheLock) {
			getCacheHandler().clear(getCacheName());
		}
	}


	@Override
	public CacheHandler<String, T> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, T> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
