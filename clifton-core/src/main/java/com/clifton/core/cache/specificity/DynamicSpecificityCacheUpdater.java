package com.clifton.core.cache.specificity;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.collections.ConcurrentMultiValueHashMap;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>DynamicSpecificityCacheUpdater</code> dynamically updates a specificity cache when changes on registered DTOs occur.
 *
 * @param <T> the {@link SpecificityTargetHolder} type
 * @author jgommels
 */
public abstract class DynamicSpecificityCacheUpdater<T extends SpecificityTargetHolder> extends SpecificityCacheUpdater<T> {

	/**
	 * Maps a registered DTO class to a heterogeneous multi-value-map which contains a mapping between specific instances of the registered DTO classes and the associated
	 * {@link SpecificityTargetHolder}s that those instances are registered with.
	 */
	private final Map<Class<? extends IdentityObject>, ConcurrentMultiValueHashMap<? extends IdentityObject, T>> sourceObjectRegistrationMap = new HashMap<>();


	/**
	 * @param registeredTypes the DTO types to register observers on for the cache updater react to events for those DTO types
	 */
	@SafeVarargs
	protected DynamicSpecificityCacheUpdater(Class<? extends IdentityObject>... registeredTypes) {
		super(registeredTypes);
	}


	/**
	 * Registers a specific instance to be associated with a {@link SpecificityTargetHolder}. When a change occurs to that registered instance (UPDATE or DELETE), then
	 * the associated target holder will be removed from the cache. In the case of UPDATE, {@link #getTargetHoldersForCacheUpdate(Object)} will be called so that new
	 * target holders can be constructed to replace the old ones in the cache.
	 *
	 * @param sourceObject
	 * @param targetHolder
	 */
	@Override
	public <V extends IdentityObject> void registerCacheUpdateEvent(V sourceObject, T targetHolder) {
		Class<? extends IdentityObject> sourceClass = sourceObject.getClass();

		//Register DAO observer for this class if haven't already
		if (!isTypeRegistered(sourceClass)) {
			registerType(sourceClass);
		}

		@SuppressWarnings({"unchecked"})
		ConcurrentMultiValueHashMap<V, T> map = (ConcurrentMultiValueHashMap<V, T>) getRegistrationMap(sourceClass);
		map.put(sourceObject, targetHolder);
	}


	protected <U extends IdentityObject> void registerType(Class<U> clazz) {
		if (isTypeRegistered(clazz)) {
			throw new RuntimeException("The class " + clazz.getName() + " has already been registered.");
		}

		configureObserver(clazz);
	}


	/**
	 * This method is called when an INSERT or UPDATE event occurs for an instance of a registered type. Implementations of this method
	 * should return {@link SpecificityTargetHolder} instances that are constructed from the new/updated instance.
	 *
	 * @param newInstance
	 */
	protected abstract List<T> getTargetHoldersForCacheUpdate(Object newInstance);


	@Override
	protected final BaseDaoEventObserver<? extends IdentityObject> createObserverForRegisteredType() {
		return new DynamicSpecificityCacheUpdaterObserver<>();
	}


	@SuppressWarnings("unchecked")
	<V extends IdentityObject> ConcurrentMultiValueHashMap<V, T> getRegistrationMap(Class<V> sourceClass) {
		ConcurrentMultiValueHashMap<V, T> map = (ConcurrentMultiValueHashMap<V, T>) this.sourceObjectRegistrationMap.get(sourceClass);
		if (map == null) {
			synchronized (this.sourceObjectRegistrationMap) {
				map = (ConcurrentMultiValueHashMap<V, T>) this.sourceObjectRegistrationMap.get(sourceClass);
				if (map == null) {
					map = new ConcurrentMultiValueHashMap<>(false);
					this.sourceObjectRegistrationMap.put(sourceClass, map);
				}
			}
		}
		return map;
	}


	/**
	 * The <code>SpecificityCacheObserver</code> is an observer that observes events for a particular DTO type that the specificity cache should know about. For example, consider the case where
	 * the cache needs to react to changes to <code>MyClass</code> objects. An UPDATE event to <code>myClassInstance1</code> will result in the removal of all target holders in the cache associated
	 * with that instance, followed by the insertion of the new target holders based on the new data.
	 *
	 * @param <V> the DTO class for which the cache should react to changes of
	 * @author jgommels
	 */
	class DynamicSpecificityCacheUpdaterObserver<V extends IdentityObject> extends BaseDaoEventObserver<V> {

		@Override
		public void afterMethodCallImpl(@SuppressWarnings("unused") ReadOnlyDAO<V> dao, DaoEventTypes event, V bean, Throwable e) {
			if (e == null) {
				SpecificityCacheStructure<?> cacheStructure = getCache().getStructure();
				if (cacheStructure != null) {
					if (event.isInsert()) {
						addNewTargetHoldersToCache(bean, cacheStructure, true);
					}
					else if (event.isUpdate()) {
						getCache().getStructure().lock();
						try {
							removeExistingTargetHolders(bean, cacheStructure, false);
							addNewTargetHoldersToCache(bean, cacheStructure, false);
						}
						finally {
							getCache().getStructure().unlock();
						}
					}
					else if (event.isDelete()) {
						removeExistingTargetHolders(bean, cacheStructure, true);
					}
				}
			}
		}


		private void addNewTargetHoldersToCache(V bean, SpecificityCacheStructure<?> cacheStructure, boolean lock) {
			if (lock) {
				cacheStructure.lock();
			}

			try {
				List<T> newTargetHolders = getTargetHoldersForCacheUpdate(bean);
				for (T targetHolder : CollectionUtils.getIterable(newTargetHolders)) {
					getCache().getStructure().addTargetHolder(targetHolder);
				}
			}

			finally {
				if (lock) {
					cacheStructure.unlock();
				}
			}
		}


		@SuppressWarnings("unchecked")
		private List<T> removeExistingTargetHolders(V bean, SpecificityCacheStructure<?> cacheStructure, boolean lock) {
			if (lock) {
				cacheStructure.lock();
			}

			List<T> holdersRemoved;
			try {
				holdersRemoved = ((ConcurrentMultiValueHashMap<V, T>) getRegistrationMap(bean.getClass())).removeAll(bean);

				for (T targetHolder : CollectionUtils.getIterable(holdersRemoved)) {
					getCache().getStructure().removeTargetHolder(targetHolder);
				}
			}
			finally {
				if (lock) {
					cacheStructure.unlock();
				}
			}

			return holdersRemoved;
		}
	}
}
