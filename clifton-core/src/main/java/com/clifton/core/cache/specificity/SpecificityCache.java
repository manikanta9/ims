package com.clifton.core.cache.specificity;


import java.util.List;


/**
 * The <code>SpecificityCache</code> is a generic hierarchical "specificity" cache. It is useful when
 * there is a need to efficiently retrieve the most specific result(s) given some input. For example, consider a
 * hypothetical situation where city laws override state laws, and state laws override national laws. Now assume that
 * there are laws regarding how various types of food are processed. We can now ask the question "What is the most specific law
 * regarding oranges in Minneapolis?" In this case, the cache is first checked for relevant laws in Minneapolis regarding
 * oranges, and if it can't find any it then checks for relevant laws in Minnesota regarding oranges, and so on. If no laws
 * regarding oranges can be found, then the next law searched for may be Minneapolis with fruit, then Minnesota with fruit, and
 * so on. The exact ordering of these combinations is dependent on the underlying implementation.
 * <p/>
 * <p/>
 * <p/>
 * The specificity cache takes in a <b>Source</b> as input and returns a <b>Target</b> or list of targets as output.
 *
 * @param <S> the source type (the data that is sent to the cache in order to get the most specific results associated with it)
 * @param <T> the target type (the data the cache returns)
 * @author jgommels
 */
public interface SpecificityCache<S, T> {

	/**
	 * Returns the most specific cached target(s) from the source. The list is determined by generating an ordered list of "specificity" keys and iterating through the list and
	 * checking for a match for each key in the cache.
	 *
	 * @param source the source for which to return matching target(s)
	 */
	public List<T> getMostSpecificResultList(S source);


	public void clearCache();
}
