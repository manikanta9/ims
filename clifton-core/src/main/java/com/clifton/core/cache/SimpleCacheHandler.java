package com.clifton.core.cache;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>SimpleCacheHandler</code> class provides a simple {@link ConcurrentHashMap} based cache implementation.
 *
 * @param <K>
 * @param <V>
 * @author vgomelsky
 */
public class SimpleCacheHandler<K, V> implements CacheHandler<K, V> {

	private final Map<String, Map<K, V>> cacheMap = new ConcurrentHashMap<>();

	private final Map<String, Map<CacheEventTypes, CacheEventListener<K, V>>> eventListenerMap = new ConcurrentHashMap<>();


	@Override
	public void clear(String cacheName) {
		Map<K, V> cache = getCache(cacheName);
		if (cache != null) {
			executeEvent(cacheName, CacheEventTypes.BEFORE_CLEAR, null, null);
			cache.clear();
			executeEvent(cacheName, CacheEventTypes.AFTER_CLEAR, null, null);
		}
	}


	@Override
	public void clearAll() {
		for (String cacheName : getCacheNames()) {
			clear(cacheName);
		}
	}


	@Override
	public V get(String cacheName, K key) {
		Map<K, V> cache = getCache(cacheName);
		if (cache == null) {
			return null;
		}
		return cache.get(key);
	}


	@Override
	public Collection<K> getKeys(String cacheName) {
		Map<K, V> cache = getCache(cacheName);
		if (cache == null) {
			return null;
		}
		return cache.keySet();
	}


	@Override
	public void put(String cacheName, K key, V value) {
		Map<K, V> cache = getCache(cacheName);
		if (cache == null) {
			cache = new ConcurrentHashMap<>();
			this.cacheMap.put(cacheName, cache);
		}
		cache.put(key, value);
	}


	@Override
	public void remove(String cacheName, K key) {
		Map<K, V> cache = getCache(cacheName);
		if (cache != null) {
			executeEvent(cacheName, CacheEventTypes.BEFORE_REMOVE, key, cache.get(key));
			V value = cache.remove(key);
			executeEvent(cacheName, CacheEventTypes.AFTER_REMOVE, key, value);
		}
	}


	@Override
	public String[] getCacheNames() {
		if (this.cacheMap.isEmpty()) {
			return new String[0];
		}
		Set<String> var = this.cacheMap.keySet();
		return var.toArray(new String[0]);
	}


	@Override
	public int setStatisticsEnabled(String cacheName, boolean enableStatistics) {
		// simple cache doesn't use stats
		return 0;
	}


	@Override
	public List<CacheStats> getCacheStats(boolean calculateSize) {
		List<CacheStats> result = new ArrayList<>();
		for (String cacheName : this.cacheMap.keySet()) {
			result.add(getCacheStats(cacheName, calculateSize));
		}
		return result;
	}


	@Override
	public CacheStats getCacheStats(String cacheName) {
		// Getting cache states for one - defaults to calculate size
		return getCacheStats(cacheName, true);
	}


	/**
	 * Can optionally not calculateSizes - useful when trying to load all caches in order to select one to clear.  This can be time consuming.
	 *
	 * @param cacheName
	 * @param calculateSize
	 */
	private CacheStats getCacheStats(String cacheName, boolean calculateSize) {
		Map<K, V> cache = getCache(cacheName);
		// only some stats -
		return new CacheStats(cacheName, calculateSize ? cache.size() : 0, 0, 0, 0, 0, false);
	}


	private Map<K, V> getCache(String cacheName) {
		return this.cacheMap.get(cacheName);
	}


	@Override
	public void registerEventListener(String cacheName, CacheEventTypes eventType, CacheEventListener<K, V> listener) {
		Map<CacheEventTypes, CacheEventListener<K, V>> listenerMap;
		if (this.eventListenerMap.containsKey(cacheName)) {
			listenerMap = this.eventListenerMap.get(cacheName);
		}
		else {
			listenerMap = new ConcurrentHashMap<>();
			this.eventListenerMap.put(cacheName, listenerMap);
		}
		listenerMap.put(eventType, listener);
	}


	private void executeEvent(String cacheName, CacheEventTypes eventType, K key, V value) {
		if (this.eventListenerMap.containsKey(cacheName)) {
			Map<CacheEventTypes, CacheEventListener<K, V>> listenerMap = this.eventListenerMap.get(cacheName);
			if (listenerMap.containsKey(eventType)) {
				listenerMap.get(eventType).execute(cacheName, key, value);
			}
		}
	}
}
