package com.clifton.core.cache.specificity;


/**
 * The <code>SpecificityScope</code> represents a single scope within a {@link SpecificityTargetHolder}.
 * Implementations of this interface must implement <code>hashCode()</code> and <code>equals()</code>.
 *
 * @author jgommels
 */
public interface SpecificityScope {

	/**
	 * @return the maximum number of results to be returned for which match this scope
	 */
	public int getMaxResults();


	/**
	 * If <code>true</code>, then results will be limited to those with the single most-explicit specificity level matched. In other words, if multiple results are found with the
	 * same most-specific specificity, then all of those results and only those results will be returned (subject to other limitations, such as {@link #getMaxResults()}).
	 * <p>
	 * For example, take a specificity cache that produces <i>three</i> results at its most specific level and <i>three more</i> results at its next-most specific level for a given
	 * source. If {@link #getMaxResults()} returns <code>6</code> and this method returns <code>false</code>, then all six results will be returned. However, if this method returns
	 * <code>true</code> then results will be limited to the three results at the most-specific level.
	 */
	public boolean isLimitToMaximumSpecificity();
}
