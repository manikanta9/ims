package com.clifton.core.cache.request;

import com.clifton.core.cache.CacheEventListener;
import com.clifton.core.cache.CacheEventTypes;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CacheStats;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;

import javax.persistence.EntityManager;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The {@link RequestContextCacheHandler} is a {@link CacheHandler} for request-spanning caches. Caches managed by this type will persist throughout the life of the request and be
 * collected once the request has completed.
 * <p>
 * If no request is currently active, then all supported operations for this will effectively be no-ops and all cache queries will return <code>null</code> or otherwise empty
 * values (empty lists, empty arrays, etc.).
 * <p>
 * This cache handler is useful for producing short-lived caches for data that can acceptably be shared throughout the life of a request. Examples of this may include retrieved
 * numeric market data values (not market data entities) or calculated account-wide values.
 * <p>
 * In most cases, care should be taken to not cache persisted entities directly. Since this cache may cross multiple {@link EntityManager} persistence contexts, entities that are
 * cached may become stale and invalid. As an alternative to caching entities directly, unique entity identifiers may be cached and then used to re-retrieve the entity using the
 * current persistence context.
 *
 * @author mikeh
 */
@Component
public class RequestContextCacheHandler<K, V> implements CacheHandler<K, V> {

	@Override
	public V get(String cacheName, K key) {
		return isRequestActive() ? getCache(cacheName).get(key) : null;
	}


	@Override
	public Collection<K> getKeys(String cacheName) {
		return isRequestActive() ? getCache(cacheName).keySet() : Collections.emptyList();
	}


	@Override
	public void put(String cacheName, K key, V value) {
		if (isRequestActive()) {
			getCache(cacheName).put(key, value);
		}
	}


	@Override
	public void remove(String cacheName, K key) {
		if (isRequestActive()) {
			getCache(cacheName).remove(key);
		}
	}


	@Override
	public void clear(String cacheName) {
		if (isRequestActive()) {
			getCache(cacheName).clear();
		}
	}


	@Override
	public void clearAll() {
		if (isRequestActive()) {
			getCacheHolder().clear();
		}
	}


	@Override
	public String[] getCacheNames() {
		return isRequestActive() ? CollectionUtils.toArray(getCacheHolder().keySet(), String.class) : new String[0];
	}


	@Override
	public int setStatisticsEnabled(String cacheName, boolean enableStatistics) {
		// Statistics are not enabled for request context statistics
		return 0;
	}


	@Override
	public List<CacheStats> getCacheStats(boolean calculateSize) {
		return isRequestActive() ? CollectionUtils.getConverted(getCacheHolder().keySet(), cacheName -> getCacheStats(cacheName, calculateSize)) : Collections.emptyList();
	}


	@Override
	public CacheStats getCacheStats(String cacheName) {
		return getCacheStats(cacheName, false);
	}


	@Override
	public void registerEventListener(String cacheName, CacheEventTypes eventType, CacheEventListener<K, V> listener) {
		// Event listener registration not supported; context lifecycle only covers the duration of the current request
		throw new UnsupportedOperationException("Event listener registration is not supported for request context caches.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Map<String, Map<K, V>> getCacheHolder() {
		RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		AssertUtils.assertNotNull(requestAttributes, "The [%s] attempted to access the request context cache while no request was active. No active [%s] object was found.", getClass().getSimpleName(), RequestAttributes.class.getSimpleName());
		@SuppressWarnings("unchecked")
		Map<String, Map<K, V>> cacheHolder = (Map<String, Map<K, V>>) requestAttributes.getAttribute(getRequestCacheAttributeName(), RequestAttributes.SCOPE_REQUEST);
		if (cacheHolder == null) {
			cacheHolder = new HashMap<>();
			requestAttributes.setAttribute(getRequestCacheAttributeName(), cacheHolder, RequestAttributes.SCOPE_REQUEST);
		}
		return cacheHolder;
	}


	private Map<K, V> getCache(String cacheName) {
		return getCacheHolder().computeIfAbsent(cacheName, k -> new HashMap<>());
	}


	private CacheStats getCacheStats(String cacheName, boolean calculateSize) {
		return new CacheStats(cacheName, calculateSize ? getCache(cacheName).size() : 0, 0, 0, 0, 0, false);
	}


	private boolean isRequestActive() {
		return RequestContextHolder.getRequestAttributes() != null;
	}


	private String getRequestCacheAttributeName() {
		return RequestContextCacheHandler.class.getName();
	}
}
