package com.clifton.core.cache;

import com.clifton.core.beans.IdentityObject;

import java.io.Serializable;
import java.time.Duration;


/**
 * <code>TimedEvictionCache</code>  Stores items in a cache until they are removed or a timeout occurs.
 */
public interface TimedEvictionCache<T extends IdentityObject> {

	public void add(T object);


	public void add(T object, Duration evictionTimeout);


	public void remove(Serializable identifier);


	public void process(Serializable identifier);


	public void evict(Serializable identifier);
}
