package com.clifton.core.cache;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.concurrent.NamedThreadFactory;
import com.clifton.core.logging.LogUtils;
import org.springframework.beans.factory.DisposableBean;

import java.io.Serializable;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


/**
 * <code>TimedEvictionCache</code>  Use sparingly, each instance will create its own monitor thread.  Stores items in a cache until they are removed or a timeout occurs.
 * Timed-out items will call the provided eviction action before being removed from the list.
 */
public abstract class AbstractTimedEvictionCache<T extends IdentityObject> implements TimedEvictionCache<T>, DisposableBean {

	/**
	 * If this single thread terminates due to a failure during execution prior to shutdown, a new one will take its place if needed to execute subsequent tasks.
	 * Tasks are guaranteed to execute sequentially, and no more than one task will be active at any given time.  (javadoc)
	 */
	private ScheduledExecutorService monitorExecutor = Executors.newSingleThreadScheduledExecutor(new NamedThreadFactory("Eviction Cache Task"));

	/**
	 * Single worker thread operating off an unbounded queue.  If this single thread terminates due to a failure during execution
	 * prior to shutdown, a new one will take its place if needed to execute subsequent tasks.  Tasks are guaranteed to execute
	 * sequentially, and no more than one task will be active at any given time. (javadoc)
	 */
	private ExecutorService evictionExecutor = Executors.newSingleThreadExecutor(new NamedThreadFactory("Eviction Cache Action"));

	/**
	 * Provides visibility into whether the timer task is still running.  Allows the timer task to be restarted if terminated.
	 */
	private ScheduledFuture<?> monitorFuture;

	/**
	 * The map of identifier to timestamp (milliseconds).
	 */
	private ConcurrentMap<Serializable, Long> cacheMap = new ConcurrentHashMap<>();

	/**
	 * A map holding optional explicit durations for a given cache item.
	 */
	private ConcurrentMap<Serializable, Long> evictionTimeoutMap = new ConcurrentHashMap<>();

	/**
	 * How often to check the map for objects older than duration.
	 */
	private final Duration frequency;

	/**
	 * How old does a queued object need to be before calling the evictionAction.
	 */
	private final Duration evictionTimeout;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractTimedEvictionCache(Duration frequency, Duration evictionTimeout) {
		this.frequency = frequency;
		this.evictionTimeout = evictionTimeout;
		initialize();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Initialize the timer task and begin looking for timed-out list items.
	 */
	public void initialize() {
		TimerTask monitorTask = new TimerTask() {
			@Override
			public void run() {
				long currentTimeMillis = System.currentTimeMillis();
				List<Serializable> expiredKeys = new ArrayList<>();
				for (Map.Entry<Serializable, Long> entry : AbstractTimedEvictionCache.this.cacheMap.entrySet()) {
					long threshold = currentTimeMillis - AbstractTimedEvictionCache.this.evictionTimeoutMap.get(entry.getKey());
					if (entry.getValue() <= threshold) {
						expiredKeys.add(entry.getKey());
					}
				}
				expiredKeys.forEach(AbstractTimedEvictionCache.this::evict);
			}
		};
		this.monitorFuture = this.monitorExecutor.scheduleAtFixedRate(monitorTask, getFrequency().toMillis(), getFrequency().toMillis(), TimeUnit.MILLISECONDS);
	}


	@Override
	public void evict(Serializable identifier) {
		this.evictionExecutor.submit(() -> this.process(identifier));
		this.cacheMap.remove(identifier);
		this.evictionTimeoutMap.remove(identifier);
	}


	/**
	 * Add (or replace) the identifier in the list.
	 */
	@Override
	public void add(T object) {
		add(object, getEvictionTimeout());
	}


	@Override
	public void add(T object, Duration evictionTimeout) {
		// if the task thread dies, queue up a new instance.
		if (this.monitorFuture.isDone()) {
			LogUtils.warn(AbstractTimedEvictionCache.class, "Eviction Cache Task terminated unexpectedly, restarting.");
			initialize();
		}

		Serializable entityKey = getEntityKey(object);
		this.cacheMap.put(entityKey, System.currentTimeMillis());
		this.evictionTimeoutMap.put(entityKey, evictionTimeout.toMillis());
	}


	/**
	 * Remove the identifier from the list (if present).
	 */
	@Override
	public void remove(Serializable identifier) {
		// if the task thread dies, queue up a new instance.
		if (this.monitorFuture.isDone()) {
			initialize();
		}
		this.cacheMap.remove(identifier);
		this.evictionTimeoutMap.remove(identifier);
	}


	/**
	 * Key generator to allow derived classes to implement their own keys.
	 */
	protected Serializable getEntityKey(T entity) {
		return entity.getIdentity();
	}


	@Override
	public void destroy() {
		this.monitorExecutor.shutdown();
		this.evictionExecutor.shutdown();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Duration getFrequency() {
		return this.frequency;
	}


	public Duration getEvictionTimeout() {
		return this.evictionTimeout;
	}
}
