package com.clifton.core.cache;


/**
 * The <code>CacheEventListener</code> defines a listener on a cache event.
 *
 * @author mwacker
 */
public interface CacheEventListener<K, V> {

	public void execute(String cacheName, K key, V value);
}
