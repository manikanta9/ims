package com.clifton.core.logging.message.graylog.converter;

import com.clifton.core.logging.message.LogMessage;
import com.clifton.core.logging.message.graylog.pojo.GraylogMessage;
import com.clifton.core.logging.message.graylog.pojo.GraylogSearchResponseMessageWrapper;
import com.clifton.core.util.converter.Converter;


/**
 * Converter to convert search response message wrapper POJO into our LogMessage
 *
 * @author theodorez
 */
public class GraylogSearchResponseMessageWrapperLogMessageConverter implements Converter<GraylogSearchResponseMessageWrapper, LogMessage> {

	private Converter<GraylogMessage, LogMessage> messageConverter;


	@Override
	public LogMessage convert(GraylogSearchResponseMessageWrapper from) {
		LogMessage message = getMessageConverter().convert(from.getMessage());
		message.setIndex(from.getIndex());
		return message;
	}


	public Converter<GraylogMessage, LogMessage> getMessageConverter() {
		return this.messageConverter;
	}


	public void setMessageConverter(Converter<GraylogMessage, LogMessage> messageConverter) {
		this.messageConverter = messageConverter;
	}
}
