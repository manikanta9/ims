package com.clifton.core.logging;


import org.springframework.util.ErrorHandler;


/**
 * The <code>SpringLoggingErrorHandler</code> can be injected into spring objects to log errors to our log instead of spring's.
 *
 * @author mwacker
 */
public class SpringLoggingErrorHandler implements ErrorHandler {

	private Class<?> errorClass;


	@Override
	public void handleError(Throwable t) {
		LogUtils.error(getErrorClass() != null ? getErrorClass() : SpringLoggingErrorHandler.class, "Spring Error", t);
	}


	public Class<?> getErrorClass() {
		return this.errorClass;
	}


	public void setErrorClass(Class<?> errorClass) {
		this.errorClass = errorClass;
	}
}
