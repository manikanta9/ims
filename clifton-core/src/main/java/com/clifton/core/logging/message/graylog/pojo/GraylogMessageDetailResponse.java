package com.clifton.core.logging.message.graylog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Used to deserialize the object returned from Graylog when retrieving a specific message
 *
 * @author theodorez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraylogMessageDetailResponse {

	@JsonProperty("message")
	private GraylogMessageDetailResponseMessageWrapper messageWrapper;

	private String index;


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public GraylogMessageDetailResponseMessageWrapper getMessageWrapper() {
		return this.messageWrapper;
	}


	public void setMessageWrapper(GraylogMessageDetailResponseMessageWrapper message) {
		this.messageWrapper = message;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}
}
