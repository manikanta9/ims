package com.clifton.core.logging;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.security.SecurityUser;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.ThreadContext;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Configurator;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;


/**
 * The <code>LogUtils</code> class should be used for logging messages at various severity levels. One should never use System.out.print...
 * <p>
 *
 * @author vgomelsky, theodorez
 */
@Component
public class LogUtils implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	public static final String DEFAULT_CONFIG_FILE_PATH = "META-INF/log4j/log4j2.xml";

	public static final JacksonHandlerImpl jacksonHandler = new JacksonHandlerImpl(new JacksonStrategy());

	private static final AtomicReference<ContextHandler> contextHandlerAtomicReference = new AtomicReference<>();

	private static LoggerContext loggerContext;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	static {
		if (!LoggerContext.getContext().isInitialized()) {
			// Initializes the configuration using a config file if the configuration hasn't been completed yet
			// Looks for the system property for an override before using default.
			String configFilePath = System.getProperty("log4j.configurationFile", DEFAULT_CONFIG_FILE_PATH);
			loggerContext = Configurator.initialize(null, configFilePath);
			info(LogCommand.ofMessage(LogUtils.class, "Initialized log context with file: ", configFilePath));
		}
		else {
			info(LogCommand.ofMessage(LogUtils.class, "Skipping Log Context initialization"));
		}
	}


	public static LoggerContext getLoggerContext() {
		return loggerContext;
	}


	public static Configuration getLoggerConfiguration() {
		return loggerContext.getConfiguration();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Logs the given message at the given {@link LogLevels level}.
	 */
	public static void log(Class<?> loggingClass, LogLevels level, String message) {
		log(loggingClass, level, message, null);
	}


	/**
	 * Logs the given message and exception at the given {@link LogLevels level}.
	 */
	public static void log(Class<?> loggingClass, LogLevels level, String message, Throwable exception) {
		Log log = getLogForClass(loggingClass);
		if (log.isErrorEnabled()) {
			doLog(log, level, message, exception, null);
		}
	}


	/**
	 * Logs the given log command at its provided {@link LogCommand#level level}.
	 */
	public static void log(LogCommand command) {
		Log log = getLogForClass(command.getLogClass());
		if (log.isErrorEnabled()) {
			doLog(log, command.getLogLevel(), command.getMessage(), command.getThrowable(), command.getRequest());
		}
	}


	/**
	 * Logs the given message at the error level
	 */
	public static void error(Class<?> loggingClass, String message) {
		error(loggingClass, message, null);
	}


	/**
	 * Logs the given message and exception at the error level
	 */
	public static void error(Class<?> loggingClass, String message, Throwable exception) {
		Log log = getLogForClass(loggingClass);
		if (log.isErrorEnabled()) {
			doLog(log, LogLevels.ERROR, message, exception, null);
		}
	}


	/**
	 * Logs the given log command at the error level
	 */
	public static void error(LogCommand command) {
		Log log = getLogForClass(command.getLogClass());
		if (log.isErrorEnabled()) {
			doLog(log, LogLevels.ERROR, command.getMessage(), command.getThrowable(), command.getRequest());
		}
	}


	/**
	 * Logs the given message at the error level if it is not a ValidationException, otherwise logs it at the info level
	 */
	public static void errorOrInfo(Class<?> loggingClass, String message) {
		errorOrInfo(loggingClass, message, null);
	}


	/**
	 * Logs the given message and exception at the error level if it is not a ValidationException, otherwise logs it at the info level
	 */
	public static void errorOrInfo(Class<?> loggingClass, String message, Throwable exception) {
		if (CoreExceptionUtils.isValidationException(exception)) {
			info(loggingClass, message, exception);
		}
		else {
			error(loggingClass, message, exception);
		}
	}


	/**
	 * Logs the command at error level if the exception is not a validation exception
	 */
	public static void errorOrInfo(LogCommand command) {
		if (CoreExceptionUtils.isValidationException(command.getThrowable())) {
			info(command);
		}
		else {
			error(command);
		}
	}


	/**
	 * Logs the given message at the warn level
	 */
	public static void warn(Class<?> loggingClass, String message) {
		warn(loggingClass, message, null);
	}


	/**
	 * Logs the given message and exception at the warn level
	 */
	public static void warn(Class<?> loggingClass, String message, Throwable exception) {
		Log log = getLogForClass(loggingClass);
		if (log.isWarnEnabled()) {
			doLog(log, LogLevels.WARN, message, exception, null);
		}
	}


	/**
	 * Logs the given command at warn level
	 */
	public static void warn(LogCommand command) {
		Log log = getLogForClass(command.getLogClass());
		if (log.isWarnEnabled()) {
			doLog(log, LogLevels.WARN, command.getMessage(), command.getThrowable(), command.getRequest());
		}
	}


	/**
	 * Logs the given message at info level
	 */
	public static void info(Class<?> loggingClass, String message) {
		info(loggingClass, message, null);
	}


	/**
	 * Logs the given message and exception at the info level
	 */
	public static void info(Class<?> loggingClass, String message, Throwable exception) {
		Log log = getLogForClass(loggingClass);
		if (log.isInfoEnabled()) {
			doLog(log, LogLevels.INFO, message, exception, null);
		}
	}


	/**
	 * Logs the given command at info level
	 */
	public static void info(LogCommand command) {
		Log log = getLogForClass(command.getLogClass());
		if (log.isInfoEnabled()) {
			doLog(log, LogLevels.INFO, command.getMessage(), command.getThrowable(), command.getRequest());
		}
	}


	/**
	 * Logs the given message at debug level
	 */
	public static void debug(Class<?> loggingClass, String message) {
		debug(loggingClass, message, null);
	}


	/**
	 * Logs the given message and exception at debug level
	 */
	public static void debug(Class<?> loggingClass, String message, Throwable exception) {
		Log log = getLogForClass(loggingClass);
		if (log.isDebugEnabled()) {
			doLog(log, LogLevels.DEBUG, message, exception, null);
		}
	}


	/**
	 * Logs the given command at debug level
	 */
	public static void debug(LogCommand command) {
		Log log = getLogForClass(command.getLogClass());
		if (log.isDebugEnabled()) {
			doLog(log, LogLevels.DEBUG, command.getMessage(), command.getThrowable(), command.getRequest());
		}
	}


	/**
	 * Logs the given message at trace level
	 */
	public static void trace(Class<?> loggingClass, String message) {
		trace(loggingClass, message, null);
	}


	/**
	 * Logs the given message and exception at trace level
	 */
	public static void trace(Class<?> loggingClass, String message, Throwable exception) {
		Log log = getLogForClass(loggingClass);
		if (log.isTraceEnabled()) {
			doLog(log, LogLevels.TRACE, message, exception, null);
		}
	}


	/**
	 * Logs the given command at trace level
	 */
	public static void trace(LogCommand command) {
		Log log = getLogForClass(command.getLogClass());
		if (log.isTraceEnabled()) {
			doLog(log, LogLevels.TRACE, command.getMessage(), command.getThrowable(), command.getRequest());
		}
	}


	private static void doLog(Log log, LogLevels level, String message, Throwable throwable, HttpServletRequest request) {
		populateContext(request);
		switch (level) {
			case FATAL:
				log.fatal(message, throwable);
				break;
			case ERROR:
				log.error(message, throwable);
				break;
			case WARN:
				log.warn(message, throwable);
				break;
			case INFO:
				log.info(message, throwable);
				break;
			case DEBUG:
				log.debug(message, throwable);
				break;
			case TRACE:
				log.trace(message, throwable);
				break;
			default:
				// Fall back to error level
				log.error("(Unrecognized log level: " + level + "; logging at level: \"" + LogLevels.ERROR.name() + "\") " + message, throwable);
		}
		ThreadContext.clearAll();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the logger for the given class.
	 * <p>
	 * Note: Logs are cached in the LogFactory
	 */
	private static Log getLogForClass(Class<?> clazz) {
		if (clazz.equals(String.class)) {
			throw new IllegalArgumentException("Cannot do logging for String class. Must use the class that does logging.");
		}
		// a thread's context class holder is not guaranteed to be non-null
		ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
		if (classLoader == null) {
			Thread.currentThread().setContextClassLoader(LogUtils.class.getClassLoader());
		}
		return LogFactory.getLog(clazz);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Adds the given context property to the <i>Mapped Diagnostic Context</i>, or MDC. These properties may be used by the underlying logging implementation to determine logging
	 * characteristics, such as additional logged properties.
	 * <p>
	 * The MDC will automatically be cleared after execution for each log statement.
	 *
	 * @see <a href="https://logging.apache.org/log4j/2.x/manual/thread-context.html">Log4j 2 "Thread Context" documentation</a>
	 * @see <a href="http://www.slf4j.org/manual.html#mdc">SLF4J MDC documentation</a>
	 */
	public static void addContextProperty(String key, String value) {
		ThreadContext.put(key, value);
	}


	private static void populateContext(HttpServletRequest request) {
		populateUserInformation();
		// When using Spring, infer request from context if available
		HttpServletRequest inferredRequest = request;
		if (inferredRequest == null) {
			RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
			if (requestAttributes instanceof ServletRequestAttributes) {
				inferredRequest = ((ServletRequestAttributes) requestAttributes).getRequest();
			}
		}
		if (inferredRequest != null) {
			populateRequestContext(inferredRequest);
			populateRequestHeaders(inferredRequest);
			HttpSession session = inferredRequest.getSession(false);
			if (session != null) {
				populateSessionContext(session);
			}
		}
	}


	private static void populateUserInformation() {
		IdentityObject currentUser = getCurrentUser();
		if (currentUser instanceof SecurityUser) {
			ThreadContext.put("current_username", ((SecurityUser) currentUser).getUserName());
		}
	}


	private static void populateRequestContext(HttpServletRequest request) {
		ThreadContext.put("request_server", request.getServerName());
		ThreadContext.put("request_client", request.getRemoteAddr());
		ThreadContext.put("request_uri", request.getRequestURI());
		ThreadContext.put("request_parameters", jacksonHandler.toJson(request.getParameterMap()));

		String key;
		String className;
		Object value;

		Map<String, String[]> attributeMap = new HashMap<>();

		// get request attributes
		Enumeration<String> attributes = request.getAttributeNames();
		if (attributes != null) {

			while (attributes.hasMoreElements()) {
				key = attributes.nextElement();

				value = request.getAttribute(key);
				if (value != null) {
					className = value.getClass().getName();
				}
				else {
					className = "null";
				}
				attributeMap.put(key, new String[]{className, value == null ? null : value.toString()});
			}
		}
		ThreadContext.put("request_attributes", jacksonHandler.toJson(attributeMap));
	}


	private static void populateRequestHeaders(HttpServletRequest request) {
		Map<String, String> headerMap = new HashMap<>();
		Enumeration<String> headerNames = request.getHeaderNames();
		while (headerNames.hasMoreElements()) {
			String headerName = headerNames.nextElement();
			if (headerName != null && !StringUtils.equalsAnyIgnoreCase(headerName, "cookie", "Authorization")) {
				String headerValue = request.getHeader(headerName);
				headerMap.put(headerName, headerValue);
			}
		}
		ThreadContext.put("request_headers", jacksonHandler.toJson(headerMap));
	}


	private static void populateSessionContext(HttpSession session) {
		ThreadContext.put("session_id", session.getId());

		String key;
		String className;
		Object value;
		// have valid session - get an array of keys
		Enumeration<String> keys = session.getAttributeNames();
		if (keys != null) {
			Map<String, String> attributeMap = new HashMap<>();

			while (keys.hasMoreElements()) {
				key = keys.nextElement();
				value = session.getAttribute(key);
				if (value != null) {
					className = value.getClass().getName();
				}
				else {
					className = "null";
				}
				attributeMap.put(key, jacksonHandler.toJson(new String[]{className, value == null ? null : value.toString()}));
			}
			ThreadContext.put("session_attributes", jacksonHandler.toJson(attributeMap));
		}
	}


	private static IdentityObject getCurrentUser() {
		ContextHandler handler = contextHandlerAtomicReference.get();
		if (handler != null) {
			return (IdentityObject) handler.getBean(Context.USER_BEAN_NAME);
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		ContextHandler handler = (ContextHandler) getApplicationContext().getBean("contextHandler");
		contextHandlerAtomicReference.set(handler);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
