package com.clifton.core.logging.message.graylog.converter;

import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.logging.message.LogMessage;
import com.clifton.core.logging.message.LogMessageProperty;
import com.clifton.core.logging.message.graylog.GraylogUtils;
import com.clifton.core.logging.message.graylog.pojo.GraylogMessage;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * Converts a GraylogMessage POJO into a LogMessage object
 *
 * @author theodorez
 */
public class GraylogMessageLogMessageConverter implements Converter<GraylogMessage, LogMessage> {

	private JsonHandler<JacksonStrategy> jsonHandler;


	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////
	@Override
	public LogMessage convert(GraylogMessage from) {
		LogMessage message = new LogMessage();
		message.setMessageId(from.getId());
		message.setTimestamp(GraylogUtils.getDateFromGraylogTime(from.getTimestamp()));
		message.setThreadId(from.getThreadId());
		message.setThreadName(from.getThreadName());
		message.setLogLevel(from.getLogLevel());
		message.setLogLevelId(from.getLevel());
		message.setLoggerName(from.getClassName());
		message.setMessage(StringUtils.isEmpty(from.getFullMessage()) ? from.getMessage() : from.getFullMessage());
		message.setCurrentUserName(from.getCurrentUserName());
		message.setStackTrace(from.getStackTrace());

		if (from.getRequestAttributes() != null) {
			message.setRequestAttributeProperties(getPropertyList((Map<String, Object>) getJsonHandler().fromJson(from.getRequestAttributes(), Map.class)));
		}

		if (from.getRequestParameters() != null) {
			message.setRequestParameterProperties(getPropertyList((Map<String, Object>) getJsonHandler().fromJson(from.getRequestParameters(), Map.class)));
		}

		message.setRequestClient(from.getRequestClient());
		message.setRequestUri(from.getRequestUri());

		message.setServer(StringUtils.isEmpty(from.getServer()) ? from.getSource() : from.getServer());
		message.setSessionId(from.getSessionId());

		if (from.getSessionAttributes() != null) {
			message.setSessionAttributeProperties(getPropertyList((Map<String, Object>) getJsonHandler().fromJson(from.getSessionAttributes(), Map.class)));
		}

		message.setApplication(from.getApplication());
		message.setEnvironment(from.getEnvironment());

		message.setLogGroup(from.getLogGroup());
		message.setFileName(from.getFileName());

		return message;
	}


	private List<LogMessageProperty> getPropertyList(Map<String, Object> objectMap) {
		List<LogMessageProperty> properties = new ArrayList<>();
		for (Map.Entry<String, Object> row : objectMap.entrySet()) {
			String rowValue = row.getValue().toString();
			if (!StringUtils.isEmpty(rowValue)) {
				properties.add(new LogMessageProperty(row.getKey(), rowValue));
			}
		}
		return properties;
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public JsonHandler<JacksonStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JacksonStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}
}
