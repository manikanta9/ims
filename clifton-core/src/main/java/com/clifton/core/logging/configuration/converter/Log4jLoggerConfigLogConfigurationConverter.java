package com.clifton.core.logging.configuration.converter;

import com.clifton.core.logging.configuration.LogAppender;
import com.clifton.core.logging.configuration.LogConfiguration;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.config.LoggerConfig;

import java.util.List;
import java.util.stream.Collectors;


/**
 * Converts a Log4j LoggerConfig object into our LogConfiguration object
 *
 * @author theodorez
 */
public class Log4jLoggerConfigLogConfigurationConverter implements Converter<LoggerConfig, LogConfiguration> {

	private Converter<Appender, LogAppender> appenderConverter;


	@Override
	public LogConfiguration convert(LoggerConfig from) {
		return new LogConfiguration((StringUtils.isEmpty(from.getName()) ? LogConfiguration.ROOT_LOGGER : from.getName()), from.getLevel().name(), from.isAdditive(), from.getState().name(), getLogAppenders(from));
	}


	private List<LogAppender> getLogAppenders(LoggerConfig loggerConfig) {
		return CollectionUtils.getStream(loggerConfig.getAppenders().values())
				.map(getAppenderConverter()::convert)
				.collect(Collectors.toList());
	}


	public Converter<Appender, LogAppender> getAppenderConverter() {
		return this.appenderConverter;
	}


	public void setAppenderConverter(Converter<Appender, LogAppender> appenderConverter) {
		this.appenderConverter = appenderConverter;
	}
}
