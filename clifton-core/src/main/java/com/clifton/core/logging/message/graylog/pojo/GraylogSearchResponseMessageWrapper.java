package com.clifton.core.logging.message.graylog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Wrapper for message in search results response
 *
 * @author theodorez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraylogSearchResponseMessageWrapper {

	private GraylogMessage message;

	@JsonProperty("highlight_ranges")
	private Object highlightRanges;

	private String index;

	@JsonProperty("decoration_stats")
	private Object decorationStats;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public GraylogMessage getMessage() {
		return this.message;
	}


	public void setMessage(GraylogMessage message) {
		this.message = message;
	}


	public Object getHighlightRanges() {
		return this.highlightRanges;
	}


	public void setHighlightRanges(Object highlightRanges) {
		this.highlightRanges = highlightRanges;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	public Object getDecorationStats() {
		return this.decorationStats;
	}


	public void setDecorationStats(Object decorationStats) {
		this.decorationStats = decorationStats;
	}
}
