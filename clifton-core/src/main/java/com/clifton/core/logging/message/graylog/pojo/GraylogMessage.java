package com.clifton.core.logging.message.graylog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * POJO for leaf message object
 *
 * @author theodorez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraylogMessage {

	@JsonProperty("session_id")
	private String sessionId;

	@JsonProperty("gl2_source_node")
	private String gl2SourceNode;

	@JsonProperty("request_client")
	private String requestClient;

	@JsonProperty("current_username")
	private String currentUserName;

	@JsonProperty("gl2_remote_port")
	private String gl2RemotePort;

	@JsonProperty("request_parameters")
	private String requestParameters;

	@JsonProperty("request_server")
	private String requestServer;

	private String timestamp;

	private Integer level;

	private String facility;

	@JsonProperty("_id")
	private String id;

	@JsonProperty("log_level")
	private String logLevel;

	@JsonProperty("gl2_source_input")
	private String gl2SourceInput;

	@JsonProperty("class_name")
	private String className;

	@JsonProperty("session_attributes")
	private String sessionAttributes;

	private String application;

	@JsonProperty("full_message")
	private String fullMessage;

	@JsonProperty("gl2_remote_ip")
	private String gl2RemoteIp;

	@JsonProperty("request_attributes")
	private String requestAttributes;

	@JsonProperty("request_uri")
	private String requestUri;

	@JsonProperty("message")
	private String message;

	@JsonProperty("server_fqdn")
	private String serverFqdn;

	private String environment;

	@JsonProperty("source")
	private String source;

	private String server;

	private String[] streams;

	@JsonProperty("StackTrace")
	private String stackTrace;

	@JsonProperty("thread_id")
	private Integer threadId;

	@JsonProperty("thread_name")
	private String threadName;

	@JsonProperty("file")
	private String fileName;

	@JsonProperty("log_group")
	private String logGroup;


	public String getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public String getGl2SourceNode() {
		return this.gl2SourceNode;
	}


	public void setGl2SourceNode(String gl2SourceNode) {
		this.gl2SourceNode = gl2SourceNode;
	}


	public String getRequestClient() {
		return this.requestClient;
	}


	public void setRequestClient(String requestClient) {
		this.requestClient = requestClient;
	}


	public String getCurrentUserName() {
		return this.currentUserName;
	}


	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}


	public String getGl2RemotePort() {
		return this.gl2RemotePort;
	}


	public void setGl2RemotePort(String gl2RemotePort) {
		this.gl2RemotePort = gl2RemotePort;
	}


	public String getRequestParameters() {
		return this.requestParameters;
	}


	public void setRequestParameters(String requestParameters) {
		this.requestParameters = requestParameters;
	}


	public String getRequestServer() {
		return this.requestServer;
	}


	public void setRequestServer(String requestServer) {
		this.requestServer = requestServer;
	}


	public String getTimestamp() {
		return this.timestamp;
	}


	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}


	public Integer getLevel() {
		return this.level;
	}


	public void setLevel(Integer level) {
		this.level = level;
	}


	public String getFacility() {
		return this.facility;
	}


	public void setFacility(String facility) {
		this.facility = facility;
	}


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getLogLevel() {
		return this.logLevel;
	}


	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}


	public String getGl2SourceInput() {
		return this.gl2SourceInput;
	}


	public void setGl2SourceInput(String gl2SourceInput) {
		this.gl2SourceInput = gl2SourceInput;
	}


	public String getClassName() {
		return this.className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public String getSessionAttributes() {
		return this.sessionAttributes;
	}


	public void setSessionAttributes(String sessionAttributes) {
		this.sessionAttributes = sessionAttributes;
	}


	public String getApplication() {
		return this.application;
	}


	public void setApplication(String application) {
		this.application = application;
	}


	public String getFullMessage() {
		return this.fullMessage;
	}


	public void setFullMessage(String fullMessage) {
		this.fullMessage = fullMessage;
	}


	public String getGl2RemoteIp() {
		return this.gl2RemoteIp;
	}


	public void setGl2RemoteIp(String gl2RemoteIp) {
		this.gl2RemoteIp = gl2RemoteIp;
	}


	public String getRequestAttributes() {
		return this.requestAttributes;
	}


	public void setRequestAttributes(String requestAttributes) {
		this.requestAttributes = requestAttributes;
	}


	public String getRequestUri() {
		return this.requestUri;
	}


	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getServerFqdn() {
		return this.serverFqdn;
	}


	public void setServerFqdn(String serverFqdn) {
		this.serverFqdn = serverFqdn;
	}


	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	public String getSource() {
		return this.source;
	}


	public void setSource(String source) {
		this.source = source;
	}


	public String getServer() {
		return this.server;
	}


	public void setServer(String server) {
		this.server = server;
	}


	public String[] getStreams() {
		return this.streams;
	}


	public void setStreams(String[] streams) {
		this.streams = streams;
	}


	public String getStackTrace() {
		return this.stackTrace;
	}


	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}


	public Integer getThreadId() {
		return this.threadId;
	}


	public void setThreadId(Integer threadId) {
		this.threadId = threadId;
	}


	public String getThreadName() {
		return this.threadName;
	}


	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getLogGroup() {
		return this.logGroup;
	}


	public void setLogGroup(String logGroup) {
		this.logGroup = logGroup;
	}
}
