package com.clifton.core.logging.configuration.converter;

import com.clifton.core.logging.configuration.LogAppender;
import com.clifton.core.util.converter.Converter;
import org.apache.logging.log4j.core.Appender;


/**
 * Converts a Log4j Appender object into our LogAppender object
 *
 * @author theodorez
 */
public class Log4jAppenderLogAppenderConverter implements Converter<Appender, LogAppender> {

	@Override
	public LogAppender convert(Appender from) {
		return new LogAppender(from.getName(), from.getState().name(), from.getClass().getCanonicalName());
	}
}
