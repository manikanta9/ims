package com.clifton.core.logging.configuration;

/**
 * The <code>LogAppender</code> represents a generic Log Appender.
 * <p>
 * An Appender can also be thought of as a destination that log messages are sent to.
 *
 * @author theodorez
 */
public class LogAppender {

	/**
	 * The name that will be used to refer to this appender
	 */
	private String name;

	/**
	 * The lifecycle state of the appender
	 */
	private String state;

	/**
	 * The implementation class of this appender
	 */
	private String clazz;


	public LogAppender() {
		//do nothing
	}


	public LogAppender(String name, String state, String clazz) {
		this.name = name;
		this.state = state;
		this.clazz = clazz;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getState() {
		return this.state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getClazz() {
		return this.clazz;
	}


	public void setClazz(String clazz) {
		this.clazz = clazz;
	}
}
