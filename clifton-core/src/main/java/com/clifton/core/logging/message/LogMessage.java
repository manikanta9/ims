package com.clifton.core.logging.message;

import java.util.Date;
import java.util.List;


/**
 * @author theodorez
 */
public class LogMessage {

	private String messageId;

	private Date timestamp;
	private Integer threadId;
	private String threadName;
	private Integer level;
	private String logLevel;
	private Integer logLevelId;
	private String loggerName;
	private String message;
	private String messageShort;
	private String currentUserName;
	private String stackTrace;

	List<LogMessageProperty> requestAttributeProperties;

	private String requestClient;

	List<LogMessageProperty> requestParameterProperties;

	private String requestUri;
	private String server;

	private List<LogMessageProperty> sessionAttributeProperties;
	private String sessionId;

	private String application;
	private String environment;

	private String index;

	private String logGroup;

	private String fileName;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getStackTrace() {
		return this.stackTrace;
	}


	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}


	public Date getTimestamp() {
		return this.timestamp;
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	public Integer getThreadId() {
		return this.threadId;
	}


	public void setThreadId(Integer threadId) {
		this.threadId = threadId;
	}


	public String getThreadName() {
		return this.threadName;
	}


	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}


	public String getLogLevel() {
		return this.logLevel;
	}


	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}


	public String getLoggerName() {
		return this.loggerName;
	}


	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getCurrentUserName() {
		return this.currentUserName;
	}


	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}


	public String getRequestClient() {
		return this.requestClient;
	}


	public void setRequestClient(String requestClient) {
		this.requestClient = requestClient;
	}


	public String getServer() {
		return this.server;
	}


	public void setServer(String server) {
		this.server = server;
	}


	public String getRequestUri() {
		return this.requestUri;
	}


	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}


	public String getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public List<LogMessageProperty> getRequestAttributeProperties() {
		return this.requestAttributeProperties;
	}


	public void setRequestAttributeProperties(List<LogMessageProperty> requestAttributeProperties) {
		this.requestAttributeProperties = requestAttributeProperties;
	}


	public List<LogMessageProperty> getRequestParameterProperties() {
		return this.requestParameterProperties;
	}


	public void setRequestParameterProperties(List<LogMessageProperty> requestParameterProperties) {
		this.requestParameterProperties = requestParameterProperties;
	}


	public List<LogMessageProperty> getSessionAttributeProperties() {
		return this.sessionAttributeProperties;
	}


	public void setSessionAttributeProperties(List<LogMessageProperty> sessionAttributeProperties) {
		this.sessionAttributeProperties = sessionAttributeProperties;
	}


	public String getMessageId() {
		return this.messageId;
	}


	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}


	public Integer getLevel() {
		return this.level;
	}


	public void setLevel(Integer level) {
		this.level = level;
	}


	public String getApplication() {
		return this.application;
	}


	public void setApplication(String application) {
		this.application = application;
	}


	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	public Integer getLogLevelId() {
		return this.logLevelId;
	}


	public void setLogLevelId(Integer logLevelId) {
		this.logLevelId = logLevelId;
	}


	public String getLogGroup() {
		return this.logGroup;
	}


	public void setLogGroup(String logGroup) {
		this.logGroup = logGroup;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getMessageShort() {
		return this.messageShort;
	}


	public void setMessageShort(String messageShort) {
		this.messageShort = messageShort;
	}
}
