package com.clifton.core.logging;

/**
 * The {@link LogLevels} enumerator lists all known log levels. These can be used to set log levels for use in {@link LogUtils} methods such as {@link
 * LogUtils#log(Class, LogLevels, String)}.
 *
 * @author MikeH
 */
public enum LogLevels {
	FATAL,
	ERROR,
	WARN,
	INFO,
	DEBUG,
	TRACE
}
