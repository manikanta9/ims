package com.clifton.core.logging.configuration.converter;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.logging.configuration.LogConfiguration;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.config.LoggerConfig;


/**
 * Converts our LogConfiguration object into a Log4j LoggerConfig object
 *
 * @author theodorez
 */
public class LogConfigurationLog4JLoggerConfigConverter implements Converter<LogConfiguration, LoggerConfig> {


	@Override
	public LoggerConfig convert(LogConfiguration from) {
		LoggerConfig originalConfig = LogUtils.getLoggerConfiguration().getLoggerConfig(from.getName());

		LoggerConfig newConfig = new LoggerConfig(from.getName(), Level.valueOf(from.getLevel()), from.isAdditive());

		for (Appender appender : CollectionUtils.getIterable(originalConfig.getAppenders().values())) {
			newConfig.addAppender(appender, null, null);
		}
		return newConfig;
	}
}
