package com.clifton.core.logging.message.graylog;

import com.clifton.core.logging.message.graylog.converter.LogMessageFieldNameGraylogFieldNameConverter;
import com.clifton.core.util.date.DateUtils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;


/**
 * @author theodorez
 */
public class GraylogUtils {

	private static final String SPECIAL_CHARACTERS = "([+\\-!\\(\\){}\\[\\]^\"~*?:\\\\]|[&\\|]{2})";

	private static final String TIMESTAMP_FORMAT_STRING = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	private static final ZoneId GRAYLOG_ZONE_ID = ZoneId.of("Z");

	private static final LogMessageFieldNameGraylogFieldNameConverter FIELD_NAME_CONVERTER = new LogMessageFieldNameGraylogFieldNameConverter();


	/**
	 * Converts our field name into the graylog field name
	 */
	public static String getGraylogFieldFromOurField(String ourFieldName) {
		return FIELD_NAME_CONVERTER.convert(ourFieldName);
	}


	/**
	 * Escapes all characters that Graylog considers special
	 */
	public static String escape(String value) {
		return value.replaceAll(SPECIAL_CHARACTERS, "\\\\$1");
	}


	/**
	 * Converts the given string that is formatted in the Zulu time format (UTC) into a date object
	 */
	public static Date getDateFromGraylogTime(String time) {
		try {
			//Gets the time in UTC
			Date date = getTimeFormat().parse(time);
			//Returns the date in the local time zone
			return DateUtils.asUtilDate(date);
		}
		catch (ParseException pe) {
			throw new RuntimeException("Could not parse time: " + time + " with format " + TIMESTAMP_FORMAT_STRING, pe);
		}
	}


	/**
	 * Gets a string representation of the given date in the format that Graylog expects
	 */
	public static String getDateStringInGraylogTime(Date date) {
		Date convertedDate = DateUtils.asUtilDate(date, GRAYLOG_ZONE_ID);
		return getTimeFormat().format(convertedDate);
	}


	private static SimpleDateFormat getTimeFormat() {
		SimpleDateFormat format = new SimpleDateFormat(TIMESTAMP_FORMAT_STRING, Locale.US);
		format.setTimeZone(TimeZone.getTimeZone("UTC"));
		return format;
	}
}
