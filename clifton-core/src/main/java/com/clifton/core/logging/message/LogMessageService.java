package com.clifton.core.logging.message;

import com.clifton.core.logging.message.search.LogMessageSearchForm;
import com.clifton.core.util.dataaccess.PagingArrayList;


/**
 * @author theodorez
 */
public interface LogMessageService {

	public LogMessage getLogMessage(String identifier);


	public PagingArrayList<LogMessage> getLogMessageList(LogMessageSearchForm searchForm);
}
