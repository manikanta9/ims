package com.clifton.core.logging.message.graylog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * POJO for index information
 *
 * @author theodorez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraylogIndicies {

	@JsonProperty("took_ms")
	private String tookMs;

	@JsonProperty("index_name")
	private String indexName;

	@JsonProperty("calculated_at")
	private String calculatedAt;

	private String end;

	private String begin;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public String getTookMs() {
		return this.tookMs;
	}


	public void setTookMs(String tookMs) {
		this.tookMs = tookMs;
	}


	public String getIndexName() {
		return this.indexName;
	}


	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}


	public String getCalculatedAt() {
		return this.calculatedAt;
	}


	public void setCalculatedAt(String calculatedAt) {
		this.calculatedAt = calculatedAt;
	}


	public String getEnd() {
		return this.end;
	}


	public void setEnd(String end) {
		this.end = end;
	}


	public String getBegin() {
		return this.begin;
	}


	public void setBegin(String begin) {
		this.begin = begin;
	}
}
