package com.clifton.core.logging.message.graylog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * POJO for search result response
 *
 * @author theodorez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraylogSearchResponse {

	private String to;

	private Integer time;

	@JsonProperty("used_indices")
	private GraylogIndicies[] usedIndices;

	@JsonProperty("decoration_stats")
	private Object decorationStats;

	private String query;

	private String from;

	@JsonProperty("built_query")
	private String builtQuery;

	@JsonProperty("messages")
	private GraylogSearchResponseMessageWrapper[] messageWrappers;

	@JsonProperty("total_results")
	private Integer totalResults;

	private String[] fields;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public String getTo() {
		return this.to;
	}


	public void setTo(String to) {
		this.to = to;
	}


	public Integer getTime() {
		return this.time;
	}


	public void setTime(Integer time) {
		this.time = time;
	}


	public GraylogIndicies[] getUsedIndices() {
		return this.usedIndices;
	}


	public void setUsedIndices(GraylogIndicies[] usedIndices) {
		this.usedIndices = usedIndices;
	}


	public Object getDecorationStats() {
		return this.decorationStats;
	}


	public void setDecorationStats(Object decorationStats) {
		this.decorationStats = decorationStats;
	}


	public String getQuery() {
		return this.query;
	}


	public void setQuery(String query) {
		this.query = query;
	}


	public String getFrom() {
		return this.from;
	}


	public void setFrom(String from) {
		this.from = from;
	}


	public String getBuiltQuery() {
		return this.builtQuery;
	}


	public void setBuiltQuery(String builtQuery) {
		this.builtQuery = builtQuery;
	}


	public GraylogSearchResponseMessageWrapper[] getMessageWrappers() {
		return this.messageWrappers;
	}


	public void setMessageWrappers(GraylogSearchResponseMessageWrapper[] messageWrappers) {
		this.messageWrappers = messageWrappers;
	}


	public Integer getTotalResults() {
		return this.totalResults;
	}


	public void setTotalResults(Integer totalResults) {
		this.totalResults = totalResults;
	}


	public String[] getFields() {
		return this.fields;
	}


	public void setFields(String[] fields) {
		this.fields = fields;
	}
}
