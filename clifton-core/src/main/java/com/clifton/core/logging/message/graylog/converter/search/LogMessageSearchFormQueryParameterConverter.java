package com.clifton.core.logging.message.graylog.converter.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.logging.message.graylog.GraylogFieldNames;
import com.clifton.core.logging.message.graylog.GraylogUtils;
import com.clifton.core.logging.message.search.LogMessageSearchForm;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Converts our search form into a Graylog search query string
 *
 * @author theodorez
 */
public class LogMessageSearchFormQueryParameterConverter implements Converter<LogMessageSearchForm, String> {

	private Map<String, Converter<List<SearchRestriction>, String>> searchStringConverterMap;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	@Override
	public String convert(LogMessageSearchForm from) {
		StringBuilder params = new StringBuilder();
		params.append("limit=").append(from.getLimit() > 10000 ? 10000 : from.getLimit());
		params.append("&offset=").append(from.getStart());
		params.append("&sort=").append(getGraylogSortString(from));
		params.append("&").append(getTimestampQueryString(from));
		params.append("&query=");

		List<String> queryParams = new ArrayList<>();

		Field[] fields = ClassUtils.getClassFields(from.getClass(), true, true);
		for (Field field : fields) {
			SearchField searchField = AnnotationUtils.findAnnotation(field, SearchField.class);
			if (searchField != null) {
				Class<?> type = field.getType();
				String searchFieldName = field.getName();
				Object value;
				try {
					field.setAccessible(true);
					value = field.get(from);
				}
				catch (Exception e) {
					throw new RuntimeException("Cannot read value of field: " + field.getName(), e);
				}

				if (value != null) {
					queryParams.add(processSearchField(CollectionUtils.createList(new SearchRestriction(searchFieldName, searchField.comparisonConditions().length == 0 ? ComparisonConditions.EQUALS : searchField.comparisonConditions()[0], value)), type));
				}
				else {
					List<SearchRestriction> restrictions = SearchUtils.getSearchRestrictionsForField(from.getRestrictionList(), searchFieldName);
					if (!CollectionUtils.isEmpty(restrictions)) {
						queryParams.add(processSearchField(restrictions, type));
					}
				}
			}
		}
		//if no fields were set, the query should be *
		if (queryParams.isEmpty()) {
			queryParams.add("*");
		}

		params.append(StringUtils.collectionToDelimitedString(queryParams, " AND "));
		params.append("&fields=").append(StringUtils.collectionToDelimitedString(GraylogFieldNames.getGraylogFieldNames(), ","));
		return params.toString();
	}


	private String processSearchField(List<SearchRestriction> restrictions, Class<?> type) {
		Converter<List<SearchRestriction>, String> converter = getSearchStringConverterMap().get(type.getName());
		ValidationUtils.assertNotNull(converter, "No converter registered for field type: " + type.getName());
		return converter.convert(restrictions);
	}


	private String getTimestampQueryString(LogMessageSearchForm from) {
		String fromDateString = null;
		String toDateString = null;

		if (from.getStartDate() != null || from.getEndDate() != null) {
			ValidationUtils.assertFalse(from.getStartDate() == null || from.getEndDate() == null, "Both Start Date and End Date MUST be specified.");
			fromDateString = GraylogUtils.getDateStringInGraylogTime(from.getStartDate());
			//Set the time to 11:59:59 on the end date
			toDateString = GraylogUtils.getDateStringInGraylogTime(DateUtils.addMinutes(from.getEndDate(), 1439));
			from.setStartDate(null);
			from.setEndDate(null);
		}
		else {
			List<SearchRestriction> timestampRestrictions = SearchUtils.getSearchRestrictionsForField(from.getRestrictionList(), "timestamp");
			for (SearchRestriction restriction : CollectionUtils.getIterable(timestampRestrictions)) {
				Date restrictionDate = DateUtils.clearTime(DateUtils.toDate((String) restriction.getValue()));
				switch (restriction.getComparison()) {
					case EQUALS:
						fromDateString = GraylogUtils.getDateStringInGraylogTime(restrictionDate);
						//Set end to 2359hrs
						toDateString = GraylogUtils.getDateStringInGraylogTime(DateUtils.addMinutes(restrictionDate, 1439));
						break;
					case LESS_THAN:
						toDateString = GraylogUtils.getDateStringInGraylogTime(restrictionDate);
						break;
					case GREATER_THAN:
						fromDateString = GraylogUtils.getDateStringInGraylogTime(restrictionDate);
						break;
					default:
						throw new ValidationException("Conversion not currently supported");
				}
			}
			ValidationUtils.assertFalse(StringUtils.isEmpty(fromDateString) || StringUtils.isEmpty(toDateString), "Either the ON or AFTER and BEFORE date MUST be set.");
		}

		return "from=" + fromDateString + "&to=" + toDateString;
	}


	private String getGraylogSortString(LogMessageSearchForm from) {
		List<OrderByField> orderFields = SearchUtils.getOrderByFieldList(from.getOrderBy());
		OrderByField orderByField = CollectionUtils.getFirstElementStrict(orderFields);
		return GraylogUtils.getGraylogFieldFromOurField(orderByField.getName()) + ":" + orderByField.getDirection();
	}

//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public Map<String, Converter<List<SearchRestriction>, String>> getSearchStringConverterMap() {
		return this.searchStringConverterMap;
	}


	public void setSearchStringConverterMap(Map<String, Converter<List<SearchRestriction>, String>> searchStringConverterMap) {
		this.searchStringConverterMap = searchStringConverterMap;
	}
}
