package com.clifton.core.logging.message.graylog.converter;

import com.clifton.core.logging.message.graylog.GraylogFieldNames;
import com.clifton.core.util.converter.Converter;


/**
 * Converts the Field Name from our LogMessage object into the Field Name that Graylog uses
 *
 * @author theodorez
 */
public class LogMessageFieldNameGraylogFieldNameConverter implements Converter<String, String> {

	@Override
	public String convert(String from) {
		switch (from) {
			case "application":
				return GraylogFieldNames.APPLICATION.getGraylogName();
			case "applicationBeginsWith":
				return GraylogFieldNames.APPLICATION.getGraylogName();
			case "currentUserName":
				return GraylogFieldNames.CURRENT_USER_NAME.getGraylogName();
			case "environment":
				return GraylogFieldNames.ENVIRONMENT.getGraylogName();
			case "message":
				return GraylogFieldNames.MESSAGE.getGraylogName();
			case "messageShort":
				return GraylogFieldNames.MESSAGE_SHORT.getGraylogName();
			case "messageId":
				return GraylogFieldNames.ID.getGraylogName();
			case "id":
				return GraylogFieldNames.ID.getGraylogName();
			case "logLevelId":
				return GraylogFieldNames.LEVEL.getGraylogName();
			case "level":
				return GraylogFieldNames.LEVEL.getGraylogName();
			case "logGroup":
				return GraylogFieldNames.LOG_GROUP.getGraylogName();
			case "logLevel":
				return GraylogFieldNames.LOG_LEVEL.getGraylogName();
			case "logLevelBeginsWith":
				return GraylogFieldNames.LOG_LEVEL.getGraylogName();
			case "loggerName":
				return GraylogFieldNames.CLASS_NAME.getGraylogName();
			case "requestClient":
				return GraylogFieldNames.REQUEST_CLIENT.getGraylogName();
			case "requestServer":
				return GraylogFieldNames.SERVER.getGraylogName();
			case "requestUri":
				return GraylogFieldNames.REQUEST_URI.getGraylogName();
			case "server":
				return GraylogFieldNames.SERVER.getGraylogName();
			case "threadId":
				return GraylogFieldNames.THREAD_ID.getGraylogName();
			case "threadName":
				return GraylogFieldNames.THREAD_NAME.getGraylogName();
			case "timestamp":
				return GraylogFieldNames.TIMESTAMP.getGraylogName();
			case "sessionId":
				return GraylogFieldNames.SESSION_ID.getGraylogName();
			case "stackTrace":
				return GraylogFieldNames.STACK_TRACE.getGraylogName();
			case "startDate":
				return GraylogFieldNames.START_DATE.getGraylogName();
			case "endDate":
				return GraylogFieldNames.END_DATE.getGraylogName();
			default:
				return from;
		}
	}
}
