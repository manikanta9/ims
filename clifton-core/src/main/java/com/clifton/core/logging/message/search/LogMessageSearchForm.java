package com.clifton.core.logging.message.search;

import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;

import java.util.Date;


/**
 * Search form for querying for log messages.
 * <p>
 * NOTE: Properties not annotated with @SearchField are handled as special fields
 *
 * @author theodorez
 */
@NonPersistentObject
@SearchForm(hasOrmDtoClass = false)
public class LogMessageSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	private Date timestamp;

	@SearchField
	private String application;

	@SearchField(comparisonConditions = ComparisonConditions.BEGINS_WITH)
	private String applicationBeginsWith;

	@SearchField
	private String environment;

	@SearchField
	private String currentUserName;

	@SearchField(comparisonConditions = ComparisonConditions.EQUALS)
	private String id;

	@SearchField
	private String logGroup;

	@SearchField
	private Integer logLevelId;

	@SearchField
	private String logLevel;

	@SearchField(comparisonConditions = ComparisonConditions.BEGINS_WITH)
	private String logLevelBeginsWith;

	@SearchField
	private String loggerName;

	@SearchField
	private String message;

	@SearchField
	private String requestClient;

	@SearchField
	private String requestUri;

	@SearchField
	private String requestServer;

	@SearchField
	private Integer threadId;

	@SearchField
	private String threadName;

	@SearchField
	private String sessionId;

	@SearchField
	private String stackTrace;

	@SearchField
	private String server;

	////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getApplication() {
		return this.application;
	}


	public void setApplication(String application) {
		this.application = application;
	}


	public String getApplicationBeginsWith() {
		return this.applicationBeginsWith;
	}


	public void setApplicationBeginsWith(String applicationBeginsWith) {
		this.applicationBeginsWith = applicationBeginsWith;
	}


	public String getEnvironment() {
		return this.environment;
	}


	public void setEnvironment(String environment) {
		this.environment = environment;
	}


	public String getCurrentUserName() {
		return this.currentUserName;
	}


	public void setCurrentUserName(String currentUserName) {
		this.currentUserName = currentUserName;
	}


	public Date getTimestamp() {
		return this.timestamp;
	}


	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}


	public Integer getLogLevelId() {
		return this.logLevelId;
	}


	public void setLogLevelId(Integer logLevelId) {
		this.logLevelId = logLevelId;
	}


	public String getLogLevel() {
		return this.logLevel;
	}


	public void setLogLevel(String logLevel) {
		this.logLevel = logLevel;
	}


	public String getLoggerName() {
		return this.loggerName;
	}


	public void setLoggerName(String loggerName) {
		this.loggerName = loggerName;
	}


	public String getMessage() {
		return this.message;
	}


	public void setMessage(String message) {
		this.message = message;
	}


	public String getRequestClient() {
		return this.requestClient;
	}


	public void setRequestClient(String requestClient) {
		this.requestClient = requestClient;
	}


	public String getRequestUri() {
		return this.requestUri;
	}


	public void setRequestUri(String requestUri) {
		this.requestUri = requestUri;
	}


	public String getRequestServer() {
		return this.requestServer;
	}


	public void setRequestServer(String requestServer) {
		this.requestServer = requestServer;
	}


	public Integer getThreadId() {
		return this.threadId;
	}


	public void setThreadId(Integer threadId) {
		this.threadId = threadId;
	}


	public String getThreadName() {
		return this.threadName;
	}


	public void setThreadName(String threadName) {
		this.threadName = threadName;
	}


	public String getSessionId() {
		return this.sessionId;
	}


	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}


	public String getStackTrace() {
		return this.stackTrace;
	}


	public void setStackTrace(String stackTrace) {
		this.stackTrace = stackTrace;
	}


	public String getLogGroup() {
		return this.logGroup;
	}


	public void setLogGroup(String logGroup) {
		this.logGroup = logGroup;
	}


	public String getServer() {
		return this.server;
	}


	public void setServer(String server) {
		this.server = server;
	}


	public String getLogLevelBeginsWith() {
		return this.logLevelBeginsWith;
	}


	public void setLogLevelBeginsWith(String logLevelBeginsWith) {
		this.logLevelBeginsWith = logLevelBeginsWith;
	}
}
