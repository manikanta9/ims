package com.clifton.core.logging.message.graylog.pojo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;


/**
 * Wrapper object for the response when retrieving a single message by ID
 *
 * @author theodorez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class GraylogMessageDetailResponseMessageWrapper {

	@JsonProperty("fields")
	private GraylogMessage message;

	private String id;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public GraylogMessage getMessage() {
		return this.message;
	}


	public void setMessage(GraylogMessage message) {
		this.message = message;
	}


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}
}
