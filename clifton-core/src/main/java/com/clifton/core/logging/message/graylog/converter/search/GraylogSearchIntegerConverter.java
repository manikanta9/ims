package com.clifton.core.logging.message.graylog.converter.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.message.graylog.GraylogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;

import java.util.ArrayList;
import java.util.List;


/**
 * Converts a search restriction list for a given field into the String representation of the appropriate lucene query
 *
 * @author theodorez
 */
public class GraylogSearchIntegerConverter implements Converter<List<SearchRestriction>, String> {

	@Override
	public String convert(List<SearchRestriction> from) {
		StringBuilder fieldNameSb = new StringBuilder();

		List<String> comparisonItems = new ArrayList<>();

		for (SearchRestriction restriction : CollectionUtils.getIterable(from)) {
			if (StringUtils.isEmpty(fieldNameSb)) {
				fieldNameSb.append(GraylogUtils.getGraylogFieldFromOurField(restriction.getField()));
			}
			ComparisonConditions comparison = restriction.getComparison();
			String value = String.valueOf(restriction.getValue());

			if (comparison == ComparisonConditions.EQUALS) {
				//Syntax for equals is field:value
				comparisonItems.add(value);
			}
			else if (comparison == ComparisonConditions.NOT_EQUALS) {
				fieldNameSb.insert(0, "NOT ");
				comparisonItems.add(value);
			}
			else {
				// EQUALS is the standard syntax (field : value)
				// Handles single bound operations <, >, <=, >=
				comparisonItems.add(comparison.getComparisonExpression() + value);
			}
		}
		return fieldNameSb.append(":(").append(StringUtils.collectionToDelimitedString(comparisonItems, " AND ")).append(")").toString();
	}
}
