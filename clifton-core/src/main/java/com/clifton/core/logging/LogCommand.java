package com.clifton.core.logging;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.Arrays;
import java.util.function.Supplier;


/**
 * @author theodorez
 */
public class LogCommand {

	private static final String MESSAGE_ITEM_DELIMITER = " ";

	private Supplier<String> messageSupplier;

	private Class<?> loggingClass;

	private LogLevels logLevel;

	private Throwable exception;

	private HttpServletRequest request;


	private LogCommand(Class<?> loggingClass, Throwable exception, Supplier<String> messageSupplier) {
		ValidationUtils.assertNotNull(loggingClass, "A log command must have a class specified");
		this.loggingClass = loggingClass;
		this.exception = exception;
		this.messageSupplier = messageSupplier;
	}


	/**
	 * Creates a log command with a message composed of the messageItems. The logging class will be the previous entry in the stack.
	 */
	public static LogCommand ofMessage(Class<?> clazz, Object... messageItems) {
		return ofThrowableAndMessage(clazz, null, messageItems);
	}


	/**
	 * Creates a logCommand with a given Message Supplier that will be called after the log level is verified and used to create the log message
	 */
	public static LogCommand ofMessageSupplier(Class<?> clazz, Supplier<String> messageSupplier) {
		return ofThrowableAndMessage(clazz, null, messageSupplier);
	}


	/**
	 * Creates a LogCommand with the given exception. The logging class will be the previous entry in the stack.
	 */
	public static LogCommand ofThrowable(Class<?> clazz, Throwable exception) {
		return ofThrowableAndMessage(clazz, exception);
	}


	/**
	 * Creates a LogCommand with the given exception and with a message composed of the given message items.
	 */
	public static LogCommand ofThrowableAndMessage(Class<?> clazz, Throwable exception, Object... messageItems) {
		ValidationUtils.assertTrue(exception != null || messageItems != null, "Cannot create a log entry without a message or exception.");
		return ofThrowableAndMessage(clazz, exception, () ->
				messageItems == null ? StringUtils.EMPTY_STRING : StringUtils.collectionToDelimitedString(Arrays.asList(messageItems), MESSAGE_ITEM_DELIMITER)
		);
	}


	/**
	 * Creates a logCommand with a given exception and Message Supplier that will be called after the log level is verified and used to create the log message
	 */
	public static LogCommand ofThrowableAndMessage(Class<?> clazz, Throwable exception, Supplier<String> messageSupplier) {
		ValidationUtils.assertTrue(exception != null || messageSupplier != null, "Cannot create a log entry without a message or exception.");
		return new LogCommand(clazz, exception, messageSupplier);
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the value from the supplier
	 */
	public String getMessage() {
		return this.messageSupplier.get();
	}


	public Class<?> getLogClass() {
		return this.loggingClass;
	}


	public LogLevels getLogLevel() {
		return this.logLevel;
	}


	public Throwable getThrowable() {
		return this.exception;
	}


	public HttpServletRequest getRequest() {
		return this.request;
	}


	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public LogCommand withMessage(String message) {
		return withMessageSupplier(() -> message);
	}


	public LogCommand withMessageSupplier(Supplier<String> messageSupplier) {
		this.messageSupplier = messageSupplier;
		return this;
	}


	public LogCommand withThrowable(Throwable e) {
		this.exception = e;
		return this;
	}


	public LogCommand withRequest(HttpServletRequest request) {
		this.request = request;
		return this;
	}


	public LogCommand withLoggingClass(Class<?> loggingClass) {
		this.loggingClass = loggingClass;
		return this;
	}


	public LogCommand withLogLevel(LogLevels logLevel) {
		this.logLevel = logLevel;
		return this;
	}
}
