package com.clifton.core.logging.configuration;

import java.util.List;


/**
 * @author theodorez
 */
public interface LogConfigurationService {


	public LogConfiguration saveLogConfigurationLogAppenderLink(String loggerName, String appenderName);


	public LogConfiguration deleteLogConfigurationLogAppenderLink(String loggerName, String appenderName);


	public LogAppender getLogAppender(String name);


	public List<LogAppender> getLogAppenderList();


	public List<LogAppender> getLogAppenderListForLogConfiguration(String loggerName);


	public LogConfiguration getLogConfiguration(String name);


	public LogConfiguration saveLogConfiguration(LogConfiguration configuration);


	public LogConfiguration deleteLogConfiguration(String name);


	public List<LogConfiguration> getLogConfigurationList();
}
