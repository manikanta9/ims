package com.clifton.core.logging.message.graylog;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Returns the appropriate graylog field name
 *
 * @author theodorez
 */
public enum GraylogFieldNames {

	APPLICATION("application"),
	CURRENT_USER_NAME("current_username"),
	ENVIRONMENT("environment"),
	MESSAGE("full_message"),
	MESSAGE_SHORT("message"),
	ID("_id"),
	LEVEL("level"),
	LOG_GROUP("log_group"),
	LOG_LEVEL("log_level"),
	CLASS_NAME("class_name"),
	REQUEST_CLIENT("request_client"),
	REQUEST_URI("request_uri"),
	SERVER("server"),
	THREAD_ID("thread_id"),
	THREAD_NAME("thread_name"),
	TIMESTAMP("timestamp"),
	SESSION_ID("session_id"),
	SOURCE("source"),
	STACK_TRACE("StackTrace"),
	START_DATE("from"),
	END_DATE("to");

	private String graylogName;


	GraylogFieldNames(String graylogName) {
		this.graylogName = graylogName;
	}


	public String getGraylogName() {
		return this.graylogName;
	}


	public static List<String> getGraylogFieldNames() {
		return Arrays.stream(GraylogFieldNames.values()).map(GraylogFieldNames::getGraylogName).collect(Collectors.toList());
	}
}
