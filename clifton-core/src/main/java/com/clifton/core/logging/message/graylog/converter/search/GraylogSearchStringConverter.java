package com.clifton.core.logging.message.graylog.converter.search;

import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.logging.message.graylog.GraylogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * @author theodorez
 */
public class GraylogSearchStringConverter implements Converter<List<SearchRestriction>, String> {

	@Override
	public String convert(List<SearchRestriction> from) {
		//Can only have one restriction in a String field
		SearchRestriction restriction = CollectionUtils.getFirstElementStrict(from);
		String fieldName = GraylogUtils.getGraylogFieldFromOurField(restriction.getField());
		Object value = restriction.getValue();
		switch (restriction.getComparison()) {
			case BEGINS_WITH:
				return fieldName + ":" + GraylogUtils.escape(String.valueOf(value)) + "*";
			case EQUALS:
				return fieldName + ":\"" + GraylogUtils.escape(String.valueOf(value)) + "\"";
			case LIKE:
				return fieldName + ":" + GraylogUtils.escape(String.valueOf(value)) + "*";
			case IN:
				ValidationUtils.assertTrue(value instanceof List, "Cannot perform an IN if the value is not a list");
				return fieldName + ":(" + StringUtils.collectionToDelimitedString((ArrayList<String>) value, " ") + ")";
			default:
				return "";
		}
	}
}
