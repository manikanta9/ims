package com.clifton.core.logging.configuration;

import java.util.List;


/**
 * Defines the basic attributes of a Log Configuration.
 *
 * @author theodorez
 */
public class LogConfiguration {

	/**
	 * Uses the following name for the root logger (instead of a blank name).  Prefix with a space so that it's first alphabetically.
	 */
	public static final String ROOT_LOGGER = " ROOT";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * The name of the class/package the logger is targeting
	 */
	private String name;

	/**
	 * The level of message that will be output
	 */
	private String level;

	/**
	 * Denotes whether to forward any message handled by this logger to the logger's parent
	 * I.e. if you log a message from com.clifton.core.logging.configuration.LogConfigurationServiceImpl AND the logger is additive,
	 * the message will also be forwarded to the logger at com.clifton
	 */
	private boolean additive;

	/**
	 * The lifecycle state of the logger
	 */
	private String state;

	/**
	 * The <code>LogAppender</code>'s that this logger will send messages to
	 */
	private List<LogAppender> appenders;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public LogConfiguration() {
		//do nothing
	}


	public LogConfiguration(String name, String level, boolean additive, String state, List<LogAppender> appenders) {
		this.name = name;
		this.level = level;
		this.additive = additive;
		this.state = state;
		this.appenders = appenders;
	}


	public List<LogAppender> getAppenders() {
		return this.appenders;
	}


	public void setAppenders(List<LogAppender> appenders) {
		this.appenders = appenders;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLevel() {
		return this.level;
	}


	public void setLevel(String level) {
		this.level = level;
	}


	public boolean isAdditive() {
		return this.additive;
	}


	public void setAdditive(boolean additive) {
		this.additive = additive;
	}


	public String getState() {
		return this.state;
	}


	public void setState(String state) {
		this.state = state;
	}
}
