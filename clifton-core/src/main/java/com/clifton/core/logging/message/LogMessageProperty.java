package com.clifton.core.logging.message;

/**
 * @author theodorez
 */
public class LogMessageProperty {

	private String propertyName;
	private String propertyValue;


	public LogMessageProperty(String propertyName, String propertyValue) {
		this.propertyName = propertyName;
		this.propertyValue = propertyValue;
	}


	public String getPropertyName() {
		return this.propertyName;
	}


	public void setPropertyName(String propertyName) {
		this.propertyName = propertyName;
	}


	public String getPropertyValue() {
		return this.propertyValue;
	}


	public void setPropertyValue(String propertyValue) {
		this.propertyValue = propertyValue;
	}
}
