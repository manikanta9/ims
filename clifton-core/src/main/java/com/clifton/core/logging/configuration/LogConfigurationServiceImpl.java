package com.clifton.core.logging.configuration;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationException;
import org.apache.logging.log4j.core.Appender;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author theodorez
 */
@Service("logConfigurationService")
public class LogConfigurationServiceImpl implements LogConfigurationService {

	private Converter<Appender, LogAppender> appenderConverter;

	private Converter<LoggerConfig, LogConfiguration> configurationConverter;

	private Converter<LogConfiguration, LoggerConfig> logConfigurationConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Configures the specified Logger to output to the specified appender
	 */
	@SecureMethod(securityResource = "Logging", permissions = SecurityPermission.PERMISSION_WRITE)
	@Override
	public LogConfiguration saveLogConfigurationLogAppenderLink(String loggerName, String appenderName) {
		LogUtils.getLoggerConfiguration().addLoggerAppender(LogUtils.getLoggerContext().getLogger(loggerName), getAppenderByName(appenderName));
		return getLogConfiguration(loggerName);
	}


	@SecureMethod(securityResource = "Logging", permissions = SecurityPermission.PERMISSION_DELETE)
	@Override
	public LogConfiguration deleteLogConfigurationLogAppenderLink(String loggerName, String appenderName) {
		LogUtils.getLoggerConfiguration().getLoggerConfig(loggerName).removeAppender(appenderName);
		LogUtils.getLoggerContext().updateLoggers();
		return getLogConfiguration(loggerName);
	}


	@SecureMethod(securityResource = "Logging")
	@Override
	public LogAppender getLogAppender(String name) {
		return getAppenderConverter().convert(getAppenderByName(name));
	}


	/**
	 * Gets a list of all available appenders
	 */
	@SecureMethod(securityResource = "Logging")
	@Override
	public List<LogAppender> getLogAppenderList() {
		List<LogAppender> appenders = new ArrayList<>();
		LogUtils.getLoggerConfiguration().getAppenders().values().forEach(a -> appenders.add(getAppenderConverter().convert(a)));
		return appenders;
	}


	/**
	 * Gets all appenders that a given logger is outputting to
	 */
	@SecureMethod(securityResource = "Logging")
	@Override
	public List<LogAppender> getLogAppenderListForLogConfiguration(String loggerName) {
		LoggerConfig config = getLoggerConfigByName(loggerName);
		List<LogAppender> appenders = new ArrayList<>();
		CollectionUtils.getStream(config.getAppenders().values()).forEach(a -> appenders.add(getAppenderConverter().convert(a)));
		return appenders;
	}


	@SecureMethod(securityResource = "Logging")
	@Override
	public LogConfiguration getLogConfiguration(String name) {
		LogUtils.info(getClass(), "Log Configuration Name: " + name);
		return getConfigurationConverter().convert(LogConfiguration.ROOT_LOGGER.equals(name) ? LogUtils.getLoggerConfiguration().getRootLogger() : getLoggerConfigByName(name));
	}


	/**
	 * Adds the given logger configuration to the logging context
	 */
	@SecureMethod(securityResource = "Logging", permissions = SecurityPermission.PERMISSION_WRITE)
	@Override
	public LogConfiguration saveLogConfiguration(LogConfiguration configuration) {
		LoggerConfig loggerConfig = getLogConfigurationConverter().convert(configuration);
		LogUtils.getLoggerConfiguration().removeLogger(configuration.getName());
		LogUtils.getLoggerConfiguration().addLogger(configuration.getName(), loggerConfig);
		LogUtils.getLoggerContext().updateLoggers();
		return getLogConfiguration(configuration.getName());
	}


	@SecureMethod(securityResource = "Logging", permissions = SecurityPermission.PERMISSION_DELETE)
	@Override
	public LogConfiguration deleteLogConfiguration(String name) {
		if (LogConfiguration.ROOT_LOGGER.equals(name)) {
			throw new ValidationException("Cannot delete the Root logger");
		}
		LoggerContext ctx = LogUtils.getLoggerContext();
		Configuration config = LogUtils.getLoggerConfiguration();
		LogConfiguration originalConfiguration = getLogConfiguration(name);
		config.removeLogger(name);
		ctx.updateLoggers();
		return originalConfiguration;
	}


	/**
	 * Gets a list of all currently configured loggers
	 */
	@SecureMethod(securityResource = "Logging")
	@Override
	public List<LogConfiguration> getLogConfigurationList() {
		return CollectionUtils.getStream(LogUtils.getLoggerConfiguration().getLoggers().values()).map(getConfigurationConverter()::convert).collect(Collectors.toList());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Get the appender with the given name
	 */
	private Appender getAppenderByName(String name) {
		return LogUtils.getLoggerConfiguration().getAppender(name);
	}


	/**
	 * Gets the logger configuration for the given name, if no logger exists with that name the configuration for the next available parent logger will be returned
	 */
	private LoggerConfig getLoggerConfigByName(String name) {
		return LogUtils.getLoggerConfiguration().getLoggerConfig(name);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Converter<LogConfiguration, LoggerConfig> getLogConfigurationConverter() {
		return this.logConfigurationConverter;
	}


	public void setLogConfigurationConverter(Converter<LogConfiguration, LoggerConfig> logConfigurationConverter) {
		this.logConfigurationConverter = logConfigurationConverter;
	}


	public Converter<Appender, LogAppender> getAppenderConverter() {
		return this.appenderConverter;
	}


	public void setAppenderConverter(Converter<Appender, LogAppender> appenderConverter) {
		this.appenderConverter = appenderConverter;
	}


	public Converter<LoggerConfig, LogConfiguration> getConfigurationConverter() {
		return this.configurationConverter;
	}


	public void setConfigurationConverter(Converter<LoggerConfig, LogConfiguration> configurationConverter) {
		this.configurationConverter = configurationConverter;
	}
}
