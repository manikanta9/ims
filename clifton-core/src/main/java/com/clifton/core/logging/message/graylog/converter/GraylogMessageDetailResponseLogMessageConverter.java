package com.clifton.core.logging.message.graylog.converter;

import com.clifton.core.logging.message.LogMessage;
import com.clifton.core.logging.message.graylog.pojo.GraylogMessage;
import com.clifton.core.logging.message.graylog.pojo.GraylogMessageDetailResponse;
import com.clifton.core.util.converter.Converter;


/**
 * @author theodorez
 */
public class GraylogMessageDetailResponseLogMessageConverter implements Converter<GraylogMessageDetailResponse, LogMessage> {

	private Converter<GraylogMessage, LogMessage> messageConverter;


	@Override
	public LogMessage convert(GraylogMessageDetailResponse from) {
		LogMessage logMessage = getMessageConverter().convert(from.getMessageWrapper().getMessage());
		logMessage.setIndex(from.getIndex());
		return logMessage;
	}


	public Converter<GraylogMessage, LogMessage> getMessageConverter() {
		return this.messageConverter;
	}


	public void setMessageConverter(Converter<GraylogMessage, LogMessage> messageConverter) {
		this.messageConverter = messageConverter;
	}
}
