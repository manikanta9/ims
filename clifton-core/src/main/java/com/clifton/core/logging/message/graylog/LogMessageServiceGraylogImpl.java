package com.clifton.core.logging.message.graylog;

import com.clifton.core.logging.message.LogMessage;
import com.clifton.core.logging.message.LogMessageService;
import com.clifton.core.logging.message.graylog.pojo.GraylogMessageDetailResponse;
import com.clifton.core.logging.message.graylog.pojo.GraylogSearchResponse;
import com.clifton.core.logging.message.graylog.pojo.GraylogSearchResponseMessageWrapper;
import com.clifton.core.logging.message.search.LogMessageSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationUtils;
import org.apache.http.HttpHost;
import org.apache.http.client.AuthCache;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.impl.auth.BasicScheme;
import org.apache.http.impl.client.BasicAuthCache;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;


/**
 * Service implementation for interacting with graylog server
 *
 * @author theodorez
 */
@Service("logMessageService")
public class LogMessageServiceGraylogImpl implements LogMessageService {

	private static final String MESSAGE_ENDPOINT = "/api/messages";
	private static final String SEARCH_ENDPOINT = "/api/search/universal/absolute";

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	private String graylogScheme;
	private String graylogHost;
	private Integer graylogPort;

	private String apiToken;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////

	private Converter<GraylogSearchResponseMessageWrapper, LogMessage> searchResponseWrapperConverter;
	private Converter<GraylogMessageDetailResponse, LogMessage> detailResponseConverter;
	private Converter<LogMessageSearchForm, String> searchFormConverter;

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	/**
	 * Graylog requires the message ID and the index that the message is contained in.
	 * <p>
	 * These values are delimited by a :
	 */
	@Override
	@SecureMethod(securityResource = "Logging")
	public LogMessage getLogMessage(String identifier) {
		ValidationUtils.assertTrue(identifier.contains(":"), "Submitted value must be in the format [MessageID]:[Index]");
		//ID is at 0, Index is at 1
		String[] arr = identifier.split(":");

		return getDetailResponseConverter().convert((doRequest(getUriComponentsBuilder().path(MESSAGE_ENDPOINT).pathSegment(arr[1], arr[0]), GraylogMessageDetailResponse.class)).getBody());
	}


	@Override
	@SecureMethod(securityResource = "Logging")
	public PagingArrayList<LogMessage> getLogMessageList(LogMessageSearchForm searchForm) {
		ValidationUtils.assertTrue(searchForm.getStart() < 10000, "Cannot page past 10,000 rows");
		PagingArrayList<LogMessage> logMessages = new PagingArrayList<>();
		UriComponentsBuilder builder = getUriComponentsBuilder().path(SEARCH_ENDPOINT).query(getSearchFormConverter().convert(searchForm));

		ResponseEntity<GraylogSearchResponse> response = doRequest(builder, GraylogSearchResponse.class);

		GraylogSearchResponse searchResponse = response.getBody();
		AssertUtils.assertNotNull(searchResponse, "Cannot get log messages because response body was null");
		if (!ArrayUtils.isEmpty(searchResponse.getMessageWrappers())) {
			for (GraylogSearchResponseMessageWrapper wrapper : searchResponse.getMessageWrappers()) {
				logMessages.add(getSearchResponseWrapperConverter().convert(wrapper));
			}
		}
		logMessages.setTotalElementCount(response.getBody().getTotalResults());
		return logMessages;
	}


	private <T> ResponseEntity<T> doRequest(UriComponentsBuilder builder, Class<T> responseType) {
		ResponseEntity<T> responseEntity = getGraylogRestTemplate().exchange(builder.build().encode().toUri(), HttpMethod.GET, getRequestEntity(), responseType);
		ValidationUtils.assertNotNull(responseEntity, "Response entity returned is null for URI: " + builder.build().encode().toUri());
		return responseEntity;
	}


	private HttpEntity<?> getRequestEntity() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
		return new HttpEntity<>(headers);
	}


	private RestTemplate getGraylogRestTemplate() {
		HttpHost host = new HttpHost(getGraylogHost(), getGraylogPort(), getGraylogScheme());
		RestTemplate restTemplate = new RestTemplate(new HttpComponentsClientHttpRequestFactoryBasicAuth(host));
		restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(getApiToken(), "token"));
		return restTemplate;
	}


	private UriComponentsBuilder getUriComponentsBuilder() {
		return UriComponentsBuilder.newInstance().scheme(getGraylogScheme()).host(getGraylogHost()).port(getGraylogPort());
	}
	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	public String getGraylogHost() {
		return this.graylogHost;
	}


	public void setGraylogHost(String graylogHost) {
		this.graylogHost = graylogHost;
	}


	public Integer getGraylogPort() {
		return this.graylogPort;
	}


	public void setGraylogPort(Integer graylogPort) {
		this.graylogPort = graylogPort;
	}


	public String getApiToken() {
		return this.apiToken;
	}


	public void setApiToken(String apiToken) {
		this.apiToken = apiToken;
	}


	public String getGraylogScheme() {
		return this.graylogScheme;
	}


	public void setGraylogScheme(String graylogScheme) {
		this.graylogScheme = graylogScheme;
	}


	public Converter<LogMessageSearchForm, String> getSearchFormConverter() {
		return this.searchFormConverter;
	}


	public void setSearchFormConverter(Converter<LogMessageSearchForm, String> searchFormConverter) {
		this.searchFormConverter = searchFormConverter;
	}


	public Converter<GraylogSearchResponseMessageWrapper, LogMessage> getSearchResponseWrapperConverter() {
		return this.searchResponseWrapperConverter;
	}


	public void setSearchResponseWrapperConverter(Converter<GraylogSearchResponseMessageWrapper, LogMessage> searchResponseWrapperConverter) {
		this.searchResponseWrapperConverter = searchResponseWrapperConverter;
	}


	public Converter<GraylogMessageDetailResponse, LogMessage> getDetailResponseConverter() {
		return this.detailResponseConverter;
	}


	public void setDetailResponseConverter(Converter<GraylogMessageDetailResponse, LogMessage> detailResponseConverter) {
		this.detailResponseConverter = detailResponseConverter;
	}

	//////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////


	class HttpComponentsClientHttpRequestFactoryBasicAuth extends HttpComponentsClientHttpRequestFactory {

		HttpHost host;


		public HttpComponentsClientHttpRequestFactoryBasicAuth(HttpHost host) {
			super();
			this.host = host;
		}


		@Override
		protected HttpContext createHttpContext(HttpMethod httpMethod, URI uri) {
			return createHttpContext();
		}


		private HttpContext createHttpContext() {
			AuthCache authCache = new BasicAuthCache();

			BasicScheme basicAuth = new BasicScheme();
			authCache.put(this.host, basicAuth);

			BasicHttpContext localcontext = new BasicHttpContext();
			localcontext.setAttribute(HttpClientContext.AUTH_CACHE, authCache);
			return localcontext;
		}
	}
}
