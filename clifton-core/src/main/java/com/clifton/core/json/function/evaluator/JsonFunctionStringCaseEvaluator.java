package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;


/**
 * @author StevenF
 */
public class JsonFunctionStringCaseEvaluator extends BaseJsonFunctionStringEvaluator {

	private boolean upper;


	public JsonFunctionStringCaseEvaluator(JsonFunctionEvaluator<String> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate, boolean upper) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
		this.upper = upper;
	}


	@Override
	public String evaluateFunction(String fieldValue, String[] params) {
		if (fieldValue != null) {
			return isUpper() ? fieldValue.toUpperCase() : fieldValue.toLowerCase();
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isUpper() {
		return this.upper;
	}


	public void setUpper(boolean upper) {
		this.upper = upper;
	}
}
