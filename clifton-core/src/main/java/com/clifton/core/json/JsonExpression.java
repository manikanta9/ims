package com.clifton.core.json;

import java.util.List;


/**
 * The <code>JsonExpression</code> is used as a generic format used to evaluate conditions and functions.
 * The current DTO is based on the front-end implementation of the 'jQuery QueryBuilder' plugin, though a new
 * UI could be created that would support the same structure, with some possible minor changes to variable names, etc.
 * <p>
 * The generics are used to allow sub-classes to provide unique enums that allow direct binding to supported data types,
 * input types, operators, and conditions.
 *
 * @author StevenF
 */
public abstract class JsonExpression<J extends JsonExpression<J, T, I, O, C>, T extends Enum<?>, I extends Enum<?>, O extends Enum<?>, C extends Enum<?>> {

	private Boolean valid;

	private String id;
	private String field;
	private Object value;
	private Object data;

	//The data type of the expression (e.g. STRING, DECIMAL, BOOLEAN, etc.)
	private T type;
	//Supported input types for the UI (e.g. TEXT, NUMBER, CHECKBOX, etc.)
	private I input;
	//Defines the available 'actions' of an expression (e.g. 'contains' condition, or 'trim' function, or '% threshold' comparator, etc.)
	private O operator;
	//Condition is a somewhat arbitrary fieldName; used to define 'AND/OR' conditions, or 'Evaluate/Concatenate' functions.
	private C condition;
	//A list of child expressions of the same type; allows for chaining of expressions.
	private List<J> rules;

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getValid() {
		return this.valid;
	}


	public void setValid(Boolean valid) {
		this.valid = valid;
	}


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getField() {
		return this.field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public Object getValue() {
		return this.value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public Object getData() {
		return this.data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public T getType() {
		return this.type;
	}


	public void setType(T type) {
		this.type = type;
	}


	public I getInput() {
		return this.input;
	}


	public void setInput(I input) {
		this.input = input;
	}


	public O getOperator() {
		return this.operator;
	}


	public void setOperator(O operator) {
		this.operator = operator;
	}


	public C getCondition() {
		return this.condition;
	}


	public void setCondition(C condition) {
		this.condition = condition;
	}


	public List<J> getRules() {
		return this.rules;
	}


	public void setRules(List<J> rules) {
		this.rules = rules;
	}
}
