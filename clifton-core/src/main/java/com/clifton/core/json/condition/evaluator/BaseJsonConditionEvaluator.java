package com.clifton.core.json.condition.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.JsonCondition;

import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public abstract class BaseJsonConditionEvaluator<P> implements JsonConditionEvaluator<Boolean> {

	private boolean not;

	protected JsonCondition jsonCondition;


	protected BaseJsonConditionEvaluator(JsonCondition jsonCondition) {
		this.jsonCondition = jsonCondition;
		this.not = false;
	}


	protected BaseJsonConditionEvaluator(JsonCondition jsonCondition, boolean not) {
		this.jsonCondition = jsonCondition;
		this.not = not;
	}


	@Override
	@SuppressWarnings("unchecked")
	public Boolean evaluateCondition(Map<String, Object> conditionData, Map<String, Object> contextData) {
		P fieldValue = (P) DataTypeNameUtils.convertObjectToDataTypeName(conditionData.get(getJsonCondition().getField()), getJsonCondition().getType().getDataTypeName());
		P[] params = getParams(getJsonCondition().getValue(), contextData);
		return evaluateCondition(fieldValue, params, contextData);
	}


	//Convert to String[] for params; but then allow implementing function convert to P when necessary.
	@SuppressWarnings("unchecked")
	private P[] getParams(Object value, Map<String, Object> contextData) {
		return getParams(value instanceof List ? ((List<String>) value).toArray(new String[0]) : value != null ? new String[]{(String) value} : null, contextData);
	}


	public abstract P[] getParams(String[] params, Map<String, Object> contextData);


	public abstract Boolean evaluateCondition(P fieldValue, P[] params, Map<String, Object> contextData);


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public JsonCondition getJsonCondition() {
		return this.jsonCondition;
	}


	public boolean isNot() {
		return this.not;
	}


	public void setNot(boolean not) {
		this.not = not;
	}
}
