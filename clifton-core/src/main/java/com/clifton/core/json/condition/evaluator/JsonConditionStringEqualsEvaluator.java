package com.clifton.core.json.condition.evaluator;

import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionStringEqualsEvaluator extends BaseJsonConditionEqualsEvaluator<String> {

	private boolean caseSensitive;


	public JsonConditionStringEqualsEvaluator(JsonCondition jsonCondition, boolean not, boolean blank, boolean caseSensitive) {
		super(jsonCondition, not, blank);
		this.caseSensitive = caseSensitive;
	}


	@Override
	public Boolean evaluateCondition(String fieldValue, String[] params, Map<String, Object> contextData) {
		if (isBlank()) {
			if (isNot()) {
				return !StringUtils.isEmpty(fieldValue);
			}
			return StringUtils.isEmpty(fieldValue);
		}
		if (fieldValue != null) {
			if (params == null) {
				return false;
			}
			ValidationUtils.assertTrue(params.length == 1, "Equal operation supports only one string parameter!");
			String ruleValue = params[0];
			if (isNot()) {
				return isCaseSensitive() ? !StringUtils.isEqual(fieldValue, ruleValue) : !StringUtils.isEqualIgnoreCase(fieldValue, ruleValue);
			}
			return isCaseSensitive() ? StringUtils.isEqual(fieldValue, ruleValue) : StringUtils.isEqualIgnoreCase(fieldValue, ruleValue);
		}
		return false;
	}


	@Override
	public String[] getParams(String[] params, Map<String, Object> contextData) {
		return params;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isCaseSensitive() {
		return this.caseSensitive;
	}


	public void setCaseSensitive(boolean caseSensitive) {
		this.caseSensitive = caseSensitive;
	}
}
