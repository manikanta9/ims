package com.clifton.core.json.custom.util;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.json.custom.CustomJsonObject;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * <code>CustomJsonObjectUtils</code> is a utility for initializing {@link CustomJsonObject}s.
 * It is intended to be used in any business use case without the need to rely on a Spring managed, or dependency injected, bean/object.
 *
 * @author nickk
 */
public class CustomJsonObjectUtils {

	protected static final Map<Class<? extends CustomJsonObject>, CustomJsonObjectInitializationUtilHandler<?>> CUSTOM_JSON_OBJECT_TO_INITIALIZER_MAP = new ConcurrentHashMap<>();

	protected static final ObjectWrapper<JsonHandler<?>> JSON_HANDLER_OBJECT_WRAPPER = new ObjectWrapper<>();


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public static <T extends CustomJsonObject> void updateJsonFromObject(T customJsonObject) {
		if (customJsonObject != null) {
			validateJsonHandler();

			CustomJsonObjectInitializationUtilHandler<T> initializationUtilHandler = getInitializationHandler(customJsonObject);
			if (initializationUtilHandler != null) {
				initializationUtilHandler.prepareObjectForJsonSerialization(customJsonObject);
			}
			Object valueToSerialize = customJsonObject.toObjectForSerialization();
			ValidationUtils.assertNotNull(customJsonObject.getSerializationObjectClass(), customJsonObject.getClass().getSimpleName() + " does not properly implement getSerializationObjectClass().");
			ValidationUtils.assertTrue(customJsonObject.getSerializationObjectClass().isInstance(valueToSerialize), customJsonObject.getClass().getSimpleName() + " has a mismatch of types between serialization class and serialization object");
			customJsonObject.setJsonValue(JSON_HANDLER_OBJECT_WRAPPER.getObject().toJson(valueToSerialize));
		}
	}


	public static <T extends CustomJsonObject> void initializeObjectFromJson(T customJsonObject) {
		if (customJsonObject != null && !StringUtils.isEmpty(customJsonObject.getJsonValue())) {
			validateJsonHandler();
			Object deserializedCustomJsonObject = JSON_HANDLER_OBJECT_WRAPPER.getObject().fromJson(customJsonObject.getJsonValue(), customJsonObject.getSerializationObjectClass());
			if (deserializedCustomJsonObject != null) {
				customJsonObject.applyDeserializedObject(deserializedCustomJsonObject);
				CustomJsonObjectInitializationUtilHandler<T> initializationUtilHandler = getInitializationHandler(customJsonObject);
				if (initializationUtilHandler != null) {
					initializationUtilHandler.postInitializeObjectAfterJsonDeserialization(customJsonObject);
				}
			}
		}
	}


	private static <T extends CustomJsonObject> CustomJsonObjectInitializationUtilHandler<T> getInitializationHandler(T customJsonObject) {
		@SuppressWarnings("unchecked")
		CustomJsonObjectInitializationUtilHandler<T> initializationUtilHandler = (CustomJsonObjectInitializationUtilHandler<T>) CUSTOM_JSON_OBJECT_TO_INITIALIZER_MAP.get(customJsonObject.getClass());
		return initializationUtilHandler;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static void validateJsonHandler() {
		ValidationUtils.assertNotNull(JSON_HANDLER_OBJECT_WRAPPER.getObject(), "Json Handler was not properly initialized.");
	}
}
