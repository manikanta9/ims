package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;


/**
 * @author StevenF
 */
public class JsonFunctionValueEvaluator extends BaseJsonFunctionStringEvaluator {

	public JsonFunctionValueEvaluator(JsonFunctionEvaluator<String> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public String evaluateFunction(String fieldValue, String[] params) {
		//Value evaluator ignores the fieldValue; and simply returns the values entered (first parameter)
		if (params != null && params.length == 1) {
			return String.valueOf(params[0]);
		}
		throw new ValidationException("Value [" + getJsonFunction().getValue() + "] was null!");
	}
}
