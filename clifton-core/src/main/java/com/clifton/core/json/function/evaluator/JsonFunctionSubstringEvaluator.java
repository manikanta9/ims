package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;


/**
 * @author StevenF
 */
public class JsonFunctionSubstringEvaluator extends BaseJsonFunctionStringEvaluator {

	private boolean left;
	private boolean right;


	public JsonFunctionSubstringEvaluator(JsonFunctionEvaluator<String> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate, boolean left, boolean right) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
		this.left = left;
		this.right = right;
	}


	@Override
	public String evaluateFunction(String fieldValue, String[] params) {
		if (fieldValue != null) {
			if (isLeft()) {
				return StringUtils.left(fieldValue, Integer.parseInt(params[0]));
			}
			else if (isRight()) {
				return StringUtils.right(fieldValue, Integer.parseInt(params[0]));
			}
			if (params.length == 1 || (params.length == 2 && StringUtils.isEmpty(params[1]))) {
				return fieldValue.substring(Integer.parseInt(params[0]));
			}
			else if (params.length == 2) {
				int startIndex = Integer.parseInt(params[0]);
				int endIndex = Integer.parseInt(params[1]);
				if (endIndex < 0) {
					return StringUtils.substring(fieldValue, startIndex, fieldValue.length() - Math.abs(endIndex));
				}
				else {
					return StringUtils.substring(fieldValue, startIndex, endIndex);
				}
			}
			throw new ValidationException("Unsupported number of params [" + params.length + "]");
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isLeft() {
		return this.left;
	}


	public void setLeft(boolean left) {
		this.left = left;
	}


	public boolean isRight() {
		return this.right;
	}


	public void setRight(boolean right) {
		this.right = right;
	}
}
