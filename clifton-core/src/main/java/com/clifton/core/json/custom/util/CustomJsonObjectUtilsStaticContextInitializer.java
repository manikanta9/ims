package com.clifton.core.json.custom.util;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.json.custom.CustomJsonObject;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;


@Component
public class CustomJsonObjectUtilsStaticContextInitializer implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		initializeJsonHandler(getApplicationContext());
		initializeCustomJsonObjectInitializationMap(getApplicationContext());
	}


	private void initializeJsonHandler(ApplicationContext applicationContext) {
		JsonHandler<?> jsonHandler = (JsonHandler<?>) getBean(applicationContext, "jsonHandler");
		CustomJsonObjectUtils.JSON_HANDLER_OBJECT_WRAPPER.setObject(jsonHandler);
	}


	private Object getBean(ApplicationContext applicationContext, String beanName) {
		try {
			return applicationContext.getBean(beanName);
		}
		catch (NoSuchBeanDefinitionException e) {
			LogUtils.warn(LogCommand.ofMessage(getClass(), "Missing " + beanName + " bean in application context."));
		}
		return null;
	}


	private void initializeCustomJsonObjectInitializationMap(ApplicationContext applicationContext) {
		@SuppressWarnings("rawtypes")
		Map<String, CustomJsonObjectInitializationUtilHandler> eventBeans = applicationContext.getBeansOfType(CustomJsonObjectInitializationUtilHandler.class);
		if (!CollectionUtils.isEmpty(eventBeans)) {
			eventBeans.forEach((beanName, initializationHandler) -> {
				@SuppressWarnings("unchecked")
				Class<? extends CustomJsonObject> jsonObjectType = initializationHandler.getCustomJsonObjectClass();
				if (jsonObjectType != null) {
					CustomJsonObjectUtils.CUSTOM_JSON_OBJECT_TO_INITIALIZER_MAP.put(jsonObjectType, initializationHandler);
				}
			});
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
