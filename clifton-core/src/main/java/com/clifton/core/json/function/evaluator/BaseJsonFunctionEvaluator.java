package com.clifton.core.json.function.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.function.JsonFunction;

import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public abstract class BaseJsonFunctionEvaluator<R> implements JsonFunctionEvaluator<R> {

	protected boolean concatenate = false;

	protected JsonFunctionEvaluator<R> jsonFunctionEvaluator;

	protected JsonFunction jsonFunction;
	protected Map<String, Object> jsonFunctionData;


	public BaseJsonFunctionEvaluator(JsonFunctionEvaluator<R> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		this.jsonFunction = jsonFunction;
		this.jsonFunctionEvaluator = jsonFunctionEvaluator;
		this.concatenate = concatenate;
	}


	public JsonFunction getJsonFunction() {
		return this.jsonFunction;
	}


	public Map<String, Object> getJsonFunctionData() {
		return this.jsonFunctionData;
	}


	@Override
	@SuppressWarnings("unchecked")
	public R evaluateFunction(Map<String, Object> functionData) {
		this.jsonFunctionData = functionData;

		R result;
		R fieldValue = (R) DataTypeNameUtils.convertObjectToDataTypeName(functionData.get(getJsonFunction().getField()), getJsonFunction().getType().getDataTypeName());
		String[] params = getParams(getJsonFunction().getValue());
		JsonFunctionEvaluator<R> functionEvaluator = getJsonFunctionEvaluator();
		if (functionEvaluator != null) {
			result = functionEvaluator.evaluateFunction(functionData);
			//both parent and child must be set to concatenate
			if (isConcatenate() && functionEvaluator.isConcatenate()) {
				return concatenateResult(evaluateFunction(fieldValue, params), result);
			}
			else {
				return evaluateFunction(result, params);
			}
		}
		return evaluateFunction(fieldValue, params);
	}


	@SuppressWarnings("unchecked")
	private String[] getParams(Object value) {
		return value instanceof List ? ((List<String>) value).toArray(new String[0]) : value != null ? new String[]{(String) value} : null;
	}


	@Override
	public boolean isConcatenate() {
		return this.concatenate;
	}


	public abstract R evaluateFunction(R fieldValue, String[] params);


	public abstract R concatenateResult(R functionResult, R childResult);

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonFunctionEvaluator<R> getJsonFunctionEvaluator() {
		return this.jsonFunctionEvaluator;
	}


	public void setJsonFunctionEvaluator(JsonFunctionEvaluator<R> jsonFunctionEvaluator) {
		this.jsonFunctionEvaluator = jsonFunctionEvaluator;
	}
}
