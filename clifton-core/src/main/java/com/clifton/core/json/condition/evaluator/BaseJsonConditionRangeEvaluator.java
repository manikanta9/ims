package com.clifton.core.json.condition.evaluator;

import com.clifton.core.json.condition.JsonCondition;


/**
 * @author StevenF
 */
public abstract class BaseJsonConditionRangeEvaluator<P> extends BaseJsonConditionEvaluator<P> {

	private boolean equal;
	private boolean lessThan;
	private boolean greaterThan;


	public BaseJsonConditionRangeEvaluator(JsonCondition jsonCondition, boolean not, boolean equal, boolean lessThan, boolean greaterThan) {
		super(jsonCondition, not);
		this.equal = equal;
		this.lessThan = lessThan;
		this.greaterThan = greaterThan;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isEqual() {
		return this.equal;
	}


	public void setEqual(boolean equal) {
		this.equal = equal;
	}


	public boolean isLessThan() {
		return this.lessThan;
	}


	public void setLessThan(boolean lessThan) {
		this.lessThan = lessThan;
	}


	public boolean isGreaterThan() {
		return this.greaterThan;
	}


	public void setGreaterThan(boolean greaterThan) {
		this.greaterThan = greaterThan;
	}
}
