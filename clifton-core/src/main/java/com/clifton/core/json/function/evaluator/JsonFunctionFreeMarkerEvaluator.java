package com.clifton.core.json.function.evaluator;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * @author StevenF
 */
public class JsonFunctionFreeMarkerEvaluator extends BaseJsonFunctionEvaluator<Object> {

	private static final String FIELD_VALUE = "fieldValue";


	public JsonFunctionFreeMarkerEvaluator(JsonFunctionEvaluator<Object> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public String evaluateFunction(Object fieldValue, String[] params) {
		if (fieldValue != null) {
			ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonFunction().getOperator().getName() + "] supports only one parameter!");
			FreemarkerTemplateConverter templateConverter = new FreemarkerTemplateConverter();
			TemplateConfig config = new TemplateConfig(params[0]);
			config.addBeanToContext(FIELD_VALUE, fieldValue);
			return templateConverter.convert(config);
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}


	@Override
	public String concatenateResult(Object functionResult, Object childResult) {
		return String.valueOf(childResult) + String.valueOf(functionResult);
	}
}
