package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class JsonFunctionNegateNumberEvaluator extends BaseJsonFunctionNumberEvaluator {


	public JsonFunctionNegateNumberEvaluator(JsonFunctionEvaluator<Number> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public Number evaluateFunction(Number fieldValue, String[] params) {
		if (fieldValue != null) {
			if (fieldValue instanceof Integer) {
				return ((Integer) fieldValue) * -1;
			}
			else if (fieldValue instanceof BigDecimal) {
				return ((BigDecimal) fieldValue).negate();
			}
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}
}
