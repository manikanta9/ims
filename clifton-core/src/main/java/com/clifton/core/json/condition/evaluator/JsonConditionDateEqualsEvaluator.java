package com.clifton.core.json.condition.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author StevenF
 */
public class JsonConditionDateEqualsEvaluator extends BaseJsonConditionEqualsEvaluator<Date> {

	//Pattern to extract the parameter value when date fieldValue is defined using a relative function
	//e.g. TODAY(-1) will allow a pattern matching to return '-1' at matcher group 1
	private static final Pattern RELATIVE_DATE_FUNCTION_PARAM_PATTERN = Pattern.compile("DATE\\((.*?)\\)");


	public JsonConditionDateEqualsEvaluator(JsonCondition jsonCondition, boolean not, boolean blank) {
		super(jsonCondition, not, blank);
	}


	@Override
	public Boolean evaluateCondition(Date fieldValue, Date[] params, Map<String, Object> contextData) {
		if (isBlank()) {
			if (isNot()) {
				return fieldValue != null;
			}
			return fieldValue == null;
		}
		if (fieldValue != null) {
			if (params == null) {
				return false;
			}
			ValidationUtils.assertTrue(params.length == 1, "Equal operation supports only one date parameter!");
			Date ruleValue = params[0];
			if (isNot()) {
				return !DateUtils.isEqualWithoutTime(fieldValue, ruleValue);
			}
			return DateUtils.isEqualWithoutTime(fieldValue, ruleValue);
		}
		return false;
	}


	@Override
	@SuppressWarnings("ConstantConditions")
	public Date[] getParams(String[] params, Map<String, Object> contextData) {
		if (params == null) {
			return null;
		}
		Date[] dateParams = new Date[params.length];
		for (int i = 0; i < params.length; i++) {
			String dateParam = params[i];
			Matcher matcher = RELATIVE_DATE_FUNCTION_PARAM_PATTERN.matcher(dateParam);
			if (matcher.find()) {
				Object relativeDateObject = contextData.get(JsonConditionEvaluator.CONTEXT_RELATIVE_DATE);
				AssertUtils.assertTrue(relativeDateObject instanceof Date, "Relative date for use with 'DATE()' function not found!");
				dateParams[i] = DateUtils.addWeekDays((Date) relativeDateObject, Integer.parseInt(matcher.group(1)));
			}
			else {
				dateParams[i] = (Date) DataTypeNameUtils.convertObjectToDataTypeName(params[i], getJsonCondition().getType().getDataTypeName());
			}
		}
		return dateParams;
	}
}
