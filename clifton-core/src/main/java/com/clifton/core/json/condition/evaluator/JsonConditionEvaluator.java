package com.clifton.core.json.condition.evaluator;

import java.util.Map;


/**
 * @author StevenF
 */
public interface JsonConditionEvaluator<R> {

	public static final String CONTEXT_RELATIVE_DATE = "RELATIVE_DATE";


	public R evaluateCondition(Map<String, Object> conditionData, Map<String, Object> contextData);
}
