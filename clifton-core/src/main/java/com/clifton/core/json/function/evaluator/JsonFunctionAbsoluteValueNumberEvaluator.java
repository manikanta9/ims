package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class JsonFunctionAbsoluteValueNumberEvaluator extends BaseJsonFunctionNumberEvaluator {


	public JsonFunctionAbsoluteValueNumberEvaluator(JsonFunctionEvaluator<Number> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public Number evaluateFunction(Number fieldValue, String[] params) {
		if (fieldValue != null) {
			if (fieldValue instanceof BigDecimal) {
				return MathUtils.abs((BigDecimal) fieldValue);
			}
			else if (fieldValue instanceof Double) {
				return Math.abs(fieldValue.doubleValue());
			}
			else {
				return Math.abs(fieldValue.intValue());
			}
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}
}
