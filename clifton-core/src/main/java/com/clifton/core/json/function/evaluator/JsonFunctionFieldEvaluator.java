package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;


/**
 * @author StevenF
 */
public class JsonFunctionFieldEvaluator extends BaseJsonFunctionEvaluator<Object> {

	public JsonFunctionFieldEvaluator(JsonFunctionEvaluator<Object> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public Object evaluateFunction(Object fieldValue, String[] params) {
		if (fieldValue != null) {
			return fieldValue;
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}


	@Override
	public String concatenateResult(Object functionResult, Object childResult) {
		return String.valueOf(childResult) + String.valueOf(functionResult);
	}
}
