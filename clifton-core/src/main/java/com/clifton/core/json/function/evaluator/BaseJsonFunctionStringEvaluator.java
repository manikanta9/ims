package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;


/**
 * @author StevenF
 */
public abstract class BaseJsonFunctionStringEvaluator extends BaseJsonFunctionEvaluator<String> {

	public BaseJsonFunctionStringEvaluator(JsonFunctionEvaluator<String> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public String concatenateResult(String functionResult, String childResult) {
		return childResult + functionResult;
	}
}
