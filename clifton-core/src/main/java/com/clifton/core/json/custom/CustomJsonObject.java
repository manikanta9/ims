package com.clifton.core.json.custom;

import java.io.Serializable;


/**
 * The <code>CustomJsonObject</code> is an interface for json values stored in the database as a string that contain custom data (i.e. Custom Column Values)
 *
 * @author nickk
 * @see com.clifton.core.dataaccess.dao.hibernate.GenericCustomJsonObjectUserType used for reading persisted JSON objects
 */
public interface CustomJsonObject extends Serializable {

	/**
	 * Returns the JSON String representation of the serialization object.
	 * <p>
	 * This is used during persistence to get the value to be stored in the database.
	 */
	public String getJsonValue();


	/**
	 * Sets the JSON String of the serialization object on this object.
	 * The string could be null if undefined.
	 * <p>
	 * This is used during reads from the database.
	 */
	public void setJsonValue(String jsonValue);


	/**
	 * Return the {@link Class} of the object that is serialized to JSON.
	 * <p>
	 * This class is used for deserializing the JSON string into an object.
	 * In some cases this will be the implementation of this interface.
	 * It could be other types, such as a map of properties.
	 * <p>
	 * This class should match the type of object returned by {@link #toObjectForSerialization()},
	 * and will be the type of object provided to {@link #applyDeserializedObject(Object)}
	 *
	 * @see #getSerializationObjectClass()
	 * @see #applyDeserializedObject(Object)
	 */
	public Class<?> getSerializationObjectClass();


	/**
	 * Returns the object to be serialized as JSON.
	 * <p>
	 * The object returned here should be same type as the result of {@link #getSerializationObjectClass()},
	 * and will be provided after deserialization to {@link #applyDeserializedObject(Object)}.
	 *
	 * @see #getSerializationObjectClass()
	 * @see #applyDeserializedObject(Object)
	 */
	public Object toObjectForSerialization();


	/**
	 * Applies the result of deserializing the JSON string to an object,
	 * which should match the type returned from {@link #getSerializationObjectClass()}
	 * and the object result of {@link #toObjectForSerialization()}.
	 *
	 * @see #getSerializationObjectClass()
	 * @see #toObjectForSerialization()
	 */
	public void applyDeserializedObject(Object object);
}
