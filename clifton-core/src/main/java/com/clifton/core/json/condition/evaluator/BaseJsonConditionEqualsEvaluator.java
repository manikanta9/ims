package com.clifton.core.json.condition.evaluator;

import com.clifton.core.json.condition.JsonCondition;


/**
 * @author StevenF
 */
public abstract class BaseJsonConditionEqualsEvaluator<P> extends BaseJsonConditionEvaluator<P> {

	private boolean blank;


	public BaseJsonConditionEqualsEvaluator(JsonCondition jsonCondition, boolean not, boolean blank) {
		super(jsonCondition, not);
		this.blank = blank;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBlank() {
		return this.blank;
	}


	public void setBlank(boolean blank) {
		this.blank = blank;
	}
}
