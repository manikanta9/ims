package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;

import java.util.regex.Pattern;


/**
 * @author StevenF
 */
public abstract class BaseJsonFunctionNumberEvaluator extends BaseJsonFunctionEvaluator<Number> {

	public static final Pattern NUMBER_PATTERN = Pattern.compile("[+-]?[0-9,]*\\.?[0-9]+");


	public BaseJsonFunctionNumberEvaluator(JsonFunctionEvaluator<Number> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	/**
	 * If there are characters, then this is assumed to be a fieldName and so grabs the value from the function data
	 */
	protected String convertFieldParameter(String param) {
		if (param == null) {
			throw new ValidationException("Value for rule [" + getJsonFunction().getOperator().getName() + "] was null!");
		}
		if (!NUMBER_PATTERN.matcher(param).matches()) {
			Object newParam = getJsonFunctionData().get(param);
			if (newParam == null) {
				throw new ValidationException("Value for rule [" + getJsonFunction().getOperator().getName() + "] was null after retrieving from functionData for field [" + param + "]!");
			}
			param = String.valueOf(newParam);
		}
		return param;
	}


	@Override
	public Number concatenateResult(Number functionResult, Number childResult) {
		throw new ValidationException("Unsupported Function!");
	}
}
