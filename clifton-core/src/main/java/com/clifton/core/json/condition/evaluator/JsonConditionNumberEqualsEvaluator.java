package com.clifton.core.json.condition.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionNumberEqualsEvaluator extends BaseJsonConditionEqualsEvaluator<Number> {

	public JsonConditionNumberEqualsEvaluator(JsonCondition jsonCondition, boolean not, boolean blank) {
		super(jsonCondition, not, blank);
	}


	@Override
	public Boolean evaluateCondition(Number fieldValue, Number[] params, Map<String, Object> contextData) {
		if (isBlank()) {
			if (isNot()) {
				return !MathUtils.isNullOrZero(fieldValue);
			}
			return MathUtils.isNullOrZero(fieldValue);
		}
		if (fieldValue != null) {
			if (params == null) {
				return false;
			}
			ValidationUtils.assertTrue(params.length == 1, "Equal operation supports only one string parameter!");
			Number ruleValue = params[0];
			if (isNot()) {
				return !MathUtils.isEqual(fieldValue, ruleValue);
			}
			return MathUtils.isEqual(fieldValue, ruleValue);
		}
		return false;
	}


	@Override
	public Number[] getParams(String[] params, Map<String, Object> contextData) {
		if (params == null) {
			return null;
		}
		Number[] numberParams = new Number[params.length];
		for (int i = 0; i < params.length; i++) {
			numberParams[i] = (Number) DataTypeNameUtils.convertObjectToDataTypeName(params[i], getJsonCondition().getType().getDataTypeName());
		}
		return numberParams;
	}
}
