package com.clifton.core.json.type;

/**
 * @author StevenF
 */
public enum JsonInputTypes {

	TEXT("text"),
	NUMBER("number"),
	TEXTAREA("textarea"),
	RADIO("radio"),
	CHECKBOX("checkbox"),
	SELECT("select");

	////////////////////////////////////////////////////////////////////////////

	private String name;


	@Override
	public String toString() {
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////


	JsonInputTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}
