package com.clifton.core.json.function;

/**
 * The <code>JsonFunctionTypes</code> defines the applicable conditions used when evaluating a function
 * e.g., functions can evaluated and/or concatenated to other evaluations, i.e. a value may have a
 * substring function applied, and then the result may be concatenated with the result of a split function.
 * primarily the enum is used to drive the display of the UI and what options are available.
 *
 * @author StevenF
 */
public enum JsonFunctionTypes {

	//'Function Rules' condition types.
	EVALUATE("Evaluate"),
	CONCATENATE("Concatenate");

	////////////////////////////////////////////////////////////////////////////

	private String name;


	@Override
	public String toString() {
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	JsonFunctionTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}
