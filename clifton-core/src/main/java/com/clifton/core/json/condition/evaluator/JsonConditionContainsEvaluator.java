package com.clifton.core.json.condition.evaluator;

import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionContainsEvaluator extends BaseJsonConditionEvaluator<String> {

	private boolean beginsWith;
	private boolean endsWith;


	public JsonConditionContainsEvaluator(JsonCondition jsonCondition, boolean not, boolean beginsWith, boolean endsWith) {
		super(jsonCondition, not);
		this.beginsWith = beginsWith;
		this.endsWith = endsWith;
	}


	@Override
	public Boolean evaluateCondition(String fieldValue, String[] params, Map<String, Object> contextData) {
		if (fieldValue != null) {
			if (params == null) {
				return false;
			}
			ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonCondition().getOperator().getName() + "] supports only one string parameter!");
			String ruleValue = params[0];
			if (isBeginsWith()) {
				if (isNot()) {
					return !fieldValue.startsWith(ruleValue);
				}
				return fieldValue.startsWith(ruleValue);
			}
			else if (isEndsWith()) {
				if (isNot()) {
					return !fieldValue.endsWith(ruleValue);
				}
				return fieldValue.endsWith(ruleValue);
			}
			if (isNot()) {
				return !fieldValue.contains(ruleValue);
			}
			return fieldValue.contains(ruleValue);
		}
		throw new RuntimeException("Unable to evaluate condition. Field value was null!");
	}


	@Override
	public String[] getParams(String[] params, Map<String, Object> contextData) {
		return params;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isBeginsWith() {
		return this.beginsWith;
	}


	public void setBeginsWith(boolean beginsWith) {
		this.beginsWith = beginsWith;
	}


	public boolean isEndsWith() {
		return this.endsWith;
	}


	public void setEndsWith(boolean endsWith) {
		this.endsWith = endsWith;
	}
}
