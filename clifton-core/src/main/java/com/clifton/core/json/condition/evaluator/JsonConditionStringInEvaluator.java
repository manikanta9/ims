package com.clifton.core.json.condition.evaluator;

import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Arrays;
import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionStringInEvaluator extends BaseJsonConditionEvaluator<String> {


	public JsonConditionStringInEvaluator(JsonCondition jsonCondition, boolean not) {
		super(jsonCondition, not);
	}


	@Override
	public Boolean evaluateCondition(String fieldValue, String[] params, Map<String, Object> contextData) {
		if (fieldValue != null && params != null) {
			if (isNot()) {
				return !CollectionUtils.contains(Arrays.asList(params), fieldValue);
			}
			return CollectionUtils.contains(Arrays.asList(params), fieldValue);
		}
		throw new RuntimeException("Unable to evaluate condition. Field value or params were null!");
	}


	@Override
	public String[] getParams(String[] params, Map<String, Object> contextData) {
		ValidationUtils.assertTrue(params != null && params.length == 1, "Operation [" + getJsonCondition().getOperator().getName() + "] supports only one non-null comma-separated parameter!");
		return params[0].split(",");
	}
}
