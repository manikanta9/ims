package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;


/**
 * @author StevenF
 */
public abstract class BaseJsonFunctionDateEvaluator extends BaseJsonFunctionEvaluator<Object> {

	public BaseJsonFunctionDateEvaluator(JsonFunctionEvaluator<Object> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public String concatenateResult(Object functionResult, Object childResult) {
		return childResult + String.valueOf(functionResult);
	}
}
