package com.clifton.core.json.custom;

import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.util.AssertUtils;

import javax.persistence.Transient;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>CustomJsonString</code> is used for json values stored in the database as a string that contain custom data (i.e. Custom Column Values)
 * The values can be converted to a map for user entry and easier reads/writes, and then back to the json string for the database.
 *
 * @author manderson
 */
public class CustomJsonString extends BaseCustomJsonObject {

	private static final String VALUE_KEY = "value";
	private static final String TEXT_KEY = "text";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Map representation of the jsonValue String
	 * Only populated when needed and then can read and update values
	 * See {@link CustomJsonStringUtils for details}
	 **/
	@Transient
	@NonPersistentField
	private Map<String, Object> jsonValueMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CustomJsonString() {
		// No-arg constructor for web binding
		super();
	}


	public CustomJsonString(String jsonValue) {
		super(jsonValue);
	}


	@Override
	public Class<?> getSerializationObjectClass() {
		return HashMap.class;
	}


	@Override
	public Object toObjectForSerialization() {
		return this.jsonValueMap;
	}


	@SuppressWarnings("unchecked")
	@Override
	public void applyDeserializedObject(Object object) {
		if (object != null && getSerializationObjectClass().isInstance(object)) {
			this.jsonValueMap = (Map<String, Object>) object;
		}
	}


	public Map<String, Object> jsonValueMap() {
		return this.jsonValueMap;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Map Helper Methods                            ////////
	////////////////////////////////////////////////////////////////////////////


	protected void initializeMap(Map<String, Object> jsonValueMap) {
		this.jsonValueMap = jsonValueMap;
	}


	@SuppressWarnings("unchecked")
	public Object getColumnValue(String columnName) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		Object value = this.jsonValueMap.get(columnName);
		if (value instanceof Map) {
			return ((Map<String, Object>) value).get(VALUE_KEY);
		}
		return value;
	}


	@SuppressWarnings("unchecked")
	public Object getColumnText(String columnName) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		Object value = this.jsonValueMap.get(columnName);
		if (value instanceof Map) {
			return ((Map<String, Object>) value).get(TEXT_KEY);
		}
		return value;
	}


	public Object getColumn(String columnName) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		return this.jsonValueMap.get(columnName);
	}


	/**
	 * Used to set the value only or value/text property based on String representations of their values
	 * if columnText is null or the same as columnValue will just set it directly, otherwise will create the value as a map with keys value and text
	 */
	public void setColumnValue(String columnName, Object columnValue, String columnText) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		if (columnText != null && !columnText.equals(columnValue)) {
			Map<String, Object> value = new HashMap<>();
			value.put(VALUE_KEY, columnValue);
			value.put(TEXT_KEY, columnText);
			this.jsonValueMap.put(columnName, value);
		}
		else {
			setColumnValueSimple(columnName, columnValue);
		}
	}


	/**
	 * Used to set the value only
	 * If value exists and is a map, will replace value only and will text as it was
	 */
	@SuppressWarnings("unchecked")
	public void replaceColumnValue(String columnName, Object columnValue) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		Object value = this.jsonValueMap.get(columnName);
		if (value instanceof Map) {
			((Map<String, Object>) value).put(VALUE_KEY, columnValue);
		}
		else {
			setColumnValueSimple(columnName, columnValue);
		}
	}


	/**
	 * Used to set the value only as a simple value.  i.e. an integer or date or string.  Values do not use the value / text combination
	 */
	public void setColumnValueSimple(String columnName, Object columnValue) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		this.jsonValueMap.put(columnName, columnValue);
	}


	public void clearColumnValue(String columnName) {
		AssertUtils.assertNotNull(this.jsonValueMap, "Json Value Map hasn't been initialized.");
		this.jsonValueMap.remove(columnName);
	}
}
