package com.clifton.core.json.condition;


import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;

import java.util.List;


/**
 * @author StevenF
 */
public class JsonConditionBuilder {

	private String id;
	private String field;
	private Object value;

	//Additional data that can be used in the rule.
	//This is not the same as the actual ruleData passed in when evaluating.
	private Object data;

	private JsonDataTypes type;
	private JsonInputTypes input;
	private JsonConditionOperatorTypes operator;

	private JsonConditionTypes condition;
	private List<JsonCondition> rules;

	////////////////////////////////////////////////////////////////////////////


	public JsonConditionBuilder andCondition(List<JsonCondition> andJsonConditions) {
		this.condition = JsonConditionTypes.AND;
		this.rules = andJsonConditions;
		return this;
	}


	public JsonConditionBuilder orCondition(List<JsonCondition> orJsonConditions) {
		this.condition = JsonConditionTypes.OR;
		this.rules = orJsonConditions;
		return this;
	}


	public JsonConditionBuilder withId(String id) {
		this.id = id;
		return this;
	}


	public JsonConditionBuilder withField(String field) {
		this.field = field;
		return this;
	}


	public JsonConditionBuilder withValue(Object value) {
		this.value = value;
		return this;
	}


	public JsonConditionBuilder ofDataType(JsonDataTypes type) {
		this.type = type;
		return this;
	}


	public JsonConditionBuilder ofInputType(JsonInputTypes input) {
		this.input = input;
		return this;
	}


	public JsonConditionBuilder ofOperatorType(JsonConditionOperatorTypes operator) {
		this.operator = operator;
		return this;
	}


	public JsonConditionBuilder withData(Object data) {
		this.data = data;
		return this;
	}


	public JsonCondition build() {
		JsonCondition jsonCondition = new JsonCondition();
		jsonCondition.setId(getId());
		jsonCondition.setField(getField());
		jsonCondition.setValue(getValue());
		jsonCondition.setData(getData());
		jsonCondition.setType(getType());
		jsonCondition.setInput(getInput());
		jsonCondition.setOperator(getOperator());
		jsonCondition.setCondition(getCondition());
		jsonCondition.setRules(getRules());
		return jsonCondition;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getField() {
		return this.field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public Object getValue() {
		return this.value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public Object getData() {
		return this.data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public JsonDataTypes getType() {
		return this.type;
	}


	public void setType(JsonDataTypes type) {
		this.type = type;
	}


	public JsonInputTypes getInput() {
		return this.input;
	}


	public void setInput(JsonInputTypes input) {
		this.input = input;
	}


	public JsonConditionOperatorTypes getOperator() {
		return this.operator;
	}


	public void setOperator(JsonConditionOperatorTypes operator) {
		this.operator = operator;
	}


	public JsonConditionTypes getCondition() {
		return this.condition;
	}


	public void setCondition(JsonConditionTypes condition) {
		this.condition = condition;
	}


	public List<JsonCondition> getRules() {
		return this.rules;
	}


	public void setRules(List<JsonCondition> rules) {
		this.rules = rules;
	}
}
