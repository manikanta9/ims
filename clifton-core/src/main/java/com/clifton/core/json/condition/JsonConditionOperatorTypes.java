package com.clifton.core.json.condition;

import com.clifton.core.json.condition.evaluator.JsonConditionContainsEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionDateEqualsEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionDateInEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionDateRangeEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionFreeMarkerEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionNumberEqualsEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionNumberInEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionNumberRangeEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionStringEqualsEvaluator;
import com.clifton.core.json.condition.evaluator.JsonConditionStringInEvaluator;
import com.clifton.core.shared.dataaccess.DataTypeNames;


/**
 * The <code>JsonConditionOperatorTypes</code> defines functions available as conditions
 * see: <code>ReconciliationRule</code> for additional details
 *
 * @author StevenF
 */
public enum JsonConditionOperatorTypes {


	////////////////////////////////////////////////////////////////////////////
	/////////                  CONDITION OPERATORS                  ////////////
	////////////////////////////////////////////////////////////////////////////

	EQUAL("equal") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberEqualsEvaluator(jsonCondition, false, false);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateEqualsEvaluator(jsonCondition, false, false);
			}
			return new JsonConditionStringEqualsEvaluator(jsonCondition, false, false, false);
		}
	},
	EQUAL_CS("equal_case_sensitive") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberEqualsEvaluator(jsonCondition, false, false);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateEqualsEvaluator(jsonCondition, false, false);
			}
			return new JsonConditionStringEqualsEvaluator(jsonCondition, false, false, true);
		}
	},
	NOT_EQUAL("not_equal") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberEqualsEvaluator(jsonCondition, true, false);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateEqualsEvaluator(jsonCondition, true, false);
			}
			return new JsonConditionStringEqualsEvaluator(jsonCondition, true, false, false);
		}
	},
	NOT_EQUAL_CS("not_equal_case_sensitive") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberEqualsEvaluator(jsonCondition, true, false);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateEqualsEvaluator(jsonCondition, true, false);
			}
			return new JsonConditionStringEqualsEvaluator(jsonCondition, true, false, true);
		}
	},
	IS_EMPTY("is_empty") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionStringEqualsEvaluator(jsonCondition, false, true, false);
		}
	},
	IS_NOT_EMPTY("is_not_empty") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionStringEqualsEvaluator(jsonCondition, true, true, false);
		}
	},
	IS_NULL("is_null") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberEqualsEvaluator(jsonCondition, false, true);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateEqualsEvaluator(jsonCondition, false, true);
			}
			return new JsonConditionStringEqualsEvaluator(jsonCondition, false, true, false);
		}
	},
	IS_NOT_NULL("is_not_null") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberEqualsEvaluator(jsonCondition, true, true);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateEqualsEvaluator(jsonCondition, true, true);
			}
			return new JsonConditionStringEqualsEvaluator(jsonCondition, true, true, false);
		}
	},
	CONTAINS("contains") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionContainsEvaluator(jsonCondition, false, false, false);
		}
	},
	NOT_CONTAINS("not_contains") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionContainsEvaluator(jsonCondition, true, false, false);
		}
	},
	BEGINS_WITH("begins_with") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionContainsEvaluator(jsonCondition, false, true, false);
		}
	},
	NOT_BEGINS_WITH("not_begins_with") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionContainsEvaluator(jsonCondition, true, true, false);
		}
	},
	ENDS_WITH("ends_with") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionContainsEvaluator(jsonCondition, false, false, true);
		}
	},
	NOT_ENDS_WITH("not_ends_with") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionContainsEvaluator(jsonCondition, true, false, true);
		}
	},
	IN("in") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberInEvaluator(jsonCondition, false);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateInEvaluator(jsonCondition, false);
			}
			return new JsonConditionStringInEvaluator(jsonCondition, false);
		}
	},
	NOT_IN("not_in") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.INTEGER == jsonCondition.getType().getDataTypeName() || DataTypeNames.DECIMAL == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionNumberInEvaluator(jsonCondition, true);
			}
			else if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateInEvaluator(jsonCondition, true);
			}
			return new JsonConditionStringInEvaluator(jsonCondition, true);
		}
	},
	LESS("less") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateRangeEvaluator(jsonCondition, false, false, true, false);
			}
			return new JsonConditionNumberRangeEvaluator(jsonCondition, false, false, true, false);
		}
	},
	LESS_OR_EQUAL("less_or_equal") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateRangeEvaluator(jsonCondition, false, true, true, false);
			}
			return new JsonConditionNumberRangeEvaluator(jsonCondition, false, true, true, false);
		}
	},
	GREATER("greater") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateRangeEvaluator(jsonCondition, false, false, false, true);
			}
			return new JsonConditionNumberRangeEvaluator(jsonCondition, false, false, false, true);
		}
	},
	GREATER_OR_EQUAL("greater_or_equal") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateRangeEvaluator(jsonCondition, false, true, false, true);
			}
			return new JsonConditionNumberRangeEvaluator(jsonCondition, false, true, false, true);
		}
	},
	BETWEEN("between") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateRangeEvaluator(jsonCondition, false, false, false, false);
			}
			return new JsonConditionNumberRangeEvaluator(jsonCondition, false, false, false, false);
		}
	},
	NOT_BETWEEN("not_between") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			if (DataTypeNames.DATE == jsonCondition.getType().getDataTypeName()) {
				return new JsonConditionDateRangeEvaluator(jsonCondition, true, false, false, false);
			}
			return new JsonConditionNumberRangeEvaluator(jsonCondition, true, false, false, false);
		}
	},
	FREE_MARKER("free_marker") {
		@Override
		public JsonConditionEvaluator<Boolean> newInstance(JsonCondition jsonCondition) {
			return new JsonConditionFreeMarkerEvaluator(jsonCondition);
		}
	};


	public abstract JsonConditionEvaluator<?> newInstance(JsonCondition jsonCondition);


	@SuppressWarnings("unused")
	//Default implementation simply calls newInstance - this is overridden by 'chainable' functions.
	public JsonConditionEvaluator<?> ofChain(JsonConditionEvaluator<?> jsonConditionEvaluator, JsonCondition jsonCondition) {
		return newInstance(jsonCondition);
	}

	////////////////////////////////////////////////////////////////////////////

	private String name;


	@Override
	public String toString() {
		return getName();
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	JsonConditionOperatorTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}

