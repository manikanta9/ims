package com.clifton.core.json.custom.generator.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableGenerator;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.datatable.bean.MapToDataTableConverter;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;

import java.lang.reflect.Field;
import java.sql.Types;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * A {@link DataTableGenerator} that finds invalid {@link CustomJsonString} entries in the database.
 *
 * @author michaelm
 */
public class CustomJsonStringInvalidDataTableGenerator implements DataTableGenerator {

	private SqlHandler sqlHandler;
	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable getDataTable() {
		Map<Object, Object> dtoWithInvalidJsonErrorMap = new HashMap<>();
		for (Class<?> dtoClass : CollectionUtils.getIterable(CoreClassUtils.getSimpleEntityClassSet())) {
			List<Field> customJsonStringFieldList = ArrayUtils.getStream(ClassUtils.getClassFields(dtoClass, true, false)).filter(field -> field.getType().equals(CustomJsonString.class)).collect(Collectors.toList());
			if (!CollectionUtils.isEmpty(customJsonStringFieldList)) {
				Integer numRowsWithInvalidJson = queryForInvalidCustomJsonStrings((Class<IdentityObject>) dtoClass, customJsonStringFieldList);
				if (numRowsWithInvalidJson != null && numRowsWithInvalidJson > 0) {
					dtoWithInvalidJsonErrorMap.put(dtoClass.getSimpleName(), numRowsWithInvalidJson);
				}
			}
		}
		return getErrorDataTable(dtoWithInvalidJsonErrorMap);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Integer queryForInvalidCustomJsonStrings(Class<IdentityObject> dtoClass, List<Field> customJsonStringFieldList) {
		final ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(dtoClass);
		if (dao == null || !dtoClass.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
			return null;
		}
		String tableName = getTableNameForDaoConfig(dao.getConfiguration());
		if (tableName == null) {
			return null;
		}
		SqlSelectCommand selectCommand = new SqlSelectCommand();
		selectCommand.setSelectClause("COUNT(*)");
		selectCommand.setFromClause(tableName);
		StringBuilder whereClause = new StringBuilder("ISJSON(" + customJsonStringFieldList.remove(0).getName() + ") = 0");
		CollectionUtils.getStream(customJsonStringFieldList).forEach(field -> whereClause.append(" OR ISJSON(").append(field.getName()).append(") = 0"));
		selectCommand.setWhereClause(whereClause.toString());
		return (Integer) getSqlHandler().queryForObject(selectCommand);
	}


	private String getTableNameForDaoConfig(DAOConfiguration<?> daoConfiguration) {
		String tableName = null;
		try {
			tableName = daoConfiguration.getTableName();
		}
		catch (IllegalStateException e) {
			// some DAOConfigurations don't have a table
		}
		return tableName;
	}


	private DataTable getErrorDataTable(Map<Object, Object> dtoWithInvalidJsonErrorMap) {
		DataColumn[] columns = new DataColumnImpl[]{
				new DataColumnImpl("Table Name", Types.NVARCHAR),
				new DataColumnImpl("Num Invalid CustomJsonString(s)", Types.INTEGER)
		};
		Function<Map.Entry<Object, Object>, Object[]> mapDataRowFunction = (Map.Entry<Object, Object> entry) -> new Object[]{entry.getKey(), entry.getValue()};
		MapToDataTableConverter converter = new MapToDataTableConverter(DataTableConverterConfiguration.ofMapDataRowFunctionUsingDataColumnArray(mapDataRowFunction, columns));
		return converter.convert(dtoWithInvalidJsonErrorMap);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
