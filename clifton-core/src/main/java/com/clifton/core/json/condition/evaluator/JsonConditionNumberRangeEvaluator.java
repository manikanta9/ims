package com.clifton.core.json.condition.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.MathUtils;

import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionNumberRangeEvaluator extends BaseJsonConditionRangeEvaluator<Number> {

	public JsonConditionNumberRangeEvaluator(JsonCondition jsonCondition, boolean not, boolean equal, boolean lessThan, boolean greaterThan) {
		super(jsonCondition, not, equal, lessThan, greaterThan);
	}


	@Override
	public Boolean evaluateCondition(Number fieldValue, Number[] params, Map<String, Object> contextData) {
		if (fieldValue != null) {
			if (params == null) {
				return false;
			}
			if (params.length == 1) {
				Number ruleValue = params[0];
				if (isLessThan()) {
					if (isEqual()) {
						return MathUtils.isLessThanOrEqual(fieldValue, ruleValue);
					}
					return MathUtils.isLessThan(fieldValue, ruleValue);
				}
				if (isGreaterThan()) {
					if (isEqual()) {
						return MathUtils.isGreaterThanOrEqual(fieldValue, ruleValue);
					}
					return MathUtils.isGreaterThan(fieldValue, ruleValue);
				}
			}
			else if (params.length == 2) {
				if (isNot()) {
					return !MathUtils.isBetween(fieldValue, params[0], params[1]);
				}
				return MathUtils.isBetween(fieldValue, params[0], params[1]);
			}
		}
		throw new RuntimeException("Unable to evaluate condition. Field value was null!");
	}


	@Override
	public Number[] getParams(String[] params, Map<String, Object> contextData) {
		if (params == null) {
			return null;
		}
		Number[] numberParams = new Number[params.length];
		for (int i = 0; i < params.length; i++) {
			numberParams[i] = (Number) DataTypeNameUtils.convertObjectToDataTypeName(params[i], getJsonCondition().getType().getDataTypeName());
		}
		return numberParams;
	}
}
