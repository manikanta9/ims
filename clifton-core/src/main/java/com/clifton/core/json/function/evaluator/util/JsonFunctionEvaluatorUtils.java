package com.clifton.core.json.function.evaluator.util;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.json.function.JsonFunctionTypes;
import com.clifton.core.json.function.evaluator.JsonFunctionEvaluator;
import com.clifton.core.util.CollectionUtils;

import java.util.Map;


/**
 * Used to properly evaluate, chain, and concatenate JsonFunctions.
 * Chained functions using the 'EVALUATE' condition utilize the result of their child function regardless of the field or value
 * set for that specific function call, functions within a 'CONCATENATE_SORT' group always use the specified field or value for executing the
 * function and then concatenate the results.
 *
 * @author StevenF
 */
public class JsonFunctionEvaluatorUtils {

	public static Object evaluateFunction(JsonFunction jsonFunction, Map<String, Object> functionData) {
		JsonFunctionEvaluator<?> jsonFunctionEvaluator = getJsonFunctionEvaluator(null, jsonFunction, functionData);
		return jsonFunctionEvaluator != null ? jsonFunctionEvaluator.evaluateFunction(functionData) : null;
	}


	public static JsonFunctionEvaluator<?> getJsonFunctionEvaluator(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Map<String, Object> functionData) {
		JsonFunctionTypes conditionType = jsonFunction.getCondition();
		if (conditionType != null) {
			for (JsonFunction childJsonFunction : CollectionUtils.getIterable(jsonFunction.getRules())) {
				JsonFunctionTypes childConditionType = childJsonFunction.getCondition();
				if (childConditionType != null) {
					jsonFunctionEvaluator = getJsonFunctionEvaluator(jsonFunctionEvaluator, childJsonFunction, functionData);
				}
				else {
					jsonFunctionEvaluator = childJsonFunction.getOperator().ofChain(jsonFunctionEvaluator, childJsonFunction, conditionType == JsonFunctionTypes.CONCATENATE);
				}
			}
		}
		else {
			jsonFunctionEvaluator = jsonFunction.getOperator().ofChain(jsonFunctionEvaluator, jsonFunction, false);
		}
		return jsonFunctionEvaluator;
	}
}
