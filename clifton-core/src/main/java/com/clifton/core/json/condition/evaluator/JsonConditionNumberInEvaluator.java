package com.clifton.core.json.condition.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Arrays;
import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionNumberInEvaluator extends BaseJsonConditionEvaluator<Number> {


	public JsonConditionNumberInEvaluator(JsonCondition jsonCondition, boolean not) {
		super(jsonCondition, not);
	}


	@Override
	public Boolean evaluateCondition(Number fieldValue, Number[] params, Map<String, Object> contextData) {
		if (fieldValue != null) {
			if (params == null) {
				if (isNot()) {
					return true;
				}
				return false;
			}
			if (isNot()) {
				return !CollectionUtils.contains(Arrays.asList(params), fieldValue);
			}
			return CollectionUtils.contains(Arrays.asList(params), fieldValue);
		}
		throw new RuntimeException("Unable to evaluate condition. Field value was null!");
	}


	@Override
	public Number[] getParams(String[] params, Map<String, Object> contextData) {
		if (params == null) {
			return null;
		}
		ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonCondition().getOperator().getName() + "] supports only one comma-separated parameter!");
		String[] inParams = params[0].split(",");
		Number[] numberParams = new Number[inParams.length];
		for (int i = 0; i < inParams.length; i++) {
			numberParams[i] = (Number) DataTypeNameUtils.convertObjectToDataTypeName(inParams[i], getJsonCondition().getType().getDataTypeName());
		}
		return numberParams;
	}
}
