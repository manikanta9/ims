package com.clifton.core.json.condition.evaluator;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.validation.ValidationUtils;
import org.apache.commons.lang.BooleanUtils;

import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionFreeMarkerEvaluator extends BaseJsonConditionEvaluator<Object> {

	private static final String FIELD_VALUE = "fieldValue";


	public JsonConditionFreeMarkerEvaluator(JsonCondition jsonCondition) {
		super(jsonCondition);
	}


	@Override
	public Boolean evaluateCondition(Object fieldValue, Object[] params, Map<String, Object> contextData) {
		if (fieldValue != null && params != null) {
			ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonCondition().getOperator().getName() + "] supports only one parameter!");
			FreemarkerTemplateConverter templateConverter = new FreemarkerTemplateConverter();
			TemplateConfig config = new TemplateConfig(String.valueOf(params[0]));
			config.addBeanToContext(FIELD_VALUE, fieldValue);
			return BooleanUtils.toBoolean(templateConverter.convert(config));
		}
		throw new RuntimeException("Unable to evaluate condition. Field value and/or params were null!");
	}


	@Override
	public Object[] getParams(String[] params, Map<String, Object> contextData) {
		return params;
	}
}
