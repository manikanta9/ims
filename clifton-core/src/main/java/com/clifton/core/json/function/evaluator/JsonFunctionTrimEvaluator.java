package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;


/**
 * @author StevenF
 */
public class JsonFunctionTrimEvaluator extends BaseJsonFunctionStringEvaluator {

	private boolean trimAll;


	public JsonFunctionTrimEvaluator(JsonFunctionEvaluator<String> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate, boolean trimAll) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
		this.trimAll = trimAll;
	}


	@Override
	public String evaluateFunction(String fieldValue, String[] params) {
		if (fieldValue != null) {
			if (isTrimAll()) {
				return StringUtils.trimAll(fieldValue);
			}
			else {
				return StringUtils.trim(fieldValue);
			}
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter & Setter Methods                 ////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isTrimAll() {
		return this.trimAll;
	}


	public void setTrimAll(boolean trimAll) {
		this.trimAll = trimAll;
	}
}
