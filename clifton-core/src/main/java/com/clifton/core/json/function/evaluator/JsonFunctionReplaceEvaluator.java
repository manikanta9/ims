package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.validation.ValidationException;


/**
 * @author StevenF
 */
public class JsonFunctionReplaceEvaluator extends BaseJsonFunctionStringEvaluator {

	public JsonFunctionReplaceEvaluator(JsonFunctionEvaluator<String> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public String evaluateFunction(String fieldValue, String[] params) {
		if (fieldValue != null) {
			if (params.length == 2) {
				return fieldValue.replace(params[0], params[1]);
			}
			else {
				return fieldValue.replace(params[0], "");
			}
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was null!");
	}
}
