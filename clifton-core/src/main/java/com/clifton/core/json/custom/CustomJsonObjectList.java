package com.clifton.core.json.custom;

import com.clifton.core.util.CollectionUtils;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Consumer;


/**
 * <code>CustomJsonObjectList</code> is a {@link CustomJsonObject} that represents a JSON object
 * with a single property for an array of generic entities (e.g. {entityList: [{...},...]}).
 * <p>
 * When the JSON string is deserialized, this object will represent the list of entities as
 * {@link Map}s containing the properties with their associated values. Nested entities in
 * the JSON will result in another map of property entries with the property name key.
 * <p>
 * A use of this object is for dynamic form fields, such as system bean properties, that
 * are of the data type TABLE. The table value represents generic entities that the bean
 * can interpret.
 *
 * @author nickk
 */
public class CustomJsonObjectList extends BaseCustomJsonObject implements Iterable<Map<String, Object>> {

	private List<Map<String, Object>> entityList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public CustomJsonObjectList() {
		// No-arg constructor for web binding
		super();
	}


	public CustomJsonObjectList(String jsonValue) {
		super(jsonValue);
	}


	@Override
	public Class<?> getSerializationObjectClass() {
		return CustomJsonObjectList.class;
	}


	@Override
	public Iterator<Map<String, Object>> iterator() {
		return isEmpty() ? Collections.emptyIterator() : getEntityList().iterator();
	}


	@Override
	public void forEach(Consumer<? super Map<String, Object>> action) {
		if (!isEmpty()) {
			getEntityList().forEach(action);
		}
	}


	@Override
	public Spliterator<Map<String, Object>> spliterator() {
		return isEmpty() ? Spliterators.emptySpliterator() : getEntityList().spliterator();
	}


	public boolean isEmpty() {
		return CollectionUtils.isEmpty(getEntityList());
	}


	public int getSize() {
		return CollectionUtils.getSize(getEntityList());
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public List<Map<String, Object>> getEntityList() {
		return this.entityList;
	}


	public void setEntityList(List<Map<String, Object>> entityList) {
		this.entityList = entityList;
	}
}
