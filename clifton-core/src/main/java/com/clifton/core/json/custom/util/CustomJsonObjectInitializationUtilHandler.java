package com.clifton.core.json.custom.util;

import com.clifton.core.json.custom.CustomJsonObject;


/**
 * <code>CustomJsonObjectInitializationUtilHandler</code> is an optional handler that has pre and
 * post processing hooks around object JSON (de)serialization.
 *
 * @author nickk
 */
public interface CustomJsonObjectInitializationUtilHandler<T extends CustomJsonObject> {

	/**
	 * Returns the {@link CustomJsonObject} class type to register this initialization handler for.
	 */
	Class<T> getCustomJsonObjectClass();


	/**
	 * Called to pre-process an object before it is serialized as JSON.
	 * <p>
	 * A use case could be to set FK fields instead of full objects for serialization.
	 */
	void prepareObjectForJsonSerialization(T customJsonObject);


	/**
	 * Called after JSON is deserialized into object.
	 * <p>
	 * The object can hydrate objects from FK fields if full objects were not serialized.
	 */
	void postInitializeObjectAfterJsonDeserialization(T customJsonObject);
}
