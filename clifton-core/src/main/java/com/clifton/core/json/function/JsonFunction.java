package com.clifton.core.json.function;


import com.clifton.core.json.JsonExpression;
import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;


/**
 * The <code>JsonFunction</code>
 * Functions support manipulation of data, such as 'trim' and 'substring', 'negate', and 'add'
 * Allows chaining of functions through CONCATENATE/EVALUATE sequences and will return an object of the same
 * type evaluated; conversion to other types would be handled outside the function evaluation at this time.
 *
 * @author StevenF
 */
public class JsonFunction extends JsonExpression<JsonFunction, JsonDataTypes, JsonInputTypes, JsonFunctionOperatorTypes, JsonFunctionTypes> {


}
