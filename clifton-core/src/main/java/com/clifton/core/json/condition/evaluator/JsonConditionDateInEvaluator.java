package com.clifton.core.json.condition.evaluator;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @author StevenF
 */
public class JsonConditionDateInEvaluator extends BaseJsonConditionEvaluator<Date> {

	//Pattern to extract the parameter value when date fieldValue is defined using a relative function
	//e.g. TODAY(-1) will allow a pattern matching to return '-1' at matcher group 1
	private static final Pattern RELATIVE_DATE_FUNCTION_PARAM_PATTERN = Pattern.compile("DATE\\((.*?)\\)");


	public JsonConditionDateInEvaluator(JsonCondition jsonCondition, boolean not) {
		super(jsonCondition, not);
	}


	@Override
	public Boolean evaluateCondition(Date fieldValue, Date[] params, Map<String, Object> contextData) {
		if (fieldValue != null) {
			if (params == null) {
				if (isNot()) {
					return true;
				}
				return false;
			}
			if (isNot()) {
				return !CollectionUtils.contains(Arrays.asList(params), fieldValue);
			}
			return CollectionUtils.contains(Arrays.asList(params), fieldValue);
		}
		throw new RuntimeException("Unable to evaluate condition. Field value was null!");
	}


	@Override
	@SuppressWarnings("ConstantConditions")
	public Date[] getParams(String[] params, Map<String, Object> contextData) {
		if (params == null) {
			return null;
		}
		ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonCondition().getOperator().getName() + "] supports only one comma-separated parameter!");
		String[] inParams = params[0].split(",");
		Date[] dateParams = new Date[inParams.length];
		for (int i = 0; i < inParams.length; i++) {
			String dateParam = inParams[i];
			Matcher matcher = RELATIVE_DATE_FUNCTION_PARAM_PATTERN.matcher(dateParam);
			if (matcher.find()) {
				Object relativeDateObject = contextData.get(JsonConditionEvaluator.CONTEXT_RELATIVE_DATE);
				AssertUtils.assertTrue(relativeDateObject instanceof Date, "Relative date for use with 'DATE()' function not found!");
				dateParams[i] = DateUtils.addWeekDays((Date) relativeDateObject, Integer.parseInt(matcher.group(1)));
			}
			else {
				dateParams[i] = (Date) DataTypeNameUtils.convertObjectToDataTypeName(inParams[i], getJsonCondition().getType().getDataTypeName());
			}
		}
		return dateParams;
	}
}
