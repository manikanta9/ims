package com.clifton.core.json.custom;

/**
 * <code>CustomJsonObjectStates</code> defines various states a {@link CustomJsonObject} can be in between its persisted JSON and object and its properties for use.
 *
 * @author nickk
 */
public enum CustomJsonObjectStates {

	/**
	 * JSON is populated from persisted storage but the object properties have not been initialized
	 */
	UNINITIALIZED,
	/**
	 * Object properties are being populated form JSON
	 */
	INITIALIZING,
	/**
	 * Object has been fully initialized from JSOn and all properties are set
	 */
	INITIALIZED,
	/**
	 * Object has been modified since initialized and has properties to be persisted in JSON
	 */
	MODIFIED
}
