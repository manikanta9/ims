package com.clifton.core.json.type;

import com.clifton.core.shared.dataaccess.DataTypeNames;


/**
 * @author StevenF
 */
public enum JsonDataTypes {
	STRING("string", DataTypeNames.STRING),
	DECIMAL("double", DataTypeNames.DECIMAL),
	INTEGER("integer", DataTypeNames.INTEGER),
	DATE("date", DataTypeNames.DATE),
	BOOLEAN("boolean", DataTypeNames.BOOLEAN);

	////////////////////////////////////////////////////////////////////////////

	private String name;
	private DataTypeNames dataTypeName;


	@Override
	public String toString() {
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////


	JsonDataTypes(String name, DataTypeNames dataTypeName) {
		this.name = name;
		this.dataTypeName = dataTypeName;
	}


	public String getName() {
		return this.name;
	}


	public DataTypeNames getDataTypeName() {
		return this.dataTypeName;
	}
}
