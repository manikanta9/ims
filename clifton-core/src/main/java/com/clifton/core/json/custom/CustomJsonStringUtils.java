package com.clifton.core.json.custom;

import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.JsonStrategy;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.apache.commons.beanutils.PropertyUtils;

import java.util.HashMap;
import java.util.Map;


/**
 * @author manderson
 * @author michaelm
 */
public class CustomJsonStringUtils {


	/**
	 * Populated in {@link CustomJsonStringStaticContextInitializer}
	 */
	protected static final ObjectWrapper<JsonHandler<JsonStrategy>> JSON_HANDLER_OBJECT_WRAPPER = new ObjectWrapper<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private CustomJsonStringUtils() {
		// hide implicit public constructor
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static CustomJsonString initializeCustomJsonStringJsonValueMap(CustomJsonString customJsonString) {
		return initializeCustomJsonStringJsonValueMap(customJsonString, false);
	}


	public static CustomJsonString initializeCustomJsonStringJsonValueMap(CustomJsonString customJsonString, boolean includeNestedMaps) {
		if (customJsonString == null) {
			customJsonString = new CustomJsonString("");
		}
		if (customJsonString.jsonValueMap() == null) {
			customJsonString.initializeMap(getCustomJsonStringAsMap(customJsonString, includeNestedMaps));
		}
		return customJsonString;
	}


	public static void resetCustomJsonStringFromJsonValueMap(CustomJsonString customJsonString) {
		validateJsonHandler();
		if (customJsonString != null) {
			if (CollectionUtils.isEmpty(customJsonString.jsonValueMap())) {
				customJsonString.setJsonValue(null);
			}
			else {
				customJsonString.setJsonValue(JSON_HANDLER_OBJECT_WRAPPER.getObject().toJson(customJsonString.jsonValueMap()));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Useful when you need the map but don't need it on the object
	 * Example: Comparing values for Auditing
	 */
	public static Map<String, Object> getCustomJsonStringAsMap(CustomJsonString customJsonString) {
		return getCustomJsonStringAsMap(customJsonString, false);
	}


	/**
	 * Because <code>SystemDataType.TABLE</code> fields are saved as strings, inner maps within a {@link CustomJsonString} will be deserialized as strings
	 * when the Json value is converted to a map. If <code>includeNestedMaps</code> is true, then this method will check for nested maps and deserialize them
	 * as maps instead.
	 */
	@SuppressWarnings("unchecked")
	public static Map<String, Object> getCustomJsonStringAsMap(CustomJsonString customJsonString, boolean includeNestedMaps) {
		validateJsonHandler();
		if (customJsonString == null || StringUtils.isEmpty(customJsonString.getJsonValue()) || "null".equals(customJsonString.getJsonValue())) {
			return new HashMap<>();
		}
		Map<String, Object> finalMap = (Map<String, Object>) JSON_HANDLER_OBJECT_WRAPPER.getObject().fromJson(customJsonString.getJsonValue(), Map.class);
		if (!includeNestedMaps) {
			return finalMap;
		}
		//Attempt to deserialize first layer of nested JSON objects
		Map<String, Object> nestedMap = new HashMap<>();
		for (Map.Entry<String, Object> entry : finalMap.entrySet()) {
			Object value = entry.getValue();
			try {
				Object innerMap = JSON_HANDLER_OBJECT_WRAPPER.getObject().fromJson(value.toString(), Object.class);
				nestedMap.put(entry.getKey(), innerMap);
			}
			catch (Exception e) {
				//Since we don't have a way to eagerly determine if a value is valid JSON, we try to parse it, and if
				//it isn't valid we'll get an exception and just add the original value to the map
				nestedMap.put(entry.getKey(), value);
			}
		}
		return nestedMap;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Convenience method.  Custom columns don't differentiate short vs. long but often these can be lookup table ids
	 * and shorts are commonly used
	 */
	public static Short getColumnValueAsShort(CustomJsonString customJsonString, String columnName) {
		Object value = getColumnValue(customJsonString, columnName);
		if (value != null) {
			if (value instanceof String) {
				return Short.parseShort((String) value);
			}
			else if (value instanceof Number) {
				return ((Number) value).shortValue();
			}
		}
		return null;
	}


	public static Integer getColumnValueAsInteger(CustomJsonString customJsonString, String columnName) {
		Object value = getColumnValue(customJsonString, columnName);
		if (value != null) {
			if (value instanceof String) {
				return Integer.parseInt((String) value);
			}
			else if (value instanceof Number) {
				return ((Number) value).intValue();
			}
		}
		return null;
	}


	/**
	 * Default return will always be false, so if the CustomJsonString is null or doesn't have a value for the column name the result will be false
	 * Otherwise uses BooleanUtils.isTrue on the resulting object
	 */
	public static Boolean getColumnValueAsBoolean(CustomJsonString customJsonString, String columnName) {
		return BooleanUtils.isTrue(getColumnValue(customJsonString, columnName));
	}


	public static String getColumnValueAsString(CustomJsonString customJsonString, String columnName) {
		Object value = getColumnValue(customJsonString, columnName);
		if (value != null) {
			if (value instanceof String) {
				return (String) value;
			}
			return value.toString();
		}
		return null;
	}


	public static <T> T getColumnValueAsObject(CustomJsonString customJsonString, String columnName, Class<T> clazz) {
		if (customJsonString == null) {
			return null;
		}
		Object columnValue = getColumnValue(customJsonString, columnName);
		return columnValue == null ? null : (T) JSON_HANDLER_OBJECT_WRAPPER.getObject().convertValue(customJsonString.getColumnValue(columnName), clazz);
	}


	public static Object getColumnValue(CustomJsonString customJsonString, String columnName) {
		if (customJsonString == null) {
			return null;
		}
		// Ensure map is available before trying to read from it
		initializeCustomJsonStringJsonValueMap(customJsonString);
		return customJsonString.getColumnValue(columnName);
	}


	public static Object getColumnText(CustomJsonString customJsonString, String columnName) {
		if (customJsonString == null) {
			return null;
		}
		// Ensure map is available before trying to read from it
		initializeCustomJsonStringJsonValueMap(customJsonString);
		return customJsonString.getColumnText(columnName);
	}


	public static Object getProperty(CustomJsonString customJsonString, String propertyPath) {
		if (customJsonString == null) {
			return null;
		}
		// Ensure map is available before trying to read from it
		initializeCustomJsonStringJsonValueMap(customJsonString, true);

		//add dot before brackets so that PropertyUtils will parse the property names correctly
		propertyPath = StringUtils.replace(propertyPath, "[", ".[");
		try {
			return PropertyUtils.getProperty(customJsonString.jsonValueMap(), propertyPath);
		}
		catch (Exception e) {
			return null;
		}
	}


	/**
	 * Used to set the value only.
	 * If value exists and is a map, will replace value only and will leave text as it was.
	 */
	public static void setColumnValue(CustomJsonString customJsonString, String columnName, Object columnValueObject) {
		initializeCustomJsonStringJsonValueMap(customJsonString);
		if (customJsonString != null) {
			customJsonString.setColumnValue(columnName, columnValueObject, null);
		}
		resetCustomJsonStringFromJsonValueMap(customJsonString);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static void validateJsonHandler() {
		ValidationUtils.assertNotNull(JSON_HANDLER_OBJECT_WRAPPER.getObject(), "Json Handler was not properly initialized.");
	}
}
