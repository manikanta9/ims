package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class JsonFunctionMultiplyNumberEvaluator extends BaseJsonFunctionNumberEvaluator {


	public JsonFunctionMultiplyNumberEvaluator(JsonFunctionEvaluator<Number> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public Number evaluateFunction(Number fieldValue, String[] params) {
		if (fieldValue != null && params != null) {
			ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonFunction().getOperator().getName() + "] supports only one parameter!");
			String param = convertFieldParameter(params[0]);
			if (fieldValue instanceof BigDecimal) {
				BigDecimal ruleValue = new BigDecimal(param);
				return MathUtils.multiply((BigDecimal) fieldValue, ruleValue);
			}
			else if (fieldValue instanceof Double) {
				return Double.valueOf(param) * fieldValue.doubleValue();
			}
			else {
				return Integer.valueOf(param) * fieldValue.intValue();
			}
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] or params [" + ArrayUtils.toString(params) + "] was null!");
	}
}
