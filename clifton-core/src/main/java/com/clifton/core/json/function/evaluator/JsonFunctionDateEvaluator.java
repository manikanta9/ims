package com.clifton.core.json.function.evaluator;

import com.clifton.core.json.function.JsonFunction;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;


/**
 * @author StevenF
 */
public class JsonFunctionDateEvaluator extends BaseJsonFunctionDateEvaluator {

	public JsonFunctionDateEvaluator(JsonFunctionEvaluator<Object> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		super(jsonFunctionEvaluator, jsonFunction, concatenate);
	}


	@Override
	public String evaluateFunction(Object fieldValue, String[] params) {
		if (fieldValue instanceof Date) {
			ValidationUtils.assertTrue(params.length == 1, "Operation [" + getJsonFunction().getOperator().getName() + "] supports only one parameter!");
			return DateUtils.fromDate((Date) fieldValue, params[0]);
		}
		throw new ValidationException("Value for field [" + getJsonFunction().getField() + "] was not a date or null!");
	}
}
