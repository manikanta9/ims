package com.clifton.core.json.condition;

/**
 * The <code>JsonConditionTypes</code> defines the applicable conditions used when evaluating a condition
 * e.g., conditions can be chained and evaluated in groups of AND/OR conditions;
 * primarily the enum is used to drive the display of the UI and what options are available.
 *
 * @author StevenF
 */
public enum JsonConditionTypes {

	//'Condition Rules' condition types.
	AND("AND"),
	OR("OR");

	////////////////////////////////////////////////////////////////////////////

	private String name;


	@Override
	public String toString() {
		return getName();
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	JsonConditionTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}
