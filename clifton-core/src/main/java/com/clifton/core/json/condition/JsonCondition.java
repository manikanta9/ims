package com.clifton.core.json.condition;


import com.clifton.core.json.JsonExpression;
import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;


/**
 * The <code>JsonCondition</code>
 * Implements numerous conditional operators such as greater than, equals, less than, contains, etc.
 * Allows chaining of conditions through AND/OR sequences, and will always return a boolean result.
 *
 * @author StevenF
 */
public class JsonCondition extends JsonExpression<JsonCondition, JsonDataTypes, JsonInputTypes, JsonConditionOperatorTypes, JsonConditionTypes> {

}
