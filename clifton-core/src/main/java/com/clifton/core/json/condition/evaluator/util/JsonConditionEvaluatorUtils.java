package com.clifton.core.json.condition.evaluator.util;

import com.clifton.core.json.condition.JsonCondition;
import com.clifton.core.json.condition.JsonConditionTypes;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author StevenF
 */
public class JsonConditionEvaluatorUtils {

	public static boolean evaluateCondition(JsonCondition jsonCondition, Map<String, Object> conditionData) {
		return evaluateCondition(jsonCondition, conditionData, new HashMap<>());
	}


	public static boolean evaluateCondition(JsonCondition jsonCondition, Map<String, Object> conditionData, Map<String, Object> contextData) {
		JsonConditionTypes conditionType = jsonCondition.getCondition();
		if (conditionType != null) {
			return evaluateConditionList(conditionType, jsonCondition.getRules(), conditionData, contextData);
		}
		return BooleanUtils.isTrue(jsonCondition.getOperator().newInstance(jsonCondition).evaluateCondition(conditionData, contextData));
	}


	private static boolean evaluateConditionList(JsonConditionTypes conditionType, List<JsonCondition> jsonConditionList, Map<String, Object> conditionData, Map<String, Object> contextData) {
		boolean returnVal = true;
		if (!CollectionUtils.isEmpty(jsonConditionList)) {
			if (conditionType == JsonConditionTypes.AND) {
				returnVal = evaluateAndConditions(jsonConditionList, conditionData, contextData);
			}
			else {
				returnVal = evaluateOrConditions(jsonConditionList, conditionData, contextData);
			}
		}
		return returnVal;
	}


	private static boolean evaluateAndConditions(List<JsonCondition> jsonConditionList, Map<String, Object> conditionData, Map<String, Object> contextData) {
		boolean returnVal = CollectionUtils.isEmpty(jsonConditionList);
		for (JsonCondition jsonCondition : CollectionUtils.getIterable(jsonConditionList)) {
			JsonConditionTypes conditionType = jsonCondition.getCondition();
			if (conditionType != null) {
				returnVal = evaluateConditionList(conditionType, jsonCondition.getRules(), conditionData, contextData);
				if (!returnVal) {
					return false;
				}
			}
			returnVal = evaluateCondition(jsonCondition, conditionData, contextData);
			if (!returnVal) {
				return false;
			}
		}
		return returnVal;
	}


	private static boolean evaluateOrConditions(List<JsonCondition> jsonConditionList, Map<String, Object> conditionData, Map<String, Object> contextData) {
		boolean returnVal = CollectionUtils.isEmpty(jsonConditionList);
		for (JsonCondition jsonCondition : CollectionUtils.getIterable(jsonConditionList)) {
			boolean evaluation = false;
			JsonConditionTypes conditionType = jsonCondition.getCondition();
			if (conditionType != null) {
				evaluation = evaluateConditionList(conditionType, jsonCondition.getRules(), conditionData, contextData);
			}
			returnVal = returnVal || evaluation;
			evaluation = evaluateCondition(jsonCondition, conditionData, contextData);
			returnVal = returnVal || evaluation;
		}
		return returnVal;
	}
}
