package com.clifton.core.json.custom;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.json.custom.util.CustomJsonObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Objects;


/**
 * <code>BaseCustomJsonObject</code> is an abstract {@link CustomJsonObject} for handling the JSON value that can be persisted to the database.
 *
 * @author nickk
 */
public abstract class BaseCustomJsonObject implements CustomJsonObject {

	@JsonIgnore // ignore this field from being included in the serialized JSON
	protected String jsonValue;

	@JsonIgnore
	@NonPersistentField
	protected CustomJsonObjectStates state;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public BaseCustomJsonObject() {
		// no-arg constructor for use of using jsonValue setter
		this.state = CustomJsonObjectStates.UNINITIALIZED;
	}


	public BaseCustomJsonObject(String jsonValue) {
		this();
		setJsonValue(jsonValue);
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	protected void initializeObject() {
		if (this.state == CustomJsonObjectStates.UNINITIALIZED) {
			this.state = CustomJsonObjectStates.INITIALIZING;
			initializeObjectFromJson();
			this.state = CustomJsonObjectStates.INITIALIZED;
		}
	}


	protected void initializeObjectFromJson() {
		CustomJsonObjectUtils.initializeObjectFromJson(this);
	}


	protected void updateJsonFromObject() {
		CustomJsonObjectUtils.updateJsonFromObject(this);
	}


	protected void updateModified(Object currentValue, Object newValue) {
		if (!CompareUtils.isEqual(currentValue, newValue)) {
			this.state = CustomJsonObjectStates.MODIFIED;
		}
	}


	@Override
	public boolean equals(Object o) {
		if (o == null || !getClass().equals(o.getClass())) {
			return false;
		}
		return StringUtils.isEqual(getJsonValue(), ((CustomJsonObject) o).getJsonValue());
	}


	@Override
	public int hashCode() {
		return Objects.hash(getJsonValue());
	}


	@Override
	public String toString() {
		return getJsonValue();
	}


	@Override
	public Class<?> getSerializationObjectClass() {
		return this.getClass();
	}


	@Override
	public Object toObjectForSerialization() {
		return this;
	}


	@Override
	public void applyDeserializedObject(Object object) {
		if (object != null && getSerializationObjectClass().isInstance(object)) {
			BeanUtils.copyProperties(object, this, new String[]{"jsonValue"});
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getJsonValue() {
		if (this.state == CustomJsonObjectStates.MODIFIED) {
			this.state = CustomJsonObjectStates.INITIALIZING;
			updateJsonFromObject();
			this.state = CustomJsonObjectStates.INITIALIZED;
		}
		return this.jsonValue;
	}


	@Override
	public void setJsonValue(String jsonValue) {
		this.jsonValue = jsonValue;
		this.state = CustomJsonObjectStates.UNINITIALIZED;
	}
}
