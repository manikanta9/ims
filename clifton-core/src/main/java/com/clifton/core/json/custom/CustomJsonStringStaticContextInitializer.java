package com.clifton.core.json.custom;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.JsonStrategy;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


/**
 * @author manderson
 */
@Component
public class CustomJsonStringStaticContextInitializer implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			JsonHandler<JsonStrategy> jsonHandler = (JsonHandler<JsonStrategy>) event.getApplicationContext().getBean("jsonHandler");
			CustomJsonStringUtils.JSON_HANDLER_OBJECT_WRAPPER.setObject(jsonHandler);
		}
		catch (NoSuchBeanDefinitionException e) {
			LogUtils.warn(LogCommand.ofMessage(getClass(), "Missing jsonHandler bean in application context."));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
