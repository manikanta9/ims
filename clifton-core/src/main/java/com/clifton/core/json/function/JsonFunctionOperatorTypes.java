package com.clifton.core.json.function;

import com.clifton.core.json.function.evaluator.JsonFunctionAbsoluteValueNumberEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionAddNumberEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionDateEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionDivideNumberEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionFieldEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionFreeMarkerEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionMultiplyNumberEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionNegateNumberEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionReplaceEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionStringCaseEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionSubstringEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionTrimEvaluator;
import com.clifton.core.json.function.evaluator.JsonFunctionValueEvaluator;


/**
 * The <code>JsonFunctionOperatorTypes</code> defines functions available
 * see: <code>ReconciliationRule</code> for additional details
 *
 * @author StevenF
 */
public enum JsonFunctionOperatorTypes {

	////////////////////////////////////////////////////////////////////////////
	/////////                   FUNCTION OPERATORS                  ////////////
	////////////////////////////////////////////////////////////////////////////

	SUBSTRING("substring") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionSubstringEvaluator(null, jsonFunction, concatenate, false, false);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionSubstringEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, false, false);
		}
	},
	LEFT("left") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionSubstringEvaluator(null, jsonFunction, concatenate, true, false);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionSubstringEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, true, false);
		}
	},
	RIGHT("right") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionSubstringEvaluator(null, jsonFunction, concatenate, false, true);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionSubstringEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, false, true);
		}
	},
	UPPER("upper") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionStringCaseEvaluator(null, jsonFunction, concatenate, true);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionStringCaseEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, true);
		}
	},
	LOWER("lower") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionStringCaseEvaluator(null, jsonFunction, concatenate, false);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionStringCaseEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, false);
		}
	},
	TRIM("trim") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionTrimEvaluator(null, jsonFunction, concatenate, false);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionTrimEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, false);
		}
	},
	TRIM_ALL("trim_all") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionTrimEvaluator(null, jsonFunction, concatenate, true);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionTrimEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate, true);
		}
	},
	REPLACE("replace") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionReplaceEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> ruleEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionReplaceEvaluator((JsonFunctionEvaluator<String>) ruleEvaluator, jsonFunction, concatenate);
		}
	},
	FIELD("select_field") {
		@Override
		public JsonFunctionEvaluator<Object> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionFieldEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionFieldEvaluator((JsonFunctionEvaluator<Object>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	VALUE("input_value") {
		@Override
		public JsonFunctionEvaluator<String> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionValueEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionValueEvaluator((JsonFunctionEvaluator<String>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	NEGATE("negate") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionNegateNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionNegateNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	ABSOLUTE_VALUE("absolute_value") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {

			return new JsonFunctionAbsoluteValueNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionAbsoluteValueNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	ADD("add") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionAddNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionAddNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	ADD_FIELD("add_field") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionAddNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionAddNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	MULTIPLY("multiply") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionMultiplyNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionMultiplyNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	MULTIPLY_FIELD("multiply_field") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionMultiplyNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionMultiplyNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	DIVIDE("divide") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionDivideNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionDivideNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	DIVIDE_FIELD("divide_field") {
		@Override
		public JsonFunctionEvaluator<Number> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionDivideNumberEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionDivideNumberEvaluator((JsonFunctionEvaluator<Number>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	DATE_TO_STRING("date_to_string") {
		@Override
		public JsonFunctionEvaluator<Object> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionDateEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionDateEvaluator((JsonFunctionEvaluator<Object>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	},
	FREE_MARKER("free_marker") {
		@Override
		public JsonFunctionEvaluator<Object> newInstance(JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionFreeMarkerEvaluator(null, jsonFunction, concatenate);
		}


		@Override
		@SuppressWarnings("unchecked")
		public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
			return new JsonFunctionFreeMarkerEvaluator((JsonFunctionEvaluator<Object>) jsonFunctionEvaluator, jsonFunction, concatenate);
		}
	};


	public abstract JsonFunctionEvaluator<?> newInstance(JsonFunction jsonFunction, Boolean concatenate);


	@SuppressWarnings("unused")
	//Default implementation simply calls newInstance - this is overridden by 'chainable' functions.
	public JsonFunctionEvaluator<?> ofChain(JsonFunctionEvaluator<?> jsonFunctionEvaluator, JsonFunction jsonFunction, Boolean concatenate) {
		return newInstance(jsonFunction, concatenate);
	}

	////////////////////////////////////////////////////////////////////////////

	private String name;


	@Override
	public String toString() {
		return getName();
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	JsonFunctionOperatorTypes(String name) {
		this.name = name;
	}


	public String getName() {
		return this.name;
	}
}

