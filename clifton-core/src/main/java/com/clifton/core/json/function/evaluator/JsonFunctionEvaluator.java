package com.clifton.core.json.function.evaluator;

import java.util.Map;


/**
 * @author StevenF
 */
public interface JsonFunctionEvaluator<R> {

	public R evaluateFunction(Map<String, Object> functionData);


	public boolean isConcatenate();
}
