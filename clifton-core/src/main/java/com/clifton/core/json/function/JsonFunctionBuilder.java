package com.clifton.core.json.function;


import com.clifton.core.json.type.JsonDataTypes;
import com.clifton.core.json.type.JsonInputTypes;

import java.util.List;


/**
 * @author StevenF
 */
public class JsonFunctionBuilder {

	private String id;
	private String field;
	private Object value;

	//Additional data that can be used in the rule.
	//This is not the same as the actual ruleData passed in when evaluating.
	private Object data;

	private JsonDataTypes type;
	private JsonInputTypes input;
	private JsonFunctionOperatorTypes operator;

	private JsonFunctionTypes condition;
	private List<JsonFunction> rules;

	////////////////////////////////////////////////////////////////////////////


	public JsonFunctionBuilder concatCondition(List<JsonFunction> concatConditions) {
		this.condition = JsonFunctionTypes.CONCATENATE;
		this.rules = concatConditions;
		return this;
	}


	public JsonFunctionBuilder withId(String id) {
		this.id = id;
		return this;
	}


	public JsonFunctionBuilder withField(String field) {
		this.field = field;
		return this;
	}


	public JsonFunctionBuilder withValue(Object value) {
		this.value = value;
		return this;
	}


	public JsonFunctionBuilder ofDataType(JsonDataTypes type) {
		this.type = type;
		return this;
	}


	public JsonFunctionBuilder ofInputType(JsonInputTypes input) {
		this.input = input;
		return this;
	}


	public JsonFunctionBuilder ofOperatorType(JsonFunctionOperatorTypes operator) {
		this.operator = operator;
		return this;
	}


	public JsonFunctionBuilder withData(Object data) {
		this.data = data;
		return this;
	}


	public JsonFunction build() {
		JsonFunction jsonFunction = new JsonFunction();
		jsonFunction.setId(getId());
		jsonFunction.setField(getField());
		jsonFunction.setValue(getValue());
		jsonFunction.setData(getData());
		jsonFunction.setType(getType());
		jsonFunction.setInput(getInput());
		jsonFunction.setOperator(getOperator());
		jsonFunction.setCondition(getCondition());
		jsonFunction.setRules(getRules());
		return jsonFunction;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getId() {
		return this.id;
	}


	public void setId(String id) {
		this.id = id;
	}


	public String getField() {
		return this.field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public Object getValue() {
		return this.value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public Object getData() {
		return this.data;
	}


	public void setData(Object data) {
		this.data = data;
	}


	public JsonDataTypes getType() {
		return this.type;
	}


	public void setType(JsonDataTypes type) {
		this.type = type;
	}


	public JsonInputTypes getInput() {
		return this.input;
	}


	public void setInput(JsonInputTypes input) {
		this.input = input;
	}


	public JsonFunctionOperatorTypes getOperator() {
		return this.operator;
	}


	public void setOperator(JsonFunctionOperatorTypes operator) {
		this.operator = operator;
	}


	public JsonFunctionTypes getCondition() {
		return this.condition;
	}


	public void setCondition(JsonFunctionTypes condition) {
		this.condition = condition;
	}


	public List<JsonFunction> getRules() {
		return this.rules;
	}


	public void setRules(List<JsonFunction> rules) {
		this.rules = rules;
	}
}
