package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;


/**
 * The <code>MathComplexExpression</code> is used for applying multiple different math function on 3 or more fields.  i.e. (Field1 + Field2 - Field3)
 * i.e. For Billing Invoices, unpaid amount = TotalAmount + Broker Fee - Paid Amount  (we add Broker Fee because it's already negative value)
 * <p/>
 * This is currently used explicitly as an expression added to criteria and not added via search form field types.  If this becomes more advanced would perhaps need to define parenthesis usage, but that is out of scope for now
 *
 * @author manderson
 */
public class MathComplexExpression implements Criterion {

	private final String[] mathFunctions;

	private final String comparisonExpression;

	private final String[] propertyNames;

	private final Object value;

	/**
	 * If true, will do ISNULL(Value, 0) for each field to prevent math with NULL that returns NULL
	 */
	private final boolean supportNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MathComplexExpression(String[] mathFunctions, boolean supportNull, String comparisonExpression, String[] propertyNames, Object value) {
		ValidationUtils.assertTrue(propertyNames.length > 2, "MathComplexExpression only applies to math expressions with more than 2 properties");
		ValidationUtils.assertTrue(mathFunctions.length == (propertyNames.length - 1), "Math functions should be one less than the number of properties.");
		this.mathFunctions = mathFunctions;
		this.supportNull = supportNull;
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.value = value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragment = new StringBuilder("(");
		for (int i = 0; i < this.propertyNames.length; i++) {
			if (this.supportNull) {
				fragment.append(SQLIfNullFunction.render(criteria, criteriaQuery, this.propertyNames[i], "0"));
			}
			else {
				fragment.append(criteriaQuery.getColumn(criteria, this.propertyNames[i]));
			}
			if (i < this.propertyNames.length - 1) {
				fragment.append(this.mathFunctions[i]);
			}
		}
		fragment.append(") ");
		fragment.append(this.comparisonExpression);
		// For cases like: LIKE '%' + ? + '%' we don't want to add a second ?, but we need to for cases like =, >, <, etc.
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragment.append(" ?");
		}
		return fragment.toString();
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[]{criteriaQuery.getTypedValue(criteria, this.propertyNames[0], this.value)};
	}
}
