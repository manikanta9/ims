package com.clifton.core.dataaccess.file;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;


/**
 * The <code>DataTableUtils</code> contains helper functions for file to datatable conversion.
 *
 * @author StevenF
 */
public class DataTableUtils {

	public static DataTable convertFileToDataTable(MultipartFile multipartFile) {
		return convertFileToDataTable(FileUtils.convertMultipartFileToFile(multipartFile));
	}


	public static DataTable convertFileToDataTable(File file) {
		FileToDataTableConverter converter = null;
		String fileExtension = FileUtils.getFileExtension(file.getName());
		if (FileFormats.CSV.getFormatString().equals(fileExtension) || FileFormats.TXT.getFormatString().equalsIgnoreCase(fileExtension)) {
			converter = FileFormats.CSV.getFileConverter();
		}
		else if (FileFormats.EXCEL_OPEN_XML.getFormatString().equals(fileExtension)) {
			converter = FileFormats.EXCEL_OPEN_XML.getFileConverter();
		}
		else if (FileFormats.EXCEL.getFormatString().equals(fileExtension)) {
			converter = FileFormats.EXCEL.getFileConverter();
		}
		if (converter == null) {
			throw new ValidationException("Unsupported File Format!");
		}
		return converter.convert(file);
	}
}
