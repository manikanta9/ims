package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.engine.spi.QueryParameters;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.loader.criteria.CriteriaJoinWalker;
import org.hibernate.loader.criteria.CriteriaQueryTranslator;
import org.hibernate.persister.entity.OuterJoinLoadable;
import org.hibernate.type.Type;

import java.util.List;


/**
 * SubqueryUnionExpression is similar to Hibernate's {@link org.hibernate.criterion.SubqueryExpression}, except that it can take multiple DetachedCriteria and union them
 * UNIONS are more efficient in SQL server than using a lot of ORs
 * <p>
 * This is an abstract class, see {@link PropertySubqueryUnionExpression} for an existing example that uses Property Name IN (UNION QUERIES)
 *
 * @author manderson
 */
public abstract class SubqueryUnionExpression implements Criterion {

	private final String operator; // I.E. IN, EXISTS, ETC.

	/**
	 * If False, uses "UNION" if true, uses "UNION ALL"
	 */
	private final boolean unionAll;

	/**
	 * Built internally from the list of DetachedCriteria
	 */
	private final CriteriaImpl[] criteriaImplList;
	private QueryParameters[] queryParametersList;
	private CriteriaQueryTranslator[] innerQueryList;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected SubqueryUnionExpression(String operator, List<DetachedCriteria> detachedCriteriaList, boolean unionAll) {
		ValidationUtils.assertNotEmpty(detachedCriteriaList, "SubqueryUnionExpression requires detached criteria list.  Note: If only using one, should use SubqueryExpression directly.");
		this.criteriaImplList = new CriteriaImpl[detachedCriteriaList.size()];
		for (int i = 0; i < detachedCriteriaList.size(); i++) {
			DetachedCriteria detachedCriteria = detachedCriteriaList.get(i);
			// Note: DetachedCriteria - CriteriaImpl getter method is not available outside of hibernates package, so using getFieldValue to get the CriteriaImpl
			this.criteriaImplList[i] = BeanUtils.getFieldValue(detachedCriteria, "impl");
		}
		this.operator = operator;
		this.unionAll = unionAll;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Should be overridden by implementing class - i.e. builds beginning (i.e.) left side of expression
	 */
	protected abstract String toLeftSqlString(Criteria criteria, CriteriaQuery outerQuery);


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		final StringBuilder buf = new StringBuilder(toLeftSqlString(criteria, criteriaQuery));
		if (this.operator != null) {
			buf.append(' ').append(this.operator).append(' ');
		}
		buf.append('(');

		final SessionFactoryImplementor factory = criteriaQuery.getFactory();
		createAndSetInnerQueryList(criteriaQuery, factory);

		for (int i = 0; i < this.criteriaImplList.length; i++) {
			CriteriaImpl criteriaImpl = this.criteriaImplList[i];
			final OuterJoinLoadable persister =
					(OuterJoinLoadable) factory.getMetamodel().entityPersister((criteriaImpl.getEntityOrClassName()));

			CriteriaQueryTranslator innerQuery = this.innerQueryList[i];
			criteriaImpl.setSession(deriveRootSession(criteria));

			final CriteriaJoinWalker walker = new CriteriaJoinWalker(
					persister,
					innerQuery,
					factory,
					criteriaImpl,
					criteriaImpl.getEntityOrClassName(),
					criteriaImpl.getSession().getLoadQueryInfluencers(),
					innerQuery.getRootSQLALias()
			);
			buf.append(walker.getSQLString());
			if (i < (this.criteriaImplList.length - 1)) {
				if (this.unionAll) {
					buf.append(" UNION ALL ");
				}
				else {
					buf.append(" UNION ");
				}
			}
		}
		buf.append(')');
		return buf.toString();
	}


	/**
	 * NOTE: Copied from {@link org.hibernate.criterion.SubqueryExpression}
	 */
	private SharedSessionContractImplementor deriveRootSession(Criteria criteria) {
		if (criteria instanceof CriteriaImpl) {
			return ((CriteriaImpl) criteria).getSession();
		}
		else if (criteria instanceof CriteriaImpl.Subcriteria) {
			return deriveRootSession(((CriteriaImpl.Subcriteria) criteria).getParent());
		}
		else {
			// could happen for custom Criteria impls.  Not likely, but...
			// 		for long term solution, see HHH-3514
			return null;
		}
	}


	/**
	 * Returns the TypedValue objects to pass to the query
	 */
	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		final SessionFactoryImplementor factory = criteriaQuery.getFactory();
		createAndSetInnerQueryList(criteriaQuery, factory);

		TypedValue[] typedValues = new TypedValue[0];

		for (QueryParameters queryParameters : this.queryParametersList) {
			Type[] types = queryParameters.getPositionalParameterTypes();
			Object[] values = queryParameters.getPositionalParameterValues();
			TypedValue[] thisTypedValues = new TypedValue[types.length];
			for (int j = 0; j < types.length; j++) {
				thisTypedValues[j] = new TypedValue(types[j], values[j]);
			}
			typedValues = ArrayUtils.addAll(typedValues, thisTypedValues);
		}
		return typedValues;
	}


	/**
	 * Creates the inner query used to extract some useful information about query parameters, since it is needed in both methods.
	 */
	private void createAndSetInnerQueryList(CriteriaQuery criteriaQuery, SessionFactoryImplementor factory) {
		if (this.innerQueryList == null) {
			this.innerQueryList = new CriteriaQueryTranslator[this.criteriaImplList.length];
			this.queryParametersList = new QueryParameters[this.criteriaImplList.length];

			for (int i = 0; i < this.criteriaImplList.length; i++) {
				CriteriaImpl criteriaImpl = this.criteriaImplList[i];

				String alias;
				if (criteriaImpl.getAlias() == null) {
					alias = criteriaQuery.generateSQLAlias();
				}
				else {
					alias = criteriaImpl.getAlias() + "_";
				}

				CriteriaQueryTranslator innerQuery = new CriteriaQueryTranslator(
						factory,
						criteriaImpl,
						criteriaImpl.getEntityOrClassName(),
						alias,
						criteriaQuery
				);
				this.innerQueryList[i] = innerQuery;
				this.queryParametersList[i] = innerQuery.getQueryParameters();
			}
		}
	}
}
