package com.clifton.core.dataaccess.dao;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.SearchConfigurer;

import java.util.List;


/**
 * The <code>AdvancedReadOnlyDAO</code> interface adds generic search methods to Data Access Objects.
 *
 * @param <T>
 * @param <C> search criteria
 * @author vgomelsky
 */
public interface AdvancedReadOnlyDAO<T extends IdentityObject, C> extends ReadOnlyDAO<T> {

	/**
	 * Returns a List of DTO's by executing a configured namedQuery.
	 *
	 * @param queryName,       name configured of the query
	 * @param parameterValues, a varArg of Objects as input to the query
	 */
	public List<T> findByNamedQuery(final String queryName, final Object... parameterValues);


	/**
	 * Returns a List of DTO's that match the specified search criteria.
	 * This method supports pagination.
	 *
	 * @param searchConfigurer
	 */
	public List<T> findBySearchCriteria(SearchConfigurer<C> searchConfigurer);
}
