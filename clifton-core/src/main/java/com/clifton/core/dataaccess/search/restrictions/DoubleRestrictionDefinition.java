package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;


/**
 * The <code>DoubleRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link Double}.
 * Supports EQUALS, GREATER_THAN, LESS_THAN comparison conditions.
 *
 * @author vgomelsky
 */
public class DoubleRestrictionDefinition extends AbstractRestrictionDefinition {

	public DoubleRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public DoubleRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, false);
	}


	public DoubleRestrictionDefinition(String fieldName, String searchFieldName, boolean required) {
		this(fieldName, searchFieldName, null, required);
	}


	public DoubleRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, orderByFieldName, false);
	}


	public DoubleRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.GREATER_THAN,
				ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.EQUALS_OR_IS_NULL,
				ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_IS_NULL});
	}


	public DoubleRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                   ComparisonConditions[] comparisonConditions) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	@Override
	public Object getValue(Object rawValue) {
		if (rawValue == null) {
			return null;
		}
		if (rawValue instanceof Number) {
			return ((Number) rawValue).intValue();
		}
		return Double.parseDouble((String) rawValue);
	}
}
