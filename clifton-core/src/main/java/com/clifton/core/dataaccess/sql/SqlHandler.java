package com.clifton.core.dataaccess.sql;

import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.ResultSetExtractor;

import javax.sql.DataSource;
import java.util.Date;
import java.util.Map;


/**
 * The SqlHandler interface defines methods for executing SQL queries.  Whenever possible, avoid direct SQL access in
 * favor of DAO API.  DAO access is much cleaner, more flexible (observers) and can take advantage of ORM and caching.
 * Use direct SQL access only in rare unusual cases or where performance is very critical.
 *
 * @author vgomelsky
 */
public interface SqlHandler {

	/**
	 * Returns the DataSource used by this SQL handler.
	 */
	public DataSource getDataSource();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes the specified SELECT SQL statement via prepared statement and returns results generated using the specified Result Set extractor.
	 */
	public <T> T executeSelect(SqlSelectCommand sql, ResultSetExtractor<T> resultSetExtractor);


	/**
	 * Executes the specified SELECT SQL statement and returns results generated using the specified Result Set extractor.
	 */
	public <T> T executeSelect(String sql, ResultSetExtractor<T> resultSetExtractor);


	/**
	 * Executes the specified prepared statement and returns results generating using the specified Result Set extractor.
	 */
	public <T> T executeSelect(PreparedStatementCreator statementCreator, ResultSetExtractor<T> resultSetExtractor);


	/**
	 * Executes the specified prepared statement using the provided timeout and returns results generating using the specified Result Set extractor
	 */
	public <T> T executeSelect(PreparedStatementCreator statementCreator, ResultSetExtractor<T> resultSetExtractor, int timeout);


	/**
	 * Executes the specified SELECT SQL statement via prepared statement and returns the String result of the query.
	 * The query must return only 1 row and 1 column and this column must be of String data type.
	 */
	public String queryForString(SqlSelectCommand sql);


	/**
	 * Executes the specified SELECT SQL statement via prepared statement and returns the Date result of the query.
	 * The query must return only 1 row and 1 column and this column must be of Date data type.
	 */
	public Date queryForDate(SqlSelectCommand sql);


	/**
	 * Executes the specified SELECT SQL statement via prepared statement and returns the Object result of the query.
	 * The query must return only 1 row and 1 column.
	 */
	public Object queryForObject(SqlSelectCommand sql);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes a single SQL update operation (insert, update or delete) via prepared statement.
	 *
	 * @return the number of rows affected
	 */
	public int executeUpdate(SqlUpdateCommand sql);


	/**
	 * Executes a single SQL update operation (insert, update or delete).
	 *
	 * @return the number of rows affected
	 */
	public int executeUpdate(String sql);


	/**
	 * Executes a single update operation (insert, update or delete).
	 */
	public <T> T executeUpdate(String sql, SqlParameterValue[] params, PreparedStatementCallback<T> action);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes multiple SQL update operations (insert, update or delete) via a single prepared statement using batch updates.
	 *
	 * @return the number of rows affected by each operation
	 */
	public int[] executeBatchUpdate(String sql, BatchPreparedStatementSetter preparedStatementSetter);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Executes the specified SQL stored procedure using the specified command configuration.
	 */
	public Map<String, Object> executeStoredProcedure(SqlStoredProcedureCommand procedureCommand);
}
