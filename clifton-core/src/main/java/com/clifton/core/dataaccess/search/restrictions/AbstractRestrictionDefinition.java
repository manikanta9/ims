package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AbstractRestrictionDefinition</code> class is a helper class that can be extended by all
 * {@link SearchRestrictionDefinition} implementations.
 *
 * @author vgomelsky
 */
public abstract class AbstractRestrictionDefinition implements SearchRestrictionDefinition {

	private final List<ComparisonConditions> searchConditionList = new ArrayList<>();
	private final String fieldName;
	private final String searchFieldName;
	private final String searchFieldPath;
	private final String orderByFieldName;
	private final boolean required;
	private final boolean leftJoin;
	private final SearchFieldCustomTypes searchFieldCustomType;
	private Class<?> searchFieldDataType;

	/**
	 * this is intended to be {@link String#format(String, Object...)} compatible.
	 */
	private String sqlFormula;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AbstractRestrictionDefinition(String fieldName, ComparisonConditions[] allowedConditions) {
		this(fieldName, fieldName, allowedConditions);
	}


	public AbstractRestrictionDefinition(String fieldName, String searchFieldName, ComparisonConditions[] allowedConditions) {
		this(fieldName, searchFieldName, null, false, null, false, allowedConditions, null);
	}


	public AbstractRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, ComparisonConditions[] allowedConditions) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, false, allowedConditions, null);
	}


	public AbstractRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                     ComparisonConditions[] allowedConditions) {
		this(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, allowedConditions, null);
	}


	public AbstractRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                     ComparisonConditions[] allowedConditions, SearchFieldCustomTypes searchFieldCustomType) {
		this.fieldName = fieldName;
		this.searchFieldPath = searchFieldPath;
		this.orderByFieldName = orderByFieldName;
		this.required = required;
		this.leftJoin = leftJoin;

		this.searchFieldCustomType = (searchFieldCustomType == null ? SearchFieldCustomTypes.OR : searchFieldCustomType);
		if (SearchFieldCustomTypes.OR_IGNORE_SIGN_FOR_EQUALS == searchFieldCustomType) {
			this.searchFieldName = searchFieldName + "," + searchFieldName;
		}
		else {
			this.searchFieldName = searchFieldName;
		}

		int count = (allowedConditions == null) ? 0 : allowedConditions.length;
		AssertUtils.assertTrue(count > 0, "At least one allowedConditions is required.");
		for (int i = 0; i < count; i++) {
			addSearchCondition(allowedConditions[i]);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append('{');
		result.append(this.fieldName);
		result.append(", ");
		result.append(this.searchFieldName);
		result.append(", ");
		result.append(this.searchFieldPath);
		result.append('}');
		return result.toString();
	}


	@Override
	public void validate(SearchRestriction restriction) {
		if (isRequired() && restriction.getValue() == null) {
			throw new FieldValidationException("Required restriction is missing for field " + restriction.getField(), restriction.getField());
		}
		if (!this.searchConditionList.contains(restriction.getComparison())) {
			throw new FieldValidationException("Unsupported search comparison: " + restriction.getComparison() + " for field " + restriction.getField(), restriction.getField());
		}
	}


	@Override
	public String getFieldName() {
		return this.fieldName;
	}


	@Override
	public String getSearchFieldName() {
		return this.searchFieldName;
	}


	@Override
	public String getSearchFieldPath() {
		return this.searchFieldPath;
	}


	@Override
	public String getOrderByFieldName() {
		if (StringUtils.isEmpty(this.orderByFieldName)) {
			return this.searchFieldName;
		}
		return this.orderByFieldName;
	}


	@Override
	public boolean isRequired() {
		return this.required;
	}


	@Override
	public Object getValue(Object rawValue) {
		return rawValue;
	}


	/**
	 * Adds the specified {@link ComparisonConditions} to the List of allowed condition.
	 */
	private void addSearchCondition(ComparisonConditions condition) {
		this.searchConditionList.add(condition);
	}


	@Override
	public List<ComparisonConditions> getSearchConditionList() {
		return this.searchConditionList;
	}


	@Override
	public boolean isLeftJoin() {
		return this.leftJoin;
	}


	@Override
	public boolean isSubQuery() {
		for (ComparisonConditions condition : this.getSearchConditionList()) {
			if (condition.isSubQuery()) {
				return condition.isSubQuery();
			}
		}
		return false;
	}


	@Override
	public Class<?> getSearchFieldDataType() {
		return this.searchFieldDataType;
	}


	@Override
	public void setSearchFieldDataType(Class<?> searchFieldDataType) {
		this.searchFieldDataType = searchFieldDataType;
	}


	@Override
	public SearchFieldCustomTypes getSearchFieldCustomType() {
		return this.searchFieldCustomType;
	}


	@Override
	public String getSqlFormula() {
		return this.sqlFormula;
	}


	@Override
	public void setSqlFormula(String sqlFormula) {
		this.sqlFormula = sqlFormula;
	}
}
