package com.clifton.core.dataaccess.migrate.schema;


import com.clifton.core.dataaccess.dao.hibernate.GenericCompressedTextUserType;
import com.clifton.core.dataaccess.dao.hibernate.GenericEnumUserType;
import com.clifton.core.dataaccess.db.DataTypeEncodingTypeNames;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;


/**
 * The <code>Column</code> class represents a definition of a a database table column.
 *
 * @author vgomelsky
 */
public class Column {

	/**
	 * Note: not used for schema, but for UI Viewing/Searching Column information across tables
	 * we need to know the tableName associated with this specific column
	 */
	private String tableName;

	private static OrderByDirections defaultSortAsc = OrderByDirections.ASC;
	private String name;
	private String label;
	private String description;
	private String beanPropertyName;
	/**
	 * Specifies that this property does NOT exist on the bean. Usually a DB Computed Column that does not have a setter on the bean but may
	 * have a getter that mimics DB computation in Java. The property can still be used in searches.
	 */
	private boolean noBeanProperty;
	private DataTypes dataType;

	/**
	 * If dataType.DataTypeName supports encoding (Strings) then this will be set
	 * Can be set explicitly, a default set on the table, or global default
	 */
	private DataTypeEncodingTypeNames dataTypeEncodingType;

	private Boolean required;
	private Boolean requiredForValidation;
	private String defaultValue;
	private Integer sortOrder;
	private OrderByDirections sortDirection;
	/**
	 * Used to upload values/return values differently than how they are stored.
	 * For example, for Trades we use IsBuy Boolean column in the database.  For uploads
	 * we instead support uploads B/Buy or S/Sell and the converter supports converting that to true/false
	 * and the reverse for exporting the data.
	 */
	private String uploadValueConverterName;

	/**
	 * Applies to FK Fields.  Specifies object class that will be used to create a new instance during uploads. Only necessary if the FK table is an interface and uses subclasses.
	 * i.e. SystemListItem -> SystemList.  For uploads we create a new instance of SystemListEntity.
	 */
	private String uploadValueClassName;

	/**
	 * Supports special methods to lookup the bean based on specific method.  We often can use natural key, but
	 * in some cases (i.e. Security by Symbol) we can use a specific method to look up existing beans.
	 */
	private String uploadLookupServiceBeanName;
	private String uploadLookupServiceMethodName;
	/**
	 * Specifies whether this is a parent field: points to parent table and can be used in audit trail, etc.
	 * A table can have only one parent (or none).
	 * parentField selection is only allowed for required fields since
	 */
	private boolean parentField;

	private Boolean indexed;
	private boolean uniqueIndex;
	/**
	 * If {@code true}, {@code null} values will be considered unique when evaluating unique indexes for this column. This has the effect of allowing multiple
	 * {@code null} values. Otherwise, unique indexes will enforce uniqueness on all values, including {@code null}s.
	 * <p>
	 * This field only has an effect if {@link #required} is {@code false} and a unique index is generated for this field.
	 */
	private boolean uniqueNulls;

	private String fkTable;
	private String fkField;
	private String lazy;

	private boolean naturalKey;

	/**
	 * Specifies the field will be ignored (not included) in uploads
	 */
	private Boolean ignoreUpload;

	/**
	 * If true, will process column & column level definitions
	 * after the entire schema table & column definitions are created.
	 * This is used for the first FK reference in a circular dependency
	 */
	private boolean delayed;

	/**
	 * If true, ORM mapping will use simple property instead of creating a relationship
	 * even when a foreign key is defined.
	 */
	private Boolean forceSimpleProperty;

	private Boolean excludedFromUpdates;
	private Boolean excludedFromInserts;

	private String typeDefinitionName;
	private String typeDefinitionClassName;
	private String typeDefinitionConverterClass;

	private String auditType;

	/**
	 * Calculation to use for DB calculated columns - when present, hibernate will set this column to not include it in inserts or updates
	 * i.e. ColumnX + ColumnY
	 */
	private String calculation;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.tableName + "." + this.name;
	}


	public Boolean getColumnRequired() {
		if ((getRequired() == null) && (isDelayed() || isCalculatedColumn())) {
			return false;
		}
		return getRequired();
	}


	public Boolean getColumnRequiredForValidation() {
		if (getRequiredForValidation() != null) {
			return getRequiredForValidation();
		}
		return getColumnRequired();
	}


	public Boolean getColumnExcludedFromUpdates() {
		if (isCalculatedColumn()) {
			return true;
		}
		return this.excludedFromUpdates;
	}


	public Boolean getColumnExcludedFromInserts() {
		if (isCalculatedColumn()) {
			return true;
		}
		return this.excludedFromInserts;
	}


	public boolean isEnum() {
		return DataTypes.ENUM == this.getDataType();
	}


	public boolean isCompressedText() {
		return DataTypes.COMPRESSED_TEXT == this.getDataType();
	}


	public boolean isCalculatedColumn() {
		return !StringUtils.isEmpty(getCalculation());
	}


	/**
	 * Returns maximum number of characters for this String (based on data type)
	 */
	public int getMaxLength() {
		if (getDataType() == null) {
			return 0;
		}
		return getDataType().getLength();
	}


	public String getTypeDefinitionConverterClass() {
		if (isEnum()) {
			return this.typeDefinitionConverterClass == null ? GenericEnumUserType.class.getName() : this.typeDefinitionConverterClass;
		}
		else if (isCompressedText()) {
			return this.typeDefinitionConverterClass == null ? GenericCompressedTextUserType.class.getName() : this.typeDefinitionConverterClass;
		}
		return this.typeDefinitionConverterClass;
	}


	public OrderByDirections getSortDirection() {
		return this.sortDirection == null ? defaultSortAsc : this.sortDirection;
	}


	public static void setDefaultSortAsc(OrderByDirections defaultSortAsc) {
		Column.defaultSortAsc = defaultSortAsc;
	}


	public static OrderByDirections getDefaultSortAsc() {
		return Column.defaultSortAsc;
	}


	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Column)) {
			return false;
		}
		Column column = (Column) o;
		return (column.getName() == null || column.getName().equals(getName()));
	}


	@Override
	public int hashCode() {
		return (getName() == null) ? 0 : getName().hashCode();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}


	public boolean isNoBeanProperty() {
		return this.noBeanProperty;
	}


	public void setNoBeanProperty(boolean noBeanProperty) {
		this.noBeanProperty = noBeanProperty;
	}


	public DataTypes getDataType() {
		return this.dataType;
	}


	public void setDataType(DataTypes dataType) {
		this.dataType = dataType;
	}


	public DataTypeEncodingTypeNames getDataTypeEncodingType() {
		return this.dataTypeEncodingType;
	}


	public void setDataTypeEncodingType(DataTypeEncodingTypeNames dataTypeEncodingType) {
		this.dataTypeEncodingType = dataTypeEncodingType;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}


	public Boolean getRequiredForValidation() {
		return this.requiredForValidation;
	}


	public void setRequiredForValidation(Boolean requiredForValidation) {
		this.requiredForValidation = requiredForValidation;
	}


	public String getDefaultValue() {
		return this.defaultValue;
	}


	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	public Integer getSortOrder() {
		return this.sortOrder;
	}


	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}


	public void setSortDirection(OrderByDirections sortDirection) {
		this.sortDirection = sortDirection;
	}


	public String getUploadValueConverterName() {
		return this.uploadValueConverterName;
	}


	public void setUploadValueConverterName(String uploadValueConverterName) {
		this.uploadValueConverterName = uploadValueConverterName;
	}


	public String getUploadValueClassName() {
		return this.uploadValueClassName;
	}


	public void setUploadValueClassName(String uploadValueClassName) {
		this.uploadValueClassName = uploadValueClassName;
	}


	public String getUploadLookupServiceBeanName() {
		return this.uploadLookupServiceBeanName;
	}


	public void setUploadLookupServiceBeanName(String uploadLookupServiceBeanName) {
		this.uploadLookupServiceBeanName = uploadLookupServiceBeanName;
	}


	public String getUploadLookupServiceMethodName() {
		return this.uploadLookupServiceMethodName;
	}


	public void setUploadLookupServiceMethodName(String uploadLookupServiceMethodName) {
		this.uploadLookupServiceMethodName = uploadLookupServiceMethodName;
	}


	public boolean isParentField() {
		return this.parentField;
	}


	public void setParentField(boolean parentField) {
		this.parentField = parentField;
	}


	public Boolean getIndexed() {
		return this.indexed;
	}


	public void setIndexed(Boolean indexed) {
		this.indexed = indexed;
	}


	public boolean isUniqueIndex() {
		return this.uniqueIndex;
	}


	public void setUniqueIndex(boolean uniqueIndex) {
		this.uniqueIndex = uniqueIndex;
	}


	public boolean isUniqueNulls() {
		return this.uniqueNulls;
	}


	public void setUniqueNulls(boolean uniqueNulls) {
		this.uniqueNulls = uniqueNulls;
	}


	public String getFkTable() {
		return this.fkTable;
	}


	public void setFkTable(String fkTable) {
		this.fkTable = fkTable;
	}


	public String getFkField() {
		return this.fkField;
	}


	public void setFkField(String fkField) {
		this.fkField = fkField;
	}


	public String getLazy() {
		return this.lazy;
	}


	public void setLazy(String lazy) {
		this.lazy = lazy;
	}


	public boolean isNaturalKey() {
		return this.naturalKey;
	}


	public void setNaturalKey(boolean naturalKey) {
		this.naturalKey = naturalKey;
	}


	public Boolean getIgnoreUpload() {
		return this.ignoreUpload;
	}


	public void setIgnoreUpload(Boolean ignoreUpload) {
		this.ignoreUpload = ignoreUpload;
	}


	public boolean isDelayed() {
		return this.delayed;
	}


	public void setDelayed(boolean delayed) {
		this.delayed = delayed;
	}


	public Boolean getForceSimpleProperty() {
		return this.forceSimpleProperty;
	}


	public void setForceSimpleProperty(Boolean forceSimpleProperty) {
		this.forceSimpleProperty = forceSimpleProperty;
	}


	public Boolean getExcludedFromUpdates() {
		return this.excludedFromUpdates;
	}


	public void setExcludedFromUpdates(Boolean excludedFromUpdates) {
		this.excludedFromUpdates = excludedFromUpdates;
	}


	public Boolean getExcludedFromInserts() {
		return this.excludedFromInserts;
	}


	public void setExcludedFromInserts(Boolean excludedFromInserts) {
		this.excludedFromInserts = excludedFromInserts;
	}


	public String getTypeDefinitionName() {
		return this.typeDefinitionName;
	}


	public void setTypeDefinitionName(String typeDefinitionName) {
		this.typeDefinitionName = typeDefinitionName;
	}


	public String getTypeDefinitionClassName() {
		return this.typeDefinitionClassName;
	}


	public void setTypeDefinitionClassName(String typeDefinitionClassName) {
		this.typeDefinitionClassName = typeDefinitionClassName;
	}


	public void setTypeDefinitionConverterClass(String typeDefinitionConverterClass) {
		this.typeDefinitionConverterClass = typeDefinitionConverterClass;
	}


	public String getAuditType() {
		return this.auditType;
	}


	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}


	public String getCalculation() {
		return this.calculation;
	}


	public void setCalculation(String calculation) {
		this.calculation = calculation;
	}
}
