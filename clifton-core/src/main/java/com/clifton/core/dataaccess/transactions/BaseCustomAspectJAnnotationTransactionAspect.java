package com.clifton.core.dataaccess.transactions;


import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.hibernate.HibernateSessionAware;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.annotation.SuppressAjWarnings;
import org.aspectj.lang.reflect.MethodSignature;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.core.NamedThreadLocal;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionManager;
import org.springframework.transaction.annotation.AnnotationTransactionAttributeSource;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.transaction.interceptor.TransactionAttribute;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>CustomAspectJAnnotationTransactionAspect</code> maintains a map of {@link SessionFactory} objects
 * to {@link HibernateTransactionManager} objects which it uses to set the correct transaction manager when a
 * a method annotated with @Transactional is called.
 * <p>
 * NOTE: When the txObject does not implement SessionAware or it doesn't match the any regex in packageRegexToTransactionManageName
 * map, it will use the default behavior of getting the transaction manager by name.
 *
 * @author mwacker
 */
@Aspect
@Configurable
public abstract class BaseCustomAspectJAnnotationTransactionAspect extends TransactionAspectSupport implements CurrentContextApplicationListener<ApplicationContextEvent> {

	public static final String TRANSACTION_MANAGER_BEAN_NAME = "transactionManager";
	/**
	 * Thread local object to hold the transaction manager attached to the session.
	 */
	private static final ThreadLocal<TransactionManagerHolder> transactionManagerHolder = new NamedThreadLocal<>("Current transaction manager object");

	@Resource
	private TransactionManagerCache transactionManagerCache;

	/**
	 * Maps session factories to transaction managers.  This is auto created by getting
	 * a list of transaction managers from the spring context.
	 */
	private volatile Map<SessionFactory, PlatformTransactionManager> sessionToTransactionManagerMap;
	private final Object sessionToTransactionManagerMapLock = new Object();

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseCustomAspectJAnnotationTransactionAspect() {
		setTransactionAttributeSource(new AnnotationTransactionAttributeSource(false));
		setTransactionManagerBeanName(TRANSACTION_MANAGER_BEAN_NAME);
	}


	protected static PlatformTransactionManager currentTransactionManager() {
		TransactionManagerHolder holder = transactionManagerHolder.get();
		return holder != null ? holder.transactionManager : null;
	}


	////////////////////////////////////////////////////////////////////////////
	// 								Aspects									  //
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	@Pointcut(value = "execution(public * ((@org.springframework.transaction.annotation.Transactional *)+).*(..)) && within(@org.springframework.transaction.annotation.Transactional *)")
	private void executionOfAnyPublicMethodInAtTransactionalType() {
		// Pointcut declaration only; method body is not permitted
	}


	@SuppressWarnings("unused")
	@Pointcut(value = "execution(@org.springframework.transaction.annotation.Transactional * *(..))")
	private void executionOfTransactionalMethod() {
		// Pointcut declaration only; method body is not permitted
	}


	@SuppressWarnings("unused")
	@Pointcut(value = "(executionOfAnyPublicMethodInAtTransactionalType() || executionOfTransactionalMethod() ) && this(txObject)", argNames = "txObject")
	private void transactionalMethodExecution(@SuppressWarnings("unused") Object txObject) {
		// Pointcut declaration only; method body is not permitted
	}


	@SuppressAjWarnings("adviceDidNotMatch")
	@Around(value = "transactionalMethodExecution(txObject)", argNames = "thisJoinPoint,txObject")
	public Object setTransactionManager(final ProceedingJoinPoint thisJoinPoint, final Object txObject) {
		MethodSignature methodSignature = (MethodSignature) thisJoinPoint.getSignature();
		PlatformTransactionManager tm = getTransactionManagerForTransactionObject(txObject);
		TransactionManagerHolder holder = null;
		if (tm != null) {
			holder = new TransactionManagerHolder(tm);
			holder.bindToThread();
		}
		// Adapt to TransactionAspectSupport invokeWithinTransaction...
		try {
			return invokeWithinTransaction(methodSignature.getMethod(), txObject.getClass(), thisJoinPoint::proceed);
		}
		catch (RuntimeException | Error ex) {
			throw ex;
		}
		catch (Throwable thr) {
			Rethrower.rethrow(thr);
			throw new IllegalStateException("Should never get here", thr);
		}
		finally {
			if (holder != null) {
				holder.restoreThreadLocalStatus();
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ApplicationContextEvent event) {
		if (event instanceof ContextClosedEvent || event instanceof ContextRefreshedEvent) {
			synchronized (getSessionToTransactionManagerMapLock()) {
				this.sessionToTransactionManagerMap = null;
			}
			getTransactionManagerCache().clear();
			transactionManagerHolder.remove();

			// Context Events Occur During Tests when Dirties Context is used
			// Need to clear the map but keep the beanFactory property on the refresh event
			// Not sure of another way to do this
			BeanFactory beanFactory = super.getBeanFactory();
			super.clearTransactionManagerCache();
			super.setBeanFactory(beanFactory);
		}
	}


	@Override
	protected TransactionManager determineTransactionManager(TransactionAttribute txAttr) {
		// only get transaction manager when the qualifier is not set
		if (txAttr == null || StringUtils.isEmpty(txAttr.getQualifier())) {
			PlatformTransactionManager tm = currentTransactionManager();
			if (tm != null) {
				return tm;
			}
		}
		return super.determineTransactionManager(txAttr);
	}


	private PlatformTransactionManager getTransactionManagerForTransactionObject(Object transactionObject) {
		PlatformTransactionManager transactionManager = null;

		// DAO objects
		if (transactionObject instanceof HibernateSessionAware) {
			SessionFactory factory = ((HibernateSessionAware<?>) transactionObject).getSessionFactory();
			if (factory != null) {
				transactionManager = getSessionToTransactionManagerMap().get(factory);
			}
		}
		// Attempt to discover a transaction manager mapped to the fully-qualified class name (used most frequently for service types)
		else {
			transactionManager = getTransactionManagerCache().getTransactionManagerForClass(transactionObject.getClass());
		}

		return transactionManager;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Custom Getters/Setters                          ////////
	////////////////////////////////////////////////////////////////////////////


	private void setSessionToTransactionManagerMap(Map<SessionFactory, PlatformTransactionManager> sessionToTransactionManagerMap) {
		this.sessionToTransactionManagerMap = sessionToTransactionManagerMap;
	}


	protected Map<SessionFactory, PlatformTransactionManager> getSessionToTransactionManagerMap() {
		// Computed getter: Auto-initialize from bean factory using double-checked locking
		if (this.sessionToTransactionManagerMap == null) {
			synchronized (getSessionToTransactionManagerMapLock()) {
				if (this.sessionToTransactionManagerMap == null) {
					Map<SessionFactory, PlatformTransactionManager> generatedMap = new HashMap<>();
					AssertUtils.assertNotNull(getBeanFactory(), "Unable to generate the session-to-transaction-manager map. The bean factory is null.");
					Map<String, HibernateTransactionManager> tml = ((ConfigurableListableBeanFactory) getBeanFactory()).getBeansOfType(HibernateTransactionManager.class);
					for (HibernateTransactionManager tm : tml.values()) {
						generatedMap.put(tm.getSessionFactory(), tm);
					}
					setSessionToTransactionManagerMap(generatedMap);
				}
			}
		}
		return this.sessionToTransactionManagerMap;
	}


	private Object getSessionToTransactionManagerMapLock() {
		return this.sessionToTransactionManagerMapLock;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Ugly but safe workaround: We need to be able to propagate checked exceptions,
	 * despite AspectJ around advice supporting specifically declared exceptions only.
	 */
	private static class Rethrower {

		public static void rethrow(final Throwable exception) {
			class CheckedExceptionRethrower<T extends Throwable> {

				@SuppressWarnings({"unchecked"})
				private void rethrow(Throwable exception) throws T {
					throw (T) exception;
				}
			}
			new CheckedExceptionRethrower<RuntimeException>().rethrow(exception);
		}
	}


	/**
	 * The <code>TransactionManagerHolder</code> holds the transaction manager on the current thread.
	 *
	 * @author mwacker
	 */
	private static final class TransactionManagerHolder {

		final PlatformTransactionManager transactionManager;
		private TransactionManagerHolder oldTransactionManagerHolder;


		public TransactionManagerHolder(PlatformTransactionManager transactionManager) {
			this.transactionManager = transactionManager;
		}


		void bindToThread() {
			// Expose current TransactionStatus, preserving any existing TransactionStatus
			// for restoration after this transaction is complete.
			this.oldTransactionManagerHolder = transactionManagerHolder.get();
			transactionManagerHolder.set(this);
		}


		void restoreThreadLocalStatus() {
			// Use stack to restore old transaction TransactionInfo.
			// Will be null if none was set.
			transactionManagerHolder.set(this.oldTransactionManagerHolder);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TransactionManagerCache getTransactionManagerCache() {
		return this.transactionManagerCache;
	}


	public void setTransactionManagerCache(TransactionManagerCache transactionManagerCache) {
		this.transactionManagerCache = transactionManagerCache;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
