package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.Map;


public abstract class BaseGroupedResult extends BaseSimpleEntity<Integer> {

	private int count;

	private Map<GroupingAggregateProperty, Number> aggregateValueMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseGroupedResult() {

	}


	public BaseGroupedResult(int count, Map<GroupingAggregateProperty, Number> aggregateValueMap) {
		this.count = count;
		this.aggregateValueMap = aggregateValueMap;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract Map<GroupingAggregateProperty, Number> getTotalAggregateValueMap();


	public abstract int getTotalCount();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String toStringFormatted() {
		StringBuilder result = new StringBuilder(20);

		result.append("Count: ").append((getTotalCount()));
		result.append(StringUtils.TAB);

		if (getTotalAggregateValueMap() != null) {
			for (Map.Entry<GroupingAggregateProperty, Number> entry : getTotalAggregateValueMap().entrySet()) {
				result.append(entry.getKey().getAggregateLabel());
				result.append(": ");
				result.append(entry.getValue());
				result.append(StringUtils.TAB);
			}
		}
		return result.toString();
	}


	/**
	 * For easier rendering to the UI, this is what we can return.
	 * The map is converted to a Json string where the map key is the beanPropertyName (or aggregate label if not property specific like count) + the Aggregate type.
	 * Example Count.COUNT, accountingNotional.SUM, quantityIntended.SUM
	 * <p>
	 * This also pulls the TOTAL value which aggregates the children
	 */
	public CustomJsonString getTotalAggregateValueMapJson() {
		Map<GroupingAggregateProperty, Number> totalAggregateValueMap = getTotalAggregateValueMap();
		if (CollectionUtils.isEmpty(totalAggregateValueMap)) {
			return null;
		}
		CustomJsonString jsonString = new CustomJsonString();
		CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(jsonString);
		for (Map.Entry<GroupingAggregateProperty, Number> entry : totalAggregateValueMap.entrySet()) {
			jsonString.setColumnValue(entry.getKey().toString(), entry.getValue(), null);
		}
		CustomJsonStringUtils.resetCustomJsonStringFromJsonValueMap(jsonString);
		return jsonString;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getCount() {
		return this.count;
	}


	public void setCount(int count) {
		this.count = count;
	}


	public Map<GroupingAggregateProperty, Number> getAggregateValueMap() {
		return this.aggregateValueMap;
	}


	public void setAggregateValueMap(Map<GroupingAggregateProperty, Number> aggregateValueMap) {
		this.aggregateValueMap = aggregateValueMap;
	}
}
