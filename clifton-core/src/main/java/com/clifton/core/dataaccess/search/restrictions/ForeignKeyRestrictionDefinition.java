package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.validation.FieldValidationException;


/**
 * The <code>ForeignKeyRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for foreign key fields of type {@link Integer}.
 * Supports EQUALS comparison conditions only.
 *
 * @author vgomelsky
 */
public class ForeignKeyRestrictionDefinition<T extends Number> extends AbstractRestrictionDefinition {

	private final T[] allowedValues;
	private final AbstractRestrictionDefinition wrappedRestriction;


	public ForeignKeyRestrictionDefinition(AbstractRestrictionDefinition wrappedRestriction) {
		this(wrappedRestriction, null);
	}


	public ForeignKeyRestrictionDefinition(AbstractRestrictionDefinition wrappedRestriction, T[] allowedValues) {
		super(wrappedRestriction.getFieldName(), wrappedRestriction.getSearchFieldName(), wrappedRestriction.getSearchFieldPath(), wrappedRestriction.isLeftJoin(),
				wrappedRestriction.getOrderByFieldName(), wrappedRestriction.isRequired(),
				wrappedRestriction.getSearchConditionList().toArray(new ComparisonConditions[wrappedRestriction.getSearchConditionList().size()]), wrappedRestriction.getSearchFieldCustomType());
		this.wrappedRestriction = wrappedRestriction;
		this.allowedValues = allowedValues;
	}


	@Override
	public Object getValue(Object rawValue) {
		return this.wrappedRestriction.getValue(rawValue);
	}


	@SuppressWarnings("unchecked")
	@Override
	public void validate(SearchRestriction restriction) {
		super.validate(restriction);
		T numValue = (T) getValue(restriction.getValue());
		if (this.allowedValues != null) {
			for (T allowedValue : this.allowedValues) {
				if (allowedValue.equals(numValue)) {
					return;
				}
			}
			throw new FieldValidationException("Unsupported value: " + numValue, restriction.getField());
		}
	}
}
