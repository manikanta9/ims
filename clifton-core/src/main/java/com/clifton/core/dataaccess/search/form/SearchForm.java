package com.clifton.core.dataaccess.search.form;


import com.clifton.core.beans.IdentityObject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>SearchForm</code> annotation marks a class as a search form. This annotation can be used to describe special properties for the search form, such
 * as its DTO type.
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface SearchForm {

	/**
	 * Specifies the DTO class for the entity for which this search form is used. If not given, the system will attempt to find an entity automatically.
	 */
	Class<? extends IdentityObject> ormDtoClass() default IdentityObject.class;


	/**
	 * If {@code true}, indicates that this search form corresponds to a DTO with associated ORM mappings. Otherwise, it is assumed that no such DTO exists.
	 * <p>
	 * Defaults to {@code true}.
	 */
	boolean hasOrmDtoClass() default true;


	/**
	 * Lists the field paths which will be skipped when performing search form validation. For all fields, mapping-validation will proceed up to but not
	 * including any listed field path.
	 */
	String[] skippedPathsForValidation() default {};
}
