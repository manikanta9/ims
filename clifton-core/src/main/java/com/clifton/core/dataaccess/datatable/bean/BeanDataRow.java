package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>BeanDataRow</code> ...
 *
 * @author manderson
 */
public class BeanDataRow<T> implements DataRow {

	private final BeanDataTable<T> dataTable;
	private final T bean;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BeanDataRow(BeanDataTable<T> dataTable, T bean) {
		this.dataTable = dataTable;
		this.bean = bean;
	}


	@Override
	public Object getValue(int columnIndex) {
		if (columnIndex < 0 || columnIndex >= this.dataTable.getColumnCount()) {
			throw new ValidationException("Column index [" + columnIndex + "] is outside of the column range 0-" + this.dataTable.getColumnCount());
		}
		return getValue(this.dataTable.getColumnList()[columnIndex]);
	}


	@Override
	public Object getValue(String columnName) {
		for (DataColumn column : this.dataTable.getColumnList()) {
			if (columnName.equalsIgnoreCase(column.getColumnName())) {
				return getValue(column);
			}
		}
		return null;
	}


	@Override
	public Object getValue(DataColumn column) {
		BeanDataColumn beanColumn = (BeanDataColumn) column;

		Object value = null;
		// Custom Logic to Pull the Data for the Property out of the CustomJsonString property.  This is triggered by a specific prefix in the property name
		if (beanColumn.getBeanPropertyName().startsWith(BeanCollectionToDataTableConverter.CUSTOM_JSON_STRING_PREFIX)) {
			String jsonProperty = StringUtils.replace(beanColumn.getBeanPropertyName(), BeanCollectionToDataTableConverter.CUSTOM_JSON_STRING_PREFIX, "");
			CustomJsonString customJsonString = (CustomJsonString) BeanUtils.getPropertyValue(this.bean, StringUtils.substringBeforeLast(jsonProperty, "."));
			if (customJsonString != null) {
				CustomJsonStringUtils.initializeCustomJsonStringJsonValueMap(customJsonString);
				value = customJsonString.getColumnText(StringUtils.substringAfterLast(jsonProperty, "."));
			}
		}
		else {
			value = BeanUtils.getPropertyValue(this.bean, beanColumn.getBeanPropertyName(), true);
		}

		if (beanColumn.getColumnValueConverter() != null) {
			return ((ReversableConverter<Object, Object>) beanColumn.getColumnValueConverter()).reverseConvert(value);
		}
		return value;
	}


	@Override
	public Map<String, Object> getRowValueMap() {
		return getRowValueMap(null, false);
	}


	@Override
	public Map<String, Object> getRowValueMap(boolean includeNullValues) {
		return getRowValueMap(null, includeNullValues);
	}


	@Override
	public Map<String, Object> getRowValueMap(List<String> columnNames, boolean includeNullValues) {
		Map<String, Object> result = new HashMap<>();
		DataColumn[] columns = this.dataTable.getColumnList();
		for (int i = 0; i < columns.length; i++) {
			String columnName = columns[i].getColumnName();
			if (columnNames == null || columnNames.contains(columnName)) {
				Object value = getValue(i);
				if (value != null || includeNullValues) {
					result.put(columnName, value);
				}
			}
		}
		return result;
	}


	@Override
	public List<Object> getRowValueList() {
		return getRowValueList(null, false);
	}


	@Override
	public List<Object> getRowValueList(boolean includeNullValues) {
		return getRowValueList(null, includeNullValues);
	}


	@Override
	public List<Object> getRowValueList(List<String> columnNames, boolean includeNullValues) {
		List<Object> result = new ArrayList<>();
		DataColumn[] columns = this.dataTable.getColumnList();
		for (int i = 0; i < columns.length; i++) {
			String columnName = columns[i].getColumnName();
			if (columnNames == null || columnNames.contains(columnName)) {
				Object value = getValue(i);
				if (value != null || includeNullValues) {
					result.add(value);
				}
			}
		}
		return result;
	}
}
