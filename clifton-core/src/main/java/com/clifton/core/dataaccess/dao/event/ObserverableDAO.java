package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.observer.Observer;

import java.util.List;


/**
 * The <code>ObserverableDAO</code> interface should be implemented by DAO's that need to support observerable events.
 *
 * @author vgomelsky
 */

public interface ObserverableDAO<T extends IdentityObject> {

	/**
	 * Registers the specified {@link DaoEventObserver} object for the specified event.
	 */
	public void registerEventObserver(DaoEventTypes event, DaoEventObserver<T> eventObserver);


	/**
	 * Unregisters the specified {@link DaoEventObserver} object for the specified event.
	 */
	public void unregisterEventObserver(DaoEventTypes event, DaoEventObserver<T> eventObserver);


	/**
	 * Returns {@link DaoEventObserver} object (proxy) for the specified event.
	 */
	public DaoEventObserver<T> getEventObserver(DaoEventTypes event);


	/**
	 * Returns true if READ event observer(s) are registered.
	 */
	public boolean isReadObserverRegistered();


	/**
	 * Returns true if INSERT event observer(s) are registered.
	 */
	public boolean isInsertObserverRegistered();


	/**
	 * Returns true if INSERT event observer is registered for the specific observer
	 * i.e. is Audit Insert Observer registered
	 */
	public boolean isInsertObserverRegisteredForObserver(DaoEventObserver<T> eventObserver);


	public List<Observer> getInsertObserversRegistered();


	/**
	 * Returns true if UPDATE event observer(s) are registered.
	 */
	public boolean isUpdateObserverRegistered();


	/**
	 * Returns true if UPDATE event observer is registered for the specific observer
	 * i.e. is Audit Update Observer registered
	 */
	public boolean isUpdateObserverRegisteredForObserver(DaoEventObserver<T> eventObserver);


	public List<Observer> getUpdateObserversRegistered();


	/**
	 * Returns true if DELETE event observer(s) are registered.
	 */
	public boolean isDeleteObserverRegistered();


	/**
	 * Returns true if DELETE event observer is registered for the specific observer
	 * i.e. is Audit Delete Observer registered
	 */
	public boolean isDeleteObserverRegisteredForObserver(DaoEventObserver<T> eventObserver);


	public List<Observer> getDeleteObserversRegistered();


	/**
	 * Returns true if all observer functionality is enabled (defaults to true).
	 * When observing is disabled all is***Registered() methods return false.
	 */
	public boolean isObservingEnabled();


	/**
	 * Enables/disables observer functionality.
	 */
	public void setObservingEnabled(boolean observingEnabled);
}
