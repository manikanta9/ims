package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BaseBeanCollectionToDataTableConverter</code> is a basic implementation of the {@link BeanCollectionToDataTableConverter}
 * that determines columns based upon the bean's properties
 *
 * @author manderson
 * @author michaelm
 */
public class BeanCollectionToDataTableConverterWithDynamicColumnCreation<T> extends BeanCollectionToDataTableConverter<T> {

	private static final Set<String> skipPropertyList = new HashSet<>();


	static {
		skipPropertyList.add("id");
		skipPropertyList.add("identity");
		skipPropertyList.add("newBean");
		skipPropertyList.add("class");
		skipPropertyList.add("rv");
		skipPropertyList.add("createUserId");
		skipPropertyList.add("updateUserId");
	}


	private boolean splitPropertyWords = true;


	public BeanCollectionToDataTableConverterWithDynamicColumnCreation(DataTableConverterConfiguration converterConfiguration) {
		super(converterConfiguration);
	}


	public BeanCollectionToDataTableConverterWithDynamicColumnCreation(DataTableConverterConfiguration converterConfiguration, boolean splitPropertyWords) {
		super(converterConfiguration);
		this.splitPropertyWords = splitPropertyWords;
	}


	@Override
	protected BeanDataColumn[] getDataColumnArrayForBeanCollection(Collection<T> beanCollection) {
		T bean = CollectionUtils.getFirstElement(beanCollection);
		if (bean == null) {
			return new BeanDataColumn[0];
		}
		Map<String, PropertyDescriptor> propDescriptorMap = BeanUtils.getPropertyDataTypes(bean.getClass());
		List<BeanDataColumn> columnList = new ArrayList<>();
		for (String property : propDescriptorMap.keySet()) {
			if (skipPropertyList.contains(property)) {
				continue;
			}
			BeanDataColumn dc = new BeanDataColumn(property, StringUtils.capitalize(this.splitPropertyWords ? StringUtils.splitWords(property) : property), null, null, null);
			columnList.add(dc);
		}
		return columnList.toArray(new BeanDataColumn[0]);
	}
}
