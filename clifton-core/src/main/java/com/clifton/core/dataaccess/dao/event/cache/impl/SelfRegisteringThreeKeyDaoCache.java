package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoThreeKeyCache;


/**
 * The <code>SelfRegisteringThreeKeyDaoCache</code> class is a helper class that should be extended by DAO caches that cache an entity based on 3 key properties
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events. (By default INSERT, UPDATE, AND DELETE)
 * <p>
 * Observes all MODIFY events:
 * - Inserts are observed to immediately cache the bean after the save. (Unless overridden to clear on all changes in which case the cache is always cleared for the key)
 * - Updates - if the key changes will clear the cache for the original key and cache the bean for the updated key (Unless overridden to clear on all changes then will also clear for the new key)
 * - Deletes are observed to clear the cache (for affected key)
 * <p>
 * This cache supports cases where the cache uses 3 properties as its key
 *
 * @param <T>
 * @author Mary Anderson
 */
public abstract class SelfRegisteringThreeKeyDaoCache<T extends IdentityObject, K1, K2, K3> extends SelfRegisteringDaoCache<T> implements DaoThreeKeyCache<T, K1, K2, K3> {

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	/**
	 * For the composite cache key - this the the FIRST property in the key
	 * Keys are stored in the Cache as K1_K2_K3
	 * NOTE: This key property must match getBeanKey1Value - i.e. if key property 1 is name, the key 1 value should return bean.getName()
	 */
	protected abstract String getBeanKey1Property();


	/**
	 * For the composite cache key - this the the SECOND property in the key
	 * Keys are stored in the Cache as K1_K2_K3
	 * NOTE: This key property must match getBeanKey2Value - i.e. if key property 2 is name, the key 2 value should return bean.getName()
	 */
	protected abstract String getBeanKey2Property();


	/**
	 * For the composite cache key - this the the THIRD property in the key
	 * Keys are stored in the Cache as K1_K2_K3
	 * NOTE: This key property must match getBeanKey3Value - i.e. if key property 3 is name, the key 3 value should return bean.getName()
	 */
	protected abstract String getBeanKey3Property();


	/**
	 * For the composite cache key - this the the FIRST property VALUE in the key
	 * <p>
	 * NOTE: This value must return the property returned by getBeanKey1Property - we override it to avoid using reflection.
	 * - i.e. if key property 1 is name, then this key value should return String bean.getName()
	 */
	protected abstract K1 getBeanKey1Value(T bean);


	/**
	 * For the composite cache key - this the the SECOND property VALUE in the key
	 * <p>
	 * NOTE: This value must return the property returned by getBeanKey2Property - we override it to avoid using reflection.
	 * - i.e. if key property 2 is name, then this key value should return String bean.getName()
	 */
	protected abstract K2 getBeanKey2Value(T bean);


	/**
	 * For the composite cache key - this the the THIRD property VALUE in the key
	 * <p>
	 * NOTE: This value must return the property returned by getBeanKey3Property - we override it to avoid using reflection.
	 * - i.e. if key property 3 is name, then this key value should return String bean.getName()
	 */
	protected abstract K3 getBeanKey3Value(T bean);

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	@Override
	protected String[] getBeanKeyProperties() {
		return new String[]{getBeanKey1Property(), getBeanKey2Property(), getBeanKey3Property()};
	}


	@Override
	protected String getBeanKey(T bean) {
		return getBeanKeyForProperties(getBeanKey1Value(bean), getBeanKey2Value(bean), getBeanKey3Value(bean));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final T getBeanForKeyValues(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2, K3 keyPropertyValue3) {
		return getBeanImpl(dao, false, keyPropertyValue1, keyPropertyValue2, keyPropertyValue3);
	}


	@Override
	public final T getBeanForKeyValuesStrict(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2, K3 keyPropertyValue3) {
		return getBeanImpl(dao, true, keyPropertyValue1, keyPropertyValue2, keyPropertyValue3);
	}
}
