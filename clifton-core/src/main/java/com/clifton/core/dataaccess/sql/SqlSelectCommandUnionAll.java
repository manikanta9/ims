package com.clifton.core.dataaccess.sql;

import com.clifton.core.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


/**
 * A class to facilitate the creation of a SQL UNION ALL statement consisting of multiple SqlSelectCommands.
 *
 * @author davidi
 */
public class SqlSelectCommandUnionAll {

	private final List<SqlSelectCommand> sqlSelectCommandList = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected SqlSelectCommandUnionAll() {
	}


	public static SqlSelectCommandUnionAll of(SqlSelectCommand... sqlSelectCommands) {
		return of(CollectionUtils.createList(sqlSelectCommands));
	}


	public static SqlSelectCommandUnionAll of(List<SqlSelectCommand> sqlSelectCommandList) {
		SqlSelectCommandUnionAll sqlSelectCommandUnionAll = new SqlSelectCommandUnionAll();
		for (SqlSelectCommand sqlSelectCommand : CollectionUtils.getIterable(sqlSelectCommandList)) {
			sqlSelectCommandUnionAll.addSqlSelectCommand(sqlSelectCommand);
		}
		return sqlSelectCommandUnionAll;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addSqlSelectCommand(SqlSelectCommand sqlSelectCommand) {
		if (sqlSelectCommand != null) {
			getSqlSelectCommandList().add(sqlSelectCommand);
		}
	}


	public void removeSqlSelectCommand(int listIndex) {
		if (getSqlSelectCommandList().size() > listIndex && listIndex >= 0) {
			getSqlSelectCommandList().remove(listIndex);
		}
	}


	/**
	 * Returns size of the list of SqlSelectCommand
	 */
	public int getSize() {
		return getSqlSelectCommandList().size();
	}


	/**
	 * Creates an SQL UNION ALL out of a list of SqlCommands and returns a new single SqlSelectCommand containing that union.
	 */
	public SqlSelectCommand getSqlSelectCommand() {
		String unionAllSql = getSqlSelectCommandList().stream()
				.map(SqlSelectCommand::getSql)
				.collect(Collectors.joining("\nUNION ALL\n"));

		SqlSelectCommand sqlSelectCommand = new SqlSelectCommand(unionAllSql);
		sqlSelectCommand.setSqlParameterValues(getCombinedParameterValueList());
		return sqlSelectCommand;
	}


	/**
	 * Combines all SqlParameterValues from the SqlSelectCommands in the SqlSelectCommand List into a single list
	 * while preserving the original parameter ordering.
	 *
	 * @return List of SqlParameterValue
	 */
	private List<SqlParameterValue> getCombinedParameterValueList() {
		List<SqlParameterValue> sqlParameterValueList = new ArrayList<>();
		for (SqlSelectCommand sqlSelectCommand : getSqlSelectCommandList()) {
			if (sqlSelectCommand.getSqlParameterValues() != null) {
				sqlParameterValueList.addAll(sqlSelectCommand.getSqlParameterValues());
			}
		}
		return sqlParameterValueList;
	}


	/**
	 * Method to return the SQL of combined UNION ALL SQL statement directly.
	 */
	public String getSql() {
		return getSqlSelectCommand().getSql();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected List<SqlSelectCommand> getSqlSelectCommandList() {
		return this.sqlSelectCommandList;
	}
}
