package com.clifton.core.dataaccess.dao;

import com.clifton.core.beans.IdentityObject;


/**
 * @author stevenf
 */
public interface DaoSessionAware<T extends IdentityObject> {

	/**
	 * Removes the provided entity from the underlying session (first level cache). Removing the entity from the session
	 * detaches the object to avoid dirty checks that could auto flush to the underlying database.
	 * <p>
	 * NOTE: This only works within an encompassing transaction or active session (e.g. within OpenSessionInView or a transactional service).
	 *
	 * @param entity The loaded entity to remove from cache.
	 */
	public void evict(T entity);


	/**
	 * Flushes the current session.
	 * Rarely used (Currently for Manager Balance Reloads) which delete and insert manager balance in same transaction
	 * - Integration Test for Above case can be found in: ProductManagerServiceTests
	 * Because Hibernate executes inserts before deletes, need to flush the session after deletes to force delete to be executed prior to inserts
	 */
	public void flushSession();


	/**
	 * Clears the current session.
	 * Used during batch updates; explicitly removes all managed entities from L1 cache so it does not grow infinitely
	 * when processing large datasets in one transaction
	 */
	public void clearSession();
}
