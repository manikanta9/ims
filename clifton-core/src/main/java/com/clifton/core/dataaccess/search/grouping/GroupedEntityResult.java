package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;

import java.util.List;
import java.util.Map;


/**
 * A GroupedEntityResult is a generic object that can be used to group on various properties of an entity and provide total counts (or other aggregated values) for those grouped properties
 * The grouped properties themselves are populated on an instance of the object that is being grouped and the aggregate values are supplied separately as count + map of values.
 * <p>
 * Example - GroupedEntityResult for OrderAllocations - count and sum of notional grouped by Security. The OrderAllocation object would be populated only with the security object.  Any grouping for an id value
 * will hydrate that object.
 */
public class GroupedEntityResult<T> extends BaseGroupedResult {

	private T entity;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public GroupedEntityResult(T entity, int count, Map<GroupingAggregateProperty, Number> aggregateValueMap) {
		super(count, aggregateValueMap);
		this.entity = entity;
	}


	public String toStringFormatted(List<GroupingProperty> groupingPropertyList) {
		StringBuilder result = new StringBuilder(20);

		for (GroupingProperty groupingProperty : groupingPropertyList) {
			result.append(groupingProperty.getLabel());
			result.append(": ");
			if (groupingProperty.getBeanPropertyName().endsWith(".id")) {
				result.append(BeanUtils.getLabel(BeanUtils.getPropertyValue(getEntity(), StringUtils.substringBeforeLast(groupingProperty.getBeanPropertyName(), ".id"))));
			}
			else {
				result.append(BeanUtils.getPropertyValue(getEntity(), groupingProperty.getBeanPropertyName()));
			}
			result.append(StringUtils.TAB);
		}

		result.append(super.toStringFormatted());

		return result.toString();
	}


	@Override
	public int getTotalCount() {
		return getCount();
	}


	@Override
	public Map<GroupingAggregateProperty, Number> getTotalAggregateValueMap() {
		return getAggregateValueMap();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public T getEntity() {
		return this.entity;
	}


	public void setEntity(T entity) {
		this.entity = entity;
	}
}
