package com.clifton.core.dataaccess.migrate.schema;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.dml.Data;
import com.clifton.core.dataaccess.migrate.schema.sql.Sql;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.CoreMapUtils;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>Schema</code> class represents a database schema: a collection of tables and data.
 *
 * @author vgomelsky
 */
public class Schema {

	private boolean doNotStartTransaction;

	/**
	 * If set, conditionalSql is executed prior to running sql for the file, and if it doesn't return any results, the file will be skipped
	 */
	private String conditionalSql;

	/**
	 * If set and true, sql will not be created/executed for the file
	 */
	private Boolean excludeFromSql;

	private final List<Table> tableList;
	private final List<Data> dataList;
	private final List<Action> actionList;
	private final Map<String, Action> actionMap;
	private final List<Sql> sqlList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Schema() {
		// Default constructor
		this.tableList = new ArrayList<>();
		this.dataList = new ArrayList<>();
		this.actionList = new ArrayList<>();
		this.actionMap = new LinkedHashMap<>();
		this.sqlList = new ArrayList<>();
	}


	@SuppressWarnings("IncompleteCopyConstructor") // Copy constructor uses shallow clone of collection fields rather than reference copy
	public Schema(Schema other) {
		// Copy constructor
		this.doNotStartTransaction = other.doNotStartTransaction;
		this.conditionalSql = other.conditionalSql;
		this.excludeFromSql = other.excludeFromSql;
		this.tableList = CoreCollectionUtils.clone(other.tableList);
		this.dataList = CoreCollectionUtils.clone(other.dataList);
		this.actionList = CoreCollectionUtils.clone(other.actionList);
		this.actionMap = CoreMapUtils.clone(other.actionMap);
		this.sqlList = CoreCollectionUtils.clone(other.sqlList);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns this Table's Table object that matches the specified name or null if one is not found.
	 */
	public Table getTable(String tableName) {
		if (tableName != null) {
			for (Table table : getTableList()) {
				if (tableName.equals(table.getName())) {
					return table;
				}
			}
		}
		return null;
	}


	/**
	 * Removes the specified table along with all references to it (data inserts) from this schema.
	 * Returns true if the table was removed.
	 */
	public boolean removeTable(Table table) {
		if (table == null) {
			return false;
		}
		int size = CollectionUtils.getSize(getDataList());
		for (int i = size; i > 0; i--) {
			Data data = getDataList().get(i - 1);
			if (table.getName().equals(data.getTable())) {
				getDataList().remove(i - 1);
			}
		}
		for (Table t : getTableList()) {
			t.removeConstraints(table);
		}
		return getTableList().remove(table);
	}


	/**
	 * Returns all items in dataList that match the runWith property.
	 * A null or blank runWith argument will return ALL dataList objects.
	 * DATA will return all Data objects that should be ran with DATA migrations,
	 * DDL will return all Data objects that should be ran with DDL migrations,
	 * and META_DATA will return all Data objects that should be ran with META_DATA migrations.
	 */
	public List<Data> getDataList(String runWith) {
		if (StringUtils.isEmpty(runWith)) {
			return getDataList();
		}
		if (MigrationSchemaConverterTypes.DATA.name().equals(runWith)) {
			return BeanUtils.filter(getDataList(), data -> data.getRunWith() == null);
		}
		return BeanUtils.filter(getDataList(), Data::getRunWith, runWith);
	}


	/**
	 * Adds specified data row to the list.
	 */
	public void addData(Data data) {
		getDataList().add(data);
	}


	/**
	 * Returns all items in sqlList that match the runWith property.
	 * A null or blank runWith argument will return ALL sqlList objects.
	 * SQL will return all Sql objects that should be ran with SQL migrations,
	 * DDL will return all Sql objects that should be ran with DDL migrations,
	 * and META_DATA will return all Sql objects that should be ran with META_DATA migrations.
	 */
	public List<Sql> getSqlList(String runWith) {
		if (StringUtils.isEmpty(runWith)) {
			return getSqlList();
		}
		if (MigrationSchemaConverterTypes.SQL.name().equals(runWith)) {
			return BeanUtils.filter(getSqlList(), sql -> sql.getRunWith() == null);
		}
		return BeanUtils.filter(getSqlList(), Sql::getRunWith, runWith);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isDoNotStartTransaction() {
		return this.doNotStartTransaction;
	}


	public void setDoNotStartTransaction(boolean doNotStartTransaction) {
		this.doNotStartTransaction = doNotStartTransaction;
	}


	public String getConditionalSql() {
		return this.conditionalSql;
	}


	public void setConditionalSql(String conditionalSql) {
		this.conditionalSql = conditionalSql;
	}


	public Boolean getExcludeFromSql() {
		return this.excludeFromSql;
	}


	public void setExcludeFromSql(Boolean excludeFromSql) {
		this.excludeFromSql = excludeFromSql;
	}


	public List<Table> getTableList() {
		return this.tableList;
	}


	public List<Data> getDataList() {
		return this.dataList;
	}


	public List<Action> getActionList() {
		return this.actionList;
	}


	public Map<String, Action> getActionMap() {
		return this.actionMap;
	}


	public List<Sql> getSqlList() {
		return this.sqlList;
	}
}
