package com.clifton.core.dataaccess.migrate;


import com.clifton.core.dataaccess.DataSourceUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>MigrationJdbcTemplate</code> is an extension of the JdbcTemplate and
 * contains additional configuration for creating/dropping the migration database,
 * creating/updating the migration table, reading from the migration table, and executing migrations.
 * <p>
 * Contains another instance of JdbcTemplate (masterJdbcTemplate) that is used to connect to the master database
 * in order to create/drop the migration database.
 *
 * @author manderson
 */
public class MigrationJdbcTemplate extends JdbcTemplate {

	private Integer restoreFromFileCount;

	private String backupToFileLocation;
	private String logFileLocation;
	private String mdfFileLocation;
	private String fileGroupLocation;
	private String migrationDatabaseName;
	private String restoreFromFileLocation;
	private String restoreFromOriginalDatabaseName;

	private DataSource masterDatasource;
	private DataSource migrationDatasource;

	////////////////////////////////////////////////////////////////////////////

	private Map<String, String> fileGroupLocationMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MigrationJdbcTemplate(DataSource datasource, DataSource masterDatasource) {
		super(datasource);
		this.migrationDatasource = datasource;
		this.masterDatasource = masterDatasource;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setFileGroupLocationMapString(String str) {
		if (StringUtils.isEmpty(str)) {
			return;
		}
		if (this.fileGroupLocationMap == null) {
			this.fileGroupLocationMap = new HashMap<>();
		}
		String[] fileGroupLocations = str.split(";");
		for (String fileGroupLocationString : fileGroupLocations) {
			String[] fileGroupLocationValue = fileGroupLocationString.split("=");
			ValidationUtils.assertEquals(2, fileGroupLocationValue.length, "[" + fileGroupLocationString + "] is not a valid file group location.  Should be something like [DwMemoryOptimizedFile=D:\\Data\\DwMemoryOptimizedFile]");
			this.fileGroupLocationMap.put(fileGroupLocationValue[0], fileGroupLocationValue[1]);
		}
	}


	public void validateNonProductionInstance() {
		// Check if either Master or Default Datasource are Production
		if (isProductionDatabase(getMasterDatasource()) || isProductionDatabase(getMigrationDatasource())) {
			throw new IllegalStateException("Cannot drop a production instance of database [" + getMigrationDatabaseName() + "]");
		}
	}


	private boolean isProductionDatabase(DataSource ds) {
		// If the Actual Database Name Contains test - not a production instance
		if (getMigrationDatabaseName().toUpperCase().contains("TEST")) {
			return false;
		}
		org.apache.tomcat.jdbc.pool.DataSource ds2 = (org.apache.tomcat.jdbc.pool.DataSource) DataSourceUtils.getTargetDataSource(ds);
		String url = ds2.getUrl();
		if (StringUtils.isEmpty(url)) {
			return false;
		}

		// Use Machine Names, not friendly names because of encryption.  All Prod IMS SQL Servers start with MSP-400-
		if (url.toUpperCase().contains("MSP-400-")) {
			return true;
		}
		// Portal
		if (url.toUpperCase().contains("MSP-490-01")) {
			return true;
		}

		return false;
	}


	public void executeMaster(String sql) {
		setDataSource(getMasterDatasource());
		execute(sql);
		setDataSource(getMigrationDatasource());
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getMdfFileLocation() {
		return this.mdfFileLocation;
	}


	public void setMdfFileLocation(String mdfFileLocation) {
		this.mdfFileLocation = mdfFileLocation;
	}


	public String getLogFileLocation() {
		return this.logFileLocation;
	}


	public void setLogFileLocation(String logFileLocation) {
		this.logFileLocation = logFileLocation;
	}


	public String getMigrationDatabaseName() {
		return this.migrationDatabaseName;
	}


	public void setMigrationDatabaseName(String migrationDatabaseName) {
		this.migrationDatabaseName = migrationDatabaseName;
	}


	public DataSource getMigrationDatasource() {
		return this.migrationDatasource;
	}


	public void setMigrationDatasource(DataSource migrationDatasource) {
		this.migrationDatasource = migrationDatasource;
	}


	public DataSource getMasterDatasource() {
		return this.masterDatasource;
	}


	public void setMasterDatasource(DataSource masterDatasource) {
		this.masterDatasource = masterDatasource;
	}


	public String getRestoreFromFileLocation() {
		return this.restoreFromFileLocation;
	}


	public void setRestoreFromFileLocation(String restoreFromFileLocation) {
		this.restoreFromFileLocation = restoreFromFileLocation;
	}


	public String getRestoreFromOriginalDatabaseName() {
		return this.restoreFromOriginalDatabaseName;
	}


	public void setRestoreFromOriginalDatabaseName(String restoreFromOriginalDatabaseName) {
		this.restoreFromOriginalDatabaseName = restoreFromOriginalDatabaseName;
	}


	public Integer getRestoreFromFileCount() {
		return this.restoreFromFileCount;
	}


	public void setRestoreFromFileCount(Integer restoreFromFileCount) {
		this.restoreFromFileCount = restoreFromFileCount;
	}


	public String getBackupToFileLocation() {
		return this.backupToFileLocation;
	}


	public void setBackupToFileLocation(String backupToFileLocation) {
		this.backupToFileLocation = backupToFileLocation;
	}


	public String getFileGroupLocation() {
		return this.fileGroupLocation;
	}


	public void setFileGroupLocation(String fileGroupLocation) {
		this.fileGroupLocation = fileGroupLocation;
	}


	public Map<String, String> getFileGroupLocationMap() {
		return this.fileGroupLocationMap;
	}


	public void setFileGroupLocationMap(Map<String, String> fileGroupLocationMap) {
		this.fileGroupLocationMap = fileGroupLocationMap;
	}
}
