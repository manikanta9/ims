package com.clifton.core.dataaccess.migrate.locator;


import java.util.List;
import java.util.function.BiPredicate;


/**
 * The <code>MigrationFileLocator</code> interface defines a method to locate <tt>MigrationModule</tt> objects.
 *
 * @author vgomelsky
 */
public interface MigrationFileLocator {


	public enum MigrationFileLocatorVersionValidation {
		UNIQUE_VERSIONS((thisVersion, previousVersion) -> thisVersion.compareTo(previousVersion) > 0, "greater than"),
		DUPLICATE_VERSIONS((thisVersion, previousVersion) -> thisVersion.compareTo(previousVersion) >= 0, "greater than or equal to");


		private final BiPredicate<Integer, Integer> isValidVersion;
		private final String comparisonPhrase;


		MigrationFileLocatorVersionValidation(BiPredicate<Integer, Integer> isValidVersion, String comparisonPhrase) {
			this.isValidVersion = isValidVersion;
			this.comparisonPhrase = comparisonPhrase;
		}


		public boolean isValidVersion(int thisVersion, int previousVersion) {
			return this.isValidVersion.test(thisVersion, previousVersion);
		}


		public String getInvalidMessage(int thisVersion, int previousVersion) {
			return "Expected Migration File version " + this.comparisonPhrase + " [" + previousVersion + "], but was version [" + thisVersion + "].";
		}
	}


	/**
	 * Locates and returns a list of <tt>MigrationModule</tt> objects.
	 */
	public List<MigrationModule> locateMigrations(MigrationFileLocatorVersionValidation versionValidation);


	/**
	 * @param list
	 */
	public void setMigrationModuleList(List<MigrationModule> list);
}
