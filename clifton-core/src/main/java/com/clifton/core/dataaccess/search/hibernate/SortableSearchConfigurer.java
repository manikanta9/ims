package com.clifton.core.dataaccess.search.hibernate;


import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.BaseSortableSearchForm;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Order;

import java.util.List;


/**
 * The <code>SortableSearchConfigurer</code> class is responsible for configuring ORDER BY clause
 * based on orderBy field of BaseSortableSearchForm.
 *
 * @author vgomelsky
 */
public abstract class SortableSearchConfigurer<T extends BaseSortableSearchForm> implements SearchConfigurer<Criteria> {

	private final T sortableSearchForm;


	public SortableSearchConfigurer(T sortableSearchForm) {
		this.sortableSearchForm = sortableSearchForm;
	}


	protected T getSortableSearchForm() {
		return this.sortableSearchForm;
	}


	@Override
	public boolean isReadUncommittedRequested() {
		return this.sortableSearchForm.isReadUncommittedRequested();
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(this.sortableSearchForm.getOrderBy());
		if (CollectionUtils.isEmpty(orderByList)) {
			return false;
		}
		for (OrderByField field : orderByList) {
			String name = getOrderByFieldName(field, criteria);
			criteria.addOrder(OrderByDirections.ASC == field.getDirection() ? Order.asc(name) : Order.desc(name));
		}
		return true;
	}


	/**
	 * Returns the name of the specified order by field.
	 */
	protected String getOrderByFieldName(OrderByField field, @SuppressWarnings("unused") Criteria criteria) {
		return field.getName();
	}


	@Override
	public int getLimit() {
		return this.sortableSearchForm.getLimit();
	}

	@Override
	public Integer getMaxLimit() {
		return this.sortableSearchForm.getMaxLimit();
	}

	@Override
	public int getStart() {
		return this.sortableSearchForm.getStart();
	}
}
