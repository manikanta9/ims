package com.clifton.core.dataaccess.dao.hibernate;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.json.custom.CustomJsonObject;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Comparator;
import java.util.Properties;


/**
 * The <code>GenericCustomJsonObjectUserType</code> converts a {@link GenericCustomJsonObjectUserType} object to and from the it's string representation for the database.
 *
 * @author manderson
 */
public class GenericCustomJsonObjectUserType implements UserType, ParameterizedType, Comparator<GenericCustomJsonObjectUserType> {


	private static final int[] SQL_TYPES = {Types.NVARCHAR};


	private Class<? extends CustomJsonObject> clazz;


	@Override
	public void setParameterValues(@SuppressWarnings("unused") Properties parameters) {
		String typeDefinitionClassName = parameters.getProperty("typeDefinitionClassName");
		try {
			if (StringUtils.isEmpty(typeDefinitionClassName)) {
				// backward compatible class
				this.clazz = CustomJsonString.class;
			}
			else {
				this.clazz = Class.forName(typeDefinitionClassName).asSubclass(CustomJsonObject.class);
			}
		}
		catch (ClassNotFoundException e) {
			throw new HibernateException("Enum class not found", e);
		}
	}


	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}


	@Override
	public Class<?> returnedClass() {
		return this.clazz;
	}


	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, @SuppressWarnings("unused") SharedSessionContractImplementor session, @SuppressWarnings("unused") Object owner) throws HibernateException,
			SQLException {
		String name = resultSet.getString(names[0]);
		if (resultSet.wasNull()) {
			return null;
		}
		return BeanUtils.newInstance(returnedClass(), name);
	}


	@Override
	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, @SuppressWarnings("unused") SharedSessionContractImplementor session) throws HibernateException, SQLException {
		if (null == value) {
			preparedStatement.setNull(index, Types.VARCHAR);
		}
		else if (value instanceof String) {
			preparedStatement.setString(index, (String) value);
		}
		else {
			preparedStatement.setString(index, ((CustomJsonObject) value).getJsonValue());
		}
	}


	@Override
	public Object deepCopy(Object value) throws HibernateException {
		if (value != null) {
			if (value instanceof String) {
				return BeanUtils.newInstance(returnedClass(), value);
			}
			else if (value instanceof CustomJsonObject) {
				Object newInstance = BeanUtils.newInstance(returnedClass());
				BeanUtils.copyProperties(value, newInstance);
				return newInstance;
			}
		}
		return null;
	}


	@Override
	public boolean isMutable() {
		return true;
	}


	@Override
	public Object assemble(Serializable cached, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return cached;
	}


	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}


	@Override
	public Object replace(Object original, @SuppressWarnings("unused") Object target, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return original;
	}


	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}


	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		}
		if ((null == x) || (null == y)) {
			return false;
		}
		return x.equals(y);
	}


	@Override
	public int compare(GenericCustomJsonObjectUserType o1, GenericCustomJsonObjectUserType o2) {
		return CompareUtils.compare(o1, o2);
	}
}
