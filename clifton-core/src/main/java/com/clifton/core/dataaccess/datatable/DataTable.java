package com.clifton.core.dataaccess.datatable;


import java.util.List;


/**
 * The <code>DataTable</code> interface defines methods for working with data tables.
 *
 * @author vgomelsky
 */
public interface DataTable {

	public int getColumnIndex(String columnName);


	/**
	 * Returns an array of this DataTable's columns.
	 */
	public DataColumn[] getColumnList();


	public List<DataRow> getRowList();


	/**
	 * Returns the number of columns for this DataTable.
	 */
	public int getColumnCount();


	/**
	 * Returns a row with the specified index. First row index is 0.
	 */
	public DataRow getRow(int rowIndex);


	/**
	 * Adds the specified row this this object.
	 */
	public void addRow(DataRow row);


	/**
	 * Returns total number of rows (not just current page).
	 */
	public int getTotalRowCount();


	/**
	 * Used by DataTableToCsvFileConverter to allow for a simple export of each row regardless of the number of columns.
	 */
	public boolean isVariableColumnCountPerRow();


	public void setVariableColumnCountPerRow(boolean simpleExport);
}
