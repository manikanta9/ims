package com.clifton.core.dataaccess.datatable;


/**
 * The <code>DataTableGenerator</code> interface
 * <p/>
 * Used for System Bean Group: DataTable Generator that must be implemented by System Beans.
 * <p/>
 * Currently used by batch notifications to execute code and return results as a {@link DataTable}, however
 * interface defined in core so that implementations of it do not need to depend on Notification
 *
 * @author Mary Anderson
 */
public interface DataTableGenerator {

	public DataTable getDataTable();
}
