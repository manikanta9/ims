package com.clifton.core.dataaccess.datatable;


/**
 * The <code>DataTableRetrievalHandlerLocator</code> interface defines method(s) for locating
 * data source specific instances of {@link DataTableRetrievalHandler}.
 *
 * @author vgomelsky
 */
public interface DataTableRetrievalHandlerLocator {

	/**
	 * Locates and returns {@link DataTableRetrievalHandler} instance for the specified data source name.
	 *
	 * @param dataSourceName
	 */
	public DataTableRetrievalHandler locate(String dataSourceName);
}
