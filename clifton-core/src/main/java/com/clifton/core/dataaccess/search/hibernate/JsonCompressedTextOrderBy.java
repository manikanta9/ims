package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.dataaccess.search.OrderByDirections;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author stevenf
 */
public class JsonCompressedTextOrderBy extends Order {

	private final String searchFieldPath;
	private final Class<?> dataTypeClass;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructor for Order.
	 */
	private JsonCompressedTextOrderBy(String searchFieldPath, String propertyName, boolean ascending, Class<?> dataTypeClass) {
		super(propertyName, ascending);
		this.searchFieldPath = searchFieldPath;
		this.dataTypeClass = dataTypeClass;
	}


	public static JsonCompressedTextOrderBy create(String searchFieldPath, String propertyName, OrderByDirections direction) {
		return new JsonCompressedTextOrderBy(searchFieldPath, propertyName, direction == OrderByDirections.ASC, null);
	}


	public static JsonCompressedTextOrderBy create(String searchFieldPath, String propertyName, OrderByDirections direction, Class<?> dataTypeClass) {
		return new JsonCompressedTextOrderBy(searchFieldPath, propertyName, direction == OrderByDirections.ASC, dataTypeClass);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return toString();
	}


	@Override
	public String toString() {
		String sql = "JSON_VALUE(CAST(DECOMPRESS(" + getSearchFieldPath() + ") AS VARCHAR(MAX)), '" + getPropertyName() + "')";
		if (getDataTypeClass() != null) {
			if (Date.class.equals(getDataTypeClass())) {
				sql = "CAST(" + sql + " AS DATE)";
			}
			else if (BigDecimal.class.equals(getDataTypeClass())) {
				sql = "CAST(" + sql + " AS DECIMAL(28,16))";
			}
		}
		return sql + " " + (isAscending() ? "ASC" : "DESC");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getSearchFieldPath() {
		return this.searchFieldPath;
	}


	public Class<?> getDataTypeClass() {
		return this.dataTypeClass;
	}
}
