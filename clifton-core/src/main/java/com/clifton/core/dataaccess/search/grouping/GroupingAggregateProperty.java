package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.util.StringUtils;

import java.util.Objects;


/**
 * Grouping Aggregate Property can be used supply aggregate details for a Grouped Result
 */
public class GroupingAggregateProperty {

	private GroupingAggregateTypes aggregateType;

	/**
	 * This is used for aggregation in the Projection.
	 * If it's multiple levels deep appropriate LEFT JOINs are added
	 */
	private String beanPropertyName;

	/**
	 * This can be used INSTEAD of bean property name if the aggregate is a custom formula
	 * The search form field name is REQUIRED to use a Formula in the search field definition
	 * which is then used with FormulaProjection.
	 * i.e. for Order Allocations - if not grouping by BUY / SELL then the notional aggregate must be signed to properly calculate the net (used for CCY orders only)
	 */
	private String searchFormFieldName;

	private String aggregateLabel;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static GroupingAggregateProperty ofCount() {
		GroupingAggregateProperty object = new GroupingAggregateProperty();
		object.setAggregateType(GroupingAggregateTypes.COUNT);
		object.setAggregateLabel("Count");
		return object;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		GroupingAggregateProperty that = (GroupingAggregateProperty) o;
		return this.aggregateType == that.aggregateType && Objects.equals(this.beanPropertyName, that.beanPropertyName) && Objects.equals(this.searchFormFieldName, that.searchFormFieldName);
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.aggregateType, this.beanPropertyName, this.searchFormFieldName);
	}


	@Override
	public String toString() {
		return StringUtils.coalesce(getBeanPropertyName(), getSearchFormFieldName(), getAggregateLabel()) + "." + getAggregateType().name();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public GroupingAggregateTypes getAggregateType() {
		return this.aggregateType;
	}


	public void setAggregateType(GroupingAggregateTypes aggregateType) {
		this.aggregateType = aggregateType;
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}


	public String getSearchFormFieldName() {
		return this.searchFormFieldName;
	}


	public void setSearchFormFieldName(String searchFormFieldName) {
		this.searchFormFieldName = searchFormFieldName;
	}


	public String getAggregateLabel() {
		return this.aggregateLabel;
	}


	public void setAggregateLabel(String aggregateLabel) {
		this.aggregateLabel = aggregateLabel;
	}
}
