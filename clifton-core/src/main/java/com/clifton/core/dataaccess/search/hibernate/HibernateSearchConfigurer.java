package com.clifton.core.dataaccess.search.hibernate;


import com.clifton.core.dataaccess.search.SearchConfigurer;
import org.hibernate.Criteria;


/**
 * The <code>HibernateSearchConfigurer</code> interface is a Hibernate Criteria-specific implementation of searches. This interface provides default implementations of all methods
 * except for {@link #configureCriteria(Object)}.
 *
 * @author vgomelsky
 */
@FunctionalInterface
public interface HibernateSearchConfigurer extends SearchConfigurer<Criteria> {

	@Override
	default boolean isReadUncommittedRequested() {
		return false;
	}


	/**
	 * Do not configure sorting and rely on default sorting defined at schema instead.
	 */
	@Override
	default boolean configureOrderBy(@SuppressWarnings("unused") Criteria criteria) {
		return false;
	}


	@Override
	default int getLimit() {
		return Integer.MAX_VALUE;
	}


	@Override
	default Integer getMaxLimit() {
		return null;
	}


	@Override
	default int getStart() {
		return 0;
	}
}
