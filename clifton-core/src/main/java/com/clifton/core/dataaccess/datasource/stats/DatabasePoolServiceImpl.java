package com.clifton.core.dataaccess.datasource.stats;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.NamedEntityWithoutLabel;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datasource.EnhancedConnectionPool;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.apache.tomcat.jdbc.pool.ConnectionPool;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolConfiguration;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;


/**
 * <code>DatabasePoolServiceImpl</code> is and implementation of {@link DatabasePoolService}
 * used for {@link DataSource}s.
 *
 * @author NickK
 */
@Service
public class DatabasePoolServiceImpl implements DatabasePoolService {

	private ApplicationContextService applicationContextService;
	private Map<String, DatabaseProperty[]> dataSourceDefaultPropertyValuesMap = new HashMap<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	@ModelAttribute("data")
	public DatabaseStatistics getDatabaseStatistics(String dataSourceName) {
		ValidationUtils.assertNotNull(dataSourceName, "A DataSource name must be specified.");
		EnhancedConnectionPool pool = (EnhancedConnectionPool) getDataSourceByName(dataSourceName).getPool();

		DatabaseStatistics statistics = new DatabaseStatistics();
		statistics.setCreatedCount(pool.getCreatedCount());
		statistics.setReconnectedCount(pool.getReconnectedCount());
		statistics.setSize(pool.getSize());
		statistics.setIdleCount(pool.getIdle());
		statistics.setActiveCount(pool.getActive());
		statistics.setMaxActiveCount(pool.getMaxActiveCount());
		statistics.setBorrowedCount(pool.getBorrowedCount());
		statistics.setReturnedCount(pool.getReturnedCount());
		statistics.setReleasedCount(pool.getReleasedCount());
		statistics.setReleasedIdleCount(pool.getReleasedIdleCount());
		statistics.setReleaseAbandonedCount(pool.getRemoveAbandonedCount());

		statistics.setDatabasePropertyList(new ArrayList<>());
		PoolConfiguration configuration = pool.getPoolProperties();
		DatabaseProperty[] databaseConfigurationProperties = getDataSourceDefaultPropertyValuesMap().get(dataSourceName);
		ArrayUtils.getStream(databaseConfigurationProperties)
				.forEach(propertySpec -> {
					DatabaseProperty property = new DatabaseProperty(propertySpec.getName(), propertySpec.getType());
					property.setValue(BeanUtils.getPropertyValue(configuration, propertySpec.getName()));
					statistics.getDatabasePropertyList().add(property);
				});

		return statistics;
	}


	@Override
	@ModelAttribute("data")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public DatabaseStatistics saveDatabasePropertyList(BeanListCommand<DatabaseProperty> propertyList, String dataSourceName) {
		updateDatabasePropertyList(propertyList.getBeanList(), dataSourceName);
		return getDatabaseStatistics(dataSourceName);
	}


	@Override
	@ModelAttribute("data")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public DatabaseStatistics processDatabaseStatisticsCommand(DatabaseStatisticsCommand command, String dataSourceName) {
		ValidationUtils.assertNotNull(dataSourceName, "The DataSourceName property of command cannot be null.");

		if (command != null) {
			DataSource dataSource = getDataSourceByName(dataSourceName);

			if (command.isClearStatistics()) {
				dataSource.resetStats();
			}
			if (command.isResetConnections()) {
				dataSource.purge();
			}
			if (command.isRestorePropertyList()) {
				DatabaseProperty[] databaseConfigurationProperties = getDataSourceDefaultPropertyValuesMap().get(dataSourceName);
				updateDatabasePropertyList(ArrayUtils.getStream(databaseConfigurationProperties).collect(Collectors.toList()), dataSourceName);
			}
		}

		return getDatabaseStatistics(dataSourceName);
	}


	@Override
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<NamedEntityWithoutLabel<String>> getDatabasePoolNameList() {
		Map<String, DataSource> dataSourceMap = getApplicationContextService().getContextBeansOfType(DataSource.class);
		ArrayList<NamedEntityWithoutLabel<String>> namedEntityList = new ArrayList<>();

		for (String dataSourceName : dataSourceMap.keySet()) {
			NamedEntityWithoutLabel<String> namedEntity = new NamedEntityWithoutLabel<>(dataSourceName, "DataSource with name '" + dataSourceName + "'.");
			namedEntity.setId(dataSourceName);
			namedEntityList.add(namedEntity);
		}
		return namedEntityList;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void updateDatabasePropertyList(List<DatabaseProperty> propertyList, String dataSourceName) {
		PoolConfiguration configuration = getDataSourceByName(dataSourceName).getPoolProperties();
		AtomicBoolean restartPoolSweeper = new AtomicBoolean();
		CollectionUtils.getStream(propertyList)
				.filter(property -> property != null && !StringUtils.isEmpty(property.getName()) && isPropertyValid(property, dataSourceName))
				.forEach(property -> {
					// The only configuration property that does not support real time changes is the timeBetweenEvictionRunsMillis
					// because it is used to schedule the pool sweeper task. Thus, if it is updated, the pool sweeper must be restarted.
					restartPoolSweeper.compareAndSet(false, "timeBetweenEvictionRunsMillis".equals(property.getName()));
					BeanUtils.setPropertyValue(configuration, property.getName(), property.getNewValue());
				});
		if (restartPoolSweeper.get()) {
			processDatabaseSweeperRestart(dataSourceName);
		}
	}


	private void processDatabaseSweeperRestart(String dataSourceName) {
		ConnectionPool pool = getDataSourceByName(dataSourceName).getPool();
		pool.terminatePoolCleaner();
		pool.initializePoolCleaner(pool.getPoolProperties());
	}


	private boolean isPropertyValid(DatabaseProperty property, String dataSourceName) {
		DatabaseProperty[] databaseConfigurationProperties = getDataSourceDefaultPropertyValuesMap().get(dataSourceName);
		for (DatabaseProperty propertySpec : databaseConfigurationProperties) {
			if (propertySpec.getName().equals(property.getName())) {
				return true;
			}
		}
		return false;
	}


	private DataSource getDataSourceByName(String dataSourceName) {
		Map<String, DataSource> dataSourceMap = getApplicationContextService().getContextBeansOfType(DataSource.class);
		if (!dataSourceMap.containsKey(dataSourceName)) {
			throw new ValidationException("Could not find DataSource with name: '" + dataSourceName + "'.");
		}
		DataSource dataSource = dataSourceMap.get(dataSourceName);
		setConfigurationProperties(dataSourceName, dataSource);
		return dataSource;
	}


	/**
	 * Stores the initial datasource property values when a datasource is first requested.
	 */
	private void setConfigurationProperties(String dataSourceName, DataSource dataSource) {
		if (dataSource != null && !getDataSourceDefaultPropertyValuesMap().containsKey(dataSourceName)) {
			DatabaseProperty[] dbPropertiesArray = ArrayUtils.createArray(
					new DatabaseProperty("defaultAutoCommit", Boolean.class),
					new DatabaseProperty("defaultReadOnly", Boolean.class),
					new DatabaseProperty("defaultTransactionIsolation", int.class),
					new DatabaseProperty("defaultCatalog", String.class),
					new DatabaseProperty("driverClassName", String.class),
					new DatabaseProperty("initialSize", int.class),
					new DatabaseProperty("initSQL", String.class),
					new DatabaseProperty("logAbandoned", boolean.class),
					new DatabaseProperty("maxActive", int.class),
					new DatabaseProperty("maxAge", int.class),
					new DatabaseProperty("maxIdle", int.class),
					new DatabaseProperty("minEvictableIdleTimeMillis", int.class),
					new DatabaseProperty("minIdle", int.class),
					new DatabaseProperty("maxWait", int.class),
					new DatabaseProperty("numTestsPerEvictionRun", int.class),
					new DatabaseProperty("testOnBorrow", boolean.class),
					new DatabaseProperty("testOnConnect", boolean.class),
					new DatabaseProperty("testOnReturn", boolean.class),
					new DatabaseProperty("testWhileIdle", boolean.class),
					new DatabaseProperty("removeAbandoned", boolean.class),
					new DatabaseProperty("removeAbandonedTimeout", int.class),
					new DatabaseProperty("timeBetweenEvictionRunsMillis", int.class),
					new DatabaseProperty("validationInterval", long.class),
					new DatabaseProperty("validatorClassName", String.class),
					new DatabaseProperty("validationQuery", String.class),
					new DatabaseProperty("validationQueryTimeout", int.class)
			);
			getDataSourceDefaultPropertyValuesMap().put(dataSourceName, dbPropertiesArray);

			// store the original values property array
			PoolConfiguration configuration = dataSource.getPoolProperties();
			ArrayUtils.getStream(dbPropertiesArray)
					.forEach(propertySpec -> {
						Object value = BeanUtils.getPropertyValue(configuration, propertySpec.getName());
						propertySpec.setValue(value);
						propertySpec.setNewValue(value);
					});
		}
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public Map<String, DatabaseProperty[]> getDataSourceDefaultPropertyValuesMap() {
		return this.dataSourceDefaultPropertyValuesMap;
	}
}
