package com.clifton.core.dataaccess.search.form.entity;

import com.clifton.core.dataaccess.search.SearchField;


/**
 * The <code>BaseAuditableEntityNamedSearchForm</code> class is a {@link BaseAuditableEntitySearchForm} which includes search parameters for the
 * name, description fields as well as the searchPattern.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseAuditableEntitySearchForm Parent fields}
 * <li>{@link #searchPattern}
 * <li>{@link #name}
 * <li>{@link #description}
 * </ul>
 *
 * @author vgomelsky
 * @see BaseEntitySearchForm
 * @see BaseUpdatableEntitySearchForm
 * @see BaseAuditableEntitySearchForm
 */
public abstract class BaseAuditableEntityNamedSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "name,description")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}
}
