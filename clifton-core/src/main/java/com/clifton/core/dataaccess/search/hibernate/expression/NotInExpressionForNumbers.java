package com.clifton.core.dataaccess.search.hibernate.expression;


/**
 * The <code>NotInExpressionForNumbers</code> implements NOT IN SQL clause
 *
 * @author jgommels
 */
public class NotInExpressionForNumbers extends AbstractInExpressionForNumbers {

	public NotInExpressionForNumbers(String propertyName, Object[] values) {
		super("NOT IN", propertyName, values);
	}
}
