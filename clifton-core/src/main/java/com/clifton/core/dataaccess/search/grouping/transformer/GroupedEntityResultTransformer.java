package com.clifton.core.dataaccess.search.grouping.transformer;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.grouping.GroupedEntityResult;
import com.clifton.core.dataaccess.search.grouping.GroupingProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchForm;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.StringUtils;

import java.util.List;


public class GroupedEntityResultTransformer<T extends IdentityObject> extends BaseGroupedResultTransformer<T> {


	public GroupedEntityResultTransformer(GroupingSearchForm groupingSearchForm, DaoLocator daoLocator, DAOConfiguration<T> daoConfiguration) {
		super(groupingSearchForm, daoLocator, daoConfiguration);
	}


	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		T bean = ClassUtils.newInstance(getDaoConfiguration().getBeanClass());
		int groupedColumnCount = getGroupingPropertyList().size();
		for (int i = 0; i < groupedColumnCount; i++) {
			GroupingProperty groupingProperty = getGroupingPropertyList().get(i);
			Object value = tuple[i];
			if (value != null) {
				populateValueOnBean(groupingProperty, bean, value);
			}
		}
		return new GroupedEntityResult<T>(bean, getCountFromTuple(tuple), getAggregatePropertyMapFromTuple(tuple));
	}


	@SuppressWarnings("rawtypes")
	@Override
	public List transformList(List tuples) {
		return tuples;
	}


	private void populateValueOnBean(GroupingProperty groupingProperty, T bean, Object value) {
		Object hydratedValue = getHydratedValue(groupingProperty, value);

		if (hydratedValue != null) {
			if (StringUtils.endsWith(groupingProperty.getBeanPropertyName(), ".id")) {
				BeanUtils.setPropertyValue(bean, StringUtils.substringBeforeLast(groupingProperty.getBeanPropertyName(), "."), hydratedValue);
			}
			else {
				BeanUtils.setPropertyValue(bean, groupingProperty.getBeanPropertyName(), hydratedValue);
			}
		}
	}
}
