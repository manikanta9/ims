package com.clifton.core.dataaccess.sql;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;


/**
 * The SqlCommand class represent SQL statement with parameters and is intended to be extended by SELECT or UPDATE commands.
 *
 * @author vgomelsky
 */
@SuppressWarnings("ALL")
public abstract class SqlCommand<T extends SqlCommand> implements Serializable {

	private static final String WHERE_CLAUSE_AND_CONJUNCTION = " AND ";

	private String sql;
	private List<SqlParameterValue> sqlParameterValues;

	private final StringBuilder whereClause = new StringBuilder();

	// timeout in seconds, default is -1, indicating to use the JDBC driver's default
	private int timeout = -1;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected SqlCommand() {
	}


	public SqlCommand(String sql) {
		setSql(sql);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Used for logging statements so we can see what parameter values were used.
	 */
	public String getSqlWithParameters() {
		StringBuilder result = new StringBuilder(getSql());
		if (getSqlParameterValues() != null) {
			result.append("\nWITH PARAMS [");
			result.append(getSqlParameterValues());
			result.append("]");
		}
		return result.toString();
	}


	public void addSqlParameterValue(SqlParameterValue sqlParameterValue) {
		if (this.sqlParameterValues == null) {
			this.sqlParameterValues = new ArrayList<>();
		}
		this.sqlParameterValues.add(sqlParameterValue);
	}


	public T addStringParameterValue(String value) {
		addSqlParameterValue(SqlParameterValue.ofString(value));
		return (T) this;
	}


	public T addShortParameterValue(Short value) {
		addSqlParameterValue(SqlParameterValue.ofShort(value));
		return (T) this;
	}


	public T addIntegerParameterValue(Integer value) {
		addSqlParameterValue(SqlParameterValue.ofInteger(value));
		return (T) this;
	}


	public T addLongParameterValue(Long value) {
		addSqlParameterValue(SqlParameterValue.ofLong(value));
		return (T) this;
	}


	public T addBigDecimalParameterValue(BigDecimal value) {
		addSqlParameterValue(SqlParameterValue.ofBigDecimal(value));
		return (T) this;
	}


	public T addDateParameterValue(Date value) {
		addSqlParameterValue(SqlParameterValue.ofDate(value));
		return (T) this;
	}


	public T addBooleanParameterValue(Boolean value) {
		addSqlParameterValue(SqlParameterValue.ofBoolean(value));
		return (T) this;
	}


	public void setSqlParameterValues(SqlParameterValue[] sqlParameterValues) {
		this.sqlParameterValues = (sqlParameterValues == null) ? null : Arrays.asList(sqlParameterValues);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Adds a SQL based restriction without a parameter value.
	 * The restriction SQL can be passed in segments and will be concatenated while appending to the where clause.
	 */
	public T addSqlRestriction(String... sqlRestrictionSegments) {
		appendRestriction(sqlRestrictionSegments);
		return (T) this;
	}


	/**
	 * Adds a SQL based restriction with a short value.
	 * The restriction SQL can be passed in segments and will be concatenated while appending to the where clause.
	 */
	public T addShortRestriction(Short value, String... sqlRestrictionSegments) {
		if (value != null) {
			appendRestriction(sqlRestrictionSegments);
			addShortParameterValue(value);
		}
		return (T) this;
	}


	/**
	 * Adds a SQL based restriction with an integer value.
	 * The restriction SQL can be passed in segments and will be concatenated while appending to the where clause.
	 */
	public T addIntegerRestriction(Integer value, String... sqlRestrictionSegments) {
		if (value != null) {
			appendRestriction(sqlRestrictionSegments);
			addIntegerParameterValue(value);
		}
		return (T) this;
	}


	/**
	 * Adds a SQL based restriction with a long value.
	 * The restriction SQL can be passed in segments and will be concatenated while appending to the where clause.
	 */
	public T addLongRestriction(Long value, String... sqlRestrictionSegments) {
		if (value != null) {
			appendRestriction(sqlRestrictionSegments);
			addLongParameterValue(value);
		}
		return (T) this;
	}


	/**
	 * Adds a SQL based restriction with a date value.
	 * The restriction SQL can be passed in segments and will be concatenated while appending to the where clause.
	 */
	public T addDateRestriction(Date value, String... sqlRestrictionSegments) {
		if (value != null) {
			appendRestriction(sqlRestrictionSegments);
			addDateParameterValue(value);
		}
		return (T) this;
	}


	/**
	 * Adds a SQL based restriction with a boolean value.
	 * The restriction SQL can be passed in segments and will be concatenated while appending to the where clause.
	 */
	public T addBooleanRestriction(Boolean value, String... sqlRestrictionSegments) {
		if (value != null) {
			appendRestriction(sqlRestrictionSegments);
			addBooleanParameterValue(value);
		}
		return (T) this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void appendRestriction(String... sqlRestrictionSegments) {
		// Avoid filteing out null segments. When objects are passed in as segments for comparison, null references can become string "null" as valid SQL.
		Iterator<String> segmentIterator = ArrayUtils.getStream(sqlRestrictionSegments).iterator();
		if (!segmentIterator.hasNext()) {
			return;
		}

		String firstSegment = segmentIterator.next();
		if (!StringUtils.isEmpty(getWhereClause()) && !firstSegment.startsWith(WHERE_CLAUSE_AND_CONJUNCTION)) {
			this.whereClause.append(WHERE_CLAUSE_AND_CONJUNCTION);
		}
		this.whereClause.append(firstSegment);
		segmentIterator.forEachRemaining(this.whereClause::append);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSql() {
		return this.sql;
	}


	public void setSql(String sql) {
		this.sql = sql;
	}


	public CharSequence getWhereClause() {
		return this.whereClause;
	}


	@ValueChangingSetter
	public void setWhereClause(String whereClause) {
		int length = StringUtils.length(whereClause);
		if (length > 0) {
			// replace clause value with new value
			this.whereClause.insert(0, whereClause);
		}
		// set length to 0 or length of provided clause
		this.whereClause.setLength(length);
	}


	public List<SqlParameterValue> getSqlParameterValues() {
		return this.sqlParameterValues;
	}


	public void setSqlParameterValues(List<SqlParameterValue> sqlParameterValues) {
		this.sqlParameterValues = sqlParameterValues;
	}


	public int getTimeout() {
		return this.timeout;
	}


	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}
}
