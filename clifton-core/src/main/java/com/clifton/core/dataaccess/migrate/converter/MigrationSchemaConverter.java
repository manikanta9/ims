package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.util.converter.Converter;


/**
 * The <code>MigrationSchemaConverter</code> interface should be implemented by classes that serialize
 * Schema objects to different formats (SQL, XML, etc.)
 *
 * @author vgomelsky
 */
public interface MigrationSchemaConverter extends Converter<Schema, String> {

	/**
	 * Returns the converter type for this converter
	 */
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType();
}
