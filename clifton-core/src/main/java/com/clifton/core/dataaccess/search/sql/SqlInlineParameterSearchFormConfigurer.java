package com.clifton.core.dataaccess.search.sql;


import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;


/**
 * The <code>SqlInlineParameterSearchFormConfigurer</code> extends the SqlSearchFormConfigurer
 * but uses inline parameters for numbers (and Boolean equals) instead of parameters.
 * <p/>
 * Use discretion - some cases this can help speed up queries, but not necessarily all cases
 *
 * @author manderson
 */
public class SqlInlineParameterSearchFormConfigurer extends SqlSearchFormConfigurer {

	/**
	 * Constructs a new {@link SearchConfigurer} based on the specified {@link BaseEntitySearchForm}
	 * and runs validation.
	 *
	 * @param searchForm
	 */
	public SqlInlineParameterSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		super(searchForm);
	}


	/**
	 * appends ids value (Integer/Short/Long) as actual number
	 * - booleans - append 0 or 1
	 * all others uses default implementation (? with parameter added)
	 */
	@Override
	protected String getParameterSyntax(Object value, SqlSelectCommand criteria) {
		if (value instanceof Integer || value instanceof Short || value instanceof Long) {
			return "" + value;
		}

		if (value instanceof Boolean) {
			if (((Boolean) value)) {
				return "1";
			}
			return "0";
		}
		return super.getParameterSyntax(value, criteria);
	}
}
