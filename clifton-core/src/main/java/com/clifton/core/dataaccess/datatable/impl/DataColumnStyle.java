package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>DataColumnStyle</code> ...
 *
 * @author manderson
 */
public class DataColumnStyle {

	public static final String FONT_COLOR_BLACK = "BLACK";
	public static final String FONT_COLOR_RED = "RED";

	public static final String FONT_WEIGHT_NORMAL = "normal";
	public static final String FONT_WEIGHT_BOLD = "bold";

	public static final String ALIGN_LEFT = "left";
	public static final String ALIGN_RIGHT = "right";
	public static final String ALIGN_CENTER = "center";

	private String fontColor = FONT_COLOR_BLACK;
	private String backgroundColor;
	private String fontWeight = FONT_WEIGHT_NORMAL;
	private String align = ALIGN_LEFT;
	private int width = 100;
	private boolean wrapText = false;

	/**
	 * DataType is used when specific data types are used, i.e. Money, Currency, etc.
	 */
	private String dataType;


	/**
	 * Expect Style string to be in format
	 * property=value,property=value
	 * i.e. align=right,fontColor=red, etc.
	 *
	 * @param style
	 */
	public DataColumnStyle(String style) {
		parseStyle(style);
	}


	private void parseStyle(String style) {
		if (style == null) {
			return;
		}
		String[] properties = style.split(",");
		if (properties.length == 0) {
			return;
		}
		for (String property : properties) {
			String[] propVal = property.split("=");
			if (propVal.length != 2 || propVal[0] == null) {
				continue;
			}
			try {
				String prop = propVal[0].trim();
				String value = propVal[1] == null ? null : propVal[1].trim();
				BeanUtils.setPropertyValue(this, prop, value);
			}
			catch (Exception e) {
				throw new ValidationException("Error setting property [" + propVal[0] + "] to value [" + propVal[1] + "]", e);
			}
		}
	}


	public String getFontColor() {
		return this.fontColor;
	}


	public void setFontColor(String fontColor) {
		this.fontColor = fontColor;
	}


	public String getFontWeight() {
		return this.fontWeight;
	}


	public void setFontWeight(String fontWeight) {
		this.fontWeight = fontWeight;
	}


	public String getAlign() {
		return this.align;
	}


	public void setAlign(String align) {
		this.align = align;
	}


	public int getWidth() {
		return this.width;
	}


	public void setWidth(int width) {
		this.width = width;
	}


	public boolean isWrapText() {
		return this.wrapText;
	}


	public void setWrapText(boolean wrapText) {
		this.wrapText = wrapText;
	}


	public String getBackgroundColor() {
		return this.backgroundColor;
	}


	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}


	public String getDataType() {
		return this.dataType;
	}


	public void setDataType(String dataType) {
		this.dataType = dataType;
	}
}
