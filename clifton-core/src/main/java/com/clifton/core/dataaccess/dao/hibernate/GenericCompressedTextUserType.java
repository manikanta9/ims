package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.clifton.core.converter.json.jackson.strategy.JacksonHibernateStrategy;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compression.GzipCompressionUtils;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.IOException;
import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Comparator;
import java.util.Objects;
import java.util.Properties;


/**
 * The <code>GenericCompressedTextUserType</code>
 *
 * @author stevenf
 */
public class GenericCompressedTextUserType implements UserType, ParameterizedType, Comparator<GenericCompressedTextUserType> {

	public static final ObjectMapper MAPPER = new JacksonObjectMapper(new JacksonHibernateStrategy());


	private static final int[] SQL_TYPES = {Types.LONGVARBINARY};
	private JavaType javaType;


	/**
	 * Allows for classes to extends and override the object mapper implementation.
	 */
	public ObjectMapper getObjectMapper() {
		return MAPPER;
	}


	@Override
	public void setParameterValues(Properties parameters) {
		String typeDefinitionClassName = parameters.getProperty("typeDefinitionClassName");
		try {
			this.javaType = getObjectMapper().getTypeFactory().constructFromCanonical(typeDefinitionClassName);
		}
		catch (IllegalArgumentException e) {
			throw new HibernateException(String.format("Unable to parse class from canonical representation [%s].", typeDefinitionClassName), e);
		}
	}


	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}


	@Override
	public Class<?> returnedClass() {
		return this.javaType.getRawClass();
	}


	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, @SuppressWarnings("unused") SharedSessionContractImplementor session, @SuppressWarnings("unused") Object owner) throws HibernateException,
			SQLException {
		Object object = null;
		try {
			byte[] bytes = resultSet.getBytes(names[0]);
			if (bytes != null && !resultSet.wasNull()) {
				object = getObjectMapper().readValue(GzipCompressionUtils.decompress(bytes), this.javaType);
			}
		}
		catch (IOException e) {
			throw new HibernateException("unable to read object from result set", e);
		}
		return object;
	}


	@Override
	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, @SuppressWarnings("unused") SharedSessionContractImplementor session) throws HibernateException, SQLException {
		if (value == null) {
			preparedStatement.setNull(index, SQL_TYPES[0]);
		}
		else {
			try {
				preparedStatement.setBytes(index, GzipCompressionUtils.compress(getObjectMapper().writeValueAsBytes(value)));
			}
			catch (IOException ioe) {
				throw new HibernateException("unable to set object to result set", ioe);
			}
		}
	}


	@Override
	public Object deepCopy(Object value) throws HibernateException {
		Object copy = null;
		if (value != null) {
			try {
				if (value instanceof String) {
					return getObjectMapper().readValue((String) value, this.javaType);
				}
				else {
					return getObjectMapper().readValue(getObjectMapper().writeValueAsString(value), this.javaType);
				}
			}
			catch (IOException e) {
				throw new HibernateException("unable to deep copy object", e);
			}
		}
		return copy;
	}


	@Override
	public boolean isMutable() {
		return true;
	}


	@Override
	public Object assemble(Serializable cached, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return this.deepCopy(cached);
	}


	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		try {
			return getObjectMapper().writeValueAsString(value);
		}
		catch (JsonProcessingException e) {
			throw new HibernateException("unable to disassemble object", e);
		}
	}


	@Override
	public Object replace(Object original, @SuppressWarnings("unused") Object target, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return original;
	}


	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}


	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		return Objects.equals(x, y);
	}


	@Override
	public int compare(GenericCompressedTextUserType o1, GenericCompressedTextUserType o2) {
		return CompareUtils.compare(o1, o2);
	}
}
