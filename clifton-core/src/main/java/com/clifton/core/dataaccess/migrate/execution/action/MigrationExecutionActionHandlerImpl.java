package com.clifton.core.dataaccess.migrate.execution.action;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonMigrationStrategy;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ManyToOneEntity;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.execution.database.MigrationExecutionDatabaseHandler;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.action.Argument;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>MigrationExecutionActionHandler</code> is used to handle json migration actions.
 * It deserialized the json to an object that is then walked and all children are looked up by
 * natural key and saved using their respective DAOs.
 *
 * @author stevenf
 */
public class MigrationExecutionActionHandlerImpl implements MigrationExecutionActionHandler {

	private static final char ENTITY_KEY_SEPARATOR = '#';

	//Top level object is mostly used during debugging to view how the population of its child objects is progressing.
	private Object topLevelObject;
	private ApplicationContext applicationContext;
	private DaoLocator daoLocator;

	private JsonHandler<JacksonMigrationStrategy> jsonHandler;
	private IdentityObject migrationUser;
	private MigrationExecutionDatabaseHandler migrationExecutionDatabaseHandler;

	private Map<String, Object> objectHasBeenWalkedMap = new LinkedHashMap<>();

	private Set<DelayedObject> delayedObjectList = new LinkedHashSet<>();

	public static class DelayedObject {

		private Object parentObject;
		private Object fieldObject;

		private String fieldName;


		public Object getParentObject() {
			return this.parentObject;
		}


		public void setParentObject(Object parentObject) {
			this.parentObject = parentObject;
		}


		public Object getFieldObject() {
			return this.fieldObject;
		}


		public void setFieldObject(Object fieldObject) {
			this.fieldObject = fieldObject;
		}


		public String getFieldName() {
			return this.fieldName;
		}


		public void setFieldName(String fieldName) {
			this.fieldName = fieldName;
		}
	}


	@Override
	public Map<String, Object> executeActions(List<Action> actionList, Map<String, Action> actionMap, ApplicationContext context) {
		//Add migration user to the context
		ContextHandler contextHandler = (ContextHandler) context.getBean("contextHandler");
		contextHandler.setBean(Context.USER_BEAN_NAME, getMigrationUser());
		setApplicationContext(context);

		Map<String, Object> actionReturnValMap = new LinkedHashMap<>();
		for (Action action : CollectionUtils.getIterable(actionList)) {
			actionReturnValMap.put(action.getName(), executeAction(action, actionMap));
			setTopLevelObject(null);
		}
		//Copy the values to a local map, and clear to global to avoid concurrent modification exception.
		Set<DelayedObject> localDelayedObjectList = new LinkedHashSet<>(getDelayedObjectList());
		getDelayedObjectList().clear();
		handleDelayedObjects(localDelayedObjectList, actionMap);
		return actionReturnValMap;
	}


	@SuppressWarnings("unchecked")
	private void handleDelayedObjects(Set<DelayedObject> delayedObjectList, Map<String, Action> actionMap) {
		for (DelayedObject delayedObject : CollectionUtils.getIterable(delayedObjectList)) {
			try {
				List<Object> fieldObjectList = (List<Object>) delayedObject.getFieldObject();
				for (Object fieldObject : fieldObjectList) {
					//Remove this object so we ensure it is walked again
					getObjectHasBeenWalkedMap().remove(getObjectKey(fieldObject));
					walkObject(fieldObject, actionMap);
					handleObject(fieldObject, delayedObject.getParentObject(), actionMap);
				}
			}
			catch (Exception e) {
				LogUtils.error(MigrationExecutionActionHandlerImpl.class, String.format("An exception occurred while handling delayed object. Field name: [%s]. Object: [%s].", delayedObject.getFieldName(), delayedObject.getFieldObject()), e);
			}
		}
		if (!getDelayedObjectList().isEmpty()) {
			//Copy the values to a local map, and clear to global to avoid concurrent modification exception.
			Set<DelayedObject> localDelayedObjectList = new LinkedHashSet<>(getDelayedObjectList());
			getDelayedObjectList().clear();
			handleDelayedObjects(localDelayedObjectList, actionMap);
		}
	}


	private Object executeAction(Action action, Map<String, Action> actionMap) {
		ApplicationContext context = getApplicationContext();
		// Get and Validate the Service Bean
		if (!context.containsBean(action.getBean())) {
			throw new RuntimeException("Missing bean [" + action.getBean() + "] in the context.");
		}
		Object service = context.getBean(action.getBean());
		// Get and Validate the Method on the Service Bean
		Method method = MethodUtils.getMethod(service.getClass(), action.getMethod(), false);
		if (method == null) {
			throw new RuntimeException("Bean of Class [" + service.getClass() + "] is missing method [" + action.getMethod() + "] or method is not accessible.");
		}

		// Get & Validate Arguments as Objects as correct types
		Class<?>[] parameterTypes = method.getParameterTypes();
		if (parameterTypes.length != CollectionUtils.getSize(action.getArgumentList())) {
			throw new RuntimeException("The number arguments passed [" + CollectionUtils.getSize(action.getArgumentList()) + "] do not match the actual number of arguments ["
					+ parameterTypes.length + "].");
		}

		List<Object> args = new ArrayList<>();
		for (int i = 0; i < method.getParameterTypes().length; i++) {
			// Add correctly typed object to the list of method arguments
			args.add(getArgumentValueAsParameterTypeObject(action.getArgumentList().get(i), method.getParameterTypes()[i], actionMap));
		}
		return DaoUtils.executeWithInsertDirtyCheckingDisabled(() -> {
			try {
				if (CollectionUtils.getSize(args) > 0 && args.get(0) != null && CompareUtils.isEqual(this.topLevelObject, args.get(0))) {
					IdentityObject argObject = (IdentityObject) args.get(0);
					// Only attempt to look up existing object if the table has a natural key, otherwise assume all inserts
					ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate(argObject);
					Set<String> naturalKeys = dao.getConfiguration().getNaturalKeys(getDaoLocator());
					if (!CollectionUtils.isEmpty(naturalKeys)) {
						IdentityObject existingObject = dao.findByNaturalKey(argObject);
						if (existingObject != null) {
							BeanUtils.setPropertyValue(argObject, "id", existingObject.getIdentity());
							if (existingObject instanceof UpdatableOnlyEntity) {
								BeanUtils.setPropertyValue(argObject, "rv", ((UpdatableOnlyEntity) existingObject).getRv());
							}
						}
					}
				}
				return method.invoke(service, args.toArray());
			}
			catch (Exception e) {
				String argumentValues = StringUtils.collectionToCommaDelimitedString(action.getArgumentList(), Argument::getValue);
				throw new RuntimeException("Error invoking method [" + action.getBean() + "." + action.getMethod() + "] " + (argumentValues != null ? " with arguments [" + argumentValues + "]" : ""), e);
			}
		});
	}


	private Object getArgumentValueAsParameterTypeObject(Argument argument, Class<?> parameterTypeClass, Map<String, Action> actionMap) {
		Object objectValue = argument.getObjectValue();
		if (objectValue != null) {
			return objectValue;
		}
		String stringValue = argument.getValue();
		try {
			// Lookup the Value in the Database
			if (argument.isSelect()) {
				return getMigrationExecutionDatabaseHandler().queryForObject(stringValue, parameterTypeClass);
			}

			// String values
			if (String.class.equals(parameterTypeClass)) {
				return stringValue;
			}

			// Short Values
			if (Short.class.equals(parameterTypeClass) || short.class.equals(parameterTypeClass)) {
				return new Short(stringValue);
			}

			// Integer Values
			if (Integer.class.equals(parameterTypeClass) || int.class.equals(parameterTypeClass)) {
				return new Integer(stringValue);
			}

			// Double Values
			if (Double.class.equals(parameterTypeClass) || double.class.equals(parameterTypeClass)) {
				return new Double(stringValue);
			}

			// Long Values
			if (Long.class.equals(parameterTypeClass) || long.class.equals(parameterTypeClass)) {
				return new Long(stringValue);
			}

			// Boolean Values
			if (Boolean.class.equals(parameterTypeClass) || boolean.class.equals(parameterTypeClass)) {
				return Boolean.valueOf(stringValue);
			}

			// Date Values
			if (Date.class.equals(parameterTypeClass)) {
				return DateUtils.toDate(stringValue);
			}

			//If parameterTypeClass is an IdentityObject, then this is a json migration; deserialize and walk the object to hydrate.
			if (IdentityObject.class.isAssignableFrom(parameterTypeClass)) {
				Object object = getJsonHandler().fromJson(stringValue, parameterTypeClass, new JacksonMigrationStrategy());
				if (this.topLevelObject == null) {
					this.topLevelObject = object;
				}
				walkObject(object, actionMap);
				return object;
			}
			// Unsupported Types
			throw new RuntimeException("Unsupported parameter type [" + parameterTypeClass.getName() + "].");
		}
		catch (Exception e) {
			throw new RuntimeException("Error converting value [" + stringValue + "] to type [" + parameterTypeClass.getName() + "]", e);
		}
	}


	@SuppressWarnings("unchecked")
	public void walkObject(Object object, Map<String, Action> actionMap) {
		String key = getObjectKey(object);
		if (!getObjectHasBeenWalkedMap().containsKey(key)) {
			//Set the object as been walked at entry - this allows child items to be aware of parent items that they reference as children.
			//e.g. SystemBeanPropertyType is a child of SystemBeanType, but also references SystemBeanType as a child..
			getObjectHasBeenWalkedMap().put(key, object);
			//Inspect the object for child fields that must be populated/saved before this object can be.
			for (Field field : getDeclaredFields(object)) {
				Object fieldObject = BeanUtils.getPropertyValue(object, field.getName());
				if (fieldObject != null) {
					if (List.class.isAssignableFrom(fieldObject.getClass())) {
						OneToManyEntity oneToManyEntity = AnnotationUtils.getAnnotation(field, OneToManyEntity.class);
						if (oneToManyEntity == null || oneToManyEntity.delayed()) {
							DelayedObject delayedObject = new DelayedObject();
							delayedObject.setParentObject(object);
							delayedObject.setFieldObject(fieldObject);
							delayedObject.setFieldName(field.getName());
							getDelayedObjectList().add(delayedObject);
						}
						else {
							List<Object> handledObjectList = new ArrayList<>();
							List<Object> fieldObjectList = (List<Object>) fieldObject;
							for (Object o : fieldObjectList) {
								//Only walk and handle if not already encountered.
								if (!getObjectHasBeenWalkedMap().containsKey(getObjectKey(o))) {
									walkObject(o, actionMap);
									handledObjectList.add(handleObject(o, object, actionMap));
								}
								else {
									handledObjectList.add(o);
								}
							}
							BeanUtils.setPropertyValue(object, field.getName(), handledObjectList);
						}
					}
					else if (IdentityObject.class.isAssignableFrom(fieldObject.getClass())) {
						ManyToOneEntity manyToOneEntity = AnnotationUtils.getAnnotation(field, ManyToOneEntity.class);
						if (manyToOneEntity == null) {
							walkObject(fieldObject, actionMap);
							BeanUtils.setPropertyValue(object, field.getName(), handleObject(fieldObject, object, actionMap));
						}
					}
				}
			}
		}
	}


	private Object handleObject(Object object, Object parentObject, Map<String, Action> actionMap) {
		object = getExistingObject(object, parentObject);
		if (object != null && IdentityObject.class.isAssignableFrom(object.getClass()) && ((IdentityObject) object).isNewBean()) {
			if (actionMap != null) {
				Action action = getAction(object, actionMap);
				if (action != null) {
					ValidationUtils.assertNotNull(action.getBean(), "An action must define a bean.");
					ValidationUtils.assertNotNull(action.getMethod(), "An action must define a method.");
					//Set this object as the argument for the action
					action.setArgumentList(Stream.of(new Argument(object)).collect(Collectors.toList()));
					executeAction(action, actionMap);
				}
			}
		}
		return object;
	}


	private Action getAction(Object object, Map<String, Action> actionMap) {
		Action action = actionMap.get(object.getClass().getName());
		if (action == null) {
			action = actionMap.get(object.getClass().getSuperclass().getName());
		}
		return action;
	}


	private Object getExistingObject(Object object, Object parentObject) {
		String key = getObjectKey(object);
		Object o = getObjectHasBeenWalkedMap().get(key);
		if (o != null && IdentityObject.class.isAssignableFrom(o.getClass())) {
			IdentityObject identityObject = (IdentityObject) o;
			//Only lookup if the id isn't populated already.
			if (identityObject.isNewBean()) {
				try {
					//Find the object by natural key and update as walked so we don't have to look it up again later.
					Object naturalKeyObject = getDaoLocator().locate(identityObject).findByNaturalKey(identityObject);
					if (naturalKeyObject != null) {
						o = naturalKeyObject;
					}
				}
				catch (IllegalArgumentException iae) {
					LogCommand.ofThrowableAndMessage(getClass(), iae, "Object has no natural keys!");
				}
				catch (Exception e) {
					LogUtils.warn(LogCommand.ofThrowableAndMessage(getClass(), e, "Failed to find object [" + o.getClass() + "] with parent [" + parentObject.getClass() + "] by natural key: " + ExceptionUtils.getOriginalMessage(e) + ".  Object will be created instead."));
				}
			}
		}
		return o != null ? o : object;
	}


	private String getObjectKey(Object object) {
		StringBuilder key = new StringBuilder();
		if (object != null && IdentityObject.class.isAssignableFrom(object.getClass())) {
			key.append(object.getClass());
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate((IdentityObject) object);
			Set<String> naturalKeys = dao.getConfiguration().getNaturalKeys(getDaoLocator());
			if (naturalKeys == null) {
				//In case of no natural keys use this for now
				for (Field field : getDeclaredFields(object)) {
					key.append(ENTITY_KEY_SEPARATOR).append(BeanUtils.getPropertyValue(object, field.getName()));
				}
				// RESTORE THIS LINE, delete performance 45, dbmigraterestore, restore 45,
				// then dbmigrate to duplicate transactional issue during json migration failure.
				//return null;
			}
			else {
				for (String naturalKey : naturalKeys) {
					key.append(ENTITY_KEY_SEPARATOR).append(BeanUtils.getPropertyValue(object, naturalKey));
				}
			}
		}
		return key.length() > 0 ? key.toString() : null;
	}


	private Field[] getDeclaredFields(Object bean) {
		Field[] fields = bean.getClass().getDeclaredFields();
		Class<?> superClass = bean.getClass().getSuperclass();
		while (superClass != null) {
			fields = ArrayUtils.addAll(fields, superClass.getDeclaredFields());
			superClass = superClass.getSuperclass();
		}
		return fields;
	}


	//This is a generic method that is used when a child object is saved by the parent it references.
	//e.g. a tradeFill references the trade to which it belongs.  The trade itself will save it's
	//tradeFillList, so we only want to populate the tradeFill, but when it comes time to save it
	//we just want to return the original object so that once the trade is done being walked and is
	//saved it will then save the tradeFillList.
	//This is set in the schema for the DTO:
	//	<table name="TradeFill" dtoClass="com.clifton.trade.TradeFill" uploadAllowed="true"
	//      migrationServiceBeanName="migrationExecutionActionHandler" migrationServiceMethodName="doNotSaveObject">
	@Override
	public IdentityObject doNotSaveObject(IdentityObject identityObject) {
		return identityObject;
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Object getTopLevelObject() {
		return this.topLevelObject;
	}


	public void setTopLevelObject(Object topLevelObject) {
		this.topLevelObject = topLevelObject;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public Map<String, Object> getObjectHasBeenWalkedMap() {
		return this.objectHasBeenWalkedMap;
	}


	public void setObjectHasBeenWalkedMap(Map<String, Object> objectHasBeenWalkedMap) {
		this.objectHasBeenWalkedMap = objectHasBeenWalkedMap;
	}


	public JsonHandler<JacksonMigrationStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JacksonMigrationStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public IdentityObject getMigrationUser() {
		return this.migrationUser;
	}


	public void setMigrationUser(IdentityObject migrationUser) {
		this.migrationUser = migrationUser;
	}


	public MigrationExecutionDatabaseHandler getMigrationExecutionDatabaseHandler() {
		return this.migrationExecutionDatabaseHandler;
	}


	public void setMigrationExecutionDatabaseHandler(MigrationExecutionDatabaseHandler migrationExecutionDatabaseHandler) {
		this.migrationExecutionDatabaseHandler = migrationExecutionDatabaseHandler;
	}


	public DaoLocator getDaoLocator() {
		if (this.daoLocator == null) {
			this.daoLocator = (DaoLocator) getApplicationContext().getBean("daoLocator");
		}
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public Set<DelayedObject> getDelayedObjectList() {
		return this.delayedObjectList;
	}


	public void setDelayedObjectList(Set<DelayedObject> delayedObjectList) {
		this.delayedObjectList = delayedObjectList;
	}
}
