package com.clifton.core.dataaccess.dao.hibernate;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import org.hibernate.CustomEntityDirtinessStrategy;
import org.hibernate.Session;
import org.hibernate.persister.entity.EntityPersister;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicReference;


/**
 * <code>DirtyCheckingStrategy</code> is an implementation of Hibernate's {@link org.hibernate.CustomEntityDirtinessStrategy}
 * used to track entities that have been saved via our DAOs. The purpose is to avoid all entities loaded within an
 * active Hibernate session from being checked for dirtiness and flushed to the database even when they were not
 * saved via our DAOs.
 * <p>
 * Entities are associated with a session by being read, saved, or deleted via the IMS DAOs or by using
 * OpenSessionInView (session opened for the duration of the request and reused by the DAOs) requests during binding.
 * Any entity associated with an active session is capable of being flushed/persisted to the database if modified once a
 * non-read-only transaction is opened. This strategy implementation assists Hibernate in determining which items
 * are capable of being flushed by limiting the 'dirty aware' entities to new or updated entities that have been
 * saved through one of our DAOs.
 * <p>
 * When a Transaction, capable of writing, is opened, Hibernate will periodically flush dirty entities loaded
 * within the active session. When a flush occurs, every entity the session knows about will have the following
 * methods of this strategy invoked for it in the specified order.
 * <p>
 * 1. {@link DirtyCheckingStrategy#canDirtyCheck(Object, EntityPersister, Session)} - invoked to determine if this
 * strategy is capable of determining if the entity is dirty. This strategy can 'dirty check' all {@link IdentityObject}s.
 * <p>
 * 2. {@link DirtyCheckingStrategy#isDirty(Object, EntityPersister, Session)} - invoked if the previous call to
 * canDirtyCheck returned {@code true} to determine if the object is dirty. This strategy will return {@code true}
 * if the {@link IdentityObject} is new or has been registered as 'dirty aware' by one of our DAOs.
 * <p>
 * 3. {@link DirtyCheckingStrategy#findDirty(Object, EntityPersister, Session, DirtyCheckContext)} - invoked if the
 * previous call to isDirty returns {@code true} to determine which entity attributes are dirty. Dirty attributes
 * can be relayed to Hibernate by implementing an {@link org.hibernate.CustomEntityDirtinessStrategy.AttributeChecker}
 * that can be used by the provided {@link org.hibernate.CustomEntityDirtinessStrategy.DirtyCheckContext}. This
 * strategy does not currently provide an {@code AttributeChecker}, which tells Hibernate to do its own dirty checking
 * on each attribute of the entity.
 * <p>
 * 4. {@link DirtyCheckingStrategy#resetDirty(Object, EntityPersister, Session)} - invoked after flush has occured if
 * an entity was identified as dirty to clear its 'dirty aware' status.
 * <p>
 * NOTE: IMS has five different data sources: IMS, Data Warehouse, Integration, Portal, and FIX. Each source has its own
 * {@link org.hibernate.SessionFactory} that extends the Core IMS one. Thus, we configure this strategy once with the
 * property: hibernate.entity_dirtiness_strategy. Each source will use its own unique singleton instance of this strategy.
 *
 * @author NickK
 */
public class DirtyCheckingStrategy implements CustomEntityDirtinessStrategy {

	private static final String DIRTY_ENTITY_ID_MAP_KEY = "DIRTY_ENTITY_ID_MAP";

	/*
	 * The ContextHandler to use for tracking the Map of Entity types to IDs that are available for dirty checking. Each
	 * Hibernate session is associated with a thread, so we can track IDs by thread as well. The default context relies on
	 * ThreadLocals. Hibernate will call the resetDirty to remove the registered dirty aware entities. Anything that does
	 * not get cleaned up will when the web request ends/exits.
	 *
	 * Use AtomicReference for tracking this singleton object's context handler. The context handler will be set
	 * the first time HibernateUpdatableDAO registers an entity for dirty checking.
	 */
	private final AtomicReference<ContextHandler> contextHandlerReference = new AtomicReference<>();


	@Override
	public boolean canDirtyCheck(Object entity, EntityPersister persister, Session session) {
		return entity instanceof IdentityObject;
	}


	@Override
	public boolean isDirty(Object entity, EntityPersister persister, Session session) {
		IdentityObject identityEntity = (IdentityObject) entity;
		return identityEntity.isNewBean() || isEntityDirtyAware(identityEntity);
	}


	@Override
	public void resetDirty(Object entity, EntityPersister persister, Session session) {
		removeRegisteredDirtyAwareEntity(entity);
	}


	@Override
	public void findDirty(Object entity, EntityPersister persister, Session session, DirtyCheckContext dirtyCheckContext) {
		/*
		 * Doing nothing will cause Hibernate to do its traditional dirty checking; comparing the new and old values
		 * the session is aware of.
		 *
		 * If we decide to track dirty fields on our own, we would do something similar to this example:
		 * 		final IdentityObject identityEntity = (IdentityObject) entity;
		 *		if (!identityEntity.isNewBean()) {
		 *			dirtyCheckContext.doDirtyChecking(
		 *					attributeInformation -> {
		 *						Object currentState = attributeInformation.getCurrentValue();
		 *						Object previousState = attributeInformation.getLoadedValue(); // Fails with NPE from Hibernate !?!
		 *						return !CompareUtils.isEqual(currentState, previousState);
		 *					}
		 *			);
		 *		}
		 *
		 * This example is very simple and may miss some cases, such as uninitialized lazy fields. For a more
		 * advanced example, you could look at Hibernate's
		 * org.hibernate.type.TypeHelper.findDirty(
		 *      NonIdentifierAttribute[] properties,
		 *      Object[] currentState,
		 *      Object[] previousState,boolean[][] includeColumns,
		 *      boolean anyUninitializedProperties,
		 *      SessionImplementor session)
		 */
	}


	/**
	 * Registers an Entity as dirty aware for the current thread's Hibernate session. Registering the entity
	 * will tell Hibernate that it can be checked for modifications that can be flushed to the database.
	 * <p>
	 * The {@link ContextHandler} is provided to use for storing the registered identities. It is expected that
	 * the context handler is backed by ThreadLocal for thread safety, which is the IMS default context handler.
	 */
	public <T extends IdentityObject> void registerEntityAsDirtyAware(ContextHandler contextHandler, T entity) {
		if (!entity.isNewBean()) {
			this.contextHandlerReference.compareAndSet(null, contextHandler);
			getDirtyAwareIdentitySetForEntity(entity, true).add(entity.getIdentity());
		}
	}


	/**
	 * Returns the Set of registered dirty aware identities for a provided Entity type (Class) for the current thread.
	 * If a Set does not currently exist matching the provided entity, a new one can be initialized and returned by providing
	 * <code>true</code> for the <code>createNewIfNotPresent</code> argument or {@link Collections#emptySet()} is returned to
	 * avoid excessive object creation.
	 * <p>
	 * It is expected that the {@link ContextHandler} is backed by ThreadLocal, so each thread uses its own values. For
	 * this reason, the Map for
	 */
	private <T extends IdentityObject> Set<Serializable> getDirtyAwareIdentitySetForEntity(T entity, boolean createNewIfNotPresent) {
		ContextHandler contextHandler = this.contextHandlerReference.get();
		if (contextHandler != null) {
			@SuppressWarnings("unchecked")
			Map<Class<?>, Set<Serializable>> dirtyEntityIdMap = (Map<Class<?>, Set<Serializable>>) contextHandler.getBean(DIRTY_ENTITY_ID_MAP_KEY);
			if (dirtyEntityIdMap == null) {
				if (createNewIfNotPresent) {
					dirtyEntityIdMap = new HashMap<>();
					contextHandler.setBean(DIRTY_ENTITY_ID_MAP_KEY, dirtyEntityIdMap);
					Set<Serializable> idSet = new HashSet<>();
					dirtyEntityIdMap.put(entity.getClass(), idSet);
					return idSet;
				}
			}
			else {
				Set<Serializable> idSet = dirtyEntityIdMap.get(entity.getClass());
				if (idSet != null) {
					return idSet;
				}
				else if (createNewIfNotPresent) {
					idSet = new HashSet<>();
					dirtyEntityIdMap.put(entity.getClass(), idSet);
					return idSet;
				}
			}
		}
		return Collections.emptySet();
	}


	/**
	 * Removes the entity from the registered dirty aware mapping, if it exists, for the current thread.
	 * Returns true if the entity existed and was removed, false otherwise.
	 */
	private boolean removeRegisteredDirtyAwareEntity(Object entity) {
		IdentityObject identityEntity = (IdentityObject) entity;
		return getDirtyAwareIdentitySetForEntity(identityEntity, false).remove(identityEntity.getIdentity());
	}


	/**
	 * Returns whether the provided entity has been registered for dirty checking for the current thread.
	 */
	private boolean isEntityDirtyAware(IdentityObject entity) {
		return getDirtyAwareIdentitySetForEntity(entity, false).contains(entity.getIdentity());
	}
}
