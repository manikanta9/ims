package com.clifton.core.dataaccess.file;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>DataTableConfig</code> contains a {@link DataTable} and
 * any configuration necessary for working with the DataTable when transferring
 * it to another file
 *
 * @author manderson
 */
public class DataTableFileConfig {

	public static final String DATA_TABLE_HEADER_DEFAULT_STYLE = "fontWeight=bold,wrapText=false";
	public static final String DATA_TABLE_HEADER_EXPORT_STYLE = "fontWeight=bold,wrapText=false,backgroundColor=cyan";
	public static final String DATA_TABLE_HEADER_ERROR_STYLE = "fontWeight=bold,wrapText=false,fontColor=red";

	private ApplicationContextService applicationContextService;

	private final DataTable dataTable;

	private String defaultHeaderStyle;

	/**
	 * The list of columns for the dataTable, but excludes
	 * any columns that shouldn't be displayed, i.e. hidden
	 */
	private List<DataColumn> columnList;

	/**
	 * Maps the String representation of a {@link DataColumnStyle} to the actual
	 * object
	 */
	private Map<String, DataColumnStyle> dataColumnStyleMap;

	/**
	 * A list of column widths for the columns in columnList attribute
	 */
	private List<Integer> columnWidthList;

	/**
	 * The total width of all of the columns in columnList attribute
	 */
	private int totalWidth;


	/**
	 * Omit outputting the header row.
	 */
	private boolean suppressHeaderRow;


	public DataTableFileConfig(DataTable dataTable) {
		this(dataTable, null);
	}


	public DataTableFileConfig(DataTable dataTable, String defaultHeaderStyle) {
		this(dataTable, defaultHeaderStyle, null);
	}


	/**
	 * Columns is a String representation of which/what columns to display and
	 * any special formatting for each column that will be used when creating the file.
	 *
	 * @param dataTable
	 * @param columns
	 */
	public DataTableFileConfig(DataTable dataTable, String defaultHeaderStyle, String columns) {
		this(dataTable, defaultHeaderStyle, columns, false, null);
	}


	public DataTableFileConfig(DataTable dataTable, String defaultHeaderStyle, String columns, boolean overrideDataTableColumnNames, ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
		this.dataTable = dataTable;
		if (dataTable == null) {
			return;
		}
		if (dataTable.getColumnList() == null) {
			return;
		}
		this.defaultHeaderStyle = StringUtils.coalesce(true, defaultHeaderStyle, DATA_TABLE_HEADER_DEFAULT_STYLE);
		this.columnList = new ArrayList<>();
		this.dataColumnStyleMap = new HashMap<>();
		this.columnWidthList = new ArrayList<>();
		this.totalWidth = 0;

		String[] uiColumns = columns != null ? columns.split("\\|") : null;
		// only override the column names if the column sizes are the same
		overrideDataTableColumnNames = overrideDataTableColumnNames && uiColumns != null && uiColumns.length == dataTable.getColumnCount();

		// marks columns where the names don't match as hidden
		processColumnString(columns, overrideDataTableColumnNames);

		for (DataColumn column : dataTable.getColumnList()) {
			// Skip Hidden Fields
			if (column.isHidden()) {
				continue;
			}
			this.columnList.add(column);
			// Data Style
			String style = getStyleString(column.getStyle(), false);
			addStyle(style);

			// Header Style
			style = getStyleString(style, true);
			DataColumnStyle styleObj = addStyle(style);
			this.columnWidthList.add(styleObj.getWidth());
			this.totalWidth += styleObj.getWidth();
		}
	}


	/**
	 * Tries to find the DataTable column name by looking at the input columns display name or dataIndex name (dataTableName)
	 * if it is not found, the column is marked as hidden and will not be included in the output
	 * <p>
	 * Also updates the DataTable column name to the display name if 'overrideColumnName' is true
	 */
	@SuppressWarnings("unchecked")
	private void processColumnString(String columns, boolean overrideColumnName) {
		if (StringUtils.isEmpty(columns)) {
			return;
		}
		String[] columnArray = columns.split("\\|");
		for (DataColumn dc : this.dataTable.getColumnList()) {
			boolean found = false;
			String columnName = dc.getColumnName();
			for (String str : columnArray) {
				String[] columnItems = str.split(":");
				String displayColumnName = !ArrayUtils.isEmpty(columnItems) && columnItems.length > 0 ? columnItems[0] : null;
				String dataTableColumnName = !ArrayUtils.isEmpty(columnItems) && columnItems.length > 1 ? columnItems[1] : null;

				String columnValueConverterName = !ArrayUtils.isEmpty(columnItems) && columnItems.length > 3 ? columnItems[3] : null;

				if ((!StringUtils.isEmpty(displayColumnName) && displayColumnName.startsWith(columnName)) || (!StringUtils.isEmpty(dataTableColumnName) && dataTableColumnName.startsWith(columnName))) {
					found = true;
					// NOT SURE WHY THIS WAS HAPPENING - IS ALREADY SET???? dc.setStyle(str.split(":")[2]);

					if (overrideColumnName && !StringUtils.isEmpty(displayColumnName)) {
						dc.setColumnName(displayColumnName);
					}

					if (!StringUtils.isEmpty(columnValueConverterName)) {
						ValidationUtils.assertNotNull(getApplicationContextService(), "You cannot use a columnValueConverter when the application context has not been injected!");
						Object columnConverterObject = getApplicationContextService().getContextBean(columnValueConverterName);
						if (columnConverterObject != null) {
							Converter<Object, Object> columnValueConverter = (Converter<Object, Object>) columnConverterObject;
							dc.setColumnValueConverter(columnValueConverter);
						}
					}
					break;
				}
			}
			// If it's not found in the column meta data consider the column to be hidden
			if (!found) {
				dc.setHidden(true);
			}
		}
	}


	/**
	 * Returns true is there is no columns, i.e. data
	 */
	public boolean isBlank() {
		return (this.columnList == null || this.columnList.isEmpty());
	}


	public DataColumnStyle getDataColumnStyle(String style) {
		return this.dataColumnStyleMap.get(style);
	}


	public String getStyleString(String style, boolean header) {
		style = StringUtils.coalesce(true, style);
		// Header Style
		if (header) {
			style += ", " + this.defaultHeaderStyle;
		}
		return style;
	}


	private DataColumnStyle addStyle(String style) {
		return this.dataColumnStyleMap.computeIfAbsent(style, DataColumnStyle::new);
	}


	public List<DataColumn> getColumnList() {
		return this.columnList;
	}


	public List<Integer> getColumnWidthList() {
		return this.columnWidthList;
	}


	public int getTotalWidth() {
		return this.totalWidth;
	}


	public boolean isSuppressHeaderRow() {
		return this.suppressHeaderRow;
	}


	public void setSuppressHeaderRow(boolean suppressHeaderRow) {
		this.suppressHeaderRow = suppressHeaderRow;
	}


	public DataTable getDataTable() {
		return this.dataTable;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
