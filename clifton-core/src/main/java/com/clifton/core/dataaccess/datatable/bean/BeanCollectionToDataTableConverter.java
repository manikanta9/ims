package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.dataaccess.db.ClassToSqlTypeConverter;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.beans.PropertyDescriptor;
import java.sql.Types;
import java.util.Collection;
import java.util.Map;


/**
 * {@link BeanCollectionToDataTableConverter} converts a typed {@link Collection} of entities to an {@link com.clifton.core.dataaccess.datatable.DataTable}.
 *
 * @author manderson
 * @author michaelm
 */
public class BeanCollectionToDataTableConverter<F> extends BaseDataTableConverter<Collection<F>> {

	/**
	 * Any property that is retrieved via a {@link com.clifton.core.json.custom.CustomJsonString}
	 * should begin the property name with this prefix.  These values cannot use get Property Value
	 * and must use custom retrieval.
	 */
	protected static final String CUSTOM_JSON_STRING_PREFIX = "CUSTOM_JSON_STRING_";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private Map<String, PropertyDescriptor> propertyDescriptorMap;

	private String columnValueConverterOverrideName;

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BeanCollectionToDataTableConverter() {
		DataTableConverterConfiguration converterConfiguration = new DataTableConverterConfiguration();
		initialize(converterConfiguration);
	}


	public BeanCollectionToDataTableConverter(DataTableConverterConfiguration converterConfiguration) {
		initialize(converterConfiguration);
	}


	@Override
	public void validateConverterConfiguration() {
		// no custom validation or this converter
	}


	/**
	 * {@param dataColumnString} is passed in with the following syntax
	 * columnName:beanPropertyName:style|columnName:beanPropertyName:style
	 * where style is also a specially formatted string (see {@link DataColumnStyle})
	 */
	@Override
	public DataColumn[] getOrBuildDataColumnArray() {
		DataTableConverterConfiguration config = getConverterConfiguration();
		if (config.getDataColumnArray() != null) {
			return config.getDataColumnArray();
		}
		if (config.getDtoClass() != null) {
			setPropertyDescriptorMap((Class<F>) config.getDtoClass());
		}
		String[] fields = config.getDataColumnString() != null ? config.getDataColumnString().split("\\|") : new String[0];
		DataColumn[] dataColumnArray = new BeanDataColumn[fields.length];
		for (int i = 0; i < fields.length; i++) {
			dataColumnArray[i] = createBeanDataColumn(getConverterConfiguration().getApplicationContextService(), fields[i]);
		}
		return dataColumnArray;
	}


	@Override
	public BeanDataTable<F> convert(Collection<F> from) {
		BeanDataTable<F> table = new BeanDataTable<>(getDataColumnArrayForBeanCollection(from));
		// A new Row for each Bean
		for (int j = 0; j < CollectionUtils.getSize(from); j++) {
			F bean = CollectionUtils.getObjectAtIndex(from, j);
			DataRow row = new BeanDataRow<>(table, bean);
			table.addRow(row);
		}
		return table;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected DataTypes getDataType(String beanPropertyName) {
		if (beanPropertyName.startsWith(CUSTOM_JSON_STRING_PREFIX)) {
			return null;
		}
		ReadOnlyDAO<IdentityObject> dao = getDAOForClass(getConverterConfiguration().getDtoClass());
		PropertyDataTypeConverter dataTypeConverter = new PropertyDataTypeConverter();

		// Let's try to find the dao config for that column and format it from the upload converter in migrations (if not already overridden) or based on DataType
		Column migrationColumn = getMigrationColumnForProperty(getConverterConfiguration().getDtoClass(), beanPropertyName, dao);
		if (migrationColumn != null) {
			// If default set and not explicitly overridden
			if (!StringUtils.isEmpty(migrationColumn.getUploadValueConverterName())) {
				setColumnValueConverterOverrideName(migrationColumn.getUploadValueConverterName());
			}
			return migrationColumn.getDataType();
		}
		// Otherwise try to determine the dataType from the bean property
		else {
			PropertyDescriptor pd = getPropertyDescriptor(beanPropertyName);
			if (pd != null) {
				return dataTypeConverter.convert(pd);
			}
		}
		return null;
	}


	protected Integer getSqlType(String beanPropertyName) {
		if (beanPropertyName.startsWith(CUSTOM_JSON_STRING_PREFIX)) {
			return null;
		}
		ClassToSqlTypeConverter sqlTypeConverter = new ClassToSqlTypeConverter();
		PropertyDescriptor pd = getPropertyDescriptor(beanPropertyName);
		if (pd != null) {
			if (Enum.class.isAssignableFrom(pd.getPropertyType())) {
				return Types.VARCHAR;
			}
			else {
				return sqlTypeConverter.convert(pd.getPropertyType());
			}
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	protected BeanDataColumn createBeanDataColumn(ApplicationContextService applicationContextService, String field) {
		String[] attr = field.split(":");
		if (attr.length < 2 || attr.length > 4) {
			throw new ValidationException("Error parsing column string text [" + field
					+ "].  There must be a bean property name and a column name for each column and an optional style and value converter.");
		}
		String columnHeader = attr[0];
		String beanPropertyName = attr[1];
		String style = (attr.length >= 3 ? attr[2] : null);
		String columnValueConverterName = (attr.length == 4 ? attr[3] : null);
		//Clear the override name from prior field
		setColumnValueConverterOverrideName(null);
		//getDataType first - this may set an override name for the converter?
		DataTypes dataType = getDataType(beanPropertyName);
		ReversableConverter<Object, Object> columnConverter = getColumnValueConverter(columnValueConverterName);
		return new BeanDataColumn(beanPropertyName, columnHeader, style, columnConverter, dataType, getSqlType(beanPropertyName));
	}


	protected PropertyDescriptor getPropertyDescriptor(String beanPropertyName) {
		int lastDot = beanPropertyName.lastIndexOf('.');
		if (lastDot > 0) {
			String nestedPropertyName = beanPropertyName.substring(0, lastDot);
			String propertyName = beanPropertyName.substring(lastDot + 1);
			Class<?> clz = BeanUtils.getPropertyType(getConverterConfiguration().getDtoClass(), nestedPropertyName);
			return BeanUtils.getPropertyDescriptor(clz, propertyName);
		}
		else {
			return getPropertyDescriptorMap().get(beanPropertyName);
		}
	}


	@SuppressWarnings("unchecked")
	protected ReversableConverter<Object, Object> getColumnValueConverter(String columnValueConverterName) {
		// Explicit Override for the Column Value Converter
		if (!StringUtils.isEmpty(columnValueConverterName)) {
			return (ReversableConverter<Object, Object>) getConverterConfiguration().getApplicationContextService().getContextBean(columnValueConverterName);
		}
		if (getColumnValueConverterOverrideName() != null) {
			return (ReversableConverter<Object, Object>) getConverterConfiguration().getApplicationContextService().getContextBean(getColumnValueConverterOverrideName());
		}
		return null;
	}


	protected BeanDataColumn[] getDataColumnArrayForBeanCollection(Collection<F> beanCollection) {
		return (BeanDataColumn[]) this.dataColumnArray;
	}


	@SuppressWarnings("unchecked")
	private ReadOnlyDAO<IdentityObject> getDAOForClass(Class<?> dtoClass) {
		ReadOnlyDAO<IdentityObject> dao = null;
		if (dtoClass != null && IdentityObject.class.isAssignableFrom(dtoClass)) {
			try {
				if (getDaoLocator() == null) {
					setDaoLocator((DaoLocator) getConverterConfiguration().getApplicationContextService().getContextBean("daoLocator"));
				}
				dao = getDaoLocator().locate((Class<IdentityObject>) dtoClass);
			}
			catch (Exception e) {
				// Don't throw exception if it can't find the dao for the class - just used to for formatting, not necessarily needs to be actual beans with DAOs
			}
		}
		return dao;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Column getMigrationColumnForProperty(Class<?> dtoClass, String beanPropertyName, ReadOnlyDAO<IdentityObject> dao) {
		if (dao == null || dtoClass == null) {
			return null;
		}
		if (beanPropertyName.startsWith(CUSTOM_JSON_STRING_PREFIX)) {
			return null;
		}

		int dotIndex = beanPropertyName.indexOf('.');
		if (dotIndex != -1) {
			String nestedPropertyName = beanPropertyName.substring(0, dotIndex);
			PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(dtoClass, nestedPropertyName);
			return getMigrationColumnForProperty(descriptor.getPropertyType(), beanPropertyName.substring(dotIndex + 1), getDAOForClass(descriptor.getPropertyType()));
		}

		if (dao.getConfiguration() instanceof NoTableDAOConfig<?>) {
			return null;
		}

		for (Column c : dao.getConfiguration().getColumnList()) {
			if (c.getBeanPropertyName().equals(beanPropertyName)) {
				return c;
			}
		}

		return null;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public Map<String, PropertyDescriptor> getPropertyDescriptorMap() {
		return this.propertyDescriptorMap;
	}


	protected void setPropertyDescriptorMap(Class<F> dtoClass) {
		this.propertyDescriptorMap = BeanUtils.getPropertyDataTypes(dtoClass);
	}


	public void setPropertyDescriptorMap(Map<String, PropertyDescriptor> propertyDescriptorMap) {
		this.propertyDescriptorMap = propertyDescriptorMap;
	}


	public String getColumnValueConverterOverrideName() {
		return this.columnValueConverterOverrideName;
	}


	public void setColumnValueConverterOverrideName(String columnValueConverterOverrideName) {
		this.columnValueConverterOverrideName = columnValueConverterOverrideName;
	}
}
