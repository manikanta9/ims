package com.clifton.core.dataaccess.migrate.execution.action;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.Map;


/**
 * @author stevenf
 */
public interface MigrationExecutionActionHandler {

	public Map<String, Object> executeActions(List<Action> actionList, Map<String, Action> actionMap, ApplicationContext springContext);


	/**
	 * This method is used when the parent object will actually save the child, or in cases in which the
	 * child references the parent being migrated and so can not be saved until the parent is fully populated.
	 * e.g. tradeFill references it's parent trade, and is saved by the saveTrade method.
	 * So we simply want to skip it using the migration params on the DTO definition.
	 * <table name="TradeFill" dtoClass="com.clifton.trade.TradeFill" uploadAllowed="true"
	 * migrationServiceBeanName="migrationExecutionActionHandler" migrationServiceMethodName="doNotSaveObject">
	 **/
	public IdentityObject doNotSaveObject(IdentityObject identityObject);
}
