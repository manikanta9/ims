package com.clifton.core.dataaccess.search.sql;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>SqlSearchFormConfigurer</code> class is a generic {@link SearchConfigurer} that can configure
 * {@link SqlSelectCommand} objects from {@link BaseEntitySearchForm} objects.
 *
 * @author vgomelsky
 */
public class SqlSearchFormConfigurer implements SearchConfigurer<SqlSelectCommand> {

	private final BaseEntitySearchForm searchForm;
	/**
	 * A static {@link Map} that holds search restriction definitions allowed for this {@link SearchConfigurer}.
	 */
	private final Map<String, SearchRestrictionDefinition> restrictionDefinitionMap = new ConcurrentHashMap<>();
	private String selectClause;
	private String fromClause;
	private String whereClause;
	private String groupByClause;
	private String havingClause;
	private String orderByClause = "1 ASC"; // default sorting


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructs a new {@link SearchConfigurer} based on the specified {@link BaseEntitySearchForm}
	 * and runs validation.
	 *
	 * @param searchForm
	 */
	public SqlSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		this.searchForm = searchForm;
	}


	@Override
	public boolean isReadUncommittedRequested() {
		return false;
	}


	@Override
	public void configureCriteria(SqlSelectCommand criteria) {
		validateRestrictions();

		criteria.setStart(getStart());
		criteria.setLimit(getLimit());

		criteria.setWhereClause(getWhereClause());
		configureWhereClause(criteria);
		criteria.setSelectClause(getSelectClause());
		criteria.setFromClause(getFromClause());
		criteria.setGroupByClause(getGroupByClause());
		criteria.setHavingClause(getHavingClause());
	}


	/**
	 * Defaults to ascending search by the first column. Override if different search is needed.
	 */
	@Override
	public boolean configureOrderBy(SqlSelectCommand criteria) {
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(this.searchForm.getOrderBy());
		if (CollectionUtils.isEmpty(orderByList)) {
			criteria.setOrderByClause(getOrderByClause());
			return false;
		}

		StringBuilder orderBy = new StringBuilder();
		for (OrderByField field : orderByList) {
			// validate the field to make sure it's valid and sorting is allowed
			SearchRestrictionDefinition definition = this.restrictionDefinitionMap.get(field.getName());
			if (definition == null) {
				throw new FieldValidationException("Sorting is not enabled for field: " + field.getName(), field.getName());
			}
			if (orderBy.length() > 0) {
				orderBy.append(", ");
			}
			orderBy.append(definition.getOrderByFieldName());
			orderBy.append(' ');
			orderBy.append(field.getDirection());
		}
		criteria.setOrderByClause(orderBy.toString());
		return true;
	}


	@Override
	public int getLimit() {
		return this.searchForm.getLimit();
	}


	@Override
	public Integer getMaxLimit() {
		return this.searchForm.getMaxLimit();
	}


	@Override
	public int getStart() {
		return this.searchForm.getStart();
	}


	public void addSearchFieldDefinition(SearchRestrictionDefinition definition) {
		this.restrictionDefinitionMap.put(definition.getFieldName(), definition);
	}


	protected void validateRestrictions() {
		SearchUtils.validateRestrictionList(this.searchForm.getRestrictionList(), this.restrictionDefinitionMap, this.searchForm);
	}


	protected void configureWhereClause(SqlSelectCommand criteria) {
		if (CollectionUtils.getSize(this.searchForm.getRestrictionList()) == 0) {
			return;
		}
		StringBuilder where = new StringBuilder(StringUtils.isEmpty(criteria.getWhereClause()) ? "" : criteria.getWhereClause());
		for (SearchRestriction restriction : CollectionUtils.getIterable(this.searchForm.getRestrictionList())) {
			SearchRestrictionDefinition definition = this.restrictionDefinitionMap.get(restriction.getField());
			Object value = definition.getValue(restriction.getValue());
			if (ComparisonConditions.IN == restriction.getComparison() && CollectionUtils.isEmptyCollectionOrArray(value)) {
				continue;
			}
			if (where.length() > 0) {
				where.append(" AND ");
			}
			configureSearchRestriction(restriction, definition.getSearchFieldName(), value, where, criteria);
		}
		criteria.setWhereClause(where.toString());
	}


	private void configureSearchRestriction(SearchRestriction restriction, String searchFieldName, Object value, StringBuilder where, SqlSelectCommand criteria) {
		if (restriction.getComparison().isOrNull()) {
			where.append("(");
		}
		where.append(searchFieldName);

		boolean customParameters = false;
		switch (restriction.getComparison()) {
			case EQUALS_OR_IS_NULL:
			case EQUALS:
				if (value instanceof Date) {
					// use BETWEEN start and end of day to match any time on that date
					customParameters = true;
					// BETWEEN ? AND ?
					where.append(" BETWEEN ").append(getParameterSyntax(value, criteria)).append(" AND ").append(getParameterSyntax(DateUtils.getEndOfDay((Date) value), criteria));
				}
				break;
			case NOT_EQUALS_OR_IS_NULL:
			case NOT_EQUALS:
				if (value instanceof Date) {
					// exclude all times for this date
					customParameters = true;
					where.append(" < ").append(getParameterSyntax(value, criteria)).append(" AND ");
					where.append(searchFieldName);
					where.append(" > ").append(getParameterSyntax(DateUtils.getEndOfDay((Date) value), criteria));
				}
				break;
			case IN_OR_IS_NULL:
			case IN:
			case NOT_IN:
				customParameters = true;
				appendInClause(restriction.getComparison(), value, where, criteria);
				break;
			case GREATER_THAN_OR_IS_NULL:
			case GREATER_THAN:
				if (value instanceof Date) {
					// after end of this day
					value = DateUtils.getEndOfDay((Date) value);
				}
				break;
			case GREATER_THAN_OR_EQUALS:
			case GREATER_THAN_OR_EQUALS_OR_IS_NULL:
				if (value instanceof Date) {
					// greater or equal beginning of this day
					value = DateUtils.clearTime((Date) value);
				}
				break;
			case LESS_THAN_OR_IS_NULL:
			case LESS_THAN:
				if (value instanceof Date) {
					// before beginning of this day
					value = DateUtils.clearTime((Date) value);
				}
				break;
			case LESS_THAN_OR_EQUALS:
			case LESS_THAN_OR_EQUALS_OR_IS_NULL:
				if (value instanceof Date) {
					// before or on end of this day
					value = DateUtils.getEndOfDay((Date) value);
				}
				break;
			case LIKE:
				customParameters = true;
				where.append(" LIKE '%' + ").append(getParameterSyntax(value, criteria)).append(" + '%'");
				break;
			case NOT_LIKE:
				customParameters = true;
				where.append(" NOT LIKE '%' + ").append(getParameterSyntax(value, criteria)).append(" + '%'");
				break;
			case BEGINS_WITH:
				customParameters = true;
				where.append(" LIKE ").append(getParameterSyntax(value, criteria)).append(" + '%'");
				break;
			case NOT_BEGINS_WITH:
				customParameters = true;
				where.append(" NOT LIKE ").append(getParameterSyntax(value, criteria)).append(" + '%'");
				break;
			case ENDS_WITH:
				customParameters = true;
				where.append(" LIKE '%' + ").append(getParameterSyntax(value, criteria));
				break;
			case NOT_ENDS_WITH:
				customParameters = true;
				where.append(" NOT LIKE '%' + ").append(getParameterSyntax(value, criteria));
				break;
			case IS_NULL:
				customParameters = true;
				if (value instanceof Boolean) {
					if (((Boolean) value)) {
						where.append(" IS NULL");
					}
					else {
						where.append(" IS NOT NULL");
					}
				}
				else {
					where.append(" IS NULL");
				}
				break;
			case IS_NOT_NULL:
				customParameters = true;
				if (value instanceof Boolean) {
					if (((Boolean) value)) {
						where.append(" IS NOT NULL");
					}
					else {
						where.append(" IS NULL");
					}
				}
				else {
					where.append(" IS NOT NULL");
				}
				break;
		}

		if (!customParameters) {
			where.append(" ").append(restriction.getComparison().getComparisonExpression()).append(" ").append(getParameterSyntax(value, criteria));
		}

		if (restriction.getComparison().isOrNull()) {
			where.append(" OR ").append(searchFieldName).append(" IS NULL");
			where.append(")");
		}
	}


	/**
	 * default implementation - appends ? and adds value to criteria parameters
	 */
	protected String getParameterSyntax(Object value, SqlSelectCommand criteria) {
		// SqlParameterValue types are only necessary for null values - at this time
		// through search form configuration that shouldn't happen because we use IS NULL or IS NOT NULL never EQUALS NULL
		criteria.addSqlParameterValue(SqlParameterValue.ofUnknownType(value));
		return "?";
	}


	public String getSelectClause() {
		return this.selectClause;
	}


	public void setSelectClause(String selectClause) {
		this.selectClause = selectClause;
	}


	public String getFromClause() {
		return this.fromClause;
	}


	public void setFromClause(String fromClause) {
		this.fromClause = fromClause;
	}


	public String getOrderByClause() {
		return this.orderByClause;
	}


	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}


	public String getWhereClause() {
		return this.whereClause;
	}


	public void setWhereClause(String whereClause) {
		this.whereClause = whereClause;
	}


	public String getGroupByClause() {
		return this.groupByClause;
	}


	public void setGroupByClause(String groupByClause) {
		this.groupByClause = groupByClause;
	}


	public String getHavingClause() {
		return this.havingClause;
	}


	public void setHavingClause(String havingClause) {
		this.havingClause = havingClause;
	}


	private void appendInClause(ComparisonConditions condition, Object value, StringBuilder where, SqlSelectCommand criteria) {
		if (value != null) {
			List<?> values = CollectionUtils.getAsList(value);
			int valueCount = values.size();
			if (valueCount > 0) {
				where.append(String.format(" %s (", condition.getComparisonExpression()));
				for (int i = 0; i < valueCount; i++) {
					if (i > 0) {
						where.append(',');
					}
					where.append(getParameterSyntax(values.get(i), criteria));
				}
				where.append(')');
			}
		}
	}
}
