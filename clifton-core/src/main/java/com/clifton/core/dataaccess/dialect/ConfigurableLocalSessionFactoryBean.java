package com.clifton.core.dataaccess.dialect;

import com.clifton.core.logging.LogUtils;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>CustomLocalSessionFactoryBean</code> is used to create inject configuration at runtime. Currently, it configures the appropriate sql functions for  each dialect,
 * that are used inside our {@link org.hibernate.criterion.Criterion} and {@link org.hibernate.criterion.Order} implementations.
 * implementations.
 *
 * @author dillonm
 */

public class ConfigurableLocalSessionFactoryBean extends LocalSessionFactoryBean {

	@Autowired(required = false)
	private List<SessionFactoryBuilderConfigurer> sessionFactoryBuilderConfigurers = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected SessionFactory buildSessionFactory(LocalSessionFactoryBuilder sfb) {
		for (SessionFactoryBuilderConfigurer sessionFactoryBuilderConfigurer : getSqlFunctionConfigurers()) {
			if (sessionFactoryBuilderConfigurer.supportsDialect(sfb.getProperty("hibernate.dialect"))) {
				LogUtils.info(getClass(), String.format("configuring LocalSessionFactoryBuilder with %s", sessionFactoryBuilderConfigurer.getClass().getName()));
				sessionFactoryBuilderConfigurer.configure(sfb);
			}
		}
		return super.buildSessionFactory(sfb);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<SessionFactoryBuilderConfigurer> getSqlFunctionConfigurers() {
		return this.sessionFactoryBuilderConfigurers;
	}


	public void setSqlFunctionConfigurers(List<SessionFactoryBuilderConfigurer> sessionFactoryBuilderConfigurers) {
		this.sessionFactoryBuilderConfigurers = sessionFactoryBuilderConfigurers;
	}
}
