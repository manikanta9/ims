package com.clifton.core.dataaccess.search;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.DoubleType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import java.math.BigDecimal;
import java.util.Date;


/**
 * SubselectParameterExpression adds the parameter to the sql parameters without adding a SQL to the where clause
 */
public class SubselectParameterExpression implements Criterion {

	private final Object value;
	private final Type valueType;


	public SubselectParameterExpression(Type valueType, Object value) {
		this.valueType = valueType;
		this.value = value;
	}


	public SubselectParameterExpression(Object value) {
		Type vType;

		if (value instanceof Date) {
			vType = new DateType();
		}
		// TODO MAY NEED TO ADJUST THESE AS NEW CASES COME UP, JUST MAPPED WHAT I THINK IS CORRECT
		// DATE IS CURRENTLY THE ONLY ONE USED (SEE INVESTMENT INSTRUMENT SERVICE - INVESTMENT INSTRUMENT POSITION LIMIT EXTENDED LIST)
		else if (value instanceof Boolean) {
			vType = new BooleanType();
		}
		else if (value instanceof Integer) {
			vType = new IntegerType();
		}
		else if (value instanceof BigDecimal) {
			vType = new DoubleType();
		}
		else {
			vType = new StringType();
		}
		this.valueType = vType;
		this.value = value;
	}


	@Override
	@SuppressWarnings("unused")
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		// No additional SQL to append, but since criteria automatically adds an "and" to the where clause, we need to send something back
		return "1 = 1";
	}


	@Override
	public TypedValue[] getTypedValues(@SuppressWarnings("unused") Criteria criteria, @SuppressWarnings("unused") CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[]{new TypedValue(this.valueType, this.value)};
	}


	@Override
	public String toString() {
		return "Subselect Parameter Value: " + this.value;
	}
}
