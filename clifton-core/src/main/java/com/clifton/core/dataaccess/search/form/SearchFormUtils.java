package com.clifton.core.dataaccess.search.form;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.dataaccess.search.restrictions.DateRestrictionDefinition;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>SearchFormUtils</code> class contains common search form methods useful for applying search form filters during application-side filtering.
 * <p>
 * Created by Mary Anderson
 */
public class SearchFormUtils {

	private static final Pattern SQL_FORMULA_ELEMENT_PATTERN = Pattern.compile("\\$([0-9]+)");


	/**
	 * Convert the restriction list items to actual properties on the search form so can be easily retrieved for filtering.
	 * If the search form already has a value set (non-null), the restriction will remain in the restriction list unless the values are equal.
	 */
	public static void convertSearchRestrictionList(BaseEntitySearchForm searchForm) {
		Iterator<SearchRestriction> restrictionIterator = CollectionUtils.getIterable(searchForm.getRestrictionList()).iterator();
		while (restrictionIterator.hasNext()) {
			SearchRestriction restriction = restrictionIterator.next();
			Object propertyValue = BeanUtils.getPropertyValue(searchForm, restriction.getField());
			if (propertyValue == null) {
				BeanUtils.setPropertyValue(searchForm, restriction.getField(), restriction.getValue());
				restrictionIterator.remove();
			}
			else if (CompareUtils.isEqual(propertyValue, restriction.getValue())) {
				restrictionIterator.remove();
			}
		}
	}


	/**
	 * Removes the restrictions from the provided search form's restriction list where a value exists for the property
	 * of the search form to prevent duplicate restrictions from being set.
	 */
	public static void removeDuplicatesFromSearchRestrictionList(BaseEntitySearchForm searchForm) {
		Iterator<SearchRestriction> restrictionIterator = CollectionUtils.getIterable(searchForm.getRestrictionList()).iterator();
		while (restrictionIterator.hasNext()) {
			SearchRestriction restriction = restrictionIterator.next();
			Object propertyValue = BeanUtils.getPropertyValue(searchForm, restriction.getField());
			if (propertyValue != null) {
				restrictionIterator.remove();
			}
		}
	}


	/**
	 * Takes a string value and a string search value and returns true if filtering should include it
	 */
	public static boolean applyStringFilter(String value, String searchValue) {
		// If searchValue is blank - no filter
		if (StringUtils.isEmpty(searchValue)) {
			return true;
		}
		if (StringUtils.isEmpty(value)) {
			return false;
		}
		return value.toLowerCase().contains(searchValue.toLowerCase());
	}


	/**
	 * Takes a boolean value and a boolean search value and returns true if filtering should include it
	 */
	public static boolean applyBooleanFilter(Boolean value, Boolean searchValue) {
		// If searchValue is null - no filter
		if (searchValue == null) {
			return true;
		}
		else if (value == null) {
			return false;
		}
		return searchValue.equals(value);
	}


	/**
	 * Takes a boolean value and a boolean search value and returns true if filtering should include it
	 */
	public static boolean applyBooleanFilterOrNull(Boolean value, Boolean searchValue) {
		// If searchValue is null - no filter
		if (searchValue == null) {
			return true;
		}
		// Null is accepted
		else if (value == null) {
			return true;
		}
		return searchValue.equals(value);
	}


	/**
	 * Applies sorting on the given list
	 */
	public static <M> List<M> applySortOrder(List<M> beanList, BaseSortableSearchForm searchForm) {
		if (CollectionUtils.getSize(beanList) > 1) {
			List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(searchForm.getOrderBy());
			if (!CollectionUtils.isEmpty(orderByList)) {
				List<String> sortFields = new ArrayList<>();
				List<Boolean> sortOrder = new ArrayList<>();
				for (OrderByField field : orderByList) {
					sortFields.add(field.getName());
					sortOrder.add(OrderByDirections.ASC == field.getDirection());
				}
				return BeanUtils.sortWithPropertyNames(beanList, sortFields, sortOrder);
			}
		}
		return beanList;
	}


	/**
	 * For a specific Date field, if restriction exists for EQUALS comparison, replaces that restriction with GREATER_THAN_OR_EQUAL that date, and LESS_THAN the
	 * next day This is useful for date fields that include time component, but we want to search for any time during that day If the EQUALS restriction doesn't
	 * exist, this method won't do anything.
	 */
	public static void convertDateEqualsToDateRangeComparison(BaseEntitySearchForm searchForm, final Map<String, SearchRestrictionDefinition> searchRestrictionDefinitionMap) {
		if (searchForm != null) {
			List<SearchRestriction> searchRestrictionList = new ArrayList<>();
			for (SearchRestriction searchRestriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
				String fieldName = searchRestriction.getField();
				SearchRestrictionDefinition searchRestrictionDefinition = searchRestrictionDefinitionMap.get(fieldName);
				if (searchRestrictionDefinition instanceof DateRestrictionDefinition) {
					if (((DateRestrictionDefinition) searchRestrictionDefinition).isDateFieldIncludesTime() && searchRestriction.getComparison() == ComparisonConditions.EQUALS) {
						Date filterDate;
						if (searchRestriction.getValue() instanceof Date) {
							filterDate = (Date) searchRestriction.getValue();
						}
						else if (searchRestriction.getValue() instanceof String) {
							filterDate = DateUtils.toDate((String) searchRestriction.getValue());
						}
						else {
							throw new RuntimeException(String.format("An invalid date form value was found: [%s]. Unable to parse values of type [%s].", searchRestriction.getValue(), searchRestriction.getValue().getClass().getName()));
						}
						searchRestriction.setComparison(ComparisonConditions.GREATER_THAN_OR_EQUALS);
						searchRestrictionList.add(new SearchRestriction(fieldName, ComparisonConditions.LESS_THAN, DateUtils.addDays(filterDate, 1)));
					}
				}
			}
			searchRestrictionList.forEach(searchForm::addSearchRestriction);
		}
	}


	/**
	 * Validates that the range of dates specified in the {@link SearchRestriction} list for a given date field does not exceed the specified number of days.
	 * An optional validation message may be specified to override the default to provide a more user friendly error message.
	 */
	public static void validateDateRangeLength(BaseEntitySearchForm searchForm, String dateFieldName, int maxNumberOfDays, String validationMessage) {
		List<SearchRestriction> dateRestrictions = searchForm.getSearchRestrictions(dateFieldName);
		ValidationUtils.assertNotEmpty(dateRestrictions, !StringUtils.isEmpty(validationMessage) ? validationMessage : "A date range not exceeding " + maxNumberOfDays + " must be selected.");
		if (!CollectionUtils.isEmpty(dateRestrictions)) {
			Date startDate = new Date();
			Date endDate = new Date();
			for (SearchRestriction dateRestriction : CollectionUtils.getIterable(dateRestrictions)) {
				if (dateRestriction.getValue() instanceof String) {
					Date date = DateUtils.toDate((String) dateRestriction.getValue());
					if (dateRestriction.getComparison() == ComparisonConditions.GREATER_THAN) {
						startDate = date;
					}
					else if (dateRestriction.getComparison() == ComparisonConditions.LESS_THAN) {
						endDate = date;
					}
				}
			}
			ValidationUtils.assertTrue(DateUtils.getDaysDifference(endDate, startDate) <= maxNumberOfDays, !StringUtils.isEmpty(validationMessage) ? validationMessage : "A maximum date range of " + maxNumberOfDays + " days may be selected.");
		}
	}


	public static void configureCriteriaForDateRangeSearchForm(DateRangeAwareSearchForm searchForm, Criteria criteria) {
		convertDateRangeSearchFormRestrictionToFields(searchForm);
		if (BooleanUtils.isTrue(searchForm.getInactive())) {
			criteria.add(ActiveExpressionForDates.forActive(false));
		}

		// apply custom filters, if set
		if (searchForm.getActiveOnDate() != null) {
			criteria.add(ActiveExpressionForDates.forActiveOnDate(true, searchForm.getActiveOnDate()));
		}
		if (searchForm.getNotActiveOnDate() != null) {
			criteria.add(ActiveExpressionForDates.forActiveOnDate(false, searchForm.getNotActiveOnDate()));
		}
		if (searchForm.getActiveOnDateRangeStartDate() != null) {
			ValidationUtils.assertNotNull(searchForm.getActiveOnDateRangeEndDate(), "To use active on date range, both start and end date are required.");
			criteria.add(ActiveExpressionForDates.forActiveOnDateRange(true, searchForm.getActiveOnDateRangeStartDate(), searchForm.getActiveOnDateRangeEndDate()));
		}
		if (searchForm.getNotActiveOnDateRangeStartDate() != null) {
			ValidationUtils.assertNotNull(searchForm.getNotActiveOnDateRangeEndDate(), "To use not active on date range, both start and end date are required.");
			criteria.add(ActiveExpressionForDates.forActiveOnDateRange(false, searchForm.getNotActiveOnDateRangeStartDate(), searchForm.getNotActiveOnDateRangeEndDate()));
		}
		if (searchForm.getEndDateRangeStartDate() != null) {
			ValidationUtils.assertNotNull(searchForm.getEndDateRangeEndDate(), "To use end date range, both start and end date are required.");
			criteria.add(Restrictions.and(Restrictions.ge("endDate", searchForm.getEndDateRangeStartDate()), Restrictions.le("endDate", searchForm.getEndDateRangeEndDate())));
		}
	}


	public static void convertDateRangeSearchFormRestrictionToFields(DateRangeAwareSearchForm searchForm) {
		// Active and Inactive are custom search restrictions that set other fields, if set as a search restriction
		// remove it because there is no definition attached to it.
		if (searchForm.containsSearchRestriction("active") || searchForm.getActive() != null) {
			Boolean isActive = searchForm.getActive();
			// If not set explicitly on the form, it's in the restriction list
			if (isActive == null) {
				SearchRestriction restrict = searchForm.getSearchRestriction("active");
				isActive = Boolean.valueOf((String) restrict.getValue());
			}
			Date today = new Date();
			if (!searchForm.isIncludeTime()) {
				today = DateUtils.clearTime(today);
			}
			if (isActive) {
				// If active, set active on date to today
				searchForm.setActiveOnDate(today);
			}
			else {
				// If not active, set not active date
				searchForm.setNotActiveOnDate(today);
			}
			searchForm.removeSearchRestriction("active");
		}
		if (searchForm.containsSearchRestriction("inactive") || searchForm.getInactive() != null) {
			Boolean isInactive = searchForm.getInactive();
			// If not set explicitly on the form, it's in the restriction list
			if (isInactive == null) {
				SearchRestriction restrict = searchForm.getSearchRestriction("inactive");
				isInactive = Boolean.valueOf((String) restrict.getValue());
			}
			Date today = new Date();
			if (!searchForm.isIncludeTime()) {
				today = DateUtils.clearTime(today);
			}
			if (isInactive) {
				// If not active, set not active on date to today
				searchForm.setNotActiveOnDate(today);
			}
			else {
				// If active, set active date
				searchForm.setActiveOnDate(today);
			}
			searchForm.removeSearchRestriction("inactive");
		}
	}


	/**
	 * converts $[0-9]+ into {@link String#format(String, Object...)} compatible. Ex: "$1 $2 $1" -> "%1$s %2$s %1$s"
	 */
	public static String convertSqlFormulaToStringFormatCompatible(String sqlFormula) {
		return SQL_FORMULA_ELEMENT_PATTERN.matcher(sqlFormula).replaceAll("%$1\\$s");
	}


	/**
	 * gets the maximum number of parameters allowed in a sql formula, where parameters follow $[0-9]+
	 */
	public static int getSqlFormulaMaximumNumberOfParameters(String sqlFormula) {
		Matcher matcher = SQL_FORMULA_ELEMENT_PATTERN.matcher(sqlFormula);
		int highestValue = 0;
		while (matcher.find()) {
			int value = Integer.parseInt(matcher.group(1));
			if (highestValue < value) {
				highestValue = value;
			}
		}
		return highestValue;
	}
}
