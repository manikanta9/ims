package com.clifton.core.dataaccess.migrate.locator;


import com.clifton.core.dataaccess.migrate.MigrationUtils;
import com.clifton.core.util.AssertUtils;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>ScanMigrationFileLocator</code> class provides a MigrationFileLocator implementation
 * that expects all modules and searches for all migration files based upon our
 * naming conventions
 *
 * @author manderson
 */
public class ScanMigrationFileLocator extends SimpleMigrationFileLocator {

	@Override
	public List<MigrationModule> locateMigrations(MigrationFileLocatorVersionValidation versionValidation) {

		for (MigrationModule module : getMigrationModuleList()) {
			List<Resource> moduleFiles = new ArrayList<>();
			if (module.getTargetedVersion() != 0 && module.getTargetedVersion() < module.getCurrentVersion()) {
				throw new RuntimeException("Current Version is higher than targeted version. Rollbacks are not supported at this time.");
			}

			try {
				Resource[] migrationResources = MigrationUtils.getMigrationResources(module.getModuleName());
				int previousVersion = 0;

				for (Resource resource : migrationResources) {
					if (resource.exists()) {
						AssertUtils.assertNotNull(resource.getFilename(), "Filename was null for resource " + resource);
						int thisVersion = MigrationUtils.getVersionForFileName(resource.getFilename());
						if ((module.getTargetedVersion() != 0 && thisVersion > module.getTargetedVersion())) {
							break;
						}
						if (!versionValidation.isValidVersion(thisVersion, previousVersion)) {
							throw new RuntimeException(versionValidation.getInvalidMessage(thisVersion, previousVersion) + " File name: "
									+ resource.getFilename() + "; Module name: " + module.getModuleName());
						}
						moduleFiles.add(resource);
						previousVersion = thisVersion;
					}
				}
				module.setMigrationResourceList(moduleFiles);
			}
			catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		return getMigrationModuleList();
	}
}
