package com.clifton.core.dataaccess.file;

import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;

import java.io.File;
import java.io.Serializable;
import java.util.Objects;


/**
 * This class is designed to be a lightweight replacement for File/FileContainer.  It was made to serialize nicely and be platform independent.
 *
 * @author mitchellf
 */
public class FilePath implements Serializable, Comparable<FilePath> {

	private final boolean remote;

	private String path;


	protected FilePath(String path) {
		this.path = path;
		this.remote = isRemotePath(path);
	}


	public static FilePath forPath(String path) {
		return new FilePath(path);
	}


	public static FilePath forFile(File file) {
		return new FilePath(file.getPath());
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static boolean isRemotePath(String filePath) {
		if (StringUtils.isEmpty(filePath)) {
			return false;
		}
		return filePath.matches(FileUtils.REMOTE_FILE_REGEX);
	}


	/**
	 * Convenience method to make usage of {@link FileContainer} objects easier - don't need to call getFileContainer every time this class is used.
	 */
	public FileContainer toFileContainer() {
		return FileContainerFactory.getFileContainer(this.path);
	}


	/**
	 * Convenience method to avoid needing to call 'new File()' every time a 3rd party method requires a {@link File} object as an argument
	 */
	public File toFile() {
		return new File(this.path);
	}


	public int compareTo(FilePath object) {
		return this.getPath().compareTo(object.getPath());
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		else if (!(obj instanceof FilePath)) {
			return false;
		}
		FilePath toCompare = (FilePath) obj;
		return CompareUtils.isEqual(getPath(), toCompare.getPath());
	}


	@Override
	public int hashCode() {
		return Objects.hash(getPath());
	}


	@Override
	public String toString() {
		return this.getPath();
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////        Getter and setter methods               ////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isRemote() {
		return this.remote;
	}


	public String getPath() {
		return this.path;
	}


	public void setPath(String path) {
		this.path = path;
	}
}
