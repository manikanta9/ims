package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.validation.ValidationException;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BeanDataTable</code> ...
 *
 * @author manderson
 */
public class BeanDataTable<T> implements DataTable {

	private final BeanDataColumn[] columnList;
	private final List<DataRow> rowList;
	private Class<T> beanClass;
	private boolean variableColumnCountPerRow = false;


	/**
	 * Used for Jackson deserialization;
	 */
	@JsonCreator
	public BeanDataTable(@JsonProperty("columnList") BeanDataColumn[] columnList, @JsonProperty("rowList") List<DataRow> rowList) {
		this.columnList = columnList;
		this.rowList = rowList;
	}


	public BeanDataTable(BeanDataColumn[] columnList) {
		this.columnList = columnList;
		this.rowList = new ArrayList<>();
	}


	@Override
	public void addRow(DataRow row) {
		this.rowList.add(row);
	}


	@Override
	public int getColumnIndex(String columnName) {
		for (int i = 0; i < this.columnList.length; i++) {
			BeanDataColumn dataColumn = this.columnList[i];
			if (dataColumn.getColumnName().equals(columnName)) {
				return i;
			}
		}
		return -1;
	}


	@Override
	public int getColumnCount() {
		return this.columnList.length;
	}


	@Override
	public DataColumn[] getColumnList() {
		return this.columnList;
	}


	@Override
	public List<DataRow> getRowList() {
		return this.rowList;
	}


	@Override
	public DataRow getRow(int rowIndex) {
		if (rowIndex < 0 || this.rowList.size() <= rowIndex) {
			throw new ValidationException("Row index [" + rowIndex + "] is outside of the row range 0-" + this.rowList.size());
		}
		return this.rowList.get(rowIndex);
	}


	@Override
	public int getTotalRowCount() {
		return this.rowList.size();
	}


	public Class<T> getBeanClass() {
		return this.beanClass;
	}


	public void setBeanClass(Class<T> beanClass) {
		this.beanClass = beanClass;
	}


	@Override
	public boolean isVariableColumnCountPerRow() {
		return this.variableColumnCountPerRow;
	}


	@Override
	public void setVariableColumnCountPerRow(boolean simpleExport) {
		this.variableColumnCountPerRow = simpleExport;
	}
}
