package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyFlexibleCache;


/**
 * The extension of {@link SelfRegisteringFlexibleDaoCache} for single-key caches.
 *
 * @param <T> the type of object which shall have its references cached
 * @param <U> the type for the <i>order</i> property
 * @param <K> the type of the key on which entities shall be cached
 */
public abstract class SelfRegisteringSingleKeyFlexibleDaoCache<T extends IdentityObject, U extends Comparable<U>, K> extends SelfRegisteringFlexibleDaoCache<T, U> implements DaoSingleKeyFlexibleCache<T, U, K> {

	/**
	 * Returns the single key property path on which entities shall be cached.
	 */
	protected abstract String getBeanKeyProperty();


	/**
	 * Returns the single key value for the given bean.
	 */
	protected abstract K getBeanKeyValue(T bean);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final T getBeanForKeyValue(ReadOnlyDAO<T> dao, U order, K keyValue) {
		return getBeanImpl(dao, false, order, keyValue);
	}


	@Override
	public final T getBeanForKeyValueStrict(ReadOnlyDAO<T> dao, U order, K keyValue) {
		return getBeanImpl(dao, true, order, keyValue);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected final String[] getBeanKeyProperties() {
		return new String[]{getBeanKeyProperty()};
	}


	@Override
	protected final String getBeanKey(T bean) {
		return getBeanKeyForProperties(getBeanKeyValue(bean));
	}
}
