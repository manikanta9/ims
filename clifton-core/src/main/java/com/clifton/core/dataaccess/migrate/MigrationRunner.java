package com.clifton.core.dataaccess.migrate;


import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;


/**
 * The <code>MigrationRunner</code> class executes database migrations (usually is called from a build script).
 *
 * @author vgomelsky
 */
public class MigrationRunner {

	/**
	 * Executes command specified by the first argument.
	 * <p>
	 * First argument is one of {@link MigrationCommands}:
	 * Second argument specifies path to the spring context file that has "migrationService" bean defined.
	 */
	public static void main(String[] args) {
		try {
			if (args == null || args.length != 2) {
				throw new IllegalArgumentException("MigrationRunner.main(String[]) requires 2 arguments: MigrationCommand, Resource File");
			}
			LogUtils.info(MigrationRunner.class, "Starting migration with arguments: " + StringUtils.join(args, ","));
			ContextBasedMigrationRunner task = new ContextBasedMigrationRunner();
			task.setMigrationCommand(MigrationCommands.valueOf(args[0]));
			task.setResourceLocation(args[1]);
			task.execute();
		}
		catch (Throwable e) {
			LogUtils.error(MigrationRunner.class, "Error occurred when running migrations", e);
			System.exit(1);
		}
		System.exit(0);
	}
}
