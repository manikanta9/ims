package com.clifton.core.dataaccess.search;


import com.clifton.core.dataaccess.search.form.SearchFormUtils;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>SearchField</code> annotation marks a form field as a search field.
 * <p>
 * Search fields may define search properties for the {@link SearchRestriction} generated for a field. These properties include such things as entity mapping
 * paths (which designate the target field or fields), comparison operators, <tt>JOIN</tt> methods, and sorting options.
 *
 * @author vgomelsky
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface SearchField {

	public static final String SORT_NOT_ALLOWED = "SORT_NOT_ALLOWED";
	public static final String SORT_BY_SEARCH_FIELD = "SORT_BY_SEARCH_FIELD";
	public static final String SAME_AS_MARKED_FIELD = "SAME_AS_MARKED_FIELD";
	public static final String SAME_TABLE = "SAME_TABLE";
	public static final String NO_FORMULA = "NO_FORMULA";


	/**
	 * Specifies whether this field is a DATETIME field
	 * Used in the {@link SearchFormUtils#convertDateEqualsToDateRangeComparison} method
	 * called by the HibernateSearchFormConfigurer to allow searching from the UI to work on dateTime fields.
	 * Setting this value to true on a SearchField annotation allows the configurer to change the restriction
	 * from EQUALS to GREATER_THAN_OR_EQUAL and LESS_THAN the next day - this essentially disregards the time.
	 */
	boolean dateFieldIncludesTime() default false;


	/**
	 * Specifies whether the value for this search field is required.  Defaults to false.
	 */
	boolean required() default false;


	/**
	 * Specifies the name of the field used for searching. If the value is not defined, defaults to the name of the field marked by this annotation.
	 * <p>
	 * NOTE: to filter more than one field against the same value using OR condition, use comma-delimited field names: "number,name", etc.
	 */
	String searchField() default SAME_AS_MARKED_FIELD;


	/**
	 * Specifies path to the search fields if joins are necessary.
	 */
	String searchFieldPath() default SAME_TABLE;


	/**
	 * Specifies whether "LEFT JOIN" should be used in SQL that joins searchFieldPath.
	 * Defaults to "INNER JOIN".
	 */
	boolean leftJoin() default false;


	/**
	 * Sort field can be different than the search field.  For example, one can restrict by a foreign key field by display the label of the corresponding entity
	 * and sort by such.
	 * <p>
	 * If a sort field is not provided, then the sort field will default to the first field in the search field list, or the name of the field marked by this
	 * annotation if no search field list is given.
	 */
	String sortField() default SORT_BY_SEARCH_FIELD;


	/**
	 * Defines comparison conditions that are allowed for this field.
	 * Defaults to empty array which means EQ/GT/LT for Date, BigDecimal, Integer data types.  BEGINS_WITH for String.
	 * If searchField ends on ".id" then EQ/NE for Integer.
	 */
	ComparisonConditions[] comparisonConditions() default {};


	/**
	 * When multiple search fields are present this determines how to handle them. Defaults to use
	 * OR clauses.  Another option would be to coalesce the values in the search/sort
	 */
	SearchFieldCustomTypes searchFieldCustomType() default SearchFieldCustomTypes.OR;


	/**
	 * Used to specify a computed column on the application side. The formula provided will be used for sorting and ordering.
	 * The formula result should match the result returned from the computed getter in the corresponding DTO.
	 * <p>
	 * This is intended to be used in conjunction with SearchField, in which the entries in search field correspond to the placeholders in
	 * the provided formula. Placeholders are declared with '$' followed by the placeholder number.
	 * EX: @SearchField(searchField = "totalTimeNano,executionCount", formula = "CONVERT(DECIMAL(21, 6), $1 / $2 / 1000000000.0)")
	 */
	String formula() default NO_FORMULA;
}
