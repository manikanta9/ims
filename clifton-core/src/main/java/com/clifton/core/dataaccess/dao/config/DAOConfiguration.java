package com.clifton.core.dataaccess.dao.config;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheTypes;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.beans.PropertyDescriptor;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.ReentrantLock;


/**
 * The <code>DAOConfiguration</code> class contains configuration information for a given DAO object.
 *
 * @param <T>
 * @author vgomelsky
 */
public class DAOConfiguration<T extends IdentityObject> {

	// for faster performance
	private final Class<T> beanClass;

	//The interface converter class used when conversion from, for instance, templateEnabled DAOs is necessary
	private Class<?> beanConverterClass;

	//The interface service class used for CRUD operations for the given bean; used in replication primarily
	private Class<?> beanServiceClass;

	private Table table;
	private List<OrderByField> defaultOrderByFieldNameList;
	private Map<String, String> beanToDBMapping;
	private final ReentrantLock naturalKeysLock = new ReentrantLock();

	/**
	 * Natural keys that can be used to locate a distinct objects
	 */
	private Set<String> naturalKeys;

	/**
	 * Foreign key that points to parent table and can be used in audit trail, etc.
	 */
	private String parentFieldName;
	private final Column primaryKeyColumn;

	/**
	 * The maximum number of records to return without throwing an error.
	 * See com.clifton.system.schema.SystemTable#maxQuerySizeLimit for overrides.
	 */
	private Integer maxQuerySizeLimit;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Can be used by sub-classes. See NoTableDAOConfig.
	 */
	@SuppressWarnings("unchecked")
	protected DAOConfiguration(String dtoClassName) {
		this((Class<T>) CoreClassUtils.getClass(dtoClassName));
	}


	protected DAOConfiguration(Class<T> dtoClass) {
		this.beanClass = dtoClass;
		//Create a primaryKeyColumn for the 'noTableDAO' configurations.
		Column primaryKeyCol = null;
		PropertyDescriptor descriptor = BeanUtils.getPropertyDataTypes(this.beanClass).get("id");
		if (descriptor != null) {
			DataTypes dataType = new PropertyDataTypeConverter().convert(descriptor);
			primaryKeyCol = new Column();
			primaryKeyCol.setDataType(dataType);
		}
		this.primaryKeyColumn = primaryKeyCol;
	}


	/**
	 * Constructs a new DAOConfiguration object using the specified table.
	 */
	@SuppressWarnings("unchecked")
	public DAOConfiguration(Table table) {
		AssertUtils.assertNotEmpty(table.getName(), "tableName cannot be an empty string");
		AssertUtils.assertNotNull(table.getDtoClass(), "beanClass cannot be null");
		AssertUtils.assertNotEmpty(table.getColumnList(), "columnList cannot be empty");

		this.table = table;
		this.beanClass = (Class<T>) CoreClassUtils.getClass(table.getDtoClass());
		//DTO Converter Class must be defined explicitly - currently only used for mongo document conversion;
		if (table.getDtoConverterClass() != null) {
			this.beanConverterClass = CoreClassUtils.getClass(table.getDtoConverterClass());
		}
		//DTO Service Class must be defined explicitly - currently only used in replication;
		if (table.getDtoServiceClass() != null) {
			this.beanServiceClass = CoreClassUtils.getClass(table.getDtoServiceClass());
		}
		this.beanToDBMapping = new HashMap<>();

		Column primaryKeyCol = null;
		for (Column column : table.getColumnList(true)) {
			column.setTableName(table.getName());
			this.beanToDBMapping.put(column.getBeanPropertyName(), column.getName());
			if (column.isParentField()) {
				AssertUtils.assertNull(this.parentFieldName, "Table %1s already has parent field: %2s and cannot have another: %3s", table.getName(), this.parentFieldName,
						column.getBeanPropertyName());
				this.parentFieldName = column.getBeanPropertyName();
			}
			//Rather than looping the column list every time set the primaryKeyColumn during initialization
			if (primaryKeyCol == null && column.getDataType() != null && (column.getDataType().isIdentity() || column.getDataType().isNaturalKey())) {
				primaryKeyCol = column;
			}
		}
		this.primaryKeyColumn = primaryKeyCol;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return '[' + (this.table == null ? "NO_TABLE" : this.table.getName() + ": " + (this.beanClass == null ? "NO_CLASS" : this.beanClass.getName()) + ']');
	}


	/**
	 * Returns TRUE if versioning is enabled for this DAO.  This means that 'rv' value is added to each update statement
	 * and throw concurrency modify exception of the row gets stale.
	 */
	public boolean isVersionEnabled() {
		if (this.table != null && this.table.getVersion() != null) {
			return this.table.getVersion();
		}
		return false;
	}


	public boolean isCachingEnabled() {
		if (getTable() != null) {
			return (getTable().getCacheType() != null && CacheTypes.NONE != getTable().getCacheType());
		}
		return false;
	}


	/**
	 * Returns the cache name for T entity
	 * which is the class name for the bean from the configuration
	 */
	public String getCacheName() {
		return getBeanClass().getCanonicalName();
	}


	public Class<T> getBeanClass() {
		return this.beanClass;
	}


	public Class<?> getBeanConverterClass() {
		return this.beanConverterClass;
	}


	public Class<?> getBeanServiceClass() {
		return this.beanServiceClass;
	}


	/**
	 * Returns the table described by this configuration.
	 */
	public Table getTable() {
		return this.table;
	}


	public String getTableName() {
		return this.getTable().getName();
	}


	/**
	 * Returns a List of table columns.
	 */
	public List<Column> getColumnList() {
		return this.getTable().getColumnList();
	}


	public Column getColumnByName(String name) {
		List<Column> columnList = BeanUtils.filter(getColumnList(), Column::getName, name);
		if (CollectionUtils.getSize(columnList) != 1) {
			throw new IllegalStateException("Cannot find column '" + name + "' in DAO Config table: " + getTableName());
		}
		return columnList.get(0);
	}


	public Column getPrimaryKeyColumn() {
		if (this.primaryKeyColumn != null) {
			return this.primaryKeyColumn;
		}
		throw new IllegalStateException("Cannot find primary key column for " + getTable().getName());
	}


	public Set<String> getNaturalKeys(DaoLocator locator) {
		return getNaturalKeys(locator, null);
	}


	public Set<String> getNaturalKeys(DaoLocator locator, String selfReferenceKey) {
		Set<String> naturalKeySet = new HashSet<>();
		if (this.naturalKeys == null) {
			try {
				this.naturalKeysLock.lock();
				String tableName = getTableName();
				for (Column column : getColumnList()) {
					if (column.isNaturalKey()) {
						if (column.getFkTable() != null && selfReferenceKey == null) {
							ReadOnlyDAO<T> fkDao = locator.locate(column.getFkTable());
							ValidationUtils.assertNotNull(fkDao, "Unable to locate DAO for table: " + column.getFkTable());
							//If tableName is the same as the fkTable then this is a self-referencing natural key and should only go one more level deep
							Set<String> fkNaturalKeys = fkDao.getConfiguration().getNaturalKeys(locator, tableName.equals(column.getFkTable()) ? column.getBeanPropertyName() : null);
							for (String fkNaturalKey : CollectionUtils.getIterable(fkNaturalKeys)) {
								naturalKeySet.add(column.getBeanPropertyName() + "." + fkNaturalKey);
							}
						}
						else if ((selfReferenceKey == null) || (!selfReferenceKey.equals(column.getBeanPropertyName()))) {
							naturalKeySet.add(column.getBeanPropertyName());
						}
					}
				}
				this.naturalKeys = Collections.unmodifiableSet(naturalKeySet);
			}
			finally {
				this.naturalKeysLock.unlock();
			}
		}
		return this.naturalKeys.isEmpty() ? null : this.naturalKeys;
	}


	/**
	 * Returns database field name that corresponds to the specified bean field name.
	 * Throws <code>IllegalArgumentException</code> exception if such mapping cannot be found.
	 */
	public String getDBFieldName(String beanFieldName) {
		String dbFieldName = this.beanToDBMapping.get(beanFieldName);
		if (dbFieldName == null) {
			throw new IllegalArgumentException("Cannot find corresponding database field for bean field: " + beanFieldName + ". Table: " + getTableName());
		}
		return dbFieldName;
	}


	/**
	 * Returns true if the specified bean field name is part of this DTO
	 */
	public boolean isValidBeanField(String beanFieldName) {
		return this.beanToDBMapping.containsKey(beanFieldName);
	}


	public String getBeanFieldName(String dbFieldName) {
		for (Column column : this.getTable().getColumnList(true)) {
			if (dbFieldName.equals(column.getName())) {
				return column.getBeanPropertyName();
			}
		}
		throw new IllegalArgumentException("Cannot find corresponding bean field for database field: " + dbFieldName + ". Table: " + getTableName());
	}


	/**
	 * Returns an ordered List of the columns that should be added to the ORDER BY clause.
	 * Defaults to columns with names "label", "name", or "id" in that order, if no explicit sort order
	 * is defined.
	 *
	 * @return an ordered list of columns to be sorted.
	 * @throws <code>IllegalStateException</code> if no explicit sort order defined or if no default found.
	 */
	public List<OrderByField> getOrderByList() {
		if (this.defaultOrderByFieldNameList == null) {
			List<OrderByField> orderList = new ArrayList<>();
			List<Column> localColumns = this.getTable().getSortOrderColumnList(true);
			for (Column column : CollectionUtils.getIterable(localColumns)) {
				orderList.add(new OrderByField(column.getBeanPropertyName(), column.getSortDirection()));
			}

			if (CollectionUtils.isEmpty(orderList)) {
				addDefaultOrder(orderList);
			}
			this.defaultOrderByFieldNameList = orderList;

			if (CollectionUtils.isEmpty(this.defaultOrderByFieldNameList)) {
				throw new IllegalStateException("Cannot find default order by field for table: " + getTableName());
			}
		}
		return this.defaultOrderByFieldNameList;
	}


	private void addDefaultOrder(List<OrderByField> orderList) {
		if (this.beanToDBMapping.containsKey("label")) {
			orderList.add(new OrderByField("label", OrderByDirections.ASC));
		}
		else if (this.beanToDBMapping.containsKey("name")) {
			orderList.add(new OrderByField("name", OrderByDirections.ASC));
		}
		else if (this.beanToDBMapping.containsKey("id")) {
			orderList.add(new OrderByField("id", OrderByDirections.ASC));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	public String getParentFieldName() {
		return this.parentFieldName;
	}


	public int getMaxQuerySizeLimit() {
		return this.maxQuerySizeLimit == null ? PagingCommand.MAX_LIMIT : this.maxQuerySizeLimit;
	}


	public void setMaxQuerySizeLimit(int maxQuerySizeLimit) {
		this.maxQuerySizeLimit = maxQuerySizeLimit;
	}
}
