package com.clifton.core.dataaccess.transactions;


import org.aspectj.lang.annotation.Aspect;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.orm.hibernate5.HibernateTransactionManager;


/**
 * The <code>CustomAspectJAnnotationTransactionAspect</code> maintains a map of {@link SessionFactory} objects
 * to {@link HibernateTransactionManager} objects which it uses to set the correct transaction manager when a
 * a method annotated with @Transactional is called.
 * <p>
 * NOTE: When the txObject does not implement SessionAware or it doesn't match the any regex in packageRegexToTransactionManageName
 * map, it will use the default behavior of getting the transaction manager by name.
 *
 * @author mwacker
 */
@Aspect
@Configurable
public class CustomAspectJAnnotationTransactionAspect extends BaseCustomAspectJAnnotationTransactionAspect implements ApplicationListener<ApplicationContextEvent> {


}
