package com.clifton.core.dataaccess.search.hibernate.expression;


import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.StringType;


/**
 * The <code>ConcatenateStringAndNumberExpression</code> is used for searching across concatenation of multiple fields where not all fields are a string.
 * <p/>
 * For example: On the BillingDefinition we rely on the ID and contract name.  ID is numeric so it needs to be cast first.
 * Resolves to (CAST(BillingDefinitionID AS VARCHAR) + BillingBusinessContract.ContractName) = ? (or LIKE, depending on search comparison used)
 *
 * @author manderson
 */
public class ConcatenateStringAndNumberExpression implements Criterion {

	private final String comparisonExpression;

	private final String[] propertyNames;

	private final Object value;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ConcatenateStringAndNumberExpression(String comparisonExpression, String[] propertyNames, Object value) {
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.value = value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragment = new StringBuilder(" (");
		for (int i = 0; i < this.propertyNames.length; i++) {
			String column = criteriaQuery.getColumn(criteria, this.propertyNames[i]);
			boolean appendStringCast = (Number.class.isAssignableFrom(criteriaQuery.getType(criteria, this.propertyNames[i]).getReturnedClass()));
			if (appendStringCast) {
				fragment.append("CAST(");
			}
			fragment.append(column);
			if (appendStringCast) {
				fragment.append(" AS VARCHAR)");
			}
			if (i < this.propertyNames.length - 1) {
				fragment.append(" + '_' + ");
			}
		}
		fragment.append(") ");
		fragment.append(this.comparisonExpression);
		// For cases like: LIKE '%' + ? + '%' we don't want to add a second ?, but we need to for cases like =, >, <, etc.
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragment.append(" ?");
		}
		return fragment.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[]{new TypedValue(StringType.INSTANCE, this.value)};
	}
}
