package com.clifton.core.dataaccess.db;

import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.logging.LogUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;


/**
 * Provides a way to lookup the name of the sql type {@link java.sql.Types} from the id
 *
 * @author KellyJ
 */
public class SqlTypeUtils {

	private static final Map<Integer, String> DATA_TYPES = new HashMap<>();


	static {
		for (Field field : Types.class.getFields()) {
			try {
				int modifiers = field.getModifiers();
				if (Modifier.isPublic(modifiers) && Modifier.isStatic(modifiers) && Modifier.isFinal(modifiers)) {
					DATA_TYPES.put((Integer) field.get(null), field.getName());
				}
			}
			catch (Throwable e) {
				LogUtils.warn(DataColumnImpl.class, "Could not find sql Type for: " + field);
			}
		}
	}


	/**
	 * @param sqlType id from {@link java.sql.Types}
	 * @return the name of the sqlType {@link java.sql.Types}
	 */
	public static String getDataTypeNameForSqlType(Integer sqlType) {
		return DATA_TYPES.get(sqlType);
	}
}
