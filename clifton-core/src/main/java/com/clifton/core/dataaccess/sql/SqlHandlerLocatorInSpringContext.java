package com.clifton.core.dataaccess.sql;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.DataSourceUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.Map;


/**
 * The <code>SqlHandlerLocatorInSpringContext</code> class locates instances of {@link SqlHandler}
 * objects in Spring's context for the specified {@link DataSource} bean name.
 *
 * @author vgomelsky
 */
@Component
public class SqlHandlerLocatorInSpringContext implements SqlHandlerLocator, ApplicationContextAware, CustomCache<String, SqlHandler> {

	private ApplicationContext applicationContext;
	private CacheHandler<String, SqlHandler> cacheHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SqlHandler locate(String dataSourceName) {
		SqlHandler result = getCacheHandler().get(getCacheName(), dataSourceName);
		if (result == null) {
			DataSource dataSource = (DataSource) this.applicationContext.getBean(dataSourceName);
			Map<String, SqlHandler> sqlHandlerMap = this.applicationContext.getBeansOfType(SqlHandler.class);
			for (SqlHandler sqlHandler : sqlHandlerMap.values()) {
				if (DataSourceUtils.isEqual(dataSource, sqlHandler.getDataSource())) {
					if (result != null && sqlHandler != result) {
						throw new IllegalStateException("Found more than one SqlHandler that uses the same data source '" + dataSourceName + "'.");
					}
					result = sqlHandler;
				}
			}
			getCacheHandler().put(getCacheName(), dataSourceName, result);
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	@Override
	public CacheHandler<String, SqlHandler> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, SqlHandler> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
