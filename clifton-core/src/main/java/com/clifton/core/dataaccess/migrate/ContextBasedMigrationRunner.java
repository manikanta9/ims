package com.clifton.core.dataaccess.migrate;


import com.clifton.core.dataaccess.migrate.execution.MigrationExecutionHandler;
import com.clifton.core.dataaccess.migrate.execution.database.MigrationExecutionDatabaseHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;

import java.util.List;


/**
 * The <code>ContextBasedMigrationRunner</code> class
 * either prints or executes database migrations
 * <p>
 * Based upon configuration, this task can also create a new instance of the database (dropping it first),
 * Clean up after itself (dropping the database after migrations are run)
 *
 * @author manderson
 */
public class ContextBasedMigrationRunner {

	private final static String MIGRATION_AFTER_RESTORE_SQL_FILE_PROPERTY = "migration.after.restore.sql.file";
	private final static String MIGRATION_AFTER_RESTORE_BEFORE_MIGRATE_SQL_FILE_PROPERTY = "migration.after.restore.before.migrate.sql.file";
	private final static String MIGRATION_BEFORE_MIGRATE_SQL_FILE_PROPERTY = "migration.before.migrate.sql.file";

	private MigrationCommands migrationCommand;

	private String resourceLocation;
	private Resource resource;

	/**
	 * One or multiple files to run.  If multiple, each is separated by a ;
	 */
	private List<String> runSqlFileNameList;
	private List<String> runSqlAfterRestoreFileNameList;
	private List<String> runSqlBeforeMigrateFileNameList;

	private MigrationExecutionHandler migrationExecutionHandler;
	private MigrationExecutionDatabaseHandler migrationExecutionDatabaseHandler;


	public final void execute() {
		if (getResource() == null) {
			setResource(new DefaultResourceLoader().getResource(getResourceLocation()));
		}
		GenericApplicationContext springContext = setup();
		run(springContext);
	}


	private GenericApplicationContext setup() {
		GenericApplicationContext springContext = getSpringContext(getResource());
		if (getMigrationCommand().isRestore()) {
			setRunSqlAfterRestoreFileNameList(StringUtils.splitOnDelimiter(springContext.getBeanFactory().resolveEmbeddedValue("${" + MIGRATION_AFTER_RESTORE_BEFORE_MIGRATE_SQL_FILE_PROPERTY + "}"), ";"));
			LogUtils.info(getClass(), "Migration After Restore, Before Migrate SQL File Path: [" + StringUtils.collectionToCommaDelimitedString(getRunSqlAfterRestoreFileNameList()) + "]");
			setRunSqlFileNameList(StringUtils.splitOnDelimiter(springContext.getBeanFactory().resolveEmbeddedValue("${" + MIGRATION_AFTER_RESTORE_SQL_FILE_PROPERTY + "}"), ";"));
			LogUtils.info(getClass(), "Migration After Restore, After Migrate File Path: [" + StringUtils.collectionToCommaDelimitedString(getRunSqlFileNameList()) + "]\n");
		}
		if (getMigrationCommand().isMigrate()) {
			setRunSqlBeforeMigrateFileNameList(StringUtils.splitOnDelimiter(springContext.getBeanFactory().resolveEmbeddedValue("${" + MIGRATION_BEFORE_MIGRATE_SQL_FILE_PROPERTY + "}"), ";"));
			LogUtils.info(getClass(), "Migration Before Migrate File Path: [" + StringUtils.collectionToCommaDelimitedString(getRunSqlBeforeMigrateFileNameList()) + "]\n");
		}

		this.migrationExecutionHandler = (MigrationExecutionHandler) springContext.getBean("migrationExecutionHandler");
		this.migrationExecutionDatabaseHandler = (MigrationExecutionDatabaseHandler) springContext.getBean("migrationExecutionDatabaseHandler");

		if (getMigrationCommand().isBackup()) {
			getMigrationExecutionDatabaseHandler().backupMigrationDatabase();
		}

		boolean reset = false;
		if (getMigrationCommand().isRebuild()) {
			getMigrationExecutionDatabaseHandler().createMigrationDatabase();
			reset = true;
		}
		else if (getMigrationCommand().isRestore()) {
			getMigrationExecutionDatabaseHandler().restoreMigrationDatabase();
			reset = true;
		}
		// Reset because DB connection is reset
		if (reset) {
			springContext.close();
			springContext = getSpringContext(getResource());
			setMigrationExecutionHandler((MigrationExecutionHandler) springContext.getBean("migrationExecutionHandler"));
			setMigrationExecutionDatabaseHandler((MigrationExecutionDatabaseHandler) springContext.getBean("migrationExecutionDatabaseHandler"));
		}

		if (reset && getMigrationCommand().isRestore() && !CollectionUtils.isEmpty(getRunSqlAfterRestoreFileNameList())) {
			getMigrationExecutionHandler().executeSQLFiles(getRunSqlAfterRestoreFileNameList());
		}
		if (getMigrationCommand().isMigrate() && !CollectionUtils.isEmpty(getRunSqlBeforeMigrateFileNameList())) {
			getMigrationExecutionHandler().executeSQLFiles(getRunSqlBeforeMigrateFileNameList());
		}
		return springContext;
	}


	private void run(GenericApplicationContext springContext) {
		if (getMigrationCommand().isMigrate()) {
			getMigrationExecutionHandler().migrate(springContext);

			if (!CollectionUtils.isEmpty(getRunSqlFileNameList())) {
				getMigrationExecutionHandler().executeSQLFiles(getRunSqlFileNameList());
			}
		}
		if (getMigrationCommand().isRestore()) {
			String applicationEnvironmentLevel = springContext.getBeanFactory().resolveEmbeddedValue("${application.environment.level}");
			getMigrationExecutionDatabaseHandler().updateEnvironmentTable(applicationEnvironmentLevel);
		}
		if (getMigrationCommand().isPrint()) {
			String sql = getMigrationExecutionHandler().generateSQL();
			LogUtils.info(getClass(), sql);
		}
	}


	/**
	 * Creates a new new application context, loads it from the specified XML file and refreshes it.
	 */
	private GenericApplicationContext getSpringContext(Resource xmlResource) {
		if (!xmlResource.exists()) {
			throw new IllegalArgumentException("Resource not found [" + xmlResource + "]");
		}
		GenericApplicationContext context = new GenericApplicationContext();
		XmlBeanDefinitionReader xmlReader = new XmlBeanDefinitionReader(context);
		try {
			xmlReader.loadBeanDefinitions(xmlResource);
		}
		catch (Exception e) {
			throw new RuntimeException("Error loading bean definitions from resource: " + xmlResource.getFilename() + ", exception: " + e.getMessage(), e);
		}
		context.refresh();
		return context;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getResourceLocation() {
		return this.resourceLocation;
	}


	public void setResourceLocation(String resourceLocation) {
		this.resourceLocation = resourceLocation;
	}


	public Resource getResource() {
		return this.resource;
	}


	public void setResource(Resource resource) {
		this.resource = resource;
	}


	public MigrationCommands getMigrationCommand() {
		return this.migrationCommand;
	}


	public void setMigrationCommand(MigrationCommands migrationCommand) {
		this.migrationCommand = migrationCommand;
	}


	public List<String> getRunSqlFileNameList() {
		return this.runSqlFileNameList;
	}


	public void setRunSqlFileNameList(List<String> runSqlFileNameList) {
		this.runSqlFileNameList = runSqlFileNameList;
	}


	public List<String> getRunSqlAfterRestoreFileNameList() {
		return this.runSqlAfterRestoreFileNameList;
	}


	public void setRunSqlAfterRestoreFileNameList(List<String> runSqlAfterRestoreFileNameList) {
		this.runSqlAfterRestoreFileNameList = runSqlAfterRestoreFileNameList;
	}


	public List<String> getRunSqlBeforeMigrateFileNameList() {
		return this.runSqlBeforeMigrateFileNameList;
	}


	public void setRunSqlBeforeMigrateFileNameList(List<String> runSqlBeforeMigrateFileNameList) {
		this.runSqlBeforeMigrateFileNameList = runSqlBeforeMigrateFileNameList;
	}


	public MigrationExecutionHandler getMigrationExecutionHandler() {
		return this.migrationExecutionHandler;
	}


	public void setMigrationExecutionHandler(MigrationExecutionHandler migrationExecutionHandler) {
		this.migrationExecutionHandler = migrationExecutionHandler;
	}


	public MigrationExecutionDatabaseHandler getMigrationExecutionDatabaseHandler() {
		return this.migrationExecutionDatabaseHandler;
	}


	public void setMigrationExecutionDatabaseHandler(MigrationExecutionDatabaseHandler migrationExecutionDatabaseHandler) {
		this.migrationExecutionDatabaseHandler = migrationExecutionDatabaseHandler;
	}
}
