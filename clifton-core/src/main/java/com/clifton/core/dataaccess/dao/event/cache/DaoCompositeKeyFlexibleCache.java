package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringFlexibleDaoCache;


/**
 * The interface for {@link SelfRegisteringFlexibleDaoCache} for dual-key caches.
 *
 * @author MikeH
 */
public interface DaoCompositeKeyFlexibleCache<T extends IdentityObject, U extends Comparable<U>, K1, K2> {

	/**
	 * Retrieves the latest entity for the given key values as of the given order value.
	 */
	public T getBeanForKeyValues(ReadOnlyDAO<T> dao, U order, K1 keyValue1, K2 keyValue2);


	/**
	 * Retrieves the latest entity for the given key values as of the given order value.
	 * <p>
	 * An exception will be thrown if the entity is not found.
	 */
	public T getBeanForKeyValuesStrict(ReadOnlyDAO<T> dao, U order, K1 keyValue1, K2 keyValue2);
}
