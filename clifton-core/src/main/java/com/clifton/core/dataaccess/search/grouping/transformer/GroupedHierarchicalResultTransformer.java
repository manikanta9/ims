package com.clifton.core.dataaccess.search.grouping.transformer;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.grouping.GroupedHierarchicalResult;
import com.clifton.core.dataaccess.search.grouping.GroupingProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchForm;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class GroupedHierarchicalResultTransformer<T extends IdentityObject> extends BaseGroupedResultTransformer<T> {


	public GroupedHierarchicalResultTransformer(GroupingSearchForm groupingSearchForm, DaoLocator daoLocator, DAOConfiguration<T> daoConfiguration) {
		super(groupingSearchForm, daoLocator, daoConfiguration);
	}


	@Override
	public Object transformTuple(Object[] tuple, String[] aliases) {
		return new GroupedHierarchicalResult(null, Arrays.copyOf(tuple, getGroupingPropertyList().size()), getCountFromTuple(tuple), getAggregatePropertyMapFromTuple(tuple));
	}


	@SuppressWarnings("rawtypes")
	@Override
	public List transformList(List tuples) {
		// We only return the top level with children property populated
		List<GroupedHierarchicalResult> topLevelList = new ArrayList<>();
		int id = -10;
		Map<String, GroupedHierarchicalResult> resultMap = new HashMap<>();

		int groupedColumnCount = getGroupingPropertyList().size();

		for (Object tuple : tuples) {
			GroupedHierarchicalResult detailedResult = (GroupedHierarchicalResult) tuple;
			String parentKey = null;
			GroupedHierarchicalResult parent = null;
			for (int i = 0; i < groupedColumnCount; i++) {
				GroupingProperty groupingProperty = getGroupingPropertyList().get(i);
				Object value = ((Object[]) detailedResult.getGroupValue())[i];
				String key = StringUtils.generateKey(parentKey, i, value == null ? groupingProperty.getEmptyValueLabel() : value);
				GroupedHierarchicalResult groupedHierarchicalResult = resultMap.get(key);
				if (groupedHierarchicalResult == null) {
					groupedHierarchicalResult = new GroupedHierarchicalResult(groupingProperty, value);
					groupedHierarchicalResult.setParentId(parent == null ? null : parent.getId());
					groupedHierarchicalResult.setId(id);
					id = id - 10;

					if (value != null) {
						populateGroupLabel(groupedHierarchicalResult);
					}

					resultMap.put(key, groupedHierarchicalResult);
					if (parent == null) {
						topLevelList.add(groupedHierarchicalResult);
					}
					else {
						parent.addChild(groupedHierarchicalResult);
					}
				}
				if ((i == (groupedColumnCount - 1))) {
					groupedHierarchicalResult.setCount(detailedResult.getCount());
					groupedHierarchicalResult.setAggregateValueMap(detailedResult.getAggregateValueMap());
				}
				parent = groupedHierarchicalResult;
				parentKey = key;
			}
		}
		return topLevelList;
	}


	private void populateGroupLabel(GroupedHierarchicalResult groupedHierarchicalResult) {
		Object hydratedValue = getHydratedValue(groupedHierarchicalResult.getGroupProperty(), groupedHierarchicalResult.getGroupValue());
		if (hydratedValue instanceof IdentityObject) {
			groupedHierarchicalResult.setGroupLabel(BeanUtils.getLabel(hydratedValue));
		}
	}
}
