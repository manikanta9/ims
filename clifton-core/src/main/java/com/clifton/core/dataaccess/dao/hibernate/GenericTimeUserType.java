package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.Time;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Comparator;
import java.util.Properties;


/**
 * The <code>GenericTimeUserType</code> converts a {@link Time} object to and from the it's integer representation for the database.
 *
 * @author mwacker
 */
public class GenericTimeUserType implements UserType, ParameterizedType, Comparator<GenericTimeUserType> {

	private static final int[] SQL_TYPES = {Types.INTEGER};


	@Override
	public void setParameterValues(@SuppressWarnings("unused") Properties parameters) {
		// empty
	}


	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}


	@Override
	public Class<?> returnedClass() {
		return Time.class;
	}


	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, @SuppressWarnings("unused") SharedSessionContractImplementor session, @SuppressWarnings("unused") Object owner) throws HibernateException,
			SQLException {
		int value = resultSet.getInt(names[0]);
		if (resultSet.wasNull()) {
			return null;
		}

		try {
			return Time.valueOf(value);
		}
		catch (Exception e) {
			throw new HibernateException("Exception while getting time value.", e);
		}
	}


	@Override
	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, @SuppressWarnings("unused") SharedSessionContractImplementor session) throws HibernateException, SQLException {
		try {
			if (null == value) {
				preparedStatement.setNull(index, Types.INTEGER);
			}
			else {
				preparedStatement.setInt(index, ((Time) value).getMilliseconds());
			}
		}
		catch (Exception e) {
			throw new HibernateException("Exception while setting time value.", e);
		}
	}


	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}


	@Override
	public boolean isMutable() {
		return false;
	}


	@Override
	public Object assemble(Serializable cached, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return cached;
	}


	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}


	@Override
	public Object replace(Object original, @SuppressWarnings("unused") Object target, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return original;
	}


	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}


	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		}
		if ((null == x) || (null == y)) {
			return false;
		}
		return x.equals(y);
	}


	@Override
	public int compare(GenericTimeUserType o1, GenericTimeUserType o2) {
		return CompareUtils.compare(o1, o2);
	}
}
