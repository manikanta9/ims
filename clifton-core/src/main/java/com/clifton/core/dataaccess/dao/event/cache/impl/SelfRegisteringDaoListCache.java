package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import static com.clifton.core.beans.BeanUtils.getPropertyValues;


/**
 * The <code>SelfRegisteringDaoListCache</code> class is a helper class that contains common methods utilized by
 * {@link SelfRegisteringSingleKeyDaoListCache} and {@link SelfRegisteringCompositeKeyDaoListCache}
 * <p>
 * NOTE: UNLESS SOMETHING CUSTOM, SHOULD NOT USE THIS CACHE DIRECTLY, INSTEAD USE THE SINGLE OR COMPOSITE KEY VERSIONS
 * <p>
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events. (By default MODIFY (INSERT UPDATE OR DELETE))
 * <p>
 * Note: Clears on inserts because the related entity that the list is stored for needs to know about the new one in the list
 *
 * @param <T>
 * @author Mary Anderson
 */
public abstract class SelfRegisteringDaoListCache<T extends IdentityObject> extends SelfRegisteringSimpleDaoCache<T, String, Serializable[]> {

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Bean Key Properties - Used for DAO Look ups
	 */
	protected abstract String[] getBeanKeyProperties();


	/**
	 * Returns the key for the bean in the cache
	 */
	protected String getBeanKey(T bean) {
		return getBeanKey(bean, getBeanKeyProperties());
	}


	private String getBeanKey(T bean, String[] keyProperties) {
		if (keyProperties == null || keyProperties.length == 0) {
			throw new IllegalStateException("Key Properties is Required to generate Key for a specified bean.");
		}
		int size = keyProperties.length;
		// Most cases we only have 1 key property - so no need to create Object[] for this case
		if (size == 1) {
			return getBeanKeyForProperties(BeanUtils.getPropertyValue(bean, keyProperties[0]));
		}
		Object[] keyPropertyValues = new Object[size];
		for (int i = 0; i < size; i++) {
			keyPropertyValues[i] = BeanUtils.getPropertyValue(bean, keyProperties[i]);
		}
		return getBeanKeyForProperties(keyPropertyValues);
	}


	///////////////////////////////////////////////////////////////////////


	@Transactional(readOnly = true) // if called outside of transaction avoid expensive transaction creation for each findByPrimaryKey call
	protected List<T> getBeanListImpl(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		String key = getBeanKeyForProperties(keyProperties);
		Serializable[] idList = getCacheHandler().get(getCacheName(), key);
		if (!ArrayUtils.isEmpty(idList)) {
			List<T> beanList = new ArrayList<>();
			for (Serializable id : idList) {
				T bean = dao.findByPrimaryKey(id);
				// If bean is missing, then the transaction was rolled back - retrieve the list again from the database and reset cache
				if (bean == null) {
					idList = null;
					break;
				}
				else {
					beanList.add(bean);
				}
			}
			if (idList != null) {
				return beanList;
			}
		}

		if (idList == null) {
			List<T> beanList = lookupBeanList(dao, throwExceptionIfNotFound, keyProperties);
			if (beanList == null) {
				beanList = new ArrayList<>();
			}
			setBeanList(key, beanList);
			return beanList;
		}
		return null;
	}


	@Transactional(readOnly = true) // if called outside of transaction avoid expensive transaction creation for each findByPrimaryKey call
	protected Serializable[] getBeanIdListImpl(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		String key = getBeanKeyForProperties(keyProperties);
		Serializable[] idList = getCacheHandler().get(getCacheName(), key);

		if (idList == null) {
			List<T> beanList = lookupBeanList(dao, throwExceptionIfNotFound, keyProperties);
			if (beanList == null) {
				beanList = new ArrayList<>();
			}
			setBeanList(key, beanList);
			idList = BeanUtils.getPropertyValues(beanList, T::getIdentity, Serializable.class);
		}

		return idList;
	}


	protected void clearBeanListImpl(Object... keyProperties) {
		getCacheHandler().remove(getCacheName(), getBeanKeyForProperties(keyProperties));
	}


	/**
	 * When bean isn't set in the cache, use this method to determine how to look it up in the database and immediately set in the cache
	 */
	protected List<T> lookupBeanList(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		List<T> beanList = dao.findByFields(getBeanKeyProperties(), keyProperties);
		if (CollectionUtils.isEmpty(beanList) && throwExceptionIfNotFound) {
			throw new ValidationException("Unable to find any " + StringUtils.splitWords(dao.getConfiguration().getTableName()) + " with properties [" + ArrayUtils.toString(getBeanKeyProperties())
					+ "] and value(s) [" + ArrayUtils.toString(keyProperties) + "]");
		}
		return beanList;
	}


	public void setBeanList(String key, List<T> beanList) {
		if (beanList == null) {
			beanList = new ArrayList<>();
		}
		if (StringUtils.isEmpty(key)) {
			T bean = CollectionUtils.getFirstElement(beanList);
			if (bean != null) {
				key = getBeanKey(bean);
			}
		}
		if (!StringUtils.isEmpty(key)) {
			getCacheHandler().put(getCacheName(), key, getPropertyValues(beanList, T::getIdentity, Serializable.class));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			String key = getBeanKey(bean);
			// Because we cache the id - we only need to clear the cache for before/after change if the key actually changes (i.e. entity belongs to a different list)
			if (event.isUpdate()) {
				T originalBean = getOriginalBean(dao, bean);
				String originalKey = getBeanKey(originalBean);
				if (!StringUtils.isEqual(key, originalKey)) {
					if (!StringUtils.isEmpty(originalKey)) {
						getCacheHandler().remove(getCacheName(), originalKey);
						getCacheHandler().remove(getCacheName(), key);
					}
				}
			}
			else if (!StringUtils.isEmpty(key)) {
				getCacheHandler().remove(getCacheName(), key);
			}
		}
	}
}
