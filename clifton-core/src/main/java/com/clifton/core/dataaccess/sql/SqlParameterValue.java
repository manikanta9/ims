package com.clifton.core.dataaccess.sql;

import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.springframework.jdbc.core.SqlTypeValue;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;


/**
 * SqlParameterValue is used to defined parameter values and their types.  The type is necessary when the value is null so we can still properly convert to the
 * correct Sql type in order to execute queries correctly.
 * <p>
 * Created by Mary on 9/29/2015.
 */
public class SqlParameterValue {

	private final DataTypeNames dataTypeName;
	private final Object value;

	////////////////////////////////////////////////////////////////////////////////
	////////////                  Helpful Constructors                 /////////////
	////////////////////////////////////////////////////////////////////////////////


	private SqlParameterValue(DataTypeNames dataTypeName, Object value) {
		this.dataTypeName = dataTypeName;
		this.value = value;
	}


	/**
	 * Used by SqlSearchFormConfigurer which needs to be re-factored in order to remove this
	 */
	public static SqlParameterValue ofUnknownType(Object value) {
		return new SqlParameterValue(null, value);
	}


	public static SqlParameterValue ofType(String dataTypeName, Object value) {
		return new SqlParameterValue(DataTypeNames.valueOf(dataTypeName), value);
	}


	public static SqlParameterValue ofType(DataTypeNames dataType, Object value) {
		return new SqlParameterValue(dataType, value);
	}


	public static SqlParameterValue ofInteger(Integer value) {
		return new SqlParameterValue(DataTypeNames.INTEGER, value);
	}


	public static SqlParameterValue ofLong(Long value) {
		return new SqlParameterValue(DataTypeNames.INTEGER, value);
	}


	public static SqlParameterValue ofShort(Short value) {
		return new SqlParameterValue(DataTypeNames.INTEGER, value);
	}


	public static SqlParameterValue ofBigDecimal(BigDecimal value) {
		return new SqlParameterValue(DataTypeNames.DECIMAL, value);
	}


	public static SqlParameterValue ofDate(Date value) {
		return new SqlParameterValue(DataTypeNames.DATE, value);
	}


	public static SqlParameterValue ofString(String value) {
		// Note: TEXT type is also a String - I don't think we need the distinction at this point
		return new SqlParameterValue(DataTypeNames.STRING, value);
	}


	public static SqlParameterValue ofBoolean(Boolean value) {
		return new SqlParameterValue(DataTypeNames.BOOLEAN, value);
	}


	public static SqlParameterValue ofSecret(byte[] value) {
		return new SqlParameterValue(DataTypeNames.SECRET, value);
	}


	public static SqlParameterValue ofBinary(byte[] value) {
		return new SqlParameterValue(DataTypeNames.BINARY, value);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getSqlType() {
		if (this.dataTypeName == null) {
			return SqlTypeValue.TYPE_UNKNOWN;
		}
		if (DataTypeNames.BOOLEAN == this.dataTypeName) {
			return Types.BIT;
		}
		if (DataTypeNames.DATE == this.dataTypeName) {
			return Types.TIMESTAMP;
		}
		if (DataTypeNames.INTEGER == this.dataTypeName) {
			return Types.INTEGER;
		}
		if (DataTypeNames.DECIMAL == this.dataTypeName) {
			return Types.DECIMAL;
		}
		if (DataTypeNames.BINARY == this.dataTypeName || DataTypeNames.SECRET == this.dataTypeName) {
			return Types.VARBINARY;
		}
		// Default - Assume a String???
		return Types.NVARCHAR;
	}


	/////////////////////////////////////////////////////
	////   Overrides
	/////////////////////////////////////////////////////


	@Override
	public String toString() {
		if (getValue() == null) {
			return StringUtils.EMPTY_STRING;
		}

		if (DataTypeNames.DATE == getDataTypeName()) {
			return DateUtils.fromDateSmart((Date) getValue());
		}

		return getValue().toString();
	}


	/////////////////////////////////////////////////////
	////   Getter and Setter Methods
	/////////////////////////////////////////////////////


	public DataTypeNames getDataTypeName() {
		return this.dataTypeName;
	}


	public Object getValue() {
		return this.value;
	}
}
