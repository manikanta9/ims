package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.multipart.MultipartFileImpl;
import com.itextpdf.text.DocListener;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfCopy;
import com.itextpdf.text.pdf.PdfImportedPage;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfSmartCopy;
import org.springframework.web.multipart.MultipartFile;

import javax.activation.MimetypesFileTypeMap;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.nio.file.DirectoryStream;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.stream.Collectors;


public class FileUtils {

	public static final String INVALID_CHARACTER_REG_EXP = "[\n\r\\\\/:\"*?<>|–\u2018\u2019\u201c\u201d]+";
	public static final String REMOTE_FILE_REGEX = "^((smb://)|(//)|(\\\\\\\\))(?:.*)";
	public static final String JAVA_TEMP_DIRECTORY = System.getProperty("java.io.tmpdir");

	private static final int PDF_PAGE_NUMBER_FONT_SIZE = 6;
	private static final int PDF_PAGE_NUMBER_MARGIN_OFFSET = 30;

	private static final MimetypesFileTypeMap mimeTypeMap = new MimetypesFileTypeMap();

	private static final Map<String, String> CUSTOM_MIME_CONTENT_TYPE_MAP = new HashMap<>();

	public static final String SYSTEM_TEMPORARY_DIRECTORY = System.getProperty("java.io.tmpdir");


	static {
		CUSTOM_MIME_CONTENT_TYPE_MAP.put("csv", "text/csv");
		CUSTOM_MIME_CONTENT_TYPE_MAP.put("xls", "application/vnd.ms-excel");
		CUSTOM_MIME_CONTENT_TYPE_MAP.put("xlsx", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static String replaceInvalidCharacters(String fileName, String replacement) {
		// First strip invalid characters from the replacement
		if (replacement == null) {
			replacement = "";
		}
		replacement = replacement.replaceAll(INVALID_CHARACTER_REG_EXP, "");

		if (fileName != null) {
			fileName = fileName.replaceAll(INVALID_CHARACTER_REG_EXP, replacement);
		}
		return fileName;
	}


	/**
	 * Returns the extension for a given file format.  For example "excel" = "xls"
	 */
	public static String getExtensionForFormat(String format) {
		String extension;
		format = format.toLowerCase();
		if ("pdf".equals(format)) {
			extension = "pdf";
		}
		else if ("excel".equals(format) || "xls".equals(format)) {
			extension = "xls";
		}
		else if ("xlsx".equals(format)) {
			extension = "xlsx";
		}
		else if ("tiff".equals(format)) {
			extension = "tif";
		}
		else if ("csv".equals(format)) {
			extension = "csv";
		}
		else if ("mhtml".equals(format)) {
			extension = "mhtml";
		}
		else if ("xml".equals(format)) {
			extension = "xml";
		}
		else if ("word".equals(format)) {
			extension = "doc";
		}
		else {
			extension = "unknown";
		}
		return extension;
	}


	/**
	 * Returns the file extension for the given fileName in all lowercase.
	 * <p>
	 * Returns null if no extension
	 */
	public static String getFileExtension(String fileName) {
		return getFileExtension(fileName, false);
	}


	/**
	 * Returns the file extension for the given fileName in all lowercase unless keepExtensionCase is true.
	 * <p>
	 * Returns null if no extension
	 */
	public static String getFileExtension(String fileName, boolean keepExtensionCase) {
		if (!StringUtils.isEmpty(fileName)) {
			int index = fileName.lastIndexOf(".");

			if (index > -1 && index + 1 < fileName.length()) {
				String ext = fileName.substring(index + 1);
				return keepExtensionCase ? ext : ext.toLowerCase();
			}
		}
		return null;
	}


	/**
	 * Returns the file name without the extension for the given fileName.
	 * <p>
	 * Returns the entire fileName if no extension is found
	 */
	public static String getFileNameWithoutExtension(String fileName) {
		if (!StringUtils.isEmpty(fileName)) {
			int index = fileName.lastIndexOf(".");

			if (index != -1) {
				return fileName.substring(0, index);
			}
		}
		return fileName;
	}


	/**
	 * Appends the given string to the file name before the extension and returns the result.
	 * <p>
	 * Example: Given the file name "data.csv" and the string to append of "_20150310", the result returned
	 * would be "data_20150310.csv"
	 */
	public static String appendToFileName(String strToAppend, String fileName) {
		String ext = FileUtils.getFileExtension(fileName, true);
		if (ext == null) {
			ext = "";
		}

		return getFileNameWithoutExtension(fileName) + strToAppend + "." + ext;
	}


	/**
	 * Returns the path without the file name.
	 */
	public static String getFilePath(String fileName) {
		if (!StringUtils.isEmpty(fileName)) {
			int index = fileName.lastIndexOf("\\");
			if (index == -1) {
				index = fileName.lastIndexOf("/");
			}

			if (index != -1) {
				return fileName.substring(0, index + 1);
			}
		}
		return "";
	}


	/**
	 * Returns the full path relative to the base path
	 */
	public static String getRelativePath(String fullPath, String basePath) {
		if (StringUtils.isEmpty(fullPath) && StringUtils.isEmpty(basePath)) {
			return "";
		}

		return StringUtils.substringAfterLast(normalizeFilePathSeparators(fullPath), normalizeFilePathSeparators(basePath));
	}


	/**
	 * Converts the path separators in the path to the separators used
	 * by the file system running your application
	 */
	public static String normalizeFilePathSeparators(String path) {
		if (StringUtils.isEmpty(path)) {
			return "";
		}

		path = path.replaceAll("(?<!\\\\)\\\\(?!\\\\)|/", Matcher.quoteReplacement(File.separator));
		return path;
	}


	/**
	 * Returns the file name without the path.
	 * <p>
	 * Returns the entire fileName if no path is found
	 */
	public static String getFileName(String fileName) {
		if (!StringUtils.isEmpty(fileName)) {
			return new File(fileName).getName();
		}
		return fileName;
	}


	/**
	 * Combines the two paths
	 */
	public static String combinePath(String path1, String path2) {
		path1 = StringUtils.isEmpty(path1) ? "" : normalizeFilePathSeparators(path1);
		path2 = StringUtils.isEmpty(path2) ? "" : normalizeFilePathSeparators(path2);
		if (StringUtils.isEmpty(path1) && StringUtils.isEmpty(path2)) {
			return "";
		}
		if (!path1.endsWith(File.separator)) {
			path1 += File.separator;
		}
		if (path2.startsWith(File.separator)) {
			path2 = path2.substring(1);
		}
		String result = path1 + path2;
		return normalizeFilePathSeparators(result);
	}


	/**
	 * Combines the list of paths. Must supply at least two 'path' parameters.
	 */
	public static String combinePaths(String... path) {
		if (path.length < 2) {
			throw new ValidationException("Must supply at at least two paths");
		}

		String pathResult = path[0];
		for (int i = 1; i < path.length; i++) {
			pathResult = combinePath(pathResult, path[i]);
		}

		return pathResult;
	}


	public static boolean isRemote(String filePath) {
		if (StringUtils.isEmpty(filePath)) {
			return false;
		}
		return filePath.matches(REMOTE_FILE_REGEX);
	}
	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public static boolean fileExists(String fileName) {
		return FileContainerFactory.getFileContainer(fileName).exists();
	}


	public static boolean fileExists(FilePath file) {
		return FileContainerFactory.getFileContainer(file).exists();
	}


	/**
	 * Moves a file. Does not create new directories or overwrite the toFile if it already exists.
	 */
	public static void moveFile(File file, String toPath) throws IOException {
		moveFile(file, new File(toPath));
	}


	/**
	 * Moves a file. Creates directories if they are missing. Does not overwrite the toFile if it already exists.
	 */
	public static void moveFileCreatePath(File file, String toPath) throws IOException {
		moveFileCreatePath(file, new File(toPath));
	}


	/**
	 * Moves a file. Does not create directories if they are missing. Overwrites the toFile if it exists.
	 */
	public static void moveFileOverwrite(File file, String toPath) throws IOException {
		moveFileOverwrite(file, new File(toPath));
	}


	/**
	 * Moves a file. Creates directories if they are missing. Overwrites the toFile if it exists.
	 */
	public static void moveFileCreatePathAndOverwrite(File file, String toPath) throws IOException {
		moveFileCreatePathAndOverwrite(file, new File(toPath));
	}


	/**
	 * Moves a file. Does not create new directories or overwrite the toFile if it already exists.
	 */
	public static void moveFile(File file, File toFile) throws IOException {
		moveFile(file, toFile, false, false);
	}


	/**
	 * Moves a file. Does not create new directories or overwrite the toFile if it already exists.
	 */
	public static void moveFile(FilePath file, FilePath toFile) throws IOException {
		moveFile(file, toFile, false, false);
	}


	/**
	 * Moves a file. Does not create new directories or overwrite the toFile if it already exists.
	 */
	public static void moveFile(FileContainer file, FileContainer toFile) throws IOException {
		moveFile(file, toFile, false, false);
	}


	/**
	 * Moves a file. Creates directories if they are missing. Does not overwrite the toFile if it already exists.
	 */
	public static void moveFileCreatePath(File file, File toFile) throws IOException {
		moveFile(file, toFile, true, false);
	}


	/**
	 * Moves a file. Creates directories if they are missing. Does not overwrite the toFile if it already exists.
	 */
	public static void moveFileCreatePath(FileContainer file, FileContainer toFile) throws IOException {
		moveFile(file, toFile, true, false);
	}


	/**
	 * Moves a file. Does not create directories if they are missing. Overwrites the toFile if it exists.
	 */
	public static void moveFileOverwrite(FileContainer file, FileContainer toFile) throws IOException {
		moveFile(file, toFile, false, true);
	}


	/**
	 * Moves a file. Does not create directories if they are missing. Overwrites the toFile if it exists.
	 */
	public static void moveFileOverwrite(File file, File toFile) throws IOException {
		moveFile(file, toFile, false, true);
	}


	/**
	 * Moves a file. Creates directories if they are missing. Overwrites the toFile if it exists.
	 */
	public static void moveFileCreatePathAndOverwrite(File file, File toFile) throws IOException {
		moveFile(file, toFile, true, true);
	}


	/**
	 * Moves a file. Does not create new directories or overwrite the toFile if it already exists.
	 */
	public static void moveFile(FilePath source, FilePath target, boolean createDirectories, boolean overrideExisting) throws IOException {
		try {
			moveFile(FileContainerFactory.getFileContainer(source), FileContainerFactory.getFileContainer(target), createDirectories, overrideExisting);
		}
		catch (IOException e) {
			throw new IOException("Failed to move [" + source + "] to [" + target + "].", e);
		}
	}


	private static void moveFile(File file, File toFile, boolean createDirectories, boolean overrideExisting) throws IOException {
		moveFile(FileContainerFactory.getFileContainer(file), FileContainerFactory.getFileContainer(toFile), createDirectories, overrideExisting);
	}


	private static void moveFile(FileContainer fromFile, FileContainer toFile, boolean createDirectories, boolean overrideExisting) throws IOException {
		if (toFile.isDirectory()) {
			toFile = FileContainerFactory.getFileContainer(toFile.getPath() + File.separator + fromFile.getName());
		}

		if (toFile.exists()) {
			if (overrideExisting) {
				delete(toFile);
			}
			else {
				throw new RuntimeException("Failed to move file [" + toFile.getPath() + "] to [" + toFile.getPath() + "] because the file already exists in the target location.");
			}
		}

		if (createDirectories) {
			FileContainer parentFile = FileContainerFactory.getFileContainer(toFile.getParent());
			if (!parentFile.exists()) {
				parentFile.makeDirs();
			}
		}


		// use the rename with a copy delete fall back if the objects are of the same type
		if (fromFile.getClass().equals(toFile.getClass())) {
			boolean success = fromFile.renameToFile(toFile);
			//If for some reason the move operation above was not successful, then fallback to copying
			if (success) {
				return;
			}
		}

		boolean success = false;
		try {
			transferFromInputToOutput(fromFile.getInputStream(), toFile.getOutputStream());
			success = true;
		}
		catch (Exception e) {
			//Do nothing, success is already defaulted to false
		}
		finally {
			//Only attempt to delete the file if the transfer was successful
			if (success) {
				delete(fromFile);
			}
		}
	}


	/**
	 * Copies the file to the specified path. Will not create directory if one does not exist.
	 */
	public static void copyFile(File file, String toPath) throws IOException {
		copyFile(file, new File(toPath));
	}


	/**
	 * Copies file to toFile. Creates missing directories. Does not overwrite existing toFile.
	 */
	public static void copyFileCreatePath(File file, String toPath) throws IOException {
		copyFileCreatePath(file, new File(toPath));
	}


	/**
	 * Copies file to toFile. Overwrites existing toFile if it exists. Does not create missing directories.
	 */
	public static void copyFileOverwrite(File file, String toPath) throws IOException {
		copyFileOverwrite(file, new File(toPath));
	}


	/**
	 * Copies file to toFile. Creates missing directories and overwrites existing toFile if it exists.
	 */
	public static void copyFileCreatePathAndOverwrite(File file, String toPath) throws IOException {
		copyFileCreatePathAndOverwrite(file, new File(toPath));
	}


	/**
	 * Copies the file to the toFile. Does not create missing directories or overwrite existing files.
	 */
	public static void copyFile(File file, File toFile) throws IOException {
		copyFile(file, toFile, false, false);
	}


	/**
	 * Copies the file to the toFile. Does not create missing directories or overwrite existing files.
	 */
	public static void copyFile(FilePath file, FilePath toFile) throws IOException {
		copyFile(FileContainerFactory.getFileContainer(file), FileContainerFactory.getFileContainer(toFile), false, false);
	}


	/**
	 * Copies the file to the toFile. Does not create missing directories or overwrite existing files.
	 */
	public static void copyFile(FileContainer file, FileContainer toFile) throws IOException {
		copyFile(file, toFile, false, false);
	}


	/**
	 * Copies file to toFile. Creates missing directories. Does not overwrite existing toFile.
	 */
	public static void copyFileCreatePath(File file, File toFile) throws IOException {
		copyFile(file, toFile, true, false);
	}


	/**
	 * Copies file to toFile. Creates missing directories. Does not overwrite existing toFile.
	 */
	public static void copyFileCreatePath(FilePath file, FilePath toFile) throws IOException {
		copyFile(FileContainerFactory.getFileContainer(file), FileContainerFactory.getFileContainer(toFile), true, false);
	}


	/**
	 * Copies file to toFile. Creates missing directories. Does not overwrite existing toFile.
	 */
	public static void copyFileCreatePath(FileContainer file, FileContainer toFile) throws IOException {
		copyFile(file, toFile, true, false);
	}


	/**
	 * Copies file to toFile. Overwrites existing toFile if it exists. Does not create missing directories.
	 */
	public static void copyFileOverwrite(File file, File toFile) throws IOException {
		copyFile(file, toFile, false, true);
	}


	/**
	 * Copies file to toFile. Overwrites existing toFile if it exists. Does not create missing directories.
	 */
	public static void copyFileOverwrite(FilePath file, FilePath toFile) throws IOException {
		copyFile(FileContainerFactory.getFileContainer(file), FileContainerFactory.getFileContainer(toFile), false, true);
	}


	/**
	 * Copies file to toFile. Overwrites existing toFile if it exists. Does not create missing directories.
	 */
	public static void copyFileOverwrite(FileContainer file, FileContainer toFile) throws IOException {
		copyFile(file, toFile, false, true);
	}


	/**
	 * Copies file to toFile. Creates missing directories and overwrites existing toFile if it exists.
	 */
	public static void copyFileCreatePathAndOverwrite(File file, File toFile) throws IOException {
		copyFile(file, toFile, true, true);
	}


	/**
	 * Copies file to toFile. Creates missing directories and overwrites existing toFile if it exists.
	 */
	public static void copyFileCreatePathAndOverwrite(FilePath file, FilePath toFile) throws IOException {
		copyFile(FileContainerFactory.getFileContainer(file), FileContainerFactory.getFileContainer(toFile), true, true);
	}


	/**
	 * Copies file to toFile. Creates missing directories and overwrites existing toFile if it exists.
	 */
	public static void copyFileCreatePathAndOverwrite(FileContainer file, FileContainer toFile) throws IOException {
		copyFile(file, toFile, true, true);
	}


	private static void copyFile(File file, File toFile, boolean createDirectories, boolean overrideExisting) throws IOException {
		copyFile(FileContainerFactory.getFileContainer(file), FileContainerFactory.getFileContainer(toFile), createDirectories, overrideExisting);
	}


	private static void copyFile(FileContainer fromFile, FileContainer toFile, boolean createDirectories, boolean overrideExisting) throws IOException {
		if (fromFile.isDirectory()) {
			throw new IllegalArgumentException("Copy: cannot copy directory '" + fromFile.getAbsolutePath() + "' to a file.");
		}
		if (toFile.isDirectory()) {
			throw new IllegalArgumentException("Copy: cannot copy file '" + fromFile.getAbsolutePath() + "' to a directory.");
		}
		if (toFile.exists()) {
			if (overrideExisting) {
				delete(toFile);
			}
			else {
				throw new RuntimeException("Failed to copy file [" + fromFile.getAbsolutePath() + "] to [" + toFile.getAbsolutePath() + "] because the file already exists in the target location.");
			}
		}
		if (createDirectories) {
			FileContainer parentFile = FileContainerFactory.getFileContainer(toFile.getParent());
			if (!parentFile.exists()) {
				parentFile.makeDirs();
			}
		}
		transferFromInputToOutput(fromFile.getInputStream(), toFile.getOutputStream());
	}


	/**
	 * Copies the contents from the source {@link InputStream} to the destination {@link Path}. The
	 * provided source stream will be closed following the copy.
	 * <p>
	 * Note: Will only work on files in mounted/mapped drives
	 */
	public static void copyToPath(InputStream source, FilePath destination) throws IOException {
		if (source == null) {
			throw new IllegalArgumentException("Copy: cannot copy from a null source.");
		}
		if (destination == null) {
			throw new IllegalArgumentException("Copy: cannot copy to an undefined path destination.");
		}
		try {
			FileContainer destinationContainer = FileContainerFactory.getFileContainer(destination.getPath());
			transferFromInputToOutput(source, destinationContainer.getOutputStream());
		}
		catch (IOException e) {
			throw new IOException("Failed to copy stream content to [" + destination + "].", e);
		}
		finally {
			close(source);
		}
	}


	/**
	 * Deletes a specified file
	 */
	public static void delete(FileContainer fileContainer) {
		if (fileContainer != null) {
			try {
				// Make sure the file or directory exists and isn't write protected
				if (!fileContainer.exists()) {
					throw new IllegalArgumentException("Delete: no such file or directory: " + fileContainer.getAbsolutePath());
				}

				if (!fileContainer.canWrite()) {
					throw new IllegalArgumentException("Delete: write protected: " + fileContainer.getAbsolutePath());
				}

				// If it is a directory, make sure it is empty
				if (fileContainer.isDirectory()) {
					String[] fileContainerList = fileContainer.list();
					if (!ArrayUtils.isEmpty(fileContainerList)) {
						throw new IllegalArgumentException("Delete: directory not empty: " + fileContainer.getAbsolutePath());
					}
				}

				// Attempt to delete it

				fileContainer.deleteFile();
			}
			catch (Throwable e) {
				throw new RuntimeException("Could not delete the file: " + fileContainer.getName(), e);
			}
		}
	}


	public static void delete(File file) {
		delete(FileContainerFactory.getFileContainer(file));
	}


	public static void delete(FilePath file) {
		delete(FileContainerFactory.getFileContainer(file));
	}


	/**
	 * recursively delete directory including all files and sub-directories.
	 *
	 * @param fileContainer
	 */
	public static void deleteDirectory(FileContainer fileContainer) {
		try {
			if (fileContainer.isDirectory()) {
				List<FileContainer> fileList = FileUtils.listFiles(fileContainer);
				for (FileContainer subFileContainer : CollectionUtils.getIterable(fileList)) {
					deleteDirectory(subFileContainer);
				}
			}
			delete(fileContainer);
		}
		catch (Throwable e) {
			throw new RuntimeException("Could not delete the file/directory: " + fileContainer.getName(), e);
		}
	}


	public static void deleteDirectory(File file) {
		deleteDirectory(FileContainerFactory.getFileContainer(file));
	}


	public static void deleteDirectory(FilePath file) {
		deleteDirectory(FileContainerFactory.getFileContainer(file));
	}


	/**
	 * Converts an MultipartFile to an actual File (a Temp File)
	 */
	public static File convertMultipartFileToFile(MultipartFile multipartFile) {
		return new MultipartFileToFileConverter().convert(multipartFile);
	}


	/**
	 * Converts a File to a MultipartFile
	 */
	public static MultipartFile convertFileToMultipartFile(String originalFileName, File file) {
		return new MultipartFileImpl(originalFileName, file);
	}


	/**
	 * Converts a File to a MultipartFile
	 */
	public static MultipartFile convertFileToMultipartFile(String originalFileName, FileContainer file) {
		try {
			return new MultipartFileImpl(originalFileName, file);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not convert file container to MultipartFile", e);
		}
	}


	public static InputStream getInputStream(FilePath file) throws FileNotFoundException {
		return getInputStream(file.getPath());
	}


	/**
	 * Returns InputStream object for the specified file using this class' class loader.
	 */
	public static InputStream getInputStream(String file) throws FileNotFoundException {
		try {
			return FileContainerFactory.getFileContainer(file).getInputStream();
		}
		catch (IOException e) {
			InputStream stream = getDefaultClassLoader().getResourceAsStream(file);
			if (stream == null) {
				throw new FileNotFoundException("Cannot find file: " + file);
			}
			return stream;
		}
	}


	/**
	 * Converts an InputStream to an actual File (a Temp File)
	 */
	public static File convertInputStreamToFile(String filePrefix, String fileSuffix, InputStream is) {
		File out;
		try {
			out = File.createTempFile(filePrefix, "." + fileSuffix);
		}
		catch (Exception e) {
			throw new RuntimeException("Error converting Input Stream to File ", e);
		}
		convertInputStreamToFile(out, is);
		out.deleteOnExit();
		return out;
	}


	/**
	 * Converts an InputStream to an actual File.
	 */
	public static File convertInputStreamToFile(String fileNameWithPath, InputStream is) {
		File out = new File(fileNameWithPath);
		convertInputStreamToFile(out, is);
		return out;
	}


	public static void convertInputStreamToFile(File file, InputStream is) {
		BufferedOutputStream tbo = null;

		try {
			FileOutputStream targetFileOut = new FileOutputStream(file);

			tbo = new BufferedOutputStream(targetFileOut);

			byte[] buf = new byte[8192]; //copy in chunks
			for (int c = is.read(buf); c != -1; c = is.read(buf)) {
				tbo.write(buf, 0, c);
				tbo.flush();
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Error converting Input Stream to File ", e);
		}
		finally {
			close(is);
			close(tbo);
		}
	}


	public static byte[] convertInputStreamToBytes(InputStream inputStream) throws IOException {
		ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
		try {
			int numberRead;
			byte[] data = new byte[1024];
			while ((numberRead = inputStream.read(data, 0, data.length)) != -1) {
				outputStream.write(data, 0, numberRead);
			}
			outputStream.flush();
			return outputStream.toByteArray();
		}
		finally {
			close(inputStream);
			close(outputStream);
		}
	}


	/**
	 * Close the specified Closeable object if it's not null;
	 */
	public static void close(Closeable closeable) {
		if (closeable != null) {
			try {
				closeable.close();
			}
			catch (IOException e) {
				LogUtils.warn(FileUtils.class, "Error closing " + closeable.getClass(), e);
			}
		}
	}


	private static ClassLoader getDefaultClassLoader() {
		ClassLoader classLoader = null;
		try {
			classLoader = Thread.currentThread().getContextClassLoader();
		}
		catch (Throwable e) {
			// Cannot access thread context ClassLoader - falling back to system class loader...
		}
		if (classLoader == null) {
			// No thread context class loader -> use class loader of this class.
			classLoader = FileUtils.class.getClassLoader();
		}
		return classLoader;
	}


	public static String readFileToString(File file) {
		return readFileToString(file.getAbsolutePath());
	}


	public static String readFileToString(String path) {
		try {
			byte[] encoded = FileContainerFactory.getFileContainer(path).readBytes();
			return new String(encoded, StandardCharsets.UTF_8);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not read file [" + path + "]", e);
		}
	}


	public static byte[] readFileBytes(File file) throws IOException {
		byte[] result = new byte[(int) file.length()];
		try (FileInputStream inputStream = new FileInputStream(file)) {
			if (inputStream.read(result) != result.length) {
				throw new RuntimeException("Invalid number of bytes read for file: " + file.getName());
			}
		}
		return result;
	}


	public static byte[] readFileBytes(FilePath file) throws IOException {
		FileContainer fileContainer = FileContainerFactory.getFileContainer(file);
		byte[] result = new byte[(int) fileContainer.length()];
		try (FileInputStream inputStream = new FileInputStream(file.getPath())) {
			if (inputStream.read(result) != result.length) {
				throw new RuntimeException("Invalid number of bytes read for file: " + fileContainer.getName());
			}
		}
		return result;
	}


	/**
	 * Writes the given text to the file specified by the filepath
	 */
	public static void writeStringToFile(String filePath, String text) {
		writeStringToFile(FileContainerFactory.getFileContainer(filePath), text);
	}


	/**
	 * Writes the given text to the specified file
	 */
	public static void writeStringToFile(File file, String text) {
		writeStringToFile(FileContainerFactory.getFileContainer(file), text);
	}


	/**
	 * Writes the given text to the specified file container
	 */
	public static void writeStringToFile(FileContainer file, String text) {
		if (!file.exists()) {
			createDirectoryForFile(file);
		}
		try {
			file.writeBytes(text.getBytes(StandardCharsets.UTF_8));
		}
		catch (IOException ioe) {
			throw new RuntimeException("Exception occurred while writing file [" + file.getAbsolutePath() + "].", ioe);
		}
	}


	/**
	 * Creates the specified {@code path} if it does not already exists.
	 *
	 * @param path to be created
	 * @return {@code true} if {@code path} created successfully, otherwise {@code false}. See {@link File#mkdirs()}.
	 */
	public static boolean createDirectory(String path) {
		FileContainer file = FileContainerFactory.getFileContainer(path);
		if (!file.exists()) {
			try {
				return file.makeDirs();
			}
			catch (IOException e) {
				throw new RuntimeException("Could not make directory for path: " + path, e);
			}
		}
		return false;
	}


	public static boolean createDirectoryForFile(File file) {
		return createDirectoryForFile(FileContainerFactory.getFileContainer(file));
	}


	public static boolean createDirectoryForFile(FileContainer file) {
		try {
			return FileContainerFactory.getFileContainer(file.getParent()).makeDirs();
		}
		catch (IOException e) {
			throw new RuntimeException("Error creating directory for file: " + file.getAbsolutePath(), e);
		}
	}


	/**
	 * Determines whether a directory is empty or not
	 */
	public static boolean directoryContainsFiles(File file) {
		FileContainer utilFile = FileContainerFactory.getFileContainer(file);
		try {
			if (utilFile.isDirectory()) {
				String[] childFileContainerList = utilFile.list();
				return !ArrayUtils.isEmpty(childFileContainerList);
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Error occurred while checking the directory for files: " + file.getAbsolutePath(), e);
		}
		return false;
	}


	/**
	 * List file names within the directory.
	 */
	public static String[] listFileNames(File file) {
		FileContainer utilFile = FileContainerFactory.getFileContainer(file);
		try {
			if (utilFile.isDirectory()) {
				return utilFile.list();
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Error occurred while listing files: " + file.getAbsolutePath(), e);
		}
		return new String[0];
	}


	/**
	 * List file containers within the directory.
	 */
	public static List<FileContainer> listFiles(File file) {
		return listFiles(FileContainerFactory.getFileContainer(file));
	}


	/**
	 * List file containers within the directory.
	 */
	public static List<FileContainer> listFiles(FileContainer fileContainer) {
		try {
			String absolutePath = fileContainer.getAbsolutePath();
			if (fileContainer.isDirectory()) {
				String[] fileNames = fileContainer.list();
				return CollectionUtils.createList(fileNames).stream()
						.map(n -> FileUtils.combinePath(absolutePath, n))
						.map(FileContainerFactory::getFileContainer)
						.collect(Collectors.toList());
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Error occurred while listing files: " + fileContainer.getAbsolutePath(), e);
		}
		return Collections.emptyList();
	}

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	public static File getPDFCopyWithCorrectTitle(File file, String fileName, String type, boolean tempFile) {
		// Only Applies to PDF files
		if (!"pdf".equalsIgnoreCase(type)) {
			return file;
		}
		FileContainer fileWrapper = FileContainerFactory.getFileContainer(file);

		BufferedInputStream inputStream = null;
		PdfCopy copy = null;
		File out;
		try {
			inputStream = new BufferedInputStream(fileWrapper.getInputStream());
			PdfReader reader = new PdfReader(inputStream);
			out = File.createTempFile(fileName, "-Copy.pdf");

			Document doc = new Document(reader.getPageSizeWithRotation(1));
			copy = new PdfCopy(doc, new BufferedOutputStream(new FileOutputStream(out)));
			doc.addTitle(FileUtils.getFileNameWithoutExtension(fileName));
			doc.open();

			int pageCount = reader.getNumberOfPages();
			for (int i = 1; i <= pageCount; i++) {
				PdfImportedPage page = copy.getImportedPage(reader, i);
				copy.addPage(page);
			}

			// Return the New PDF as the File to view
			return out;
		}
		catch (Throwable e) {
			throw new RuntimeException("Error copying PDF file: " + fileName + " of type: " + type, e);
		}
		finally {
			if (inputStream != null) {
				try {
					inputStream.close();
				}
				catch (IOException e) {
					LogUtils.warn(FileUtils.class, "Failed to close input stream when copying pdf [" + file.getName() + "].", e);
				}
			}
			if (copy != null) {
				copy.close();
			}
			// Delete the original PDF - if it was a temp file
			if (tempFile) {
				delete(fileWrapper);
			}
		}
	}


	public static File concatenatePDFs(String outputFileName, String outputExtension, List<File> pdfFiles, boolean appendPageNumber, boolean deleteSourceFiles) {
		File out = null;

		Document document = null;
		PdfReader reader = null;
		PdfSmartCopy pdfSmartCopy = null;

		boolean deleteOnError = false;
		try {
			reader = new PdfReader(new BufferedInputStream(new FileInputStream(pdfFiles.get(0))));
			document = new Document(reader.getPageSizeWithRotation(1));
			// create a writer that listens to the document
			out = File.createTempFile(outputFileName, outputExtension);
			out.deleteOnExit();
			pdfSmartCopy = new PdfSmartCopy(document, new BufferedOutputStream(new FileOutputStream(out)));
			// we open the document
			document.open();
			// create the page number font
			//Font search code:
			//http://developers.itextpdf.com/examples/itext-action-second-edition/chapter-16#616-listusedfonts.java
			Font pageNumberFont = new Font(BaseFont.createFont(BaseFont.HELVETICA, BaseFont.CP1252, BaseFont.NOT_EMBEDDED), PDF_PAGE_NUMBER_FONT_SIZE);

			// add the content
			int pageCounter = 1;
			for (int index = 0; index < pdfFiles.size(); index++) {
				try {
					int n = reader.getNumberOfPages();
					for (int i = 1; i <= n; i++) {
						String pageText = String.format("Page %d", pageCounter);
						PdfImportedPage page = pdfSmartCopy.getImportedPage(reader, i);
						if (appendPageNumber) {
							PdfCopy.PageStamp pageStamp = pdfSmartCopy.createPageStamp(page);
							ColumnText.showTextAligned(pageStamp.getUnderContent(), Element.ALIGN_RIGHT, new Phrase(pageText, pageNumberFont),
									reader.getPageSizeWithRotation(i).getRight(document.rightMargin()) - PDF_PAGE_NUMBER_MARGIN_OFFSET, 15, 0);
							pageStamp.alterContents();
						}
						pdfSmartCopy.addPage(page);
						pageCounter++;
					}
				}
				finally {
					closeReaderAndThrowError(reader, pdfSmartCopy);
				}

				// we create the next reader
				if (index + 1 < pdfFiles.size()) {
					reader = new PdfReader(new BufferedInputStream(new FileInputStream(pdfFiles.get(index + 1))));
				}
			}
		}
		catch (Exception e) {
			deleteOnError = true;
			throw new RuntimeException("Failed to concatenate pdf files.", e);
		}
		finally {
			// close any open streams
			closeReaderAndSuppressError(reader, pdfSmartCopy); // just in case it failed to close above
			closeDocListenerWithNoError(document, "Failed to close pdf document for pdf concatenation.");
			closeDocListenerWithNoError(pdfSmartCopy, "Failed to close pdf writer for pdf concatenation.");

			// clean up the source and temp files
			if (deleteSourceFiles) {
				for (File pdfFile : pdfFiles) {
					if ((pdfFile != null) && pdfFile.exists()) {
						delete(pdfFile);
					}
				}
			}

			if (deleteOnError && (out != null) && out.exists()) {
				delete(out);
			}
		}
		return out;
	}


	private static void closeDocListenerWithNoError(DocListener docListener, String errorMessage) {
		if (docListener != null) {
			try {
				docListener.close();
			}
			catch (Exception e) {
				LogUtils.error(FileUtils.class, errorMessage, e);
			}
		}
	}


	private static void closeReaderAndSuppressError(PdfReader reader, PdfSmartCopy pdfSmartCopy) {
		closeReaderWithNoError(reader, pdfSmartCopy, false);
	}


	private static void closeReaderAndThrowError(PdfReader reader, PdfSmartCopy pdfSmartCopy) {
		closeReaderWithNoError(reader, pdfSmartCopy, true);
	}


	private static void closeReaderWithNoError(PdfReader reader, PdfSmartCopy pdfSmartCopy, boolean throwErrorWhenReaderFailsToClose) {
		try {
			if (reader != null) {
				if (pdfSmartCopy != null) {
					freePdfReader(reader, pdfSmartCopy);
				}
				reader.close();
			}
		}
		catch (Exception e) {
			if (throwErrorWhenReaderFailsToClose) {
				throw e;
			}
			else {
				LogUtils.error(FileUtils.class, "Failed to close pdf reader for pdf concatenation.", e);
			}
		}
	}


	private static void freePdfReader(PdfReader reader, PdfSmartCopy pdfSmartCopy) {
		try {
			pdfSmartCopy.freeReader(reader);
		}
		catch (Exception e) {
			LogUtils.error(FileUtils.class, "Failed to free the pdf reader from the smart copy.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a map containing information for the files contained within the folder. The map maps a {@link FilePath} to its corresponding {@link
	 * BasicFileAttributes}.  All returned paths are absolute.
	 * <p>
	 * Note: Works for Internal Files only
	 *
	 * @param filePath             the FilePath that represents the folder location
	 * @param recursive            whether or not to process the entire directory tree
	 * @param folderNamesToProcess a list of folder names to process. The names must be lower case. If all folders should be processed, then this parameter
	 *                             should be <code>null</code>.
	 */
	public static Map<FilePath, BasicFileAttributes> getFilesFromDirectory(FilePath filePath, boolean recursive, final Collection<String> folderNamesToProcess) throws IOException {

		ValidationUtils.assertFalse(filePath.isRemote(), "Remote files are not supported!");

		Map<FilePath, BasicFileAttributes> files = new HashMap<>();

		String newPath = normalizeFilePathSeparators(filePath.getPath());

		Path path = Paths.get(newPath).normalize().toAbsolutePath();

		if (recursive) {
			FileVisitor<Path> visitor = new SimpleFileVisitor<Path>() {

				@Override
				public FileVisitResult preVisitDirectory(Path dir, @SuppressWarnings("unused") BasicFileAttributes fileAttributes) {
					String folderName = dir.getFileName() == null ? null : dir.getFileName().toString().toLowerCase();
					if (folderNamesToProcess != null && folderName != null && !CollectionUtils.contains(folderNamesToProcess, folderName)) {
						return FileVisitResult.SKIP_SUBTREE;
					}

					return FileVisitResult.CONTINUE;
				}


				@Override
				public FileVisitResult visitFile(Path file, BasicFileAttributes fileAttributes) {
					if (!fileAttributes.isDirectory()) {
						files.put(FilePath.forPath(file.toString()), fileAttributes);
					}

					return FileVisitResult.CONTINUE;
				}
			};

			Files.walkFileTree(path, visitor);
		}
		else {
			try (DirectoryStream<Path> ds = Files.newDirectoryStream(path)) {
				for (Path p : ds) {
					BasicFileAttributes attributes = Files.readAttributes(p, BasicFileAttributes.class);
					if (!attributes.isDirectory()) {
						files.put(FilePath.forPath(p.toString()), attributes);
					}
				}
			}
		}

		return files;
	}


	/**
	 * Returns a map containing information for the files contained within the folder. The map maps a {@link Path} to its corresponding {@link
	 * BasicFileAttributes}.
	 *
	 * @param filePath  the Path that represents the folder location
	 * @param recursive whether or not to process the entire directory tree
	 */
	public static Map<FilePath, BasicFileAttributes> getFilesFromDirectory(FilePath filePath, boolean recursive) throws IOException {
		return getFilesFromDirectory(filePath, recursive, null);
	}


	public static String getMimeContentType(String fileName) {
		String fileExtension = getFileExtension(fileName).toLowerCase();
		if (CUSTOM_MIME_CONTENT_TYPE_MAP.containsKey(fileExtension)) {
			return CUSTOM_MIME_CONTENT_TYPE_MAP.get(fileExtension);
		}
		return mimeTypeMap.getContentType(fileName);
	}


	public static File getClasspathResourceAsFile(String filePath, Class<?> clazz) {
		URL url = clazz.getResource(filePath);
		try {
			return new File(url.toURI());
		}
		catch (URISyntaxException e) {
			throw new RuntimeException("Problem getting URI", e);
		}
	}


	/**
	 * Transfers the contents of the input stream to the output stream, closing both streams when complete
	 */
	public static void transferFromInputToOutput(InputStream inputStream, OutputStream outputStream) throws IOException {
		try {
			// 16 kb
			final byte[] b = new byte[16 * 1024];
			int read;
			while ((read = inputStream.read(b, 0, b.length)) > 0) {
				outputStream.write(b, 0, read);
			}
		}
		finally {
			close(inputStream);
			close(outputStream);
		}
	}


	/**
	 * Util for getting absolute path of a {@link FilePath}. Converts the FilePath to a {@link FileContainer} and then gets absolute path
	 *
	 * @return String representation of the absolute path
	 */
	public static String getAbsolutePath(FilePath file) {
		return FileContainerFactory.getFileContainer(file).getAbsolutePath();
	}


	public static long getLength(FilePath file) throws IOException {
		return FileContainerFactory.getFileContainer(file.getPath()).length();
	}
}
