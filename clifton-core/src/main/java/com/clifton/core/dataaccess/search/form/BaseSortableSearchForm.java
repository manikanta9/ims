package com.clifton.core.dataaccess.search.form;


import com.clifton.core.dataaccess.search.SearchUtils;


/**
 * The <code>BaseSortableSearchForm</code> class is a search form which provides fields for sorting.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseSearchForm Parent fields}
 * <li>{@link #orderBy}
 * </ul>
 *
 * @author vgomelsky
 * @see SearchUtils#getOrderByFieldList(String)
 */
public abstract class BaseSortableSearchForm extends BaseSearchForm {

	/**
	 * Syntax example: "field1#field2:asc#field3:desc"
	 */
	private String orderBy;

	/**
	 * Set to true to skip default sorting when {@link #orderBy} field is not defined (improves SQL performance)
	 */
	private boolean skipDefaultSorting;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Syntax example: "field1#field2:asc#field3:desc"
	 *
	 * @return the orderBy
	 */
	public String getOrderBy() {
		return this.orderBy;
	}


	/**
	 * Syntax example: "field1#field2:asc#field3:desc"
	 *
	 * @param orderBy the orderBy to set
	 */
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}


	public boolean isSkipDefaultSorting() {
		return this.skipDefaultSorting;
	}


	public void setSkipDefaultSorting(boolean skipDefaultSorting) {
		this.skipDefaultSorting = skipDefaultSorting;
	}
}
