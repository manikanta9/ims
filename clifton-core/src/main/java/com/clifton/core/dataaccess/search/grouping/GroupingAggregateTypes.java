package com.clifton.core.dataaccess.search.grouping;

/**
 * Aggregation Types supported.  Count is just row count.  Sum is summing a specific property.
 * Note: Can add additional options like MIN, MAX, AVG if needed
 */
public enum GroupingAggregateTypes {

	COUNT,
	SUM
}
