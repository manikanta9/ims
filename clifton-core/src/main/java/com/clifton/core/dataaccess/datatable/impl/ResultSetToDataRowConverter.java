package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;

import java.sql.Clob;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;


/**
 * The <code>ResultSetToDataRowConverter</code> class converts current <code>ResultSet</code> row to a corresponding
 * <code>DataRow</code>.
 *
 * @author vgomelsky
 */
public class ResultSetToDataRowConverter implements Converter<ResultSet, DataRow> {

	private final int columnCount;
	private final DataTable dataTable;


	public ResultSetToDataRowConverter(DataTable dataTable) {
		this.dataTable = dataTable;
		this.columnCount = dataTable.getColumnCount();
	}


	@Override
	public DataRow convert(ResultSet resultSet) {
		int index = 0;
		Object[] data = new Object[this.columnCount];
		try {
			while (index < this.columnCount) {
				Object obj = resultSet.getObject(index + 1);

				if (obj != null) {
					// Special Handling for NTEXT - If Clob object, use String value for DataTable
					if (Clob.class.isAssignableFrom(obj.getClass())) {

						obj = resultSet.getString(index + 1);
					}
					// Special handling for Dates that are returned as String length 10
					else if (String.class.isAssignableFrom(obj.getClass()) && ((String) obj).length() == 10) {
						// If DataColumn was already mapped to Date Object - convert it
						DataColumn dc = this.dataTable.getColumnList()[index];
						if (Types.DATE == dc.getDataType()) {
							obj = DateUtils.toDate((String) obj, DateUtils.DATE_FORMAT_SQL);
						}
					}
				}
				data[index] = obj;
				index++;
			}
		}
		catch (SQLException e) {
			throw new RuntimeException("Error retrieving Object at position " + index, e);
		}

		return new DataRowImpl(this.dataTable, data);
	}
}
