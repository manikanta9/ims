package com.clifton.core.dataaccess.search.hibernate.expression;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.engine.spi.TypedValue;

import java.util.Date;


/**
 * The <code>ActiveExpressionForDates</code> can be used to generate the {@link LogicalExpression}
 * for active/inactive based on start/end date properties
 * <p/>
 * For Active this is: (Start Date IS NULL OR "ActiveOnDate" >= Start Date) AND (End Date IS NULL OR "ActiveOnDate" <= End Date)
 * For Inactive: (StartDate > "ActiveOnDate" OR EndDate < "ActiveOnDate")
 * By default uses "startDate" and "endDate" as the property names.  Can also pass in the pathAlias if start/end dates are on a referenced bean.
 * Active On date defaults to "Today" when left blank
 *
 * @author manderson
 */
public class ActiveExpressionForDates implements Criterion {

	private String startDateProperty;
	private String endDateProperty;

	private final boolean active;
	private final Date activeOnStartDate;
	private final Date activeOnEndDate;

	private final LogicalExpression expression;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static ActiveExpressionForDates forActive(boolean active) {
		return new ActiveExpressionForDates(active, null, null, null, null, null);
	}


	public static ActiveExpressionForDates forActiveOnDate(boolean active, Date activeOnDate) {
		return new ActiveExpressionForDates(active, null, null, null, activeOnDate, activeOnDate);
	}


	public static ActiveExpressionForDates forActiveOnDateRange(boolean active, Date activeOnStartDate, Date activeOnEndDate) {
		return new ActiveExpressionForDates(active, null, null, null, activeOnStartDate, activeOnEndDate);
	}


	public static ActiveExpressionForDates forActiveWithAlias(boolean active, String pathAlias) {
		return new ActiveExpressionForDates(active, pathAlias, null, null, null, null);
	}


	public static ActiveExpressionForDates forActiveOnDateWithAliasAndProperties(boolean active, String pathAlias, String startDateProperty, String endDateProperty, Date activeOnDate) {
		return new ActiveExpressionForDates(active, pathAlias, startDateProperty, endDateProperty, activeOnDate, activeOnDate);
	}


	public static ActiveExpressionForDates forActiveOnDateRangeWithAliasAndProperties(boolean active, String pathAlias, String startDateProperty, String endDateProperty, Date activeOnStartDate, Date activeOnEndDate) {
		return new ActiveExpressionForDates(active, pathAlias, startDateProperty, endDateProperty, activeOnStartDate, activeOnEndDate);
	}


	private ActiveExpressionForDates(boolean active, String pathAlias, String startDateProperty, String endDateProperty, Date activeOnStartDate, Date activeOnEndDate) {
		this.active = active;
		this.startDateProperty = StringUtils.coalesce(false, startDateProperty, "startDate");
		this.endDateProperty = StringUtils.coalesce(false, endDateProperty, "endDate");

		if (!StringUtils.isEmpty(pathAlias)) {
			this.startDateProperty = pathAlias + "." + this.startDateProperty;
			this.endDateProperty = pathAlias + "." + this.endDateProperty;
		}

		this.activeOnStartDate = (activeOnStartDate == null ? new Date() : activeOnStartDate);
		this.activeOnEndDate = (activeOnEndDate == null ? new Date() : activeOnEndDate);

		if (this.active) {
			LogicalExpression startDateFilter = Restrictions.or(Restrictions.isNull(this.startDateProperty), Restrictions.le(this.startDateProperty, this.activeOnEndDate));
			LogicalExpression endDateFilter = Restrictions.or(Restrictions.isNull(this.endDateProperty), Restrictions.ge(this.endDateProperty, this.activeOnStartDate));
			this.expression = Restrictions.and(startDateFilter, endDateFilter);
		}
		else {
			SimpleExpression startDateFilter = Restrictions.gt(this.startDateProperty, this.activeOnEndDate);
			SimpleExpression endDateFilter = Restrictions.lt(this.endDateProperty, this.activeOnStartDate);
			this.expression = Restrictions.or(startDateFilter, endDateFilter);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return this.expression.getTypedValues(criteria, criteriaQuery);
	}


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return this.expression.toSqlString(criteria, criteriaQuery);
	}


	@Override
	public String toString() {
		if (DateUtils.isEqualWithoutTime(this.activeOnStartDate, this.activeOnEndDate)) {
			return (this.active ? "Active: " : "Inactive: ") + " on " + DateUtils.fromDateShort(this.activeOnStartDate) + " search against Start Date [" + this.startDateProperty + "] and End Date ["
					+ this.endDateProperty + "]";
		}
		return (this.active ? "Active: " : "Inactive: ") + " during period " + DateUtils.fromDateRange(this.activeOnStartDate, this.activeOnEndDate, true, true) + " search against Start Date [" + this.startDateProperty + "] and End Date ["
				+ this.endDateProperty + "]";
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getters & Setters                               ////////
	////////////////////////////////////////////////////////////////////////////


	public String getStartDateProperty() {
		return this.startDateProperty;
	}


	public String getEndDateProperty() {
		return this.endDateProperty;
	}


	public boolean isActive() {
		return this.active;
	}


	public Date getActiveOnStartDate() {
		return this.activeOnStartDate;
	}


	public Date getActiveOnEndDate() {
		return this.activeOnEndDate;
	}


	public LogicalExpression getExpression() {
		return this.expression;
	}
}
