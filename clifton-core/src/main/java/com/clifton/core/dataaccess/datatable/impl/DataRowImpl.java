package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.converter.Converter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static java.sql.Types.TIMESTAMP;


/**
 * The <code>DataRowImpl</code> class provides Object Array backed implementation of for <code>DataRow</code>.
 *
 * @author vgomelsky
 */
public class DataRowImpl implements DataRow {

	private final DataColumn[] columnList;
	private final Object[] data;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Needed for Jackson deserialization
	 * Because DataTable is special in that it stores data as generic objects and adds type information
	 * we must convert the data when necessary; primarily this applies to the java.sql.TIMESTAMP
	 * dataType in which the serialization of the object results in a String representation that needs to
	 * be converted back into a TimeStamp object.
	 */
	@JsonCreator
	public DataRowImpl(@JsonProperty("columnList") DataColumn[] columnList, @JsonProperty("data") Object[] data) {
		if (ArrayUtils.isEmpty(columnList)) {
			this.columnList = new DataColumn[0];
			this.data = new Object[0];
		}
		else {
			this.columnList = columnList;
			Object[] convertedData = new Object[data.length];
			for (int i = 0; i < data.length; i++) {
				convertedData[i] = convertData(data[i], columnList[i].getDataType());
			}
			this.data = convertedData;
		}
	}


	/**
	 * Used to convert data as necessary to the project Object.
	 */
	private Object convertData(Object data, int dataType) {
		switch (dataType) {
			case TIMESTAMP:
				return Timestamp.valueOf(String.valueOf(data));
			default:
				return data;
		}
	}


	public DataRowImpl(DataTable dataTable, Object[] data) {
		this.columnList = dataTable.getColumnList();
		this.data = data;
	}


	@Override
	public String toString() {
		return '{' + ArrayUtils.toString(this.data) + '}';
	}


	/**
	 * Returns a copy (clone) of the data array representing the row.
	 */
	public DataColumn[] getColumnList() {
		return this.columnList;
	}


	/**
	 * Returns a copy (clone) of the data array representing the row.
	 */
	public Object[] getData() {
		return (this.data == null) ? null : this.data.clone();
	}


	@Override
	public Object getValue(int columnIndex) {
		if (this.data.length > columnIndex) {
			Object value = this.data[columnIndex];
			DataColumn column = this.columnList[columnIndex];
			if (column != null) {
				Converter<Object, Object> converter = column.getColumnValueConverter();
				if (converter != null) {
					return converter.convert(value);
				}
			}
			return value;
		}
		return null;
	}


	@Override
	public Object getValue(String columnName) {
		DataColumn[] columns = this.columnList;
		for (int i = 0; i < columns.length; i++) {
			if (columnName.equals(columns[i].getColumnName())) {
				return getValue(i);
			}
		}
		return null;
	}


	@Override
	public Object getValue(DataColumn column) {
		return getValue(column.getColumnName());
	}


	@Override
	@JsonIgnore
	public Map<String, Object> getRowValueMap() {
		return getRowValueMap(null, false);
	}


	@Override
	@JsonIgnore
	public Map<String, Object> getRowValueMap(boolean includeNullValues) {
		return getRowValueMap(null, includeNullValues);
	}


	@Override
	@JsonIgnore
	public Map<String, Object> getRowValueMap(List<String> columnNames, boolean includeNullValues) {
		Map<String, Object> result = new LinkedHashMap<>();
		DataColumn[] columns = this.columnList;
		for (int i = 0; i < columns.length; i++) {
			String columnName = columns[i].getColumnName();
			if (columnNames == null || columnNames.contains(columnName)) {
				Object value = getValue(i);
				if (value != null || includeNullValues) {
					result.put(columnName, value);
				}
			}
		}
		return result;
	}


	@Override
	@JsonIgnore
	public List<Object> getRowValueList() {
		return getRowValueList(null, false);
	}


	@Override
	@JsonIgnore
	public List<Object> getRowValueList(boolean includeNullValues) {
		return getRowValueList(null, includeNullValues);
	}


	@Override
	@JsonIgnore
	public List<Object> getRowValueList(List<String> columnNames, boolean includeNullValues) {
		List<Object> result = new ArrayList<>();
		DataColumn[] columns = this.columnList;
		for (int i = 0; i < columns.length; i++) {
			String columnName = columns[i].getColumnName();
			if (columnNames == null || columnNames.contains(columnName)) {
				Object value = getValue(i);
				if (value != null || includeNullValues) {
					result.add(value);
				}
			}
		}
		return result;
	}
}
