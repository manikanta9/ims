package com.clifton.core.dataaccess.migrate.schema.search;

import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
@SearchForm(hasOrmDtoClass = false)
public class TableSearchForm extends BaseAuditableEntitySearchForm {

	private String searchPattern;

	private String dataSource;

	private OperationTypes operationType;

	private String name;

	private String auditType;

	private String cacheType;

	private String tableType;

	private String daoName;

	private String dtoClass;

	private String dtoProxy;

	private String migrationServiceBeanName;

	private String migrationServiceMethodName;

	private Boolean uploadAllowed;

	private String uploadServiceBeanName;

	private String uploadServiceMethodName;

	private String discriminatorFormula;

	private String discriminatorValue;

	private Boolean excludeFromSql;

	private Boolean version;

	/**
	 * Table has an "rv" column
	 */
	private Boolean versionSupported;

	/////////////////////////////////////////////////////////////////////////


	public String getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}


	public OperationTypes getOperationType() {
		return this.operationType;
	}


	public void setOperationType(OperationTypes operationType) {
		this.operationType = operationType;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getAuditType() {
		return this.auditType;
	}


	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}


	public String getCacheType() {
		return this.cacheType;
	}


	public void setCacheType(String cacheType) {
		this.cacheType = cacheType;
	}


	public String getTableType() {
		return this.tableType;
	}


	public void setTableType(String tableType) {
		this.tableType = tableType;
	}


	public String getDaoName() {
		return this.daoName;
	}


	public void setDaoName(String daoName) {
		this.daoName = daoName;
	}


	public String getDtoClass() {
		return this.dtoClass;
	}


	public void setDtoClass(String dtoClass) {
		this.dtoClass = dtoClass;
	}


	public String getDtoProxy() {
		return this.dtoProxy;
	}


	public void setDtoProxy(String dtoProxy) {
		this.dtoProxy = dtoProxy;
	}


	public String getMigrationServiceBeanName() {
		return this.migrationServiceBeanName;
	}


	public void setMigrationServiceBeanName(String migrationServiceBeanName) {
		this.migrationServiceBeanName = migrationServiceBeanName;
	}


	public String getMigrationServiceMethodName() {
		return this.migrationServiceMethodName;
	}


	public void setMigrationServiceMethodName(String migrationServiceMethodName) {
		this.migrationServiceMethodName = migrationServiceMethodName;
	}


	public Boolean getUploadAllowed() {
		return this.uploadAllowed;
	}


	public void setUploadAllowed(Boolean uploadAllowed) {
		this.uploadAllowed = uploadAllowed;
	}


	public String getUploadServiceBeanName() {
		return this.uploadServiceBeanName;
	}


	public void setUploadServiceBeanName(String uploadServiceBeanName) {
		this.uploadServiceBeanName = uploadServiceBeanName;
	}


	public String getUploadServiceMethodName() {
		return this.uploadServiceMethodName;
	}


	public void setUploadServiceMethodName(String uploadServiceMethodName) {
		this.uploadServiceMethodName = uploadServiceMethodName;
	}


	public String getDiscriminatorFormula() {
		return this.discriminatorFormula;
	}


	public void setDiscriminatorFormula(String discriminatorFormula) {
		this.discriminatorFormula = discriminatorFormula;
	}


	public String getDiscriminatorValue() {
		return this.discriminatorValue;
	}


	public void setDiscriminatorValue(String discriminatorValue) {
		this.discriminatorValue = discriminatorValue;
	}


	public Boolean getExcludeFromSql() {
		return this.excludeFromSql;
	}


	public void setExcludeFromSql(Boolean excludeFromSql) {
		this.excludeFromSql = excludeFromSql;
	}


	public Boolean getVersion() {
		return this.version;
	}


	public void setVersion(Boolean version) {
		this.version = version;
	}


	public Boolean getVersionSupported() {
		return this.versionSupported;
	}


	public void setVersionSupported(Boolean versionSupported) {
		this.versionSupported = versionSupported;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
