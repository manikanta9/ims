package com.clifton.core.dataaccess.file;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.FunctionUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;


/**
 * This class is made to allow for temp file creation while using the {@link FilePath} design, rather than {@link File}.
 * <p>
 * Creates a {@link File} that is flagged for deleteOnExit, along with corresponding FilePath wrapper object.
 *
 * @author mitchellf
 */
public class TempFilePath extends FilePath {

	private final File tempFile;


	private TempFilePath(File tempFile) {
		super(tempFile.getPath());
		this.tempFile = tempFile;
	}


	public static TempFilePath createTempPath(String prefix, String suffix) throws IOException {
		File temp = File.createTempFile(prefix, suffix.contains(".") ? suffix : "." + suffix);
		temp.deleteOnExit();
		return new TempFilePath(temp);
	}


	/**
	 * Creates a temporary directory with the given prefix, automatically registering the directory for deletion on JVM shutdown.
	 */
	public static TempFilePath createTempDirectory(String prefix) {
		try {
			Path tempDir = Files.createTempDirectory(prefix);
			// Recursively delete contents on shutdown
			Runtime.getRuntime().addShutdownHook(new Thread(FunctionUtils.uncheckedRunnable(() ->
					Files.walkFileTree(tempDir, new SimpleFileVisitor<Path>() {
						@Override
						public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
							try {
								Files.delete(file);
							}
							catch (Exception e) {
								LogUtils.warn(FileUtils.class, String.format("An error occurred while attempting to delete the temporary file [%s] on JVM shutdown.", file), e);
							}
							return FileVisitResult.CONTINUE;
						}


						@Override
						public FileVisitResult postVisitDirectory(Path dir, IOException exc) {
							try {
								Files.delete(dir);
							}
							catch (Exception e) {
								LogUtils.warn(FileUtils.class, String.format("An error occurred while attempting to delete the temporary directory [%s] on JVM shutdown.", dir), e);
							}
							return FileVisitResult.CONTINUE;
						}
					})
			)));
			return new TempFilePath(tempDir.toFile());
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while creating a temporary directory with the prefix [%s].", prefix), e);
		}
	}


	public File getTempFile() {
		return this.tempFile;
	}
}
