package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.date.DateUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.QuoteMode;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public class DataTableToCsvFileConverter implements DataTableToFileConverter {

	private boolean quoteAll;
	private boolean onError;

	/**
	 * For cases where we need to create a csv file with a different delimiter, i.e. pipe | delimited files
	 */
	private Character delimiterOverride;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DataTableToCsvFileConverter() {
		this(true, false, null);
	}


	public DataTableToCsvFileConverter(char delimiterOverride) {
		this(true, false, delimiterOverride);
	}


	public DataTableToCsvFileConverter(boolean quoteAll) {
		this(quoteAll, false, null);
	}


	public DataTableToCsvFileConverter(boolean quoteAll, boolean onError) {
		this(quoteAll, onError, null);
	}


	public DataTableToCsvFileConverter(boolean quoteAll, boolean onError, Character delimiterOverride) {
		this.quoteAll = quoteAll;
		this.onError = onError;
		this.delimiterOverride = delimiterOverride;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public File convert(DataTableFileConfig dataTableFileConfig) {
		if (dataTableFileConfig == null || dataTableFileConfig.isBlank()) {
			return null;
		}

		String fileName = "DataFile";

		File file = null;
		FileWriter fileWriter = null;
		CSVPrinter csvFilePrinter = null;
		CSVFormat csvFileFormat = CSVFormat.DEFAULT;

		if (getDelimiterOverride() != null) {
			csvFileFormat = csvFileFormat.withDelimiter(getDelimiterOverride());
		}

		// using this in case the error message has new line characters
		// otherwise the message gets cut off and even though it is there if you expand the cell it is not obvious to the user
		if (isOnError()) {
			csvFileFormat = csvFileFormat.withQuote('"');
		}

		if (isQuoteAll()) {
			csvFileFormat = csvFileFormat.withQuoteMode(QuoteMode.ALL);
		}
		else {
			csvFileFormat = csvFileFormat.withQuoteMode(QuoteMode.MINIMAL);
		}

		DataTable dataTable = dataTableFileConfig.getDataTable();
		List<DataColumn> columnList = dataTableFileConfig.getColumnList();
		int columnCount = columnList.size();
		List<String> headerNameList = columnList.stream().map(DataColumn::getColumnName).collect(Collectors.toList());
		try {
			file = File.createTempFile(fileName, ".csv");
			file.deleteOnExit();

			//initialize FileWriter object
			fileWriter = new FileWriter(file);

			//initialize CSVPrinter object
			csvFilePrinter = new CSVPrinter(fileWriter, csvFileFormat);

			//Create CSV file header - exclude header for simple appends
			if (!dataTable.isVariableColumnCountPerRow() && !dataTableFileConfig.isSuppressHeaderRow()) {
				csvFilePrinter.printRecord(headerNameList);
			}

			Map<Integer, Integer> indexMap = new HashMap<>();
			// Write each row of data
			for (int i = 0; i < dataTable.getTotalRowCount(); i++) {
				List<String> values = new ArrayList<>();
				DataRow row = dataTable.getRow(i);
				if (dataTable.isVariableColumnCountPerRow()) {
					columnCount = row.getRowValueList().size();
				}
				for (int j = 0; j < columnCount; j++) {
					// headerNameList size may not match data row column count.
					if (dataTable.isVariableColumnCountPerRow() || columnCount != headerNameList.size()) {
						values.add(getColumnValue(row, j));
					}
					else {
						// remap column index for grid exports based on column header names
						int columnIndex = indexMap.computeIfAbsent(j, index -> dataTable.getColumnIndex(headerNameList.get(index)));
						values.add(getColumnValue(row, columnIndex));
					}
				}
				csvFilePrinter.printRecord(values);
			}
			return file;
		}
		catch (Exception e) {
			if (file != null) {
				// If there's an error, but the temp file was created, delete it
				file.delete();
			}
			LogUtils.error(getClass(), "Error in CsvFileWriter !!!", e);
			throw new RuntimeException("Error generating csv file: " + fileName, e);
		}
		finally {
			try {
				if (fileWriter != null) {
					fileWriter.flush();
					fileWriter.close();
				}
				if (csvFilePrinter != null) {
					csvFilePrinter.close();
				}
			}
			catch (IOException ioe) {
				LogUtils.error(getClass(), "Error while flushing/closing fileWriter/csvPrinter !!!", ioe);
			}
		}
	}


	private String getColumnValue(DataRow row, int columnIndex) {
		Object value = row.getValue(columnIndex);
		if (value == null) {
			return "";
		}
		if (value instanceof Date) {
			return DateUtils.fromDate((Date) value);
		}
		return value.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public boolean isQuoteAll() {
		return this.quoteAll;
	}


	public void setQuoteAll(boolean quoteAll) {
		this.quoteAll = quoteAll;
	}


	public boolean isOnError() {
		return this.onError;
	}


	public void setOnError(boolean onError) {
		this.onError = onError;
	}


	public Character getDelimiterOverride() {
		return this.delimiterOverride;
	}


	public void setDelimiterOverride(Character delimiterOverride) {
		this.delimiterOverride = delimiterOverride;
	}
}
