package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.FieldValidationException;


/**
 * The <code>BooleanRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link Boolean}.
 * Supports EQUALS comparison condition.
 *
 * @author vgomelsky
 */
public class BooleanRestrictionDefinition extends AbstractRestrictionDefinition {

	public BooleanRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public BooleanRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, false);
	}


	public BooleanRestrictionDefinition(String fieldName, String searchFieldName, boolean required) {
		this(fieldName, searchFieldName, null, required);
	}


	public BooleanRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, orderByFieldName, false);
	}


	public BooleanRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS}, null);
	}


	public BooleanRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                    ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomType) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, searchFieldCustomType);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	@Override
	public Object getValue(Object rawValue) {
		if (rawValue == null) {
			return null;
		}
		if (rawValue instanceof Boolean) {
			return rawValue;
		}
		return Boolean.parseBoolean((String) rawValue);
	}


	@Override
	public void validate(SearchRestriction restriction) {
		if (isRequired() && restriction.getValue() == null) {
			throw new FieldValidationException("Required restriction is missing for field " + restriction.getField(), restriction.getField());
		}
		ComparisonConditions comparison = restriction.getComparison();
		if (!getSearchConditionList().contains(comparison)) {
			boolean validate = true;
			if (comparison == ComparisonConditions.EQUALS && getSearchConditionList().size() == 1) {
				ComparisonConditions definedComparison = getSearchConditionList().get(0);
				if (definedComparison == ComparisonConditions.IS_NULL || definedComparison == ComparisonConditions.IS_NOT_NULL || definedComparison == ComparisonConditions.NOT_EQUALS) {
					// allow remapping EQUALS to IS NOT or IS NOT NULL
					validate = false;
				}
			}
			if (validate) {
				throw new FieldValidationException("Unsupported search comparison: " + restriction.getComparison() + " for field " + restriction.getField(), restriction.getField());
			}
		}
	}
}
