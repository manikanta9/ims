package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.db.DatabaseDataTypeConverter;
import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Index;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>MSSQLSchemaDDLUpdateSQLConverter</code> class generates SQL statements from the specified schema using Microsoft compliant syntax.
 * <p/>
 * Used only for table operationType="UPDATE_SQL_ONLY" to do the final alterations (i.e. make the column not null, add indexes)
 * It's called to run right after the SQL migrations complete (See {@link MSSQLSchemaSQLConverter}
 *
 * @author manderson
 */
@Component
public class MSSQLSchemaDDLUpdateSQLConverter implements MigrationSchemaConverter {

	private DatabaseDataTypeConverter sqlDataTypeConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.DDL;
	}


	@Override
	public String convert(Schema schema) {
		if (schema.getExcludeFromSql() != null && schema.getExcludeFromSql()) {
			return "";
		}
		StringBuilder sql = new StringBuilder(1024);
		for (Table table : CollectionUtils.getIterable(schema.getTableList())) {
			if (table.getExcludeFromSql() != null && table.getExcludeFromSql()) {
				// Table was explicitly marked to skip SQL for
				continue;
			}
			if (!StringUtils.isEmpty(table.getDataSource()) && !"dataSource".equals(table.getDataSource())) {
				// Don't execute DDL SQL for tables that are not part of this database.
				continue;
			}
			// Only applies to update sql only
			if (table.getOperationType() != OperationTypes.UPDATE_SQL_ONLY) {
				continue;
			}
			// 1. alter columns
			appendTableSQL(sql, table);

			// 2. add indexes
			appendIndexSQL(sql, table);

			sql.append("\n\n\n");
		}

		return sql.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void appendTableSQL(StringBuilder sql, Table table) {
		List<Column> columnList = table.getColumnList(true);
		int columnCount = CollectionUtils.getSize(columnList);
		for (int i = 0; i < columnCount; i++) {
			Column column = columnList.get(i);
			if (column.getColumnRequired() && column.getDefaultValue() == null) {
				sql.append("\nALTER TABLE ");
				sql.append(table.getName());
				sql.append(" ALTER COLUMN ");
				sql.append(column.getName());
				sql.append(' ');
				sql.append(getSqlDataTypeConverter().convert(column.getDataType(), column.getDataTypeEncodingType()));
				if (column.getDataType().isIdentity() && !table.isBatchEnabled()) {
					sql.append(" IDENTITY (1, 1)");
				}
				sql.append(column.getColumnRequired() ? " NOT NULL" : " NULL");
				if (i != (columnCount - 1)) {
					sql.append(getMigrationSchemaConverterType().getBatchSeparator());
				}
			}
		}
		sql.append(getMigrationSchemaConverterType().getBatchSeparator());
	}


	private void appendIndexSQL(StringBuilder sql, Table table) {
		// Only adding in UX indexes at the end - the rest should be OK in regular DDL
		for (Index index : CollectionUtils.getIterable(table.getIndexList())) {
			sql.append("CREATE");
			if (index.isUnique()) {
				sql.append(" UNIQUE");
			}
			sql.append(" INDEX [");
			sql.append(index.getName());
			sql.append("] ON ");
			sql.append(table.getName());
			sql.append(" (");
			sql.append(BeanUtils.getPropertyValues(index.getColumnList(), "name", ", "));
			sql.append(")");
			if (!CollectionUtils.isEmpty(index.getIncludeColumnList())) {
				sql.append(" INCLUDE ( ");
				sql.append(BeanUtils.getPropertyValues(index.getIncludeColumnList(), "name", ", "));
				sql.append(")");
			}
			// Restrict unique indexes for nullable properties with unique NULLs to non-null rows only
			if (index.isUnique()) {
				String nullableSql = Stream.concat(CollectionUtils.getStream(index.getColumnList()), CollectionUtils.getStream(index.getIncludeColumnList()))
						.filter(column -> BooleanUtils.isFalse(column.getRequired()))
						.filter(Column::isUniqueNulls)
						.map(column -> column.getName() + " IS NOT NULL")
						.collect(Collectors.joining(" OR "));
				if (!StringUtils.isEmpty(nullableSql)) {
					sql.append(" WHERE ").append(nullableSql);
				}
			}
			sql.append(getMigrationSchemaConverterType().getBatchSeparator());
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DatabaseDataTypeConverter getSqlDataTypeConverter() {
		return this.sqlDataTypeConverter;
	}


	public void setSqlDataTypeConverter(DatabaseDataTypeConverter sqlDataTypeConverter) {
		this.sqlDataTypeConverter = sqlDataTypeConverter;
	}
}
