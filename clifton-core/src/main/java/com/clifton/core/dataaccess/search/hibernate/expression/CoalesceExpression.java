package com.clifton.core.dataaccess.search.hibernate.expression;


import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;


/**
 * The <code>CoalesceExpression</code> is used for Coalesce search fields and generates a coalesce in the where clause for the multiple properties as specified in order.
 * By default, when multiple search properties are defined, we append these as or clauses, however by setting the searchFieldCustomType to COALESCE this expression will be used:
 * <p/>
 * For example: On the InvestmentAccount we have an AUMCalculationTypeOverride, which can be used to override the Business Service AUMCalculationType
 * When viewing accounts, we want to show the coalesce of the account override, business service aum type and allow filtering on that coalesce
 * i.e. searchField = "aumCalculationTypeOverride,businessService.aumCalculationType", searchFieldCustomType = SearchFieldCustomTypes.COALESCE, leftJoin = true)
 * Which resolves to COALESCE(AUMCalculationTypeOverride, BusinessService.AUMCalculationType) = ? (or LIKE, depending on search comparison used)
 *
 * @author manderson
 */
public class CoalesceExpression implements Criterion {

	private final String comparisonExpression;

	private final String[] propertyNames;

	/**
	 * Can be used to coalesce with a default value, i.e. COALESCE(x.IsTrue, false)
	 */
	private final Object defaultValue;
	private final Object value;

	private final boolean allowNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CoalesceExpression(String comparisonExpression, String[] propertyNames, Object value) {
		this(comparisonExpression, propertyNames, value, null, false);
	}


	public CoalesceExpression(String comparisonExpression, String[] propertyNames, Object value, Object defaultValue, boolean allowNull) {
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.defaultValue = defaultValue;
		this.value = value;
		this.allowNull = allowNull;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragment = new StringBuilder("COALESCE(");
		for (int i = 0; i < this.propertyNames.length; i++) {
			String column = criteriaQuery.getColumn(criteria, this.propertyNames[i]);
			fragment.append(column);
			if (i < this.propertyNames.length - 1) {
				fragment.append(",");
			}
		}
		if (this.defaultValue != null) {
			fragment.append(",");
			fragment.append(" ? ");
		}
		fragment.append(") ");
		fragment.append(this.comparisonExpression);
		// For cases like: LIKE '%' + ? + '%' we don't want to add a second ?, but we need to for cases like =, >, <, etc.
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragment.append(" ?");
		}
		if (this.allowNull) {
			fragment.insert(0, "(");
			fragment.append(" or (");
			for (int i = 0; i < this.propertyNames.length; i++) {
				String column = criteriaQuery.getColumn(criteria, this.propertyNames[i]);
				fragment.append(column);
				fragment.append(" is null");
				if (i < this.propertyNames.length - 1) {
					fragment.append(" and ");
				}
			}
			fragment.append("))");
		}
		return fragment.toString();
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		TypedValue typedValue = getTypedValue(criteria, criteriaQuery, this.value);
		if (this.defaultValue == null) {
			return new TypedValue[]{typedValue};
		}
		return new TypedValue[]{getTypedValue(criteria, criteriaQuery, this.defaultValue), typedValue};
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private TypedValue getTypedValue(Criteria criteria, CriteriaQuery criteriaQuery, Object value) {
		return criteriaQuery.getTypedValue(criteria, this.propertyNames[0], value);
	}
}
