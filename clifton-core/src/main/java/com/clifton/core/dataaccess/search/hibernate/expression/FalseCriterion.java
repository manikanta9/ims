package com.clifton.core.dataaccess.search.hibernate.expression;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;


/**
 * A no-op {@link Criterion} which will always evaluate to {@code false}.
 *
 * @author MikeH
 */
public class FalseCriterion implements Criterion {

	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return "1 = 0";
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[0];
	}
}
