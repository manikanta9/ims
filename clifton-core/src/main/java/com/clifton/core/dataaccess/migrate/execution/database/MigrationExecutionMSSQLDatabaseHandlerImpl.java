package com.clifton.core.dataaccess.migrate.execution.database;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.migrate.MigrationJdbcTemplate;
import com.clifton.core.dataaccess.migrate.locator.MigrationModule;
import com.clifton.core.dataaccess.migrate.locator.MigrationModuleVersion;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlProvider;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;


public class MigrationExecutionMSSQLDatabaseHandlerImpl implements MigrationExecutionDatabaseHandler {

	/**
	 * The {@link Pattern} which indicates a separation of batches. Any provided SQL will be separated on this delimiter and executed in separate batches.
	 */
	private static final Pattern BATCH_SEPARATOR_PATTERN = Pattern.compile("(\\n|^)\\s*GO\\s*(\\n|$)");

	private MigrationJdbcTemplate migrationJdbcTemplate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void createEnvironmentSchema() {
		StringBuilder schemaSql = new StringBuilder("IF NOT EXISTS (SELECT * FROM sys.schemas WHERE name = 'env')");
		schemaSql.append(" BEGIN ");
		schemaSql.append("EXECUTE ('CREATE SCHEMA env'); ");
		schemaSql.append(" END ");
		getMigrationJdbcTemplate().execute(schemaSql.toString());
	}


	@Override
	public void createMigrationDatabase() {
		dropMigrationDatabase();
		String databaseName = getMigrationJdbcTemplate().getMigrationDatabaseName();
		String logFileLocation = getMigrationJdbcTemplate().getLogFileLocation();
		String mdfFileLocation = getMigrationJdbcTemplate().getMdfFileLocation();
		getMigrationJdbcTemplate().executeMaster("CREATE DATABASE [" + databaseName + "] ON  PRIMARY " + "( NAME = N'" + databaseName + "', FILENAME = N'" + mdfFileLocation
				+ "' , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB ) " + " LOG ON " + "( NAME = N'" + databaseName + "_log', FILENAME = N'" + logFileLocation
				+ "' , SIZE = 2560KB , MAXSIZE = 2048GB , FILEGROWTH = 10%) " + " COLLATE SQL_Latin1_General_CP1_CI_AS ");
	}


	@Override
	public void createMigrationTable() {
		StringBuilder sql = new StringBuilder("IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'dbo' AND TABLE_NAME = 'Migration') BEGIN ");
		sql.append(" ALTER SCHEMA env TRANSFER dbo.Migration;");
		sql.append(" EXEC sp_rename 'env.Migration', 'MigrationModuleVersion';");
		sql.append(" ALTER TABLE env.MigrationModuleVersion ADD MigrationModuleVersionID INT IDENTITY(1,1) NOT NULL END");
		sql.append(" ELSE IF EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'env' AND TABLE_NAME = 'Migration') BEGIN");
		sql.append(" EXEC sp_rename 'env.Migration', 'MigrationModuleVersion';");
		sql.append(" ALTER TABLE env.MigrationModuleVersion ADD MigrationModuleVersionID INT IDENTITY(1,1) NOT NULL END");
		sql.append(" ELSE IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'env' AND TABLE_NAME = 'MigrationModuleVersion') " + " BEGIN " + " CREATE TABLE env.MigrationModuleVersion ( ");
		sql.append("  MigrationModuleVersionID INT IDENTITY(1,1) NOT NULL");
		sql.append(", MigrationModuleName NVARCHAR(100) NOT NULL ");
		sql.append(", MigrationPath NVARCHAR(500) NOT NULL ");
		sql.append(", VersionNumber INT NOT NULL ");
		sql.append(", PreDDLStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", DDLStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", MetaDataStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", DataStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", ActionStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", DataActionStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", SQLStepCompleted BIT NOT NULL DEFAULT(0) ");
		sql.append(", SkippedDueToConditionalSql BIT NOT NULL DEFAULT(0) ");
		sql.append(", InitialExecutionDate DATETIME NOT NULL ");
		sql.append(", LastExecutionDate DATETIME NULL ");
		sql.append(") CREATE UNIQUE INDEX [ux_MigrationModuleVersion_MigrationModuleName_MigrationPath] ON env.MigrationModuleVersion (MigrationModuleName, MigrationPath) " + " END " + " SELECT * FROM env.MigrationModuleVersion ");
		getMigrationJdbcTemplate().execute(sql.toString());
	}


	@Override
	public void getCurrentVersionsForModule(MigrationModule module) {
		String sql = "SELECT MigrationPath, VersionNumber, PreDDLStepCompleted, DDLStepCompleted, MetaDataStepCompleted, DataStepCompleted, ActionStepCompleted, DataActionStepCompleted, SQLStepCompleted FROM env.MigrationModuleVersion WHERE MigrationModuleName = '" + module.getModuleName() + "'";
		module.setCurrentVersion(0);
		ResultSetExtractor<MigrationModule> resultSetExtractor = resultSet -> {
			while (resultSet.next()) {
				MigrationModuleVersion migrationVersion = mapSqlToMigrationModuleVersion(resultSet);
				module.addExistingMigrationVersion(migrationVersion);
				if (migrationVersion.isComplete()) {
					// this is okay even if there are two migration files that have the same number because this is only used
					// to validate the targeted version passed in via the migration.modules property
					module.setCurrentVersion(migrationVersion.getVersionNumber());
				}
			}
			return module;
		};
		getMigrationJdbcTemplate().query(sql, resultSetExtractor);
	}


	protected MigrationModuleVersion mapSqlToMigrationModuleVersion(ResultSet rowSet) {
		MigrationModuleVersion moduleVersion = new MigrationModuleVersion();
		try {
			moduleVersion.setMigrationPath(rowSet.getString("MigrationPath"));
			moduleVersion.setVersionNumber(rowSet.getInt("VersionNumber"));
			moduleVersion.setPreDdlStepCompleted(rowSet.getBoolean("PreDDLStepCompleted"));
			moduleVersion.setDdlStepCompleted(rowSet.getBoolean("DDLStepCompleted"));
			moduleVersion.setMetaDataStepCompleted(rowSet.getBoolean("MetaDataStepCompleted"));
			moduleVersion.setDataStepCompleted(rowSet.getBoolean("DataStepCompleted"));
			moduleVersion.setActionStepCompleted(rowSet.getBoolean("ActionStepCompleted"));
			moduleVersion.setDataActionStepCompleted(rowSet.getBoolean("DataActionStepCompleted"));
			moduleVersion.setSqlStepCompleted(rowSet.getBoolean("SQLStepCompleted"));
			moduleVersion.setExistingMigrationVersion(true);

			return moduleVersion;
		}
		catch (ValidationException | SQLException e) {
			throw new RuntimeException("Invalid SqlRowSet", e);
		}
	}


	/**
	 * Although this method is transactional (all or none of the migration versions will get updated), in practice with failures it is recommended to start fresh with a new
	 * restore.
	 * <p>
	 * The full restore is recommended because of conditional SQL on files.  If the condition SQL refers to the DDL part of the
	 * script and the DDL ran, but SQL didn't run yet - the second try at running will see conditional SQL causes the second run to skip.
	 */
	@Transactional
	@Override
	public void updateMigrationModuleVersions(List<MigrationModule> moduleList) {
		StringBuilder sql = new StringBuilder();
		for (MigrationModule module : CollectionUtils.getIterable(moduleList)) {
			String moduleName = module.getModuleName();
			StringBuilder str = new StringBuilder();
			for (MigrationModuleVersion version : module.getMigrationVersionList()) {
				if (version.isExistingMigrationVersion()) {
					str.append("UPDATE env.MigrationModuleVersion SET ");
					str.append(" PreDDLStepCompleted = '").append(version.isPreDdlStepCompleted()).append("'");
					str.append(", DDLStepCompleted = '").append(version.isDdlStepCompleted()).append("'");
					str.append(", MetaDataStepCompleted = '").append(version.isMetaDataStepCompleted()).append("'");
					str.append(", DataStepCompleted = '").append(version.isDataStepCompleted()).append("'");
					str.append(", ActionStepCompleted = '").append(version.isActionStepCompleted()).append("'");
					str.append(", DataActionStepCompleted = '").append(version.isDataActionStepCompleted()).append("'");
					str.append(", SQLStepCompleted = '").append(version.isSqlStepCompleted()).append("'");
					str.append(", SkippedDueToConditionalSql = '").append(version.isSkippedDueToConditionalSql()).append("'");
					str.append(", LastExecutionDate = '").append(DateUtils.fromDate(version.getLastExecutionDate(), DateUtils.DATE_FORMAT_FULL)).append("'");
					str.append(" WHERE MigrationModuleName = '").append(module.getModuleName()).append('\'');
					str.append(" AND MigrationPath = '").append(version.getMigrationPath()).append("';");
				}
				else {
					str.append(" INSERT INTO env.MigrationModuleVersion VALUES ('");
					str.append(moduleName).append("'");
					str.append(", '").append(version.getMigrationPath()).append("'");
					str.append(", ").append(version.getVersionNumber());
					str.append(", '").append(version.isPreDdlStepCompleted()).append("'");
					str.append(", '").append(version.isDdlStepCompleted()).append("'");
					str.append(", '").append(version.isMetaDataStepCompleted()).append("'");
					str.append(", '").append(version.isDataStepCompleted()).append("'");
					str.append(", '").append(version.isActionStepCompleted()).append("'");
					str.append(", '").append(version.isDataActionStepCompleted()).append("'");
					str.append(", '").append(version.isSqlStepCompleted()).append("'");
					str.append(", '").append(version.isSkippedDueToConditionalSql()).append("'");
					String initialExecutionDate = DateUtils.fromDate(version.getInitialExecutionDate(), DateUtils.DATE_FORMAT_FULL);
					str.append(", '").append(initialExecutionDate).append("'");
					String lastExecutionDate = DateUtils.fromDate(version.getLastExecutionDate(), DateUtils.DATE_FORMAT_FULL);
					str.append(", '").append(lastExecutionDate == null ? initialExecutionDate : lastExecutionDate).append("');");
				}
			}
			sql.append(str);
		}
		getMigrationJdbcTemplate().execute(sql.toString());
	}


	@Override
	public void backupMigrationDatabase() {
		String backupFile = getMigrationJdbcTemplate().getBackupToFileLocation();
		String databaseName = getMigrationJdbcTemplate().getMigrationDatabaseName();
		if (StringUtils.isEmpty(backupFile)) {
			throw new ValidationException("Missing backupToFileLocation property");
		}
		backupFile = backupFile.replaceAll("DATE-TIME", DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE));
		backupFile = backupFile.replaceAll("DATE", DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_SQL));
		LogUtils.info(getClass(), "Backing up " + databaseName + " to file: " + backupFile);
		String sql = "BACKUP DATABASE [" + databaseName + "] TO  DISK = '" + "" + backupFile + "' " + //
				" WITH NOFORMAT " + //
				", COMPRESSION " + //
				", INIT " + //
				", NAME = '" + databaseName + "-Full Database Backup' " + //
				", SKIP " + //
				", NOREWIND " + //
				", NOUNLOAD " + //
				", STATS = 10 ";
		getMigrationJdbcTemplate().executeMaster(sql);
		LogUtils.info(getClass(), "Back up File successfully created");
	}


	@Override
	public void restoreMigrationDatabase() {
		dropMigrationDatabase();
		String diskSql = "DISK = @BackupFile";
		String databaseName = getMigrationJdbcTemplate().getMigrationDatabaseName();
		String logFileLocation = getMigrationJdbcTemplate().getLogFileLocation();
		String mdfFileLocation = getMigrationJdbcTemplate().getMdfFileLocation();
		String originalDatabaseName = getMigrationJdbcTemplate().getRestoreFromOriginalDatabaseName();
		String restoreFromFileLocation = getMigrationJdbcTemplate().getRestoreFromFileLocation();

		Integer restoreFromFileCount = getMigrationJdbcTemplate().getRestoreFromFileCount();
		if (restoreFromFileCount != null && restoreFromFileCount > 1) {
			String[] files = new String[restoreFromFileCount];
			String extension = FileUtils.getFileExtension(restoreFromFileLocation);
			String pathWithoutExtension = FileUtils.getFileNameWithoutExtension(restoreFromFileLocation);
			for (int i = 0; i < restoreFromFileCount; i++) {
				files[i] = "DISK = N'" + pathWithoutExtension + (i + 1) + "." + extension + "'";
			}
			diskSql = StringUtils.join(files, ",");
		}

		StringBuilder restoreSQLSb = new StringBuilder()
				.append("DECLARE @BackupFile NVARCHAR(500) ")
				.append(StringUtils.NEW_LINE)
				.append("DECLARE @LogFileLocation NVARCHAR(500) ")
				.append(StringUtils.NEW_LINE)
				.append("DECLARE @ToMDF NVARCHAR(500) ")
				.append(StringUtils.NEW_LINE)
				.append("DECLARE @ToLDF NVARCHAR(500) ")
				.append(StringUtils.NEW_LINE)
				.append("SET @BackupFile = '")
				.append(restoreFromFileLocation)
				.append("' ")
				.append(StringUtils.NEW_LINE)
				.append("SET @ToMDF = '")
				.append(mdfFileLocation)
				.append("' ")
				.append(StringUtils.NEW_LINE)
				.append("SET @ToLDF = '")
				.append(logFileLocation)
				.append("' ")
				.append(StringUtils.NEW_LINE)
				.append("RESTORE DATABASE ")
				.append(databaseName)
				.append(" FROM ")
				.append(diskSql)
				.append(" WITH REPLACE, FILE = 1,  NOUNLOAD,  REPLACE, BUFFERCOUNT = 2200, MOVE N'")
				.append(StringUtils.coalesce(true, originalDatabaseName, databaseName))
				.append("' TO @ToMDF, MOVE N'")
				.append(StringUtils.coalesce(true, originalDatabaseName, databaseName))
				.append("_log' TO @ToLDF ");


		// add the move for each file group
		if (getMigrationJdbcTemplate().getFileGroupLocationMap() != null) {
			for (Map.Entry<String, String> entry : CollectionUtils.getIterable(getMigrationJdbcTemplate().getFileGroupLocationMap().entrySet())) {
				restoreSQLSb.append(",").append(StringUtils.NEW_LINE).append(" MOVE N'").append(entry.getKey()).append("' TO N'").append(entry.getValue()).append("'");
			}
		}


		getMigrationJdbcTemplate().executeMaster(restoreSQLSb.toString());
	}


	@Override
	public void dropMigrationDatabase() {
		getMigrationJdbcTemplate().validateNonProductionInstance();
		String databaseName = getMigrationJdbcTemplate().getMigrationDatabaseName();
		getMigrationJdbcTemplate().executeMaster("IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" + databaseName + "')ALTER DATABASE " + databaseName
				+ " SET SINGLE_USER WITH ROLLBACK IMMEDIATE");
		getMigrationJdbcTemplate().executeMaster("IF EXISTS (SELECT name FROM sys.databases WHERE name = N'" + databaseName + "') DROP DATABASE [" + databaseName + "]");
	}


	@Override
	public void batchUpdate(String[] sqlBatches) {
		getMigrationJdbcTemplate().batchUpdate(sqlBatches);
	}


	@Override
	public SqlRowSet queryForRowSet(String sql) {
		return getMigrationJdbcTemplate().queryForRowSet(sql);
	}


	@Override
	public void createEnvironmentTable() {
		StringBuilder tableSql = new StringBuilder("IF NOT EXISTS(SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = 'env' AND TABLE_NAME = 'Environment')");
		tableSql.append(" BEGIN ");
		tableSql.append("EXECUTE('CREATE TABLE env.Environment (EnvironmentLevel NVARCHAR(50) NOT NULL, UniqueCheck BIT NOT NULL DEFAULT 0, CONSTRAINT UniqueCheck CHECK (UniqueCheck = 0), CONSTRAINT UQ_UniqueCheck UNIQUE(UniqueCheck))'); ");
		tableSql.append("EXECUTE('INSERT INTO env.Environment VALUES(''PROD'', 0);'); ");
		tableSql.append("END");
		getMigrationJdbcTemplate().execute(tableSql.toString());
	}


	@Override
	public void updateEnvironmentTable(String applicationEnvironmentLevel) {
		if (!StringUtils.isEmpty(applicationEnvironmentLevel)) {
			StringBuilder updateBuilder = new StringBuilder("UPDATE env.Environment SET EnvironmentLevel = '");
			updateBuilder.append(applicationEnvironmentLevel);
			updateBuilder.append("';");
			getMigrationJdbcTemplate().execute(updateBuilder.toString());
		}
	}


	@Override
	public Object queryForObject(String sql, Class<?> clazz) {
		return getMigrationJdbcTemplate().queryForObject(sql, clazz);
	}


	/**
	 * Warning: This is overridden to catch cases where multiple statements are executed, but one
	 * that fails may be hidden.  By default, if the first update works it doesn't return the error
	 * so migrations appear to pass when they shouldn't have
	 * Found this technique here: https://github.com/flyway/flyway/pull/780/commits/e7d29d79ca9adfa139ae9d3724c3e65b083bffd2
	 */
	@Override
	public void execute(String sql) {
		class ExecuteStatementCallback implements StatementCallback<Object>, SqlProvider {

			@Override
			public Object doInStatement(Statement stmt) throws SQLException {
				for (String batchSql : BATCH_SEPARATOR_PATTERN.split(sql)) {
					boolean hasResults = stmt.execute(batchSql);
					// retrieve all results to ensure all errors are detected
					while (hasResults || stmt.getUpdateCount() != -1) {
						hasResults = stmt.getMoreResults();
					}
				}
				return null;
			}


			@Override
			public String getSql() {
				return sql;
			}
		}

		getMigrationJdbcTemplate().execute(new ExecuteStatementCallback());
	}


	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public MigrationJdbcTemplate getMigrationJdbcTemplate() {
		return this.migrationJdbcTemplate;
	}


	public void setMigrationJdbcTemplate(MigrationJdbcTemplate migrationJdbcTemplate) {
		this.migrationJdbcTemplate = migrationJdbcTemplate;
	}
}
