package com.clifton.core.dataaccess.dao.event.cache.impl.name;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * CacheByName annotation is used by Named Entity objects that would like to utilize caching by name.
 * i.e. getAccountAccountByName
 * <p>
 * When an UpdatableDAO is registered for a DTO that is annotated with CacheByName, new instance of NamedEntityCache is created and registered for the dao of that entity, where
 * the default cache spring bean name is ClassName + "Cache", (first letter lowercase) i.e. accountingAccountCache, however the cache name can be overridden if necessary
 * and the cache name will be the DTO package + cache bean spring name (without the lower case) i.e. com.clifton.accounting.account.AccountingAccountCache.  The isFilterable
 * attribute, if set to true, indicates that a NamedEntityFilterableCache will be used as the cache.
 *
 * @author manderson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface CacheByName {

	/**
	 * If null, uses the default logic of className + Cache, i.e. AccountingAccountCache
	 */
	String cacheName() default "";


	/**
	 * If true, a filterable named cache is used.
	 */
	boolean filterable() default false;
}
