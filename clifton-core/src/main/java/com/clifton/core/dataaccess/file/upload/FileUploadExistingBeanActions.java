package com.clifton.core.dataaccess.file.upload;

/**
 * The <code>FileUploadExistingBeanActions</code> are common options when uploading data into the system
 * 1.  SKIP - i.e. for System Uploads, Will lookup beans by Natural Keys and if it exists, will skip that
 * row in the upload.
 * 2.  UPDATE - i.e. for System Uploads, Will lookup bean by Natural Keys and if it exists, will update that
 * record. (Columns not included in the upload file will not be overwritten in the database)
 * 3.  INSERT - Will NOT lookup beans in the database (FK look ups are still performed)
 * and will assume all rows are inserts.  If there is an existing bean it should be caught
 * by a UX constraint violation during the saves (Used for Improved Performance)
 *
 * @author manderson
 */
public enum FileUploadExistingBeanActions {

	SKIP,
	UPDATE,
	INSERT
}
