package com.clifton.core.dataaccess;

import org.springframework.jdbc.datasource.DelegatingDataSource;

import javax.sql.DataSource;


/**
 * <code>DataSourceUtils</code> is a utility class for working with {@link DataSource} Spring managed beans. A data source may be decorated with a
 * {@link DelegatingDataSource}, masking the target dataSource used by a Spring managed bean (e.g. JdbcTemplate, TransactionManager, SessionFactory).
 *
 * @author NickK
 */
public class DataSourceUtils {

	private DataSourceUtils() {
		// prevent instantiation
	}


	/**
	 * Validates if the provided data sources are both using the same target data source.
	 * <p>
	 * Each data source's target is obtained via {@link #getTargetDataSource(DataSource)} prior to comparing.
	 */
	public static boolean isEqual(DataSource dataSource1, DataSource dataSource2) {
		if (dataSource1 == null) {
			return dataSource2 == null;
		}

		DataSource targetDataSource1 = getTargetDataSource(dataSource1);
		DataSource targetDataSource2 = getTargetDataSource(dataSource2);
		return targetDataSource1.equals(targetDataSource2);
	}


	/**
	 * Recursively unwraps the provided data source if a {@link DelegatingDataSource} to obtain the target data source.
	 */
	public static DataSource getTargetDataSource(DataSource dataSource) {
		if (dataSource instanceof DelegatingDataSource) {
			return getTargetDataSource(((DelegatingDataSource) dataSource).getTargetDataSource());
		}
		return dataSource;
	}
}
