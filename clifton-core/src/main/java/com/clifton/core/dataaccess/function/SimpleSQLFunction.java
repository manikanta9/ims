package com.clifton.core.dataaccess.function;

import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;

import java.util.List;


/**
 * Simplified version of {@link org.hibernate.dialect.function.StandardSQLFunction}. Meant to be extend by {@link SQLFunction} implementations that
 * {@link SQLFunction#render(Type, List, SessionFactoryImplementor)} or {@link SQLFunction#getReturnType(Type, Mapping)} are dependent on the dialect.
 *
 * @author dillonm
 */
public abstract class SimpleSQLFunction implements SQLFunction {

	@Override
	public boolean hasArguments() {
		return true;
	}


	@Override
	public boolean hasParenthesesIfNoArguments() {
		return true;
	}
}
