package com.clifton.core.dataaccess.dao.event.cache.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoThreeKeyListCache;

import java.util.List;


/**
 * The <code>SelfRegisteringThreeKeyDaoListCache</code> class is a helper class that should be extended by DAO caches that cache list of entities based on 3 key properties
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events. (By default UPDATE OR DELETE)
 * <p>
 * This cache supports cases where the cache uses 3 properties as its key
 *
 * @param <T>
 * @author Mary Anderson
 */
public abstract class SelfRegisteringThreeKeyDaoListCache<T extends IdentityObject, K1, K2, K3> extends SelfRegisteringDaoListCache<T> implements DaoThreeKeyListCache<T, K1, K2, K3> {

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	protected abstract String getBeanKey1Property();


	protected abstract String getBeanKey2Property();


	protected abstract String getBeanKey3Property();


	protected abstract K1 getBeanKey1Value(T bean);


	protected abstract K2 getBeanKey2Value(T bean);


	protected abstract K3 getBeanKey3Value(T bean);


	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	@Override
	protected String[] getBeanKeyProperties() {
		return new String[]{getBeanKey1Property(), getBeanKey2Property(), getBeanKey3Property()};
	}


	@Override
	protected String getBeanKey(T bean) {
		return getBeanKeyForProperties(getBeanKey1Value(bean), getBeanKey2Value(bean), getBeanKey3Value(bean));
	}

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	@Override
	public final List<T> getBeanListForKeyValues(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2, K3 keyPropertyValue3) {
		return getBeanListImpl(dao, false, keyPropertyValue1, keyPropertyValue2, keyPropertyValue3);
	}


	@Override
	public final List<T> getBeanListForKeyValuesStrict(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2, K3 keyPropertyValue3) {
		return getBeanListImpl(dao, true, keyPropertyValue1, keyPropertyValue2, keyPropertyValue3);
	}
}
