package com.clifton.core.dataaccess.function;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.MapUtils;
import org.hibernate.Criteria;
import org.hibernate.QueryException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;

import java.util.ArrayList;
import java.util.List;


/**
 * Utilities for working with {@link SQLFunction} implementations.
 *
 * @author dillonm
 */
public class SQLFunctionUtils {


	private SQLFunctionUtils() {
		// Disable instantiation
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieve a {@link SQLFunction} registered by the {@link org.springframework.orm.hibernate5.LocalSessionFactoryBuilder} and
	 * the {@link com.clifton.core.dataaccess.dialect.SessionFactoryBuilderConfigurer} implementations..
	 */
	public static SQLFunction getFunction(CriteriaQuery criteriaQuery, String functionName) {
		final SQLFunction function = criteriaQuery.getFactory()
				.getSqlFunctionRegistry()
				.findSQLFunction(functionName);
		AssertUtils.assertNotNull(function, "Unable to locate mapping for function named [%s]", functionName);
		return function;
	}


	/**
	 * Build the arguments field for {@link SQLFunction#render(Type, List, SessionFactoryImplementor)} implementations.
	 */
	public static List<String> buildFunctionParameterList(Criteria criteria, CriteriaQuery criteriaQuery, String... properties) {
		List<String> parameters = new ArrayList<>(properties.length);
		for (String property : properties) {
			String parameter;
			try {
				parameter = criteriaQuery.getColumn(criteria, property);
			}
			//treat parameters with no associated column as a literal value. Ex: numeric and string parameters
			catch (QueryException e) {
				parameter = property;
			}
			parameters.add(parameter);
		}
		return parameters;
	}


	/**
	 * Add the {@link SQLFunction} to the {@link LocalSessionFactoryBuilder} if the function does not already exist.
	 */
	public static void addSqlFunction(LocalSessionFactoryBuilder sfb, String functionName, SQLFunction sqlFunction) {
		AssertUtils.assertFalse(MapUtils.contains(functionName, sfb.getSqlFunctions()), "A SQL function named %s already exists in %s", functionName, sfb.getClass().getName());
		sfb.addSqlFunction(functionName, sqlFunction);
	}
}
