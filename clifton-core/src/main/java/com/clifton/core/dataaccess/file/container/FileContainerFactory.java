package com.clifton.core.dataaccess.file.container;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.ServiceAuthenticationHandler;
import com.clifton.core.util.StringUtils;
import com.hierynomus.smbj.auth.AuthenticationContext;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.concurrent.atomic.AtomicReference;


/**
 * @author theodorez
 */
@Component
public class FileContainerFactory implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private static final AtomicReference<ServiceAuthenticationHandler> serviceAuthAtomicReference = new AtomicReference<>();
	//If the host operating system is Windows, always use the Internal File impl as windows handles SMB/CIFS/NTLM natively
	private static final boolean INTERNAL_ONLY = System.getProperty("os.name").contains("Windows");
	private static AuthenticationContext authenticationContext = null;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the appropriate FileContainer implementation for the file object that is passed in.
	 * <p>
	 * NOTE: Remote files MUST have paths that are prefixed with two backslashes.
	 */
	public static FileContainer getFileContainer(File file) {
		return getFileContainer(file.getPath());
	}


	/**
	 * Returns the appropriate FileContainer implementation for the {@link FilePath} object that is passed in.
	 * <p>
	 * NOTE: Remote files MUST have paths that are prefixed with two backslashes.
	 */
	public static FileContainer getFileContainer(FilePath file) {
		return getFileContainer(file.getPath());
	}


	/**
	 * Returns the appropriate FileContainer implementation for the file path that is passed in.
	 * <p>
	 * NOTE: Remote file paths MUST be prefixed with two backslashes.
	 */
	public static FileContainer getFileContainer(String filePath) {
		FileContainer utilFile;
		if (!INTERNAL_ONLY && FileUtils.isRemote(filePath)) {
			utilFile = getFileContainerRemote(filePath);
		}
		else {
			utilFile = new FileContainerNative(filePath);
		}
		return utilFile;
	}


	private static FileContainer getFileContainerRemote(String filePath) {
		//If there is no extension is probably a directory
		if (FileUtils.getFileExtension(filePath) == null) {
			//SMB Files require a trailing slash for directories
			return new FileContainerRemote(getSmbPath(filePath + "/"), new FileContainerContext(getAuthenticationContext()));
		}
		else {
			return new FileContainerRemote(getSmbPath(filePath), new FileContainerContext(getAuthenticationContext()));
		}
	}


	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	private static AuthenticationContext getAuthenticationContext() {
		LogUtils.info(LogCommand.ofMessage(FileContainerFactory.class, "Service Auth Reference empty: ", serviceAuthAtomicReference.get() == null));
		if (serviceAuthAtomicReference.get() != null && authenticationContext == null) {
			ServiceAuthenticationHandler handler = serviceAuthAtomicReference.get();
			LogUtils.info(LogCommand.ofMessage(FileContainerFactory.class, "Handler is empty: ", handler == null, " ServiceDomain: ", handler != null ? handler.getServiceDomain() : "empty"));
			if (handler != null && !StringUtils.isEmpty(handler.getServiceDomain())) {
				//Should only create a new auth if the properties are populated
				authenticationContext = new AuthenticationContext(handler.getServiceUsername(), handler.getServicePassword().toCharArray(), handler.getServiceDomain());
			}
		}
		LogUtils.info(LogCommand.ofMessage(FileContainerFactory.class, "Auth Context: ", authenticationContext));
		return authenticationContext;
	}


	private static String getSmbPath(String filePath) {
		if (StringUtils.isEmpty(filePath)) {
			return "";
		}
		final String pathPrefix = "//";
		if (filePath.startsWith(pathPrefix)) {
			return filePath;
		}
		String path = FileUtils.normalizeFilePathSeparators(filePath).replace(File.separator, "/");
		return pathPrefix + path.replaceFirst("^(/|\\\\)+", "");
	}


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		ServiceAuthenticationHandler handler = (ServiceAuthenticationHandler) getApplicationContext().getBean("serviceAuthenticationHandler");
		serviceAuthAtomicReference.set(handler);
		LogUtils.info(LogCommand.ofMessage(FileContainerFactory.class, "Auth Context Reference Set: ", serviceAuthAtomicReference.get()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
