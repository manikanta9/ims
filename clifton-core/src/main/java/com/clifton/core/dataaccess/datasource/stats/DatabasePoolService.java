package com.clifton.core.dataaccess.datasource.stats;

import com.clifton.core.beans.BeanListCommand;
import com.clifton.core.beans.NamedEntityWithoutLabel;

import java.util.List;


/**
 * <code>DatabasePoolService</code> provides services for monitoring and administering
 * database pools.
 *
 * @author NickK
 */
public interface DatabasePoolService {

	/**
	 * Returns the current {@link DatabaseStatistics} for the active database connection pool with the specified pool name.
	 */
	public DatabaseStatistics getDatabaseStatistics(String poolName);


	/**
	 * Updates the underlying database connection pool real-time to reflect the provided list of updated {@link DatabaseProperty}.
	 */
	public DatabaseStatistics saveDatabasePropertyList(BeanListCommand<DatabaseProperty> propertyList, String dataSourceName);


	/**
	 * Processes the provided {@link DatabaseStatisticsCommand} to perform administrator functions on the database connection pool.
	 */
	public DatabaseStatistics processDatabaseStatisticsCommand(DatabaseStatisticsCommand command, String dataSourceName);


	/**
	 * Returns a list of DataSource class names used within IMS.
	 */
	public List<NamedEntityWithoutLabel<String>> getDatabasePoolNameList();
}
