package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import java.sql.Types;


/**
 * The <code>ExcelFileToDataTableConverter</code> ...
 *
 * @author manderson
 */
public class ExcelFileToDataTableConverter implements FileToDataTableConverter {

	private int sheetIndex = 0;


	@Override
	public DataTable convert(Object file) {
		Workbook workbook;
		if (file instanceof Workbook) {
			workbook = (Workbook) file;
		}
		else {
			workbook = ExcelFileUtils.createWorkbook(file);
		}
		Sheet sheet = workbook.getSheetAt(getSheetIndex());
		DataColumnImpl[] columnList = new DataColumnImpl[0];
		int columnIndex = 0;
		Cell cell = ExcelFileUtils.getCell(sheet, 0, columnIndex, false);
		while (cell != null) {
			Cell dataCell = getFirstDataCell(sheet, columnIndex);
			String columnName = (String) ExcelFileUtils.getCellValue(sheet, 0, columnIndex, Types.NVARCHAR);
			if (columnName != null) {
				columnName = columnName.trim();
			}
			DataColumnImpl column = new DataColumnImpl(columnName, ExcelFileUtils.getSqlDataType(columnName, (dataCell == null ? CellType.STRING : dataCell.getCellTypeEnum())));
			columnList = ArrayUtils.add(columnList, column);
			cell = ExcelFileUtils.getCell(sheet, 0, ++columnIndex, false);
		}
		DataTable dataTable = new PagingDataTableImpl(columnList);
		// Actual Data starts at index 1
		int rowIndex = 1;
		Row row = ExcelFileUtils.getRow(sheet, rowIndex, false);
		while (row != null) {
			boolean emptyRow = true;
			Object[] data = new Object[columnList.length];
			for (int j = 0; j < columnList.length; j++) {
				Object value = ExcelFileUtils.getCellValue(row, j, columnList[j].getDataType());
				if (value != null && emptyRow) {
					emptyRow = false;
				}
				data[j] = value;
			}
			if (!emptyRow) {
				DataRow dataRow = new DataRowImpl(dataTable, data);
				dataTable.addRow(dataRow);
			}
			row = ExcelFileUtils.getRow(sheet, ++rowIndex, false);
		}
		return dataTable;
	}


	private Cell getFirstDataCell(Sheet sheet, int columnIndex) {
		int rowIndex = 1;
		Row row = ExcelFileUtils.getRow(sheet, rowIndex, false);
		while (row != null) {
			Cell cell = row.getCell(columnIndex);
			if (cell != null) {
				return cell;
			}
			row = ExcelFileUtils.getRow(sheet, ++rowIndex, false);
		}
		return null;
	}


	public int getSheetIndex() {
		return this.sheetIndex;
	}


	public void setSheetIndex(int sheetIndex) {
		this.sheetIndex = sheetIndex;
	}
}
