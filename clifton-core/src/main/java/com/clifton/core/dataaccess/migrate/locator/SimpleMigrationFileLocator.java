package com.clifton.core.dataaccess.migrate.locator;


import java.util.List;


/**
 * The <code>SimpleMigrationFileLocator</code> class provides a very simple MigrationFileLocator implementation
 * that expects all data to be injected into it.
 *
 * @author vgomelsky
 */
public abstract class SimpleMigrationFileLocator implements MigrationFileLocator {

	private List<MigrationModule> migrationModuleList;


	@Override
	public List<MigrationModule> locateMigrations(MigrationFileLocatorVersionValidation versionValidation) {
		return getMigrationModuleList();
	}


	/**
	 * @return the moduleList
	 */
	public List<MigrationModule> getMigrationModuleList() {
		return this.migrationModuleList;
	}


	/**
	 * @param migrationModuleList the moduleList to set
	 */
	@Override
	public void setMigrationModuleList(List<MigrationModule> migrationModuleList) {
		this.migrationModuleList = migrationModuleList;
	}
}
