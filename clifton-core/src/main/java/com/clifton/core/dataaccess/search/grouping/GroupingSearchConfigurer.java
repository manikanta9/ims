package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.dataaccess.search.SearchConfigurer;
import org.hibernate.Criteria;


public interface GroupingSearchConfigurer extends SearchConfigurer<Criteria> {


	public SearchConfigurer<Criteria> getEntitySearchFormConfigurer();


	public GroupingSearchForm getGroupingSearchForm();
}
