package com.clifton.core.dataaccess.file.upload;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.DataTableToBeanListConverter;
import com.clifton.core.dataaccess.file.CSVFileToDataTableConverter;
import com.clifton.core.dataaccess.file.ExcelFileToDataTableConverter;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileToDataTableConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;


/**
 * @author manderson
 */
@Component
public class FileUploadHandlerImpl implements FileUploadHandler {

	@Override
	public <T> List<T> convertFileUploadFileToBeanList(FileUploadCommand<T> uploadCommand) {
		FileToDataTableConverter fileToDataTableConverter;
		if (StringUtils.isEqualIgnoreCase(FileFormats.CSV.getFormatString(), FileUtils.getFileExtension(uploadCommand.getFile().getOriginalFilename()))) {
			fileToDataTableConverter = new CSVFileToDataTableConverter();
		}
		else {
			fileToDataTableConverter = new ExcelFileToDataTableConverter();
			if (uploadCommand.getSheetIndex() != 0) {
				((ExcelFileToDataTableConverter) fileToDataTableConverter).setSheetIndex(uploadCommand.getSheetIndex());
			}
		}
		DataTable dt = fileToDataTableConverter.convert(uploadCommand.getFile());
		DataTableToBeanListConverter<T> beanConverter = new DataTableToBeanListConverter<>(uploadCommand.getUploadBeanClass(), uploadCommand.getRequiredProperties(), uploadCommand::applyUnmappedColumnValues);
		return beanConverter.convert(dt);
	}
}
