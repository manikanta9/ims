package com.clifton.core.dataaccess.search;


/**
 * The <code>OrderByDirection</code> enumeration defines available directions for the "ORDER BY" SQL clause.
 *
 * @author vgomelsky
 */
public enum OrderByDirections {

	ASC, DESC
}
