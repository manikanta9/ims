package com.clifton.core.dataaccess.file.container;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.function.Predicate;
import java.util.stream.Stream;


/**
 * Represents a common File object.
 *
 * @author theodorez
 */
public interface FileContainer {

	public void deleteFile() throws IOException;


	public boolean exists();


	public boolean isDirectory();


	public boolean canWrite() throws IOException;


	/**
	 * Return an array of file names contained within this Directory FileContainer.
	 * This does not return paths, only file names.
	 */
	public String[] list() throws IOException;


	/**
	 * Return Stream of file names contained within this Directory FileContainer that match the (glob on Windows, wildcard expression on Linux) and the predicate.
	 * This does not return paths, only file names.
	 */
	public Stream<String> list(String wildcard, Predicate<String> fileNameMatcher) throws IOException;


	public String getAbsolutePath();


	public String getPath();


	/**
	 * Returns the name of the file or directory denoted by this abstract
	 * pathname.  This is just the last name in the pathname's name
	 * sequence.  If the pathname's name sequence is empty, then the empty
	 * string is returned.
	 *
	 * @see File#getName()
	 */
	public String getName();


	public InputStream getInputStream() throws IOException;


	public OutputStream getOutputStream() throws IOException;


	/**
	 * <p> The <em>parent</em> of an abstract pathname consists of the
	 * pathname's prefix, if any, and each name in the pathname's name
	 * sequence except for the last.  If the name sequence is empty then
	 * the pathname does not name a parent directory.
	 *
	 * @see File#getParent()
	 */
	public String getParent();


	public boolean makeDirs() throws IOException;


	public byte[] readBytes() throws IOException;


	public void writeBytes(byte[] bytes) throws IOException;


	public long length() throws IOException;


	public boolean renameToFile(FileContainer toFile);
}
