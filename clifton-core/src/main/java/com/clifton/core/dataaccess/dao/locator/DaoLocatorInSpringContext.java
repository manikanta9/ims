package com.clifton.core.dataaccess.dao.locator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>DaoLocatorInSpringContext</code> class provides Spring context based implementation of the {@link DaoLocator} interface.
 *
 * @author vgomelsky
 */
@Component
public class DaoLocatorInSpringContext implements DaoLocator, ApplicationContextAware {

	private ApplicationContext applicationContext;
	private final Map<Class<IdentityObject>, String> dtoClassToDaoNameMap = new ConcurrentHashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(ParameterizedType dtoType) {
		String daoName = getDaoName(dtoType);
		return (ReadOnlyDAO<T>) getApplicationContext().getBean(daoName);
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(Class<T> dtoClass, boolean suppressNotFoundError) {
		// lookup DAO bean name in cache first
		String daoName = this.dtoClassToDaoNameMap.get(dtoClass);
		if (daoName == null) {
			daoName = doLocateDaoName(dtoClass);
			if (daoName == null) {
				String message = "Cannot locate DAO for " + dtoClass + " and no superclass with DAO found.";
				if (suppressNotFoundError) {
					LogUtils.warn(getClass(), message);
					return null;
				}
				else {
					throw new IllegalArgumentException(message);
				}
			}
			this.dtoClassToDaoNameMap.put((Class<IdentityObject>) dtoClass, daoName);
		}
		return (ReadOnlyDAO<T>) getApplicationContext().getBean(daoName);
	}


	@Override
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(Class<T> dtoClass) {
		return locate(dtoClass, false);
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(T dto) {
		return locate((Class<T>) dto.getClass());
	}


	@SuppressWarnings("unchecked")
	private <T extends IdentityObject> String doLocateDaoName(Class<T> dtoClass) {
		// should we use naming convention to identify DAO's???
		String daoName = DaoUtils.getDaoName(dtoClass);

		// For DAO that cover multiple classes (Table per class hierarchy)
		// the DAO itself will exist for the top level DTO Class, not one
		// for each subclass
		if (!getApplicationContext().containsBean(daoName)) {
			if (AnnotationUtils.isAnnotationPresent(dtoClass, DtoLocator.class)) {
				DtoLocator dtoLocator = AnnotationUtils.getAnnotation(dtoClass, DtoLocator.class);
				return doLocateDaoName(dtoLocator.dtoClass());
			}
			// Special Case During Converting Projects from "Old"
			// Table names will remain but DTO Names contain "Old" - so if the DTO Class contains "Old" strip it and see if that can find the dao
			if (daoName.contains("Old")) {
				daoName = daoName.replace("Old", "");
				if (getApplicationContext().containsBean(daoName)) {
					return daoName;
				}
			}
			Class<IdentityObject> superClass = (Class<IdentityObject>) dtoClass.getSuperclass();
			if (superClass == null) {
				return null;
			}
			return doLocateDaoName(superClass);
		}
		return daoName;
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(String tableName) {
		// should we use naming convention to identify DAO's???
		String daoName = DaoUtils.getDaoName(tableName);
		return (ReadOnlyDAO<T>) getApplicationContext().getBean(daoName);
	}


	@Override
	public Collection<ReadOnlyDAO<?>> locateAll() {
		@SuppressWarnings("unchecked")
		Collection<ReadOnlyDAO<?>> result = (Collection<ReadOnlyDAO<?>>) (Collection<?>) getApplicationContext().getBeansOfType(ReadOnlyDAO.class).values();
		return result;
	}


	@Override
	public <T> Collection<ReadOnlyDAO<? extends T>> locateAll(Class<T> dtoInstanceOfClass) {
		Collection<ReadOnlyDAO<? extends T>> result = new ArrayList<>();
		for (ReadOnlyDAO<?> dao : locateAll()) {
			if (dtoInstanceOfClass.isAssignableFrom(dao.getConfiguration().getBeanClass())) {
				@SuppressWarnings("unchecked")
				ReadOnlyDAO<? extends T> typedDao = (ReadOnlyDAO<? extends T>) dao;
				result.add(typedDao);
			}
		}
		return result;
	}


	@SuppressWarnings("unchecked")
	private <T extends IdentityObject> String getDaoName(ParameterizedType dtoType) {
		Class<?> dtoClass = (Class<?>) dtoType.getRawType();
		if (ManyToManyEntity.class.equals(dtoClass)) {
			Type[] types = dtoType.getActualTypeArguments();
			String name1 = DaoUtils.getDaoName((Class<T>) types[0]);
			String name2 = DaoUtils.getDaoName((Class<T>) types[1]);
			StringBuilder result = new StringBuilder();
			// append non-matching ending of the second argument to the first: ManyToManyEntity<SecurityUser, SecurityGroup> => securityUserGroupDAO
			int beginIndex = 0;
			while (true) {
				String word1 = StringUtils.getLeadingWord(name1.substring(beginIndex));
				String word2 = StringUtils.getLeadingWord(name2.substring(beginIndex));
				if (!word1.equals(word2)) {
					break;
				}
				result.append(word1);
				beginIndex = result.length();
			}
			result.append(name1, beginIndex, name1.length() - 3);
			result.append(name2.substring(beginIndex));
			return result.toString();
		}
		return DaoUtils.getDaoName(dtoClass);
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
