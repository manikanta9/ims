package com.clifton.core.dataaccess.file;


import java.io.InputStream;


public class DocumentContentStream {

	private long length;

	private String filename;
	private String mimeType;
	private InputStream stream;


	public String getFilename() {
		return this.filename;
	}


	public void setFilename(String filename) {
		this.filename = filename;
	}


	public long getLength() {
		return this.length;
	}


	public void setLength(long length) {
		this.length = length;
	}


	public String getMimeType() {
		return this.mimeType;
	}


	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}


	public InputStream getStream() {
		return this.stream;
	}


	public void setStream(InputStream stream) {
		this.stream = stream;
	}
}
