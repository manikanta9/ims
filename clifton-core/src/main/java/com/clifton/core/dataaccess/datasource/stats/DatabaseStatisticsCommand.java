package com.clifton.core.dataaccess.datasource.stats;

import java.io.Serializable;


/**
 * <code>DatabaseStatisticsCommand</code> is used to customize the behavior and response data
 * when fetching a {@link DatabaseStatistics}.
 *
 * @author NickK
 */
public class DatabaseStatisticsCommand implements Serializable {

	// flag to signal a reset of statistics
	private boolean clearStatistics;

	// flag to signal a purge of current connections
	private boolean resetConnections;

	// flag to restore database property list to values at startup
	private boolean restorePropertyList;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public boolean isClearStatistics() {
		return this.clearStatistics;
	}


	public void setClearStatistics(boolean clearStatistics) {
		this.clearStatistics = clearStatistics;
	}


	public boolean isResetConnections() {
		return this.resetConnections;
	}


	public void setResetConnections(boolean resetConnections) {
		this.resetConnections = resetConnections;
	}


	public boolean isRestorePropertyList() {
		return this.restorePropertyList;
	}


	public void setRestorePropertyList(boolean restorePropertyList) {
		this.restorePropertyList = restorePropertyList;
	}
}
