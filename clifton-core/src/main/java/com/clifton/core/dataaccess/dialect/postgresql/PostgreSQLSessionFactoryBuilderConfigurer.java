package com.clifton.core.dataaccess.dialect.postgresql;

import com.clifton.core.dataaccess.dialect.SessionFactoryBuilderConfigurer;
import com.clifton.core.dataaccess.function.SQLFunctionUtils;
import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.stereotype.Component;


/**
 * @author dillonm
 */
@Component
public class PostgreSQLSessionFactoryBuilderConfigurer implements SessionFactoryBuilderConfigurer {

	@Override
	public void configure(LocalSessionFactoryBuilder sfb) {
		SQLFunctionUtils.addSqlFunction(sfb, SQLIfNullFunction.FUNCTION_NAME, new PostgreSQLIfNullFunction());
		SQLFunctionUtils.addSqlFunction(sfb, SQLIfElseFunction.FUNCTION_NAME, new PostgreSQLIfElseFunction());
	}


	@Override
	public boolean supportsDialect(String dialect) {
		return dialect.startsWith("org.hibernate.dialect.PostgreSQL");
	}
}
