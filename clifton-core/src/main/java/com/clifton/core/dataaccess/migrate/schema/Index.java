package com.clifton.core.dataaccess.migrate.schema;


import java.util.List;


/**
 * The <code>Index</code> class represents a database table index.
 *
 * @author vgomelsky
 */
public class Index {

	private String name;
	private boolean unique;
	private boolean clustered;

	private Table table;
	private List<Column> columnList;

	/**
	 * If populated, generates index with includes
	 */
	private List<Column> includeColumnList;

	private boolean includes;


	public Index() {
		super();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.name;
	}


	public boolean isUnique() {
		return this.unique;
	}


	public void setUnique(boolean unique) {
		this.unique = unique;
	}


	public boolean isClustered() {
		return this.clustered;
	}


	public void setClustered(boolean clustered) {
		this.clustered = clustered;
	}


	/**
	 * @return the table
	 */
	public Table getTable() {
		return this.table;
	}


	/**
	 * @param table the table to set
	 */
	public void setTable(Table table) {
		this.table = table;
	}


	/**
	 * @return the columnList
	 */
	public List<Column> getColumnList() {
		return this.columnList;
	}


	/**
	 * @param columnList the columnList to set
	 */
	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public boolean isIncludes() {
		return this.includes;
	}


	public void setIncludes(boolean includes) {
		this.includes = includes;
	}


	public List<Column> getIncludeColumnList() {
		return this.includeColumnList;
	}


	public void setIncludeColumnList(List<Column> includeColumnList) {
		this.includeColumnList = includeColumnList;
	}
}
