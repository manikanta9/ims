package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.DataFormat;
import org.apache.poi.ss.usermodel.DataValidation;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.HorizontalAlignment;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>DataTableToExcelFileConverter</code> converts a {@link DataTable} to an
 * Excel File
 *
 * @author manderson
 */
public class DataTableToExcelFileConverter implements DataTableToFileConverter {

	private static final int EXCEL_LIMIT = 65500; // Excel's Limit (65536)

	/**
	 * The name of a hidden sheet that contains validation information for the columns.
	 */
	private static final String HIDDEN_VALIDATION_SHEET_NAME = "hidden_validation_sheet";

	private String fileExtension = "xls";


	public DataTableToExcelFileConverter() {
		// nothing
	}


	public DataTableToExcelFileConverter(String fileExtension) {
		this.fileExtension = fileExtension;
	}


	@Override
	public File convert(DataTableFileConfig from) {
		if (from == null || from.isBlank()) {
			return null;
		}
		DataTable dataTable = from.getDataTable();
		int totalRows = dataTable.getTotalRowCount();
		if (totalRows > EXCEL_LIMIT && "xls".equals(this.fileExtension)) {
			throw new ValidationException("Cannot export more than " + EXCEL_LIMIT + " rows into Excel.");
		}

		// Auto Sizing Columns can be very slow for large files....depends on the number of rows and the number of columns
		Integer autoSizeColumnMax = getAutoSizeColumnMax(dataTable);

		Workbook workbook = ExcelFileUtils.createWorkbook(this.fileExtension);
		Map<String, CellStyle> cellStyleMap = new HashMap<>();

		Sheet sheet = workbook.getSheetAt(0);
		Sheet hidden = workbook.createSheet(HIDDEN_VALIDATION_SHEET_NAME);
		workbook.setSheetHidden(1, true);

		int numColumnsSoFarWithExplicitOptions = 0;
		int numRowsCreatedSoFar = 0;

		List<DataColumn> columnList = from.getColumnList();
		for (int i = 0; i < columnList.size(); i++) {
			DataColumn column = columnList.get(i);
			addCellHeaderValue(workbook, i, column, from, cellStyleMap);

			String[] validOptions = column.getValidOptions();

			/*
			 * If the column has an explicit list of valid options, then we need to set up the validation. Excel limits
			 * validation range strings to 255 characters, and this limit is reached very easily. The workaround for
			 * this is to create a hidden sheet and insert the actual valid data into the columns of that sheet, and
			 * then reference the columns from the "real" sheet with validation restrictions to the appropriate columns
			 * of the hidden sheet.
			 */
			if (validOptions != null && validOptions.length > 0) {

				for (int j = 0, length = validOptions.length; j < length; j++) {
					String validOption = validOptions[j];
					Row row;
					if (j < numRowsCreatedSoFar) {
						row = hidden.getRow(j);
					}
					else {
						row = hidden.createRow(numRowsCreatedSoFar++);
					}
					Cell cell = row.createCell(numColumnsSoFarWithExplicitOptions);
					cell.setCellValue(validOption);
				}

				String colName = CellReference.convertNumToColString(numColumnsSoFarWithExplicitOptions);
				String cellFormula = HIDDEN_VALIDATION_SHEET_NAME + "!$" + colName + "$1" + ":$" + colName + '$' + validOptions.length;

				DVConstraint constraint = DVConstraint.createFormulaListConstraint(cellFormula);
				CellRangeAddressList cellRanges = new CellRangeAddressList(1, 200, i, i);
				DataValidation validation = new HSSFDataValidation(cellRanges, constraint);
				validation.setSuppressDropDownArrow(false);
				sheet.addValidationData(validation);

				numColumnsSoFarWithExplicitOptions++;
			}

			for (int j = 0; j < totalRows; j++) {
				DataRow row = dataTable.getRow(j);
				addCellValue(workbook, j + 1, i, row.getValue(column), column, from, cellStyleMap);
			}
			// Because this method affects performance it should ONLY be called once per column after the data is entered
			if (autoSizeColumnMax == null || i < autoSizeColumnMax) {
				sheet.autoSizeColumn(i);
			}
		}
		return ExcelFileUtils.getFileFromWorkbook(workbook, "DataFile");
	}


	/**
	 * Based on the amount of data returned the auto sizing of columns can cause a significant performance hit
	 * This is a know issue with POI and is affected by the number of rows as well as the number of columns returned.
	 * So, based on the results, we try to determine how many columns, if any, we should auto size.
	 * See: DataTableToExcelFileConverterTests testPerformanceOfFileGeneration to view various scenarios that test is not enabled by default but can be used to view performance
	 *
	 * @return 0 if shouldn't auto size at all, null if no limit, or the max columns to auto size
	 */
	private Integer getAutoSizeColumnMax(DataTable dataTable) {
		// Anything over 10,000 rows 0 don't auto size
		if (dataTable.getTotalRowCount() >= 10000) {
			return 0;
		}
		// Anything over 5,000 rows - only auto size the first 5 columns
		if (dataTable.getTotalRowCount() >= 5000) {
			return 5;
		}

		// Anything over 1,000 rows - only auto size the first 10 columns
		if (dataTable.getTotalRowCount() >= 1000) {
			return 10;
		}
		// Otherwise no max
		return null;
	}


	/**
	 * Adds a new Header Cell for the given column at the specified index
	 *
	 * @param workbook
	 * @param columnIndex
	 * @param column
	 */
	private void addCellHeaderValue(Workbook workbook, int columnIndex, DataColumn column, DataTableFileConfig config, Map<String, CellStyle> cellStyleMap) {
		Cell cell = ExcelFileUtils.getCell(workbook.getSheetAt(0), 0, columnIndex, true);
		cell.setCellValue(column.getColumnName());
		if (!StringUtils.isEmpty(column.getDescription()) && !StringUtils.isEqual(column.getColumnName(), column.getDescription())) {
			ExcelFileUtils.createCellComment(workbook.getSheetAt(0), cell, column.getDescription());
		}
		setCellStyle(workbook, cell, column, true, config, cellStyleMap);
	}


	/**
	 * Adds a new Cell for the given row and column and value at the specified indexes
	 *
	 * @param workbook
	 * @param columnIndex
	 * @param column
	 */
	private void addCellValue(Workbook workbook, int rowIndex, int columnIndex, Object value, DataColumn column, DataTableFileConfig config, Map<String, CellStyle> cellStyleMap) {
		if (value == null) {
			return;
		}

		Row row = ExcelFileUtils.getRow(workbook.getSheetAt(0), rowIndex, true);
		Cell cell = ExcelFileUtils.getCell(workbook.getSheetAt(0), rowIndex, columnIndex, true);
		if (cell == null) {
			cell = row.createCell(columnIndex);
		}
		if (value instanceof Boolean) {
			cell.setCellValue((Boolean) value);
		}
		else if (value instanceof Date) {
			cell.setCellValue((Date) value);
		}
		else if (value instanceof Number) {
			cell.setCellValue(((Number) value).doubleValue());
		}
		else {
			cell.setCellValue(value.toString());
		}
		setCellStyle(workbook, cell, column, false, config, cellStyleMap);
	}


	/**
	 * Sets the appropriate Cell Style for the given cell and column.  Header indicates no wrapping
	 * and a bold value.
	 *
	 * @param workbook
	 * @param cell
	 * @param column
	 * @param header
	 */
	private void setCellStyle(Workbook workbook, Cell cell, DataColumn column, boolean header, DataTableFileConfig config, Map<String, CellStyle> cellStyleMap) {
		String style = config.getStyleString(column.getStyle(), header);
		if (cellStyleMap.containsKey(style)) {
			cell.setCellStyle(cellStyleMap.get(style));
			return;
		}

		DataColumnStyle styleObj = config.getDataColumnStyle(style);
		Font font = workbook.createFont();
		CellStyle cellStyle = workbook.createCellStyle();

		// Setup Font
		font.setFontHeightInPoints((short) 10);
		font.setColor(getColor(styleObj.getFontColor()));
		if (DataColumnStyle.FONT_WEIGHT_BOLD.equals(styleObj.getFontWeight())) {
			font.setBold(true);
		}

		if (!StringUtils.isEmpty(styleObj.getBackgroundColor())) {
			cellStyle.setFillPattern(FillPatternType.SOLID_FOREGROUND);
			cellStyle.setFillForegroundColor(getColor(styleObj.getBackgroundColor()));
		}

		// Setup Style
		cellStyle.setWrapText(styleObj.isWrapText());
		// Right Align & Center Align - auto set No Text Wrapping (Would be a number or date)
		if (DataColumnStyle.ALIGN_RIGHT.equals(styleObj.getAlign())) {
			cellStyle.setAlignment(HorizontalAlignment.RIGHT);
			cellStyle.setWrapText(false);
		}
		else if (DataColumnStyle.ALIGN_CENTER.equals(styleObj.getAlign())) {
			cellStyle.setAlignment(HorizontalAlignment.CENTER);
			cellStyle.setWrapText(false);
		}

		if (!StringUtils.isEmpty(styleObj.getDataType())) {
			DataFormat df = workbook.createDataFormat();
			DataTypes dt = DataTypes.valueOf(styleObj.getDataType());
			if (DataTypeNames.INTEGER == dt.getTypeName()) {
				cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_INTEGER));
			}
			else if (DataTypeNames.DATE == dt.getTypeName()) {
				cellStyle.setDataFormat(df.getFormat(DateUtils.DATE_FORMAT_INPUT));
			}
			else if (DataTypeNames.DECIMAL == dt.getTypeName()) {
				if (DataTypes.MONEY == dt) {
					cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_MONEY));
				}
				else if (DataTypes.MONEY4 == dt) {
					cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_MONEY4));
				}
				else if (dt.isPercent()) {
					cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_PERCENT));
				}
				else {
					cellStyle.setDataFormat(df.getFormat(MathUtils.NUMBER_FORMAT_DECIMAL));
				}
			}
			// Use TEXT formatting only if the column length is 50 or less.
			// This will work for fields like CUSIP where it's a number, but should be displayed
			// as text.  Anything longer, that could have a line break in it, the TEXT formatting
			// in Excel doesn't work and just gives ######### as the value.
			else if (DataTypeNames.STRING == dt.getTypeName() && dt.getLength() <= 50) {
				cellStyle.setDataFormat(df.getFormat("TEXT"));
			}
		}

		cellStyle.setFont(font);
		cellStyleMap.put(style, cellStyle);
		cell.setCellStyle(cellStyle);
	}


	/**
	 * Returns the short value for the given color name
	 *
	 * @param colorName
	 */
	private short getColor(String colorName) {
		colorName = colorName == null ? null : colorName.toUpperCase();
		if (StringUtils.isEmpty(colorName) || "BLACK".equals(colorName)) {
			return IndexedColors.BLACK.getIndex();
		}
		// Need to handle special cases since declared colors are different between PDF & Excel
		if ("cyan".equalsIgnoreCase(colorName)) {
			return IndexedColors.PALE_BLUE.getIndex();
		}
		return IndexedColors.valueOf(colorName).getIndex();
	}
}
