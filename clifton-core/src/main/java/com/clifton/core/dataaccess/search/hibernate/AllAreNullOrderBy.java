package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import com.clifton.core.dataaccess.search.OrderByDirections;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * The <code>AllAreNullOrderBy</code> is used for sorting by if all of the passed fields are null.
 * <p/>
 * For example, sorting by "global" in getComplianceRuleAssignmentList,
 * which only exists in the UI as a composite of the existence of other fields.
 * For this example, the resulting query would be
 * <code>IIF(COALESCE(this_.BusinessServiceID, this_.ClientAccountID) IS NULL, 1, 0) asc</code>.
 *
 * @author dillonm
 */
public class AllAreNullOrderBy extends Order {

	private final String[] propertyNames;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private AllAreNullOrderBy(String[] propertyNames, boolean ascending) {
		super(null, ascending);
		this.propertyNames = propertyNames;
	}


	public static AllAreNullOrderBy create(OrderByDirections direction, String... propertyNames) {
		return new AllAreNullOrderBy(propertyNames, direction == OrderByDirections.ASC);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder innerFragmentSb = new StringBuilder("COALESCE(");
		boolean hasContent = false;
		for (String propertyName : this.propertyNames) {
			String column = criteriaQuery.getColumn(criteria, propertyName);
			if (hasContent) {
				innerFragmentSb.append(", ");
			}
			innerFragmentSb.append(column);
			if (!hasContent) {
				hasContent = true;
			}
		}
		innerFragmentSb.append(") IS Null");
		String innerSql = innerFragmentSb.toString();
		StringBuilder fragmentSb = new StringBuilder();
		fragmentSb.append(SQLIfElseFunction.render(criteria, criteriaQuery, innerSql, "1", "0"));
		fragmentSb.append(super.isAscending() ? "asc" : "desc");
		return fragmentSb.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getPropertyNames() {
		return this.propertyNames;
	}
}
