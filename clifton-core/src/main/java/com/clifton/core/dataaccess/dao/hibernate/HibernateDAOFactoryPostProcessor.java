package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.dataaccess.migrate.reader.XmlMigrationHelper;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.AbstractApplicationContext;
import org.springframework.context.weaving.AspectJWeavingEnabler;
import org.springframework.core.Ordered;
import org.springframework.core.PriorityOrdered;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.beans.Introspector;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * The {@link BeanFactoryPostProcessor} responsible for registering {@link ReadOnlyDAO DAO} beans for schema definitions.
 * <p>
 * This bean factory post processor loads all schema files for the configured {@link #schemaNameList schema list}. Schema files are then scanned for relevant tables. A DAO bean
 * definition is then registered in the bean factory for each table.
 * <p>
 * This class also registers {@link DAOConfiguration} beans for each table. This configuration contains information which is used in the DAO. These configurations are also used to
 * identify DTOs which should have caching enabled via the {@link CacheByName} annotation. These configuration beans are instantiated early so that their properties may be used by
 * the {@link DtoCacheByNameFactoryPostProcessor}. Since they are instantiated early, (i.e., during the bean factory post processor phase), they should not require autowired
 * properties.
 * <p>
 * This post processor must be executed before the {@link DtoCacheByNameFactoryPostProcessor}.
 *
 * @author vgomelsky
 */
public class HibernateDAOFactoryPostProcessor implements BeanFactoryPostProcessor, Ordered {

	private String[] schemaNameList;
	/**
	 * The class name for the DAOs to generate for each schema.
	 * <p>
	 * Note: <i>The value for this property should not be set such that it requires class-loading.</i> Class-loading on entity instantiation for this type will precede the
	 * activation of AspectJ weaving and thus will not be weaved.
	 * <p>
	 * AspectJ weaving is enabled via the {@link AspectJWeavingEnabler} {@link BeanFactoryPostProcessor}. Load-time weaving means that classes are weaved as they are loaded via the
	 * classloader. The AspectJ enabler assigns a modified classloader that performs weaving. This means that any classes loaded before {@link AspectJWeavingEnabler} is executed
	 * will not be weaved.
	 * <p>
	 * For the {@link AbstractApplicationContext#invokeBeanFactoryPostProcessors(org.springframework.beans.factory.config.ConfigurableListableBeanFactory) default application
	 * context}, bean factory post processors are loaded in groups. These groups are segregated by precedence and loaded in the following order:
	 * <ol>
	 * <li>{@link BeanDefinitionRegistryPostProcessor} (assigned directly to the {@link ApplicationContext})
	 * <li>{@link BeanDefinitionRegistryPostProcessor} ({@link PriorityOrdered})
	 * <li>{@link BeanDefinitionRegistryPostProcessor} ({@link Ordered})
	 * <li>{@link BeanDefinitionRegistryPostProcessor} (no order set)
	 * <li>{@link BeanFactoryPostProcessor} (assigned directly to the {@link ApplicationContext})
	 * <li>{@link BeanFactoryPostProcessor} ({@link PriorityOrdered})
	 * <li>{@link BeanFactoryPostProcessor} ({@link Ordered})
	 * <li>{@link BeanFactoryPostProcessor} (no order set)
	 * </ol>
	 * Every bean in a group will be instantiated before any beans in that group are executed. Since this bean factory post processor and the {@link AspectJWeavingEnabler} are both
	 * in the {@link Ordered} group, performing class-loading during bean instantiation in this class will preclude class weaving for loaded classes.
	 * <p>
	 * See {@link org.springframework.context.support.PostProcessorRegistrationDelegate#invokeBeanFactoryPostProcessors(org.springframework.beans.factory.config.ConfigurableListableBeanFactory, java.util.List<org.springframework.beans.factory.config.BeanFactoryPostProcessor>)
	 * PostProcessorRegistrationDelegate#invokeBeanFactoryPostProcessors} for more information.
	 */
	private String daoClassName;
	/**
	 * Only add DAO's with names that match this pattern.  By default match everything.
	 */
	private String daoNamePattern = ".*";
	private String schemaFilePrefix = "META-INF/schema/schema-clifton-";
	private String schemaFileSuffix = ".xml";

	/**
	 * Use to inject DAO's with non-default session factory instances (different DB's, etc.).
	 * Key is table's dataSource name and value is corresponding Hibernate sessionFactory bean name.
	 */
	private Map<String, String> dataSourceToSessionFactoryMapping = new HashMap<>();
	/**
	 * Use to inject all DAO's for a schema with non-default session factory instances (different DB's, etc.).
	 * Key is schema name and value is corresponding Hibernate sessionFactory bean name. Data source name
	 * overrides this setting.
	 */
	private Map<String, String> schemaNameToSessionFactoryMapping = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		// This must be executed before DtoCacheByNameFactoryPostProcessor
		return 0;
	}


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// Assign default during process phase to delay class-loading until the load-time weaver has been processed
		if (getDaoClassName() == null) {
			setDaoClassName(HibernateUpdatableDAO.class.getName());
		}
		generateDaoBeanDefinitions((DefaultListableBeanFactory) beanFactory);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void generateDaoBeanDefinitions(DefaultListableBeanFactory beanFactory) {
		XmlMigrationHelper xmlHelper = new XmlMigrationHelper();
		Pattern compiledDaoNamePattern = Pattern.compile(getDaoNamePattern());
		ResourceLoader loader = new DefaultResourceLoader();
		List<String> uniqueSchemaNameList = ArrayUtils.getStream(getSchemaNameList())
				.distinct()
				.sorted()
				.collect(Collectors.toList());
		for (String schemaName : uniqueSchemaNameList) {
			// Allow missing schema files (schema names will typically be projects, and not all projects have schemas)
			String fileName = getSchemaFilePrefix() + schemaName + getSchemaFileSuffix();
			Resource resource = loader.getResource(fileName);
			if (!resource.exists()) {
				LogUtils.info(getClass(), String.format("Skipping schema metadata loading for schema name [%s]. The schema metadata file does not exist.", schemaName));
				continue;
			}

			String schemaSessionFactoryBeanName = getSchemaNameToSessionFactoryMapping().get(schemaName);
			Schema schema = xmlHelper.loadFromXml(fileName);
			long daoCount = CollectionUtils.getStream(schema.getTableList())
					.filter(table -> compiledDaoNamePattern.matcher(table.getDaoName()).matches())
					.peek(table -> generateDaoBeanDefinition(beanFactory, table, schemaSessionFactoryBeanName))
					.count();
			if (daoCount > 0) {
				LogUtils.info(getClass(), String.format("Registered [%d] DAO components for schema [%s]", daoCount, schemaName));
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void generateDaoBeanDefinition(DefaultListableBeanFactory beanFactory, Table table, String sessionFactoryBeanName) {
		// Guard-clause: Do not repeat registrations
		String daoBeanName = table.getDaoName();
		if (beanFactory.containsBeanDefinition(daoBeanName)) {
			return;
		}

		try {
			// Register DAO configuration definition
			String daoConfigBeanName = generateDaoConfigBeanDefinition(beanFactory, table);

			// Generate bean definition
			String appliedDaoClassName = getDaoClassName();
			if (table.isBatchEnabled() && appliedDaoClassName.equals(HibernateUpdatableDAO.class.getName())) {
				appliedDaoClassName = HibernateUpdatableBatchDAO.class.getName();
			}
			else if (table.isTemplateEnabled()) {
				appliedDaoClassName = table.getDaoClassName();
			}

			BeanDefinitionBuilder daoBeanDefinitionBuilder = BeanDefinitionBuilder.genericBeanDefinition(appliedDaoClassName)
					.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME)
					.setLazyInit(true)
					.addPropertyReference("configuration", daoConfigBeanName);
			if (getDataSourceToSessionFactoryMapping().containsKey(table.getDataSource())) {
				daoBeanDefinitionBuilder.addPropertyReference("sessionFactory", getDataSourceToSessionFactoryMapping().get(table.getDataSource()));
			}
			else if (sessionFactoryBeanName != null) {
				daoBeanDefinitionBuilder.addPropertyReference("sessionFactory", sessionFactoryBeanName);
			}
			BeanDefinition daoBeanDefinition = daoBeanDefinitionBuilder.getBeanDefinition();

			// Register bean definition
			beanFactory.registerBeanDefinition(daoBeanName, daoBeanDefinition);
			LogUtils.debug(getClass(), String.format("Registered DAO: %s", daoBeanName));
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Error creating DAO bean definitions for table [%s]", table.getName()), e);
		}
	}


	private String generateDaoConfigBeanDefinition(DefaultListableBeanFactory beanFactory, Table table) {
		// Guard-clause: Do not repeat registrations
		String daoConfigBeanName = Introspector.decapitalize(table.getName()) + "DaoConfig";
		if (beanFactory.containsBeanDefinition(daoConfigBeanName)) {
			return daoConfigBeanName;
		}

		// Register DAO configuration bean definition
		AbstractBeanDefinition daoConfigBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(DAOConfiguration.class)
				// Disable autowiring to prevent following dependency chains during early instantiation
				.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_NO)
				.setLazyInit(true)
				.addConstructorArgValue(table)
				.getBeanDefinition();
		beanFactory.registerBeanDefinition(daoConfigBeanName, daoConfigBeanDefinition);
		return daoConfigBeanName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getSchemaNameList() {
		return this.schemaNameList;
	}


	public void setSchemaNameList(String[] schemaNameList) {
		this.schemaNameList = schemaNameList;
	}


	public String getDaoNamePattern() {
		return this.daoNamePattern;
	}


	public void setDaoNamePattern(String daoNamePattern) {
		this.daoNamePattern = daoNamePattern;
	}


	public String getDaoClassName() {
		return this.daoClassName;
	}


	public void setDaoClassName(String daoClassName) {
		this.daoClassName = daoClassName;
	}


	public String getSchemaFilePrefix() {
		return this.schemaFilePrefix;
	}


	public void setSchemaFilePrefix(String schemaFilePrefix) {
		this.schemaFilePrefix = schemaFilePrefix;
	}


	public String getSchemaFileSuffix() {
		return this.schemaFileSuffix;
	}


	public void setSchemaFileSuffix(String schemaFileSuffix) {
		this.schemaFileSuffix = schemaFileSuffix;
	}


	public Map<String, String> getDataSourceToSessionFactoryMapping() {
		return this.dataSourceToSessionFactoryMapping;
	}


	public void setDataSourceToSessionFactoryMapping(Map<String, String> dataSourceToSessionFactoryMapping) {
		this.dataSourceToSessionFactoryMapping = dataSourceToSessionFactoryMapping;
	}


	public Map<String, String> getSchemaNameToSessionFactoryMapping() {
		return this.schemaNameToSessionFactoryMapping;
	}


	public void setSchemaNameToSessionFactoryMapping(Map<String, String> schemaNameToSessionFactoryMapping) {
		this.schemaNameToSessionFactoryMapping = schemaNameToSessionFactoryMapping;
	}
}
