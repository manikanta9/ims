package com.clifton.core.dataaccess.migrate.schema;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonMigrationActionStrategy;
import com.clifton.core.converter.json.jackson.strategy.JacksonMigrationStrategy;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.MigrationUtils;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.action.Argument;
import com.clifton.core.dataaccess.migrate.schema.search.ColumnSearchForm;
import com.clifton.core.dataaccess.migrate.schema.search.TableSearchForm;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;


/**
 * @author manderson
 */
@Service
public class MigrationSchemaServiceImpl implements MigrationSchemaService, ApplicationContextAware {

	private static Map<String, Action> actionMap;

	private ApplicationContextService applicationContextService;

	private DaoLocator daoLocator;
	private JsonHandler<JacksonMigrationStrategy> jsonHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ApplicationContext applicationContext;


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getJsonMigrationAction(String entityTableName, Integer entityFkFieldId) {
		return getJsonMigrationAction(entityTableName, entityFkFieldId, false);
	}


	@Override
	public String getJsonMigrationAction(String entityTableName, Integer entityFkFieldId, boolean xmlActionData) {
		ReadOnlyDAO<?> dao = getDaoLocator().locate(entityTableName);
		IdentityObject causeEntity = dao.findByPrimaryKey(entityFkFieldId);

		// populate properties of lists that are by default not loaded.
		getApplicationContextService().populateManySideDependencies(causeEntity);

		//Only populate once, should only never change once created
		if (actionMap == null) {
			populateActionMap();
		}

		List<Argument> argumentList = new ArrayList<>();
		argumentList.add(new Argument(causeEntity));
		Action action = actionMap.get(dao.getConfiguration().getTable().getDtoClass());
		if (action == null) {
			throw new ValidationException("Cannot find action for DTO Class: " + dao.getConfiguration().getTable().getDtoClass());
		}
		action.setArgumentList(argumentList);

		//if xmlActionData is true, then we produce the actual XML migration action data; this is used when exporting grids as migration data.
		if (xmlActionData) {
			StringBuilder jsonAction = new StringBuilder();
			jsonAction.append("<action bean=\"").append(action.getBean()).append("\" method=\"").append(action.getMethod()).append("\">\n");
			for (Argument argument : CollectionUtils.getIterable(action.getArgumentList())) {
				jsonAction.append("\t<arg><![CDATA[\n");
				jsonAction.append(getJsonHandler().toJson(argument.getObjectValue(), new JacksonMigrationActionStrategy())).append("\n");
				jsonAction.append("\t]]></arg>\n");
			}
			jsonAction.append("</action>\n");
			return jsonAction.toString();
		}
		//Otherwise we serialize the object as is; this is used on the 'Object Data' tab from the info screens in the UI.
		return getJsonHandler().toJson(action, new JacksonMigrationStrategy());
	}


	private synchronized void populateActionMap() {
		actionMap = MigrationUtils.populateActionMap(getApplicationContext());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getMigrationParentTableName(String tableName) {
		List<DAOConfiguration<?>> configurationList = getDaoConfigurationList(tableName);
		for (DAOConfiguration<?> config : CollectionUtils.getIterable(configurationList)) {
			for (Column column : CollectionUtils.getIterable(config.getColumnList())) {
				if (column.isParentField()) {
					return column.getFkTable();
				}
			}
		}
		return null;
	}


	@Override
	@SuppressWarnings("rawtypes")
	public List<Table> getTableList() {
		Map<String, ReadOnlyDAO> daoMap = getApplicationContextService().getContextBeansOfType(ReadOnlyDAO.class);
		List<ReadOnlyDAO> daoList = new ArrayList<>(daoMap.values());
		return Arrays.asList(BeanUtils.getPropertyValues(daoList, "configuration.table", Table.class, true));
	}


	@Override
	public List<Table> getTableList(TableSearchForm searchForm) {
		SearchFormUtils.convertSearchRestrictionList(searchForm);

		List<Table> tableList = new ArrayList<>();

		List<DAOConfiguration<?>> configurationList = getDaoConfigurationList(null);
		for (DAOConfiguration<?> daoConfiguration : CollectionUtils.getIterable(configurationList)) {
			applyTableFilters(daoConfiguration, searchForm, tableList);
		}

		SearchFormUtils.applySortOrder(tableList, searchForm);

		return tableList;
	}


	private void applyTableFilters(DAOConfiguration<?> daoConfiguration, TableSearchForm searchForm, List<Table> tableList) {
		Table table = daoConfiguration.getTable();

		if (!SearchFormUtils.applyStringFilter(table.getName(), searchForm.getSearchPattern())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getDataSource(), searchForm.getDataSource())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getName(), searchForm.getName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getAuditType(), searchForm.getAuditType())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getCacheType() == null ? null : table.getCacheType().name(), searchForm.getCacheType())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getTableType() == null ? null : table.getTableType().name(), searchForm.getTableType())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getDaoName(), searchForm.getDaoName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getDtoClass(), searchForm.getDtoClass())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getDtoProxy(), searchForm.getDtoProxy())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(table.getUploadAllowed(), searchForm.getUploadAllowed())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getUploadServiceBeanName(), searchForm.getUploadServiceBeanName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getUploadServiceMethodName(), searchForm.getUploadServiceBeanName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getDiscriminatorFormula(), searchForm.getDiscriminatorFormula())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(table.getDiscriminatorValue(), searchForm.getDiscriminatorValue())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(table.getExcludeFromSql(), searchForm.getExcludeFromSql())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilterOrNull(table.getVersion(), searchForm.getVersion())) {
			return;
		}

		if (BooleanUtils.isTrue(searchForm.getVersionSupported())) {
			// If version is enabled, then it's supported
			if (!BooleanUtils.isTrue(table.getVersion())) {
				// Otherwise return if there is no rv column
				ColumnSearchForm columnSearchForm = new ColumnSearchForm();
				columnSearchForm.setTableName(table.getName());
				columnSearchForm.setBeanPropertyName("rv");
				if (CollectionUtils.isEmpty(getColumnList(columnSearchForm))) {
					return;
				}
			}
		}

		// If passed all filters - add the table to the list
		tableList.add(table);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<Column> getColumnList(ColumnSearchForm searchForm) {
		SearchFormUtils.convertSearchRestrictionList(searchForm);

		List<Column> columnList = new ArrayList<>();
		List<DAOConfiguration<?>> configurationList = getDaoConfigurationList(searchForm.getTableName());
		for (DAOConfiguration<?> daoConfiguration : CollectionUtils.getIterable(configurationList)) {
			applyTableColumnFilters(daoConfiguration, searchForm, columnList);
		}
		return columnList;
	}


	private void applyTableColumnFilters(DAOConfiguration<?> daoConfiguration, ColumnSearchForm searchForm, List<Column> columnList) {
		Table table = daoConfiguration.getTable();

		// Table Level Filters - Besides Name - anything else?
		if (SearchFormUtils.applyStringFilter(table.getName(), searchForm.getTableNameContains())) {
			for (Column column : CollectionUtils.getIterable(daoConfiguration.getColumnList())) {
				// Need to ensure this is populated for viewing from UI
				column.setTableName(table.getName());
				applyColumnFilters(column, searchForm, columnList);
			}
		}
	}


	private void applyColumnFilters(Column column, ColumnSearchForm searchForm, List<Column> columnList) {
		if (!SearchFormUtils.applyStringFilter(column.getName(), searchForm.getSearchPattern())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getBeanPropertyName(), searchForm.getBeanPropertyName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getDataType() == null ? null : column.getDataType().name(), searchForm.getDataType())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getRequired(), searchForm.getRequired())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getRequiredForValidation(), searchForm.getRequiredForValidation())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getDefaultValue(), searchForm.getDefaultValue())) {
			return;
		}
		// NOTE: INTEGER FILTERS sortOrder is currently disabled - doesn't seem very important and would need to figure out less than, greater than, equal comparison in order to filter
		// Sorting is still allowed for this column though.

		if (!SearchFormUtils.applyStringFilter(column.getSortDirection() == null ? null : column.getSortDirection().name(), searchForm.getSortDirection())) {
			return;
		}

		if (!SearchFormUtils.applyStringFilter(column.getUploadLookupServiceBeanName(), searchForm.getUploadLookupServiceBeanName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getUploadLookupServiceMethodName(), searchForm.getUploadLookupServiceMethodName())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.isParentField(), searchForm.getParentField())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getIndexed(), searchForm.getIndexed())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.isUniqueIndex(), searchForm.getUniqueIndex())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getFkTable(), searchForm.getFkTable())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getFkField(), searchForm.getFkField())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getLazy(), searchForm.getLazy())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.isNaturalKey(), searchForm.getNaturalKey())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getIgnoreUpload(), searchForm.getIgnoreUpload())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.isDelayed(), searchForm.getDelayed())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getForceSimpleProperty(), searchForm.getForceSimpleProperty())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getColumnExcludedFromInserts(), searchForm.getExcludedFromInserts())) {
			return;
		}
		if (!SearchFormUtils.applyBooleanFilter(column.getColumnExcludedFromUpdates(), searchForm.getExcludedFromUpdates())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getTypeDefinitionName(), searchForm.getTypeDefinitionName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getTypeDefinitionClassName(), searchForm.getTypeDefinitionClassName())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getTypeDefinitionConverterClass(), searchForm.getTypeDefinitionConverterClass())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getAuditType(), searchForm.getAuditType())) {
			return;
		}
		if (!SearchFormUtils.applyStringFilter(column.getCalculation(), searchForm.getCalculation())) {
			return;
		}
		columnList.add(column);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<DAOConfiguration<?>> getDaoConfigurationList(String tableName) {
		List<DAOConfiguration<?>> configurationList = new ArrayList<>();
		if (!StringUtils.isEmpty(tableName)) {
			ReadOnlyDAO<?> dao = getDaoLocator().locate(tableName);
			if (!(dao.getConfiguration() instanceof NoTableDAOConfig)) {
				configurationList.add(dao.getConfiguration());
			}
		}
		else {
			Collection<ReadOnlyDAO<?>> daoList = getDaoLocator().locateAll();
			for (ReadOnlyDAO<?> dao : CollectionUtils.getIterable(daoList)) {
				if (!(dao.getConfiguration() instanceof NoTableDAOConfig)) {
					configurationList.add(dao.getConfiguration());
				}
			}
		}
		return configurationList;
	}

	////////////////////////////////////////////////////////////////
	/////////          Getter and Setter Methods          //////////
	////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public JsonHandler<JacksonMigrationStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JacksonMigrationStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}
}
