package com.clifton.core.dataaccess.migrate.locator;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.util.StringUtils;

import java.util.Date;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>MigrationModuleVersion</code> holds all the DDL SQL, META DATA SQL, AND Action List
 * for a specific migration module version.
 *
 * @author manderson
 */
public class MigrationModuleVersion extends BaseSimpleEntity<Integer> {

	// Persisted Values
	private int versionNumber;
	private String migrationModuleName;
	private String migrationPath;
	private boolean preDdlStepCompleted;
	private boolean ddlStepCompleted;
	private boolean metaDataStepCompleted;
	private boolean dataStepCompleted;
	private boolean actionStepCompleted;
	private boolean dataActionStepCompleted;
	private boolean sqlStepCompleted;
	private boolean skippedDueToConditionalSql;
	private Date initialExecutionDate;
	private Date lastExecutionDate;

	// If true, this migration version was in the database when this migration started
	@NonPersistentField
	private boolean existingMigrationVersion;
	@NonPersistentField
	private boolean doNotStartTransaction;
	@NonPersistentField
	private String conditionalSql;

	// If no conditional sql, will return true.
	// Otherwise, if NULL assumes conditional sql not evaluated and will evaluate it.  If true, condition SQL returned results so skip file,
	// if false, conditional SQL didn't return results so process this file.
	@NonPersistentField
	private Boolean executeVersion;

	@NonPersistentField
	private Map<MigrationSchemaConverterTypes, String> migrationSqlMap;
	@NonPersistentField
	private List<Action> actionList;

	/**
	 * A map of 'Action' objects with a key of 'className'
	 * <p>
	 * Allows a user to specify a specific action to take for an object of a specific class.
	 * <p>
	 * e.g.
	 * <p>
	 * Action action = new Action();
	 * action.setBean("systemConditionService");
	 * action.setMethod("saveSystemCondition");
	 * actionMap.put("com.clifton.system.condition.SystemCondition", action);
	 * <p>
	 * Is synonymous with:
	 * <action bean="systemConditionService" method="saveSystemCondition">
	 * <arg className="com.clifton.system.condition.SystemCondition">
	 * ...
	 * </arg>
	 * </action>
	 * <p>
	 * (TODO) update with class name once sub branch is merged.
	 * With the exception that the argument data that will be passed is the object that matched
	 * the key className while walking the object - see 'handleObject'
	 * <p>
	 * (TODO) update test and json after this and sub branch are merged
	 * See 'SystemSchemaJsonMigrationTests' and '0001-json-migration-test.xml' for example usage.
	 */
	@NonPersistentField
	private Map<String, Action> actionMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void markTypeComplete(MigrationSchemaConverterTypes type) {
		switch (type) {
			case PRE_DDL:
				setPreDdlStepCompleted(true);
				break;
			case DDL:
				setDdlStepCompleted(true);
				break;
			case META_DATA:
				setMetaDataStepCompleted(true);
				break;
			case DATA:
				setDataStepCompleted(true);
				break;
			case ACTION:
				setActionStepCompleted(true);
				break;
			case DATA_ACTION:
				setDataActionStepCompleted(true);
				break;
			case SQL:
				setSqlStepCompleted(true);
				break;
			default:
				throw new IllegalStateException("Migration Modules version not defined for " + type);
		}
		Date now = new Date();
		if (getInitialExecutionDate() == null) {
			setInitialExecutionDate(now);
		}
		setLastExecutionDate(now);
	}


	public boolean isStepCompletedForType(MigrationSchemaConverterTypes type) {
		switch (type) {
			case PRE_DDL:
				return isPreDdlStepCompleted();
			case DDL:
				return isDdlStepCompleted();
			case META_DATA:
				return isMetaDataStepCompleted();
			case DATA:
				return isDataStepCompleted();
			case ACTION:
				return isActionStepCompleted();
			case DATA_ACTION:
				return isDataActionStepCompleted();
			case SQL:
				return isSqlStepCompleted();
			default:
				throw new IllegalStateException("Migration Module version not defined for " + type);
		}
	}


	public boolean isComplete() {
		return this.preDdlStepCompleted && this.ddlStepCompleted && this.metaDataStepCompleted && this.dataStepCompleted && this.actionStepCompleted && this.dataActionStepCompleted && this.sqlStepCompleted;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addMigrationSql(MigrationSchemaConverterTypes type, String sql) {
		if (this.migrationSqlMap == null) {
			this.migrationSqlMap = new EnumMap<>(MigrationSchemaConverterTypes.class);
		}
		this.migrationSqlMap.put(type, sql);
	}


	public String getMigrationSql(MigrationSchemaConverterTypes type) {
		String str = null;
		if (this.migrationSqlMap != null) {
			str = this.migrationSqlMap.get(type);
		}
		return StringUtils.coalesce(true, str);
	}


	public Boolean getExecuteVersion() {
		if (StringUtils.isEmpty(getConditionalSql())) {
			return true;
		}
		return this.executeVersion;
	}


	public int getVersionNumber() {
		return this.versionNumber;
	}


	public void setVersionNumber(int versionNumber) {
		this.versionNumber = versionNumber;
	}


	public String getMigrationModuleName() {
		return this.migrationModuleName;
	}


	public void setMigrationModuleName(String migrationModuleName) {
		this.migrationModuleName = migrationModuleName;
	}


	public String getMigrationPath() {
		return this.migrationPath;
	}


	public void setMigrationPath(String migrationPath) {
		this.migrationPath = migrationPath;
	}


	public boolean isSkippedDueToConditionalSql() {
		return this.skippedDueToConditionalSql;
	}


	public void setSkippedDueToConditionalSql(boolean skippedDueToConditionalSql) {
		this.skippedDueToConditionalSql = skippedDueToConditionalSql;
	}


	public Date getInitialExecutionDate() {
		return this.initialExecutionDate;
	}


	public void setInitialExecutionDate(Date initialExecutionDate) {
		this.initialExecutionDate = initialExecutionDate;
	}


	public Date getLastExecutionDate() {
		return this.lastExecutionDate;
	}


	public void setLastExecutionDate(Date lastExecutionDate) {
		this.lastExecutionDate = lastExecutionDate;
	}


	public boolean isPreDdlStepCompleted() {
		return this.preDdlStepCompleted;
	}


	public void setPreDdlStepCompleted(boolean preDdlStepCompleted) {
		this.preDdlStepCompleted = preDdlStepCompleted;
	}


	public boolean isDdlStepCompleted() {
		return this.ddlStepCompleted;
	}


	public void setDdlStepCompleted(boolean ddlStepCompleted) {
		this.ddlStepCompleted = ddlStepCompleted;
	}


	public boolean isMetaDataStepCompleted() {
		return this.metaDataStepCompleted;
	}


	public void setMetaDataStepCompleted(boolean metaDataStepCompleted) {
		this.metaDataStepCompleted = metaDataStepCompleted;
	}


	public boolean isDataStepCompleted() {
		return this.dataStepCompleted;
	}


	public void setDataStepCompleted(boolean dataStepCompleted) {
		this.dataStepCompleted = dataStepCompleted;
	}


	public boolean isDataActionStepCompleted() {
		return this.dataActionStepCompleted;
	}


	public void setDataActionStepCompleted(boolean dataActionStepCompleted) {
		this.dataActionStepCompleted = dataActionStepCompleted;
	}


	public boolean isActionStepCompleted() {
		return this.actionStepCompleted;
	}


	public void setActionStepCompleted(boolean actionStepCompleted) {
		this.actionStepCompleted = actionStepCompleted;
	}


	public boolean isSqlStepCompleted() {
		return this.sqlStepCompleted;
	}


	public void setSqlStepCompleted(boolean sqlStepCompleted) {
		this.sqlStepCompleted = sqlStepCompleted;
	}


	public List<Action> getActionList() {
		return this.actionList;
	}


	public void setActionList(List<Action> actionList) {
		this.actionList = actionList;
	}


	public boolean isDoNotStartTransaction() {
		return this.doNotStartTransaction;
	}


	public void setDoNotStartTransaction(boolean doNotStartTransaction) {
		this.doNotStartTransaction = doNotStartTransaction;
	}


	public String getConditionalSql() {
		return this.conditionalSql;
	}


	public void setConditionalSql(String conditionalSql) {
		this.conditionalSql = conditionalSql;
	}


	public void setExecuteVersion(Boolean executeVersion) {
		this.executeVersion = executeVersion;
	}


	public Map<String, Action> getActionMap() {
		return this.actionMap;
	}


	public void setActionMap(Map<String, Action> actionMap) {
		this.actionMap = actionMap;
	}


	public boolean isExistingMigrationVersion() {
		return this.existingMigrationVersion;
	}


	public void setExistingMigrationVersion(boolean existingMigrationVersion) {
		this.existingMigrationVersion = existingMigrationVersion;
	}
}
