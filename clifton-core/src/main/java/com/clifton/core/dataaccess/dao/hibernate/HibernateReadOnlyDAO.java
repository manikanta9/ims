package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.BaseObserverableDAOImpl;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.search.grouping.AdvancedReadOnlyWithGroupingDAO;
import com.clifton.core.dataaccess.search.grouping.GroupedEntityResult;
import com.clifton.core.dataaccess.search.grouping.GroupedHierarchicalResult;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.LockOptions;
import org.hibernate.Query;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.spi.EntityEntry;
import org.hibernate.engine.spi.EntityKey;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.persister.entity.EntityPersister;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>HibernateReadOnlyDAO</code> class provides DAO implementation based on Hibernate.
 *
 * @author vgomelsky
 */

public class HibernateReadOnlyDAO<T extends IdentityObject> extends BaseObserverableDAOImpl<T> implements AdvancedReadOnlyDAO<T, Criteria>, HibernateSessionAware<T>, AdvancedReadOnlyWithGroupingDAO<T> {

	public static final int PROPOSED_MAX_QUERY_LIMIT = 100_000;

	private SessionFactory sessionFactory;

	////////////////////////////////////////////////////////////////////////////
	////////                  ReadOnlyDAO Methods                  /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T findByNaturalKey(final T searchObject) {
		Set<String> naturalKeys = getConfiguration().getNaturalKeys((DaoLocator) getApplicationContext().getBean("daoLocator"));
		if (naturalKeys != null) {
			int i = 0;
			String[] keys = new String[naturalKeys.size()];
			for (String naturalKey : naturalKeys) {
				String key = naturalKey;
				if (naturalKey.contains(".") && !naturalKey.contains(".id")) {
					key = naturalKey.substring(0, naturalKey.lastIndexOf(".")) + ".id";
				}
				keys[i] = key;
				i++;
			}
			return findOneByFields(keys, BeanUtils.getPropertyValues(searchObject, keys));
		}
		throw new IllegalArgumentException(searchObject + " object does not have any natural key(s) defined for " + this);
	}


	@Override
	@Transactional(readOnly = true)
	public T findByPrimaryKey(final Serializable key) {
		Object entity;
		Session session = getSessionFactory().getCurrentSession();
		final Serializable primaryKey = convertToPrimaryKeyDataType(key);
		// when detached mode is used, retrieve ORIGINAL object (before any updates) and make sure it's disconnected from Hibernate session
		if (DaoUtils.isDetachedModeEnabled()) {
			// NOTE: this code was obtained by reverse engineering Hibernate source code. It needs to be revisited after each Hibernate upgrade
			// retrieve the object from Hibernate first level (session) cache
			SharedSessionContractImplementor sessionImplementor = (SharedSessionContractImplementor) session;
			EntityPersister persister = sessionImplementor.getFactory().getMetamodel().entityPersister(getConfiguration().getBeanClass());
			EntityKey entityKey = sessionImplementor.generateEntityKey(primaryKey, persister);
			Object sessionObject = sessionImplementor.getEntityUsingInterceptor(entityKey);

			// if the object is not in session level cache, then perform regular lookup (unmodified)
			if (sessionObject != null) {
				// Hibernate creates a copy of each object in session when that object is retrieved.
				// It uses this copy to check if session object was modified: compares each property.
				// Create a new disconnected instance from the same copy
				EntityEntry entry = sessionImplementor.getPersistenceContext().getEntry(sessionObject);
				ClassMetadata metaData = (ClassMetadata) sessionImplementor.getFactory().getMetamodel().entityPersister(sessionObject.getClass());
				entity = metaData.instantiate(primaryKey, sessionImplementor);
				metaData.setPropertyValues(entity, entry.getLoadedState());
			}
			else {
				// now that we know that session cache doesn't have our object: perform a database lookup
				entity = session.get(getConfiguration().getBeanClass(), primaryKey, LockOptions.READ);
				if (entity != null && DaoUtils.isEvictFromSession()) {
					session.evict(entity);
				}
			}
		}
		else {
			entity = session.get(getConfiguration().getBeanClass(), primaryKey);
		}
		@SuppressWarnings("unchecked")
		T castEntity = (T) entity;
		return castEntity;
	}


	@Override
	public T findOneByField(String beanFieldName, Object value) {
		return findOneByFields(new String[]{beanFieldName}, new Object[]{value});
	}


	@Override
	public T findOneByFields(String[] beanFieldNames, Object[] values) {
		List<T> list = findByFields(beanFieldNames, values, false);

		int count = CollectionUtils.getSize(list);
		if (count == 0) {
			return null;
		}
		if (count > 1) {
			throw new IllegalStateException("The query for " + this + " with parameters (" + ArrayUtils.toString(beanFieldNames) + ") and values (" + ArrayUtils.toString(values)
					+ ") cannot return more than 1 row; count = " + count);
		}
		return list.get(0);
	}


	@Override
	public List<T> findByPrimaryKeys(Serializable[] ids) {
		if (ids == null || ids.length == 0) {
			return null;
		}

		// If Only 1, Use Find By Primary Key on the One ID
		if (ids.length == 1) {
			return CollectionUtils.createList(findByPrimaryKey(ids[0]));
		}
		// Else if Hibernate Caching is Enabled, use Find By Primary Key on Each ID
		// Significantly faster then one DB call for all where ID IN (...)
		if (getConfiguration().isCachingEnabled()) {
			return findByPrimaryKeysOneByOne(ids);
		}
		// Otherwise, no caching enabled - use the Find By Field for all where ID IN (...)
		return findByField("id", ids);
	}


	/**
	 * Wrapped in a transaction to improve performance for multiple look ups (starting a new transaction is a lot more expensive than enrolling into an existing one)
	 */
	@Transactional(readOnly = true)
	protected List<T> findByPrimaryKeysOneByOne(Serializable[] ids) {
		List<T> beanList = new ArrayList<>();
		for (Serializable id : ids) {
			beanList.add(findByPrimaryKey(id));
		}
		return beanList;
	}


	@Override
	@Transactional(readOnly = true)
	public List<T> findAll() {
		try {
			Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(getConfiguration().getBeanClass());
			configureOrderBy(criteria);
			@SuppressWarnings("unchecked")
			List<T> entityList = criteria.list();
			return entityList;
		}
		catch (Throwable e) {
			throw new RuntimeException("Error in findAll() for " + this, e);
		}
	}


	@Override
	public List<T> findByField(String beanFieldName, Object value) {
		try {
			return findByFields(new String[]{beanFieldName}, new Object[]{value});
		}
		catch (Throwable e) {
			throw new RuntimeException("Error in findByField(" + beanFieldName + " = " + value + ") for " + this, e);
		}
	}


	//This function takes a list of beanFieldNames and values and will automatically create the hibernate aliases
	//necessary for the given paths and create the restrictions.
	@Override
	public List<T> findByFields(String[] beanFieldNames, Object[] values) {
		return findByFields(beanFieldNames, values, true);
	}


	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	protected List<T> findByFields(String[] beanFieldNames, Object[] values, boolean configureOrderBy) {
		try {
			int restrictionCount = beanFieldNames.length;
			if (restrictionCount != values.length) {
				throw new IllegalArgumentException("The number of beanFieldNames must be the same as the number of values: " + restrictionCount + " <> " + values.length + " for " + this);
			}
			Map<String, Criteria> processedPathToCriteria = new HashMap<>();
			Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(getConfiguration().getBeanClass());
			for (int i = 0; i < restrictionCount; i++) {
				HibernateUtils.addEqualsRestriction(beanFieldNames[i], values[i], criteria, processedPathToCriteria);
			}
			if (configureOrderBy) {
				configureOrderBy(criteria);
			}
			return criteria.list();
		}
		catch (Throwable e) {
			throw new RuntimeException("Error in findByFields(" + ArrayUtils.toString(beanFieldNames) + ") using [" + ArrayUtils.toString(values) + "] for " + this, e);
		}
	}


	@Override
	@Transactional(readOnly = true)
	public List<T> findByField(String beanFieldName, Object[] values) {
		if (values == null) {
			return null;
		}
		try {
			int inCount = values.length;
			int maxInCount = 1000; // RDBMS have limitations on IN counts: SQL Server 2008 limit is 2000
			if (inCount > maxInCount) {
				// if over the limit, break into smaller queries and merge results
				List<T> result = null;
				int start = 0;
				while (inCount > 0) {
					int length = Math.min(inCount, maxInCount);
					Object[] subValues = new Object[length];
					System.arraycopy(values, start, subValues, 0, length);
					Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(getConfiguration().getBeanClass()).add(Restrictions.in(beanFieldName, subValues));
					configureOrderBy(criteria);
					@SuppressWarnings("unchecked")
					List<T> moreResults = (List<T>) criteria.list();
					if (moreResults != null) {
						if (result == null) {
							result = moreResults;
						}
						else {
							result.addAll(moreResults);
						}
					}
					inCount -= maxInCount;
					start += maxInCount;
				}
				return result;
			}
			Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(getConfiguration().getBeanClass()).add(Restrictions.in(beanFieldName, values));
			configureOrderBy(criteria);
			@SuppressWarnings("unchecked")
			List<T> entityList = criteria.list();
			return entityList;
		}
		catch (Throwable e) {
			throw new RuntimeException("Error in findByField('" + beanFieldName + "') using [" + ArrayUtils.toString(values) + "] for " + this, e);
		}
	}


	@Override
	@SuppressWarnings("unchecked")
	@Transactional(readOnly = true)
	public List<T> findByNamedQuery(String queryName, Object... parameterValues) {
		try {
			Query<T> query = getSessionFactory().getCurrentSession().getNamedQuery(queryName);
			int position = 1; // javadoc requires 0 for the first element but after upgrade to Hibernate 5.4.2 it started failing on 0: had to switch to 1
			for (Object value : parameterValues) {
				query.setParameter(position++, value);
			}
			return query.list();
		}
		catch (Throwable e) {
			throw new RuntimeException("Error executing named query: " + queryName + "(" + ArrayUtils.toString(parameterValues) + ") for " + this, e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////             AdvancedReadOnlyDAO Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<T> findBySearchCriteria(SearchConfigurer<Criteria> searchConfigurer) {
		try {
			if (searchConfigurer.isReadUncommittedRequested()) {
				return doFindBySearchCriteria_READ_UNCOMMITTED(searchConfigurer);
			}
			return doFindBySearchCriteria_DEFAULT(searchConfigurer);
		}
		catch (Throwable e) {
			throw new RuntimeException("Error in findBySearchCriteria(" + searchConfigurer + ") for " + this, e);
		}
	}


	@Transactional(readOnly = true)
	protected List<T> doFindBySearchCriteria_DEFAULT(SearchConfigurer<Criteria> searchConfigurer) {
		return doFindBySearchCriteria(searchConfigurer);
	}


	@Transactional(readOnly = true, isolation = Isolation.READ_UNCOMMITTED)
	protected List<T> doFindBySearchCriteria_READ_UNCOMMITTED(SearchConfigurer<Criteria> searchConfigurer) {
		return doFindBySearchCriteria(searchConfigurer);
	}


	private List<T> doFindBySearchCriteria(SearchConfigurer<Criteria> searchConfigurer) {
		Criteria criteria = getSessionFactory().getCurrentSession().createCriteria(getConfiguration().getBeanClass());
		searchConfigurer.configureCriteria(criteria);
		if (!searchConfigurer.configureOrderBy(criteria)) {
			configureOrderBy(criteria);
		}
		boolean evaluateWarning = false;
		int limit = searchConfigurer.getLimit();
		int maxLimit = ObjectUtils.coalesce(searchConfigurer.getMaxLimit(), getConfiguration().getMaxQuerySizeLimit());
		// check if it's a simple case with no paging
		if (searchConfigurer.getStart() == 0 && (limit == Integer.MAX_VALUE || limit < 10)) {
			if (limit < 10) {
				// if limit is less than 10 then we are not using pagination (doesn't make sense to page with such small pages)
				// instead, one is likely trying to limit the number of rows returned with TOP clause (usually TOP 1)
				criteria.setMaxResults(limit);
			}
			else if (limit > maxLimit && criteria instanceof CriteriaImpl) {
				Integer maxResults = ((CriteriaImpl) criteria).getMaxResults();
				if (maxResults == null || maxResults > maxLimit) {
					evaluateWarning = (maxResults == null);
					//If max results has not previously been set by an overridden configureCriteria then we implement a hard max limit.
					//We set the max results to one greater than the max limit so that in the case of a file download, rather than returning the
					//actual max limit (truncating data) it will actually fail validation in the fileDownloadView and return a proper error message.
					//This limit guarantees that a user can't execute a giant query for an export and crash the system.
					criteria.setMaxResults(maxLimit + 1);
				}
			}
			@SuppressWarnings("unchecked")
			List<T> entityList = criteria.list();
			if (evaluateWarning) {
				int size = CollectionUtils.getSize(entityList);
				String limitSource = "PagingCommand Max Limit";
				if (searchConfigurer.getMaxLimit() != null) {
					limitSource = "SearchForm";
				}
				//This is a LIE highlighting saying this statement is always true
				//It doesn't account for dynamic manipulation of the maxLimit at the daoConfiguration level (configurable on SystemTable)
				else if (maxLimit != PagingCommand.MAX_LIMIT) {
					limitSource = "SystemTable";
				}
				limitSource += " [" + getConfiguration().getTableName() + "]";
				//TODO - added %s to allow for prefix to avoid sonar issue - when proposed limit gets removed can also remove the prefix and extra param in String.format calls.
				String message = "%sThe size of the resultSet returned exceeds %d%% of the maximum size of " + NumberFormat.getIntegerInstance().format(maxLimit) + " defined by the %s";
				if (size > PROPOSED_MAX_QUERY_LIMIT) {
					// only warn for now, later this should be a hard limit.
					LogUtils.warn(getClass(), String.format(message, "[PROPOSED] ", 100, limitSource));
				}
				if (size > maxLimit) {
					LogUtils.error(getClass(), String.format(message, "", 100, limitSource));
					throw new ValidationException(String.format(message, "", 100, limitSource));
				}
				else if (size > (maxLimit * 0.8)) {
					LogUtils.warn(getClass(), String.format(message, "", 80, limitSource));
				}
			}
			return entityList;
		}

		// use scrolling to support paging and total count
		List<T> result = new ArrayList<>(limit <= 1000 ? limit : 10); // don't allow more than 1000 elements?
		ScrollableResults resultSet = null;
		int rowCount = 0;
		try {
			criteria.setFirstResult(searchConfigurer.getStart());
			resultSet = criteria.scroll(limit == Integer.MAX_VALUE ? ScrollMode.FORWARD_ONLY : ScrollMode.SCROLL_INSENSITIVE);
			if (!resultSet.next()) {
				return result;
			}
			boolean endOfResultSet = false;
			while (limit > rowCount++) {
				@SuppressWarnings("unchecked")
				T entity = (T) resultSet.get(0);
				result.add(entity);
				if (!resultSet.next()) {
					endOfResultSet = true;
					break;
				}
				ValidationUtils.assertTrue(rowCount < maxLimit, () -> "Unable to return results. Requested record count exceeds maximum limit of " + NumberFormat.getIntegerInstance().format(maxLimit) + ". Add additional filters before attempting to run again.");
			}
			if (endOfResultSet) {
				rowCount += searchConfigurer.getStart();
			}
			else {
				resultSet.last();
				rowCount = resultSet.getRowNumber() + 1;
			}
		}
		finally {
			if (resultSet != null) {
				resultSet.close();
			}
		}

		return new PagingArrayList<>(result, searchConfigurer.getStart(), rowCount, limit);
	}


	void configureOrderBy(Criteria criteria) {
		for (OrderByField orderByField : CollectionUtils.getIterable(getConfiguration().getOrderByList())) {
			switch (orderByField.getDirection()) {
				case ASC: {
					criteria.addOrder(Order.asc(orderByField.getName()));
					break;
				}

				case DESC:
					criteria.addOrder(Order.desc(orderByField.getName()));
			}
		}
	}


	@Override
	public void flushSession() {
		if (TransactionSynchronizationManager.isActualTransactionActive()) {
			getSessionFactory().getCurrentSession().flush();
		}
	}


	@Override
	public void clearSession() {
		if (TransactionSynchronizationManager.isActualTransactionActive()) {
			getSessionFactory().getCurrentSession().clear();
		}
	}


	@Override
	public void evict(T entity) {
		if (entity != null) {
			Session currentSession = getSessionFactory().getCurrentSession();
			if (currentSession.isOpen()) {
				currentSession.evict(entity);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	///////           AdvancedReadOnlyWithGroupingDAO Methods           ////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unchecked")
	@Override
	public List<GroupedHierarchicalResult> findBySearchCriteriaGrouped(GroupingSearchConfigurer searchConfigurer) {
		return (List<GroupedHierarchicalResult>) findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<GroupedEntityResult<T>> findBySearchCriteriaAggregate(GroupingSearchConfigurer searchConfigurer) {
		return (List<GroupedEntityResult<T>>) findBySearchCriteria(searchConfigurer);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
