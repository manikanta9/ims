package com.clifton.core.dataaccess.search.form.entity;


import com.clifton.core.dataaccess.search.form.SearchForm;


/**
 * The <code>AuditableEntitySearchForm</code> is a basic concrete implementation of {@link BaseAuditableEntitySearchForm}.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseAuditableEntitySearchForm Parent fields}
 * </ul>
 */
@SearchForm(hasOrmDtoClass = false)
public final class AuditableEntitySearchForm extends BaseAuditableEntitySearchForm {

}
