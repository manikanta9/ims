package com.clifton.core.dataaccess.transactions;

import com.clifton.core.cache.CustomCache;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.Transactional;


/**
 * The {@link CustomCache} type for caching {@link PlatformTransactionManager} instances on a per-class basis.
 * <p>
 * This cache is used to store and retrieve the results for system logic that maps qualified type names to specific transaction managers. For example, to use a custom transaction
 * manager for all {@link Transactional} service class annotations within the "clifton-integration" project, the pattern key {@code "com.clifton.integration..*ServiceImpl"} might
 * be mapped to the transaction manager bean of name {@code "integrationTransactionManager"}.
 *
 * @author MikeH
 * @see BaseCustomAspectJAnnotationTransactionAspect
 */
public interface TransactionManagerCache extends CustomCache<Class<?>, PlatformTransactionManager> {

	/**
	 * Gets the configured transaction manager for the given class. If no matching transaction manager is found, then {@code null} is returned.
	 */
	public PlatformTransactionManager getTransactionManagerForClass(Class<?> clazz);


	/**
	 * Clears the cache. This can be useful on application events which may affect the available transaction manager mappings, such as context refresh.
	 */
	public void clear();
}
