package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Constraint;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;


/**
 * The <code>MSSQLSchemaMetaDataSQLConverter</code> class generates DML SQL statements to insert/delete meta data info
 * for table/column/relationship database schema meta data.
 * <p/>
 * TODO: support removal of columns/tables (delete vs mark deleted?)
 *
 * @author vgomelsky
 */
@Component
public class MSSQLSchemaMetaDataSQLConverter implements MigrationSchemaConverter {

	// Columns that are specifically set to no audit.
	// These are columns whose changes are generally managed elsewhere in the system, i.e. Workflow State changes can be found in Workflow History
	// Document Update information can be found in the document revision history from Alfresco
	private static final Set<String> NO_AUDIT_COLUMNS = new HashSet<>();


	static {
		NO_AUDIT_COLUMNS.add("WorkflowStateID");
		NO_AUDIT_COLUMNS.add("WorkflowStatusID");
		NO_AUDIT_COLUMNS.add("WorkflowStateEffectiveStartDate");
		NO_AUDIT_COLUMNS.add("DocumentUpdateDate");
		NO_AUDIT_COLUMNS.add("DocumentUpdateUser");
		NO_AUDIT_COLUMNS.add("DocumentFileType");
	}

	////////////////////////////////////////////////////////////////////////////
	/**
	 * Data Migration Converter that runs Data SQL that is marked
	 * to runWith=DDL
	 */
	private MigrationSchemaConverter dataMigrationConverter;
	/**
	 * SQL Migration Converter that runs SQL SQL that is marked
	 * to runWith=DDL
	 */
	private MigrationSchemaConverter sqlMigrationConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.META_DATA;
	}


	@Override
	public String convert(Schema schema) {
		if (schema.getExcludeFromSql() != null && schema.getExcludeFromSql()) {
			return "";
		}
		StringBuilder tableColumnSql = new StringBuilder(1024);
		StringBuilder relationshipSqlBefore = new StringBuilder(1024);
		StringBuilder relationshipSql = new StringBuilder(1024);

		for (Table table : CollectionUtils.getIterable(schema.getTableList())) {
			if (table.getExcludeFromSql() != null && table.getExcludeFromSql()) {
				// Table was explicitly marked to skip SQL for
				continue;
			}
			appendTableSQL(tableColumnSql, table);
			for (Column column : CollectionUtils.getIterable(table.getColumnList(true))) {
				appendColumnSQL(tableColumnSql, table, column);
			}
			// when deleting, first delete constraints and then columns
			if (OperationTypes.DELETE == table.getOperationType()) {
				for (Constraint constraint : CollectionUtils.getIterable(table.getConstraintList())) {
					appendRelationshipSQL(relationshipSqlBefore, constraint, table.getOperationType());
				}
				relationshipSqlBefore.append("\n\n");
			}
			else {
				for (Constraint constraint : CollectionUtils.getIterable(table.getConstraintList())) {
					appendRelationshipSQL(relationshipSql, constraint, table.getOperationType());
				}
				relationshipSql.append("\n\n");
			}
			tableColumnSql.append("\n\n");
		}
		// Relationship SQL has to come after all Table Column SQL to handle delayed column relationships
		tableColumnSql.append(relationshipSql);

		appendRunWithSql(tableColumnSql, schema);

		return relationshipSqlBefore.toString() + tableColumnSql.toString();
	}


	/**
	 * Appends table definition insert to the specified statement.
	 */
	private void appendTableSQL(StringBuilder sql, Table table) {
		// don't insert the table on update/delete: it's already there
		if (table.getOperationType() == null || OperationTypes.CREATE == table.getOperationType() || OperationTypes.CREATE_SQL_ONLY == table.getOperationType()) {
			sql.append("INSERT INTO SystemTable(SystemDataSourceID");
			if (!StringUtils.isEmpty(table.getAuditType())) {
				sql.append(", SystemAuditTypeID");
			}
			sql.append(", TableName, TableLabel, TableAlias, TableDescription, IsUploadAllowed, CreateUserID, CreateDate, UpdateUserID, UpdateDate)");
			sql.append("\n\tSELECT (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ");
			sql.append(StringUtils.toSQL(table.getDataSource()));
			sql.append(')');
			if (!StringUtils.isEmpty(table.getAuditType())) {
				sql.append(", ");
				sql.append("(SELECT COALESCE((SELECT CAST(SystemAuditTypeID AS VARCHAR) FROM SystemAuditType WHERE AuditTypeName = ");
				sql.append(StringUtils.toSQL(table.getAuditType()));
				sql.append("), ");
				sql.append(StringUtils.toSQL("Invalid Audit Type: " + table.getAuditType()));
				sql.append("))");
			}
			sql.append(", ").append(StringUtils.toSQL(table.getName()));
			String label = table.getLabel();
			if (StringUtils.isEmpty(label) || table.getName().equals(label)) {
				label = StringUtils.splitWords(table.getName());
			}
			sql.append(", ").append(StringUtils.toSQL(label));
			sql.append(", ").append(StringUtils.toSQL(table.getAlias()));
			String description = table.getDescription();
			if (StringUtils.isEmpty(description) || table.getName().equals(description)) {
				description = label;
			}
			sql.append(", ").append(StringUtils.toSQL(description));
			sql.append(", ").append(BooleanUtils.isTrue(table.getUploadAllowed()) ? '1' : '0');
			sql.append(", 0, GETDATE(), 0, GETDATE()").append(getMigrationSchemaConverterType().getBatchSeparator());
		}
	}


	/**
	 * Appends column definition insert to the specified statement.
	 */
	private void appendColumnSQL(StringBuilder sql, Table table, Column column) {
		if (OperationTypes.DELETE == table.getOperationType()) {
			sql.append("DELETE FROM SystemColumn WHERE ColumnName = '");
			sql.append(column.getName());
			sql.append("' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = '");
			sql.append(table.getName());
			sql.append("' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ").append(StringUtils.toSQL(table.getDataSource()));
			sql.append("))").append(getMigrationSchemaConverterType().getBatchSeparator());
		}
		else {
			sql.append("INSERT INTO SystemColumn(SystemTableID, SystemDataTypeID, ");
			String columnAuditType = getColumnLevelAuditing(table, column);
			if (!StringUtils.isEmpty(columnAuditType)) {
				sql.append("SystemAuditTypeID, ");
			}
			sql.append("ColumnName, ColumnLabel, ColumnDescription, BeanPropertyName, IsSystemDefined, CreateUserID, CreateDate, UpdateUserID, UpdateDate)");
			sql.append("\n\tSELECT ");
			sql.append("(SELECT SystemTableID FROM SystemTable WHERE TableName = '");
			sql.append(table.getName());
			sql.append("' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ").append(StringUtils.toSQL(table.getDataSource()));
			sql.append(")), (SELECT SystemDataTypeID FROM SystemDataType WHERE DataTypeName = '");
			AssertUtils.assertNotNull(column.getDataType(), "Cannot determine data type for column: " + column.getName());
			sql.append(column.getDataType().getTypeName());
			if (!StringUtils.isEmpty(columnAuditType)) {
				sql.append("'), (SELECT COALESCE((SELECT CAST(SystemAuditTypeID AS VARCHAR) FROM SystemAuditType WHERE AuditTypeName = ");
				sql.append(StringUtils.toSQL(columnAuditType));
				sql.append("), ");
				sql.append(StringUtils.toSQL("Invalid Audit Type: " + columnAuditType));
				sql.append(")), '");
			}
			else {
				sql.append("'), '");
			}
			sql.append(column.getName());
			String label = column.getLabel();
			if (StringUtils.isEmpty(label) || column.getName().equals(label)) {
				label = StringUtils.splitWords(column.getName());
			}
			sql.append("', '");
			sql.append(label);
			sql.append("', ");
			String description = column.getDescription();
			if (StringUtils.isEmpty(description) || column.getName().equals(description)) {
				description = label;
			}
			sql.append(StringUtils.toSQL(description));
			sql.append(", '");
			sql.append(column.getBeanPropertyName());
			sql.append("', 1");
			sql.append(", 0, GETDATE(), 0, GETDATE()").append(getMigrationSchemaConverterType().getBatchSeparator());
		}
	}


	/**
	 * Returns true if column audit type is specified (and different than the table level)
	 * Or not specified but column name is in the NO_AUDIT_COLUMNS list.
	 * Also skips calculated columns from auditing
	 */
	private String getColumnLevelAuditing(Table table, Column column) {
		if (!StringUtils.isEmpty(column.getAuditType()) && !column.getAuditType().equals(table.getAuditType())) {
			return column.getAuditType();
		}
		if (NO_AUDIT_COLUMNS.contains(column.getName()) || column.isCalculatedColumn()) {
			return "NO AUDIT";
		}
		return null;
	}


	/**
	 * Appends relationship definition insert to the specified statement (foreign key meta data).
	 */
	private void appendRelationshipSQL(StringBuilder sql, Constraint constraint, OperationTypes operationType) {
		if (constraint.getReferenceTable() == null) {
			return;
		}
		if (OperationTypes.DELETE == operationType) {
			sql.append("DELETE FROM SystemRelationship WHERE SystemColumnID = ");
			sql.append("(SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = '");
			sql.append(constraint.getPrimaryColumnList().get(0).getName());
			sql.append("' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = '");
			sql.append(constraint.getPrimaryTable().getName());
			sql.append("' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ").append(StringUtils.toSQL(constraint.getPrimaryTable().getDataSource()));
			sql.append("))) AND ParentSystemColumnID = ");
			sql.append("(SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = '");
			sql.append(constraint.getReferenceColumnList().get(0).getName());
			sql.append("' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = '");
			sql.append(constraint.getReferenceTable().getName());
			sql.append("' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ").append(StringUtils.toSQL(constraint.getReferenceTable().getDataSource()));
			sql.append(")))").append(getMigrationSchemaConverterType().getBatchSeparator());
		}
		else {
			sql.append("INSERT INTO SystemRelationship(SystemColumnID, ParentSystemColumnID, RelationshipName, RelationshipLabel, RelationshipDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)");
			sql.append("\n\tSELECT ");
			sql.append("(SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = '");
			sql.append(constraint.getPrimaryColumnList().get(0).getName());
			sql.append("' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = '");
			sql.append(constraint.getPrimaryTable().getName());
			sql.append("' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ").append(StringUtils.toSQL(constraint.getPrimaryTable().getDataSource()));
			sql.append("))), (SELECT SystemColumnID FROM SystemColumn WHERE ColumnName = '");
			sql.append(constraint.getReferenceColumnList().get(0).getName());
			sql.append("' AND SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = '");
			sql.append(constraint.getReferenceTable().getName());
			sql.append("' AND SystemDataSourceID = (SELECT SystemDataSourceID FROM SystemDataSource WHERE DataSourceName = ").append(StringUtils.toSQL(constraint.getReferenceTable().getDataSource()));
			sql.append("))), '");
			sql.append(constraint.getName());
			sql.append("', ");
			sql.append(StringUtils.toSQL(constraint.getLabel()));
			sql.append(", ");
			sql.append(StringUtils.toSQL(constraint.getDescription()));
			sql.append(", 0, GETDATE(), 0, GETDATE()").append(getMigrationSchemaConverterType().getBatchSeparator());
		}
	}


	private void appendRunWithSql(StringBuilder sql, Schema schema) {
		// Append Data Migrations that are RUN_WITH META_DATA
		String batchSep = getMigrationSchemaConverterType().getBatchSeparator();
		String appendBatchSep = getDataMigrationConverter().getMigrationSchemaConverterType().getBatchSeparator();
		String appendSql = getDataMigrationConverter().convert(schema);
		if (!StringUtils.isEmpty(appendSql) && !StringUtils.isEmpty(appendBatchSep) && !appendBatchSep.equalsIgnoreCase(batchSep)) {
			appendSql = appendSql.replaceAll(appendBatchSep, batchSep + StringUtils.NEW_LINE);
		}
		sql.append(appendSql);

		// Append SQL Migrations that are RUN_WITH META_DATA
		appendBatchSep = getSqlMigrationConverter().getMigrationSchemaConverterType().getBatchSeparator();
		appendSql = getSqlMigrationConverter().convert(schema);
		if (!StringUtils.isEmpty(appendSql) && !StringUtils.isEmpty(appendBatchSep) && !appendBatchSep.equalsIgnoreCase(batchSep)) {
			appendSql = appendSql.replaceAll(appendBatchSep, batchSep + StringUtils.NEW_LINE);
		}
		sql.append(appendSql);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public MigrationSchemaConverter getDataMigrationConverter() {
		return this.dataMigrationConverter;
	}


	public void setDataMigrationConverter(MigrationSchemaConverter dataMigrationConverter) {
		this.dataMigrationConverter = dataMigrationConverter;
	}


	public MigrationSchemaConverter getSqlMigrationConverter() {
		return this.sqlMigrationConverter;
	}


	public void setSqlMigrationConverter(MigrationSchemaConverter sqlMigrationConverter) {
		this.sqlMigrationConverter = sqlMigrationConverter;
	}
}
