package com.clifton.core.dataaccess.dao;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>NonPersistentField</code> annotation marks DTO fields explicitly to indicate
 * they are not persisted to the database, and in essence are 'transient', but without
 * utilizing the transient modifier so as to not affect serialization inherently.
 *
 * @author stevenf
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface NonPersistentField {

}
