package com.clifton.core.dataaccess.search;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.form.RestrictiveSearchForm;
import com.clifton.core.dataaccess.search.form.ValidatingSearchForm;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>SearchUtils</code> class defines various search utility methods.
 *
 * @author vgomelsky
 */
public class SearchUtils {

	// a Map of SearchForm class to corresponding custom restriction configurer list: used to register custom search configuration for search forms in dependent projects
	private static final Map<Class<?>, List<SearchFormRestrictionConfigurer>> searchFormRestrictionConfigurerMap = new ConcurrentHashMap<>();

	// a Map of SearchForm interface class to corresponding custom search form configurer list: used to register custom search configuration for search forms
	@SuppressWarnings("rawtypes")
	private static final Map<Class<?>, SearchFormCustomConfigurer> searchFormCustomConfigurerMap = new ConcurrentHashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Parses the specified orderBy string and returns a List of OrderByField objects. Argument format: "field1#field2:asc#field3:desc".
	 */
	public static List<OrderByField> getOrderByFieldList(String orderBy) {
		List<OrderByField> result = null;
		if (!StringUtils.isEmpty(orderBy)) {
			String[] orderByFields = orderBy.split("#");
			result = new ArrayList<>(orderByFields.length);
			for (String field : orderByFields) {
				int end = field.indexOf(':');
				if (end == -1) {
					result.add(new OrderByField(field, OrderByDirections.ASC));
				}
				else {
					String direction = field.substring(end + 1);
					if ("asc".equalsIgnoreCase(direction)) {
						result.add(new OrderByField(field.substring(0, end), OrderByDirections.ASC));
					}
					else if ("desc".equalsIgnoreCase(direction)) {
						result.add(new OrderByField(field.substring(0, end), OrderByDirections.DESC));
					}
					else {
						throw new IllegalArgumentException("Illegal sort direction '" + direction + "' for field '" + field.substring(0, end));
					}
				}
			}
		}
		return result;
	}


	/**
	 * Remove items from the restriction list the are properties of the search form for custom search fields.
	 */
	public static void moveCustomRestrictions(RestrictiveSearchForm searchForm, Map<String, SearchRestrictionDefinition> restrictionDefinitionMap) {
		List<String> fieldsToRemove = new ArrayList<>();
		for (SearchRestriction restriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
			SearchRestrictionDefinition definition = restrictionDefinitionMap.get(restriction.getField());
			if ((definition == null) && BeanUtils.isPropertyPresent(searchForm.getClass(), restriction.getField())) {
				BeanUtils.setPropertyValue(searchForm, restriction.getField(), restriction.getValue());
				fieldsToRemove.add(restriction.getField());
			}
		}

		for (String field : CollectionUtils.getIterable(fieldsToRemove)) {
			searchForm.removeSearchRestriction(field);
		}
	}

	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates the specified restrictionList and throws {@link FieldValidationException} exception if it finds invalid fields (unsupported search types, invalid data types,
	 * required that's missing, etc.).
	 */
	public static void validateRestrictionList(List<SearchRestriction> restrictionList, Map<String, SearchRestrictionDefinition> restrictionDefinitionMap, ValidatingSearchForm searchForm) {
		// check that all search fields are allowed, have proper search types and values
		for (SearchRestriction restriction : CollectionUtils.getIterable(restrictionList)) {
			// Otherwise custom json column value which can't be validated because they are dynamic
			if (!restriction.getField().contains(".")) {
				SearchRestrictionDefinition definition = restrictionDefinitionMap.get(restriction.getField());
				if (definition == null) {
					if (getSearchFormRestrictionConfigurer(searchForm.getClass(), restriction.getField()) == null) {
						throw new FieldValidationException("Unsupported search field: '" + restriction.getField() + "' for " + searchForm.getClass(), restriction.getField());
					}
				}
				else {
					definition.validate(restriction);
				}
			}
		}

		// check that all required fields are populated
		for (Map.Entry<String, SearchRestrictionDefinition> stringSearchRestrictionDefinitionEntry : restrictionDefinitionMap.entrySet()) {
			SearchRestrictionDefinition definition = stringSearchRestrictionDefinitionEntry.getValue();
			if (definition.isRequired()) {
				boolean found = false;
				for (SearchRestriction restriction : CollectionUtils.getIterable(restrictionList)) {
					if (restriction.getField().equals(definition.getFieldName())) {
						found = true;
						break;
					}
				}
				if (!found) {
					throw new FieldValidationException("Required restriction is missing for field: " + definition.getFieldName(), definition.getFieldName());
				}
			}
		}

		if (searchForm != null) {
			if (searchForm.isFilterRequired() && CollectionUtils.getSize(restrictionList) == 0) {
				// check if custom filters were specified
				List<String> diff = getPopulatedSearchFormProperties(searchForm);
				if (CollectionUtils.isEmpty(diff)) {
					throw new ValidationException("At least one search restriction must be specified for SearchForm: " + searchForm.getClass());
				}
			}
			searchForm.validate();
		}
	}


	/**
	 * Validates the specified search form to make sure it has at least one search restriction populated. The logic will check both, filters that are directly seat via search form
	 * properties as well as restrictionList field. The method can optionally exclude the specified fields from its check (broad filters that are not limiting enough). The method
	 * will throw a ValidationException if it cannot find a single restriction set (excluding ignoreFields).
	 *
	 * @return Set of populated fields names (excludes the specified ignoreFields). The set can be used to perform additional validation.
	 */
	public static Set<String> validateRequiredFilter(RestrictiveSearchForm searchForm, String... ignoreFields) {
		Set<String> populatedFields = getPopulatedSearchFields(searchForm);

		// remove fields to ignore
		for (String ignoreField : ignoreFields) {
			populatedFields.remove(ignoreField);
		}

		if (CollectionUtils.isEmpty(populatedFields)) {
			throw new ValidationException("At least one required search restriction must be specified for Search Form: " + searchForm.getClass().getSimpleName() + ". Ignore restrictions: " + ArrayUtils.toString(ignoreFields));
		}

		return populatedFields;
	}


	/**
	 * If the size of the specified populatedFields is 1, then provides additional validation that does not allow the fields in the specified map to contain specified values.
	 * Search for use cases to learn more details.
	 * <p>
	 * This method is usually called after {@link #validateRequiredFilter} method in order to provide additional restriction and uses populatedFields returned by this method.
	 */
	public static void validateInvalidValuesForSingleRestriction(RestrictiveSearchForm searchForm, Set<String> populatedFields, Map<String, String> invalidFieldValueMap) {
		if (populatedFields.size() == 1) {
			// additional validation when only one restriction is populated
			String fieldName = populatedFields.iterator().next();
			String invalidValue = invalidFieldValueMap.get(fieldName);
			if (invalidValue != null) {
				// populated field matches the field we need to inspect
				Object value = getRestrictionValue(searchForm, fieldName);
				if (value != null) {
					if (invalidValue.toLowerCase().contains(value.toString().toLowerCase())) {
						throw new ValidationException("When " + searchForm.getClass().getSimpleName() + " has only one restriction specified and it is '" + fieldName + "', the value cannot be any part of word '" + invalidValue + "' (returns too many results): " + value);
					}
				}
			}
		}
	}


	/**
	 * Returns the value for the specified field restriction: either from restriction list of directly from the search form property.
	 */
	public static Object getRestrictionValue(RestrictiveSearchForm searchForm, String fieldName) {
		SearchRestriction restriction = searchForm.getSearchRestriction(fieldName);
		if (restriction == null) {
			return BeanUtils.getPropertyValue(searchForm, fieldName, false);
		}
		return restriction.getValue();
	}


	/**
	 * Returns a Set of all search fields that have been populated: either directly via properties or via Restriction List.
	 */
	public static Set<String> getPopulatedSearchFields(RestrictiveSearchForm searchForm) {
		// get all populated restriction fields regardless of how they have been set
		Set<String> populatedFields = new HashSet<>(getPopulatedSearchFormProperties(searchForm));
		for (SearchRestriction restriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
			populatedFields.add(restriction.getField());
		}
		return populatedFields;
	}


	private static List<String> getPopulatedSearchFormProperties(Object searchForm) {
		return CoreCompareUtils.getNoEqualProperties(searchForm, BeanUtils.newInstance(searchForm.getClass()), true, "start", "limit", "orderBy", "readUncommittedRequested", "restrictionList");
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Clears the map - called during ContextClosedEvent
	 */
	public synchronized static void clearSearchFormRestrictionConfigurers() {
		searchFormRestrictionConfigurerMap.clear();
	}


	/**
	 * Allows registration of custom SearchFormRestrictionConfigurer objects. Usually used to address cross-project dependencies.
	 */
	public synchronized static void registerSearchFormRestrictionConfigurer(SearchFormRestrictionConfigurer configurer) {
		List<SearchFormRestrictionConfigurer> configurerList = searchFormRestrictionConfigurerMap.computeIfAbsent(configurer.getSearchFormClass(), k -> new ArrayList<>());
		for (SearchFormRestrictionConfigurer existing : CollectionUtils.getIterable(configurerList)) {
			if (configurer.getSearchFieldName().equals(existing.getSearchFieldName())) {
				if (configurer.getClass().equals(existing.getClass())) {
					LogUtils.warn(SearchUtils.class, "Attempting to register same search form restriction configurer twice. Skipping second call. Search Field: " + configurer.getSearchFieldName() + " and class " + configurer.getClass());
				}
				else {
					throw new IllegalStateException("Cannot register the same search field '" + configurer.getSearchFieldName() + "' more than once for the same " + configurer.getSearchFormClass());
				}
			}
		}
		configurerList.add(configurer);
	}


	/**
	 * Returns SearchFormRestrictionConfigurer object for the specified form and field. Returns null if one was not registered.
	 */
	public static SearchFormRestrictionConfigurer getSearchFormRestrictionConfigurer(Class<?> searchFormClass, String fieldName) {
		List<SearchFormRestrictionConfigurer> list = searchFormRestrictionConfigurerMap.get(searchFormClass);
		for (SearchFormRestrictionConfigurer configurer : CollectionUtils.getIterable(list)) {
			if (configurer.getSearchFieldName().equals(fieldName)) {
				return configurer;
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public synchronized static void clearSearchFormCustomConfigurers() {
		searchFormCustomConfigurerMap.clear();
	}


	/**
	 * Allows registration of custom SearchFormCustomConfigurer objects.
	 *
	 * If Multiple custom configurers are found for a search form interface class, and one extends the other, pick the child instead of the parent.
	 * This will allow for customization of the configurer, especially in the case of functionality being subproject dependent.
	 */
	@SuppressWarnings("rawtypes")
	public synchronized static void registerSearchFormCustomConfigurer(SearchFormCustomConfigurer configurer) {
		SearchFormCustomConfigurer customConfigurer = searchFormCustomConfigurerMap.get(configurer.getSearchFormInterfaceClassName());
		if (customConfigurer == null || customConfigurer.getClass().isAssignableFrom(configurer.getClass())) {
			searchFormCustomConfigurerMap.put(configurer.getSearchFormInterfaceClassName(), configurer);
		}

		else {
			if (customConfigurer.getClass().equals(configurer.getClass())) {
				LogUtils.warn(SearchUtils.class, "Attempting to register same search form custom configurer twice. Skipping second call. Search Form Interface: " + configurer.getSearchFormInterfaceClassName() + " and class " + configurer.getClass());
			}
			else if (configurer.getClass().isAssignableFrom(customConfigurer.getClass())) {
				LogUtils.warn(SearchUtils.class, "Attempting to register a super class of an already register search form custom configurer. Skipping second call. Search Form Interface: " + configurer.getSearchFormInterfaceClassName() + " and class " + configurer.getClass() + " with sub class " + customConfigurer.getClass());
			}
			else {
				throw new IllegalStateException("Cannot register the same search form interface '" + configurer.getSearchFormInterfaceClassName() + "' to multiple configurer. Found " + configurer.getClass() + " and " + customConfigurer.getClass());
			}
		}
	}


	@SuppressWarnings("rawtypes")
	public static Set<SearchFormCustomConfigurer> getSearchFormCustomConfigurerSetForSearchFormClass(Class<?> searchFormClass) {
		Set<SearchFormCustomConfigurer> customConfigurerSet = new HashSet<>();
		searchFormCustomConfigurerMap.forEach((searchFormInterfaceClass, searchFormInterfaceConfigurer) -> {
			if (searchFormInterfaceClass.isAssignableFrom(searchFormClass)) {
				customConfigurerSet.add(searchFormInterfaceConfigurer);
			}
		});
		return customConfigurerSet;
	}


	/**
	 * Filters the search restriction list and returns a list that only contains restrictions for the specified field
	 */
	public static List<SearchRestriction> getSearchRestrictionsForField(List<SearchRestriction> fullList, String fieldName) {
		if (!CollectionUtils.isEmpty(fullList)) {
			return BeanUtils.filter(fullList, SearchRestriction::getField, fieldName);
		}
		else {
			return Collections.emptyList();
		}
	}
}
