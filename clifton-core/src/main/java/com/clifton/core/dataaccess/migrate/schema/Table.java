package com.clifton.core.dataaccess.migrate.schema;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheTypes;
import com.clifton.core.dataaccess.db.DataTypeEncodingTypeNames;
import com.clifton.core.dataaccess.db.TableTypes;
import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>Table</code> class represents a database table.
 *
 * @author vgomelsky
 */
public class Table {

	List<Subclass> subclassList;
	private String dataSource;
	private OperationTypes operationType;
	private String name;
	private String label;
	private String alias;
	private String description;
	private List<Column> columnList;
	private List<Constraint> constraintList;
	private List<Index> indexList;
	private List<Link> linkList;
	private String auditType;
	private CacheTypes cacheType;
	private TableTypes tableType;
	private String daoName;
	private String daoClassName;
	private String dtoClass;
	private String dtoConverterClass;
	private String dtoServiceClass;
	private String dtoProxy;
	/**
	 * Default encoding for all columns (that support) it in the table.  This can be overridden by each column
	 */
	private DataTypeEncodingTypeNames defaultDataTypeEncodingType;

	/**
	 * This optional attribute defines an alternative database schema to use.  If this is not specified, Hibernate will use the default 'dbo' schema.
	 * <p>
	 * For example, the table storing migration metadata 'MigrationModuleVersion' is in the 'env' schema, so this attribute is specified in the appropriate migration file.
	 */
	private String schema;

	/**
	 * If set will use the service bean & method to save beans during json migrations
	 * instead of calling the dao save method directly method must take one argument only - the DTO object
	 */
	private String migrationServiceBeanName;
	private String migrationServiceMethodName;

	/**
	 * batchEnabled used to enable hibernate batch functionality; this will create a database SEQUENCE;
	 * This is used when a table needs to support hibernate batch updates and results in using the 'enhanced-sequence' generator type
	 * in the hibernate schema mapping file for the table:
	 * https://stackoverflow.com/questions/27697810/hibernate-disabled-insert-batching-when-using-an-identity-identifier-generator
	 */
	private boolean batchEnabled;

	/**
	 * templateEnabled is used to define an entity that will be accessed using a template (session-less database access)
	 */
	private boolean templateEnabled;

	/**
	 * Can be use to identify the service and corresponding methods that manage CRUD operations for this table.
	 * Services often provide additional functionality and validation that do not existing on DAO.
	 */
	private String serviceBeanName;
	private String serviceGetMethodName;
	private String serviceSaveMethodName;
	private String serviceDeleteMethodName;

	/**
	 * Determines whether uploads are permitted for inserting/updating data to this table
	 */
	private Boolean uploadAllowed;
	/**
	 * Upload logic does COALESCE(uploadServiceMethodName, serviceSaveMethodName, DAO save method)
	 * Method must take one argument only - the DTO object.
	 */
	private String uploadServiceBeanName;
	private String uploadServiceMethodName;

	// Inheritance (Table per class hierarchy)
	private String discriminatorFormula;
	private String discriminatorValue; // String value that represents a row belongs to this class

	/**
	 * If set and true, table sql will not be created/executed for the table, including the MetaData SQL for the table
	 */
	private Boolean excludeFromSql;

	/**
	 * If set and true, will add 'rv' value to each update statement and throw concurrency modify exception of the row gets stale.
	 * NOTE: this is simplified impl which ties to our 'rv'. If necessary, will refactor to make it more flexible.
	 */
	private Boolean version;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.name;
	}


	public boolean isCacheUsed() {
		return getCacheType() != null && getCacheType() != CacheTypes.NONE;
	}


	/**
	 * Returns this table's explicitly set sort columns in specified sort order
	 *
	 * @param includeSubclassColumns
	 * @return an ordered <code>List</code> of <code>Column</code>s with the sortOrder attribute set.
	 */
	public List<Column> getSortOrderColumnList(boolean includeSubclassColumns) {
		List<Column> sortOrderColumns = new ArrayList<>();
		for (Column column : CollectionUtils.getIterable(getColumnList(includeSubclassColumns))) {
			if (column.getSortOrder() != null) {
				sortOrderColumns.add(column);
			}
		}
		if (sortOrderColumns.size() > 1) {
			Collections.sort(sortOrderColumns, (column1, column2) -> column1.getSortOrder().compareTo(column2.getSortOrder()));
		}
		return sortOrderColumns;
	}


	public List<Column> getColumnList(boolean includeSubclassColumns) {
		if (!includeSubclassColumns) {
			return getColumnList();
		}
		List<Column> totalColumnList = new ArrayList<>();
		List<Column> systemManagedColumnList = new ArrayList<>();

		// This will put the system managed fields (CreateUserId, CreateDate, etc at the end of the Table)
		for (Column column : CollectionUtils.getIterable(getColumnList())) {
			if (!StringUtils.isEmpty(column.getBeanPropertyName()) && BeanUtils.isSystemManagedField(column.getBeanPropertyName())) {
				systemManagedColumnList.add(column);
			}
			else {
				totalColumnList.add(column);
			}
		}

		for (Subclass subclass : CollectionUtils.getIterable(getSubclassList())) {
			if (!CollectionUtils.isEmpty(subclass.getColumnList())) {
				//In subclasses, discriminators could allow two subclasses to define the same column
				//We only want to add once or the DDL converter will try to create the column twice in SQL.
				for (Column column : CollectionUtils.getIterable(subclass.getColumnList())) {
					if (!totalColumnList.contains(column)) {
						totalColumnList.add(column);
					}
				}
			}
		}
		totalColumnList.addAll(systemManagedColumnList);
		return totalColumnList;
	}


	/**
	 * Returns this table's constraint that has the specified column as a primary column.
	 * Returns null if one is not found (the column doesn't have constraints).
	 *
	 * @param primaryColumn
	 */
	public Constraint getForeignKeyConstraint(Column primaryColumn) {
		if (this.constraintList != null) {
			for (Constraint constraint : this.constraintList) {
				List<Column> primaryColumns = constraint.getPrimaryColumnList();
				if (primaryColumns != null) {
					for (Column column : primaryColumns) {
						if (column.equals(primaryColumn)) {
							return constraint;
						}
					}
				}
			}
		}
		return null;
	}


	/**
	 * Returns the name of corresponding DAO bean: lower case first letter + DAO suffix.
	 */
	public String getDaoName() {
		if (getName() != null && (this.daoName == null || this.daoName.isEmpty())) {
			StringBuilder result = new StringBuilder();
			result.append(Character.toLowerCase(getName().charAt(0)));
			result.append(getName().substring(1));
			result.append("DAO");
			return result.toString();
		}
		return this.daoName;
	}


	/**
	 * @return the columnList
	 */
	public List<Column> getColumnList() {
		if (this.columnList == null) {
			this.columnList = new ArrayList<>();
		}
		return this.columnList;
	}


	/**
	 * Returns this Table's Column object that matches the specified name or null if one is not found.
	 *
	 * @param columnName
	 */
	public Column getColumn(String columnName) {
		List<Column> cList = getColumnList(true);
		if (columnName != null && cList != null) {
			for (Column column : cList) {
				if (columnName.equals(column.getName())) {
					return column;
				}
			}
		}
		return null;
	}


	/**
	 * Adds the specified column to this table.
	 *
	 * @param column
	 */
	public void addColumn(Column column) {
		if (this.columnList == null) {
			this.columnList = new ArrayList<>();
		}
		this.columnList.add(column);
	}


	/**
	 * Removes the specified column from this table.  Returns true if the column was removed successfully.
	 *
	 * @param column
	 */
	public boolean removeColumn(Column column) {
		if (this.columnList == null) {
			return false;
		}
		return this.columnList.remove(column);
	}


	/**
	 * Adds the specified constraint to this table.
	 *
	 * @param constraint
	 */
	public void addConstraint(Constraint constraint) {
		if (this.constraintList == null) {
			this.constraintList = new ArrayList<>();
		}
		this.constraintList.add(constraint);
	}


	/**
	 * Adds the specified constraint(s) to this table.
	 *
	 * @param list
	 */
	public void addConstraints(List<Constraint> list) {
		if (list != null) {
			if (this.constraintList == null) {
				this.constraintList = list;
			}
			else {
				this.constraintList.addAll(list);
			}
		}
	}


	/**
	 * Returns this Table's Constraint object that matches the specified name or null if one is not found.
	 *
	 * @param constraintName
	 */
	public Constraint getConstraint(String constraintName) {
		if (constraintName != null && this.constraintList != null) {
			for (Constraint constraint : this.constraintList) {
				if (constraintName.equals(constraint.getName())) {
					return constraint;
				}
			}
		}
		return null;
	}


	/**
	 * Removes constraints that reference the specified table.
	 * Returns a list of removed constraints.
	 *
	 * @return the constraintList
	 */
	public List<Constraint> removeConstraints(Table table) {
		if (this.constraintList == null || table == null) {
			return null;
		}
		List<Constraint> list = null;
		for (int i = this.constraintList.size(); i > 0; i--) {
			Constraint constraint = this.constraintList.get(i - 1);
			if (table.equals(constraint.getReferenceTable())) {
				if (list == null) {
					list = new ArrayList<>();
				}
				this.constraintList.remove(i - 1);
				list.add(constraint);
			}
		}
		return list;
	}


	/**
	 * Removes indexes that have the specified column.
	 * Returns a list of removed indexes.
	 *
	 * @return the indexList
	 */
	public List<Index> removeIndexes(Column column) {
		if (this.indexList == null) {
			return null;
		}
		List<Index> list = null;
		for (int i = this.indexList.size(); i > 0; i--) {
			Index index = this.indexList.get(i - 1);
			if (index.getColumnList().contains(column)) {
				if (list == null) {
					list = new ArrayList<>();
				}
				this.indexList.remove(i - 1);
				list.add(index);
			}
		}
		return list;
	}


	/**
	 * Adds the specified index to this table.
	 *
	 * @param index
	 */
	public void addIndex(Index index) {
		if (this.indexList == null) {
			this.indexList = new ArrayList<>();
		}
		this.indexList.add(index);
	}


	/**
	 * Adds the specified index(es) to this table.
	 *
	 * @param list
	 */
	public void addIndexes(List<Index> list) {
		if (list != null) {
			if (this.indexList == null) {
				this.indexList = list;
			}
			else {
				this.indexList.addAll(list);
			}
		}
	}


	/**
	 * Returns this Table's Index object that matches the specified name or null if one is not found.
	 *
	 * @param indexName
	 */
	public Index getIndex(String indexName) {
		if (indexName != null && this.indexList != null) {
			for (Index index : this.indexList) {
				if (indexName.equals(index.getName())) {
					return index;
				}
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (!(o instanceof Table)) {
			return false;
		}
		Table table = (Table) o;
		return (table.getName() == null || table.getName().equals(getName()));
	}


	@Override
	public int hashCode() {
		return (getName() == null) ? 0 : getName().hashCode();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<Subclass> getSubclassList() {
		return this.subclassList;
	}


	public void setSubclassList(List<Subclass> subclassList) {
		this.subclassList = subclassList;
	}


	public String getDataSource() {
		return this.dataSource;
	}


	public void setDataSource(String dataSource) {
		this.dataSource = dataSource;
	}


	public OperationTypes getOperationType() {
		return this.operationType;
	}


	public void setOperationType(OperationTypes operationType) {
		this.operationType = operationType;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getAlias() {
		return this.alias;
	}


	public void setAlias(String alias) {
		this.alias = alias;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public void setColumnList(List<Column> columnList) {
		this.columnList = columnList;
	}


	public List<Constraint> getConstraintList() {
		return this.constraintList;
	}


	public void setConstraintList(List<Constraint> constraintList) {
		this.constraintList = constraintList;
	}


	public List<Index> getIndexList() {
		return this.indexList;
	}


	public void setIndexList(List<Index> indexList) {
		this.indexList = indexList;
	}


	public List<Link> getLinkList() {
		return this.linkList;
	}


	public Link getLink(String linkName) {
		for (Link searchLink : CollectionUtils.getIterable(getLinkList())) {
			if (linkName.equals(searchLink.getName())) {
				return searchLink;
			}
		}
		return null;
	}


	public void setLinkList(List<Link> linkList) {
		this.linkList = linkList;
	}


	public String getAuditType() {
		return this.auditType;
	}


	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}


	public CacheTypes getCacheType() {
		return this.cacheType;
	}


	public void setCacheType(CacheTypes cacheType) {
		this.cacheType = cacheType;
	}


	public TableTypes getTableType() {
		return this.tableType;
	}


	public void setTableType(TableTypes tableType) {
		this.tableType = tableType;
	}


	public void setDaoName(String daoName) {
		this.daoName = daoName;
	}


	public String getDaoClassName() {
		return this.daoClassName;
	}


	public void setDaoClassName(String daoClassName) {
		this.daoClassName = daoClassName;
	}


	public String getDtoClass() {
		return this.dtoClass;
	}


	public void setDtoClass(String dtoClass) {
		this.dtoClass = dtoClass;
	}


	public String getDtoConverterClass() {
		return this.dtoConverterClass;
	}


	public void setDtoConverterClass(String dtoConverterClass) {
		this.dtoConverterClass = dtoConverterClass;
	}


	public String getDtoServiceClass() {
		return this.dtoServiceClass;
	}


	public void setDtoServiceClass(String dtoServiceClass) {
		this.dtoServiceClass = dtoServiceClass;
	}


	public String getDtoProxy() {
		return this.dtoProxy;
	}


	public void setDtoProxy(String dtoProxy) {
		this.dtoProxy = dtoProxy;
	}


	public String getMigrationServiceBeanName() {
		return this.migrationServiceBeanName;
	}


	public void setMigrationServiceBeanName(String migrationServiceBeanName) {
		this.migrationServiceBeanName = migrationServiceBeanName;
	}


	public String getMigrationServiceMethodName() {
		return this.migrationServiceMethodName;
	}


	public void setMigrationServiceMethodName(String migrationServiceMethodName) {
		this.migrationServiceMethodName = migrationServiceMethodName;
	}


	public boolean isBatchEnabled() {
		return this.batchEnabled;
	}


	public void setBatchEnabled(boolean batchEnabled) {
		this.batchEnabled = batchEnabled;
	}


	public boolean isTemplateEnabled() {
		return this.templateEnabled;
	}


	public void setTemplateEnabled(boolean templateEnabled) {
		this.templateEnabled = templateEnabled;
	}


	public String getServiceBeanName() {
		return this.serviceBeanName;
	}


	public void setServiceBeanName(String serviceBeanName) {
		this.serviceBeanName = serviceBeanName;
	}


	public String getServiceGetMethodName() {
		return this.serviceGetMethodName;
	}


	public void setServiceGetMethodName(String serviceGetMethodName) {
		this.serviceGetMethodName = serviceGetMethodName;
	}


	public String getServiceSaveMethodName() {
		return this.serviceSaveMethodName;
	}


	public void setServiceSaveMethodName(String serviceSaveMethodName) {
		this.serviceSaveMethodName = serviceSaveMethodName;
	}


	public String getServiceDeleteMethodName() {
		return this.serviceDeleteMethodName;
	}


	public void setServiceDeleteMethodName(String serviceDeleteMethodName) {
		this.serviceDeleteMethodName = serviceDeleteMethodName;
	}


	public Boolean getUploadAllowed() {
		return this.uploadAllowed;
	}


	public void setUploadAllowed(Boolean uploadAllowed) {
		this.uploadAllowed = uploadAllowed;
	}


	public String getUploadServiceBeanName() {
		return this.uploadServiceBeanName;
	}


	public void setUploadServiceBeanName(String uploadServiceBeanName) {
		this.uploadServiceBeanName = uploadServiceBeanName;
	}


	public String getUploadServiceMethodName() {
		return this.uploadServiceMethodName;
	}


	public void setUploadServiceMethodName(String uploadServiceMethodName) {
		this.uploadServiceMethodName = uploadServiceMethodName;
	}


	public String getDiscriminatorFormula() {
		return this.discriminatorFormula;
	}


	public void setDiscriminatorFormula(String discriminatorFormula) {
		this.discriminatorFormula = discriminatorFormula;
	}


	public String getDiscriminatorValue() {
		return this.discriminatorValue;
	}


	public void setDiscriminatorValue(String discriminatorValue) {
		this.discriminatorValue = discriminatorValue;
	}


	public Boolean getExcludeFromSql() {
		return this.excludeFromSql;
	}


	public void setExcludeFromSql(Boolean excludeFromSql) {
		this.excludeFromSql = excludeFromSql;
	}


	public Boolean getVersion() {
		return this.version;
	}


	public void setVersion(Boolean version) {
		this.version = version;
	}


	public String getSchema() {
		return this.schema;
	}


	public void setSchema(String schema) {
		this.schema = schema;
	}


	public DataTypeEncodingTypeNames getDefaultDataTypeEncodingType() {
		return this.defaultDataTypeEncodingType;
	}


	public void setDefaultDataTypeEncodingType(DataTypeEncodingTypeNames defaultDataTypeEncodingType) {
		this.defaultDataTypeEncodingType = defaultDataTypeEncodingType;
	}
}
