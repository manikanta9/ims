package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;


/**
 * The <code>DataTableRetrievalHandlerLocatorInSpringContext</code> class locates instances of {@link DataTableRetrievalHandler}
 * objects in Spring's context for the specified {@link DataSource} bean name.
 *
 * @author vgomelsky
 */
@Component
public class DataTableRetrievalHandlerLocatorInSpringContext implements DataTableRetrievalHandlerLocator, ApplicationContextAware {

	public static final String CACHE_NAME = DataTableRetrievalHandlerLocatorInSpringContext.class.getName();

	private CacheHandler<String, DataTableRetrievalHandler> cacheHandler;

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTableRetrievalHandler locate(String dataSourceName) {
		DataTableRetrievalHandler result = getCacheHandler().get(CACHE_NAME, dataSourceName);
		if (result == null) {
			// if not found in cache, search Spring context and then cache result
			String beanName = ContextConventionUtils.getBeanNameForDataSource(dataSourceName, DataTableRetrievalHandler.class);
			if (!this.applicationContext.containsBean(beanName)) {
				// did not find local implementation: look for external application
				beanName = ContextConventionUtils.getExternalApplicationBeanNameForDataSource(dataSourceName, DataTableRetrievalHandler.class);
				if (!this.applicationContext.containsBean(beanName)) {
					throw new IllegalStateException("Cannot find DataTableRetrievalHandler for data source '" + dataSourceName + "'.");
				}
			}

			result = (DataTableRetrievalHandler) this.applicationContext.getBean(beanName);
			getCacheHandler().put(CACHE_NAME, dataSourceName, result);
		}
		if (result == null) {
			throw new IllegalArgumentException("Cannot locate DataTableRetrievalHandler for the specified data source '" + dataSourceName + "'.");
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public CacheHandler<String, DataTableRetrievalHandler> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<String, DataTableRetrievalHandler> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
