package com.clifton.core.dataaccess.search.hibernate.expression;


/**
 * The <code>InExpressionForNumbers</code> class implements IN SQL clause
 *
 * @author vgomelsky
 */
public class InExpressionForNumbers extends AbstractInExpressionForNumbers {

	public InExpressionForNumbers(String propertyName, Object[] values) {
		super("IN", propertyName, values);
	}
}
