package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.db.DatabaseDataTypeConverter;
import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Constraint;
import com.clifton.core.dataaccess.migrate.schema.Index;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>MSSQLSchemaDDLSQLConverter</code> class generates SQL statements from the specified schema using Microsoft compliant syntax.
 * <p/>
 * TODO: support different SQL styles: plain, error if exists, skip if exists
 *
 * @author vgomelsky
 */
@Component
public class MSSQLSchemaDDLSQLConverter implements MigrationSchemaConverter {

	private DatabaseDataTypeConverter sqlDataTypeConverter;

	/**
	 * Data Migration Converter that runs Data SQL that is marked
	 * to runWith=DDL
	 */
	private MigrationSchemaConverter dataMigrationConverter;

	/**
	 * SQL Migration Converter that runs SQL SQL that is marked
	 * to runWith=DDL
	 */
	private MigrationSchemaConverter sqlMigrationConverter;


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.DDL;
	}


	@Override
	public String convert(Schema schema) {
		if (schema.getExcludeFromSql() != null && schema.getExcludeFromSql()) {
			return "";
		}
		StringBuilder sql = new StringBuilder(1024);
		for (Table table : CollectionUtils.getIterable(schema.getTableList())) {
			if (table.getExcludeFromSql() != null && table.getExcludeFromSql()) {
				// Table was explicitly marked to skip SQL for
				continue;
			}
			if (!StringUtils.isEmpty(table.getDataSource()) && !"dataSource".equals(table.getDataSource())) {
				// Don't execute DDL SQL for tables that are not part of this database.
				continue;
			}
			if (table.getOperationType() == OperationTypes.DELETE) {
				// 1. remove sequence(s)
				appendSequenceSQL(sql, table);

				// 2. remove index(s)
				appendIndexSQL(sql, table);

				// 3. remove constraint(s)
				appendConstraintSQL(sql, table);

				// 4. remove column(s)
				appendTableSQL(sql, table);
			}
			else {
				// 1. table with columns
				appendTableSQL(sql, table);

				// 2. constraints
				appendConstraintSQL(sql, table);

				// 3. indexes
				appendIndexSQL(sql, table);

				// 4. sequences
				appendSequenceSQL(sql, table);
			}
			sql.append("\n\n\n");
		}

		for (Table table : CollectionUtils.getIterable(schema.getTableList())) {
			appendConstraintSQL(sql, table, true);
		}

		appendRunWithSql(sql, schema);
		return sql.toString();
	}


	private void appendTableSQL(StringBuilder sql, Table table) {
		if (CollectionUtils.isEmpty(table.getColumnList(true))) {
			if (table.getOperationType() == OperationTypes.DELETE) {
				sql.append("DROP TABLE ");
				sql.append(table.getName());
				sql.append(getMigrationSchemaConverterType().getBatchSeparator());
			}
			return;
		}

		if (table.getOperationType() == OperationTypes.UPDATE || table.getOperationType() == OperationTypes.UPDATE_SQL_ONLY || table.getOperationType() == OperationTypes.DELETE) {
			sql.append("ALTER TABLE ");
		}
		else {
			sql.append("CREATE TABLE ");
		}
		if (!StringUtils.isEmpty(table.getSchema())) {
			sql.append(table.getSchema()).append(".");
		}
		sql.append(table.getName());
		if (table.getOperationType() == OperationTypes.UPDATE || table.getOperationType() == OperationTypes.UPDATE_SQL_ONLY) {
			sql.append(" ADD\n");
		}
		else if (table.getOperationType() == OperationTypes.DELETE) {
			sql.append(" DROP COLUMN ");
		}
		else {
			sql.append(" (\n");
		}

		List<Column> columnList = table.getColumnList(true);
		int columnCount = CollectionUtils.getSize(columnList);
		for (int i = 0; i < columnCount; i++) {
			Column column = columnList.get(i);
			if (table.getOperationType() != OperationTypes.DELETE) {
				sql.append("\t");
			}
			if (i != 0) {
				sql.append(", ");
			}
			sql.append(column.getName());
			if (table.getOperationType() != OperationTypes.DELETE) {
				sql.append(' ');
				if (column.isCalculatedColumn()) {
					sql.append(" AS ").append(column.getCalculation());
				}
				else {
					sql.append(getSqlDataTypeConverter().convert(column.getDataType(), column.getDataTypeEncodingType()));
					if (column.getDataType().isIdentity() && !table.isBatchEnabled()) {
						sql.append(" IDENTITY (1, 1)");
					}
					// If we are adding a required column that doesn't have a default - create as NULL, will change to NOT NULL in SQL migration
					if (table.getOperationType() == OperationTypes.UPDATE_SQL_ONLY && column.getColumnRequired() && column.getDefaultValue() == null) {
						sql.append(" NULL");
					}
					else {
						sql.append(column.getColumnRequired() ? " NOT NULL" : " NULL");
					}
					if (column.getDefaultValue() != null) {
						if (DataTypes.BIT != column.getDataType()) {
							throw new RuntimeException("Only BIT fields can have default values.  Please check table [" + table.getName() + "] and column [" + column.getName() + "]");
						}
						sql.append(" CONSTRAINT DF_");
						sql.append(table.getName());
						sql.append('_');
						sql.append(column.getName());
						sql.append(" DEFAULT(");
						sql.append(column.getDefaultValue());
						sql.append(')');
					}
				}
				if (i != (columnCount - 1)) {
					sql.append("\n");
				}
			}
		}
		if (table.getOperationType() != OperationTypes.UPDATE && table.getOperationType() != OperationTypes.UPDATE_SQL_ONLY && table.getOperationType() != OperationTypes.DELETE) {
			sql.append("\n)");
		}
		sql.append(getMigrationSchemaConverterType().getBatchSeparator());
	}


	private void appendConstraintSQL(StringBuilder sql, Table table) {
		appendConstraintSQL(sql, table, false);
	}


	private void appendConstraintSQL(StringBuilder sql, Table table, boolean processDelayed) {
		for (Constraint constraint : CollectionUtils.getIterable(table.getConstraintList())) {
			if (processDelayed != constraint.isDelayed()) {
				continue;
			}
			sql.append("ALTER TABLE ");
			sql.append(table.getName());
			if (table.getOperationType() == OperationTypes.DELETE) {
				sql.append(" DROP CONSTRAINT [");
				sql.append(constraint.getName());
				sql.append("]").append(getMigrationSchemaConverterType().getBatchSeparator());
			}
			else {
				if (constraint.getReferenceTable() == null) {
					sql.append(" ADD CONSTRAINT [");
					sql.append(constraint.getName());
					sql.append("]\n\tPRIMARY KEY CLUSTERED (");
					sql.append(BeanUtils.getPropertyValues(constraint.getPrimaryColumnList(), "name", ", "));
				}
				else {
					sql.append(" ADD CONSTRAINT [");
					sql.append(constraint.getName());
					sql.append("] FOREIGN KEY (");
					sql.append(BeanUtils.getPropertyValues(constraint.getPrimaryColumnList(), "name", ", "));
					sql.append(")\n\tREFERENCES ");
					sql.append(constraint.getReferenceTable().getName());
					sql.append("(");
					sql.append(BeanUtils.getPropertyValues(constraint.getReferenceColumnList(), "name", ", "));
				}
				sql.append(")").append(getMigrationSchemaConverterType().getBatchSeparator());
			}
		}
	}


	private void appendIndexSQL(StringBuilder sql, Table table) {
		for (Index index : CollectionUtils.getIterable(table.getIndexList())) {
			if (table.getOperationType() == OperationTypes.UPDATE_SQL_ONLY) {
				// FIXME NEED TO ADD IN SQL SQL, NOT DDL SQL
				continue;
			}
			if (table.getOperationType() == OperationTypes.DELETE) {
				sql.append("DROP");
			}
			else {
				sql.append("CREATE");
				if (index.isUnique()) {
					sql.append(" UNIQUE");
				}
			}
			sql.append(" INDEX [");
			sql.append(index.getName());
			sql.append("] ON ");
			sql.append(table.getName());
			if (table.getOperationType() != OperationTypes.DELETE) {
				sql.append(" (");
				sql.append(BeanUtils.getPropertyValues(index.getColumnList(), "name", ", "));
				sql.append(")");
				if (!CollectionUtils.isEmpty(index.getIncludeColumnList())) {
					sql.append(" INCLUDE ( ");
					sql.append(BeanUtils.getPropertyValues(index.getIncludeColumnList(), "name", ", "));
					sql.append(")");
				}
				// Restrict unique indexes for nullable properties with unique NULLs to non-null rows only
				if (index.isUnique()) {
					String nullableSql = Stream.concat(CollectionUtils.getStream(index.getColumnList()), CollectionUtils.getStream(index.getIncludeColumnList()))
							.filter(column -> BooleanUtils.isFalse(column.getRequired()))
							.filter(Column::isUniqueNulls)
							.map(column -> column.getName() + " IS NOT NULL")
							.collect(Collectors.joining(" OR "));
					if (!StringUtils.isEmpty(nullableSql)) {
						sql.append(" WHERE ").append(nullableSql);
					}
				}
			}

			sql.append(getMigrationSchemaConverterType().getBatchSeparator());
		}
	}


	private void appendSequenceSQL(StringBuilder sql, Table table) {
		if (table.isBatchEnabled() && table.getOperationType() != OperationTypes.UPDATE_SQL_ONLY) {
			if (table.getOperationType() == OperationTypes.DELETE) {
				sql.append("DROP");
			}
			else {
				sql.append("CREATE");
			}
			sql.append(" SEQUENCE [");
			sql.append(table.getName()).append("Sequence");
			sql.append("]");
			if (table.getOperationType() != OperationTypes.DELETE) {
				sql.append("START WITH 1 INCREMENT BY 50 MINVALUE 1 CACHE 50;");
			}
			sql.append(getMigrationSchemaConverterType().getBatchSeparator());
		}
	}


	private void appendRunWithSql(StringBuilder sql, Schema schema) {
		// Append Data Migrations that are RUN_WITH DDL
		String batchSep = getMigrationSchemaConverterType().getBatchSeparator();
		String appendBatchSep = getDataMigrationConverter().getMigrationSchemaConverterType().getBatchSeparator();
		String appendSql = getDataMigrationConverter().convert(schema);
		if (!StringUtils.isEmpty(appendSql) && !StringUtils.isEmpty(appendBatchSep) && !appendBatchSep.equalsIgnoreCase(batchSep)) {
			appendSql = appendSql.replaceAll(appendBatchSep, batchSep + StringUtils.NEW_LINE);
		}
		sql.append(appendSql);

		// Append SQL Migrations that are RUN_WITH DDL
		appendBatchSep = getSqlMigrationConverter().getMigrationSchemaConverterType().getBatchSeparator();
		appendSql = getSqlMigrationConverter().convert(schema);
		if (!StringUtils.isEmpty(appendSql) && !StringUtils.isEmpty(appendBatchSep) && !appendBatchSep.equalsIgnoreCase(batchSep)) {
			appendSql = appendSql.replaceAll(appendBatchSep, batchSep + StringUtils.NEW_LINE);
		}
		sql.append(appendSql);
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public DatabaseDataTypeConverter getSqlDataTypeConverter() {
		return this.sqlDataTypeConverter;
	}


	public void setSqlDataTypeConverter(DatabaseDataTypeConverter sqlDataTypeConverter) {
		this.sqlDataTypeConverter = sqlDataTypeConverter;
	}


	public MigrationSchemaConverter getDataMigrationConverter() {
		return this.dataMigrationConverter;
	}


	public void setDataMigrationConverter(MigrationSchemaConverter dataMigrationConverter) {
		this.dataMigrationConverter = dataMigrationConverter;
	}


	public MigrationSchemaConverter getSqlMigrationConverter() {
		return this.sqlMigrationConverter;
	}


	public void setSqlMigrationConverter(MigrationSchemaConverter sqlMigrationConverter) {
		this.sqlMigrationConverter = sqlMigrationConverter;
	}
}
