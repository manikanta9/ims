package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;

import java.util.List;


public interface SqlGroupedResultHandler {


	public <T extends IdentityObject> List<GroupedHierarchicalResult> getGroupedResult(HibernateSearchFormConfigurer entitySearchFormConfigurer, AdvancedReadOnlyWithGroupingDAO<T> dao);


	public <T extends IdentityObject> List<GroupedEntityResult<T>> getAggregateResult(HibernateSearchFormConfigurer entitySearchFormConfigurer, AdvancedReadOnlyWithGroupingDAO<T> dao);
}
