package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.nio.charset.Charset;
import java.sql.Types;
import java.util.List;


/**
 * The <code>CSVFileToDataTableConverter</code> ...
 *
 * @author stevenf
 */
public class CSVFileToDataTableConverter implements FileToDataTableConverter {

	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';
	private static final boolean DEFAULT_SKIP_EMPTY_LINES = true;


	////////////////////////////////////////////////////////////////////////////////

	private final char separator;
	private final boolean skipEmptyLines;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public CSVFileToDataTableConverter() {
		this(DEFAULT_SEPARATOR, DEFAULT_SKIP_EMPTY_LINES);
	}


	public CSVFileToDataTableConverter(char separator) {
		this(separator, DEFAULT_SKIP_EMPTY_LINES);
	}


	public CSVFileToDataTableConverter(char separator, boolean skipEmptyLines) {
		this.separator = separator;
		this.skipEmptyLines = skipEmptyLines;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private static List<CSVRecord> parseFile(File file, char separator, char customQuote) {
		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}

		List<CSVRecord> result;
		try (CSVParser parser = CSVParser.parse(file, Charset.defaultCharset(), CSVFormat.newFormat(separator).withQuote(customQuote))) {
			result = parser.getRecords();
		}
		catch (Exception e) {
			throw new RuntimeException("Error parsing csv file [" + e.getMessage() + "]", e);
		}
		return result;
	}

	@Override
	public DataTable convert(Object fileObj) {
		DataTable dataTable = new PagingDataTableImpl(new DataColumnImpl[0]);
		try {
			File file;
			if (fileObj instanceof MultipartFile) {
				file = FileUtils.convertMultipartFileToFile((MultipartFile) fileObj);
			}
			else {
				file = (File) fileObj;
			}
			DataColumnImpl[] columnList = new DataColumnImpl[0];
			List<CSVRecord> rows = parseFile(file, getSeparator(), DEFAULT_QUOTE);
			boolean headerRow = true;
			for (CSVRecord row : CollectionUtils.getIterable(rows)) {
				Object[] data = new Object[0];
				for (String column : row) {
					if (headerRow) {
						column = StringUtils.trim(column);
						DataColumnImpl columnName = new DataColumnImpl(column, Types.NVARCHAR);
						columnList = ArrayUtils.add(columnList, columnName);
					}
					else {
						data = ArrayUtils.add(data, column);
					}
				}
				if (headerRow) {
					dataTable = new PagingDataTableImpl(columnList);
					headerRow = false;
				}
				else {
					DataRow dataRow = new DataRowImpl(dataTable, data);
					if (isSkipEmptyLines() && isRowEmpty(dataRow)) {
						continue;
					}
					dataTable.addRow(dataRow);
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to process csv file.", e);
		}
		return dataTable;
	}




	private boolean isRowEmpty(DataRow data){
		for(Object fieldValue : CollectionUtils.getIterable(data.getRowValueList(false))){
				if(!StringUtils.isEmpty(String.valueOf(fieldValue))){
					return false;
				}
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public char getSeparator() {
		return this.separator;
	}


	public boolean isSkipEmptyLines() {
		return this.skipEmptyLines;
	}
}
