package com.clifton.core.dataaccess.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>ManyToOneEntity</code> annotation marks DTO fields as being many to one database relationships.
 * These are typically fields that reference a parent object which contains a list of the DTO object such as
 * the 'SystemBean' field in 'SystemBeanProperty' where SystemBean has a field List<SystemBeanProperty> propertyList
 * The annotation allows custom handling/loading of these properties.  For example, during JSON migrations ManyToOneEntity
 * fields are skipped when encountered since the child property is dependent on the parent being saved first.
 * <p>
 * Typically use of this annotation is only necessary when the parent object specifies the 'OneToManyEntity' annotation on
 * a field with the 'delayed' attribute set to 'false'
 *
 * @author stevenf
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface ManyToOneEntity {

}
