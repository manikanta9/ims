package com.clifton.core.dataaccess.migrate;


/**
 * The <code>MigrationCommand</code> defines the Migration Commands available when running
 * migrations;
 *
 * @author Mary Anderson
 */
public enum MigrationCommands {

	PRINT(false, false, false, true, false),
	BACKUP(false, false, false, false, true),
	MIGRATE(true, false, false, false, false),
	BACKUP_MIGRATE(true, false, false, false, true),
	REBUILD_PRINT(false, true, false, true, false),
	REBUILD_MIGRATE(true, true, false, false, false),
	RESTORE_PRINT(false, false, true, true, false),
	RESTORE_MIGRATE(true, false, true, false, false);

	////////////////////////////////////////////////////////////////////////////////


	MigrationCommands(boolean migrate, boolean rebuild, boolean restore, boolean print, boolean backup) {
		this.migrate = migrate;
		this.rebuild = rebuild;
		if (!rebuild) {
			this.restore = restore;
		}
		else {
			this.restore = false;
		}
		this.print = print;
		this.backup = backup;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * If true, will run migrations against the database
	 */
	private final boolean migrate;


	/**
	 * If true, will print out SQL
	 */
	private final boolean print;

	/**
	 * If true, will drop & recreate database, and will run/print SQL from scratch
	 * If false, will run/print migrations against current database
	 */
	private final boolean rebuild;

	/**
	 * Only valid if rebuild = false
	 * If true, will restore a previous version of the database from a specified file
	 * If false, will use existing database
	 */
	private final boolean restore;

	/**
	 * If true, will back up database to specified file prior to doing anything else
	 */
	private final boolean backup;


	////////////////////////////////////////////////////////////////////////////////
	/////////////           Getter and Setter Methods               ////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isMigrate() {
		return this.migrate;
	}


	public boolean isPrint() {
		return this.print;
	}


	public boolean isRebuild() {
		return this.rebuild;
	}


	public boolean isRestore() {
		return this.restore;
	}


	public boolean isBackup() {
		return this.backup;
	}
}
