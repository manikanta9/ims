package com.clifton.core.dataaccess.search.sql;


import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.util.StringUtils;


/**
 * The <code>SqlUnionSearchFormConfigurer</code> class is a SearchConfigurer that configures SqlSelectCommand objects with
 * SQL UNION clauses.  Each individual select is defined by the specified SqlSearchFormConfigurer objects.
 *
 * @author vgomelsky
 */
public class SqlUnionSearchFormConfigurer implements SearchConfigurer<SqlSelectCommand> {

	private final SqlSearchFormConfigurer[] sqlConfigurers;
	private final boolean unionAll;


	public SqlUnionSearchFormConfigurer(SqlSearchFormConfigurer[] sqlConfigurers, boolean unionAll) {
		if (sqlConfigurers == null || sqlConfigurers.length < 2) {
			throw new IllegalArgumentException("Two or more SqlSearchFormConfigurer objects must be passed for UNION");
		}
		this.sqlConfigurers = sqlConfigurers;
		this.unionAll = unionAll;
	}


	@Override
	public boolean isReadUncommittedRequested() {
		return false;
	}


	@Override
	public void configureCriteria(SqlSelectCommand criteria) {
		StringBuilder where = new StringBuilder(1024);
		for (int i = 0; i < this.sqlConfigurers.length; i++) {
			SqlSearchFormConfigurer sqlConfigurer = this.sqlConfigurers[i];
			sqlConfigurer.validateRestrictions();

			sqlConfigurer.configureWhereClause(criteria);
			if (i == 0) {
				// configure select and from based on the first configurer and add union's after the first where
				if (!StringUtils.isEmpty(criteria.getWhereClause())) {
					where.append(criteria.getWhereClause());
					criteria.setWhereClause(null);
				}
				else {
					where.append("1 = 1");
				}
				criteria.setSelectClause(sqlConfigurer.getSelectClause());
				criteria.setFromClause(sqlConfigurer.getFromClause());
			}
			else {
				where.append(this.unionAll ? "\n\nUNION ALL\n\nSELECT " : "\n\nUNION\n\nSELECT ");
				where.append(sqlConfigurer.getSelectClause());
				if (sqlConfigurer.getFromClause() != null) {
					where.append("\nFROM ");
					where.append(sqlConfigurer.getFromClause());
				}
				if (!StringUtils.isEmpty(criteria.getWhereClause())) {
					where.append("\nWHERE ");
					where.append(criteria.getWhereClause());
					criteria.setWhereClause(null);
				}
			}
		}

		criteria.setStart(getStart());
		criteria.setLimit(getLimit());

		criteria.setWhereClause(where.toString());
	}


	@Override
	public boolean configureOrderBy(SqlSelectCommand criteria) {
		return this.sqlConfigurers[0].configureOrderBy(criteria);
	}


	@Override
	public int getLimit() {
		return this.sqlConfigurers[0].getLimit();
	}


	@Override
	public Integer getMaxLimit() {
		return this.sqlConfigurers[0].getMaxLimit();
	}


	@Override
	public int getStart() {
		return this.sqlConfigurers[0].getStart();
	}
}
