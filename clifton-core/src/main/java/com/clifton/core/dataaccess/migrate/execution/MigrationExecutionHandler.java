package com.clifton.core.dataaccess.migrate.execution;


import org.springframework.context.support.GenericApplicationContext;

import java.util.List;


/**
 * The <code>MigrationExecutionHandler</code> defines methods for generating and executing database migrations.
 * <p>
 * Database migrations are SQL statements that evolve databases along with code. All database changes (DDL and DML)
 * should be performed via database migrations. This process will provide meta-data for DAO components, audit trail of
 * all database changes, ability to test database changes, consistent updates across all environments.
 * </p>
 *
 * @author vgomelsky
 */
public interface MigrationExecutionHandler {

	/**
	 * Generates and executes SQL statements that migrate all dependent modules to the latest version.
	 */
	public void migrate(GenericApplicationContext springContext);


	/**
	 * Generates and returns SQL statements necessary to migrate all dependent modules to the latest version.
	 */
	public String generateSQL();


	/**
	 * Executes sql defined in specified file
	 */
	public void executeSQLFile(String sqlFileName);


	/**
	 * Executes sql defined in list of specified files
	 */
	public void executeSQLFiles(List<String> sqlFileNames);
}
