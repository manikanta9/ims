package com.clifton.core.dataaccess.search.form;


import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;

import java.util.List;


/**
 * The <code>RestrictiveSearchForm</code> interface declares methods to be implemented by a search form with explicit search restrictions on individual fields.
 * <p>
 * Form fields can be annotated with the {@link SearchField} annotation to define search field restrictions which are automatically configured by the {@link
 * HibernateSearchFormConfigurer}.
 *
 * @author vgomelsky
 */
public interface RestrictiveSearchForm {

	public List<SearchRestriction> getRestrictionList();


	public void setRestrictionList(List<SearchRestriction> restrictionList);


	/**
	 * Returns true if the specified field is part of search restrictions for this SearchForm.
	 */
	public boolean containsSearchRestriction(String fieldName);


	/**
	 * Removes {@link SearchRestriction} for the field with the specified name.
	 * Returns removed restriction or null.
	 */
	public SearchRestriction removeSearchRestriction(String fieldName);


	public SearchRestriction getSearchRestriction(String fieldName);


	/**
	 * Add a restriction to the list, creates the list if it is null.
	 */
	public void addSearchRestriction(SearchRestriction restriction);
}
