package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>DaoEventContext</code> class represents a DAO event context. Before/After refer to before and  after method call aspects.
 * The context can store original DTO and should be cleared on last after method call (so that other DAO calls get their own context).
 *
 * @param <T>
 * @author vgomelsky
 */
public class DaoEventContext<T extends IdentityObject> {

	/**
	 * A Map of unique bean id (class + id) to corresponding original bean (prior to update)
	 */
	private final Map<String, T> originalBeanMap = new HashMap<>();
	/**
	 * A Map of optional/custom attributes for a specific bean instance.
	 */
	private Map<String, Object> attributeMap;
	/**
	 * A stack used for tracking DTO DAO actions. Each nested DAO action will result in a new DaoActionCallCounter pushed
	 * to the stack that tracks before and after calls to know when the action is complete.
	 */
	private Deque<DaoActionCallCounter> actionStack = new ArrayDeque<>();


	/**
	 * Returns true if this is the last after method call for an initial DAO action, including all nested DAO actions that occurred.
	 * <p>
	 * To determine when the initial DAO action, and each inner/nested action, is complete use {@link #isLastIntermediateAfterCall()}.
	 */
	public boolean isLastAfterCall() {
		return isLastIntermediateAfterCall() && this.actionStack.size() == 1;
	}


	/**
	 * Returns true if this is the last after method call for an unique DAO action. This method will return true for each
	 * DAO action performed, even if they are nested (occur within an initiating DAO action).
	 * <p>
	 * To determine when the initial DAO action completes use {@link #isLastAfterCall()}.
	 */
	public boolean isLastIntermediateAfterCall() {
		return !this.actionStack.isEmpty() && this.actionStack.peek().isComplete();
	}


	/**
	 * Increments beforeCallCount by one and returns the new currentCallDepth.
	 */
	public int recordBeforeCall() {
		if (this.actionStack.isEmpty() || this.actionStack.peek().isCountingDown()) {
			this.actionStack.push(new DaoActionCallCounter());
		}
		if (this.actionStack.peek() == null) {
			// This should never happen, but throw an exception to be safe.
			throw new IllegalStateException("DAO action stack is empty.");
		}
		return this.actionStack.peek().incrementBeforeCount();
	}


	/**
	 * Increments afterCallCount by one and returns the new currentCallDepth.
	 */
	public int recordAfterCall() {
		if (!this.actionStack.isEmpty()) {
			// Check for complete and pop before decrementing so the isLast... methods perform correctly (the last action remains in the stack).
			if (this.actionStack.peek().isComplete()) {
				this.actionStack.pop(); // pop complete and process next
			}
			if (this.actionStack.peek() == null) {
				// More after calls were recorded than before calls. This should never happen, but throw an exception to be safe.
				throw new IllegalStateException("More DAO action after calls occurred than before calls. This violates anticipated Observer behavior.");
			}
			return this.actionStack.peek().incrementAfterCount();
		}
		return 0;
	}


	/**
	 * @return the number of recorded before calls
	 */
	public int getBeforeCallCount() {
		return this.actionStack.isEmpty() ? 0 : this.actionStack.peek().getBeforeCallCount();
	}


	/**
	 * @return the number of recorded after calls
	 */
	public int getAfterCallCount() {
		return this.actionStack.isEmpty() ? 0 : (this.actionStack.peek().getAfterCallCount());
	}


	public T getOriginalBean(T bean) {
		return bean == null ? null : this.originalBeanMap.get(generateBeanKey(bean));
	}


	public void setOriginalBean(T originalBean) {
		if (originalBean != null) {
			this.originalBeanMap.put(generateBeanKey(originalBean), originalBean);
		}
	}


	public void removeOriginalBean(T bean) {
		if (bean != null) {
			this.originalBeanMap.remove(generateBeanKey(bean));
		}
	}


	/**
	 * Saves a custom attribute value for the specified bean.  It can be retrieved later by using getBeanAttribute.
	 */
	public void setBeanAttribute(T bean, String attributeName, Object attributeValue) {
		if (bean != null) {
			if (this.attributeMap == null) {
				this.attributeMap = new HashMap<>();
			}
			this.attributeMap.put(generateBeanKey(bean) + attributeName, attributeValue);
		}
	}


	/**
	 * Removes the specified custom attribute if present: could be used multiple times.
	 */
	public void removeBeanAttribute(T bean, String attributeName) {
		if (this.attributeMap != null && bean != null) {
			this.attributeMap.remove(generateBeanKey(bean) + attributeName);
		}
	}


	/**
	 * Returns the value of custom bean attributed saved by setBeanAttribute.
	 */
	public Object getBeanAttribute(T bean, String attributeName) {
		return (this.attributeMap == null || bean == null) ? null : this.attributeMap.get(generateBeanKey(bean) + attributeName);
	}


	/**
	 * Updates all attribute keys and bean key from new bean's to this of the specified bean.
	 * This is necessary for inserts so that original object/attributes can still be found.
	 */
	public void updateFromNewTo(T bean) {
		String oldKey = generateNewBeanKey(bean);
		String newKey = generateBeanKey(bean);
		if (this.attributeMap != null) {
			String[] keys = this.attributeMap.keySet().toArray(new String[0]);
			for (String key : keys) {
				if (key.startsWith(oldKey)) {
					Object value = this.attributeMap.remove(key);
					this.attributeMap.put(newKey + key.substring(oldKey.length()), value);
				}
			}
		}
		T value = this.originalBeanMap.remove(oldKey);
		if (value != null) {
			this.originalBeanMap.put(newKey, value);
		}
	}


	private String generateBeanKey(T bean) {
		return bean.getClass().getName() + bean.getIdentity();
	}


	private String generateNewBeanKey(T bean) {
		return bean.getClass().getName() + null;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////

	/**
	 * A class to help track the observer counts around DAO action. Each unique action will be tracked independently to be able to
	 * determine when each action is complete. Each action has a number of before and after usages. The number of before usages
	 * must match the number of after usages.
	 */
	public static final class DaoActionCallCounter {

		int currentCallDepth = 0;
		int maxCallDepth = 0;
		boolean countingDown = false;


		/**
		 * Increase the before action counter.
		 */
		int incrementBeforeCount() {
			this.currentCallDepth++;
			this.maxCallDepth++;
			return this.currentCallDepth;
		}


		/**
		 * Increase the after action counter.
		 * <p>
		 * This actually decreases the before action depth and sets the state for {@link #isCountingDown()} to true.
		 */
		int incrementAfterCount() {
			this.countingDown = true;
			this.currentCallDepth--;
			if (isComplete()) {
				this.countingDown = false;
			}
			return this.currentCallDepth;
		}


		/**
		 * Returns the current call depth. The returned value is the number of {@link #incrementAfterCount()} ()}s expected to complete this action.
		 */
		int getCurrentCallDepth() {
			return this.currentCallDepth;
		}


		/**
		 * Returns the number of before calls made for this DAO action.
		 */
		int getBeforeCallCount() {
			return this.maxCallDepth;
		}


		/**
		 * Returns the number of after calls made for this DAO action.
		 */
		int getAfterCallCount() {
			return getBeforeCallCount() - getCurrentCallDepth();
		}


		/**
		 * Returns true if the DAO action has been performed and has started decrementing (counting down).
		 */
		boolean isCountingDown() {
			return this.countingDown;
		}


		/**
		 * Returns true if the before calls match the after calls resulting in a {@link #getCurrentCallDepth()} of 0.
		 */
		boolean isComplete() {
			return this.currentCallDepth == 0;
		}
	}
}
