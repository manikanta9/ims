package com.clifton.core.dataaccess.migrate;


/**
 * The <code>OperationTypes</code> enumeration defines available types of migrations operations. CREATE should be the
 * default type.
 *
 * @author vgomelsky
 */
public enum OperationTypes {

	CREATE, // Default Operation Type - used to Create a new table/columns/indexes
	CREATE_SQL_ONLY, // Adds table to SQL migration only. Used when adding a table after the fact, but the table schema must be added to a previous migration because of dependency, but want the SQL from the create statement to still be executed in a later migration.  Changes DO NOT update the schema.
	UPDATE, // Adds columns, indexes to an existing table. Should be used only to print a migration and copy the sql into the file.  Rarely should actually be a committed change
	UPDATE_SQL_ONLY, // Adds columns, indexes to an existing table.  The original table definition should be updated to contain the new columns.  Will not update the schema but will execute the SQL
	DELETE // Removes table and/or columns from an existing table.  Should be used only to print a migration and copy the sql into the file.  Should never be a committed change
}
