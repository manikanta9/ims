package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * @author davidi
 */
public class DateDiffOrderBy extends Order {

	private String dateDiffFunction;
	private String[] propertyNames;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DateDiffOrderBy(String dateDiffFunction, String[] propertyNames, boolean isAscending) {

		super(null, isAscending);
		this.dateDiffFunction = dateDiffFunction;
		this.propertyNames = propertyNames;
	}


	public static DateDiffOrderBy create(OrderByDirections direction, String dateDiffFunction, String[] propertyNames) {
		return new DateDiffOrderBy(dateDiffFunction, propertyNames, direction == OrderByDirections.ASC);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		ValidationUtils.assertTrue(this.propertyNames.length == 2, "Two date fields are required for the DATEDIFF function.");
		String startDate = criteriaQuery.getColumn(criteria, this.propertyNames[0]);
		String endDate = criteriaQuery.getColumn(criteria, this.propertyNames[1]);

		StringBuilder stringBuilder = new StringBuilder(String.format(this.dateDiffFunction, startDate, endDate));
		stringBuilder.append(" ");
		stringBuilder.append(isAscending() ? "asc" : "desc");

		return stringBuilder.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getPropertyNames() {
		return this.propertyNames;
	}
}
