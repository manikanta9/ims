package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>BaseDaoEventObserver</code> class is a helper class that should be extended by all implementations of
 * {@link DaoEventObserver} interface. In addition to providing helper methods for getting the original bean,
 * it also maintains {@link DaoEventContext} and clears it after the last after call (can be very useful when multiple observers are registered for the same event).
 *
 * @param <T>
 * @author vgomelsky
 */
public abstract class BaseDaoEventObserver<T extends IdentityObject> implements DaoEventObserver<T> {

	public static final String DAO_EVENT_CONTEXT_KEY = "DAO_EVENT_CONTEXT";

	private int order;
	private ContextHandler contextHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.getClass().getName() + " [Order = " + this.order + "]";
	}


	@Override
	@SuppressWarnings("unused")
	public void configurePersistedInstance(T persistedInstance, T originalInstance) {
		// do nothing
	}


	@SuppressWarnings("unused")
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// do nothing
	}


	@SuppressWarnings("unused")
	protected void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// do nothing
	}


	@SuppressWarnings("unused")
	protected void beforeTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// do nothing
	}


	@SuppressWarnings("unused")
	protected void afterTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// do nothing
	}


	@Override
	public final void beforeMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		getDaoEventContext().recordBeforeCall();

		try {
			beforeMethodCallImpl(dao, event, bean);
		}
		catch (Throwable e) {
			// after will not be called: remove the bean
			getContextHandler().removeBean(DAO_EVENT_CONTEXT_KEY);
			if (e instanceof ValidationException) {
				throw (ValidationException) e;
			}
			throw new RuntimeException("Error in beforeMethodCallImpl(dao, " + event + ", " + bean + ")", e);
		}
	}


	@Override
	public final void afterMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		DaoEventContext<T> eventContext = getDaoEventContext();
		eventContext.recordAfterCall();

		try {
			afterMethodCallImpl(dao, event, bean, e);
		}
		catch (Throwable ex) {
			// perform clean-up even if there was an exception
			getContextHandler().removeBean(DAO_EVENT_CONTEXT_KEY);
			if (ex instanceof ValidationException) {
				throw (ValidationException) ex;
			}
			throw new RuntimeException("Error in afterMethodCallImpl(dao, " + event + ", " + bean + ", " + ExceptionUtils.getDetailedMessage(e) + ")", ex);
		}

		if (eventContext.isLastAfterCall() || e != null) {
			getContextHandler().removeBean(DAO_EVENT_CONTEXT_KEY);
		}
	}


	@Override
	public final void beforeTransactionMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		getDaoEventContext().recordBeforeCall();

		try {
			beforeTransactionMethodCallImpl(dao, event, bean);
		}
		catch (Throwable e) {
			// after will not be called: remove the bean
			getContextHandler().removeBean(DAO_EVENT_CONTEXT_KEY);
			if (e instanceof ValidationException) {
				throw (ValidationException) e;
			}
			throw new RuntimeException("Error in beforeTransactionMethodCallImpl(dao, " + event + ", " + bean + ")", e);
		}
	}


	@Override
	public final void afterTransactionMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		DaoEventContext<T> eventContext = getDaoEventContext();
		eventContext.recordAfterCall();

		try {
			afterTransactionMethodCallImpl(dao, event, bean, e);
		}
		catch (Throwable ex) {
			// perform clean-up even if there was an exception
			getContextHandler().removeBean(DAO_EVENT_CONTEXT_KEY);
			if (ex instanceof ValidationException) {
				throw (ValidationException) ex;
			}
			throw new RuntimeException("Error in afterTransactionMethodCall(" + dao + ", " + event + ", " + bean + ", " + e + ")", ex);
		}

		if (eventContext.isLastAfterCall() || e != null) {
			getContextHandler().removeBean(DAO_EVENT_CONTEXT_KEY);
		}
	}


	protected DaoEventContext<T> getDaoEventContext() {
		@SuppressWarnings("unchecked")
		DaoEventContext<T> eventContext = (DaoEventContext<T>) getContextHandler().getBean(DAO_EVENT_CONTEXT_KEY);
		if (eventContext == null) {
			eventContext = new DaoEventContext<>();
			getContextHandler().setBean(DAO_EVENT_CONTEXT_KEY, eventContext);
		}
		return eventContext;
	}


	/**
	 * Returns the original bean instance of the specified bean (the one that hasn't been updated yet in the database).
	 * Assumes that passed bean instance was modified but hasn't been persisted yet.
	 */
	protected T getOriginalBean(final ReadOnlyDAO<T> dao, final T bean) {
		DaoEventContext<T> eventContext = getDaoEventContext();
		T originalBean = eventContext.getOriginalBean(bean);
		if (originalBean == null && !bean.isNewBean()) {
			originalBean = DaoUtils.executeDetached(true, () -> dao.findByPrimaryKey(bean.getIdentity()));
			eventContext.setOriginalBean(originalBean);
		}
		return originalBean;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
