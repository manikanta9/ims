package com.clifton.core.dataaccess.search.hibernate;


import com.clifton.core.dataaccess.search.OrderByDirections;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * The <code>CoalesceOrderBy</code> is used for Coalesce search fields and parses the propertyName string for the multiple properties and appends as coalesce in order by clause
 *
 * @author manderson
 */
public class CoalesceOrderBy extends Order {

	private final String[] propertyNames;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructor for Order.
	 */
	private CoalesceOrderBy(String[] propertyNames, boolean ascending) {
		super(null, ascending);
		this.propertyNames = propertyNames;
	}


	public static CoalesceOrderBy create(OrderByDirections direction, String[] propertyNames) {
		return new CoalesceOrderBy(propertyNames, direction == OrderByDirections.ASC);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragmentSb = new StringBuilder("COALESCE(");
		for (int i = 0; i < this.propertyNames.length; i++) {
			String column = criteriaQuery.getColumn(criteria, this.propertyNames[i]);
			fragmentSb.append(column);
			if (i < this.propertyNames.length - 1) {
				fragmentSb.append(",");
			}
		}
		fragmentSb.append(") ");
		fragmentSb.append(isAscending() ? "asc" : "desc");
		return fragmentSb.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getPropertyNames() {
		return this.propertyNames;
	}
}
