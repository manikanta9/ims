package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.validation.ValidationException;

import java.util.List;


/**
 * @author manderson
 */
public interface DaoCompositeKeyListCache<T extends IdentityObject, K1, K2> {

	/**
	 * Returns the list of beans with the give key values
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 * Note: Does not enforce that the list is not empty, use strict method to require at least one entity exists.
	 */
	public List<T> getBeanListForKeyValues(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2);


	/**
	 * Returns the list of beans with the give key values
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 *
	 * @throws ValidationException if the list is empty
	 */
	public List<T> getBeanListForKeyValuesStrict(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2);
}
