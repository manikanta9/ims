package com.clifton.core.dataaccess.datatable.bean;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;

import java.util.Map;
import java.util.function.Function;


/**
 * {@link DataTableConverterConfiguration} contains fields required for various {@link DataTableConverter}s.
 *
 * @author michaelm
 */
public class DataTableConverterConfiguration {

	private ApplicationContextService applicationContextService;
	private Class<?> dtoClass;

	private String dataColumnString;
	private DataColumn[] dataColumnArray;

	/**
	 * {@link Function} that can be used to convert each {@link Map.Entry} to {@link Object[]} data that can be used to create a {@link DataRow}.
	 */
	private Function<Map.Entry<Object, Object>, Object[]> mapEntryToDataArrayFunction;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DataTableConverterConfiguration() {
		// use static constructors instead
	}


	public static DataTableConverterConfiguration ofDtoClassUsingDataColumnString(ApplicationContextService applicationContextService, Class<?> dtoClass, String dataColumnString) {
		DataTableConverterConfiguration command = ofDataColumnString(dataColumnString);
		command.applicationContextService = applicationContextService;
		command.dtoClass = dtoClass;
		return command;
	}


	public static DataTableConverterConfiguration ofDtoClassUsingDataColumnArray(ApplicationContextService applicationContextService, Class<?> dtoClass, DataColumn[] dataColumnArray) {
		DataTableConverterConfiguration command = ofDataColumnString(null);
		command.applicationContextService = applicationContextService;
		command.dtoClass = dtoClass;
		command.dataColumnArray = dataColumnArray;
		return command;
	}


	public static DataTableConverterConfiguration ofMapDataRowFunctionUsingDataColumnString(Function<Map.Entry<Object, Object>, Object[]> mapDataRowFunction, String dataColumnString) {
		DataTableConverterConfiguration command = ofDataColumnString(dataColumnString);
		command.mapEntryToDataArrayFunction = mapDataRowFunction;
		return command;
	}


	public static DataTableConverterConfiguration ofMapDataRowFunctionUsingDataColumnArray(Function<Map.Entry<Object, Object>, Object[]> mapDataRowFunction, DataColumn[] dataColumnArray) {
		DataTableConverterConfiguration command = ofDataColumnString(null);
		command.mapEntryToDataArrayFunction = mapDataRowFunction;
		command.dataColumnArray = dataColumnArray;
		return command;
	}


	public static DataTableConverterConfiguration ofDtoClass(ApplicationContextService applicationContextService, Class<?> dtoClass) {
		DataTableConverterConfiguration command = ofDataColumnString(null);
		command.applicationContextService = applicationContextService;
		command.dtoClass = dtoClass;
		return command;
	}


	public static DataTableConverterConfiguration ofDataColumnString(String dataColumnString) {
		DataTableConverterConfiguration command = new DataTableConverterConfiguration();
		command.dataColumnString = dataColumnString;
		return command;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public Class<?> getDtoClass() {
		return this.dtoClass;
	}


	public String getDataColumnString() {
		return this.dataColumnString;
	}


	public Function<Map.Entry<Object, Object>, Object[]> getMapEntryToDataArrayFunction() {
		return this.mapEntryToDataArrayFunction;
	}


	public DataColumn[] getDataColumnArray() {
		return this.dataColumnArray;
	}
}
