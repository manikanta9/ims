package com.clifton.core.dataaccess.migrate.schema;


/**
 * The <code>Subclass</code> class represents a subclass of database table.
 * Used for inheritance (Table per class hierarchy)
 *
 * @author manderson
 */
public class Subclass extends Table {

	// NOTHING HERE
}
