package com.clifton.core.dataaccess.search.grouping;

import java.util.List;


/**
 * The <code>GroupingSearchForm</code> interface should be implemented by any search form that will support grouped searches
 */
public interface GroupingSearchForm {

	public List<GroupingProperty> getGroupingPropertyList();


	public List<GroupingAggregateProperty> getGroupingAggregatePropertyList();
}
