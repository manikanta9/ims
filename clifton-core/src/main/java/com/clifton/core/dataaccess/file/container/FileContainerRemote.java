package com.clifton.core.dataaccess.file.container;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.hierynomus.msdtyp.AccessMask;
import com.hierynomus.msfscc.FileAttributes;
import com.hierynomus.msfscc.fileinformation.FileDirectoryQueryableInformation;
import com.hierynomus.mssmb2.SMB2CreateDisposition;
import com.hierynomus.mssmb2.SMB2ShareAccess;
import com.hierynomus.protocol.commons.EnumWithValue;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.common.SmbPath;
import com.hierynomus.smbj.connection.Connection;
import com.hierynomus.smbj.session.Session;
import com.hierynomus.smbj.share.DiskEntry;
import com.hierynomus.smbj.share.DiskShare;
import com.hierynomus.smbj.share.File;
import com.hierynomus.utils.Strings;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.EnumSet;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;


/**
 * Represents files that are NOT accessible to the host operating system natively
 * <p>
 * I.e. on Linux hosts, SMB shares are not accessible natively
 * <p>
 * //server/share/dirpath/file
 *
 * @author theodorez
 */
public class FileContainerRemote implements FileContainer {

	// SmbPath normalizes to backslashes
	// example format:  //server/share/dirpath/file
	public static final String SMB_PATH_SEPARATOR = "\\";

	// Linux will include these with the directory listing
	private static final String CURRENT_DIRECTORY_NAME = ".";
	private static final String PARENT_DIRECTORY_NAME = "..";

	private final FileContainerContext fileContainerContext;
	private final SmbPath path;


	public FileContainerRemote(String url, FileContainerContext fileContainerContext) {
		// don't need the smb: prefix.
		if (StringUtils.startsWith(url, "smb:") || StringUtils.startsWith(url, "SMB:")) {
			this.path = SmbPath.parse(url.substring(4));
		}
		else {
			this.path = SmbPath.parse(Strings.isNotBlank(url) ? url : "");
		}
		this.fileContainerContext = fileContainerContext;
	}


	@Override
	public void deleteFile() {
		performOperation(diskShare -> {
			diskShare.rm(this.getFilePathOnly());
			return null;
		});
	}


	@Override
	public boolean exists() {
		return performOperation(diskShare -> {
			try {
				diskShare.open(this.getFilePathOnly(), EnumSet.of(AccessMask.GENERIC_READ), null, EnumSet.of(SMB2ShareAccess.FILE_SHARE_READ),
						SMB2CreateDisposition.FILE_OPEN, null);
				return true;
			}
			catch (Exception e) {
				return false;
			}
		});
	}


	@Override
	public boolean isDirectory() {
		return performOperation(diskShare -> diskShare.folderExists(this.getFilePathOnly()));
	}


	@Override
	public boolean canWrite() {
		return performOperation(diskShare -> {
			DiskEntry diskEntry = diskShare.open(this.getFilePathOnly(), EnumSet.of(AccessMask.GENERIC_READ), null,
					EnumSet.of(SMB2ShareAccess.FILE_SHARE_READ), SMB2CreateDisposition.FILE_OPEN, null);
			return !EnumWithValue.EnumUtils.isSet(diskEntry.getFileInformation().getBasicInformation().getFileAttributes(), FileAttributes.FILE_ATTRIBUTE_READONLY);
		});
	}


	@Override
	public String[] list() {
		return performOperation(diskShare -> CollectionUtils.buildStream(diskShare.list(this.getFilePathOnly()))
				.map(FileDirectoryQueryableInformation::getFileName)
				.filter(entry -> !(entry.equals(CURRENT_DIRECTORY_NAME) || (entry.equals(PARENT_DIRECTORY_NAME))))
				.toArray(String[]::new)
		);
	}


	@Override
	public Stream<String> list(String wildcard, Predicate<String> fileNameMatcher) {
		String wildcardPattern = StringUtils.isEmpty(wildcard) ? "*" : wildcard;
		Predicate<String> fileNamePredicate = fileNameMatcher == null ? name -> true : fileNameMatcher;
		return performOperation(diskShare -> CollectionUtils.buildStream(diskShare.list(this.getFilePathOnly(), wildcardPattern))
				.map(FileDirectoryQueryableInformation::getFileName)
				.filter(entry -> !(entry.equals(CURRENT_DIRECTORY_NAME) || (entry.equals(PARENT_DIRECTORY_NAME))))
				.filter(fileNamePredicate)
		);
	}


	/**
	 * @return //server/share/dirpath/file
	 */
	@Override
	public String getAbsolutePath() {
		return this.path.toUncPath();
	}


	/**
	 * @return //server/share/dirpath/file
	 */
	@Override
	public String getPath() {
		return this.path.toUncPath();
	}


	/**
	 * //server/share/dirpath/file
	 *
	 * @return file
	 */
	@Override
	public String getName() {
		String name = getFilePathOnly();
		if (Strings.isNotBlank(name)) {
			int index = name.lastIndexOf(SMB_PATH_SEPARATOR);
			if (index > 1 && (index + 1) < name.length()) {
				name = name.substring(index + 1);
			}
		}
		return name;
	}


	@Override
	public InputStream getInputStream() throws IOException {
		return new RemoteInputStreamWrapper(this);
	}


	@Override
	public OutputStream getOutputStream() throws IOException {
		return new RemoteOutStreamWrapper(this);
	}


	@Override
	public String getParent() {
		String parent = getAbsolutePath();
		if (Strings.isNotBlank(parent)) {
			int index = parent.lastIndexOf(SMB_PATH_SEPARATOR);
			if (index > 1) {
				return parent.substring(0, index);
			}
		}
		return null;
	}


	@Override
	public boolean makeDirs() {
		return performOperation(diskShare -> {
			try {
				diskShare.mkdir(this.getFilePathOnly());
				return true;
			}
			catch (Exception e) {
				return false;
			}
		});
	}


	@Override
	public byte[] readBytes() throws IOException {
		if (this.length() > 1_073_741_824) {
			throw new RuntimeException("Cannot read the bytes from file: " + this.getAbsolutePath() + "larger than 1 gigabytes");
		}

		try (
				InputStream is = this.getInputStream();
				//Taken from: http://www.baeldung.com/convert-input-stream-to-array-of-bytes
				ByteArrayOutputStream buffer = new ByteArrayOutputStream()
		) {
			int numberRead;
			byte[] data = new byte[1024];
			while ((numberRead = is.read(data, 0, data.length)) != -1) {
				buffer.write(data, 0, numberRead);
			}
			buffer.flush();
			return buffer.toByteArray();
		}
	}


	@Override
	public void writeBytes(byte[] bytes) throws IOException {
		try (OutputStream os = getOutputStream()) {
			os.write(bytes);
		}
	}


	@Override
	public long length() {
		return performOperation(diskShare -> {
			DiskEntry diskEntry = diskShare.open(this.getFilePathOnly(), EnumSet.of(AccessMask.GENERIC_READ), null,
					EnumSet.of(SMB2ShareAccess.FILE_SHARE_READ), SMB2CreateDisposition.FILE_OPEN, null);
			return diskEntry.getFileInformation().getStandardInformation().getEndOfFile();
		});
	}


	@Override
	public boolean renameToFile(FileContainer toFile) {
		AssertUtils.assertTrue(toFile instanceof FileContainerRemote, "Expected a FileContainerRemote");

		return performOperation(diskShare -> {
			DiskEntry fromDiskEntry = diskShare.open(this.getFilePathOnly(), EnumSet.of(AccessMask.DELETE, AccessMask.GENERIC_WRITE),
					EnumWithValue.EnumUtils.toEnumSet(diskShare.getFileInformation(getFilePathOnly()).getBasicInformation().getFileAttributes(), FileAttributes.class),
					SMB2ShareAccess.ALL, SMB2CreateDisposition.FILE_OPEN, null);
			if (fromDiskEntry instanceof File && toFile instanceof FileContainerRemote) {
				FileContainerRemote toFileContainerRemote = (FileContainerRemote) toFile;
				if (toFileContainerRemote.path.isOnSameHost(this.path) && toFileContainerRemote.path.isOnSameShare(this.path)) {
					// rename needs a path with all DFS segments resolved.
					String resolvedPath = resolvePath(diskShare, toFileContainerRemote.getPathOnly());
					if (!resolvedPath.endsWith(SMB_PATH_SEPARATOR)) {
						resolvedPath += SMB_PATH_SEPARATOR;
					}
					SmbPath newPath = SmbPath.parse(resolvedPath + toFileContainerRemote.getName());
					try {
						LogUtils.info(FileContainerRemote.class, String.format("Renaming from [%s] to [%s].", fromDiskEntry.getFileName(), newPath.getPath()));
						fromDiskEntry.rename(newPath.getPath());
						return true;
					}
					catch (Exception e) {
						// swallow exception, log it and return false.
						LogUtils.error(FileContainerRemote.class, String.format("Could not rename [%s] to [%s].", this.getAbsolutePath(), toFile.getAbsolutePath()), e);
					}
				}
			}
			return false;
		});
	}

	//////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////


	/**
	 * Resolve any DFS segments in the provided path.  The directory
	 * represented by the path must exist to resolve the segments.
	 * DO NOT CLOSE provided DiskShare.
	 * <p>
	 * //DFS/share/dir/dir
	 * resolves to
	 * //server/share/dir/dir
	 * <p>
	 * //paraport.com/MPLS/Development/QA
	 * resolves to
	 * //msp-700-03/Development/QA
	 */
	private String resolvePath(DiskShare diskShare, String pathOnly) {
		// if pathOnly is a file: it shouldn't exist, diskShare.open will fail and pathOnly will be returned unchanged.
		try (DiskEntry resolvedEntry = diskShare.open(pathOnly, EnumSet.of(AccessMask.GENERIC_READ), null,
				SMB2ShareAccess.ALL, SMB2CreateDisposition.FILE_OPEN, null)) {
			return resolvedEntry.getFileName();
		}
		catch (Exception e) {
			// if pathOnly is a file only (no path) and doesn't exist, this error is ok.
			LogUtils.warn(FileContainerRemote.class, String.format("Could not resolve path [%s] from [%s].", pathOnly, diskShare.getSmbPath()), e);
		}
		return pathOnly;
	}


	/**
	 * Return the path without the file (last), server and share segments.
	 * <p>
	 * //server/share/dirpath/file
	 *
	 * @return dirpath
	 */
	private String getPathOnly() {
		String name = getFilePathOnly();
		if (Strings.isNotBlank(name)) {
			int index = name.lastIndexOf(SMB_PATH_SEPARATOR);
			if (index > 1) {
				name = name.substring(0, index);
			}
		}
		return name;
	}


	/**
	 * Performs all smbj related prerequisites necessary to establish a connection to a share, performs the provided operation and closes the resources upon completion.
	 */
	private <R> R performOperation(Function<DiskShare, R> operation) {
		// client maintains a cache of connections, do not close.
		SMBClient client = this.fileContainerContext.getSmbClient();
		Connection connection;
		try {
			connection = client.connect(this.path.getHostname());
		}
		catch (IOException e) {
			throw new RuntimeException("Could not connect to remote server ", e);
		}
		// session and share should be closed.
		try (
				Session session = connection.authenticate(this.fileContainerContext.getAuthenticationContext());
				DiskShare share = (DiskShare) session.connectShare(this.path.getShareName())
		) {
			return operation.apply(share);
		}
		catch (IOException e) {
			throw new RuntimeException("Could not connect to share to remote server ", e);
		}
	}


	/**
	 * The last portion of the path without server and share portions, used by most smbj operations.
	 * Do not call "this.path.getPath()" directly, it is misleading since its not the absolute path.
	 * <p>
	 * //server/share/dirpath/file
	 *
	 * @return dirpath/file
	 */
	private String getFilePathOnly() {
		return this.path.getPath();
	}

//////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////

	/**
	 * Enable closing all smbj related resources necessary to connect to a share.
	 */
	private static class DiskShareWrapper {

		private final Session session;
		private final DiskShare diskShare;
		private boolean closed = false;


		DiskShareWrapper(FileContainerRemote fileContainerRemote) throws IOException {
			SMBClient client = fileContainerRemote.fileContainerContext.getSmbClient();
			Connection connection = client.connect(fileContainerRemote.path.getHostname());

			this.session = connection.authenticate(fileContainerRemote.fileContainerContext.getAuthenticationContext());
			this.diskShare = (DiskShare) this.session.connectShare(fileContainerRemote.path.getShareName());
		}


		void close() throws IOException {
			if (!this.closed) {
				if (this.diskShare != null) {
					this.diskShare.close();
				}
				if (this.session != null) {
					this.session.close();
				}
				this.closed = true;
			}
		}


		DiskShare getDiskShare() {
			return this.diskShare;
		}
	}

	/**
	 * Wraps the smbj input stream so cleanup can be done on all connection-related resources.
	 */
	public static class RemoteInputStreamWrapper extends InputStream {

		private final DiskShareWrapper diskShareWrapper;
		private final DiskEntry diskEntry;
		private final InputStream inputStream;
		private boolean closed = false;


		RemoteInputStreamWrapper(FileContainerRemote fileContainerRemote) throws IOException {
			this.diskShareWrapper = new DiskShareWrapper(fileContainerRemote);
			this.diskEntry = this.diskShareWrapper.getDiskShare().open(fileContainerRemote.getFilePathOnly(), EnumSet.of(AccessMask.GENERIC_READ), null,
					EnumSet.of(SMB2ShareAccess.FILE_SHARE_READ), SMB2CreateDisposition.FILE_OPEN, null);
			if (this.diskEntry instanceof File) {
				this.inputStream = ((File) this.diskEntry).getInputStream();
			}
			else {
				throw new RuntimeException("Could not create input stream for " + fileContainerRemote.path);
			}
		}


		@Override
		public int read() throws IOException {
			return this.inputStream.read();
		}


		@Override
		public int read(byte[] b, int off, int len) throws IOException {
			return this.inputStream.read(b, off, len);
		}


		@Override
		public void close() throws IOException {
			if (!this.closed) {
				super.close();
				if (this.inputStream != null) {
					this.inputStream.close();
				}
				ObjectUtils.doIfPresent(this.diskEntry, DiskEntry::close);
				this.diskShareWrapper.close();
				this.closed = true;
			}
		}
	}

	/**
	 * Wraps the smbj output stream so cleanup can be done on all connection-related resources.
	 */
	public static class RemoteOutStreamWrapper extends OutputStream {

		private final DiskShareWrapper diskShareWrapper;
		private final DiskEntry diskEntry;
		private final OutputStream outputStream;
		private boolean closed = false;


		RemoteOutStreamWrapper(FileContainerRemote fileContainerRemote) throws IOException {
			this.diskShareWrapper = new DiskShareWrapper(fileContainerRemote);
			// Overwrite the file if it already exists; otherwise, create the file
			this.diskEntry = this.diskShareWrapper.getDiskShare().open(fileContainerRemote.getFilePathOnly(),
					EnumSet.of(AccessMask.GENERIC_WRITE), null, SMB2ShareAccess.ALL, SMB2CreateDisposition.FILE_OVERWRITE_IF, null);
			if (this.diskEntry instanceof File) {
				this.outputStream = ((File) this.diskEntry).getOutputStream();
			}
			else {
				throw new RuntimeException("Could not create output stream for " + fileContainerRemote.path);
			}
		}


		@Override
		public void write(byte[] b) throws IOException {
			this.outputStream.write(b);
		}


		@Override
		public void write(byte[] b, int off, int len) throws IOException {
			this.outputStream.write(b, off, len);
		}


		@Override
		public void write(int b) throws IOException {
			this.outputStream.write(b);
		}


		@Override
		public void close() throws IOException {
			if (!this.closed) {
				super.close();
				if (this.outputStream != null) {
					this.outputStream.flush();
					this.outputStream.close();
				}
				ObjectUtils.doIfPresent(this.diskEntry, DiskEntry::close);
				this.diskShareWrapper.close();
				this.closed = true;
			}
		}
	}
}
