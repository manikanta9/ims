package com.clifton.core.dataaccess.function;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.dialect.function.SQLFunction;

import java.util.List;


/**
 * <code>SQLIfFunction</code> implements the base functionality needed for an if else statement. Intended to extended by a dialect specific implementation.
 * Result should be equivalent to:
 * f(c,x,y) =  if c then x else y
 *
 * @author dillonm
 */
public interface SQLIfElseFunction extends SQLFunction {

	public static final String FUNCTION_NAME = "ppa_ifelse";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates an if else SQL statement.
	 * The resulting SQL statement is dependent on the dialect configured by the {@link com.clifton.core.dataaccess.dialect.SessionFactoryBuilderConfigurer} implementation.
	 */
	public static String render(Criteria criteria, CriteriaQuery criteriaQuery, String condition, String trueResult, String falseResult) {
		List<String> arguments = SQLFunctionUtils.buildFunctionParameterList(criteria, criteriaQuery, condition, trueResult, falseResult);
		SQLFunction sqlFunction = SQLFunctionUtils.getFunction(criteriaQuery, FUNCTION_NAME);
		return sqlFunction.render(null, arguments, criteriaQuery.getFactory());
	}
}
