package com.clifton.core.dataaccess.dao.event.cache.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicReference;
import java.util.function.Supplier;


/**
 * <code>SelfRegisteringSimpleMultiKeyDaoCache</code> is similar to the self registering key based DAO caches
 * except it allows for custom bean lookup via a {@link Supplier} to support caching {@link IdentityObject}s
 * with multiple keys. The DAO is also looked up by {@link DaoLocator} for a DTO class instead of taking in
 * a {@link ReadOnlyDAO} reference to the get method.
 * <p>
 * Upon DAO events, cache entries pertaining to an IdentityObject will be removed and not updated.
 *
 * @author NickK
 */
public abstract class SelfRegisteringSimpleMultiKeyDaoCache<T extends IdentityObject> extends SelfRegisteringSimpleDaoCache<T, String, Serializable> {

	private final Map<Serializable, Set<String>> valueToKeyListMap = new ConcurrentHashMap<>();

	private final AtomicReference<ReadOnlyDAO<T>> dao = new AtomicReference<>();

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////                      Cache Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	protected abstract Class<T> getDtoClass();


	protected ReadOnlyDAO<T> getDao() {
		ReadOnlyDAO<T> daoReference = this.dao.get();
		if (daoReference == null) {
			daoReference = this.dao.updateAndGet(currentValue -> (currentValue != null) ? currentValue : getDaoLocator().locate(getDtoClass()));
			ValidationUtils.assertNotNull(daoReference, "Unable to find DAO for DTO class: " + getDtoClass());
		}
		return daoReference;
	}


	/**
	 * If no bean retriever is passed, then it is assumed the cache has been pre-built so there is not need to populate cache.
	 * Simply calls the other getValue method and will either return the bean or null;
	 */
	public T getValue(Object[] keyProperties) {
		return getValue(keyProperties, null);
	}


	/**
	 * This cache caches the identity of the cached objects. If an identity is cached for the provided
	 * keys, the object is looked up using {@link ReadOnlyDAO#findByPrimaryKey(Serializable)}. If an identity is
	 * not cached for they keys, the supplier will be used to look up the object, cache its identity, and return it.
	 * <p>
	 * If no bean retriever is passed, then it is assumed the cache has been pre-built so there is not need to populate cache.
	 */
	public T getValue(Object[] keyProperties, Supplier<T> beanRetrieverIfAbsent) {
		T bean = null;
		String key = getBeanKeyForProperties(keyProperties);
		Serializable id = getCacheHandler().get(getCacheName(), key);
		if (id != null) {
			bean = getDao().findByPrimaryKey(id);
			if (bean != null) {
				return bean;
			}
		}
		if (beanRetrieverIfAbsent != null) {
			bean = beanRetrieverIfAbsent.get();
			populateCache(key, bean);
		}
		return bean;
	}


	protected void populateCache(String key, T bean) {
		if (bean != null) {
			getCacheHandler().put(getCacheName(), key, bean.getIdentity());
			CollectionUtils.getValue(this.valueToKeyListMap, bean.getIdentity(), HashSet::new).add(key);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			// remove cache entries and keys; cannot update cache because keys may be invalid
			Set<String> beanKeySet = this.valueToKeyListMap.get(bean.getIdentity());
			if (beanKeySet != null) {
				Iterator<String> beanCacheKeyIterator = beanKeySet.iterator();
				while (beanCacheKeyIterator.hasNext()) {
					getCacheHandler().remove(getCacheName(), beanCacheKeyIterator.next());
					beanCacheKeyIterator.remove();
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////                 Getters and Setters                   /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
