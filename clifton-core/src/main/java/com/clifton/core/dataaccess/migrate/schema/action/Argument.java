package com.clifton.core.dataaccess.migrate.schema.action;


/**
 * The <code>Argument</code> represents an argument passed to the {@link Action} service method
 *
 * @author manderson
 */
public class Argument {

	private String className;
	/**
	 * Indicates that the value is a SQL select that needs to be evaluated prior to passing
	 * it to the service method
	 */
	private boolean select;

	private String value;

	private Object objectValue;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Argument() {
		//default constructor
	}


	public Argument(Object object) {
		this.objectValue = object;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getClassName() {
		return this.className;
	}


	public void setClassName(String className) {
		this.className = className;
	}


	public boolean isSelect() {
		return this.select;
	}


	public void setSelect(boolean select) {
		this.select = select;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public Object getObjectValue() {
		return this.objectValue;
	}


	public void setObjectValue(Object objectValue) {
		this.objectValue = objectValue;
	}
}
