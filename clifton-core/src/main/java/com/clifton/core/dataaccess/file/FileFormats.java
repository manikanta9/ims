package com.clifton.core.dataaccess.file;


public enum FileFormats {
	PDF("pdf", new DataTableToPdfFileConverter(), null), //
	EXCEL("xls", new DataTableToExcelFileConverter(), new ExcelFileToDataTableConverter()), //
	EXCEL_OPEN_XML("xlsx", new DataTableToExcelFileConverter("xlsx"), new ExcelFileToDataTableConverter()), //
	//Assumes csv formatted text file
	TXT("txt", new DataTableToCsvFileConverter(), new CSVFileToDataTableConverter()),
	CSV("csv", new DataTableToCsvFileConverter(), new CSVFileToDataTableConverter()),
	CSV_MIN("csv_min", new DataTableToCsvFileConverter(false), new CSVFileToDataTableConverter()),
	CSV_PIPE("csv_pipe", new DataTableToCsvFileConverter(false, false, '|'), new CSVFileToDataTableConverter('|', true)),
	WORD("word", null, null),
	XML("xml", null, null);

	private final String formatString;
	private final DataTableToFileConverter dataTableConverter;
	private final FileToDataTableConverter fileConverter;


	FileFormats(String formatString, DataTableToFileConverter dataTableConverter, FileToDataTableConverter fileConverter) {
		this.formatString = formatString;
		this.dataTableConverter = dataTableConverter;
		this.fileConverter = fileConverter;
	}


	public String getFormatString() {
		return this.formatString;
	}


	public String getExtension() {
		return ".".concat(this.formatString);
	}


	public static FileFormats getEnum(String value) {
		for (FileFormats v : values()) {
			if (v.getFormatString().equalsIgnoreCase(value)) {
				return v;
			}
		}
		throw new IllegalArgumentException();
	}


	public DataTableToFileConverter getDataTableConverter() {
		return this.dataTableConverter;
	}


	public FileToDataTableConverter getFileConverter() {
		return this.fileConverter;
	}
}
