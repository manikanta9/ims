package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;


/**
 * The <code>StringRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link String}.
 *
 * @author vgomelsky
 */
public class StringRestrictionDefinition extends AbstractRestrictionDefinition {

	private int minLength;
	private int maxLength = Integer.MAX_VALUE;


	public StringRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public StringRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, 0, Integer.MAX_VALUE);
	}


	public StringRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                   ComparisonConditions[] comparisonConditions) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, null);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	public StringRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                   ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomType) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, searchFieldCustomType);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	public StringRestrictionDefinition(String fieldName, String searchFieldName, int minLength, int maxLength) {
		super(fieldName, searchFieldName, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.LIKE, ComparisonConditions.BEGINS_WITH, ComparisonConditions.ENDS_WITH,
				ComparisonConditions.NOT_LIKE, ComparisonConditions.NOT_BEGINS_WITH, ComparisonConditions.NOT_ENDS_WITH});
		AssertUtils.assertTrue(minLength >= 0, "Minimum restriction length cannot be less than 0 characters.");
		AssertUtils.assertTrue(maxLength >= 1, "Minimum restriction length cannot be less than 1 character.");
		this.minLength = minLength;
		this.maxLength = maxLength;
	}


	@Override
	public void validate(SearchRestriction restriction) {
		super.validate(restriction);
		int length = StringUtils.length((String) restriction.getValue());
		if (length < this.minLength) {
			throw new FieldValidationException("Restriction must be at lest " + this.minLength + " characters long.", restriction.getField());
		}
		if (length > this.maxLength) {
			throw new FieldValidationException("Restriction cannot be longer than " + this.minLength + " characters.", restriction.getField());
		}
	}
}
