package com.clifton.core.dataaccess.migrate.schema.action;


import java.util.List;
import java.util.Map;


/**
 * The <code>Action</code> class represents a single call to a Service Method
 *
 * @author manderson
 */
public class Action {

	private String bean;
	private String name;
	private String method;

	private List<Argument> argumentList;

	private Map<String, Action> actionMap;

	private boolean validationDisabled;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getBean() {
		return this.bean;
	}


	public void setBean(String bean) {
		this.bean = bean;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getMethod() {
		return this.method;
	}


	public void setMethod(String method) {
		this.method = method;
	}


	public boolean isValidationDisabled() {
		return this.validationDisabled;
	}


	public void setValidationDisabled(boolean validationDisabled) {
		this.validationDisabled = validationDisabled;
	}


	public List<Argument> getArgumentList() {
		return this.argumentList;
	}


	public void setArgumentList(List<Argument> argumentList) {
		this.argumentList = argumentList;
	}


	public Map<String, Action> getActionMap() {
		return this.actionMap;
	}


	public void setActionMap(Map<String, Action> actionMap) {
		this.actionMap = actionMap;
	}
}
