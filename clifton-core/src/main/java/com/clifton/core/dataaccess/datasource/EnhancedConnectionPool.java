package com.clifton.core.dataaccess.datasource;

import org.apache.tomcat.jdbc.pool.ConnectionPool;
import org.apache.tomcat.jdbc.pool.PoolConfiguration;
import org.apache.tomcat.jdbc.pool.PooledConnection;

import java.sql.SQLException;
import java.util.concurrent.atomic.AtomicLong;


/**
 * This class is an extension of the JDBC ConnectionPool that has been designed to capture a new statistic, maxActiveCount,
 * which represents the maximum number of active connections created during the life of the pool or since the last time the stats
 * were reset.
 *
 * @author davidi
 */
public class EnhancedConnectionPool extends ConnectionPool {

	private final AtomicLong maxActiveCount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public EnhancedConnectionPool(PoolConfiguration prop) throws SQLException {
		super(prop);
		this.maxActiveCount = new AtomicLong(0);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected PooledConnection createConnection(long now, PooledConnection notUsed, String username, String password) throws SQLException {
		PooledConnection pooledConnection = super.createConnection(now, notUsed, username, password);
		this.maxActiveCount.updateAndGet(
				currentValue -> {
					long size = getSize();
					return currentValue > size ? currentValue : size;
				});
		return pooledConnection;
	}


	@Override
	public void resetStats() {
		super.resetStats();
		this.maxActiveCount.set(0);
	}


	/**
	 * Returns the maximum number of active connections created in this pool.
	 */
	public long getMaxActiveCount() {
		return this.maxActiveCount.get();
	}
}
