package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.Validator;


/**
 * The <code>SelfRegisteringDaoValidator</code> class is a helper class that should be extended by DAO validators.
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events.
 *
 * @param <T>
 * @author vgomelsky
 */
public abstract class SelfRegisteringDaoValidator<T extends IdentityObject> extends SelfRegisteringDaoObserver<T> implements Validator<T, DaoEventTypes> {

	private DaoLocator daoLocator;


	/**
	 * In rare cases when DAO is needed, use this method instead.
	 * You still have to override the validate without DAO :(
	 */
	public void validate(T bean, DaoEventTypes config, @SuppressWarnings("unused") ReadOnlyDAO<T> dao) throws ValidationException {
		validate(bean, config);
	}


	@Override
	protected final void beforeTransactionMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		// validate before the actual method call and before transaction is started (is possible)
		validate(bean, event, dao);
	}


	/**
	 * Returns the original bean instance of the specified bean (the one that hasn't been updated yet in the database).
	 * Assumes that passed bean instance was modified but hasn't been persisted yet.
	 */
	@SuppressWarnings("unchecked")
	protected T getOriginalBean(T bean) {
		return getOriginalBean(getDaoLocator().locate((Class<T>) bean.getClass()), bean);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
