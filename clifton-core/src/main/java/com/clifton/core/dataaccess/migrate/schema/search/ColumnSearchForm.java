package com.clifton.core.dataaccess.migrate.schema.search;

import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author manderson
 */
@SearchForm(hasOrmDtoClass = false)
public class ColumnSearchForm extends BaseAuditableEntitySearchForm {

	private String searchPattern;

	/**
	 * Note: Performs Equals search
	 */
	private String tableName;
	private String tableNameContains;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String beanPropertyName;

	private String dataType;

	private Boolean required;

	private Boolean requiredForValidation;

	private String defaultValue;

	private Integer sortOrder;

	private String sortDirection;

	private String uploadValueConverterName;

	private String uploadLookupServiceBeanName;

	private String uploadLookupServiceMethodName;

	private Boolean parentField;

	private Boolean indexed;

	private Boolean uniqueIndex;

	private String fkTable;

	private String fkField;

	private String lazy;

	private Boolean naturalKey;

	private Boolean ignoreUpload;

	private Boolean delayed;

	private Boolean forceSimpleProperty;

	private Boolean excludedFromUpdates;

	private Boolean excludedFromInserts;

	private String typeDefinitionName;

	private String typeDefinitionClassName;

	private String typeDefinitionConverterClass;

	private String auditType;

	private String calculation;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTableName() {
		return this.tableName;
	}


	public void setTableName(String tableName) {
		this.tableName = tableName;
	}


	public String getTableNameContains() {
		return this.tableNameContains;
	}


	public void setTableNameContains(String tableNameContains) {
		this.tableNameContains = tableNameContains;
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}


	public String getDataType() {
		return this.dataType;
	}


	public void setDataType(String dataType) {
		this.dataType = dataType;
	}


	public Boolean getRequired() {
		return this.required;
	}


	public void setRequired(Boolean required) {
		this.required = required;
	}


	public Boolean getRequiredForValidation() {
		return this.requiredForValidation;
	}


	public void setRequiredForValidation(Boolean requiredForValidation) {
		this.requiredForValidation = requiredForValidation;
	}


	public String getDefaultValue() {
		return this.defaultValue;
	}


	public void setDefaultValue(String defaultValue) {
		this.defaultValue = defaultValue;
	}


	public Integer getSortOrder() {
		return this.sortOrder;
	}


	public void setSortOrder(Integer sortOrder) {
		this.sortOrder = sortOrder;
	}


	public String getSortDirection() {
		return this.sortDirection;
	}


	public void setSortDirection(String sortDirection) {
		this.sortDirection = sortDirection;
	}


	public String getUploadValueConverterName() {
		return this.uploadValueConverterName;
	}


	public void setUploadValueConverterName(String uploadValueConverterName) {
		this.uploadValueConverterName = uploadValueConverterName;
	}


	public String getUploadLookupServiceBeanName() {
		return this.uploadLookupServiceBeanName;
	}


	public void setUploadLookupServiceBeanName(String uploadLookupServiceBeanName) {
		this.uploadLookupServiceBeanName = uploadLookupServiceBeanName;
	}


	public String getUploadLookupServiceMethodName() {
		return this.uploadLookupServiceMethodName;
	}


	public void setUploadLookupServiceMethodName(String uploadLookupServiceMethodName) {
		this.uploadLookupServiceMethodName = uploadLookupServiceMethodName;
	}


	public Boolean getParentField() {
		return this.parentField;
	}


	public void setParentField(Boolean parentField) {
		this.parentField = parentField;
	}


	public Boolean getIndexed() {
		return this.indexed;
	}


	public void setIndexed(Boolean indexed) {
		this.indexed = indexed;
	}


	public Boolean getUniqueIndex() {
		return this.uniqueIndex;
	}


	public void setUniqueIndex(Boolean uniqueIndex) {
		this.uniqueIndex = uniqueIndex;
	}


	public String getFkTable() {
		return this.fkTable;
	}


	public void setFkTable(String fkTable) {
		this.fkTable = fkTable;
	}


	public String getFkField() {
		return this.fkField;
	}


	public void setFkField(String fkField) {
		this.fkField = fkField;
	}


	public String getLazy() {
		return this.lazy;
	}


	public void setLazy(String lazy) {
		this.lazy = lazy;
	}


	public Boolean getNaturalKey() {
		return this.naturalKey;
	}


	public void setNaturalKey(Boolean naturalKey) {
		this.naturalKey = naturalKey;
	}


	public Boolean getIgnoreUpload() {
		return this.ignoreUpload;
	}


	public void setIgnoreUpload(Boolean ignoreUpload) {
		this.ignoreUpload = ignoreUpload;
	}


	public Boolean getDelayed() {
		return this.delayed;
	}


	public void setDelayed(Boolean delayed) {
		this.delayed = delayed;
	}


	public Boolean getForceSimpleProperty() {
		return this.forceSimpleProperty;
	}


	public void setForceSimpleProperty(Boolean forceSimpleProperty) {
		this.forceSimpleProperty = forceSimpleProperty;
	}


	public Boolean getExcludedFromUpdates() {
		return this.excludedFromUpdates;
	}


	public void setExcludedFromUpdates(Boolean excludedFromUpdates) {
		this.excludedFromUpdates = excludedFromUpdates;
	}


	public Boolean getExcludedFromInserts() {
		return this.excludedFromInserts;
	}


	public void setExcludedFromInserts(Boolean excludedFromInserts) {
		this.excludedFromInserts = excludedFromInserts;
	}


	public String getTypeDefinitionName() {
		return this.typeDefinitionName;
	}


	public void setTypeDefinitionName(String typeDefinitionName) {
		this.typeDefinitionName = typeDefinitionName;
	}


	public String getTypeDefinitionClassName() {
		return this.typeDefinitionClassName;
	}


	public void setTypeDefinitionClassName(String typeDefinitionClassName) {
		this.typeDefinitionClassName = typeDefinitionClassName;
	}


	public String getTypeDefinitionConverterClass() {
		return this.typeDefinitionConverterClass;
	}


	public void setTypeDefinitionConverterClass(String typeDefinitionConverterClass) {
		this.typeDefinitionConverterClass = typeDefinitionConverterClass;
	}


	public String getAuditType() {
		return this.auditType;
	}


	public void setAuditType(String auditType) {
		this.auditType = auditType;
	}


	public String getCalculation() {
		return this.calculation;
	}


	public void setCalculation(String calculation) {
		this.calculation = calculation;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
