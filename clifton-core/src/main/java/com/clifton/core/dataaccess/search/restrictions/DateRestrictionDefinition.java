package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;

import java.util.Date;


/**
 * The <code>DateRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link Date}.
 *
 * @author vgomelsky
 */
public class DateRestrictionDefinition extends AbstractRestrictionDefinition {

	public static final int START_OF_THIS_DAY = 1;
	public static final int START_OF_NEXT_DAY = 2;
	public static final int END_OF_PREVIOUS_DAY = 3;

	/**
	 * Optionally can change the specified date using the specified adjustment type.
	 */
	private int dayAdjustmentType;

	private boolean dateFieldIncludesTime;


	public DateRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public DateRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, false);
	}


	public DateRestrictionDefinition(String fieldName, String searchFieldName, boolean required) {
		this(fieldName, searchFieldName, null, false, null, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.GREATER_THAN,
				ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.EQUALS_OR_IS_NULL,
				ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_IS_NULL,
				ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, ComparisonConditions.IN});
	}


	public DateRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                 ComparisonConditions[] comparisonConditions) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	public DateRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                 ComparisonConditions[] comparisonConditions, boolean dateFieldIncludesTime) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions);
		setDateFieldIncludesTime(dateFieldIncludesTime);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	public DateRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                 ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomTypes, boolean dateFieldIncludesTime) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, searchFieldCustomTypes);
		setDateFieldIncludesTime(dateFieldIncludesTime);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	@Override
	public Object getValue(Object rawValue) {
		if (rawValue == null) {
			return rawValue;
		}
		Date result = (rawValue instanceof Date) ? (Date) rawValue : DateUtils.toDate((String) rawValue);
		if (this.dayAdjustmentType == START_OF_THIS_DAY) {
			result = DateUtils.clearTime(result);
		}
		else if (this.dayAdjustmentType == START_OF_NEXT_DAY) {
			result = DateUtils.addDays(DateUtils.clearTime(result), 1);
		}
		else if (this.dayAdjustmentType == END_OF_PREVIOUS_DAY) {
			result = DateUtils.addDays(DateUtils.getEndOfDay(result), -1);
		}
		return result;
	}


	@Override
	public void validate(SearchRestriction restriction) {
		super.validate(restriction);
		Object value = restriction.getValue();
		if (restriction.getComparison() != ComparisonConditions.IS_NULL && !(value instanceof Date) && !DateUtils.isValidDate((String) value)) {
			throw new FieldValidationException("'" + restriction.getValue() + "' is not a valid date.", restriction.getField());
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getDayAdjustmentType() {
		return this.dayAdjustmentType;
	}


	public void setDayAdjustmentType(int dayAdjustmentType) {
		this.dayAdjustmentType = dayAdjustmentType;
	}


	public boolean isDateFieldIncludesTime() {
		return this.dateFieldIncludesTime;
	}


	public void setDateFieldIncludesTime(boolean dateFieldIncludesTime) {
		this.dateFieldIncludesTime = dateFieldIncludesTime;
	}
}
