package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ImmutableObjectWrapper;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.io.Serializable;


/**
 * The <code>SelfRegisteringDaoCache</code> class is a helper class that contains common methods utilized by
 * {@link SelfRegisteringSingleKeyDaoCache} and {@link SelfRegisteringCompositeKeyDaoCache} AND {@link SelfRegisteringThreeKeyDaoCache}
 * <p>
 * NOTE: UNLESS SOMETHING CUSTOM, SHOULD NOT USE THIS CACHE DIRECTLY, INSTEAD USE THE SINGLE OR COMPOSITE KEY VERSIONS
 * <p>
 * Observes all MODIFY events:
 * - Inserts are observed to immediately cache the bean after the save. (Unless overridden to clear on all changes in which case the cache is always cleared for the key)
 * - Updates - if the key changes will clear the cache for the original key and cache the bean for the updated key (Unless overridden to clear on all changes then will also clear for the new key)
 * - Deletes are observed to clear the cache (for affected key)
 *
 * @author Mary Anderson
 */
public abstract class SelfRegisteringDaoCache<T extends IdentityObject> extends SelfRegisteringSimpleDaoCache<T, String, ObjectWrapper<Serializable>> {

	private static final ImmutableObjectWrapper<Serializable> NULL_ENTITY = new ImmutableObjectWrapper<>(null);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected abstract String[] getBeanKeyProperties();


	protected abstract String getBeanKey(T bean);


	/**
	 * Optionally log empty results (INFO level)
	 * i.e. to view consistent retrievals of a missing value
	 */
	protected boolean isLogEmptyResults() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected T getBeanImpl(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		String key = getBeanKeyForProperties(keyProperties);
		ObjectWrapper<Serializable> idWrapper = getCacheHandler().get(getCacheName(), key);
		if (idWrapper != null) {
			if (idWrapper.isPresent()) {
				T bean = dao.findByPrimaryKey(idWrapper.getObject());
				// If bean is null, then transaction was rolled back, look it up again and reset the cache
				if (bean != null) {
					return bean;
				}
			}
			else {
				if (throwExceptionIfNotFound) {
					throw new ValidationException("Unable to find " + StringUtils.splitWords(dao.getConfiguration().getTableName()) + " with properties [" + ArrayUtils.toString(getBeanKeyProperties())
							+ "] and value(s) [" + ArrayUtils.toString(keyProperties) + "]");
				}
				return null;
			}
		}
		T bean = lookupBean(dao, throwExceptionIfNotFound, keyProperties);
		if (bean != null) {
			setBean(bean);
		}
		else {
			setEmptyBean(getBeanKeyForProperties(keyProperties));
		}
		return bean;
	}


	/**
	 * When bean isn't set in the cache, use this method to determine how to look it up in the database and immediately set in the cache
	 */
	protected T lookupBean(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		T bean = dao.findOneByFields(getBeanKeyProperties(), keyProperties);
		if (bean == null && throwExceptionIfNotFound) {
			throw new ValidationException("Unable to find " + StringUtils.splitWords(dao.getConfiguration().getTableName()) + " with properties [" + ArrayUtils.toString(getBeanKeyProperties())
					+ "] and value(s) [" + ArrayUtils.toString(keyProperties) + "]");
		}
		return bean;
	}


	public void setBean(T bean) {
		if (bean != null) {
			getCacheHandler().put(getCacheName(), getBeanKey(bean), new ObjectWrapper<>(bean.getIdentity()));
		}
	}


	public void setEmptyBean(String beanKey) {
		if (isLogEmptyResults()) {
			LogUtils.info(LogCommand.ofMessage(getClass(), "No value retrieved for bean key " + beanKey + " in cache " + getCacheName()));
		}
		getCacheHandler().put(getCacheName(), beanKey, NULL_ENTITY);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If false (default) - Inserts will set the bean in the cache right after update, otherwise will not set the cache and will clear the cache for the key
	 * Updates that affect the generated key will clear the cache for the original key and if false, set the bean in the cache for the new key, else clear the cache for the new key as well
	 */
	protected boolean isClearOnAllKeyChanges() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		if (event.isUpdate()) { // Only needed for updates in case the key has changed - want to clear the original key so we don't leave bad objects in the cache
			// Call it so we have the original
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			String key = getBeanKey(bean);
			// Immediately add to the cache after inserts
			if (event.isInsert() && !isClearOnAllKeyChanges()) {
				setBean(bean);
			}
			else {
				if (event.isUpdate()) {
					T originalBean = getOriginalBean(dao, bean);
					String originalKey = getBeanKey(originalBean);
					// For updates - because we only cache the id of the object, we only need to clear the cache if the change
					// results in a different key.  Otherwise, not necessary to clear the cache
					if (!key.equals(originalKey)) {
						getCacheHandler().remove(getCacheName(), originalKey);
						if (isClearOnAllKeyChanges()) {
							getCacheHandler().remove(getCacheName(), key);
						}
						else {
							setBean(bean);
						}
					}
				}
				else {
					getCacheHandler().remove(getCacheName(), key);
				}
			}
		}
	}
}
