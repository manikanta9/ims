package com.clifton.core.dataaccess.dialect;


import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;


/**
 * Used to bootstrap the {@link org.springframework.orm.hibernate5.LocalSessionFactoryBuilder} for customizing SessionFactory objects.
 *
 * @author dillonm
 */
public interface SessionFactoryBuilderConfigurer {

	/**
	 * Used to configure the {@link org.springframework.orm.hibernate5.LocalSessionFactoryBuilder}
	 */
	public void configure(LocalSessionFactoryBuilder sfb);


	/**
	 * returns true if the dialect is supported by the SessionFactoryBuilderConfigurer or if the configurer is dialect independent.
	 */
	public boolean supportsDialect(String dialect);
}
