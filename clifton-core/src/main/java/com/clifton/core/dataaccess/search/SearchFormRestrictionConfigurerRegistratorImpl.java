package com.clifton.core.dataaccess.search;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.logging.LogUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The <code>SearchFormRestrictionConfigurerRegistratorImpl</code> finds all spring managed beans that implement {@link SearchFormRestrictionConfigurer}
 * and registers the search restriction
 *
 * @author manderson
 */
@Component
public class SearchFormRestrictionConfigurerRegistratorImpl implements CurrentContextApplicationListener<ApplicationContextEvent> {

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ApplicationContextEvent event) {
		ApplicationContext eventContext = event.getApplicationContext();

		if (event instanceof ContextRefreshedEvent) {
			// Lookup beans implementing SearchFormRestrictionConfigurer
			Map<String, SearchFormRestrictionConfigurer> beanMap = eventContext.getBeansOfType(SearchFormRestrictionConfigurer.class);
			if (beanMap != null) {
				for (Map.Entry<String, SearchFormRestrictionConfigurer> stringSearchFormRestrictionConfigurerEntry : beanMap.entrySet()) {
					SearchUtils.registerSearchFormRestrictionConfigurer(stringSearchFormRestrictionConfigurerEntry.getValue());
					LogUtils.info(getClass(), "Registered Search Form Restriction Configurer: " + stringSearchFormRestrictionConfigurerEntry.getKey());
				}
			}
		}

		else if (event instanceof ContextClosedEvent) {
			SearchUtils.clearSearchFormRestrictionConfigurers();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
