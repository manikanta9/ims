package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.validation.Validator;


/**
 * The <code>BaseDaoValidator</code> class is a helper class that should be extended by all DAO validators.
 *
 * @param <T>
 * @author vgomelsky
 */
public abstract class BaseDaoValidator<T extends IdentityObject> extends BaseDaoEventObserver<T> implements Validator<T, ReadOnlyDAO<T>> {

	@Override
	protected final void beforeMethodCallImpl(ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, T bean) {
		// validate before the actual method call
		validate(bean, dao);
	}


	@Override
	@SuppressWarnings("unused")
	protected final void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		// do nothing: all validation must be done before the actual method call
	}
}
