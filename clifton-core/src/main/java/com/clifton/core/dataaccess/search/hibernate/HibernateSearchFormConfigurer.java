package com.clifton.core.dataaccess.search.hibernate;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.OrderByField;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchFormCustomConfigurer;
import com.clifton.core.dataaccess.search.SearchFormRestrictionConfigurer;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.dataaccess.search.SearchUtils;
import com.clifton.core.dataaccess.search.form.BaseSortableSearchForm;
import com.clifton.core.dataaccess.search.form.DateRangeAwareSearchForm;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.dataaccess.search.hibernate.expression.AllAreNullExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.CoalesceExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.ConcatenateStringAndNumberExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.DateDiffExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.FormulaExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.InExpressionForNumbers;
import com.clifton.core.dataaccess.search.hibernate.expression.JsonCompressedTextExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.MathExpression;
import com.clifton.core.dataaccess.search.hibernate.expression.NotInExpressionForNumbers;
import com.clifton.core.dataaccess.search.restrictions.ArrayRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.BigDecimalRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.BooleanRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.DateRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.DoubleRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.EnumRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ForeignKeyRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.IntegerRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.LongRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.ShortRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.StringRestrictionDefinition;
import com.clifton.core.dataaccess.search.restrictions.TimeRestrictionDefinition;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.sql.JoinType;
import org.springframework.core.annotation.AnnotationUtils;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;


/**
 * The <code>HibernateSearchFormConfigurer</code> class is a SearchConfigurer that configures Hibernate Criteria
 * to filter using restrictions of the specified SearchForm.
 *
 * @author vgomelsky
 */
public class HibernateSearchFormConfigurer extends SortableSearchConfigurer<BaseEntitySearchForm> {

	private final Map<String, SearchRestrictionDefinition> restrictionDefinitionMap = new ConcurrentHashMap<>();

	/**
	 * The map of joined path's to resulting Criteria objects.
	 */
	private Map<String, Criteria> processedPathToCriteria;

	/**
	 * If sorting is not specified via {@link BaseSortableSearchForm#setOrderBy} method, default sorting by multiple columns that requires joins will be used.
	 * Set this property to true in order to avoid default sorting (improves performance for cases when sorting is not needed: may even skip extra joins).
	 */
	private final boolean skipDefaultSorting;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public HibernateSearchFormConfigurer(BaseEntitySearchForm searchForm) {
		this(searchForm, false, searchForm.isSkipDefaultSorting());
	}


	public HibernateSearchFormConfigurer(BaseEntitySearchForm searchForm, boolean doNotConfigure) {
		this(searchForm, doNotConfigure, searchForm.isSkipDefaultSorting());
	}


	/**
	 * @param skipDefaultSorting When true and Order By columns are not defined, will skip default sorting configuration and will not produce ORDER BY clause (thus improving performance).
	 */
	public HibernateSearchFormConfigurer(BaseEntitySearchForm searchForm, boolean doNotConfigure, boolean skipDefaultSorting) {
		super(searchForm);
		if (!doNotConfigure) {
			configureSearchForm();
		}
		this.skipDefaultSorting = skipDefaultSorting;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append('{');
		result.append(isReadUncommittedRequested());
		result.append(", ");
		result.append(getSortableSearchForm().getRestrictionList());
		result.append('}');
		return result.toString();
	}


	@SuppressWarnings({"unchecked"})
	protected <E extends Enum<E>> void configureSearchForm() {
		BaseEntitySearchForm searchForm = getSortableSearchForm();
		// populate restriction definition map for each search field (form fields marked by @SearchField)
		// also checks inherited fields
		Field[] fields = ClassUtils.getClassFields(searchForm.getClass(), true, true);
		for (Field field : fields) {
			SearchField searchField = AnnotationUtils.findAnnotation(field, SearchField.class);

			if (searchField != null) {
				SearchFieldCustomTypes multipleFieldTypes = searchField.searchFieldCustomType();
				SearchRestrictionDefinition definition;
				Class<?> type = field.getType();
				String searchFieldName = SearchField.SAME_AS_MARKED_FIELD.equals(searchField.searchField()) ? field.getName() : searchField.searchField();
				String searchFieldPath = SearchField.SAME_TABLE.equals(searchField.searchFieldPath()) ? null : searchField.searchFieldPath();
				String searchFieldFormula = SearchField.NO_FORMULA.equals(searchField.formula()) ? null : searchField.formula();
				AssertUtils.assertTrue(searchFieldPath == null || searchFieldFormula == null, "The use of searchFieldPath property values are mutually exclusive with formula. for field: " + field.getName());
				final String sortFieldName;
				if (SearchField.SORT_NOT_ALLOWED.equals(searchField.sortField())) {
					sortFieldName = null;
				}
				else if (SearchField.SORT_BY_SEARCH_FIELD.equals(searchField.sortField())) {
					if (ArrayUtils.anyMatch(searchField.comparisonConditions(), ComparisonConditions::isSubQuery)) {
						// Disable sorting by default for sub-query conditions
						sortFieldName = null;
					}
					else if (multipleFieldTypes.isOrderByMultipleSupported()) {
						sortFieldName = searchFieldName;
					}
					else {
						sortFieldName = searchFieldName.split(",")[0];
					}
				}
				else {
					sortFieldName = searchField.sortField();
				}

				// created the definition based on field's data type
				if (Integer.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL,
								ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.NOT_EQUALS_OR_IS_NULL};
					}

					if (searchFieldName.endsWith(".id") || StringUtils.isEqual("id", searchFieldName)) {
						definition = new ForeignKeyRestrictionDefinition<Integer>(new IntegerRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(),
								sortFieldName, searchField.required(), comparisonConditions, multipleFieldTypes), null);
					}
					else {
						definition = new IntegerRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions, multipleFieldTypes);
					}
				}
				else if (Long.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL,
								ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.NOT_EQUALS_OR_IS_NULL};
					}

					if (searchFieldName.endsWith(".id") || StringUtils.isEqual("id", searchFieldName)) {
						definition = new ForeignKeyRestrictionDefinition<Long>(new LongRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName,
								searchField.required(), comparisonConditions, multipleFieldTypes), null);
					}
					else {
						definition = new LongRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions, multipleFieldTypes);
					}
				}
				else if (Short.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL,
								ComparisonConditions.EQUALS_OR_IS_NULL, ComparisonConditions.NOT_EQUALS_OR_IS_NULL};
					}

					if (searchFieldName.endsWith(".id") || StringUtils.isEqual("id", searchFieldName)) {
						definition = new ForeignKeyRestrictionDefinition<Long>(new ShortRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName,
								searchField.required(), comparisonConditions, multipleFieldTypes), null);
					}
					else {
						definition = new ShortRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions, multipleFieldTypes);
					}
				}
				else if (String.class.equals(type)) {
					// default to LIKE comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.LIKE, ComparisonConditions.EQUALS, ComparisonConditions.IN, ComparisonConditions.BEGINS_WITH,
								ComparisonConditions.NOT_LIKE, ComparisonConditions.NOT_EQUALS, ComparisonConditions.ENDS_WITH};
					}
					definition = new StringRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
							comparisonConditions, multipleFieldTypes);
				}
				else if (Date.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.EQUALS_OR_IS_NULL,
								ComparisonConditions.NOT_EQUALS_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL,
								ComparisonConditions.LESS_THAN_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, ComparisonConditions.IS_NULL};
					}
					definition = new DateRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(), comparisonConditions, multipleFieldTypes, searchField.dateFieldIncludesTime());
				}
				else if (Time.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.EQUALS_OR_IS_NULL,
								ComparisonConditions.NOT_EQUALS_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_IS_NULL, ComparisonConditions.GREATER_THAN_OR_EQUALS_OR_IS_NULL,
								ComparisonConditions.LESS_THAN_OR_IS_NULL, ComparisonConditions.LESS_THAN_OR_EQUALS_OR_IS_NULL, ComparisonConditions.IS_NULL};
					}
					definition = new TimeRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(), comparisonConditions, multipleFieldTypes);
				}
				else if (BigDecimal.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS};
					}
					definition = new BigDecimalRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
							comparisonConditions, multipleFieldTypes);
				}
				else if (Boolean.class.equals(type)) {
					// default to EQ comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS};
					}
					definition = new BooleanRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
							comparisonConditions, multipleFieldTypes);
				}
				else if (Double.class.equals(type)) {
					// default to EQ/GT/LT comparison if none is defined
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						comparisonConditions = new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN,
								ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS};
					}
					definition = new DoubleRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(), comparisonConditions);
				}
				else if (Enum.class.isAssignableFrom(type)) {
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						//noinspection rawtypes
						definition = new EnumRestrictionDefinition<>((Class<E>) type, field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required());
					}
					else {
						//noinspection rawtypes
						definition = new EnumRestrictionDefinition<>((Class<E>) type, field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, searchField.required(),
								comparisonConditions);
					}
				}
				else if (type.isArray()) {
					ComparisonConditions[] comparisonConditions = searchField.comparisonConditions();
					if (comparisonConditions.length == 0) {
						definition = new ArrayRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, multipleFieldTypes);
					}
					else {
						definition = new ArrayRestrictionDefinition(field.getName(), searchFieldName, searchFieldPath, searchField.leftJoin(), sortFieldName, comparisonConditions, multipleFieldTypes);
					}
				}
				else {
					throw new FieldValidationException("Unsupported search field data type '" + type + "' for field: " + field.getName(), field.getName());
				}
				if (searchFieldFormula != null) {
					String[] formulaFields = searchFieldName.split(",");
					if (formulaFields.length > SearchFormUtils.getSqlFormulaMaximumNumberOfParameters(searchFieldFormula)) {
						throw new RuntimeException(String.format("parameters: %1$s are more than expected for sqlFormula: %2$s", String.join(",", formulaFields), searchFieldFormula));
					}
					String sqlFormula = SearchFormUtils.convertSqlFormulaToStringFormatCompatible(searchFieldFormula);
					definition.setSqlFormula(sqlFormula);
				}
				definition.setSearchFieldDataType(type.isArray() ? type.getComponentType() : type);
				this.restrictionDefinitionMap.put(field.getName(), definition);

				// populate additional restrictions (field values that are not part of restrictionList
				Object value;
				try {
					field.setAccessible(true);
					value = field.get(searchForm);
				}
				catch (Exception e) {
					throw new RuntimeException("Cannot read value of field: " + field.getName(), e);
				}
				if (value != null) {
					if (searchForm.getRestrictionList() == null) {
						searchForm.setRestrictionList(new ArrayList<>());
					}
					for (SearchRestriction restriction : searchForm.getRestrictionList()) {
						if (field.getName().equals(restriction.getField())) {
							throw new FieldValidationException("Cannot set restrictions for the same field '" + field.getName() + "' in two ways.", field.getName());
						}
					}
					SearchRestriction restriction = new SearchRestriction();
					restriction.setField(field.getName());
					// use the first search comparison by default
					int conditionCount = CollectionUtils.getSize(definition.getSearchConditionList());
					if (conditionCount > 0) {
						ComparisonConditions comparison = definition.getSearchConditionList().iterator().next();
						restriction.setComparison(comparison);
						restriction.setValue(value);
						searchForm.getRestrictionList().add(restriction);
					}
					else {
						throw new FieldValidationException("When '" + field.getName()
								+ "' search field value is set directly, only 1 comparison condition must be defined or EQUALS must be present. Found "
								+ CollectionUtils.getSize(definition.getSearchConditionList()), field.getName());
					}
				}
			}
		}

		// Remove items from the restriction list the are properties of the search form.
		SearchUtils.moveCustomRestrictions(searchForm, this.restrictionDefinitionMap);
		// validate everything (FormUtils??? move method from SqlSearchFormConfigurer???)
		SearchUtils.validateRestrictionList(searchForm.getRestrictionList(), this.restrictionDefinitionMap, searchForm);
	}


	@SuppressWarnings({"rawtypes", "unchecked"})
	@Override
	public void configureCriteria(Criteria criteria) {
		BaseEntitySearchForm searchForm = getSortableSearchForm();

		// Fix Equals Comparison to check >= Date < Next Day
		SearchFormUtils.convertDateEqualsToDateRangeComparison(searchForm, this.restrictionDefinitionMap);

		//If this is a BaseEntityDateRangeWithoutTimeSearchForm, run custom logic to apply filters, etc.
		if (searchForm instanceof DateRangeAwareSearchForm) {
			((DateRangeAwareSearchForm) searchForm).configureDateRangeSearchForm(criteria);
		}

		Set<SearchFormCustomConfigurer> customConfigurerSet = SearchUtils.getSearchFormCustomConfigurerSetForSearchFormClass(searchForm.getClass());
		for (SearchFormCustomConfigurer searchFormCustomConfigurer : CollectionUtils.getIterable(customConfigurerSet)) {
			// Need to Pass Processed Path to Criteria Map so we don't overlap with aliases
			searchFormCustomConfigurer.configureCriteria(criteria, searchForm, getProcessedPathToCriteria());
		}

		for (SearchRestriction restriction : CollectionUtils.getIterable(searchForm.getRestrictionList())) {
			SearchRestrictionDefinition definition = this.restrictionDefinitionMap.get(restriction.getField());
			if (definition == null) {
				// check custom configurer
				SearchFormRestrictionConfigurer configurer = SearchUtils.getSearchFormRestrictionConfigurer(searchForm.getClass(), restriction.getField());
				if (configurer == null) {
					throw new IllegalArgumentException("Cannot find SearchFormRestrictionConfigurer for field '" + restriction.getField() + "' on form " + searchForm.getClass());
				}
				configurer.configureCriteria(criteria, restriction);
			}
			else {
				Object value = definition.getValue(restriction.getValue());

				Criteria restrictionCriteria = criteria;
				String[] searchFields = getSearchFieldsForDefinition(definition, criteria);
				if (!StringUtils.isEmpty(definition.getSearchFieldPath())) {
					if (!definition.getSearchFieldCustomType().isCompressed()) {
						restrictionCriteria = getProcessedPathToCriteria().get(definition.getSearchFieldPath());
					}
				}

				if (restrictionCriteria != null) {
					Criterion expression = getSearchFieldCriterion(searchFields[0], value, restriction, definition, criteria);
					if (definition.getSqlFormula() != null) {
						expression = new FormulaExpression(restriction.getComparison().getComparisonExpression(), definition.getSqlFormula(), searchFields, value);
					}
					else if (searchFields.length > 1 || definition.getSearchFieldCustomType().isSupportSingleField()) {
						SearchFieldCustomTypes fieldType = definition.getSearchFieldCustomType();
						if (SearchFieldCustomTypes.OR_IGNORE_SIGN_FOR_EQUALS == fieldType) {
							if (ComparisonConditions.EQUALS == restriction.getComparison()) {
								Number negatedValue;
								if (value instanceof BigDecimal) {
									negatedValue = MathUtils.negate((BigDecimal) value);
								}
								else if (value instanceof Number) {
									negatedValue = -((Number) value).longValue();
								}
								else {
									throw new IllegalArgumentException("The value of '" + value + "' of '" + searchFields[0] + "' property must be numeric.");
								}
								expression = Restrictions.or(expression, getSearchFieldCriterion(searchFields[1], negatedValue, restriction, definition, criteria));
							}
						}
						// default support for OR clause when multiple fields are specified
						else if (fieldType == null || fieldType.isOrSearch()) {
							for (int i = 1; i < searchFields.length; i++) {
								expression = Restrictions.or(expression, getSearchFieldCriterion(searchFields[i], value, restriction, definition, criteria));
							}
						}
						else if (SearchFieldCustomTypes.COALESCE == fieldType || SearchFieldCustomTypes.COALESCE_FALSE == fieldType || SearchFieldCustomTypes.COALESCE_TRUE == fieldType) {
							expression = new CoalesceExpression(restriction.getComparison().getComparisonExpression(), searchFields, value, fieldType.getCoalesceDefaultValue(), false);
						}
						else if (SearchFieldCustomTypes.COALESCE_NULL == fieldType) {
							expression = new CoalesceExpression(restriction.getComparison().getComparisonExpression(), searchFields, value, fieldType.getCoalesceDefaultValue(), true);
						}
						else if (SearchFieldCustomTypes.COALESCE_SORT_ON_ALL_NULL == fieldType) {
							expression = new AllAreNullExpression(restriction.getComparison().getComparisonExpression(), searchFields, value);
						}
						else if (SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER == fieldType) {
							expression = new ConcatenateStringAndNumberExpression(restriction.getComparison().getComparisonExpression(), searchFields, value);
						}
						else if (SearchFieldCustomTypes.JSON_COMPRESSED_TEXT == fieldType) {
							expression = new JsonCompressedTextExpression(definition.getSearchFieldPath(), searchFields[0], restriction.getComparison().getComparisonExpression(), value, definition.getSearchFieldDataType());
						}
						else if (fieldType.isMathFunctionSupported()) {
							expression = new MathExpression(fieldType.getMathFunction(), fieldType.isMathSupportNull(), restriction.getComparison().getComparisonExpression(), searchFields, value);
						}
						else if (fieldType.isDateDiffFunctionSupported()) {
							expression = new DateDiffExpression(fieldType.getDateDiffFunction(), restriction.getComparison().getComparisonExpression(), searchFields, value);
						}
						else {
							throw new ValidationException("Search Multiple Field Type [" + fieldType.name() + "] is not implemented.");
						}
					}

					restrictionCriteria.add(expression);
				}
			}
		}
	}


	private String[] getSortFieldsForDefinition(SearchRestrictionDefinition definition, Criteria criteria) {
		return getFieldsExpandedForDefinition(definition, criteria, definition.getOrderByFieldName().split(","));
	}


	protected String[] getSearchFieldsForDefinition(SearchRestrictionDefinition definition, Criteria criteria) {
		return getFieldsExpandedForDefinition(definition, criteria, definition.getSearchFieldName().split(","));
	}


	private String[] getFieldsExpandedForDefinition(SearchRestrictionDefinition definition, Criteria criteria, String[] fields) {
		// do joins as necessary
		for (int i = 0; i < fields.length; i++) {
			String searchField = fields[i];
			if (definition.isSubQuery()) {
				// Field joins will be created in the context of the sub-query; create path join and maintain raw field names here
				String pathAlias = getPathAlias(definition.getSearchFieldPath(), criteria, (definition.isLeftJoin() ? JoinType.LEFT_OUTER_JOIN : JoinType.INNER_JOIN));
				fields[i] = StringUtils.joinExcludingNulls(".", pathAlias, searchField);
			}
			else if (definition.getSearchFieldCustomType().isCompressed()) {
				fields[i] = searchField;
			}
			else {
				// composite field: may need to do additional joins
				fields[i] = HibernateUtils.getAliasedCriteriaPropertyPath(StringUtils.joinExcludingNulls(".", definition.getSearchFieldPath(), searchField), criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, definition.isLeftJoin() ? JoinType.LEFT_OUTER_JOIN : JoinType.INNER_JOIN, getProcessedPathToCriteria());
			}
		}
		return fields;
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		List<OrderByField> orderByList = SearchUtils.getOrderByFieldList(getSortableSearchForm().getOrderBy());
		if (CollectionUtils.isEmpty(orderByList)) {
			return this.skipDefaultSorting;
		}
		for (OrderByField field : orderByList) {
			SearchRestrictionDefinition definition = this.restrictionDefinitionMap.get(field.getName());
			boolean simpleDefinition = true;
			if (definition != null) {
				if (definition.getSqlFormula() != null) {
					criteria.addOrder(FormulaOrderBy.create(field.getDirection(), definition.getSqlFormula(), getSearchFieldsForDefinition(definition, criteria)));
					simpleDefinition = false;
				}
				SearchFieldCustomTypes fieldType = definition.getSearchFieldCustomType();
				if (fieldType != null && (fieldType.isOrderByMultipleSupported() || fieldType.isCompressed())) {
					String[] sortFields = getSortFieldsForDefinition(definition, criteria);
					if (sortFields != null && (sortFields.length > 1 || fieldType.isCompressed())) {
						simpleDefinition = false;
						switch (fieldType) {
							case COALESCE:
								criteria.addOrder(CoalesceOrderBy.create(field.getDirection(), sortFields));
								break;
							case COALESCE_SORT_ON_ALL_NULL:
								criteria.addOrder(AllAreNullOrderBy.create(field.getDirection(), sortFields));
								break;
							case CONCATENATE_SORT:
							case CONCATENATE_SORT_WITH_DELIMITER:
							case CONCATENATE_SORT_WITH_DELIMITER_AND_SPACE_PADDING:
							case CONCATENATE_SORT_WITH_PADDING:
							case CONCATENATE_STRING_AND_NUMBER:
								criteria.addOrder(ConcatenateOrderBy.create(field.getDirection(), fieldType, sortFields));
								break;
							case JSON_COMPRESSED_TEXT:
								criteria.addOrder(JsonCompressedTextOrderBy.create(definition.getSearchFieldPath(), sortFields[0], field.getDirection(), definition.getSearchFieldDataType()));
								break;
							default:
								if (fieldType.isMathFunctionSupported()) {
									criteria.addOrder(MathOrderBy.create(field.getDirection(), fieldType.getMathFunction(), fieldType.isMathSupportNull(), sortFields));
								}
								else if (fieldType.isDateDiffFunctionSupported()) {
									criteria.addOrder(DateDiffOrderBy.create(field.getDirection(), fieldType.getDateDiffFunction(), sortFields));
								}
								else {
									throw new ValidationException("Missing custom implementation for sorting. Unable to append order by for multiple fields for type [" + fieldType.name() + "].");
								}
								break;
						}
					}
				}
			}
			if (simpleDefinition) {
				configureOrderByField(criteria, field);
			}
		}
		return true;
	}


	protected void configureOrderByField(Criteria criteria, OrderByField field) {
		String name = getOrderByFieldName(field, criteria);
		criteria.addOrder(OrderByDirections.ASC == field.getDirection() ? Order.asc(name) : Order.desc(name));
	}


	private Criterion getSearchFieldCriterion(String searchField, Object value, SearchRestriction restriction, SearchRestrictionDefinition definition, Criteria criteria) {
		Criterion result = null;
		switch (restriction.getComparison()) {
			case EQUALS_OR_IS_NULL:
			case EQUALS:
				if (value instanceof Boolean) {
					// remap boolean null/not null conditions based on true or false
					List<ComparisonConditions> conditions = definition.getSearchConditionList();
					if (CollectionUtils.getSize(conditions) == 1) {
						ComparisonConditions condition = conditions.get(0);
						boolean booleanValue = ((Boolean) value);
						if (ComparisonConditions.IS_NOT_NULL == condition) {
							result = booleanValue ? Restrictions.isNotNull(searchField) : Restrictions.isNull(searchField);
							break;
						}
						else if (ComparisonConditions.IS_NULL == condition) {
							result = booleanValue ? Restrictions.isNull(searchField) : Restrictions.isNotNull(searchField);
							break;
						}
						else if (ComparisonConditions.NOT_EQUALS == condition) {
							result = Restrictions.eq(searchField, !booleanValue);
							break;
						}
					}
				}
				result = Restrictions.eq(searchField, value);
				break;
			case GREATER_THAN_OR_IS_NULL:
			case GREATER_THAN:
				result = Restrictions.gt(searchField, value);
				break;
			case GREATER_THAN_OR_EQUALS:
			case GREATER_THAN_OR_EQUALS_OR_IS_NULL:
				result = Restrictions.ge(searchField, value);
				break;
			case LESS_THAN_OR_IS_NULL:
			case LESS_THAN:
				result = Restrictions.lt(searchField, value);
				break;
			case LESS_THAN_OR_EQUALS:
			case LESS_THAN_OR_EQUALS_OR_IS_NULL:
				result = Restrictions.le(searchField, value);
				break;
			case NOT_EQUALS_OR_IS_NULL:
			case NOT_EQUALS:
				result = Restrictions.ne(searchField, value);
				break;
			case LIKE:
				result = Restrictions.like(searchField, (String) value, MatchMode.ANYWHERE);
				break;
			case NOT_LIKE:
				result = Restrictions.not(Restrictions.like(searchField, (String) value, MatchMode.ANYWHERE));
				break;
			case BEGINS_WITH:
				result = Restrictions.like(searchField, (String) value, MatchMode.START);
				break;
			case NOT_BEGINS_WITH:
				result = Restrictions.not(Restrictions.like(searchField, (String) value, MatchMode.START));
				break;
			case ENDS_WITH:
				result = Restrictions.like(searchField, (String) value, MatchMode.END);
				break;
			case NOT_ENDS_WITH:
				result = Restrictions.not(Restrictions.like(searchField, (String) value, MatchMode.END));
				break;
			case IS_NULL:
				if (value instanceof Boolean) {
					result = ((Boolean) value) ? Restrictions.isNull(searchField) : Restrictions.isNotNull(searchField);
				}
				else {
					result = Restrictions.isNull(searchField);
				}
				break;
			case IS_NOT_NULL:
				if (value instanceof Boolean) {
					result = ((Boolean) value) ? Restrictions.isNotNull(searchField) : Restrictions.isNull(searchField);
				}
				else {
					result = Restrictions.isNotNull(searchField);
				}
				break;
			case IN_OR_IS_NULL:
			case IN:
				result = getResultForInExpression(true, value, searchField);
				break;
			case NOT_IN:
				result = getResultForInExpression(false, value, searchField);
				break;
			case EXISTS:
			case EXISTS_LIKE:
			case NOT_EXISTS:
			case NOT_EXISTS_LIKE:
				result = getResultForExistsExpression(restriction, criteria, definition);
				break;
		}

		if (restriction.getComparison().isOrNull()) {
			result = Restrictions.or(result, Restrictions.isNull(searchField));
		}
		if (result == null) {
			throw new IllegalArgumentException("Unsupported search restriction comparison: " + restriction);
		}
		return result;
	}


	private Criterion getResultForInExpression(boolean in, Object value, String searchField) {
		Criterion result = null;

		Object[] values = null;

		if (value != null) {
			if (value instanceof String) {
				if (((String) value).contains("<OR>")) {
					value = Arrays.stream(((String) value).split("<OR>")).map(StringUtils::trim).collect(Collectors.toList());
				}
				value = new Object[]{value};
			}
			if (value.getClass().isArray()) {
				assert value instanceof Object[];
				values = (Object[]) value;
			}
			else {
				assert value instanceof List<?>;
				values = ((List<?>) value).toArray();
			}

			if (values.length == 0) {
				throw new IllegalArgumentException("The array of values passed into an 'IN' or 'NOT IN' expression cannot be empty.");
			}

			// convert String values to corresponding numeric representation if necessary
			SearchRestrictionDefinition restrictionDefinition = this.restrictionDefinitionMap.get(searchField);
			Class<?> fieldType = (restrictionDefinition == null) ? null : restrictionDefinition.getSearchFieldDataType();
			boolean numeric = (fieldType != null && Number.class.isAssignableFrom(fieldType));
			if (numeric) {
				if (values[0] instanceof String) {
					Object[] numericValues = new Object[values.length];
					for (int i = 0; i < values.length; i++) {
						numericValues[i] = BeanUtils.newInstance(fieldType, values[i]);
					}
					values = numericValues;
				}
			}

			if (numeric || value.getClass().getComponentType() == Integer.class) {
				// special case of optimized criteria without prepared params: often runs much faster
				if (in) {
					result = new InExpressionForNumbers(searchField, values);
				}
				else {
					result = new NotInExpressionForNumbers(searchField, values);
				}
			}
		}

		if (result == null) {
			if (in) {
				result = Restrictions.in(searchField, values);
			}
			else {
				result = Restrictions.not(Restrictions.in(searchField, values));
			}
		}

		return result;
	}


	private Criterion getResultForExistsExpression(SearchRestriction restriction, Criteria criteria, SearchRestrictionDefinition definition) {
		// parse the first qualifier to find the path to the target bean and the search properties.
		int index = definition.getSearchFieldName().indexOf(".");
		if (index == -1) {
			throw new RuntimeException("searchField [" + definition.getSearchFieldName() + "] must be a fully qualified property path.");
		}

		// If metadata is not available (e.g., application- or XML-based filtering criteria) then fall back to standard join behavior
		if (!(criteria instanceof CriteriaImpl)) {
			String fullPropertyPath = StringUtils.joinExcludingNulls(".", definition.getSearchFieldPath(), definition.getSearchFieldName());
			String aliasedPropertyPath = HibernateUtils.getAliasedCriteriaPropertyPath(fullPropertyPath, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.LEFT_OUTER_JOIN, getProcessedPathToCriteria());
			return restriction.getComparison().getSubQueryCriterion().apply(aliasedPropertyPath, restriction);
		}

		String associationPath = definition.getSearchFieldName().substring(0, index);

		// Retrieve schema relationship information via the ORM layer
		SessionFactory sessionFactory = HibernateUtils.getSessionFactory(criteria);
		String criteriaClassName = StringUtils.isEmpty(definition.getSearchFieldPath()) ? HibernateUtils.getCriteriaEntityClass(criteria).getName()
				: HibernateUtils.getEntityPropertyTypeForPath(sessionFactory, HibernateUtils.getCriteriaEntityClass(criteria), definition.getSearchFieldPath()).getName();
		String criteriaAlias = StringUtils.isEmpty(definition.getSearchFieldPath()) ? criteria.getAlias() : getPathAlias(definition.getSearchFieldPath(), criteria);
		String associationRole = criteriaClassName + "." + associationPath;
		String sourceIdentifierPropertyName = HibernateUtils.getIdentifierPropertyName(sessionFactory, criteriaClassName);
		Class<?> associationClass = HibernateUtils.getAssociationClass(sessionFactory, associationRole);
		String reversePropertyName = HibernateUtils.getAssociationKeyProperty(sessionFactory, associationRole, associationClass);

		// Generate detached criteria for exists clause
		DetachedCriteria subSelect = DetachedCriteria.forClass(associationClass, associationPath)
				.setProjection(Projections.id())
				.add(Restrictions.eqProperty(reversePropertyName + "." + sourceIdentifierPropertyName, criteriaAlias + "." + sourceIdentifierPropertyName));

		// when filtering on a field other than id (e.g. name), join the association table with the table containing the searched field
		String associationPathElement = (HibernateUtils.getCollectionPersister(sessionFactory, associationRole).isManyToMany())
				? HibernateUtils.getAssociationElementKeyProperty(sessionFactory, associationRole, associationClass)
				: null;
		String postAssociationSearchFieldName = definition.getSearchFieldName().substring(index + 1);
		String fullTargetFieldPath = StringUtils.joinExcludingNulls(".", associationPathElement, postAssociationSearchFieldName);
		String aliasedTargetPath = HibernateUtils.getAliasedCriteriaPropertyPath(fullTargetFieldPath, criteria, subSelect, "e", JoinType.INNER_JOIN, new HashMap<>());

		// compare col and value from SearchRestrictionDefinition if value is an array, use IN
		subSelect.add(restriction.getComparison().getSubQueryCriterion().apply(aliasedTargetPath, restriction));
		return (restriction.getComparison() == ComparisonConditions.EXISTS || restriction.getComparison() == ComparisonConditions.EXISTS_LIKE)
				? Subqueries.exists(subSelect)
				: Subqueries.notExists(subSelect);
	}


	@Override
	protected String getOrderByFieldName(OrderByField field, Criteria criteria) {
		SearchRestrictionDefinition definition = this.restrictionDefinitionMap.get(field.getName());
		if (definition == null) {
			// should we throw an exception here or is it OK to sort by a field that's not part of search definition?
			return field.getName();
		}
		String fieldPath = StringUtils.joinExcludingNulls(".", definition.getSearchFieldPath(), definition.getOrderByFieldName());
		return HibernateUtils.getAliasedCriteriaPropertyPath(fieldPath, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.LEFT_OUTER_JOIN, getProcessedPathToCriteria());
	}


	/**
	 * Returns the alias of the criteria for the specified path. If the joins haven't been processed yet,
	 * adds the necessary INNER joins to form the path first.
	 */
	protected String getPathAlias(String fieldPath, Criteria criteria) {
		return getPathAlias(fieldPath, criteria, JoinType.INNER_JOIN);
	}


	/**
	 * Returns the alias of the criteria for the specified path. If the joins haven't been processed yet,
	 * adds the necessary joins of the specified type (JoinType.INNER_JOIN, JoinType.LEFT_OUTER_JOIN, etc.) to form the path first.
	 */
	protected String getPathAlias(String fieldPath, Criteria criteria, JoinType joinType) {
		return HibernateUtils.getPathAlias(fieldPath, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, joinType, getProcessedPathToCriteria());
	}


	protected Map<String, Criteria> getProcessedPathToCriteria() {
		if (this.processedPathToCriteria == null) {
			this.processedPathToCriteria = new HashMap<>();
		}
		return this.processedPathToCriteria;
	}


	protected Map<String, SearchRestrictionDefinition> getRestrictionDefinitionMap() {
		return this.restrictionDefinitionMap;
	}
}
