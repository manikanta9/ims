package com.clifton.core.dataaccess.migrate.locator;

import com.clifton.core.security.authorization.SecureMethod;

import java.util.List;


/**
 * The MigrationModuleVersionService is for accessing the migration module version table.
 *
 * @author mitchellf
 */
public interface MigrationModuleVersionService {

	@SecureMethod(dtoClass = MigrationModuleVersion.class, securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<MigrationModuleVersion> getMigrationModuleVersionList(MigrationModuleVersionSearchForm searchForm);
}
