package com.clifton.core.dataaccess.sql;

import com.clifton.core.util.StringUtils;


/**
 * The SqlUpdateCommand class represents an SQL update statement (insert, update or delete).
 *
 * @author vgomelsky
 */
public class SqlUpdateCommand extends SqlCommand<SqlUpdateCommand> {


	/**
	 * If you are going to add WHERE clause conditions, make sure the WHERE clause is not present in the SQL.
	 */
	public SqlUpdateCommand(String sql) {
		super(sql);
	}


	/**
	 * If you are going to add WHERE clause conditions, make sure the WHERE clause is not present in the SQL.
	 */
	public static SqlUpdateCommand forSql(String sql) {
		return new SqlUpdateCommand(sql);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the value of the sql field if it was set or builds SQL statement based on set clauses.
	 */
	@Override
	public String getSql() {
		StringBuilder result = new StringBuilder(super.getSql());
		if (!StringUtils.isEmpty(getWhereClause())) {
			result.append("\nWHERE ");
			result.append(getWhereClause());
		}
		return result.toString();
	}
}
