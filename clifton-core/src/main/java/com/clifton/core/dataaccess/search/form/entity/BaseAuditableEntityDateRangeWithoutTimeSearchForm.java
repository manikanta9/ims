package com.clifton.core.dataaccess.search.form.entity;


import com.clifton.core.dataaccess.search.SearchField;

import java.util.Date;


/**
 * The <code>BaseAuditableEntityDateRangeWithoutTimeSearchForm</code> is a {@link BaseAuditableEntityDateRangeSearchForm} which includes fields for searching for the start date
 * and end date of the entity.
 * <p>
 * These fields take the time attached to the {@link Date} object into account, rather than just the date. Use {@link BaseAuditableEntityDateRangeWithTimeSearchForm} to
 * automatically adjust the field values into a 24-hour date range.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseAuditableEntityDateRangeSearchForm Parent fields}
 * <li>{@link #startDate}
 * <li>{@link #endDate}
 * </ul>
 *
 * @author jgommels
 * @see BaseAuditableEntityDateRangeWithTimeSearchForm
 */
public abstract class BaseAuditableEntityDateRangeWithoutTimeSearchForm extends BaseAuditableEntityDateRangeSearchForm {

	@SearchField
	private Date startDate;

	@SearchField
	private Date endDate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isIncludeTime() {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
}
