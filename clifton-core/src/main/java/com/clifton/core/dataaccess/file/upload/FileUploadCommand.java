package com.clifton.core.dataaccess.file.upload;


import com.clifton.core.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;


public abstract class FileUploadCommand<T> {

	/**
	 * The file is automatically bound by Spring
	 * This is then turned into a DataTable and then a list of <T> objects
	 * to process and create database entities
	 */
	private MultipartFile file;
	private int sheetIndex = 0; // Allows specifying which sheet to upload.  By default we always use the first sheet (currently customized/used for tests only)

	private String uploadResultsString;

	/**
	 * Used to determine how to handle existing beans in the database
	 * 1.  SKIP - Will lookup beans by Natural Keys and if it exists, will skip that
	 * row in the upload.
	 * 2.  UPDATE - Will lookup bean by Natural Keys and if it exists, will update that
	 * record. (Columns not included in the upload file will not be overwritten in the database)
	 * 3.  INSERT - Will NOT lookup beans in the database (FK look ups are still performed)
	 * and will assume all rows are inserts.  If there is an existing bean it should be caught
	 * by a UX constraint violation during the saves (Used for Improved Performance)
	 */
	private FileUploadExistingBeanActions existingBeans;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the class name of the beans the file will be converted into a list of
	 */
	public abstract Class<T> getUploadBeanClass();


	/**
	 * Override to support handling of columns that can't be mapped to a bean property
	 * i.e. can be useful for dynamic columns on a bean
	 */
	public void applyUnmappedColumnValues(T bean, Map<String, Object> unmappedColumnValueMap) {
		// DO NOTHING BY DEFAULT
	}


	/**
	 * Override if need to set some properties as required
	 */
	public String[] getRequiredProperties() {
		return null;
	}


	public void prependResult(String str) {
		if (this.uploadResultsString == null) {
			this.uploadResultsString = str;
		}
		else {
			this.uploadResultsString = str + this.uploadResultsString;
		}
	}


	public void appendResult(String str) {
		if (this.uploadResultsString == null) {
			this.uploadResultsString = str;
		}
		else {
			this.uploadResultsString += StringUtils.NEW_LINE + str;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}


	public String getUploadResultsString() {
		return this.uploadResultsString;
	}


	public void setUploadResultsString(String uploadResultsString) {
		this.uploadResultsString = uploadResultsString;
	}


	public FileUploadExistingBeanActions getExistingBeans() {
		return this.existingBeans;
	}


	public void setExistingBeans(FileUploadExistingBeanActions existingBeans) {
		this.existingBeans = existingBeans;
	}


	public int getSheetIndex() {
		return this.sheetIndex;
	}


	public void setSheetIndex(int sheetIndex) {
		this.sheetIndex = sheetIndex;
	}
}
