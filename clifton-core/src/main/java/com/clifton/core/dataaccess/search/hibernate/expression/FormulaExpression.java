package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.BasicTypeRegistry;


/**
 * The <code>FormulaExpression</code> is use by {@link SearchField#formula()} to sort based on a custom sql query.
 * <p/>
 * The sqlFormula provided is expected to be {@link String#format(String, Object...)} compatible,
 * with the expected number of arguments equal to propertyNames length. Compatibility is is taken care of at runtime
 * by {@link com.clifton.core.dataaccess.search.restrictions.AbstractRestrictionDefinition#setSqlFormula(String)}
 *
 * @author dillonm
 */
public class FormulaExpression implements Criterion {

	private final String[] propertyNames;
	private final String comparisonExpression;
	private final String sqlFormula;
	private final Object value;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FormulaExpression(String comparisonExpression, String sqlFormula, String[] propertyNames, Object value) {
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.sqlFormula = sqlFormula;
		this.value = value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		String[] columns = new String[this.propertyNames.length];
		for (int i = 0; i < this.propertyNames.length; ++i) {
			columns[i] = (criteriaQuery.getColumn(criteria, this.propertyNames[i]));
		}
		StringBuilder fragmentSb = new StringBuilder(String.format(this.sqlFormula, (Object[]) columns));
		fragmentSb.append(" ");
		fragmentSb.append(this.comparisonExpression);
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragmentSb.append(" ?");
		}
		return fragmentSb.toString();
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		TypedValue typedValue = new TypedValue(new BasicTypeRegistry().getRegisteredType(this.value.getClass().getName()), this.value);
		return new TypedValue[]{typedValue};
	}
}
