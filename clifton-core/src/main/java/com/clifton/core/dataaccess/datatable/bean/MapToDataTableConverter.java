package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.PagingDataTable;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Map;
import java.util.function.Function;


/**
 * {@link MapToDataTableConverter} converts a {@link java.util.Map} to a {@link DataTable}.
 *
 * @author michaelm
 */
public class MapToDataTableConverter extends BaseDataTableConverter<Map<Object, Object>> {

	public MapToDataTableConverter(DataTableConverterConfiguration converterConfiguration) {
		initialize(converterConfiguration);
	}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts the {@param beanMap} to a {@link PagingDataTable}, generating each {@link DataRow} by applying the {@link #getMapDataRowFunction()} to each entry in the map.
	 */
	@Override
	public PagingDataTableImpl convert(Map<Object, Object> beanMap) {
		PagingDataTableImpl table = new PagingDataTableImpl(getDataColumnArray());
		if (CollectionUtils.isEmpty(beanMap)) {
			return table;
		}
		Map<Object, Object> map = beanMap;
		for (Map.Entry<Object, Object> objectEntry : map.entrySet()) {
			table.addRow(new DataRowImpl(getDataColumnArray(), getMapDataRowFunction().apply(objectEntry)));
		}
		return table;
	}


	@Override
	public void validateConverterConfiguration() {
		ValidationUtils.assertNotNull(getConverterConfiguration().getMapEntryToDataArrayFunction(), "MapDataRowFunction is required for the MapToDataTableConverter.");
	}


	@Override
	public DataColumn[] getOrBuildDataColumnArray() {
		DataTableConverterConfiguration config = getConverterConfiguration();
		if (config != null) {
			if (config.getDataColumnArray() != null) {
				return config.getDataColumnArray();
			}
			ValidationUtils.assertNull(config.getDataColumnString(), "DataColumnSting column generation is not currently supported by the " + this.getClass().getSimpleName());
		}
		return new DataColumn[0];
	}

////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


	public Function<Map.Entry<Object, Object>, Object[]> getMapDataRowFunction() {
		return this.converterConfiguration.getMapEntryToDataArrayFunction();
	}
}
