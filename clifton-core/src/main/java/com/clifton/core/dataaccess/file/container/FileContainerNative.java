package com.clifton.core.dataaccess.file.container;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.function.Predicate;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;


/**
 * Represents files that are accessible to the host operating system natively.
 * <p>
 * I.e. on Windows, SMB shares are accessible through the OS itself.
 * <p>
 * The {@link FileContainerNative#list(String, Predicate)} should include the most-restrictive glob pattern as possible, especially for
 * large directories otherwise performance will be a significant problem.
 *
 * @author theodorez
 */
public class FileContainerNative extends File implements FileContainer {

	public FileContainerNative(File file) {
		super(file.getPath());
	}


	public FileContainerNative(String filePath) {
		super(filePath);
	}


	@Override
	public Stream<String> list(String glob, Predicate<String> fileNameMatcher) throws IOException {
		String globPattern = StringUtils.isEmpty(glob) ? "*" : glob;
		Predicate<String> fileNamePredicate = fileNameMatcher == null ? name -> true : fileNameMatcher;
		DirectoryStream<Path> directoryStream = Files.newDirectoryStream(this.toPath(), globPattern);
		return StreamSupport.stream(directoryStream.spliterator(), false)
				.map(Path::getFileName)
				.map(Path::toString)
				.filter(fileNamePredicate);
	}


	@Override
	public void deleteFile() throws IOException {
		Files.delete(Paths.get(getAbsolutePath()));
	}


	@Override
	public InputStream getInputStream() throws IOException {
		return new FileInputStream(this);
	}


	@Override
	public OutputStream getOutputStream() throws IOException {
		return new FileOutputStream(this);
	}


	@Override
	public boolean makeDirs() {
		return this.mkdirs();
	}


	@Override
	public byte[] readBytes() throws IOException {
		if (this.length() > 1_073_741_824) {
			throw new RuntimeException("Cannot read the bytes from file: " + this.getAbsolutePath() + "larger than 1 gigabytes");
		}
		return Files.readAllBytes(Paths.get(FileUtils.normalizeFilePathSeparators(this.getAbsolutePath())));
	}


	@Override
	public void writeBytes(byte[] bytes) throws IOException {
		Files.write(this.toPath(), bytes);
	}


	@Override
	public boolean renameToFile(FileContainer toFile) {
		ValidationUtils.assertTrue(toFile instanceof FileContainerNative, "Rename is not supported unless to and from files are of the same type.");
		return renameTo((File) toFile);
	}
}
