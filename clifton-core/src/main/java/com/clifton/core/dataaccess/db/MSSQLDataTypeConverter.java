package com.clifton.core.dataaccess.db;


import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;


/**
 * The <code>MSSQLDataTypeConverter</code> converts from DataTypes to corresponding SQL data types.
 *
 * @author vgomelsky
 */
public class MSSQLDataTypeConverter implements DatabaseDataTypeConverter {

	@Override
	public String convert(DataTypes dataType, DataTypeEncodingTypeNames encodingTypeName) {
		if (dataType == null) {
			throw new IllegalArgumentException("Argument data type cannot be null.");
		}
		if (dataType.getTypeName().isSupportEncoding() && encodingTypeName == null) {
			throw new IllegalArgumentException(("Argument encoding type name is required for data type " + dataType.getTypeName().name()));
		}
		if (dataType == DataTypes.ROWVERSION) {
			return "ROWVERSION";
		}
		if (dataType.getTypeName() == DataTypeNames.STRING) {
			if (dataType == DataTypes.CUSTOM_JSON_STRING) {
				return getStringDataType(encodingTypeName, "MAX");
			}
			return getStringDataType(encodingTypeName, dataType.getLength() + "");
		}
		switch (dataType) {
			case INT:
			case IDENTITY:
			case NATURAL_ID:
			case TIME:
				return "INT";
			case LONG:
			case IDENTITY_LONG:
			case NATURAL_ID_LONG:
				return "BIGINT";
			case SHORT:
			case IDENTITY_SHORT:
			case NATURAL_ID_SHORT:
				return "SMALLINT";
			case TINY:
			case IDENTITY_TINY:
			case NATURAL_ID_TINY:
				return "TINYINT";
		}
		if (dataType.getTypeName() == DataTypeNames.DECIMAL) {
			return "DECIMAL(" + dataType.getLength() + "," + dataType.getPrecision() + ")";
		}
		switch (dataType) {
			case DATETIME:
				return "DATETIME";
			case DATE:
				return "DATE";
			case BIT:
				return "BIT";
			case TEXT:
				return getStringDataType(encodingTypeName, "MAX");
			case SECRET:
			case SECRET_SHORT:
				return "VARBINARY(" + dataType.getLength() + ")";
			case SECRET_MAX:
			case COMPRESSED_TEXT:
				return "VARBINARY(MAX)";
		}
		throw new IllegalArgumentException("Unsupported data type: " + dataType);
	}


	private String getStringDataType(DataTypeEncodingTypeNames encodingTypeName, String maxLengthString) {
		if (encodingTypeName == DataTypeEncodingTypeNames.UTF_8) {
			return "VARCHAR(" + maxLengthString + ") COLLATE LATIN1_GENERAL_100_CI_AS_SC_UTF8";
		}
		return "NVARCHAR(" + maxLengthString + ")";
	}
}
