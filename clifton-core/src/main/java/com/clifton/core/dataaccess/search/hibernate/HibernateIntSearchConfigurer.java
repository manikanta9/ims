package com.clifton.core.dataaccess.search.hibernate;


/**
 * The <code>HibernateIntSearchConfigurer</code> class is a hibernate Criteria specific implementation of searches
 * by a single int field value.
 *
 * @author vgomelsky
 */
public abstract class HibernateIntSearchConfigurer implements HibernateSearchConfigurer {

	private final int intValue;


	/**
	 * Constructs a new HibernateSearchByIntCriteria that wraps the specified intValue.
	 *
	 * @param intValue
	 */
	public HibernateIntSearchConfigurer(int intValue) {
		this.intValue = intValue;
	}


	/**
	 * @return the intValue
	 */
	public int getIntValue() {
		return this.intValue;
	}
}
