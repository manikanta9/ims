package com.clifton.core.dataaccess.function;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.dialect.function.SQLFunction;
import org.hibernate.type.Type;

import java.util.List;


/**
 * <code>SQLIfNullFunction</code> intended to be passed the dialect specific function for:
 * f(x,y) = if x is null then y else x
 *
 * @author dillonm
 */
public interface SQLIfNullFunction {

	public static final String FUNCTION_NAME = "ppa_ifnull";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates a SQL statement that returns the property if not null, otherwise returns the nullResult.
	 * The resulting SQL statement is dependent on the dialect configured by the {@link com.clifton.core.dataaccess.dialect.SessionFactoryBuilderConfigurer} implementation.
	 */
	public static String render(Criteria criteria, CriteriaQuery criteriaQuery, String property, String nullResult) {
		Type argumentType = criteriaQuery.getType(criteria, property);
		List<String> arguments = SQLFunctionUtils.buildFunctionParameterList(criteria, criteriaQuery, property, nullResult);
		SQLFunction sqlFunction = SQLFunctionUtils.getFunction(criteriaQuery, FUNCTION_NAME);
		return sqlFunction.render(argumentType, arguments, criteriaQuery.getFactory());
	}
}
