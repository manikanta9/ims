package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.cache.CacheTypes;
import com.clifton.core.cache.hibernate.CacheTypeToHibernateTypeConverter;
import com.clifton.core.dataaccess.db.TableTypes;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverter;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Constraint;
import com.clifton.core.dataaccess.migrate.schema.Index;
import com.clifton.core.dataaccess.migrate.schema.Link;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>HibernateSchemaConverter</code> class converts Schema objects to Hibernate XML definitions.
 *
 * @author vgomelsky
 */
public class HibernateSchemaConverter implements MigrationSchemaConverter {

	private static final Set<String> excludedRequiredFieldList = new HashSet<>(Arrays.asList("WorkflowStateID", "WorkflowStatusID", "CreateUserID", "CreateDate"));


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.HIBERNATE;
	}


	@Override
	public String convert(Schema schema) {
		return convert(schema, false);
	}


	/**
	 * When inMemoryDB is true the mapping file excludes the '<version></version>' tag that is used for the rv column for some tables.
	 * The version tag is not handled properly by the hsql in memory database and causes inserts into those tables to fail.
	 */
	public String convert(Schema schema, boolean inMemoryDB) {
		CacheTypeToHibernateTypeConverter cacheTypeConverter = new CacheTypeToHibernateTypeConverter();
		StringBuilder result = new StringBuilder(1024);
		result.append("<?xml version=\"1.0\"?>\n");
		result.append("<!DOCTYPE hibernate-mapping SYSTEM \"http://www.hibernate.org/dtd/hibernate-mapping-3.0.dtd\">\n");
		result.append("<hibernate-mapping>\n\n");

		String schemaTypeDefinitions = getSchemaTypeDefinitions(schema);
		if (!schemaTypeDefinitions.isEmpty()) {
			result.append(schemaTypeDefinitions).append("\n\n");
		}

		for (Table table : CollectionUtils.getIterable(schema.getTableList())) {
			result.append("\t<class name=\"");
			result.append(table.getDtoClass());
			result.append("\" table=\"");
			result.append(table.getName());
			// append schema if not empty
			if (!StringUtils.isEmpty(table.getSchema())) {
				result.append("\" schema=\"");
				result.append(table.getSchema());
			}
			if (!StringUtils.isEmpty(table.getDtoProxy())) {
				result.append("\" proxy=\"");
				result.append(table.getDtoProxy());
			}
			if (table.getDiscriminatorValue() != null) {
				result.append("\" discriminator-value=\"");
				result.append(table.getDiscriminatorValue());
			}
			if (table.isCacheUsed() && table.getCacheType() == CacheTypes.READ_ONLY) {
				result.append("\" mutable=\"false");
			}
			result.append("\">\n");

			if (table.isCacheUsed()) {
				result.append("\t\t<cache usage=\"");
				result.append(cacheTypeConverter.convert(table.getCacheType()));
				result.append("\" />\n");
			}

			// Append Columns
			appendColumns(table, table.getColumnList(), result, "\t\t", false, inMemoryDB);

			// append links if any
			appendLinks(table.getLinkList(), result, "\t\t");

			for (Subclass subclass : CollectionUtils.getIterable(table.getSubclassList())) {
				result.append("\n\t\t<subclass name=\"");
				result.append(subclass.getDtoClass());
				result.append("\" discriminator-value=\"");
				result.append(subclass.getDiscriminatorValue());
				result.append("\">\n");
				appendColumns(table, subclass.getColumnList(), result, "\t\t\t", true, inMemoryDB);
				appendLinks(subclass.getLinkList(), result, "\t\t\t");
				result.append("\t\t</subclass>\n");
			}
			result.append("\t</class>\n\n\n");
		}
		result.append("</hibernate-mapping>");
		return result.toString();
	}


	private String getSchemaTypeDefinitions(Schema schema) {
		// For all tables, get unique schema-level (i.e. named) type definitions
		return CollectionUtils.getStream(schema.getTableList())
				.map(table -> CollectionUtils.combineCollections(Collections.singletonList(table), table.getSubclassList()))
				.flatMap(Collection::stream)
				.flatMap(table -> table.getColumnList().stream())
				.filter(column -> column.getTypeDefinitionName() != null)
				.filter(CollectionUtils.createDistinctPredicate(Column::getTypeDefinitionName))
				.map(column -> getTypeDefinition(column, "\t", false))
				.collect(Collectors.joining("\n"));
	}


	private String getTypeDefinition(Column column, String prefix, boolean inline) {
		// Validate column properties
		if (inline) {
			AssertUtils.assertNull(column.getTypeDefinitionName(), "The type definition for table [%s] and column [%s] must not have a type definition name defined.", column.getTableName(), column.getName());
		}
		else {
			AssertUtils.assertNotNull(column.getTypeDefinitionName(), "The type definition for table [%s] and column [%s] must have a type definition name defined.", column.getTableName(), column.getName());
		}
		AssertUtils.assertNotNull(column.getTypeDefinitionClassName(), "The type definition for table [%s] and column [%s] must have a type definition class name defined.", column.getTableName(), column.getName());
		AssertUtils.assertNotNull(column.getTypeDefinitionConverterClass(), "The type definition for table [%s] and column [%s] must have a type definition converter class defined.", column.getTableName(), column.getName());

		// Generate type definition
		StringBuilder result = new StringBuilder();
		String typeDefinitionTagName = inline ? "type" : "typedef";
		result.append(prefix).append("<").append(typeDefinitionTagName);
		if (inline) {
			// Inline property-specific type definition
			result.append(" name=\"").append(column.getTypeDefinitionConverterClass()).append("\"");
		}
		else {
			// Shared schema-level type definition
			result.append(" name=\"").append(column.getTypeDefinitionName()).append("\"");
			result.append(" class=\"").append(column.getTypeDefinitionConverterClass()).append("\"");
		}
		result.append(">\n");
		result.append(prefix).append("\t<param name=\"typeDefinitionClassName\"><![CDATA[").append(column.getTypeDefinitionClassName()).append("]]></param>\n");
		result.append(prefix).append("</").append(typeDefinitionTagName).append(">\n");
		return result.toString();
	}


	private void appendColumns(Table table, List<Column> columnList, StringBuilder result, String prefix, boolean subclass, boolean inMemoryDB) {
		boolean pkFound = false;
		List<Index> indices = table.getIndexList();
		for (Column column : CollectionUtils.getIterable(columnList)) {
			if (column.getDataType().isIdentity() || column.getDataType().isNaturalKey()) {
				pkFound = true;
				if (column.getDataType().isIdentity()) {
					// auto-generated primary key
					result.append(prefix).append("<id name=\"");
					result.append(column.getBeanPropertyName());
					result.append("\" column=\"");
					result.append(column.getName());
					String typeString = getIdentityTypeString(column.getDataType());
					if (!StringUtils.isEmpty(typeString)) {
						result.append("\" type=\"");
						result.append(typeString);
					}
					if (table.isBatchEnabled()) {
						result.append("\">\n").append(prefix).append("\t<generator class=\"enhanced-sequence\">\n")
								.append(prefix).append("\t\t<param name=\"optimizer\">pooled-lo</param>\n")
								.append(prefix).append("\t\t<param name=\"initial_value\">1</param>\n")
								.append(prefix).append("\t\t<param name=\"increment_size\">50</param>\n")
								.append(prefix).append("\t\t<param name=\"sequence_name\">").append(table.getName()).append("Sequence").append("</param>\n")
								.append(prefix).append("\t</generator>\n")
								.append(prefix).append("</id>\n");
					}
					else {
						result.append("\">\n").append(prefix).append("\t<generator class=\"identity\" />\n").append(prefix).append("</id>\n");
					}
				}
				else {
					result.append(prefix).append("<id name=\"");
					result.append(column.getBeanPropertyName());
					result.append("\" column=\"");
					result.append(column.getName());
					String typeString = getIdentityTypeString(column.getDataType());
					if (!StringUtils.isEmpty(typeString)) {
						result.append("\" type=\"");
						result.append(typeString);
					}
					result.append("\">\n").append(prefix).append("\t<generator class=\"assigned\" />\n").append(prefix).append("</id>\n");
				}
				// Append Discriminator Formula if any
				if (table.getDiscriminatorFormula() != null) {
					result.append("\t\t<discriminator formula=\"");
					result.append(table.getDiscriminatorFormula());
					result.append("\" type=\"string");
					result.append("\" />\n");
				}
				if (Boolean.TRUE.equals(table.getVersion()) && !inMemoryDB) {
					// "always" generated by the database; excluded from "insert" statements
					result.append(prefix).append("<version name=\"rv\" column=\"rv\" type=\"binaryUserType\" insert=\"false\" generated=\"always\" />\n");
				}
			}
			// Do not need to check for Primary Key or Natural Key at Subclass Level
			else if (!subclass && TableTypes.SIMPLE == table.getTableType() && !pkFound) {
				result.append(prefix).append("<composite-id>");
				for (Column keyColumn : CollectionUtils.getIterable(table.getColumnList())) {
					if (keyColumn.isNaturalKey()) {
						result.append("\n\t\t\t<key-property name=\"").append(keyColumn.getBeanPropertyName());
						if (DataTypes.DATE == keyColumn.getDataType()) {
							result.append("\" type=\"date");
						}
						result.append("\" />");
						pkFound = true;
					}
				}
				if (!pkFound) {
					throw new IllegalStateException("Table '" + table.getName() + "' of SIMPLE table type must have Natural Key defined.");
				}
				result.append("\n\t\t</composite-id>\n");
			}
			else if (!"rv".equals(column.getName()) || !Boolean.TRUE.equals(table.getVersion())) {
				Constraint constraint = table.getForeignKeyConstraint(column);
				if (constraint != null && (column.getForceSimpleProperty() == null || column.getForceSimpleProperty().equals(false))) {
					if (constraint.getPrimaryColumnList().size() > 1) {
						throw new RuntimeException("Composite foreign keys are not supported. Table = " + table.getName());
					}
					AssertUtils.assertNotNull(constraint.getReferenceTable(), "The reference table must be set for constraint '%1s'", constraint.getName());
					result.append(prefix).append("<many-to-one");
					/*
					 * The embed-xml attribute is not needed, but adding this here removes a false warning that gets displayed during initialization.
					 *
					 * Once we have upgraded to a version of Hibernate that fixes this false warning, the following steps should be taken:
					 * - Remove the appended "embed-xml" attribute in this method
					 * - Remove the "embed-xml" attribute from many-to-one elements in query files
					 * - Remove the "embed-xml" attribute from many-to-one elements in expected results for tests
					 *
					 * See https://hibernate.atlassian.net/browse/HHH-9882
					 */
					result.append(" embed-xml=\"false\"");
					result.append(" lazy=\"").append(column.getLazy() == null ? "false" : column.getLazy()).append('"');
					result.append(" name=\"").append(column.getBeanPropertyName()).append('"');
					result.append(" column=\"").append(column.getName()).append('"');
					//For in memory tests we want to add the mapping for required fields so validation exceptions are thrown when required data is missing
					if (isRequiredForHibernate(column, inMemoryDB)) {
						result.append(" not-null=\"true\"");
					}
					result.append(" class=\"").append(constraint.getReferenceTable().getDtoClass()).append('"');
					if (inMemoryDB) {
						String uniqueIndices = getIndicesForColumn(column, indices);
						if (!uniqueIndices.isEmpty()) {
							result.append(" unique-key=\"").append(uniqueIndices).append('"');
						}
					}
					result.append(" />\n");
				}
				else {
					StringBuilder innerContent = new StringBuilder();
					result.append(prefix).append("<property");
					result.append(" name=\"").append(column.getBeanPropertyName()).append('"');
					if (inMemoryDB || !(column.isCalculatedColumn() && table.isCacheUsed())) {
						result.append(" column=\"").append(column.getName()).append('"');
					}
					//For in memory tests we want to add the mapping for required fields so validation exceptions are thrown when required data is missing
					if (isRequiredForHibernate(column, inMemoryDB)) {
						result.append(" not-null=\"true\"");
					}
					// append type specific attributes
					if (column.getTypeDefinitionName() != null) {
						result.append(" type=\"").append(column.getTypeDefinitionName()).append('"');
					}
					else if (column.getTypeDefinitionClassName() != null) {
						innerContent.append(getTypeDefinition(column, prefix + '\t', true));
					}
					else if (DataTypes.DATE == column.getDataType()) {
						result.append(" type=\"date\"");
					}
					else if (DataTypes.TIME == column.getDataType()) {
						result.append(" type=\"timeType\"");
					}
					else if (DataTypes.CUSTOM_JSON_STRING == column.getDataType()) {
						result.append(" type=\"customJsonString\"");
					}
					else if (DataTypeNames.DECIMAL == column.getDataType().getTypeName()) {
						result.append(" type=\"big_decimal\"");
						result.append(" precision=\"").append(column.getDataType().getLength()).append('"');
						result.append(" scale=\"").append(column.getDataType().getPrecision()).append('"');
					}
					// append STRING column length
					// Ignore ROWVERSION because it has length 0, which breaks the in-memory database creation with hsql
					// Ignore calculated columns if caching is used
					if (DataTypes.ROWVERSION != column.getDataType()
							&& !(column.isCalculatedColumn() && table.isCacheUsed())
							&& (DataTypeNames.STRING == column.getDataType().getTypeName() || DataTypeNames.TEXT == column.getDataType().getTypeName())) {
						result.append(" length=\"").append(column.getDataType().getLength()).append('"');
					}
					// hsqldb used for in memory db tests does not support calculated columns so ignore them in schema mappings
					if (!inMemoryDB) {
						// append attributes specific to calculated/generated values
						if (column.getColumnExcludedFromInserts() != null && column.getColumnExcludedFromInserts()) {
							result.append(" insert=\"false\"");
						}
						if (column.getColumnExcludedFromUpdates() != null && column.getColumnExcludedFromUpdates()) {
							result.append(" update=\"false\"");
						}
						// Append calculation to hibernate file as formula if caching is used
						// so that hibernate will properly cache calculated values when entity is updated
						if (column.isCalculatedColumn() && table.isCacheUsed()) {
							result.append(" formula=\"").append(column.getCalculation()).append('"');
						}
					}
					if (column.isNoBeanProperty()) {
						result.append(" access=\"noop\"");
					}
					if (inMemoryDB) {
						String uniqueIndices = getIndicesForColumn(column, indices);
						if (!uniqueIndices.isEmpty()) {
							result.append(" unique-key=\"").append(uniqueIndices).append('"');
						}
					}
					if (innerContent.length() > 0) {
						result.append(">\n");
						result.append(innerContent.toString());
						result.append(prefix).append("</property>\n");
					}
					else {
						result.append(" />\n");
					}
				}
			}
		}
	}


	private String getIdentityTypeString(DataTypes dataType) {
		if (DataTypeNames.INTEGER == dataType.getTypeName()) {
			if (dataType.toString().endsWith("LONG")) {
				return "long";
			}
			else if (dataType.toString().endsWith("SHORT") || dataType.toString().endsWith("TINY")) {
				return "short";
			}
			else {
				return "int";
			}
		}
		return null;
	}


	private void appendLinks(List<Link> links, StringBuilder result, String prefix) {
		for (Link link : CollectionUtils.getIterable(links)) {
			result.append(prefix).append("<bag lazy=\"").append(!link.isReal()).append("\" name=\"");
			result.append(link.getName());
			if (!StringUtils.isEmpty(link.getManyToManyTable())) {
				result.append("\" table=\"");
				result.append(link.getManyToManyTable());
			}
			if (!link.isReal()) {
				// DTO class doesn't have this property; used for SQL navigation only
				result.append("\" access=\"noop");
			}
			if (!StringUtils.isEmpty(link.getCascade())) {
				result.append("\" cascade=\"").append(link.getCascade());
			}
			result.append("\" inverse=\"true\">\n"); // means that the other side table has "inverse" relationship pointing here and it will manage related changes
			result.append(prefix).append("\t<key column=\"");
			if (StringUtils.isEmpty(link.getManyToManyTable())) {
				result.append(link.getReferencingColumn());
				result.append("\" />\n").append(prefix).append("\t<one-to-many class=\"");
			}
			else {
				result.append(link.getManyToManyColumn());
				result.append("\" />\n").append(prefix).append("\t<many-to-many column=\"");
				result.append(link.getReferencingColumn());
				result.append("\" class=\"");
			}
			result.append(link.getReferencingClass());
			result.append("\" />\n");
			result.append(prefix).append("</bag>\n");
		}
	}


	private boolean isRequiredForHibernate(Column column, boolean inMemoryDB) {
		if (inMemoryDB && BooleanUtils.isTrue(column.getColumnRequired()) && !"rv".equals(column.getName()) && !excludedRequiredFieldList.contains(column.getName())) {
			return true;
		}
		return false;
	}


	private String getIndicesForColumn(Column column, List<Index> indices) {
		StringBuilder indexIncludes = new StringBuilder();
		if (indices != null) {
			for (Index index : CollectionUtils.getIterable(indices)) {
				if (index.isUnique()) {
					List<Column> columnList = index.getColumnList();
					if (columnList != null && columnList.size() > 1) {
						for (Column indexColumn : CollectionUtils.getIterable(columnList)) {
							if (column.getName().equals(indexColumn.getName())) {
								if (indexIncludes.length() != 0) {
									indexIncludes.append(',');
								}
								indexIncludes.append(index.getName());
							}
						}
					}
				}
			}
		}
		return indexIncludes.toString();
	}
}
