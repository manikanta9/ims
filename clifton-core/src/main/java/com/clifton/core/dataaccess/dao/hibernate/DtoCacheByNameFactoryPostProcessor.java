package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityFilterableCache;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.core.Ordered;

import java.beans.Introspector;
import java.util.Collection;
import java.util.stream.Collectors;


/**
 * The {@link BeanFactoryPostProcessor} responsible for registering {@link NamedEntityCache} beans based on existing {@link DAOConfiguration} beans.
 * <p>
 * This bean factory post processor performs early instantiation on all registered {@link DAOConfiguration} and scans associated DTO classes for {@link CacheByName}-annotated
 * types. For each {@link CacheByName} type, a {@link NamedEntityCache} is registered in the bean factory.
 * <p>
 * This bean factory post processor must be executed after the {@link HibernateDAOFactoryPostProcessor}.
 *
 * @author MikeH
 */
public class DtoCacheByNameFactoryPostProcessor implements BeanFactoryPostProcessor, Ordered {

	@Override
	public int getOrder() {
		// This must be executed after HibernateDAOFactoryPostProcessor
		return 100;
	}


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		for (Class<? extends IdentityObject> dtoClass : getCacheDtoClassList(beanFactory)) {
			generateDtoCache((DefaultListableBeanFactory) beanFactory, dtoClass);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Collection<Class<? extends IdentityObject>> getCacheDtoClassList(ConfigurableListableBeanFactory beanFactory) {
		return beanFactory.getBeansOfType(DAOConfiguration.class, true, false)
				.values().stream()
				.map(daoConfiguration -> {
					@SuppressWarnings("unchecked")
					Class<? extends IdentityObject> dtoClass = daoConfiguration.getBeanClass();
					return dtoClass;
				})
				.filter(dtoClass -> AnnotationUtils.isAnnotationPresent(dtoClass, CacheByName.class))
				.collect(Collectors.toList());
	}


	private void generateDtoCache(DefaultListableBeanFactory beanFactory, Class<? extends IdentityObject> beanClass) {
		try {
			// Get and validate cache name
			String cacheBeanName = getDtoCacheName(beanClass);
			AssertUtils.assertFalse(beanFactory.containsBeanDefinition(cacheBeanName), "The named entity cache bean definition [%s] has already been configured.", cacheBeanName);

			// Create and register cache definition
			CacheByName beanClassAnnotation = AnnotationUtils.getAnnotation(beanClass, CacheByName.class);
			BeanDefinition cacheDefinition = BeanDefinitionBuilder.genericBeanDefinition(beanClassAnnotation.filterable() ? NamedEntityFilterableCache.class : NamedEntityCache.class)
					.setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_NAME)
					.setLazyInit(true)
					.addPropertyValue("dtoClass", beanClass)
					// For View Cache Stats, etc. We want to use the full package of the DTO + Cache name so caches stay together
					.addPropertyValue("cacheName", String.format("%s.%s",
							StringUtils.substringBeforeLast(beanClass.getName(), "."), StringUtils.capitalize(cacheBeanName)))
					.getBeanDefinition();
			beanFactory.registerBeanDefinition(cacheBeanName, cacheDefinition);

			LogUtils.debug(getClass(), String.format("Registered Cache By Name: %s", cacheBeanName));
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Error creating NamedEntityCache definition for DTO '%s'", beanClass.getSimpleName()), e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getDtoCacheName(Class<? extends IdentityObject> dtoClass) {
		String cacheBeanName = AnnotationUtils.getAnnotation(dtoClass, CacheByName.class).cacheName();
		if (StringUtils.isEmpty(cacheBeanName)) {
			cacheBeanName = Introspector.decapitalize(dtoClass.getSimpleName() + "Cache");
		}
		return cacheBeanName;
	}
}
