package com.clifton.core.dataaccess.datatable;


import java.util.List;
import java.util.Map;


/**
 * The <code>DataRow</code> interface defines methods for working with data rows.
 *
 * @author vgomelsky
 */
public interface DataRow {

	/**
	 * Returns the value in the specified position. First column index is 0.
	 */
	public Object getValue(int columnIndex);


	/**
	 * Returns the value of the column with the specified name.
	 */
	public Object getValue(String columnName);


	/**
	 * Returns the value of the specified data column.
	 */
	public Object getValue(DataColumn column);


	/**
	 * Returns a map of objects where the key is the column header and the value is the row value
	 * for the given column. Keys with null values are not included in the map.
	 */
	public Map<String, Object> getRowValueMap(boolean includeNullValues);


	/**
	 * Returns a map of objects where the key is the column header and the value is the row value
	 * for the given column. Keys with null values are included in the map.
	 */
	public Map<String, Object> getRowValueMap();


	/**
	 * Returns a list of objects for the row values - this should be used when a dataTable had duplicate column names;
	 * using getRowValueMap will drop one of the columns otherwise.
	 */
	public Map<String, Object> getRowValueMap(List<String> columnNames, boolean includeNullValues);


	public List<Object> getRowValueList();


	public List<Object> getRowValueList(boolean includeNullValues);


	public List<Object> getRowValueList(List<String> columnNames, boolean includeNullValues);
}
