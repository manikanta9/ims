package com.clifton.core.dataaccess.datatable;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.converter.Converter;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;


/**
 * The <code>DataTableRetrievalHandler</code> defines methods for retrieving {@link DataTable} objects based on
 * specified SQL statements and parameters.
 *
 * @author vgomelsky
 */
public interface DataTableRetrievalHandler {


	/**
	 * Returns a {@link DataTable} using the argument {@link SearchConfigurer}.
	 */
	public DataTable findDataTable(SearchConfigurer<SqlSelectCommand> searchConfigurer);


	/**
	 * Returns a {@link DataTable} using the argument command
	 */
	@SecureMethod(disableSecurity = true)
	@RequestMapping("coreDataTableRetrievalFindDataTableByCommand")
	public DataTable findDataTable(SqlSelectCommand command);


	@DoNotAddRequestMapping
	public DataTable findDataTable(SqlSelectCommand command, Map<String, Converter<Object, Object>> columnConverterMap);


	/**
	 * Returns a {@link DataTable} returned by the specified query
	 */
	@SecureMethod(disableSecurity = true)
	@RequestMapping("coreDataTableRetrievalFindDataTableBySql")
	public DataTable findDataTable(String sql);


	@DoNotAddRequestMapping
	public DataTable findDataTable(String sql, Map<String, Converter<Object, Object>> columnConverterMap);


	/**
	 * Returns a {@link DataTable} using the specified arguments
	 *
	 * @param firstIndex the index of the first row to retrieve (first element index is 0)
	 * @param maxSize    maximum number of rows that the {@link DataTable} can hold
	 */
	@SecureMethod(disableSecurity = true)
	@RequestMapping("coreDataTableRetrievalFindDataTable")
	public DataTable findDataTable(String sql, SqlParameterValue[] sqlParameterValues, int firstIndex, int maxSize);


	/**
	 * Returns a {@link DataTable} using the specified arguments.
	 * Internal use only due to the exposure of the query timeout parameter.
	 *
	 * @param firstIndex the index of the first row to retrieve (first element index is 0)
	 * @param maxSize    maximum number of rows that the {@link DataTable} can hold
	 * @param timeout    query timeout in seconds
	 */
	@SecureMethod(disableSecurity = true)
	@RequestMapping("coreDataTableRetrievalFindDataTableWithTimeout")
	public DataTable findDataTable(String sql, SqlParameterValue[] sqlParameterValues, int firstIndex, int maxSize, int timeout);


	@DoNotAddRequestMapping
	public DataTable findDataTable(String sql, SqlParameterValue[] sqlParameterValues, int firstIndex, int maxSize, Map<String, Converter<Object, Object>> columnConverterMap);
}
