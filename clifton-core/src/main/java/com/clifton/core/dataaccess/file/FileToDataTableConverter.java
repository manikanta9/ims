package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.converter.Converter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;


/**
 * The <code>FileToDataTableConverter</code> interface defines the converter
 * for {@link File} or {@link MultipartFile} or {@link org.apache.poi.ss.usermodel.Workbook} objects to {@link DataTable} objects
 *
 * @author manderson
 */
public interface FileToDataTableConverter extends Converter<Object, DataTable> {

	// NOTHING HERE
}
