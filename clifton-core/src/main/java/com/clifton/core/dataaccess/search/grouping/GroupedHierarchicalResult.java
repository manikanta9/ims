package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A GroupedHierarchicalResult is a generic object that can be used to group on various properties of an entity and provide total counts or other aggregated values for each level.
 * All results are rolled up into the topmost level and each subsequent level is contained in the children property.
 * i.e. get results with 3 levels
 * Level 1: Value 1
 * + Level 2: Value 1-1
 * + + Level 3: Value 1-1-1
 * + + Level 3: Value 1-1-2
 * + Level 2: Value 1-2
 * + + Level 3: Value 1-2-1
 * Level 1: Value 2
 * + Level 2: Value 2-1
 * + + Level 3: Value 2-1-1
 * In the above example, 2 records are returned for the Level 1 values and the rest are included in the children property
 */
public class GroupedHierarchicalResult extends BaseGroupedResult {

	/**
	 * Just track the id of the parent to prevent recursion (since we send the children back to the UI)
	 */
	private Integer parentId;

	/**
	 * The grouping property for this record.
	 */
	private GroupingProperty groupProperty;

	/**
	 * The groupValue for this groupingProperty
	 * i.e. for an Order Allocation, this could be the orderDestination id
	 */
	private Object groupValue;

	/**
	 * The friendlier label for the groupValue
	 * i.e. for an Order Allocation and orderDestination id this is the name i.e. FX Connect
	 */
	private String groupLabel;


	/**
	 * The list of child grouped results
	 */
	private List<GroupedHierarchicalResult> children = new ArrayList<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public GroupedHierarchicalResult(GroupingProperty groupingProperty, Object groupValue) {
		this.groupProperty = groupingProperty;
		this.groupValue = groupValue;
	}


	public GroupedHierarchicalResult(GroupingProperty groupingProperty, Object groupValue, int count, Map<GroupingAggregateProperty, Number> aggregateValueMap) {
		super(count, aggregateValueMap);
		this.groupProperty = groupingProperty;
		this.groupValue = groupValue;
	}


	public String toStringFormatted(String currentTab) {
		StringBuilder result = new StringBuilder(20);

		result.append(currentTab);
		result.append("Grouping Property: ");
		result.append(getGroupProperty().getLabel());
		result.append(StringUtils.TAB);

		result.append("Value: ").append(getGroupValue());
		result.append(StringUtils.TAB);

		result.append("Label: ").append(getGroupValue() == null ? getGroupProperty().getEmptyValueLabel() : getGroupLabel());
		result.append(StringUtils.TAB);

		result.append(super.toStringFormatted());

		if (!CollectionUtils.isEmpty(getChildren())) {
			for (GroupedHierarchicalResult child : getChildren()) {
				result.append(StringUtils.NEW_LINE);
				result.append(child.toStringFormatted(currentTab + StringUtils.TAB));
			}
		}

		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void addChild(GroupedHierarchicalResult child) {
		getChildren().add(child);
	}


	@Override
	public int getTotalCount() {
		return MathUtils.getNumberAsInteger(getTotalCountAsBigDecimal());
	}


	private BigDecimal getTotalCountAsBigDecimal() {
		return MathUtils.add(CoreMathUtils.sumProperty(getChildren(), GroupedHierarchicalResult::getTotalCountAsBigDecimal), getCount());
	}


	@Override
	public Map<GroupingAggregateProperty, Number> getTotalAggregateValueMap() {
		if (isLeaf()) {
			return getAggregateValueMap();
		}

		Map<GroupingAggregateProperty, Number> result = new HashMap<>();
		if (!CollectionUtils.isEmpty(getChildren())) {
			for (GroupedHierarchicalResult child : getChildren()) {
				child.addToTotalAggregateValueMap(result);
			}
		}
		return result;
	}


	protected void addToTotalAggregateValueMap(Map<GroupingAggregateProperty, Number> result) {
		if (isLeaf()) {
			for (Map.Entry<GroupingAggregateProperty, Number> entry : getAggregateValueMap().entrySet()) {
				result.putIfAbsent(entry.getKey(), 0);
				// Note: This has only been implemented for simple additional, if adding new options like average, may need different logic (weighted averages?)
				result.put(entry.getKey(), MathUtils.add(MathUtils.getNumberAsBigDecimal(entry.getValue()), MathUtils.getNumberAsBigDecimal(result.get(entry.getKey()))));
			}
		}
		else {
			if (!CollectionUtils.isEmpty(getChildren())) {
				for (GroupedHierarchicalResult child : getChildren()) {
					child.addToTotalAggregateValueMap(result);
				}
			}
		}
	}


	public boolean isLeaf() {
		return CollectionUtils.isEmpty(getChildren());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getParentId() {
		return this.parentId;
	}


	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}


	public GroupingProperty getGroupProperty() {
		return this.groupProperty;
	}


	public void setGroupProperty(GroupingProperty groupProperty) {
		this.groupProperty = groupProperty;
	}


	public Object getGroupValue() {
		return this.groupValue;
	}


	public void setGroupValue(Object groupValue) {
		this.groupValue = groupValue;
	}


	public String getGroupLabel() {
		return this.groupLabel;
	}


	public void setGroupLabel(String groupLabel) {
		this.groupLabel = groupLabel;
	}


	public List<GroupedHierarchicalResult> getChildren() {
		return this.children;
	}


	public void setChildren(List<GroupedHierarchicalResult> children) {
		this.children = children;
	}
}
