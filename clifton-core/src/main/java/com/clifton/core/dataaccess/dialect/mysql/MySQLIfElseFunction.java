package com.clifton.core.dataaccess.dialect.mysql;

import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import org.hibernate.dialect.function.StandardSQLFunction;


/**
 * <code>MySQLIfElseFunction</code> is the MySQL implementation for rendering and an if else function.
 *
 * @author dillonm
 */
public class MySQLIfElseFunction extends StandardSQLFunction implements SQLIfElseFunction {

	public MySQLIfElseFunction() {
		super("IF");
	}
}
