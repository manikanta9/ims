package com.clifton.core.dataaccess.search.form;

import org.hibernate.Criteria;

import java.util.Date;


/**
 * The <code>DateRangeAwareSearchForm</code> interface declares methods for configuring date range restrictions on {@link RestrictiveSearchForm} search forms.
 * These methods may be used to perform custom logic in order to manually integrate date fields into the {@link Criteria} object.
 *
 * @author stevenf on 8/24/2016.
 */
public interface DateRangeAwareSearchForm extends RestrictiveSearchForm {


	public void configureDateRangeSearchForm(Criteria criteria);


	/**
	 * Returns if the start and end dates included in the date range include time or not.
	 */
	public boolean isIncludeTime();

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getActive();


	public void setActive(Boolean active);


	public Boolean getInactive();


	public void setInactive(Boolean inactive);


	public Date getActiveOnDate();


	public void setActiveOnDate(Date activeOnDate);


	public Date getNotActiveOnDate();


	public void setNotActiveOnDate(Date notActiveOnDate);


	public Date getActiveOnDateRangeStartDate();


	public void setActiveOnDateRangeStartDate(Date activeOnDateRangeStartDate);


	public Date getActiveOnDateRangeEndDate();


	public void setActiveOnDateRangeEndDate(Date activeOnDateRangeEndDate);


	public Date getNotActiveOnDateRangeStartDate();


	public void setNotActiveOnDateRangeStartDate(Date notActiveOnDateRangeStartDate);


	public Date getNotActiveOnDateRangeEndDate();


	public void setNotActiveOnDateRangeEndDate(Date notActiveOnDateRangeEndDate);


	public Date getEndDateRangeStartDate();


	public void setEndDateRangeStartDate(Date endDateRangeStartDate);


	public Date getEndDateRangeEndDate();


	public void setEndDateRangeEndDate(Date endDateRangeEndDate);
}
