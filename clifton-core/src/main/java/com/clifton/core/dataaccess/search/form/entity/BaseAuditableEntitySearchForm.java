package com.clifton.core.dataaccess.search.form.entity;


import com.clifton.core.dataaccess.search.SearchField;

import java.util.Date;


/**
 * The <code>BaseAuditableEntitySearchForm</code> class is a {@link BaseUpdatableEntitySearchForm} which includes search parameters for the each of the standard
 * audit fields for updatable and creatable entities.
 * <p>
 * If the searched entity includes either no audit fields or the updatable audit fields only, then the {@link BaseEntitySearchForm} or {@link
 * BaseUpdatableEntitySearchForm} should be used instead.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseUpdatableEntitySearchForm Parent fields}
 * <li>{@link #createDate}
 * <li>{@link #createUserId}
 * </ul>
 *
 * @author MikeH
 * @see BaseEntitySearchForm
 * @see BaseUpdatableEntitySearchForm
 */
public abstract class BaseAuditableEntitySearchForm extends BaseUpdatableEntitySearchForm implements EntityAuditFieldSearchForm {

	@SearchField(dateFieldIncludesTime = true)
	private Date createDate;

	@SearchField
	private Short createUserId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getCreateDate() {
		return this.createDate;
	}


	@Override
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}


	@Override
	public Short getCreateUserId() {
		return this.createUserId;
	}


	@Override
	public void setCreateUserId(Short createUserId) {
		this.createUserId = createUserId;
	}
}
