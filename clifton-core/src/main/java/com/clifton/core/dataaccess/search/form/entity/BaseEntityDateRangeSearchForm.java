package com.clifton.core.dataaccess.search.form.entity;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.DateRangeAwareSearchForm;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import org.hibernate.Criteria;

import java.util.Date;


/**
 * The <code>BaseEntityDateRangeSearchForm</code> is a {@link BaseEntitySearchForm} that can be used to search for resources which have start and end
 * dates. This class provides parameters for determining whether a resource is active on a particular date.
 * <p>
 * This class should be extended when searching DTOs which use the <tt>startDate</tt> and <tt>endDate</tt> fields.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseEntitySearchForm Parent fields}
 * <li>{@link #startDateOrNull}
 * <li>{@link #noEndDate}
 * <li>{@link #active}
 * <li>{@link #inactive}
 * <li>{@link #activeOnDate}
 * <li>{@link #notActiveOnDate}
 * <li>{@link #startOrEndDateNotNull}
 * </ul>
 *
 * @author NickK
 */
public abstract class BaseEntityDateRangeSearchForm extends BaseEntitySearchForm implements DateRangeAwareSearchForm {

	@SearchField(searchField = "startDate", comparisonConditions = {ComparisonConditions.EQUALS_OR_IS_NULL})
	private Date startDateOrNull;

	@SearchField(searchField = "endDate", comparisonConditions = {ComparisonConditions.IS_NULL})
	private Boolean noEndDate;

	// CUSTOM FILTER: If active == true, sets activeOnDate to today, if active == false, sets endDate to yesterday
	private Boolean active;

	//CUSTOM FILTER
	private Boolean inactive;

	// CUSTOM FILTER: (startDate IS NULL OR startDate >= activeOnDate) AND (endDate IS NULL OR endDate <= activeOnDate)
	private Date activeOnDate;

	// CUSTOM FILTER: (startDate IS NOT NULL AND startDate > activeOnDate) OR (endDate IS NOT NULL AND endDate < activeOnDate)
	private Date notActiveOnDate;

	@SearchField(searchField = "startDate,endDate", comparisonConditions = ComparisonConditions.IS_NOT_NULL)
	private Boolean startOrEndDateNotNull;

	// Active On Date Range Start/End should be used together
	// Uses (StartDate IS NULL OR StartDate <= ActiveOnDateRangeEndDate) AND (EndDate IS NULL OR EndDate >= ActiveOnDateRangeStartDate)
	private Date activeOnDateRangeStartDate;
	private Date activeOnDateRangeEndDate;


	// Not Active On Date Range Start/End should be used together
	// CUSTOM FILTER: (startDate IS NOT NULL AND startDate > NotActiveOnEndDate) OR (endDate IS NOT NULL AND endDate < NotActiveOnStartDate)
	private Date notActiveOnDateRangeStartDate;
	private Date notActiveOnDateRangeEndDate;

	// End Date Range Start/End should be used together
	// Uses  EndDate >= EndDateRangeStartDate AND EndDate <= EndDateRangeEndDate
	private Date endDateRangeStartDate;
	private Date endDateRangeEndDate;

	@Override
	public void configureDateRangeSearchForm(Criteria criteria) {
		SearchFormUtils.configureCriteriaForDateRangeSearchForm(this, criteria);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Boolean getNoEndDate() {
		return this.noEndDate;
	}


	public void setNoEndDate(Boolean noEndDate) {
		this.noEndDate = noEndDate;
	}


	@Override
	public Boolean getActive() {
		return this.active;
	}


	@Override
	public void setActive(Boolean active) {
		this.active = active;
	}


	@Override
	public Boolean getInactive() {
		return this.inactive;
	}


	@Override
	public void setInactive(Boolean inactive) {
		this.inactive = inactive;
	}


	@Override
	public Date getActiveOnDate() {
		return this.activeOnDate;
	}


	@Override
	public void setActiveOnDate(Date activeOnDate) {
		this.activeOnDate = activeOnDate;
	}


	@Override
	public Date getNotActiveOnDate() {
		return this.notActiveOnDate;
	}


	@Override
	public void setNotActiveOnDate(Date notActiveOnDate) {
		this.notActiveOnDate = notActiveOnDate;
	}


	@Override
	public Date getActiveOnDateRangeStartDate() {
		return this.activeOnDateRangeStartDate;
	}


	@Override
	public void setActiveOnDateRangeStartDate(Date activeOnDateRangeStartDate) {
		this.activeOnDateRangeStartDate = activeOnDateRangeStartDate;
	}


	@Override
	public Date getActiveOnDateRangeEndDate() {
		return this.activeOnDateRangeEndDate;
	}


	@Override
	public void setActiveOnDateRangeEndDate(Date activeOnDateRangeEndDate) {
		this.activeOnDateRangeEndDate = activeOnDateRangeEndDate;
	}


	@Override
	public Date getNotActiveOnDateRangeStartDate() {
		return this.notActiveOnDateRangeStartDate;
	}


	@Override
	public void setNotActiveOnDateRangeStartDate(Date notActiveOnDateRangeStartDate) {
		this.notActiveOnDateRangeStartDate = notActiveOnDateRangeStartDate;
	}


	@Override
	public Date getNotActiveOnDateRangeEndDate() {
		return this.notActiveOnDateRangeEndDate;
	}


	@Override
	public void setNotActiveOnDateRangeEndDate(Date notActiveOnDateRangeEndDate) {
		this.notActiveOnDateRangeEndDate = notActiveOnDateRangeEndDate;
	}


	@Override
	public Date getEndDateRangeStartDate() {
		return this.endDateRangeStartDate;
	}


	@Override
	public void setEndDateRangeStartDate(Date endDateRangeStartDate) {
		this.endDateRangeStartDate = endDateRangeStartDate;
	}


	@Override
	public Date getEndDateRangeEndDate() {
		return this.endDateRangeEndDate;
	}


	@Override
	public void setEndDateRangeEndDate(Date endDateRangeEndDate) {
		this.endDateRangeEndDate = endDateRangeEndDate;
	}


	public Boolean getStartOrEndDateNotNull() {
		return this.startOrEndDateNotNull;
	}


	public void setStartOrEndDateNotNull(Boolean startOrEndDateNotNull) {
		this.startOrEndDateNotNull = startOrEndDateNotNull;
	}


	public Date getStartDateOrNull() {
		return this.startDateOrNull;
	}


	public void setStartDateOrNull(Date startDateOrNull) {
		this.startDateOrNull = startDateOrNull;
	}
}
