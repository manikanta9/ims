package com.clifton.core.dataaccess.file.container;

import com.hierynomus.mssmb2.SMB2Dialect;
import com.hierynomus.smbj.SMBClient;
import com.hierynomus.smbj.SmbConfig;
import com.hierynomus.smbj.auth.AuthenticationContext;


/**
 * @author TerryS
 */
public class FileContainerContext implements AutoCloseable {

	private final SMBClient smbClient;
	private final AuthenticationContext authenticationContext;

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	FileContainerContext(AuthenticationContext authenticationContext) {
		this.smbClient = new SMBClient(getSmbConfig());
		this.authenticationContext = authenticationContext;
	}


	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	@Override
	public void close() {
		this.smbClient.close();
	}


	private SmbConfig getSmbConfig() {
		return SmbConfig.builder()
				.withDfsEnabled(true)
				.withDialects(SMB2Dialect.SMB_2_0_2, SMB2Dialect.SMB_2_1)
				.build();
	}

	/////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////


	public SMBClient getSmbClient() {
		return this.smbClient;
	}


	public AuthenticationContext getAuthenticationContext() {
		return this.authenticationContext;
	}
}
