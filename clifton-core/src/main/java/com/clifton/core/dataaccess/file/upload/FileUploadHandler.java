package com.clifton.core.dataaccess.file.upload;

import java.util.List;


/**
 * The <code>FileUploadHandler</code> interface defines methods for converting a file to a list of beans.
 * There are no database retrievals, just matches columns to properties and sets their values
 *
 * @author manderson
 */
public interface FileUploadHandler {

	/**
	 * Called by methods that use a an upload to handle converting the file - datatable - list of beans where the bean properties are populated by matching to the column name
	 */
	public <T> List<T> convertFileUploadFileToBeanList(FileUploadCommand<T> uploadCommand);
}
