package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.validation.ValidationException;

import java.io.Serializable;
import java.util.List;


/**
 * @author manderson
 */
public interface DaoSingleKeyListCache<T extends IdentityObject, K> {

	/**
	 * Returns the list of beans with the give key value.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 * Note: Does not enforce that the list is not empty, use strict method to require at least one entity exists.
	 */
	public List<T> getBeanListForKeyValue(ReadOnlyDAO<T> dao, K keyPropertyValue);


	/**
	 * Returns the list of beans with the give key value.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 *
	 * @throws ValidationException if the list is empty
	 */
	public List<T> getBeanListForKeyValueStrict(ReadOnlyDAO<T> dao, K keyPropertyValue);


	/**
	 * Returns the array of beans id's with the give key value.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 * Note: Does not enforce that the list is not empty.
	 */
	public Serializable[] getBeanIdListForKeyValue(ReadOnlyDAO<T> dao, K keyPropertyValue);


	/**
	 * Clears the cached list of beans with the give key value.
	 */
	public void clearBeanListForKeyValue(K keyPropertyValue);
}
