package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.db.SqlTypeUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.converter.Converter;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.sql.Types;


/**
 * The <code>DataColumnImpl</code> class provides default implementation for <code>DataColumn</code> interface.
 *
 * @author vgomelsky
 */
public class DataColumnImpl implements DataColumn {

	private String columnName;
	/**
	 * Column's SQL data type: <code>java.sql.Types</code>
	 */
	private final int dataType;

	private String style;
	private boolean hidden;

	/**
	 * Optional description.  Used by Excel Downloads to add a tooltip comment to headers if populated and different than the column name.
	 */
	private String description;

	private String[] validOptions = null;

	private Converter<Object, Object> columnValueConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * @param columnName database column name or alias
	 * @param dataType   Column's SQL data type: {@link java.sql.Types}
	 */
	@JsonCreator
	public DataColumnImpl(@JsonProperty("columnName") String columnName, @JsonProperty("dataType") int dataType) {
		this(columnName, dataType, null);
	}


	public DataColumnImpl(String columnName, int dataType, String description) {
		this(columnName, dataType, description, null);
	}


	/**
	 * @param columnName  database column name or alias
	 * @param dataType    Column's SQL data type: {@link java.sql.Types}
	 * @param description column description (can be used for tooltip)
	 */
	public DataColumnImpl(String columnName, int dataType, String description, Converter<Object, Object> columnValueConverter) {
		this.columnName = columnName;
		this.dataType = dataType;
		this.description = description;
		this.columnValueConverter = columnValueConverter;

		DataTypes dt = DataTypes.NAME;
		if (Types.INTEGER == dataType) {
			dt = DataTypes.INT;
		}
		else if (Types.SMALLINT == dataType) {
			dt = DataTypes.SHORT;
		}
		else if (Types.BIGINT == dataType) {
			dt = DataTypes.LONG;
		}
		else if (Types.DECIMAL == dataType) {
			dt = DataTypes.DECIMAL_PRECISE;
		}
		else if (Types.BIT == dataType) {
			dt = DataTypes.BIT;
		}
		else if (Types.TIMESTAMP == dataType) {
			dt = DataTypes.DATETIME;
		}
		else if (Types.DATE == dataType) {
			dt = DataTypes.DATE;
		}
		this.style = "dataType=" + dt.name();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public String toString() {
		return getColumnName() + ": " + getStyle();
	}

	@Override
	public String getDataTypeName() {
		return SqlTypeUtils.getDataTypeNameForSqlType(getDataType());
	}


	@Override
	public String getColumnName() {
		return this.columnName;
	}


	@Override
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	@Override
	public int getDataType() {
		return this.dataType;
	}


	@Override
	public String getStyle() {
		return this.style;
	}


	@Override
	public void setStyle(String style) {
		this.style = style;
	}


	@Override
	public boolean isHidden() {
		return this.hidden;
	}


	@Override
	public void setHidden(boolean hidden) {
		this.hidden = hidden;
	}


	@Override
	public String getDescription() {
		return this.description;
	}


	@Override
	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String[] getValidOptions() {
		return this.validOptions;
	}


	@Override
	public void setValidOptions(String[] validOptions) {
		this.validOptions = validOptions;
	}


	@Override
	public Converter<Object, Object> getColumnValueConverter() {
		return this.columnValueConverter;
	}


	@Override
	public void setColumnValueConverter(Converter<Object, Object> columnValueConverter) {
		this.columnValueConverter = columnValueConverter;
	}
}
