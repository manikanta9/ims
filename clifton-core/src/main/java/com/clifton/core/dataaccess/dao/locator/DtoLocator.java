package com.clifton.core.dataaccess.dao.locator;

import com.clifton.core.beans.IdentityObject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The DtoLocator annotation is used on interfaces to identify corresponding DTO class that implements this interface
 * and is persisted in the database.  It can be used to lookup corresponding class that can be used during deserialization.
 *
 * @author vgomelsky
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DtoLocator {

	Class<? extends IdentityObject> dtoClass();
}
