package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.Map;
import java.util.Set;


/**
 * The <code>DaoEventRegistrator</code> class is a {@link ApplicationListener} that registers configured
 * DAO event observer for the DAO's with matching names for the specified events.
 * <p>
 * Registration is done after application context is refreshed (all beans are loaded).
 * <p>
 * It should be used to register observers (listeners) for specific DAO beans and events.
 *
 * @param <T>
 * @author vgomelsky
 */
public class DaoEventRegistrator<T extends IdentityObject> implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	/**
	 * A list of DAOs that this DAO Observer should be registered for
	 */
	private Set<String> daoNameList;
	/**
	 * A list of Class names that the dao bean's should be an instance of
	 * to determine which DAOs this DAO Observer should be registered for.
	 */
	private Set<String> beanInstanceOfList;

	private DaoEventTypes daoEvents = DaoEventTypes.MODIFY;
	private DaoEventObserver<T> daoEventObserver;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings({"unchecked", "rawtypes"})
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		ApplicationContext eventContext = getApplicationContext();

		// Register Explicit DAOs
		for (String beanName : CollectionUtils.getIterable(getDaoNameList())) {
			ObserverableDAO<T> dao = (ObserverableDAO<T>) eventContext.getBean(beanName);
			registerDao(dao);
		}

		// Lookup DAOs for beans instance of specified classes
		for (String className : CollectionUtils.getIterable(getBeanInstanceOfList())) {
			Class clazz = CoreClassUtils.getClass(className);
			Map<String, UpdatableDAO> beanMap = eventContext.getBeansOfType(UpdatableDAO.class);
			for (Map.Entry<String, UpdatableDAO> stringUpdatableDAOEntry : beanMap.entrySet()) {
				UpdatableDAO<T> updatableDAO = (UpdatableDAO<T>) stringUpdatableDAOEntry.getValue();
				// register only for DAO's that manage WorkflowableEntity DTO's
				if (updatableDAO.getConfiguration() != null && clazz.isAssignableFrom(updatableDAO.getConfiguration().getBeanClass())) {
					ObserverableDAO<T> dao = (ObserverableDAO<T>) updatableDAO;
					registerDao(dao);
				}
			}
		}
	}


	private void registerDao(ObserverableDAO<T> dao) {
		if (getDaoEvents().isRead()) {
			dao.registerEventObserver(DaoEventTypes.READ, getDaoEventObserver());
		}
		if (getDaoEvents().isInsert()) {
			dao.registerEventObserver(DaoEventTypes.INSERT, getDaoEventObserver());
		}
		if (getDaoEvents().isUpdate()) {
			dao.registerEventObserver(DaoEventTypes.UPDATE, getDaoEventObserver());
		}
		if (getDaoEvents().isDelete()) {
			dao.registerEventObserver(DaoEventTypes.DELETE, getDaoEventObserver());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public Set<String> getDaoNameList() {
		return this.daoNameList;
	}


	public void setDaoNameList(Set<String> daoNameList) {
		this.daoNameList = daoNameList;
	}


	public DaoEventTypes getDaoEvents() {
		return this.daoEvents;
	}


	public void setDaoEvents(DaoEventTypes daoEvents) {
		this.daoEvents = daoEvents;
	}


	public DaoEventObserver<T> getDaoEventObserver() {
		return this.daoEventObserver;
	}


	public void setDaoEventObserver(DaoEventObserver<T> daoEventObserver) {
		this.daoEventObserver = daoEventObserver;
	}


	public Set<String> getBeanInstanceOfList() {
		return this.beanInstanceOfList;
	}


	public void setBeanInstanceOfList(Set<String> beanInstanceOfList) {
		this.beanInstanceOfList = beanInstanceOfList;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
