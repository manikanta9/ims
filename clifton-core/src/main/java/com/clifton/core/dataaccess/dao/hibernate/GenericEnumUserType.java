package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.util.compare.CompareUtils;
import org.hibernate.HibernateException;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Comparator;
import java.util.Properties;


/**
 * The <code>GenericEnumUserType</code> converts an Enum to a string for storing in the database.
 *
 * @author mwacker
 */
public class GenericEnumUserType implements UserType, ParameterizedType, Comparator<GenericEnumUserType> {

	private static final int[] SQL_TYPES = {Types.NVARCHAR};
	@SuppressWarnings("rawtypes")
	private Class<? extends Enum> clazz;


	@Override
	public void setParameterValues(Properties parameters) {
		String typeDefinitionClassName = parameters.getProperty("typeDefinitionClassName");
		try {
			this.clazz = Class.forName(typeDefinitionClassName).asSubclass(Enum.class);
		}
		catch (ClassNotFoundException e) {
			throw new HibernateException("Enum class not found", e);
		}
	}


	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}


	@Override
	public Class<?> returnedClass() {
		return this.clazz;
	}


	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, @SuppressWarnings("unused") SharedSessionContractImplementor session, @SuppressWarnings("unused") Object owner) throws HibernateException,
			SQLException {
		String name = resultSet.getString(names[0]);
		Object result = null;
		if (!resultSet.wasNull()) {
			result = Enum.valueOf(this.clazz, name);
		}
		return result;
	}


	@Override
	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, @SuppressWarnings("unused") SharedSessionContractImplementor session) throws HibernateException, SQLException {
		if (null == value) {
			preparedStatement.setNull(index, Types.VARCHAR);
		}
		else if (value instanceof String) {
			preparedStatement.setString(index, (String) value);
		}
		else {
			preparedStatement.setString(index, ((Enum<?>) value).name());
		}
	}


	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}


	@Override
	public boolean isMutable() {
		return false;
	}


	@Override
	public Object assemble(Serializable cached, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return cached;
	}


	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}


	@Override
	public Object replace(Object original, @SuppressWarnings("unused") Object target, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return original;
	}


	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}


	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		if (x == y) {
			return true;
		}
		if ((null == x) || (null == y)) {
			return false;
		}
		return x.equals(y);
	}


	@Override
	public int compare(GenericEnumUserType o1, GenericEnumUserType o2) {
		return CompareUtils.compare(o1, o2);
	}
}
