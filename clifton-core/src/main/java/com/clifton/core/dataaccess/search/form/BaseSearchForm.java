package com.clifton.core.dataaccess.search.form;


import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.search.SearchConfigurer;

import java.io.Serializable;


/**
 * The <code>BaseSearchForm</code> class is the base class from which all search forms inherit. This class provides standard query detail fields which may be
 * used by any query.
 * <p>
 * Search forms provide query information when performing searches via a {@link SearchConfigurer}. These forms may provide any details that may be used by the
 * query, such as property values, patterns, or start and end rows for paging.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link #start}
 * <li>{@link #limit}
 * <li>{@link #readUncommittedRequested}
 * </ul>
 *
 * @author vgomelsky
 */
@NonPersistentObject
@SearchForm
public abstract class BaseSearchForm implements ValidatingSearchForm, PagingCommand, Serializable {

	/**
	 * Allows for turning off row count retrieval, specifically used for mongo where a separate query is executed to get the count.
	 * The count is unnecessary to retrieve in many cases such as internal calls and grid downloads in excel/csv.
	 */
	private boolean retrieveRowCount = true;

	private int start = DEFAULT_START;
	private int limit = DEFAULT_LIMIT;
	private Integer maxLimit;

	/**
	 * Optionally request READ_UNCOMMITTED transaction isolation level.
	 * NOTE: avoid doing this unless absolutely necessary.
	 */
	private boolean readUncommittedRequested = false;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isFilterRequired() {
		// default implementation does not require a single filter.
		return false;
	}


	@Override
	public void validate() {
		// NO CUSTOM VALIDATION
	}


	@Override
	public int getStart() {
		return this.start;
	}


	public void setStart(int start) {
		this.start = start;
	}


	@Override
	public int getLimit() {
		return this.limit;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}


	@Override
	public Integer getMaxLimit() {
		return this.maxLimit;
	}


	public void setMaxLimit(Integer maxLimit) {
		this.maxLimit = maxLimit;
	}


	public boolean isReadUncommittedRequested() {
		return this.readUncommittedRequested;
	}


	public void setReadUncommittedRequested(boolean readUncommittedRequested) {
		this.readUncommittedRequested = readUncommittedRequested;
	}


	public boolean isRetrieveRowCount() {
		return this.retrieveRowCount;
	}


	public void setRetrieveRowCount(boolean retrieveRowCount) {
		this.retrieveRowCount = retrieveRowCount;
	}
}
