package com.clifton.core.dataaccess.search.form;


import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>ValidatingSearchForm</code> interface declares methods for search form validation. These methods are used during query preparation to validate the
 * search form.
 *
 * @author vgomelsky
 */
public interface ValidatingSearchForm {

	/**
	 * Specifies whether the search form requires at least one populated filter property.
	 *
	 * @return {@code true} if this search form is marked as requiring at least one filter is required, or {@code false} otherwise
	 */
	public boolean isFilterRequired();


	/**
	 * Performs additional search form property validation. This may be used for complex validation logic involving multiple fields, for example.
	 *
	 * @throws ValidationException on validation failure
	 */
	public void validate() throws ValidationException;
}
