package com.clifton.core.dataaccess.migrate.schema;


/**
 * The <code>Link</code> class defines links between {@link Table} objects.
 * A link is usually represented by a {@link java.util.List} or {@link java.util.Set} on a DTO class.
 * A link can be "many-to-one" or a "many-to-many" (when the many to many properties are set).
 * <p/>
 * By default links are not "real", meaning that there is no corresponding property on the DTO class (used for SQL navigation only).
 *
 * @author vgomelsky
 */
public class Link {

	private String name;
	private String referencingClass;
	private String referencingColumn;

	private String manyToManyTable;
	private String manyToManyColumn;

	private boolean real;

	/**
	 * Determine the behavior for cascading operations to the child table.  By default nothing will cascade to the children.
	 * Available options are "all|none|save-update|delete|all-delete-orphan|delete-orphan".
	 */
	private String cascade;


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the referencingClass
	 */
	public String getReferencingClass() {
		return this.referencingClass;
	}


	/**
	 * @param referencingClass the referencingClass to set
	 */
	public void setReferencingClass(String referencingClass) {
		this.referencingClass = referencingClass;
	}


	/**
	 * @return the referencingColumn
	 */
	public String getReferencingColumn() {
		return this.referencingColumn;
	}


	/**
	 * @param referencingColumn the referencingColumn to set
	 */
	public void setReferencingColumn(String referencingColumn) {
		this.referencingColumn = referencingColumn;
	}


	/**
	 * @return the manyToManyTable
	 */
	public String getManyToManyTable() {
		return this.manyToManyTable;
	}


	/**
	 * @param manyToManyTable the manyToManyTable to set
	 */
	public void setManyToManyTable(String manyToManyTable) {
		this.manyToManyTable = manyToManyTable;
	}


	/**
	 * @return the manyToManyColumn
	 */
	public String getManyToManyColumn() {
		return this.manyToManyColumn;
	}


	/**
	 * @param manyToManyColumn the manyToManyColumn to set
	 */
	public void setManyToManyColumn(String manyToManyColumn) {
		this.manyToManyColumn = manyToManyColumn;
	}


	/**
	 * @return the real
	 */
	public boolean isReal() {
		return this.real;
	}


	/**
	 * @param real the real to set
	 */
	public void setReal(boolean real) {
		this.real = real;
	}


	public String getCascade() {
		return this.cascade;
	}


	public void setCascade(String cascade) {
		this.cascade = cascade;
	}
}
