package com.clifton.core.dataaccess.sql;

/**
 * The <code>SqlHandlerLocator</code> interface defines method(s) for locating data source specific instances of {@link SqlHandlerLocator}.
 *
 * @author vgomelsky
 */
public interface SqlHandlerLocator {

	/**
	 * Locates and returns SqlHandler instance for the specified data source name.
	 */
	public SqlHandler locate(String dataSourceName);
}
