package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Component;


/**
 * The <code>MSSQLSchemaPreDDLSQLConverter</code> class generates SQL statements from DATA or SQL
 * that use runWith="PRE_DDL" to be run first before anything else.
 *
 * @author manderson
 */
@Component
public class MSSQLSchemaPreDDLSQLConverter implements MigrationSchemaConverter {

	/**
	 * Data Migration Converter that runs Data SQL that is marked
	 * to runWith=PRE_DDL
	 */
	private MigrationSchemaConverter dataMigrationConverter;

	/**
	 * SQL Migration Converter that runs SQL SQL that is marked
	 * to runWith=PRE_DDL
	 */
	private MigrationSchemaConverter sqlMigrationConverter;


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.PRE_DDL;
	}


	@Override
	public String convert(Schema schema) {
		if (schema.getExcludeFromSql() != null && schema.getExcludeFromSql()) {
			return "";
		}
		StringBuilder sql = new StringBuilder(1024);
		appendRunWithSql(sql, schema);
		return sql.toString();
	}


	private void appendRunWithSql(StringBuilder sql, Schema schema) {
		// Append Data Migrations that are RUN_WITH PRE_DDL
		String batchSep = getMigrationSchemaConverterType().getBatchSeparator();
		String appendBatchSep = getDataMigrationConverter().getMigrationSchemaConverterType().getBatchSeparator();
		String appendSql = getDataMigrationConverter().convert(schema);
		if (!StringUtils.isEmpty(appendSql) && !StringUtils.isEmpty(appendBatchSep) && !appendBatchSep.equalsIgnoreCase(batchSep)) {
			appendSql = appendSql.replaceAll(appendBatchSep, batchSep + StringUtils.NEW_LINE);
		}
		sql.append(appendSql);

		// Append SQL Migrations that are RUN_WITH PRE_DDL
		appendBatchSep = getSqlMigrationConverter().getMigrationSchemaConverterType().getBatchSeparator();
		appendSql = getSqlMigrationConverter().convert(schema);
		if (!StringUtils.isEmpty(appendSql) && !StringUtils.isEmpty(appendBatchSep) && !appendBatchSep.equalsIgnoreCase(batchSep)) {
			appendSql = appendSql.replaceAll(appendBatchSep, batchSep + StringUtils.NEW_LINE);
		}
		sql.append(appendSql);
	}


	public MigrationSchemaConverter getDataMigrationConverter() {
		return this.dataMigrationConverter;
	}


	public void setDataMigrationConverter(MigrationSchemaConverter dataMigrationConverter) {
		this.dataMigrationConverter = dataMigrationConverter;
	}


	public MigrationSchemaConverter getSqlMigrationConverter() {
		return this.sqlMigrationConverter;
	}


	public void setSqlMigrationConverter(MigrationSchemaConverter sqlMigrationConverter) {
		this.sqlMigrationConverter = sqlMigrationConverter;
	}
}
