package com.clifton.core.dataaccess.dialect.mssql;

import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import org.hibernate.dialect.function.StandardSQLFunction;


/**
 * <code>MSSQLIfNullFunction</code> is the MSSQL implementation for rendering and an if null function.
 *
 * @author dillonm
 */
public class MSSQLIfNullFunction extends StandardSQLFunction implements SQLIfNullFunction {

	public MSSQLIfNullFunction() {
		super("ISNULL");
	}
}
