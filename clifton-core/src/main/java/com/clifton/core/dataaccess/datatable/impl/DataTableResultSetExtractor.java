package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.PagingDataTable;
import com.clifton.core.util.converter.Converter;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>DataTableResultSetExtractor</code> class implements <code>ResultSetExtractor</code> call back interface and
 * populates a <code>DataTable</code> from a <code>ResultSet</code>.
 *
 * @author vgomelsky
 */
public class DataTableResultSetExtractor implements ResultSetExtractor<DataTable> {

	private final int firstRowIndex;
	private final int maxRowsToRetrieve;
	private final Map<String, Converter<Object, Object>> converterMap;


	public DataTableResultSetExtractor(int firstIndex, int maxRowsToRetrieve, Map<String, Converter<Object, Object>> converterMap) {
		this.firstRowIndex = firstIndex;
		this.maxRowsToRetrieve = maxRowsToRetrieve;
		this.converterMap = converterMap;
	}


	@Override
	public DataTable extractData(ResultSet resultSet) throws SQLException, DataAccessException {
		DataColumn[] dataColumns = extractDataColumns(resultSet.getMetaData());

		int rowIndex = 1;
		if (getFirstRowIndex() != PagingCommand.DEFAULT_START) {
			// scroll to the first requested element(works with ResultSet.TYPE_SCROLL_INSENSITIVE)
			resultSet.absolute(getFirstRowIndex() + 1);
			rowIndex = resultSet.getRow();
		}
		else {
			// check if resultSet is empty
			if (!resultSet.next()) {
				rowIndex = 0;
			}
		}

		// if no rows left to be retrieved, return an empty table
		if (rowIndex == 0) {
			return new PagingDataTableImpl(dataColumns);
		}
		else if (rowIndex != (getFirstRowIndex() + 1)) {
			return new PagingDataTableImpl(dataColumns, rowIndex, rowIndex);
		}

		return extractDataRows(resultSet, dataColumns, rowIndex);
	}


	/**
	 * Returns an array of <code>DataColumn</code> objects where each array element corresponds to a column in the
	 * <code>ResultSet</code>
	 *
	 * @param resultSetMetaData
	 */
	private DataColumn[] extractDataColumns(ResultSetMetaData resultSetMetaData) throws SQLException {
		int columnCount = resultSetMetaData.getColumnCount();
		if (columnCount == 0) {
			return null;
		}
		DataColumn[] dataColumns = new DataColumn[columnCount];
		for (int i = 1; i <= columnCount; i++) {
			String columnName = resultSetMetaData.getColumnLabel(i);
			int type = resultSetMetaData.getColumnType(i);

			// Special Handling for "DATE" fields that are returned as String Size of 10
			// Convert to use Date
			if (columnName.endsWith("Date") && Types.VARCHAR == type && resultSetMetaData.getColumnDisplaySize(i) == 10) {
				type = Types.DATE;
			}
			dataColumns[i - 1] = new DataColumnImpl(columnName, type, null, getConverterMap().get(columnName));
		}
		return dataColumns;
	}


	/**
	 * Returns a <code>DataTable</code> with rows populated from the argument <code>ResultSet</code>
	 *
	 * @param resultSet
	 * @param dataColumns
	 * @param rowIndex
	 */
	private DataTable extractDataRows(ResultSet resultSet, DataColumn[] dataColumns, int rowIndex) throws SQLException {
		PagingDataTable dataTable = new PagingDataTableImpl(dataColumns, getFirstRowIndex(), getMaxRowsToRetrieve());
		ResultSetToDataRowConverter rowConverter = new ResultSetToDataRowConverter(dataTable);

		int lastIndex = getMaxRowsToRetrieve();
		if (lastIndex != PagingCommand.DEFAULT_LIMIT) {
			lastIndex += getFirstRowIndex();
		}
		// get up to maxRowsToRetrieve elements in the PageList
		if (getMaxRowsToRetrieve() != 0) {
			DataRow row = rowConverter.convert(resultSet);
			dataTable.addRow(row);
			while (rowIndex < lastIndex && resultSet.next()) {
				rowIndex++;
				row = rowConverter.convert(resultSet);
				dataTable.addRow(row);
			}
		}

		// scroll through remaining rows if any
		if (rowIndex == lastIndex) {
			resultSet.last();
			rowIndex = resultSet.getRow();
		}
		if (getMaxRowsToRetrieve() == PagingCommand.DEFAULT_LIMIT) {
			dataTable.setPageSize(rowIndex - getFirstRowIndex() + 2);
		}
		dataTable.setTotalRowCount(rowIndex);
		return dataTable;
	}


	public int getFirstRowIndex() {
		return this.firstRowIndex;
	}


	public int getMaxRowsToRetrieve() {
		return this.maxRowsToRetrieve;
	}


	public Map<String, Converter<Object, Object>> getConverterMap() {
		return this.converterMap != null ? this.converterMap : new HashMap<>();
	}
}
