package com.clifton.core.dataaccess.file;


import org.springframework.web.multipart.MultipartFile;


/**
 * The <code>FileUploadWrapper</code> used to upload files from drag and drop with having to create a custom object for uploading each time.
 *
 * @author mwacker
 */
public class FileUploadWrapper {

	private MultipartFile file;


	public MultipartFile getFile() {
		return this.file;
	}


	public void setFile(MultipartFile file) {
		this.file = file;
	}
}
