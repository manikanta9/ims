package com.clifton.core.dataaccess.dao.xml;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Link;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.dataaccess.search.hibernate.expression.AbstractInExpressionForNumbers;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import org.hibernate.CacheMode;
import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.ScrollMode;
import org.hibernate.ScrollableResults;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Disjunction;
import org.hibernate.criterion.ExistsSubqueryExpression;
import org.hibernate.criterion.InExpression;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.NotNullExpression;
import org.hibernate.criterion.NullExpression;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.PropertyExpression;
import org.hibernate.criterion.SimpleExpression;
import org.hibernate.sql.JoinType;
import org.hibernate.transform.ResultTransformer;
import org.springframework.context.ApplicationContext;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>XmlBasedCriteria</code> class is implements Hibernate's {@link Criteria} interface.
 * Implementation is based on schema defined in Spring's context.
 *
 * @param <T>
 * @author vgomelsky
 */
@SuppressWarnings("unused")
public class XmlBasedCriteria<T extends IdentityObject> implements Criteria {

	/**
	 * The collection of supported operators search form conditions.
	 */
	private static final Collection<String> SUPPORTED_OPERATORS = CollectionUtils.createHashSet("=", "<>", "IN", "NOT IN", "LIKE", ">=", "<=", ">", "<");

	private final ApplicationContext applicationContext;
	private final DAOConfiguration<T> configuration;
	private final List<Join> joinList = new ArrayList<>();
	private final List<Criterion> restrictionList = new ArrayList<>();
	/**
	 * Specifies whether instances of {@link ExistsSubqueryExpression} should evaluate to true or false.
	 */
	private final boolean existsEvaluatesToTrue;
	/**
	 * Specifies whether instances of {@link LogicalExpression} should evaluate to true or false
	 */
	private final boolean logicalEvaluatesToTrue;
	/**
	 * Specifies that criterion using joins added to this criteria will be ignored. This implementation can have severe performance
	 * issues if joins to large tables are used because all entities of the joined table are loaded and used to filter.
	 */
	private final boolean ignoreJoins;
	private List<XmlBasedCriteria<T>> subCriteriaList = new ArrayList<>();
	/**
	 * Criteria path used as the property path when this criteria is a sub-criteria.
	 */
	private String criteriaPath;
	/**
	 * Criteria alias used as the property path when this criteria is a sub-criteria.
	 */
	private String criteriaAlias;
	private List<String> orderProperties;
	private List<Boolean> orderAscending;
	private Integer maxResults = null;


	public XmlBasedCriteria(DAOConfiguration<T> configuration) {
		this(configuration, null, true, true);
	}


	public XmlBasedCriteria(DAOConfiguration<T> configuration, ApplicationContext applicationContext, boolean existsEvaluatesToTrue, boolean logicalEvaluatesToTrue) {
		this(configuration, applicationContext, existsEvaluatesToTrue, logicalEvaluatesToTrue, false);
	}


	public XmlBasedCriteria(DAOConfiguration<T> configuration, ApplicationContext applicationContext, boolean existsEvaluatesToTrue, boolean logicalEvaluatesToTrue, boolean ignoreJoins) {
		this.configuration = configuration;
		this.applicationContext = applicationContext;
		this.existsEvaluatesToTrue = existsEvaluatesToTrue;
		this.logicalEvaluatesToTrue = logicalEvaluatesToTrue;
		this.ignoreJoins = ignoreJoins;
	}


	private DaoLocator getDaoLocator() {
		return (DaoLocator) this.applicationContext.getBean("daoLocator");
	}


	@Override
	public Criteria add(Criterion criterion) {
		this.restrictionList.add(criterion);
		return this;
	}


	@Override
	public Criteria createAlias(String associationPath, String alias) throws HibernateException {
		Join join = getAssociationPathJoin(this.configuration, associationPath, alias);
		if (join != null) {
			this.joinList.add(join);
			return this;
		}


		String[] associations = associationPath.split("\\.");
		if (associations.length > 1) {
			// Look at the last segment for the property on the alias of the second to last segment. Previous segments would have already been processed
			String associationAlias = associations[associations.length - 2];
			String associationAliasPath = associations[associations.length - 1];
			for (Join associatedJoin : this.joinList) {
				if (associatedJoin.getAlias().equals(associationAlias)) {
					String associationClass = associatedJoin.getColumn() != null ? associatedJoin.getColumn().getFkTable() : associatedJoin.getLink().getReferencingClass();
					DAOConfiguration<?> associationConfiguration = getDaoLocator().locate(associationClass).getConfiguration();
					if (associationConfiguration != null) {
						Join nestedAssociatedJoin = getAssociationPathJoin(associationConfiguration, associationAliasPath, alias);
						if (nestedAssociatedJoin != null) {
							this.joinList.add(nestedAssociatedJoin);
							return this;
						}
					}
				}
			}
		}
		throw new IllegalArgumentException("Cannot find a link or foreign key for path '" + associationPath + "' from table '" + this.configuration.getTableName() + "'");
	}


	private Join getAssociationPathJoin(DAOConfiguration<?> daoConfiguration, String associationPath, String alias) {
		// find the link and store it with the alias
		for (Link link : CollectionUtils.getIterable(daoConfiguration.getTable().getLinkList())) {
			if (link.getName().equals(associationPath)) {
				return new Join(link, alias);
			}
		}
		// check for foreign key relationship
		for (Column column : daoConfiguration.getTable().getColumnList()) {
			if (column.getBeanPropertyName().equals(associationPath)) {
				return new Join(column, alias);
			}
		}
		return null;
	}


	@Override
	public Criteria createCriteria(String associationPath, String alias) throws HibernateException {
		Class<T> propertyType = getPropertyType(associationPath);
		if (propertyType == null) {
			return null;
		}
		XmlBasedCriteria<T> subCriteria = new XmlBasedCriteria<>(getDaoLocator().locate(propertyType).getConfiguration(), this.applicationContext, this.existsEvaluatesToTrue, this.logicalEvaluatesToTrue, this.ignoreJoins);
		subCriteria.criteriaPath = associationPath;
		subCriteria.criteriaAlias = alias;
		this.subCriteriaList.add(subCriteria);
		return subCriteria;
	}


	@SuppressWarnings("unchecked")
	private Class<T> getPropertyType(String associationPath) {
		StringBuilder error = new StringBuilder(16);
		try {
			return (Class<T>) BeanUtils.getPropertyType(this.configuration.getBeanClass(), associationPath);
		}
		catch (Exception e) {
			error.append(e.getMessage());
			// For cases where subclasses are used - check if the property is on one of those subclasses
			for (Subclass subClass : CollectionUtils.getIterable(this.configuration.getTable().getSubclassList())) {
				try {
					return (Class<T>) BeanUtils.getPropertyType(CoreClassUtils.getClass(subClass.getDtoClass()), associationPath);
				}
				catch (Exception e2) {
					error.append(e2.getMessage());
				}
			}
		}
		if (this.ignoreJoins) {
			return null;
		}
		throw new RuntimeException("Error determining property type for path [" + associationPath + "]: " + error.toString());
	}


	@Override
	@SuppressWarnings({"rawtypes"})
	public List list() throws HibernateException {
		// I. get full list of results
		ReadOnlyDAO<T> dao = getDaoLocator().locate(this.configuration.getBeanClass());
		List<T> allRecords = dao.findAll();
		return list(allRecords);
	}


	/**
	 * Returns a list of records filtered based on configured criteria.
	 * <p>
	 * Criterion using joins can have a negative affect on performance as each joined table is listed in its entirety for filtering.
	 * For production use cases, it is preferred to use a XML based criteria created with {@link #ignoreJoins} set to true.
	 * <p>
	 * Returns and empty ArrayList if there is nothing found to simulate non-null results to that of the Hibernate implementation.
	 * ArrayList is used over {#link Collections.emptyList()} because in rare usages, returned lists are modified.
	 */
	@SuppressWarnings({"unchecked", "rawtypes"})
	public List list(List<T> allRecords) throws HibernateException {
		ReadOnlyDAO<T> dao = (this.applicationContext == null) ? null : getDaoLocator().locate(this.configuration.getBeanClass());
		if (CollectionUtils.isEmpty(allRecords)) {
			return new ArrayList();
		}
		List<T> result = new ArrayList<>(allRecords);

		// II. apply one restriction at a time to narrow results
		for (Criterion criterion : this.restrictionList) {
			String propertyName;
			Object propertyValue = null;
			String operator = "=";
			boolean matchIfNotEquals = false;
			if (criterion instanceof SimpleExpression) {
				// get private field values
				propertyName = BeanUtils.getFieldValue(criterion, "propertyName");
				propertyValue = BeanUtils.getFieldValue(criterion, "value");
				operator = BeanUtils.getFieldValue(criterion, "op");
			}
			else if (criterion instanceof NullExpression) {
				propertyName = BeanUtils.getFieldValue(criterion, "propertyName");
			}
			else if (criterion instanceof NotNullExpression) {
				propertyName = BeanUtils.getFieldValue(criterion, "propertyName");
				operator = "<>";
				matchIfNotEquals = true;
			}
			else if (criterion instanceof ExistsSubqueryExpression) {
				// NOTE: not a real implementation but can be predefined in tests
				if (this.existsEvaluatesToTrue) {
					continue;
				}
				return new ArrayList();
			}
			else if (criterion instanceof LogicalExpression || criterion instanceof Disjunction) {
				// NOTE: not a real implementation but can be predefined in tests
				// One Or = LogicalExpression, Multiple Ors = Disjunction so treat them the same way
				if (this.logicalEvaluatesToTrue) {
					continue;
				}
				return new ArrayList();
			}
			else if (criterion instanceof InExpression) {
				propertyName = BeanUtils.getFieldValue(criterion, "propertyName");
				operator = "IN";
				propertyValue = BeanUtils.getFieldValue(criterion, "values");
			}
			else if (criterion instanceof AbstractInExpressionForNumbers) {
				try {
					// Retrieve private property field references
					Field propertyNameField = AbstractInExpressionForNumbers.class.getDeclaredField("propertyName");
					Field comparisonField = AbstractInExpressionForNumbers.class.getDeclaredField("comparisonExpression");
					Field valuesField = AbstractInExpressionForNumbers.class.getDeclaredField("values");
					propertyName = BeanUtils.getFieldValue(criterion, propertyNameField);
					operator = BeanUtils.getFieldValue(criterion, comparisonField);
					propertyValue = BeanUtils.getFieldValue(criterion, valuesField);
				}
				catch (NoSuchFieldException e) {
					throw new RuntimeException("An unexpected error occurred while attempting to retrieve criterion values.", e);
				}
			}
			// Check start/end dates
			else if (criterion instanceof ActiveExpressionForDates) {
				ActiveExpressionForDates dateCriterion = (ActiveExpressionForDates) criterion;
				boolean active = dateCriterion.isActive();
				Date startDate = dateCriterion.getActiveOnStartDate();
				Date endDate = dateCriterion.getActiveOnEndDate();

				List<T> filteredResult = new ArrayList<>();
				for (T object : CollectionUtils.getIterable(result)) {
					if (DateUtils.isOverlapInDates(startDate, endDate, (Date) BeanUtils.getPropertyValue(object, dateCriterion.getStartDateProperty()),
							(Date) BeanUtils.getPropertyValue(object, dateCriterion.getEndDateProperty()))) {
						if (active) {
							filteredResult.add(object);
						}
					}
					else if (!active) {
						filteredResult.add(object);
					}
				}
				result = filteredResult;
				continue;
			}
			else if (criterion instanceof PropertyExpression) {
				String resultPropertyName = BeanUtils.getFieldValue(criterion, "propertyName");

				propertyName = BeanUtils.getFieldValue(criterion, "otherPropertyName");
				operator = BeanUtils.getFieldValue(criterion, "op");
				Set<Object> values = new HashSet<>();
				for (T object : CollectionUtils.getIterable(result)) {
					values.add(BeanUtils.getPropertyValue(object, resultPropertyName));
				}
				if ("=".equals(operator)) {
					operator = "IN";
					propertyValue = values.stream().filter(Objects::nonNull).toArray();
				}
				else {
					if (values.isEmpty()) {
						propertyValue = null;
					}
					else if (values.size() == 1) {
						propertyValue = values.iterator().next();
					}
					else {
						operator = "NOT IN";
						propertyValue = values.stream().filter(Objects::nonNull).toArray();
					}
				}
			}
			else {
				throw new RuntimeException("Unsupported Criterion: " + criterion.getClass());
			}

			// Validate operator type
			operator = operator.trim().toUpperCase();
			AssertUtils.assertTrue(SUPPORTED_OPERATORS.contains(operator), "Unsupported operator: %s for property %s", operator, propertyName);

			// Added LIKE as an operator, works in XML DAO as an Equal comparison
			// Need to change Property Value to remove the %'s
			if ("LIKE".equals(operator) && propertyValue instanceof String) {
				propertyValue = ((String) propertyValue).replace("%", "");
			}

			if ("<>".equals(operator)) {
				matchIfNotEquals = true;
			}

			int index = propertyName.indexOf('.');
			if (index != -1 && this.criteriaAlias != null && this.criteriaAlias.endsWith(propertyName.substring(0, index))) {
				propertyName = propertyName.substring(index + 1);
				index = -1;
			}

			if (index == -1 || ("id".equals(propertyName.substring(index + 1))) && AssertUtils.assertNotNull(dao, "No DAO was found.").getConfiguration().isValidBeanField(propertyName.substring(0, index))) {
				// simple restriction on this table (no joins)
				if ("IN".equals(operator)) {
					result = BeanUtils.filterByPropertyName(result, propertyName, (Object[]) propertyValue, false, true);
				}
				else if ("NOT IN".equals(operator)) {
					result = BeanUtils.filterByPropertyName(result, propertyName, (Object[]) propertyValue, true, true);
				}
				else if (Arrays.asList(">=", ">", "<=", "<").contains(operator)) {
					// Greater-than/less-than comparison operator
					List<T> filteredResult = new ArrayList<>();
					for (T object : CollectionUtils.getIterable(result)) {
						int compare = CompareUtils.compare(BeanUtils.getPropertyValue(object, propertyName), propertyValue);
						if (">=".equals(operator) && compare >= 0) {
							filteredResult.add(object);
							continue;
						}
						if (">".equals(operator) && compare > 0) {
							filteredResult.add(object);
							continue;
						}
						if ("<=".equals(operator) && compare <= 0) {
							filteredResult.add(object);
							continue;
						}
						if ("<".equals(operator) && compare < 0) {
							filteredResult.add(object);
						}
					}
					result = filteredResult;
				}
				else if ("LIKE".equals(operator)) {
					final String finalPropertyName = propertyName;
					final String finalPropertyValue = (String) propertyValue;
					result = result.stream().filter(bean -> StringUtils.isLikeIgnoringCase((String) BeanUtils.getPropertyValue(bean, finalPropertyName), finalPropertyValue)).collect(Collectors.toList());
				}
				else {
					result = BeanUtils.filterByPropertyName(result, propertyName, new Object[]{propertyValue}, matchIfNotEquals, true);
				}
			}
			else {
				// 1. get property alias and find corresponding link
				AssertUtils.assertTrue(index > 0, "Cannot extract table alias from property '%s'", propertyName);
				String alias = propertyName.substring(0, index);
				Join join = CollectionUtils.getOnlyElement(BeanUtils.filter(this.joinList, Join::getAlias, alias));
				if (join == null) {
					if (getSubCriteriaForAlias(alias) == null) {
						AssertUtils.fail("Cannot find join for alias '%s'", alias);
					}
					// there is no join with the alias because the alias is used in the subcriteria, continue to process subcriteria below
				}
				else {
					if (join.getColumn() != null) {
						// FOREIGN KEY JOIN (MANY-TO-ONE)
						// 2. get FK DAO by table name and find all instance(s) matching the value
						dao = getDaoLocator().locate(join.getColumn().getFkTable());
						List<T> beans = dao.findByField(propertyName.substring(index + 1), propertyValue);
						// 3. if no results, return null
						if (CollectionUtils.isEmpty(beans)) {
							return new ArrayList();
						}
						// 4. get id(s) from the beans
						Number[] beanIds = new Number[beans.size()];
						for (int i = 0; i < beanIds.length; i++) {
							beanIds[i] = (Number) beans.get(i).getIdentity();
						}
						// 5. limit original list based on latest id(s)
						result = BeanUtils.filterByPropertyName(result, join.getColumn().getBeanPropertyName() + ".id", beanIds, false, true);
					}
					else if (join.getLink().getManyToManyTable() == null) {
						// ONE-TO-MANY
						// 2. get bean type from the list and find all instance(s) matching the value
						Class<T> dtoClass = (Class<T>) CoreClassUtils.getClass(join.getLink().getReferencingClass());
						dao = getDaoLocator().locate(dtoClass);
						List<T> beans = dao.findByField(propertyName.substring(index + 1), propertyValue);
						// 3. if no results, return null
						if (CollectionUtils.isEmpty(beans)) {
							return new ArrayList();
						}
						// 4. get id(s) from the beans and re-map to the referencing id's
						Number[] beanIds = new Number[beans.size()];
						String filterFieldName = dao.getConfiguration().getBeanFieldName(join.getLink().getReferencingColumn());
						for (int i = 0; i < beanIds.length; i++) {
							beanIds[i] = (Number) BeanUtils.getPropertyValue(beans.get(i), filterFieldName + ".id");
						}
						// 5. limit original list based on latest id(s)
						result = BeanUtils.filter(result, bean -> ArrayUtils.contains(beanIds, bean.getIdentity()));
					}
					else {
						// MANY-TO-MANY JOIN
						// 2. get bean type from the list and find all instance(s) matching the value
						Class<T> dtoClass = (Class<T>) CoreClassUtils.getClass(join.getLink().getReferencingClass());
						dao = getDaoLocator().locate(dtoClass);
						List<T> beans = dao.findByField(propertyName.substring(index + 1), propertyValue);
						// 3. if no results, return null
						if (CollectionUtils.isEmpty(beans)) {
							return new ArrayList();
						}
						// 4. get id(s) from the beans
						Number[] beanIds = new Number[beans.size()];
						for (int i = 0; i < beanIds.length; i++) {
							beanIds[i] = (Number) beans.get(i).getIdentity();
						}
						// 5. find many-to-many rows that match id's
						dao = getDaoLocator().locate(join.getLink().getManyToManyTable());
						String filterFieldName = dao.getConfiguration().getBeanFieldName(join.getLink().getReferencingColumn());
						AssertUtils.assertNotNull(filterFieldName, "Cannot find referencing bean field name for '%s' column", join.getLink().getReferencingColumn());
						List<T> manyToManyBeans = dao.findByField(filterFieldName + ".id", beanIds);
						// 6. if no results, return null
						if (CollectionUtils.isEmpty(manyToManyBeans)) {
							return new ArrayList();
						}
						// 7. find id(s) corresponding to the other side
						Number[] resultIds = new Number[manyToManyBeans.size()];
						filterFieldName = dao.getConfiguration().getBeanFieldName(join.getLink().getManyToManyColumn());
						for (int i = 0; i < resultIds.length; i++) {
							resultIds[i] = (Number) BeanUtils.getPropertyValue(manyToManyBeans.get(i), filterFieldName + ".id");
						}
						// 8. limit original list based on latest id(s)
						result = BeanUtils.filter(result, bean -> ArrayUtils.contains(resultIds, bean.getIdentity()));
					}
				}
			}
		}

		// join these results with sub-criteria results
		if (!CollectionUtils.isEmpty(result) && !this.subCriteriaList.isEmpty() && !this.ignoreJoins) {
			for (XmlBasedCriteria<T> subCriteria : this.subCriteriaList) {
				List<T> subList = subCriteria.list();
				if (CollectionUtils.isEmpty(subList)) {
					return new ArrayList();
				}
				Number[] resultIds = new Number[subList.size()];
				for (int i = 0; i < resultIds.length; i++) {
					resultIds[i] = (Number) subList.get(i).getIdentity();
				}
				result = BeanUtils.filterByPropertyName(result, subCriteria.criteriaPath + ".id", resultIds, false, true);
			}
		}

		//  Order results (if necessary)
		if (!CollectionUtils.isEmpty(this.orderProperties)) {
			// Convert orderEntries to PropertyNames & Boolean ascending lists.
			result = BeanUtils.sortWithPropertyNames(result, this.orderProperties, this.orderAscending);
		}

		// Limit the results returned (Need to do this after re-ordering since results could be different depending on order
		if (this.maxResults != null && result != null && CollectionUtils.getSize(result) > this.maxResults) {
			return result.subList(0, this.maxResults);
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Criteria addOrder(Order order) {
		if (this.orderProperties == null) {
			this.orderProperties = new ArrayList<>();
			this.orderAscending = new ArrayList<>();
		}
		String oString = order.toString();
		String propertyName = oString.substring(0, oString.indexOf(' '));
		// convert aliases to corresponding paths if necessary
		int aliasEnd = propertyName.indexOf('.');
		if (aliasEnd != -1) {
			String alias = propertyName.substring(0, aliasEnd);
			if (alias.equals(this.criteriaAlias)) {
				propertyName = this.criteriaPath + propertyName.substring(aliasEnd);
			}
			else {
				XmlBasedCriteria<T> subCriteria = getSubCriteriaForAlias(alias);
				if (subCriteria != null) {
					propertyName = subCriteria.criteriaPath + propertyName.substring(aliasEnd);
				}
			}
		}
		this.orderProperties.add(propertyName);
		this.orderAscending.add(oString.endsWith("asc"));
		return this;
	}


	private XmlBasedCriteria<T> getSubCriteriaForAlias(String alias) {
		if (StringUtils.isEmpty(alias)) {
			return null;
		}
		for (XmlBasedCriteria<T> subCriteria : this.subCriteriaList) {
			if (alias.equals(subCriteria.criteriaAlias)) {
				return subCriteria;
			}
		}
		return null;
	}


	@Deprecated
	@Override
	public Criteria createAlias(String associationPath, String alias, int joinType) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria createCriteria(String associationPath) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Deprecated
	@Override
	public Criteria createCriteria(String associationPath, int joinType) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	/**
	 * NOTE: implements INNER_JOIN only: disregards joinType argument.
	 *
	 * @deprecated
	 */
	@Deprecated
	@Override
	public Criteria createCriteria(String associationPath, String alias, int joinType) throws HibernateException {
		return createCriteria(associationPath, alias);
	}


	@Override
	public Criteria createCriteria(String associationPath, String alias, JoinType joinType) throws HibernateException {
		return createCriteria(associationPath, alias);
	}


	@Override
	public String getAlias() {
		if (this.criteriaAlias != null) {
			return this.criteriaAlias;
		}
		return this.configuration.getTableName();
	}


	@Override
	public ScrollableResults scroll() throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public ScrollableResults scroll(ScrollMode scrollMode) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setCacheMode(CacheMode cacheMode) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setCacheRegion(String cacheRegion) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setCacheable(boolean cacheable) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setComment(String comment) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria addQueryHint(String hint) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setFetchMode(String associationPath, FetchMode mode) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setFetchSize(int fetchSize) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setFirstResult(int firstResult) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setFlushMode(FlushMode flushMode) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setLockMode(LockMode lockMode) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setLockMode(String alias, LockMode lockMode) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setMaxResults(int maxResults) {
		this.maxResults = maxResults;
		return this;
	}


	@Override
	public Criteria setProjection(Projection projection) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setResultTransformer(ResultTransformer resultTransformer) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setTimeout(int timeout) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Object uniqueResult() throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Deprecated
	@Override
	public Criteria createAlias(String associationPath, String alias, int joinType, Criterion withClause) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Deprecated
	@Override
	public Criteria createCriteria(String associationPath, String alias, int joinType, Criterion withClause) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public boolean isReadOnly() {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public boolean isReadOnlyInitialized() {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria setReadOnly(boolean readOnly) {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria createAlias(String associationPath, String alias, JoinType joinType) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria createAlias(String associationPath, String alias, JoinType joinType, Criterion withClause) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria createCriteria(String associationPath, JoinType joinType) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}


	@Override
	public Criteria createCriteria(String associationPath, String alias, JoinType joinType, Criterion withClause) throws HibernateException {
		throw new RuntimeException("THIS METHOD WAS NOT IMPLEMENTED");
	}
}
