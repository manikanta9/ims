package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;


/**
 * The <code>ArrayRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type array types (Integer[], etc.)
 * It's usually used for SQL IN comparisons.
 *
 * @author vgomelsky
 */
public class ArrayRestrictionDefinition extends AbstractRestrictionDefinition {

	public ArrayRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public ArrayRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, null, false, null, null);
	}


	public ArrayRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, null);
	}


	public ArrayRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, SearchFieldCustomTypes searchFieldCustomType) {
		this(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, new ComparisonConditions[]{ComparisonConditions.IN, ComparisonConditions.NOT_IN, ComparisonConditions.IN_OR_IS_NULL}, searchFieldCustomType);
	}


	public ArrayRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomType) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, false, comparisonConditions, searchFieldCustomType);
	}
}
