package com.clifton.core.dataaccess.datasource.stats;

import java.util.List;


/**
 * <code>DatabaseStatistics</code> contains statistics and properties for a {@link org.apache.tomcat.jdbc.pool.DataSource}.
 *
 * @author NickK
 */
public class DatabaseStatistics {

	private long reconnectedCount;
	private long idleCount;
	private long activeCount;
	private long maxActiveCount;
	private long createdCount;
	private long size;
	private long releasedCount;
	private long borrowedCount;
	private long returnedCount;
	private long releaseAbandonedCount;
	private long releasedIdleCount;

	private List<DatabaseProperty> databasePropertyList;


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public long getReconnectedCount() {
		return this.reconnectedCount;
	}


	public void setReconnectedCount(long reconnectedCount) {
		this.reconnectedCount = reconnectedCount;
	}


	public long getIdleCount() {
		return this.idleCount;
	}


	public void setIdleCount(long idleCount) {
		this.idleCount = idleCount;
	}


	public long getActiveCount() {
		return this.activeCount;
	}


	public void setActiveCount(long activeCount) {
		this.activeCount = activeCount;
	}


	public long getMaxActiveCount() {
		return this.maxActiveCount;
	}


	public void setMaxActiveCount(long maxActiveCount) {
		this.maxActiveCount = maxActiveCount;
	}


	public long getCreatedCount() {
		return this.createdCount;
	}


	public void setCreatedCount(long createdCount) {
		this.createdCount = createdCount;
	}


	public long getSize() {
		return this.size;
	}


	public void setSize(long size) {
		this.size = size;
	}


	public long getReleasedCount() {
		return this.releasedCount;
	}


	public void setReleasedCount(long releasedCount) {
		this.releasedCount = releasedCount;
	}


	public long getBorrowedCount() {
		return this.borrowedCount;
	}


	public void setBorrowedCount(long borrowedCount) {
		this.borrowedCount = borrowedCount;
	}


	public long getReturnedCount() {
		return this.returnedCount;
	}


	public void setReturnedCount(long returnedCount) {
		this.returnedCount = returnedCount;
	}


	public long getReleaseAbandonedCount() {
		return this.releaseAbandonedCount;
	}


	public void setReleaseAbandonedCount(long releaseAbandonedCount) {
		this.releaseAbandonedCount = releaseAbandonedCount;
	}


	public long getReleasedIdleCount() {
		return this.releasedIdleCount;
	}


	public void setReleasedIdleCount(long releasedIdleCount) {
		this.releasedIdleCount = releasedIdleCount;
	}


	public List<DatabaseProperty> getDatabasePropertyList() {
		return this.databasePropertyList;
	}


	public void setDatabasePropertyList(List<DatabaseProperty> databasePropertyList) {
		this.databasePropertyList = databasePropertyList;
	}
}
