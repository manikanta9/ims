package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Ordered;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.observer.Observer;


/**
 * The <code>DaoEventObserver</code> interface defines DAO events that varies services
 * can register for as listeners to perform certain actions: clear cache, audit events, etc.
 *
 * @param <T>
 * @author vgomelsky
 */
public interface DaoEventObserver<T extends IdentityObject> extends Observer, Ordered {

	/**
	 * The method is invoked before corresponding DAO method is called.
	 * The method is executed inside of DAO transaction.
	 */
	public void beforeMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean);


	/**
	 * The method is invoked after corresponding DAO method is called.
	 * If the DAO method raises exception, it will be passed (not null) as the last argument.
	 * The method is executed inside of DAO transaction.
	 */
	public void afterMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e);


	/**
	 * The method is invoked before corresponding DAO method is called and transaction is started.
	 */
	public void beforeTransactionMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean);


	/**
	 * The method is invoked after corresponding DAO method is called and transaction was committed.
	 * If the DAO method raises exception, it will be passed (not null) as the last argument.
	 */
	public void afterTransactionMethodCall(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e);


	/**
	 * DAO update operation returns a new persistedInstance which is tied to DAO provider's session.
	 * Depending on implementation (Hibernate does this), this instance maybe a result of pre-update
	 * database instance that has only persisted properties updated form the originalInstance.
	 * If originalInstance has other properties that are not persisted, they will not be copied
	 * to persistedInstance and passed to "after" observer methods.  If you want to preserve some
	 * non-persisted properties, this method must be implemented and the properties must be copied from
	 * original to persisted instance.
	 */
	public void configurePersistedInstance(T persistedInstance, T originalInstance);
}
