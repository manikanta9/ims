package com.clifton.core.dataaccess.db;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.SimpleEntity;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.Time;

import java.beans.PropertyDescriptor;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Date;


/**
 * The <code>PropertyDataTypeConverter</code> converts a Java Bean property to corresponding DataTypes.
 *
 * @author vgomelsky
 */
public class PropertyDataTypeConverter implements Converter<PropertyDescriptor, DataTypes> {

	@Override
	public DataTypes convert(PropertyDescriptor from) {
		String propertyName = from.getName();
		Class<?> propertyType = from.getPropertyType();
		if ("id".equals(propertyName)) {
			if (propertyType == int.class || propertyType == Integer.class) {
				return DataTypes.IDENTITY;
			}
			if (propertyType == long.class || propertyType == Long.class) {
				return DataTypes.IDENTITY_LONG;
			}
			if (propertyType == short.class || propertyType == Short.class) {
				return DataTypes.IDENTITY_SHORT;
			}
			if (propertyType == String.class) {
				//Deals with uuid for NoTableDAO objects (e.g. AccountingBalance)
				return DataTypes.NAME;
			}
			throw new IllegalStateException("'id' property must be an int or long: " + propertyType);
		}

		if (propertyType.isEnum()) {
			return DataTypes.ENUM;
		}
		if (propertyType == Time.class) {
			return DataTypes.TIME;
		}
		if (propertyType == int.class || propertyType == Integer.class) {
			return DataTypes.INT;
		}
		if (propertyType == short.class || propertyType == Short.class) {
			return DataTypes.SHORT;
		}
		if (propertyType == long.class || propertyType == Long.class) {
			return DataTypes.LONG;
		}
		if (propertyType == BigDecimal.class) {
			return DataTypes.PRICE;
		}
		if (propertyType == boolean.class || propertyType == Boolean.class) {
			return DataTypes.BIT;
		}
		if (propertyType == Date.class || propertyType == OffsetDateTime.class) {
			return DataTypes.DATE;
		}
		if (propertyType == String.class) {
			String lowerCasedProperty = propertyName.toLowerCase();
			if (lowerCasedProperty.contains("note") || lowerCasedProperty.contains("description")) {
				return DataTypes.DESCRIPTION;
			}
			return DataTypes.NAME;
		}
		if ("rv".equals(propertyName) && propertyType == byte[].class) {
			return DataTypes.ROWVERSION;
		}
		if (IdentityObject.class == propertyType) {
			// FK reference to the IdentityObject interface and not concrete class (i.e. ManyToManyEntity.referenceOne)
			return DataTypes.INT;
		}
		if (IdentityObject.class.isAssignableFrom(propertyType) || (propertyType.isInterface() && SimpleEntity.class.isAssignableFrom(propertyType))) {
			// the field is a foreign key to another DTO: check the type of id field
			Class<?> idType = null;
			if (propertyType.isInterface() && SimpleEntity.class.isAssignableFrom(propertyType)) {
				Type[] interfaceTypes = propertyType.getGenericInterfaces();
				for (Type interfaceType : interfaceTypes) {
					if (interfaceType instanceof ParameterizedType) {
						ParameterizedType parameterizedType = (ParameterizedType) interfaceType;
						if (SimpleEntity.class.isAssignableFrom((Class<?>) parameterizedType.getRawType())) {
							idType = (Class<?>) parameterizedType.getActualTypeArguments()[0];
							break;
						}
					}
				}
				AssertUtils.assertNotNull(idType, "Cannot map DB type for property '%1s' because it's IdentityObject but doesn't have id field.", propertyName);
			}
			else {
				PropertyDescriptor idField = BeanUtils.getPropertyDescriptor(propertyType, "id");
				AssertUtils.assertNotNull(idField, "Cannot map DB type for property '%1s' because it's IdentityObject but doesn't have id field.", propertyName);
				idType = idField.getPropertyType();
			}
			if (idType == int.class || idType == Integer.class) {
				return DataTypes.INT;
			}
			if (idType == long.class || idType == Long.class) {
				return DataTypes.LONG;
			}
			if (idType == short.class || idType == Short.class) {
				return DataTypes.SHORT;
			}

			throw new RuntimeException("Cannot map DB data type from property '" + propertyName + "' of type " + propertyType + ". Only int, long, and short are supported for foreign keys and not: "
					+ idType);
		}
		throw new RuntimeException("Cannot map DB data type from property '" + propertyName + "' of type " + propertyType);
	}
}
