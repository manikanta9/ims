package com.clifton.core.dataaccess;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.event.DaoDtoClassAware;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.observer.Observer;

import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.lang.reflect.TypeVariable;
import java.util.ArrayDeque;
import java.util.Collections;
import java.util.Deque;
import java.util.Set;
import java.util.function.Predicate;


/**
 * The <code>DaoUtils</code> class defines DAO utility methods: DAO naming, working with detached objects, etc.
 *
 * @author vgomelsky
 */
public class DaoUtils {

	private static final ThreadLocal<Boolean> detachedMode = new ThreadLocal<>();
	private static final ThreadLocal<Boolean> postUpdateFlush = new ThreadLocal<>(); // non-null value means flush mode is enabled
	private static final ThreadLocal<Boolean> disableInsertDirtyChecking = new ThreadLocal<>(); // non-null value means flush mode is enabled
	private static final ThreadLocal<Set<Class<?>>> disabledObserverSet = new ThreadLocal<>();
	private static final ThreadLocal<Set<Class<?>>> enabledObserverSet = new ThreadLocal<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static final Predicate<Observer> observerPredicate = o -> {
		if (getEnabledObserverSet() != null) {
			// disable all
			if (getEnabledObserverSet().isEmpty()) {
				return false;
			}
			// enabled set
			return getEnabledObserverSet().contains(o.getClass());
		}
		// disabled set
		if (getDisabledObserverSet() != null) {
			return !getDisabledObserverSet().contains(o.getClass());
		}
		return true;
	};

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates DAO context bean name based on the corresponding DTO class.
	 */
	public static String getDaoName(Class<?> dtoClass) {
		String className = dtoClass.getName();
		int start = className.lastIndexOf('.') + 1;
		return Character.toLowerCase(className.charAt(start)) + className.substring(start + 1) + "DAO";
	}


	/**
	 * Generates DAO context bean name based on the specified table name.
	 */
	public static String getDaoName(String tableName) {
		return Character.toLowerCase(tableName.charAt(0)) + tableName.substring(1) + "DAO";
	}


	/**
	 * Returns true if the table name and DTO class result in the same database table, or DAO.
	 */
	public static <T extends IdentityObject> boolean isDtoClassTableMatch(String tableName, Class<T> dtoClass) {
		if (tableName == null || dtoClass == null) {
			// cannot match if either are undefined
			return false;
		}
		return StringUtils.isEqual(tableName, dtoClass.getSimpleName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the DTO class from the provided DAO observer class. This method uses the generic type arguments from the {@link SelfRegisteringDaoObserver} inheritance
	 * declaration.
	 *
	 * @param daoObserverClass the DAO observer class for which to retrieve the corresponding DTO class
	 * @return the corresponding DTO class
	 */
	public static Class<? extends IdentityObject> getDtoClass(Class<? extends SelfRegisteringDaoObserver<?>> daoObserverClass) {
		// Search for appropriate superclass declaration with generic type parameters
		Class<?> genericDeclaringClass = daoObserverClass;
		Deque<Class<?>> classHierarchy = new ArrayDeque<>();
		while (genericDeclaringClass != null && !SelfRegisteringDaoObserver.class.equals(genericDeclaringClass.getSuperclass())) {
			classHierarchy.push(genericDeclaringClass);
			genericDeclaringClass = genericDeclaringClass.getSuperclass();
		}
		AssertUtils.assertNotNull(genericDeclaringClass, "Unable to determine appropriate generic superclass [%s] from the provided class [%s] for DTO class detection.", SelfRegisteringDaoObserver.class, daoObserverClass);

		// Traverse parents until the generic type variable for DTO is defined
		// Suppress given that it is guaranteed non-null by assert contract
		@SuppressWarnings("ConstantConditions")
		Type typeParameter = ((ParameterizedType) genericDeclaringClass.getGenericSuperclass()).getActualTypeArguments()[0];
		while (typeParameter instanceof TypeVariable) {
			AssertUtils.assertNotEmpty(classHierarchy, "No explicit DTO type parameter was defined for the given DAO observer [%s].", daoObserverClass.getName());

			// Seek type parameter from next implementor
			String typeParamName = ((TypeVariable<?>) typeParameter).getName();
			int typeParamIndex = ArrayUtils.indexOf(genericDeclaringClass.getTypeParameters(), type -> type.getName().equals(typeParamName));
			genericDeclaringClass = classHierarchy.pop();
			typeParameter = ((ParameterizedType) genericDeclaringClass.getGenericSuperclass()).getActualTypeArguments()[typeParamIndex];
		}

		// Retrieve generic type parameter from declared superclass
		@SuppressWarnings("unchecked")
		Class<? extends IdentityObject> dtoClass = (Class<? extends IdentityObject>) typeParameter;
		return dtoClass;
	}


	/**
	 * Retrieves the DTO class from the provided DAO observer. This method uses the generic type arguments from the {@link SelfRegisteringDaoObserver} inheritance declaration, or
	 * the {@link DaoDtoClassAware#getDtoClass()} reference for {@link DaoDtoClassAware} instances.
	 *
	 * @param daoObserver the DAO observer for which to retrieve the corresponding DTO class
	 * @return the corresponding DTO class
	 */
	public static Class<? extends IdentityObject> getDtoClass(SelfRegisteringDaoObserver<?> daoObserver) {
		if (daoObserver instanceof DaoDtoClassAware) {
			return ((DaoDtoClassAware<?>) daoObserver).getDtoClass();
		}
		else {
			@SuppressWarnings("unchecked")
			Class<? extends SelfRegisteringDaoObserver<?>> daoObserverClass = (Class<? extends SelfRegisteringDaoObserver<?>>) daoObserver.getClass();
			return getDtoClass(daoObserverClass);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if detached mode is enabled.  When detached mode is used (NON DEFAULT), a DTO will be retrieved directly from the database (bypassing all caches) and it will
	 * also be optionally detached (session.evict(dto)) from current session when (evictFromSession == true) so that Hibernate can't modify it.
	 */
	public static boolean isDetachedModeEnabled() {
		return (detachedMode.get() != null);
	}


	/**
	 * Get the list of disabled observer classes or null.
	 */
	public static Set<Class<?>> getDisabledObserverSet() {
		return disabledObserverSet.get();
	}


	/**
	 * Get the list of enabled observer classes or null.
	 */
	public static Set<Class<?>> getEnabledObserverSet() {
		return enabledObserverSet.get();
	}


	/**
	 * Returns true when detached mode was started with eviction request.
	 */
	public static boolean isEvictFromSession() {
		Boolean evict = detachedMode.get();
		if (evict == null) {
			throw new IllegalStateException("Cannot determine eviction status because detached mode is not enabled");
		}
		return evict;
	}


	/**
	 * Returns true if flushes should be performed when DTOs are updated. When enabled, each save of a DTO will also invoke a flush to synchronized Hibernate's session.
	 * <p>
	 * A flush will only occur if the save is invoked within an active transaction.
	 */
	public static boolean isPostUpdateFlushEnabled() {
		return postUpdateFlush.get() != null;
	}


	/**
	 * Returns true if dirty checking should be disabled when new DTOs are saved.
	 * When disabled, beans will not be registered for the dirty checking strategy after initial save
	 * This is necessary during JSON migration actions.
	 */
	public static boolean isInsertDirtyCheckingDisabled() {
		return disableInsertDirtyChecking.get() != null;
	}


	/**
	 * Enables detached mode and specifies whether retrieved DTO needs to be evicted from current session or not.
	 */
	private static void enableDetachedMode(boolean evictFromSession) {
		detachedMode.set(evictFromSession);
	}


	private static void disableDetachedMode() {
		detachedMode.remove();
	}


	/**
	 * Disables all observers from executing around DTO updates.
	 */
	private static void disableAllObservers() {
		enabledObserverSet.set(Collections.emptySet());
		disabledObserverSet.remove();
	}


	/**
	 * Enables all observers from executing around DTO updates.
	 */
	private static void enableAllObservers() {
		enabledObserverSet.remove();
		disabledObserverSet.remove();
	}


	/**
	 * Enables the ability to flush following DTO updates.
	 */
	private static void enablePostUpdateFlush() {
		postUpdateFlush.set(true);
	}


	private static void disablePostUpdateFlush() {
		postUpdateFlush.remove();
	}


	/**
	 * Disables dirty checking registration after saving news beans
	 */
	private static void enableInsertDirtyChecking() {
		disableInsertDirtyChecking.remove();
	}


	private static void disableInsertDirtyChecking() {
		disableInsertDirtyChecking.set(true);
	}


	/**
	 * Executes the specified callback in detached mode. Guarantees to enter detached mode before callback execution and exit it after.
	 * <p>
	 * Detached mode should be used within a transaction when DAO calls should go directly against the database bypassing all caches and returned DTO's are detached or evicted from
	 * current session.
	 */
	public static <T> T executeDetached(boolean evictFromSession, DaoCallback<T> callback) {
		try {
			enableDetachedMode(evictFromSession);
			return callback.execute();
		}
		finally {
			disableDetachedMode();
		}
	}


	/**
	 * Executes the specified callback with specific observers enabled: all other observers are disabled.
	 *
	 * @SafeVarargs, only need to enforce the class type at compilation time, class type is not used any further.
	 */
	@SafeVarargs
	public static <T> T executeWithSpecificObserversEnabled(DaoCallback<T> callback, Class<? extends Observer>... enabledObservers) {
		if (enabledObservers == null || enabledObservers.length == 0) {
			throw new IllegalArgumentException("At least one Observer must be specified for method: executeWithSpecificObserversEnabled");
		}

		try {
			DaoUtils.enabledObserverSet.set(CollectionUtils.createHashSet(enabledObservers));
			DaoUtils.disabledObserverSet.remove();
			return callback.execute();
		}
		finally {
			enableAllObservers();
		}
	}


	/**
	 * Executes the specified callback with specific observers enabled: all other observers are disabled.
	 *
	 * @SafeVarargs, only need to enforce the class type at compilation time, class type is not used any further.
	 */
	@SafeVarargs
	public static void executeWithSpecificObserversEnabled(DaoCallbackWithoutResponse callback, Class<? extends Observer>... enabledObservers) {
		if (enabledObservers == null || enabledObservers.length == 0) {
			throw new IllegalArgumentException("At least one Observer must be specified for method: executeWithSpecificObserversEnabled");
		}

		try {
			DaoUtils.enabledObserverSet.set(CollectionUtils.createHashSet(enabledObservers));
			DaoUtils.disabledObserverSet.remove();
			callback.execute();
		}
		finally {
			enableAllObservers();
		}
	}


	/**
	 * Executes the specified callback with specific observers disabled.
	 *
	 * @SafeVarargs, only need to enforce the class type at compilation time, class type is not used any further.
	 */
	@SafeVarargs
	public static <T> T executeWithSpecificObserversDisabled(DaoCallback<T> callback, Class<? extends Observer>... disabledObservers) {
		if (disabledObservers == null || disabledObservers.length == 0) {
			throw new IllegalArgumentException("At least one Observer must be specified for method: executeWithSpecificObserversDisabled");
		}

		try {
			DaoUtils.disabledObserverSet.set(CollectionUtils.createHashSet(disabledObservers));
			DaoUtils.enabledObserverSet.remove();
			return callback.execute();
		}
		finally {
			enableAllObservers();
		}
	}


	/**
	 * Executes the specified callback with specific observers disabled.
	 *
	 * @SafeVarargs, only need to enforce the class type at compilation time, class type is not used any further.
	 */
	@SafeVarargs
	public static void executeWithSpecificObserversDisabled(DaoCallbackWithoutResponse callback, Class<? extends Observer>... disabledObservers) {
		if (disabledObservers == null || disabledObservers.length == 0) {
			throw new IllegalArgumentException("At least one Observer must be specified for method: executeWithSpecificObserversDisabled");
		}

		try {
			DaoUtils.disabledObserverSet.set(CollectionUtils.createHashSet(disabledObservers));
			DaoUtils.enabledObserverSet.remove();
			callback.execute();
		}
		finally {
			enableAllObservers();
		}
	}


	/**
	 * Executes the specified callback with observers disabled.
	 */
	public static <T> T executeWithAllObserversDisabled(DaoCallback<T> callback) {
		try {
			disableAllObservers();
			return callback.execute();
		}
		finally {
			enableAllObservers();
		}
	}


	public static void executeWithAllObserversDisabled(DaoCallbackWithoutResponse callback) {
		try {
			disableAllObservers();
			callback.execute();
		}
		finally {
			enableAllObservers();
		}
	}


	/**
	 * Executes the specified callback and will flush on every DTO update that occurs. After the callback completes, the post update flush will be disabled.
	 * <p>
	 * Post update flush mode should be used within a transaction when DAO calls need to be manually flushed. There are cases where observers will see inconsistent DTO states due
	 * to unprocessed session flushes.
	 */
	public static <T> T executeWithPostUpdateFlushEnabled(DaoCallback<T> callback) {
		try {
			enablePostUpdateFlush();
			return callback.execute();
		}
		finally {
			disablePostUpdateFlush();
		}
	}


	public static void executeWithPostUpdateFlushEnabled(DaoCallbackWithoutResponse callback) {
		try {
			enablePostUpdateFlush();
			callback.execute();
		}
		finally {
			disablePostUpdateFlush();
		}
	}


	/**
	 * Executes the specified callback and will disable dirty checking registration after save of a new DTO.
	 * After the callback completes, dirty checking registration will be enabled again
	 * <p>
	 * Should be disabled when executing JSON migrations that chain parent and child objects
	 */
	public static <T> T executeWithInsertDirtyCheckingDisabled(DaoCallback<T> callback) {
		try {
			disableInsertDirtyChecking();
			return callback.execute();
		}
		finally {
			enableInsertDirtyChecking();
		}
	}


	public static void executeWithInsertDirtyCheckingDisabled(DaoCallbackWithoutResponse callback) {
		try {
			disableInsertDirtyChecking();
			callback.execute();
		}
		finally {
			enableInsertDirtyChecking();
		}
	}


	public static Serializable convertToPrimaryKeyDataType(Serializable key, DAOConfiguration<?> configuration) {
		return convertToDataType(key, configuration.getPrimaryKeyColumn().getDataType(), configuration.getBeanClass());
	}


	public static Serializable convertToDataType(Serializable key, DataTypes dataType, Class<?> clazz) {
		try {
			return convertToDataType(key, dataType);
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("%s primary key is not supported for %s", dataType, clazz), e);
		}
	}


	/**
	 * Converts the given number to the specified data type.
	 *
	 * @param key      the serializable to convert
	 * @param dataType the data type to convert to
	 * @return the converted number
	 * @throws RuntimeException if the specified data type is not supported
	 */
	public static Serializable convertToDataType(Serializable key, DataTypes dataType) {
		switch (dataType) {
			case TINY:
			case IDENTITY_TINY:
			case NATURAL_ID_TINY:

			case SHORT:
			case IDENTITY_SHORT:
			case NATURAL_ID_SHORT:
				return new Short(key.toString());

			case INT:
			case IDENTITY:
			case NATURAL_ID:
				return new Integer(key.toString());

			case LONG:
			case IDENTITY_LONG:
			case NATURAL_ID_LONG:
				return new Long(key.toString());

			case NAME_LONG:
			case IDENTITY_STRING:
				return key.toString();

			default:
				throw new RuntimeException(String.format("No valid converter exists for data type [%s].", dataType));
		}
	}


	/**
	 * The <code>DetachedDaoCallback</code> is a callback interface to be used with executeDetached method.
	 */
	public interface DaoCallback<T> {

		public T execute();
	}

	/**
	 * The <code>DaoCallbackWithoutResponse</code> is a callback interface to be used with executeDetached method that has no response object.
	 */
	public interface DaoCallbackWithoutResponse {

		public void execute();
	}
}
