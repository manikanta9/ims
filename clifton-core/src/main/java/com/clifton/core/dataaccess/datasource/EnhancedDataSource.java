package com.clifton.core.dataaccess.datasource;

import org.apache.tomcat.jdbc.pool.ConnectionPool;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolConfiguration;

import java.sql.SQLException;


/**
 * A subclass of the DataSource which tracks the maximum active connections for the data source.
 *
 * @author davidi
 */
public class EnhancedDataSource extends DataSource {

	public EnhancedDataSource() {
		super();
	}


	public EnhancedDataSource(PoolConfiguration poolProperties) {
		super(poolProperties);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ConnectionPool createPool() throws SQLException {
		if (this.pool != null) {
			return this.pool;
		}
		else {
			return createEnhancedConnectionPool();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected synchronized ConnectionPool createEnhancedConnectionPool() throws SQLException {
		if (this.pool == null) {
			this.pool = new EnhancedConnectionPool(poolProperties);
		}
		return this.pool;
	}
}
