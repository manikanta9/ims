package com.clifton.core.dataaccess.search.hibernate;

import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.SimpleProjection;
import org.hibernate.type.Type;


/**
 * The FormulaProjection works similar to the Formula Expression which takes the formula from the search field definition but in this case builds it into the projection
 * with proper support for joined table aliases.  Currently only used by GroupingHibernateSearchFormConfigurer.
 */
public class FormulaProjection extends SimpleProjection {

	private final String[] propertyNames;
	private final String sqlFormula;
	private final Type type;


	public FormulaProjection(String sqlFormula, String[] propertyNames, Type type) {
		this.sqlFormula = sqlFormula;
		this.propertyNames = propertyNames;
		this.type = type;
	}

	// note: not implemented for group by functionality


	@Override
	public Type[] getTypes(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return new Type[]{this.type};
	}


	@Override
	public String toSqlString(Criteria criteria, int loc, CriteriaQuery criteriaQuery) {
		String[] columns = new String[this.propertyNames.length];
		for (int i = 0; i < this.propertyNames.length; ++i) {
			columns[i] = (criteriaQuery.getColumn(criteria, this.propertyNames[i]));
		}
		StringBuilder fragmentSb = new StringBuilder(String.format(this.sqlFormula, (Object[]) columns));
		return fragmentSb + " as y" + loc + '_';
	}
}
