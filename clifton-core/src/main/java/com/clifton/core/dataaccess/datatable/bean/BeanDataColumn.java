package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.converter.reversable.ReversableConverter;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.shared.dataaccess.DataTypes;

import java.sql.Types;


/**
 * The <code>BeanDataColumn</code> ...
 *
 * @author manderson
 */
public class BeanDataColumn extends DataColumnImpl {

	private final String beanPropertyName;


	public BeanDataColumn(String beanPropertyName, String columnName, String style, ReversableConverter<Object, Object> columnValueConverter, DataTypes beanDataType) {
		this(beanPropertyName, columnName, style, columnValueConverter, beanDataType, null);
	}


	public BeanDataColumn(String beanPropertyName, String columnName, String style, ReversableConverter<Object, Object> columnValueConverter, DataTypes beanDataType, Integer sqlType) {
		super(columnName, sqlType != null ? sqlType : Types.NVARCHAR, null, columnValueConverter);
		this.beanPropertyName = beanPropertyName;
		if (style == null) {
			style = "";
		}
		// If already explicitly set - ignore the bean data type
		// Can be used for percentages that explicitly are exported as the value i.e. 1.0 instead of 100 %
		if (beanDataType != null && !style.contains("dataType=")) {
			style += ",dataType=" + beanDataType.name();
		}
		setStyle(style);
	}


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}
}
