package com.clifton.core.dataaccess.migrate.schema.dml;


import com.clifton.core.util.converter.Converter;


/**
 * The <code>ValueToColumnConverter</code> class extracts column names from <code>Value</code> objects.
 *
 * @author vgomelsky
 */
public class ValueToColumnConverter implements Converter<Value, String> {

	@Override
	public String convert(Value from) {
		return from.getColumn();
	}
}
