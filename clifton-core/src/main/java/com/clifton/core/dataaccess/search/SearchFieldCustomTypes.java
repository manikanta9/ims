package com.clifton.core.dataaccess.search;


/**
 * The <code>SearchFieldCustomTypes</code> defines how search is handled if multiple values
 * are passed for the search fields. Default would be to use OR
 *
 * @author manderson
 */
public enum SearchFieldCustomTypes {

	OR(false, true, false),

	/**
	 * Special case for numeric fields that matches to either negative or positive value.
	 * The same field must be specified only once and for EQUALS comparison only will use (field = value OR field = -value)
	 */
	OR_IGNORE_SIGN_FOR_EQUALS(false, true, false),


	/**
	 * Used for things that can have an override, but we want to search/sort on the coalesce of those fields
	 * i.e. Coalesce(label,name) or Coalesce(ValueOverride,Value)
	 */
	COALESCE(true, false, false),

	/**
	 * Coalesce, but adds false as last item i.e. COALESCE(x, y, false)
	 * Should only be used with boolean search fields
	 */
	COALESCE_FALSE(true, false, false, false),

	/**
	 * Coalesce, but adds true as last item i.e. COALESCE(x, y, true)
	 * Should only be used with boolean search fields
	 */
	COALESCE_TRUE(true, false, false, true),

	/**
	 * Created for search forms to allow a null value for a coalesced field
	 */
	COALESCE_NULL(true, false, false),

	/**
	 * Created for search forms to search/sort on the coalesce of fields where the result is null or not, and returns the appropriate boolean i.e. IIF(COALESCE(x, y) IS NULL, 1, 0)
	 * This is useful for composite ui fields where the backend field does not exist and is instead comprised of the existence of values in multiple fields.
	 */
	COALESCE_SORT_ON_ALL_NULL(true, false, false),


	/**
	 * Allows for filtering/ordering for compressed text fields
	 */
	JSON_COMPRESSED_TEXT(false, false, true),


	/**
	 * Used for cases where the field is the value of adding two or more fields
	 * This can often be avoided by using a calculated column, but in some cases if you need to span tables the calculated column may not be appropriate
	 */
	MATH_ADD(true, "+", FunctionTypes.MATH),


	/**
	 * Used for cases where the field is the value of adding two or more fields
	 * This can often be avoided by using a calculated column, but in some cases if you need to span tables the calculated column may not be appropriate
	 * SUPPORTS IF ONE OR MORE VALUES IS NULL AND USES ISNULL(COLUMN,0)
	 */
	MATH_ADD_NULLABLE(true, "+", FunctionTypes.MATH_NULLABLE),


	/**
	 * Used for cases where the field is the value of subtracting two or more fields
	 * This can often be avoided by using a calculated column, but in some cases if you need to span tables the calculated column may not be appropriate
	 */
	MATH_SUBTRACT(true, "-", FunctionTypes.MATH),

	/**
	 * Used for cases where the field is the value of subtracting two or more fields
	 * This can often be avoided by using a calculated column, but in some cases if you need to span tables the calculated column may not be appropriate
	 */
	MATH_SUBTRACT_NULLABLE(true, "-", FunctionTypes.MATH_NULLABLE),

	/**
	 * Used for cases where the field is the value of multiplying two or more fields
	 * This can often be avoided by using a calculated column, but in some cases if you need to span tables the calculated column may not be appropriate
	 */
	MATH_MULTIPLY(true, "*", FunctionTypes.MATH),

	/**
	 * Used for cases where the field is the value of dividing two or more fields
	 * This can often be avoided by using a calculated column, but in some cases if you need to span tables the calculated column may not be appropriate
	 */
	MATH_DIVIDE(true, "/", FunctionTypes.MATH),

	/**
	 * Concatenates the listed fields when performing sorts. Searches use OR comparisons (Should be used when all fields are the same data type)
	 * <p>
	 * This concatenates the fields with a space character. This configuration should be used when the included fields are joined with a space character (<tt>" "</tt>) when
	 * displayed to the client.
	 * <p>
	 * This is ideal when the included fields will be displayed with space-joined concatenation. For example, if the sorted fields are {@code firstName} and {@code lastName}, then
	 * "Jane Doe" will come before "John Doe". Additionally, this will handle strings that have spaces in them appropriately. For example, the entity with first name "Minneapolis
	 * Heart Institute" and no last name will come <i>before</i> the entity with first name "Minneapolis" and last name "Institute of Arts". Types such as {@link
	 * #CONCATENATE_SORT_WITH_DELIMITER} will function differently in such cases.
	 */
	CONCATENATE_SORT(true, true, false),

	/**
	 * Uses Concatenation for Searching and Sorting
	 * For searching properly casts the numeric field values to String for the concatenation in search and order by
	 */
	CONCATENATE_STRING_AND_NUMBER(true, false, false),

	/**
	 * Concatenates the listed fields when performing sorts using a delimiter instead of a space between fields. Searches use OR comparisons (Should be used when all fields are the same data type)
	 * Used for cases like hierarchical objects to sort by the full hierarchy name
	 * and performance the search like an or on each field
	 * Example: Search - Grandparent Name or Parent Name or Name
	 * Sort COALESCE(Grandparent name + ' / ','') + COALESCE(Parent Name + ' / ', '') + COALESCE(Name, ' / ', '')
	 * Primarily used for Strings - see CONCATENATE_SORT_WITH_PADDING for numerical hierarchical searches
	 * <p>
	 * Note that this configuration will separate values with a delimiter which may affect sorting. This configuration should be used when the included fields are delimited with a
	 * slash character (<tt>"/"</tt>) when displayed to the client.
	 */
	CONCATENATE_SORT_WITH_DELIMITER(true, true, false),

	/**
	 * Same as {@link #CONCATENATE_SORT_WITH_DELIMITER} but when the value is NULL, appends space character vs nothing:
	 * Sort COALESCE(Client Account Number + ' / ',' ') + COALESCE(Business Service Name + ' / ', ' ')
	 * This is useful when sort columns are mutually exclusive but there's specific priority of order for each field.
	 */
	CONCATENATE_SORT_WITH_DELIMITER_AND_SPACE_PADDING(true, true, false),

	/**
	 * Used for numeric hierarchical sorting - pads numbers so that sorting is in numerical order (i.e. 10 doesn't come before 2) - Uses OR for searches.
	 * Used for cases like hierarchical objects to sort by the full hierarchy order
	 * Works similar to concatenate but for order by casts to a string with padding so that order of 20 comes before 100 because of text searching
	 * Uses a padding of 6 digits.
	 * Example: Search - Grandparent Order + Parent Order + My Order
	 * Sort COALESCE(RIGHT('000000' + CAST(Grandparent Order AS VARCHAR(6)), 6) + ' / ','') + COALESCE(RIGHT('000000' + CAST(Parent Order AS VARCHAR(6)), 6) + ' / ',''), etc.
	 * Primarily used for Numerical fields - see CONCATENATE_SORT for STRING hierarchical searches
	 */
	CONCATENATE_SORT_WITH_PADDING(true, true, false),

	/**
	 * Used when the filtering is required based on the difference between two dates. Using MILLISECOND to
	 * improve precision in time difference calculation, as DATEDIFF uses a different calculation method
	 * than our methods in DateUtils, leading to a maximum precision of +/- 1 second.  Using MILLISECOND instead
	 * of SECOND mitigates this precision error during sorting operations (e.g. via OrderBy).
	 */
	DATE_DIFFERENCE_SECONDS(true, "(DATEDIFF(MILLISECOND, %s, %s) / 1000)", FunctionTypes.DATE),

	/**
	 * Used when the filtering is required based on the difference between two dates.
	 */
	DATE_DIFFERENCE_MILLISECONDS(true, "(DATEDIFF(MILLISECOND, %s, %s))", FunctionTypes.DATE);

	////////////////////////////////////////////////////////////////////////////////

	private boolean compressed;

	/**
	 * In order for multiple fields logic to be used there must be multiple fields defined in the search fields
	 * However, in some cases, like COALESCE_FALSE - we want it to work with just one field present
	 */
	private boolean supportSingleField;

	private final boolean orderByMultipleSupported;

	/**
	 * Use OR logic/SQL keyword for searches across multiple fields.
	 */
	private final boolean orSearch;

	private String mathFunction;
	private boolean mathSupportNull;

	private String dateDiffFunction;

	private Object coalesceDefaultValue;

	private enum FunctionTypes {
		MATH, MATH_NULLABLE, DATE
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	SearchFieldCustomTypes(boolean orderByMultipleSupported, String expression, FunctionTypes functionType) {
		this(orderByMultipleSupported, false, false);
		switch (functionType) {
			case MATH:
				this.mathFunction = expression;
				break;
			case MATH_NULLABLE:
				this.mathFunction = expression;
				this.mathSupportNull = true;
				break;
			case DATE:
				this.dateDiffFunction = expression;
		}
	}


	SearchFieldCustomTypes(boolean orderByMultipleSupported, boolean orSearch, boolean compressed) {
		this.orderByMultipleSupported = orderByMultipleSupported;
		this.orSearch = orSearch;
		this.compressed = compressed;
		this.supportSingleField = compressed;
	}


	SearchFieldCustomTypes(boolean orderByMultipleSupported, boolean orSearch, boolean compressed, Object coalesceDefaultValue) {
		this(orderByMultipleSupported, orSearch, compressed);
		this.coalesceDefaultValue = coalesceDefaultValue;
		if (coalesceDefaultValue != null) {
			this.supportSingleField = true;
		}
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isCompressed() {
		return this.compressed;
	}


	public boolean isOrderByMultipleSupported() {
		return this.orderByMultipleSupported;
	}


	public boolean isOrSearch() {
		return this.orSearch;
	}


	public boolean isMathFunctionSupported() {
		return this.mathFunction != null;
	}


	public String getMathFunction() {
		return this.mathFunction;
	}


	public boolean isDateDiffFunctionSupported() {
		return this.dateDiffFunction != null;
	}


	public String getDateDiffFunction() {
		return this.dateDiffFunction;
	}


	public boolean isMathSupportNull() {
		return this.mathSupportNull;
	}


	public Object getCoalesceDefaultValue() {
		return this.coalesceDefaultValue;
	}


	public boolean isSupportSingleField() {
		return this.supportSingleField;
	}
}
