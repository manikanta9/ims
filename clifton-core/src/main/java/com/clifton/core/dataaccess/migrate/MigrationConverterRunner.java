package com.clifton.core.dataaccess.migrate;


import com.clifton.core.dataaccess.dao.hibernate.HibernateSchemaConverter;
import com.clifton.core.dataaccess.db.PropertyDataTypeConverter;
import com.clifton.core.dataaccess.migrate.reader.XmlMigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.reader.XmlMigrationHelper;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.core.util.date.TimeTrackedOperation;
import com.clifton.core.util.date.TimeUtils;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.OutputStream;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.List;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.stream.Collectors;


/**
 * The <code>MigrationConverterRunner</code> class generates consolidated migrations file from all migrations for the project specified by the first argument.
 * It also generates a corresponding Hibernate mapping file.
 *
 * @author vgomelsky
 * @see #getUsage()
 */
public class MigrationConverterRunner {

	/**
	 * The list of all names of discovered schemas on the class path.
	 */
	private static final Lazy<List<String>> CLASSPATH_SCHEMA_NAME_LIST = new Lazy<>(MigrationConverterRunner::getClasspathSchemaNameList);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Generates mapping files.
	 *
	 * @see #getUsage()
	 */
	public static void main(String[] args) {
		try {
			if (ArrayUtils.getLength(args) < 3) {
				throw new IllegalArgumentException(getUsage());
			}

			// Read arguments
			String projectName = args[0];
			String outputFolderStr = args[1];
			String modeStr = args[2];
			String[] schemaDependencies = Arrays.copyOfRange(args, 3, args.length);

			// Normalize arguments
			String schemaName;
			File outputFolder;
			StringTokenizer tokenizer;
			List<MigrationConverterMappingTypes> mappingTypeList;
			List<String> schemaDependencyList;
			try {
				schemaName = StringUtils.replace(projectName, "clifton-", "");
				outputFolder = new File(outputFolderStr);
				tokenizer = new StringTokenizer(modeStr, " \t\n\r\f,");
				mappingTypeList = new ArrayList<>();
				while (tokenizer.hasMoreElements()) {
					mappingTypeList.add(MigrationConverterMappingTypes.valueOf(tokenizer.nextToken()));
				}
				schemaDependencyList = Arrays.asList(schemaDependencies);
			}
			catch (Exception e) {
				throw new IllegalArgumentException("An error occurred while processing arguments: " + e.getMessage() + "\n\n" + getUsage(), e);
			}
			LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class,
					"Generating mapping files with parameters:",
					"\n\tProject name:", projectName,
					"\n\tSchema name:", schemaName,
					"\n\tOutput folder:", outputFolderStr,
					"\n\tMapping types:", mappingTypeList,
					"\n\tSchema dependencies:", schemaDependencyList
			));

			TimeTrackedOperation<Void> generateSchemaMappingsOp = TimeUtils.getElapsedTime(() -> generateSchemaMappings(projectName, schemaName, outputFolder, mappingTypeList, schemaDependencyList));
			LogUtils.info(LogCommand.ofMessage(MigrationConverterRunner.class, "Completed schema mapping generation for project [", projectName, "]. Time elapsed: [", generateSchemaMappingsOp.getElapsedSeconds(), "] seconds."));
			System.exit(0);
		}
		catch (Exception e1) {
			try {
				LogUtils.error(LogCommand.ofThrowableAndMessage(MigrationConverterRunner.class, e1, "An error occurred while generating mappings."));
			}
			catch (Exception e2) {
				System.err.println("An exception occurred while executing migrations, but a subsequent error occurred while rendering this exception.");
			}
			finally {
				System.exit(1);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static void generateSchemaMappings(String projectName, String schemaName, File outputFolder, List<MigrationConverterMappingTypes> mappingTypeList, List<String> schemaDependencyList) {
		// Get list of migrations to execute
		TimeTrackedOperation<Resource[]> getMigrationResourcesOp = TimeUtils.getElapsedTime(() -> MigrationUtils.getMigrationResources(projectName));
		Resource[] migrationResources = getMigrationResourcesOp.getResult();
		LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class, "Completed migration resource discovery for project [", projectName, "]. Time elapsed: [", getMigrationResourcesOp.getElapsedSeconds(), "] seconds."));
		if (ArrayUtils.isEmpty(migrationResources)) {
			LogUtils.info(LogCommand.ofMessage(MigrationConverterRunner.class, "No migration resources were found for project [", projectName, "]. Skipping mapping generation."));
			return;
		}

		// Prepare schema loader
		XmlMigrationDefinitionReader reader = new XmlMigrationDefinitionReader();
		reader.setPropertyDataTypeConverter(new PropertyDataTypeConverter());
		SchemaLoader schemaLoader = new SchemaLoader();
		schemaLoader.setReader(reader);

		// Load dependent schemas
		TimeTrackedOperation<Void> loadDependentSchemaListOp = TimeUtils.getElapsedTime(() -> loadDependentSchemaList(schemaLoader, schemaDependencyList));
		LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class, "Completed loading dependent schemas for project [", projectName, "]. Time elapsed: [", loadDependentSchemaListOp.getElapsedSeconds(), "] seconds."));

		// Load project schema
		TimeTrackedOperation<Schema> loadSchemaFromMigrationsOp = TimeUtils.getElapsedTime(() -> loadSchemaFromMigrations(schemaName, migrationResources, schemaLoader));
		Schema projectSchema = loadSchemaFromMigrationsOp.getResult();
		LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class, "Completed loading schema for project [", projectName, "]. Time elapsed: [", loadSchemaFromMigrationsOp.getElapsedSeconds(), "] seconds."));

		// Generate mapping files
		for (MigrationConverterMappingTypes mappingType : mappingTypeList) {
			switch (mappingType) {
				case HIBERNATE:
					createHibernateMappingFile(projectSchema, outputFolder, schemaName, false);
					break;
				case HIBERNATE_IN_MEMORY:
					createHibernateMappingFile(projectSchema, outputFolder, schemaName, true);
					break;
				case SCHEMA_METADATA:
					createSchemaMetadataFile(projectSchema, outputFolder, schemaName);
					break;
			}
		}
	}


	/**
	 * Uses the provided list of dependent schemas which shall be loaded and consolidated.
	 * Returns a {@link SchemaLoader} containing the reader and consolidated schema.
	 */
	private static void loadDependentSchemaList(SchemaLoader schemaLoader, List<String> schemaDependencyList) {
		// load dependent schemas first
		ResourceLoader loader = new DefaultResourceLoader();
		Deque<String> schemaExecutionQueue = new ArrayDeque<>(schemaDependencyList);
		while (!schemaExecutionQueue.isEmpty()) {
			String schemaName = schemaExecutionQueue.pop();
			Schema fullSchemaOnFail = new Schema(schemaLoader.getFullSchema());
			try {
				loadSchemaResource(schemaName, loader, schemaLoader);
			}
			catch (Exception e) {
				String schemaDependency = inferSchemaDependency(e);
				LogUtils.info(LogCommand.ofMessage(MigrationConverterRunner.class, "Loading dependent schema [", schemaName, "] failed. Inferred required schema: [", schemaDependency, "]."));
				// Reattempt to process the schema after loading the schema dependency
				schemaExecutionQueue.push(schemaName);
				schemaExecutionQueue.remove(schemaDependency);
				schemaExecutionQueue.push(schemaDependency);
				schemaLoader.setFullSchema(fullSchemaOnFail);
			}
		}
	}


	/**
	 * Consolidates the schema migration files into the provided {@link SchemaLoader}.
	 * Returns the updated schema.
	 */
	private static Schema loadSchemaFromMigrations(String schemaName, Resource[] migrationResources, SchemaLoader schemaLoader) {
		List<String> existingSchemaDependencyList = new ArrayList<>();
		existingSchemaDependencyList.add(schemaName);
		List<String> resourceLoadedList = new ArrayList<>();
		Schema schema = new Schema();
		Schema schemaOnFail;
		Schema fullSchemaOnFail;
		int currentVersion = 0;
		for (Resource migrationResource : migrationResources) {
			boolean resourceLoaded = false;
			while (!resourceLoaded) {
				String filename = Objects.requireNonNull(migrationResource.getFilename());
				if (resourceLoadedList.contains(migrationResource.getFilename())) {
					throw new RuntimeException(String.format("The migration resource [%s] has already been loaded. Redundant loads are not allowed.", migrationResource.getFilename()));
				}

				// Determine version
				int thisVersion = MigrationUtils.getVersionForFileName(filename);
				if (thisVersion < currentVersion) {
					throw new RuntimeException(String.format("Expected migration file version greater than or equal to [%d], but was version [%d].", currentVersion, thisVersion));
				}
				currentVersion = thisVersion;

				schemaOnFail = new Schema(schema);
				fullSchemaOnFail = new Schema(schemaLoader.getFullSchema());
				try {
					// Load migration
					TimeTrackedOperation<Schema> schemaChangesOp = TimeUtils.getElapsedTime(() -> schemaLoader.getReader().loadMigration(schemaLoader.getFullSchema(), migrationResource));
					Schema changes = schemaChangesOp.getResult();
					LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class, "Loaded migration file for schema [", schemaName, "]. File: [", filename, "]. Time elapsed: [", schemaChangesOp.getElapsedSeconds(), "] seconds."));

					// Update both full schema and new schema object
					schemaLoader.getReader().updateSchema(schemaLoader.getFullSchema(), changes);
					schemaLoader.getReader().updateSchema(schema, changes);
					resourceLoadedList.add(filename);
					resourceLoaded = true;
				}
				catch (Exception e1) {
					// Infer missing schema dependency, load its content, and retry
					String schemaDependency = inferSchemaDependency(e1);
					LogUtils.warn(LogCommand.ofMessage(MigrationConverterRunner.class, "Loading migrations for schema [", schemaName, "] failed. Inferred required schema: [", schemaDependency, "]. Consider including dependent schema as an attached argument to prevent inferred schema dependencies."));
					if (existingSchemaDependencyList.contains(schemaDependency)) {
						throw new RuntimeException(String.format("Dependent schema inference produced a recursive dependency for schema [%s] on dependent schema [%s] for resource file [%s].", schemaName, schemaDependency, filename), e1);
					}
					existingSchemaDependencyList.add(schemaDependency);
					try {
						schema = schemaOnFail;
						schemaLoader.setFullSchema(fullSchemaOnFail);
						loadDependentSchemaList(schemaLoader, Collections.singletonList(schemaDependency));
					}
					catch (Exception e2) {
						RuntimeException exception = new RuntimeException(String.format("An error occurred while reading migration XML files for schema [%s] and resource file [%s].", schemaName, filename), e2);
						exception.addSuppressed(e1); // Include original exception message
						throw exception;
					}
				}
			}
		}
		LogUtils.info(LogCommand.ofMessage(MigrationConverterRunner.class, "Loaded migrations for schema [", schemaName, "]. Latest migration version: [", currentVersion, "]."));
		return Objects.requireNonNull(schema, () -> String.format("Unexpected missing schema result for schema [%s].", schemaName));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static void createSchemaMetadataFile(Schema schema, File outputFolder, String schemaName) {
		// write consolidated migrations file
		File file = new File(outputFolder, "META-INF/schema/schema-clifton-" + schemaName + ".xml");
		try {
			file.getParentFile().mkdirs();
			try (OutputStream stream = new FileOutputStream(file)) {
				XmlMigrationHelper xmlHelper = new XmlMigrationHelper();
				// Pass True for schemaOnly
				// We only need to generate xml in this file for tables, columns, etc.
				// Not for Data, Sql,
				xmlHelper.storeSchema(schema, stream, true);
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Error generating consolidated migrations file: " + file.getPath(), e);
		}
		LogUtils.info(LogCommand.ofMessage(MigrationConverterRunner.class, "Schema updated in file [", file.getPath(), "]."));
	}


	private static void createHibernateMappingFile(Schema schema, File outputFolder, String schemaName, boolean inMemoryDbFile) {
		// write hibernate XML
		String fileName = inMemoryDbFile
				? "schema-hibernate-in-memory-db-" + schemaName + ".xml"
				: "schema-hibernate-" + schemaName + ".xml";
		File file = new File(outputFolder, "META-INF/schema/" + fileName);
		try {
			file.getParentFile().mkdirs();
			try (BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
				HibernateSchemaConverter converter = new HibernateSchemaConverter();
				String xml = converter.convert(schema, inMemoryDbFile);
				writer.write(xml);
				writer.flush();
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Error generating Hibernate mapping file: " + file.getPath(), e);
		}
		LogUtils.info(LogCommand.ofMessage(MigrationConverterRunner.class, "Schema updated in file [", file.getPath(), "]."));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static void loadSchemaResource(String schemaName, ResourceLoader loader, SchemaLoader schemaLoader) {
		String filename = "META-INF/schema/schema-clifton-" + schemaName + ".xml";
		Resource resource = loader.getResource(filename);
		if (!resource.exists()) {
			LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class, "Skipped loading dependent schema [", schemaName, "]. Migration file does not exist."));
			return;
		}

		TimeTrackedOperation<Void> loadMigrationOp = TimeUtils.getElapsedTime(() -> {
			Schema changes = schemaLoader.getReader().loadMigration(schemaLoader.getFullSchema(), resource);
			schemaLoader.getReader().updateSchema(schemaLoader.getFullSchema(), changes);
		});
		LogUtils.debug(LogCommand.ofMessage(MigrationConverterRunner.class, "Loaded dependent schema [", schemaName, "]. File: [", filename, "]. Time elapsed: [", loadMigrationOp.getElapsedSeconds(), "] seconds."));
	}


	/**
	 * Attempts to infer a dependent schema name from the given exception. This method can be used to attempt to resolve missing schema dependencies from an exception message at
	 * runtime.
	 * <p>
	 * This method derives a table name from the first delimited substring in the exception cause message (eligible delimiters: {@code [...]} or {@code '...'}). The inferred
	 * schema name will be the discovered class path schema whose name most closely matches the table name.
	 * <p>
	 * Table matching is done by simulating a schema name which matches the table name with full specificity and then finding the longest matching schema name. For example, the
	 * hypothetical {@code "FixQuickfixTableForMyData"} table would use the simulated schema name {@code "fix-quickfix-table-for-my-data"}, for which the closest schema match may
	 * be {@code "fix-quickfix"}. On the other hand, the hypothetical table {@code "FixTableForMyOtherData"} would use the simulated schema name {@code
	 * "fix-table-for-my-other-data"}, which might then instead match the schema {@code "fix"}.
	 *
	 * @see #CLASSPATH_SCHEMA_NAME_LIST
	 */
	private static String inferSchemaDependency(Exception e) {
		String msg = e.getCause() != null ? e.getCause().getMessage() : null;
		if (msg == null) {
			// Fall-back attempt is unable to proceed; fail immediately
			throw new RuntimeException("An unexpected error occured while attempting to load migrations. The system was unable to infer a schema dependency from this error.", e);
		}

		// Determine table substring range
		final char beginDelim;
		final char endDelim;
		if (msg.indexOf('\'') < 0 || (msg.indexOf('[') > -1 && msg.indexOf('[') < msg.indexOf('\''))) {
			beginDelim = '[';
			endDelim = ']';
		}
		else {
			beginDelim = '\'';
			endDelim = '\'';
		}
		int beginIndex = msg.indexOf(beginDelim);
		int endIndex = msg.indexOf(endDelim, beginIndex + 1);
		if (beginIndex >= endIndex) {
			// Fall-back attempt is unable to proceed; fail immediately
			throw new RuntimeException("An unexpected error occured while attempting to load migrations. The system was unable to infer a schema dependency from this error.", e);
		}

		// Get schema name from table name
		String table = msg.substring(beginIndex + 1, endIndex).trim();
		String fullySpecifiedSchemaName = StringUtils.splitWords(table).toLowerCase().replace(' ', '-');

		// Discover matching schema for table name
		String inferredSchemaDependency = CLASSPATH_SCHEMA_NAME_LIST.get().stream()
				.filter(fullySpecifiedSchemaName::startsWith)
				.max(Comparator.comparing(String::length))
				.orElse(null);

		// Allow schema names without delimiters (e.g., match "marketdata" instead of "market-data" for tables prefixed with "MarketData")
		if (inferredSchemaDependency == null) {
			String nonDelimitedFullySpecifiedSchemaName = table.toLowerCase();
			inferredSchemaDependency = CLASSPATH_SCHEMA_NAME_LIST.get().stream()
					.filter(nonDelimitedFullySpecifiedSchemaName::startsWith)
					.max(Comparator.comparing(String::length))
					.orElseThrow(() -> new RuntimeException(String.format("An unexpected error occurred while attempting to load migrations. An attempt was made to infer a schema dependency from the error, but this attempt failed. No class path schema was found matching the inferred schema name [%s].", fullySpecifiedSchemaName), e));
		}
		return inferredSchemaDependency;
	}


	/**
	 * Gets the list of all names of discoverable schemas on the class path. This is generated by scanning the class path for schema resources.
	 * <p>
	 * <em>Note: Given that this is a full class path resource scan, this is an expensive operation. This should be called sparingly.</em>
	 *
	 * @see #CLASSPATH_SCHEMA_NAME_LIST
	 */
	private static List<String> getClasspathSchemaNameList() {
		PathMatchingResourcePatternResolver resourceResolver = new PathMatchingResourcePatternResolver();
		try {
			Resource[] schemaResources = resourceResolver.getResources("classpath*:META-INF/schema/schema-*clifton-*.xml");
			return Arrays.stream(schemaResources)
					.map(resource -> {
						// Convert file name into schema name
						String filename = Objects.requireNonNull(resource.getFilename());
						String strippedFilename = filename.substring("schema-".length(), filename.lastIndexOf("."));
						return StringUtils.replace(strippedFilename, "clifton-", "");
					})
					.collect(Collectors.toList());
		}
		catch (Exception e) {
			throw new RuntimeException("An error occurred while attempting to locate class path schema names.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the usage string for the main command.
	 */
	private static String getUsage() {
		return MigrationConverterRunner.class.getSimpleName() + ".main(String[]) usage:"
				+ "\n    java <class name> <schema name> <output folder> <mapping types>             "
				+ "\n        <dependencies...>                                                       "
				+ "\n                                                                                "
				+ "\nParameters:                                                                     "
				+ "\n                                                                                "
				+ "\n    <class name>                                                                "
				+ "\n        The name of the current class.                                          "
				+ "\n                                                                                "
				+ "\n    <project name>                                                              "
				+ "\n        The name of the project who's schema which is being processed. This is  "
				+ "\n        used to determine the migration directories used, which must match the  "
				+ "\n        pattern \"<project name>-\\d+\". The output files will be formatted     "
				+ "\n        using the project name, as well.                                        "
				+ "\n                                                                                "
				+ "\n    <output folder>                                                             "
				+ "\n        The directory in which the output files will be created.                "
				+ "\n                                                                                "
				+ "\n    <modes>                                                                     "
				+ "\n        The comma-, newline-, or space-delimited list of mapping types to be    "
				+ "\n        generated. Available modes:                                             "
				+ "\n                                                                                "
				+ "\n        HIBERNATE:                                                              "
				+ "\n            Standard Hibernate schema mappings.                                 "
				+ "\n                                                                                "
				+ "\n        HIBERNATE_IN_MEMORY:                                                    "
				+ "\n            Modified Hibernate schema mappings for use with an in-memory        "
				+ "\n            database.                                                           "
				+ "\n                                                                                "
				+ "\n        SCHEMA_METADATA:                                                        "
				+ "\n            Schema metadata only. This can be ingested by applications at       "
				+ "\n            runtime to provide schema-awareness.                                "
				+ "\n                                                                                "
				+ "\n    <dependencies...>                                                           "
				+ "\n        The list of upstream schemas which are required for generation. This is "
				+ "\n        used as an optimization parameter. The system will attempt to           "
				+ "\n        automatically infer dependent schemas if an error occurs during         "
				+ "\n        execution due to missing schema information.                            ";
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The {@code SchemaLoader} is a wrapper used to maintain the current state of loaded schema/resources.
	 * <p>
	 * In case schemas are out of order we want to be able to recover and retry in the correct order while maintaining what is accurate.
	 *
	 * @author apopp
	 */
	private static class SchemaLoader {

		private XmlMigrationDefinitionReader reader;
		private Schema fullSchema = new Schema();


		public XmlMigrationDefinitionReader getReader() {
			return this.reader;
		}


		public void setReader(XmlMigrationDefinitionReader reader) {
			this.reader = reader;
		}


		public Schema getFullSchema() {
			return this.fullSchema;
		}


		public void setFullSchema(Schema fullSchema) {
			this.fullSchema = fullSchema;
		}
	}


	/**
	 * The schema mapping types to be generated.
	 */
	private enum MigrationConverterMappingTypes {
		/**
		 * Generate standard Hibernate mapping files.
		 */
		HIBERNATE,
		/**
		 * Generate modified in-memory Hibernate mapping files for usage with in-memory databases.
		 */
		HIBERNATE_IN_MEMORY,
		/**
		 * Generate schema metadata files which can be used at runtime to allow applications to be aware of schema details.
		 */
		SCHEMA_METADATA
	}
}
