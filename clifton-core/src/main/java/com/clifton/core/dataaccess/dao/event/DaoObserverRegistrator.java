package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>DaoValidatorRegistrator</code> class is a {@link ApplicationListener} that registers DAO event
 * observers that extend the {@link SelfRegisteringDaoValidator} helper class.
 * <p>
 * Registration is done after application context is refreshed (all beans are loaded).
 *
 * @author vgomelsky
 */
@SuppressWarnings({"unchecked", "rawtypes"})
public class DaoObserverRegistrator implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private DaoLocator daoLocator;

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		registerDaoObservers(getApplicationContext());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void registerDaoObservers(ApplicationContext context) {
		Map<String, Set<String>> dtoClassToBeanMap = new HashMap<>();
		Map<String, SelfRegisteringDaoObserver> eventBeans = context.getBeansOfType(SelfRegisteringDaoObserver.class);
		// for each DAO event observer bean
		for (Map.Entry<String, SelfRegisteringDaoObserver> stringSelfRegisteringDaoObserverEntry : eventBeans.entrySet()) {
			SelfRegisteringDaoObserver eventBean = stringSelfRegisteringDaoObserverEntry.getValue();
			Class dtoClass = DaoUtils.getDtoClass(eventBean);
			if (!IdentityObject.class.isAssignableFrom(dtoClass)) {
				throw new IllegalStateException(dtoClass + " is not a valid DTO class for " + eventBean.getClass() + " DAO observer");
			}
			configureObserver(dtoClass, stringSelfRegisteringDaoObserverEntry.getKey(), eventBean, dtoClassToBeanMap);

			// register additional observers
			for (Class dtoAdditionalClass : (List<Class>) CollectionUtils.getIterable(eventBean.getAdditionalDtoTypeList())) {
				if (!IdentityObject.class.isAssignableFrom(dtoAdditionalClass)) {
					throw new IllegalStateException(dtoAdditionalClass + " is not a valid DTO class for " + eventBean.getClass() + " DAO observer");
				}
				configureObserver(dtoAdditionalClass, stringSelfRegisteringDaoObserverEntry.getKey(), eventBean, dtoClassToBeanMap);
			}
		}
	}


	private <U extends IdentityObject> void configureObserver(Class dtoClass, String beanName, SelfRegisteringDaoObserver eventBean, Map<String, Set<String>> dtoClassToBeanMap) {
		// avoid duplicate registration
		Set<String> registeredBeans = dtoClassToBeanMap.computeIfAbsent(dtoClass.getName(), k -> new HashSet<>());
		if (!registeredBeans.contains(beanName)) {
			registeredBeans.add(beanName);

			ObserverableDAO<U> dao = (ObserverableDAO<U>) getDaoLocator().locate(dtoClass, true);
			if (dao != null && eventBean.getRegisteredDaoEventTypes() != null) {
				if (eventBean.getRegisteredDaoEventTypes().isRead()) {
					dao.registerEventObserver(DaoEventTypes.READ, eventBean);
				}
				if (eventBean.getRegisteredDaoEventTypes().isInsert()) {
					dao.registerEventObserver(DaoEventTypes.INSERT, eventBean);
				}
				if (eventBean.getRegisteredDaoEventTypes().isUpdate()) {
					dao.registerEventObserver(DaoEventTypes.UPDATE, eventBean);
				}
				if (eventBean.getRegisteredDaoEventTypes().isDelete()) {
					dao.registerEventObserver(DaoEventTypes.DELETE, eventBean);
				}
			}
		}
	}


	//////////////////////////////////////////////////////////////////////////// 
	/////////               Getter and Setter Methods                 ////////// 
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
