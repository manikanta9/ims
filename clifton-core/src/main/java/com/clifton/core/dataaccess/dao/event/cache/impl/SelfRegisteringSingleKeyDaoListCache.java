package com.clifton.core.dataaccess.dao.event.cache.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyListCache;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>SelfRegisteringSingleKeyDaoListCache</code> class is a helper class that should be extended by DAO caches that cache a list of entities based on 1 key property
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events. (By default MODIFY (INSERT UPDATE OR DELETE))
 * <p>
 * Note: Clears on inserts because the related entity that the list is stored for needs to know about the new one in the list
 * <p>
 * This cache supports cases where the cache using a single property as its key
 *
 * @param <T>
 * @author Mary Anderson
 */
public abstract class SelfRegisteringSingleKeyDaoListCache<T extends IdentityObject, K> extends SelfRegisteringDaoListCache<T> implements DaoSingleKeyListCache<T, K> {

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	/**
	 * Returns the Bean Key Property Name
	 */
	protected abstract String getBeanKeyProperty();


	/**
	 * Return the Bean Key Property Value for the Bean
	 */
	protected abstract K getBeanKeyValue(T bean);


	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	@Override
	protected String[] getBeanKeyProperties() {
		return new String[]{getBeanKeyProperty()};
	}


	@Override
	protected String getBeanKey(T bean) {
		return getBeanKeyForProperties(getBeanKeyValue(bean));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public final List<T> getBeanListForKeyValue(ReadOnlyDAO<T> dao, K keyPropertyValue) {
		return getBeanListImpl(dao, false, keyPropertyValue);
	}


	@Override
	public final List<T> getBeanListForKeyValueStrict(ReadOnlyDAO<T> dao, K keyPropertyValue) {
		return getBeanListImpl(dao, true, keyPropertyValue);
	}


	@Override
	public final Serializable[] getBeanIdListForKeyValue(ReadOnlyDAO<T> dao, K keyPropertyValue) {
		return getBeanIdListImpl(dao, false, keyPropertyValue);
	}


	@Override
	public void clearBeanListForKeyValue(K keyPropertyValue) {
		clearBeanListImpl(keyPropertyValue);
	}
}
