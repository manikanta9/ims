package com.clifton.core.dataaccess.datatable.bean;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.converter.Converter;


/**
 * @author stevenf
 * @author michaelm
 */
public interface DataTableConverter<F> extends Converter<F, DataTable> {

	public void initialize(DataTableConverterConfiguration configuration);
}


