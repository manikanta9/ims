package com.clifton.core.dataaccess.search.hibernate;


import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import com.clifton.core.dataaccess.search.OrderByDirections;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * The <code>MathOrderBy</code> is used for grouping multiple fields together in a math function for sorting.  For example, sorting by the multiplication of 2 fields
 * Uses (propertyValue * property2Value), etc.
 *
 * @author manderson
 */
public class MathOrderBy extends Order {

	private final String mathFunction;

	private final String[] propertyNames;

	/**
	 * If true, will do ISNULL(Value, 0) for each field to prevent math with NULL that returns NULL
	 */
	private final boolean supportNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructor for Order.
	 */
	private MathOrderBy(String mathFunction, boolean supportNull, String[] propertyNames, boolean ascending) {
		super(null, ascending);
		this.mathFunction = mathFunction;
		this.supportNull = supportNull;
		this.propertyNames = propertyNames;
	}


	public static MathOrderBy create(OrderByDirections direction, String mathFunction, boolean supportNull, String[] propertyNames) {
		return new MathOrderBy(mathFunction, supportNull, propertyNames, direction == OrderByDirections.ASC);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragmentSb = new StringBuilder("(");
		for (int i = 0; i < this.propertyNames.length; i++) {
			if (this.supportNull) {
				fragmentSb.append(SQLIfNullFunction.render(criteria, criteriaQuery, this.propertyNames[i], "0"));
			}
			else {
				fragmentSb.append(criteriaQuery.getColumn(criteria, this.propertyNames[i]));
			}
			if (i < this.propertyNames.length - 1) {
				fragmentSb.append(this.mathFunction);
			}
		}
		fragmentSb.append(") ");
		fragmentSb.append(isAscending() ? "asc" : "desc");
		return fragmentSb.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getPropertyNames() {
		return this.propertyNames;
	}
}
