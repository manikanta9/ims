package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringFlexibleDaoCache;


/**
 * The interface for {@link SelfRegisteringFlexibleDaoCache} for single-key caches.
 *
 * @author MikeH
 */
public interface DaoSingleKeyFlexibleCache<T extends IdentityObject, U extends Comparable<U>, K> {

	/**
	 * Retrieves the latest entity for the given key value as of the given order value.
	 */
	public T getBeanForKeyValue(ReadOnlyDAO<T> dao, U order, K keyValue);


	/**
	 * Retrieves the latest entity for the given key value as of the given order value.
	 * <p>
	 * An exception will be thrown if the entity is not found.
	 */
	public T getBeanForKeyValueStrict(ReadOnlyDAO<T> dao, U order, K keyValue);
}
