package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.observer.Observer;
import com.clifton.core.util.observer.ObserverHandler;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanNameAware;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import java.util.List;


/**
 * The <code>ObserverableDAOImpl</code> class should be extended by DAO implementations that need to support
 * event handling.  Implementing classes must raise before/after events using getEventObserver method.
 *
 * @author vgomelsky
 */
public abstract class ObserverableDAOImpl<T extends IdentityObject> implements ObserverableDAO<T>, ApplicationContextAware, BeanNameAware {

	////////////////////////////////////////////////////////////////////////////
	////////             DaoEventRegistrator Methods               /////////////
	////////////////////////////////////////////////////////////////////////////

	private ObserverHandler readObserverHandler;
	private ObserverHandler insertObserverHandler;
	private ObserverHandler updateObserverHandler;
	private ObserverHandler deleteObserverHandler;

	private boolean observingEnabled = true;
	private ApplicationContext applicationContext;
	private String beanName;


	@Override
	public boolean isObservingEnabled() {
		return this.observingEnabled;
	}


	@Override
	public void setObservingEnabled(boolean observingEnabled) {
		this.observingEnabled = observingEnabled;
	}


	@Override
	@SuppressWarnings("unchecked")
	public DaoEventObserver<T> getEventObserver(DaoEventTypes event) {
		if (DaoEventTypes.INSERT == event) {
			return getInsertObserverHandler().getNotifier(DaoEventObserver.class, DaoUtils.observerPredicate);
		}
		if (DaoEventTypes.UPDATE == event) {
			return getUpdateObserverHandler().getNotifier(DaoEventObserver.class, DaoUtils.observerPredicate);
		}
		if (DaoEventTypes.DELETE == event) {
			return getDeleteObserverHandler().getNotifier(DaoEventObserver.class, DaoUtils.observerPredicate);
		}
		if (DaoEventTypes.READ == event) {
			return getReadObserverHandler().getNotifier(DaoEventObserver.class, DaoUtils.observerPredicate);
		}
		throw new IllegalArgumentException("Cannot find ObserverHandler for " + event);
	}


	@Override
	public void registerEventObserver(DaoEventTypes event, DaoEventObserver<T> eventObserver) {
		boolean success;
		if (DaoEventTypes.INSERT == event) {
			if (this.insertObserverHandler == null) {
				this.insertObserverHandler = (ObserverHandler) this.applicationContext.getBean("prototypeObserverHandler");
			}
			success = this.insertObserverHandler.register(DaoEventObserver.class, eventObserver);
		}
		else if (DaoEventTypes.UPDATE == event) {
			if (this.updateObserverHandler == null) {
				this.updateObserverHandler = (ObserverHandler) this.applicationContext.getBean("prototypeObserverHandler");
			}
			success = this.updateObserverHandler.register(DaoEventObserver.class, eventObserver);
		}
		else if (DaoEventTypes.DELETE == event) {
			if (this.deleteObserverHandler == null) {
				this.deleteObserverHandler = (ObserverHandler) this.applicationContext.getBean("prototypeObserverHandler");
			}
			success = this.deleteObserverHandler.register(DaoEventObserver.class, eventObserver);
		}
		else if (DaoEventTypes.READ == event) {
			if (this.readObserverHandler == null) {
				this.readObserverHandler = (ObserverHandler) this.applicationContext.getBean("prototypeObserverHandler");
			}
			success = this.readObserverHandler.register(DaoEventObserver.class, eventObserver);
		}
		else {
			throw new IllegalArgumentException("Cannot register DaoEventObserver for " + event);
		}
		if (success) {
			LogUtils.debug(getClass(), "DAO: " + getBeanName() + " Event: " + event + " Registered observer: " + eventObserver);
		}
	}


	@Override
	public void unregisterEventObserver(DaoEventTypes event, DaoEventObserver<T> eventObserver) {
		Observer observer = null;
		if (DaoEventTypes.INSERT == event) {
			if (this.insertObserverHandler != null) {
				observer = this.insertObserverHandler.unregister(DaoEventObserver.class, eventObserver);
			}
		}
		else if (DaoEventTypes.UPDATE == event) {
			if (this.updateObserverHandler != null) {
				observer = this.updateObserverHandler.unregister(DaoEventObserver.class, eventObserver);
			}
		}
		else if (DaoEventTypes.DELETE == event) {
			if (this.deleteObserverHandler != null) {
				observer = this.deleteObserverHandler.unregister(DaoEventObserver.class, eventObserver);
			}
		}
		else if (DaoEventTypes.READ == event) {
			if (this.readObserverHandler != null) {
				observer = this.readObserverHandler.unregister(DaoEventObserver.class, eventObserver);
			}
		}
		else {
			throw new IllegalArgumentException("Cannot unregister DaoEventObserver for " + event);
		}
		if (observer != null) {
			LogUtils.debug(getClass(), "DAO: " + getBeanName() + " Event: " + event + " Unregistered observer: " + eventObserver);
		}
	}


	@Override
	public boolean isReadObserverRegistered() {
		return isObservingEnabled() && (getReadObserverHandler() != null);
	}


	@Override
	public boolean isInsertObserverRegistered() {
		return isObservingEnabled() && (getInsertObserverHandler() != null);
	}


	@Override
	public boolean isInsertObserverRegisteredForObserver(DaoEventObserver<T> eventObserver) {
		if (isInsertObserverRegistered()) {
			return getInsertObserverHandler().isRegistered(DaoEventObserver.class, eventObserver);
		}
		return false;
	}


	@Override
	public List<Observer> getInsertObserversRegistered() {
		if (isInsertObserverRegistered()) {
			return getInsertObserverHandler().getRegisteredObservers();
		}
		return null;
	}


	@Override
	public boolean isUpdateObserverRegistered() {
		return isObservingEnabled() && (getUpdateObserverHandler() != null);
	}


	@Override
	public boolean isUpdateObserverRegisteredForObserver(DaoEventObserver<T> eventObserver) {
		if (isUpdateObserverRegistered()) {
			return getUpdateObserverHandler().isRegistered(DaoEventObserver.class, eventObserver);
		}
		return false;
	}


	@Override
	public List<Observer> getUpdateObserversRegistered() {
		if (isUpdateObserverRegistered()) {
			return getUpdateObserverHandler().getRegisteredObservers();
		}
		return null;
	}


	@Override
	public boolean isDeleteObserverRegistered() {
		return isObservingEnabled() && (getDeleteObserverHandler() != null);
	}


	@Override
	public boolean isDeleteObserverRegisteredForObserver(DaoEventObserver<T> eventObserver) {
		if (isDeleteObserverRegistered()) {
			return getDeleteObserverHandler().isRegistered(DaoEventObserver.class, eventObserver);
		}
		return false;
	}


	@Override
	public List<Observer> getDeleteObserversRegistered() {
		if (isDeleteObserverRegistered()) {
			return getDeleteObserverHandler().getRegisteredObservers();
		}
		return null;
	}

	////////////////////////////////////////////////////////////////////////////
	////////           ApplicationContextAware Methods             /////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                 BeanNameAware Methods                 /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanName() {
		return this.beanName;
	}


	@Override
	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                  Getters and Setters                  /////////////
	////////////////////////////////////////////////////////////////////////////


	public ObserverHandler getReadObserverHandler() {
		return this.readObserverHandler;
	}


	public ObserverHandler getInsertObserverHandler() {
		return this.insertObserverHandler;
	}


	public ObserverHandler getUpdateObserverHandler() {
		return this.updateObserverHandler;
	}


	public ObserverHandler getDeleteObserverHandler() {
		return this.deleteObserverHandler;
	}
}
