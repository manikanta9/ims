package com.clifton.core.dataaccess.db;


import com.clifton.core.util.converter.Converter;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.OffsetDateTime;
import java.util.Date;


/**
 * The <code>ClassToSqlTypeConverter</code> class returns java.sql.Types that corresponds to
 * the specified class.
 *
 * @author vgomelsky
 */
public class ClassToSqlTypeConverter implements Converter<Class<?>, Integer> {

	@Override
	public Integer convert(Class<?> clazz) {
		if (clazz == String.class) {
			return Types.VARCHAR;
		}
		if (clazz == int.class || clazz == Integer.class) {
			return Types.INTEGER;
		}
		if (clazz == short.class || clazz == Short.class) {
			return Types.SMALLINT;
		}

		if (clazz == long.class || clazz == Long.class) {
			return Types.BIGINT;
		}
		if (clazz == float.class || clazz == double.class || clazz == BigDecimal.class || clazz == Double.class) {
			return Types.DECIMAL;
		}
		if (clazz == boolean.class || clazz == Boolean.class) {
			return Types.BIT;
		}
		if (clazz == Date.class || clazz == OffsetDateTime.class) {
			return Types.TIMESTAMP;
		}
		if (clazz == Timestamp.class) {
			return Types.TIMESTAMP;
		}

		throw new IllegalArgumentException("Do not know what java.sql.Types corresponds to " + clazz);
	}
}
