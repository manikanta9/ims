package com.clifton.core.dataaccess.dao;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.validation.CauseEntityAware;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


/**
 * The <code>DaoException</code> class represents a DAO (Data Access Object) exception
 * that in addition to the error message and cause also contains offending DTO.
 *
 * @author vgomelsky
 */
public class DaoException extends RuntimeException implements CauseEntityAware {

	@JsonTypeInfo(use = JsonTypeInfo.Id.CLASS)
	private final IdentityObject causeEntity;


	@JsonCreator
	public DaoException(@JsonProperty("message") String message, @JsonProperty("causeEntity") IdentityObject causeEntity, @JsonProperty("cause") Throwable cause) {
		super(message, cause);
		this.causeEntity = causeEntity;
	}


	@Override
	public IdentityObject getCauseEntity() {
		return this.causeEntity;
	}
}
