package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.hibernate.Session;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>HibernateUpdatableBatchDAO</code> class provides DAO batch implementation based on Hibernate.
 * <p>
 * By default, the HibernateUpdatableBatchDAO simply more directly manages the session cache through manually
 * clearing and flushing based on the jdbc_batch_size parameter;  Observers are still triggered during saves
 * by default, and the executing code should call the appropriate methods with DaoUtils.executeWithAllObserversDisabled
 * when appropriate for the most efficient batch performance.
 *
 * @param <T>
 * @author StevenF
 */
public class HibernateUpdatableBatchDAO<T extends IdentityObject> extends HibernateUpdatableDAO<T> {

	@Override
	@Transactional
	public void saveList(List<T> beanList) {
		if (!CollectionUtils.isEmpty(beanList)) {
			int batchSize = getSessionFactory().getSessionFactoryOptions().getJdbcBatchSize();
			LogUtils.info(getClass(), "Batch Size: " + batchSize);
			for (int i = 0; i < beanList.size(); i++) {
				beanList.set(i, save(beanList.get(i), false));
				if (i % batchSize == 0) {
					LogUtils.info(getClass(), "Current Batch Index: " + i + " - flushing/clearing session.");
					//flush a batch of inserts and release memory:
					flushSession();
					clearSession();
				}
			}
			flushSession();
			clearSession();
		}
	}


	@Override
	@Transactional
	public void saveList(List<T> newList, List<T> oldList) {
		int batchSize = getSessionFactory().getSessionFactoryOptions().getJdbcBatchSize();
		for (int i = 0; i < CollectionUtils.getSize(newList); i++) {
			newList.set(i, save(newList.get(i), false));
			if (i % batchSize == 0) {
				//flush a batch of inserts and release memory:
				flushSession();
				clearSession();
			}
		}
		for (T old : CollectionUtils.getIterable(oldList)) {
			if (newList == null || !newList.contains(old)) {
				delete(old);
			}
		}
		//flush a batch of inserts and release memory:
		flushSession();
		clearSession();
	}


	@Override
	public T save(T bean) {
		return save(bean, true);
	}


	/**
	 * Because batching needs to manage the L1 cache manually, we want to control when we flush.
	 * If save is called from the 'saveList' method, we manage the flushing and clearing of
	 * the session within that method; rather than in the individual save to optimize batching;
	 * however, if save is called directly from external code within a loop we want to flush
	 * and clear after each iteration; while this is slightly less efficient, it will prevent
	 * the accidental occurrence of an OutOfMemoryException on large lists.
	 */
	private T save(T bean, boolean resetSession) {
		bean = super.save(bean);
		if (resetSession) {
			flushSession();
			clearSession();
		}
		return bean;
	}


	@Override
	public void delete(Serializable primaryKey) {
		T bean = findByPrimaryKey(primaryKey);
		AssertUtils.assertNotNull(bean, "Cannot find bean to delete with primary key: " + primaryKey);
		delete(bean, true);
	}


	@Override
	public void delete(T bean) {
		delete(bean, true);
	}


	/**
	 * Because batching needs to manage the L1 cache manually, we want to control when we flush.
	 * If delete is called from the 'deleteList' method, we manage the flushing and clearing of
	 * the session within that method; rather than in the individual delete to optimize batching;
	 * however, if delete is called directly from external code within a loop we want to flush
	 * and clear after each iteration; while this is slightly less efficient, it will prevent
	 * the accidental occurrence of an OutOfMemoryException on large lists.
	 */
	private void delete(T bean, boolean resetSession) {
		super.delete(bean);
		if (resetSession) {
			flushSession();
			clearSession();
		}
	}


	@Override
	@Transactional
	public void deleteList(List<T> deleteList) {
		if (!CollectionUtils.isEmpty(deleteList)) {
			int batchSize = getSessionFactory().getSessionFactoryOptions().getJdbcBatchSize();
			Session session = getSessionFactory().getCurrentSession();
			for (int i = 0; i < deleteList.size(); i++) {
				delete(deleteList.get(i), false);
				if (i % batchSize == 0) {
					//flush a batch of inserts and release memory:
					session.flush();
					session.clear();
				}
			}
			//Flush and clear now that list has been completely processed
			session.flush();
			session.clear();
		}
	}
}
