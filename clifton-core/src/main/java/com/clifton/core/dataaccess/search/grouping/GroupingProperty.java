package com.clifton.core.dataaccess.search.grouping;

import java.io.Serializable;


public class GroupingProperty implements Serializable {

	/**
	 * This is used for grouping in the Projection.
	 * If it's multiple levels deep appropriate LEFT JOINs are added
	 */
	private String beanPropertyName;

	/**
	 * Once a {@link GroupedHierarchicalResult} is returned that result can be used to filter the list of details within that group
	 * In order to properly filter, the search field name from the search form is needed.
	 * For example, for an Order Allocation - the beanProperty for grouping on order destination is orderDestination.id, but the filter on the search form is orderDestinationId
	 */
	private String searchFieldName;

	/**
	 * The label of the property, i.e. Order Destination
	 */
	private String label;

	/**
	 * If the value is missing what to display for empty.  Can be NONE, Not Applicable, Multiple depending on the context
	 */
	private String emptyValueLabel;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanPropertyName() {
		return this.beanPropertyName;
	}


	public void setBeanPropertyName(String beanPropertyName) {
		this.beanPropertyName = beanPropertyName;
	}


	public String getSearchFieldName() {
		return this.searchFieldName;
	}


	public void setSearchFieldName(String searchFieldName) {
		this.searchFieldName = searchFieldName;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getEmptyValueLabel() {
		return this.emptyValueLabel;
	}


	public void setEmptyValueLabel(String emptyValueLabel) {
		this.emptyValueLabel = emptyValueLabel;
	}
}
