package com.clifton.core.dataaccess.migrate.reader;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.cache.CacheTypes;
import com.clifton.core.dataaccess.dao.hibernate.GenericCompressedTextUserType;
import com.clifton.core.dataaccess.dao.hibernate.GenericCustomJsonObjectUserType;
import com.clifton.core.dataaccess.dao.hibernate.GenericEnumUserType;
import com.clifton.core.dataaccess.db.DataTypeEncodingTypeNames;
import com.clifton.core.dataaccess.db.PropertyNameConverter;
import com.clifton.core.dataaccess.db.TableTypes;
import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Constraint;
import com.clifton.core.dataaccess.migrate.schema.Index;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.migrate.schema.dml.Data;
import com.clifton.core.dataaccess.migrate.schema.dml.Value;
import com.clifton.core.dataaccess.migrate.schema.sql.Sql;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.beans.PropertyDescriptor;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * The <code>XmlMigrationDefinitionReader</code> implements reading and loading of database migrations from XML.
 * Migrations can be used to either generate SQL or for ORM mappings by DAO components.
 *
 * @author vgomelsky
 */
@Component
public class XmlMigrationDefinitionReader implements MigrationDefinitionReader {

	private static final String PERCENT_COLUMN_REVERSABLE_CONVERTER = "percentColumnReversableConverter";
	private String defaultDataSourceName = "dataSource";
	private DataTypeEncodingTypeNames defaultDataTypeEncodingType = DataTypeEncodingTypeNames.UCS_2;
	private CacheTypes defaultCacheType;
	private Converter<PropertyDescriptor, DataTypes> propertyDataTypeConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * RULES:
	 * <ul>
	 * <li>default type=BasicTable: PK, record stamp, default data source
	 * <li>primary key based on naming convention</li>
	 * <li>index each foreign key</li>
	 * </ul>
	 * Uses default datasource name where not explicitly set.
	 */
	@Override
	public Schema loadMigration(Schema currentSchema, String fileName) {
		return loadMigration(currentSchema, fileName, getDefaultDataSourceName());
	}


	@Override
	public Schema loadMigration(Schema currentSchema, String fileName, String dataSourceName) {
		try {
			ResourceLoader loader = new DefaultResourceLoader();
			return loadMigration(currentSchema, loader.getResource(fileName), dataSourceName);
		}
		catch (Exception e) {
			throw new RuntimeException("Error loading migration file: " + fileName, e);
		}
	}


	@Override
	public Schema loadMigration(Schema currentSchema, Resource resource) {
		return loadMigration(currentSchema, resource, getDefaultDataSourceName());
	}


	@Override
	public Schema loadMigration(Schema currentSchema, Resource resource, String dataSourceName) {
		try {
			// 1. read schema from XML
			XmlMigrationHelper xmlHelper = new XmlMigrationHelper();
			Schema schema = xmlHelper.loadFromXml(resource);

			// 2. populate missing information based on defaults
			populateSchema(currentSchema, schema, dataSourceName);

			return schema;
		}
		catch (Exception e) {
			String uri;
			try {
				uri = resource.getURI().toString();
			}
			catch (IOException e1) {
				uri = "ERROR READING URI: " + e1.getMessage();
			}
			throw new RuntimeException("Error loading migration for dataSource: " + dataSourceName + " and file: " + resource.getFilename() + " [URI = " + uri + "]", e);
		}
	}


	/**
	 * Populates defaults and validates schemaUpdates.  The updates may reference currentSchema.
	 */
	private void populateSchema(Schema currentSchema, Schema schemaUpdates, String dataSourceName) {
		// Process all Table & Column Definitions (Skipping Delayed Columns)
		processTables(currentSchema, schemaUpdates, false, dataSourceName);
		// Process all of the delayed Table Columns
		processTables(currentSchema, schemaUpdates, true, dataSourceName);

		for (Data data : CollectionUtils.getIterable(schemaUpdates.getDataList())) {
			try {
				Table table = null;
				if (currentSchema != null) {
					table = currentSchema.getTable(data.getTable());
				}
				if (table == null) {
					table = schemaUpdates.getTable(data.getTable());
				}
				if (table == null) {
					throw new RuntimeException(String.format("The table [%s] cannot be found.", data.getTable()));
				}

				for (Value value : CollectionUtils.getIterable(data.getValueList())) {
					Column column = table.getColumn(value.getColumn());
					if (column == null) {
						throw new RuntimeException(String.format("Cannot find column [%s] for table [%s] while attempting to insert value [%s].", value.getColumn(), data.getTable(), value.getValue()));
					}
					if (column.isCalculatedColumn()) {
						throw new RuntimeException(String.format("Column [%s] for table [%s] is a calculated column and cannot be explicitly inserted with value [%s].", value.getColumn(), data.getTable(), value.getValue()));
					}
					value.setDataType(column.getDataType());
					validateValue(table, column, value);
				}
			}
			catch (Exception e) {
				throw new RuntimeException(String.format("An error occurred while processing data row for table [%s].", data.getTable()), e);
			}
		}

		for (Sql sql : CollectionUtils.getIterable(schemaUpdates.getSqlList())) {
			if (!StringUtils.isEmpty(sql.getFileName())) {
				if (!CollectionUtils.isEmpty(sql.getStatementList())) {
					throw new RuntimeException("Must specify a SQL file to run OR explicitly SQL statements, but not both.");
				}
			}
			if (StringUtils.isEmpty(sql.getFileName())) {
				if (CollectionUtils.isEmpty(sql.getStatementList())) {
					throw new RuntimeException("Must specify a SQL file to run OR explicitly SQL statements, but not both.");
				}
			}

			if (!StringUtils.isEmpty(sql.getRunWith())) {
				MigrationSchemaConverterTypes runWithType = MigrationSchemaConverterTypes.valueOf(sql.getRunWith());
				if (!runWithType.isRunWithAllowed()) {
					StringBuilder msg = new StringBuilder("SQL RunWith Option of [" + sql.getRunWith() + "] is invalid.  Valid Options are [");
					boolean found = false;
					for (MigrationSchemaConverterTypes type : MigrationSchemaConverterTypes.values()) {
						if (type.isRunWithAllowed()) {
							if (found) {
								msg.append(", ");
							}
							msg.append(type.name());
							found = true;
						}
					}
					msg.append("].");
					throw new RuntimeException(msg.toString());
				}
			}
		}
	}


	private void validateValue(Table table, Column column, Value value) {
		if (StringUtils.isEmpty(value.getValue()) || value.isSelect()) {
			return;
		}
		if (DataTypeNames.STRING != column.getDataType().getTypeName()) {
			return;
		}
		if (value.getValue().length() > column.getMaxLength()) {
			throw new RuntimeException(String.format("Invalid data for table [%s] and column [%s]. Max length is [%d], but value [%s] has a length of [%d].", table.getName(), column.getName(), column.getMaxLength(), value.getValue(), value.getValue().length()));
		}
	}


	/**
	 * The first pass processes all Tables & Columns, Constraints, Indexes, etc (skipping all FK Field
	 * columns & their respective constraints that are marked as delayed)
	 * The second pass processes just those delayed columns & their constraints.
	 */
	private void processTables(Schema currentSchema, Schema schemaUpdates, boolean processDelayed, String dataSourceName) {
		for (Table table : CollectionUtils.getIterable(schemaUpdates.getTableList())) {
			Table currentTable = getCurrentTable(currentSchema, table);

			// These are only ran when not processing Delayed Columns
			if (!processDelayed) {
				// add standard columns: primary key, record stamp, etc.
				populateDefaults(table, currentTable, dataSourceName);

				// expand table level index definitions
				processTableLevelIndexDefinitions(table);

				// expand table level constraint definitions
				processTableLevelConstraintDefinitions(schemaUpdates, table);
			}

			// expand column information
			processColumnDefinitions(table, table, currentTable, currentSchema, schemaUpdates, processDelayed);

			// expends subclass column definitions
			for (Subclass subclass : CollectionUtils.getIterable(table.getSubclassList())) {
				processColumnDefinitions(table, subclass, currentTable, currentSchema, schemaUpdates, processDelayed);
			}

			processNaturalKeyDefinitions(table);
		}
	}


	@Override
	public void updateSchema(Schema currentSchema, Schema schemaUpdates) {
		for (Table table : CollectionUtils.getIterable(schemaUpdates.getTableList())) {
			Table currentTable = getCurrentTable(currentSchema, table);
			if (table.getOperationType() == null || table.getOperationType() == OperationTypes.CREATE) {
				AssertUtils.assertNull(currentTable, "Cannot CREATE table " + table + " because it already exists in the current schema.");
				currentSchema.getTableList().add(table);
			}
			else if (table.getOperationType() == OperationTypes.CREATE_SQL_ONLY) {
				AssertUtils.assertNotNull(currentTable, "Cannot run CREATE_SQL_ONLY migration for Table '" + table + "' because original migration was not updated to include this table.");
			}
			else if (table.getOperationType() == OperationTypes.DELETE) {
				if (CollectionUtils.getSize(table.getColumnList()) == 0) {
					currentSchema.removeTable(currentTable);
				}
			}
			else if (table.getOperationType() == OperationTypes.UPDATE) {
				AssertUtils.assertNotNull(currentTable, "Cannot UPDATE table " + table + " because it does not exist in the current schema.");
			}
		}

		for (Data data : CollectionUtils.getIterable(schemaUpdates.getDataList())) {
			currentSchema.addData(data);
		}
	}


	protected void processColumnDefinitions(Table mainTable, Table table, Table currentTable, Schema currentSchema, Schema schemaUpdates, boolean processDelayed) {

		Map<String, PropertyDescriptor> beanProperties = getDtoPropertyMap(currentTable, table);
		PropertyNameConverter nameConverter = new PropertyNameConverter(beanProperties, table.getName());

		for (Column column : CollectionUtils.getIterable(table.getColumnList())) {
			if (processDelayed != column.isDelayed()) {
				continue;
			}
			if (table.getOperationType() == OperationTypes.DELETE) {
				if (!currentTable.removeColumn(column)) {
					throw new RuntimeException("Cannot remove column '" + column.getName() + "' because it's not found on current table '" + table.getName() + "'");
				}
			}
			else {
				if (table instanceof Subclass) {
					if (column.getColumnRequired() == null) {
						column.setRequired(false);
					}
					else if (column.getColumnRequired()) {
						throw new RuntimeException("Subclass columns cannot be required.  Please check [" + table.getDtoClass() + "] column [" + column.getName() + "]");
					}
				}
				else if (column.getColumnRequired() == null) {
					column.setRequired(true);
				}

				if (column.isParentField() && !column.getColumnRequired()) {
					throw new RuntimeException("Parent columns must be required.  Please check [" + table.getDtoClass() + "] column [" + column.getName() + "]");
				}

				// map DB fields to DTO properties and make sure all match
				if (column.getBeanPropertyName() != null) {
					if (!beanProperties.containsKey(column.getBeanPropertyName())) {
						throw new RuntimeException("Bean property '" + column.getBeanPropertyName() + "' is not found on bean " + table.getDtoClass());
					}
				}
				else {
					// need to find a matching property based on column name
					column.setBeanPropertyName(nameConverter.convert(column.getName()));
				}

				PropertyDescriptor propertyDescriptor = beanProperties.get(column.getBeanPropertyName());
				if (propertyDescriptor.getPropertyType().isEnum()) {
					//Enums are automatically set as indexed.
					if (column.getIndexed() == null) {
						column.setIndexed(true);
					}
					// Type definition names can be inferred for enums with default type definition parameters
					if (column.getTypeDefinitionName() == null && column.getTypeDefinitionClassName() == null && column.getTypeDefinitionConverterClass() == null) {
						Type propertyType = propertyDescriptor.getReadMethod().getGenericReturnType();
						String propertyTypeCanonicalName = CoreClassUtils.getCanonicalName(propertyType);
						column.setTypeDefinitionName(propertyTypeCanonicalName.replaceAll("[.<>, ]", "_"));
					}
				}

				// Configure types which require type definitions
				if (propertyDescriptor.getPropertyType().isEnum() || column.getDataType() == DataTypes.COMPRESSED_TEXT || column.getDataType() == DataTypes.CUSTOM_JSON_STRING) {
					/*
					 * Enums and compressed text values require custom type definitions for serialization and deserialization. This block infers relevant parameters from the target
					 * class field. These may be overridden for specific use-cases, such as targeting deserialization to subclasses or to other desired types (e.g. Strings).
					 */
					// Set default properties
					if (column.getTypeDefinitionClassName() == null) {
						Type propertyType = propertyDescriptor.getReadMethod().getGenericReturnType();
						column.setTypeDefinitionClassName(CoreClassUtils.getCanonicalName(propertyType));
					}
					if (column.getTypeDefinitionConverterClass() == null) {
						if (propertyDescriptor.getPropertyType().isEnum()) {
							column.setTypeDefinitionConverterClass(GenericEnumUserType.class.getName());
						}
						else if (column.getDataType() == DataTypes.COMPRESSED_TEXT) {
							column.setTypeDefinitionConverterClass(GenericCompressedTextUserType.class.getName());
						}
						else if (column.getDataType() == DataTypes.CUSTOM_JSON_STRING) {
							column.setTypeDefinitionConverterClass(GenericCustomJsonObjectUserType.class.getName());
						}
						else {
							throw new RuntimeException(String.format("The type definition for table [%s] and column [%s] has no type definition converter class and one cannot be inferred.", column.getTableName(), column.getName()));
						}
					}
				}

				// get DB data types
				propertyDescriptor = beanProperties.remove(column.getBeanPropertyName());
				if (column.getDataType() == null) {
					column.setDataType(getPropertyDataTypeConverter().convert(propertyDescriptor));
				}

				if (column.getDataType().getTypeName().isSupportEncoding()) {
					if (column.getDataTypeEncodingType() == null) {
						if (mainTable.getDefaultDataTypeEncodingType() != null) {
							column.setDataTypeEncodingType(mainTable.getDefaultDataTypeEncodingType());
						}
						else {
							column.setDataTypeEncodingType(getDefaultDataTypeEncodingType());
						}
					}
				}
				else if (column.getDataTypeEncodingType() != null) {
					throw new RuntimeException(String.format("Data Type Encoding is not supported for data type [%s] used by table [%s] and column [%s].", column.getDataType().getTypeName().name(), column.getTableName(), column.getName()));
				}

				// exclude auto-generated PKs from inserts and updates
				if (column.getDataType().isIdentity()) {
					column.setExcludedFromUpdates(true);
					column.setExcludedFromInserts(true);
				}
				if (column.getDataType().isNaturalKey()) {
					column.setExcludedFromUpdates(true);
				}

				// set default values
				if (column.getDefaultValue() == null && column.getDataType() == DataTypes.BIT && column.getColumnRequired()) {
					column.setDefaultValue("0");
				}

				// Default Percentage Formatting for Exports/Imports
				if (column.getUploadValueConverterName() == null && column.getDataType().isPercent()) {
					column.setUploadValueConverterName(PERCENT_COLUMN_REVERSABLE_CONVERTER);
				}

				// Validate nullable columns are not int
				// NOTE: Not checked for Subclasses as often times a field is really required, but must be nullable in the Database because
				// it doesn't apply to one of the shared DTO classes for the table.
				// Example: SystemColumnCustom has order & required fields, however these properties do not exist
				// on the SystemColumnStandard bean.
				if (!(table instanceof Subclass)) {
					if (propertyDescriptor.getPropertyType().isPrimitive() && !column.isCalculatedColumn() && !(column.getColumnRequired() || column.getColumnRequiredForValidation())) {
						throw new RuntimeException("Table '" + table.getName() + "' Column '" + column.getName() + "' is nullable, but the bean property is primitive type '"
								+ propertyDescriptor.getPropertyType().getName() + "'.  Either change the field to required or change the bean property to it's Object equivalent.");
					}
				}

				// update current schema
				if (table.getOperationType() == OperationTypes.UPDATE) {
					if (currentTable.getColumn(column.getName()) != null) {
						throw new RuntimeException("Table '" + table.getName() + "' already has column '" + column.getName() + "'");
					}
					currentTable.addColumn(column);
				}
				// update sql only - confirm column already defined in schema
				if (table.getOperationType() == OperationTypes.UPDATE_SQL_ONLY) {
					if (currentTable.getColumn(column.getName()) == null) {
						throw new RuntimeException("Cannot run UPDATE_SQL_ONLY migration for Table '" + table.getName() + "' because original migration was not updated to include new column '" + column.getName() + "'");
					}
				}
			}

			// expand FK's defined as column attributes
			if (processDelayed == column.isDelayed()) {
				processColumnLevelConstraintDefinitions(currentTable, schemaUpdates, currentSchema, mainTable, column);
			}

			// expand indexes defined as column attributes
			processColumnLevelIndexDefinitions(currentTable, mainTable, column);
		}
	}


	/**
	 * Depending on table type, add corresponding columns if necessary.
	 */
	private void populateDefaults(Table table, Table currentTable, String dataSourceName) {
		if (StringUtils.isEmpty(table.getDataSource())) {
			table.setDataSource(dataSourceName);
		}
		if (table.getCacheType() == null) {
			table.setCacheType(getDefaultCacheType());
		}

		// add columns only when creating a new table
		if (table.getOperationType() != null && table.getOperationType() != OperationTypes.CREATE && table.getOperationType() != OperationTypes.CREATE_SQL_ONLY) {
			return;
		}
		if (table.getTableType() == null) {
			table.setTableType(TableTypes.BASIC);
		}

		if (table.getTableType().isAppendStandardFields()) {
			Column column;
			// add primary key as first field
			String pkName = table.getName() + "ID";
			if (table.getColumn(pkName) == null) {
				column = new Column();
				column.setName(pkName);

				Map<String, PropertyDescriptor> beanProperties = getDtoPropertyMap(currentTable, table);
				if (beanProperties.get("id") == null) {
					throw new ValidationException("Cannot find id property for " + table.getName());
				}
				Class<?> idType = beanProperties.get("id").getPropertyType();

				if (table.getTableType().isUsesNaturalId()) {
					if (idType == long.class || idType == Long.class) {
						column.setDataType(DataTypes.NATURAL_ID_LONG);
					}
					if (idType == short.class || idType == Short.class) {
						column.setDataType(DataTypes.NATURAL_ID_SHORT);
					}
					else {
						column.setDataType(DataTypes.NATURAL_ID);
					}
					column.setSortOrder(1);
					column.setNaturalKey(true);
				}
				else {
					if (idType == int.class || idType == Integer.class) {
						column.setDataType(DataTypes.IDENTITY);
					}
					else if (idType == long.class || idType == Long.class) {
						column.setDataType(DataTypes.IDENTITY_LONG);
					}
					else if (idType == short.class || idType == Short.class) {
						column.setDataType(DataTypes.IDENTITY_SHORT);
					}
					else {
						throw new RuntimeException("Invalid PK property data type '" + idType + "' for table " + table.getName());
					}
				}
				table.getColumnList().add(0, column);
			}

			// add 5 standard record stamp fields
			if (table.getColumn("CreateUserID") == null) {
				column = new Column();
				column.setName("CreateUserID");
				column.setDataType(DataTypes.SHORT);
				column.setExcludedFromUpdates(true);
				table.addColumn(column);
			}

			if (table.getColumn("CreateDate") == null) {
				column = new Column();
				column.setName("CreateDate");
				column.setDataType(DataTypes.DATETIME);
				column.setExcludedFromUpdates(true);
				table.addColumn(column);
			}

			if (table.getColumn("UpdateUserID") == null) {
				column = new Column();
				column.setName("UpdateUserID");
				column.setDataType(DataTypes.SHORT);
				table.addColumn(column);
			}

			if (table.getColumn("UpdateDate") == null) {
				column = new Column();
				column.setName("UpdateDate");
				column.setDataType(DataTypes.DATETIME);
				table.addColumn(column);
			}

			if (table.getColumn("rv") == null) {
				column = new Column();
				column.setName("rv");
				column.setDataType(DataTypes.ROWVERSION);
				column.setExcludedFromInserts(true);
				column.setExcludedFromUpdates(true);
				table.addColumn(column);
			}
		}
	}


	/**
	 * If there are no explicit natural keys and only one unique index
	 * defined for the table, then auto populate the natural keys as the
	 * same columns as the unique index
	 */
	private void processNaturalKeyDefinitions(Table table) {
		// If only one unique index process that index as the natural key
		List<Index> uniqueIndexList = BeanUtils.filter(table.getIndexList(), Index::isUnique);
		if (CollectionUtils.getSize(uniqueIndexList) != 1) {
			return;
		}

		// Unless some columns are already explicitly marked as natural keys
		if (!CollectionUtils.isEmpty(BeanUtils.filter(table.getColumnList(), Column::isNaturalKey))) {
			return;
		}
		for (Column column : uniqueIndexList.get(0).getColumnList()) {
			column.setNaturalKey(true);
		}
	}


	/**
	 * Validate and map table level indexes to corresponding tables and columns.
	 */
	private void processTableLevelIndexDefinitions(Table table) {
		for (Index index : CollectionUtils.getIterable(table.getIndexList())) {
			if (StringUtils.isEmpty(index.getName())) {
				String indexName = getIndexName(table, index.getColumnList(), index.isUnique(), index.getIncludeColumnList());
				index.setName(indexName);
			}
			index.setTable(table);
			List<Column> indexColumnList = index.getColumnList();
			for (int i = 0; i < indexColumnList.size(); i++) {
				Column indexColumn = indexColumnList.get(i);
				Column tableColumn = table.getColumn(indexColumn.getName());
				if (tableColumn == null) {
					throw new RuntimeException("Index column '" + indexColumn.getName() + "' is not defined on the table '" + table.getName() + "'");
				}
				indexColumnList.set(i, tableColumn);
			}
		}
	}


	/**
	 * Add table level index definitions for indexes defined as column attributes
	 */
	private void processColumnLevelIndexDefinitions(Table currentTable, Table table, Column column) {
		if (table.getOperationType() == OperationTypes.DELETE) {
			table.addIndexes(currentTable.removeIndexes(column));
		}
		else {
			if (column.isUniqueIndex()) {
				column.setIndexed(true);
			}
			else if (column.getFkTable() != null) {
				for (Index index : CollectionUtils.getIterable(table.getIndexList())) {
					if (column.getName().equals(index.getColumnList().get(0).getName())) {
						column.setIndexed(false);
					}
				}
			}
			if (column.getIndexed() != null && column.getIndexed()) {
				// add column index definition if one doesn't already exist
				String indexName = getIndexName(table, CollectionUtils.createList(column), column.isUniqueIndex(), null);
				if (table.getIndex(indexName) == null) {
					Index index = new Index();
					index.setName(indexName);
					index.setUnique(column.isUniqueIndex());
					index.setTable(table);
					index.setColumnList(CollectionUtils.createList(column));
					table.addIndex(index);
					if (table.getOperationType() == OperationTypes.UPDATE) {
						currentTable.addIndex(index);
					}
				}
			}
		}
	}


	/**
	 * Validate and map table level constraints to corresponding tables and columns
	 */
	private void processTableLevelConstraintDefinitions(Schema schema, Table table) {
		for (Constraint constraint : CollectionUtils.getIterable(table.getConstraintList())) {
			if (constraint.getPrimaryTable() != null) {
				continue; // already populated
			}

			Table referenceTable = schema.getTable(constraint.getReferenceTable().getName());
			if (referenceTable == null) {
				throw new RuntimeException("Cannot find reference table '" + constraint.getReferenceTable().getName() + "' for a foreign key constraint on '" + table.getName() + "'");
			}
			constraint.setPrimaryTable(table);

			List<Column> primaryColumnList = constraint.getPrimaryColumnList();
			int primaryColumnCount = CollectionUtils.getSize(primaryColumnList);
			if (primaryColumnCount == 0) {
				throw new RuntimeException("No primary column(s) defined for a foreign key constraint on '" + table.getName() + "'");
			}
			for (int i = 0; i < primaryColumnCount; i++) {
				Column column = primaryColumnList.get(i);
				Column primaryColumn = table.getColumn(column.getName());
				if (primaryColumn == null) {
					throw new RuntimeException("Cannot find column '" + column.getName() + "' which is a foreign key reference for '" + table.getName() + "'");
				}
				if (primaryColumnCount == 1) {
					primaryColumn.setIndexed(true);
				}
				primaryColumnList.set(i, primaryColumn);
				table.setName(table.getName() + "_" + primaryColumn.getName());
			}

			List<Column> referenceColumnList = constraint.getReferenceTable().getColumnList();
			int referenceColumnCount = CollectionUtils.getSize(referenceColumnList);
			if (referenceColumnCount == 0) {
				throw new RuntimeException("No reference column(s) defined for a foreign key constraint on '" + table.getName() + "'");
			}
			for (int i = 0; i < referenceColumnCount; i++) {
				Column column = referenceColumnList.get(i);
				Column referenceColumn = referenceTable.getColumn(column.getName());
				if (referenceColumn == null) {
					throw new RuntimeException("Cannot find column '" + column.getName() + "' which is a foreign key reference for '" + table.getName() + "'");
				}
				referenceColumnList.set(i, referenceColumn);
			}
			constraint.setReferenceTable(referenceTable);
			constraint.setReferenceColumnList(referenceColumnList);
			constraint.setName(getForeignKeyConstraintName(constraint));
		}
	}


	/**
	 * Add table level foreign key constraints defined via column attributes.
	 */
	private void processColumnLevelConstraintDefinitions(Table currentTable, Schema schema, Schema currentSchema, Table table, Column column) {
		if (table.getOperationType() == OperationTypes.DELETE) {
			// delete all constraint(s) associated with this column
			Iterator<Constraint> iterator = CollectionUtils.getIterable(currentTable.getConstraintList()).iterator();
			while (iterator.hasNext()) {
				Constraint constraint = iterator.next();
				if (constraint.getPrimaryColumnList().contains(column)) {
					table.addConstraint(constraint);
					iterator.remove();
				}
			}

			// TODO: what about other tables that point to this column??? ERROR OUT???
			return;
		}

		if (column.getDataType().isIdentity() || column.getDataType() == DataTypes.NATURAL_ID || column.getDataType() == DataTypes.NATURAL_ID_LONG
				|| column.getDataType() == DataTypes.NATURAL_ID_SHORT) {
			// add primary key constraint
			Constraint primaryKey = new Constraint();
			primaryKey.setName("PK_" + table.getName());
			primaryKey.setPrimaryTable(table);
			primaryKey.setPrimaryColumnList(CollectionUtils.createList(column));
			table.addConstraint(primaryKey);
		}
		if (column.getFkTable() != null) {
			Table fkTable = schema.getTable(column.getFkTable());
			if (fkTable == null || OperationTypes.UPDATE == fkTable.getOperationType() || OperationTypes.UPDATE_SQL_ONLY == fkTable.getOperationType()) {
				if (currentSchema != null) {
					fkTable = currentSchema.getTable(column.getFkTable());
				}
				if (fkTable == null) {
					throw new RuntimeException(String.format("Cannot find table [%s] referenced by foreign key from [%s.%s].", column.getFkTable(), table.getName(), column.getName()));
				}
			}
			if (table.getName().equalsIgnoreCase(fkTable.getName()) && column.getName().equalsIgnoreCase(column.getFkField())) {
				throw new RuntimeException(String.format("Foreign key column [%s.%s] cannot reference itself.", table.getName(), column.getName()));
			}
			Column fkColumn = fkTable.getColumn(column.getFkField() == null ? column.getName() : column.getFkField());
			if (fkColumn == null) {
				throw new RuntimeException(String.format("Cannot find column [%s.%s] referenced by foreign key from [%s.%s].", column.getFkTable(), column.getFkField(), table.getName(), column.getName()));
			}
			Constraint foreignKey = new Constraint();
			foreignKey.setDelayed(column.isDelayed());
			foreignKey.setPrimaryTable(table);
			foreignKey.setPrimaryColumnList(CollectionUtils.createList(column));
			foreignKey.setReferenceTable(fkTable);
			foreignKey.setReferenceColumnList(CollectionUtils.createList(fkColumn));
			foreignKey.setName(getForeignKeyConstraintName(foreignKey));
			table.addConstraint(foreignKey);
			if (currentTable != null) {
				// also add constraint to current schema
				currentTable.addConstraint(foreignKey);
			}

			// index all foreign keys
			if (column.getIndexed() == null) {
				column.setIndexed(true);
			}

			// IF SHORT is the PK Column type - change FK column to be SMALLINT too
			if (fkColumn.getDataType() == DataTypes.IDENTITY_SHORT) {
				column.setDataType(DataTypes.SHORT);
			}
		}
	}


	/**
	 * Generates database table index name based on the specified arguments.
	 */
	private String getIndexName(Table table, List<Column> columnList, boolean unique, List<Column> includeColumnList) {
		if (CollectionUtils.getSize(columnList) == 0) {
			throw new RuntimeException(String.format("An index for table [%s] must have at least one column.", table.getName()));
		}

		StringBuilder indexName = new StringBuilder(50);
		indexName.append(unique ? "ux_" : "ix_");
		indexName.append(table.getName());
		for (Column column : columnList) {
			indexName.append('_');
			indexName.append(column.getName());
		}
		if (!CollectionUtils.isEmpty(includeColumnList)) {
			indexName.append("_includes");
		}
		return indexName.toString();
	}


	/**
	 * Generates database table FK name using our naming convention.
	 */
	private String getForeignKeyConstraintName(Constraint foreignKey) {
		StringBuilder name = new StringBuilder();
		name.append("FK_");
		name.append(foreignKey.getPrimaryTable().getName());
		name.append('_');
		name.append(foreignKey.getReferenceTable().getName());
		for (Column column : foreignKey.getPrimaryColumnList()) {
			name.append('_');
			name.append(column.getName());
		}
		return name.toString();
	}


	/**
	 * Returns a Map of the DTO class property names to corresponding PropertyDescriptor objects.
	 */
	private Map<String, PropertyDescriptor> getDtoPropertyMap(Table currentTable, Table table) {
		if (table.getOperationType() == OperationTypes.DELETE) {
			return Collections.emptyMap();
		}
		if (table.getDtoClass() == null) {
			if (table.getOperationType() == OperationTypes.UPDATE) {
				// adding new column(s), etc.
				table.setDtoClass(currentTable.getDtoClass());
			}
			else {
				throw new RuntimeException("dtoClass attribute cannot be found for table '" + table.getName() + "'");
			}
		}
		Class<?> clazz;
		try {
			clazz = Class.forName(table.getDtoClass());
		}
		catch (ClassNotFoundException e) {
			throw new RuntimeException("Cannot load DTO Class: " + table.getDtoClass() + " for table: " + table.getName(), e);
		}
		return BeanUtils.getPropertyDataTypes(clazz);
	}


	private Table getCurrentTable(Schema currentSchema, Table table) {
		boolean allowMissing = (table.getOperationType() == null || table.getOperationType() == OperationTypes.CREATE);

		if (currentSchema == null) {
			if (allowMissing) {
				return null;
			}
			throw new RuntimeException("Cannot find table '" + table.getName() + "' in current schema because it is null.");
		}
		Table currentTable = currentSchema.getTable(table.getName());
		if (currentTable == null) {
			if (allowMissing) {
				return null;
			}
			throw new RuntimeException("Cannot find table '" + table.getName() + "' in current schema.");
		}
		return currentTable;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Converter<PropertyDescriptor, DataTypes> getPropertyDataTypeConverter() {
		return this.propertyDataTypeConverter;
	}


	public void setPropertyDataTypeConverter(Converter<PropertyDescriptor, DataTypes> propertyDataTypeConverter) {
		this.propertyDataTypeConverter = propertyDataTypeConverter;
	}


	public String getDefaultDataSourceName() {
		return this.defaultDataSourceName;
	}


	public void setDefaultDataSourceName(String defaultDataSourceName) {
		this.defaultDataSourceName = defaultDataSourceName;
	}


	public CacheTypes getDefaultCacheType() {
		return this.defaultCacheType;
	}


	public void setDefaultCacheType(CacheTypes defaultCacheType) {
		this.defaultCacheType = defaultCacheType;
	}


	public DataTypeEncodingTypeNames getDefaultDataTypeEncodingType() {
		return this.defaultDataTypeEncodingType;
	}


	public void setDefaultDataTypeEncodingType(DataTypeEncodingTypeNames defaultDataTypeEncodingType) {
		this.defaultDataTypeEncodingType = defaultDataTypeEncodingType;
	}
}
