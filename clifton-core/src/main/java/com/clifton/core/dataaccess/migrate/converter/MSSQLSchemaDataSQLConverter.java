package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.dataaccess.db.TableTypes;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.dml.Data;
import com.clifton.core.dataaccess.migrate.schema.dml.Value;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import org.springframework.stereotype.Component;


/**
 * The <code>MSSQLSchemaDataSQLConverter</code> class generates DML SQL statements to insert/delete data info
 * for Schema's data tags
 *
 * @author manderson
 */
@Component
public class MSSQLSchemaDataSQLConverter implements MigrationSchemaConverter {

	private Converter<Value, String> dataValueToSQLConverter;
	private Converter<Value, String> dataValueToColumnConverter;

	private String runWith = MigrationSchemaConverterTypes.DATA.name();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.DATA;
	}


	@Override
	public String convert(Schema schema) {
		if (schema.getExcludeFromSql() != null && schema.getExcludeFromSql()) {
			return "";
		}
		StringBuilder sql = new StringBuilder("");
		for (Data data : CollectionUtils.getIterable(schema.getDataList(getRunWith()))) {
			boolean identityInsert = false;
			for (Value val : data.getValueList()) {
				if (val.getDataType().isIdentity()) {
					identityInsert = true;
					break;
				}
			}
			if (identityInsert) {
				sql.append("SET IDENTITY_INSERT ").append(data.getTable()).append(" ON\n\n");
			}
			sql.append("INSERT INTO ");
			sql.append(data.getTable());
			sql.append('(');

			sql.append(StringUtils.collectionToCommaDelimitedString(data.getValueList(), getDataValueToColumnConverter()::convert));
			if (TableTypes.SIMPLE != data.getTableType()) {
				sql.append(", CreateUserID, CreateDate, UpdateUserID, UpdateDate");
			}
			sql.append(")\n\tVALUES(");
			sql.append(StringUtils.collectionToCommaDelimitedString(data.getValueList(), getDataValueToSQLConverter()::convert));
			if (TableTypes.SIMPLE != data.getTableType()) {
				sql.append(", 0, GETDATE(), 0, GETDATE()");
			}
			sql.append(")").append(getMigrationSchemaConverterType().getBatchSeparator());
			if (identityInsert) {
				sql.append("SET IDENTITY_INSERT ").append(data.getTable()).append(" OFF\n\n");
			}
		}
		return sql.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public Converter<Value, String> getDataValueToSQLConverter() {
		return this.dataValueToSQLConverter;
	}


	public void setDataValueToSQLConverter(Converter<Value, String> dataValueToSQLConverter) {
		this.dataValueToSQLConverter = dataValueToSQLConverter;
	}


	public Converter<Value, String> getDataValueToColumnConverter() {
		return this.dataValueToColumnConverter;
	}


	public void setDataValueToColumnConverter(Converter<Value, String> dataValueToColumnConverter) {
		this.dataValueToColumnConverter = dataValueToColumnConverter;
	}


	public String getRunWith() {
		return this.runWith;
	}


	public void setRunWith(String runWith) {
		this.runWith = runWith;
	}
}
