package com.clifton.core.dataaccess.datasource.stats;

/**
 * <code>DatabaseProperty</code> defines a database configuration property included name, data type,
 * value, and option newValue used for updating properties real-time if supported.
 *
 * @author NickK
 */
public class DatabaseProperty {

	private String name;
	private Class<?> type;
	private Object value;
	private Object newValue;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public DatabaseProperty() {
		// noop constructor
	}


	public DatabaseProperty(String name, Class<?> type) {
		this.name = name;
		this.type = type;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Class<?> getType() {
		return this.type;
	}


	public void setType(Class<?> type) {
		this.type = type;
	}


	public Object getValue() {
		return this.value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public Object getNewValue() {
		return this.newValue;
	}


	public void setNewValue(Object newValue) {
		this.newValue = newValue;
	}
}
