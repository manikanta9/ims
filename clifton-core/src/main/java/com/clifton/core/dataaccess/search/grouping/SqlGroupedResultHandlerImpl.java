package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.GroupingHibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.springframework.stereotype.Component;

import java.util.List;


@Component
public class SqlGroupedResultHandlerImpl implements SqlGroupedResultHandler {

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> List<GroupedHierarchicalResult> getGroupedResult(HibernateSearchFormConfigurer entitySearchFormConfigurer, AdvancedReadOnlyWithGroupingDAO<T> dao) {
		return dao.findBySearchCriteriaGrouped(new GroupingHibernateSearchFormConfigurer<T>(entitySearchFormConfigurer, false, getDaoLocator(), dao.getConfiguration()));
	}


	@Override
	public <T extends IdentityObject> List<GroupedEntityResult<T>> getAggregateResult(HibernateSearchFormConfigurer entitySearchFormConfigurer, AdvancedReadOnlyWithGroupingDAO<T> dao) {
		return dao.findBySearchCriteriaAggregate(new GroupingHibernateSearchFormConfigurer<T>(entitySearchFormConfigurer, true, getDaoLocator(), dao.getConfiguration()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
