package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationAdapter;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.NavigableMap;
import java.util.TreeMap;


/**
 * The {@link SelfRegisteringFlexibleDaoCache} is a self-registering cache for flexible retrieval based off of an {@link #getBeanOrderProperty() order} property.
 * <p>
 * This cache provides a mechanism for retrieving the nearest entity whose order property is <i>equal to</i> or <i>less than</i> a provided order value. One common such type of
 * order property is {@link Date}. In these cases, the "flexible" cache can be used to retrieve the latest value which was active on a given date.
 * <p>
 * This cache also allows an {@link #getBeanEndOrderProperty() end order property}. If populated, the end order property will be used to indicate whether a value is expired and
 * cannot be returned. A typical use for the end order property is the "end date" for an entity.
 * <p>
 * Implementors should be registered as Spring-managed beans (typically via the {@link Component} annotation) for automatic registration.
 *
 * @param <T> the type of object which shall have its references cached
 * @param <U> the type for the <i>order</i> property
 */
public abstract class SelfRegisteringFlexibleDaoCache<T extends IdentityObject, U extends Comparable<U>> extends SelfRegisteringSimpleDaoCache<T, String, NavigableMap<U, Serializable>> {

	/**
	 * The synchronization lock for cache write operations.
	 */
	private final Object cacheWriteLock = new Object();
	/**
	 * If <code>true</code> then end order constraints are enabled.
	 */
	private final boolean useEndOrder;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SelfRegisteringFlexibleDaoCache() {
		this.useEndOrder = getBeanEndOrderProperty() != null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the list of unique properties on which entities shall be cached, excluding the order property.
	 */
	protected abstract String[] getBeanKeyProperties();


	/**
	 * Returns the key value for the given bean for caching.
	 */
	protected abstract String getBeanKey(T bean);


	/**
	 * Returns the name of the property which shall be used for ordering.
	 */
	protected abstract String getBeanOrderProperty();


	/**
	 * Returns the order for the given bean.
	 */
	protected abstract U getBeanOrderValue(T bean);


	/**
	 * Returns the name of the property which shall be used to determine if an entity has expired, such as <code>"endDate"</code>.
	 * <p>
	 * If <code>null</code>, then end order filtering will be disabled.
	 */
	protected String getBeanEndOrderProperty() {
		return null;
	}


	/**
	 * Returns the end order value for the given bean.
	 */
	protected U getBeanEndOrderValue(T bean) {
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean) {
		if (event.isUpdate()) {
			// Cache original bean on update for afterMethodCall usage
			getOriginalBean(dao, bean);
		}
	}


	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e != null) {
			return;
		}

		// Remove all entries for bean key
		String key = getBeanKey(bean);
		evictBeanCache(key);

		// Remove previous key if key has changed during update
		if (event.isUpdate()) {
			T originalBean = getOriginalBean(dao, bean);
			String originalKey = getBeanKey(originalBean);
			if (!key.equals(originalKey)) {
				evictBeanCache(originalKey);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected final T getBeanImpl(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, U order, Object... keyProperties) {
		T bean = getBeanFromCache(dao, order, keyProperties);
		if (bean == null && throwExceptionIfNotFound) {
			throw new ValidationException(String.format("Unable to find %s with properties [%s] and value(s) [%s] for order value [%s].", StringUtils.splitWords(dao.getConfiguration().getTableName()), ArrayUtils.toString(getBeanKeyProperties()), ArrayUtils.toString(keyProperties), order));
		}
		return bean;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private T getBeanFromCache(ReadOnlyDAO<T> dao, U order, Object... keyProperties) {
		NavigableMap<U, Serializable> idByOrder = getBeanCache(dao, keyProperties);
		Map.Entry<U, Serializable> matchedEntry = idByOrder.floorEntry(order);
		// Guard-clause: No entities exist for order
		if (matchedEntry == null) {
			return null;
		}

		T nearestBean = dao.findByPrimaryKey(matchedEntry.getValue());
		final T bean;
		if (isUseEndOrder()) {
			// Compare against end order constraint
			U endOrderValue = getBeanEndOrderValue(nearestBean);
			if (endOrderValue == null || CompareUtils.compare(endOrderValue, order) >= 0) {
				bean = nearestBean;
			}
			else {
				bean = null;
			}
		}
		else {
			bean = nearestBean;
		}
		return bean;
	}


	private NavigableMap<U, Serializable> getBeanCache(ReadOnlyDAO<T> dao, Object... keyProperties) {
		// Guard-clause: Exit early if bean cache already populated
		String beanKey = getBeanKeyForProperties(keyProperties);
		NavigableMap<U, Serializable> cacheMap = getCacheHandler().get(getCacheName(), beanKey);
		if (cacheMap != null) {
			return cacheMap;
		}

		// Populate cache
		synchronized (getCacheWriteLock()) {
			// Check for concurrent cache population
			cacheMap = getCacheHandler().get(getCacheName(), beanKey);
			if (cacheMap == null) {
				// Validate presence of order property
				List<T> beanList = CollectionUtils.asNonNullList(lookupBeanList(dao, keyProperties));
				List<T> missingOrderBeanList = CollectionUtils.getFiltered(beanList, bean -> getBeanOrderValue(bean) == null);
				if (!missingOrderBeanList.isEmpty()) {
					AssertUtils.fail("Entities were found with missing order property [%s]. This field is required for self-registering flexible cache entries. Invalid entity list: %s.", getBeanOrderProperty(), CollectionUtils.toString(missingOrderBeanList, 5));
				}
				// Generate cache
				cacheMap = new TreeMap<>();
				getCacheHandler().put(getCacheName(), beanKey, cacheMap);
				for (T bean : beanList) {
					cacheMap.put(getBeanOrderValue(bean), bean.getIdentity());
				}
			}
		}
		return cacheMap;
	}


	private List<T> lookupBeanList(ReadOnlyDAO<T> dao, Object... keyProperties) {
		// If currently in a transaction, evict cache on transaction rollback since a rollback may revert queried data
		if (TransactionSynchronizationManager.isActualTransactionActive()) {
			TransactionSynchronizationManager.registerSynchronization(new TransactionSynchronizationAdapter() {
				@Override
				public void afterCompletion(int status) {
					if (status == TransactionSynchronization.STATUS_ROLLED_BACK) {
						evictBeanCacheForProperties(keyProperties);
					}
				}
			});
		}
		return dao.findByFields(getBeanKeyProperties(), keyProperties);
	}


	private void evictBeanCacheForProperties(Object... keyProperties) {
		evictBeanCache(getBeanKeyForProperties(keyProperties));
	}


	private void evictBeanCache(String beanKey) {
		getCacheHandler().remove(getCacheName(), beanKey);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Object getCacheWriteLock() {
		return this.cacheWriteLock;
	}


	public boolean isUseEndOrder() {
		return this.useEndOrder;
	}
}
