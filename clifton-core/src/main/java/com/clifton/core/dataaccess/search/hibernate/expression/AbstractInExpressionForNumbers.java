package com.clifton.core.dataaccess.search.hibernate.expression;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.internal.util.StringHelper;

import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * The <code>InExpressionForNumbers</code> class implements IN SQL clause
 *
 * @author vgomelsky
 */
public abstract class AbstractInExpressionForNumbers implements Criterion {

	private final String comparisonExpression;

	private final String propertyName;

	private final Object[] values;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected AbstractInExpressionForNumbers(String comparisonExpression, String propertyName, Object[] values) {
		for (Object v : values) {
			if (!(v instanceof Number)) {
				if (v != null) { // Skip NULLs
					throw new IllegalArgumentException("Each value for " + comparisonExpression + " clause must be an instance of a Number.  Value [" + v + "] is not a number.");
				}
			}
		}

		this.comparisonExpression = comparisonExpression;
		this.propertyName = propertyName;
		this.values = values;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getColumns(Criteria criteria, CriteriaQuery criteriaQuery) {
		return criteriaQuery.getColumnsUsingProjection(criteria, this.propertyName);
	}


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		String[] columns = getColumns(criteria, criteriaQuery);

		if (criteriaQuery.getFactory().getServiceRegistry().getService( JdbcServices.class ).getDialect().supportsRowValueConstructorSyntaxInInList() || columns.length <= 1) {
			String params = Arrays.stream(this.values)
					.map(String::valueOf)
					.collect(Collectors.joining(", "));
			if (columns.length > 1) {
				params = '(' + params + ')';
				params = StringHelper.repeat(params + ", ", columns.length - 1) + params;
			}
			String cols = StringHelper.join(", ", Arrays.stream(columns).iterator());
			if (columns.length > 1) {
				cols = '(' + cols + ')';
			}

			return cols + ' ' + this.comparisonExpression + " (" + params + ')';
		}
		else {
			String cols = " ( " + StringHelper.join(" = ? and ", Arrays.stream(columns).iterator()) + "= ? ) ";
			cols = (this.values.length > 0) ? (StringHelper.repeat(cols + "or ", this.values.length - 1) + cols) : "";

			cols = " ( " + cols + " ) ";
			return cols;
		}
	}


	@Override
	@SuppressWarnings("unused")
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		// all values are inlined
		return new TypedValue[0];
	}


	@Override
	public String toString() {
		return this.propertyName + ' ' + this.comparisonExpression + " (" + StringHelper.toString(this.values) + ')';
	}
}
