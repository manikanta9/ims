package com.clifton.core.dataaccess.migrate.schema;


import java.util.List;


/**
 * The <code>Constraint</code> class represents a foreign key database constraint.
 *
 * @author vgomelsky
 */
public class Constraint {

	private String name;
	private String label;
	private String description;

	private Table primaryTable;
	private List<Column> primaryColumnList;

	private Table referenceTable;
	private List<Column> referenceColumnList;

	/**
	 * If true, when generating SQL will add the SQL for the constraint
	 * after ALL tables & columns SQL
	 */
	private boolean delayed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.name;
	}


	public Table getPrimaryTable() {
		return this.primaryTable;
	}


	public void setPrimaryTable(Table primaryTable) {
		this.primaryTable = primaryTable;
	}


	public List<Column> getPrimaryColumnList() {
		return this.primaryColumnList;
	}


	public void setPrimaryColumnList(List<Column> primaryColumnList) {
		this.primaryColumnList = primaryColumnList;
	}


	public Table getReferenceTable() {
		return this.referenceTable;
	}


	public void setReferenceTable(Table referenceTable) {
		this.referenceTable = referenceTable;
	}


	public List<Column> getReferenceColumnList() {
		return this.referenceColumnList;
	}


	public void setReferenceColumnList(List<Column> referenceColumnList) {
		this.referenceColumnList = referenceColumnList;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getLabel() {
		return this.label;
	}


	public void setLabel(String label) {
		this.label = label;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public boolean isDelayed() {
		return this.delayed;
	}


	public void setDelayed(boolean delayed) {
		this.delayed = delayed;
	}
}
