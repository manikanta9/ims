package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.PagingDataTable;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;


/**
 * The <code>PagingDataTableImpl</code> class implements <code>DataTable</code> that supports paging.
 *
 * @author vgomelsky
 */
public class PagingDataTableImpl implements PagingDataTable {

	private final DataColumn[] dataColumnList;
	private final PagingArrayList<DataRow> rowList;
	private boolean variableColumnCountPerRow = false;


	/**
	 * Used for Jackson deserialization;
	 * Because we want to ensure that the dataTable references the same columnList object
	 * we allow for serialization to set the columnList on the dataRows, then we set the dataTable
	 * columnList with the same reference from the rowList.
	 */
	@JsonCreator
	public PagingDataTableImpl(@JsonProperty("rowList") PagingArrayList<DataRow> rowList) {
		this.rowList = rowList;
		if (!CollectionUtils.isEmpty(rowList)) {
			this.dataColumnList = ((DataRowImpl) rowList.get(0)).getColumnList();
		}
		else {
			this.dataColumnList = new DataColumn[0];
		}
	}


	/**
	 * Constructs a new instance of PagingDataTableImpl using the specified arguments.
	 */
	public PagingDataTableImpl(DataColumn[] dataColumnList, int firstRowIndex, int totalSize) {
		this.dataColumnList = dataColumnList;
		this.rowList = new PagingArrayList<>(firstRowIndex, totalSize);
	}


	public PagingDataTableImpl(DataColumn[] dataColumnList) {
		this(dataColumnList, 0, 0);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return "{totalElementCount=" + getTotalElementCount() + '}';
	}


	@Override
	public int getColumnIndex(String columnName) {
		for (int i = 0; i < this.dataColumnList.length; i++) {
			DataColumn dataColumn = this.dataColumnList[i];
			if (dataColumn.getColumnName().equals(columnName)) {
				return i;
			}
		}
		return -1;
	}


	@Override
	public int getColumnCount() {
		return (this.dataColumnList == null) ? 0 : this.dataColumnList.length;
	}


	@Override
	public DataColumn[] getColumnList() {
		return this.dataColumnList;
	}


	@Override
	public List<DataRow> getRowList() {
		return this.rowList;
	}


	@Override
	public DataRow getRow(int rowIndex) {
		return this.rowList.get(rowIndex);
	}


	@Override
	public void addRow(DataRow row) {
		this.rowList.add(row);
	}


	public int getCurrentElementRowCount() {
		return this.rowList.getCurrentPageElementCount();
	}


	@Override
	public int getFirstElementIndex() {
		return this.rowList.getFirstElementIndex();
	}


	@Override
	public int getPageSize() {
		return this.rowList.getPageSize();
	}


	@Override
	@ValueChangingSetter
	public void setPageSize(int pageSize) {
		this.rowList.setPageSize(pageSize);
	}


	@Override
	public int getTotalElementCount() {
		return this.rowList.getTotalElementCount();
	}


	@Override
	@ValueChangingSetter
	public void setTotalRowCount(int totalRowCount) {
		this.rowList.setTotalElementCount(totalRowCount);
	}


	@Override
	public int getCurrentPageNumber() {
		return this.rowList.getCurrentPageNumber();
	}


	@Override
	public int getTotalPageCount() {
		return this.rowList.getTotalPageCount();
	}


	@Override
	public boolean isFirstPage() {
		return this.rowList.isFirstPage();
	}


	@Override
	public boolean isLastPage() {
		return this.rowList.isLastPage();
	}


	@Override
	public int getTotalRowCount() {
		return getTotalElementCount();
	}


	@Override
	public int getCurrentPageElementCount() {
		return this.rowList.getCurrentPageElementCount();
	}


	@Override
	public boolean isVariableColumnCountPerRow() {
		return this.variableColumnCountPerRow;
	}


	@Override
	public void setVariableColumnCountPerRow(boolean simpleExport) {
		this.variableColumnCountPerRow = simpleExport;
	}
}
