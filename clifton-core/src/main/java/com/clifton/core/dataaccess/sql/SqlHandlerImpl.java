package com.clifton.core.dataaccess.sql;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.db.ClassToSqlTypeConverter;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.SqlProvider;
import org.springframework.jdbc.core.StatementCallback;
import org.springframework.jdbc.support.JdbcUtils;
import org.springframework.lang.Nullable;
import org.springframework.util.Assert;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * @author vgomelsky
 */
public class SqlHandlerImpl implements SqlHandler {

	private JdbcTemplate jdbcTemplate;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataSource getDataSource() {
		return getJdbcTemplate().getDataSource();
	}


	@Override
	public <T> T executeSelect(SqlSelectCommand sql, ResultSetExtractor<T> resultSetExtractor) {
		long start = System.currentTimeMillis();

		T result;
		if (CollectionUtils.isEmpty(sql.getSqlParameterValues())) {
			result = getJdbcTemplate().execute(new StatementCallback<T>() {
				@Override
				@Nullable
				public T doInStatement(Statement stmt) throws SQLException {
					ResultSet rs = null;
					try {
						if (sql.getTimeout() > 0) {
							stmt.setQueryTimeout(sql.getTimeout());
						}
						rs = stmt.executeQuery(sql.getSql());
						return resultSetExtractor.extractData(rs);
					}
					finally {
						JdbcUtils.closeResultSet(rs);
					}
				}
			});
		}
		else {
			Object[] sqlParameterValues = BeanUtils.getPropertyValues(sql.getSqlParameterValues(), SqlParameterValue::getValue);
			PreparedStatementSetter pss = new ArgumentPreparedStatementSetter(sqlParameterValues);
			SimplePreparedStatementCreator psc = new SimplePreparedStatementCreator(sql.getSql(), sql.getTimeout());
			result = getJdbcTemplate().query(psc, pss, resultSetExtractor);
		}

		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> {
			String message = sql.getSqlWithParameters();
			message += "\nExecution Time = " + (System.currentTimeMillis() - start) + "ms";
			if (result instanceof Collection) {
				message += "; Rows = " + ((Collection<?>) result).size();
			}
			return message;
		}));

		return result;
	}


	@Override
	public <T> T executeSelect(PreparedStatementCreator statementCreator, ResultSetExtractor<T> resultSetExtractor) {
		return this.executeSelect(statementCreator, resultSetExtractor, SqlSelectCommand.DEFAULT_QUERY_TIMEOUT);
	}


	@Override
	public <T> T executeSelect(PreparedStatementCreator statementCreator, ResultSetExtractor<T> resultSetExtractor, int timeout) {
		long start = System.currentTimeMillis();

		T result = getJdbcTemplate().execute(statementCreator, new PreparedStatementCallback<T>() {
			@Override
			@Nullable
			public T doInPreparedStatement(PreparedStatement ps) throws SQLException {
				ResultSet rs = null;
				try {
					if (timeout > 0) {
						ps.setQueryTimeout(timeout);
					}
					rs = ps.executeQuery();
					return resultSetExtractor.extractData(rs);
				}
				finally {
					JdbcUtils.closeResultSet(rs);
				}
			}
		});

		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () -> {
			String message = statementCreator.toString();
			message += "\nExecution Time = " + (System.currentTimeMillis() - start) + "ms";
			if (result instanceof Collection) {
				message += "; Rows = " + ((Collection<?>) result).size();
			}
			return message;
		}));

		return result;
	}


	@Override
	public <T> T executeSelect(String sql, ResultSetExtractor<T> resultSetExtractor) {
		return executeSelect(new SqlSelectCommand(sql), resultSetExtractor);
	}


	@Override
	public String queryForString(SqlSelectCommand sql) {
		return queryForObject(sql, String.class);
	}


	@Override
	public Date queryForDate(SqlSelectCommand sql) {
		return queryForObject(sql, Date.class);
	}


	@Override
	public Object queryForObject(SqlSelectCommand sql) {
		return queryForObject(sql, Object.class);
	}


	private <T> T queryForObject(SqlSelectCommand sql, Class<T> returnType) {
		long start = System.currentTimeMillis();

		Object[] sqlParameterValues = BeanUtils.getPropertyValues(sql.getSqlParameterValues(), SqlParameterValue::getValue);
		T result = getJdbcTemplate().queryForObject(sql.getSql(), returnType, sqlParameterValues);

		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () ->
				sql.getSqlWithParameters() + "\nExecution Time = " + (System.currentTimeMillis() - start) + "ms; Value = " + result
		));

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int executeUpdate(SqlUpdateCommand sql) {
		long start = System.currentTimeMillis();

		int result;
		if (CollectionUtils.isEmpty(sql.getSqlParameterValues())) {
			result = getJdbcTemplate().update(sql.getSql());
		}
		else {
			Object[] sqlParameterValues = BeanUtils.getPropertyValues(sql.getSqlParameterValues(), SqlParameterValue::getValue);
			result = getJdbcTemplate().update(sql.getSql(), sqlParameterValues);
		}

		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () ->
				sql.getSqlWithParameters() + "\nExecution Time = " + (System.currentTimeMillis() - start) + "ms; Update Count = " + result
		));

		return result;
	}


	@Override
	public int executeUpdate(String sql) {
		return executeUpdate(new SqlUpdateCommand(sql));
	}


	@Override
	public <T> T executeUpdate(String sql, SqlParameterValue[] params, PreparedStatementCallback<T> action) {
		long start = System.currentTimeMillis();

		T result;
		if (params != null && params.length > 0) {
			PreparedStatementCreatorFactory factory = new PreparedStatementCreatorFactory(sql);
			Object[] sqlParameterValues = new Object[params.length];
			for (int i = 0; i < params.length; i++) {
				SqlParameterValue param = params[i];
				if (param != null) {
					sqlParameterValues[i] = param.getValue();
					factory.addParameter(new SqlParameter(param.getSqlType()));
				}
			}
			PreparedStatementCreator creator = factory.newPreparedStatementCreator(sqlParameterValues);
			result = getJdbcTemplate().execute(creator, action);
		}
		else {
			result = getJdbcTemplate().execute(sql, action);
		}

		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () ->
				sql + "\nExecution Time = " + (System.currentTimeMillis() - start) + "ms; Result = " + result
		));

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int[] executeBatchUpdate(String sql, BatchPreparedStatementSetter preparedStatementSetter) {
		long start = System.currentTimeMillis();

		int[] result = getJdbcTemplate().batchUpdate(sql, preparedStatementSetter);

		LogUtils.info(LogCommand.ofMessageSupplier(getClass(), () ->
				sql + "\nExecution Time = " + (System.currentTimeMillis() - start) + "ms; Update Counts = " + Arrays.toString(result)
		));

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<String, Object> executeStoredProcedure(SqlStoredProcedureCommand procedureCommand) {
		SqlStoredProcedure sp = new SqlStoredProcedure(getJdbcTemplate(), procedureCommand.getProcedureName());
		// must declare parameters or they will be ignored
		if (procedureCommand.getParameters() != null) {
			ClassToSqlTypeConverter typeConverter = new ClassToSqlTypeConverter();
			for (Map.Entry<String, Object> paramEntry : procedureCommand.getParameters().entrySet()) {
				if (paramEntry.getValue() != null) {
					sp.declareParameter(new SqlParameter(paramEntry.getKey(), typeConverter.convert(paramEntry.getValue().getClass())));
				}
			}
		}
		if (!MathUtils.isNullOrZero(procedureCommand.getTimeoutInSeconds())) {
			sp.setQueryTimeout(procedureCommand.getTimeoutInSeconds());
		}
		return sp.execute(procedureCommand.getParameters() == null ? new HashMap<>() : procedureCommand.getParameters());
	}

	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public JdbcTemplate getJdbcTemplate() {
		return this.jdbcTemplate;
	}


	public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////                      Inner Class                        //////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Simple adapter for PreparedStatementCreator, allowing to use a plain SQL statement and to override the transactional timeout settings.
	 * This DOES NOT set '?' parameter values.
	 */
	private static class SimplePreparedStatementCreator implements PreparedStatementCreator, SqlProvider {

		private final String sql;
		private final int timeout;


		public SimplePreparedStatementCreator(String sql, int timeout) {
			Assert.notNull(sql, "SQL must not be null");
			this.sql = sql;
			this.timeout = timeout;
		}


		@Override
		public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
			PreparedStatement preparedStatement = con.prepareStatement(this.sql);
			if (getTimeout() > 0) {
				preparedStatement.setQueryTimeout(getTimeout());
			}
			return preparedStatement;
		}


		@Override
		public String getSql() {
			return this.sql;
		}


		public int getTimeout() {
			return this.timeout;
		}
	}
}
