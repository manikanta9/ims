package com.clifton.core.dataaccess.migrate.reader;


import com.clifton.core.cache.CacheTypes;
import com.clifton.core.dataaccess.db.DataTypeEncodingTypeNames;
import com.clifton.core.dataaccess.db.TableTypes;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.migrate.OperationTypes;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Constraint;
import com.clifton.core.dataaccess.migrate.schema.Index;
import com.clifton.core.dataaccess.migrate.schema.Link;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.dataaccess.migrate.schema.action.Argument;
import com.clifton.core.dataaccess.migrate.schema.dml.Data;
import com.clifton.core.dataaccess.migrate.schema.dml.Value;
import com.clifton.core.dataaccess.migrate.schema.sql.Sql;
import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.beans.Lazy;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.converters.MarshallingContext;
import com.thoughtworks.xstream.converters.UnmarshallingContext;
import com.thoughtworks.xstream.converters.reflection.PureJavaReflectionProvider;
import com.thoughtworks.xstream.io.HierarchicalStreamReader;
import com.thoughtworks.xstream.io.HierarchicalStreamWriter;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.InputStream;
import java.io.OutputStream;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>XmlMigrationHelper</code> class defines migrations XML format and helps loading Schema from XML
 * or converting Schema objects to XML.
 *
 * @author vgomelsky
 */
public class XmlMigrationHelper {

	/**
	 * Map of singleton thread-safe {@link XStream} instances because creation is expensive.
	 * Map is used for schemaOnly and non-schemaOnly instances.
	 */
	private static final Map<Boolean, Lazy<XStream>> xStreamInstanceMap = new ConcurrentHashMap<>();


	static {
		xStreamInstanceMap.put(Boolean.TRUE, new Lazy<>(() -> createXStreamInstance(true)));
		xStreamInstanceMap.put(Boolean.FALSE, new Lazy<>(() -> createXStreamInstance(false)));
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Returns Schema object populated from the specified file.
	 */
	public Schema loadFromXml(String fileName) {
		ResourceLoader loader = new DefaultResourceLoader();
		return loadFromXml(loader.getResource(fileName));
	}


	/**
	 * Returns Schema object populated from the specified resource.
	 *
	 * @param resource
	 */
	public Schema loadFromXml(Resource resource) {
		InputStream in = null;
		Schema schema;
		try {
			XStream xml = getConfiguredXStream(false);
			in = resource.getInputStream();
			schema = (Schema) xml.fromXML(in);
		}
		catch (Exception e) {
			throw new RuntimeException("Error loading migrations from file: " + resource.getFilename(), e);
		}
		finally {
			FileUtils.close(in);
		}
		return schema;
	}


	/**
	 * Serializes the specified Schema to XML and writes it to the specified output stream.
	 *
	 * @param schema
	 * @param out
	 * @param schemaOnly - If true, generates XML for actual schema only (i.e. Tables, Columns) Skips Data, Sql, and Action tags
	 */
	public void storeSchema(Schema schema, OutputStream out, boolean schemaOnly) {
		XStream xml = getConfiguredXStream(schemaOnly);
		xml.toXML(schema, out);
	}


	private XStream getConfiguredXStream(boolean schemaOnly) {
		return xStreamInstanceMap.get(schemaOnly).get();
	}


	/**
	 * Create a new XStream and configure aliases, attributes and collections for database migrations schema.
	 *
	 * @param schemaOnly - If true, generates XML for actual schema only (i.e. Tables, Columns) Skips Data, Sql, and Action tags
	 */
	private static XStream createXStreamInstance(boolean schemaOnly) {
		// Manually select the Java reflection provider so that Java constructors are called to apply instance initializers (see https://stackoverflow.com/a/29747705/1941654)
		XStream xml = new XStream(new PureJavaReflectionProvider());
		xml.useAttributeFor(Boolean.class);
		xml.useAttributeFor(boolean.class);
		xml.useAttributeFor(int.class);
		xml.useAttributeFor(Integer.class);
		xml.useAttributeFor(String.class);
		xml.useAttributeFor(DataTypes.class);
		xml.useAttributeFor(DataTypeEncodingTypeNames.class);
		xml.useAttributeFor(TableTypes.class);
		xml.useAttributeFor(OperationTypes.class);
		xml.useAttributeFor(CacheTypes.class);
		xml.useAttributeFor(OrderByDirections.class);

		// Assign permissions for allowed types (see https://stackoverflow.com/a/45152845/1941654)
		XStream.setupDefaultSecurity(xml);
		xml.allowTypesByWildcard(new String[]{"com.clifton.**"});

		xml.alias("migrations", Schema.class);

		// DDL
		xml.alias("table", Table.class);
		xml.alias("subclass", Subclass.class);
		xml.alias("column", Column.class);
		xml.alias("constraint", Constraint.class);
		xml.alias("index", Index.class);
		xml.alias("link", Link.class);

		xml.addImplicitCollection(Schema.class, "tableList", Table.class);
		xml.addImplicitCollection(Table.class, "columnList", Column.class);
		xml.addImplicitCollection(Table.class, "subclassList", Subclass.class);
		xml.addImplicitCollection(Table.class, "constraintList", Constraint.class);
		xml.addImplicitCollection(Table.class, "indexList", Index.class);
		xml.addImplicitCollection(Table.class, "linkList", Link.class);

		xml.addImplicitCollection(Subclass.class, "columnList", Column.class);
		xml.addImplicitCollection(Subclass.class, "subclassList", Subclass.class);
		xml.addImplicitCollection(Subclass.class, "constraintList", Constraint.class);
		xml.addImplicitCollection(Subclass.class, "indexList", Index.class);
		xml.addImplicitCollection(Subclass.class, "linkList", Link.class);

		xml.addImplicitCollection(Index.class, "columnList", "column", Column.class);
		xml.addImplicitCollection(Index.class, "includeColumnList", "includeColumn", Column.class);
		xml.addImplicitCollection(Constraint.class, "primaryColumnList", Column.class);

		if (schemaOnly) {
			xml.omitField(Schema.class, "dataList");
			xml.omitField(Schema.class, "sqlList");
			xml.omitField(Schema.class, "actionList");
		}
		else {
			// DML
			xml.alias("data", Data.class);
			xml.alias("value", Value.class);
			// need a converter so that the value of value tag is mapped to value attribute.
			xml.registerConverter(new com.thoughtworks.xstream.converters.Converter() {

				@Override
				public boolean canConvert(Class clazz) {
					return Value.class == clazz;
				}


				@Override
				public void marshal(Object valueArg, HierarchicalStreamWriter writer, @SuppressWarnings("unused") MarshallingContext context) {
					Value value = (Value) valueArg;
					writer.addAttribute("column", value.getColumn());
					writer.addAttribute("select", (value.isSelect() ? "true" : "false"));
					writer.setValue(value.getValue());
				}


				@Override
				public Object unmarshal(HierarchicalStreamReader reader, @SuppressWarnings("unused") UnmarshallingContext context) {
					Value value = new Value();
					value.setColumn(reader.getAttribute("column"));
					value.setSelect("true".equals(reader.getAttribute("select")));
					value.setValue(reader.getValue());
					return value;
				}
			});

			xml.addImplicitCollection(Schema.class, "dataList", Data.class);
			xml.addImplicitCollection(Data.class, "valueList", Value.class);

			// Action
			xml.alias("action", Action.class);
			xml.alias("arg", Argument.class);
			xml.alias("actionMapKey", String.class);
			xml.alias("actionMapValue", Action.class);
			// need a converter so that the value of arg tag is mapped to value attribute.
			xml.registerConverter(new com.thoughtworks.xstream.converters.Converter() {

				@Override
				public boolean canConvert(Class clazz) {
					return Argument.class == clazz;
				}


				@Override
				public void marshal(Object valueArg, HierarchicalStreamWriter writer, @SuppressWarnings("unused") MarshallingContext context) {
					Argument arg = (Argument) valueArg;
					writer.addAttribute("select", (arg.isSelect() ? "true" : "false"));
					writer.setValue(arg.getValue());
				}


				@Override
				public Object unmarshal(HierarchicalStreamReader reader, @SuppressWarnings("unused") UnmarshallingContext context) {
					Argument arg = new Argument();
					arg.setSelect("true".equals(reader.getAttribute("select")));
					arg.setValue(reader.getValue());
					return arg;
				}
			});

			xml.addImplicitCollection(Schema.class, "actionList", Action.class);
			xml.addImplicitCollection(Action.class, "argumentList", Argument.class);

			// SQL
			xml.alias("sql", Sql.class);
			xml.alias("statement", String.class);
			xml.addImplicitCollection(Schema.class, "sqlList", Sql.class);
			xml.addImplicitCollection(Sql.class, "statementList", String.class);
		}
		return xml;
	}
}
