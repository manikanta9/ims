package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;


/**
 * The <code>IntegerRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link Integer}.
 * Supports EQUALS, GREATER_THAN, LESS_THAN comparison conditions.
 *
 * @author vgomelsky
 */
public class IntegerRestrictionDefinition extends AbstractRestrictionDefinition {

	public IntegerRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public IntegerRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, false);
	}


	public IntegerRestrictionDefinition(String fieldName, String searchFieldName, boolean required) {
		this(fieldName, searchFieldName, null, required);
	}


	public IntegerRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, orderByFieldName, false);
	}


	public IntegerRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, required,
				new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN, ComparisonConditions.GREATER_THAN_OR_EQUALS,
						ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL}, null);
	}


	public IntegerRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS,
				ComparisonConditions.GREATER_THAN, ComparisonConditions.LESS_THAN}, null);
	}


	public IntegerRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                    ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomType) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, searchFieldCustomType);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	@Override
	public Object getValue(Object rawValue) {
		if (rawValue == null) {
			return null;
		}
		if (rawValue instanceof Number) {
			return ((Number) rawValue).intValue();
		}
		String stringValue = (String) rawValue;
		return (stringValue.isEmpty()) ? null : Integer.parseInt(stringValue);
	}
}
