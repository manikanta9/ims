package com.clifton.core.dataaccess.search;

import org.hibernate.Criteria;

import java.util.Map;


/**
 * The <code>SearchFormCustomConfigurer</code> interface can be implemented to allow custom criteria implementation for search forms that implement a specified search form interface
 * <p>
 * This can allow for cross project dependency as well as the use of multiple interfaces used without having to worry about inheritance
 *
 * @author manderson
 */
public interface SearchFormCustomConfigurer<T> {


	public Class<T> getSearchFormInterfaceClassName();


	public void configureCriteria(Criteria criteria, T searchForm, Map<String, Criteria> processedPathToCriteria);
}
