package com.clifton.core.dataaccess.file;

/**
 * The <code>FileDuplicateActions</code> enum contains common file actions that are used when saving files and a duplicate is found. i.e. Error, Replace, Skip
 *
 * @author manderson
 */
public enum FileDuplicateActions {


	SKIP(false, false), // Leave Existing File, Skip New File
	REPLACE(true, false), // Replace Existing File
	ERROR(false, true); // Throw Error

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////

	private final boolean replaceFile;
	private final boolean error;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	FileDuplicateActions(boolean replaceFile, boolean error) {
		this.replaceFile = replaceFile;
		this.error = error;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public boolean isReplaceFile() {
		return this.replaceFile;
	}


	public boolean isError() {
		return this.error;
	}
}
