package com.clifton.core.dataaccess.migrate.schema.sql;


import java.util.List;


/**
 * The <code>Sql</code> ...
 *
 * @author manderson
 */
public class Sql {

	/**
	 * Resource to load sql from
	 */
	private String fileName;

	/**
	 * List of Statements to execute
	 */
	private List<String> statementList;

	/**
	 * SQL statements can be grouped with DDL or META DATA
	 * to customize the order of SQL execution for a file.
	 */
	private String runWith;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public String getRunWith() {
		return this.runWith;
	}


	public void setRunWith(String runWith) {
		this.runWith = runWith;
	}


	public List<String> getStatementList() {
		return this.statementList;
	}


	public void setStatementList(List<String> statementList) {
		this.statementList = statementList;
	}
}
