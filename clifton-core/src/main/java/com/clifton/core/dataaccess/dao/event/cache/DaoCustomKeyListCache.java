package com.clifton.core.dataaccess.dao.event.cache;


import com.clifton.core.beans.IdentityObject;

import java.util.List;
import java.util.function.Function;


/**
 * Generic version of a cache which stores lists of IdentityObject
 *
 * @param <K> Key class type
 * @param <C> Class type of cached objects
 * @author davidi, MarkW
 */
public interface DaoCustomKeyListCache<K, C extends IdentityObject> {

	public List<C> computeIfAbsent(K key, Function<K, List<C>> lookUpMethod);
}

