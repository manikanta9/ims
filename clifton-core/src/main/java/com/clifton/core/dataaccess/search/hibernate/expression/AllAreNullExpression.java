package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.StandardBasicTypes;


/**
 * The <code>AllAreNullExpression</code> is used for search fields and generates a result where 1 is returned if all the passed fields are null and 0 otherwise.
 * By default, when multiple search properties are defined, we append these, by setting the searchFieldCustomType to COALESCE_SORT_ON_ALL_NULL this expression will be used:
 * <p/>
 * This is useful for composite ui fields where the backend field does not exist and is instead comprised of the existence of values in multiple fields.
 * An example of this Global Assignment used in Compliance Rules -> Rule Assignments, which is a composite of the existence of businessService.id and clientInvestmentAccount.id
 * Which resolves to
 * <code>IIF(COALESCE(this._businessService.id, this._clientInvestmentAccount.id) IS NULL, 1, 0)</code>
 *
 * @author dillonm
 */
public class AllAreNullExpression implements Criterion {

	private final String comparisonExpression;

	private final String[] propertyNames;

	private final Object value;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AllAreNullExpression(String comparisonExpression, String[] propertyNames, Object value) {
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.value = value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder innerFragmentSb = new StringBuilder("COALESCE(");
		boolean hasContent = false;
		for (String propertyName : this.propertyNames) {
			String column = criteriaQuery.getColumn(criteria, propertyName);
			if (hasContent) {
				innerFragmentSb.append(", ");
			}
			innerFragmentSb.append(column);
			if (!hasContent) {
				hasContent = true;
			}
		}
		innerFragmentSb.append(") IS Null");
		String innerSql = innerFragmentSb.toString();
		StringBuilder fragmentSb = new StringBuilder();
		fragmentSb.append(SQLIfElseFunction.render(criteria, criteriaQuery, innerSql, "1", "0"));
		fragmentSb.append(" ");
		fragmentSb.append(this.comparisonExpression);
		// For cases like: LIKE '%' + ? + '%' we don't want to add a second ?, but we need to for cases like =, >, <, etc.
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragmentSb.append(" ?");
		}
		return fragmentSb.toString();
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		TypedValue typedValue = new TypedValue(StandardBasicTypes.BOOLEAN, this.value);
		return new TypedValue[]{typedValue};
	}
}
