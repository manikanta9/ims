package com.clifton.core.dataaccess.dao.hibernate;

/**
 * Microsoft SQL Server 2012 Dialect
 * <p>
 * This version includes the "max" keyword needed when using data types like nvarchar(max) in read formulas like the one below which is
 * created be the DataTypes.COMPRESSED_TEXT data type.
 * <p>
 * <p>
 * <property name="entityJson">
 * <column name="EntityJson" read="CAST(DECOMPRESS(EntityJson) AS NVARCHAR(MAX))" write="COMPRESS(?)" />
 * </property>
 *
 * @author mwacker
 */
public class SQLServer2012Dialect extends org.hibernate.dialect.SQLServer2012Dialect {

	public SQLServer2012Dialect() {
		super();

		registerKeyword("max");
		registerKeyword("bit");
	}
}
