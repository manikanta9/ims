package com.clifton.core.dataaccess.dao;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapperWithBooleanResult;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>UpdatableDAO</code> interface defines methods exposed by all updatable Data Access Objects.
 *
 * @param <T>
 * @author vgomelsky
 */
public interface UpdatableDAO<T extends IdentityObject> extends ReadOnlyDAO<T> {

	/**
	 * Inserts or updates the specified DTOs in the database.
	 *
	 * @param beanList
	 */
	public void saveList(List<T> beanList);


	/**
	 * Replaces the old List with the new one in the database.
	 * Inserts/updates newList entries and deletes oldList entries that are not part of the newList.
	 *
	 * @param newList
	 * @param oldList
	 */
	public void saveList(List<T> newList, List<T> oldList);


	/**
	 * Inserts or updates the specified DTO in the database.
	 * Returns persistent instance tied to current session if one was started by the caller.
	 * Returned object, unlike passed detached object, may still have its fields updated on flush: "version".
	 *
	 * @param bean
	 */
	public T save(T bean);


	/**
	 * If an update, will check if non-system managed fields actually changed
	 * If not will skip the update
	 * Returns an object wrapper with the bean, as well as boolean result of true = save occurred and false = save was skipped;
	 */
	public ObjectWrapperWithBooleanResult<T> saveSmart(T bean);


	/**
	 * Deletes the specified bean from the database.
	 * Returns deleted DTO.
	 *
	 * @param bean
	 */
	public void delete(T bean);


	/**
	 * Deletes DTO object with the specified primary key.
	 * Returns deleted DTO.
	 *
	 * @param primaryKey
	 */
	public void delete(Serializable primaryKey);


	/**
	 * Deletes list of DTO objects from the database.
	 * For each DTO in the list will call the delete method
	 * individually so that auditing, etc are still handled.
	 *
	 * @param deleteList
	 */
	public void deleteList(List<T> deleteList);
}
