package com.clifton.core.dataaccess.dao.config;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.search.OrderByField;

import java.util.List;


/**
 * The <code>NoTableDAOConfig</code> class defines DAO configuration for DAO's that do not have an underlying table
 *
 * @param <T>
 * @author vgomelsky
 */
public class NoTableDAOConfig<T extends IdentityObject> extends DAOConfiguration<T> {

	public NoTableDAOConfig(String dtoClassName) {
		super(dtoClassName);
	}


	public NoTableDAOConfig(Class<T> dtoClass) {
		super(dtoClass);
	}


	@Override
	public Table getTable() {
		throw new IllegalStateException("No table is allowed for DAO " + getBeanClass());
	}


	@Override
	public List<OrderByField> getOrderByList() {
		return null;
	}
}
