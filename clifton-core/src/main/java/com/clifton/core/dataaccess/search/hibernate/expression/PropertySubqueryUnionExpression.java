package com.clifton.core.dataaccess.search.hibernate.expression;

import org.hibernate.Criteria;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.DetachedCriteria;

import java.util.List;


/**
 * The <code>PropertySubqueryUnionExpression</code> is an implementation of SubqueryUnionExpression for searching for a property name, i.e. property IN (union criteria)
 * Similar to {@link org.hibernate.criterion.PropertySubqueryExpression}, but uses UNION of multiple detached criteria
 *
 * @author manderson
 */
public class PropertySubqueryUnionExpression extends SubqueryUnionExpression {

	private final String propertyName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private PropertySubqueryUnionExpression(String propertyName, String operator, List<DetachedCriteria> detachedCriteriaList, boolean unionAll) {
		super(operator, detachedCriteriaList, unionAll);
		this.propertyName = propertyName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static PropertySubqueryUnionExpression forPropertyIn(String propertyName, List<DetachedCriteria> detachedCriteriaList) {
		return new PropertySubqueryUnionExpression(propertyName, "IN", detachedCriteriaList, false);
	}


	public static PropertySubqueryUnionExpression forPropertyInUnionAll(String propertyName, List<DetachedCriteria> detachedCriteriaList) {
		return new PropertySubqueryUnionExpression(propertyName, "IN", detachedCriteriaList, true);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String toLeftSqlString(Criteria criteria, CriteriaQuery criteriaQuery) {
		return criteriaQuery.getColumn(criteria, this.propertyName);
	}
}
