package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.ShortType;
import org.hibernate.type.Type;


/**
 * A class used to construct a criterion based on the difference between two dates.
 *
 * @author davidi
 */
public class DateDiffExpression implements Criterion {

	private String dateDiffFunction;

	private String comparisonExpression;

	private String[] propertyNames;

	private Object value;

	private Type valueType;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructs and validates the DateDiffExpression.  The 'value' parameter must be an integer representing a time duration in seconds.
	 * <p>
	 * Note:  Internally, comparisons are done in milliseconds to mitigate precision issues between the DATEDIFF SQL function
	 * and DateUtils time difference calculations.
	 */
	public DateDiffExpression(String dateDiffFunction, String comparisonExpression, String[] propertyNames, Object value) {
		this.dateDiffFunction = dateDiffFunction;
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.value = value;

		if (this.value instanceof Long) {
			this.valueType = LongType.INSTANCE;
		}
		else if (this.value instanceof Integer) {
			this.valueType = IntegerType.INSTANCE;
		}
		else if (this.value instanceof Short) {
			this.valueType = ShortType.INSTANCE;
		}
		else {
			throw new IllegalArgumentException("Invalid 'value' data type '" + (value == null ? null : value.getClass()) + "' in DateDiffExpression. Only Long, Integer, and Short types are supported.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		ValidationUtils.assertTrue(this.propertyNames.length == 2, "Two date fields are required for the DATEDIFF function.");
		String sqlFormat = String.format("%s %s ?", this.dateDiffFunction, this.comparisonExpression);
		String startDate = criteriaQuery.getColumn(criteria, this.propertyNames[0]);
		String endDate = criteriaQuery.getColumn(criteria, this.propertyNames[1]);

		return String.format(sqlFormat, startDate, endDate);
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[]{new TypedValue(this.valueType, this.value)};
	}
}
