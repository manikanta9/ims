package com.clifton.core.dataaccess.search;


import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>SearchRestriction</code> class defines a single restriction (condition) in a search query.
 * It specifies a name, comparator and string representation of the value.
 *
 * @author vgomelsky
 */
public class SearchRestriction implements Serializable {

	private String field;
	private ComparisonConditions comparison;
	private Object value;
	private DataTypeNames dataTypeName;

	/**
	 * This field can be used to create multiple criteria that should be configured as an OR condition by the code
	 * handling the search restriction list.  In the case of using the orRestrictionList the field, comparison, and value fields can
	 * optionally be set to create a <field <comparison> value> AND (searchRestriction1 <OR> searchRestriction2 <OR> searchRestriction...)
	 * but that is left up to the implementing code.
	 */
	List<SearchRestriction> orRestrictionList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SearchRestriction() {
		super();
	}


	public SearchRestriction(String beanFieldName, ComparisonConditions comparison, Object value) {
		this(beanFieldName, comparison, value, null);
	}


	public SearchRestriction(String beanFieldName, ComparisonConditions comparison, Object value, DataTypeNames dataTypeName) {
		this.field = beanFieldName;
		this.comparison = comparison;
		this.value = value;
		this.dataTypeName = dataTypeName;
	}


	@Override
	public String toString() {
		return this.field + " " + this.comparison + " " + ObjectUtils.toString(this.value);
	}


	public String toSqlString() {
		return this.field + " " + this.comparison.getComparisonExpression() + (!StringUtils.contains(this.comparison.getComparisonExpression(), "LIKE") ? " ?" : "");
	}


	/**
	 * Returns a new search restriction instance using the provided field with this restriction's comparison and value.
	 */
	public SearchRestriction copyForField(String field) {
		return new SearchRestriction(field, getComparison(), getValue());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getField() {
		return this.field;
	}


	public void setField(String field) {
		this.field = field;
	}


	public ComparisonConditions getComparison() {
		return this.comparison;
	}


	public void setComparison(ComparisonConditions comparison) {
		this.comparison = comparison;
	}


	public Object getValue() {
		return this.value;
	}


	public void setValue(Object value) {
		this.value = value;
	}


	public DataTypeNames getDataTypeName() {
		return this.dataTypeName;
	}


	public void setDataTypeName(DataTypeNames dataTypeName) {
		this.dataTypeName = dataTypeName;
	}


	public List<SearchRestriction> getOrRestrictionList() {
		return this.orRestrictionList;
	}


	public void setOrRestrictionList(List<SearchRestriction> orRestrictionList) {
		this.orRestrictionList = orRestrictionList;
	}
}
