package com.clifton.core.dataaccess.search.form.entity;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;

import java.util.Date;


/**
 * The <code>BaseEntityDateRangeWithTimeSearchForm</code> is a date range search form which allows for searches for entities whose date values fall into any
 * time on a given day, rather than at a precise in time. This allows searching for entities which have a {@link #startDate} or {@link #endDate} at any point on
 * a given day.
 * <p>
 * When processed, this search form expands each provided date parameter to a full 24-hour period, starting at the provided time. If specific times are desired
 * rather than the expanded 24-hour time range, then use {@link BaseEntityDateRangeWithoutTimeSearchForm}.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseEntityDateRangeSearchForm Parent fields}
 * <li>{@link #startDate}
 * <li>{@link #endDate}
 * </ul>
 *
 * @author NickK
 * @see BaseEntityDateRangeWithoutTimeSearchForm
 */
public abstract class BaseEntityDateRangeWithTimeSearchForm extends BaseEntityDateRangeSearchForm {

	@SearchField(dateFieldIncludesTime = true)
	private Date startDate;

	@SearchField(dateFieldIncludesTime = true)
	private Date endDate;

	@SearchField(searchField = "startDate,endDate", searchFieldCustomType = SearchFieldCustomTypes.DATE_DIFFERENCE_SECONDS)
	private Long executionTimeInSeconds;

	@SearchField(searchField = "startDate,endDate", searchFieldCustomType = SearchFieldCustomTypes.DATE_DIFFERENCE_MILLISECONDS)
	private Long executionTimeInMillis;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isIncludeTime() {
		return true;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Long getExecutionTimeInSeconds() {
		return this.executionTimeInSeconds;
	}


	public void setExecutionTimeInSeconds(Long executionTimeInSeconds) {
		this.executionTimeInSeconds = executionTimeInSeconds;
	}


	public Long getExecutionTimeInMillis() {
		return this.executionTimeInMillis;
	}


	public void setExecutionTimeInMillis(Long executionTimeInMillis) {
		this.executionTimeInMillis = executionTimeInMillis;
	}
}
