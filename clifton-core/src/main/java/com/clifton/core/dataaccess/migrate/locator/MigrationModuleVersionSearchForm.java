package com.clifton.core.dataaccess.migrate.locator;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author mitchellf
 */
public class MigrationModuleVersionSearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "migrationModuleName,migrationPath")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField
	private Integer versionNumber;

	@SearchField
	private String migrationModuleName;

	@SearchField
	private String migrationPath;

	@SearchField
	private Boolean preDdlStepCompleted;

	@SearchField
	private Boolean ddlStepCompleted;

	@SearchField
	private Boolean metaDataStepCompleted;

	@SearchField
	private Boolean dataStepCompleted;

	@SearchField
	private Boolean actionStepCompleted;

	@SearchField
	private Boolean dataActionStepCompleted;

	@SearchField
	private Boolean sqlStepCompleted;

	@SearchField
	private Boolean skippedDueToConditionalSql;

	@SearchField
	private Date initialExecutionDate;

	@SearchField
	private Date lastExecutionDate;


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getVersionNumber() {
		return this.versionNumber;
	}


	public void setVersionNumber(Integer versionNumber) {
		this.versionNumber = versionNumber;
	}


	public String getMigrationModuleName() {
		return this.migrationModuleName;
	}


	public void setMigrationModuleName(String migrationModuleName) {
		this.migrationModuleName = migrationModuleName;
	}


	public String getMigrationPath() {
		return this.migrationPath;
	}


	public void setMigrationPath(String migrationPath) {
		this.migrationPath = migrationPath;
	}


	public Boolean getPreDdlStepCompleted() {
		return this.preDdlStepCompleted;
	}


	public void setPreDdlStepCompleted(Boolean preDdlStepCompleted) {
		this.preDdlStepCompleted = preDdlStepCompleted;
	}


	public Boolean getDdlStepCompleted() {
		return this.ddlStepCompleted;
	}


	public void setDdlStepCompleted(Boolean ddlStepCompleted) {
		this.ddlStepCompleted = ddlStepCompleted;
	}


	public Boolean getMetaDataStepCompleted() {
		return this.metaDataStepCompleted;
	}


	public void setMetaDataStepCompleted(Boolean metaDataStepCompleted) {
		this.metaDataStepCompleted = metaDataStepCompleted;
	}


	public Boolean getDataStepCompleted() {
		return this.dataStepCompleted;
	}


	public void setDataStepCompleted(Boolean dataStepCompleted) {
		this.dataStepCompleted = dataStepCompleted;
	}


	public Boolean getActionStepCompleted() {
		return this.actionStepCompleted;
	}


	public void setActionStepCompleted(Boolean actionStepCompleted) {
		this.actionStepCompleted = actionStepCompleted;
	}


	public Boolean getDataActionStepCompleted() {
		return this.dataActionStepCompleted;
	}


	public void setDataActionStepCompleted(Boolean dataActionStepCompleted) {
		this.dataActionStepCompleted = dataActionStepCompleted;
	}


	public Boolean getSqlStepCompleted() {
		return this.sqlStepCompleted;
	}


	public void setSqlStepCompleted(Boolean sqlStepCompleted) {
		this.sqlStepCompleted = sqlStepCompleted;
	}


	public Boolean getSkippedDueToConditionalSql() {
		return this.skippedDueToConditionalSql;
	}


	public void setSkippedDueToConditionalSql(Boolean skippedDueToConditionalSql) {
		this.skippedDueToConditionalSql = skippedDueToConditionalSql;
	}


	public Date getInitialExecutionDate() {
		return this.initialExecutionDate;
	}


	public void setInitialExecutionDate(Date initialExecutionDate) {
		this.initialExecutionDate = initialExecutionDate;
	}


	public Date getLastExecutionDate() {
		return this.lastExecutionDate;
	}


	public void setLastExecutionDate(Date lastExecutionDate) {
		this.lastExecutionDate = lastExecutionDate;
	}
}
