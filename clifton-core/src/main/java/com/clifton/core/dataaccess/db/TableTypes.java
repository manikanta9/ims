package com.clifton.core.dataaccess.db;


/**
 * The <code>DataTypes</code> enumeration defines supported table types.
 *
 * @author vgomelsky
 */
public enum TableTypes {

	/**
	 * A basic table that instead of using an int IDENTITY for the Primary key uses an
	 * int NATURAL ID which is expected to be set in the code, not generated via
	 * the database as an identity is.
	 */
	BASIC_WITH_NATURAL_KEY(true, true),

	/**
	 * A table that has 5 standard record stamp fields. Most tables are of this type.
	 */
	BASIC(true),
	/**
	 * A simple table with no additional standard fields.
	 */
	SIMPLE(false);


	TableTypes(boolean appendStandardFields) {
		this(appendStandardFields, false);
	}


	TableTypes(boolean appendStandardFields, boolean usesNaturalId) {
		this.appendStandardFields = appendStandardFields;
		this.usesNaturalId = usesNaturalId;
	}


	private final boolean appendStandardFields;
	private final boolean usesNaturalId;


	public boolean isAppendStandardFields() {
		return this.appendStandardFields;
	}


	public boolean isUsesNaturalId() {
		return this.usesNaturalId;
	}
}
