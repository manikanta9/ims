package com.clifton.core.dataaccess.datasource;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.core.Ordered;
import org.springframework.jdbc.CannotGetJdbcConnectionException;
import org.springframework.jdbc.core.JdbcTemplate;

import java.sql.SQLException;


/**
 * Post processor that has two functions
 * 1. Pauses startup until the database is up and running
 * 2. Aborts startup if the database is not for the same environment level as the application
 * <p>
 * Note: if healthCheckSecondsBeforeRetry is set to 0 the post processor will be skipped
 *
 * @author theodorez
 */
public class DatabaseHealthCheckPostProcessor implements BeanFactoryPostProcessor, Ordered {

	private int healthCheckSecondsBeforeRetry = 10;
	private String healthCheckSql = "SELECT 1";

	private String applicationEnvironment;
	private String applicationEnvironmentCheckSql = "SELECT EnvironmentLevel FROM env.Environment";

	private JdbcTemplate healthCheckJdbcTemplate;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Ensures that this post processor will be the first Ordered one to be executed.
	 * <p>
	 * Note: Execution of post processors is as follows: PriorityOrdered, Ordered, Non-ordered
	 */
	@Override
	public int getOrder() {
		return Ordered.HIGHEST_PRECEDENCE;
	}


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		checkDatabaseConnection();
		checkDatabaseEnvironmentLevel();
	}


	private void checkDatabaseConnection() throws BeansException {
		if (getHealthCheckSecondsBeforeRetry() != 0) {
			while (!Thread.interrupted()) {
				try {
					getHealthCheckJdbcTemplate().queryForList(getHealthCheckSql());
					break;
				}
				catch (CannotGetJdbcConnectionException e) {
					if (e.getCause() instanceof SQLException) {
						SQLException sqlException = (SQLException) e.getCause();
						//Error code 0 means the database hasn't started yet
						if (sqlException.getErrorCode() == 0) {
							LogUtils.error(getClass(), "Error occurred checking status of database connection." + sqlException);
							sleep();
							continue;
						}
					}
					throw e;
				}
			}
		}
	}


	private void checkDatabaseEnvironmentLevel() {
		//If the App Environment is Null it is probably due to a test context that doesn't contain the value
		//If the value is not null but is empty, then the property has been intentionally set to blank
		if (getApplicationEnvironment() == null) {
			LogUtils.warn(getClass(), "Cannot validate database environment matches application environment - Application Environment value is empty");
		}
		else if (!StringUtils.isEmpty(getApplicationEnvironment())) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "Database Environment SQL: ", getApplicationEnvironmentCheckSql(), " Application Environment: ", getApplicationEnvironment()));
			String databaseEnvironment = getHealthCheckJdbcTemplate().queryForObject(getApplicationEnvironmentCheckSql(), String.class);
			if (!StringUtils.isEmpty(databaseEnvironment)) {
				LogUtils.debug(getClass(), "Database Environment: " + databaseEnvironment);
				if (!StringUtils.isEqualIgnoreCase(getApplicationEnvironment(), databaseEnvironment)) {
					throw new RuntimeException("ABORTING STARTUP: Application Environment [" + getApplicationEnvironment() + "] does not match Database Environment [" + databaseEnvironment + "]");
				}
			}
			else {
				throw new RuntimeException("ABORTING STARTUP: Cannot validate database environment matches application environment - Database Environment value is empty");
			}
		}
		else {
			throw new RuntimeException("ABORTING STARTUP: Cannot validate database environment matches application environment - Application Environment value is empty");
		}
	}


	private void sleep() {
		try {
			LogUtils.info(getClass(), "Sleeping thread that verifies database is up.");
			Thread.sleep(getHealthCheckSecondsBeforeRetry() * 1000);
		}
		catch (InterruptedException e) {
			LogUtils.error(getClass(), "Thread interrupted while sleeping the database check.", e);
			Thread.currentThread().interrupt();
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getHealthCheckSecondsBeforeRetry() {
		return this.healthCheckSecondsBeforeRetry;
	}


	public void setHealthCheckSecondsBeforeRetry(int healthCheckSecondsBeforeRetry) {
		this.healthCheckSecondsBeforeRetry = healthCheckSecondsBeforeRetry;
	}


	public JdbcTemplate getHealthCheckJdbcTemplate() {
		return this.healthCheckJdbcTemplate;
	}


	public void setHealthCheckJdbcTemplate(JdbcTemplate healthCheckJdbcTemplate) {
		this.healthCheckJdbcTemplate = healthCheckJdbcTemplate;
	}


	public String getHealthCheckSql() {
		return this.healthCheckSql;
	}


	public void setHealthCheckSql(String healthCheckSql) {
		this.healthCheckSql = healthCheckSql;
	}


	public String getApplicationEnvironment() {
		return this.applicationEnvironment;
	}


	public void setApplicationEnvironment(String applicationEnvironment) {
		this.applicationEnvironment = applicationEnvironment;
	}


	public String getApplicationEnvironmentCheckSql() {
		return this.applicationEnvironmentCheckSql;
	}


	public void setApplicationEnvironmentCheckSql(String applicationEnvironmentCheckSql) {
		this.applicationEnvironmentCheckSql = applicationEnvironmentCheckSql;
	}
}
