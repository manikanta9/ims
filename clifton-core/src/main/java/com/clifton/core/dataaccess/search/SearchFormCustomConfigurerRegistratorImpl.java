package com.clifton.core.dataaccess.search;

import com.clifton.core.context.CurrentContextApplicationListener;
import com.clifton.core.logging.LogUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ApplicationContextEvent;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * The <code>SearchFormRestrictionConfigurerRegistratorImpl</code> finds all spring managed beans that implement {@link SearchFormRestrictionConfigurer}
 * and registers the search restriction
 *
 * @author manderson
 */
@Component
public class SearchFormCustomConfigurerRegistratorImpl implements CurrentContextApplicationListener<ApplicationContextEvent> {

	private ApplicationContext applicationContext;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("rawtypes")
	public void onCurrentContextApplicationEvent(ApplicationContextEvent event) {
		ApplicationContext eventContext = getApplicationContext();

		if (event instanceof ContextRefreshedEvent) {
			// Lookup beans implementing SearchFormRestrictionConfigurer
			Map<String, SearchFormCustomConfigurer> beanMap = eventContext.getBeansOfType(SearchFormCustomConfigurer.class);
			if (beanMap != null) {
				for (Map.Entry<String, SearchFormCustomConfigurer> stringSearchFormCustomConfigurerEntry : beanMap.entrySet()) {
					SearchUtils.registerSearchFormCustomConfigurer(stringSearchFormCustomConfigurerEntry.getValue());
					LogUtils.info(getClass(), "Registered Search Form Custom Configurer: " + stringSearchFormCustomConfigurerEntry.getKey());
				}
			}
		}

		else if (event instanceof ContextClosedEvent) {
			SearchUtils.clearSearchFormCustomConfigurers();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
