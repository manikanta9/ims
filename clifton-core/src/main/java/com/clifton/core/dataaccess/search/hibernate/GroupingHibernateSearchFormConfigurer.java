package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.dataaccess.search.grouping.GroupingAggregateProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingAggregateTypes;
import com.clifton.core.dataaccess.search.grouping.GroupingProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchConfigurer;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchForm;
import com.clifton.core.dataaccess.search.grouping.transformer.GroupedEntityResultTransformer;
import com.clifton.core.dataaccess.search.grouping.transformer.GroupedHierarchicalResultTransformer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.Criteria;
import org.hibernate.criterion.Projection;
import org.hibernate.criterion.ProjectionList;
import org.hibernate.criterion.Projections;
import org.hibernate.sql.JoinType;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.IntegerType;


public class GroupingHibernateSearchFormConfigurer<T extends IdentityObject> implements GroupingSearchConfigurer {

	private final HibernateSearchFormConfigurer entitySearchFormConfigurer;

	private final GroupingSearchForm groupingSearchForm;

	private final boolean entityView;

	private final DaoLocator daoLocator;

	private final DAOConfiguration<T> daoConfiguration;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * @param entityView - if True will return a list of GroupedEntityResults, else a list of GroupedHierarchicalResults
	 */
	public GroupingHibernateSearchFormConfigurer(HibernateSearchFormConfigurer entitySearchFormConfigurer, boolean entityView, DaoLocator daoLocator, DAOConfiguration<T> daoConfiguration) {
		ValidationUtils.assertNotNull(entitySearchFormConfigurer, "Entity Search Form Configurer is required");
		ValidationUtils.assertTrue(entitySearchFormConfigurer.getSortableSearchForm() instanceof GroupingSearchForm, "Search form must implement GroupingSearchForm");
		ValidationUtils.assertNotEmpty(((GroupingSearchForm) entitySearchFormConfigurer.getSortableSearchForm()).getGroupingPropertyList(), "At least one property to group on is required.");

		this.entitySearchFormConfigurer = entitySearchFormConfigurer;
		this.groupingSearchForm = ((GroupingSearchForm) entitySearchFormConfigurer.getSortableSearchForm());
		this.entityView = entityView;
		this.daoLocator = daoLocator;
		this.daoConfiguration = daoConfiguration;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria) {
		getEntitySearchFormConfigurer().configureCriteria(criteria);

		ProjectionList projectionList = Projections.projectionList();
		criteria.setProjection(projectionList);

		for (GroupingProperty groupingProperty : getGroupingSearchForm().getGroupingPropertyList()) {
			String propertyName = groupingProperty.getBeanPropertyName();

			ValidationUtils.assertNotEmpty(propertyName, "Missing bean property name for grouping property " + groupingProperty);
			if (propertyName.contains(".")) {
				projectionList.add(Projections.groupProperty(HibernateUtils.getAliasedCriteriaPropertyPath(propertyName, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.LEFT_OUTER_JOIN, getEntitySearchFormConfigurer().getProcessedPathToCriteria())));
			}
			else {
				projectionList.add(Projections.groupProperty(groupingProperty.getBeanPropertyName()));
			}
		}

		// Always add COUNT
		projectionList.add(Projections.rowCount());

		// Add additional aggregates if necessary
		for (GroupingAggregateProperty groupingAggregateProperty : CollectionUtils.getIterable(getGroupingSearchForm().getGroupingAggregatePropertyList())) {
			if (!StringUtils.isEmpty(groupingAggregateProperty.getBeanPropertyName())) {
				projectionList.add(getProjectionForAggregateBeanPropertyName(groupingAggregateProperty.getAggregateType(), groupingAggregateProperty.getBeanPropertyName(), criteria));
			}
			else {
				// If no bean property name, then use the search form field name and it's formula
				projectionList.add(getProjectionForAggregateSearchFormFieldName(groupingAggregateProperty.getAggregateType(), groupingAggregateProperty.getSearchFormFieldName(), criteria));
			}
		}

		if (this.entityView) {
			criteria.setResultTransformer(new GroupedEntityResultTransformer<>(getGroupingSearchForm(), this.daoLocator, this.daoConfiguration));
		}
		else {
			criteria.setResultTransformer(new GroupedHierarchicalResultTransformer<>(getGroupingSearchForm(), this.daoLocator, this.daoConfiguration));
		}
	}


	protected Projection getProjectionForAggregateBeanPropertyName(GroupingAggregateTypes aggregateType, String propertyName, Criteria criteria) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(propertyName), "Aggregate type " + aggregateType.name() + " supplied without a property name");
		String aliasProperty = HibernateUtils.getAliasedCriteriaPropertyPath(propertyName, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.LEFT_OUTER_JOIN, getEntitySearchFormConfigurer().getProcessedPathToCriteria());
		switch (aggregateType) {
			case COUNT:
				return Projections.count(aliasProperty);
			case SUM:
				return Projections.sum(aliasProperty);
			default:
				throw new ValidationException("Unsupported aggregate type " + aggregateType.name());
		}
	}


	protected Projection getProjectionForAggregateSearchFormFieldName(GroupingAggregateTypes aggregateType, String searchFormFieldName, Criteria criteria) {
		ValidationUtils.assertFalse(StringUtils.isEmpty(searchFormFieldName), "Aggregate type " + aggregateType.name() + " supplied without a property name or search form field name.");
		SearchRestrictionDefinition restrictionDefinition = this.entitySearchFormConfigurer.getRestrictionDefinitionMap().get(searchFormFieldName);
		ValidationUtils.assertNotNull(restrictionDefinition, "Search restriction definition for search form field name " + searchFormFieldName + " does not exist");
		ValidationUtils.assertNotEmpty(restrictionDefinition.getSqlFormula(), "You can only use search form fields for formulas, otherwise please use the bean property name.");
		String[] searchFields = restrictionDefinition.getSearchFieldName().split(",");
		String[] processedSearchFields = new String[searchFields.length];
		for (int i = 0; i < searchFields.length; i++) {
			String aliasedProperty = HibernateUtils.getAliasedCriteriaPropertyPath(searchFields[i], criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.LEFT_OUTER_JOIN, getEntitySearchFormConfigurer().getProcessedPathToCriteria());
			processedSearchFields[i] = aliasedProperty;
		}
		switch (aggregateType) {
			case COUNT:
				return new FormulaProjection("COUNT(" + restrictionDefinition.getSqlFormula() + ")", processedSearchFields, IntegerType.INSTANCE);
			case SUM:
				return new FormulaProjection("SUM(" + restrictionDefinition.getSqlFormula() + ")", processedSearchFields, BigDecimalType.INSTANCE);
			default:
				throw new ValidationException("Unsupported aggregate type " + aggregateType.name());
		}
	}


	@Override
	public boolean configureOrderBy(Criteria criteria) {
		return true;
	}


	@Override
	public boolean isReadUncommittedRequested() {
		return getEntitySearchFormConfigurer().isReadUncommittedRequested();
	}


	@Override
	public int getLimit() {
		return getEntitySearchFormConfigurer().getLimit();
	}


	@Override
	public int getStart() {
		return getEntitySearchFormConfigurer().getStart();
	}


	@Override
	public Integer getMaxLimit() {
		return getEntitySearchFormConfigurer().getMaxLimit();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public HibernateSearchFormConfigurer getEntitySearchFormConfigurer() {
		return this.entitySearchFormConfigurer;
	}


	@Override
	public GroupingSearchForm getGroupingSearchForm() {
		return this.groupingSearchForm;
	}
}
