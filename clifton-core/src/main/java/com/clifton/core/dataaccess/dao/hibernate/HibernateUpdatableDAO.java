package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapperWithBooleanResult;
import com.clifton.core.beans.UpdatableEntity;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.Criteria;
import org.hibernate.CustomEntityDirtinessStrategy;
import org.hibernate.Query;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import java.io.Serializable;
import java.util.Date;
import java.util.List;


/**
 * The <code>HibernateUpdatableDAO</code> class provides DAO implementation based on Hibernate.
 *
 * @param <T>
 * @author vgomelsky
 */
public class HibernateUpdatableDAO<T extends IdentityObject> extends HibernateReadOnlyDAO<T> implements AdvancedUpdatableDAO<T, Criteria> {

	private ContextHandler contextHandler;


	@Override
	@Transactional
	public void saveList(List<T> beanList) {
		for (int i = 0; i < CollectionUtils.getSize(beanList); i++) {
			beanList.set(i, save(beanList.get(i), true));
		}
		if (DaoUtils.isPostUpdateFlushEnabled()) {
			flushSession();
		}
	}


	@Override
	@Transactional
	public void saveList(List<T> newList, List<T> oldList) {
		for (int i = 0; i < CollectionUtils.getSize(newList); i++) {
			if (isUpsertNeeded(newList.get(i), oldList)) {
				newList.set(i, save(newList.get(i), true));
			}
		}
		if (DaoUtils.isPostUpdateFlushEnabled()) {
			flushSession();
		}
		for (T old : CollectionUtils.getIterable(oldList)) {
			if (newList == null || !newList.contains(old)) {
				delete(old, true);
			}
		}
		if (DaoUtils.isPostUpdateFlushEnabled()) {
			flushSession();
		}
	}


	/**
	 * Checks if the newBean is in the oldList and if it is equal to the oldBean in the oldList
	 * If not in the old list or in the list and not equal to the oldBean it will save the newBean.
	 **/
	private boolean isUpsertNeeded(T newBean, List<T> oldList) {
		boolean upsert = true;
		if (!CollectionUtils.isEmpty(oldList)) {
			int oldBeanIndex = oldList.indexOf(newBean);
			if (oldBeanIndex != -1) {
				if (CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(newBean, oldList.get(oldBeanIndex), false))) {
					upsert = false;
				}
			}
		}
		return upsert;
	}


	@Override
	public T save(T bean) {
		return save(bean, false);
	}


	@Override
	public ObjectWrapperWithBooleanResult<T> saveSmart(T bean) {
		if (!bean.isNewBean()) {
			T originalBean = findByPrimaryKey(bean.getIdentity());
			if (CollectionUtils.isEmpty(CoreCompareUtils.getNoEqualPropertiesWithSetters(originalBean, bean, false))) {
				return ObjectWrapperWithBooleanResult.fail(originalBean);
			}
		}
		return ObjectWrapperWithBooleanResult.success(save(bean));
	}


	private T save(T bean, boolean delaySessionFlush) {
		T result = bean;
		populateRecordStamp(bean);
		boolean insert = bean.isNewBean();
		boolean observer = insert ? super.isInsertObserverRegistered() : super.isUpdateObserverRegistered();
		DaoEventTypes event = insert ? DaoEventTypes.INSERT : DaoEventTypes.UPDATE;

		if (observer) {
			super.getEventObserver(event).beforeTransactionMethodCall(this, event, bean);
		}

		try {
			if (insert) {
				insert(bean);
			}
			else {
				result = update(bean);
			}
			/*
			 * Within an active transaction, workflow aware entities can become out of sync in the Hibernate session.
			 * Flush the state to ensure the subsequent updates within the same transaction are not getting stale data.
			 * One use for this is on saving an EFP trade group (the group and it's trades are saved and then a
			 * workflow transition occurs within the same transaction, which would fail without a flush).
			 */
			if (DaoUtils.isPostUpdateFlushEnabled() && !delaySessionFlush) {
				flushSession();
			}
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(event).afterTransactionMethodCall(this, event, bean, e);
			}
			throw new DaoException("Error in " + (insert ? "insert" : "update") + "(" + bean + ")", bean, e);
		}

		if (observer) {
			// must pass persistent instance which may get modified on flush
			super.getEventObserver(event).afterTransactionMethodCall(this, event, result, null);
		}
		return result;
	}


	@Transactional
	protected void insert(T bean) {
		boolean observer = super.isInsertObserverRegistered();
		if (observer) {
			super.getEventObserver(DaoEventTypes.INSERT).beforeMethodCall(this, DaoEventTypes.INSERT, bean);
		}

		try {
			getSessionFactory().getCurrentSession().save(bean);
			if (!DaoUtils.isInsertDirtyCheckingDisabled() && TransactionSynchronizationManager.isActualTransactionActive()) {
				/*
				 * Register the saved version of a new bean for flushing since it now has an ID and will be picked up
				 * by the dirty checking strategy. When saving hierarchical lists, a new object may reference another
				 * new object in the list as its parent. To avoid the child being saved before the parent in the list,
				 * register the child so it will be correctly flushed when the parent object is saved.
				 */
				registerBeanForDirtyChecking(bean);
			}
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(DaoEventTypes.INSERT).afterMethodCall(this, DaoEventTypes.INSERT, bean, e);
			}
			if (e instanceof ValidationException) {
				throw e; // keep validation exceptions unchanged to prevent them from logging
			}
			throw new DaoException("Error in " + getConfiguration().getTableName() + " insert(" + bean + ")", bean, e);
		}

		if (observer) {
			super.getEventObserver(DaoEventTypes.INSERT).afterMethodCall(this, DaoEventTypes.INSERT, bean, null);
		}
	}


	/**
	 * Returns persistent instance tied to current session.  It's important to use this object when the caller
	 * started a transaction because Hibernate session may not be flushed until that transaction is committed.
	 * Returned object fields but not passed detached object fields may still be updated: "version".
	 */
	@Transactional
	protected T update(final T bean) {
		boolean observer = super.isUpdateObserverRegistered();
		T result = null;
		if (observer) {
			super.getEventObserver(DaoEventTypes.UPDATE).beforeMethodCall(this, DaoEventTypes.UPDATE, bean);
		}

		try {
			registerBeanForDirtyChecking(bean);
			@SuppressWarnings("unchecked")
			T castResult = (T) getSessionFactory().getCurrentSession().merge(bean);
			result = castResult;
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(DaoEventTypes.UPDATE).configurePersistedInstance(result, bean);
				super.getEventObserver(DaoEventTypes.UPDATE).afterMethodCall(this, DaoEventTypes.UPDATE, bean, e);
			}
			if (e instanceof ValidationException) {
				throw e; // keep validation exceptions unchanged to prevent them from logging
			}
			throw new DaoException("Error in " + getConfiguration().getTableName() + " update(" + bean + ")", bean, e);
		}

		if (observer) {
			// must pass persistent instance which may get modified on flush
			super.getEventObserver(DaoEventTypes.UPDATE).configurePersistedInstance(result, bean);
			super.getEventObserver(DaoEventTypes.UPDATE).afterMethodCall(this, DaoEventTypes.UPDATE, result, null);
		}
		return result;
	}


	/**
	 * Hibernate checks all entities loaded in an active session for modifications when flushing to the database. If
	 * associated entities were modified during binding or other means, they could be saved. To avoid this, we get
	 * our {@link org.hibernate.CustomEntityDirtinessStrategy} implementation that is used by Hibernate to register
	 * a bean as one available for dirty checking and ultimately flushing to the DB. Thus, the only entities that
	 * will be flushed to the database will be those that we have specifically called for update via our DAOs.
	 * <p>
	 * NOTE: This method assumes that Hibernate is configured with a SessionFactory with the following property set:
	 * hibernate.entity_dirtiness_strategy=com.clifton.core.dataaccess.dao.hibernate.DirtyCheckingStrategy
	 * The retrieved {@link CustomEntityDirtinessStrategy} is cast to our {@link DirtyCheckingStrategy} without
	 * checking for null and instanceof to improve performance.
	 */
	private void registerBeanForDirtyChecking(T bean) {
		CustomEntityDirtinessStrategy strategy = this.getSessionFactory().getSessionFactoryOptions().getCustomEntityDirtinessStrategy();
		((DirtyCheckingStrategy) strategy).registerEntityAsDirtyAware(getContextHandler(), bean);
	}


	@Override
	public void delete(Serializable primaryKey) {
		T bean = findByPrimaryKey(primaryKey);
		AssertUtils.assertNotNull(bean, "Cannot find bean to delete with primary key: " + primaryKey);
		delete(bean);
	}


	@Override
	public void delete(T bean) {
		delete(bean, false);
	}


	private void delete(T bean, boolean delaySessionFlush) {
		boolean observer = super.isDeleteObserverRegistered();
		if (observer) {
			super.getEventObserver(DaoEventTypes.DELETE).beforeTransactionMethodCall(this, DaoEventTypes.DELETE, bean);
		}

		try {
			AssertUtils.assertNotNull(bean, "Bean to delete cannot be null.");
			doDelete(bean, observer, delaySessionFlush);
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(DaoEventTypes.DELETE).afterTransactionMethodCall(this, DaoEventTypes.DELETE, bean, e);
			}
			if (e instanceof ValidationException) {
				throw e; // keep validation exceptions unchanged to prevent them from logging
			}
			throw new DaoException("Error in " + getConfiguration().getTableName() + " delete(" + bean + ")", bean, e);
		}

		if (observer) {
			super.getEventObserver(DaoEventTypes.DELETE).afterTransactionMethodCall(this, DaoEventTypes.DELETE, bean, null);
		}
	}


	protected void doDelete(T bean, boolean observer) {
		doDelete(bean, observer, false);
	}


	@Transactional
	protected void doDelete(T bean, boolean observer, boolean delaySessionFlush) {
		if (observer) {
			super.getEventObserver(DaoEventTypes.DELETE).beforeMethodCall(this, DaoEventTypes.DELETE, bean);
		}

		try {
			AssertUtils.assertNotNull(bean, "Bean to delete cannot be null.");
			getSessionFactory().getCurrentSession().delete(bean);
			if (DaoUtils.isPostUpdateFlushEnabled() && !delaySessionFlush) {
				flushSession();
			}
		}
		catch (Throwable e) {
			if (observer) {
				super.getEventObserver(DaoEventTypes.DELETE).afterMethodCall(this, DaoEventTypes.DELETE, bean, e);
			}
			if (e instanceof ValidationException) {
				throw e; // keep validation exceptions unchanged to prevent them from logging
			}
			throw new DaoException("Error in " + getConfiguration().getTableName() + " delete(" + bean + ")", bean, e);
		}

		if (observer) {
			super.getEventObserver(DaoEventTypes.DELETE).afterMethodCall(this, DaoEventTypes.DELETE, bean, null);
		}
	}


	@Override
	@Transactional
	public void deleteList(List<T> deleteList) {
		if (CollectionUtils.isEmpty(deleteList)) {
			return;
		}
		for (T deleteBean : deleteList) {
			delete(deleteBean, true);
		}
		if (DaoUtils.isPostUpdateFlushEnabled()) {
			flushSession();
		}
	}


	@Override
	@Transactional
	public void executeNamedQuery(String queryName, Object... parameterValues) {
		try {
			Query<T> query = getSessionFactory().getCurrentSession().getNamedQuery(queryName);
			int position = 1;  // javadoc requires 0 for the first element but after upgrade to Hibernate 5.4.2 it started failing on 0: had to switch to 1
			for (Object value : parameterValues) {
				query.setParameter(position++, value);
			}
			query.executeUpdate();
		}
		catch (Throwable e) {
			throw new RuntimeException("Error executing named query: " + queryName + "(" + ArrayUtils.toString(parameterValues) + ") for " + toString(), e);
		}
	}


	/**
	 * Populates record stamp fields (create/update user/date) for the specified DTO.
	 */
	private void populateRecordStamp(T bean) {
		if (bean instanceof UpdatableOnlyEntity) {
			UpdatableOnlyEntity updatableOnlyEntity = (UpdatableOnlyEntity) bean;
			Date now = new Date();
			IdentityObject currentUser = (IdentityObject) getContextHandler().getBean(Context.USER_BEAN_NAME);
			AssertUtils.assertNotNull(currentUser, "Current user is not set");
			AssertUtils.assertNotNull(currentUser.getIdentity(), "Current user identity is not set");
			short currentUserID = ((Short) currentUser.getIdentity());
			if (bean.isNewBean() && bean instanceof UpdatableEntity) {
				UpdatableEntity updatableBean = (UpdatableEntity) bean;
				updatableBean.setCreateUserId(currentUserID);
				updatableBean.setCreateDate(now);
			}
			updatableOnlyEntity.setUpdateUserId(currentUserID);
			updatableOnlyEntity.setUpdateDate(now);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////               Getter and Setter Methods                       ///////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
