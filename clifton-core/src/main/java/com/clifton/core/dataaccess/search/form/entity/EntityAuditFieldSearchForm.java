package com.clifton.core.dataaccess.search.form.entity;


import java.util.Date;


/**
 * The <code>EntityAuditFieldSearchForm</code> interface declares getter and setter methods for the standard fields for entities which audit standard events.
 * Implementors of this interface shall implement these fields to enable search form functionality.
 *
 * @author MikeH
 */
public interface EntityAuditFieldSearchForm extends EntityUpdateFieldSearchForm {


	Date getCreateDate();


	void setCreateDate(Date createDate);


	Short getCreateUserId();


	void setCreateUserId(Short createUserId);
}
