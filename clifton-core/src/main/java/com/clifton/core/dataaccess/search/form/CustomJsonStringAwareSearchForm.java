package com.clifton.core.dataaccess.search.form;

/**
 * The <code>CustomJsonStringAwareSearchForm</code> interface needs to be implemented  by SearchForms that need to support filtering within a {@link com.clifton.core.json.custom.CustomJsonString} property
 * There are no actual methods to implement at this time, however the implementation of the interface will auto register the {@link com.clifton.core.dataaccess.search.CustomJsonStringAwareSearchFormConfigurer}
 * which will handle filtering within the json value appropriately
 *
 * @author manderson
 */
public interface CustomJsonStringAwareSearchForm extends RestrictiveSearchForm {

	// NOTHING HERE
}
