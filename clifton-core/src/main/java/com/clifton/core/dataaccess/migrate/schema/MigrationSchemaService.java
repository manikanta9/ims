package com.clifton.core.dataaccess.migrate.schema;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.migrate.schema.search.ColumnSearchForm;
import com.clifton.core.dataaccess.migrate.schema.search.TableSearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;


/**
 * The MigrationSchemaService exposes methods for view/searching Table/Column Information from the Migration Schema.
 *
 * @author manderson
 */
public interface MigrationSchemaService {

	/**
	 * Returns an instance of a json migration action for the specified object
	 * This object is used in the UI to allow for display, and editing of the JSON,
	 * and allows for the manipulated JSON to be converted to the necessary XML migration
	 * syntax used in our schema migration files.  This allows a user to create an object
	 * using the UI, and export the configuration for use in a migration.  Additionally,
	 * this can be used to migrate objects from one environment to another.
	 */
	@ResponseBody
	@RequestMapping("jsonMigrationAction")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public String getJsonMigrationAction(String entityTableName, Integer entityFkFieldId) throws Exception;


	@DoNotAddRequestMapping
	public String getJsonMigrationAction(String entityTableName, Integer entityFkFieldId, boolean xmlActionData) throws Exception;


	/**
	 * If the argument table has parent column (parentField = true), then returns the name of parent (FK) table.
	 */
	@ResponseBody
	@SecureMethod(securityResource = "Schema")
	public String getMigrationParentTableName(String tableName);


	/**
	 * Get list of all migration level table objects for admin purposes
	 */
	@SecureMethod(securityResource = "Schema")
	public List<Table> getTableList();


	@SecureMethod(securityResource = "Schema")
	public List<Table> getTableList(TableSearchForm searchForm);


	@SecureMethod(securityResource = "Schema")
	public List<Column> getColumnList(ColumnSearchForm searchForm);
}
