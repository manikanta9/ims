package com.clifton.core.dataaccess.transactions;

import com.clifton.core.cache.CacheHandler;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionException;
import org.springframework.transaction.TransactionStatus;

import java.util.Map;
import java.util.regex.Pattern;


/**
 * The <code>TransactionManagerCacheImpl</code> provides the default {@link TransactionManagerCache} implementation.
 * <p>
 * This is typically used as a manually registered Spring bean so that it can injected without requiring component scans. This is particularly useful in some contexts, such as
 * migration contexts, which tend to use a very limited set of beans.
 *
 * @author MikeH
 * @see BaseCustomAspectJAnnotationTransactionAspect
 */
public class TransactionManagerCacheImpl implements TransactionManagerCache {

	private static final PlatformTransactionManager NULL_TRANSACTION_MANAGER = new NullPlatformTransactionManager();

	private CacheHandler<Class<?>, PlatformTransactionManager> cacheHandler;

	/**
	 * The map of qualified class name regular expression patterns to transaction managers. When determining the transaction manager to use for a class, the first matching pattern
	 * key in this map (via iteration order) will be used to determine the corresponding transaction manager.
	 */
	private Map<Pattern, PlatformTransactionManager> hibernatePackageRegexToTransactionManagerName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getCacheName() {
		return getClass().getName();
	}


	@Override
	public PlatformTransactionManager getTransactionManagerForClass(Class<?> clazz) {
		PlatformTransactionManager transactionManager = getCacheHandler().get(getCacheName(), clazz);
		if (transactionManager == null) {
			// Populate cache
			transactionManager = NULL_TRANSACTION_MANAGER;
			if (getHibernatePackageRegexToTransactionManagerName() != null) {
				for (Map.Entry<Pattern, PlatformTransactionManager> patternToManagerEntry : getHibernatePackageRegexToTransactionManagerName().entrySet()) {
					if (patternToManagerEntry.getKey().matcher(clazz.getName()).matches()) {
						transactionManager = patternToManagerEntry.getValue();
						break;
					}
				}
			}
			getCacheHandler().put(getCacheName(), clazz, transactionManager);
		}
		return transactionManager == NULL_TRANSACTION_MANAGER ? null : transactionManager;
	}


	@Override
	public void clear() {
		getCacheHandler().clear(getCacheName());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The null transaction manager, indicating that no matching transaction manager was found. This is a placeholder to provide support for caches which do not support null
	 * values.
	 */
	private static class NullPlatformTransactionManager implements PlatformTransactionManager {

		@Override
		public TransactionStatus getTransaction(TransactionDefinition definition) throws TransactionException {
			throw new UnsupportedOperationException("The " + getClass().getName() + " type does not support operations.");
		}


		@Override
		public void commit(TransactionStatus status) throws TransactionException {
			throw new UnsupportedOperationException("The " + getClass().getName() + " type does not support operations.");
		}


		@Override
		public void rollback(TransactionStatus status) throws TransactionException {
			throw new UnsupportedOperationException("The " + getClass().getName() + " type does not support operations.");
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<Class<?>, PlatformTransactionManager> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<Class<?>, PlatformTransactionManager> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public Map<Pattern, PlatformTransactionManager> getHibernatePackageRegexToTransactionManagerName() {
		return this.hibernatePackageRegexToTransactionManagerName;
	}


	public void setHibernatePackageRegexToTransactionManagerName(Map<Pattern, PlatformTransactionManager> hibernatePackageRegexToTransactionManagerName) {
		this.hibernatePackageRegexToTransactionManagerName = hibernatePackageRegexToTransactionManagerName;
	}
}
