package com.clifton.core.dataaccess.migrate.execution.database;


import com.clifton.core.dataaccess.migrate.locator.MigrationModule;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import java.util.List;


/**
 * The <code>MigrationExecutionDatabaseHandler</code> defines methods for generating and executing database migrations.
 * <p>
 * Database migrations are SQL statements that evolve databases along with code. All database changes (DDL and DML)
 * should be performed via database migrations. This process will provide meta-data for DAO components, audit trail of
 * all database changes, ability to test database changes, consistent updates across all environments.
 * </p>
 *
 * @author vgomelsky
 */
public interface MigrationExecutionDatabaseHandler {

	/**
	 * Creates the "Env" Schema that will contain non-business data
	 */
	public void createEnvironmentSchema();


	/**
	 * Creates database for migrations to be run on.  If it already exists, it
	 * will drop and create a new instance of the database.
	 */
	public void createMigrationDatabase();


	/**
	 * Creates table for migrations module versions to be tracked in.
	 */
	public void createMigrationTable();


	/**
	 * Gets the migration versions that were in the database at the start of this migration and adds them to the
	 * {@link MigrationModule#existingMigrationVersionMap}.
	 *
	 * @param migrationModule
	 */
	public void getCurrentVersionsForModule(MigrationModule migrationModule);


	/**
	 * Updates versions for migration modules
	 */
	public void updateMigrationModuleVersions(List<MigrationModule> moduleList);


	/**
	 * Creates a back up of the database
	 */
	public void backupMigrationDatabase();


	/**
	 * Drop database that migrations were run on.
	 */
	public void dropMigrationDatabase();


	/**
	 * Restores database to a previous version defined by a BAK file
	 */
	public void restoreMigrationDatabase();


	public void batchUpdate(String[] sqlBatches);


	public void execute(String sql);


	public Object queryForObject(String sql, Class<?> clazz);


	public SqlRowSet queryForRowSet(String sql);


	/**
	 * Creates the environment table where the current database environment level will be stored
	 */
	public void createEnvironmentTable();


	/**
	 * Updates the environment table with the level of the current application
	 */
	public void updateEnvironmentTable(String applicationEnvironmentLevel);
}
