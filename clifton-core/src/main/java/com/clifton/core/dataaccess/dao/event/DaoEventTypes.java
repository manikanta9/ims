package com.clifton.core.dataaccess.dao.event;


/**
 * The <code>DaoEventTypes</code> enumeration defines combinations of read/insert/update/delete events
 * that DAO objects raise.
 *
 * @author vgomelsky
 */
public enum DaoEventTypes {

	READ(true, false, false, false),
	INSERT(false, true, false, false),
	UPDATE(false, false, true, false),
	DELETE(false, false, false, true),
	/**
	 * INSERT or UPDATE
	 */
	SAVE(false, true, true, false),
	/**
	 * INSERT, UPDATE, or DELETE
	 */
	MODIFY(false, true, true, true),
	ALL(true, true, true, true),
	NONE(false, false, false, false),
	UPDATE_OR_DELETE(false, false, true, true),
	INSERT_OR_DELETE(false, true, false, true);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	DaoEventTypes(boolean read, boolean insert, boolean update, boolean delete) {
		this.read = read;
		this.insert = insert;
		this.update = update;
		this.delete = delete;
	}


	private final boolean read;
	private final boolean insert;
	private final boolean update;
	private final boolean delete;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isRead() {
		return this.read;
	}


	public boolean isInsert() {
		return this.insert;
	}


	public boolean isUpdate() {
		return this.update;
	}


	public boolean isDelete() {
		return this.delete;
	}


	/**
	 * Returns true if this type has TRUE value for each TRUE attribute of the specified type.
	 */
	public boolean isSuperSetOf(DaoEventTypes anotherType) {
		if (anotherType.isRead() && !isRead()) {
			return false;
		}
		if (anotherType.isInsert() && !isInsert()) {
			return false;
		}
		if (anotherType.isUpdate() && !isUpdate()) {
			return false;
		}
		if (anotherType.isDelete() && !isDelete()) {
			return false;
		}
		return true;
	}
}
