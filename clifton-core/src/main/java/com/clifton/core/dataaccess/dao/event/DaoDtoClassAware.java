package com.clifton.core.dataaccess.dao.event;

import com.clifton.core.beans.IdentityObject;


/**
 * @author manderson
 */
public interface DaoDtoClassAware<T extends IdentityObject> {

	/**
	 * Used for cases where the implementations are created at runtime, therefore we need to set the DtoClass as a property
	 * rather than attempt to get them through the generic type arguments.
	 */
	public Class<T> getDtoClass();
}
