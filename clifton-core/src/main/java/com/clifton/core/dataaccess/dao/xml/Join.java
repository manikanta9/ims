package com.clifton.core.dataaccess.dao.xml;


import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Link;


/**
 * The <code>Join</code> class represents a JOIN between 2 tables.
 *
 * @author vgomelsky
 */
public class Join {

	private final String alias;
	private Link link;
	private Column column;


	/**
	 * Constructs a new MANY TO MANY join using the specified link.
	 *
	 * @param link
	 * @param alias
	 */
	public Join(Link link, String alias) {
		this.link = link;
		this.alias = alias;
	}


	/**
	 * Constructs a new FOREIGN KEY join using the specified column.
	 *
	 * @param column
	 * @param alias
	 */
	public Join(Column column, String alias) {
		this.column = column;
		this.alias = alias;
	}


	/**
	 * @return the alias
	 */
	public String getAlias() {
		return this.alias;
	}


	/**
	 * @return the link
	 */
	public Link getLink() {
		return this.link;
	}


	/**
	 * @return the column
	 */
	public Column getColumn() {
		return this.column;
	}
}
