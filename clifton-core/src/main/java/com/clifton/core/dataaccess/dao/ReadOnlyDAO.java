package com.clifton.core.dataaccess.dao;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>ReadOnlyDAO</code> interface defines methods exposed by all Data Access Objects.
 *
 * @param <T>
 * @author vgomelsky
 */
public interface ReadOnlyDAO<T extends IdentityObject> {

	/**
	 * Returns DAOConfiguration object for this entity.
	 */
	public DAOConfiguration<T> getConfiguration();


	/**
	 * Creates and returns a new instance of this DAO's DTO object.
	 * If required properties are passed, returns the class itself or first sub-class that has all of the properties.
	 *
	 * @param requiredProperties optionally can specify one or more bean properties that the bean must have.
	 *                           This is applicable to cases with multiple DTO sub-classes with the abstract
	 *                           base DTO class that cannot be instantiated.
	 */
	public T newBean(String... requiredProperties);


	/**
	 * Returns the DTO found by looking up using the natural key of the object
	 * <p>
	 * Must be called with populated children - if a child item is referenced by a natural key
	 * and it is not fully populated (i.e. the 'id' attribute is null) then the lookup will fail.
	 */
	public T findByNaturalKey(T searchObject);


	/**
	 * Returns DTO object with the specified primary key.
	 */
	public T findByPrimaryKey(Serializable primaryKey);


	/**
	 * Returns DTO object with the specified field matching the specified value.
	 * Throws an exception if more than one row is returned.
	 */
	public T findOneByField(String beanFieldName, Object value);


	/**
	 * Returns DTO object with the specified fields matching the specified values (AND clause).
	 * Throws an exception if more than one row is returned.
	 */
	public T findOneByFields(String[] beanFieldNames, Object[] values);


	/**
	 * Returns a List of all DTO's for this entity.
	 */
	public List<T> findAll();


	public List<T> findByPrimaryKeys(Serializable[] ids);


	/**
	 * Returns a List of DTO's that have specified field match the specified value.
	 */
	public List<T> findByField(String beanFieldName, Object value);


	/**
	 * Returns a List of DTO's that have specified field match one of the specified values: SQL IN clause.
	 */
	public List<T> findByField(String beanFieldName, Object[] values);


	/**
	 * Returns a List of DTO's that have specified fields match the specified values (AND clause).
	 */
	public List<T> findByFields(String[] beanFieldNames, Object[] values);


	public Serializable convertToPrimaryKeyDataType(Serializable key);
}
