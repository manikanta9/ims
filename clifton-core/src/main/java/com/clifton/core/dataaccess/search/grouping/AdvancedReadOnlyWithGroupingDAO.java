package com.clifton.core.dataaccess.search.grouping;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;

import java.util.List;


public interface AdvancedReadOnlyWithGroupingDAO<T extends IdentityObject> {


	/**
	 * Returns DAOConfiguration object for this entity.
	 */
	public DAOConfiguration<T> getConfiguration();


	/**
	 * Returns a List of GroupedResults that match the specified search criteria
	 * Grouped by specified properties with counts.
	 * If grouped by multiple properties, results are returned in a hierarchy format for tree navigation
	 */
	public List<GroupedHierarchicalResult> findBySearchCriteriaGrouped(GroupingSearchConfigurer searchConfigurer);


	/**
	 * Returns a List of AggregateResults that match the specified search criteria
	 * Grouped by specified properties with counts.
	 * If grouped by multiple properties, results are returned in a hierarchy format for tree navigation
	 */
	public List<GroupedEntityResult<T>> findBySearchCriteriaAggregate(GroupingSearchConfigurer searchConfigurer);
}
