package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.SearchField;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * The <code>FormulaOrderBy</code> is use by {@link SearchField#formula()} to order based on a custom sql query.
 * <p/>
 * The sqlFormula provided is expected to be {@link String#format(String, Object...)} compatible,
 * with the expected number of arguments equal to propertyNames length. Compatibility is is taken care of at runtime
 * by {@link com.clifton.core.dataaccess.search.restrictions.AbstractRestrictionDefinition#setSqlFormula(String)}
 *
 * @author dillonm
 */
public class FormulaOrderBy extends Order {

	private final String sqlFormula;
	private final String[] propertyNames;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private FormulaOrderBy(String sqlFormula, boolean ascending, String[] propertyNames) {
		super(null, ascending);
		this.sqlFormula = sqlFormula;
		this.propertyNames = propertyNames;
	}


	public static FormulaOrderBy create(OrderByDirections direction, String sqlFormula, String[] propertyNames) {
		return new FormulaOrderBy(sqlFormula, direction == OrderByDirections.ASC, propertyNames);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		String[] columns = new String[this.propertyNames.length];
		for (int i = 0; i < this.propertyNames.length; ++i) {
			columns[i] = (criteriaQuery.getColumn(criteria, this.propertyNames[i]));
		}
		return String.format(this.sqlFormula, (Object[]) columns) + (super.isAscending() ? " asc" : " desc");
	}
}
