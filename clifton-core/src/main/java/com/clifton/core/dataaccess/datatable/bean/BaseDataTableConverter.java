package com.clifton.core.dataaccess.datatable.bean;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * @author stevenf
 * @author michaelm
 */
public abstract class BaseDataTableConverter<F> implements DataTableConverter<F> {

	protected DataTableConverterConfiguration converterConfiguration;

	protected DataColumn[] dataColumnArray;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void initialize(DataTableConverterConfiguration configuration) {
		ValidationUtils.assertNotNull(configuration, "Data Table Converter Configuration is required for all Data Table Converters.");
		this.converterConfiguration = configuration;
		validateConverterConfiguration();
		setDataColumnArray(getOrBuildDataColumnArray());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract void validateConverterConfiguration();


	public abstract DataColumn[] getOrBuildDataColumnArray();

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DataTableConverterConfiguration getConverterConfiguration() {
		return this.converterConfiguration;
	}


	public DataColumn[] getDataColumnArray() {
		return this.dataColumnArray;
	}


	public void setDataColumnArray(DataColumn[] dataColumnArray) {
		this.dataColumnArray = dataColumnArray;
	}
}


