package com.clifton.core.dataaccess.dao.template;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.search.SearchConfigurer;


/**
 *  The <code>TemplateUpdatableDAO</code> interface adds extends the {@link TemplateReadOnlyDAO} and adds a delete method by query string
 *  for template based DAOs.
 *
 * @author stevenf
 */
public interface TemplateUpdatableDAO<T extends IdentityObject, S extends SearchConfigurer<C>, C> extends TemplateReadOnlyDAO<T, S, C>, UpdatableDAO<T> {

	public void deleteByQuery(String query);
}
