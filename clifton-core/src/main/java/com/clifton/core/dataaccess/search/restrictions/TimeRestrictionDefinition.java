package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.date.Time;


/**
 * The {@link TimeRestrictionDefinition} class implements {@link SearchRestrictionDefinition} for fields of type {@link Time}.
 *
 * @author MikeH
 */
public class TimeRestrictionDefinition extends AbstractRestrictionDefinition {

	public TimeRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public TimeRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, false);
	}


	public TimeRestrictionDefinition(String fieldName, String searchFieldName, boolean required) {
		this(fieldName, searchFieldName, null, required);
	}


	public TimeRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, orderByFieldName, false);
	}


	public TimeRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN, ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS, ComparisonConditions.IS_NULL}, null);
	}


	public TimeRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS, ComparisonConditions.GREATER_THAN, ComparisonConditions.LESS_THAN}, null);
	}


	public TimeRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required, ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomType) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, searchFieldCustomType);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	@Override
	public Object getValue(Object rawValue) {
		return rawValue instanceof Time
				? rawValue
				: Time.parse((String) rawValue);
	}
}
