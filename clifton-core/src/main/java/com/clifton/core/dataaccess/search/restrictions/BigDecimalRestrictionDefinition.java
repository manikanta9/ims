package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.AssertUtils;

import java.math.BigDecimal;


/**
 * The <code>BigDecimalRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link BigDecimal}.
 * Supports EQUALS, GREATER_THAN, LESS_THAN comparison conditions.
 *
 * @author vgomelsky
 */
public class BigDecimalRestrictionDefinition extends AbstractRestrictionDefinition {

	public BigDecimalRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public BigDecimalRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, false);
	}


	public BigDecimalRestrictionDefinition(String fieldName, String searchFieldName, boolean required) {
		this(fieldName, searchFieldName, null, required);
	}


	public BigDecimalRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, orderByFieldName, false);
	}


	public BigDecimalRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, boolean required) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.NOT_EQUALS,
				ComparisonConditions.GREATER_THAN, ComparisonConditions.GREATER_THAN_OR_EQUALS, ComparisonConditions.LESS_THAN, ComparisonConditions.LESS_THAN_OR_EQUALS}, null);
	}


	public BigDecimalRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                       ComparisonConditions[] comparisonConditions, SearchFieldCustomTypes searchFieldCustomType) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions, searchFieldCustomType);
		AssertUtils.assertNotEmpty(comparisonConditions, "At least one comparison condition is required.");
	}


	@Override
	public Object getValue(Object rawValue) {
		if (rawValue == null) {
			return null;
		}
		if (rawValue instanceof BigDecimal) {
			return rawValue;
		}
		return new BigDecimal((String) rawValue);
	}
}
