package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.apache.poi.hssf.usermodel.HSSFClientAnchor;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.openxml4j.opc.PackageAccess;
import org.apache.poi.openxml4j.opc.PackagePart;
import org.apache.poi.openxml4j.opc.PackagingURIHelper;
import org.apache.poi.openxml4j.opc.internal.PackagePropertiesPart;
import org.apache.poi.poifs.crypt.EncryptionInfo;
import org.apache.poi.poifs.crypt.EncryptionMode;
import org.apache.poi.poifs.crypt.Encryptor;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Comment;
import org.apache.poi.ss.usermodel.Drawing;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFClientAnchor;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.security.GeneralSecurityException;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ExcelFileUtils</code> ...
 *
 * @author manderson
 */
public class ExcelFileUtils {

	// Allow Creating Workbooks from Files Up to 10 MB
	private static final long MAX_FILE_SIZE_BYTES = 10_485_760;


	/**
	 * Returns a new blank Excel Workbook with a new Sheet & header row created automatically.
	 */
	public static Workbook createWorkbook() {
		return createWorkbook(null);
	}


	public static Workbook createWorkbook(String fileExtension) {
		// create a new workbook
		Workbook workbook = "xls".equals(fileExtension) ? new HSSFWorkbook() : new XSSFWorkbook();
		//create a new worksheet
		Sheet sheet = workbook.createSheet();
		// Create the Header Row
		sheet.createRow(0);
		return workbook;
	}


	/**
	 * Creates an Excel Workbook & sheet populated with the data found in the
	 * InputStream for the given filename.
	 */
	public static Workbook createWorkbook(Object file) {
		String fileName = null;
		InputStream fis = null;
		long fileSize;

		try {
			if (file instanceof MultipartFile) {
				MultipartFile mFile = (MultipartFile) file;
				fileSize = mFile.getSize();
				fileName = mFile.getOriginalFilename();
				validateFileSize(fileName, fileSize);
				fis = mFile.getInputStream();
			}
			else if (file instanceof File) {
				File fFile = (File) file;
				fileName = fFile.getName();
				fileSize = fFile.length();
				validateFileSize(fileName, fileSize);
				fis = new FileInputStream(fFile);
			}
			else if (file instanceof FileContainer) {
				FileContainer fileContainer = (FileContainer) file;
				fileName = fileContainer.getName();
				fileSize = fileContainer.length();
				validateFileSize(fileName, fileSize);
				fis = fileContainer.getInputStream();
			}
			else {
				throw new IllegalArgumentException("Object File [" + file.getClass() + "] from must be an instance of a Multi-part File or a File.");
			}

			return createWorkbook(fis, fileName);
		}
		catch (Throwable e) {
			throw new ValidationException("Error reading file [" + fileName + "] with error: " + e.getMessage(), e);
		}
		finally {
			FileUtils.close(fis);
		}
	}


	/**
	 * XLSX files tend to get extremely large and there can be issues if there are a lot of blank lines
	 * Causes out of memory issues.  Best options would be to:
	 * 1. Save your file as an xls file and try again
	 * 2. Open the file and delete all empty rows (so XLSX doesn't even try to read them)
	 * 3. Split the file into multiple files
	 */
	private static void validateFileSize(String fileName, long fileSize) {
		if (fileSize > MAX_FILE_SIZE_BYTES) {
			throw new ValidationException("File '" + fileName + "' is too large to convert to a workbook for processing (Max size allowed is 10 MB).  Please try the following:<br>1. If you are using an xlsx file, save your file as an xls file and try again.<br>2. Open the file and manually delete any empty rows at the end of the file<br>3. Split your file into smaller files.");
		}
	}


	/**
	 * Creates an Excel Workbook & sheet populated with the data found in the
	 * InputStream for the given filename.
	 */
	private static Workbook createWorkbook(InputStream fis, String fileName) {
		try {
			Workbook workbook = WorkbookFactory.create(fis);
			Sheet sheet = workbook.getSheetAt(0);
			if (sheet == null) {
				throw new ValidationException("Unable to retrieve Excel Sheet from the given file.");
			}
			return workbook;
		}
		catch (Throwable e) {
			throw new ValidationException("Error reading file [" + fileName + "].  Make sure your file is a valid Excel file (.xls or .xlsx): " + e.getMessage(), e);
		}
	}


	/**
	 * Converts an excel file with multiple sheets to a String,DataTable map.
	 * Sheets that are in the tabKeyMap only are processed
	 */
	public static Map<String, DataTable> convertFileToDataTableMap(MultipartFile file, Map<String, String> tabKeyMap) {
		return convertWorkbookToDataTableMap(createWorkbook(file), tabKeyMap);
	}


	/**
	 * Converts an excel file with multiple sheets to a String,DataTable map.
	 * Sheets that are in the tabKeyMap only are processed
	 */
	public static Map<String, DataTable> convertWorkbookToDataTableMap(Workbook workbook, Map<String, String> tabKeyMap) {
		Map<String, DataTable> expectedResults = new HashMap<>();
		ExcelFileToDataTableConverter excelConverter = new ExcelFileToDataTableConverter();

		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			Sheet sheet = workbook.getSheetAt(i);
			if (sheet != null) {
				String sheetName = sheet.getSheetName();
				if (tabKeyMap.containsKey(sheetName)) {
					excelConverter.setSheetIndex(i);
					expectedResults.put(tabKeyMap.get(sheetName), excelConverter.convert(workbook));
				}
			}
		}
		return expectedResults;
	}


	/**
	 * Returns the {@link Types} data type in value for the given Excel cell type
	 * & column name.
	 * <p>
	 * Column name is used to distinguish Date cells that are actually dates, and not numeric or string
	 * cells depending on how the person entered the data.
	 */
	public static int getSqlDataType(String columnName, CellType cellType) {
		if (columnName != null && (columnName.trim().toUpperCase().endsWith("DATE"))) {
			return Types.DATE;
		}

		if (columnName != null && columnName.trim().toUpperCase().endsWith("TIME")) {
			return Types.TIME;
		}

		if (CellType.BOOLEAN == cellType) {
			return Types.BOOLEAN;
		}

		if (CellType.NUMERIC == cellType) {
			return Types.NUMERIC;
		}
		return Types.NVARCHAR;
	}


	/**
	 * Returns the correctly typed Object value for the given Sheet,
	 * row index, column index and {@link Types}
	 */
	public static Object getCellValue(Sheet sheet, int rowIndex, int columnIndex, int sqlDataType) {
		return getCellValue(getRow(sheet, rowIndex, false), columnIndex, sqlDataType);
	}


	/**
	 * Returns the correctly typed Object value for the given Row
	 * column index and {@link Types}
	 */
	public static Object getCellValue(Row row, int columnIndex, int sqlDataType) {
		Object result;
		try {
			Cell cell = getCell(row, columnIndex, false);
			if (cell == null || CellType.BLANK == cell.getCellTypeEnum()) {
				result = null;
			}
			else if (CellType.BOOLEAN == cell.getCellTypeEnum()) {
				result = cell.getBooleanCellValue();
			}
			else if (CellType.NUMERIC == cell.getCellTypeEnum()) {
				if (Types.DATE == sqlDataType || Types.TIME == sqlDataType) {
					result = cell.getDateCellValue();
				}
				else {
					double val = cell.getNumericCellValue();
					result = BigDecimal.valueOf(val).stripTrailingZeros();
					if (Types.NVARCHAR == sqlDataType) {
						result = ((BigDecimal) result).toPlainString();
					}
				}
			}
			else if (CellType.FORMULA == cell.getCellTypeEnum()) {
				result = getCellFormulaValue(cell, sqlDataType);
			}
			else {
				result = cell.getStringCellValue();
			}

			if (result instanceof String) {
				result = StringUtils.trim((String) result);
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error retrieving cell value for column " + columnIndex + " of row " + row.getRowNum() + " of data type " + sqlDataType + ": " + e.getMessage(), e);
		}
		return result;
	}


	private static Object getCellFormulaValue(Cell cell, int sqlDataType) {
		try {
			// If we are looking for a date - pull it as a date value
			if (Types.DATE == sqlDataType || Types.TIME == sqlDataType) {
				return cell.getDateCellValue();
			}
			// Otherwise, would be a number
			else {
				return Double.toString(cell.getNumericCellValue());
			}
		}
		catch (Throwable e) {
			// Don't throw the exception - just try to get the String value if it's not numeric
			return cell.getStringCellValue();
		}
	}


	/**
	 * Returns the Row for the given sheet & row index.  If createIfMissing is true
	 * then will create a row at the given index if it is null.
	 */
	public static Row getRow(Sheet sheet, int rowIndex, boolean createIfMissing) {
		Row thisRow = sheet.getRow(rowIndex);
		if (createIfMissing && thisRow == null) {
			return sheet.createRow(rowIndex);
		}
		return thisRow;
	}


	/**
	 * Returns the Cell for the given sheet, row index, & column index.  If createIfMissing is true
	 * then will create a row at the given row index if it is null, and the cell at the given column
	 * index if they are null.
	 */
	public static Cell getCell(Sheet sheet, int rowIndex, int columnIndex, boolean createIfMissing) {
		Row row = getRow(sheet, rowIndex, createIfMissing);
		return getCell(row, columnIndex, createIfMissing);
	}


	/**
	 * Returns the Cell for the given row & column index.  If createIfMissing is true
	 * then will create the cell at the given column index if it is null.
	 */
	public static Cell getCell(Row row, int columnIndex, boolean createIfMissing) {
		if (row == null) {
			return null;
		}
		Cell cell = row.getCell(columnIndex);
		if (cell == null && createIfMissing) {
			return row.createCell(columnIndex);
		}
		return cell;
	}


	public static void createCellComment(Sheet sheet, Cell cell, String commentText) {
		Comment comment;
		if (sheet instanceof XSSFSheet) {
			Drawing<?> drawing = sheet.createDrawingPatriarch();
			XSSFClientAnchor anchor = new XSSFClientAnchor(0, 0, 0, 0, (short) 4, 2, (short) 6, 5);
			comment = drawing.createCellComment(anchor);
			comment.setString(new XSSFRichTextString(commentText));
		}
		else {
			HSSFPatriarch patriarch = (HSSFPatriarch) sheet.createDrawingPatriarch();
			HSSFClientAnchor anchor = new HSSFClientAnchor(0, 0, 0, 0, (short) 4, 2, (short) 6, 5);
			comment = patriarch.createComment(anchor);
			comment.setString(new HSSFRichTextString(commentText));
		}
		cell.setCellComment(comment);
	}


	/**
	 * Returns a temporary file Workbook from a workbook object.
	 */
	public static File getFileFromWorkbook(Workbook workbook, String fileName) {
		AssertUtils.assertNotNull(workbook, "Cannot create a File from a null workbook");
		AssertUtils.assertFalse(StringUtils.isEmpty(fileName), "Cannot create a file with a blank name");

		// write workbook to file
		OutputStream stream = null;
		File file = null;
		try {
			file = File.createTempFile(fileName, (workbook instanceof HSSFWorkbook) ? ".xls" : ".xlsx");
			// .xlsx files are very inefficient - always generate samples as .xls files
			//file = File.createTempFile(fileName, ".xls");
			file.deleteOnExit();
			stream = new FileOutputStream(file);
			workbook.write(stream);
			return file;
		}
		catch (Throwable e) {
			if (file != null) {
				// If there's an error, but the temp file was created, delete it
				file.delete();
			}
			throw new RuntimeException("Error generating workbook file: " + fileName, e);
		}
		finally {
			FileUtils.close(stream);
		}
	}


	/**
	 * Password-protects an Excel (.XLSX) file.
	 *
	 * @param sourcePath      the full path to the source Excel file
	 * @param destinationPath the full path to the destination of the encrypted Excel file
	 * @param password        the password to encrypt the file with
	 */
	public static void encryptExcelFile(String sourcePath, String destinationPath, String password) {
		if (!sourcePath.endsWith(".xlsx") || !destinationPath.endsWith("xlsx")) {
			throw new RuntimeException("Only .xlsx Excel files are currently supported for encryption.");
		}

		BufferedOutputStream fos = null;
		POIFSFileSystem fs;
		try {
			fs = new POIFSFileSystem();
			encryptExcelFile(sourcePath, password, fs);

			File destination = new File(destinationPath);
			if (!destination.getParentFile().exists()) {
				FileUtils.createDirectoryForFile(destination);
			}

			fos = new BufferedOutputStream(new FileOutputStream(destinationPath));
			fs.writeFilesystem(fos);
		}
		catch (Throwable e) {
			throw new RuntimeException("Error encrypting Excel file: " + sourcePath, e);
		}
		finally {
			if (fos != null) {
				try {
					fos.close();
				}
				catch (Throwable e) {
					// do nothing, it may already be closed.
				}
			}
		}
	}


	/**
	 * Writes the encrypted excel file to a {@link POIFSFileSystem} object.
	 */
	private static void encryptExcelFile(String sourcePath, String password, POIFSFileSystem fs) throws InvalidFormatException, IOException, GeneralSecurityException {
		EncryptionInfo info = new EncryptionInfo(EncryptionMode.agile);

		Encryptor enc = info.getEncryptor();
		enc.confirmPassword(password);

		OPCPackage opc = null;
		try {
			opc = OPCPackage.open(new File(sourcePath), PackageAccess.READ_WRITE);
			// if there is a core.xml part without any properties set, then remove it so the api doesn't choke
			PackagePart part = opc.getPart(PackagingURIHelper.createPartName("/docProps/core.xml"));
			if (part instanceof PackagePropertiesPart && !((PackagePropertiesPart) part).getCreatorProperty().hasValue()) {
				opc.removePart(PackagingURIHelper.createPartName("/docProps/core.xml"));
			}
			OutputStream os = enc.getDataStream(fs);
			opc.save(os);
		}
		finally {
			if (opc != null) {
				opc.revert();
			}
		}
	}
}
