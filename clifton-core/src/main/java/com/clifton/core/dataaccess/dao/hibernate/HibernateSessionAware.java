package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.DaoSessionAware;
import org.hibernate.SessionFactory;


public interface HibernateSessionAware<T extends IdentityObject> extends DaoSessionAware<T> {

	public SessionFactory getSessionFactory();
}
