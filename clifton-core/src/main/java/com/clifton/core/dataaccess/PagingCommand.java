package com.clifton.core.dataaccess;


/**
 * The <code>PagingCommand</code> interface marks objects that request paging (partial list retrieval).
 *
 * @author vgomelsky
 */
public interface PagingCommand {

	public static final int MAX_LIMIT = Integer.MAX_VALUE;

	public static final int DEFAULT_START = 0;
	public static final int DEFAULT_LIMIT = Integer.MAX_VALUE;


	/**
	 * Returns the index of the first element.  Starts with and defaults to 0.
	 */
	public int getStart();


	/**
	 * Returns the number of elements allowed (same as page size).  Defaults to Integer.MAX_VALUE meaning return everything.
	 */
	public int getLimit();


	/**
	 * Returns the maximum number of elements allowed (same as page size).  Returns null by default - default is set in dao configuration globally
	 * and can be override by setting the MaxQuerySizeLimit parameter on individual tables.
	 */
	public Integer getMaxLimit();
}
