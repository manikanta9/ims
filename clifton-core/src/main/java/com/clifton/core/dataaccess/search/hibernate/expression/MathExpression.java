package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;


/**
 * The <code>MathExpression</code> is used for applying a single math function on 2 or more fields.  i.e. (Field1 * Field2)
 * <p/>
 * For example: On the InvestmentAccountCalculationSnapshotDetail table we have calculated value, the FX rate is on the Snapshot table
 * So, we need to search/sort on the (CalculatedValue * snapshot.FXRateToBase) for the CalculatedValueBase on the details
 * i.e. searchField = "calculatedValue,calculationSnapshot.fxRateToBase", searchFieldCustomType = SearchFieldCustomTypes.MATH_MULTIPLY)
 * Which resolves to (CalculatedValue * InvestmentAccountCalculationSnapshot.FXRateToBase) = ? (or >, <, etc. depending on search comparison used)
 *
 * @author manderson
 */
public class MathExpression implements Criterion {

	private final String mathFunction;

	private final String comparisonExpression;

	private final String[] propertyNames;

	private final Object value;

	/**
	 * If true, will do ISNULL(Value, 0) for each field to prevent math with NULL that returns NULL
	 */
	private final boolean supportNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public MathExpression(String mathFunction, boolean supportNull, String comparisonExpression, String[] propertyNames, Object value) {
		this.mathFunction = mathFunction;
		this.supportNull = supportNull;
		this.comparisonExpression = comparisonExpression;
		this.propertyNames = propertyNames;
		this.value = value;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragment = new StringBuilder("(");
		for (int i = 0; i < this.propertyNames.length; i++) {
			if (this.supportNull) {
				fragment.append(SQLIfNullFunction.render(criteria, criteriaQuery, this.propertyNames[i], "0"));
			}
			else {
				fragment.append(criteriaQuery.getColumn(criteria, this.propertyNames[i]));
			}
			if (i < this.propertyNames.length - 1) {
				fragment.append(this.mathFunction);
			}
		}
		fragment.append(") ");
		fragment.append(this.comparisonExpression);
		// For cases like: LIKE '%' + ? + '%' we don't want to add a second ?, but we need to for cases like =, >, <, etc.
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragment.append(" ?");
		}
		return fragment.toString();
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return new TypedValue[]{criteriaQuery.getTypedValue(criteria, this.propertyNames[0], this.value)};
	}
}
