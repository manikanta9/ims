package com.clifton.core.dataaccess.datatable;


import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.util.converter.Converter;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


/**
 * The <code>DataColumn</code> interface defines methods for working with data columns.
 *
 * @author vgomelsky
 */
@JsonDeserialize(as = DataColumnImpl.class)
public interface DataColumn {

	public String getColumnName();


	public void setColumnName(String columnName);


	public int getDataType();


	public String getDescription();


	public void setDescription(String description);


	public String getStyle();


	public void setStyle(String style);


	public boolean isHidden();


	public void setHidden(boolean hidden);


	/**
	 * Sets the explicit list of valid options for this column. If this is not set,
	 * then there are effectively no restrictions (other than the data type, etc.)
	 *
	 * @param validOptions
	 */
	public void setValidOptions(String[] validOptions);


	/**
	 * @return the list of valid options for this column. If null, then there are no
	 * explicit restrictions.
	 */
	public String[] getValidOptions();


	public String getDataTypeName();


	public Converter<Object, Object> getColumnValueConverter();


	public void setColumnValueConverter(Converter<Object, Object> columnValueConverter);
}
