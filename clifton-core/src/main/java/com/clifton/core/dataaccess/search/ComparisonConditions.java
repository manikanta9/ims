package com.clifton.core.dataaccess.search;


import com.clifton.core.util.validation.FieldValidationException;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;

import java.util.function.BiFunction;


/**
 * The <code>ComparisonConditions</code> enumeration defines available search comparison conditions.
 *
 * @author vgomelsky
 */
public enum ComparisonConditions {

	EQUALS("=", false), //
	NOT_EQUALS("<>", false), //
	IN("IN", false), //
	IN_OR_IS_NULL("IN", true), //
	NOT_IN("NOT IN", false), //
	LESS_THAN("<", false), //
	LESS_THAN_OR_EQUALS("<=", false), //
	GREATER_THAN(">", false), //
	GREATER_THAN_OR_EQUALS(">=", false), //
	LIKE("LIKE '%' + ? + '%'", false), //
	NOT_LIKE("NOT LIKE '%' + ? + '%'", false), //
	BEGINS_WITH("LIKE ? + '%'", false), //
	NOT_BEGINS_WITH("NOT LIKE ? + '%'", false), //
	ENDS_WITH("LIKE '%' + ?", false), //
	NOT_ENDS_WITH("NOT LIKE '%' + ?", false), //
	IS_NULL("IS NULL", false), //
	IS_NOT_NULL("IS NOT NULL", false), //
	EQUALS_OR_IS_NULL("=", true), //
	NOT_EQUALS_OR_IS_NULL("<>", true), //
	LESS_THAN_OR_IS_NULL("<", true), //
	LESS_THAN_OR_EQUALS_OR_IS_NULL("<=", true), //
	GREATER_THAN_OR_IS_NULL(">", true), //
	GREATER_THAN_OR_EQUALS_OR_IS_NULL(">=", true), //
	/**
	 * Uses EQUALS inside EXISTS clause. {@link ComparisonConditions#EXISTS_LIKE} uses LIKE inside EXISTS clause.
	 * <p>
	 * Initial implementation will only work with many-to-many relationships, as the code is specifically looking for a "link" in the table definition such as:
	 * <pre><code>
	 * &lt;link name="childRuleList" real="false" referencingClass="com.clifton.compliance.rule.ComplianceRule" referencingColumn="childComplianceRuleID"
	 *      manyToManyTable="ComplianceRuleRollup" manyToManyColumn="parentComplianceRuleID"/>
	 * </code></pre>
	 * The searchField should begin with the association. For example, the below search field is from <code>ComplianceRuleSearchForm</code>.
	 * It will look for <code>ComplianceRules</code> that have rows in <code>ComplianceRuleRollup</code>
	 * with parent id equal to the specified parentComplianceRuleId.
	 * <pre><code>
	 * {@literal@}SearchField(searchField = "parentRuleList.id", comparisonConditions = ComparisonConditions.EXISTS)
	 * private Integer parentComplianceRuleId;
	 * </code></pre>
	 * The searchFieldPath should contain the path up until the association. For example, the search field below is from
	 * <code>TradeGroupSearchForm</code>. It will create an inner join between <code>TradeGroup</code> and
	 * <code>SecurityUser</code> on user id. Then it will add an exist clause on the <code>SecurityUserGroup</code> association,
	 * filtering for the <code>TradeGroups</code> with users who belong to the <code>SecurityGroup</code> with a <code>securityGroupId</code> equal to the specified
	 * <code>securityUserGroupId</code>.
	 * <pre><code>
	 * {@literal@}SearchField(searchField = "groupList.id", searchFieldPath = "traderUser", comparisonConditions = {ComparisonConditions.EXISTS}}
	 * private Short securityUserGroupId;
	 * </code></pre>
	 */
	EXISTS("EXISTS", false, Constants.SUBQUERY_EQUALS_FN),
	/**
	 * Uses LIKE inside EXISTS clause. Use {@link ComparisonConditions#EXISTS} to use EQUALS inside EXISTS clause.
	 */
	EXISTS_LIKE("EXISTS", false, Constants.SUBQUERY_LIKE_FN),
	/**
	 * Uses EQUALS inside NOT EXISTS clause. Use {@link ComparisonConditions#NOT_EXISTS_LIKE} to use LIKE inside NOT EXISTS clause.
	 */
	NOT_EXISTS("NOT EXISTS", false, Constants.SUBQUERY_EQUALS_FN),
	/**
	 * Uses LIKE inside NOT EXISTS clause. Use {@link ComparisonConditions#NOT_EXISTS} to use EQUALS inside NOT EXISTS clause.
	 */
	NOT_EXISTS_LIKE("NOT EXISTS", false, Constants.SUBQUERY_LIKE_FN);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private final String comparisonExpression;
	private final boolean orNull;
	private final boolean subQuery;
	private final BiFunction<String, SearchRestriction, Criterion> subQueryCriterion;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ComparisonConditions(String comparisonExpression, boolean orNull, BiFunction<String, SearchRestriction, Criterion> subQueryCriterion) {
		this.comparisonExpression = comparisonExpression;
		this.orNull = orNull;
		this.subQuery = subQueryCriterion != null;
		this.subQueryCriterion = subQueryCriterion;
	}


	ComparisonConditions(String comparisonExpression, boolean orNull) {
		this(comparisonExpression, orNull, null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Private static class for constants
	private static final class Constants {

		private static final BiFunction<String, SearchRestriction, Criterion> SUBQUERY_EQUALS_FN = (path, restriction) ->
				restriction.getValue().getClass().isArray()
						? Restrictions.in(path, (Object[]) restriction.getValue())
						: Restrictions.eq(path, restriction.getValue());
		private static final BiFunction<String, SearchRestriction, Criterion> SUBQUERY_LIKE_FN = (path, restriction) -> {
			if (restriction.getValue().getClass().isArray()) {
				throw new FieldValidationException("Invalid search restriction [" + restriction + "] ComparisonConditions.EXISTS_LIKE and ComparisonConditions.NOT_EXISTS_LIKE do not support searching for an array of values.");
			}
			return Restrictions.like(path, "%" + restriction.getValue() + "%");
		};
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getComparisonExpression() {
		return this.comparisonExpression;
	}


	public boolean isOrNull() {
		return this.orNull;
	}


	public boolean isSubQuery() {
		return this.subQuery;
	}


	public BiFunction<String, SearchRestriction, Criterion> getSubQueryCriterion() {
		return this.subQueryCriterion;
	}
}
