package com.clifton.core.dataaccess.migrate.locator;


import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedMap;
import java.util.TreeMap;


public class MigrationModule {

	private String moduleName;
	private String dataSourceName = "dataSource";
	private List<String> migrationFileList;
	private List<Resource> migrationResourceList;
	/**
	 * Sorted map of the migration versions that existed in the DB when this migration was started, keyed on {@link MigrationModuleVersion#migrationPath}. Since this is a tree map
	 * keyed on migrationPath, the migrations are guaranteed to be sorted in ascending order.
	 */
	private SortedMap<String, MigrationModuleVersion> existingMigrationVersionMap;
	/**
	 * List of the migration versions that need to be executed for this module.
	 */
	private List<MigrationModuleVersion> migrationVersionList;
	/**
	 * This is the version that we will run migrations up to for this module.
	 * A version of 0 means run everything
	 */
	private int targetedVersion = 0;
	/**
	 * Used when loading existing migration versions from the DB to keep track of the lowest migration version number from the database that has not been fully migrated.
	 */
	private int currentVersion;


	public void markModuleCompleteForType(MigrationSchemaConverterTypes type) {
		getMigrationVersionList().forEach(version -> version.markTypeComplete(type));
	}


	public void addExistingMigrationVersion(MigrationModuleVersion existingMigrationVersion) {
		if (this.existingMigrationVersionMap == null) {
			this.existingMigrationVersionMap = new TreeMap<>();
		}
		this.existingMigrationVersionMap.put(existingMigrationVersion.getMigrationPath(), existingMigrationVersion);
	}


	public void addMigrationVersion(MigrationModuleVersion migrationVersion) {
		if (this.migrationVersionList == null) {
			this.migrationVersionList = new ArrayList<>();
		}
		this.migrationVersionList.add(migrationVersion);
	}


	public String getModuleName() {
		return this.moduleName;
	}


	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}


	public int getTargetedVersion() {
		return this.targetedVersion;
	}


	public void setTargetedVersion(int targetedVersion) {
		this.targetedVersion = targetedVersion;
	}


	public int getCurrentVersion() {
		return this.currentVersion;
	}


	public void setCurrentVersion(int currentVersion) {
		this.currentVersion = currentVersion;
	}


	public List<Resource> getMigrationResourceList() {
		return this.migrationResourceList;
	}


	public void setMigrationResourceList(List<Resource> migrationResourceList) {
		this.migrationResourceList = migrationResourceList;
	}


	public List<String> getMigrationFileList() {
		return this.migrationFileList;
	}


	public void setMigrationFileList(List<String> migrationFileList) {
		this.migrationFileList = migrationFileList;
	}


	public List<MigrationModuleVersion> getMigrationVersionList() {
		if (this.migrationVersionList == null) {
			return new ArrayList<>();
		}
		return this.migrationVersionList;
	}


	public SortedMap<String, MigrationModuleVersion> getExistingMigrationVersionMap() {
		if (this.existingMigrationVersionMap == null) {
			return new TreeMap<>();
		}
		return this.existingMigrationVersionMap;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}
}
