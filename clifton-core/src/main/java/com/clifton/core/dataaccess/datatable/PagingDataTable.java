package com.clifton.core.dataaccess.datatable;


import com.clifton.core.util.dataaccess.PagingList;


/**
 * The <code>PagingDataTable</code> is a DataTable that supports paging.
 *
 * @author vgomelsky
 */
public interface PagingDataTable extends DataTable, PagingList {

	/**
	 * Sets page size (max number of elements per page).
	 */
	public void setPageSize(int pageSize);


	/**
	 * Sets total number of rows (not just current page).
	 */
	public void setTotalRowCount(int totalRowCount);
}
