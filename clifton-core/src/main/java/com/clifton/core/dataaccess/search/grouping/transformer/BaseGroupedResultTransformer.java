package com.clifton.core.dataaccess.search.grouping.transformer;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.grouping.GroupingAggregateProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingProperty;
import com.clifton.core.dataaccess.search.grouping.GroupingSearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.hibernate.transform.ResultTransformer;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public abstract class BaseGroupedResultTransformer<T extends IdentityObject> implements ResultTransformer {

	private final List<GroupingProperty> groupingPropertyList;

	private final List<GroupingAggregateProperty> groupingAggregatePropertyList;

	private final DAOConfiguration<T> daoConfiguration;

	private final DaoLocator daoLocator;

	private final Map<String, Object> fkEntityLookupMap = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseGroupedResultTransformer(GroupingSearchForm groupingSearchForm, DaoLocator daoLocator, DAOConfiguration<T> daoConfiguration) {
		super();
		ValidationUtils.assertNotNull(groupingSearchForm, "Grouping Search Form is required.");
		ValidationUtils.assertNotEmpty(groupingSearchForm.getGroupingPropertyList(), "At least one property to group on is required.");
		this.groupingPropertyList = groupingSearchForm.getGroupingPropertyList();

		this.groupingAggregatePropertyList = groupingSearchForm.getGroupingAggregatePropertyList();

		this.daoConfiguration = daoConfiguration;
		this.daoLocator = daoLocator;
	}


	protected int getCountFromTuple(Object[] tuple) {
		return MathUtils.getNumberAsInteger((Number) tuple[getGroupingPropertyList().size()]);
	}


	protected Map<GroupingAggregateProperty, Number> getAggregatePropertyMapFromTuple(Object[] tuple) {
		Map<GroupingAggregateProperty, Number> groupingAggregatePropertyObjectMap = new HashMap<>();

		if (!CollectionUtils.isEmpty(getGroupingAggregatePropertyList())) {
			int aggregatePropertyIndex = getGroupingPropertyList().size() + 1;

			for (GroupingAggregateProperty aggregateProperty : getGroupingAggregatePropertyList()) {
				groupingAggregatePropertyObjectMap.put(aggregateProperty, (Number) tuple[aggregatePropertyIndex]);
				aggregatePropertyIndex++;
			}
		}
		return groupingAggregatePropertyObjectMap;
	}


	@SuppressWarnings("unchecked")
	protected Object getHydratedValue(GroupingProperty groupingProperty, Object value) {
		Object hydratedValue = value;

		if (StringUtils.endsWith(groupingProperty.getBeanPropertyName(), ".id") && value != null) {
			String key = StringUtils.generateKey(groupingProperty.getBeanPropertyName(), value);
			hydratedValue = this.fkEntityLookupMap.computeIfAbsent(key, k -> {
				Class<?> clz = BeanUtils.getPropertyType(this.daoConfiguration.getBeanClass(), StringUtils.substringBeforeLast(groupingProperty.getBeanPropertyName(), "."));
				if (IdentityObject.class.isAssignableFrom(clz)) {
					@SuppressWarnings("unchecked")
					Class<? extends IdentityObject> dtoClz = (Class<? extends IdentityObject>) clz;
					ReadOnlyDAO<IdentityObject> dao = this.daoLocator.locate((Class<IdentityObject>) dtoClz);
					IdentityObject daoBean = dao.findByPrimaryKey(dao.convertToPrimaryKeyDataType((Serializable) value));
					return daoBean;
				}
				return null;
			});
		}
		return hydratedValue;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<GroupingProperty> getGroupingPropertyList() {
		return this.groupingPropertyList;
	}


	public List<GroupingAggregateProperty> getGroupingAggregatePropertyList() {
		return this.groupingAggregatePropertyList;
	}


	public DAOConfiguration<T> getDaoConfiguration() {
		return this.daoConfiguration;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public Map<String, Object> getFkEntityLookupMap() {
		return this.fkEntityLookupMap;
	}
}
