package com.clifton.core.dataaccess.search.form;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BaseRestrictiveSearchForm</code> class is a concrete implementation of the {@link RestrictiveSearchForm} interface. This search form provides a
 * restriction list field for designating explicit field restrictions within the query.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseSortableSearchForm Parent fields}
 * <li>{@link #restrictionList Restriction list search fields} - These fields may be configured via the {@link SearchField} annotation
 * <ul>
 *
 * @author vgomelsky
 * @see RestrictiveSearchForm
 */
public abstract class BaseRestrictiveSearchForm extends BaseSortableSearchForm implements RestrictiveSearchForm {

	/**
	 * The list of {@link SearchRestriction} restrictions for the query.
	 */
	private List<SearchRestriction> restrictionList;


	/**
	 * Creates and returns a "clone" of this SearchForm. Creates a deep copy of restrictions.
	 */
	public BaseEntitySearchForm newInstance() {
		BaseEntitySearchForm clone = new AuditableEntitySearchForm();
		clone.setLimit(this.getLimit());
		clone.setStart(this.getStart());
		clone.setOrderBy(this.getOrderBy());
		clone.setRestrictionList(CoreCollectionUtils.clone(this.getRestrictionList()));
		return clone;
	}


	@Override
	public List<SearchRestriction> getRestrictionList() {
		return this.restrictionList;
	}


	@Override
	public void setRestrictionList(List<SearchRestriction> restrictionList) {
		this.restrictionList = restrictionList;
	}


	/**
	 * Returns true if the specified field is part of search restrictions for this SearchForm.
	 */
	@Override
	public boolean containsSearchRestriction(String fieldName) {
		return getSearchRestriction(fieldName) != null;
	}


	/**
	 * Removes {@link SearchRestriction} for the field with the specified name.
	 * Returns removed restriction or null.
	 */
	@Override
	public SearchRestriction removeSearchRestriction(String fieldName) {
		SearchRestriction restriction = getSearchRestriction(fieldName);
		if (restriction != null) {
			this.restrictionList.remove(restriction);
		}
		return restriction;
	}


	/**
	 * Retrieves the first {@link SearchRestriction} in the restriction list that matches the specified field name
	 */
	@Override
	public SearchRestriction getSearchRestriction(String fieldName) {
		for (SearchRestriction restriction : CollectionUtils.getIterable(this.restrictionList)) {
			if (fieldName.equals(restriction.getField())) {
				return restriction;
			}
		}
		return null;
	}


	/**
	 * Retrieve a list of {@link SearchRestriction} that match any of the specified field names
	 */
	public List<SearchRestriction> getSearchRestrictions(String... fieldNames) {
		return BeanUtils.filter(this.restrictionList, restriction -> ArrayUtils.contains(fieldNames, restriction.getField()));
	}


	/**
	 * Returns true if for ANY field names there is a {@link SearchRestriction} that matches the field name
	 * OR if the field value is set on the search form.
	 */
	public boolean isSearchRestrictionSetAny(String... fieldNames) {
		for (String fieldName : fieldNames) {
			if (getSearchRestriction(fieldName) != null || BeanUtils.getPropertyValue(this, fieldName, true) != null) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Returns true if for ALL field names there is a {@link SearchRestriction} that matches the field name
	 * OR if the field value is set on the search form.
	 */
	public boolean isSearchRestrictionSetAll(String... fieldNames) {
		for (String fieldName : fieldNames) {
			if (getSearchRestriction(fieldName) == null && BeanUtils.getPropertyValue(this, fieldName, true) == null) {
				return false;
			}
		}
		return true;
	}


	/**
	 * Add a restriction to the list, creates the list if it is null.
	 */
	@Override
	public void addSearchRestriction(SearchRestriction restriction) {
		if (getRestrictionList() == null) {
			setRestrictionList(new ArrayList<>());
		}
		getRestrictionList().add(restriction);
	}


	/**
	 * If the specified 'value' argument is not null, creates a new SearchRestriction for the specified arguments and
	 * add the to the list.  Return true. Otherwise, return false and does not do anything.
	 */
	public boolean addSearchRestriction(String beanFieldName, ComparisonConditions comparison, Object value) {
		if (value != null) {
			addSearchRestriction(new SearchRestriction(beanFieldName, comparison, value));
			return true;
		}
		return false;
	}


	/**
	 * Returns true if for the given field name there is a {@link SearchRestriction} that matches the field name
	 * OR if the field value is set on the search form.
	 */
	public boolean isSearchRestrictionSet(String fieldName) {
		return isSearchRestrictionSetAny(fieldName);
	}


	/**
	 * Returns the value of the field name first checking the restriction list and then checking bean field property.
	 */
	public Object getSearchRestrictionValue(String fieldName) {
		SearchRestriction restriction = getSearchRestriction(fieldName);
		if (restriction != null) {
			return restriction.getValue();
		}
		return BeanUtils.getPropertyValue(this, fieldName, true);
	}


	/**
	 * Sets the SearchRestriction corresponding to the fieldName to null and also sets the bean property value to null;
	 */
	public void removeSearchRestrictionAndBeanField(String fieldName) {
		removeSearchRestriction(fieldName);
		BeanUtils.setPropertyValue(this, fieldName, null);
	}
}
