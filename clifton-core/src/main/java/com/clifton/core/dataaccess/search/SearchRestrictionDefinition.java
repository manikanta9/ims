package com.clifton.core.dataaccess.search;


import com.clifton.core.util.validation.FieldValidationException;

import java.util.List;


/**
 * The <code>SearchRestrictionDefinition</code> interface defines methods that are exposed by data access
 * search restriction definition objects.
 *
 * @author vgomelsky
 */
public interface SearchRestrictionDefinition {

	/**
	 * Returns true if this restriction is required (must always be applied to query).
	 */
	public boolean isRequired();


	/**
	 * Returns the name of the field used for this restriction.  Usually a bean field name.
	 */
	public String getFieldName();


	/**
	 * Returns the search field name for this restriction. Usually DB field name (including alias if necessary).
	 */
	public String getSearchFieldName();


	/**
	 * Returns path for the search field assuming joins are necessary.  Returns null if the search field is part of this entity.
	 */
	public String getSearchFieldPath();


	/**
	 * Returns the data type (Class) of search field.
	 */
	public Class<?> getSearchFieldDataType();


	/**
	 * Sets the data type (Class) of search field.
	 */
	public void setSearchFieldDataType(Class<?> dataType);


	/**
	 * Specifies whether LEFT JOIN (true) or INNER JOIN (false) is used in SQL to join search field path.
	 */
	public boolean isLeftJoin();


	/**
	 * Specifies whether if this is a sub query instead of a Join.
	 */
	public boolean isSubQuery();


	/**
	 * Returns the order by field name. Usually DB field name (including alias if necessary).
	 * If order by field is not set, returns search field name.
	 */
	public String getOrderByFieldName();


	/**
	 * Validates the specified search restriction against this definition.
	 * Throws {@link FieldValidationException} if the specified {@link SearchRestriction} has invalid
	 * value or condition.
	 */
	public void validate(SearchRestriction restriction);


	/**
	 * Returns object representation ({@link java.util.Date}, {@link Integer}, etc.) of the specified String value.
	 * Performs conversion based on the data type that the restriction defines.
	 * If the argument is not a String, returns unchanged value (already in required form)
	 */
	public Object getValue(Object rawValue);


	/**
	 * Returns how to handle cases when multiple searchFields are present.  Default is to use OR clauses.
	 */
	public SearchFieldCustomTypes getSearchFieldCustomType();


	/**
	 * Returns a Set of ComparisonCondition objects that are allowed for this restriction.
	 */
	public List<ComparisonConditions> getSearchConditionList();


	/**
	 * Returns the sql formula used for sorting and ordering.
	 */
	public String getSqlFormula();


	/**
	 * Sets the properly formatted sql formula used for sorting and ordering.
	 */
	public void setSqlFormula(String sqlFormula);
}
