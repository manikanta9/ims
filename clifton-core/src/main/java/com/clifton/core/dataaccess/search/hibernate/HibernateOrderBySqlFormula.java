package com.clifton.core.dataaccess.search.hibernate;


import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * Extends {@link org.hibernate.criterion.Order} to allow ordering by an SQL formula passed by the user.
 * Is simply appends the <code>sqlFormula</code> passed by the user to the resulting SQL query, without any verification.
 *
 * @author Sorin Postelnicu
 * @since Jun 10, 2008
 */
public class HibernateOrderBySqlFormula extends Order {

	private final String sqlFormula;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructor for Order.
	 *
	 * @param sqlFormula an SQL formula that will be appended to the resulting SQL query
	 */
	private HibernateOrderBySqlFormula(String sqlFormula) {
		super(sqlFormula, true);
		this.sqlFormula = sqlFormula;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return this.sqlFormula;
	}


	@Override
	public String toSqlString(@SuppressWarnings("unused") Criteria criteria, @SuppressWarnings("unused") CriteriaQuery criteriaQuery) throws HibernateException {
		return this.sqlFormula;
	}


	/**
	 * Custom order
	 *
	 * @param sqlFormula an SQL formula that will be appended to the resulting SQL query
	 * @return Order
	 */
	public static Order sqlFormula(String sqlFormula) {
		return new HibernateOrderBySqlFormula(sqlFormula);
	}
}
