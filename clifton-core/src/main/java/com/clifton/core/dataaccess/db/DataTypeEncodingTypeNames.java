package com.clifton.core.dataaccess.db;

/**
 * The <code>DataTypeEncodingTypeNames</code> is used by Migration Columns to define what encoding the column uses.  This is used to create columns differently.
 * i.e. USC_2 is the default and uses NVARCHAR, and UTF_8 is newly available in SQL 2019 and supports VARCHAR which can reduce column sizes in half
 * <p>
 * Only used for Columns whose Data Type . Data Type Name Supports Encoding (i.e. String values)
 *
 * @author manderson
 */
public enum DataTypeEncodingTypeNames {

	UCS_2, // Default for SQL Server - uses NCHAR, NVARCHAR
	UTF_8 // Supports use of CHAR, VARCHAR columns with UTF-8 encoding
}
