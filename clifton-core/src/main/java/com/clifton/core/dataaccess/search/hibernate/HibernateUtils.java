package com.clifton.core.dataaccess.search.hibernate;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.internal.CriteriaImpl;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.metamodel.internal.MetamodelImpl;
import org.hibernate.persister.collection.AbstractCollectionPersister;
import org.hibernate.persister.entity.AbstractEntityPersister;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.proxy.HibernateProxy;
import org.hibernate.sql.JoinType;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.CollectionType;
import org.hibernate.type.IntegerType;
import org.hibernate.type.LongType;
import org.hibernate.type.ShortType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * Utility methods used for hibernate functionality
 * <p>
 *
 * @author stevenf on 9/28/2016.
 */
public class HibernateUtils {

	public static final String DEFAULT_ALIAS_PREFIX = "a";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static void addEqualsRestriction(String beanFieldName, Object beanFieldValue, Criteria criteria) {
		addEqualsRestriction(beanFieldName, beanFieldValue, criteria, null);
	}


	public static void addEqualsRestriction(String beanFieldName, Object beanFieldValue, Criteria criteria, Map<String, Criteria> processedPathToCriteria) {
		String[] paths = beanFieldName.split("\\.");
		String name = paths[paths.length - 1];
		if (paths.length > 2 || (paths.length > 1 && !"id".equals(name))) {
			String path = beanFieldName.substring(0, beanFieldName.lastIndexOf("."));
			if ("id".equals(name)) {
				name = paths[paths.length - 2] + "." + name;
				path = path.substring(0, beanFieldName.lastIndexOf("." + name));
			}
			String alias = getPathAlias(path, criteria, DEFAULT_ALIAS_PREFIX, beanFieldValue == null ? JoinType.LEFT_OUTER_JOIN : JoinType.INNER_JOIN, processedPathToCriteria);
			beanFieldName = alias + "." + name;
		}
		if (beanFieldValue == null) {
			criteria.add(Restrictions.isNull(beanFieldName));
		}
		else {
			criteria.add(Restrictions.eq(beanFieldName, beanFieldValue));
		}
	}


	/**
	 * Returns the alias of the detached criteria for the specified path.
	 *
	 * @see #getPathAlias(String, Criteria, String, JoinType, Map)
	 */
	public static String getPathAlias(String fieldPath, DetachedCriteria criteria, String aliasPrefix, JoinType joinType, Map<String, Criteria> processedPathToCriteria) {
		return getPathAlias(fieldPath, criteria.getExecutableCriteria(null), aliasPrefix, joinType, processedPathToCriteria);
	}


	/**
	 * Returns the alias of the criteria for the specified path. If the joins haven't been processed yet,
	 * adds the necessary joins of the specified type (JoinType.INNER_JOIN, JoinType.LEFT_OUTER_JOIN, etc.) to form the path first.
	 *
	 * @param fieldPath
	 * @param criteria
	 */
	public static String getPathAlias(String fieldPath, Criteria criteria, String aliasPrefix, JoinType joinType, Map<String, Criteria> processedPathToCriteria) {
		if (StringUtils.isEmpty(fieldPath)) {
			return null;
		}
		if (processedPathToCriteria == null) {
			processedPathToCriteria = new HashMap<>();
		}

		Criteria pathCriteria = processedPathToCriteria.get(fieldPath);
		if (pathCriteria == null) {
			// path hasn't been processed: need to add joins
			pathCriteria = criteria;

			// 1. find the starting point first: deepest join that already exists (if none, start with passed Criteria)
			String matchedPath = fieldPath;
			int splitIndex = fieldPath.lastIndexOf('.');
			while (splitIndex != -1) {
				matchedPath = fieldPath.substring(0, splitIndex);
				if (processedPathToCriteria.containsKey(matchedPath)) {
					pathCriteria = processedPathToCriteria.get(matchedPath);
					splitIndex++;
					break;
				}
				splitIndex = matchedPath.lastIndexOf('.');
			}

			// 2. add remaining joins from starting point
			if (splitIndex == -1) {
				matchedPath = "";
				splitIndex = 0;
			}
			String[] paths = fieldPath.substring(splitIndex).split("\\.");
			for (String path : paths) {
				String alias = aliasPrefix + processedPathToCriteria.size();
				pathCriteria = pathCriteria.createCriteria(path, alias, joinType);
				matchedPath = (matchedPath.isEmpty()) ? path : matchedPath + "." + path;
				processedPathToCriteria.put(matchedPath, pathCriteria);
			}
		}
		return pathCriteria == null ? null : pathCriteria.getAlias();
	}


	/**
	 * @see #getAliasedCriteriaPropertyPath(String, Criteria, String, JoinType, Map)
	 */
	public static String getAliasedCriteriaPropertyPath(String fieldPath, Criteria criteria, DetachedCriteria detachedCriteria, String aliasPrefix, JoinType joinType, Map<String, Criteria> processedPathToCriteria) {
		Session session = (Session) HibernateUtils.getCriteriaImpl(criteria).getSession();
		return getAliasedCriteriaPropertyPath(fieldPath, detachedCriteria.getExecutableCriteria(session), aliasPrefix, joinType, processedPathToCriteria);
	}


	/**
	 * Gets the property path for the field to be used in {@link Criteria} components, such as {@link Restrictions}. Any necessary joins are automatically created and stored in the
	 * given <code>processedPathToCriteria</code>.
	 * <p>
	 * The returned property path is normalized and properly handles parameters that can be short-referenced, such as IDs. For example, the path
	 * <code>"investmentSecurity.instrument.hierarchy.id"</code> may be converted to <code>"a2.hierarchy.id"</code>, where <code>"a2"</code> is the alias for the joined
	 * <code>"instrument"</code> reference, where <code>"investmentSecurity.instrument.hierarchy.name"</code> would more likely be converted to <code>"a3.name"</code>, where
	 * <code>"a3"</code> is the alias for the <code>"hierarchy"</code> reference.
	 * <p>
	 * <i>Note: Reducing unnecessary joins for short-references currently assumes that all joins are done on ID references.</i>
	 *
	 * @param fieldPath               the path to alias and normalize
	 * @param criteria                the criteria against which the path will be evaluated
	 * @param aliasPrefix             the prefix to use when generating aliases for joined tables, such as <code>"a1"</code> or <code>"e1"</code>
	 * @param joinType                the type of join that should be used when joining tables that have not previously been joined
	 * @param processedPathToCriteria the map of processed join paths to prevent redundant join clauses
	 * @return the normalized property path for use in {@link Criteria} components
	 */
	public static String getAliasedCriteriaPropertyPath(String fieldPath, Criteria criteria, String aliasPrefix, JoinType joinType, Map<String, Criteria> processedPathToCriteria) {
		// Determine if a join needs to be made for the final element or if it is directly referencable from its source (typically applicable for foreign key references)
		int startOfPropertyIndex = fieldPath.lastIndexOf('.');
		if (fieldPath.endsWith(".id")) {
			int startOfPossibleFkPropertyIndex = fieldPath.lastIndexOf('.', startOfPropertyIndex - 1);
			if (criteria instanceof CriteriaImpl) {
				// Short-references can only be used if the last element is a foreign key reference (this is only supported when using Hibernate criteria (CriteriaImpl))
				String possibleAssociationName = fieldPath.substring(startOfPossibleFkPropertyIndex + 1, startOfPropertyIndex);
				SessionFactory sessionFactory = getSessionFactory(criteria);
				Class<?> associationSourceType = startOfPossibleFkPropertyIndex != -1
						? getEntityPropertyTypeForPath(sessionFactory, getCriteriaEntityClass(criteria), fieldPath.substring(0, startOfPossibleFkPropertyIndex))
						: getCriteriaEntityClass(criteria);
				String targetRole = associationSourceType.getName() + '.' + possibleAssociationName;
				// TODO: When JPA entity metamodels are re-enabled, use the following:
				//  ManagedTypeDescriptor<?> entityMetamodel = (ManagedTypeDescriptor<?>) sessionFactory.getMetamodel().entity(associationSourceType);
				//  PersistentAttributeDescriptor<?, ?> possibleAssociationAttribute = entityMetamodel.findAttribute(possibleAssociationName);
				//  boolean secondToLastElementIsAssociation = possibleAssociationAttribute != null && possibleAssociationAttribute.isAssociation();
				boolean secondToLastElementIsAssociation = ((MetamodelImpl) sessionFactory.getMetamodel()).collectionPersisters().get(targetRole) != null;
				if (!secondToLastElementIsAssociation) {
					// Treat last two elements as one; a short-reference can be used since the FK "id" field is already provided in the source table
					startOfPropertyIndex = startOfPossibleFkPropertyIndex;
				}
			}
			else {
				// Non-standard criteria implementation used; assume all possible short-paths are automatically handled
				startOfPropertyIndex = startOfPossibleFkPropertyIndex;
			}
		}
		// Split field path into "path" (for joins) and "property"
		String targetPath = startOfPropertyIndex != -1 ? fieldPath.substring(0, startOfPropertyIndex) : null;
		String targetProperty = fieldPath.substring(startOfPropertyIndex + 1);
		// Generate aliases
		String targetPathAlias = HibernateUtils.getPathAlias(targetPath, criteria, aliasPrefix, joinType, processedPathToCriteria);
		return StringUtils.joinExcludingNulls(".", targetPathAlias, targetProperty);
	}


	/**
	 * If the provided entity is a non-null Hibernate proxy, the entity will be returned after looking up the value from either second level Hibernate cache,
	 * if it is present, or the database. If the provided entity is not a Hibernate proxy, the provided object is returned.
	 * <p>
	 * NOTE: A provided proxy entity can be unproxied only if the transaction used to initially retrieve the proxy is still active.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T unwrapEntityIfProxied(Object entity) {
		if (entity instanceof HibernateProxy) {
			return (T) ((HibernateProxy) entity).getHibernateLazyInitializer().getImplementation();
		}
		return (T) entity;
	}


	/**
	 * Returns the appropriate type based on the class of the value (Long, Integer, Short, BigDecimal, etc.) defaults to Integer
	 *
	 * @param value
	 */
	public static Type getNumericType(Serializable value) {
		if (value instanceof Long) {
			return LongType.INSTANCE;
		}
		if (value instanceof Short) {
			return ShortType.INSTANCE;
		}
		if (value instanceof BigDecimal) {
			return BigDecimalType.INSTANCE;
		}
		return IntegerType.INSTANCE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the session factory associated with the given criteria.
	 *
	 * @param criteria the criteria object to analyze
	 * @return the session factory associated with the criteria object
	 */
	public static SessionFactory getSessionFactory(Criteria criteria) {
		return getCriteriaImpl(criteria).getSession().getFactory();
	}


	/**
	 * Gets the entity name associated with the given criteria. This is the root object upon which the criteria query is based.
	 *
	 * @param criteria the criteria object to analyze
	 * @return the entity name associated with the criteria object
	 */
	public static String getCriteriaEntityName(Criteria criteria) {
		return getCriteriaImpl(criteria).getEntityOrClassName();
	}


	/**
	 * Gets the entity class associated with the given criteria. This is the root object upon which the criteria query is based.
	 *
	 * @param criteria the criteria object to analyze
	 * @return the class of the entity associated with the criteria object
	 */
	public static Class<?> getCriteriaEntityClass(Criteria criteria) {
		return ((MetamodelImpl) getSessionFactory(criteria).getMetamodel()).entityPersister(getCriteriaEntityName(criteria)).getMappedClass();
	}


	/**
	 * Retrieves the collection persister for the given collection role.
	 *
	 * @param sessionFactory the session factory to use to retrieve ORM information
	 * @param role           the association role; this is typically in the form "<tt>&lt;source class name&gt;.&lt;link name&gt;</tt>"
	 * @return the collection persister
	 */
	public static AbstractCollectionPersister getCollectionPersister(SessionFactory sessionFactory, String role) {
		final AbstractCollectionPersister collectionPersister;
		try {
			CollectionMetadata collectionMetadata = (CollectionMetadata) ((MetamodelImpl) sessionFactory.getMetamodel()).collectionPersister(role);
			AssertUtils.assertTrue(collectionMetadata instanceof AbstractCollectionPersister, "Association class information can only be retrieved for roles with type [%s]. Actual type: [%s].", AbstractCollectionPersister.class.getName(), collectionMetadata.getClass().getName());
			collectionPersister = (AbstractCollectionPersister) collectionMetadata;
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to get collection persister for role [%s].", role), e);
		}
		return collectionPersister;
	}


	/**
	 * Gets the expected property type for the given property path on the given entity class.
	 *
	 * @param sessionFactory the session factory to use to retrieve ORM information
	 * @param entityClass    the class for which the property should be sought
	 * @param propertyPath   the dot-delimited path of the property for which to retrieve type information
	 * @return the type for the given property name on the given class
	 */
	public static Class<?> getEntityPropertyTypeForPath(SessionFactory sessionFactory, Class<?> entityClass, String propertyPath) {
		// Guard-clause: Trivial case, return initial type
		if (StringUtils.isEmpty(propertyPath)) {
			return entityClass;
		}
		Class<?> currentPropertyType = entityClass;
		try {
			for (String property : propertyPath.split("\\.")) {
				currentPropertyType = getTypeForEntityPropertyName(sessionFactory, currentPropertyType.getName(), property);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while getting entity property type info for class [%s] and property path [%s].", entityClass.getName(), propertyPath), e);
		}
		return currentPropertyType;
	}


	/**
	 * Gets the identifier property for the given class.
	 *
	 * @param sessionFactory the session factory to use to retrieve ORM information
	 * @param className      the name of the class for whose identifier property should be found
	 * @return the identifier property
	 */
	public static String getIdentifierPropertyName(SessionFactory sessionFactory, String className) {
		ClassMetadata sourceClassMetadata = (ClassMetadata) ((MetamodelImpl) sessionFactory.getMetamodel()).entityPersister(className);
		return sourceClassMetadata.getIdentifierPropertyName();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the association class for the association with the given <i>role</i>. The association class is the one-to-many or many-to-many entity type for the given role. For
	 * many-to-many references, this is the join association table. For one-to-many references, this is the associated table.
	 *
	 * @param sessionFactory the session factory to use to retrieve ORM information
	 * @param role           the association role; this is typically in the form "<tt>&lt;source class name&gt;.&lt;link name&gt;</tt>"
	 * @return the class used for the association
	 */
	public static Class<?> getAssociationClass(SessionFactory sessionFactory, String role) {
		AbstractCollectionPersister abstractCollectionPersister = getCollectionPersister(sessionFactory, role);
		final Class<?> associationClass;
		try {
			if (abstractCollectionPersister.isOneToMany()) {
				// Join table is referenced; return class is available for join table
				associationClass = abstractCollectionPersister.getElementType().getReturnedClass();
			}
			else {
				// Table reference must be manually derived
				AssertUtils.assertTrue(sessionFactory instanceof SessionFactoryImplementor, "A session factory of type [%s] is required for derivation of entity types from table names. Actual type: [%s].", SessionFactoryImplementor.class.getName(), sessionFactory.getClass().getName());
				SessionFactoryImplementor sessionFactoryImpl = (SessionFactoryImplementor) sessionFactory;
				@SuppressWarnings("unchecked")
				Collection<AbstractEntityPersister> entityPersisterList = (Collection<AbstractEntityPersister>) (Collection<? extends EntityPersister>)
						sessionFactoryImpl.getMetamodel().entityPersisters().values();
				associationClass = entityPersisterList.stream()
						.filter(entityPersister -> abstractCollectionPersister.getTableName().equals(entityPersister.getTableName()))
						.map(entityPersister -> entityPersister.getEntityType().getReturnedClass())
						.findAny()
						.orElseThrow(() -> new RuntimeException(String.format("No entity matching required table name [%s] was found in the current session factory context.", abstractCollectionPersister.getTableName())));
			}
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while retrieving the association class for role [%s].", role), e);
		}
		return associationClass;
	}


	/**
	 * Gets the key property name associated with the given association role. This is the property name on the association which is used for the relationship for the provided role.
	 *
	 * @param sessionFactory   the session factory to use to retrieve ORM information
	 * @param role             the association role; this is typically in the form "<tt>&lt;source class name&gt;.&lt;link name&gt;</tt>"
	 * @param associationClass the type of the association class, as retrieved via {@link #getAssociationClass(SessionFactory, String)}
	 * @return the key property name which links the given association class to the source in the provided role
	 */
	public static String getAssociationKeyProperty(SessionFactory sessionFactory, String role, Class<?> associationClass) {
		final String propertyName;
		try {
			// Get column name for relationship
			AbstractCollectionPersister abstractCollectionPersister = getCollectionPersister(sessionFactory, role);
			String[] targetKeyColumnNames = abstractCollectionPersister.getKeyColumnNames();
			AssertUtils.assertTrue(targetKeyColumnNames.length == 1, "Association key properties can only be retrieved for associations on a single key. Table name: [%s]. Association keys: [%s].", abstractCollectionPersister.getTableName(), ArrayUtils.toString(targetKeyColumnNames));
			String targetKeyColumnName = targetKeyColumnNames[0];

			// Get property name matching relationship column
			propertyName = getPropertyNameForColumnName(sessionFactory, associationClass, targetKeyColumnName);
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to get association key property name for role [%s] and association class [%s].", role, associationClass.getName()), e);
		}
		return propertyName;
	}


	/**
	 * Gets the element property name associated with the given association role. This is the property name on the association which is used for the relationship for the provided role.
	 *
	 * @param sessionFactory   the session factory to use to retrieve ORM information
	 * @param role             the association role; this is typically in the form "<tt>&lt;source class name&gt;.&lt;link name&gt;</tt>"
	 * @param associationClass the type of the association class, as retrieved via {@link #getAssociationClass(SessionFactory, String)}
	 * @return the element property name which links the given association class to the element in the provided role
	 */
	public static String getAssociationElementKeyProperty(SessionFactory sessionFactory, String role, Class<?> associationClass) {
		final String propertyName;
		try {
			// Get column name for relationship
			AbstractCollectionPersister abstractCollectionPersister = getCollectionPersister(sessionFactory, role);
			String[] targetElementKeyColumnNames = abstractCollectionPersister.getElementColumnNames();
			AssertUtils.assertTrue(targetElementKeyColumnNames.length == 1, "Association element key properties can only be retrieved for associations on a single element key. Table name: [%s]. Association element keys: [%s].", abstractCollectionPersister.getTableName(), ArrayUtils.toString(targetElementKeyColumnNames));
			String targetElementKeyColumnName = targetElementKeyColumnNames[0];

			// Get property name matching relationship column
			propertyName = getPropertyNameForColumnName(sessionFactory, associationClass, targetElementKeyColumnName);
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to get association element key property name for role [%s] and association class [%s].", role, associationClass.getName()), e);
		}
		return propertyName;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the property name matching the relationship column of the association.
	 *
	 * @param sessionFactory the session factory to use to retrieve ORM information
	 * @param entityType     the type of the entity
	 * @param columnName     the name of the relationship column
	 * @return the property name for the relationship column
	 */
	private static String getPropertyNameForColumnName(SessionFactory sessionFactory, Class<?> entityType, String columnName) {
		final String columnPropertyName;
		try {
			ClassMetadata targetClassMetadata = (ClassMetadata) ((MetamodelImpl) sessionFactory.getMetamodel()).entityPersister(entityType);
			AssertUtils.assertTrue(targetClassMetadata instanceof AbstractEntityPersister, "Interpolating property names from column names can only be performed for entity class metadata of type [%s]. Actual type: [%s].", AbstractEntityPersister.class.getName(), targetClassMetadata.getClass().getName());
			AbstractEntityPersister targetEntityPersister = ((AbstractEntityPersister) targetClassMetadata);
			List<String> targetEntityPropertyNameList = CollectionUtils.combineCollections(
					Collections.singleton(targetEntityPersister.getIdentifierPropertyName()),
					Arrays.asList(targetEntityPersister.getPropertyNames()));
			List<String> columnPropertyNameList = targetEntityPropertyNameList.stream()
					// One-to-many collection types (e.g. links) are not supported, as there may be more than one of these for a given column name
					.filter(propertyName -> !targetEntityPersister.getPropertyType(propertyName).isCollectionType())
					// Column names are regarded case-insensitively
					.filter(propertyName -> ArrayUtils.getStream(targetEntityPersister.getPropertyColumnNames(propertyName))
							.map(String::toUpperCase)
							.anyMatch(columnName.toUpperCase()::equals))
					.collect(Collectors.toList());
			if (columnPropertyNameList.isEmpty()) {
				throw new RuntimeException(String.format("No matching property name was found for column [%s] in table [%s].", columnName, targetEntityPersister.getTableName()));
			}
			else if (columnPropertyNameList.size() > 1) {
				throw new RuntimeException(String.format("Multiple matching property names were found for column [%s] in table [%s]: %s", columnName, targetEntityPersister.getTableName(), columnPropertyNameList));
			}
			else {
				columnPropertyName = CollectionUtils.getOnlyElementStrict(columnPropertyNameList);
			}
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to get property name for column name [%s] on entity type [%s].", columnName, entityType.getName()), e);
		}
		return columnPropertyName;
	}


	/**
	 * Gets the type for the given property name on the given class.
	 *
	 * @param sessionFactory the session factory to use to retrieve ORM information
	 * @param className      the name of the class for which the property should be sought
	 * @param propertyName   the name of the property for which to retrieve type information
	 * @return the type for the given property name on the given class
	 */
	private static Class<?> getTypeForEntityPropertyName(SessionFactory sessionFactory, String className, String propertyName) {
		final Class<?> entityPropertyType;
		try {
			ClassMetadata sourceClassMetadata = (ClassMetadata) ((MetamodelImpl) sessionFactory.getMetamodel()).entityPersister(className);
			AssertUtils.assertTrue(sourceClassMetadata instanceof AbstractEntityPersister, "Interpolating property types can only be performed for entity class metadata of type [%s]. Actual type: [%s].", AbstractEntityPersister.class.getName(), sourceClassMetadata.getClass().getName());
			Type directPropertyType = sourceClassMetadata.getPropertyType(propertyName);
			Type resolvedPropertyType = directPropertyType.isCollectionType()
					? ((CollectionType) directPropertyType).getElementType((SessionFactoryImplementor) sessionFactory)
					: directPropertyType;
			entityPropertyType = resolvedPropertyType.getReturnedClass();
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to interpolate property types for class [%s] and property name [%s]", className, propertyName), e);
		}
		return entityPropertyType;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Casts the {@link Criteria} object to a {@link CriteriaImpl}, throwing an exception if the argument is not of the required type.
	 *
	 * @param criteria the criteria object
	 * @return the casted criteria implementation object
	 */
	private static CriteriaImpl getCriteriaImpl(Criteria criteria) {
		AssertUtils.assertTrue(criteria instanceof CriteriaImpl, "Session information can only be retrieved from criteria object of type [%s]. Actual type: [%s].", CriteriaImpl.class.getName(), criteria.getClass().getName());
		return (CriteriaImpl) criteria;
	}
}
