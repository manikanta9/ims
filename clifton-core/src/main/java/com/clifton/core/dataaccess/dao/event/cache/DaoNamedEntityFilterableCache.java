package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.NamedObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;

import java.io.Serializable;
import java.util.List;
import java.util.function.Predicate;


/**
 * The <code>DaoNamedEntityFilterableCache</code> A named cache with additional filter methods to specify a dao and string predicate for
 * caching search results on based on a Predicate.  The filter method returns a list of entity ids for entities matching the predicate test rule.
 * The filterPopulated method also uses a Predicate for filtering, but returns and returns a list of NamedObject entities.
 *
 * @author davidi
 */
public interface DaoNamedEntityFilterableCache<T extends NamedObject> extends DaoNamedEntityCache<T> {

	/**
	 * Returns a list of ID's for entities that match the filter criteria
	 */
	<I extends Serializable> List<I> filter(ReadOnlyDAO<T> dao, Predicate<T> predicate);


	/**
	 * Returns a filtered list of entities
	 */
	List<T> filterPopulated(ReadOnlyDAO<T> dao, Predicate<T> predicate);
}
