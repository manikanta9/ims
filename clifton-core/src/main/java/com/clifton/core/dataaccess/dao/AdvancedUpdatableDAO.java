package com.clifton.core.dataaccess.dao;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>AdvancedUpdatableDAO</code> interface is an updatable Data Access Object that supports
 * advanced search criteria
 *
 * @param <T>
 * @param <C> search criteria
 * @author vgomelsky
 */
public interface AdvancedUpdatableDAO<T extends IdentityObject, C> extends UpdatableDAO<T>, AdvancedReadOnlyDAO<T, C> {

	/**
	 * Executes a named query (stored procedure) that updates or inserts records.
	 *
	 * @param queryName
	 * @param parameterValues
	 */
	public void executeNamedQuery(String queryName, Object... parameterValues);
}
