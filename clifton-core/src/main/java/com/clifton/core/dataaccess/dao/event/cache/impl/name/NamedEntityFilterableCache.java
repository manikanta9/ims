package com.clifton.core.dataaccess.dao.event.cache.impl.name;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.NamedObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityFilterableCache;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * The <code>DaoNamedEntityFilterableCache</code> A named cache with additional filter methods to specify a dao and string predicate for
 * caching search results on based on a Predicate.  The filter method returns a list of entity ids for entities matching the predicate test rule.
 * The filterPopulated method also uses a Predicate for filtering, but returns and returns a list of NamedObject entities.  The maximum
 * cache size is currently 2000 entries.
 *
 * @author davidi
 */
public class NamedEntityFilterableCache<T extends NamedObject> extends NamedEntityCache<T> implements DaoNamedEntityFilterableCache<T> {

	@Override
	@SuppressWarnings("unchecked")
	public <I extends Serializable> List<I> filter(ReadOnlyDAO<T> dao, Predicate<T> predicate) {
		return (List<I>) CollectionUtils.getConverted(filterPopulated(dao, predicate), IdentityObject::getIdentity);
	}


	@Override
	public List<T> filterPopulated(ReadOnlyDAO<T> dao, Predicate<T> predicate) {
		initCache(dao);
		return CollectionUtils.getStream(getCacheHandler().getKeys(getCacheName())).map(key -> this.getBeanForKeyValue(dao, key)).filter(predicate).collect(Collectors.toList());
	}


	@Override
	protected T getBeanImpl(ReadOnlyDAO<T> dao, boolean throwExceptionIfNotFound, Object... keyProperties) {
		return getBeanImplWithClear(true, dao, keyProperties);
	}


	private T getBeanImplWithClear(boolean firstCall, ReadOnlyDAO<T> dao, Object... keyProperties) {
		// Ensure the cache is initialized - we need to make sure all entities are loaded so filtering works correctly
		initCache(dao);

		String key = getBeanKeyForProperties(keyProperties);
		// Note: This does NOT utilize an "empty" bean for missing cache
		ObjectWrapper<Serializable> idWrapper = getCacheHandler().get(getCacheName(), key);
		Serializable id = (idWrapper != null ? idWrapper.getObject() : null);
		if (id != null) {
			T bean = dao.findByPrimaryKey(id);

			if (bean != null) {
				return bean;
			}
		}
		// If the cache is expired then the keys are there, but the elements are missing so if ID is null
		// clear the entire cache and call getBeanImpl again so the cache is re-initialized
		if (firstCall && id == null) {
			getCacheHandler().clear(getCacheName());
			// Set firstCall to false to prevent infinite loop
			return getBeanImplWithClear(false, dao, keyProperties);
		}
		throw new ValidationException("Unable to find " + StringUtils.splitWords(dao.getConfiguration().getTableName()) + " with properties [" + ArrayUtils.toString(getBeanKeyProperties())
				+ "] and value(s) [" + ArrayUtils.toString(keyProperties) + "]");
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void initCache(ReadOnlyDAO<T> dao) {

		final int MAX_CACHE_ROW_COUNT = 2000;

		if (!isCacheInitialized()) {
			Collection<T> cacheEntryList = dao.findAll();
			if (cacheEntryList != null && cacheEntryList.size() > MAX_CACHE_ROW_COUNT) {
				throw new ValidationException("Cached item count exceeds maximum allowable limit of: " + MAX_CACHE_ROW_COUNT);
			}

			for (T entity : CollectionUtils.getIterable(cacheEntryList)) {
				setBean(entity);
			}
		}
	}


	private boolean isCacheInitialized() {
		return getCacheHandler().getKeys(getCacheName()) != null && !getCacheHandler().getKeys(getCacheName()).isEmpty();
	}
}
