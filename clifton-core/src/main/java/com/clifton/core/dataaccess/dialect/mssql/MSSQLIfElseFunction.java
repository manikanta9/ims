package com.clifton.core.dataaccess.dialect.mssql;

import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import org.hibernate.dialect.function.StandardSQLFunction;


/**
 * <code>MSSQLIfElseFunction</code> is the MSSQL implementation for rendering and an if else function.
 *
 * @author dillonm
 */
public class MSSQLIfElseFunction extends StandardSQLFunction implements SQLIfElseFunction {

	public MSSQLIfElseFunction() {
		super("IIF");
	}
}
