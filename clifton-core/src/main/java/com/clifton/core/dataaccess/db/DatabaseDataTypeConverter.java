package com.clifton.core.dataaccess.db;

import com.clifton.core.shared.dataaccess.DataTypes;


/**
 * The <code>DatabaseDataTypeConverter</code> is a converted used to convert the data type to the database specific representation. i.e. in MSSQL String with a max length of 50 = NVARCHAR(50).  Using UTF-8 it's VARCHAR(50) with UTF 8 collation defined
 *
 * @author manderson
 */
public interface DatabaseDataTypeConverter {


	/**
	 * Returns String representation of the data type and optionally the encoding type
	 *
	 * @param encodingTypeName is not always necessary, but can be determined if required based on the dataType.typeName.isEncodingSupported()
	 */
	public String convert(DataTypes dataType, DataTypeEncodingTypeNames encodingTypeName);
}
