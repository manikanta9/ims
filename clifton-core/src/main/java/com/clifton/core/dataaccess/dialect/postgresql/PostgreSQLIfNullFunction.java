package com.clifton.core.dataaccess.dialect.postgresql;

import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import org.hibernate.dialect.function.StandardSQLFunction;


/**
 * <code>PostgreSQLIfNullFunction</code> is the PostgreSQLIfNullFunction implementation for rendering and an if null function.
 *
 * @author dillonm
 */
public class PostgreSQLIfNullFunction extends StandardSQLFunction implements SQLIfNullFunction {

	public PostgreSQLIfNullFunction() {
		super("COALESCE");
	}
}
