package com.clifton.core.dataaccess.migrate.schema.dml;


import com.clifton.core.shared.dataaccess.DataTypes;


/**
 * The <code>Value</code> class represents a value of a single database cell for the specified column.
 *
 * @author vgomelsky
 */
public class Value {

	private String column;
	private String value;
	private DataTypes dataType;
	private boolean select;


	/**
	 * @return the column
	 */
	public String getColumn() {
		return this.column;
	}


	/**
	 * @param column the column to set
	 */
	public void setColumn(String column) {
		this.column = column;
	}


	/**
	 * @return the value
	 */
	public String getValue() {
		return this.value;
	}


	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}


	/**
	 * @return the dataType
	 */
	public DataTypes getDataType() {
		return this.dataType;
	}


	/**
	 * @param dataType the dataType to set
	 */
	public void setDataType(DataTypes dataType) {
		this.dataType = dataType;
	}


	public boolean isSelect() {
		return this.select;
	}


	public void setSelect(boolean select) {
		this.select = select;
	}
}
