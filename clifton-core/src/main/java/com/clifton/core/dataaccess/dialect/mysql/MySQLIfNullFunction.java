package com.clifton.core.dataaccess.dialect.mysql;

import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import org.hibernate.dialect.function.StandardSQLFunction;


/**
 * <code>MySQLIfNullFunction</code> is the MySQL implementation for rendering and an if null function.
 *
 * @author dillonm
 */
public class MySQLIfNullFunction extends StandardSQLFunction implements SQLIfNullFunction {

	public MySQLIfNullFunction() {
		super("IFNULL");
	}
}
