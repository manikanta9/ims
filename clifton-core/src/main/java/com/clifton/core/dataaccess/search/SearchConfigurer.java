package com.clifton.core.dataaccess.search;


import com.clifton.core.dataaccess.PagingCommand;


/**
 * The <code>SearchConfigurer</code> class configures search restrictions, ordering and pagination for managed search criteria.
 *
 * @author vgomelsky
 */
public interface SearchConfigurer<T> extends PagingCommand {

	/**
	 * Configures the specified search criteria: adds restrictions, order by, etc.
	 */
	public void configureCriteria(T criteria);


	/**
	 * Configures the order by part of the criteria. If false is returned, then the default sort configuration from the schema file will be used.
	 *
	 * @return true if order by was configured, false otherwise
	 */
	public boolean configureOrderBy(T criteria);


	/**
	 * Returns true if READ_UNCOMMITTED transaction isolation level should be used for this search.
	 * If a transaction with different isolation level was started, then this won't matter.
	 * <p/>
	 * NOTE: avoid using READ_UNCOMMITTED unless absolutely necessary for performance reasons and only in special cases.
	 * Make sure caching is not enabled for entities that are used with read uncommitted.
	 */
	public boolean isReadUncommittedRequested();
}
