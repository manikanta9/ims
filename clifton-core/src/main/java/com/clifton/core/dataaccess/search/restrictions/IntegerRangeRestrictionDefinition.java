package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.SearchRestrictionDefinition;
import com.clifton.core.util.validation.FieldValidationException;

import java.util.List;


/**
 * The <code>IntegerRangeRestrictionDefinition</code> class implements {@link SearchRestrictionDefinition} for fields of type {@link Integer}.
 *
 * @author vgomelsky
 */
public class IntegerRangeRestrictionDefinition extends AbstractRestrictionDefinition {

	private final int[] allowedValues;


	public IntegerRangeRestrictionDefinition(String fieldName) {
		this(fieldName, fieldName);
	}


	public IntegerRangeRestrictionDefinition(String fieldName, String searchFieldName) {
		this(fieldName, searchFieldName, null, false, null, null);
	}


	public IntegerRangeRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, null);
	}


	public IntegerRangeRestrictionDefinition(String fieldName, String searchFieldName, String orderByFieldName, int[] allowedValues) {
		this(fieldName, searchFieldName, null, false, orderByFieldName, allowedValues);
	}


	public IntegerRangeRestrictionDefinition(String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, int[] allowedValues) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, false, new ComparisonConditions[]{ComparisonConditions.IN});
		this.allowedValues = allowedValues;
	}


	@Override
	public void validate(SearchRestriction restriction) {
		super.validate(restriction);
		@SuppressWarnings("unchecked")
		List<String> values = (List<String>) getValue(restriction.getValue());
		if (values != null) {
			for (String value : values) {
				int intValue = Integer.parseInt(value);
				if (this.allowedValues != null) {
					boolean allowed = false;
					for (int allowedValue : this.allowedValues) {
						if (allowedValue == intValue) {
							allowed = true;
							break;
						}
					}
					if (!allowed) {
						throw new FieldValidationException("Unsupported value: " + intValue, restriction.getField());
					}
				}
			}
		}
	}
}
