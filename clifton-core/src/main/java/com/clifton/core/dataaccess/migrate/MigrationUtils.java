package com.clifton.core.dataaccess.migrate;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;


/**
 * This class contains shared migration utility methods
 *
 * @author StevenF on 10/5/2016.
 */
public class MigrationUtils {


	/**
	 * Returns the migration resource files in order for the given moduleName (i.e. project name)
	 */
	public static Resource[] getMigrationResources(String moduleName) {
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		try {
			// Note: Would prefer to match against specific numbers, however the path matcher only supports: ? - any character, * - any string, ** - any directory
			// If we ever get to three digits in the folders, will need to re-think this pattern, or append all resources matching against two digits with resources matching three digits
			Resource[] migrationResources = resolver.getResources("classpath*:META-INF/schema/" + moduleName + "-??/*.xml");

			// Must sort migrations loaded off classpath, they may not be in order, sort alphabetically
			Arrays.sort(migrationResources, Comparator.comparing(MigrationUtils::getMigrationPathForResource));
			return migrationResources;
		}
		catch (Exception e) {
			throw new RuntimeException("Error reading migration xml files for project [" + moduleName + "].", e);
		}
	}


	public static Map<String, Action> populateActionMap(ApplicationContext context) {
		Map<String, Action> actionMap = new HashMap<>();
		DaoLocator daoLocator = (DaoLocator) context.getBean("daoLocator");
		Map<String, Object> serviceBeans = getServiceBeans(context);

		serviceBeans.forEach((serviceName, service) -> {
			String serviceClassName = service.getClass().getName();
			// loop through service methods to find DAO's based on getter method pattern
			for (Method method : service.getClass().getMethods()) {
				String dtoName = "";
				String daoMethodName = method.getName();
				if (daoMethodName.startsWith("get") && daoMethodName.endsWith("DAO")) {
					try {
						// found DAO - test basic methods
						dtoName = StringUtils.capitalize(daoMethodName.substring(3, daoMethodName.length() - 3));
						ReadOnlyDAO<IdentityObject> dao = daoLocator.locate(dtoName);
						// If the DAO is directly tied to an abstract class don't continue with basic DAO tests.
						if (dao.getConfiguration() == null ||
								Modifier.isAbstract(dao.getConfiguration().getBeanClass().getModifiers()) ||
								NoTableDAOConfig.class.isAssignableFrom(dao.getConfiguration().getClass())) {
							continue;
						}
						int beanPathIndex = serviceClassName.lastIndexOf(".");
						String beanName = beanPathIndex >= 0 ? serviceClassName.substring(beanPathIndex + 1, beanPathIndex + 2).toLowerCase() +
								serviceClassName.substring(beanPathIndex + 2).replace("Impl", "") : null;
						String methodName = "save" + dtoName;

						IdentityObject bean = dao.newBean();
						Method methodSave = MethodUtils.getMethod(service.getClass(), "save" + dtoName, bean.getClass());
						if (methodSave != null) {
							//Check the schemaTable for migration bean and method overrides and apply if they exist
							String serviceBeanName = dao.getConfiguration().getTable().getMigrationServiceBeanName();
							String serviceMethodName = dao.getConfiguration().getTable().getMigrationServiceMethodName();
							if (serviceBeanName != null && serviceMethodName != null) {
								beanName = serviceBeanName;
								methodName = serviceMethodName;
							}
							Action action = new Action();
							action.setBean(beanName);
							action.setMethod(methodName);
							actionMap.put(dao.getConfiguration().getTable().getDtoClass(), action);
						}
					}
					catch (Exception e) {
						LogUtils.warn(MigrationUtils.class, "Failed to create action for service: " + serviceClassName + "] and method [" + daoMethodName + "]\n\t" +
								"May need to import the projects context file or will be unable to save objects of type: " + dtoName + ": " + e.getMessage());
					}
				}
			}
		});
		//TODO - leaving below as an example - when there are abstract classes the existing functionality won't populate the actionMap with the
		//proper bean and method to save an item or create the migration action - I added this manually so I could export my notifications to
		//use as a JSON action for importing again.
//		Action action = new Action();
//		action.setBean("notificationDefinitionService");
//		action.setMethod("saveNotificationDefinitionBatch");
//		actionMap.put("com.clifton.notification.definition.NotificationDefinition", action);
		return actionMap;
	}


	private static Map<String, Object> getServiceBeans(ApplicationContext context) {
		Map<String, Object> result = new HashMap<>();
		String[] names = context.getBeanDefinitionNames();
		for (String name : names) {
			if (name.endsWith("Service")) {
				result.put(name, context.getBean(name));
			}
		}
		return result;
	}


	public static int getVersionForFileName(String fileName) {
		return new Integer(fileName.split("-")[0]);
	}


	public static String getMigrationPathForResource(Resource resource) {
		String resourceURI;
		try {
			resourceURI = resource.getURI().toString();
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
		return resourceURI.substring(resourceURI.lastIndexOf("schema/") + 7);
	}
}
