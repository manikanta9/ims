package com.clifton.core.dataaccess.search;


import org.hibernate.Criteria;


/**
 * The <code>SearchFormRestrictionConfigurer</code> interface can be implemented to allow additional restrictions (configuration) for searches.
 * For example, it can be used to add additional search restrictions from a different project that SearchForm project doesn't depend on.
 * For example, add "reconciled" restriction to TradeSearchForm from reconciliation project which depends on trade but not the other way around.
 *
 * @author vgomelsky
 */
public interface SearchFormRestrictionConfigurer {

	/**
	 * Returns the Class of the SearchForm that this configurer should apply to.
	 */
	public Class<?> getSearchFormClass();


	/**
	 * Returns the name of the search field used for restrictions
	 */
	public String getSearchFieldName();


	/**
	 * Configures the specified search criteria using the specified search field restriction.
	 */
	public void configureCriteria(Criteria criteria, SearchRestriction restriction);
}
