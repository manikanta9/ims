package com.clifton.core.dataaccess.search.restrictions;


import com.clifton.core.dataaccess.search.ComparisonConditions;

import java.util.List;


/**
 * The <code>EnumRestrictionDefinition</code> class defines restriction definitions for Enum properties.
 * Supports EQUALS and IN comparisons.
 *
 * @author vgomelsky
 */
public class EnumRestrictionDefinition<T extends Enum<T>> extends AbstractRestrictionDefinition {

	private final Class<T> enumClass;


	public EnumRestrictionDefinition(Class<T> enumClass, String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required) {
		this(enumClass, fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, new ComparisonConditions[]{ComparisonConditions.EQUALS, ComparisonConditions.IN});
	}


	public EnumRestrictionDefinition(Class<T> enumClass, String fieldName, String searchFieldName, String searchFieldPath, boolean leftJoin, String orderByFieldName, boolean required,
	                                 ComparisonConditions[] comparisonConditions) {
		super(fieldName, searchFieldName, searchFieldPath, leftJoin, orderByFieldName, required, comparisonConditions);
		this.enumClass = enumClass;
	}


	@Override
	@SuppressWarnings("unchecked")
	public Object getValue(Object rawValue) {
		if (rawValue instanceof List<?>) {
			// convert a List of String objects to the List of corresponding enums 
			List<Object> valueList = (List<Object>) rawValue;
			for (int i = 0; i < valueList.size(); i++) {
				Object value = valueList.get(i);
				if (value instanceof String) {
					// need to convert to corresponding enum
					valueList.set(i, Enum.valueOf(this.enumClass, value.toString()));
				}
			}
		}
		else if (rawValue instanceof String) {
			return Enum.valueOf(this.enumClass, rawValue.toString());
		}

		return rawValue;
	}
}
