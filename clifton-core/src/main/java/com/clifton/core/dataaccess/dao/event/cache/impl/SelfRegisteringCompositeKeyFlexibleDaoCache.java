package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoCompositeKeyFlexibleCache;


/**
 * The extension of {@link SelfRegisteringFlexibleDaoCache} for dual-key caches.
 *
 * @param <T>  the type of object which shall have its references cached
 * @param <U>  the type for the <i>order</i> property
 * @param <K1> the type of the first key on which entities shall be cached
 * @param <K2> the type of the second key on which entities shall be cached
 */
public abstract class SelfRegisteringCompositeKeyFlexibleDaoCache<T extends IdentityObject, U extends Comparable<U>, K1, K2> extends SelfRegisteringFlexibleDaoCache<T, U> implements DaoCompositeKeyFlexibleCache<T, U, K1, K2> {

	/**
	 * Returns the first key property path on which entities shall be cached.
	 */
	protected abstract String getBeanKey1Property();


	/**
	 * Returns the second key property path on which entities shall be cached.
	 */
	protected abstract String getBeanKey2Property();


	/**
	 * Returns the first key value for the given bean.
	 */
	protected abstract K1 getBeanKey1Value(T bean);


	/**
	 * Returns the second key value for the given bean.
	 */
	protected abstract K2 getBeanKey2Value(T bean);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public final T getBeanForKeyValues(ReadOnlyDAO<T> dao, U order, K1 keyValue1, K2 keyValue2) {
		return getBeanImpl(dao, false, order, keyValue1, keyValue2);
	}


	@Override
	public final T getBeanForKeyValuesStrict(ReadOnlyDAO<T> dao, U order, K1 keyValue1, K2 keyValue2) {
		return getBeanImpl(dao, true, order, keyValue1, keyValue2);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected final String[] getBeanKeyProperties() {
		return new String[]{getBeanKey1Property(), getBeanKey2Property()};
	}


	@Override
	protected final String getBeanKey(T bean) {
		return getBeanKeyForProperties(getBeanKey1Value(bean), getBeanKey2Value(bean));
	}
}
