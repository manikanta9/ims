package com.clifton.core.dataaccess.migrate.reader;


import com.clifton.core.dataaccess.migrate.schema.Schema;
import org.springframework.core.io.Resource;


/**
 * The <code>MigrationDefinitionReader</code> defines methods for reading database migrations.
 *
 * @author vgomelsky
 */
public interface MigrationDefinitionReader {

	/**
	 * Loads schema changes from the specified file. Uses currentSchema for dependency reference.
	 * Returns <code>Schema</code> object that represents only changes from the loaded file.
	 *
	 * @param currentSchema
	 * @param fileName
	 */
	public Schema loadMigration(Schema currentSchema, String fileName);


	public Schema loadMigration(Schema currentSchema, String fileName, String dataSourceName);


	/**
	 * Loads schema changes from the specified file. Uses currentSchema for dependency reference.
	 * Returns <code>Schema</code> object that represents only changes from the loaded file.
	 *
	 * @param currentSchema
	 * @param resource
	 */
	public Schema loadMigration(Schema currentSchema, Resource resource);


	public Schema loadMigration(Schema currentSchema, Resource resource, String dataSourceName);


	/**
	 * Applies the specified updates to current schema.
	 *
	 * @param currentSchema
	 * @param updates
	 */
	public void updateSchema(Schema currentSchema, Schema updates);
}
