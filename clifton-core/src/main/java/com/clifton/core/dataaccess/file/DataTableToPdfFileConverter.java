package com.clifton.core.dataaccess.file;


import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnStyle;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>DataTableToPdfFileConverter</code> converts a {@link DataTable} to
 * a PDF File
 *
 * @author manderson
 */
public class DataTableToPdfFileConverter implements DataTableToFileConverter {

	@Override
	public File convert(DataTableFileConfig from) {
		if (from == null || from.isBlank()) {
			return null;
		}
		File file = null;
		Document document = null;
		OutputStream os = null;
		boolean written = false;
		try {
			file = File.createTempFile("DataFile", ".pdf");
			file.deleteOnExit();

			float margin = 36;

			// Not sure the best way to handle this, but for now - if there are 4 or less columns
			// will use portrait view, else will use landscape view.
			if (from.getColumnList().size() <= 4) {
				document = new Document(PageSize.LETTER, margin, margin, margin, margin);
			}
			else {
				document = new Document(PageSize.LETTER.rotate(), margin, margin, margin, margin);
			}
			os = new FileOutputStream(file);

			PdfWriter.getInstance(document, os);
			document.open();

			PdfPTable table = createTable(from);
			table.setWidthPercentage(100);
			Map<String, Font> fontMap = new HashMap<>();
			for (DataColumn column : from.getColumnList()) {
				addCellHeaderValue(table, column, from, fontMap);
			}
			// Set the First Row as the Header Row so it's repeated if the data
			// extends past one page.
			table.setHeaderRows(1);
			DataTable dataTable = from.getDataTable();
			if (dataTable.getTotalRowCount() == 0) {
				addEmptyDataRow(table, from.getColumnList().size());
			}
			for (int j = 0; j < dataTable.getTotalRowCount(); j++) {
				DataRow row = dataTable.getRow(j);

				for (DataColumn column : from.getColumnList()) {
					addCellValue(table, row.getValue(column), column, from, fontMap);
				}
			}
			document.add(table);
			written = true;
		}
		catch (Exception e) {
			if (file != null) {
				file.delete();
			}
			throw new RuntimeException(e);
		}
		finally {
			// Can only close the document if it's open and has something written to it, otherwise
			// an exception is thrown
			if (document != null && document.isOpen() && written) {
				document.close();
			}
			FileUtils.close(os);
		}
		return file;
	}


	/**
	 * Creates a new cell in the table for the header of the given column.
	 *
	 * @param table
	 * @param column
	 */
	private void addCellHeaderValue(PdfPTable table, DataColumn column, DataTableFileConfig config, Map<String, Font> fontMap) {
		PdfPCell cell = new PdfPCell(new Phrase(column.getColumnName(), getFontForColumn(column, true, config, fontMap)));
		setCellStyle(cell, column, true, config);
		table.addCell(cell);
	}


	/**
	 * Creates a new cell in the table for a data record of the given column.
	 *
	 * @param table
	 * @param column
	 */
	private void addCellValue(PdfPTable table, Object value, DataColumn column, DataTableFileConfig config, Map<String, Font> fontMap) {
		String valueStr = "";

		if (value != null) {
			if (value instanceof BigDecimal) {
				String style = column.getStyle();
				DataColumnStyle dcs = new DataColumnStyle(style);
				DataTypes dt = dcs.getDataType() != null ? DataTypes.valueOf(dcs.getDataType()) : null;
				if (dt != null) {
					if (DataTypeNames.INTEGER == dt.getTypeName()) {
						valueStr = CoreMathUtils.formatNumberInteger((BigDecimal) value);
					}
					else if (DataTypes.MONEY == dt) {
						valueStr = CoreMathUtils.formatNumberMoney((BigDecimal) value);
					}
					else if (DataTypes.MONEY4 == dt) {
						valueStr = CoreMathUtils.formatNumber((BigDecimal) value, MathUtils.NUMBER_FORMAT_MONEY4);
					}
					else if (dt.isPercent()) {
						valueStr = CoreMathUtils.formatNumber((BigDecimal) value, MathUtils.NUMBER_FORMAT_PERCENT);
					}
					else {
						valueStr = CoreMathUtils.formatNumberDecimal((BigDecimal) value);
					}
				}
				else {
					valueStr = CoreMathUtils.formatNumberDecimal((BigDecimal) value);
				}
			}
			else if (value instanceof Date) {
				valueStr = DateUtils.fromDateShort((Date) value);
			}
			else {
				valueStr = value.toString();
			}
		}
		PdfPCell cell = new PdfPCell(new Phrase(valueStr, getFontForColumn(column, false, config, fontMap)));
		setCellStyle(cell, column, false, config);
		table.addCell(cell);
	}


	private void addEmptyDataRow(PdfPTable table, int colSpan) {
		int fontWeight = Font.BOLD;
		Font font = FontFactory.getFont(FontFactory.HELVETICA, 8, fontWeight, getColor("BLACK"));
		PdfPCell cell = new PdfPCell(new Phrase("NO DATA", font));
		cell.setHorizontalAlignment(Element.ALIGN_CENTER);
		cell.setNoWrap(true);
		cell.setColspan(colSpan);
		table.addCell(cell);
	}


	/**
	 * Returns the Font object created for the given column.  Headers
	 * are bold and don't allow wrapping.
	 *
	 * @param column
	 * @param header
	 */
	private Font getFontForColumn(DataColumn column, boolean header, DataTableFileConfig config, Map<String, Font> fontMap) {
		String styleString = config.getStyleString(column.getStyle(), header);
		Font font = fontMap.get(styleString);
		if (font == null) {
			DataColumnStyle styleObj = config.getDataColumnStyle(styleString);
			int fontWeight = Font.NORMAL;
			if (DataColumnStyle.FONT_WEIGHT_BOLD.equals(styleObj.getFontWeight())) {
				fontWeight = Font.BOLD;
			}
			font = FontFactory.getFont(FontFactory.HELVETICA, 8, fontWeight, getColor(styleObj.getFontColor()));
			fontMap.put(styleString, font);
		}
		return font;
	}


	/**
	 * Returns the Color value for the given color name
	 *
	 * @param colorName
	 */
	private BaseColor getColor(String colorName) {
		// Black is the default value, not necessary to look it up
		if (StringUtils.isEmpty(colorName) || "BLACK".equals(colorName)) {
			return null;
		}
		try {
			Field field = BaseColor.class.getField(colorName.toUpperCase());
			return (BaseColor) field.get(null);
		}
		catch (Exception e) {
			throw new ValidationException("Unable to determine color for [" + colorName + "]");
		}
	}


	/**
	 * Sets the Column's style to the given cell.  Headers do not allow wrapping
	 *
	 * @param cell
	 * @param column
	 * @param header
	 */
	private void setCellStyle(PdfPCell cell, DataColumn column, boolean header, DataTableFileConfig config) {
		DataColumnStyle style = config.getDataColumnStyle(config.getStyleString(column.getStyle(), header));
		cell.setHorizontalAlignment(Element.ALIGN_LEFT);
		if (DataColumnStyle.ALIGN_RIGHT.equals(style.getAlign())) {
			cell.setHorizontalAlignment(Element.ALIGN_RIGHT);
			cell.setNoWrap(true);
		}
		else if (DataColumnStyle.ALIGN_CENTER.equals(style.getAlign())) {
			cell.setHorizontalAlignment(Element.ALIGN_CENTER);
			cell.setNoWrap(true);
		}
		if (!StringUtils.isEmpty(style.getBackgroundColor())) {
			cell.setBackgroundColor(getColor(style.getBackgroundColor()));
		}
	}


	/**
	 * Determines the widths of each column, skipping hidden columns
	 * and creates and returns a new Table based upon the column sizes
	 */
	private PdfPTable createTable(DataTableFileConfig config) {
		List<Integer> widthList = new ArrayList<>();
		int total = 0;
		for (DataColumn column : CollectionUtils.getIterable(config.getColumnList())) {
			if (column.isHidden()) {
				continue;
			}
			int size = 10;
			DataColumnStyle style = config.getDataColumnStyle(column.getStyle());
			if (style.getDataType() != null) {
				DataTypes dataType = DataTypes.valueOf(style.getDataType());
				if (dataType != null) {
					DataTypeNames dataTypeName = dataType.getTypeName();

					if (DataTypeNames.INTEGER == dataTypeName || DataTypeNames.BOOLEAN == dataTypeName) {
						size = 10;
					}
					else if (DataTypeNames.DECIMAL == dataTypeName || DataTypeNames.DATE == dataTypeName) {
						size = 20;
					}
					else if (DataTypeNames.STRING == dataTypeName) {
						size = dataType.getLength() / 2;
						if (size > 100) {
							size = 100;
						}
					}
					else if (DataTypeNames.TEXT == dataTypeName) {
						size = 100;
					}
				}
			}

			widthList.add(size);
			total = total + size;
		}

		float[] actualWidth = new float[widthList.size()];
		for (int i = 0; i < widthList.size(); i++) {
			BigDecimal percentage = CoreMathUtils.getPercentValue(BigDecimal.valueOf(widthList.get(i)), BigDecimal.valueOf(total), true);
			actualWidth[i] = percentage.floatValue();
		}
		return new PdfPTable(actualWidth);
	}
}
