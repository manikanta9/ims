package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.util.compare.CompareUtils;
import org.hibernate.Hibernate;
import org.hibernate.HibernateException;
import org.hibernate.cfg.Environment;
import org.hibernate.engine.jdbc.LobCreator;
import org.hibernate.engine.jdbc.spi.JdbcServices;
import org.hibernate.engine.spi.SharedSessionContractImplementor;
import org.hibernate.type.descriptor.WrapperOptions;
import org.hibernate.type.descriptor.java.JavaTypeDescriptor;
import org.hibernate.type.descriptor.java.PrimitiveByteArrayTypeDescriptor;
import org.hibernate.type.descriptor.sql.SqlTypeDescriptor;
import org.hibernate.type.descriptor.sql.VarbinaryTypeDescriptor;
import org.hibernate.usertype.ParameterizedType;
import org.hibernate.usertype.UserVersionType;

import java.io.Serializable;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Properties;
import java.util.TimeZone;


public class BinaryUserType implements UserVersionType, ParameterizedType {

	private static final int[] SQL_TYPES = {Types.VARBINARY};
	private static final JavaTypeDescriptor<byte[]> JAVA_TYPE_DESCRIPTOR = PrimitiveByteArrayTypeDescriptor.INSTANCE;
	private static final SqlTypeDescriptor SQL_TYPE_DESCRIPTOR = VarbinaryTypeDescriptor.INSTANCE;


	@Override
	public void setParameterValues(@SuppressWarnings("unused") Properties parameters) {
		// empty
	}


	@Override
	public int[] sqlTypes() {
		return SQL_TYPES;
	}


	@Override
	public Class<?> returnedClass() {
		return byte[].class;
	}


	@Override
	public Object nullSafeGet(ResultSet resultSet, String[] names, SharedSessionContractImplementor session, @SuppressWarnings("unused") Object owner) throws HibernateException, SQLException {
		WrapperOptions options = getOptions(session);
		return remapSqlTypeDescriptor(options).getExtractor(JAVA_TYPE_DESCRIPTOR).extract(resultSet, names[0], options);
	}


	public void nullSafeSet(PreparedStatement preparedStatement, Object value, int index, SharedSessionContractImplementor session) throws HibernateException, SQLException {
		WrapperOptions options = getOptions(session);
		remapSqlTypeDescriptor(options).getBinder(JAVA_TYPE_DESCRIPTOR).bind(preparedStatement, (byte[]) value, index, options);
	}


	private SqlTypeDescriptor remapSqlTypeDescriptor(WrapperOptions options) {
		return options.remapSqlTypeDescriptor(SQL_TYPE_DESCRIPTOR);
	}


	private WrapperOptions getOptions(final SharedSessionContractImplementor session) {
		return new WrapperOptions() {

			@Override
			public boolean useStreamForLobBinding() {
				return Environment.useStreamsForBinary() || session.getFactory().getServiceRegistry().getService(JdbcServices.class).getDialect().useInputStreamToInsertBlob();
			}


			@Override
			public LobCreator getLobCreator() {
				return Hibernate.getLobCreator(session);
			}


			@Override
			public SqlTypeDescriptor remapSqlTypeDescriptor(@SuppressWarnings("hiding") SqlTypeDescriptor sqlTypeDescriptor) {
				final SqlTypeDescriptor remapped = sqlTypeDescriptor.canBeRemapped() ? session.getFactory().getDialect().remapSqlTypeDescriptor(sqlTypeDescriptor) : sqlTypeDescriptor;
				return remapped == null ? sqlTypeDescriptor : remapped;
			}


			@Override
			public TimeZone getJdbcTimeZone() {
				return TimeZone.getDefault();
			}
		};
	}


	@Override
	public Object deepCopy(Object value) throws HibernateException {
		return value;
	}


	@Override
	public boolean isMutable() {
		return false;
	}


	@Override
	public Object assemble(Serializable cached, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return cached;
	}


	@Override
	public Serializable disassemble(Object value) throws HibernateException {
		return (Serializable) value;
	}


	@Override
	public Object replace(Object original, @SuppressWarnings("unused") Object target, @SuppressWarnings("unused") Object owner) throws HibernateException {
		return original;
	}


	@Override
	public int hashCode(Object x) throws HibernateException {
		return x.hashCode();
	}


	@Override
	public boolean equals(Object x, Object y) throws HibernateException {
		return compare(x, y) == 0;
	}


	@Override
	public Object seed(@SuppressWarnings("unused") SharedSessionContractImplementor session) {
		// Note : simply returns null for seed() and next() as the only known
		// 		application of binary types for versioning is for use with the
		// 		TIMESTAMP datatype supported by Sybase and SQL Server, which
		// 		are completely db-generated values...
		return null;
	}


	@Override
	public Object next(Object current, @SuppressWarnings("unused") SharedSessionContractImplementor session) {
		return current;
	}


	@Override
	public int compare(Object o1, Object o2) {
		if (o1 instanceof byte[] && o2 instanceof byte[]) {
			return compare((byte[]) o1, (byte[]) o2);
		}
		return CompareUtils.compare(o1, o2);
	}


	private int compare(byte[] left, byte[] right) {
		for (int i = 0, j = 0; i < left.length && j < right.length; i++, j++) {
			int a = (left[i] & 0xff);
			int b = (right[j] & 0xff);
			if (a != b) {
				return a - b;
			}
		}
		return left.length - right.length;
	}
}
