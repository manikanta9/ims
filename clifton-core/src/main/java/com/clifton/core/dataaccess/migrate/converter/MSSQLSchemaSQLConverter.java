package com.clifton.core.dataaccess.migrate.converter;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.sql.Sql;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>MSSQLSchemaSQLConverter</code> ...
 *
 * @author manderson
 */
public class MSSQLSchemaSQLConverter implements MigrationSchemaConverter {

	private String runWith = MigrationSchemaConverterTypes.SQL.name();
	private Map<String, String> sqlReplacementMap;


	/**
	 * For table operation types that use UPDATE_SQL_ONLY there is some sql that needs to run after SQL
	 * this will auto add it to the end of the SQL - only appends if runWith is SQL
	 */
	private MigrationSchemaConverter ddlUpdateMigrationConverter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public MigrationSchemaConverterTypes getMigrationSchemaConverterType() {
		return MigrationSchemaConverterTypes.SQL;
	}


	@Override
	public String convert(Schema schema) {
		if (schema.getExcludeFromSql() != null && schema.getExcludeFromSql()) {
			return "";
		}
		StringBuilder sqlString = new StringBuilder();
		for (Sql sql : CollectionUtils.getIterable(schema.getSqlList(getRunWith()))) {
			if (StringUtils.isEmpty(sql.getFileName())) {
				for (String statement : CollectionUtils.getIterable(sql.getStatementList())) {
					sqlString.append(statement);
					sqlString.append(getMigrationSchemaConverterType().getBatchSeparator());
				}
			}
			else {
				ResourceLoader loader = new DefaultResourceLoader();
				Resource resource = loader.getResource(sql.getFileName());
				System.out.println("Reading: " + resource.getFilename());
				InputStream in = null;
				try {
					in = resource.getInputStream();
					BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
					String line;
					boolean inCommentBlock = false;
					while ((line = reader.readLine()) != null) {
						// Skip SQL Comments
						if (line.startsWith("/*")) {
							inCommentBlock = true;
						}
						if (!inCommentBlock && !line.startsWith("--")) {
							sqlString.append(line);
							sqlString.append(StringUtils.NEW_LINE);
						}
						if (inCommentBlock && line.endsWith("*/")) {
							inCommentBlock = false;
						}
					}
					sqlString.append(getMigrationSchemaConverterType().getBatchSeparator());
				}
				catch (Exception e) {
					throw new RuntimeException("Error reading SQL from sql file: " + sql.getFileName() + " [" + e.getMessage() + "]", e);
				}
				finally {
					FileUtils.close(in);
				}
			}
		}

		if (StringUtils.isEqual(MigrationSchemaConverterTypes.SQL.name(), getRunWith())) {
			sqlString.append(getDdlUpdateMigrationConverter().convert(schema));
		}

		String sql = sqlString.toString();
		if (!StringUtils.isEmpty(sql) && getSqlReplacementMap() != null) {
			for (Map.Entry<String, String> sqlEntry : getSqlReplacementMap().entrySet()) {
				sql = sql.replaceAll(sqlEntry.getKey(), sqlEntry.getValue());
			}
		}

		return sql;
	}


	public void setSqlReplacementString(String sqlReplacementString) {
		if (!StringUtils.isEmpty(sqlReplacementString)) {
			this.sqlReplacementMap = new HashMap<>();
			String[] replacements = sqlReplacementString.split(";");
			for (String replacement : replacements) {
				if (!StringUtils.isEmpty(replacement)) {
					String[] keyVal = replacement.split("=");
					if (keyVal.length != 2) {
						throw new RuntimeException("Invalid Replacement String [" + replacement + "] Syntax should be key=value");
					}
					this.sqlReplacementMap.put(keyVal[0], keyVal[1]);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String getRunWith() {
		return this.runWith;
	}


	public void setRunWith(String runWith) {
		this.runWith = runWith;
	}


	public Map<String, String> getSqlReplacementMap() {
		return this.sqlReplacementMap;
	}


	public MigrationSchemaConverter getDdlUpdateMigrationConverter() {
		return this.ddlUpdateMigrationConverter;
	}


	public void setDdlUpdateMigrationConverter(MigrationSchemaConverter ddlUpdateMigrationConverter) {
		this.ddlUpdateMigrationConverter = ddlUpdateMigrationConverter;
	}
}
