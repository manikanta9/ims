package com.clifton.core.dataaccess.dao.template;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.search.SearchConfigurer;

import java.util.List;


/**
 * The <code>TemplateReadOnlyDAO</code> interface adds generic search methods to Data Access Objects using Template based implementations
 *
 * @author stevenf
 */
public interface TemplateReadOnlyDAO<T extends IdentityObject, S extends SearchConfigurer<C>, C> extends ReadOnlyDAO<T> {

	public List<T> findByQuery(String query);


	public List<T> findBySearchCriteria(S searchConfigurer);
}
