package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>DaoSingleKeyCache</code> interface defines the methods to use with DAO caches that return an entity with one key value
 *
 * @author manderson
 */
public interface DaoCompositeKeyCache<T extends IdentityObject, K1, K2> {


	/**
	 * Returns the bean with the give key values.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 * Note: Does not enforce that the bean exists, use strict method to require it exists.
	 */
	public T getBeanForKeyValues(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2);


	/**
	 * Returns the bean with the give key value.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 *
	 * @throws ValidationException if the bean is missing
	 */
	public T getBeanForKeyValuesStrict(ReadOnlyDAO<T> dao, K1 keyPropertyValue1, K2 keyPropertyValue2);
}
