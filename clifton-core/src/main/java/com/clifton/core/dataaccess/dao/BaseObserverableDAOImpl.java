package com.clifton.core.dataaccess.dao;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.event.ObserverableDAOImpl;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;

import java.io.Serializable;


/**
 * @author vgomelsky
 */
public abstract class BaseObserverableDAOImpl<T extends IdentityObject> extends ObserverableDAOImpl<T> {

	private DAOConfiguration<T> configuration;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		if (this.configuration != null) {
			return this.configuration.toString();
		}
		return "[MISSING DAO CONFIGURATION]";
	}


	////////////////////////////////////////////////////////////////////////////


	public T newBean(String... requiredProperties) {
		T bean;
		try {
			bean = getConfiguration().getBeanClass().newInstance();
		}
		catch (Exception e) {
			if (ArrayUtils.getLength(requiredProperties) > 0) {
				for (Subclass subclass : CollectionUtils.getIterable(getConfiguration().getTable().getSubclassList())) {
					@SuppressWarnings("unchecked")
					Class<T> beanClass = (Class<T>) CoreClassUtils.getClass(subclass.getDtoClass());
					bean = BeanUtils.newInstance(beanClass);
					for (String requiredProperty : requiredProperties) {
						if (!BeanUtils.isPropertyPresent(beanClass, requiredProperty)) {
							bean = null;
							break; // move to next sub-class
						}
					}
					if (bean != null) {
						return bean;
					}
				}
			}
			throw new RuntimeException("Error instantiating DTO object for " + toString(), e);
		}
		return bean;
	}


	public Serializable convertToPrimaryKeyDataType(Serializable key) {
		return DaoUtils.convertToPrimaryKeyDataType(key, this.getConfiguration());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DAOConfiguration<T> getConfiguration() {
		return this.configuration;
	}


	public void setConfiguration(DAOConfiguration<T> configuration) {
		this.configuration = configuration;
	}
}
