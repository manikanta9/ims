package com.clifton.core.dataaccess.migrate.locator;

import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * @author mitchellf
 */
@Service
public class MigrationModuleVersionServiceImpl implements MigrationModuleVersionService {

	private AdvancedReadOnlyDAO<MigrationModuleVersion, Criteria> migrationModuleVersionDAO;


	////////////////////////////////////////////////////////////////////////////////
	//////////        Migration Module Version Business Methods           //////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<MigrationModuleVersion> getMigrationModuleVersionList(MigrationModuleVersionSearchForm searchForm) {
		HibernateSearchFormConfigurer searchConfigurer = new HibernateSearchFormConfigurer(searchForm);
		return getMigrationModuleVersionDAO().findBySearchCriteria(searchConfigurer);
	}

	////////////////////////////////////////////////////////////////
	/////////          Getter and Setter Methods          //////////
	////////////////////////////////////////////////////////////////


	public AdvancedReadOnlyDAO<MigrationModuleVersion, Criteria> getMigrationModuleVersionDAO() {
		return this.migrationModuleVersionDAO;
	}


	public void setMigrationModuleVersionDAO(AdvancedReadOnlyDAO<MigrationModuleVersion, Criteria> migrationModuleVersionDAO) {
		this.migrationModuleVersionDAO = migrationModuleVersionDAO;
	}
}
