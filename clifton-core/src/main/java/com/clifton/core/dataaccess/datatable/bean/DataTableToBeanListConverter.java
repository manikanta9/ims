package com.clifton.core.dataaccess.datatable.bean;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationException;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.BiConsumer;


/**
 * {@link DataTableToBeanListConverter} takes a data table and attempts to create a record for each row in the DataTable
 * by using the column name/bean property convention of BeanUtils.setProperty(column name) where the column name passed uses lower case for first letter
 * i.e. ColumnName = StartDate, would use BeanUtils.setProperty("startDate");
 * <p/>
 * Skips all system managed fields.  To see what column names are available, use getAvailableColumnNames method
 * <p/>
 * NOTE: THIS USES THE BEAN'S PROPERTIES ONLY AND DOESN'T PERFORM ANY DAO LOOK UPS SO THE CLASSES PASSED TO POPULATE DO NOT NEED TO BE IDENTITY OBJECTS
 *
 * @author manderson
 */
public class DataTableToBeanListConverter<T> implements Converter<DataTable, List<T>> {

	private final Class<T> clazz;
	/**
	 * The ability to do something with unmapped properties.
	 * i.e. Bean has a dynamic list of other fields - so put the unmapped column values in a map so the processing later can pull the values
	 */
	private final BiConsumer<T, Map<String, Object>> applyUnmappedColumnValueMapToBean;
	private Map<String, String> beanPropertyToColumnNameMap;
	private Map<String, String> columnNameToBeanPropertyMap;
	private Set<String> requiredBeanProperties;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DataTableToBeanListConverter(Class<T> clazz) {
		this(clazz, null);
	}


	public DataTableToBeanListConverter(Class<T> clazz, String[] requiredProperties) {
		this(clazz, requiredProperties, null);
	}


	public DataTableToBeanListConverter(Class<T> clazz, String[] requiredProperties, BiConsumer<T, Map<String, Object>> applyUnmappedColumnValueMapToBean) {
		super();
		this.clazz = clazz;
		this.applyUnmappedColumnValueMapToBean = applyUnmappedColumnValueMapToBean;
		setup(requiredProperties);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private void setup(String[] requiredProperties) {
		this.beanPropertyToColumnNameMap = new HashMap<>();
		this.columnNameToBeanPropertyMap = new HashMap<>();
		this.requiredBeanProperties = new HashSet<>();

		Field[] fields = ClassUtils.getClassFields(this.clazz, true, false);
		Set<String> fieldNames = new HashSet<>();
		for (Field field : fields) {
			if (!BeanUtils.isSystemManagedField(field.getName()) && !"id".equals(field.getName())) {
				String columnName = StringUtils.capitalize(field.getName());
				this.beanPropertyToColumnNameMap.put(field.getName(), columnName);
				this.columnNameToBeanPropertyMap.put(columnName.toLowerCase(), field.getName());
				fieldNames.add(field.getName());
			}
		}

		if (requiredProperties != null && requiredProperties.length > 0) {
			List<String> missingRequired = new ArrayList<>();
			for (String req : requiredProperties) {
				if (!fieldNames.contains(req)) {
					missingRequired.add(req);
				}
				this.requiredBeanProperties.add(req);
			}

			if (!CollectionUtils.isEmpty(missingRequired)) {
				throw new RuntimeException("Invalid Setup. The following properties are required but are not available: " + //
						"[" + StringUtils.collectionToCommaDelimitedString(missingRequired) + //
						"], Available Properties [" + StringUtils.collectionToCommaDelimitedString(this.beanPropertyToColumnNameMap.keySet()) + "]");
			}
		}
	}

	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////


	@Override
	public List<T> convert(DataTable table) {
		if (table == null || table.getTotalRowCount() == 0) {
			return null;
		}

		Map<Integer, String> unmappedColumnMap = new HashMap<>();
		Map<Integer, String> beanPropertyNameMap = getBeanPropertyNameMap(table.getColumnList(), unmappedColumnMap);

		List<T> beanList = new ArrayList<>();
		for (int i = 0; i < table.getTotalRowCount(); i++) {
			beanList.add(getBeanForRow(i, table.getRow(i), beanPropertyNameMap, unmappedColumnMap));
		}
		return beanList;
	}

	///////////////////////////////////////////////////////
	///////////////////////////////////////////////////////


	private T getBeanForRow(int rowIndex, DataRow row, Map<Integer, String> beanPropertyNameMap, Map<Integer, String> unmappedColumnMap) {
		T bean = BeanUtils.newInstance(this.clazz);
		for (Map.Entry<Integer, String> columnIndexEntry : beanPropertyNameMap.entrySet()) {
			String property = columnIndexEntry.getValue();
			Object value = row.getValue(columnIndexEntry.getKey());
			if (value == null || (value instanceof String && StringUtils.isEmpty((String) value))) {
				if (isRequired(property)) {
					throw new ValidationException("Missing Required Value for row # [" + (rowIndex + 1) + "] and Column [" + this.beanPropertyToColumnNameMap.get(property) + "]");
				}
			}
			else {
				BeanUtils.setPropertyValue(bean, property, row.getValue(columnIndexEntry.getKey()));
			}
		}
		if (this.applyUnmappedColumnValueMapToBean != null && unmappedColumnMap != null && !CollectionUtils.isEmpty(unmappedColumnMap)) {
			Map<String, Object> unmappedColumnValueMap = new HashMap<>();
			for (Map.Entry<Integer, String> columnIndexEntry : unmappedColumnMap.entrySet()) {
				Object value = row.getValue(columnIndexEntry.getKey());
				if (value != null) {
					unmappedColumnValueMap.put(columnIndexEntry.getValue(), value);
				}
			}
			this.applyUnmappedColumnValueMapToBean.accept(bean, unmappedColumnValueMap);
		}
		return bean;
	}


	private boolean isRequired(String propertyName) {
		return CollectionUtils.contains(this.requiredBeanProperties, propertyName);
	}


	/**
	 * Generates a map of column index to the bean property name to use for setting values
	 * Also validates all required properties exist in the DataTable
	 */
	private Map<Integer, String> getBeanPropertyNameMap(DataColumn[] columnList, Map<Integer, String> unmappedColumnMap) {
		Map<Integer, String> beanPropertyNameMap = new HashMap<>();

		for (int i = 0; i < columnList.length; i++) {
			String columnName = columnList[i].getColumnName() != null ? columnList[i].getColumnName().toLowerCase() : null;
			if (columnName != null) {
				if (this.columnNameToBeanPropertyMap.containsKey(columnName)) {
					beanPropertyNameMap.put(i, this.columnNameToBeanPropertyMap.get(columnName));
				}
				else {
					// If column contains a - or any spaces try to take them out and then match it
					String cleanColumnName = columnName.replaceAll("-", "").replaceAll(" ", "");
					if (this.columnNameToBeanPropertyMap.containsKey(cleanColumnName)) {
						beanPropertyNameMap.put(i, this.columnNameToBeanPropertyMap.get(cleanColumnName));
					}
					// Otherwise it's unmapped
					else {
						unmappedColumnMap.put(i, columnName);
					}
				}
			}
		}
		validateRequiredBeanPropertiesExist(beanPropertyNameMap);
		return beanPropertyNameMap;
	}


	private void validateRequiredBeanPropertiesExist(Map<Integer, String> beanPropertyNameMap) {
		if (!CollectionUtils.isEmpty(this.requiredBeanProperties)) {
			Collection<String> containsBeanProperties = beanPropertyNameMap.values();
			List<String> missingRequired = new ArrayList<>();
			for (String req : this.requiredBeanProperties) {
				if (!containsBeanProperties.contains(req)) {
					missingRequired.add(this.beanPropertyToColumnNameMap.get(req));
				}
			}

			if (!CollectionUtils.isEmpty(missingRequired)) {
				throw new ValidationException("Missing Required Columns: " + StringUtils.collectionToCommaDelimitedString(missingRequired));
			}
		}
	}
}
