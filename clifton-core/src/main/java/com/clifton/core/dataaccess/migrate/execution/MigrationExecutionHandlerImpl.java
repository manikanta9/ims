package com.clifton.core.dataaccess.migrate.execution;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.migrate.MigrationUtils;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverter;
import com.clifton.core.dataaccess.migrate.converter.MigrationSchemaConverterTypes;
import com.clifton.core.dataaccess.migrate.execution.action.MigrationExecutionActionHandler;
import com.clifton.core.dataaccess.migrate.execution.database.MigrationExecutionDatabaseHandler;
import com.clifton.core.dataaccess.migrate.locator.MigrationFileLocator;
import com.clifton.core.dataaccess.migrate.locator.MigrationModule;
import com.clifton.core.dataaccess.migrate.locator.MigrationModuleVersion;
import com.clifton.core.dataaccess.migrate.reader.MigrationDefinitionReader;
import com.clifton.core.dataaccess.migrate.schema.Schema;
import com.clifton.core.dataaccess.migrate.schema.action.Action;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.context.support.GenericApplicationContext;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.support.rowset.SqlRowSet;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.ResourceTransactionManager;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * The <code>MigrationExecutionHandlerImpl</code> class is a basic implementation of MigrationExecutionHandler.
 *
 * @author vgomelsky
 */
public class MigrationExecutionHandlerImpl implements MigrationExecutionHandler {

	private GenericApplicationContext springContext;
	private MigrationExecutionActionHandler migrationExecutionActionHandler;
	private MigrationExecutionDatabaseHandler migrationExecutionDatabaseHandler;
	private MigrationFileLocator migrationFileLocator;
	private MigrationDefinitionReader migrationDefinitionReader;
	private List<MigrationSchemaConverter> migrationSQLConverterList;
	private ResourceTransactionManager transactionManager;

	private Map<String, String> sqlReplacementMap;

	////////////////////////////////////////////////////////////////////////////
	private Map<String, Action> defaultActionMap;
	private List<MigrationModule> migrationModuleList;
	////////////////////////////////////////////////////////////////////////////


	//This pre-populates the moduleList by parsing the string passed from the context config
	public void setModuleListString(String str) {
		if (StringUtils.isEmpty(str)) {
			return;
		}
		if (this.migrationModuleList == null) {
			this.migrationModuleList = new ArrayList<>();
		}
		String[] modules = str.split(";");
		for (String moduleDesc : modules) {
			MigrationModule migrationModule = new MigrationModule();
			migrationModule.setModuleName(moduleDesc.split("=")[0]);
			String[] versionDataSourceString = moduleDesc.split("=")[1].split(":");
			migrationModule.setTargetedVersion(new Integer(versionDataSourceString[0]));
			if (versionDataSourceString.length == 2) {
				migrationModule.setDataSourceName(versionDataSourceString[1]);
			}
			this.migrationModuleList.add(migrationModule);
		}
	}

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void migrate(GenericApplicationContext springContext) {
		setSpringContext(springContext);
		executeMigrations(populateMigrationModuleList());
	}


	private List<MigrationModule> populateMigrationModuleList() {
		Schema schema = null;
		getMigrationExecutionDatabaseHandler().createEnvironmentSchema();
		getMigrationExecutionDatabaseHandler().createEnvironmentTable();
		getMigrationExecutionDatabaseHandler().createMigrationTable();
		for (MigrationModule migrationModule : getMigrationModuleList()) {
			getMigrationExecutionDatabaseHandler().getCurrentVersionsForModule(migrationModule);
		}
		getMigrationFileLocator().setMigrationModuleList(getMigrationModuleList());
		for (MigrationModule module : CollectionUtils.getIterable(getMigrationFileLocator().locateMigrations(MigrationFileLocator.MigrationFileLocatorVersionValidation.DUPLICATE_VERSIONS))) {
			for (Resource resource : CollectionUtils.getIterable(module.getMigrationResourceList())) {
				AssertUtils.assertNotNull(resource.getFilename(), "Filename is null for resource " + resource);
				int version = MigrationUtils.getVersionForFileName(resource.getFilename());
				Schema thisSchema = getMigrationDefinitionReader().loadMigration(schema, resource, module.getDataSourceName());
				if (schema == null) {
					schema = thisSchema;
				}
				else {
					getMigrationDefinitionReader().updateSchema(schema, thisSchema);
				}
				String migrationPath = MigrationUtils.getMigrationPathForResource(resource);
				MigrationModuleVersion moduleVersion = module.getExistingMigrationVersionMap().remove(migrationPath);
				if (moduleVersion != null) {
					// If all DDL, Data, Meta, & Actions have been run for this file, don't create a new Module Version object
					if (moduleVersion.isComplete()) {
						continue;
					}
				}
				else {
					moduleVersion = new MigrationModuleVersion();
					moduleVersion.setVersionNumber(version);
					moduleVersion.setMigrationPath(migrationPath);
				}
				Set<String> completedVersionsWithHigherVersions = module.getExistingMigrationVersionMap().tailMap(migrationPath).entrySet().stream()
						.filter(entry -> entry.getValue().isComplete())
						.map(Map.Entry::getKey)
						.collect(Collectors.toSet());
				if (!CollectionUtils.isEmpty(completedVersionsWithHigherVersions)) {
					LogUtils.warn(getClass(), "Migration version [" + migrationPath + "] has not been completely executed, but there are more recent versions that have been completely executed " + completedVersionsWithHigherVersions);
				}
				moduleVersion.setDoNotStartTransaction(thisSchema.isDoNotStartTransaction());

				String conditionalSql = thisSchema.getConditionalSql();
				if (!StringUtils.isEmpty(conditionalSql) && getSqlReplacementMap() != null) {
					for (Map.Entry<String, String> sqlEntry : getSqlReplacementMap().entrySet()) {
						conditionalSql = conditionalSql.replaceAll(sqlEntry.getKey(), sqlEntry.getValue());
					}
				}
				moduleVersion.setConditionalSql(conditionalSql);

				// If explicitly excluding SQL for the file, mark it as such, and don't add the converted sql for the file
				if (thisSchema.getExcludeFromSql() != null && thisSchema.getExcludeFromSql()) {
					moduleVersion.setExecuteVersion(false);
				}
				else {
					// Do not add SQL or Actions, if they've already been run
					for (MigrationSchemaConverter sqlConverter : getMigrationSQLConverterList()) {
						if (!moduleVersion.isStepCompletedForType(sqlConverter.getMigrationSchemaConverterType())) {
							String sql = sqlConverter.convert(thisSchema);
							moduleVersion.addMigrationSql(sqlConverter.getMigrationSchemaConverterType(), sql);
						}
					}
					moduleVersion.setActionList(thisSchema.getActionList());
					moduleVersion.setActionMap(thisSchema.getActionMap());
					module.addMigrationVersion(moduleVersion);
				}
			}
			AssertUtils.assertEmpty(module.getExistingMigrationVersionMap(), "The following migrations from the database no longer exist in the file system: " + module.getExistingMigrationVersionMap().keySet());
		}
		return getMigrationModuleList();
	}


	private void executeMigrations(List<MigrationModule> moduleList) {
		RuntimeException error = null;
		try {
			// First Run ALL PRE_DDL
			executeMigrationModules(moduleList, MigrationSchemaConverterTypes.PRE_DDL);
			// First Run ALL DDL
			executeMigrationModules(moduleList, MigrationSchemaConverterTypes.DDL);
			// Then Run ALL Meta Data
			executeMigrationModules(moduleList, MigrationSchemaConverterTypes.META_DATA);
			// Then Run ALL Data & Actions
			executeMigrationModules(moduleList, MigrationSchemaConverterTypes.DATA);
			// Then Run ALL After Data Actions
			executeMigrationModules(moduleList, MigrationSchemaConverterTypes.DATA_ACTION);
			// Finally Run ALL Sql
			executeMigrationModules(moduleList, MigrationSchemaConverterTypes.SQL);
		}
		catch (Exception e) {
			error = new RuntimeException("Error executing SQL Migrations with error [" + ExceptionUtils.getDetailedMessage(e) + "]", e);
		}
		finally {
			try {
				getMigrationExecutionDatabaseHandler().updateMigrationModuleVersions(moduleList);
			}
			catch (Exception e) {
				if (error == null) {
					// only update the error if a previous one has not been thrown to avoid hiding the root cause
					error = new RuntimeException("Error updating migrating versions [" + ExceptionUtils.getDetailedMessage(e) + "]", e);
				}
				else {
					LogUtils.error(getClass(), "Error occurred updating module migration versions. May be the result of a previous error.", e);
				}
			}
		}
		if (error != null) {
			throw error;
		}
	}


	private void executeMigrationModules(List<MigrationModule> moduleList, MigrationSchemaConverterTypes type) {
		for (MigrationModule module : CollectionUtils.getIterable(moduleList)) {
			// If DataSource Is NOT the default, then only need to run META Migrations.
			if (!"dataSource".equals(module.getDataSourceName()) && MigrationSchemaConverterTypes.META_DATA != type) {
				// Skip, Update to last version # and continue to next module
				module.markModuleCompleteForType(type);
				continue;
			}
			for (MigrationModuleVersion moduleVersion : module.getMigrationVersionList()) {
				try {
					// Ensure the Module Version hasn't already been run (Since versions are different between types)
					if (!moduleVersion.isStepCompletedForType(type)) {
						if (isExecuteVersion(module, moduleVersion, type)) {
							String sql = moduleVersion.getMigrationSql(type);
							if (!StringUtils.isEmpty(sql)) {
								long start = System.currentTimeMillis();
								executeInTransaction(sql, type.isRunInBatch(), type.getBatchSeparator(), moduleVersion.isDoNotStartTransaction());
								if (MigrationSchemaConverterTypes.SQL == type) {
									long seconds = (System.currentTimeMillis() - start) / 1000;
									if (seconds > 5) {
										LogUtils.info(getClass(), "Time to execute [" + module.getModuleName() + "] path [" + moduleVersion.getMigrationPath() + "] [" + seconds + "] seconds.");
									}
								}
							}
						}
						moduleVersion.markTypeComplete(type);
						// If running Data migrations, also run Action calls
						if (MigrationSchemaConverterTypes.DATA == type) {
							if (isExecuteVersion(module, moduleVersion, MigrationSchemaConverterTypes.ACTION)) {
								if (!moduleVersion.isStepCompletedForType(MigrationSchemaConverterTypes.ACTION)) {
									List<Action> actionList = moduleVersion.getActionList();
									if (!CollectionUtils.isEmpty(actionList)) {
										Map<String, Action> actionMap = getDefaultActionMap();
										Map<String, Action> actionMapOverrides = moduleVersion.getActionMap();
										if (actionMapOverrides != null) {
											actionMap.putAll(actionMapOverrides);
										}
										executeActions(actionList, actionMap, moduleVersion.isDoNotStartTransaction());
									}
								}
							}
							moduleVersion.markTypeComplete(MigrationSchemaConverterTypes.ACTION);
						}
					}
				}
				catch (Exception e) {
					throw new RuntimeException("Error executing [" + type + "] for module [" + module.getModuleName() + "] and path [" + moduleVersion.getMigrationPath() + "]", e);
				}
			}
		}
	}


	private boolean isExecuteVersion(MigrationModule module, MigrationModuleVersion version, MigrationSchemaConverterTypes type) {
		if (version.getExecuteVersion() != null) {
			return version.getExecuteVersion();
		}

		// PRE_DDL Type - Only evaluate Conditional SQL at this time if there is PRE_DDL sql
		// otherwise return false so it will actually be evaluated with DDL
		// Most cases we use DDL and compare if the field exists - so need to evaluate just before running actual DDL
		if (MigrationSchemaConverterTypes.PRE_DDL == type) {
			String preSql = version.getMigrationSql(type);
			if (StringUtils.isEmpty(preSql)) {
				return false;
			}
		}
		// Otherwise, evaluate and set the boolean for later use
		String sql = version.getConditionalSql();
		SqlRowSet rowSet = getMigrationExecutionDatabaseHandler().queryForRowSet(sql);
		if (rowSet == null || !rowSet.first()) {
			LogUtils.info(getClass(), "Skipping module [" + module.getModuleName() + "] and path [" + version.getMigrationPath() + "] because condition SQL did not return any results.");
			version.setExecuteVersion(false);
			version.setSkippedDueToConditionalSql(true);
		}
		else {
			LogUtils.info(getClass(), "Running module [" + module.getModuleName() + "] and path [" + version.getMigrationPath() + "] because condition SQL did return results.");
			version.setExecuteVersion(true);
		}
		return version.getExecuteVersion();
	}


	private void executeInTransaction(final String sql, boolean batch, String batchSeparator, boolean doNotStartTransaction) throws DataAccessException {
		if (StringUtils.isEmpty(sql)) {
			return;
		}
		final String[] sqlBatches = sql.split(batchSeparator);
		TransactionStatus status = null;
		if (!doNotStartTransaction) {
			status = getTransactionManager().getTransaction(new DefaultTransactionDefinition());
		}
		String errorSQL = null;
		try {
			if (batch) {
				getMigrationExecutionDatabaseHandler().batchUpdate(sqlBatches);
			}
			else {
				for (String s : sqlBatches) {
					errorSQL = s;
					getMigrationExecutionDatabaseHandler().execute(s);
				}
			}
		}
		catch (DataAccessException e) {
			String errorMsg = "ERROR SQL: " + StringUtils.coalesce(true, errorSQL, sql);
			if (!doNotStartTransaction) {
				getTransactionManager().rollback(status);
			}
			else {
				errorMsg = "[****WARNING: SQL was not run in a transaction, so a full rebuild may be necessary.****] " + errorMsg;
			}
			throw new RuntimeException(errorMsg, e);
		}
		if (!doNotStartTransaction) {
			getTransactionManager().commit(status);
		}
	}


	private void executeActions(List<Action> actionList, Map<String, Action> actionMap, boolean doNotStartTransaction) {
		TransactionStatus status = null;
		if (!doNotStartTransaction) {
			status = getTransactionManager().getTransaction(new DefaultTransactionDefinition());
		}
		try {
			getMigrationExecutionActionHandler().executeActions(actionList, actionMap, getSpringContext());
		}
		catch (Exception e) {
			if (status != null) {
				getTransactionManager().rollback(status);
			}
			throw new RuntimeException("Failed to execute actions: " + actionList, e);
		}
		if (status != null) {
			getTransactionManager().commit(status);
		}
	}


	@Override
	public String generateSQL() {
		StringBuilder preDdlSql = new StringBuilder();
		StringBuilder ddlSql = new StringBuilder();
		StringBuilder metaSql = new StringBuilder();
		StringBuilder dataSql = new StringBuilder();
		StringBuilder dataActionSql = new StringBuilder();
		StringBuilder sqlSql = new StringBuilder();

		for (MigrationModule module : CollectionUtils.getIterable(populateMigrationModuleList())) {
			for (MigrationModuleVersion moduleVersion : module.getMigrationVersionList()) {
				preDdlSql.append(moduleVersion.getMigrationSql(MigrationSchemaConverterTypes.PRE_DDL));
				ddlSql.append(moduleVersion.getMigrationSql(MigrationSchemaConverterTypes.DDL));
				metaSql.append(moduleVersion.getMigrationSql(MigrationSchemaConverterTypes.META_DATA));
				dataSql.append(moduleVersion.getMigrationSql(MigrationSchemaConverterTypes.DATA));
				dataActionSql.append(moduleVersion.getMigrationSql(MigrationSchemaConverterTypes.DATA_ACTION));
				sqlSql.append(moduleVersion.getMigrationSql(MigrationSchemaConverterTypes.SQL));
			}
		}
		return preDdlSql.toString() + ddlSql.toString() + metaSql.toString() + dataSql.toString() + dataActionSql.toString() + sqlSql.toString();
	}


	@Override
	public void executeSQLFile(String sqlFileName) {
		ResourceLoader loader = new DefaultResourceLoader();
		Resource resource = loader.getResource(sqlFileName);
		LogUtils.info(getClass(), "Executing Additional SQL File: " + resource.getFilename());
		InputStream in = null;
		StringBuilder sqlString = new StringBuilder();
		try {
			in = resource.getInputStream();
			BufferedReader reader = new BufferedReader(new InputStreamReader(in, StandardCharsets.UTF_8));
			String line;
			boolean inCommentBlock = false;
			while ((line = reader.readLine()) != null) {
				// Skip SQL Comments
				if (line.startsWith("/*")) {
					inCommentBlock = true;
				}
				if (!inCommentBlock && !line.startsWith("--")) {
					sqlString.append(line);
					sqlString.append(StringUtils.NEW_LINE);
				}
				if (inCommentBlock && line.endsWith("*/")) {
					inCommentBlock = false;
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Error reading SQL from sql file: " + sqlFileName + " [" + e.getMessage() + "]", e);
		}
		finally {
			FileUtils.close(in);
		}
		getMigrationExecutionDatabaseHandler().execute(sqlString.toString());
	}


	@Override
	public void executeSQLFiles(List<String> sqlFileNames) {
		for (String sqlFileName : CollectionUtils.getIterable(sqlFileNames)) {
			if (!StringUtils.isEmpty(sqlFileName)) {
				executeSQLFile(sqlFileName);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setSqlReplacementString(String sqlReplacementString) {
		if (!StringUtils.isEmpty(sqlReplacementString)) {
			this.sqlReplacementMap = new HashMap<>();
			String[] replacements = sqlReplacementString.split(";");
			for (String replacement : replacements) {
				if (!StringUtils.isEmpty(replacement)) {
					String[] keyVal = replacement.split("=");
					if (keyVal.length != 2) {
						throw new RuntimeException("Invalid Replacement String [" + replacement + "] Syntax should be key=value");
					}
					this.sqlReplacementMap.put(keyVal[0], keyVal[1]);
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	@ValueIgnoringGetter
	public Map<String, Action> getDefaultActionMap() {
		if (this.defaultActionMap == null) {
			this.defaultActionMap = MigrationUtils.populateActionMap(getSpringContext());
		}
		return this.defaultActionMap;
	}


	public void setDefaultActionMap(Map<String, Action> defaultActionMap) {
		this.defaultActionMap = defaultActionMap;
	}


	public GenericApplicationContext getSpringContext() {
		return this.springContext;
	}


	public void setSpringContext(GenericApplicationContext springContext) {
		this.springContext = springContext;
	}


	public ResourceTransactionManager getTransactionManager() {
		return this.transactionManager;
	}


	public void setTransactionManager(ResourceTransactionManager transactionManager) {
		this.transactionManager = transactionManager;
	}


	public MigrationFileLocator getMigrationFileLocator() {
		return this.migrationFileLocator;
	}


	public void setMigrationFileLocator(MigrationFileLocator migrationFileLocator) {
		this.migrationFileLocator = migrationFileLocator;
	}


	public MigrationDefinitionReader getMigrationDefinitionReader() {
		return this.migrationDefinitionReader;
	}


	public void setMigrationDefinitionReader(MigrationDefinitionReader migrationDefinitionReader) {
		this.migrationDefinitionReader = migrationDefinitionReader;
	}


	public List<MigrationSchemaConverter> getMigrationSQLConverterList() {
		return this.migrationSQLConverterList;
	}


	public void setMigrationSQLConverterList(List<MigrationSchemaConverter> migrationSQLConverterList) {
		this.migrationSQLConverterList = migrationSQLConverterList;
	}


	public List<MigrationModule> getMigrationModuleList() {
		return this.migrationModuleList;
	}


	public void setMigrationModuleList(List<MigrationModule> migrationModuleList) {
		this.migrationModuleList = migrationModuleList;
	}


	public MigrationExecutionActionHandler getMigrationExecutionActionHandler() {
		return this.migrationExecutionActionHandler;
	}


	public void setMigrationExecutionActionHandler(MigrationExecutionActionHandler migrationExecutionActionHandler) {
		this.migrationExecutionActionHandler = migrationExecutionActionHandler;
	}


	public MigrationExecutionDatabaseHandler getMigrationExecutionDatabaseHandler() {
		return this.migrationExecutionDatabaseHandler;
	}


	public void setMigrationExecutionDatabaseHandler(MigrationExecutionDatabaseHandler migrationExecutionDatabaseHandler) {
		this.migrationExecutionDatabaseHandler = migrationExecutionDatabaseHandler;
	}


	public Map<String, String> getSqlReplacementMap() {
		return this.sqlReplacementMap;
	}
}
