package com.clifton.core.dataaccess.search;


/**
 * The <code>OrderByField</code> class defines SQL statement ORDER BY field to be used for sorting.
 *
 * @author vgomelsky
 */
public class OrderByField {

	private final String name;
	private final OrderByDirections direction;


	/**
	 * Creates a new OrderByField object with the specified name and ascending sort direction.
	 *
	 * @param name
	 */
	public OrderByField(String name) {
		this(name, OrderByDirections.ASC);
	}


	/**
	 * Creates a new order by field with the specified name and direction.
	 *
	 * @param name
	 * @param direction
	 */
	public OrderByField(String name, OrderByDirections direction) {
		this.name = name;
		this.direction = direction;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @return the direction
	 */
	public OrderByDirections getDirection() {
		return this.direction;
	}
}
