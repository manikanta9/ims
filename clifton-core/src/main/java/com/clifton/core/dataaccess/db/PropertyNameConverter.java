package com.clifton.core.dataaccess.db;


import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;

import java.beans.Introspector;
import java.beans.PropertyDescriptor;
import java.util.Map;


/**
 * The <code>PropertyNameConverter</code> converts DB column name to corresponding Java Bean property name.
 *
 * @author vgomelsky
 */
public class PropertyNameConverter implements Converter<String, String> {

	private final String tableName;
	private final Map<String, PropertyDescriptor> beanProperties;


	public PropertyNameConverter(Map<String, PropertyDescriptor> beanProperties, String tableName) {
		this.beanProperties = beanProperties;
		this.tableName = tableName;
	}


	@Override
	public String convert(String from) {
		// need to find a matching property based on column name
		String tentativePropertyName = from;
		if (tentativePropertyName.startsWith("Is") && this.beanProperties.containsKey(Introspector.decapitalize(tentativePropertyName.substring(2)))) {
			return Introspector.decapitalize(tentativePropertyName.substring(2));
		}

		if (tentativePropertyName.equals(this.tableName + "ID")) {
			if (!this.beanProperties.containsKey("id")) {
				throw new RuntimeException("Cannot find 'id' property on DTO for table: " + this.tableName);
			}
			return "id";
		}

		if (tentativePropertyName.equals("Parent" + this.tableName + "ID")) {
			if (this.beanProperties.containsKey("parentId")) {
				return "parentId";
			}
			if (this.beanProperties.containsKey("parent")) {
				return "parent";
			}
		}

		// change ID suffix to Id (DB vs bean property name conventions)
		boolean foreignKey = (tentativePropertyName.length() > 2 && "ID".equals(tentativePropertyName.substring(tentativePropertyName.length() - 2)));
		if (foreignKey) {
			tentativePropertyName = tentativePropertyName.substring(0, tentativePropertyName.length() - 1) + 'd';
		}
		// keep stripping the first word of the property and looking for it
		while (tentativePropertyName != null) {
			tentativePropertyName = Introspector.decapitalize(tentativePropertyName);
			if (!"id".equals(tentativePropertyName) && this.beanProperties.containsKey(tentativePropertyName)) {
				break;
			}
			tentativePropertyName = StringUtils.removeLeadingWord(tentativePropertyName);
		}

		// try to match to corresponding relationship bean
		if (tentativePropertyName == null && foreignKey) {
			tentativePropertyName = from.substring(0, from.length() - 2);
			while (tentativePropertyName != null) {
				tentativePropertyName = Introspector.decapitalize(tentativePropertyName);
				if (this.beanProperties.containsKey(tentativePropertyName)) {
					break;
				}
				tentativePropertyName = StringUtils.removeLeadingWord(tentativePropertyName);
			}
		}

		if (tentativePropertyName != null) {
			return tentativePropertyName;
		}

		// check if there's ONE property that matches the field ignoring case
		// For FKs - can match on both with or without Id suffix  Some cases ending in ID is not a real FK (i.e. SourceFKFieldId = sourceFkFieldId)
		String fromWithoutId = (foreignKey ? from.substring(0, from.length() - 2) : from);
		for (String propertyName : this.beanProperties.keySet()) {
			if (propertyName.equalsIgnoreCase(fromWithoutId) || propertyName.equalsIgnoreCase(from)) {
				if (tentativePropertyName != null) {
					// second matching property found
					tentativePropertyName = null;
					break;
				}
				tentativePropertyName = propertyName;
			}
		}
		if (tentativePropertyName != null) {
			return tentativePropertyName;
		}

		throw new IllegalArgumentException("Cannot map DB field '" + this.tableName + "." + from + "' to a property: " + this.beanProperties);
	}
}
