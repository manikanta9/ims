package com.clifton.core.dataaccess.dao;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>OneToManyEntity</code> annotation marks DTO fields as being one to many database relationships.
 * These are typically collections such as properties that are not loaded by default by Hibernate.
 * The annotation allows custom handling/loading of these properties.
 * For example, when calling the 'getIdentityObjectAsJson' method we want the entity to be fully hydrated so it can be properly serialized to JSON.
 *
 * @author stevenf
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface OneToManyEntity {

	/**
	 * Spring managed service bean name that has a method to retrieve corresponding many-side data.
	 */
	String serviceBeanName() default "";


	/**
	 * Service method name that retrieves corresponding many-side data.
	 * The method must take only one argument which is the primary key of the DTO that has this annotation (one-side entity).
	 */
	String serviceMethodName() default "";


	/**
	 * Setting this property to false will allow for JSON migration actions to traverse and populate these items when they are
	 * encountered rather than delaying their processing/save.  In some cases, such as 'SystemBean' where the property list is saved
	 * when the bean is saved this is necessary to have the list walked and its object properties saved before saving the bean or it will fail;
	 * in most cases you want to delay the processing because the child object holds a reference to the parent object and will fail to save if
	 * the parent has not yet been completed.
	 */
	boolean delayed() default true;
}
