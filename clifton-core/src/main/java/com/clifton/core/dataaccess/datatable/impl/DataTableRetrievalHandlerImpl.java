package com.clifton.core.dataaccess.datatable.impl;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandler;
import com.clifton.core.dataaccess.search.SearchConfigurer;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlParameterValue;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.converter.Converter;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.SqlParameter;

import java.sql.ResultSet;
import java.util.List;
import java.util.Map;


/**
 * @author vgomelsky
 */
public class DataTableRetrievalHandlerImpl implements DataTableRetrievalHandler {

	private SqlHandler sqlHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable findDataTable(SearchConfigurer<SqlSelectCommand> searchConfigurer) {
		SqlSelectCommand command = new SqlSelectCommand();
		searchConfigurer.configureCriteria(command);
		searchConfigurer.configureOrderBy(command);
		return findDataTable(command, null);
	}


	@Override
	public DataTable findDataTable(SqlSelectCommand command) {
		return findDataTable(command, null);
	}


	@Override
	public DataTable findDataTable(SqlSelectCommand command, Map<String, Converter<Object, Object>> columnConverterMap) {
		ResultSetExtractor<DataTable> resultSetExtractor = new DataTableResultSetExtractor(command.getStart(), command.getLimit(), columnConverterMap);
		String sql = command.getSql();
		LogUtils.info(getClass(), "SQL: " + command.getSqlWithParameters());
		List<SqlParameterValue> parameterValueList = command.getSqlParameterValues();
		Object[] parameterValues = BeanUtils.getPropertyValues(parameterValueList, SqlParameterValue::getValue);
		try {
			if (!CollectionUtils.isEmpty(parameterValueList) || command.isPagingDefined()) {
				PreparedStatementCreatorFactory statementFactory = new PreparedStatementCreatorFactory(sql);
				if (!CollectionUtils.isEmpty(parameterValueList)) {
					for (SqlParameterValue parameterValue : parameterValueList) {
						statementFactory.addParameter(new SqlParameter(parameterValue.getSqlType()));
					}
				}
				// create statement factory to be able to override result set scroll type
				if (command.isPagingDefined()) {
					statementFactory.setResultSetType(ResultSet.TYPE_SCROLL_INSENSITIVE);
				}
				PreparedStatementCreator statementCreator = statementFactory.newPreparedStatementCreator(parameterValues);
				return getSqlHandler().executeSelect(statementCreator, resultSetExtractor, command.getTimeout());
			}
			SqlSelectCommand sqlSelectCommand = new SqlSelectCommand(sql);
			sqlSelectCommand.setTimeout(command.getTimeout());
			return getSqlHandler().executeSelect(sqlSelectCommand, resultSetExtractor);
		}
		// If an exception happens during query - and the sql sets no count on (used by query exports that use temp tables)
		// Then we need to make sure no count is turned off or it could affect subsequent database calls (i.e. inserting query run history record))
		catch (Throwable e) {
			try {
				if (sql.contains(SqlSelectCommand.SET_NO_COUNT_ON)) {
					getSqlHandler().executeUpdate(SqlSelectCommand.SET_NO_COUNT_OFF);
				}
			}
			catch (Throwable e2) {
				LogUtils.error(getClass(), "Error trying to run " + SqlSelectCommand.SET_NO_COUNT_OFF + " after exception found.", e2);
			}
			throw e;
		}
	}


	@Override
	public DataTable findDataTable(String sql) {
		return findDataTable(new SqlSelectCommand(sql), null);
	}


	@Override
	public DataTable findDataTable(String sql, Map<String, Converter<Object, Object>> columnConverterMap) {
		return findDataTable(new SqlSelectCommand(sql), columnConverterMap);
	}


	@Override
	public DataTable findDataTable(String sql, SqlParameterValue[] sqlParameterValues, int firstIndex, int maxSize) {
		return findDataTable(new SqlSelectCommand(sql, sqlParameterValues, firstIndex, maxSize), null);
	}


	@Override
	public DataTable findDataTable(String sql, SqlParameterValue[] sqlParameterValues, int firstIndex, int maxSize, int timeout) {
		return findDataTable(new SqlSelectCommand(sql, sqlParameterValues, firstIndex, maxSize, timeout), null);
	}


	@Override
	public DataTable findDataTable(String sql, SqlParameterValue[] sqlParameterValues, int firstIndex, int maxSize, Map<String, Converter<Object, Object>> columnConverterMap) {
		return findDataTable(new SqlSelectCommand(sql, sqlParameterValues, firstIndex, maxSize), columnConverterMap);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}
}
