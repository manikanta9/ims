package com.clifton.core.dataaccess.migrate.schema.dml;


import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;


/**
 * The <code>ValueToSQLConverter</code> class converts data cell values to corresponding SQL field representation.
 *
 * @author vgomelsky
 */
public class ValueToSQLConverter implements Converter<Value, String> {

	@Override
	public String convert(Value from) {
		DataTypes dataType = from.getDataType();
		String value = from.getValue();
		if (dataType == null) {
			throw new IllegalArgumentException("Value.dataType property cannot be null for column: " + from.getColumn() + " and value: " + value);
		}
		if (from.isSelect()) {
			if (DataTypes.INT == dataType) {
				// If it's an INT, coalesce it with text so we'll know it's missing
				return "(SELECT COALESCE(" + value + ", 'Cannot Find Record for SQL: [" + value.replaceAll("'", "") + "]'))";
			}
			return value;
		}
		if (DataTypes.BIT == dataType) {
			if ("true".equalsIgnoreCase(value) || "yes".equalsIgnoreCase(value)) {
				return "1";
			}
			if ("false".equalsIgnoreCase(value) || "no".equalsIgnoreCase(value)) {
				return "0";
			}
			throw new IllegalArgumentException("Illegal boolean value '" + value + "'.");
		}
		else if (DataTypes.COMPRESSED_TEXT == dataType) {
			//The 'N' forces the value to be of nvarchar
			return "COMPRESS(" + StringUtils.toSQL(value) + ")";
		}
		if (dataType.getLength() > 0 || DataTypes.DATE == dataType || DataTypes.DATETIME == dataType || DataTypes.TEXT == dataType) {
			return StringUtils.toSQL(value);
		}
		return value;
	}
}
