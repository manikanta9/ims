package com.clifton.core.dataaccess.dao.locator;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;

import java.lang.reflect.ParameterizedType;
import java.util.Collection;


/**
 * The <code>DaoLocator</code> interface defines methods for retrieving DAO instances from application context.
 *
 * @author vgomelsky
 */
public interface DaoLocator {

	/**
	 * Returns application context managed DAO instance that manages DTO objects of the specified type.
	 * <p>
	 * If the DAO cannot be found and suppressNotFoundError is true, the error will be logged instead of throwing {@link IllegalArgumentException}.
	 */
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(Class<T> dtoClass, boolean suppressNotFoundError);


	/**
	 * Returns application context managed DAO instance that manages DTO objects of the specified type.
	 *
	 * @throws IllegalArgumentException when the DAO cannot be found
	 */
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(Class<T> dtoClass);


	/**
	 * Returns application context managed DAO instance that manages DTO object
	 */
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(T dto);


	/**
	 * Returns application context managed DAO instance that manages DTO objects
	 * of the specified type.
	 */
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(ParameterizedType dtoType);


	/**
	 * Returns application context managed DAO instance that manages the specified table.
	 */
	public <T extends IdentityObject> ReadOnlyDAO<T> locate(String tableName);


	/**
	 * Returns a Collection of all DAO instances defined in the system.
	 */
	public Collection<ReadOnlyDAO<?>> locateAll();


	/**
	 * Returns a Collection of all DAO instances that have DTO's that are instances of the specified argument.
	 */
	public <T> Collection<ReadOnlyDAO<? extends T>> locateAll(Class<T> dtoInstanceOfClass);
}
