package com.clifton.core.dataaccess.dao.event.cache.impl;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.dataaccess.dao.event.cache.DaoCustomKeyListCache;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.ArrayUtils;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import static com.clifton.core.beans.BeanUtils.getPropertyValues;


/**
 * Generic version of a cache which stores lists of IdentityObject
 *
 * @param <K> Key class type
 * @param <C> Class type of cached objects
 * @author davidi, MarkW
 */
public abstract class SelfRegisteringCustomKeyDaoListCache<K, C extends IdentityObject> extends SelfRegisteringDaoObserver<C> implements DaoCustomKeyListCache<K, C>, CustomCache<K, Serializable[]> {

	private CacheHandler<K, Serializable[]> cacheHandler;

	private DaoLocator daoLocator;

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	protected abstract Class<C> getDtoClass();


	protected ReadOnlyDAO<C> getDao() {
		return getDaoLocator().locate(getDtoClass());
	}


	@Override
	public String getCacheName() {
		return getClass().getName();
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	/**
	 * Gets a list of entities of type 'C' from the cache, based on a key derived from the list of IdentityObjects
	 */
	@Override
	@Transactional(readOnly = true) // if called outside of transaction avoid expensive transaction creation for each findByPrimaryKey call
	public List<C> computeIfAbsent(K key, Function<K, List<C>> lookUpMethod) {
		Serializable[] idList = getCacheHandler().get(getCacheName(), key);
		if (!ArrayUtils.isEmpty(idList)) {
			List<C> beanList = new ArrayList<>();
			for (Serializable id : idList) {
				C bean = getDao().findByPrimaryKey(id);
				// If bean is missing, then the transaction was rolled back - retrieve the list again from the database and reset cache
				if (bean == null) {
					idList = null;
					break;
				}
				else {
					beanList.add(bean);
				}
			}
			if (idList != null) {
				return beanList;
			}
		}

		if (idList == null) {
			List<C> beanList = lookUpMethod.apply(key);
			if (beanList == null) {
				beanList = new ArrayList<>();
			}
			setBeanList(key, beanList);
			return beanList;
		}
		return null;
	}


	///////////////////////////////////////////////////////////////////////


	/**
	 * Adds the list of entities to be cached to the cache using the specified key.
	 */
	public void setBeanList(K key, List<C> beanList) {
		if (beanList == null) {
			beanList = new ArrayList<>();
		}
		getCacheHandler().put(getCacheName(), key, getPropertyValues(beanList, C::getIdentity, Serializable.class));
	}


	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	/**
	 * Override of observer method used to clear the cache if data in an observed table is changed.
	 */
	@Override
	public void afterMethodCallImpl(ReadOnlyDAO<C> dao, DaoEventTypes event, C bean, Throwable e) {
		if (e == null) {
			getCacheHandler().clear(getCacheName());
		}
	}


	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<K, Serializable[]> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<K, Serializable[]> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
