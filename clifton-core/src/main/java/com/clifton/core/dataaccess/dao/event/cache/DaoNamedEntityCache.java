package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.NamedObject;


/**
 * The <code>DaoNamedEntityCache</code> interface is an extension of the DaoSingleKeyCache for when we use Cache By Name.
 * Since this is the most common cache we have, prevents us from having to set String as the Key object type.
 *
 * @author manderson
 */
public interface DaoNamedEntityCache<T extends NamedObject> extends DaoSingleKeyCache<T, String> {

	public void put(String key, T bean);
}
