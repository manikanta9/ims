package com.clifton.core.dataaccess.migrate.converter;


/**
 * The <code>MigrationSchemaConverterType</code> enumeration defines supported converter types.
 *
 * @author manderson
 */
public enum MigrationSchemaConverterTypes {

	// PRE_DDL IS PRE-PENDED TO DDL SO THAT CAN ADD PRE_DDL IN IMS PROJECT RUN BEFORE DDL IN ANOTHER PROJECT
	PRE_DDL(true), DDL(true), META_DATA(true), DATA, ACTION, DATA_ACTION(true), HIBERNATE, SQL("\r\nGO\r\n", false);

	private final String batchSeparator;
	private final boolean runInBatch;
	/**
	 * Indicates if use as sql runWith tag is allowed
	 * Currently supported only for PRE_DDL, DDL, and META_DATA, DATA_ACTION
	 */
	private final boolean runWithAllowed;


	MigrationSchemaConverterTypes() {
		this(false);
	}


	MigrationSchemaConverterTypes(boolean runWithAllowed) {
		this(runWithAllowed, ";\n\n", true);
	}


	MigrationSchemaConverterTypes(String batchSeparator, boolean runInBatch) {
		this(false, batchSeparator, runInBatch);
	}


	MigrationSchemaConverterTypes(boolean runWithAllowed, String batchSeparator, boolean runInBatch) {
		this.runWithAllowed = runWithAllowed;
		this.batchSeparator = batchSeparator;
		this.runInBatch = runInBatch;
	}


	public String getBatchSeparator() {
		return this.batchSeparator;
	}


	public boolean isRunInBatch() {
		return this.runInBatch;
	}


	public boolean isRunWithAllowed() {
		return this.runWithAllowed;
	}
}
