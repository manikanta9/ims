package com.clifton.core.dataaccess.dao.hibernate;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.util.BooleanUtils;
import org.springframework.dao.DataAccessException;
import org.springframework.ui.ModelMap;
import org.springframework.web.context.request.WebRequest;

import java.util.Set;


/**
 * The <code>OpenSessionInViewInterceptor</code> class enables Spring's implementation of Hibernate's open session in view
 * only if corresponding WebRequest parameter is set (default parameter name: enableOpenSessionInView=true).
 *
 * @author vgomelsky
 */
public class OpenSessionInViewInterceptor extends org.springframework.orm.hibernate5.support.OpenSessionInViewInterceptor {

	/**
	 * If request parameter with this name is set to true, enable opens session in view.
	 */
	private String enableOpenSessionInViewParameter = "enableOpenSessionInView";
	/**
	 * If request URL ('trade.json', 'accountingTransaction.json', etc.) belongs to this set,
	 * then set open session in view for this request.
	 */
	private Set<String> enableOpenSessionInViewURLs;

	/**
	 * When this object is used for a request, we want to register it with the context for other services within the transaction to use.
	 * One particular case is when binding the object on an update. IMS will evict the loaded entities to avoid premature database flushes.
	 */
	private ContextHandler contextHandler;


	/**
	 * Returns true if Hibernate session should be open in view.
	 *
	 * @param request
	 */
	private boolean isOpenSessionInViewRequested(WebRequest request) {
		if (request != null) {
			if ("true".equals(request.getParameter(getEnableOpenSessionInViewParameter()))) {
				return true;
			}
			if (getEnableOpenSessionInViewURLs() != null) {
				// Get URL from (Example: uri=/app-clifton-ims/trade.json)
				String url = request.getDescription(false);
				int start = url.lastIndexOf('/');
				if (start > 0) {
					url = url.substring(start + 1);
				}
				return getEnableOpenSessionInViewURLs().contains(url);
			}
		}
		return false;
	}


	@Override
	public void preHandle(WebRequest request) throws DataAccessException {
		if (isOpenSessionInViewRequested(request)) {
			getContextHandler().setBean(Context.OPEN_SESSION_IN_VIEW_FLAG_NAME, Boolean.TRUE);
			super.preHandle(request);
		}
	}


	@Override
	public void postHandle(WebRequest request, ModelMap model) throws DataAccessException {
		if (isOpenSessionInViewActive()) {
			super.postHandle(request, model);
		}
	}


	@Override
	public void afterCompletion(WebRequest request, Exception ex) throws DataAccessException {
		if (isOpenSessionInViewActive()) {
			super.afterCompletion(request, ex);
			/*
			 * Removing the bean is probably not necessary as it should be cleared with the rest of the context in SecurityUserManagementFilter.
			 */
			getContextHandler().removeBean(Context.OPEN_SESSION_IN_VIEW_FLAG_NAME);
		}
	}


	private boolean isOpenSessionInViewActive() {
		Boolean openSessionInView = (Boolean) getContextHandler().getBean(Context.OPEN_SESSION_IN_VIEW_FLAG_NAME);
		return BooleanUtils.isTrue(openSessionInView);
	}


	public String getEnableOpenSessionInViewParameter() {
		return this.enableOpenSessionInViewParameter;
	}


	public void setEnableOpenSessionInViewParameter(String enableOpenSessionInViewParameter) {
		this.enableOpenSessionInViewParameter = enableOpenSessionInViewParameter;
	}


	public Set<String> getEnableOpenSessionInViewURLs() {
		return this.enableOpenSessionInViewURLs;
	}


	public void setEnableOpenSessionInViewURLs(Set<String> enableOpenSessionInViewURLs) {
		this.enableOpenSessionInViewURLs = enableOpenSessionInViewURLs;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}
}
