package com.clifton.core.dataaccess.search.form.entity;

import com.clifton.core.dataaccess.search.SearchField;

import java.util.Date;


/**
 * The <code>BaseUpdatableEntitySearchForm</code> class is a {@link BaseEntitySearchForm} which includes search parameters for the standard audit fields for
 * updatable entities.
 * <p>
 * If the searched entity does not include the updatable audit fields or if it includes the creatable audit fields as well, then the {@link
 * BaseEntitySearchForm} or {@link BaseAuditableEntitySearchForm} should be used instead.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseEntitySearchForm Parent fields}
 * <li>{@link #updateDate}
 * <li>{@link #updateUserId}
 * </ul>
 *
 * @author MikeH
 * @see BaseEntitySearchForm
 * @see BaseAuditableEntitySearchForm
 */
public abstract class BaseUpdatableEntitySearchForm extends BaseEntitySearchForm implements EntityUpdateFieldSearchForm {

	@SearchField(dateFieldIncludesTime = true)
	private Date updateDate;

	@SearchField
	private Short updateUserId;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getUpdateDate() {
		return this.updateDate;
	}


	@Override
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	@Override
	public Short getUpdateUserId() {
		return this.updateUserId;
	}


	@Override
	public void setUpdateUserId(Short updateUserId) {
		this.updateUserId = updateUserId;
	}
}
