package com.clifton.core.dataaccess.dao;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>NonPersistentObject</code> annotation marks DTO classes as being "non-persistent":
 * there is no database table or DAO that maps to this DTO.  NonPersistent DTO's are
 * usually based on one or more actual DTO's and require custom processing.
 * <p/>
 * Class with this annotation bypass special binding (no need to retrieve original
 * bean from the database). Also, if a service method takes one argument that is
 * a NonPersistentObject, then it's model key will be replaced with "data" during serialization
 * instead of service method name.
 *
 * @author vgomelsky
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface NonPersistentObject {

	/**
	 * Specifies whether NonPersistent DTO's properties that are real DTO's should be
	 * populated before binding NonPersistent DTO.
	 */
	boolean populatePropertiesBeforeBinding() default false;
}
