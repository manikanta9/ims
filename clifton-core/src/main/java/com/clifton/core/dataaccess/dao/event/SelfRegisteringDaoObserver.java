package com.clifton.core.dataaccess.dao.event;


import com.clifton.core.beans.IdentityObject;

import java.util.Collections;
import java.util.List;


/**
 * The <code>SelfRegisteringDaoObserver</code> class should be extended by specific DaoEventObserver implementations.
 * DaoObserverRegistrator will find every context-managed bean that extends this class and will automatically register it
 * for the corresponding events of the specified DAO object.
 *
 * @author Mary Anderson
 */
public abstract class SelfRegisteringDaoObserver<T extends IdentityObject> extends BaseDaoEventObserver<T> {


	/**
	 * Returns {@link DaoEventTypes} that this observer must be registered for.
	 */
	public abstract DaoEventTypes getRegisteredDaoEventTypes();


	/**
	 * Returns a list of DTO classes that will be used to find additional DAO to register with.
	 * Used in rare cases when this observer needs to register as an observer for multiple DAO's.
	 */
	public List<Class<? extends IdentityObject>> getAdditionalDtoTypeList() {
		return Collections.emptyList();
	}
}
