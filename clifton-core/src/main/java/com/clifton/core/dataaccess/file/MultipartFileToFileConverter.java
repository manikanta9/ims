package com.clifton.core.dataaccess.file;


import com.clifton.core.util.converter.Converter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;


/**
 * The <code>MultipartFileToFileConverter</code> converts a MultipartFile to File object
 *
 * @author manderson
 */
public class MultipartFileToFileConverter implements Converter<MultipartFile, File> {

	@Override
	public File convert(MultipartFile from) {
		if (from == null) {
			return null;
		}
		File file = null;
		try {
			file = File.createTempFile(FileUtils.getFileNameWithoutExtension(from.getOriginalFilename()), "." + FileUtils.getFileExtension(from.getOriginalFilename()));
			from.transferTo(file);
			file.deleteOnExit();
			return file;
		}
		catch (Exception e) {
			// Delete the File Immediately if there's an error
			if (file != null) {
				file.delete();
			}
			throw new RuntimeException("Error converting Multipart File to File", e);
		}
	}
}
