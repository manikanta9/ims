package com.clifton.core.dataaccess.dao.event.cache;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>DaoSingleKeyCache</code> interface defines the methods to use with DAO caches that return an entity with one key value
 *
 * @author manderson
 */
public interface DaoSingleKeyCache<T extends IdentityObject, K> {


	/**
	 * Returns the bean with the give key value.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 * <p>
	 * Note: Does not enforce that the bean exists, use strict method to require it exists.
	 */
	public T getBeanForKeyValue(ReadOnlyDAO<T> dao, K value);


	/**
	 * Returns the bean with the give key value.
	 * Checks the cache first, and if doesn't exist then will look it up in the database.
	 *
	 * @throws ValidationException if the bean is missing
	 */
	public T getBeanForKeyValueStrict(ReadOnlyDAO<T> dao, K value);
}
