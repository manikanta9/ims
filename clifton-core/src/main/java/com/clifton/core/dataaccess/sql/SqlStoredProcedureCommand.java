package com.clifton.core.dataaccess.sql;

import java.io.Serializable;
import java.util.LinkedHashMap;


/**
 * The SqlStoredProcedureCommand class represents a stored procedure.
 *
 * @author vgomelsky
 */
public class SqlStoredProcedureCommand implements Serializable {

	private String procedureName;
	private LinkedHashMap<String, Object> parameters;
	private Integer timeoutInSeconds;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SqlStoredProcedureCommand() {
	}


	public static SqlStoredProcedureCommand forProcedure(String procedureName) {
		SqlStoredProcedureCommand command = new SqlStoredProcedureCommand();
		command.setProcedureName(procedureName);
		return command;
	}


	/**
	 * The order of parameters is important.  It must match the order of parameters in corresponding stored procedure.
	 * Names are purely used to help mapping.
	 */
	public SqlStoredProcedureCommand withParameters(LinkedHashMap<String, Object> parameters) {
		this.setParameters(parameters);
		return this;
	}


	public SqlStoredProcedureCommand withTimeoutInSeconds(Integer timeoutInSeconds) {
		this.setTimeoutInSeconds(timeoutInSeconds);
		return this;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getProcedureName() {
		return this.procedureName;
	}


	public void setProcedureName(String procedureName) {
		this.procedureName = procedureName;
	}


	public LinkedHashMap<String, Object> getParameters() {
		return this.parameters;
	}


	public void setParameters(LinkedHashMap<String, Object> parameters) {
		this.parameters = parameters;
	}


	public Integer getTimeoutInSeconds() {
		return this.timeoutInSeconds;
	}


	public void setTimeoutInSeconds(Integer timeoutInSeconds) {
		this.timeoutInSeconds = timeoutInSeconds;
	}
}
