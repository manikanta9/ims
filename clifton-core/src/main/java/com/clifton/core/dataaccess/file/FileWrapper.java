package com.clifton.core.dataaccess.file;


import java.io.File;
import java.io.Serializable;


/**
 * The <code>FileWrapper</code> class wraps the actual {@link File} object.
 * It should be used instead of the File directly in order to take advantage of automatic
 * deletion of temp-files as well as user friendly naming convention.
 *
 * @author manderson
 */
public class FileWrapper implements Serializable {

	/**
	 * The actual File
	 */
	private FilePath file;

	/**
	 * The fileName to use
	 * Temp files that are created add extra timestamp to the file name that we don't want
	 * to give to the user, so this is used to override in the response.
	 */
	private String fileName;

	/**
	 * If true when the FileDownloadView is done with the file it will be deleted
	 */
	private boolean tempFile;


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	public FileWrapper(File file, String fileName, boolean tempFile) {
		this.file = new FilePath(file.getPath());
		this.fileName = fileName;
		this.tempFile = tempFile;
	}


	public FileWrapper(FilePath file, String fileName, boolean tempFile) {
		this.file = file;
		this.fileName = fileName;
		this.tempFile = tempFile;
	}


	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	public FilePath getFile() {
		return this.file;
	}


	public void setFile(FilePath file) {
		this.file = file;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public boolean isTempFile() {
		return this.tempFile;
	}


	public void setTempFile(boolean tempFile) {
		this.tempFile = tempFile;
	}
}
