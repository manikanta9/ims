package com.clifton.core.dataaccess.sql;


import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.object.StoredProcedure;


public class SqlStoredProcedure extends StoredProcedure {

	public SqlStoredProcedure(JdbcTemplate jdbcTemplate, String name) {
		super(jdbcTemplate, name);
	}
}
