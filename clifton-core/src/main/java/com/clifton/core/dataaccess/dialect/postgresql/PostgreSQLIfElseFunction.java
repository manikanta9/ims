package com.clifton.core.dataaccess.dialect.postgresql;

import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import com.clifton.core.dataaccess.function.SimpleSQLFunction;
import org.hibernate.QueryException;
import org.hibernate.engine.spi.Mapping;
import org.hibernate.engine.spi.SessionFactoryImplementor;
import org.hibernate.type.Type;

import java.util.List;


/**
 * <code>PostgresIfSQLFunction</code> is the equivalent to MSSQL 'IIF' and MySQL equivalent to 'IF'. Used as an if then else then clause.
 *
 * @author dillonm
 */
public class PostgreSQLIfElseFunction extends SimpleSQLFunction implements SQLIfElseFunction {

	@Override
	public Type getReturnType(Type firstArgumentType, Mapping mapping) throws QueryException {
		return firstArgumentType;
	}


	@Override
	public String render(Type firstArgumentType, List arguments, SessionFactoryImplementor factory) throws QueryException {
		if (arguments.size() != 3) {
			throw new RuntimeException(getClass().getName() + " needs 3 arguments. " + arguments.size() + " were provided.");
		}
		final StringBuilder buf = new StringBuilder();
		buf.append("(CASE");
		buf.append(arguments.get(0));
		buf.append(" THEN ");
		buf.append(arguments.get(1));
		buf.append(" ELSE ");
		buf.append(arguments.get(2));
		buf.append(" END)");
		return buf.toString();
	}
}
