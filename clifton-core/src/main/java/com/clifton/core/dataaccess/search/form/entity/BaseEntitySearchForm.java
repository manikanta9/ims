package com.clifton.core.dataaccess.search.form.entity;


import com.clifton.core.dataaccess.search.form.BaseRestrictiveSearchForm;


/**
 * The <code>BaseEntitySearchForm</code> class is a {@link BaseRestrictiveSearchForm} for entity searches. This class should be extended by search forms which
 * search some specified set of entity fields.
 * <p>
 * If the searched entity includes audit fields, then the {@link BaseUpdatableEntitySearchForm} or {@link BaseAuditableEntitySearchForm} should be used instead.
 * <p>
 * Available fields:
 * <ul>
 * <li>{@link BaseRestrictiveSearchForm Parent fields}
 * </ul>
 *
 * @author MikeH
 * @see BaseUpdatableEntitySearchForm
 * @see BaseAuditableEntitySearchForm
 */
public abstract class BaseEntitySearchForm extends BaseRestrictiveSearchForm {

}
