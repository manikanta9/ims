package com.clifton.core.dataaccess.search.hibernate;


import com.clifton.core.dataaccess.search.OrderByDirections;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Order;


/**
 * The <code>ConcatenateOrderBy</code> is used for concatenating multiple fields together for sorting.  For example, sorting by the expanded name of a hierarchical object.
 * Uses COALESCE(propertyValue + ' / ') + COALESCE(property2Value + ' / '), etc.
 *
 * @author manderson
 */
public class ConcatenateOrderBy extends Order {

	private final String[] propertyNames;

	/**
	 * If {@code true}, a slash delimiter (<tt>"/"</tt>) will be placed between the included fields. Otherwise, the included fields will be joined with a space character
	 * (<tt>" "</tt>).
	 *
	 * @see SearchFieldCustomTypes#CONCATENATE_SORT_WITH_DELIMITER
	 */
	private final boolean useDelimiter;

	/**
	 * Can be used for numeric fields to pad with zeros
	 * LENGTH OF 6 so that 20 (000020) would come before 100 (000100) in a text search
	 */
	private final boolean usePadding;

	/**
	 * Use space characters as opposed to nothing for the default (very last one) COALESCE value.
	 */
	private final boolean useSpaceWhenNull;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructor for Order.
	 */
	private ConcatenateOrderBy(String[] propertyNames, boolean ascending, boolean useDelimiter, boolean usePadding, boolean useSpaceWhenNull) {
		super(null, ascending);
		this.propertyNames = propertyNames;
		this.useDelimiter = useDelimiter;
		this.usePadding = usePadding;
		this.useSpaceWhenNull = useSpaceWhenNull;
	}


	public static ConcatenateOrderBy create(OrderByDirections direction, SearchFieldCustomTypes fieldType, String[] propertyNames) {
		final ConcatenateOrderBy order;
		boolean ascending = direction == OrderByDirections.ASC;
		switch (fieldType) {
			case CONCATENATE_SORT:
			case CONCATENATE_STRING_AND_NUMBER:
				order = new ConcatenateOrderBy(propertyNames, ascending, false, false, false);
				break;
			case CONCATENATE_SORT_WITH_DELIMITER:
				order = new ConcatenateOrderBy(propertyNames, ascending, true, false, false);
				break;
			case CONCATENATE_SORT_WITH_DELIMITER_AND_SPACE_PADDING:
				order = new ConcatenateOrderBy(propertyNames, ascending, true, false, true);
				break;
			case CONCATENATE_SORT_WITH_PADDING:
				order = new ConcatenateOrderBy(propertyNames, ascending, true, true, false);
				break;
			default:
				throw new IllegalArgumentException("Unrecognized SearchFieldCustomTypes value for concatenation ordering: " + fieldType);
		}
		return order;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		StringBuilder fragmentSb = new StringBuilder("(");
		for (int i = 0; i < this.propertyNames.length; i++) {
			// Could have a mixture of string and numeric fields, so check each one and if it is numeric it must be cast to varchar in order to concatenate
			boolean appendStringCast = (this.usePadding || Number.class.isAssignableFrom(criteriaQuery.getType(criteria, this.propertyNames[i]).getReturnedClass()));
			String column = criteriaQuery.getColumn(criteria, this.propertyNames[i]);
			fragmentSb.append("COALESCE(");
			if (this.usePadding) {
				fragmentSb.append("RIGHT('000000' +");
			}
			if (appendStringCast) {
				fragmentSb.append("CAST(");
			}
			fragmentSb.append(column);
			if (this.usePadding) {
				fragmentSb.append(" AS VARCHAR(6)), 6)");
			}
			else if (appendStringCast) {
				fragmentSb.append(" AS VARCHAR)");
			}
			fragmentSb.append(this.useDelimiter ? " + ' / '" : " + ' '");
			fragmentSb.append(this.useSpaceWhenNull ? ", ' ') " : ", '') ");
			if (i < this.propertyNames.length - 1) {
				fragmentSb.append(" + ");
			}
		}
		fragmentSb.append(") ");
		fragmentSb.append(isAscending() ? "asc" : "desc");
		return fragmentSb.toString();
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String[] getPropertyNames() {
		return this.propertyNames;
	}
}
