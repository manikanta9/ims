package com.clifton.core.dataaccess.search;

import com.clifton.core.dataaccess.search.form.CustomJsonStringAwareSearchForm;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.dataaccess.search.hibernate.expression.CustomJsonStringExpression;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.sql.JoinType;
import org.springframework.stereotype.Component;

import java.util.Iterator;
import java.util.Map;


/**
 * The <code>CustomJsonStringAwareSearchFormConfigurer</code> is registered for any search form that implements {@link CustomJsonStringAwareSearchForm}
 * It looks for every search restriction field name that contains a "." (nested property) which is generally not allowed and uses that to determine that it's a json nested value.
 * Then builds the search expression using the {@link CustomJsonStringExpression}
 *
 * @author manderson
 */
@Component
public class CustomJsonStringAwareSearchFormConfigurer implements SearchFormCustomConfigurer<CustomJsonStringAwareSearchForm> {

	@Override
	public Class<CustomJsonStringAwareSearchForm> getSearchFormInterfaceClassName() {
		return CustomJsonStringAwareSearchForm.class;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void configureCriteria(Criteria criteria, CustomJsonStringAwareSearchForm searchForm, Map<String, Criteria> processedPathToCriteria) {
		if (!CollectionUtils.isEmpty(searchForm.getRestrictionList())) {
			Iterator<SearchRestriction> searchRestrictionIterator = searchForm.getRestrictionList().iterator();
			while (searchRestrictionIterator.hasNext()) {
				SearchRestriction searchRestriction = searchRestrictionIterator.next();
				if (searchRestriction.getField().contains(".")) {
					// Custom Json Value
					//  COALESCE(json_value(customcolumns, '$.TestTableColumn.value'), json_value(customcolumns, '$.TestTableColumn'))
					String jsonValueColumn = StringUtils.substringBeforeLast(searchRestriction.getField(), "."); // i.e. customColumns
					String propertyName = StringUtils.substringAfterLast(searchRestriction.getField(), "."); //  i.e. TestTableColumn

					// Nested Path (Search on a table that joins to the table with the custom columns)
					String path = StringUtils.substringBeforeLast(jsonValueColumn, ".");
					if (!StringUtils.isEmpty(path)) {
						path = HibernateUtils.getPathAlias(path, criteria, HibernateUtils.DEFAULT_ALIAS_PREFIX, JoinType.INNER_JOIN, processedPathToCriteria);
						jsonValueColumn = StringUtils.substringAfterFirst(jsonValueColumn, ".");
					}

					criteria.add(new CustomJsonStringExpression(searchRestriction.getComparison().getComparisonExpression(), path, jsonValueColumn, propertyName, searchRestriction.getValue(), searchRestriction.getDataTypeName()));
					searchRestrictionIterator.remove();
				}
			}
		}
	}
}
