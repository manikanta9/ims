package com.clifton.core.dataaccess.db;

import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Pattern;


public class DataTypeNameUtils {

	//Allows for values like 0E-10 to be cleaned properly.
	//https://stackoverflow.com/questions/638565/parsing-scientific-notation-sensibly
	public static final Pattern CLEAN_STRING_DECIMAL_PATTERN = Pattern.compile("/-?(?:0|[1-9]\\d*)(?:\\.\\d*)?(?:[eE][+\\-]?\\d+)?/");


	public static final Set<DateTimeFormatter> MONTH_DAY_YEAR_FORMATTERS = new LinkedHashSet<>();
	public static final Set<DateTimeFormatter> YEAR_MONTH_DAY_FORMATTERS = new LinkedHashSet<>();


	static {
		for (String delimiter : Arrays.asList("/", "-", "\\")) {
			for (String month : Arrays.asList("MM", "M")) {
				for (String day : Arrays.asList("dd", "d")) {
					for (String year : Arrays.asList("yy", "yyyy")) {
						MONTH_DAY_YEAR_FORMATTERS.add(DateTimeFormatter.ofPattern(month + delimiter + day + delimiter + year));
					}
				}
			}
		}
		MONTH_DAY_YEAR_FORMATTERS.add(DateTimeFormatter.ofPattern("MMddyy"));
		MONTH_DAY_YEAR_FORMATTERS.add(DateTimeFormatter.ofPattern("MMddyyyy"));
		MONTH_DAY_YEAR_FORMATTERS.add(DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_SQL_PRECISE));

		YEAR_MONTH_DAY_FORMATTERS.add(DateTimeFormatter.ofPattern("yyyyMMdd"));
		for (String delimiter : Arrays.asList("/", "-", "\\")) {
			YEAR_MONTH_DAY_FORMATTERS.add(DateTimeFormatter.ofPattern("yyyy" + delimiter + "MM" + delimiter + "dd"));
			YEAR_MONTH_DAY_FORMATTERS.add(DateTimeFormatter.ofPattern("yyyy" + delimiter + "MM" + delimiter + "dd HH:mm:ss.SSSSSSS"));
		}
	}


	public static Object convertObjectToDataTypeName(Object value, DataTypeNames dataTypeName) {
		if (value == null) {
			return null;
		}
		if (value instanceof String) {
			return convertStringToDataTypeName((String) value, dataTypeName);
		}
		else if (value instanceof Short) {
			return convertShortToDataTypeName((Short) value, dataTypeName);
		}
		else if (value instanceof Integer) {
			return convertIntegerToDataTypeName((Integer) value, dataTypeName);
		}
		else if (value instanceof Double) {
			return convertDoubleToDataTypeName((Double) value, dataTypeName);
		}
		else if (value instanceof BigDecimal) {
			return convertBigDecimalToDataTypeName((BigDecimal) value, dataTypeName);
		}
		else if (value instanceof Boolean) {
			return convertBooleanToDataTypeName((Boolean) value, dataTypeName);
		}
		else if (value instanceof Date) {
			return convertDateToDataTypeName((Date) value, dataTypeName);
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	//Can result in class cast exception
	@SuppressWarnings("unchecked")
	public static <C> C convertObjectToDataTypeName(Object value, DataTypeNames dataTypeName, Class<C> expectedClass) {
		return (C) convertObjectToDataTypeName(value, dataTypeName);
	}


	public static Object convertStringToDataTypeName(String value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case STRING:
			case TEXT:
				return value;
			case INTEGER:
				return new Integer(value);
			case DECIMAL:
				if (StringUtils.isEmpty(value)) {
					return null;
				}
				String cleanDecimal = CLEAN_STRING_DECIMAL_PATTERN.matcher(StringUtils.trim(value)).replaceAll("").replaceAll("[^eE0-9.\\-)(]", "");
				//Potentially could result in an empty string or just a decimal or just a '-'?
				if (StringUtils.isEmpty(cleanDecimal) || ".".equals(cleanDecimal) || "-".equals(cleanDecimal)) {
					return null;
				}
				if (cleanDecimal.startsWith("(") && cleanDecimal.endsWith(")")) {
					cleanDecimal = "-" + StringUtils.substring(cleanDecimal, 1, cleanDecimal.length() - 1);
				}
				try {
					return new BigDecimal(cleanDecimal);
				}
				catch (Exception e) {
					throw new ValidationException("Failed to convert string [" + cleanDecimal + "] to a decimal value!", e);
				}
			case DATE:
				if (StringUtils.isEmpty(value)) {
					return null;
				}
				if (value.startsWith("20") || value.startsWith("19")) {
					for (DateTimeFormatter formatter : YEAR_MONTH_DAY_FORMATTERS) {
						try {
							LocalDate localDate = LocalDate.parse(value, formatter);
							return DateUtils.asUtilDate(localDate);
						}
						catch (Exception ex) {
							// ignore
						}
					}
				}
				else {
					for (DateTimeFormatter formatter : MONTH_DAY_YEAR_FORMATTERS) {
						try {
							LocalDate localDate = LocalDate.parse(value, formatter);
							return DateUtils.asUtilDate(localDate);
						}
						catch (Exception ex) {
							// ignore
						}
					}
				}

				throw new ValidationException("Failed to convert string [" + value + "] to a date value!");
			case BOOLEAN:
				return BooleanUtils.isTrue(value);
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertShortToDataTypeName(Short value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case INTEGER:
				return value.intValue();
			case DECIMAL:
				return new BigDecimal(value);
			case STRING:
			case TEXT:
				return Short.toString(value);
			case BOOLEAN:
				return BooleanUtils.isTrue(value);
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertIntegerToDataTypeName(Integer value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case INTEGER:
				return value;
			case DECIMAL:
				return new BigDecimal(value);
			case STRING:
			case TEXT:
				return Integer.toString(value);
			case BOOLEAN:
				return BooleanUtils.isTrue(value);
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertDoubleToDataTypeName(Double value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case INTEGER:
				return value.intValue();
			case DECIMAL:
				return BigDecimal.valueOf(value);
			case STRING:
			case TEXT:
				return Double.toString(value);
			case BOOLEAN:
				return BooleanUtils.isTrue(value);
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertLongToDataTypeName(Long value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case DECIMAL:
				return BigDecimal.valueOf(value);
			case INTEGER:
				return value.intValue();
			case STRING:
			case TEXT:
				return Long.toString(value);
			case BOOLEAN:
				return BooleanUtils.isTrue(value);
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertBigDecimalToDataTypeName(BigDecimal value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case DECIMAL:
				return value;
			case STRING:
			case TEXT:
				return value.toPlainString();
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertBooleanToDataTypeName(Boolean value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case BOOLEAN:
				return value;
			case STRING:
			case TEXT:
				return value.toString();
			case INTEGER:
				return value ? 1 : 0;
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}


	public static Object convertDateToDataTypeName(Date value, DataTypeNames dataTypeName) {
		switch (dataTypeName) {
			case DATE:
				return value;
			case STRING:
				return value.toString();
		}
		throw new UnsupportedOperationException("Unsupported conversion of [" + value.getClass() + "] to [" + dataTypeName.name() + "]!");
	}
}
