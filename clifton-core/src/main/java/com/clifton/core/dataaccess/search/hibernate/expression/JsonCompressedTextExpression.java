package com.clifton.core.dataaccess.search.hibernate.expression;


import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.BigDecimalType;
import org.hibernate.type.DateType;
import org.hibernate.type.StringType;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author stevenf
 */
public class JsonCompressedTextExpression implements Criterion {

	private final String comparisonExpression;

	private final String propertyPath;

	private final String propertyName;

	private final Object value;

	private final Class<?> dataTypeClass;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonCompressedTextExpression(String propertyPath, String propertyName, String comparisonExpression, Object value, Class<?> dataTypeClass) {
		this.propertyPath = propertyPath;
		this.propertyName = propertyName;
		this.comparisonExpression = comparisonExpression;
		this.value = value;
		this.dataTypeClass = dataTypeClass;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		if (this.value instanceof Date) {
			return new TypedValue[]{new TypedValue(DateType.INSTANCE, this.value)};
		}
		else if (this.value instanceof BigDecimal) {
			return new TypedValue[]{new TypedValue(BigDecimalType.INSTANCE, this.value)};
		}
		return new TypedValue[]{new TypedValue(StringType.INSTANCE, this.value)};
	}


	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		return toString();
	}


	@Override
	public String toString() {
		String sql = "JSON_VALUE(CAST(DECOMPRESS(" + getPropertyPath() + ") AS VARCHAR(MAX)), '" + getPropertyName() + "')";
		if (getDataTypeClass() != null) {
			if (Date.class.equals(getDataTypeClass())) {
				sql = "CAST(" + sql + " AS DATE)";
			}
			else if (BigDecimal.class.equals(getDataTypeClass())) {
				sql = "CAST(" + sql + " AS DECIMAL(28,16))";
			}
		}
		return sql + " " + getComparisonExpression() + (!StringUtils.contains(getComparisonExpression(), "LIKE") ? " ?" : "");
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public String getComparisonExpression() {
		return this.comparisonExpression;
	}


	public String getPropertyPath() {
		return this.propertyPath;
	}


	public String getPropertyName() {
		return this.propertyName;
	}


	public Object getValue() {
		return this.value;
	}


	public Class<?> getDataTypeClass() {
		return this.dataTypeClass;
	}
}
