package com.clifton.core.dataaccess.search.hibernate.expression;

import com.clifton.core.dataaccess.db.DataTypeNameUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.StringUtils;
import org.hibernate.Criteria;
import org.hibernate.HibernateException;
import org.hibernate.criterion.CriteriaQuery;
import org.hibernate.criterion.Criterion;
import org.hibernate.engine.spi.TypedValue;
import org.hibernate.type.BooleanType;
import org.hibernate.type.DateType;
import org.hibernate.type.StringType;
import org.hibernate.type.Type;

import java.io.Serializable;
import java.util.Date;


/**
 * Builds expression for retrieving a value from a {@link com.clifton.core.json.custom.CustomJsonString} value.  Depending on the data type the value may or may NOT be nested within a "value" property.
 * i.e. String values just have the String value, no value/text properties.  However, ID/Label values have value = ID and text =  label
 * In order to support both versions, we use a coalesce
 * Syntax is: COALESCE(JSON_VALUE(customJsonStringPropertyName, '$.propertyName.value'), JSON_VALUE(customJsonStringPropertyName, '$.propertyName'))
 *
 * @author manderson
 * @author michaelm
 */
public class CustomJsonStringExpression implements Criterion {

	private final String comparisonExpression;

	/**
	 * The column that contains the JSON values
	 */
	private final String customJsonStringPropertyName;

	/**
	 * The property we are looking for within the JSON values
	 */
	private final String propertyName;


	private final Object value;

	private final DataTypeNames dataTypeName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public CustomJsonStringExpression(String comparisonExpression, String customJsonStringPropertyName, String propertyName, Object value) {
		this(comparisonExpression, null, customJsonStringPropertyName, propertyName, value, null);
	}


	public CustomJsonStringExpression(String comparisonExpression, String pathAlias, String customJsonStringPropertyName, String propertyName, Object value, DataTypeNames dataTypeName) {
		this.comparisonExpression = comparisonExpression;
		this.customJsonStringPropertyName = (!StringUtils.isEmpty(pathAlias) ? pathAlias + "." : "") + customJsonStringPropertyName;
		this.propertyName = propertyName;
		this.value = value;
		this.dataTypeName = dataTypeName;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method adds an ISJSON(column) check in addition to the JSON_VALUE(column, ?) criteria to avoid an issue where the JSON_VALUE sql function throws an error immediately when it encounters a
	 * row with invalid json. Without this check, a single row in the table can cause the CustomJsonStringExpression to fail for all entities.
	 */
	@Override
	public String toSqlString(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		String column = criteriaQuery.getColumn(criteria, this.customJsonStringPropertyName);
		StringBuilder fragment = new StringBuilder("ISJSON(" + column + ") > 0 AND COALESCE(");
		String jsonValueSyntax = "JSON_VALUE(" + column + ", ?)";

		fragment.append(jsonValueSyntax);
		fragment.append(",");
		fragment.append(jsonValueSyntax);

		fragment.append(") ");
		fragment.append(this.comparisonExpression);
		// For cases like: LIKE '%' + ? + '%' we don't want to add a second ?, but we need to for cases like =, >, <, etc.
		if (!StringUtils.contains(this.comparisonExpression, '?')) {
			fragment.append(" ?");
		}
		return fragment.toString();
	}


	@Override
	public TypedValue[] getTypedValues(Criteria criteria, CriteriaQuery criteriaQuery) throws HibernateException {
		String firstJsonValue = getJsonValuePath(true);
		String secondJsonValue = getJsonValuePath(false);

		return new TypedValue[]{new TypedValue(StringType.INSTANCE, firstJsonValue), new TypedValue(StringType.INSTANCE, secondJsonValue), getTypedValue()};
	}


	private TypedValue getTypedValue() {
		Type type = StringType.INSTANCE;
		Object valueObject = this.value;
		if (this.value != null) {
			if (this.dataTypeName != null) {
				valueObject = DataTypeNameUtils.convertStringToDataTypeName(this.value.toString(), this.dataTypeName);
			}
		}
		if (valueObject instanceof Number) {
			type = HibernateUtils.getNumericType((Serializable) valueObject);
		}
		else if (valueObject instanceof Boolean) {
			type = BooleanType.INSTANCE;
		}
		else if (valueObject instanceof Date) {
			type = DateType.INSTANCE;
		}
		return new TypedValue(type, valueObject);
	}


	private String getJsonValuePath(boolean appendValue) {
		// Use Double Quotes for the property name (If the property name has a space it must be encapsulated with double quotes.)
		return "$.\"" + this.propertyName + "\"" + (appendValue ? ".value" : "");
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String getComparisonExpression() {
		return this.comparisonExpression;
	}


	public String getCustomJsonStringPropertyName() {
		return this.customJsonStringPropertyName;
	}


	public String getPropertyName() {
		return this.propertyName;
	}


	public Object getValue() {
		return this.value;
	}
}
