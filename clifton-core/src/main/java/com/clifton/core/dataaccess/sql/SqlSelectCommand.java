package com.clifton.core.dataaccess.sql;


import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;

import java.io.Serializable;


/**
 * The <code>SqlSelectCommand</code> class encapsulates properties and parameters necessary to execute a SELECT SQL
 * command.
 *
 * @author vgomelsky
 */
public class SqlSelectCommand extends SqlCommand<SqlSelectCommand> implements PagingCommand, Serializable {

	// -1 indicating to use the JDBC driver's default
	public static final int DEFAULT_QUERY_TIMEOUT = -1;
	public static final String SET_NO_COUNT_ON = "SET NOCOUNT ON";
	public static final String SET_NO_COUNT_OFF = "SET NOCOUNT OFF";

	private String tempTableClause;
	private String withClause;
	private String selectClause;
	private String fromClause;
	private String groupByClause;
	private String havingClause;
	private String orderByClause;

	private int start;
	private int limit;
	private Integer maxLimit;

	private boolean optionRecompile;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SqlSelectCommand() {
		this(null);
	}


	public SqlSelectCommand(String sql) {
		this(sql, null);
	}


	public SqlSelectCommand(String sql, SqlParameterValue[] sqlParameterValues) {
		this(sql, sqlParameterValues, DEFAULT_START, DEFAULT_LIMIT);
	}


	public SqlSelectCommand(String sql, SqlParameterValue[] sqlParameterValues, PagingCommand pagingCommand) {
		this(sql, sqlParameterValues, pagingCommand.getStart(), pagingCommand.getLimit());
	}


	public SqlSelectCommand(String sql, SqlParameterValue[] sqlParameterValues, int start, int limit) {
		this(sql, sqlParameterValues, start, limit, DEFAULT_QUERY_TIMEOUT);
	}


	public SqlSelectCommand(String sql, SqlParameterValue[] sqlParameterValues, int start, int limit, int timeout) {
		super(sql);
		setSqlParameterValues(sqlParameterValues);

		AssertUtils.assertTrue(start >= 0, "The start argument must be greater than 0");
		AssertUtils.assertTrue(limit > 0, "The limit argument must be greater than 0");
		setStart(start);
		setLimit(limit);
		setTimeout(timeout);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the value of the sql field if it was set or builds SQL statement based on set clauses.
	 */
	@Override
	public String getSql() {
		if (super.getSql() != null) {
			return super.getSql();
		}
		StringBuilder result = new StringBuilder(100);
		if (!StringUtils.isEmpty(getTempTableClause())) {
			result.append("SET NOCOUNT ON; ");
			result.append(getTempTableClause());
			result.append("SET NOCOUNT OFF; ");
		}
		if (!StringUtils.isEmpty(getWithClause())) {
			result.append(getWithClause());
		}
		result.append("SELECT ");
		result.append(getSelectClause());
		if (!StringUtils.isEmpty(getFromClause())) {
			result.append("\nFROM ");
			result.append(getFromClause());
		}
		if (!StringUtils.isEmpty(getWhereClause())) {
			result.append("\nWHERE ");
			result.append(getWhereClause());
		}
		if (!StringUtils.isEmpty(getGroupByClause())) {
			result.append("\nGROUP BY ");
			result.append(getGroupByClause());
		}
		if (!StringUtils.isEmpty(getHavingClause())) {
			result.append("\nHAVING ");
			result.append(getHavingClause());
		}
		if (!StringUtils.isEmpty(getOrderByClause())) {
			result.append("\nORDER BY ");
			result.append(getOrderByClause());
		}
		if (isOptionRecompile()) {
			result.append("\nOPTION (RECOMPILE)");
		}
		return result.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the index of the first element.  Starts with and defaults to 0.
	 */
	@Override
	public int getStart() {
		return this.start;
	}


	/**
	 * Returns the maximum number of elements allowed.  Defaults to Integer.MAX_VALUE.
	 */
	@Override
	public int getLimit() {
		return this.limit;
	}


	@Override
	public Integer getMaxLimit() {
		return this.maxLimit;
	}


	/**
	 * Returns true if defaults for start or limit were changed.
	 */
	public boolean isPagingDefined() {
		return (this.start != DEFAULT_START) || (this.limit != DEFAULT_LIMIT);
	}


	public void setStart(int start) {
		this.start = start;
	}


	public void setLimit(int limit) {
		this.limit = limit;
	}


	public void setMaxLimit(Integer maxLimit) {
		this.maxLimit = maxLimit;
	}


	public String getTempTableClause() {
		return this.tempTableClause;
	}


	public void setTempTableClause(String tempTableClause) {
		this.tempTableClause = tempTableClause;
	}


	public String getWithClause() {
		return this.withClause;
	}


	public void setWithClause(String withClause) {
		this.withClause = withClause;
	}


	public String getSelectClause() {
		return this.selectClause;
	}


	public void setSelectClause(String selectClause) {
		this.selectClause = selectClause;
	}


	public String getFromClause() {
		return this.fromClause;
	}


	public void setFromClause(String fromClause) {
		this.fromClause = fromClause;
	}


	public String getGroupByClause() {
		return this.groupByClause;
	}


	public void setGroupByClause(String groupByClause) {
		this.groupByClause = groupByClause;
	}


	public String getHavingClause() {
		return this.havingClause;
	}


	public void setHavingClause(String havingClause) {
		this.havingClause = havingClause;
	}


	public String getOrderByClause() {
		return this.orderByClause;
	}


	public void setOrderByClause(String orderByClause) {
		this.orderByClause = orderByClause;
	}


	public boolean isOptionRecompile() {
		return this.optionRecompile;
	}


	public void setOptionRecompile(boolean optionRecompile) {
		this.optionRecompile = optionRecompile;
	}
}
