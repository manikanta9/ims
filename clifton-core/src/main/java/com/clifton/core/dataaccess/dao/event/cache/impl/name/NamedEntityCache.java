package com.clifton.core.dataaccess.dao.event.cache.impl.name;


import com.clifton.core.beans.NamedObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.event.DaoDtoClassAware;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.dao.event.cache.impl.SelfRegisteringSingleKeyDaoCache;
import com.clifton.core.util.StringUtils;


/**
 * The <code>NamedEntityCache</code> class is used for all NamedEntity DAO caches
 * that store/retrieve beans from the cache by name.
 * <p>
 * Uses {@link NamedObject} so can be used by any entity that uses getName()
 * <p>
 * A new cache is registered for each UpdatableDAO whose DTO is annotated with @CacheByName.  This is done in the HibernateDAOFactoryPostProcessor after the dao is registered.
 *
 * @author Mary Anderson
 */
public class NamedEntityCache<T extends NamedObject> extends SelfRegisteringSingleKeyDaoCache<T, String> implements DaoNamedEntityCache<T>, DaoDtoClassAware<T> {

	/**
	 * Since these are registered at runtime, we can't set generics at that point, so we need the dtoClass as a property to set/retrieve
	 */
	private Class<T> dtoClass;

	/**
	 * Need to override the default logic of using the class name as the cache name since these all use the same class
	 */
	private String cacheName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public void put(String key, T bean) {
		if (bean != null && !StringUtils.isEmpty(key)) {
			getCacheHandler().put(getCacheName(), key, new ObjectWrapper<>(bean.getIdentity()));
		}
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected String getBeanKeyProperty() {
		return "name";
	}


	@Override
	protected String getBeanKeyValue(T bean) {
		return bean.getName();
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Class<T> getDtoClass() {
		return this.dtoClass;
	}


	public void setDtoClass(Class<T> dtoClass) {
		this.dtoClass = dtoClass;
	}


	@Override
	public String getCacheName() {
		return this.cacheName;
	}


	public void setCacheName(String cacheName) {
		this.cacheName = cacheName;
	}
}
