package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CustomCache;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.StringUtils;

import java.util.Arrays;
import java.util.stream.Collectors;


/**
 * The <code>SelfRegisteringSimpleDaoCache</code> class is a helper class that can be extended by DAO caches.
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events. (By default INSERT, UPDATE OR DELETE)
 * <p/>
 * Default implementation fully clears cache after any modify operation.
 *
 * @param <T>
 * @author vgomelsky
 */
public abstract class SelfRegisteringSimpleDaoCache<T extends IdentityObject, K, C> extends SelfRegisteringDaoObserver<T> implements CustomCache<K, C> {

	private CacheHandler<K, C> cacheHandler;


	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	/**
	 * Returns the name of the cache
	 */
	@Override
	public String getCacheName() {
		return this.getClass().getName();
	}


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	////////////////////////////////////////////////////////////////////////////
	////////                   Observer Methods                    /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<T> dao, DaoEventTypes event, T bean, Throwable e) {
		if (e == null) {
			getCacheHandler().clear(getCacheName());
		}
	}


	///////////////////////////////////////////////////////////////////////
	//  Helper Classes for Extending Classes that utilize bean properties for keys
	///////////////////////////////////////////////////////////////////////


	protected String getBeanKeyForProperties(Object... keyProperties) {
		int size = (keyProperties == null ? 0 : keyProperties.length);
		if (size == 0) {
			// throw an exception here?
			return null;
		}
		// If Only On Property - Return the string representation of that object.
		if (size == 1) {
			return getBeanKeySegmentForProperty(keyProperties[0]);
		}

		String key = Arrays.stream(keyProperties)
				.map(this::getBeanKeySegmentForProperty)
				.collect(Collectors.joining("_"));
		return key;
	}

	/**
	 * Returns the string representation of the provided object, or {@link StringUtils#NULL_STRING} if the object is null.
	 */
	protected String getBeanKeySegmentForProperty(Object keyProperty) {
		return keyProperty == null ? StringUtils.NULL_STRING : keyProperty.toString();
	}


	////////////////////////////////////////////////////////////////////////////
	////////                Getter and Setter Methods                  /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public CacheHandler<K, C> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<K, C> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}
}
