package com.clifton.core.dataaccess.file;


import com.clifton.core.util.converter.Converter;

import java.io.File;


/**
 * The <code>DataTableToFileConverter</code> interface defines the converter
 * for {@link DataTableFileConfig} objects to {@link File} objects
 *
 * @author manderson
 */
public interface DataTableToFileConverter extends Converter<DataTableFileConfig, File> {

	// NOTHING HERE
}
