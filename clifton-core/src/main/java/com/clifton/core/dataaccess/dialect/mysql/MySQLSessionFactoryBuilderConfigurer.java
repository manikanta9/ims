package com.clifton.core.dataaccess.dialect.mysql;

import com.clifton.core.dataaccess.dialect.SessionFactoryBuilderConfigurer;
import com.clifton.core.dataaccess.function.SQLFunctionUtils;
import com.clifton.core.dataaccess.function.SQLIfElseFunction;
import com.clifton.core.dataaccess.function.SQLIfNullFunction;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.stereotype.Component;


/**
 * @author dillonm
 */
@Component
public class MySQLSessionFactoryBuilderConfigurer implements SessionFactoryBuilderConfigurer {

	@Override
	public void configure(LocalSessionFactoryBuilder sfb) {
		SQLFunctionUtils.addSqlFunction(sfb, SQLIfNullFunction.FUNCTION_NAME, new MySQLIfNullFunction());
		SQLFunctionUtils.addSqlFunction(sfb, SQLIfElseFunction.FUNCTION_NAME, new MySQLIfElseFunction());
	}


	@Override
	public boolean supportsDialect(String dialect) {
		return dialect.startsWith("org.hibernate.dialect.MySQL");
	}
}
