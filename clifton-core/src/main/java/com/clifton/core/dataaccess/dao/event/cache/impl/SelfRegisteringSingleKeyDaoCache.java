package com.clifton.core.dataaccess.dao.event.cache.impl;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoSingleKeyCache;
import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>SelfRegisteringSingleKeyDaoCache</code> class is a helper class that should be extended by DAO caches that cache an entity based on 1 key property
 * Implementations of this class should be marked as @Component and will be automatically registered as listeners
 * for corresponding DAO's (T as DTO) for the specified events. (By default INSERT, UPDATE, AND DELETE)
 * <p>
 * Observes all MODIFY events:
 * - Inserts are observed to immediately cache the bean after the save. (Unless overridden to clear on all changes in which case the cache is always cleared for the key)
 * - Updates - if the key changes will clear the cache for the original key and cache the bean for the updated key (Unless overridden to clear on all changes then will also clear for the new key)
 * - Deletes are observed to clear the cache (for affected key)
 * <p>
 * This cache supports cases where the cache using a single property as its key, i.e. cache by name (See {@link com.clifton.core.dataaccess.dao.event.cache.impl.name.NamedEntityCache}
 *
 * @author Mary Anderson
 */
public abstract class SelfRegisteringSingleKeyDaoCache<T extends IdentityObject, K> extends SelfRegisteringDaoCache<T> implements DaoSingleKeyCache<T, K> {


	/**
	 * Returns the Bean Key Property Name
	 */
	protected abstract String getBeanKeyProperty();


	/**
	 * Return the Bean Key Property Value for the Bean
	 */
	protected abstract K getBeanKeyValue(T bean);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	protected final String[] getBeanKeyProperties() {
		return new String[]{getBeanKeyProperty()};
	}


	@Override
	protected final String getBeanKey(T bean) {
		return getBeanKeyForProperties(getBeanKeyValue(bean));
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * Return the Bean for the Given key value, Return Null if Not Found
	 */
	@Override
	public final T getBeanForKeyValue(ReadOnlyDAO<T> dao, K keyPropertyValue) {
		return getBeanImpl(dao, false, keyPropertyValue);
	}


	/**
	 * Return the Bean for the Given key value
	 *
	 * @throws ValidationException if bean is null
	 */
	@Override
	public final T getBeanForKeyValueStrict(ReadOnlyDAO<T> dao, K keyPropertyValue) {
		return getBeanImpl(dao, true, keyPropertyValue);
	}
}
