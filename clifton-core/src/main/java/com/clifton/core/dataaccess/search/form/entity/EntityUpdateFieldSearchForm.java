package com.clifton.core.dataaccess.search.form.entity;

import java.util.Date;


/**
 * The <code>EntityUpdateFieldSearchForm</code> interface declares getter and setter methods for the standard fields for entities which audit update events.
 * Implementors of this interface shall implement these fields to enable search form functionality.
 *
 * @author MikeH
 */
public interface EntityUpdateFieldSearchForm {


	Date getUpdateDate();


	void setUpdateDate(Date updateDate);


	Short getUpdateUserId();


	void setUpdateUserId(Short updateUserId);
}
