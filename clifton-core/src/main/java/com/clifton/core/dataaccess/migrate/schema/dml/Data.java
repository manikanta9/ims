package com.clifton.core.dataaccess.migrate.schema.dml;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.db.TableTypes;
import org.springframework.util.CollectionUtils;

import java.util.List;


/**
 * The <code>Data</code> class represents a single database row for the specified table.
 *
 * @author vgomelsky
 */
public class Data {

	private String table;
	private List<Value> valueList;

	/**
	 * Data inserts can be grouped with DDL or META DATA
	 * to customize the order of SQL execution for a file.
	 */
	private String runWith;

	private TableTypes tableType;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTable() {
		return this.table;
	}


	public void setTable(String table) {
		this.table = table;
	}


	public List<Value> getValueList() {
		return this.valueList;
	}


	public void setValueList(List<Value> valueList) {
		this.valueList = valueList;
	}


	public boolean isSelect() {
		List<Value> selectValues = BeanUtils.filter(getValueList(), Value::isSelect);
		return !CollectionUtils.isEmpty(selectValues);
	}


	public String getRunWith() {
		return this.runWith;
	}


	public void setRunWith(String runWith) {
		this.runWith = runWith;
	}


	public TableTypes getTableType() {
		return this.tableType;
	}


	public void setTableType(TableTypes tableType) {
		this.tableType = tableType;
	}
}
