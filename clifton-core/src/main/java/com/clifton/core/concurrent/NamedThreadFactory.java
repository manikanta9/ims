package com.clifton.core.concurrent;

import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * The <code>NamedThreadFactory</code> is an implementation of {@link ThreadFactory} that creates threads
 * that are named with a prefix and a number.
 *
 * @author jgommels
 */
public class NamedThreadFactory implements ThreadFactory {

	private final AtomicInteger counter = new AtomicInteger(0);
	private String prefix = "";


	public NamedThreadFactory(String prefix) {
		this.prefix = prefix;
	}


	@Override
	public Thread newThread(Runnable r) {
		return new Thread(r, this.prefix + "-" + this.counter.getAndIncrement());
	}


	public String getPrefix() {
		return this.prefix;
	}
}
