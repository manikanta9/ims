package com.clifton.core.concurrent;

import com.clifton.core.util.MathUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;


/**
 * @author jgommels
 */
public class ConcurrentUtils {

	/**
	 * Creates a bounded cached thread pool with a max work queue size of 100 and thread keep-alive time of 60 seconds.
	 *
	 * @see #newBoundedCachedThreadPool(String, int, int, int)
	 */
	public static ExecutorService newBoundedCachedThreadPool(String threadNamePrefix, int maxThreadPoolSize) {
		return newBoundedCachedThreadPool(threadNamePrefix, maxThreadPoolSize, 100, 60);
	}


	/**
	 * Creates a cached thread pool with a maximum size. Uses a {@link ArrayBlockingQueue} for the work queue.
	 *
	 * @param threadNamePrefix       the name prefix for the threads
	 * @param maxThreadPoolSize      the maximum size of the thread pool
	 * @param maxWorkQueueSize       the maximum size of the work queue. When the work queue is full, submissions to the executor will block until the queue is no longer full.
	 * @param keepAliveTimeInSeconds the number of seconds a thread in the pool can remain idle before being terminated
	 */
	public static ExecutorService newBoundedCachedThreadPool(String threadNamePrefix, int maxThreadPoolSize, int maxWorkQueueSize, int keepAliveTimeInSeconds) {
		/*
			We set the core pool size and max pool size to the same value, but allow core threads to time out. This allows the pool to dynamically grow
			to a maximum size while also allowing any threads to terminate when they become idle. See the ThreadPoolExecutor documentation for more info.
		 */
		ThreadPoolExecutor executor = new ThreadPoolExecutor(maxThreadPoolSize, maxThreadPoolSize,
				keepAliveTimeInSeconds, TimeUnit.SECONDS,
				new ArrayBlockingQueue<>(maxWorkQueueSize),
				new NamedThreadFactory(threadNamePrefix));

		//We need to allow core threads to time out.
		executor.allowCoreThreadTimeOut(true);

		return executor;
	}


	/**
	 * Returns an unconfigurable {@link ScheduledThreadPoolExecutor} with one core thread for scheduling tasks.
	 *
	 * @see #newScheduledExecutorService(String, int)
	 */
	public static ScheduledExecutorService newSingleThreadScheduledExecutorService(String threadNamePrefix) {
		return newScheduledExecutorService(threadNamePrefix, 1);
	}


	/**
	 * Returns an unconfigurable {@link ScheduledThreadPoolExecutor} with a specified number of core threads for scheduling tasks.
	 * <p>
	 * The scheduled executor is configured with {@link ScheduledThreadPoolExecutor#setRemoveOnCancelPolicy(boolean)} with true to
	 * immediately remove canceled tasks from the queue to prevent unintended memory leaks.
	 */
	public static ScheduledExecutorService newScheduledExecutorService(String threadNamePrefix, int coreThreadPoolSize) {
		ScheduledThreadPoolExecutor threadPoolExecutor = new ScheduledThreadPoolExecutor(coreThreadPoolSize, new NamedThreadFactory(threadNamePrefix));
		threadPoolExecutor.setRemoveOnCancelPolicy(true); // remove the cancelled tasks from executor queue
		return Executors.unconfigurableScheduledExecutorService(threadPoolExecutor);
	}


	/**
	 * Shutdowns the provided {@link ExecutorService} and returns true if the service was successfully shutdown.
	 * Invokes {@link ConcurrentUtils#shutdownExecutorService(ExecutorService, long, int)} with a single 5 second wait.
	 */
	public static boolean shutdownExecutorService(ExecutorService executorService) {
		return shutdownExecutorService(executorService, 5, 1);
	}


	/**
	 * Shutdowns the provided {@link ExecutorService} and returns if the service was successfully shutdown. The service provided
	 * will be signalled shutdown before validating the provided wait criteria.
	 *
	 * @param executorService            service to shutdown
	 * @param awaitTerminationSeconds    seconds to wait for the service to shutdown; allowed values between 0 and 30
	 * @param awaitTerminationIterations number of times to wait before returning; allowed values between 0 and 10
	 * @return true if executorService is null or executorService becomes shutdown within the provided wait criteria; false otherwise
	 */
	public static boolean shutdownExecutorService(ExecutorService executorService, long awaitTerminationSeconds, int awaitTerminationIterations) {
		if (executorService == null) {
			return true;
		}
		executorService.shutdown();
		ValidationUtils.assertTrue(MathUtils.isBetween(awaitTerminationSeconds, 0, 30), "Invalid wait seconds provided: " + awaitTerminationIterations);
		ValidationUtils.assertTrue(MathUtils.isBetween(awaitTerminationIterations, 0, 10), "Invalid wait iterations provided: " + awaitTerminationIterations);
		for (int i = 0; i < awaitTerminationIterations; i++) {
			try {
				if (executorService.awaitTermination(awaitTerminationSeconds, TimeUnit.SECONDS)) {
					return true;
				}
			}
			catch (InterruptedException e) {
				executorService.shutdownNow();
				Thread.currentThread().interrupt();
			}
		}
		return false;
	}
}
