package com.clifton.core.web.view;

import org.springframework.web.servlet.RequestToViewNameTranslator;
import org.springframework.web.servlet.view.DefaultRequestToViewNameTranslator;

import javax.servlet.http.HttpServletRequest;


/**
 * The {@link DynamicRequestToViewNameTranslator} view name translator assigns the {@link DynamicViewResolver} as the default view to use when one is not yet explicitly specified
 * in the request model.
 * <p>
 * By default, the {@link DefaultRequestToViewNameTranslator} uses the request path to determine the view name to use. This can be helpful for applications with many different
 * views. However, in our case, almost all views are resolved by the {@link DynamicViewResolver}.
 *
 * @author MikeH
 * @see DefaultRequestToViewNameTranslator
 * @see DynamicViewResolver
 */
public class DynamicRequestToViewNameTranslator implements RequestToViewNameTranslator {

	@Override
	public String getViewName(HttpServletRequest request) {
		return DynamicViewResolver.DYNAMIC_VIEW_NAME;
	}
}
