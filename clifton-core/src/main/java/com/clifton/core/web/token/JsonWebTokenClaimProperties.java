package com.clifton.core.web.token;

/**
 * A list of spring security OAuth properties and sub-properties that could be used
 * in a JWT. They act as keys in the JSON object.
 * <p>
 * For more information on Claims see here: https://tools.ietf.org/html/rfc7519#section-4
 *
 * @author theodorez
 */
public enum JsonWebTokenClaimProperties {
	CLIENT_ID("client_id"),
	USERNAME("user_name"),
	AUTHORITIES("authorities"),
	SCOPE("scope"),
	IMPERSONATION_USER_SCOPE("ACT_AS_USER");


	private String claimName;


	JsonWebTokenClaimProperties(String claimName) {
		this.claimName = claimName;
	}


	public String getClaimName() {
		return this.claimName;
	}
}
