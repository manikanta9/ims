package com.clifton.core.web.stats;


import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.CacheStats;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import org.springframework.beans.factory.BeanFactoryUtils;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * The <code>SystemRequestStatsServiceImpl</code> class provides basic implementation of SystemRequestStatsService interface
 *
 * @author vgomelsky
 */
@Service
public class SystemRequestStatsServiceImpl implements SystemRequestStatsService, InitializingBean, ApplicationContextAware {

	/**
	 * using a read write lock allows the statsMap to be locked for update when it is being cleared --
	 * preventing other threads from adding to it during the process, When items in the map are being updated, the
	 * statMap gets a read lock to prevent a concurrent thread from clearing it.
	 */
	private ReentrantReadWriteLock readWriteLock = new ReentrantReadWriteLock();
	private Date statsLastCleared = new Date();
	private final Map<String, SystemRequestStats> statsMap = new ConcurrentHashMap<>();
	/**
	 * The collection of rules which should be applied to logged URIs.
	 */
	private Collection<SystemRequestStatsUriRule> uriRuleList = Collections.emptyList();


	/**
	 * The collection of rules which should be applied to filter WEB request stats.
	 */
	private Collection<SystemRequestStatsStatusExcludeRule> statusExcludeRuleList = Collections.emptyList();


	@Override
	public void systemRequestExclusiveLock() {
		getReadWriteLock().writeLock().lock();
	}


	@Override
	public void systemRequestExclusiveUnlock() {
		getReadWriteLock().writeLock().unlock();
	}


	/**
	 * Max number of slowest requests to collect detail stats about.
	 */
	private int slowestMaxCount = 20;

	/**
	 * Min request duration after which it's considered slow.
	 */
	private long slowestMinNano = 1_000_000_000;

	private CacheHandler<?, ?> cacheHandler;

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void afterPropertiesSet() {
		// Manually autowire properties by type since we default to autowiring properties by name
		Collection<SystemRequestStatsUriRule> ruleBeanList = BeanFactoryUtils.beansOfTypeIncludingAncestors(getApplicationContext(), SystemRequestStatsUriRule.class).values();
		setUriRuleList(ruleBeanList);

		Collection<SystemRequestStatsStatusExcludeRule> statusExcludeRuleBeanList = BeanFactoryUtils.beansOfTypeIncludingAncestors(getApplicationContext(), SystemRequestStatsStatusExcludeRule.class).values();
		setStatusExcludeRuleList(statusExcludeRuleBeanList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////                 Web Request Stats                  ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date getSystemStatsLastCleared() {
		return getStatsLastCleared();
	}


	@Override
	public List<SystemRequestStats> getSystemRequestStatsList(AuditableEntitySearchForm searchForm) {
		Collection<SystemRequestStats> fullList;
		Lock readLock = getReadWriteLock().readLock();
		readLock.lock();
		try {
			fullList = this.statsMap.values();

			if (searchForm != null && !CollectionUtils.isEmpty(searchForm.getRestrictionList())) {
				// implement some simplistic server side filtering to minimize amount of data returned
				SearchRestriction requestSource = searchForm.getSearchRestriction("requestSource");
				SearchRestriction requestURI = searchForm.getSearchRestriction("requestURI");
				if (requestSource != null || requestURI != null) {
					Stream<SystemRequestStats> result = fullList.stream();
					if (requestSource != null) {
						result = result.filter(stats -> StringUtils.isLikeIgnoringCase(stats.getRequestSource(), (String) requestSource.getValue()));
					}
					if (requestURI != null) {
						result = result.filter(stats -> StringUtils.isLikeIgnoringCase(stats.getRequestURI(), (String) requestURI.getValue()));
					}
					return result.collect(Collectors.toList());
				}
			}
		}
		finally {
			readLock.unlock();
		}

		return new ArrayList<>(fullList);
	}


	@Override
	public void saveSystemRequestStats(HttpServletRequest request, HttpServletResponse response, Object user, long requestTimeNano, int dbHits, long dbTimeNano) {
		String requestSource = "WEB";
		for (SystemRequestStatsStatusExcludeRule rule : getStatusExcludeRuleList()) {
			if (!rule.matches(response)) {
				return;
			}
		}
		String requestURI = getLoggedUri(request);
		// Skip empty and excluded URIs
		if (requestURI != null) {
			saveSystemRequestStats(requestSource, requestURI, request, user, requestTimeNano, dbHits, dbTimeNano);
		}
	}


	@Override
	public void saveSystemRequestStats(String requestSource, String requestURI, Object user, long requestTimeNano, int dbHits, long dbTimeNano) {
		saveSystemRequestStats(requestSource, requestURI, null, user, requestTimeNano, dbHits, dbTimeNano);
	}


	private void saveSystemRequestStats(String requestSource, String requestURI, HttpServletRequest request, Object user, long requestTimeNano, int dbHits, long dbTimeNano) {
		String key = requestSource + requestURI;
		SystemRequestStats stats = this.statsMap.computeIfAbsent(key, k -> new SystemRequestStats(requestSource, requestURI, getSlowestMaxCount(), getSlowestMinNano()));

		// lock to prevent concurrent updates?
		Lock readLock = getReadWriteLock().readLock();
		readLock.lock();
		try {
			//noinspection SynchronizationOnLocalVariableOrMethodParameter
			synchronized (stats) {
				if (stats.addRequestExecution(requestTimeNano, dbHits, dbTimeNano)) {
					// new slow request found: log it
					StringBuilder parameters = new StringBuilder();
					if (user != null) {
						parameters.append(user);
						parameters.append(": ");
					}
					if (request != null) {
						parameters.append(convertRequestToString(request));
					}
					SystemRequestStatsDetail detail = new SystemRequestStatsDetail(requestTimeNano, dbHits, dbTimeNano, parameters.length() > 0 ? parameters.toString() : null, (stats.getExecuteCount() == 1));
					stats.addSystemRequestStatsDetail(detail);
				}
			}
		}
		finally {
			readLock.unlock();
		}
	}


	@Override
	public void deleteSystemRequestStats(String requestSource, String requestURI) {
		String key = requestSource + requestURI;
		Lock readLock = getReadWriteLock().readLock();
		readLock.lock();
		try {
			this.statsMap.remove(key);
		}
		finally {
			readLock.unlock();
		}
	}


	@Override
	public void deleteSystemRequestStatsAll() {
		Lock writeLock = getReadWriteLock().writeLock();
		writeLock.lock();
		try {
			this.statsMap.clear();
			setStatsLastCleared(new Date());
		}
		finally {
			writeLock.unlock();
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////                 Caching and Memory                 ////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int setSystemCacheStatsEnabled(String cacheName, boolean enableStatistics) {
		return getCacheHandler().setStatisticsEnabled(cacheName, enableStatistics);
	}


	@Override
	public List<CacheStats> getSystemCacheStatsList(boolean calculateSize) {
		return getCacheHandler().getCacheStats(calculateSize);
	}


	@Override
	public CacheStats getSystemCacheStatsForName(String cacheName) {
		return getCacheHandler().getCacheStats(cacheName);
	}


	@Override
	public void deleteSystemCache(String cacheName) {
		getCacheHandler().clear(cacheName);
	}


	@Override
	public void deleteSystemCacheAll() {
		getCacheHandler().clearAll();
	}


	@Override
	public MemoryState getSystemMemoryState() {
		System.gc();
		Runtime runtime = Runtime.getRuntime();
		MemoryState result = new MemoryState();
		result.setFreeMemory(runtime.freeMemory());
		result.setTotalMemory(runtime.totalMemory());
		result.setMaxMemory(runtime.maxMemory());
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String convertRequestToString(HttpServletRequest request) {
		StringBuilder result = new StringBuilder();

		Enumeration<String> parameters = request.getParameterNames();
		boolean firstParameter = true;
		if (parameters != null) {
			while (parameters.hasMoreElements()) {
				String key = parameters.nextElement();
				if (!firstParameter) {
					result.append("; ");
				}
				result.append(key);
				result.append('=');
				String[] parameterValues = request.getParameterValues(key);
				if (parameterValues != null && parameterValues.length > 0) {
					if (parameterValues.length > 1) {
						boolean firstValue = true;
						for (String parameterValue : parameterValues) {
							if (!firstValue) {
								result.append(',');
							}
							result.append(parameterValue);
							firstValue = false;
						}
					}
					else {
						result.append(parameterValues[0]);
					}
				}
				firstParameter = false;
			}
		}
		return result.toString();
	}


	/**
	 * Gets the processed URI to be logged from the given URI.
	 * <p>
	 * This applies {@link SystemRequestStatsUriRule} rules to the given URI.
	 */
	private String getLoggedUri(HttpServletRequest request) {
		// Normalize URIs
		String requestUri = request.getRequestURI();
		// Trim context URI if present
		String contextUri = requestUri.startsWith(request.getContextPath())
				? requestUri.substring(request.getContextPath().length())
				: requestUri;
		String normalizedUri = contextUri.replaceAll("^/+", "/");

		// Find the longest rule that matches the given URI
		Optional<SystemRequestStatsUriRule> matchingRule = CollectionUtils.getStream(getUriRuleList())
				.filter(rule -> rule.matches(normalizedUri))
				.max((rule1, rule2) -> CompareUtils.compare(rule1.getUriPattern().length(), rule2.getUriPattern().length()));

		// Apply the rule
		return matchingRule.isPresent()
				? matchingRule.get().apply(normalizedUri)
				: normalizedUri;
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public ReentrantReadWriteLock getReadWriteLock() {
		return this.readWriteLock;
	}


	public void setReadWriteLock(ReentrantReadWriteLock readWriteLock) {
		this.readWriteLock = readWriteLock;
	}


	public Date getStatsLastCleared() {
		return this.statsLastCleared;
	}


	public void setStatsLastCleared(Date statsLastCleared) {
		this.statsLastCleared = statsLastCleared;
	}


	public int getSlowestMaxCount() {
		return this.slowestMaxCount;
	}


	public void setSlowestMaxCount(int slowestMaxCount) {
		this.slowestMaxCount = slowestMaxCount;
	}


	public long getSlowestMinNano() {
		return this.slowestMinNano;
	}


	public void setSlowestMinNano(long slowestMinNano) {
		this.slowestMinNano = slowestMinNano;
	}


	public CacheHandler<?, ?> getCacheHandler() {
		return this.cacheHandler;
	}


	public void setCacheHandler(CacheHandler<?, ?> cacheHandler) {
		this.cacheHandler = cacheHandler;
	}


	public Collection<SystemRequestStatsUriRule> getUriRuleList() {
		return this.uriRuleList;
	}


	public void setUriRuleList(Collection<SystemRequestStatsUriRule> uriRuleList) {
		this.uriRuleList = uriRuleList;
	}


	public Collection<SystemRequestStatsStatusExcludeRule> getStatusExcludeRuleList() {
		return this.statusExcludeRuleList;
	}


	public void setStatusExcludeRuleList(Collection<SystemRequestStatsStatusExcludeRule> statusExcludeRuleList) {
		this.statusExcludeRuleList = statusExcludeRuleList;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}
}
