package com.clifton.core.web.view;


/**
 * The <code>FileUploadView</code> is the view to use when using forms that also upload a file into the system.
 * <p>
 * There is currently no difference between this view and the standard {@link JsonView}.
 * <p>
 * Historically, this view would change the response type to <code>"text/html"</code> so that the response could be properly parsed on the client-side by ExtJS, but this is no
 * longer necessary. Browsers wrap <code>"application/json"</code> form responses with <code> "&lt;pre&gt;"</code> tags which is not handled by ExtJS by default, but this custom
 * functionality has been added in order to avoid browser DOM auto-correction issues.
 *
 * @author manderson
 */
public class FileUploadView extends JsonView {
}
