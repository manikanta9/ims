package com.clifton.core.web.mvc;


import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.http.MediaType;

import javax.servlet.AsyncContext;
import javax.servlet.DispatcherType;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpUpgradeHandler;
import javax.servlet.http.Part;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.Principal;
import java.util.Collection;
import java.util.Collections;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;


/**
 * The <code>HttpServletRequestImpl</code> is an internal class that provides an HttpServletRequest which only
 * implements the url and parameter methods needed to call the spring MVC methods.
 *
 * @author mwacker
 */
class HttpServletRequestImpl implements HttpServletRequest {

	private final Map<String, String[]> parameters = new LinkedHashMap<>(16);

	private String contextPath = "/";
	private String servletPath = "";
	private String requestURI;


	public HttpServletRequestImpl(String path) {
		this.servletPath = path;
		this.requestURI = path;
	}


	/**
	 * Set a single value for the specified HTTP parameter.
	 * <p/>
	 * If there are already one or more values registered for the given
	 * parameter name, they will be replaced.
	 */
	public void setParameter(String name, String value) {
		setParameter(name, new String[]{value});
	}


	/**
	 * Set an array of values for the specified HTTP parameter.
	 * <p/>
	 * If there are already one or more values registered for the given
	 * parameter name, they will be replaced.
	 */
	public void setParameter(String name, String[] values) {
		ValidationUtils.assertNotNull(name, "Parameter name must not be null");
		this.parameters.put(name, values);
	}


	/**
	 * Sets all provided parameters <emphasis>replacing</emphasis> any existing
	 * values for the provided parameter names. To add without replacing
	 * existing values, use {@link #addParameters(java.util.Map)}.
	 */
	@SuppressWarnings("rawtypes")
	public void setParameters(Map params) {
		ValidationUtils.assertNotNull(params, "Parameter map must not be null");
		for (Object o : params.entrySet()) {
			ValidationUtils.assertInstanceOf(String.class, ((Map.Entry) o).getKey(), "Parameter map key must be of type [" + String.class.getName() + "]");
			String key = (String) ((Map.Entry) o).getKey();
			Object value = ((Map.Entry) o).getValue();
			if (value instanceof String) {
				this.setParameter(key, (String) value);
			}
			else if (value instanceof String[]) {
				this.setParameter(key, (String[]) value);
			}
			else {
				throw new IllegalArgumentException("Parameter map value must be single value " + " or array of type [" + String.class.getName() + "]");
			}
		}
	}


	/**
	 * Add a single value for the specified HTTP parameter.
	 * <p/>
	 * If there are already one or more values registered for the given
	 * parameter name, the given value will be added to the end of the list.
	 */
	public void addParameter(String name, String value) {
		addParameter(name, new String[]{value});
	}


	/**
	 * Add an array of values for the specified HTTP parameter.
	 * <p/>
	 * If there are already one or more values registered for the given
	 * parameter name, the given values will be added to the end of the list.
	 */
	public void addParameter(String name, String[] values) {
		ValidationUtils.assertNotNull(name, "Parameter name must not be null");
		String[] oldArr = this.parameters.get(name);
		if (oldArr != null) {
			String[] newArr = new String[oldArr.length + values.length];
			System.arraycopy(oldArr, 0, newArr, 0, oldArr.length);
			System.arraycopy(values, 0, newArr, oldArr.length, values.length);
			this.parameters.put(name, newArr);
		}
		else {
			this.parameters.put(name, values);
		}
	}


	/**
	 * Adds all provided parameters <emphasis>without</emphasis> replacing any
	 * existing values. To replace existing values, use
	 * {@link #setParameters(java.util.Map)}.
	 */
	@SuppressWarnings("rawtypes")
	public void addParameters(Map params) {
		ValidationUtils.assertNotNull(params, "Parameter map must not be null");
		for (Object o : params.entrySet()) {
			ValidationUtils.assertInstanceOf(String.class, ((Map.Entry) o).getKey(), "Parameter map key must be of type [" + String.class.getName() + "]");
			String key = (String) ((Map.Entry) o).getKey();
			Object value = ((Map.Entry) o).getValue();
			if (value instanceof String) {
				this.addParameter(key, (String) value);
			}
			else if (value instanceof String[]) {
				this.addParameter(key, (String[]) value);
			}
			else {
				throw new IllegalArgumentException("Parameter map value must be single value " + " or array of type [" + String.class.getName() + "]");
			}
		}
	}


	/**
	 * Remove already registered values for the specified HTTP parameter, if
	 * any.
	 */
	public void removeParameter(String name) {
		ValidationUtils.assertNotNull(name, "Parameter name must not be null");
		this.parameters.remove(name);
	}


	/**
	 * Removes all existing parameters.
	 */
	public void removeAllParameters() {
		this.parameters.clear();
	}


	@Override
	public String getParameter(String name) {
		ValidationUtils.assertNotNull(name, "Parameter name must not be null");
		String[] arr = this.parameters.get(name);
		return (arr != null && arr.length > 0 ? arr[0] : null);
	}


	@Override
	public Enumeration<String> getParameterNames() {
		return Collections.enumeration(this.parameters.keySet());
	}


	@Override
	public String[] getParameterValues(String name) {
		ValidationUtils.assertNotNull(name, "Parameter name must not be null");
		return this.parameters.get(name);
	}


	@Override
	public Map<String, String[]> getParameterMap() {
		return Collections.unmodifiableMap(this.parameters);
	}


	@Override
	public String getContextPath() {
		return this.contextPath;
	}


	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}


	@Override
	public String getServletPath() {
		return this.servletPath;
	}


	public void setServletPath(String servletPath) {
		this.servletPath = servletPath;
	}


	@Override
	public String getRequestURI() {
		return this.requestURI;
	}


	public void setRequestURI(String requestURI) {
		this.requestURI = requestURI;
	}


	@Override
	public Object getAttribute(String arg0) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Enumeration<String> getAttributeNames() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getCharacterEncoding() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public int getContentLength() {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public long getContentLengthLong() {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public String getContentType() {
		// Auto-generated method stub
		return MediaType.APPLICATION_FORM_URLENCODED_VALUE;
	}


	@Override
	public ServletInputStream getInputStream() throws IOException {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getLocalAddr() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getLocalName() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public int getLocalPort() {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public ServletContext getServletContext() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public AsyncContext startAsync() throws IllegalStateException {
		// Auto-generated method stub
		return null;
	}


	@Override
	public AsyncContext startAsync(ServletRequest servletRequest, ServletResponse servletResponse) throws IllegalStateException {
		// Auto-generated method stub
		return null;
	}


	@Override
	public boolean isAsyncStarted() {
		// Auto-generated method stub
		return false;
	}


	@Override
	public boolean isAsyncSupported() {
		// Auto-generated method stub
		return false;
	}


	@Override
	public AsyncContext getAsyncContext() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public DispatcherType getDispatcherType() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Locale getLocale() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Enumeration<Locale> getLocales() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getProtocol() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public BufferedReader getReader() throws IOException {
		// Auto-generated method stub
		return null;
	}


	@Deprecated
	@Override
	public String getRealPath(String arg0) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getRemoteAddr() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getRemoteHost() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public int getRemotePort() {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public RequestDispatcher getRequestDispatcher(String arg0) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getScheme() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getServerName() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public int getServerPort() {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public boolean isSecure() {
		// Auto-generated method stub
		return false;
	}


	@Override
	public void removeAttribute(String arg0) {
		// Auto-generated method stub

	}


	@Override
	public void setAttribute(String arg0, Object arg1) {
		// Auto-generated method stub

	}


	@Override
	public void setCharacterEncoding(String arg0) throws UnsupportedEncodingException {
		// Auto-generated method stub

	}


	@Override
	public String getAuthType() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Cookie[] getCookies() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public long getDateHeader(String arg0) {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public String getHeader(String arg0) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Enumeration<String> getHeaderNames() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Enumeration<String> getHeaders(String arg0) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public int getIntHeader(String arg0) {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public String getMethod() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getPathInfo() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getPathTranslated() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getQueryString() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getRemoteUser() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public StringBuffer getRequestURL() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String getRequestedSessionId() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public HttpSession getSession() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public String changeSessionId() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public HttpSession getSession(boolean arg0) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Principal getUserPrincipal() {
		// Auto-generated method stub
		return null;
	}


	@Override
	public boolean isRequestedSessionIdFromCookie() {
		// Auto-generated method stub
		return false;
	}


	@Override
	public boolean isRequestedSessionIdFromURL() {
		// Auto-generated method stub
		return false;
	}


	@Deprecated
	@Override
	public boolean isRequestedSessionIdFromUrl() {
		// Auto-generated method stub
		return false;
	}


	@Override
	public boolean authenticate(HttpServletResponse response) throws IOException, ServletException {
		// Auto-generated method stub
		return false;
	}


	@Override
	public void login(String username, String password) throws ServletException {
		// Auto-generated method stub
	}


	@Override
	public void logout() throws ServletException {
		// Auto-generated method stub
	}


	@Override
	public Collection<Part> getParts() throws IOException, ServletException {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Part getPart(String name) throws IOException, ServletException {
		// Auto-generated method stub
		return null;
	}


	@Override
	public <T extends HttpUpgradeHandler> T upgrade(Class<T> handlerClass) throws IOException, ServletException {
		// Auto-generated method stub
		return null;
	}


	@Override
	public boolean isRequestedSessionIdValid() {
		// Auto-generated method stub
		return false;
	}


	@Override
	public boolean isUserInRole(String arg0) {
		// Auto-generated method stub
		return false;
	}
}
