package com.clifton.core.web.api.client.openapi;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.ManagedList;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;

import java.util.List;
import java.util.Objects;
import java.util.Set;


/**
 * The {@link OpenApiClientBeanFactoryPostProcessor} finds OpenApi 'Api' classes to configure and register them as beans.
 *
 * @author lnaylor
 */
public class OpenApiClientBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

	/**
	 * The list of configurer bean names that will be used to set up the rest template passed to the ApiClient
	 */

	private List<String> restTemplateConfigurerBeanNameList;

	/**
	 * The base path url where the Api should send requests
	 */
	private String basePathUrl;


	/**
	 * The package name to search for Api classes
	 */
	private String packageName;

	/**
	 * The prefix that should be attached to the name of the registered bean
	 */
	private String beanPrefix;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		String apiClientBeanDefinitionClassName = getPackageName() + ".invoker.ApiClient";
		AssertUtils.assertTrue(CoreClassUtils.isClassPresent(apiClientBeanDefinitionClassName), "No ApiClient class exists for package " + getPackageName());

		BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;

		ManagedList<RuntimeBeanReference> restTemplateConfigurerList = new ManagedList<>();
		restTemplateConfigurerList.addAll(CollectionUtils.getConverted(getRestTemplateConfigurerBeanNameList(), RuntimeBeanReference::new));
		BeanDefinition restTemplateDefinition = BeanDefinitionBuilder.genericBeanDefinition(ConfigurableRestTemplateFactoryBean.class)
				.addPropertyValue("restTemplateConfigurerList", restTemplateConfigurerList)
				.getBeanDefinition();
		BeanDefinition apiClientBeanDefinitionWithProperties = BeanDefinitionBuilder.genericBeanDefinition(apiClientBeanDefinitionClassName)
				.addConstructorArgValue(restTemplateDefinition)
				.addPropertyValue("basePath", getBasePathUrl())
				.getBeanDefinition();
		String apiClientBeanName = getBeanNameForClass(apiClientBeanDefinitionClassName);
		registry.registerBeanDefinition(apiClientBeanName, apiClientBeanDefinitionWithProperties);

		ClassPathScanningCandidateComponentProvider openApiClassPathScanner = new ClassPathScanningCandidateComponentProvider(false);
		openApiClassPathScanner.addIncludeFilter((metadataReader, metadataReaderFactory) -> metadataReader.getClassMetadata().getClassName().endsWith("Api"));
		Set<BeanDefinition> apiList = openApiClassPathScanner.findCandidateComponents(getPackageName());
		for (BeanDefinition apiDefinition : CollectionUtils.getIterable(apiList)) {
			String apiDefinitionBeanClassName = apiDefinition.getBeanClassName();
			AssertUtils.assertNotNull(apiDefinitionBeanClassName, "Unable to get a bean class name for Api definition in package " + getPackageName());
			BeanDefinition apiBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(Objects.requireNonNull(apiDefinitionBeanClassName))
					.addConstructorArgReference(apiClientBeanName)
					.getBeanDefinition();
			registry.registerBeanDefinition(getBeanNameForClass(apiDefinition.getBeanClassName()), apiBeanDefinition);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getBeanNameForClass(String beanClassName) {
		return StringUtils.isEmpty(getBeanPrefix())
				? ContextConventionUtils.getBeanNameFromClassName(beanClassName)
				: getBeanPrefix() + StringUtils.capitalize(ContextConventionUtils.getBeanNameFromClassName(beanClassName));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<String> getRestTemplateConfigurerBeanNameList() {
		return this.restTemplateConfigurerBeanNameList;
	}


	public void setRestTemplateConfigurerBeanNameList(List<String> restTemplateConfigurerBeanNameList) {
		this.restTemplateConfigurerBeanNameList = restTemplateConfigurerBeanNameList;
	}


	public String getBasePathUrl() {
		return this.basePathUrl;
	}


	public void setBasePathUrl(String basePathUrl) {
		this.basePathUrl = basePathUrl;
	}


	public String getPackageName() {
		return this.packageName;
	}


	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}


	public String getBeanPrefix() {
		return this.beanPrefix;
	}


	public void setBeanPrefix(String beanPrefix) {
		this.beanPrefix = beanPrefix;
	}
}
