package com.clifton.core.web.api.client;

import org.springframework.beans.factory.annotation.AnnotatedBeanDefinition;
import org.springframework.context.annotation.ClassPathScanningCandidateComponentProvider;
import org.springframework.core.type.classreading.MetadataReader;

import java.io.IOException;


/**
 * The <code>ExternalServiceClassPathScanner</code> is used to filter scanned classes for proxying
 * to only those that end with our 'Service' naming convention; the second check is to allow for
 * interfaces to be returned during the scan which are not be default.
 *
 * @author StevenF
 */
public class ExternalServiceClassPathScanner extends ClassPathScanningCandidateComponentProvider {

	public ExternalServiceClassPathScanner(final boolean useDefaultFilters) {
		super(useDefaultFilters);
	}


	@Override
	protected boolean isCandidateComponent(MetadataReader metadataReader) throws IOException {
		return metadataReader.getClassMetadata().isInterface() && metadataReader.getClassMetadata().getClassName().endsWith("Service");
	}


	/**
	 * Since we already check for if the class is an interface and ends with 'Service' in the first candidate check
	 * we simply return true here as we've vetted our candidates as much as we need to already.
	 */
	@Override
	protected boolean isCandidateComponent(AnnotatedBeanDefinition beanDefinition) {
		return true;
	}
}
