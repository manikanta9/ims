package com.clifton.core.web.stats;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Objects;


/**
 * A rule for filtering WEB system request stats based on the status code of the response.
 *
 * @author lnaylor
 */
public class SystemRequestStatsStatusExcludeRule {

	/**
	 * The list of status codes to filter on.
	 */
	private final List<Integer> statusCodeFilterList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemRequestStatsStatusExcludeRule(List<Integer> statusCodeFilterList) {
		this.statusCodeFilterList = Objects.requireNonNull(statusCodeFilterList);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@code true} if the response status code should be included (i.e., the SystemRequestStat should be saved)
	 */
	public boolean matches(HttpServletResponse response) {
		return !getStatusCodeFilterList().contains(response.getStatus());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<Integer> getStatusCodeFilterList() {
		return this.statusCodeFilterList;
	}
}
