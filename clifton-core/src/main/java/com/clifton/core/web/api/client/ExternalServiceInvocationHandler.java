package com.clifton.core.web.api.client;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.oauth.SecurityOAuthUserTokenService;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.http.BadStatusException;
import com.clifton.core.util.http.HttpEntityFactory;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.core.util.http.MultipartEntityBuilderUtils;
import com.clifton.core.util.http.SimpleHttpClientFactory;
import com.clifton.core.util.http.SimpleHttpResponse;
import com.fasterxml.jackson.databind.JavaType;
import org.aopalliance.intercept.MethodInterceptor;
import org.aopalliance.intercept.MethodInvocation;
import org.apache.http.Header;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.util.EntityUtils;
import org.springframework.core.MethodParameter;
import org.springframework.security.oauth2.client.resource.OAuth2AccessDeniedException;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The class <code>ExternalServiceInvocationHandler</code> uses AOP to intercept the method calls
 * to proxied service and make an application/json call to an external application.
 *
 * @author StevenF
 */
public class ExternalServiceInvocationHandler implements MethodInterceptor {

	private String externalServiceURL;
	private Map<String, Class<?>> interfaceNameToImplementationClassMap;

	private ContextHandler contextHandler;
	private JacksonHandlerImpl jacksonHandler;
	private SecurityOAuthUserTokenService securityOAuthUserTokenService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object invoke(MethodInvocation methodInvocation) throws Throwable {
		// Ability to not attempt to connect if it's not configured in the environment
		if ("DO_NOT_USE".equals(getExternalServiceURL())) {
			return null;
		}

		Method method = methodInvocation.getMethod();
		if (getInterfaceNameToImplementationClassMap() != null) {
			//Check if we have an implementation class and pull the method from there.
			Class<?> implementationClass = getInterfaceNameToImplementationClassMap().get(method.getDeclaringClass().getName());
			Method implementationMethod = MethodUtils.getMethod(implementationClass, method.getName(), method.getParameterTypes());
			if (implementationMethod != null) {
				method = implementationMethod;
			}
		}

		String url = getExternalServiceURL() + ContextConventionUtils.API_ROOT + ContextConventionUtils.getUrlFromMethod(method, true);
		Object[] arguments = methodInvocation.getArguments();
		boolean multipartFileRequest = isMultipartFileRequest(arguments);

		HttpPost request = new HttpPost(url);
		request.setEntity(createRequestEntityForArguments(methodInvocation, multipartFileRequest));
		if (!multipartFileRequest) {
			request.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString());
		}

		HttpClientContext context = HttpClientContext.create();
		context.setAttribute("endpointUrl", getExternalServiceURL());
		try (SimpleHttpResponse response = executeRequest(request, context)) {
			return handleResponse(response, method);
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to deserialize response for URL: " + url, e);
		}
	}


	private SimpleHttpResponse executeRequest(HttpUriRequest request, HttpClientContext context) throws Throwable {
		return executeRequest(request, context, false);
	}


	/**
	 * The execute method will catch a bad status exception once and do a single retry in which it will clear the user token cache in
	 * order to create a new one - this is meant to handle situations in which the target server has been restarted, but the client
	 * server is still holding on to an invalid token.  If the second request fails the error is thrown back.
	 */
	private SimpleHttpResponse executeRequest(HttpUriRequest request, HttpClientContext context, boolean clearTokenCache) throws Throwable {
		try {
			String bearerToken = getSecurityOAuthUserTokenService().getTokenForCurrentUserIfPresent(String.valueOf(context.getAttribute("endpointUrl")), clearTokenCache);
			if (bearerToken != null) {
				request.setHeader("Authorization", "Bearer " + bearerToken);
			}
			//third boolean parameter here is not the 'clearTokenCache' but is used to determine if the SimpleHttpClient should handle the response status check or not.
			//In our case, since the external service returns it's own json and the exceptions are wrapped so we can deserialize properly we want to check the status ourselves.
			SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(request, context, false);
			if (response.getStatusCode() != HttpServletResponse.SC_OK) {
				handleErrorResponse(response);
			}
			return response;
		}
		catch (Exception e) {
			if (!clearTokenCache && (e instanceof BadStatusException || e.getCause() instanceof BadStatusException)) {
				//Clear the token cache and try the request one more time
				return executeRequest(request, context, true);
			}
			else if (e instanceof OAuth2AccessDeniedException || e.getCause() instanceof OAuth2AccessDeniedException) {
				//The Illegal State Exception here happens when the token returned from the remote server is empty.
				//This is due to an error happening on the remote system and a token not being returned.
				//Most commonly this is thrown when a user does not exist on the remote system.
				throw new RuntimeException("Current user is not authorized to access the remote server: " + request.getURI() + ". Check the remote system logs for more information.");
			}
			throw e;
		}
	}


	private boolean isMultipartFileRequest(Object[] arguments) {
		for (Object argument : arguments) {
			if (argument != null) {
				return isMultipartArgument(argument);
			}
		}
		return false;
	}


	//TODO - rather than checking the return type, have the class implement some sort of interface that forces getFile/setFile for multipart?
	//TODO - the argument resolver is dependent on setFile/getFile being the method names so this should be enforced
	private boolean isMultipartArgument(Object argument) {
		List<Method> methods = MethodUtils.getAllDeclaredMethods(argument.getClass());
		for (Method m : methods) {
			if (MultipartFile.class.isAssignableFrom(m.getReturnType())) {
				return true;
			}
		}
		return false;
	}


	private HttpEntity createRequestEntityForArguments(MethodInvocation invocation, boolean multipartFileRequest) {
		Method method = invocation.getMethod();
		Object[] arguments = invocation.getArguments();
		Class<?>[] parameterTypes = method.getParameterTypes();
		List<String> paramNames = CollectionUtils.getStream(Arrays.asList(method.getParameters())).map(Parameter::getName).collect(Collectors.toList());

		if (multipartFileRequest) {
			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			for (int i = 0; i < paramNames.size(); i++) {
				try {
					String paramName = paramNames.get(i);
					Object argument = arguments[i];
					Class<?> parameterType = parameterTypes[i];
					if (argument != null && isMultipartArgument(argument)) {
						Method getFileMethod = MethodUtils.getMethod(argument.getClass(), "getFile");
						MultipartFile multipartFile = (MultipartFile) getFileMethod.invoke(argument);
						if (multipartFile != null) {
							MultipartEntityBuilderUtils.addFile(paramName + "File", multipartFile, builder);
							//Now we want to null the value out so that jackson doesn't attempt to read the stream during serialization
							Method setFileMethod = MethodUtils.getMethod(argument.getClass(), "setFile", MultipartFile.class);
							setFileMethod.invoke(argument, (Object) null);
						}
					}
					builder.addTextBody(paramName + "Json", getJsonArgument(parameterType, argument), ContentType.APPLICATION_JSON);
				}
				catch (Exception e) {
					LogUtils.error(LogCommand.ofThrowableAndMessage(ExternalServiceInvocationHandler.class, e, "Failed to create multipart message for: " + invocation.getMethod().getName()));
					throw new RuntimeException("Failed to create multipart message for: " + invocation.getMethod().getName(), e);
				}
			}
			builder.addTextBody(Context.REQUEST_PARAMETER_MAP, getRequestParameterMap());
			return builder.build();
		}
		else {
			Map<String, String> parameterMap = new LinkedHashMap<>();
			for (int i = 0; i < paramNames.size(); i++) {
				String paramName = paramNames.get(i);
				Class<?> parameterType = parameterTypes[i];
				Object argument = arguments[i];
				parameterMap.put(paramName, getJsonArgument(parameterType, argument));
				if (argument != null && argument.getClass().getTypeParameters().length != parameterType.getTypeParameters().length) {
					//If the argument type parameter list length does not match the parameterType type parameter list then this indicates
					//that the method uses a signature with generic type information of some sort, but that the argument that is has been
					//passed is already a concrete implementation of the parameter.
					//
					//In this case, we include the exact type so when the object is deserialized in the external application it won't lose
					//type information (e.g. ReconciliationRuleFunction is saved using 'saveReconciliationRule(ReconciliationRule<?> rule)'
					//So if we don't pass the type information the object is not deserialized as ReconciliationRuleFunction properly.
					parameterMap.put(paramName + "Type", argument.getClass().getName());
				}
			}
			parameterMap.put(Context.REQUEST_PARAMETER_MAP, getRequestParameterMap());
			return HttpEntityFactory.createHttpEntity(getJacksonHandler().toJson(parameterMap), ContentType.APPLICATION_JSON);
		}
	}


	private String getRequestParameterMap() {
		return getJacksonHandler().toJson(getContextHandler().getBean("REQUEST_PARAMETER_MAP"));
	}


	/*
	 * If the argument is an interface we wrap the it so we can include the concrete implementation
	 * className in the request; this allows deserialization to the proper concrete type and prevents us from
	 * losing data due to type erasure.
	 * e.g., sending 'PortalFileCategory' when the method signature uses 'IdentityObject'
	 */
	private String getJsonArgument(Class<?> parameterType, Object argument) {
		if (parameterType.isInterface() && argument != null) {
			ExternalServiceObjectWrapper objectWrapper = new ExternalServiceObjectWrapper();
			objectWrapper.setObjectClassName(argument.getClass().getName());
			objectWrapper.setObjectJsonString(getJacksonHandler().toJson(argument));
			argument = objectWrapper;
		}
		return getJacksonHandler().toJson(argument);
	}


	private Object handleResponse(SimpleHttpResponse response, Method method) throws Throwable {
		AssertUtils.assertNotNull(response, "Response was null!  Unable to process for method: " + method.getName());
		if (isJsonResponse(response.getHttpResponse())) {
			String jsonResponse = EntityUtils.toString(response.getEntity(), "UTF8");
			if (isResponseHandlingNeeded(method, jsonResponse)) {
				Class<?> returnTypeClass = method.getReturnType();
				if (returnTypeClass.isInterface()) {
					//Unwrap the returnValue
					ExternalServiceObjectWrapper objectWrapper = getJacksonHandler().getObjectMapper().readValue(jsonResponse, ExternalServiceObjectWrapper.class);
					// Empty Object and Empty Json String the Result is Null
					if (StringUtils.isEmpty(objectWrapper.getObjectClassName()) && StringUtils.isEmpty(objectWrapper.getObjectJsonString())) {
						return null;
					}

					returnTypeClass = CoreClassUtils.getClass(objectWrapper.getObjectClassName());
					jsonResponse = objectWrapper.getObjectJsonString();

					//Now we have to actually locate the generic parameter types and use the 'constructParametricType' because
					//it is possible that despite the method returning a generic List for instance, that the objects actual
					//class is 'PagingArrayList', so we explicitly use the class returned in the wrapper object and the known parameter
					//types.  It's possible this could still fail if the returnValue were something odd where even the parameter types of
					//the generic list were also generics that were lost - in this case our wrapper would need to be enhanced, but for now
					//we assume only the top-level object may not be exactly the type indicated in the method's return value signature.
					MethodParameter methodParameter = new MethodParameter(method, -1);
					if (methodParameter.getGenericParameterType() instanceof ParameterizedType) {
						try {
							Type[] types = ((ParameterizedType) methodParameter.getGenericParameterType()).getActualTypeArguments();
							Class<?>[] parameterTypes = new Class[types.length];
							for (int i = 0; i < types.length; i++) {
								parameterTypes[i] = Class.forName(types[i].getTypeName());
							}
							JavaType type = getJacksonHandler().getObjectMapper().getTypeFactory().constructParametricType(returnTypeClass, parameterTypes);
							return getJacksonHandler().getObjectMapper().readValue(jsonResponse, type);
						}
						catch (Exception e) {
							//failed - likely caused by multi-level generics within the top-level interface class; e.g. List<Map<String, Object>
							//Try standard deserialization now that the top-level interface concrete implementation returnTypeClass is known.
						}
					}
					// If Not ParameterizedType then use the returnTypeClass
				}
				return getJacksonHandler().fromJson(jsonResponse, returnTypeClass);
			}
		}
		else {
			if (FileWrapper.class.isAssignableFrom(method.getReturnType())) {
				String disposition = response.getHttpResponse().getFirstHeader("Content-Disposition").getValue();
				String fileName = disposition.replaceFirst("(?i)^.*filename=\"([^\"]+)\".*$", "$1");

				String filePrefix = fileName;
				String fileSuffix = "tmp";
				int extensionIndex = fileName.indexOf(".");
				if (extensionIndex > 0) {
					filePrefix = fileName.substring(0, fileName.indexOf("."));
					fileSuffix = fileName.substring(fileName.indexOf(".") + 1);
				}
				File file = FileUtils.convertInputStreamToFile(filePrefix, fileSuffix, response.getContent());
				return new FileWrapper(file, fileName, false);
			}
		}
		return null;
	}


	private boolean isJsonResponse(HttpResponse response) {
		AssertUtils.assertNotNull(response, "Response cannot be null!");
		//If the Content-Length header is '0' then the response was likely null and should not be processed.
		Header contentLengthHeader = response.getFirstHeader(HttpHeaders.CONTENT_LENGTH);
		String contentLength = contentLengthHeader != null ? contentLengthHeader.getValue() : null;
		if ("0".equals(contentLength)) {
			return false;
		}
		//Depending on whether the target server is using Spring MediaType, or Apache ContentType constants, in addition to the
		// possibility that various charsets are used in the Content-Type header, the string may vary slightly...
		// So we just check for contains 'application/json' instead which should be consistent...
		Header contentTypeHeader = response.getFirstHeader(HttpHeaders.CONTENT_TYPE);
		String contentType = contentTypeHeader != null ? contentTypeHeader.getValue() : null;
		if (contentType != null && contentType.contains("application/json")) {
			return true;
		}
		return false;
	}


	private void handleErrorResponse(SimpleHttpResponse simpleHttpResponse) throws Throwable {
		String errorResponse;
		try {
			errorResponse = EntityUtils.toString(simpleHttpResponse.getHttpResponse().getEntity(), "UTF8");
		}
		catch (Exception e) {
			errorResponse = "[Server reply content could not be retrieved]";
		}
		finally {
			//Close the response
			HttpUtils.closeResponse(simpleHttpResponse);
		}
		//We only handle our wrapped exceptions - all other status codes are thrown back as bad status exceptions.
		if (simpleHttpResponse.getStatusCode() == 418) {
			//Unwrap the original exception - then deserialize
			ExternalServiceExceptionWrapper objectWrapper = getJacksonHandler().getObjectMapper().readValue(errorResponse, ExternalServiceExceptionWrapper.class);
			String exceptionResponse = objectWrapper.getObjectJsonString();
			String exceptionClassName = objectWrapper.getObjectClassName();
			if (exceptionResponse != null && exceptionClassName != null) {
				Class<?> exceptionClass = CoreClassUtils.getClass(exceptionClassName);
				throw (Throwable) getJacksonHandler().fromJson(exceptionResponse, exceptionClass);
			}
		}
		throw new BadStatusException(simpleHttpResponse.getURI(), simpleHttpResponse.getStatusCode(), errorResponse);
	}


	private boolean isResponseHandlingNeeded(Method method, String jsonResponse) {
		if (method.getReturnType() == null || Void.TYPE.isAssignableFrom(method.getReturnType())) {
			return false;
		}
		if (StringUtils.isEmpty(jsonResponse) || StringUtils.isEqual(jsonResponse, "{}")) {
			return false;
		}
		return true;
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public JacksonHandlerImpl getJacksonHandler() {
		return this.jacksonHandler;
	}


	public void setJacksonHandler(JacksonHandlerImpl jacksonHandler) {
		this.jacksonHandler = jacksonHandler;
	}


	public String getExternalServiceURL() {
		return this.externalServiceURL;
	}


	public void setExternalServiceURL(String externalServiceURL) {
		this.externalServiceURL = externalServiceURL;
	}


	public Map<String, Class<?>> getInterfaceNameToImplementationClassMap() {
		return this.interfaceNameToImplementationClassMap;
	}


	public void setInterfaceNameToImplementationClassMap(Map<String, Class<?>> interfaceNameToImplementationClassMap) {
		this.interfaceNameToImplementationClassMap = interfaceNameToImplementationClassMap;
	}


	public SecurityOAuthUserTokenService getSecurityOAuthUserTokenService() {
		return this.securityOAuthUserTokenService;
	}


	public void setSecurityOAuthUserTokenService(SecurityOAuthUserTokenService securityOAuthUserTokenService) {
		this.securityOAuthUserTokenService = securityOAuthUserTokenService;
	}
}
