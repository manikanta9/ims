package com.clifton.core.web.mvc;


import com.clifton.core.web.bind.ValidatingServletRequestDataBinder;
import org.springframework.web.bind.ServletRequestDataBinder;
import org.springframework.web.bind.support.WebBindingInitializer;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.ServletRequestDataBinderFactory;

import java.util.List;


/**
 * The <code>ValidatingServletRequestDataBinderFactory</code> class extends {@link ServletRequestDataBinderFactory} and
 * overrides only createBinderInstance method to return an instance of new {@link ValidatingServletRequestDataBinder} object.
 *
 * @author mwacker
 */
public class ValidatingServletRequestDataBinderFactory extends ServletRequestDataBinderFactory {

	/**
	 * Specifies whether lookups related to bean population should be performed in a single read-only transaction.
	 * Single transaction improves performance when multiple lookups are made.
	 */
	private final boolean transactionalBeanPrepare;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ValidatingServletRequestDataBinderFactory(List<InvocableHandlerMethod> binderMethods, WebBindingInitializer initializer, boolean transactionalBeanPrepare) {
		super(binderMethods, initializer);
		this.transactionalBeanPrepare = transactionalBeanPrepare;
	}


	@Override
	protected ServletRequestDataBinder createBinderInstance(Object target, String objectName, @SuppressWarnings("unused") NativeWebRequest request) {
		return new ValidatingServletRequestDataBinder(target, objectName, this.transactionalBeanPrepare);
	}
}
