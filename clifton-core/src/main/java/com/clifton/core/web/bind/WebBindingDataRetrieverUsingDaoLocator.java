package com.clifton.core.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.DaoSessionAware;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.util.BooleanUtils;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;


/**
 * @author vgomelsky
 */
public class WebBindingDataRetrieverUsingDaoLocator implements WebBindingDataRetriever {

	private DaoLocator daoLocator;
	private ContextHandler contextHandler;

	private int order;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int getOrder() {
		return this.order;
	}


	@Override
	public boolean isApplicableForRequest(WebRequest request) {
		return true;
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T extends IdentityObject> T getEntity(Class<T> entityType, Serializable entityId) {
		ReadOnlyDAO<T> dao = getDaoLocator().locate(entityType);
		T entity = dao.findByPrimaryKey(entityId);
		if (entity != null) {
			Boolean openSessionInView = (Boolean) getContextHandler().getBean(Context.OPEN_SESSION_IN_VIEW_FLAG_NAME);
			if (dao.getClass().isAssignableFrom(DaoSessionAware.class) && BooleanUtils.isTrue(openSessionInView)) {
				((DaoSessionAware<T>) dao).evict(entity);
			}
		}
		return entity;
	}


	@Override
	public List<Column> getEntityColumnList(Class<? extends IdentityObject> entityType) {
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(entityType);
		return dao.getConfiguration().getColumnList();
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
