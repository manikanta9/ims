package com.clifton.core.web.mvc;


import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.core.Ordered;
import org.springframework.web.HttpRequestHandler;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.Controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * The <code>DelegatingHandlerAdapter</code> acts as a router for requests; if a request contains /api/ as part of the
 * url it is routed to our <code>ExternalServiceHandlerAdapter</code> which is used to allow raw application/json
 * data to be posted to a method without that methods parameters being annotated with @RequestBody; otherwise, the request is
 * routed to our <code>WebMethodHandlerAdapter</code>
 *
 * @author stevenf
 */
public class DelegatingHandlerAdapter implements HandlerAdapter, Ordered {

	//Spring injects this as a LinkedHashMap to maintain order, this is necessary for delegation to work properly.
	private Map<Pattern, HandlerAdapter> urlPatternToHandlerAdapterMap;

	private int order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean supports(Object handler) {
		if (handler instanceof Controller) {
			return false;
		}
		else if (handler instanceof HttpRequestHandler) {
			return false;
		}
		return true;
	}


	@Override
	public ModelAndView handle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

		String requestURI = request.getRequestURI();
		Map<Pattern, HandlerAdapter> handlerAdapterMap = getUrlPatternToHandlerAdapterMap();
		ValidationUtils.assertNotNull(handlerAdapterMap, "'urlPatternToHandlerMap' may not be null!");
		//Use pre-compiled patterns (http://stackoverflow.com/questions/1360113/is-java-regex-thread-safe)
		for (Map.Entry<Pattern, HandlerAdapter> adapterEntry : handlerAdapterMap.entrySet()) {
			if (adapterEntry.getKey().matcher(requestURI).matches()) {
				HandlerAdapter handlerAdapter = handlerAdapterMap.get(adapterEntry.getKey());
				return handlerAdapter.handle(request, response, handler);
			}
		}


		throw new ValidationException("Failed to locate handler for request URI: " + requestURI);
	}


	@Override
	public long getLastModified(HttpServletRequest request, Object handler) {
		return -1;
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Map<Pattern, HandlerAdapter> getUrlPatternToHandlerAdapterMap() {
		return this.urlPatternToHandlerAdapterMap;
	}


	public void setUrlPatternToHandlerAdapterMap(Map<Pattern, HandlerAdapter> urlPatternToHandlerAdapterMap) {
		this.urlPatternToHandlerAdapterMap = urlPatternToHandlerAdapterMap;
	}


	@Override
	public int getOrder() {
		return this.order;
	}


	public void setOrder(int order) {
		this.order = order;
	}
}
