package com.clifton.core.web.converter;


/**
 * The <code>BooleanHttpMessageConverter</code> class supports methods that return java.lang.Boolean value and are annotated with @ResponseBody.
 * It's a copy of StringHttpMessageConverter with Boolean specific changes.
 * <p/>
 * Uses Boolean.toString()
 *
 * @author vgomelsky
 */
public class BooleanHttpMessageConverter extends BaseHttpMessageConverter<Boolean> {

	@Override
	public boolean supports(Class<?> clazz) {
		return Boolean.class.equals(clazz);
	}


	@Override
	protected Boolean convertFromString(String stringValue) {
		return Boolean.valueOf(stringValue);
	}
}
