package com.clifton.core.web.view;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.PagingCommand;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverter;
import com.clifton.core.dataaccess.datatable.bean.BeanCollectionToDataTableConverterWithDynamicColumnCreation;
import com.clifton.core.dataaccess.datatable.bean.DataTableConverterConfiguration;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DocumentContentStream;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.mvc.WebRequestUtils;

import javax.servlet.http.HttpServletRequest;
import java.io.File;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;


/**
 * The <code>FileDownloadView</code> handles returning the content of a file for the user to download
 *
 * @author manderson
 */
public class FileDownloadView<T> extends BaseDownloadView {

	private ApplicationContextService applicationContextService;


	/**
	 * For DataTables only: When true, uses the column names defined in the request instead of the query in the {@link DataTableFileConfig}.
	 * ex:  For reconciliation since we are manipulating the column names in the UI we want to use those names instead of the names from the query.
	 * Used in reconciliation-shared.js for all of the reconciliation detail grids.
	 */
	private String overrideDataTableColumnNamesParameter = "overrideDataTableColumnNames";


	@Override
	@SuppressWarnings("unchecked")
	public void processRequestObject(DownloadViewCommand command, Object requestObject) throws Exception {
		HttpServletRequest request = command.getRequest();
		Map<String, Object> model = command.getModel();

		if (requestObject instanceof DocumentContentStream) {
			DocumentContentStream dcs = (DocumentContentStream) requestObject;
			command.setFileName(dcs.getFilename());
			command.setInputStream(dcs.getStream());
		}
		else {
			if (requestObject instanceof File) {
				command.setFile(FilePath.forFile((File) requestObject));
			}
			else if (requestObject instanceof FileWrapper) {
				FileWrapper wrapper = (FileWrapper) requestObject;
				command.setFileSize(wrapper.getFile().toFileContainer().length());
				command.setFileName(wrapper.getFileName());
				command.setFile(wrapper.getFile());
				command.setTempFile(wrapper.isTempFile());
			}
			else if (requestObject instanceof DataTable) {
				command.setUseRequestParamFileName(true);
				DataTable dataTable = (DataTable) requestObject;
				handleDataTable(command, dataTable);
			}
			else if (requestObject instanceof List) {
				List<T> requestObjectList = (List<T>) requestObject;
				try {
					ValidationUtils.assertTrue(CollectionUtils.getSize(requestObjectList) <= getMaxLimit(request), () -> "Unable to process.  Requested data is larger than the allowed maximum size of " + CoreMathUtils.formatNumberInteger(BigDecimal.valueOf(getMaxLimit(request))) + ". Add additional filters before attempting to export.");

					command.setUseRequestParamFileName(true);
					String columns = HttpUtils.getUrlDecodedParameterValue(request, "columns");
					String converterBeanName = HttpUtils.getUrlDecodedParameterValue(request, "beanCollectionToDataTableConverter");
					if (!CollectionUtils.isEmpty(requestObjectList)) {
						Class<T> dtoClass = (Class<T>) CollectionUtils.getFirstElementStrict(requestObjectList).getClass();
						// convert List to a DataTable
						BeanCollectionToDataTableConverter<T> converter = getListToDataTableConverter(columns, dtoClass, converterBeanName);
						requestObject = converter.convert(requestObjectList);
						handleDataTable(command, (DataTable) requestObject);
					}
					else {
						// If null - Return response that there is no data to build file from
						model.put(getSuccessPropertyName(), false);
						model.put("message", "There is no data to build file from");
					}
				}
				catch (Exception e) {
					model.put(getSuccessPropertyName(), false);
					model.put("message", ExceptionUtils.getDetailedMessage(e));
					if (!CoreExceptionUtils.isValidationException(e)) {
						LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, "Error converting DataTable to File").withRequest(request));
					}
				}
			}
			else {
				model.put(getSuccessPropertyName(), false);
				model.put("message", "Expected a File, List, or DataTable object to be in the model but it was a " + requestObject.getClass());
				LogUtils.error(LogCommand.ofMessage(getClass(), "Expected a File, List, or DataTable object to be in the model but it was a ", requestObject.getClass()).withRequest(request));
			}
		}
	}


	private int getMaxLimit(HttpServletRequest request) {
		return WebRequestUtils.getIntParameterValue(request, "maxLimit", PagingCommand.MAX_LIMIT);
	}


	@SuppressWarnings("unchecked")
	private BeanCollectionToDataTableConverter<T> getListToDataTableConverter(String columns, Class<T> dtoClass, String converterBeanName) {
		if (!StringUtils.isEmpty(converterBeanName)) {
			BeanCollectionToDataTableConverter<T> converter = (BeanCollectionToDataTableConverter<T>) getApplicationContextService().getContextBean(converterBeanName);
			converter.initialize(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(), dtoClass, columns));
			return converter;
		}
		if (StringUtils.isEmpty(columns)) {
			return new BeanCollectionToDataTableConverterWithDynamicColumnCreation<>(DataTableConverterConfiguration.ofDtoClass(getApplicationContextService(), null));
		}
		else {
			return new BeanCollectionToDataTableConverter<>(DataTableConverterConfiguration.ofDtoClassUsingDataColumnString(getApplicationContextService(), dtoClass, columns));
		}
	}


	@Override
	public void handleError(DownloadViewCommand command) {
		// CREATE PDF or EXCEL FILE WITH ERROR MESSAGE
		DataTable dataTable = new PagingDataTableImpl(new DataColumnImpl[]{new DataColumnImpl("There was an error while processing your request", java.sql.Types.NVARCHAR)});
		dataTable.addRow(new DataRowImpl(dataTable, new Object[]{command.getModel().get("message")}));

		DataTableFileConfig config = new DataTableFileConfig(dataTable, DataTableFileConfig.DATA_TABLE_HEADER_ERROR_STYLE);
		command.setFile(FilePath.forFile(getDataTableToFileConverter(command.getRequest().getParameter("outputFormat"), true).convert(config)));
		command.setTempFile(true);
	}


	private void handleDataTable(DownloadViewCommand command, DataTable dataTable) {
		HttpServletRequest request = command.getRequest();
		Map<String, Object> model = command.getModel();
		try {
			ValidationUtils.assertTrue(dataTable.getTotalRowCount() <= getMaxLimit(request), () -> "Unable to process.  Requested data is larger than the allowed maximum size of " + CoreMathUtils.formatNumberInteger(BigDecimal.valueOf(getMaxLimit(request))) + ". Add additional filters before attempting to export.");
			// convert DataTable to a File
			String columns = HttpUtils.getUrlDecodedParameterValue(request, "columns");
			boolean overrideDataTableColumnNames = BooleanUtils.isTrue(request.getParameter(getOverrideDataTableColumnNamesParameter()));
			DataTableFileConfig config = new DataTableFileConfig(dataTable, DataTableFileConfig.DATA_TABLE_HEADER_EXPORT_STYLE, columns, overrideDataTableColumnNames, getApplicationContextService());
			FilePath file = FilePath.forFile(getDataTableToFileConverter(request.getParameter("outputFormat"), false).convert(config));
			if (file == null) {
				model.put(getSuccessPropertyName(), false);
				model.put("message", "There is no datatable or columns available to build file from");
			}
			else {
				command.setFile(file);
				command.setTempFile(true);
			}
		}
		catch (Exception e) {
			model.put(getSuccessPropertyName(), false);
			model.put("message", ExceptionUtils.getDetailedMessage(e));
			if (!CoreExceptionUtils.isValidationException(e)) {
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, "Error converting DataTable to File").withRequest(request));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public String getOverrideDataTableColumnNamesParameter() {
		return this.overrideDataTableColumnNamesParameter;
	}


	public void setOverrideDataTableColumnNamesParameter(String overrideDataTableColumnNamesParameter) {
		this.overrideDataTableColumnNamesParameter = overrideDataTableColumnNamesParameter;
	}
}
