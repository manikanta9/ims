package com.clifton.core.web.view;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.migrate.schema.MigrationSchemaService;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * The <code>MigrationActionDownloadView</code> handles returning an XML file containing migration actions for the given list
 *
 * @author stevenF
 */
public class MigrationActionDownloadView extends BaseDownloadView {

	private DaoLocator daoLocator;
	private MigrationSchemaService migrationSchemaService;


	@Override
	@SuppressWarnings("unchecked")
	public void processRequestObject(DownloadViewCommand command, Object requestObject) throws Exception {
		if (requestObject instanceof List) {
			List<String> lines = new ArrayList<>();
			List<IdentityObject> identityObjectList = (List<IdentityObject>) requestObject;
			Class<IdentityObject> identityObjectClass = !CollectionUtils.isEmpty(identityObjectList) ? (Class<IdentityObject>) identityObjectList.get(0).getClass() : null;
			String tableName = getDaoLocator().locate(identityObjectClass).getConfiguration().getTableName();
			for (IdentityObject object : CollectionUtils.getIterable(identityObjectList)) {
				lines.add(getMigrationSchemaService().getJsonMigrationAction(tableName, ((Number) object.getIdentity()).intValue(), true));
			}
			File tempFile = File.createTempFile("migrationActions", ".xml");
			FileUtils.writeLines(tempFile, lines);
			command.setFile(FilePath.forFile(tempFile));
			command.setTempFile(true);
			command.setUseRequestParamFileName(true);
		}
		else {
			command.getModel().put(getSuccessPropertyName(), false);
			command.getModel().put("message", "Expected a List object to be in the model but it was: " + (requestObject != null ? requestObject.getClass() : null));
			LogUtils.error(LogCommand.ofMessage(getClass(), "Expected a List object to be in the model but it was: ", (requestObject != null ? requestObject.getClass() : null)).withRequest(command.getRequest()));
		}
	}


	@Override
	public void handleError(DownloadViewCommand command) throws Exception {
		File tempFile = File.createTempFile("error", ".txt");
		FileUtils.writeLines(tempFile, Collections.singletonList("There was an error while processing your request: " + command.getModel().get("message")));
		command.setFile(FilePath.forFile(tempFile));
		command.setTempFile(true);
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public MigrationSchemaService getMigrationSchemaService() {
		return this.migrationSchemaService;
	}


	public void setMigrationSchemaService(MigrationSchemaService migrationSchemaService) {
		this.migrationSchemaService = migrationSchemaService;
	}
}
