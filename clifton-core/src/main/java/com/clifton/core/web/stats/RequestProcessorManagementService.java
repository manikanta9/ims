package com.clifton.core.web.stats;

import com.clifton.core.security.authorization.SecureMethod;


/**
 * The {@link RequestProcessorManagementService} provides methods for working with servlet container request processors.
 *
 * @author MikeH
 */
public interface RequestProcessorManagementService {

	/**
	 * Retrieves the current {@link RequestProcessorStatus}.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public RequestProcessorStatus getRequestProcessorStatus();


	/**
	 * Saves parameters for request processors.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public RequestProcessorStatus saveRequestProcessorParameters(Integer responseBufferSize, Long responseLimitSize);
}
