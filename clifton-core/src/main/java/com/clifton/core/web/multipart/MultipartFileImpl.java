package com.clifton.core.web.multipart;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import org.apache.commons.io.IOUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;


/**
 * The <code>MultipartFileImpl</code> class is an implementation for MultipartFile interface that implements needed methods.
 *
 * @author vgomelsky
 */
public class MultipartFileImpl implements MultipartFile {

	private final String name;
	private String originalFilename;
	private String contentType;
	private long size;

	// Either the file or stream are used instead of byte[] to avoid java heap consumption issues.
	private InputStream contentStream;
	private File file;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private MultipartFileImpl(String name, String originalFilename, String contentType, long contentSize) {
		if (StringUtils.isEmpty(name)) {
			throw new IllegalArgumentException("Name must be defined");
		}
		this.name = name;
		this.originalFilename = (originalFilename != null ? originalFilename : "");
		this.contentType = contentType;
		this.size = contentSize;
	}


	/**
	 * Creates a {@link MultipartFile} for the {@link InputStream} provided.
	 *
	 * @param name             the name of the file part
	 * @param originalFilename the original file name for file of this file part
	 * @param contentType      the content type of file
	 * @param contentSize      the length of the file (file size)
	 * @param contentStream    the file content stream
	 */
	public MultipartFileImpl(String name, String originalFilename, String contentType, long contentSize, InputStream contentStream) {
		this(name, originalFilename, contentType, contentSize);
		this.contentStream = contentStream;
		this.file = null;
	}


	/**
	 * Creates a {@link MultipartFile} for the {@link File} provided.
	 *
	 * @param originalFilename the original file name for the file of this file part
	 * @param file             the file this file part
	 */
	public MultipartFileImpl(String originalFilename, File file) {
		this(file.getName(), originalFilename, null, file.length());
		this.file = file;
		this.contentStream = null;
	}


	/**
	 * Creates a {@link MultipartFile} for the {@link File} provided.
	 *
	 * @param originalFilename the original file name for the file of this file part
	 * @param fileContainer    the file this file part
	 */
	public MultipartFileImpl(String originalFilename, FileContainer fileContainer) throws IOException {
		this(fileContainer.getName(), originalFilename, null, fileContainer.length());
		this.contentStream = fileContainer.getInputStream();
	}


	/**
	 * Creates a {@link MultipartFile} for the {@link File} provided.
	 *
	 * @param file the file this file part
	 */
	public MultipartFileImpl(File file) {
		this(file.getName(), file);
	}


	/**
	 * Creates a {@link MultipartFile} for the file name provided.
	 *
	 * @param fileName the file name/path of the file for this file part
	 */
	public MultipartFileImpl(String fileName) {
		this(new File(fileName));
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String getName() {
		return this.name;
	}


	@Override
	public String getOriginalFilename() {
		return this.originalFilename;
	}


	@Override
	public String getContentType() {
		return this.contentType;
	}


	@Override
	public boolean isEmpty() {
		return (this.size < 1);
	}


	@Override
	public long getSize() {
		return isEmpty() ? 0 : this.size;
	}


	@Override
	public byte[] getBytes() throws IOException {
		// Reading files/streams into memory is generally a bad idea because it consumes java heap space
		return IOUtils.toByteArray(getInputStream());
	}


	@Override
	public InputStream getInputStream() throws IOException {
		if (this.file == null) {
			return this.contentStream;
		}
		else {
			return new FileInputStream(this.file);
		}
	}


	@Override
	public void transferTo(File dest) throws IOException, IllegalStateException {
		if (this.file == null) {
			FileUtils.copyToPath(this.contentStream, FilePath.forFile(dest));
		}
		else {
			FileUtils.copyFile(this.file, dest);
		}
	}
}
