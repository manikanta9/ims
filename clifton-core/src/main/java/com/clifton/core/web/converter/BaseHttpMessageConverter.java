package com.clifton.core.web.converter;


import com.clifton.core.util.AssertUtils;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpInputMessage;
import org.springframework.http.HttpOutputMessage;
import org.springframework.http.MediaType;
import org.springframework.util.StreamUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BaseHttpMessageConverter</code> class provides core functionality for serializing generic
 * objects for methods annotated with @ResponseBody.
 * <p>
 * It's a copy of StringHttpMessageConverter with generic type.
 *
 * @author NickK
 */
public abstract class BaseHttpMessageConverter<T> extends org.springframework.http.converter.AbstractHttpMessageConverter<T> {

	public static final Charset DEFAULT_CHARSET = StandardCharsets.ISO_8859_1;

	private final List<Charset> availableCharsets;

	private boolean writeAcceptCharset = true;


	public BaseHttpMessageConverter() {
		super(new MediaType("text", "plain", DEFAULT_CHARSET), MediaType.ALL);
		this.availableCharsets = new ArrayList<>(Charset.availableCharsets().values());
	}


	/**
	 * Indicates whether the {@code Accept-Charset} should be written to any outgoing request.
	 * <p>Default is {@code true}.
	 */
	public void setWriteAcceptCharset(boolean writeAcceptCharset) {
		this.writeAcceptCharset = writeAcceptCharset;
	}


	@Override
	protected T readInternal(@SuppressWarnings({"unused", "rawtypes"}) Class clazz, HttpInputMessage inputMessage) throws IOException {
		Charset charset = getContentTypeCharset(inputMessage.getHeaders().getContentType());
		String stringResult = StreamUtils.copyToString(inputMessage.getBody(), charset);
		return convertFromString(stringResult);
	}


	protected abstract T convertFromString(String stringValue);


	@Override
	protected Long getContentLength(T value, MediaType contentType) {
		Charset charset = getContentTypeCharset(contentType);
		try {
			return (long) convertToString(value).getBytes(charset.name()).length;
		}
		catch (UnsupportedEncodingException ex) {
			// should not occur
			throw new InternalError(ex.getMessage());
		}
	}


	@Override
	protected void writeInternal(T value, HttpOutputMessage outputMessage) throws IOException {
		if (this.writeAcceptCharset) {
			outputMessage.getHeaders().setAcceptCharset(getAcceptedCharsets());
		}
		Charset charset = getContentTypeCharset(outputMessage.getHeaders().getContentType());
		String s = convertToString(value);
		StreamUtils.copy(s, charset, outputMessage.getBody());
	}


	/**
	 * Returns the toString() representation of value being converted.
	 */
	protected String convertToString(T value) {
		return value.toString();
	}


	/**
	 * Return the list of supported {@link Charset}.
	 * <p/>
	 * <p>By default, returns {@link Charset#availableCharsets()}. Can be overridden in subclasses.
	 *
	 * @return the list of accepted charsets
	 */
	protected List<Charset> getAcceptedCharsets() {
		return this.availableCharsets;
	}


	@NotNull
	private Charset getContentTypeCharset(MediaType contentType) {
		if (contentType != null && contentType.getCharset() != null) {
			return AssertUtils.assertNotNull(contentType.getCharset(), "Content type charset is null.");
		}
		else {
			return DEFAULT_CHARSET;
		}
	}
}
