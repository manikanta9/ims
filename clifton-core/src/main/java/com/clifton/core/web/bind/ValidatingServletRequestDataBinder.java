package com.clifton.core.web.bind;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import org.springframework.beans.MutablePropertyValues;
import org.springframework.beans.PropertyValue;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestDataBinder;

import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;


/**
 * The <code>ValidatingServletRequestDataBinder</code> class allows replacing existing target bean with a fully
 * populated DTO from the database and validates it when validate is set to true.
 *
 * @author vgomelsky
 */
public class ValidatingServletRequestDataBinder extends ServletRequestDataBinder {

	private WebBindingDataRetriever webBindingDataRetriever;

	private boolean prepareBean;
	private boolean validationDisabled;
	/**
	 * Specifies whether lookups related to bean population should be performed in a single read-only transaction.
	 * Single transaction improves performance when multiple lookups are made.
	 */
	private final boolean transactionalBeanPrepare;

	////////////////////////////////////////////////////////////////////////////
	////////      ValidatingServletRequestDataBinder Methods       /////////////
	////////////////////////////////////////////////////////////////////////////


	public ValidatingServletRequestDataBinder(Object target, String objectName, boolean transactionalBeanPrepare) {
		super(target, objectName);
		this.transactionalBeanPrepare = transactionalBeanPrepare;
	}


	@Override
	protected void doBind(MutablePropertyValues propertyValues) {
		Object target = getTarget();
		IdentityObject bean = null;
		if (isPrepareBean()) {
			bean = (IdentityObject) target;
			prepareBean(bean, propertyValues);
		}
		else if (target != null && AnnotationUtils.isAnnotationPresent(target.getClass(), NonPersistentObject.class)) {
			if (AnnotationUtils.getAnnotation(target.getClass(), NonPersistentObject.class).populatePropertiesBeforeBinding()) {
				// Virtual DTO may have real DTO's as its attributes: populate them
				prepareBean(target, propertyValues);
			}
		}

		super.doBind(propertyValues);

		if (bean != null && !isValidationDisabled()) { // VALIDATE AFTER BINDING
			validateBean(bean);
		}
	}


	/**
	 * Prepare nested properties for binding: instantiate or clear dependent beans when necessary.
	 * <p>
	 * Each request will have the posted data bound to DTOs before invoking the service, so it is
	 * safe to use a readOnly transaction for the entire binding process.
	 *
	 * @see org.springframework.web.method.support.InvocableHandlerMethod#invokeForRequest(org.springframework.web.context.request.NativeWebRequest, org.springframework.web.method.support.ModelAndViewContainer, Object...)
	 */
	protected void prepareBean(Object bean, MutablePropertyValues propertyValues) {
		if (this.transactionalBeanPrepare) {
			doPrepareBeanTransactional(bean, propertyValues);
		}
		else {
			doPrepareBean(bean, propertyValues);
		}
	}


	@Transactional(readOnly = true)
	protected void doPrepareBeanTransactional(Object bean, MutablePropertyValues propertyValues) {
		doPrepareBean(bean, propertyValues);
	}


	@SuppressWarnings("unchecked")
	protected void doPrepareBean(Object bean, MutablePropertyValues propertyValues) {
		// pre-instantiate first level beans for composite properties (allows deep binding)
		Set<String> nullFKs = new LinkedHashSet<>(); // composite foreign key fields with null values
		for (PropertyValue pv : propertyValues.getPropertyValues()) {
			String propertyName = pv.getName();
			int compositePropertyIndex = propertyName.lastIndexOf('.');
			if (compositePropertyIndex != -1) {
				String compositePropertyName = propertyName.substring(0, compositePropertyIndex);
				if (propertyName.endsWith(".id")) {
					// empty id property: reset to null and remove the id so that it's not set
					IdentityObject newValue = null;
					Object idObject = pv.getValue();
					if (idObject instanceof String) {
						String idValue = (String) idObject;
						if (!StringUtils.isEmpty(idValue)) {
							Long id = Long.parseLong(idValue);
							newValue = (IdentityObject) BeanUtils.getPropertyValue(bean, compositePropertyName, true, true);
							if (newValue == null || !id.equals(BeanUtils.getIdentityAsLong(newValue))) {
								// dependent DTO is being changed: get fully populated dependent DTO for the id
								Class<? extends IdentityObject> clazz = (Class<? extends IdentityObject>) BeanUtils.getPropertyType(bean, compositePropertyName);
								newValue = getWebBindingDataRetriever().getEntity(clazz, id);
								if (newValue == null) {
									throw new FieldValidationException("Cannot find '" + clazz.getSimpleName() + "' entity for id = " + id, compositePropertyName);
								}
							}
						}
					}
					else if (idObject != null) {
						// sometimes multiple values are accidentally submitted for the same property
						Class<?> idClass = idObject.getClass();
						if (idClass.isArray()) {
							throw new FieldValidationException("Expected String value for '" + propertyName + "' property but was array with values: " + ArrayUtils.toString((Object[]) idObject), propertyName);
						}
						throw new FieldValidationException("Expected String value for '" + propertyName + "' property but was '" + idClass.getName() + "' with value: " + idObject, propertyName);
					}

					boolean doNotSet = false;
					if (newValue == null) {
						// if "a.b = null" skip setting "a.b.c" to null (don't want to create instance of "b" with null values
						for (String prop : nullFKs) {
							if (StringUtils.isEqual(compositePropertyName, prop) || compositePropertyName.indexOf(prop + ".") == 0) {
								doNotSet = true;
								break;
							}
						}
						if (nullFKs.add(compositePropertyName) && !doNotSet) {
							// Form data properties can be in an undefined order and caution should be taken setting null values.
							// - If nested properties are processed before the parent properties, avoid setting the parent property to null to prevent clearing populated data
							// (e.g., investmentSecurity.investmentInstrument.hierarchy.id processed with value before null investmentSecurity.investmentInstrument.id; want to keep hierarchy on null instrument).
							// - If composite property is null and there is no existing value for it, avoid populating the value to prevent the parent object being populated with nulls
							// (e.g., trade.holdingInvestmentAccount.baseCurrency.id is null and trade.holdingInvestmentAccount.id is null the holding account should not be populated).
							Object existingValue = BeanUtils.getPropertyValue(bean, compositePropertyName);
							doNotSet = existingValue == null || (existingValue instanceof IdentityObject && ((IdentityObject) existingValue).isNewBean());
						}
					}

					if (!doNotSet) {
						// set fully populated dependent DTO and remove the id property (setting to null allows clearing a property)
						// NOTE: if "instrument.hierarchy.id" is set first and then "instrument.id" is set later, the instrument.id may clear the hierarchy with null if it's not defined
						//       should we sort properties before setting them? or should we not pass arguments that don't make sense?
						BeanUtils.setPropertyValue(bean, compositePropertyName, newValue);
					}
					propertyValues.removePropertyValue(pv);
				}
				else if (BeanUtils.getPropertyValue(bean, compositePropertyName, true, true) == null) {
					Class<?> clazz = BeanUtils.getPropertyType(bean.getClass(), compositePropertyName);
					BeanUtils.setPropertyValue(bean, compositePropertyName, BeanUtils.newInstance(clazz));
				}
			}
		}
	}


	private void validateBean(IdentityObject bean) {
		Map<String, Object> propertyValueMap = BeanUtils.describe(bean);
		for (Column column : getWebBindingDataRetriever().getEntityColumnList(bean.getClass())) {
			// skip validation for fields that are not updated
			if (BeanUtils.isSystemManagedField(column.getBeanPropertyName()) || (bean.isNewBean() && column.getColumnExcludedFromInserts() != null && column.getColumnExcludedFromInserts())
					|| (!bean.isNewBean() && column.getColumnExcludedFromUpdates() != null && column.getColumnExcludedFromUpdates())) {
				continue;
			}
			// validate required and max field length
			Object value = propertyValueMap.get(column.getBeanPropertyName());
			if (column.getColumnRequiredForValidation() && (value == null || (value instanceof String && ((String) value).isEmpty()))) {
				String msg = "The field '" + column.getName() + "' is required.";
				getBindingResult().addError(new FieldError(getObjectName(), column.getBeanPropertyName(), msg));
			}
			if (value != null && column.getDataType() != null && DataTypeNames.STRING == column.getDataType().getTypeName() && !column.isEnum()
					&& value.toString().length() > column.getMaxLength()) {
				String msg = "The maximum length for field '" + column.getName() + "' is " + column.getMaxLength() + ".";
				getBindingResult().addError(new FieldError(getObjectName(), column.getBeanPropertyName(), msg));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WebBindingDataRetriever getWebBindingDataRetriever() {
		return this.webBindingDataRetriever;
	}


	public void setWebBindingDataRetriever(WebBindingDataRetriever webBindingDataRetriever) {
		this.webBindingDataRetriever = webBindingDataRetriever;
	}


	public boolean isPrepareBean() {
		return this.prepareBean;
	}


	public void setPrepareBean(boolean prepareBean) {
		this.prepareBean = prepareBean;
	}


	public boolean isValidationDisabled() {
		return this.validationDisabled;
	}


	public void setValidationDisabled(boolean validationDisabled) {
		this.validationDisabled = validationDisabled;
	}


	public boolean isTransactionalBeanPrepare() {
		return this.transactionalBeanPrepare;
	}
}
