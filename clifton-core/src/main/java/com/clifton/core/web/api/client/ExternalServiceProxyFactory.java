package com.clifton.core.web.api.client;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.api.client.locator.ExternalServiceLocatorUtils;
import com.clifton.core.web.mvc.ConventionBasedRequestMappingHandlerMapping;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanClassLoaderAware;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;

import java.util.List;
import java.util.Map;


/**
 * The <code>ExternalServiceProxyFactory</code> is used to scan for interfaces of our api client
 * jars and dynamically create proxies for them with the 'ExternalServiceInvocationHandler' used
 * as the method interceptor which will execute for all methods invoked on the proxied class.
 * <p>
 * This class extends our <code>ConventionBasedRequestMappingHandlerMapping</code> class so that the same
 * overrides we use to generate our url mappings are used here as well.  The only difference is I override
 * the afterPropertiesSet method so that it directly calls the detectHandlerMethods for ONLY the services
 * that have been proxied by this factory.
 *
 * @author StevenF
 */
public class ExternalServiceProxyFactory extends ConventionBasedRequestMappingHandlerMapping implements BeanFactoryPostProcessor, BeanClassLoaderAware {

	private ClassLoader classLoader;

	/**
	 * The URL of external service.
	 */
	private String externalServiceURL;
	/**
	 * Optionally prefix bean names with the following string instead of using our default naming convention.
	 * This will prevent duplicate bean names for case when we have more than one application with the same services.
	 * For example, reconciliationApplication, clientPortalApplication, integrationApplication, fixApplication, swiftApplication.
	 * It's important to follow this naming convention because external service lookups rely on it. Starts with the data source name.
	 */
	private String externalServiceBeanNamePrefix;
	/**
	 * Specifies whether web-accessible service methods should be exposed for external services or not. Defaults to true.
	 */
	private boolean serviceWebMethodsExposed = true;
	/**
	 * A Map of package names with class patterns that are used to locate applicable classes for proxying.'
	 * By default a class pattern is not necessary, e.g.:
	 * <util:map>
	 * <entry key="com.clifton.portal." value="" />
	 * </util:map>
	 * However, the value may contain a pattern to be used to proxy only specific services, such as
	 * I use single line comment below, because the '**' used to specify that this could occur in any sub-package
	 * relative to the base package breaks the block comment, the pattern below would would return both
	 * 'com.clifton.portal.tracking.PortalTrackingService' and 'com.clifton.portal.tracking.ip.PortalTrackingIpService'
	 */
	// <entry key="com.clifton.portal." value="**/PortalTracking*" />

	private Map<String, String> externalServiceInterfaceBasePackageClassPatternMap;

	private List<String> externalServiceInterfaceClassNameList;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setBeanClassLoader(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}


	/**
	 * Using configured pattern(s), search for all interfaces that end with word Service.
	 */
	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		BeanDefinitionRegistry registry = (BeanDefinitionRegistry) beanFactory;
		createAndRegisterInvocationHandlerBeanDefinition(registry);

		ExternalServiceClassPathScanner classpathScanner = new ExternalServiceClassPathScanner(false);
		for (Map.Entry<String, String> basePackageClassPatternEntry : CollectionUtils.getIterable(getExternalServiceInterfaceBasePackageClassPatternMap().entrySet())) {
			String basePackage = basePackageClassPatternEntry.getKey();
			String classPattern = basePackageClassPatternEntry.getValue();
			//Optionally specify an additional pattern to bring in only specific classes; we append the *.class to ensure minimal scanning
			// Spring uses the following resource pattern if none is specified: "**/*.class";
			if (!StringUtils.isEmpty(classPattern)) {
				classpathScanner.setResourcePattern(classPattern + "*.class");
			}
			for (BeanDefinition beanDefinition : classpathScanner.findCandidateComponents(basePackage)) {
				try {
					registerExternalServiceProxyBean(beanDefinition.getBeanClassName(), registry);
				}
				catch (Exception e) {
					LogUtils.error(getClass(), "Exception while creating proxy for [" + beanDefinition.getBeanClassName() + "] found with base package [" + basePackage + "] and class pattern [" + classPattern + "]");
				}
			}
		}
		for (String className : CollectionUtils.getIterable(getExternalServiceInterfaceClassNameList())) {
			try {
				registerExternalServiceProxyBean(className, registry);
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Exception while creating proxy for [" + className + "]");
			}
		}
	}


	private void registerExternalServiceProxyBean(String className, BeanDefinitionRegistry registry) throws Exception {
		Class<?> beanClass = Class.forName(className);
		String beanName = ContextConventionUtils.getBeanNameFromClassName(beanClass.getSimpleName());
		if (!StringUtils.isEmpty(getExternalServiceBeanNamePrefix())) {
			beanName = getExternalServiceBeanNamePrefix() + StringUtils.capitalize(beanName);
		}
		if (registry.containsBeanDefinition(beanName)) {
			LogUtils.error(getClass(), "Cannot register External Service Proxy bean because a bean with the name already exists: " + beanName);
		}
		else {
			createAndRegisterProxiedBeanDefinition(registry, beanName, beanClass);
		}
	}


	/**
	 * We already know each of these proxied beans is a handler that should have request mappings made for its methods
	 * so we override this method and call detectHandlerMethods directly;
	 */
	@Override
	public void afterPropertiesSet() {
		// intentionally NOT call super to avoid extra initialization already done by a different handler
	}
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Create the 'externalServiceInvocationHandler' beanDefinition so it is available for use
	 * when creating the proxies later.
	 */
	private void createAndRegisterInvocationHandlerBeanDefinition(BeanDefinitionRegistry registry) {
		String invocationHandlerBeanNamePrefix = createInvocationHandlerBeanNamePrefix(getExternalServiceURL());
		ExternalServiceLocatorUtils.registerExternalServiceURL(getExternalServiceURL());

		BeanDefinition invocationHandlerBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(ExternalServiceInvocationHandler.class)
				.addPropertyReference("contextHandler", "contextHandler")
				.addPropertyReference("jacksonHandler", "externalServiceJacksonHandler")
				.addPropertyReference("securityOAuthUserTokenService", "securityOAuthUserTokenService")
				.addPropertyValue("externalServiceURL", getExternalServiceURL())
				.getBeanDefinition();
		registry.registerBeanDefinition(invocationHandlerBeanNamePrefix + "externalServiceInvocationHandler", invocationHandlerBeanDefinition);
	}


	/**
	 * Add corresponding bean definitions proxied to the corresponding specified external services.
	 * Configure and expose web URL's if needed.
	 */
	private void createAndRegisterProxiedBeanDefinition(BeanDefinitionRegistry registry, String beanName, Class<?> beanClass) {
		String invocationHandlerBeanNamePrefix = createInvocationHandlerBeanNamePrefix(getExternalServiceURL());

		BeanDefinition proxyBeanDefinition = BeanDefinitionBuilder.genericBeanDefinition(beanClass)
				.addConstructorArgValue(getBeanClassLoader())
				.addConstructorArgValue(beanClass)
				.addConstructorArgReference(invocationHandlerBeanNamePrefix + "externalServiceInvocationHandler")
				.setFactoryMethodOnBean("createExternalServiceProxyBean", "externalServiceProxyBeanFactory")
				//Without this dependsOn, Portal Admin will get a circular dependency error between portalSecurityUserService -> externalServiceInvocationHandler -> securityOAuthUserTokenService
          		//Because portalSecurityUserService uses a factory method, it can't be instantiated until all of its dependencies are resolved.
          		//However, we can temporarily instantiate securityOAuthUserTokenService, since it does not use a factory method, by adding this dependency, so that it can be used to resolve the portalSecurityUserService dependencies.
				.addDependsOn("securityOAuthUserTokenService")
				.getBeanDefinition();
		registry.registerBeanDefinition(beanName, proxyBeanDefinition);

		if (isServiceWebMethodsExposed()) {
			detectHandlerMethods(beanName);
		}
	}


	private String createInvocationHandlerBeanNamePrefix(String externalServiceURL) {
		return FileUtils.replaceInvalidCharacters(StringUtils.removeAll(externalServiceURL, " "), "_");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public ClassLoader getBeanClassLoader() {
		return this.classLoader;
	}


	public Map<String, String> getExternalServiceInterfaceBasePackageClassPatternMap() {
		return this.externalServiceInterfaceBasePackageClassPatternMap;
	}


	public void setExternalServiceInterfaceBasePackageClassPatternMap(Map<String, String> externalServiceInterfaceBasePackageClassPatternMap) {
		this.externalServiceInterfaceBasePackageClassPatternMap = externalServiceInterfaceBasePackageClassPatternMap;
	}


	public List<String> getExternalServiceInterfaceClassNameList() {
		return this.externalServiceInterfaceClassNameList;
	}


	public void setExternalServiceInterfaceClassNameList(List<String> externalServiceInterfaceClassNameList) {
		this.externalServiceInterfaceClassNameList = externalServiceInterfaceClassNameList;
	}


	public String getExternalServiceURL() {
		String url = this.externalServiceURL;
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		return url;
	}


	public void setExternalServiceURL(String externalServiceURL) {
		this.externalServiceURL = externalServiceURL;
	}


	public String getExternalServiceBeanNamePrefix() {
		return this.externalServiceBeanNamePrefix;
	}


	public void setExternalServiceBeanNamePrefix(String externalServiceBeanNamePrefix) {
		this.externalServiceBeanNamePrefix = externalServiceBeanNamePrefix;
	}


	public boolean isServiceWebMethodsExposed() {
		return this.serviceWebMethodsExposed;
	}


	public void setServiceWebMethodsExposed(boolean serviceWebMethodsExposed) {
		this.serviceWebMethodsExposed = serviceWebMethodsExposed;
	}
}
