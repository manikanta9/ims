package com.clifton.core.web.view;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.file.DataTableToCsvFileConverter;
import com.clifton.core.dataaccess.file.DataTableToExcelFileConverter;
import com.clifton.core.dataaccess.file.DataTableToFileConverter;
import com.clifton.core.dataaccess.file.DataTableToPdfFileConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.util.Map;


/**
 * @author stevenf
 */
public abstract class BaseDownloadView extends JsonView {

	private static final String DEFAULT_ATTACHMENT_CONTENT_TYPE = "application/octet-stream";

	/**
	 * Used to determine where the file download object is within an object.
	 * ex:  For reconciliation we have a ReconciliationRunDetail which has a DataTable so the root is 'dataTable'.
	 * Used in reconciliation-shared.js for all of the reconciliation detail grids.
	 */
	private String downloadPropertiesRoot = "downloadPropertiesRoot";


	@Override
	public void writeResponse(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		DownloadViewCommand command = new DownloadViewCommand(request, response, model);
		if (BooleanUtils.isTrue(model.get(getSuccessPropertyName()))) {
			processRequestObject(command, getRequestObject(command));
		}
		handleResponse(command);
	}


	public Object getRequestObject(DownloadViewCommand command) {
		String downloadPropertiesRootValue = command.getRequest().getParameter(getDownloadPropertiesRoot());
		Map<String, Object> model = command.getModel();
		Object object = model.get(getRootNodeName());
		// If no object in the model, check if there is a datatable that is to be converted
		if (object == null) {
			object = model.get("dataTable");
			if (object == null) {
				String message = "Unable to locate object for processing!";
				model.put("message", message);
				model.put(getSuccessPropertyName(), false);
				LogUtils.error(LogCommand.ofMessage(getClass(), message).withRequest(command.getRequest()));
			}
		}
		else if (!StringUtils.isEmpty(downloadPropertiesRootValue)) {
			Class<?> objectClass = object.getClass();
			object = BeanUtils.getPropertyValue(object, downloadPropertiesRootValue);
			if (object == null) {
				String message = String.format("Unable to find property \"%s\" of %s", getDownloadPropertiesRoot(), objectClass);
				model.put("message", message);
				model.put(getSuccessPropertyName(), false);
				LogUtils.error(LogCommand.ofMessage(getClass(), message).withRequest(command.getRequest()));
			}
		}
		return object;
	}


	public void handleResponse(DownloadViewCommand command) throws Exception {
		HttpServletRequest request = command.getRequest();
		HttpServletResponse response = command.getResponse();
		Map<String, Object> model = command.getModel();

		if (!BooleanUtils.isTrue((Boolean) model.get(getSuccessPropertyName()))) {
			handleError(command);
		}

		// Use Request Param File Name or table name if supplied when returning
		String fileName = null;
		try (BufferedInputStream in = new BufferedInputStream(command.getInputStream()); BufferedOutputStream out = new BufferedOutputStream(response.getOutputStream())) {
			fileName = command.getFileName();
			if (BooleanUtils.isTrue(command.isUseRequestParamFileName())) {
				String requestFileName = request.getParameter("fileName");
				if (StringUtils.isEmpty(requestFileName) || "null".equalsIgnoreCase(requestFileName)) {
					requestFileName = request.getParameter("tableName");
				}
				if (!StringUtils.isEmpty(requestFileName) && !"null".equalsIgnoreCase(requestFileName)) {
					fileName = requestFileName + "." + FileUtils.getFileExtension(fileName);
				}
			}
			response.setContentLengthLong(command.getFileSize());
			response.setHeader("Content-Disposition", "attachment; filename=\"" + FileUtils.replaceInvalidCharacters(fileName, "") + "\"");
			int length;
			byte[] buffer = new byte[8192];
			while ((length = in.read(buffer)) != -1) {
				out.write(buffer, 0, length);
			}
		}
		catch (Throwable e) {
			if (ExceptionUtils.isConnectionBrokenException(e)) {
				// the client closed connection: not much we can do here and no reason logging it
				LogUtils.info(LogCommand.ofThrowableAndMessage(getClass(), e, "Error downloading file '", fileName, "': ", e.toString(), "; ", e.getClass()).withRequest(request));
			}
			else {
				LogUtils.warn(LogCommand.ofThrowableAndMessage(getClass(), e, "Error downloading file '", fileName, "': ", e.toString(), "; ", e.getClass()).withRequest(request));
			}
		}
		finally {
			// clean up resources (DO NOT CLOSE OR FLUSH RESPONSE)
			if (command.isTempFile() && (command.getFile() != null)) {
				// If we know it's a temp file, delete it
				FileUtils.delete(command.getFile());
			}
		}
	}


	public DataTableToFileConverter getDataTableToFileConverter(String outputFormat, boolean onError) {
		if ("pdf".equals(outputFormat)) {
			return new DataTableToPdfFileConverter();
		}
		else if ("csv".equals(outputFormat)) {
			return new DataTableToCsvFileConverter(true, onError);
		}
		else if ("csv_min".equals(outputFormat)) {
			return new DataTableToCsvFileConverter(false, onError);
		}
		else if ("csv_pipe".equals(outputFormat)) {
			return new DataTableToCsvFileConverter(false, onError, '|');
		}
		// Consider for now, if it's not PDF or CSV, then it's Excel
		return new DataTableToExcelFileConverter(outputFormat);
	}


	public abstract void processRequestObject(DownloadViewCommand downloadViewCommand, Object requestObject) throws Exception;


	public abstract void handleError(DownloadViewCommand downloadViewCommand) throws Exception;


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getContentType() {
		return DEFAULT_ATTACHMENT_CONTENT_TYPE;
	}


	@Override
	public void setResponseContentType(@SuppressWarnings("unused") HttpServletRequest request, HttpServletResponse response) {
		response.setContentType(DEFAULT_ATTACHMENT_CONTENT_TYPE);
	}


	public String getDownloadPropertiesRoot() {
		return this.downloadPropertiesRoot;
	}


	public void setDownloadPropertiesRoot(String downloadPropertiesRoot) {
		this.downloadPropertiesRoot = downloadPropertiesRoot;
	}
}
