package com.clifton.core.web.bind;


import com.clifton.core.beans.propertyeditors.JsonPropertyEditor;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.AnnotationUtils;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.context.request.WebRequest;

import java.util.List;


/**
 * The <code>WebBindingInitializerWithValidation</code> class adds validation to the binder for requests
 * that require validation (usually save and update).
 *
 * @author vgomelsky
 */
public class WebBindingInitializerWithValidation extends WebBindingInitializerWithPropertyEditors {

	private WebBindingHandler webBindingHandler;


	////////////////////////////////////////////////////////////////////////////
	////////      WebBindingInitializerWithValidation Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void initBinder(WebDataBinder binder, WebRequest request) {
		WebBindingDataRetriever bindingDataRetriever = getWebBindingHandler().getWebBindingDataRetriever(request);

		if (binder.getTarget() != null && getWebBindingHandler().isValidatingBindingEnabled(binder.getTarget().getClass(), request)) {
			// enable validation
			ValidatingServletRequestDataBinder validatingBinder = (ValidatingServletRequestDataBinder) binder;
			validatingBinder.setWebBindingDataRetriever(bindingDataRetriever); // needed for NonPersistentObject to bind DAO attributes
			Class<?> dtoClass = binder.getTarget().getClass();

			if (!AnnotationUtils.isAnnotationPresent(dtoClass, NonPersistentObject.class)) {
				validatingBinder.setPrepareBean(true);

				if (getWebBindingHandler().isDisableValidatingBindingValidation(request)) {
					validatingBinder.setValidationDisabled(true);
				}
			}
		}

		// execute regular Spring binding initialization
		super.initBinder(binder, request);

		binder.registerCustomEditor(List.class, new JsonPropertyEditor<>(getPropertyEditorMap(), bindingDataRetriever));
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WebBindingHandler getWebBindingHandler() {
		return this.webBindingHandler;
	}


	public void setWebBindingHandler(WebBindingHandler webBindingHandler) {
		this.webBindingHandler = webBindingHandler;
	}
}
