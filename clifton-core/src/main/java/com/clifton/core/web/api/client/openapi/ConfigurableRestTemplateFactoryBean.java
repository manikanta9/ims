package com.clifton.core.web.api.client.openapi;

import org.springframework.beans.factory.FactoryBean;
import org.springframework.web.client.RestTemplate;

import java.util.List;


/**
 * The {@link ConfigurableRestTemplateFactoryBean} is used to create and configure the {@link RestTemplate} object. This is used by the {@link OpenApiClientBeanFactoryPostProcessor}
 * to configure the {@link RestTemplate} objects that are set in the ApiClient beans.
 *
 * @author lnaylor
 */
public class ConfigurableRestTemplateFactoryBean implements FactoryBean<RestTemplate> {

	/**
	 * A list of configurers that are used to provide custom configuration to the restTemplate
	 */
	private List<RestTemplateConfigurer> restTemplateConfigurerList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public RestTemplate getObject() {
		RestTemplate restTemplate = new RestTemplate();
		for (RestTemplateConfigurer configurer : getRestTemplateConfigurerList()) {
			configurer.configure(restTemplate);
		}
		return restTemplate;
	}


	@Override
	public Class<?> getObjectType() {
		return RestTemplate.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<RestTemplateConfigurer> getRestTemplateConfigurerList() {
		return this.restTemplateConfigurerList;
	}


	public void setRestTemplateConfigurerList(List<RestTemplateConfigurer> restTemplateConfigurerList) {
		this.restTemplateConfigurerList = restTemplateConfigurerList;
	}
}
