package com.clifton.core.web.stats;

import java.util.Objects;
import java.util.function.BiFunction;


/**
 * A rule for normalizing system request stats URIs.
 * <p>
 * These rules allow URIs to be modified before being logged for {@link SystemRequestStats}. Modifications include trimming prefixes and suffixes or excluding URIs altogether. This
 * can be useful when URIs contain sensitive information (such as session IDs), when multiple URIs have the same behavior (such as multiple versions of the same endpoint, like
 * {@code /xml/myEndpoint} vs. {@code /json/myEndpoint}), or when URIs should be ignored.
 * <p>
 * This rule match URIs by the {@link RulePatternType} provided. When multiple rules match a URI, the rule with the longest {@link #uriPattern} value will win.
 *
 * @author MikeH
 */
public class SystemRequestStatsUriRule {

	/**
	 * The pattern to match.
	 */
	private final String uriPattern;
	/**
	 * The type for the assigned pattern, used to determine the method in which pattern matching will be performed.
	 */
	private final RulePatternType rulePatternType;
	/**
	 * The type of effect to apply to matched URIs.
	 */
	private final RuleEffect ruleEffect;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemRequestStatsUriRule(String uriPattern, RulePatternType rulePatternType, RuleEffect ruleEffect) {
		this.uriPattern = Objects.requireNonNull(uriPattern);
		this.ruleEffect = Objects.requireNonNull(ruleEffect);
		this.rulePatternType = Objects.requireNonNull(rulePatternType);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns {@code true} if the rule pattern matches the given URI.
	 */
	public boolean matches(String uri) {
		switch (getRulePatternType()) {
			case PREFIX:
				return uri.startsWith(getUriPattern());
			case SUFFIX:
				return uri.endsWith(getUriPattern());
			case EXACT:
				return uri.equals(getUriPattern());
			default:
				throw new UnsupportedOperationException(String.format("Unknown rule pattern type [%s].", getRulePatternType()));
		}
	}


	/**
	 * Returns the modified URI according to the {@link #ruleEffect rule effect}.
	 */
	public String apply(String uri) {
		return getRuleEffect().apply(uri, this);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The type for an assigned pattern, used to determine the method in which pattern matching will be performed.
	 *
	 * @see #matches(String)
	 */
	public enum RulePatternType {
		PREFIX,
		SUFFIX,
		EXACT
	}


	/**
	 * The rule effect to apply to matched URIs.
	 */
	public enum RuleEffect {

		/**
		 * Hides request stats for the given URI all together.
		 */
		HIDE((uri, rule) -> null),
		/**
		 * Uses only the pattern of the rule as the logged URI.
		 */
		PATTERN_ONLY((uri, rule) -> rule.getUriPattern()),
		/**
		 * Trims all instances of the rule pattern from the given URI.
		 */
		TRIMMED((uri, rule) -> uri.replace(rule.getUriPattern(), ""));


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private final BiFunction<String, SystemRequestStatsUriRule, String> applyRuleFn;


		RuleEffect(BiFunction<String, SystemRequestStatsUriRule, String> applyRuleFn) {
			this.applyRuleFn = applyRuleFn;
		}


		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private String apply(String uri, SystemRequestStatsUriRule rule) {
			return this.applyRuleFn.apply(uri, rule);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUriPattern() {
		return this.uriPattern;
	}


	public RulePatternType getRulePatternType() {
		return this.rulePatternType;
	}


	public RuleEffect getRuleEffect() {
		return this.ruleEffect;
	}
}
