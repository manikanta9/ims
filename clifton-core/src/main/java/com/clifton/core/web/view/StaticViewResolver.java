package com.clifton.core.web.view;


import org.springframework.core.Ordered;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.ContentNegotiatingViewResolver;

import java.util.Locale;


/**
 * The <code>StaticViewResolver</code> class implements Spring's ViewResolver that
 * always returns the same static view injected into it (JSON, etc.)
 *
 * @author vgomelsky
 */
public class StaticViewResolver extends ContentNegotiatingViewResolver {

	private View staticView;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public StaticViewResolver() {

		// Change the ContentNegotiatingViewResolver's order from highest precedence (always used)
		// to lowest precedence, so that the DynamicViewResolver is used instead
		setOrder(Ordered.LOWEST_PRECEDENCE);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unused")
	public View resolveViewName(String viewName, Locale locale) throws Exception {
		return getStaticView();
	}


	/**
	 * @return the staticView
	 */
	public View getStaticView() {
		return this.staticView;
	}


	/**
	 * @param staticView the staticView to set
	 */
	public void setStaticView(View staticView) {
		this.staticView = staticView;
	}
}
