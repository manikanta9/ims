package com.clifton.core.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.migrate.schema.Column;
import org.springframework.core.Ordered;
import org.springframework.web.context.request.WebRequest;

import java.io.Serializable;
import java.util.List;


/**
 * The WebBindingDataRetriever interface defines methods that retrieve the data used during
 * web request binding and validation.
 *
 * @author vgomelsky
 */
public interface WebBindingDataRetriever extends Ordered {

	/**
	 * Returns true if this retriever should apply to the specified request.
	 * An application can configure more than one retriever that can be used for different URL patterns, etc.
	 */
	public boolean isApplicableForRequest(WebRequest request);


	/**
	 * Returns the DTO of the specified type for the specified primary key.
	 */
	public <T extends IdentityObject> T getEntity(Class<T> entityType, Serializable entityId);


	/**
	 * Returns Column meta-data for each column of the specified DTO type.
	 */
	public List<Column> getEntityColumnList(Class<? extends IdentityObject> entityType);
}
