package com.clifton.core.web.token;

import java.util.Date;
import java.util.Map;


/**
 * Simple representation of a JWT and its parts
 *
 * @author theodorez
 */
public class JsonWebToken {

	private String algorithmName;

	private Map<String, ?> headerClaims;

	private Map<String, ?> payloadClaims;

	private String signatureKey;

	private String encodedToken;

	private Date expirationDate;


	public JsonWebToken(String algorithmName, Map<String, ?> payloadClaims, String signatureKey, String encodedToken) {
		this.algorithmName = algorithmName;
		this.payloadClaims = payloadClaims;
		this.encodedToken = encodedToken;
		this.signatureKey = signatureKey;
	}


	public String getAlgorithmName() {
		return this.algorithmName;
	}


	public void setAlgorithmName(String algorithmName) {
		this.algorithmName = algorithmName;
	}


	public Date getExpirationDate() {
		return this.expirationDate;
	}


	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}


	public Map<String, ?> getHeaderClaims() {
		return this.headerClaims;
	}


	public void setHeaderClaims(Map<String, ?> headerClaims) {
		this.headerClaims = headerClaims;
	}


	public Map<String, ?> getPayloadClaims() {
		return this.payloadClaims;
	}


	public void setPayloadClaims(Map<String, ?> payloadClaims) {
		this.payloadClaims = payloadClaims;
	}


	public String getEncodedToken() {
		return this.encodedToken;
	}


	public void setEncodedToken(String encodedToken) {
		this.encodedToken = encodedToken;
	}


	public String getSignatureKey() {
		return this.signatureKey;
	}


	public void setSignatureKey(String signatureKey) {
		this.signatureKey = signatureKey;
	}
}
