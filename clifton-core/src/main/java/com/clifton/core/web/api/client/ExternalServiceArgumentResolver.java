package com.clifton.core.web.api.client;

import com.clifton.core.context.Context;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.JavaType;
import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>ExternalServiceArgumentResolver</code> is used to avoid the need to annotate method parameters.
 * By default Spring will only use the 'RequestResponseBodyMethodProcessor' when parameters are annotated with @RequestBody
 * We now use <code>DelegatingHandlerAdapter</code> to route requests based on the request URI path.  Requests routed through
 * /api/ are sent to this custom argument resolver which allows us to send application/json raw data in a post request and have it processed
 * as if the methods were annotated with @RequestBody - additionally this supports posting json to methods with multiple arguments
 * This is not possible if using the default @RequestBody functionality.
 *
 * @author StevenF
 */
public class ExternalServiceArgumentResolver extends HandlerMethodArgumentResolverComposite {

	private static final String ENTITY_WRAPPER_PARAMETER_MAP = "EntityWrapperParameterMap";

	private JacksonHandlerImpl jacksonHandler;
	public JsonFactory factory = new JsonFactory();

	////////////////////////////////////////////////////////////////////////////


	//If a request was sent here by <code>DelegatingHandlerAdapter</code> then we want to process
	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return true;
	}


	/*
	 * When the request is multi-part, each argument is serialized and set as '<parameterName>Json' on the request
	 * as a new text part. This allows us to grab them by name as the 'resolveArgument' method is called for each
	 * parameter. In the case of a normal HttpServletRequest it is always by 'ExternalServiceHttpEntityWrapper' the
	 * internal map of the wrapper is a LinkedHashMap, so we can rely on the order of the arguments being maintained.
	 * We deserialize the object from the stream completely, and set as an attribute on the request so it is available
	 * when the next argument needs to be resolved.  Regardless of the type of request, the original request parameterMap
	 * is added just as arguments are; this means it will be pulled from the multipart parameters, or from our entity wrapper.
	 * After it is pulled and deserialized it is set as an attribute on the request which can be used by other code farther
	 * down the request chain (e.g. see: 'PortalTrackingEventGeneratorImpl')
	 */
	@Override
	public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
		Object argument = null;
		Object nativeRequest = nativeWebRequest.getNativeRequest();
		if (nativeRequest instanceof HttpServletRequest) {
			String parameterName = methodParameter.getParameterName();
			if (nativeRequest instanceof MultipartHttpServletRequest) {
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) nativeRequest;
				//Resolve the argument from json first
				String jsonArgument = multipartRequest.getParameter(parameterName + "Json");
				argument = deserializeJsonArgument(methodParameter, jsonArgument, null);
				if (argument != null) {
					//Now get the file out of the request and invoke the setFile method on the argument
					Method method = MethodUtils.getMethod(argument.getClass(), "setFile", MultipartFile.class);
					if (method != null) {
						method.invoke(argument, multipartRequest.getFileMap().get(parameterName + "File"));
					}
				}
			}
			else {
				HttpServletRequest httpServletRequest = (HttpServletRequest) nativeRequest;
				//First we deserialize to our wrapper and pull params from the map
				ValidationUtils.assertNotNull(methodParameter.getMethod(), "Unable to get method parameter name");
				String jsonArgument = getJsonArgumentFromEntityWrapper(methodParameter.getMethod().getName(), parameterName, httpServletRequest);
				//Now get the type if it was included; otherwise we attempt to infer the type from the method parameter.
				String jsonArgumentType = getJsonArgumentFromEntityWrapper(methodParameter.getMethod().getName(), parameterName + "Type", httpServletRequest);
				argument = deserializeJsonArgument(methodParameter, jsonArgument, jsonArgumentType);
			}
			setOriginalRequestParameterMap((HttpServletRequest) nativeRequest);
		}
		return argument;
	}


	private void setOriginalRequestParameterMap(HttpServletRequest httpServletRequest) {
		//We could have multiple arguments and so this code will run for each argument, but we only want to set this once;
		Object parameterMapObject = httpServletRequest.getAttribute(Context.REQUEST_PARAMETER_MAP);
		if (parameterMapObject == null) {
			String parameterMapJson;
			//Retrieve the request parameter map that has been added to the requests by 'ExternalServiceInvocationHandler'
			if (httpServletRequest instanceof MultipartHttpServletRequest) {
				MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest) httpServletRequest;
				parameterMapJson = multipartRequest.getParameter(Context.REQUEST_PARAMETER_MAP);
			}
			else {
				parameterMapJson = getJsonArgumentFromEntityWrapper(Context.REQUEST_PARAMETER_MAP, Context.REQUEST_PARAMETER_MAP, httpServletRequest);
			}
			if (!StringUtils.isEmpty(parameterMapJson)) {
				//Cache it as an attribute on the request.
				httpServletRequest.setAttribute(Context.REQUEST_PARAMETER_MAP, getJacksonHandler().fromJson(parameterMapJson, Map.class));
			}
		}
	}


	/**
	 * We read the full input stream, but then parse each parameter individually from the string and store in the map.
	 * This map is stored in the web request so when resolveArgument is called for subsequent arguments it simply
	 * need to deserialize the stored json for the current argument using the parameter type information for it.
	 */
	@SuppressWarnings("unchecked")
	private String getJsonArgumentFromEntityWrapper(String methodName, String parameterName, HttpServletRequest httpServletRequest) {
		Map<String, String> parameterMap = (Map<String, String>) httpServletRequest.getAttribute(ENTITY_WRAPPER_PARAMETER_MAP);
		if (parameterMap == null) {
			try {
				parameterMap = new HashMap<>();
				String jsonBody = IOUtils.toString(httpServletRequest.getInputStream(), StandardCharsets.UTF_8.name());
				if (!StringUtils.isEmpty(jsonBody)) {
					parameterMap = (Map<String, String>) getJacksonHandler().fromJson(jsonBody, Map.class);
				}
				httpServletRequest.setAttribute(ENTITY_WRAPPER_PARAMETER_MAP, parameterMap);
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to retrieve parameter [" + parameterName + "] for method [" + methodName + "] from request body.", e);
			}
		}
		return parameterMap.get(parameterName);
	}


	private Object deserializeJsonArgument(MethodParameter methodParameter, String jsonArgument, String jsonArgumentType) throws Exception {
		String parameterName = methodParameter.getParameterName();
		ValidationUtils.assertNotNull(jsonArgument, "Failed to retrieve parameter [" + parameterName + "] for method [" + (methodParameter.getMethod() == null ? "null" : methodParameter.getMethod().getName()) + "] from request body.");
		Class<?> parameterTypeClass = methodParameter.getParameterType();
		if (jsonArgumentType != null) {
			parameterTypeClass = Class.forName(jsonArgumentType);
		}
		if (parameterTypeClass.isInterface()) {
			//Unwrap the returnValue and get the returnTypeClass
			ExternalServiceObjectWrapper objectWrapper = getJacksonHandler().getObjectMapper().readValue(jsonArgument, ExternalServiceObjectWrapper.class);
			jsonArgument = objectWrapper.getObjectJsonString();
			JavaType type = getJacksonHandler().getObjectMapper().getTypeFactory().constructType(methodParameter.getNestedGenericParameterType());
			// If it's not a container (list, map, etc.) then try to use the defined class
			if (type == null || !type.isContainerType()) {
				if (objectWrapper.getObjectClassName() != null) {
					return getJacksonHandler().fromJson(jsonArgument, Class.forName(objectWrapper.getObjectClassName()));
				}
			}
			return getJacksonHandler().getObjectMapper().readValue(jsonArgument, type);
		}
		return getJacksonHandler().fromJson(jsonArgument, parameterTypeClass);
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonFactory getFactory() {
		return this.factory;
	}


	public void setFactory(JsonFactory factory) {
		this.factory = factory;
	}


	public JacksonHandlerImpl getJacksonHandler() {
		return this.jacksonHandler;
	}


	public void setJacksonHandler(JacksonHandlerImpl jacksonHandler) {
		this.jacksonHandler = jacksonHandler;
	}
}
