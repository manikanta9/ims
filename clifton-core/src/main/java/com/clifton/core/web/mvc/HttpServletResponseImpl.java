package com.clifton.core.web.mvc;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.Locale;


/**
 * The <code>HttpServletResponseImpl</code> is an internal class that provides an {@link HttpServletResponse}
 * which relies on an {@link HttpServletRequest} for some values, and defaults the rest. This response is
 * needed to avoid using null in the spring MVC methods.
 *
 * @author NickK
 */
public class HttpServletResponseImpl implements HttpServletResponse {

	private final HttpServletRequest request;


	public HttpServletResponseImpl(HttpServletRequest request) {
		this.request = request;
	}


	@Override
	public void addCookie(Cookie cookie) {
		// Auto-generated method stub
	}


	@Override
	public boolean containsHeader(String name) {
		return false;
	}


	@Override
	public String encodeURL(String url) {
		return url;
	}


	@Override
	public String encodeRedirectURL(String url) {
		return url;
	}


	@Override
	public String encodeUrl(String url) {
		return url;
	}


	@Override
	public String encodeRedirectUrl(String url) {
		return url;
	}


	@Override
	public void sendError(int sc, String msg) throws IOException {
		// Auto-generated method stub
	}


	@Override
	public void sendError(int sc) throws IOException {
		// Auto-generated method stub
	}


	@Override
	public void sendRedirect(String location) throws IOException {
		// Auto-generated method stub
	}


	@Override
	public void setDateHeader(String name, long date) {
		// Auto-generated method stub
	}


	@Override
	public void addDateHeader(String name, long date) {
		// Auto-generated method stub
	}


	@Override
	public void setHeader(String name, String value) {
		// Auto-generated method stub
	}


	@Override
	public void addHeader(String name, String value) {
		// Auto-generated method stub
	}


	@Override
	public void setIntHeader(String name, int value) {
		// Auto-generated method stub
	}


	@Override
	public void addIntHeader(String name, int value) {
		// Auto-generated method stub
	}


	@Override
	public void setStatus(int sc) {
		// Auto-generated method stub
	}


	@Override
	public void setStatus(int sc, String sm) {
		// Auto-generated method stub
	}


	@Override
	@ValueIgnoringGetter
	public int getStatus() {
		// Auto-generated method stub
		return 0;
	}


	@Override
	public String getHeader(String name) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Collection<String> getHeaders(String name) {
		// Auto-generated method stub
		return null;
	}


	@Override
	public Collection<String> getHeaderNames() {
		// Auto-generated method stub
		return null;
	}


	@Override
	@ValueIgnoringGetter
	public String getCharacterEncoding() {
		return this.request.getCharacterEncoding();
	}


	@Override
	@ValueIgnoringGetter
	public String getContentType() {
		return this.request.getContentType();
	}


	@Override
	public ServletOutputStream getOutputStream() throws IOException {
		return null;
	}


	@Override
	public PrintWriter getWriter() throws IOException {
		return null;
	}


	@Override
	public void setCharacterEncoding(String charset) {
		// Auto-generated method stub
	}


	@Override
	public void setContentLength(int len) {
		// Auto-generated method stub
	}


	@Override
	public void setContentLengthLong(long len) {
		// Auto-generated method stub
	}


	@Override
	public void setContentType(String type) {
		// Auto-generated method stub
	}


	@Override
	public void setBufferSize(int size) {
		// Auto-generated method stub
	}


	@Override
	@ValueIgnoringGetter
	public int getBufferSize() {
		return 0;
	}


	@Override
	public void flushBuffer() throws IOException {
		// Auto-generated method stub
	}


	@Override
	public void resetBuffer() {
		// Auto-generated method stub
	}


	@Override
	public boolean isCommitted() {
		return false;
	}


	@Override
	public void reset() {
		// Auto-generated method stub
	}


	@Override
	public void setLocale(Locale loc) {
		// Auto-generated method stub
	}


	@Override
	public Locale getLocale() {
		return this.request.getLocale();
	}
}
