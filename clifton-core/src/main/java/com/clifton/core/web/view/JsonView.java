package com.clifton.core.web.view;


import com.clifton.core.context.ContextHandler;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.MapToJsonStringWriter;
import com.clifton.core.converter.json.MapWithRules;
import com.clifton.core.converter.json.MapWithRulesToJsonStringWriter;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.http.HttpUtils;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedWriter;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>JsonView</code> serializes Spring's Model object to JSON format.
 *
 * @author vgomelsky
 */
public class JsonView extends BaseView {

	// Lower limits for serialization parameters
	public static final int MINIMUM_RESPONSE_BUFFER_SIZE = 8 * (1 << 10); // 8 KB
	public static final long MINIMUM_RESPONSE_LIMIT_SIZE = 128 * (1 << 10); // 128 KB

	/**
	 * The attribute name under which the map for pre-serialization entity-conversions may be stored when application types need to be mapped for serialization purposes only.
	 *
	 * @see ServletRequest#getAttribute(String)
	 * @see JsonSession#entityConversionMap
	 */
	public static final String ENTITY_CONVERSION_MAP_ATTRIBUTE_NAME = "entityConversionMap";
	private static final String DEFAULT_JSON_CONTENT_TYPE = "application/json; charset=UTF-8";
	private static final String DEFAULT_JAVASCRIPT_CONTENT_TYPE = "text/javascript; charset=UTF-8";

	/**
	 * The string delimiter used to indicate a <i>reset</i> within a streamed response.
	 * <p>
	 * This is handled specially by custom client response handling. Anytime that this string is discovered, the client will disregard this string and all contents before it,
	 * effectively resetting the response body content.
	 * <p>
	 * This is necessary when streamed responses are interrupted. For example, if an exception is thrown mid-serialization, then the client may have already received a partially-
	 * serialized JSON string. Since this string has not been completed serialized, it is likely to be malformatted JSON and must be discarded. Additionally, it may be necessary to
	 * send further information to the client, such as a string describing the exception. By using this delimiter, we can direct the client to discard the malformatted JSON and to
	 * only regard the newly-serialized string describing the exception.
	 */
	private static final String RESPONSE_RESET_DELIMITER = StringUtils.NEW_LINE + "// RESET ";

	/**
	 * Request parameter name that contains | delimited properties that need to be serialized and returned.
	 * If parameter is not specified, return all properties.
	 * Example: requestedProperties="id|interestRateDate|interestRate|dataSource.label"
	 */
	private String requestedPropertiesFilter = "requestedProperties";

	private String requestedPropertiesToExclude = "requestedPropertiesToExclude";

	private String requestedPropertiesToExcludeGlobally = "requestedPropertiesToExcludeGlobally";

	/**
	 * Request parameter name that contains the optional root of properties specified by the requestedPropertiesFilter
	 * Example: requestedPropertiesRoot="data.rows"
	 */
	private String requestedPropertiesRoot = "requestedPropertiesRoot";
	/**
	 * The maximum depth of nested bean properties to process
	 */
	private String requestedMaxDepth = "requestedMaxDepth";

	/**
	 * The pre-response buffer size. This is a preliminary buffer before the content is written to and handled by the response stream. While the response stream is buffered as
	 * well, benchmarks show that large responses are transmitted in significantly less time when completed in batch. This appears to be caused by delays due to excessive expensive
	 * operations within the response stream writer.
	 * <p>
	 * Each response requires a new block of memory allocated for this buffer. Large buffers may be unsuitable for systems which have many small responses.
	 */
	private int responseBufferSize;
	/**
	 * The maximum serialized response length to write before an exception is raised. This field is useful for terminating inordinately large responses.
	 */
	private long responseLimitSize;

	private MapToJsonStringWriter jsonWriter = new MapToJsonStringWriter();
	private MapWithRulesToJsonStringWriter filteringJsonWriter = new MapWithRulesToJsonStringWriter();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getContentType() {
		return DEFAULT_JSON_CONTENT_TYPE;
	}


	@Override
	public void writeResponse(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception {
		// Wrap response stream
		BufferedWriter bufferedWriter = new BufferedWriter(response.getWriter(), MathUtils.max(getResponseBufferSize(), MINIMUM_RESPONSE_BUFFER_SIZE));
		SizeLimitedAppender bufferAppender = new SizeLimitedAppender(bufferedWriter, MathUtils.max(getResponseLimitSize(), MINIMUM_RESPONSE_LIMIT_SIZE));

		// Write response
		try {
			String callBack = request.getParameter("callback");
			if (callBack != null) {
				bufferAppender.append(callBack).append("(");
			}
			@SuppressWarnings("unchecked")
			Map<Object, Object> entityConversionMap = (Map<Object, Object>) request.getAttribute(ENTITY_CONVERSION_MAP_ATTRIBUTE_NAME);
			String filterStartPattern = request.getParameter(getRequestedPropertiesRoot());
			String propertiesToFilter = HttpUtils.getUrlDecodedParameterValue(request, getRequestedPropertiesFilter());
			String propertiesToExclude = HttpUtils.getUrlDecodedParameterValue(request, getRequestedPropertiesToExclude());
			String propertiesToExcludeGlobally = HttpUtils.getUrlDecodedParameterValue(request, getRequestedPropertiesToExcludeGlobally());
			String maxDepth = request.getParameter(getRequestedMaxDepth());
			int depth = StringUtils.isEmpty(maxDepth) ? MapWithRules.DEFAULT_MAX_DEPTH : Integer.parseInt(maxDepth);
			MapWithRules mapWithRules = new MapWithRules(model, entityConversionMap, filterStartPattern, propertiesToFilter, propertiesToExclude, propertiesToExcludeGlobally, depth, (depth != MapWithRules.DEFAULT_MAX_DEPTH));
			writeModelAsJson(bufferAppender, mapWithRules);
			if (callBack != null) {
				bufferAppender.append(");");
			}
		}
		finally {
			// Manually flush buffered writer, as any remaining unflushed content in the buffer will otherwise be ignored
			bufferedWriter.flush();
		}
	}


	@Override
	public void setResponseContentType(HttpServletRequest request, HttpServletResponse response) {
		String callBack = request.getParameter("callback");
		if (callBack != null) {
			response.setContentType(DEFAULT_JAVASCRIPT_CONTENT_TYPE);
		}
		else {
			response.setContentType(DEFAULT_JSON_CONTENT_TYPE);
		}
	}


	/**
	 * Returns JSON representation of the specified model.  If an error occurred during serialization,
	 * returns the JSON error message instead.
	 */
	private void writeModelAsJson(SizeLimitedAppender sizeLimitedAppender, MapWithRules modelWithRules) {
		try {
			Map<String, Object> map = modelWithRules.getMap();
			if (map.size() == 3) {
				// when serializing exceptions, exclude the object itself (it can have lazily initialized Hibernate properties that may fail if session in view is closed
				if (map.containsKey(getSuccessPropertyName()) && map.containsKey(getErrorMessagePropertyName())) {
					map.remove("exception");
				}
			}

			// add additional signal(s) if any: could be acted upon in UI
			@SuppressWarnings("unchecked")
			MultiValueMap<String, Object> eventMap = (MultiValueMap<String, Object>) getContextHandler().getBean(ContextHandler.APPLICATION_EVENTS_KEY);
			if (eventMap != null) {
				map.put(ContextHandler.APPLICATION_EVENTS_KEY, eventMap.cloneMap());
			}

			getFilteringJsonWriter().write(sizeLimitedAppender, modelWithRules);
		}
		catch (Throwable e) {
			RequestAttributes attributes = RequestContextHolder.currentRequestAttributes();
			HttpServletRequest request = attributes instanceof ServletRequestAttributes
					? ((ServletRequestAttributes) attributes).getRequest()
					: null;
			try {
				// Attempt recovery, serializing exception
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, "Error serializing Json View: ", modelWithRules).withRequest(request));
				Map<String, Object> errorModel = new HashMap<>();
				errorModel.put("exception", e);
				errorModel.put(getErrorMessagePropertyName(), e.toString());
				errorModel.put(getSuccessPropertyName(), false);
				// Use re-wrapped writer to reset size limits
				Appendable outputWriter = new SizeLimitedAppender(sizeLimitedAppender.getOut(), sizeLimitedAppender.getMaxSize());
				outputWriter.append(RESPONSE_RESET_DELIMITER);
				getJsonWriter().write(outputWriter, errorModel);
			}
			catch (Throwable e2) {
				try {
					// Attempt recovery, serializing generic error message
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e2, "Error serializing Json View error: ", modelWithRules).withRequest(request));
					// Use re-wrapped writer to reset size limits
					Appendable outputWriter = new SizeLimitedAppender(sizeLimitedAppender.getOut(), sizeLimitedAppender.getMaxSize());
					outputWriter.append(RESPONSE_RESET_DELIMITER)
							.append("{\"")
							.append(getErrorMessagePropertyName())
							.append("\":\"")
							.append(StringUtils.replace(e.toString(), "\"", "\\\""))
							.append("\",\"")
							.append(getSuccessPropertyName())
							.append("\":false}");
				}
				catch (Throwable e3) {
					// Recovery failed; terminate
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e3, "Error recovering from Json View serialization error: ", modelWithRules).withRequest(request));
					throw new RuntimeException("An unrecoverable exception occurred during serialization.", e3);
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRequestedPropertiesFilter() {
		return this.requestedPropertiesFilter;
	}


	public void setRequestedPropertiesFilter(String requestedPropertiesFilter) {
		this.requestedPropertiesFilter = requestedPropertiesFilter;
	}


	public String getRequestedPropertiesToExclude() {
		return this.requestedPropertiesToExclude;
	}


	public void setRequestedPropertiesToExclude(String requestedPropertiesToExclude) {
		this.requestedPropertiesToExclude = requestedPropertiesToExclude;
	}


	public String getRequestedPropertiesToExcludeGlobally() {
		return this.requestedPropertiesToExcludeGlobally;
	}


	public void setRequestedPropertiesToExcludeGlobally(String requestedPropertiesToExcludeGlobally) {
		this.requestedPropertiesToExcludeGlobally = requestedPropertiesToExcludeGlobally;
	}


	public String getRequestedPropertiesRoot() {
		return this.requestedPropertiesRoot;
	}


	public void setRequestedPropertiesRoot(String requestedPropertiesRoot) {
		this.requestedPropertiesRoot = requestedPropertiesRoot;
	}


	public String getRequestedMaxDepth() {
		return this.requestedMaxDepth;
	}


	public void setRequestedMaxDepth(String requestedMaxDepth) {
		this.requestedMaxDepth = requestedMaxDepth;
	}


	public int getResponseBufferSize() {
		return this.responseBufferSize;
	}


	public void setResponseBufferSize(int responseBufferSize) {
		this.responseBufferSize = responseBufferSize;
	}


	public long getResponseLimitSize() {
		return this.responseLimitSize;
	}


	public void setResponseLimitSize(long responseLimitSize) {
		this.responseLimitSize = responseLimitSize;
	}


	public MapToJsonStringWriter getJsonWriter() {
		return this.jsonWriter;
	}


	public void setJsonWriter(MapToJsonStringWriter jsonWriter) {
		this.jsonWriter = jsonWriter;
	}


	public MapWithRulesToJsonStringWriter getFilteringJsonWriter() {
		return this.filteringJsonWriter;
	}


	public void setFilteringJsonWriter(MapWithRulesToJsonStringWriter filteringJsonWriter) {
		this.filteringJsonWriter = filteringJsonWriter;
	}
}
