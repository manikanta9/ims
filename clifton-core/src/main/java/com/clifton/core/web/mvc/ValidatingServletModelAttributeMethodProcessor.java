package com.clifton.core.web.mvc;

import com.clifton.core.beans.BaseEntityWithNaturalKey;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.bind.WebBindingDataRetriever;
import com.clifton.core.web.bind.WebBindingHandler;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ServletModelAttributeMethodProcessor;

import java.lang.reflect.Constructor;


/**
 * The <code>ValidatingServletModelAttributeMethodProcessor</code> class extends {@link ServletModelAttributeMethodProcessor} and
 * overrides the <code>constructAttribute</code> method to handle validation logic and NonPersistentObject logic during data binding.
 *
 * @author michaelm
 */
public class ValidatingServletModelAttributeMethodProcessor extends ServletModelAttributeMethodProcessor {

	private WebBindingHandler webBindingHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Class constructor.
	 *
	 * @param annotationNotRequired if "true", non-simple method arguments and
	 *                              return values are considered model attributes with or without a
	 *                              {@code @ModelAttribute} annotation
	 */
	public ValidatingServletModelAttributeMethodProcessor(boolean annotationNotRequired) {
		super(annotationNotRequired);
	}


	////////////////////////////////////////////////////////////////////////////
	////////     ServletModelAttributeMethodProcessor Methods      /////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method ensures that the <code>ModelAttributeMethodProcessor.attribute</code> object will reference the desired <code>DataBinder.target</code> object
	 * before it is passed to the data binder in {@link org.springframework.web.method.annotation.ModelAttributeMethodProcessor#resolveArgument}.
	 * It is important to inject the correct target object into the binder because Spring expects an immutable object to populate.
	 */
	@Override
	protected Object constructAttribute(Constructor<?> ctor, String attributeName, MethodParameter parameter,
	                                    WebDataBinderFactory binderFactory, NativeWebRequest request) throws Exception {
		Class<?> dtoClass = ctor.getDeclaringClass();
		if (getWebBindingHandler().isValidatingBindingEnabled(dtoClass, request)) {
			if (!AnnotationUtils.isAnnotationPresent(dtoClass, NonPersistentObject.class)) {
				Long id = getLongParameter(request, "id", (long) 0);
				if (id != null && id > 0) {
					WebBindingDataRetriever bindingDataRetriever = getWebBindingHandler().getWebBindingDataRetriever(request);
					// get existing DTO instance from DB (if not new) and replace the target with populated object
					@SuppressWarnings("unchecked")
					Class<? extends IdentityObject> identityObjectDtoClass = (Class<? extends IdentityObject>) dtoClass;
					Object existingDTO = bindingDataRetriever.getEntity(identityObjectDtoClass, id);
					if (existingDTO != null) {
						return existingDTO;
					}
					AssertUtils.assertTrue(BaseEntityWithNaturalKey.class.isAssignableFrom(identityObjectDtoClass), "Cannot find %1s DTO for id = %2d", dtoClass.getSimpleName(), id);
					// Entities with a natural ID are created with an ID on initial save. Thus, create an instance with the ID for binding.
					IdentityObject newInstance = BeanUtils.newInstance(identityObjectDtoClass);
					BeanUtils.setPropertyValue(newInstance, "id", id);
					return newInstance;
				}
			}
			else {
				// Check if class is an instance of the class that you pass in
				String bindingClassDtoName = getWebBindingHandler().getBindingClassDtoName(request);
				if (!StringUtils.isEmpty(bindingClassDtoName)) {
					Class<?> bindingDtoClass = CoreClassUtils.getClass(bindingClassDtoName);
					if (dtoClass.isAssignableFrom(bindingDtoClass)) {
						return BeanUtils.newInstance(bindingDtoClass);
					}
					else {
						throw new RuntimeException("Parameter [" + WebBindingHandler.DTO_CLASS_FOR_BINDING_PARAMETER + "] excepts to bind to class [" + bindingClassDtoName + "], but method parameter is of class [" + dtoClass.getName() + "] which is not compatible");
					}
				}
			}
		}
		return super.constructAttribute(ctor, attributeName, parameter, binderFactory, request);
	}


	////////////////////////////////////////////////////////////////////////////
	////////  ValidatingServletModelAttributeMethodProcessor Methods  //////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns int value of the specified parameter or defaultValue is the value is not present or blank.
	 */
	private Long getLongParameter(WebRequest request, String parameterName, Long defaultValue) {
		String result = request.getParameter(parameterName);
		if (!StringUtils.isEmpty(result)) {
			try {
				return Long.parseLong(result);
			}
			catch (NumberFormatException e) {
				throw new IllegalArgumentException("Parameter '" + parameterName + "' value '" + result + "' must be an integer or long.", e);
			}
		}
		return defaultValue;
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public WebBindingHandler getWebBindingHandler() {
		return this.webBindingHandler;
	}


	public void setWebBindingHandler(WebBindingHandler webBindingHandler) {
		this.webBindingHandler = webBindingHandler;
	}
}
