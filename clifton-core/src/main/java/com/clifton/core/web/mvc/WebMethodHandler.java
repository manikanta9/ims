package com.clifton.core.web.mvc;


import java.util.Map;


/**
 * The <code>WebMethodHandler</code> defines a web method handler used to call service methods based on there urls.
 *
 * @author mwacker
 */
public interface WebMethodHandler {

	/**
	 * Execute a web service method given the url and parameters.  For example,
	 * <p>
	 * NOTE: parameters will be parsed from the url and combined with the parameter map that is passed in.
	 * <p>
	 * Example:
	 * <pre>
	 * url = 'systemListItemListFind.json?listName=Bond Coupon Frequency'
	 * parameters = [value='Weekly']
	 * </pre>
	 */
	public <T> T executeServiceCall(String url, Map<String, String> parameters);
}
