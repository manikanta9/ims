package com.clifton.core.web.stats;

import com.clifton.core.util.jmx.MBeanWrapper;
import com.clifton.core.util.runner.ExecutorServiceStatus;

import java.util.Collection;
import java.util.Comparator;
import java.util.Objects;
import java.util.OptionalLong;


/**
 * The {@link RequestProcessorStatus} is a status object for an instantaneous state of request processors within the system.
 * <p>
 * This object encapsulates properties which convey the current state of the servlet container request handling system, including the {@link ExecutorServiceStatus}, the {@link
 * #endpointList endpoints}, and the {@link #requestProcessorList request processors}.
 *
 * @author MikeH
 */
public class RequestProcessorStatus {

	private final int responseBufferSize;
	private final long responseLimitSize;

	private final Collection<MBeanWrapper> endpointList;
	private final Collection<ExecutorServiceStatus> executorServiceStatusList;
	private final Collection<MBeanWrapper> requestProcessorList;

	/**
	 * The total number of requests that have been processed.
	 */
	private final Integer totalRequestCount;
	/**
	 * The total number of requests which have resulted in errors (HTTP status code of 400 or greater).
	 */
	private final Integer totalErrorCount;
	/**
	 * The total processing time used for handling requests.
	 */
	private final Long totalProcessingTime;
	/**
	 * The average processing time used per request.
	 */
	private final Long averageProcessingTime;
	/**
	 * The request URI which has taken the longest time to process.
	 */
	private final String maxRequestUri;
	/**
	 * The amount of time that it took to processing the {@link #maxRequestUri longest request URI}.
	 */
	private final Long maxRequestTime;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RequestProcessorStatus(int responseBufferSize, long responseLimitSize, Collection<MBeanWrapper> endpointList, Collection<ExecutorServiceStatus> executorServiceStatusList, Collection<MBeanWrapper> requestProcessorList) {
		this.responseBufferSize = responseBufferSize;
		this.responseLimitSize = responseLimitSize;
		this.endpointList = endpointList;
		this.executorServiceStatusList = executorServiceStatusList;
		this.requestProcessorList = requestProcessorList;

		this.totalRequestCount = computeTotalRequestCount();
		this.totalErrorCount = computeTotalErrorCount();
		this.totalProcessingTime = computeTotalProcessingTime();
		this.averageProcessingTime = computeAverageProcessingTime();
		this.maxRequestUri = computeMaxRequestUri();
		this.maxRequestTime = computeMaxRequestTime();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int computeTotalRequestCount() {
		return getRequestProcessorList().stream()
				.map(requestProcessor -> requestProcessor.getAttributeMap().get("requestCount"))
				.filter(Objects::nonNull)
				.mapToInt(requestCountAttr -> (int) requestCountAttr.getValue())
				.sum();
	}


	private int computeTotalErrorCount() {
		return getRequestProcessorList().stream()
				.map(requestProcessor -> requestProcessor.getAttributeMap().get("errorCount"))
				.filter(Objects::nonNull)
				.mapToInt(errorCountAttr -> (int) errorCountAttr.getValue())
				.sum();
	}


	private long computeTotalProcessingTime() {
		return getRequestProcessorList().stream()
				.map(requestProcessor -> requestProcessor.getAttributeMap().get("processingTime"))
				.filter(Objects::nonNull)
				.mapToLong(processingTimeAttr -> (long) processingTimeAttr.getValue())
				.sum();
	}


	private Long computeAverageProcessingTime() {
		return getTotalRequestCount() > 0
				? getTotalProcessingTime() / getTotalRequestCount()
				: null;
	}


	private String computeMaxRequestUri() {
		return getRequestProcessorList().stream()
				.max(Comparator.comparing(requestProcessor -> {
					MBeanWrapper.AttributeWrapper maxRequestTimeAttr = requestProcessor.getAttributeMap().get("maxTime");
					return maxRequestTimeAttr != null ? (long) maxRequestTimeAttr.getValue() : 0;
				}))
				.map(requestProcessor -> requestProcessor.getAttributeMap().get("maxRequestUri"))
				.map(maxRequestUriAttr -> (String) maxRequestUriAttr.getValue())
				.orElse("N/A");
	}


	private Long computeMaxRequestTime() {
		OptionalLong maxTimeOptional = getRequestProcessorList().stream()
				.map(requestProcessor -> requestProcessor.getAttributeMap().get("maxTime"))
				.filter(Objects::nonNull)
				.mapToLong(maxTime -> (long) maxTime.getValue())
				.max();
		return maxTimeOptional.isPresent()
				? maxTimeOptional.getAsLong()
				: null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getResponseBufferSize() {
		return this.responseBufferSize;
	}


	public long getResponseLimitSize() {
		return this.responseLimitSize;
	}


	public Collection<MBeanWrapper> getEndpointList() {
		return this.endpointList;
	}


	public Collection<ExecutorServiceStatus> getExecutorServiceStatusList() {
		return this.executorServiceStatusList;
	}


	public Collection<MBeanWrapper> getRequestProcessorList() {
		return this.requestProcessorList;
	}


	public Integer getTotalRequestCount() {
		return this.totalRequestCount;
	}


	public Integer getTotalErrorCount() {
		return this.totalErrorCount;
	}


	public Long getTotalProcessingTime() {
		return this.totalProcessingTime;
	}


	public Long getAverageProcessingTime() {
		return this.averageProcessingTime;
	}


	public String getMaxRequestUri() {
		return this.maxRequestUri;
	}


	public Long getMaxRequestTime() {
		return this.maxRequestTime;
	}
}
