package com.clifton.core.web.converter;


/**
 * The <code>IntegerHttpMessageConverter</code> class supports methods that return java.lang.Integer value and are annotated with @ResponseBody.
 * It's a copy of StringHttpMessageConverter with Integer specific changes.
 * <p/>
 * Uses BigDecimal.toString()
 *
 * @author vgomelsky
 */
public class IntegerHttpMessageConverter extends BaseHttpMessageConverter<Integer> {


	@Override
	public boolean supports(Class<?> clazz) {
		return Integer.class.equals(clazz);
	}


	@Override
	protected Integer convertFromString(String stringValue) {
		return Integer.valueOf(stringValue);
	}
}
