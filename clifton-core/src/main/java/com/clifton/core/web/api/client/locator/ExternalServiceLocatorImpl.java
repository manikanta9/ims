package com.clifton.core.web.api.client.locator;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextConventionUtils;
import org.springframework.stereotype.Component;


/**
 * @author vgomelsky
 */
@Component
public class ExternalServiceLocatorImpl implements ExternalServiceLocator {

	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public <T> T getExternalService(Class<T> serviceInterface, String dataSourceName) {
		String beanName = ContextConventionUtils.getExternalApplicationBeanNameForDataSource(dataSourceName, serviceInterface);
		return (T) getApplicationContextService().getContextBean(beanName);
	}


	@Override
	public boolean isExternalService(Object serviceObject) {
		// external services are implemented as java proxies
		String className = serviceObject.getClass().getName();
		if (className.startsWith("com.sun.proxy.")) {
			return true;
		}
		if (className.startsWith("com.clifton.")) {
			return false;
		}
		throw new IllegalArgumentException("Cannot determine if the specified object is external or internal service: " + className);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////             Getter and Setter Methods            //////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
