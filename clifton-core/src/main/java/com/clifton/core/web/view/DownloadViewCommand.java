package com.clifton.core.web.view;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.util.Map;


/**
 * @author stevenf
 */
public class DownloadViewCommand {

	private HttpServletRequest request;
	private HttpServletResponse response;
	private Map<String, Object> model;


	private FilePath file;
	private String fileName;
	private InputStream inputStream;

	private long fileSize = 0;

	boolean tempFile = false;
	boolean useRequestParamFileName = false;


	public DownloadViewCommand(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) {
		this.request = request;
		this.response = response;
		this.model = model;
	}

	////////////////////////////////////////////////////////////////////////////////


	public InputStream getInputStream() throws IOException {
		if (getFile() != null && this.inputStream == null) {
			setFileSize(FileUtils.getLength(getFile()));
			if (StringUtils.isEmpty(this.fileName)) {
				setFileName(FileUtils.getFileName(getFile().getPath()));
			}
			this.inputStream = FileUtils.getInputStream(getFile());
		}
		return this.inputStream;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////////


	public HttpServletRequest getRequest() {
		return this.request;
	}


	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}


	public HttpServletResponse getResponse() {
		return this.response;
	}


	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}


	public Map<String, Object> getModel() {
		return this.model;
	}


	public void setModel(Map<String, Object> model) {
		this.model = model;
	}


	public FilePath getFile() {
		return this.file;
	}


	public void setFile(FilePath file) {
		this.file = file;
	}


	public String getFileName() {
		return this.fileName;
	}


	public void setFileName(String fileName) {
		this.fileName = fileName;
	}


	public void setInputStream(InputStream inputStream) {
		this.inputStream = inputStream;
	}


	public long getFileSize() {
		return this.fileSize;
	}


	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}


	public boolean isTempFile() {
		return this.tempFile;
	}


	public void setTempFile(boolean tempFile) {
		this.tempFile = tempFile;
	}


	public boolean isUseRequestParamFileName() {
		return this.useRequestParamFileName;
	}


	public void setUseRequestParamFileName(boolean useRequestParamFileName) {
		this.useRequestParamFileName = useRequestParamFileName;
	}
}
