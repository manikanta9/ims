package com.clifton.core.web.handler;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CoreExceptionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.web.api.client.ExternalServiceExceptionWrapper;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.handler.SimpleMappingExceptionResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * The <code>LoggingExceptionResolver</code> class logs exception details before passing processing to SimpleMappingExceptionResolver.
 * It also converts data access exceptions to corresponding validation exceptions if possible.
 *
 * @author vgomelsky
 */
public class LoggingExceptionResolver extends SimpleMappingExceptionResolver {

	private JacksonHandlerImpl jacksonHandler;
	private Map<Pattern, String> urlPatternToErrorViewNameMap;
	private Converter<Exception, Exception> dataAccessExceptionConverter;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * The override handles logging our exceptions only (because we need access to the handler), then calls super method to actually resolve and return the
	 * ModelAndView Because we log here, we override logException method as well, but do nothing so we don't duplicate errors in the logs - or log errors that
	 * shouldn't be logged (ValidationExceptions)
	 */
	@Override
	public ModelAndView resolveException(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) {
		if (ex instanceof DaoException) {
			ex = getDataAccessExceptionConverter().convert(ex);
		}
		else if (ex instanceof RuntimeException) {
			RuntimeException original = (RuntimeException) ex;
			ex = getDataAccessExceptionConverter().convert(new WebMethodCallException(request, handler, ex));
			// If no conversion occurs - go back to original exception
			if (ex instanceof WebMethodCallException) {
				ex = original;
			}
		}


		if (ex instanceof BindException) {
			FieldError fieldError = ((BindException) ex).getFieldError();
			if (fieldError == null) {
				ex = new ValidationException(ex.getMessage(), ex);
			}
			else {
				ex = new FieldValidationException(fieldError.getDefaultMessage(), fieldError.getField(), ex);
			}
		}
		else if (!(ex instanceof ValidationException)) {
			// check if original exception is a validation exception
			Throwable original = ExceptionUtils.getOriginalException(ex);
			if (original instanceof ValidationException) {
				ex = (Exception) original;
			}
			else {
				// log security exceptions; ignore exceptions when the client is disconnected: can't do much
				if (ex instanceof SecurityException || ExceptionUtils.isConnectionBrokenException(ex)) {
					// TODO: if we make it so that UI doesn't allow service calls that the user doesn't have permissions for, then we can log it as WARN
					LogUtils.info(LogCommand.ofThrowable(getClass(), ex).withRequest(request));
				}
				else {
					String msg = ExceptionUtils.getNormalizedMessage(CoreExceptionUtils.getLoggableOrOriginalException(ex));
					LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), ex, msg).withRequest(request));
				}
			}
		}

		if (ex instanceof ValidationException) {
			// info log validation errors so that it's easy to find the problem in development
			LogUtils.info(LogCommand.ofThrowable(getClass(), ex).withRequest(request));
		}

		//If this was an external service call, then we need to wrap the exception for deserialization
		Map<Pattern, String> errorViewNameMap = getUrlPatternToErrorViewNameMap();
		for (Map.Entry<Pattern, String> viewNameEntry : errorViewNameMap.entrySet()) {
			if (viewNameEntry.getKey().matcher(request.getRequestURI()).matches()) {
				ExternalServiceExceptionWrapper exceptionWrapper = new ExternalServiceExceptionWrapper(ex.getMessage());
				exceptionWrapper.setObjectClassName(ex.getClass().getName());
				exceptionWrapper.setObjectJsonString(getJacksonHandler().toJson(ex));
				ex = exceptionWrapper;
			}
		}

		return super.resolveException(request, response, handler, ex);
	}


	/**
	 * Because we need the handler in order to properly log our exceptions, we do the actual logging in above method for
	 * resolveException.  To prevent duplicates, we override logException method from spring and have it just do nothing
	 * as the base implementation will always log the exception (even our ValidationExceptions) as an additional WARN
	 */
	@Override
	protected void logException(Exception ex, HttpServletRequest request) {
		// DO NOTHING
	}


	@Override
	protected String determineViewName(@SuppressWarnings("unused") Exception ex, HttpServletRequest request) {
		String requestURI = request.getRequestURI();
		Map<Pattern, String> errorViewNameMap = getUrlPatternToErrorViewNameMap();
		for (Map.Entry<Pattern, String> viewNameEntry : errorViewNameMap.entrySet()) {
			if (viewNameEntry.getKey().matcher(requestURI).matches()) {
				return getUrlPatternToErrorViewNameMap().get(viewNameEntry.getKey());
			}
		}
		return super.determineViewName(ex, request);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public JacksonHandlerImpl getJacksonHandler() {
		return this.jacksonHandler;
	}


	public void setJacksonHandler(JacksonHandlerImpl jacksonHandler) {
		this.jacksonHandler = jacksonHandler;
	}


	@ValueIgnoringGetter
	public Map<Pattern, String> getUrlPatternToErrorViewNameMap() {
		return this.urlPatternToErrorViewNameMap != null ? this.urlPatternToErrorViewNameMap : new LinkedHashMap<>();
	}


	public void setUrlPatternToErrorViewNameMap(Map<Pattern, String> urlPatternToErrorViewNameMap) {
		this.urlPatternToErrorViewNameMap = urlPatternToErrorViewNameMap;
	}


	public Converter<Exception, Exception> getDataAccessExceptionConverter() {
		return this.dataAccessExceptionConverter;
	}


	public void setDataAccessExceptionConverter(Converter<Exception, Exception> dataAccessExceptionConverter) {
		this.dataAccessExceptionConverter = dataAccessExceptionConverter;
	}
}
