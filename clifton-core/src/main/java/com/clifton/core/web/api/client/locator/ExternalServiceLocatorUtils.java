package com.clifton.core.web.api.client.locator;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;


public class ExternalServiceLocatorUtils {

	private static final Collection<String> EXTERNAL_SERVICE_URLS = new LinkedHashSet<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ExternalServiceLocatorUtils() {
		// Private constructor
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns a List of all end point URL's for external applications. This method can be used to obtain a full list
	 * of external applications in order to establish corresponding OAuth connections.
	 */
	public static Collection<String> getExternalServiceURLs() {
		return Collections.unmodifiableCollection(EXTERNAL_SERVICE_URLS);
	}


	/**
	 * Registers the specified URL as the URL available for an external services end point.
	 * The system may integrate with multiple external systems and each system will have its own URL.
	 *
	 * @return true if new URL was registered and false if the URL was already registered before
	 */
	public static synchronized boolean registerExternalServiceURL(String externalServiceURL) {
		if (EXTERNAL_SERVICE_URLS.contains(externalServiceURL)) {
			return false;
		}
		EXTERNAL_SERVICE_URLS.add(externalServiceURL);
		return true;
	}
}
