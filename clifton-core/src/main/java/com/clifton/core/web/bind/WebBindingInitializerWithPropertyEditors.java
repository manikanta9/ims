package com.clifton.core.web.bind;

import com.clifton.core.beans.propertyeditors.ByteArrayPropertyEditor;
import com.clifton.core.beans.propertyeditors.DatePropertyEditor;
import com.clifton.core.beans.propertyeditors.EnumPropertyEditor;
import com.clifton.core.beans.propertyeditors.IntArrayPropertyEditor;
import com.clifton.core.beans.propertyeditors.IntegerArrayPropertyEditor;
import com.clifton.core.beans.propertyeditors.LocalDatePropertyEditor;
import com.clifton.core.beans.propertyeditors.LongArrayPropertyEditor;
import com.clifton.core.beans.propertyeditors.ShortArrayPropertyEditor;
import com.clifton.core.beans.propertyeditors.TimePropertyEditor;
import com.clifton.core.util.date.Time;
import org.springframework.beans.propertyeditors.CustomNumberEditor;
import org.springframework.beans.propertyeditors.StringArrayPropertyEditor;
import org.springframework.beans.propertyeditors.StringTrimmerEditor;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.ConfigurableWebBindingInitializer;
import org.springframework.web.context.request.WebRequest;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * The <code>WebBindingInitializerWithValidation</code> class extends {@link ConfigurableWebBindingInitializer} and
 * registers custom property editors that follow our formats and conventions.
 * <p>
 * It also registers all custom property editors: {@link StringTrimmerEditor}
 *
 * @author vgomelsky
 */
public class WebBindingInitializerWithPropertyEditors extends ConfigurableWebBindingInitializer {

	@Override
	public void initBinder(WebDataBinder binder, WebRequest request) {
		// execute regular Spring binding initialization
		super.initBinder(binder, request);

		// register custom property editors (MUST BE RIGHT AFTER CALLING SUPER TO OVERRIDE DEFAULT BEHAVIOR)
		if (!"true".equals(request.getParameter("doNotTrimStrings"))) {
			binder.registerCustomEditor(String.class, new StringTrimmerEditor(true));
		}

		if (getPropertyEditorMap() != null) {
			for (Map.Entry<Class<?>, PropertyEditorSupport> clazzPropertyEntry : getPropertyEditorMap().entrySet()) {
				binder.registerCustomEditor(clazzPropertyEntry.getKey(), clazzPropertyEntry.getValue());
			}
		}
	}


	/**
	 * Need to create a new instance of each property editor every time because they're not thread safe.
	 */
	protected Map<Class<?>, PropertyEditorSupport> getPropertyEditorMap() {
		Map<Class<?>, PropertyEditorSupport> propertyEditorMap = new HashMap<>();
		propertyEditorMap.put(Date.class, new DatePropertyEditor());
		propertyEditorMap.put(LocalDate.class, new LocalDatePropertyEditor());
		propertyEditorMap.put(Time.class, new TimePropertyEditor());
		propertyEditorMap.put(byte[].class, new ByteArrayPropertyEditor());
		propertyEditorMap.put(int[].class, new IntArrayPropertyEditor());
		propertyEditorMap.put(Integer[].class, new IntegerArrayPropertyEditor());
		propertyEditorMap.put(Short[].class, new ShortArrayPropertyEditor());
		propertyEditorMap.put(Long[].class, new LongArrayPropertyEditor());
		propertyEditorMap.put(Enum.class, new EnumPropertyEditor());
		propertyEditorMap.put(String[].class, new StringArrayPropertyEditor(",", true, true));
		NumberFormat numberFormat = NumberFormat.getInstance(); // support inputs with commas
		propertyEditorMap.put(int.class, new CustomNumberEditor(Integer.class, numberFormat, true));
		propertyEditorMap.put(Integer.class, new CustomNumberEditor(Integer.class, numberFormat, true));
		propertyEditorMap.put(long.class, new CustomNumberEditor(Long.class, numberFormat, true));
		propertyEditorMap.put(Long.class, new CustomNumberEditor(Long.class, numberFormat, true));
		propertyEditorMap.put(Double.class, new CustomNumberEditor(Double.class, numberFormat, true));
		propertyEditorMap.put(BigDecimal.class, new CustomNumberEditor(BigDecimal.class, numberFormat, true));
		return propertyEditorMap;
	}
}
