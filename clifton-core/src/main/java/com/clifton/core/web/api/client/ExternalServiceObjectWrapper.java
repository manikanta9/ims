package com.clifton.core.web.api.client;

import java.io.Serializable;


/**
 * The class <code>ExternalServiceObjectWrapper</code> is utilized when a method utilizes interfaces
 * for argument or return value parameters;  We wrap any object we are sending, or returning in this
 * object so we can store the json representation of the object in addition to the name of the concrete
 * implementation class.  This allows us to deserialize properly when needed.
 *
 * @author StevenF
 */
public class ExternalServiceObjectWrapper implements Serializable {

	private String objectClassName;
	private String objectJsonString;

	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getObjectClassName() {
		return this.objectClassName;
	}


	public void setObjectClassName(String objectClassName) {
		this.objectClassName = objectClassName;
	}


	public String getObjectJsonString() {
		return this.objectJsonString;
	}


	public void setObjectJsonString(String objectJsonString) {
		this.objectJsonString = objectJsonString;
	}
}
