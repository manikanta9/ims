package com.clifton.core.web.bind;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;

import java.util.List;


/**
 * The standard {@link WebBindingHandler} implementation.
 *
 * @author mikeh
 * @implNote This is registered as a bean within the <i>*-web.xml</i> Spring context of the active application. While this allows bean instances of this type to have access to
 * other resources within the <i>web</i> context, it also means that instances of this bean can only be accessed by other beans within the <i>web</i> context. Attempts to access
 * this bean from other contexts are likely to fail.
 */
public class WebBindingHandlerImpl implements WebBindingHandler {

	/**
	 * One or more retrievers in the order of applicability. The first retriever from the list
	 * that return true for {@link WebBindingDataRetriever#isApplicableForRequest} will be used.
	 * Different retrievers can be implemented for different URL patterns.
	 * The last one is usually the default one that always returns true.
	 */
	@SuppressWarnings("SpringJavaAutowiredMembersInspection") // Bean is defined in *-web.xml contexts
	@Autowired
	private List<WebBindingDataRetriever> webBindingDataRetrieverList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean isValidatingBindingEnabled(Class<?> targetClass, WebRequest request) {
		String enable = request.getParameter(WebBindingHandler.ENABLE_VALIDATING_BINDING_PARAMETER);
		String url = ((ServletWebRequest) request).getRequest().getRequestURI();
		return (url.endsWith(WebBindingHandler.VALIDATE_URL_SUFFIX) || "true".equalsIgnoreCase(enable))
				&& targetClass != null
				&& (AnnotationUtils.isAnnotationPresent(targetClass, NonPersistentObject.class) || IdentityObject.class.isAssignableFrom(targetClass));
	}


	@Override
	public boolean isDisableValidatingBindingValidation(WebRequest request) {
		return "true".equalsIgnoreCase(request.getParameter(WebBindingHandler.DISABLE_VALIDATING_BINDING_VALIDATION_PARAMETER));
	}


	@Override
	public String getBindingClassDtoName(WebRequest request) {
		return request.getParameter(WebBindingHandler.DTO_CLASS_FOR_BINDING_PARAMETER);
	}


	@Override
	public WebBindingDataRetriever getWebBindingDataRetriever(WebRequest request) {
		for (WebBindingDataRetriever retriever : CollectionUtils.getIterable(getWebBindingDataRetrieverList())) {
			if (retriever.isApplicableForRequest(request)) {
				return retriever;
			}
		}
		throw new RuntimeException("Cannot find WebBindingDataRetriever for " + request.getDescription(true));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<WebBindingDataRetriever> getWebBindingDataRetrieverList() {
		return this.webBindingDataRetrieverList;
	}


	public void setWebBindingDataRetrieverList(List<WebBindingDataRetriever> webBindingDataRetrieverList) {
		this.webBindingDataRetrieverList = webBindingDataRetrieverList;
	}
}
