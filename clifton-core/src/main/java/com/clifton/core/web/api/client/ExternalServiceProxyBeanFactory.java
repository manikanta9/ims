package com.clifton.core.web.api.client;

import org.aopalliance.intercept.MethodInterceptor;
import org.springframework.aop.framework.ProxyFactory;


/**
 * The <code>ExternalServiceProxyBeanFactory</code> is used to create the proxies for services scanned and passed
 * by the <code>ExternalServiceProxyFactory</code>
 *
 * @author StevenF
 */
public class ExternalServiceProxyBeanFactory {

	@SuppressWarnings("unchecked")
	private <WS> WS createExternalServiceProxyBean(ClassLoader classLoader, Class<WS> serviceInterface, MethodInterceptor methodInterceptor) {
		ProxyFactory proxyFactory = new ProxyFactory(serviceInterface, methodInterceptor);
		return (WS) proxyFactory.getProxy(classLoader);
	}
}
