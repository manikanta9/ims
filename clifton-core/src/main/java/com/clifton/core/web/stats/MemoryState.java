package com.clifton.core.web.stats;


/**
 * The <code>MemoryState</code> class represents JFM memory state.
 *
 * @author vgomelsky
 */
public class MemoryState {

	private long freeMemory;
	private long totalMemory;
	private long maxMemory;


	public long getFreeMemory() {
		return this.freeMemory;
	}


	public void setFreeMemory(long freeMemory) {
		this.freeMemory = freeMemory;
	}


	public long getTotalMemory() {
		return this.totalMemory;
	}


	public void setTotalMemory(long totalMemory) {
		this.totalMemory = totalMemory;
	}


	public long getMaxMemory() {
		return this.maxMemory;
	}


	public void setMaxMemory(long maxMemory) {
		this.maxMemory = maxMemory;
	}
}
