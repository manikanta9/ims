package com.clifton.core.web.stats;

import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.jmx.CoreJmxUtils;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.jmx.JmxUtils;
import com.clifton.core.util.jmx.MBeanWrapper;
import com.clifton.core.util.runner.ExecutorServiceStatus;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.view.JsonView;
import org.springframework.stereotype.Service;

import javax.management.ObjectName;
import java.lang.reflect.Method;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;


/**
 * The standard implementation for {@link RequestProcessorManagementService}.
 *
 * @author MikeH
 */
@Service
@ExcludeFromComponentScan
public class RequestProcessorManagementServiceImpl implements RequestProcessorManagementService {

	// Tomcat-specific ObjectName parameters
	private static final String CATALINA_MBEAN_DOMAIN = "Catalina"; // External Tomcat
	private static final String TOMCAT_MBEAN_DOMAIN = "Tomcat"; // Embedded Tomcat
	private static final String TOMCAT_MBEAN_REQUEST_PROCESSOR_TYPE = "RequestProcessor";
	private static final String TOMCAT_MBEAN_REQUEST_PROCESSOR_ENDPOINT_TYPE = "ThreadPool";

	/**
	 * The injected view object. This object is used for statistics and for mutable properties.
	 */
	private JsonView jsonView;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public RequestProcessorStatus getRequestProcessorStatus() {
		int responseBufferSize = getJsonView().getResponseBufferSize();
		long responseLimitSize = getJsonView().getResponseLimitSize();
		Collection<ObjectName> requestProcessorEndpointObjectNameList = getRequestProcessorEndpointObjectNameList();
		Collection<MBeanWrapper> requestProcessorEndpointList = CollectionUtils.getConverted(requestProcessorEndpointObjectNameList, JmxUtils::getMBeanWrapperNormalized);
		List<ExecutorServiceStatus> executorServiceStatusList = requestProcessorEndpointObjectNameList.stream()
				.map(this::getRequestProcessorExecutorServiceStatus)
				.filter(Objects::nonNull)
				.collect(Collectors.toList());
		Collection<MBeanWrapper> requestProcessorList = getRequestProcessorList();
		return new RequestProcessorStatus(responseBufferSize, responseLimitSize, requestProcessorEndpointList, executorServiceStatusList, requestProcessorList);
	}


	@Override
	public RequestProcessorStatus saveRequestProcessorParameters(Integer responseBufferSize, Long responseLimitSize) {
		if (responseBufferSize != null) {
			ValidationUtils.assertTrue(responseBufferSize >= JsonView.MINIMUM_RESPONSE_BUFFER_SIZE, () -> String.format("The response buffer size [%d] must be greater than or equal to [%d].", responseBufferSize, JsonView.MINIMUM_RESPONSE_BUFFER_SIZE));
			getJsonView().setResponseBufferSize(responseBufferSize);
		}
		if (responseLimitSize != null) {
			ValidationUtils.assertTrue(responseLimitSize >= JsonView.MINIMUM_RESPONSE_LIMIT_SIZE, () -> String.format("The response limit size [%d] must be greater than or equal to [%d].", responseLimitSize, JsonView.MINIMUM_RESPONSE_LIMIT_SIZE));
			getJsonView().setResponseLimitSize(responseLimitSize);
		}
		return getRequestProcessorStatus();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Collection<MBeanWrapper> getRequestProcessorList() {
		Collection<ObjectName> mBeanNameList = JmxUtils.queryMBeans(CATALINA_MBEAN_DOMAIN, Collections.singletonMap("type", TOMCAT_MBEAN_REQUEST_PROCESSOR_TYPE));
		if (mBeanNameList.isEmpty()) {
			mBeanNameList = JmxUtils.queryMBeans(TOMCAT_MBEAN_DOMAIN, Collections.singletonMap("type", TOMCAT_MBEAN_REQUEST_PROCESSOR_TYPE));
		}
		return CollectionUtils.getConverted(mBeanNameList, JmxUtils::getMBeanWrapperNormalized);
	}


	private Collection<ObjectName> getRequestProcessorEndpointObjectNameList() {
		HashMap<String, String> keyValueQueryMap = new HashMap<>();
		keyValueQueryMap.put("type", TOMCAT_MBEAN_REQUEST_PROCESSOR_ENDPOINT_TYPE);
		keyValueQueryMap.put("name", "*");
		Collection<ObjectName> objectNameList = JmxUtils.queryMBeansStrict(CATALINA_MBEAN_DOMAIN, keyValueQueryMap);
		if (objectNameList.isEmpty()) {
			objectNameList = JmxUtils.queryMBeansStrict(TOMCAT_MBEAN_DOMAIN, keyValueQueryMap);
		}
		return objectNameList;
	}


	private ExecutorServiceStatus getRequestProcessorExecutorServiceStatus(ObjectName endpointObjectName) {
		try {
			// Get endpoint name
			Object endpoint = CoreJmxUtils.getMBeanResource(endpointObjectName);
			Class<?> abstractEndpointClass = CoreClassUtils.getClass("org.apache.tomcat.util.net.AbstractEndpoint");
			if (!abstractEndpointClass.isAssignableFrom(endpoint.getClass())) {
				// Executor service extraction from resource is hard-coded; ignore unmatched incompatible resource types
				LogUtils.warn(LogCommand.ofMessage(RequestProcessorManagementServiceImpl.class, String.format("An incompatible endpoint MBean for name [%s] was found. Class [%s] is not a recognized type.", endpointObjectName, endpoint.getClass().getName())));
				return null;
			}
			Method getNameMethod = MethodUtils.getMethod(endpoint.getClass(), "getName");
			String name = (String) MethodUtils.invoke(getNameMethod, endpoint);

			// Get executor service status
			Method getExecutorMethod = MethodUtils.getMethod(endpoint.getClass(), "getExecutor");
			ExecutorService executor = (ExecutorService) MethodUtils.invoke(getExecutorMethod, endpoint);
			return new ExecutorServiceStatus(name, executor);
		}
		catch (Exception e) {
			// Fail gracefully in the event that the current classpath types are not supported; this is common when changing versions of Tomcat, for example
			LogUtils.warn(RequestProcessorManagementServiceImpl.class, String.format("Executor service status for object name [%s] could not be obtained.", endpointObjectName), e);
			return null;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonView getJsonView() {
		return this.jsonView;
	}


	public void setJsonView(JsonView jsonView) {
		this.jsonView = jsonView;
	}
}
