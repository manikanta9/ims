package com.clifton.core.web.api.client;

import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.util.AssertUtils;
import org.apache.http.entity.ContentType;
import org.springframework.core.MethodParameter;
import org.springframework.http.server.ServletServerHttpResponse;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodReturnValueHandlerComposite;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.nio.charset.StandardCharsets;


/**
 * The <code>ExternalServiceReturnValueHandler</code> is used to avoid the need to annotate method return values or classes.
 * By default Spring will only use the 'RequestResponseBodyMethodProcessor' when methods or classes are annotated with @RequestBody
 * We now use <code>DelegatingHandlerAdapter</code> to route requests based on the request URI path.  Requests routed through
 * /api/ are sent to the custom argument resolver allows us to send application/json raw data in a post request and have it processed
 * as if the methods were annotated with @RequestBody - after processing this custom return value handler allows the return value
 * to be processed as application/json without having to annotate the method and/or class with @ResponseBody
 *
 * @author StevenF
 */
public class ExternalServiceReturnValueHandler extends HandlerMethodReturnValueHandlerComposite {

	private JacksonHandlerImpl jacksonHandler;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean supportsReturnType(MethodParameter returnType) {
		return true;
	}


	@Override
	public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
		AssertUtils.assertNotNull(returnType.getMethod(), "Return type method is null");
		if (!Void.TYPE.isAssignableFrom(returnType.getMethod().getReturnType()) && returnValue != null) {
			HttpServletResponse nativeResponse = webRequest.getNativeResponse(HttpServletResponse.class);
			ServletServerHttpResponse servletResponse = new ServletServerHttpResponse(AssertUtils.assertNotNull(nativeResponse, "Native response is null"));
			HttpServletResponse response = servletResponse.getServletResponse();
			if (returnValue instanceof FileWrapper) {
				FileWrapper fileWrapper = (FileWrapper) returnValue;
				FileContainer file = fileWrapper.getFile().toFileContainer();
				if (!file.exists()) {
					// throw exception BEFORE modifying response so that spring will handle the LoggingExceptionResolver result correctly
					throw new FileNotFoundException(file.getName() + " does not exist.");
				}
				response.setContentType(FileUtils.getMimeContentType(fileWrapper.getFileName()));
				response.setContentLength((int) file.length());
				response.setHeader("Content-Disposition", "attachment; filename=\"" + FileUtils.replaceInvalidCharacters(fileWrapper.getFileName(), "") + "\"");
				FileUtils.transferFromInputToOutput(file.getInputStream(), response.getOutputStream());
			}
			else {
				/*
				 * If the returnType is an interface we wrap the return so that we can include the concrete implementation
				 * className in the response; this allows deserialization to the proper concrete type and prevents us from
				 * losing data due to type erasure.
				 * e.g., returning 'PortalFileCategory' when the method signature returns 'IdentityObject'
				 */
				if (returnType.getMethod().getReturnType().isInterface()) {
					ExternalServiceObjectWrapper objectWrapper = new ExternalServiceObjectWrapper();
					objectWrapper.setObjectClassName(returnValue.getClass().getName());
					objectWrapper.setObjectJsonString(getJacksonHandler().toJson(returnValue));
					returnValue = objectWrapper;
				}
				byte[] jsonBytes = getJacksonHandler().toJson(returnValue).getBytes(StandardCharsets.UTF_8);
				response.setContentType(ContentType.APPLICATION_JSON.toString());
				response.setContentLength(jsonBytes.length);
				FileUtils.transferFromInputToOutput(new ByteArrayInputStream(jsonBytes), response.getOutputStream());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public JacksonHandlerImpl getJacksonHandler() {
		return this.jacksonHandler;
	}


	public void setJacksonHandler(JacksonHandlerImpl jacksonHandler) {
		this.jacksonHandler = jacksonHandler;
	}
}
