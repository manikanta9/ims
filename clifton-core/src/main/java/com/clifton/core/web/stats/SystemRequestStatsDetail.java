package com.clifton.core.web.stats;


import java.util.Date;


/**
 * The <code>SystemRequestStatsDetail</code> class represents details about a single request.
 *
 * @author vgomelsky
 */
public class SystemRequestStatsDetail {

	private final long logTimeMillis;

	private final long durationNano;
	private final long dbDurationNano;
	private final int dbExecuteCount;

	private final String parameters;
	private final boolean firstHit;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemRequestStatsDetail() {
		// Empty Constructor - Needed for binding (i.e. Portal Admin getting list from Portal)
		this(0, 0, 0, null, false);
	}


	public SystemRequestStatsDetail(long duration, int dbHits, long dbDuration, String parameters, boolean firstHit) {
		this.logTimeMillis = System.currentTimeMillis();
		this.durationNano = duration;
		this.dbDurationNano = dbDuration;
		this.dbExecuteCount = dbHits;
		this.parameters = parameters;
		this.firstHit = firstHit;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private double convertNanoToSeconds(long nano) {
		return ((double) nano / 1_000_000_000);
	}


	public Date getLogTime() {
		return new Date(this.logTimeMillis);
	}


	public long getLogTimeMillis() {
		return this.logTimeMillis;
	}


	public long getDurationNano() {
		return this.durationNano;
	}


	public double getDurationSeconds() {
		return convertNanoToSeconds(this.durationNano);
	}


	public double getDbDurationSeconds() {
		return convertNanoToSeconds(getDbDurationNano());
	}


	public int getDbExecuteCount() {
		return this.dbExecuteCount;
	}


	public String getParameters() {
		return this.parameters;
	}


	public boolean isFirstHit() {
		return this.firstHit;
	}


	public long getDbDurationNano() {
		return this.dbDurationNano;
	}
}
