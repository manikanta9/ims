package com.clifton.core.web.mvc;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.core.annotation.AnnotatedElementUtils;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.condition.RequestCondition;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.AnnotatedElement;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * The ConventionBasedRequestMappingHandlerMapping class registers additional URL's based on our naming convention.
 * It will register every method annotated with @RequestMapping as well as every public method defined on our service
 * interfaces for services annotated with @Service that are packaged in our ("com.clifton.") package and do not
 * have arguments that are interfaces (cannot bind to an interface).
 *
 * @author vgomelsky
 */
public class ConventionBasedRequestMappingHandlerMapping extends RequestMappingHandlerMapping {


	/**
	 * Indicates that all methods should be exposed using our standard url convention in addition to
	 * adding the mapping that will be prefixed with /api/ which will allow them to be accessed via an
	 * internal oAuth connection and from the local server.
	 */
	private boolean exposeAPI = false;

	/**
	 * List of additional interface class names that should be treated as handlers
	 * and have API only request mappings created for them.
	 */
	private List<String> exposeInterfaceClassNameList;


	private List<String> exposeInterfaceBeanNameList;

	/**
	 * Terrible name that is used to specify that only classes annotated with @Controller
	 * should be exposed using our standard <requestMapping>.json convention.  All other eligible
	 * methods should follow the same convention, but will be prefixed with /api/ which will only
	 * allow them to be accessed via an internal oAuth connection.  The urls will not be accessible
	 * or functional externally.
	 */
	private boolean exposeControllersOnly = false;
	private String classNamePrefix = "com.clifton.";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected boolean isHandler(Class<?> beanType) {
		if (super.isHandler(beanType)) {
			return true;
		}

		// our beans annotated with @Service should be exposed externally
		if (AnnotationUtils.isAnnotationPresent(beanType, Service.class)) {
			if (beanType.getName().startsWith(getClassNamePrefix())) {
				return true;
			}
		}

		return false;
	}


	@Override
	protected void processCandidateBean(String beanName) {
		Class<?> beanType = null;
		try {
			beanType = obtainApplicationContext().getType(beanName);
		}
		catch (Throwable ex) {
			// An unresolvable bean type, probably from a lazy bean - let's ignore it.
			if (logger.isTraceEnabled()) {
				logger.trace("Could not resolve type for bean '" + beanName + "'", ex);
			}
		}
		if (beanType != null && (isHandler(beanType) || isExposedInterfaceBean(beanName))) {
			detectHandlerMethods(beanName);
		}
	}


	@Override
	protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
		RequestMappingInfo info = createRequestMappingInfoForMethod(method, handlerType);
		if (info != null) {
			RequestMappingInfo typeInfo = copyOfCreateRequestMappingInfo(handlerType);
			if (typeInfo != null) {
				info = typeInfo.combine(info);
			}
		}
		return info;
	}


	/**
	 * Use default Spring MVC behaviour for methods annotated with @RequestMapping.
	 * Otherwise, rely on our naming convention to match and create URL for methods that should be externally exposed.
	 */
	private RequestMappingInfo createRequestMappingInfoForMethod(Method method, Class<?> handlerType) {
		RequestMappingInfo info = copyOfCreateRequestMappingInfo(method);

		if (info == null && ContextConventionUtils.isUrlFromMethodAllowed(method, handlerType)) {
			String url = ContextConventionUtils.getUrlFromMethod(method);
			if (url != null) {
				//Expose both request mapping and /json request mapping for json post support.
				String[] paths = new String[]{url, ContextConventionUtils.JSON_ROOT + url};
				if ((isExposeControllersOnly() && !AnnotationUtils.isAnnotationPresent(method.getDeclaringClass(), Controller.class))
						|| (AnnotationUtils.isTrue(method, DoNotAddRequestMapping.class, DoNotAddRequestMapping::allowAPI))) {
					//Expose only api mappings.
					paths = new String[]{ContextConventionUtils.API_ROOT + url};
				}
				else if (isExposeAPI()) {
					//Expose only standard url mapping, json mapping, and api mappings.
					paths = ArrayUtils.addAll(paths, new String[]{ContextConventionUtils.API_ROOT + url});
				}
				info = RequestMappingInfo
						.paths(resolveEmbeddedValuesInPatterns(paths))
						.options((RequestMappingInfo.BuilderConfiguration) getPrivateFieldValue("config"))
						.build();
			}
		}
		return info;
	}


	//TODO - clean up this method/make more reusable.
	// had to copy/paste Spring's implementation because it's a private method
	//This only manipulates the result of explicit request mappings to expose the api mappings.
	private RequestMappingInfo copyOfCreateRequestMappingInfo(AnnotatedElement element) {
		boolean exposedInterfaceClass = false;

		//If the element is a method; check if it is declared as an explicitly exposed interface
		//If it is, then we only want to create the /api/ request mapping for it so it is only accessible through oAuth
		if (element instanceof Method) {
			exposedInterfaceClass = isExposedInterfaceClass(((Method) element).getDeclaringClass());
		}

		RequestMappingInfo requestMappingInfo = null;
		RequestMapping requestMapping = AnnotatedElementUtils.findMergedAnnotation(element, RequestMapping.class);
		RequestCondition<?> condition = (element instanceof Class<?> ?
				getCustomTypeCondition((Class<?>) element) : getCustomMethodCondition((Method) element));
		if (requestMapping != null) {
			requestMappingInfo = createRequestMappingInfo(requestMapping, condition);

			//Create array list of existing paths that will be extended
			List<String> requestMappingPaths = new ArrayList<>();
			if (!exposedInterfaceClass) {
				requestMappingPaths.addAll(Arrays.asList(requestMapping.path()));

				//Create jsonPaths for existing paths
				String[] jsonPaths = requestMapping.path();
				for (int i = 0; i < jsonPaths.length; i++) {
					String prefix = "";
					if (!jsonPaths[i].startsWith("/")) {
						prefix = "/";
					}
					if (!jsonPaths[i].endsWith(ContextConventionUtils.URL_DEFAULT_EXTENSION)) {
						jsonPaths[i] = jsonPaths[i] + ContextConventionUtils.URL_DEFAULT_EXTENSION;
					}
					jsonPaths[i] = ContextConventionUtils.JSON_ROOT + prefix + jsonPaths[i];
				}
				requestMappingPaths.addAll(Arrays.asList(jsonPaths));
			}

			boolean isController = false;
			if (isExposeControllersOnly() || (AnnotationUtils.isTrue(element, DoNotAddRequestMapping.class, DoNotAddRequestMapping::allowAPI))) {
				if (element instanceof Class<?>) {
					isController = AnnotationUtils.isAnnotationPresent(element, Controller.class);
				}
				if (element instanceof Method) {
					isController = AnnotationUtils.isAnnotationPresent(((Method) element).getDeclaringClass(), Controller.class);
				}
				if (!isController) {
					//Since we only want to expose the api path, we overwrite with new paths.
					String[] controllerPaths = requestMapping.path();
					for (int i = 0; i < controllerPaths.length; i++) {
						String prefix = "";
						if (!controllerPaths[i].startsWith("/")) {
							prefix = "/";
						}
						controllerPaths[i] = ContextConventionUtils.API_ROOT + prefix + controllerPaths[i];
					}
					requestMappingInfo = RequestMappingInfo
							.paths(resolveEmbeddedValuesInPatterns(controllerPaths))
							.methods(requestMapping.method())
							.params(requestMapping.params())
							.headers(requestMapping.headers())
							.consumes(requestMapping.consumes())
							.produces(requestMapping.produces())
							.mappingName(requestMapping.name())
							.options((RequestMappingInfo.BuilderConfiguration) getPrivateFieldValue("config"))
							.build();
				}
			}
			else if (isExposeAPI()) {
				//Since we want to expose original paths, json paths, and api paths, we append to the list.
				String[] apiPaths = requestMapping.path();
				for (int i = 0; i < apiPaths.length; i++) {
					String prefix = "";
					if (!apiPaths[i].startsWith("/")) {
						prefix = "/";
					}
					if (!apiPaths[i].endsWith(ContextConventionUtils.URL_DEFAULT_EXTENSION)) {
						apiPaths[i] = apiPaths[i] + ContextConventionUtils.URL_DEFAULT_EXTENSION;
					}
					apiPaths[i] = ContextConventionUtils.API_ROOT + prefix + apiPaths[i];
				}
				requestMappingPaths.addAll(Arrays.asList(apiPaths));
				requestMappingInfo = RequestMappingInfo
						.paths(resolveEmbeddedValuesInPatterns(requestMappingPaths.toArray(new String[requestMappingPaths.size()])))
						.methods(requestMapping.method())
						.params(requestMapping.params())
						.headers(requestMapping.headers())
						.consumes(requestMapping.consumes())
						.produces(requestMapping.produces())
						.mappingName(requestMapping.name())
						.options((RequestMappingInfo.BuilderConfiguration) getPrivateFieldValue("config"))
						.build();
			}
		}
		return requestMappingInfo;
	}


	private boolean isExposedInterfaceClass(Class<?> clazz) {
		for (Class<?> interfaceClass : clazz.getInterfaces()) {
			if (CollectionUtils.contains(getExposeInterfaceClassNameList(), interfaceClass.getName())) {
				return true;
			}
		}
		return false;
	}


	private boolean isExposedInterfaceBean(String beanName) {
		return CollectionUtils.contains(getExposeInterfaceBeanNameList(), beanName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private Object getPrivateFieldValue(String fieldName) {
		try {
			Field field = RequestMappingHandlerMapping.class.getDeclaredField(fieldName);
			field.setAccessible(true);
			return field.get(this);
		}
		catch (Exception e) {
			throw new RuntimeException("Error reading private field '" + fieldName + "' on RequestMappingHandlerMapping", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isExposeAPI() {
		return this.exposeAPI;
	}


	public void setExposeAPI(boolean exposeAPI) {
		this.exposeAPI = exposeAPI;
	}


	public List<String> getExposeInterfaceClassNameList() {
		return this.exposeInterfaceClassNameList;
	}


	public void setExposeInterfaceClassNameList(List<String> exposeInterfaceClassNameList) {
		this.exposeInterfaceClassNameList = exposeInterfaceClassNameList;
	}


	public List<String> getExposeInterfaceBeanNameList() {
		return this.exposeInterfaceBeanNameList;
	}


	public void setExposeInterfaceBeanNameList(List<String> exposeInterfaceBeanNameList) {
		this.exposeInterfaceBeanNameList = exposeInterfaceBeanNameList;
	}


	public boolean isExposeControllersOnly() {
		return this.exposeControllersOnly;
	}


	public void setExposeControllersOnly(boolean exposeControllersOnly) {
		this.exposeControllersOnly = exposeControllersOnly;
	}


	public String getClassNamePrefix() {
		return this.classNamePrefix;
	}


	public void setClassNamePrefix(String classNamePrefix) {
		this.classNamePrefix = classNamePrefix;
	}
}
