package com.clifton.core.web.view;

import java.io.IOException;


/**
 * The {@link SizeLimitedAppender} type provides a facade to pipe content to a backing {@link Appendable} while monitoring the length of the piped content. Once the configured
 * {@link #maxSize} is reached, an exception will be thrown.
 *
 * @author MikeH
 */
public class SizeLimitedAppender implements Appendable {

	/**
	 * The backing {@link Appendable}. All content will be piped directly to this {@link Appendable} after validating content length checks.
	 */
	private final Appendable out;
	/**
	 * The maximum allowed content length before an exception is thrown.
	 */
	private final long maxSize;
	/**
	 * The current written length.
	 */
	private int writtenLen;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SizeLimitedAppender(Appendable out, long maxSize) {
		this.out = out;
		this.maxSize = maxSize;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SizeLimitedAppender append(CharSequence csq) throws IOException {
		// Use common null representation over backing representation for consistency
		String content = csq == null ? "null" : csq.toString();
		checkLength(content.length());
		getOut().append(content);
		return this;
	}


	@Override
	public SizeLimitedAppender append(CharSequence csq, int start, int end) throws IOException {
		// Use common null representation over backing representation for consistency
		String content = csq == null ? "null" : csq.toString();
		checkLength(end - start);
		getOut().append(content, start, end);
		return this;
	}


	@Override
	public SizeLimitedAppender append(char c) throws IOException {
		checkLength(1);
		getOut().append(c);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void checkLength(int added) {
		this.writtenLen += added;
		if (getWrittenLen() > getMaxSize()) {
			throw new RuntimeException(String.format("The current attempted written length [%d] exceeds the maximum allowed written length [%d].", getWrittenLen(), getMaxSize()));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("unused")
	private void setWrittenLen(int writtenLen) {
		// Private to avoid visible accessor
		this.writtenLen = writtenLen;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Appendable getOut() {
		return this.out;
	}


	public long getMaxSize() {
		return this.maxSize;
	}


	public int getWrittenLen() {
		return this.writtenLen;
	}
}
