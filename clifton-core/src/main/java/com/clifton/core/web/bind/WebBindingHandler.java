package com.clifton.core.web.bind;

import org.springframework.web.context.request.WebRequest;


/**
 * The {@link WebBindingHandler} declares methods for working with web binding. In particular, this interface declares methods used in web binding validation.
 *
 * @author mikeh
 * @see WebBindingInitializerWithValidation
 * @see ValidatingServletRequestDataBinder
 */
public interface WebBindingHandler {

	/**
	 * For cases where the method parameter is a base class and we want to bind to a specific class pass this parameter
	 */
	public static final String DTO_CLASS_FOR_BINDING_PARAMETER = "dtoClassForBinding";
	/**
	 * If the URL ends on this suffix, perform full binding and validation.
	 */
	public static final String VALIDATE_URL_SUFFIX = "Save.json";
	/**
	 * If the specified request parameter is set present and equals "true", then enable validating binding regardless of URL suffix.
	 */
	public static final String ENABLE_VALIDATING_BINDING_PARAMETER = "enableValidatingBinding";
	/**
	 * When validating binding is enabled, allows optionally disable validation while still performing bindings.
	 * Set to "true" to disable validation for a specific request.
	 */
	public static final String DISABLE_VALIDATING_BINDING_VALIDATION_PARAMETER = "disableValidatingBindingValidation";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns true if full binding (get full FK field objects before overriding values from request) and validation should be performed.
	 */
	public boolean isValidatingBindingEnabled(Class<?> targetClass, WebRequest request);


	/**
	 * Returns <code>true</code> if validation should be disabled while performing bindings.
	 *
	 * @see #DISABLE_VALIDATING_BINDING_VALIDATION_PARAMETER
	 */
	public boolean isDisableValidatingBindingValidation(WebRequest request);


	/**
	 * Gets the specific DTO implementation to use during binding. This is particularly relevant when the target type is an abstract or base class and a specific type is desired
	 * for binding.
	 *
	 * @see #DTO_CLASS_FOR_BINDING_PARAMETER
	 */
	public String getBindingClassDtoName(WebRequest request);


	/**
	 * Gets the {@link WebBindingDataRetriever} to use for the given <tt>request</tt>.
	 *
	 * @throws Exception when no matching data retriever is found
	 */
	public WebBindingDataRetriever getWebBindingDataRetriever(WebRequest request);
}
