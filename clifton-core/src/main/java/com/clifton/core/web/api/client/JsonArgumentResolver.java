package com.clifton.core.web.api.client;

import com.clifton.core.converter.json.jackson.JacksonHandlerImpl;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.JavaType;
import org.apache.commons.io.IOUtils;
import org.springframework.core.MethodParameter;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.HandlerMethodArgumentResolverComposite;
import org.springframework.web.method.support.ModelAndViewContainer;

import javax.servlet.http.HttpServletRequest;
import java.io.StringWriter;
import java.nio.charset.StandardCharsets;


/**
 * The <code>JsonArgumentResolver</code>
 * By default Spring will only use the 'RequestResponseBodyMethodProcessor' when parameters are annotated with @RequestBody
 * We now use <code>DelegatingHandlerAdapter</code> to route requests based on the request URI path.  Requests routed through
 * /json/ are sent to this custom argument resolver which allows us to send application/json raw data in a post request and have it processed
 * as if the methods were annotated with @RequestBody - additionally this supports posting json to methods with multiple arguments
 * This is not possible if using the default @RequestBody functionality.
 *
 * @author StevenF
 */
public class JsonArgumentResolver extends HandlerMethodArgumentResolverComposite {

	private static final String JSON_BODY = "JsonBody";
	private static final String JSON_PARAMETER_TYPE_CLASS_NAMES = "jsonParameterTypeClassNames";

	private JacksonHandlerImpl jacksonHandler;
	public JsonFactory factory = new JsonFactory();

	////////////////////////////////////////////////////////////////////////////


	//If a request was sent here by <code>DelegatingHandlerAdapter</code> then we want to process
	@Override
	public boolean supportsParameter(MethodParameter methodParameter) {
		return true;
	}


	@Override
	public Object resolveArgument(MethodParameter methodParameter, ModelAndViewContainer modelAndViewContainer, NativeWebRequest nativeWebRequest, WebDataBinderFactory webDataBinderFactory) throws Exception {
		Object nativeRequest = nativeWebRequest.getNativeRequest();
		if (nativeRequest instanceof HttpServletRequest) {
			HttpServletRequest httpServletRequest = (HttpServletRequest) nativeRequest;
			String jsonArgument = getJsonArgument(methodParameter.getParameterName(), getJsonBody(httpServletRequest));
			return deserializeJsonArgument(httpServletRequest, methodParameter, jsonArgument);
		}
		throw new RuntimeException("Unsupported request: " + nativeRequest);
	}


	private Object deserializeJsonArgument(HttpServletRequest httpServletRequest, MethodParameter methodParameter, String jsonArgument) throws Exception {
		String parameterName = methodParameter.getParameterName();
		ValidationUtils.assertNotEmpty(jsonArgument, "Failed to retrieve parameter [" + parameterName + "] for method [" + (methodParameter.getMethod() == null ? "null" : methodParameter.getMethod().getName()) + "] from request body.");
		Class<?> parameterTypeClass = getParameterTypeClass(httpServletRequest, methodParameter);
		if (parameterTypeClass.isInterface()) {
			JavaType type = getJacksonHandler().getObjectMapper().getTypeFactory().constructType(methodParameter.getNestedGenericParameterType());
			return getJacksonHandler().getObjectMapper().readValue(jsonArgument, type);
		}
		return getJacksonHandler().fromJson(jsonArgument, parameterTypeClass);
	}


	//If the jsonParameterTypeClassNames is set and the value for the current parameter is not empty then we use
	//that as the class to deserialize as - this allows for concrete implementations to be deserialized when methods
	//with parameters with unknown generics are called, e.g. saveReconciliationRule(ReconciliationRule<?> reconciliationRule)
	//It is assumed that the parameterClass when passed will not be an interface; additional updates would need to be made to support that.
	private Class<?> getParameterTypeClass(HttpServletRequest httpServletRequest, MethodParameter methodParameter) {
		String[] jsonParameterClassNames = httpServletRequest.getParameterMap().get(JSON_PARAMETER_TYPE_CLASS_NAMES);
		if (jsonParameterClassNames != null && !StringUtils.isEmpty(jsonParameterClassNames[methodParameter.getParameterIndex()])) {
			Class<?> parameterTypeClass = CoreClassUtils.getClass(jsonParameterClassNames[methodParameter.getParameterIndex()]);
			//Ensure the parameterTypeClass is a valid subclass of the actual method parameter to prevent malicious deserialization attempts; the error message is intentionally vague.
			AssertUtils.assertTrue(methodParameter.getParameterType().isAssignableFrom(parameterTypeClass), "The parameter type class specified is not a valid subclass of the expected methodParameter.");
			return parameterTypeClass;
		}
		return methodParameter.getParameterType();
	}


	/**
	 * This method allows for the json request body to be parsed for the current argument parameterName.
	 * This is what allows this class to bind multiple-parameter arguments by selectively choosing the json
	 * to deserialize for each argument.
	 */
	private String getJsonArgument(String parameterName, String jsonBody) throws Exception {
		int depth = -1;
		boolean parameterNameFound = false;
		StringWriter stringWriter = new StringWriter();
		JsonGenerator generator = getFactory().createGenerator(stringWriter);
		JsonParser parser = getFactory().createParser(jsonBody);
		while (parser.nextToken() != null) {
			JsonToken token = parser.currentToken();
			if (token == JsonToken.START_OBJECT || token == JsonToken.START_ARRAY) {
				depth++;
			}
			else if (token == JsonToken.END_OBJECT || token == JsonToken.END_ARRAY) {
				depth--;
			}
			if (parameterNameFound) {
				if (token == JsonToken.START_ARRAY) {
					generator.writeStartArray();
				}
				else if (token == JsonToken.END_ARRAY) {
					generator.writeEndArray();
				}
				else if (token == JsonToken.START_OBJECT) {
					generator.writeStartObject();
				}
				else if (token == JsonToken.END_OBJECT) {
					generator.writeEndObject();
				}
				else if (token == JsonToken.VALUE_FALSE) {
					generator.writeBoolean(false);
				}
				else if (token == JsonToken.VALUE_TRUE) {
					generator.writeBoolean(true);
				}
				else if (token == JsonToken.VALUE_NULL) {
					generator.writeNull();
				}
				else if (token == JsonToken.VALUE_NUMBER_FLOAT) {
					generator.writeNumber(parser.getDecimalValue());
				}
				else if (token == JsonToken.VALUE_NUMBER_INT) {
					generator.writeNumber(parser.getIntValue());
				}
				else if (token == JsonToken.VALUE_STRING) {
					generator.writeString(parser.getText());
				}
				else if (token == JsonToken.FIELD_NAME) {
					generator.writeFieldName(parser.getText());
				}
				else {
					throw new UnsupportedOperationException("Unable to process token: " + token.name());
				}
				if (depth == 0) {
					break;
				}
			}
			else if (token == JsonToken.FIELD_NAME) {
				String tokenFieldName = parser.getText();
				if (parameterName.equals(tokenFieldName) && depth == 0) {
					parameterNameFound = true;
				}
			}
		}
		generator.close();
		return stringWriter.toString();
	}


	/**
	 * We read the full input stream and then store in the map
	 * This map is stored in the web request so when resolveArgument is called for subsequent argument we can parse the
	 * original json body for the current argument using the parameter type information for it.
	 */
	private String getJsonBody(HttpServletRequest httpServletRequest) {
		String jsonBody = (String) httpServletRequest.getAttribute(JSON_BODY);
		if (StringUtils.isEmpty(jsonBody)) {
			try {
				jsonBody = IOUtils.toString(httpServletRequest.getInputStream(), StandardCharsets.UTF_8.name());
				httpServletRequest.setAttribute(JSON_BODY, jsonBody);
			}
			catch (Exception e) {
				throw new RuntimeException("Failed to retrieve JSON request body!", e);
			}
		}
		return jsonBody;
	}

	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonFactory getFactory() {
		return this.factory;
	}


	public void setFactory(JsonFactory factory) {
		this.factory = factory;
	}


	public JacksonHandlerImpl getJacksonHandler() {
		return this.jacksonHandler;
	}


	public void setJacksonHandler(JacksonHandlerImpl jacksonHandler) {
		this.jacksonHandler = jacksonHandler;
	}
}
