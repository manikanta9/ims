package com.clifton.core.web.mvc;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ServletContextAware;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.FrameworkServlet;
import org.springframework.web.servlet.HandlerAdapter;
import org.springframework.web.servlet.HandlerExecutionChain;
import org.springframework.web.servlet.HandlerMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.ServletContext;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;


@Component
public class WebMethodHandlerImpl implements WebMethodHandler, ServletContextAware {

	private ServletContext servletContext;

	private HandlerMapping handlerMapping;
	private HandlerAdapter handlerAdapter;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public <T> T executeServiceCall(String url, Map<String, String> parameters) {
		// validate the url
		ValidationUtils.assertFalse(StringUtils.isEmpty(url), "Cannot execute method with out a url.");

		String originalUrl = url; // used for the error message

		// add a host to the url if it's missing, this is needed so the java URL object can be used to parse the query string
		if (!url.startsWith("http://")) {
			url = "http://localhost/" + url;
		}
		// get the base url without an query arguments, should be the same as the original url with out the query parameters
		String urlPath = parseUrlPath(url);

		// create the dummy request and add the parameters
		HttpServletRequestImpl request = new HttpServletRequestImpl(urlPath);
		Map<String, String> params = parseUrlParameters(url, parameters);
		if (params != null) {
			request.setParameters(params);
		}

		HandlerExecutionChain method = null;
		try {
			// get the registered handler for the url
			method = getHandlerMapping().getHandler(request);
			AssertUtils.assertNotNull(method, "Method is null");
			// execute the handler
			ModelAndView mv = getHandlerAdapter().handle(request, new HttpServletResponseImpl(request), method.getHandler());
			AssertUtils.assertNotNull(mv, "ModelAndView is null");
			Map<String, Object> model = mv.getModel();
			// get the result from the view
			WebRequestUtils.removeBindingResults(model);
			Object result = model.get(ContextConventionUtils.getMainKeyFromModel(model));
			if (result != null) {
				return (T) result;
			}
			return null;
		}
		catch (Throwable e) {
			// validate that the method in not null here so the the correct error is thrown
			ValidationUtils.assertNotNull(method, "No handler method found for [" + url + "].");
			throw new RuntimeException("Failed to call method on url [" + originalUrl + "].", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The the url path.  For example, url 'http://localhost/systemListItemListFind.json?listName=Bond Coupon Frequency' would return
	 * 'systemListItemListFind.json?listName=Bond Coupon Frequency'.
	 *
	 * @param url
	 */
	private String parseUrlPath(String url) {
		try {
			URL u = new URL(url);
			return u.getPath();
		}
		catch (MalformedURLException e) {
			throw new RuntimeException("Failed to parse url [" + url + "].", e);
		}
	}


	private Map<String, String> parseUrlParameters(String url, Map<String, String> parameters) {
		try {
			URL u = new URL(url);
			String query = u.getQuery();
			if (StringUtils.isEmpty(query)) {
				return parameters;
			}
			if (parameters == null) {
				parameters = new HashMap<>();
			}
			String[] stringParams = query.split("&");
			for (String stringParam : stringParams) {
				String[] pair = stringParam.split("=");
				if (pair.length == 2) {
					parameters.put(pair[0], pair[1]);
				}
			}
			return parameters;
		}
		catch (MalformedURLException e) {
			throw new RuntimeException("Failed to parse url [" + url + "].", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private WebApplicationContext getWebApplicationContext() {
		// ugly but works; don't know a better way to get web context
		// services are defined in main application context and can't be wired with beans from web context
		// the following commented out line for getting web context doesn't work - returns application context
		// WebApplicationContext wac = WebApplicationContextUtils.getRequiredWebApplicationContext(this.filterConfig.getServletContext());
		String contextAttributeName = FrameworkServlet.SERVLET_CONTEXT_PREFIX + "jsonAppDispatcherServlet";
		return (WebApplicationContext) getServletContext().getAttribute(contextAttributeName);
	}


	@ValueIgnoringGetter
	public HandlerMapping getHandlerMapping() {
		// Compute from the child Spring context if not autowired or cached
		if (this.handlerMapping == null) {
			this.handlerMapping = this.getWebApplicationContext().getBean("handlerMapping", HandlerMapping.class);
		}
		return this.handlerMapping;
	}


	@ValueIgnoringGetter
	public HandlerAdapter getHandlerAdapter() {
		// Compute from the child Spring context if not autowired or cached
		if (this.handlerAdapter == null) {
			this.handlerAdapter = this.getWebApplicationContext().getBean("handlerAdapter", HandlerAdapter.class);
		}
		return this.handlerAdapter;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ServletContext getServletContext() {
		return this.servletContext;
	}


	@Override
	public void setServletContext(ServletContext servletContext) {
		this.servletContext = servletContext;
	}


	public void setHandlerMapping(HandlerMapping handlerMapping) {
		this.handlerMapping = handlerMapping;
	}


	public void setHandlerAdapter(HandlerAdapter handlerAdapter) {
		this.handlerAdapter = handlerAdapter;
	}
}
