package com.clifton.core.web.handler;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.StringUtils;
import org.springframework.web.method.HandlerMethod;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;


/**
 * The WebMethodCallException wraps the cause exception with additional information about the request which can be used to find additional details when
 * attempting to give users a friendlier message.
 *
 * @author manderson
 */
public class WebMethodCallException extends RuntimeException {

	private final Serializable requestId;
	private final String handlerMethodName;


	public WebMethodCallException(HttpServletRequest request, Object handler, Exception cause) {
		super(cause);
		this.requestId = getIdRequestParameter(request);
		if (handler instanceof HandlerMethod) {
			this.handlerMethodName = ((HandlerMethod) handler).getMethod().getName();
		}
		else {
			this.handlerMethodName = null;
		}
	}

	///////////////////////////////////////////////////////////////////


	public Serializable getIdRequestParameter(HttpServletRequest request) {
		if (request != null) {
			String id = request.getParameter("id");
			if (!StringUtils.isEmpty(id)) {
				return id;
			}
		}
		return null;
	}


	public String getTableNameFromMethod() {
		if (getHandlerMethodName() != null) {
			return ContextConventionUtils.getTableNameFromMethodName(getHandlerMethodName());
		}
		return null;
	}


	///////////////////////////////////////////////////////////////////


	public Serializable getRequestId() {
		return this.requestId;
	}


	public String getHandlerMethodName() {
		return this.handlerMethodName;
	}
}
