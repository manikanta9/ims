package com.clifton.core.web.api.client.locator;

/**
 * The ExternalServiceLocator interface provides methods for identifying and locating external applications
 * and their corresponding services. Our application can be integrated with multiple other applications.
 * In some instances, multiple applications will have their own implementations and data structures for the
 * same Service interface.  This locator can be used to obtain application specific implementations.
 *
 * @author vgomelsky
 */
public interface ExternalServiceLocator {

	/**
	 * Returns an instance of the service for the specified data sources.
	 * Using our naming conventions to lookup the bean and throws an exception if it cannot be found.
	 */
	public <T> T getExternalService(Class<T> serviceInterface, String dataSourceName);


	/**
	 * Returns true if the specified service object is a proxy to an external service.
	 * Returns false, if the object is implemented in local application that is likely using local data source(s).
	 */
	public boolean isExternalService(Object serviceObject);
}
