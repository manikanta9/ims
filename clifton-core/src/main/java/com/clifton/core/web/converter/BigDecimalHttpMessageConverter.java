package com.clifton.core.web.converter;


import java.math.BigDecimal;


/**
 * The <code>BigDecimalHttpMessageConverter</code> class supports methods that return java.math.BigDecimal value and are annotated with @ResponseBody.
 * It's a copy of StringHttpMessageConverter with BigDecimal specific changes.
 * <p/>
 * Uses BigDecimal.toString()
 *
 * @author vgomelsky
 */
public class BigDecimalHttpMessageConverter extends BaseHttpMessageConverter<BigDecimal> {

	@Override
	public boolean supports(Class<?> clazz) {
		return BigDecimal.class.equals(clazz);
	}


	@Override
	protected BigDecimal convertFromString(String stringValue) {
		return new BigDecimal(stringValue);
	}
}
