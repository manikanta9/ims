package com.clifton.core.web.mvc;


import org.springframework.util.ReflectionUtils;
import org.springframework.web.method.annotation.InitBinderDataBinderFactory;
import org.springframework.web.method.support.InvocableHandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerAdapter;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.List;


/**
 * The <code>ValidatingAnnotationMethodHandlerAdapter</code> class extends {@link RequestMappingHandlerAdapter} and
 * overrides only createDataBinderFactory method to return an instance of new {@link ValidatingServletRequestDataBinderFactory} object.
 *
 * @author vgomelsky
 */
public class ValidatingRequestMappingHandlerAdapter extends RequestMappingHandlerAdapter {

	// We NEVER use @ModelAttribute without explicit or implicit @RequestMapping. Because many of our methods
	// with @ModelAttribute do not have explicit @RequestMapping (they just use our naming convention), we need to always return false.
	public static final ReflectionUtils.MethodFilter MODEL_ATTRIBUTE_METHODS_OVERRIDE = method -> (false);

	/**
	 * Specifies whether lookups related to bean population should be performed in a single read-only transaction.
	 * Single transaction improves performance when multiple lookups are made.
	 */
	private boolean transactionalBeanPrepare = true;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ValidatingRequestMappingHandlerAdapter() {
		super();

		// need to override the value of final field: doesn't looks like there's a cleaner way to do this with Spring
		try {
			Field field = RequestMappingHandlerAdapter.class.getField("MODEL_ATTRIBUTE_METHODS");
			Field modifiersField = Field.class.getDeclaredField("modifiers");
			modifiersField.setAccessible(true);
			modifiersField.setInt(field, field.getModifiers() & ~Modifier.FINAL);

			field.set(null, MODEL_ATTRIBUTE_METHODS_OVERRIDE);

			// after the field is updated, return the modifier to prior state
			modifiersField.setInt(field, field.getModifiers() | Modifier.FINAL);
			modifiersField.setAccessible(false);
		}
		catch (Exception e) {
			throw new RuntimeException("Cannot override final field: MODEL_ATTRIBUTE_METHODS", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected InitBinderDataBinderFactory createDataBinderFactory(List<InvocableHandlerMethod> binderMethods) throws Exception {
		return new ValidatingServletRequestDataBinderFactory(binderMethods, getWebBindingInitializer(), isTransactionalBeanPrepare());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public boolean isTransactionalBeanPrepare() {
		return this.transactionalBeanPrepare;
	}


	public void setTransactionalBeanPrepare(boolean transactionalBeanPrepare) {
		this.transactionalBeanPrepare = transactionalBeanPrepare;
	}
}
