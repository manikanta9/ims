package com.clifton.core.web.converter;


import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>DateHttpMessageConverter</code> class supports methods that return java.util.Date value and are annotated with @ResponseBody.
 * It's a copy of StringHttpMessageConverter with Date specific changes.
 * <p/>
 * Uses the following date format: DateUtils.DATE_FORMAT_FULL = "yyyy-MM-dd HH:mm:ss"
 *
 * @author vgomelsky
 */
public class DateHttpMessageConverter extends BaseHttpMessageConverter<Date> {

	@Override
	public boolean supports(Class<?> clazz) {
		// support both java.util.Date as well as java.sql.Date
		return Date.class.isAssignableFrom(clazz);
	}


	@Override
	protected Date convertFromString(String stringValue) {
		return DateUtils.toDate(stringValue, DateUtils.DATE_FORMAT_FULL);
	}


	@Override
	protected String convertToString(Date value) {
		return DateUtils.fromDate(value, DateUtils.DATE_FORMAT_FULL);
	}
}
