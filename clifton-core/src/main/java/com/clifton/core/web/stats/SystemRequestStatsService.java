package com.clifton.core.web.stats;


import com.clifton.core.cache.CacheStats;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.search.form.entity.AuditableEntitySearchForm;
import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Date;
import java.util.List;


/**
 * The <code>SystemRequestStatsService</code> interface defines methods for logging SystemRequestStats as well as retrieving them.
 *
 * @author vgomelsky
 */
public interface SystemRequestStatsService {

	////////////////////////////////////////////////////////////////////////////
	////////////                 Web Request Stats                  ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * get the Date time the stats collection was started.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public Date getSystemStatsLastCleared();


	/**
	 * Returns a Collection of all SystemRequestStats objects. (defaults to use locking)
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<SystemRequestStats> getSystemRequestStatsList(AuditableEntitySearchForm searchForm);


	/**
	 * Adds the specified SystemRequestStats to currently saved stats. Use this method for web requests
	 */
	@DoNotAddRequestMapping
	public void saveSystemRequestStats(HttpServletRequest request, HttpServletResponse response, Object user, long requestTimeNano, int dbHits, long dbTimeNano);


	/**
	 * Adds the specified SystemRequestStats to currently saved stats.
	 */
	@DoNotAddRequestMapping
	public void saveSystemRequestStats(String requestSource, String requestURI, Object user, long requestTimeNano, int dbHits, long dbTimeNano);


	/**
	 * Removes request stats for the specified source and URI from the cache.
	 * Can be used to cleanup invalid data.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void deleteSystemRequestStats(String requestSource, String requestURI);


	/**
	 * Removes all request stats by clearing full cache.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void deleteSystemRequestStatsAll();


	////////////////////////////////////////////////////////////////////////////
	////////////                 Caching and Memory                 ////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Enables/disables statistics collection for the specified cache.
	 * If the specified cacheName == null, then does this for ALL caches.
	 *
	 * @return returns the number of caches affected
	 */
	@ResponseBody
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public int setSystemCacheStatsEnabled(String cacheName, boolean enableStatistics);


	/**
	 * Returns a List of CacheStats for all caches.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<CacheStats> getSystemCacheStatsList(boolean calculateSize);


	/**
	 * Returns CacheStats for the cache with the specified cacheName.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public CacheStats getSystemCacheStatsForName(String cacheName);


	/**
	 * Clears all items from the cache with the specified name
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void deleteSystemCache(String cacheName);


	/**
	 * Clears all caches from the cache handler.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public void deleteSystemCacheAll();


	/**
	 * Runs garbage collection and returns memory state: free, total, max.
	 */
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public MemoryState getSystemMemoryState();


	/**
	 * set a exclusive lock
	 */
	@DoNotAddRequestMapping
	public void systemRequestExclusiveLock();


	/**
	 * release a exclusive lock
	 */
	@DoNotAddRequestMapping
	public void systemRequestExclusiveUnlock();
}
