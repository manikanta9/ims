package com.clifton.core.web.mvc;

import com.clifton.core.context.Context;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.http.HttpUtils;
import org.springframework.validation.BindingResult;

import javax.servlet.http.HttpServletRequest;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


/**
 * The <code>WebRequestUtils</code> contains helper methods used when making webrequests.
 * Specifically; when external service calls are made using raw application/json data rather
 * than url GET params or POST data; then the properties are stored in an argument map after being resolved
 * which are later converted into a map similar to the nativeWebRequests parameter map;  This allows
 * our even tracking code to obtain request parameters consistently regardless of using form, url, or raw data.
 *
 * @author StevenF
 */
public class WebRequestUtils {


	@SuppressWarnings("unchecked")
	public static Map<String, String[]> getParameterMap(HttpServletRequest request) {
		Map<String, String[]> parameterMap = (Map<String, String[]>) request.getAttribute(Context.REQUEST_PARAMETER_MAP);
		if (parameterMap != null) {
			return parameterMap;
		}
		return request.getParameterMap();
	}


	/**
	 * Returns the URI for the specified request including all request parameters.
	 */
	public static String getFullURL(HttpServletRequest request) {
		StringBuilder result = new StringBuilder(request.getRequestURI());

		Map<String, String[]> params = request.getParameterMap();
		Set<String> names = CollectionUtils.getKeys(params);
		if (!CollectionUtils.isEmpty(names)) {
			result.append('?');
			int index = 1;
			for (String name : names) {
				result.append(name).append('=').append(ArrayUtils.toString(params.get(name)));
				if (index++ < names.size()) {
					result.append('&');
				}
			}
		}

		return result.toString();
	}


	/**
	 * Removes all Spring's {@link BindingResult} objects from the model.
	 * Returns true if at least one of them has errors;
	 *
	 * @param model
	 */
	public static boolean removeBindingResults(Map<String, Object> model) {
		boolean hasErrors = false;
		Iterator<Map.Entry<String, Object>> modelEntryIterator = model.entrySet().iterator();
		while (modelEntryIterator.hasNext()) {
			Map.Entry<String, Object> modelEntry = modelEntryIterator.next();
			if (modelEntry.getKey().startsWith(BindingResult.MODEL_KEY_PREFIX)) {
				BindingResult bindingResult = (BindingResult) modelEntry.getValue();
				modelEntryIterator.remove();
				if (bindingResult.hasErrors()) {
					hasErrors = true;
				}
			}
		}
		return hasErrors;
	}


	public static int getIntParameterValue(HttpServletRequest request, String parameterName, int defaultValue) {
		try {
			return Integer.parseInt(HttpUtils.getUrlDecodedParameterValue(request, parameterName));
		}
		catch (Exception e) {
			return defaultValue;
		}
	}
}
