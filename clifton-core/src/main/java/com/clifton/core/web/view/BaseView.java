package com.clifton.core.web.view;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.CauseEntityAware;
import com.clifton.core.validation.CauseEventTypeAware;
import com.clifton.core.validation.LinkedEntityAware;
import com.clifton.core.validation.UserIgnorableValidationException;
import com.clifton.core.web.mvc.WebRequestUtils;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


/**
 * The <code>BaseView</code> is an abstract class that our view extend. It contains generic View information
 * <p/>
 * Optional "resultKeysToExclude" request parameter can be used to filter results returned.
 *
 * @author manderson
 */
public abstract class BaseView implements View {

	private ContextHandler contextHandler;

	/**
	 * Root node name to use when the model contains only one object (replaces model's key).
	 * If not set, don't replace the root.
	 */
	private String rootNodeName;

	/**
	 * If set, will add this property to JSON response with true or false value (false when there's an error)
	 */
	private String successPropertyName = "success";
	/**
	 * When there is an error, will add a property with this name to JSON response.  The value will be the error message.
	 */
	private String errorMessagePropertyName = "message";

	/**
	 * When an exception occurs that is not an instance of {@link ValidationException}
	 * and this message override is populated, the error will be logged, but this message override will be thrown
	 */
	private String nonValidationExceptionMessageOverride;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract void writeResponse(HttpServletRequest request, HttpServletResponse response, Map<String, Object> model) throws Exception;


	public abstract void setResponseContentType(HttpServletRequest request, HttpServletResponse response);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void render(Map<String, ?> modelArg, HttpServletRequest request, HttpServletResponse response) throws Exception {
		@SuppressWarnings("unchecked")
		Map<String, Object> model = (Map<String, Object>) modelArg;

		boolean success = true;
		if (WebRequestUtils.removeBindingResults(model)) {
			// TODO: 1. ExtJS JSON formatting (include error support) - put errors into the model
			throw new RuntimeException("TODO: implement BindingResult with errors");
		}
		else if (model.size() == 1) {
			Iterator<Map.Entry<String, Object>> entryIterator = model.entrySet().iterator();
			Map.Entry<String, Object> entry = entryIterator.next();
			String key = entry.getKey();
			Object value = entry.getValue();
			if ("exception".equals(key) && value instanceof Throwable) {
				success = false;

				if (!(value instanceof ValidationException) && !StringUtils.isEmpty(getNonValidationExceptionMessageOverride())) {
					model.put(getErrorMessagePropertyName(), getNonValidationExceptionMessageOverride());
				}
				else {
					// NOTE: Need to set message when errors - FileDownloadView expects message to be there when errors so users can see the actual errors, not just "error processing request"
					// TODO: move exception translation to another place
					// TODO: do not log validation exceptions (ValidationHelper?)
					if (value instanceof ServletRequestBindingException) {
						BindException bindException = (BindException) ((ServletRequestBindingException) value).getCause();
						List<FieldError> list = bindException.getFieldErrors();
						List<com.clifton.core.validation.FieldError> fieldErrors = new ArrayList<>();
						entryIterator.remove();
						model.put("errors", fieldErrors);
						StringBuilder msg = new StringBuilder(16);
						for (FieldError error : list) {
							fieldErrors.add(new com.clifton.core.validation.FieldError(error.getField(), error.getDefaultMessage()));
							msg.append(error.getDefaultMessage()).append(";");
						}
						model.put(getErrorMessagePropertyName(), msg.toString());
					}
					// TODO: other validation exceptions? multiple fields?
					else if (value instanceof ValidationException) {
						entryIterator.remove();
						model.put(getErrorMessagePropertyName(), ((ValidationException) value).getMessage());
						model.put("validationError", true);
						model.put("errorType", value.getClass().getSimpleName());
						if (value instanceof CauseEntityAware) {
							IdentityObject causeEntity = ((CauseEntityAware) value).getCauseEntity();
							if (causeEntity != null) {
								model.put("causeEntity", causeEntity);
								model.put("causeEntityId", causeEntity.getIdentity());
								model.put("causeEntityClass", causeEntity.getClass().getName());
								model.put("causeEntityLabel", BeanUtils.getLabel(causeEntity));
								model.put("causeEntityTable", ContextConventionUtils.getTableNameFromClass(causeEntity.getClass()));
							}
						}
						if (value instanceof CauseEventTypeAware) {
							DaoEventTypes eventType = ((CauseEventTypeAware) value).getCauseEvent();
							if (eventType != null) {
								model.put("eventType", eventType);
							}
						}
						if (value instanceof LinkedEntityAware) {
							IdentityObject linkedEntity = ((LinkedEntityAware) value).getLinkedEntity();
							if (linkedEntity != null) {
								model.put("linkedEntityId", linkedEntity.getIdentity());
								model.put("linkedEntityLabel", BeanUtils.getLabel(linkedEntity));
								model.put("linkedEntityTable", ContextConventionUtils.getTableNameFromClass(linkedEntity.getClass()));
							}
						}
						if (value instanceof UserIgnorableValidationException) {
							model.put("ignoreExceptionUrl", ((UserIgnorableValidationException) value).getIgnoreExceptionUrlWithParameters());
							model.put("confirmationMessage", ((UserIgnorableValidationException) value).getConfirmationMessage());
						}
						if (value instanceof FieldValidationException) {
							FieldValidationException fieldException = (FieldValidationException) value;
							model.put("errors", Collections.singletonList(new com.clifton.core.validation.FieldError(fieldException.getFieldName(), fieldException.getMessage())));
						}
					}
					else {
						String msg = ExceptionUtils.getOriginalMessage((Throwable) value);
						if (StringUtils.isEmpty(msg)) {
							msg = ((Throwable) value).getMessage();
							if (StringUtils.isEmpty(msg)) {
								msg = ExceptionUtils.getOriginalException((Throwable) value).getClass().getName();
							}
						}
						model.put(getErrorMessagePropertyName(), "Unexpected Error Occurred: [" + msg + "].");
					}
				}
			}
		}

		if (success && !StringUtils.isEmpty(getRootNodeName())) {
			String key = ContextConventionUtils.getMainKeyFromModel(model);

			if (key != null) {
				model.put(getRootNodeName(), model.remove(key));
			}
		}
		if (!StringUtils.isEmpty(getSuccessPropertyName())) {
			model.put(getSuccessPropertyName(), success);
		}

		String resultKeysToExclude = request.getParameter("resultKeysToExclude");
		if (resultKeysToExclude != null) {
			for (String key : resultKeysToExclude.split(",")) {
				model.remove(key);
			}
		}

		try {
			setResponseContentType(request, response);
			writeResponse(request, response, model);
		}
		catch (Throwable e) {
			if (ExceptionUtils.isConnectionBrokenException(e)) {
				// the client closed connection: not much we can do here and no reason logging it
				LogUtils.info(LogCommand.ofThrowableAndMessage(getClass(), e, "Error writing response").withRequest(request));
			}
			else {
				// adding this for now because "request" details do not get logged in some cases: hopefully they're present here
				LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), e, "Error writing response: ", e.getMessage()).withRequest(request));
			}
			throw e;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public String getRootNodeName() {
		return this.rootNodeName;
	}


	public void setRootNodeName(String rootNodeName) {
		this.rootNodeName = rootNodeName;
	}


	public String getSuccessPropertyName() {
		return this.successPropertyName;
	}


	public void setSuccessPropertyName(String successPropertyName) {
		this.successPropertyName = successPropertyName;
	}


	public String getErrorMessagePropertyName() {
		return this.errorMessagePropertyName;
	}


	public void setErrorMessagePropertyName(String errorMessagePropertyName) {
		this.errorMessagePropertyName = errorMessagePropertyName;
	}


	public String getNonValidationExceptionMessageOverride() {
		return this.nonValidationExceptionMessageOverride;
	}


	public void setNonValidationExceptionMessageOverride(String nonValidationExceptionMessageOverride) {
		this.nonValidationExceptionMessageOverride = nonValidationExceptionMessageOverride;
	}
}
