package com.clifton.core.web.api.client.openapi;

import org.springframework.web.client.RestTemplate;


/**
 * The {@link RestTemplateConfigurer} interface defines a method for configuring {@link org.springframework.web.client.RestTemplate} objects,
 * e.g. adding specific security interceptors.
 *
 * @author lnaylor
 */
public interface RestTemplateConfigurer {


	/**
	 * Configures the given RestTemplate
	 *
	 * @param restTemplate the RestTemplate to be configured
	 */
	public void configure(RestTemplate restTemplate);
}
