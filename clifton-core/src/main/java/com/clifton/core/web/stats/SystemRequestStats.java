package com.clifton.core.web.stats;


import com.clifton.core.util.date.DateUtils;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SystemRequestStats</code> class collects and aggregates execution statistics for system requests:
 * web requests for a specific URI, batch jobs, etc.
 *
 * @author vgomelsky
 */
public class SystemRequestStats {

	/**
	 * Identifies the source of the request: WEB-UI, BATCH, etc.
	 */
	private String requestSource;
	/**
	 * Identifies URI of the request for the given source: URL for web requests.
	 */
	private final String requestURI;

	/**
	 * Total number of times this request was executed
	 */
	private int executeCount;
	/**
	 * Number of times this request was execute with requests that are longer than slowestMinNano.
	 */
	private int slowExecuteCount;

	private long totalDBTimeNano;
	private long totalTimeNano;
	private int dbExecuteCount;

	private long minDBTimeNano = -1;
	private long minTotalTimeNano = -1;

	private long maxDBTimeNano = -1;
	private long maxTotalTimeNano = -1;

	/**
	 * Max number of slowest requests to collect detail stats about.
	 */
	private final int slowestMaxCount;

	/**
	 * Min request duration after which it's considered slow.
	 */
	private long slowestMinNano;

	/**
	 * Min time of slowestStats items cached for performance.
	 */
	private long minTimeOutOfSlowest = -1;

	/**
	 * Min index of slowest out of slowestStats items.
	 */
	private int minIndexOutOfSlowest;

	/**
	 * A list of up to slowestMaxCount slowest ActionStatDetail objects
	 */
	private List<SystemRequestStatsDetail> slowestStats;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemRequestStats() {
		// Empty Constructor - Needed for binding (i.e. Portal Admin getting cache list from Portal)
		this(null, null, 0, 0);
	}


	public SystemRequestStats(String requestSource, String requestURI, int slowestMaxCount, long slowestMinNano) {
		this.requestSource = requestSource;
		this.requestURI = requestURI;
		this.slowestMaxCount = slowestMaxCount;
		this.slowestMinNano = slowestMinNano;
	}


	/**
	 * Updates to includes the specified request parameters into stats.
	 *
	 * @return Returns true if the specified request is a new slow request (may want to collect more data about it).
	 * Returns false otherwise.
	 */
	public boolean addRequestExecution(long requestTimeNano, int dbHits, long dbTimeNano) {
		// update totals
		this.executeCount++;
		this.dbExecuteCount += dbHits;

		this.totalDBTimeNano += dbTimeNano;
		this.totalTimeNano += requestTimeNano;

		// update min/max if needed
		if (this.minDBTimeNano == -1 || (dbTimeNano < this.minDBTimeNano)) {
			this.minDBTimeNano = dbTimeNano;
		}
		if (this.maxDBTimeNano == -1 || (dbTimeNano > this.maxDBTimeNano)) {
			this.maxDBTimeNano = dbTimeNano;
		}

		if (this.minTotalTimeNano == -1 || (requestTimeNano < this.minTotalTimeNano)) {
			this.minTotalTimeNano = requestTimeNano;
		}
		if (this.maxTotalTimeNano == -1 || (requestTimeNano > this.maxTotalTimeNano)) {
			this.maxTotalTimeNano = requestTimeNano;
		}

		if (requestTimeNano >= this.slowestMinNano) {
			this.slowExecuteCount++;
			return (requestTimeNano > this.minTimeOutOfSlowest || (getSlowestExecuteCount() < this.slowestMaxCount));
		}
		return false;
	}


	public void addSystemRequestStatsDetail(SystemRequestStatsDetail detail) {
		if (detail == null) {
			return;
		}
		if (this.slowestStats == null) {
			// first time: create an empty list
			this.slowestStats = new ArrayList<>(getSlowestMaxCount());
		}

		int size = this.slowestStats.size();
		if (size < getSlowestMaxCount()) {
			// the list is empty: add to the end  
			this.slowestStats.add(detail);
			size++;
		}
		else {
			// the list is full: replace fastest detail
			this.slowestStats.set(this.minIndexOutOfSlowest, detail);
			this.minTimeOutOfSlowest = detail.getDurationNano();
		}

		// find new slowest
		for (int i = 0; i < size; i++) {
			detail = this.slowestStats.get(i);
			if ((this.minTimeOutOfSlowest == -1) || (detail.getDurationNano() < this.minTimeOutOfSlowest)) {
				this.minTimeOutOfSlowest = detail.getDurationNano();
				this.minIndexOutOfSlowest = i;
			}
		}
	}


	private double convertNanoToSeconds(long nano) {
		return ((double) nano / 1_000_000_000);
	}


	public int getSlowestExecuteCount() {
		return (this.slowestStats == null) ? 0 : this.slowestStats.size();
	}


	public double getAvgTimeSeconds() {
		return getTotalTimeSeconds() / getExecuteCount();
	}


	public String getAvgTimeFormatted() {
		return DateUtils.getTimeDifference((long) (getAvgTimeSeconds() * DateUtils.ONE_SECOND), false);
	}


	public double getAvgDBTimeSeconds() {
		return getTotalDBTimeSeconds() / getExecuteCount();
	}


	public int getAvgDbExecuteCount() {
		return (getDbExecuteCount() / getExecuteCount());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getRequestURI() {
		return this.requestURI;
	}


	public int getExecuteCount() {
		return this.executeCount;
	}


	public int getSlowExecuteCount() {
		return this.slowExecuteCount;
	}


	public double getTotalDBTimeSeconds() {
		return convertNanoToSeconds(getTotalDBTimeNano());
	}


	public double getTotalTimeSeconds() {
		return convertNanoToSeconds(getTotalTimeNano());
	}


	public String getTotalTimeFormatted() {
		return DateUtils.getTimeDifference((long) (getTotalTimeSeconds() * DateUtils.ONE_SECOND), false);
	}


	public int getDbExecuteCount() {
		return this.dbExecuteCount;
	}


	public double getMinDBTimeSeconds() {
		return convertNanoToSeconds(getMinDBTimeNano());
	}


	public double getMinTotalTimeSeconds() {
		return convertNanoToSeconds(getMinTotalTimeNano());
	}


	public String getMinTotalTimeFormatted() {
		return DateUtils.getTimeDifference((long) (getMinTotalTimeSeconds() * DateUtils.ONE_SECOND), false);
	}


	public double getMaxDBTimeSeconds() {
		return convertNanoToSeconds(getMaxDBTimeNano());
	}


	public double getMaxTotalTimeSeconds() {
		return convertNanoToSeconds(getMaxTotalTimeNano());
	}


	public String getMaxTotalTimeFormatted() {
		return DateUtils.getTimeDifference((long) (getMaxTotalTimeSeconds() * DateUtils.ONE_SECOND), false);
	}


	public int getSlowestMaxCount() {
		return this.slowestMaxCount;
	}


	public double getSlowestMinSeconds() {
		return convertNanoToSeconds(this.slowestMinNano);
	}


	public long getMinTimeOutOfSlowest() {
		return this.minTimeOutOfSlowest;
	}


	public int getMinIndexOutOfSlowest() {
		return this.minIndexOutOfSlowest;
	}


	public List<SystemRequestStatsDetail> getSlowestStats() {
		return this.slowestStats;
	}


	public String getRequestSource() {
		return this.requestSource;
	}


	public void setRequestSource(String requestSource) {
		this.requestSource = requestSource;
	}


	public long getTotalDBTimeNano() {
		return this.totalDBTimeNano;
	}


	public long getMinDBTimeNano() {
		return this.minDBTimeNano;
	}


	public long getMinTotalTimeNano() {
		return this.minTotalTimeNano;
	}


	public long getMaxDBTimeNano() {
		return this.maxDBTimeNano;
	}


	public long getMaxTotalTimeNano() {
		return this.maxTotalTimeNano;
	}


	public long getTotalTimeNano() {
		return this.totalTimeNano;
	}


	@Override
	public String toString() {
		return "{URI: " + getRequestURI() + ", Source: " + getRequestSource() + ", Count: " + getExecuteCount() + "}";
	}
}
