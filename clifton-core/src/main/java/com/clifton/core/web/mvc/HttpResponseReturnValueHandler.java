package com.clifton.core.web.mvc;

import com.clifton.core.util.dataaccess.PagingArrayList;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpEntity;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.annotation.ModelFactory;
import org.springframework.web.method.support.HandlerMethodReturnValueHandler;
import org.springframework.web.method.support.ModelAndViewContainer;

import java.util.List;


/**
 * The {@link HttpResponseReturnValueHandler} is used to add the response body of an {@link ResponseEntity} to the {@link ModelAndViewContainer}
 * so that it is available for the UI to display. The model is used to render {@link org.springframework.web.servlet.View} objects as opposed to
 * serializing return values directly to clients, e.g. see {@link com.clifton.core.web.view.JsonView}
 *
 * @author lnaylor
 */
public class HttpResponseReturnValueHandler implements HandlerMethodReturnValueHandler {

	@Override
	public boolean supportsReturnType(MethodParameter returnType) {
		return (HttpEntity.class.isAssignableFrom(returnType.getParameterType()) &&
				!RequestEntity.class.isAssignableFrom(returnType.getParameterType()));
	}


	@Override
	public void handleReturnValue(Object returnValue, MethodParameter returnType, ModelAndViewContainer mavContainer, NativeWebRequest webRequest) throws Exception {
		if (returnValue != null) {
			String name = ModelFactory.getNameForReturnValue(returnValue, returnType);
			if (returnValue instanceof ResponseEntity) {
				ResponseEntity<?> responseEntity = (ResponseEntity<?>) returnValue;
				if (responseEntity.getBody() instanceof List) {
					mavContainer.addAttribute(name, new PagingArrayList<>((List<?>) responseEntity.getBody()));
				}
			}
		}
	}
}
