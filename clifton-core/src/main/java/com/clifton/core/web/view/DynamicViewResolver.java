package com.clifton.core.web.view;


import com.clifton.core.util.StringUtils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.View;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;


/**
 * The <code>DynamicViewResolver</code> returns the appropriate view depending on the url pattern.
 * If none exists it calls the StaticViewResolver.
 *
 * @author manderson
 */
public class DynamicViewResolver extends StaticViewResolver {

	/**
	 * The singular view name for which the {@link DynamicViewResolver} will be applied. Views which do not use this view name will not be processed by this view resolver.
	 */
	public static final String DYNAMIC_VIEW_NAME = DynamicViewResolver.class.getName();

	private View defaultDownloadView;
	private Map<Pattern, View> urlPatternToViewMap;
	private Map<String, View> outputFormatToDownloadViewMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public View resolveViewName(String viewName, Locale locale) throws Exception {
		// In Order to get the Request - See: http://stackoverflow.com/questions/1192883/spring-accessing-httpServletRequest-in-a-custom-viewResolver
		HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.currentRequestAttributes()).getRequest();

		// Only apply this resolver when selected
		if (!DYNAMIC_VIEW_NAME.equals(viewName)) {
			return null;
		}

		// when 'outputFormat' request parameter is specified, it means corresponding file download was requested
		String outputFormat = request.getParameter("outputFormat");
		if (!StringUtils.isEmpty(outputFormat)) {
			View downloadView = null;
			if (getOutputFormatToDownloadViewMap() != null) {
				downloadView = getOutputFormatToDownloadViewMap().get(outputFormat);
			}
			return downloadView != null ? downloadView : getDefaultDownloadView() != null ? getDefaultDownloadView() : super.resolveViewName(viewName, locale);
		}

		String requestUri = request.getRequestURI();
		if (getUrlPatternToViewMap() != null) {
			//Use pre-compiled patterns (http://stackoverflow.com/questions/1360113/is-java-regex-thread-safe)
			for (Map.Entry<Pattern, View> patternViewEntry : getUrlPatternToViewMap().entrySet()) {
				if (patternViewEntry.getKey().matcher(requestUri).matches()) {
					return patternViewEntry.getValue();
				}
			}
		}

		return super.resolveViewName(viewName, locale);
	}


	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public View getDefaultDownloadView() {
		return this.defaultDownloadView;
	}


	public void setDefaultDownloadView(View defaultDownloadView) {
		this.defaultDownloadView = defaultDownloadView;
	}


	public Map<Pattern, View> getUrlPatternToViewMap() {
		return this.urlPatternToViewMap;
	}


	public void setUrlPatternToViewMap(Map<Pattern, View> urlPatternToViewMap) {
		this.urlPatternToViewMap = urlPatternToViewMap;
	}


	public Map<String, View> getOutputFormatToDownloadViewMap() {
		return this.outputFormatToDownloadViewMap;
	}


	public void setOutputFormatToDownloadViewMap(Map<String, View> outputFormatToDownloadViewMap) {
		this.outputFormatToDownloadViewMap = outputFormatToDownloadViewMap;
	}
}
