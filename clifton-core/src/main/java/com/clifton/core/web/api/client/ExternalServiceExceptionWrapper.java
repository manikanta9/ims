package com.clifton.core.web.api.client;

/**
 * The class <code>ExternalServiceObjectWrapper</code> is utilized when a method utilizes interfaces
 * for argument or return value parameters;  We wrap any object we are sending, or returning in this
 * object so we can store the json representation of the object in addition to the name of the concrete
 * implementation class.  This allows us to deserialize properly when needed.
 *
 * @author StevenF
 */
public class ExternalServiceExceptionWrapper extends Exception {

	private String objectClassName;
	private String objectJsonString;


	public ExternalServiceExceptionWrapper(String message) {
		super(message);
	}


	////////////////////////////////////////////////////////////////////////////
	////////               Getter and Setter Methods               /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getObjectClassName() {
		return this.objectClassName;
	}


	public void setObjectClassName(String objectClassName) {
		this.objectClassName = objectClassName;
	}


	public String getObjectJsonString() {
		return this.objectJsonString;
	}


	public void setObjectJsonString(String objectJsonString) {
		this.objectJsonString = objectJsonString;
	}
}
