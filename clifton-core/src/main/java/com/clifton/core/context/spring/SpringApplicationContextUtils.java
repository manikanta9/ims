package com.clifton.core.context.spring;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.compare.CompareUtils;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.ListableBeanFactory;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;

import java.util.List;
import java.util.stream.Collectors;


/**
 * The SpringApplicationContextUtils class provides static utility methods that simplify working with Spring's ApplicationContext and related objects.
 *
 * @author vgomelsky
 */
public class SpringApplicationContextUtils {

	/**
	 * Retrieves the bean definitions of the given type from the provided registry.
	 * <p>
	 * This method should be used to find bean definitions as <i>currently defined</i> within the bean registry with the knowledge that these definitions may be added, modified, or
	 * removed later, depending on the current state of the {@link ApplicationContext}.
	 * <p>
	 * This method has a key distinction from {@link ListableBeanFactory#getBeansOfType(Class)} and related methods in that it discovers bean definitions of the given type <i>
	 * without</i> the possibility of instantiating beans. The standard {@link BeanFactory} {@code getBean} methods typically instantiate beans during the process in order to
	 * ensure completeness during the search. However, this leads to issues during the Bean Factory Post Processor phase, during which bean instantiation can lead to
	 * <a href="https://jira.paraport.com/browse/IMS-176">numerous issues</a>.
	 * <p>
	 * Due to limitations inherent to bean definitions before instantiation, this method does not conclusively find all definitions that will be instantiated with the given type.
	 * Specifically, child beans whose type is defined by their parent and beans whose definitions are changed after this is run (but before bean instantiation) may produce
	 * unexpected results.
	 *
	 * @param registry the registry which shall be searched for bean definition
	 * @param type     the type of bean definitions to return
	 * @return the list of bean definitions
	 */
	public static List<BeanDefinition> getBeanDefinitionsOfType(BeanDefinitionRegistry registry, Class<?> type) {
		return ArrayUtils.getStream(registry.getBeanDefinitionNames())
				.map(registry::getBeanDefinition)
				.filter(definition -> CompareUtils.isEqual(definition.getBeanClassName(), type.getName()))
				.collect(Collectors.toList());
	}


	/**
	 * Registers a bean definition for the given type in the provided registry.
	 *
	 * @param registry      the registry in which the definition should be added
	 * @param beanType      the type of the bean
	 * @param ctorArgValues the constructor argument values for the bean definition
	 * @return the generated bean definition
	 */
	public static BeanDefinition registerBeanDefinition(BeanDefinitionRegistry registry, Class<?> beanType, Object... ctorArgValues) {
		String beanName = ContextConventionUtils.getBeanNameFromClassName(beanType.getName());
		return registerBeanDefinition(registry, beanName, beanType, ctorArgValues);
	}


	/**
	 * Registers a bean definition for the given type in the provided registry.
	 *
	 * @param registry      the registry in which the definition should be added
	 * @param beanName      the name of the bean to register
	 * @param beanType      the type of the bean
	 * @param ctorArgValues the constructor argument values for the bean definition
	 * @return the generated bean definition
	 */
	public static BeanDefinition registerBeanDefinition(BeanDefinitionRegistry registry, String beanName, Class<?> beanType, Object... ctorArgValues) {
		/*
		 * Matching bean class by name is essential, as the runtime class loader (typically sun.misc.Launcher.AppClassLoader) may vary from the class loader used during context
		 * initialization (e.g., org.springframework.context.support.ContextTypeMatchClassLoader.ContextOverridingClassLoader).
		 */
		BeanDefinitionBuilder bdb = BeanDefinitionBuilder.genericBeanDefinition(beanType.getName())
				.setScope(BeanDefinition.SCOPE_SINGLETON)
				.setAutowireMode(AutowireCapableBeanFactory.AUTOWIRE_BY_NAME);
		for (Object argValue : ctorArgValues) {
			bdb.addConstructorArgValue(argValue);
		}
		AbstractBeanDefinition beanDefinition = bdb.getBeanDefinition();
		registry.registerBeanDefinition(beanName, beanDefinition);
		return beanDefinition;
	}
}
