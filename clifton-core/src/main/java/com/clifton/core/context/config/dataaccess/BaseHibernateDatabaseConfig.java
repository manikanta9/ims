package com.clifton.core.context.config.dataaccess;

import com.clifton.core.cache.CacheHandler;
import com.clifton.core.cache.ehcache.EhCacheHandler;
import com.clifton.core.cache.hibernate.EhCacheRegionFactory;
import com.clifton.core.context.AutowireByNameBean;
import com.clifton.core.context.AutowireByNameBeanPostProcessor;
import com.clifton.core.dataaccess.dao.hibernate.HibernateDAOFactoryPostProcessor;
import com.clifton.core.dataaccess.transactions.CustomAspectJAnnotationTransactionAspect;
import com.clifton.core.dataaccess.transactions.TransactionManagerCache;
import com.clifton.core.dataaccess.transactions.TransactionManagerCacheImpl;
import net.sf.ehcache.CacheManager;
import org.aspectj.lang.Aspects;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.cache.ehcache.EhCacheManagerFactoryBean;
import org.springframework.context.annotation.AdviceMode;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.interceptor.TransactionAspectSupport;

import javax.sql.DataSource;
import java.util.Collections;
import java.util.Map;
import java.util.Objects;
import java.util.Properties;
import java.util.regex.Pattern;


/**
 * @author StevenF
 */
@Configuration
/*
 * Using AspectJ-based weaving here enables transaction management even between methods called by other methods of the same instance. Spring's standard proxying will only apply
 * when calling methods externally.
 */
@EnableTransactionManagement(mode = AdviceMode.ASPECTJ)
public abstract class BaseHibernateDatabaseConfig extends BaseDatabaseConfig {

	@Bean
	@DependsOn({"cacheRegionFactory", "transactionManager"})
	public BeanFactoryPostProcessor daoFactoryPostProcessor() {
		HibernateDAOFactoryPostProcessor daoFactoryPostProcessor = new HibernateDAOFactoryPostProcessor();
		daoFactoryPostProcessor.setSchemaNameList(getProjectNameList());
		return daoFactoryPostProcessor;
	}


	@Bean
	public BeanPostProcessor autowireByNameBeanPostProcessor() {
		return new AutowireByNameBeanPostProcessor();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Bean
	public EhCacheRegionFactory cacheRegionFactory() {
		return new EhCacheRegionFactory();
	}


	@Bean
	public EhCacheManagerFactoryBean cacheManagerFactory() {
		EhCacheManagerFactoryBean cacheManagerFactory = new EhCacheManagerFactoryBean();
		cacheManagerFactory.setShared(true);
		cacheManagerFactory.setConfigLocation(new ClassPathResource("META-INF/schema/clifton-ehcache.xml"));
		cacheManagerFactory.setCacheManagerName("Default Cache Manager");
		return cacheManagerFactory;
	}


	@Bean
	@DependsOn({"cacheManagerFactory"})
	public CacheHandler<String, String> cacheHandler(CacheManager cacheManagerFactory) {
		EhCacheHandler<String, String> cacheHandler = new EhCacheHandler<>();
		cacheHandler.setCacheManager(cacheManagerFactory);
		return cacheHandler;
	}


	@Bean
	@DependsOn({"sessionFactory", "dataSource"})
	@Qualifier("transactionManager")
	public HibernateTransactionManager transactionManager(DataSource dataSource, SessionFactory sessionFactory) {
		HibernateTransactionManager htm = new HibernateTransactionManager();
		htm.setSessionFactory(sessionFactory);
		htm.setDataSource(dataSource);
		return htm;
	}


	@Bean
	@DependsOn({"cacheHandler", "dataSource"})
	public SessionFactory sessionFactory(DataSource dataSource) throws Exception {
		LocalSessionFactoryBean localSessionFactoryBean = new LocalSessionFactoryBean();
		localSessionFactoryBean.setDataSource(dataSource);
		localSessionFactoryBean.setHibernateProperties(getHibernateProperties());
		localSessionFactoryBean.setMappingJarLocations(getMappingJarLocations());
		localSessionFactoryBean.setMappingResources(getMappingResources());
		localSessionFactoryBean.afterPropertiesSet();
		return localSessionFactoryBean.getObject();
	}


	@Bean
	public TransactionAspectSupport transactionAspectSupport() {
		return Aspects.aspectOf(CustomAspectJAnnotationTransactionAspect.class);
	}


	@AutowireByNameBean
	public TransactionManagerCache transactionManagerCache() {
		return new TransactionManagerCacheImpl();
	}


	@Bean
	public Map<Pattern, PlatformTransactionManager> hibernatePackageRegexToTransactionManagerName() {
		return Collections.emptyMap();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	//Default properties that can be overridden or extended.
	public Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", Objects.requireNonNull(getEnvironment().getProperty("hibernate.dialect")));
		properties.put("hibernate.show_sql", Objects.requireNonNull(getEnvironment().getProperty("hibernate.show_sql")));
		properties.put("hibernate.cache.region.factory_class", "SingletonEhcacheRegionFactory");
		properties.put("hibernate.cache.cache_manager_factory_bean_name", "cacheManagerFactory");
		properties.put("hibernate.entity_dirtiness_strategy", "com.clifton.core.dataaccess.dao.hibernate.DirtyCheckingStrategy");
		properties.put("hibernate.ejb.metamodel.population", "disabled"); // Disable JPA
		properties.put("hibernate.cache.ehcache.missing_cache_strategy", "create"); // Disable missing cache log warnings
		return properties;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract String[] getMappingResources();


	public abstract Resource[] getMappingJarLocations();


	public abstract String[] getProjectNameList();
}
