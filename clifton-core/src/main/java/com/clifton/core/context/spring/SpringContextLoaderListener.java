package com.clifton.core.context.spring;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.http.SimpleHttpClientFactory;
import org.springframework.web.context.ContextLoaderListener;

import javax.servlet.ServletContextEvent;
import java.sql.Driver;
import java.sql.DriverManager;
import java.util.Enumeration;


public class SpringContextLoaderListener extends ContextLoaderListener {

	/**
	 * Close the root web application context.
	 */
	@Override
	public void contextDestroyed(ServletContextEvent event) {
		LogUtils.info(SpringContextLoaderListener.class, "Destroying context...");
		//Close out our static httpClient properly
		SimpleHttpClientFactory.close();
		//Unregister our JDBC drivers to prevent memory leaks (this prevents one, but the ThreadTimeout leak is
		//an issue with the driver itself not properly closing and needs to be addressed by the driver.
		unRegisterDrivers();
		super.contextDestroyed(event);
	}


	public static void unRegisterDrivers() {
		LogUtils.info(SpringContextLoaderListener.class, "Destroying context: Unregistering drivers...");
		try {
			for (Enumeration<Driver> drivers = DriverManager.getDrivers(); drivers.hasMoreElements(); ) {
				DriverManager.deregisterDriver(drivers.nextElement());
			}
		}
		catch (Exception e) {
			LogUtils.error(SpringContextLoaderListener.class, "Failed to unregister drivers: " + e.getMessage());
		}
	}
}
