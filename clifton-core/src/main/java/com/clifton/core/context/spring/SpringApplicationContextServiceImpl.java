package com.clifton.core.context.spring;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.converter.json.MapWithRules;
import com.clifton.core.converter.json.MapWithRulesToJsonStringWriter;
import com.clifton.core.converter.json.appenders.MapWithCleanKeyValueAppender;
import com.clifton.core.converter.json.appenders.ObjectUsingFieldsValueAppender;
import com.clifton.core.dataaccess.dao.OneToManyEntity;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.validation.ValidationException;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.ChildBeanDefinition;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.FileSystemResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.core.type.MethodMetadata;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>SpringApplicationContextServiceImpl</code> class provides methods for getting information out of Spring's application context.
 *
 * @author vgomelsky
 */
@Service("applicationContextService")
public class SpringApplicationContextServiceImpl implements ApplicationContextService, ApplicationContextAware {

	private ApplicationContext applicationContext;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
		this.applicationContext = applicationContext;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T> T getContextBean(String name, Class<T> requiredType) {
		return getApplicationContext().getBean(name, requiredType);
	}


	@Override
	public <T> Map<String, T> getContextBeansOfType(Class<T> requiredType) {
		return getApplicationContext().getBeansOfType(requiredType);
	}


	@Override
	public Object getContextBean(String name) {
		return getApplicationContext().getBean(name);
	}


	@Override
	public void autowireBean(Object bean) {
		getApplicationContext().getAutowireCapableBeanFactory().autowireBeanProperties(bean, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
	}


	@Override
	public void populateManySideDependencies(IdentityObject bean) {
		populateManySideDependencies(bean, new HashSet<>());
	}


	private void populateManySideDependencies(IdentityObject bean, Set<IdentityObject> visitedBeans) {
		// avoid circular dependencies with additional sanity check
		if (bean != null && visitedBeans.size() < 1000 && !visitedBeans.contains(bean)) {
			visitedBeans.add(bean);
			Field[] fields = ClassUtils.getClassFields(bean.getClass(), true, true);
			for (Field field : fields) {
				if (AnnotationUtils.isAnnotationPresent(field, OneToManyEntity.class)) {
					// only look up if the list is not already set
					if (BeanUtils.getPropertyValue(bean, field.getName()) == null) {
						OneToManyEntity oneToManyEntityField = AnnotationUtils.getAnnotation(field, OneToManyEntity.class);
						String serviceName = oneToManyEntityField.serviceBeanName();
						if (!StringUtils.isEmpty(serviceName)) {
							String methodName = oneToManyEntityField.serviceMethodName();
							if (!StringUtils.isEmpty(methodName)) {
								Object serviceBean = getContextBean(serviceName);
								if (serviceBean == null) {
									throw new RuntimeException("Cannot find bean with name = '" + serviceName + "' in application context for DTO [" + bean.getClass().getSimpleName() + "].");
								}
								try {
									Method method = getInvocationMethod(serviceBean, methodName);
									@SuppressWarnings("unchecked")
									List<IdentityObject> manySideList = (List<IdentityObject>) method.invoke(serviceBean, bean.getIdentity());
									if (!CollectionUtils.isEmpty(manySideList)) {
										BeanUtils.setPropertyValue(bean, field.getName(), manySideList);
										for (IdentityObject manySideBean : manySideList) {
											populateManySideDependencies(manySideBean, visitedBeans);
										}
									}
								}
								catch (Exception e) {
									throw new ValidationException("Failed to load OneToManyEntity [" + field.getName() + "]. " +
											"Error invoking method [" + methodName + "] for service [" + serviceName + "]", e);
								}
							}
						}
					}
				}
				else if (IdentityObject.class.isAssignableFrom(field.getType())) {
					populateManySideDependencies((IdentityObject) BeanUtils.getPropertyValue(bean, field.getName()), visitedBeans);
				}
			}
		}
	}


	private Method getInvocationMethod(Object serviceBean, String methodName) throws Exception {
		Method method;
		try {
			method = serviceBean.getClass().getMethod(methodName, int.class);
		}
		catch (NoSuchMethodException noIntMethod) {
			try {
				method = serviceBean.getClass().getMethod(methodName, short.class);
			}
			catch (NoSuchMethodException noShortMethod) {
				method = serviceBean.getClass().getMethod(methodName, long.class);
			}
		}
		return method;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<BeanDefinitionDescriptor> getContextBeanDefinitionList() {
		List<BeanDefinitionDescriptor> result = new ArrayList<>();
		DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) getApplicationContext().getAutowireCapableBeanFactory();
		Set<String> beansSet = new HashSet<>();
		Collections.addAll(beansSet, getApplicationContext().getBeanDefinitionNames());
		for (String name : beansSet) {
			BeanDefinition definition = beanFactory.getBeanDefinition(name);
			if (definition instanceof ChildBeanDefinition) {
				definition = beanFactory.getMergedBeanDefinition(name);
			}
			BeanDefinitionDescriptor descriptor = new BeanDefinitionDescriptor();
			descriptor.setBeanName(name);
			descriptor.setBeanClassName(definition.getBeanClassName());
			if (descriptor.getBeanClassName() == null && definition.getParentName() != null) {
				BeanDefinition parentDefinition = beanFactory.getBeanDefinition(AssertUtils.assertNotNull(definition.getParentName(), "Bean definition parent name is null."));
				descriptor.setBeanClassName("Parent Class: " + parentDefinition.getBeanClassName());
			}
			descriptor.setSingleton(definition.isSingleton());
			descriptor.setLazyInit(definition.isLazyInit());
			descriptor.setAutowireCandidate(definition.isAutowireCandidate());
			descriptor.setDefinitionSource(getBeanDefinitionSource(name, definition));
			descriptor.setResourceDescription(definition.getResourceDescription());
			result.add(descriptor);
		}

		// also add beans without a definition
		for (String name : beanFactory.getSingletonNames()) {
			if (!beansSet.contains(name)) {
				Object bean = beanFactory.getSingleton(name);
				BeanDefinitionDescriptor descriptor = new BeanDefinitionDescriptor();
				descriptor.setBeanName(name);
				descriptor.setBeanClassName(bean == null ? null : bean.getClass().getSimpleName());
				descriptor.setSingleton(true);
				descriptor.setWithoutDefinition(true);
				descriptor.setDefinitionSource("(Registered singleton)");
				result.add(descriptor);
			}
		}

		Collections.sort(result);
		return result;
	}


	@Override
	@ResponseBody
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public String getContextBeanDetails(String beanName) {
		Map<String, Object> result = new HashMap<>();
		Object bean = getContextBean(beanName);
		if (bean != null) {
			result.put("data", bean);
			result.put("class", bean.getClass().getName());
		}

		MapWithRules mapWithRules = new MapWithRules(result, 4, true) // without limiting depth, results can get very large
				.withJsonConfigInitializer(jsonConfig -> {
					jsonConfig.setDefaultAppender(new ObjectUsingFieldsValueAppender("ajc$").includeClassProperty()); // exclude properties added by AspectJ
					jsonConfig.registerAppender(Map.class, new MapWithCleanKeyValueAppender()); // filter out dots and spaces from key names
				});
		StringBuilder resultSb = new MapWithRulesToJsonStringWriter().write(new StringBuilder(400), mapWithRules);
		return resultSb.toString();
	}


	@Override
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT)
	public List<UrlDefinitionDescriptor> getContextUrlMappingDefinitionList() {
		List<UrlDefinitionDescriptor> result = new ArrayList<>();
		// NOTE: ideally we want to get this data from ConventionBasedRequestMappingHandlerMapping bean.
		// However, because it's declared in child application context used by DispatcherServlet, it is not visible
		// in the main application context (only one way visibility: children can see parent's beans).
		// So we have to recreate the logic here.

		DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) getApplicationContext().getAutowireCapableBeanFactory();
		for (String beanName : getApplicationContext().getBeanDefinitionNames()) {
			BeanDefinition definition = beanFactory.getBeanDefinition(beanName);
			if (definition instanceof ChildBeanDefinition) {
				definition = beanFactory.getMergedBeanDefinition(beanName);
			}

			String className = definition.getBeanClassName();
			if (!definition.isAbstract() && !StringUtils.isEmpty(className)) {
				Class<?> beanClass = CoreClassUtils.getClass(className);
				// all our services our publicly exposed
				if (AnnotationUtils.isAnnotationPresent(beanClass, Service.class) || AnnotationUtils.isAnnotationPresent(beanClass, Controller.class)) {
					for (Method method : CollectionUtils.getIterable(MethodUtils.getAllDeclaredMethods(beanClass))) {
						// spring MVC style
						if (AnnotationUtils.isAnnotationPresent(method, RequestMapping.class)) {
							RequestMapping mapping = AnnotationUtils.getAnnotation(method, RequestMapping.class);
							for (String url : mapping.value()) {
								if (url.lastIndexOf(".") < 0) {
									url = url + ContextConventionUtils.URL_DEFAULT_EXTENSION;
								}
								if (!url.startsWith("/")) {
									url = "/" + url;
								}
								result.add(new UrlDefinitionDescriptor(url, beanName, className, method));
							}
						}
						// or using our naming convention
						else if (ContextConventionUtils.isUrlFromMethodAllowed(method, beanClass)) {
							String url = ContextConventionUtils.getUrlFromMethod(method);
							if (url != null) {
								result.add(new UrlDefinitionDescriptor(url, beanName, className, method));
							}
						}
					}
				}
			}
		}

		Collections.sort(result);
		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Produces a user-friendly string describing the source for the given bean definition, such as a context XML file, a {@link Bean}-annotated method, or a {@link
	 * Component}-annotated class.
	 */
	private String getBeanDefinitionSource(String beanName, BeanDefinition beanDefinition) {
		String source;
		if (beanDefinition.getSource() instanceof MethodMetadata) {
			MethodMetadata methodMetadataSource = (MethodMetadata) beanDefinition.getSource();
			String sourceClassName = StringUtils.substringAfterLast(methodMetadataSource.getDeclaringClassName(), ".");
			source = "Method: " + sourceClassName + "#" + methodMetadataSource.getMethodName();
		}
		else if (beanDefinition instanceof AbstractBeanDefinition) {
			AbstractBeanDefinition abstractBeanDefinition = (AbstractBeanDefinition) beanDefinition;
			try {
				Resource resource = abstractBeanDefinition.getResource();
				if (resource == null) {
					source = "(Programmatically generated)";
				}
				else if (resource instanceof ClassPathResource) {
					// Generally XML class path resources
					source = "File: " + ((ClassPathResource) resource).getPath();
				}
				else if (resource instanceof UrlResource || resource instanceof FileSystemResource) {
					// UrlResource: Generally classes within JARs
					// FileSystemResource: Generally classes directly on file system
					String filePath = resource.getURI().toString();
					if (!filePath.endsWith(".class")) {
						source = "URI: " + filePath;
					}
					else {
						// Trim long paths; get simple class name
						source = "Class: " + filePath.substring(filePath.lastIndexOf("/") + 1, filePath.lastIndexOf(".class"));
					}
				}
				else {
					source = String.format("Error: Unknown resource type [%s].", resource.getClass().getName());
				}
			}
			catch (Exception e) {
				LogUtils.info(LogCommand.ofThrowableAndMessage(getClass(), e, "Unable to determine bean definition resource for bean [", beanName, "]."));
				source = "Error: " + ExceptionUtils.getOriginalMessage(e);
			}
		}
		else {
			source = String.format("Error: Unrecognized bean definition type [%s].", beanDefinition.getClass().getName());
		}
		return source;
	}
}
