package com.clifton.core.context;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Supplier;


/**
 * The <code>ContextImpl</code> class provides a simple implementation of {@link Context}
 * interface based on {@link ConcurrentHashMap}.
 *
 * @author vgomelsky
 */
public class ContextImpl implements Context {

	private Map<String, Object> beanMap = new ConcurrentHashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextImpl() {
		// used in tests when Context is not applicable
	}


	/**
	 * Create this ContextImpl directly using the provided context.
	 * This is not a clone of the provided context but creates
	 * this context using the provided contexts underlying map
	 */
	public ContextImpl(Context context) {
		if (context != null) {
			this.setBeanMap(context.getBeanMap());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		return (this.beanMap == null) ? "null" : this.beanMap.toString();
	}


	@Override
	public Object getBean(String beanName) {
		return this.beanMap.get(beanName);
	}


	@Override
	public <T> T getOrSupplyBean(String beanName, Supplier<T> beanSupplier) {
		return getOrSupplyBean(beanName, beanSupplier, null);
	}


	@Override
	public <T> T getOrSupplyBean(String beanName, Supplier<T> beanSupplier, Supplier<T> secondaryBeanSupplier) {
		@SuppressWarnings("unchecked")
		T bean = (T) this.beanMap.computeIfAbsent(beanName, key -> {
			T computedBean = beanSupplier.get();
			if (computedBean == null && secondaryBeanSupplier != null) {
				computedBean = secondaryBeanSupplier.get();
			}
			return computedBean;
		});
		return bean;
	}


	@Override
	public void setBean(String beanName, Object bean) {
		this.beanMap.put(beanName, bean);
	}


	@Override
	public Object removeBean(String beanName) {
		return this.beanMap.remove(beanName);
	}


	@Override
	public String[] getBeanNames() {
		java.util.Set<String> names = this.beanMap.keySet();
		return names.toArray(new String[names.size()]);
	}


	@Override
	@ValueIgnoringGetter
	public Map<String, Object> getBeanMap() {
		return new HashMap<>(this.beanMap);
	}


	@Override
	public void setBeanMap(Map<String, Object> beanMap) {
		this.beanMap = beanMap;
	}


	@Override
	public void clear() {
		this.beanMap.clear();
	}
}
