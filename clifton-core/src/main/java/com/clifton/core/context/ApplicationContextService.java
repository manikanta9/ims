package com.clifton.core.context;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.context.spring.BeanDefinitionDescriptor;
import com.clifton.core.context.spring.UrlDefinitionDescriptor;

import java.util.List;
import java.util.Map;


/**
 * The <code>ApplicationContextService</code> interface defines methods for working with application contexts (like Spring's context)
 * Avoid using this unless absolutely necessary.
 *
 * @author vgomelsky
 */
public interface ApplicationContextService {

	/**
	 * Returns an instance of context managed bean with the specified name of the specified required type.
	 */
	@DoNotAddRequestMapping
	public <T> T getContextBean(String name, Class<T> requiredType);


	/**
	 * Returns a map of context managed beans that are assignable from the requiredType
	 */
	@DoNotAddRequestMapping
	public <T> Map<String, T> getContextBeansOfType(Class<T> requiredType);


	/**
	 * Returns an instance of context managed bean with the specified name.
	 */
	@DoNotAddRequestMapping
	public Object getContextBean(String name);


	/**
	 * Auto-wires the bean properties of the specified bean by name. Do not check dependencies.
	 */
	@DoNotAddRequestMapping
	public void autowireBean(Object bean);


	/**
	 * If the specified bean has List properties annotated with {@link com.clifton.core.dataaccess.dao.OneToManyEntity},
	 * use annotation configuration to load corresponding related object lists.
	 */
	@DoNotAddRequestMapping
	public void populateManySideDependencies(IdentityObject bean);


	/**
	 * Returns a List of all context managed bean definitions.
	 * Also includes descriptors for singleton beans registered directly in the context that do not have a definition.
	 */
	public List<BeanDefinitionDescriptor> getContextBeanDefinitionList();


	/**
	 * Returns JSON string representation of the specified context bean.
	 */
	public String getContextBeanDetails(String beanName);


	/**
	 * Returns a List of all URL mapping definitions identified by @RequestMapping.
	 */
	public List<UrlDefinitionDescriptor> getContextUrlMappingDefinitionList();
}
