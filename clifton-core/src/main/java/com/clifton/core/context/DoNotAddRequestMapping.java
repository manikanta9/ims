package com.clifton.core.context;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>DoNotAddRequestMapping</code> marks a method on a service that will not be exposed with RequestMapping annotation
 *
 * @author mwacker
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface DoNotAddRequestMapping {

	/**
	 * Setting this property to true creates a request mapping url for the oauth api only
	 */
	boolean allowAPI() default false;
}
