package com.clifton.core.context.config;

import com.clifton.core.dataaccess.dao.locator.DaoLocator;


/**
 * @author StevenF
 */
public interface AppConfig {

	public DaoLocator daoLocator();
}
