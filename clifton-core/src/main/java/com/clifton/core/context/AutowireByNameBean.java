package com.clifton.core.context;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowire;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The {@link AutowireByNameBean} is a <i><a href="https://github.com/spring-projects/spring-framework/wiki/Spring-Annotation-Programming-Model#meta-annotations">meta-annotation
 * </a></i> for {@link Bean} using {@link Autowire#BY_NAME autowire-by-name}. Bean factory methods (typically those annotated with {@link Bean}) annotated with this will have their
 * fields automatically autowired by name during bean creation.
 * <p>
 * This differs from <code>{@link Bean @Bean}(autowire = {@link Autowire#BY_NAME Autowire.BY_NAME})</code> in that autowiring is performed after standard autowiring but before any
 * initialization callbacks are executed, such as {@link InitializingBean#afterPropertiesSet()}. This allows standard autowiring (such as constructor autowiring) to take place in
 * addition to by-name autowiring.
 * <p>
 * If by-name autowiring isn't needed, then the {@link Bean} annotation on its own is typically preferred over this. By-name autowiring is deprecated in {@link Bean#autowire()
 * Bean} annotations and is generally <a href="https://github.com/spring-projects/spring-framework/issues/21814">discouraged</a> by the Spring platform team in favor of explicit
 * autowiring in all locations.
 *
 * @author MikeH
 * @see AutowireByNameBeanPostProcessor
 */
@Target({ElementType.METHOD, ElementType.ANNOTATION_TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Bean
public @interface AutowireByNameBean {

	@AliasFor(annotation = Bean.class, attribute = "value")
	String[] value() default {};


	@AliasFor(annotation = Bean.class, attribute = "name")
	String[] name() default {};
}
