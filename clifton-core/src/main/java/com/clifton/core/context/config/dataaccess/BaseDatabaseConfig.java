package com.clifton.core.context.config.dataaccess;

import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.env.EnvironmentCapable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;

import javax.annotation.Resource;
import javax.sql.DataSource;


/**
 * @author StevenF
 */
@Configuration
public abstract class BaseDatabaseConfig implements DatabaseConfig<DataSource, JdbcTemplate>, EnvironmentCapable, EnvironmentAware {

	@Resource
	private Environment environment;


	@Bean
	@Override
	public DataSource dataSource() {
		org.apache.tomcat.jdbc.pool.DataSource dataSource = new org.apache.tomcat.jdbc.pool.DataSource();
		dataSource.setUrl(getEnvironment().getProperty("jdbc.url"));
		dataSource.setDriverClassName(getEnvironment().getProperty("jdbc.driverClassName"));
		dataSource.setUsername(getEnvironment().getProperty("jdbc.username"));
		dataSource.setPassword(getEnvironment().getProperty("jdbc.password"));
		return new LazyConnectionDataSourceProxy(dataSource);
	}


	@Bean
	@Override
	public JdbcTemplate jdbcTemplate(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate();
		jdbcTemplate.setDataSource(dataSource);
		return jdbcTemplate;
	}

	////////////////////////////////////////////////////////////////////////////////
	/////////////              Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Environment getEnvironment() {
		return this.environment;
	}


	@Override
	public void setEnvironment(Environment environment) {
		this.environment = environment;
	}
}
