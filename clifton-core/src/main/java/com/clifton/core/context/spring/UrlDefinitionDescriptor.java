package com.clifton.core.context.spring;


import com.clifton.core.util.beans.MethodUtils;

import java.lang.reflect.Method;


/**
 * The <code>UrlDefinitionDescriptor</code> class represents a @RequestMapping definition.
 *
 * @author vgomelsky
 */
public class UrlDefinitionDescriptor implements Comparable<UrlDefinitionDescriptor> {

	private String url;
	private String beanName;
	private String beanClassName;
	private String methodName;
	private String methodSignature;
	private String methodParameters; // includes fully qualified class names
	private String methodParametersShort; // includes simple class names
	private String methodReturnType; // includes fully qualified class name
	private String methodReturnTypeShort;
	private String requestMethod;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public UrlDefinitionDescriptor() {
		super();
	}


	public UrlDefinitionDescriptor(String url, String beanName, String className, Method method) {
		super();
		setUrl(url);
		setBeanName(beanName);
		setBeanClassName(className);
		setMethodName(method.getName());
		setMethodParameters(MethodUtils.getMethodParametersAsString(method, false));
		setMethodParametersShort(MethodUtils.getMethodParametersAsString(method, true));
		setMethodReturnType(method.getReturnType().getName());
		setMethodReturnTypeShort(method.getReturnType().getSimpleName());
		setMethodSignature(method.toGenericString());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (o instanceof UrlDefinitionDescriptor) {
			UrlDefinitionDescriptor d = (UrlDefinitionDescriptor) o;
			return getUrl().equals(d.getUrl());
		}
		return false;
	}


	@Override
	public int compareTo(UrlDefinitionDescriptor o) {
		return getUrl().compareTo(o.getUrl());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanName() {
		return this.beanName;
	}


	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}


	public String getBeanClassName() {
		return this.beanClassName;
	}


	public void setBeanClassName(String beanClassName) {
		this.beanClassName = beanClassName;
	}


	public String getUrl() {
		return this.url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getMethodSignature() {
		return this.methodSignature;
	}


	public void setMethodSignature(String methodSignature) {
		this.methodSignature = methodSignature;
	}


	public String getMethodParameters() {
		return this.methodParameters;
	}


	public void setMethodParameters(String methodParameters) {
		this.methodParameters = methodParameters;
	}


	public String getMethodParametersShort() {
		return this.methodParametersShort;
	}


	public void setMethodParametersShort(String methodParametersShort) {
		this.methodParametersShort = methodParametersShort;
	}


	public String getMethodReturnType() {
		return this.methodReturnType;
	}


	public void setMethodReturnType(String methodReturnType) {
		this.methodReturnType = methodReturnType;
	}


	public String getMethodReturnTypeShort() {
		return this.methodReturnTypeShort;
	}


	public void setMethodReturnTypeShort(String methodReturnTypeShort) {
		this.methodReturnTypeShort = methodReturnTypeShort;
	}


	public String getMethodName() {
		return this.methodName;
	}


	public void setMethodName(String methodName) {
		this.methodName = methodName;
	}


	public String getRequestMethod() {
		return this.requestMethod;
	}


	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}
}
