package com.clifton.core.context;

import org.springframework.beans.factory.config.BeanPostProcessor;


/**
 * The {@link BeanModifier} interface declares methods used to modify existing Spring beans which qualify certain bean name and type criteria. The primary use case for this type is
 * to simplify cases where the application prefers to <i>modify</i> existing beans, such as those defined by the Spring Framework, rather than <i>overriding</i> those beans. This
 * is useful when existing bean definitions provide internal logic which must be retained.
 * <p>
 * The standard Spring way of modifying existing beans is to create {@link BeanPostProcessor} bean definitions with programmatic constraints to isolate the targeted beans. For
 * example, modifying the <code>"resourceHandlerMapping"</code> bean to allow "POST" requests could conventionally be done as follows:
 *
 * <pre><code>{@literal
 * @Bean
 * public BeanPostProcessor resourceHandlerMappingPostMethodPostProcessor() {
 *     return new BeanPostProcessor() {
 *         @Override
 *         public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
 *             if ("resourceHandlerMapping".equals(beanName)) {
 *                 SimpleUrlHandlerMapping simpleUrlHandlerMapping = ((SimpleUrlHandlerMapping) bean);
 *                 WebContentGenerator currentValue = (WebContentGenerator) simpleUrlHandlerMapping.getHandlerMap().get("/**");
 *                 if (currentValue != null) {
 *                     currentValue.setSupportedMethods(ArrayUtils.add(currentValue.getSupportedMethods(), "POST", 0));
 *                 }
 *             }
 *             return bean;
 *         }
 *     };
 * }
 * }</code></pre>
 * <p>
 * The {@link BeanModifier} type simplifies such definitions, providing means of defining the targeted bean name and type explicitly. Targeted bean types can be automatically
 * inferred from the declared {@link BeanModifier} generic parameter, and targeted bean names and types can be added via the {@link BeanModifierTarget} method annotation. The
 * equivalent {@link BeanModifier} for the {@link BeanPostProcessor} above could be written as follows:
 * <pre><code>{@literal
 * @BeanModifierTarget("resourceHandlerMapping")
 * public BeanModifier<SimpleUrlHandlerMapping> resourceHandlerMappingModifier() {
 *     return (bean, beanName) -> {
 *         WebContentGenerator currentValue = (WebContentGenerator) bean.getHandlerMap().get("/**");
 *         if (currentValue != null) {
 *             currentValue.setSupportedMethods(ArrayUtils.add(currentValue.getSupportedMethods(), "POST", 0));
 *         }
 *     };
 * }
 * }</code></pre>
 *
 * @param <T> the type for all beans against which this modifier should apply
 * @author MikeH
 * @see BeanModifierTarget
 * @see BeanModifierBeanPostProcessor
 */
public interface BeanModifier<T> {

	/**
	 * The action to execute on qualifying beans.
	 */
	public abstract void modifyBean(T bean, String beanName) throws Exception;
}
