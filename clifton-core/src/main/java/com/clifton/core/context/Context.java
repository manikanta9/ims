package com.clifton.core.context;


import java.util.Map;
import java.util.function.Supplier;


/**
 * The <code>Context</code> interface defines methods for populating and getting data from Context.
 * Context can be used to store request/session specific information.
 *
 * @author vgomelsky
 */
public interface Context {

	/**
	 * Current user object should be stored under this name.
	 */
	public static final String USER_BEAN_NAME = "USER_BEAN_NAME";

	/**
	 * Key name for open session in view condition. The object value will be a Boolean type.
	 */
	public static final String OPEN_SESSION_IN_VIEW_FLAG_NAME = "OPEN_SESSION_IN_VIEW_FLAG_NAME";

	/**
	 * There maybe times when we want to signal the caller about an event, but because the caller
	 * should not depend on or be aware of processing logic, we cannot return this result, throw
	 * an exception or raise an event. The MultiValueMap stored under this key, can be used to indicate
	 * that something happened. It contains mappings for event name to corresponding event data.
	 */
	public static final String APPLICATION_EVENTS_KEY = "APPLICATION_EVENTS_KEY";

	/**
	 * Key Name used to store the original request parameters in the context; these can be used when
	 * making proxied external service calls so the original parameters may be serialized with the
	 * request and available on the target server.
	 */
	public static final String REQUEST_PARAMETER_MAP = "REQUEST_PARAMETER_MAP";

	/**
	 * Key name used to store the client network address in the context. This can be used to identify
	 * the client machine to authorize Bloomberg users or push data.
	 */
	public static final String REQUEST_CLIENT_ADDRESS = "REQUEST_CLIENT_ADDRESS";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the bean stored under the specified name or null if not found.
	 */
	public Object getBean(String beanName);


	/**
	 * Returns the bean stored under the specified name.
	 * <p>
	 * If no bean is found, the <tt>beanSupplier</tt> will be used to compute the bean. If the supplier generates a non-null bean, then the resulting bean will be stored in the
	 * context under the provided name.
	 *
	 * @param beanName     the name of the bean to retrieve
	 * @param beanSupplier the bean-generator whose value to use if the bean is not present in the context
	 * @param <T>          the type of the bean
	 * @return the retrieved or generated bean
	 */
	public <T> T getOrSupplyBean(String beanName, Supplier<T> beanSupplier);


	/**
	 * Returns the bean stored under the specified name.
	 * <p>
	 * If no bean is found, the <tt>beanSupplier</tt> will be used to compute the bean. The suppliers will be executed in order until a bean is generated or the list of suppliers
	 * is exhausted. If one of the suppliers generates a non-null bean, then the resulting bean will be stored in the context under the provided name.
	 * <p>
	 * Supplier-coalescing can be used to supply default values.
	 *
	 * @param beanName              the name of the bean to retrieve
	 * @param beanSupplier          the bean-generator whose value to use if the bean is not present in the context
	 * @param secondaryBeanSupplier the bean-generator whose value to use if the <tt>beanSupplier</tt> provides no value
	 * @param <T>                   the type of the bean
	 * @return the retrieved or generated bean
	 */
	public <T> T getOrSupplyBean(String beanName, Supplier<T> beanSupplier, Supplier<T> secondaryBeanSupplier);


	/**
	 * Stores the specified bean under the specified name.
	 */
	public void setBean(String beanName, Object bean);


	/**
	 * Removes the bean with the specified name.  Returns removed bean.
	 */
	public Object removeBean(String beanName);


	/**
	 * Returns an array of bean names for all beans stored in context.
	 */
	public String[] getBeanNames();


	/**
	 * Returns the bean map of all beans stored in the context
	 */
	public Map<String, Object> getBeanMap();


	/**
	 * Sets the bean map
	 */
	public void setBeanMap(Map<String, Object> beanMap);


	/**
	 * Clears this context (removes all beans).
	 */
	public void clear();
}
