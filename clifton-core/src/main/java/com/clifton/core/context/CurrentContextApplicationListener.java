package com.clifton.core.context;

import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ApplicationContextEvent;


/**
 * The <code>CurrentContextApplicationListener</code> interface defines a method for determining if the applicationContext
 * of a given event matches the applicationContext of the implementing class.
 * <p>
 * Classes that only implement <code>ApplicationListener</code> will handle events in all contexts in the current context hierarchy,
 * which may result in the onApplicationEvent logic executing multiple times.
 * <p>
 * This interface allows a class to ensure that logic defined in onCurrentContextApplicationEvent is only executed for the context
 * in which the class was defined. While the onApplicationEvent method will still be called for events in all contexts, we have added
 * a check to only run onCurrentContextApplicationEvent if the event context equals that of the implementing class, which would generally be
 * set using the <code>ApplicationContextAware</code> interface.
 * <p>
 * An example need for this interface is to initialize static properties on start-up, as in the <code>LogUtils</code> class.
 *
 * @author lnaylor
 */
public interface CurrentContextApplicationListener<E extends ApplicationContextEvent> extends ApplicationListener<E>, ApplicationContextAware {

	/**
	 * Defines logic that should be run when the applicationContext of the given event matches the applicationContext
	 * of the implementing class.
	 *
	 * @param event
	 */
	public void onCurrentContextApplicationEvent(E event);


	public ApplicationContext getApplicationContext();


	@Override
	default void onApplicationEvent(E event) {
		if (event.getApplicationContext().equals(getApplicationContext())) {
			onCurrentContextApplicationEvent(event);
		}
	}
}
