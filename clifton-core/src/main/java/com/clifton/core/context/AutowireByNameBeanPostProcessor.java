package com.clifton.core.context;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AnnotationUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.BeanInitializationException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.RootBeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;


/**
 * The {@link AutowireByNameBeanPostProcessor} {@link BeanPostProcessor} injects field values by name for all Spring beans whose factory methods (typically {@link Bean} methods)
 * are annotated with {@link AutowireByNameBean}.
 * <p>
 * see {@link AutowireByNameBean} for more information.
 *
 * @author MikeH
 * @see AutowireByNameBean
 */
@Component
public class AutowireByNameBeanPostProcessor implements BeanPostProcessor, BeanFactoryAware {

	private ConfigurableListableBeanFactory beanFactory;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	// Post-processing must occur before initialization so that properties are set before execution of initialization callbacks such as InitializingBean#afterPropertiesSet
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		try {
			if (getBeanFactory().containsBeanDefinition(beanName)) {
				RootBeanDefinition beanDefinition = (RootBeanDefinition) getBeanFactory().getMergedBeanDefinition(beanName);
				if (beanDefinition.getFactoryMethodName() != null) {
					if (beanDefinition.getResolvedFactoryMethod() == null) {
						LogUtils.error(LogCommand.ofMessage(getClass(), "Unable to find a resolved factory method for bean [", beanName, "]. Expected [", beanDefinition.getFactoryBeanName(), "#", beanDefinition.getFactoryMethodName(), "]. This bean may not have been resolved yet. Autowire-by-name post-processing will not be attempted."));
					}
					else {
						Method factoryMethod = beanDefinition.getResolvedFactoryMethod();
						AutowireByNameBean autowireByNameAnnotation = AnnotationUtils.getAnnotation(factoryMethod, AutowireByNameBean.class);
						if (autowireByNameAnnotation != null) {
							getBeanFactory().autowireBeanProperties(bean, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
						}
					}
				}
			}
		}
		catch (Exception e) {
			throw new BeanInitializationException(String.format("An error occurred while executing autowire-by-name post-processing for bean [%s].", beanName), e);
		}
		return bean;
	}


	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		this.beanFactory = (ConfigurableListableBeanFactory) beanFactory;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ConfigurableListableBeanFactory getBeanFactory() {
		return this.beanFactory;
	}


	public void setBeanFactory(ConfigurableListableBeanFactory beanFactory) {
		this.beanFactory = beanFactory;
	}
}
