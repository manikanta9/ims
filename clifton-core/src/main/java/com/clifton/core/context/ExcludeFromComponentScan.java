package com.clifton.core.context;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>ExcludeFromComponentScan</code> used to exclude class from a component scan.  You
 * can use this to exclude items annotated with @Controller, @Service or @Component from being
 * discovered on the class path.  This is useful when you have multiple implementations of a service
 * and want to control which one is added to the context.
 * <p/>
 * To enable this edit your context:component-scan to something like:
 * <p/>
 * <context:component-scan base-package="com.clifton.portal" name-generator="com.clifton.core.context.spring.AnnotationUsingOurConventionBeanNameGenerator">
 * <context:exclude-filter type="annotation" expression="com.clifton.core.context.ExcludeFromComponentScan"/>
 * </context:component-scan>
 *
 * @author mwacker
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface ExcludeFromComponentScan {
	//
}
