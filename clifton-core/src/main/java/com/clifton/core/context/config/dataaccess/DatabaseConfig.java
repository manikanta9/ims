package com.clifton.core.context.config.dataaccess;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;


/**
 * @author StevenF
 */
@Configuration
public interface DatabaseConfig<D extends DataSource, T extends JdbcTemplate> {

	@Bean
	public D dataSource();


	@Bean
	public T jdbcTemplate(DataSource dataSource);
}
