package com.clifton.core.context;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanPostProcessor;


/**
 * The <code>AfterInitializationBeanPostProcessor</code> type is a convenience {@link BeanPostProcessor} SAM type which exposes only the {@link
 * #postProcessAfterInitialization(Object, String)} interface method.
 *
 * @author MikeH
 */
@FunctionalInterface
public interface AfterInitializationBeanPostProcessor extends BeanPostProcessor {

	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException;
}
