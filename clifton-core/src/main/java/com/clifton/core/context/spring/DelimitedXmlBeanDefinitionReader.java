package com.clifton.core.context.spring;

import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.xml.XmlBeanDefinitionReader;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

import java.util.Arrays;


/**
 * The {@link DelimitedXmlBeanDefinitionReader} loads beans from each resource in the provided location string, where resources are delimited by {@value #DELIMITER}.
 * <p>
 * This is intended to be used in {@link ImportResource} annotations, typically on {@link Configuration} classes. This allows resource imports to use property substitution to
 * substitute any number of resources to import.
 * <p>
 * Example:
 * <pre><code>{@literal
 * @ImportResource(
 *     value = "classpath:/META-INF/spring/app-clifton-reconciliation-context.xml;${reconciliation.security.config.files}",
 *     reader = DelimitedXmlBeanDefinitionReader.class
 * )}</code></pre>
 *
 * @author jacke
 */
public class DelimitedXmlBeanDefinitionReader extends XmlBeanDefinitionReader {

	private static final String DELIMITER = ";";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DelimitedXmlBeanDefinitionReader(BeanDefinitionRegistry registry) {
		super(registry);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public int loadBeanDefinitions(String locationStr) {
		return Arrays.stream(locationStr.split(DELIMITER))
				.mapToInt(location -> loadBeanDefinitions(location, null))
				.sum();
	}
}
