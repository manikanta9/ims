package com.clifton.core.context;


import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.AliasFor;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The {@link BeanModifierTarget} annotation is used to designate targeted {@link #beanName() bean names} and {@link #beanType() bean types} for use with {@link BeanModifier}.
 * <p>
 * This is a meta-annotation for {@link Bean}. This annotation has no other impact if no properties are set or if the annotated method does not produce a {@link BeanModifier}.
 *
 * @author MikeH
 * @see BeanModifier
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Bean
@Documented
public @interface BeanModifierTarget {

	/**
	 * @see #beanName()
	 */
	@AliasFor(attribute = "beanName")
	String[] value() default {};


	/**
	 * The targeted bean names. The annotated {@link BeanModifier} will only be applied to beans matching any one of these names.
	 */
	@AliasFor(attribute = "value")
	String[] beanName() default {};


	/**
	 * The targeted bean types. The annotated {@link BeanModifier} will only be applied to beans matching any one of these types.
	 */
	Class<?>[] beanType() default {};
}
