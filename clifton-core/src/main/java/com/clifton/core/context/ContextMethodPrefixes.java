package com.clifton.core.context;


import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.MathUtils;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Map;


/**
 * The <code>ContextMethodPrefixes</code> enum defines standard method name prefixes
 * and corresponding format and security permission.
 *
 * @author manderson
 */
public enum ContextMethodPrefixes {

	//TODO - Everything maps to POST right now - if we move to true REST, then this will change.
	ARCHIVE("archive", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST),
	RESTORE("restore", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST),

	APPROVE("approve", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	UNAPPROVE("unapprove", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //

	CLEAR("clear", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	COPY("copy", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	CREATE("create", SecurityPermission.PERMISSION_CREATE, true, RequestMethod.POST), //

	DELETE("delete", SecurityPermission.PERMISSION_DELETE, true, RequestMethod.POST), //
	DOWNLOAD("download", SecurityPermission.PERMISSION_READ, true, RequestMethod.POST), //

	FIX("fix", SecurityPermission.PERMISSION_FULL_CONTROL, true, RequestMethod.POST), //

	GENERATE("generate", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	GET("get", SecurityPermission.PERMISSION_READ, false, RequestMethod.POST), //

	IMPORT("import", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	IS("is", SecurityPermission.PERMISSION_READ, false, RequestMethod.POST), //

	LINK("link", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	UNLINK("unlink", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	LOCK("lock", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //

	MOVE("move", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //

	POST("post", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	PREVIEW("preview", SecurityPermission.PERMISSION_READ, true, RequestMethod.POST), //
	PROCESS("process", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST),

	REBUILD("rebuild", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	RUN("run", SecurityPermission.PERMISSION_EXECUTE, true, RequestMethod.POST), //

	SAVE("save", SecurityPermission.PERMISSION_CREATE, true, RequestMethod.POST) {
		/** Special Case for Saves - if it's a new bean (no id present) that we are saving, then return CREATE Permission **/
		@Override
		public int calculateSecurityPermission(Map<String, ?> parameterMap) {
			if (!CollectionUtils.isEmpty(parameterMap)) {
				Long id = MapUtils.getParameterAsLong("id", parameterMap);
				if (id != null && MathUtils.isGreaterThan(id, 0)) {
					return SecurityPermission.PERMISSION_WRITE; // WRITE ACCESS
				}
			}
			return getSecurityPermission(); // CREATE ACCESS
		}
	}, //
	SET("set", SecurityPermission.PERMISSION_WRITE, false, RequestMethod.POST), //

	UNLOCK("unlock", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	UPDATE("update", SecurityPermission.PERMISSION_WRITE, true, RequestMethod.POST), //
	UPLOAD("upload", SecurityPermission.PERMISSION_CREATE, true, RequestMethod.POST), //

	VALIDATE("validate", SecurityPermission.PERMISSION_READ, true, RequestMethod.POST); //

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ContextMethodPrefixes(String prefix, int securityPermission, boolean appendPrefixAsUrlSuffix, RequestMethod requestMethod) {
		this.prefix = prefix;
		this.securityPermission = securityPermission;
		this.appendPrefixAsUrlSuffix = appendPrefixAsUrlSuffix;
		this.requestMethod = requestMethod;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final String prefix;

	private final int securityPermission;

	private final boolean appendPrefixAsUrlSuffix;

	private final RequestMethod requestMethod;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int calculateSecurityPermission(Map<String, ?> parameterMap) {
		return getSecurityPermission();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getPrefix() {
		return this.prefix;
	}


	protected int getSecurityPermission() {
		return this.securityPermission;
	}


	public boolean isAppendPrefixAsUrlSuffix() {
		return this.appendPrefixAsUrlSuffix;
	}


	public RequestMethod getRequestMethod() {
		return this.requestMethod != null ? this.requestMethod : RequestMethod.POST;
	}
}
