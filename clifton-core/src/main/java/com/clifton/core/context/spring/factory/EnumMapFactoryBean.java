package com.clifton.core.context.spring.factory;

import com.clifton.core.util.AssertUtils;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.AbstractFactoryBean;

import java.util.EnumMap;
import java.util.Map;


/**
 * A simple {@link FactoryBean} for generating {@link EnumMap} objects.
 *
 * @author MikeH
 */
public class EnumMapFactoryBean<K extends Enum<K>> extends AbstractFactoryBean<EnumMap<K, Object>> {

	/**
	 * The key class for the {@link EnumMap} to be generated.
	 *
	 * @see EnumMap#keyType
	 */
	private Class<K> keyClass;
	/**
	 * The source map which will be used to populate the generated map.
	 */
	private Map<K, ?> sourceMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("rawtypes")
	public Class<EnumMap> getObjectType() {
		return EnumMap.class;
	}


	@Override
	protected EnumMap<K, Object> createInstance() {
		AssertUtils.assertNotNull(this.sourceMap, "'sourceMap' is required");
		AssertUtils.assertNotNull(this.keyClass, "'keyClass' is required");
		EnumMap<K, Object> result = new EnumMap<>(this.keyClass);
		result.putAll(this.sourceMap);
		return result;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setKeyClass(Class<K> keyClass) {
		this.keyClass = keyClass;
	}


	public void setSourceMap(Map<K, ?> sourceMap) {
		this.sourceMap = sourceMap;
	}
}
