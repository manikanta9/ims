package com.clifton.core.context.spring;

import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ConfigurationClassPostProcessor;
import org.springframework.context.support.AbstractXmlApplicationContext;
import org.springframework.context.weaving.AspectJWeavingEnabler;


/**
 * The {@link SpringLoadTimeWeavingBeanDefinitionRegistryPostProcessor} {@link BeanDefinitionRegistryPostProcessor} hooks into the bean factory post-processing phase to enable
 * class-loader weaving earlier than usual. As of Spring Framework 5.2, this is necessary to allow runtime weaving of all beans which are defined early on in the bean factory
 * post-processor phase. Without this, all early-registered beans, including those defined via XML contexts, {@link Configuration} classes (both full and lite), or component scans,
 * will have their classes loaded too early and will not be subject to runtime weaving.
 * <p>
 * <h2>Background</h2>
 * The need for this class stems from a change to the {@link ConfigurationClassPostProcessor} class which causes all bean definitions to have their types resolved by the
 * standard context class loader early on in the bean factory post-processor phase (<a
 * href="https://github.com/spring-projects/spring-framework/commit/40c62139ae6f8faa09fc0047ebc8ec65c5a2e809?diff=split#diff-69d29ef31d8e2982897a09a37a0adb4eL374">source
 * commit</a>). This normally occurs during the {@link BeanDefinitionRegistryPostProcessor} sub-phase, which takes place before the {@link BeanFactoryPostProcessor} sub-phase.
 * Weaving is enabled by {@link AspectJWeavingEnabler}, which occurs during the {@link BeanFactoryPostProcessor} sub-phase. This class effectively promotes the priority of {@link
 * AspectJWeavingEnabler}, causing it to be executed just before the class-loading portion of {@link ConfigurationClassPostProcessor}.
 * <p>
 * Standard context weaving-enablement process:
 * <ol>
 * <li>Create and prepare context
 * <li>Parse XML files (for {@link AbstractXmlApplicationContext} contexts) and register all beans, including those discovered through {@code <context:component-scan>} elements
 * <li>Register the {@link AspectJWeavingEnabler} {@link BeanFactoryPostProcessor} bean definition when the {@code <context:load-time-weaver>} element is parsed (via {@link
 * org.springframework.context.config.LoadTimeWeaverBeanDefinitionParser LoadTimeWeaverBeanDefinitionParser})
 * <li>Run all {@link BeanDefinitionRegistryPostProcessor#postProcessBeanDefinitionRegistry(BeanDefinitionRegistry)} methods
 * <ul>
 * <li>At this point, the {@link ConfigurationClassPostProcessor} will attempt to discover all bean definitions defined by JavaConfigs and will load the necessary types to do so,
 * such as classes and annotations used for component scans, but will not typically load bean classes.
 * </ul>
 * <li>Run all {@link BeanDefinitionRegistryPostProcessor#postProcessBeanFactory(ConfigurableListableBeanFactory)} methods
 * <ul>
 * <li>At this point, the {@link ConfigurationClassPostProcessor} post-processor is run. As of Spring Framework 5.2, this loads the classes for all registered beans (<a href=
 * "https://github.com/spring-projects/spring-framework/commit/40c62139ae6f8faa09fc0047ebc8ec65c5a2e809?diff=split#diff-69d29ef31d8e2982897a09a37a0adb4eL374">source commit</a>).
 * </ul>
 * <li>Run all {@link BeanFactoryPostProcessor} post-processors, including {@link AspectJWeavingEnabler}, enabling class weaving
 * </ol>
 * <p>
 * Altered process:
 * <ol>
 * <li>Create and prepare context
 * <li>Parse XML files (for {@link AbstractXmlApplicationContext} contexts) and register all beans, including those discovered through {@code <context:component-scan>} elements
 * <li>Register the {@link AspectJWeavingEnabler} {@link BeanFactoryPostProcessor} bean definition when the {@code <context:load-time-weaver>} element is parsed
 * <li>Run higher-priority {@link BeanDefinitionRegistryPostProcessor#postProcessBeanDefinitionRegistry(BeanDefinitionRegistry)} methods
 * <li>Run the {@link SpringLoadTimeWeavingBeanDefinitionRegistryPostProcessor}, executing {@link AspectJWeavingEnabler}, enabling class weaving, and removing the weaving-enabler
 * <li>Run the {@link ConfigurationClassPostProcessor} and any remaining {@link BeanDefinitionRegistryPostProcessor} post-processors, loading classes for all registered beans
 * <li>Run all {@link BeanDefinitionRegistryPostProcessor#postProcessBeanFactory(ConfigurableListableBeanFactory)} methods
 * <li>Run all {@link BeanFactoryPostProcessor} post-processors
 * </ol>
 *
 * @author MikeH
 * @see SpringLoadTimeWeavingConfiguration
 */
public class SpringLoadTimeWeavingBeanDefinitionRegistryPostProcessor implements BeanDefinitionRegistryPostProcessor {

	/*
	 * Ordering this type (e.g., via the Ordered interface) does not typically significantly alter behavior. This type is loaded by the ConfigurationClassPostProcessor's
	 * postProcessBeanDefinitionRegistry method. The important part is that the behavior for this type is included in its own postProcessBeanDefinitionRegistry method, as all such
	 * methods are executed before any postProcessBeanFactory methods, and the ConfigurationClassPostProcessor's offending method is its own postProcessBeanFactory method. This
	 * type cannot be otherwise ordered to precede ConfigurationClassPostProcessor execution when it is loaded by the ConfigurationClassPostProcessor.
	 */


	@Override
	public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
		DefaultListableBeanFactory beanFactory = (DefaultListableBeanFactory) registry;
		String[] weavingEnablerBeanNames = beanFactory.getBeanNamesForType(AspectJWeavingEnabler.class, true, false);
		if (!ArrayUtils.isEmpty(weavingEnablerBeanNames)) {
			// Initiate weaving
			AssertUtils.assertFalse(weavingEnablerBeanNames.length > 1, () -> String.format("Multiple ([%d]) weaving enabler beans found. Only a single weaving enabler is expected. Weaving enabler bean names: [%s].", weavingEnablerBeanNames.length, ArrayUtils.toString(weavingEnablerBeanNames)));
			String weavingEnablerBeanName = weavingEnablerBeanNames[0];
			AspectJWeavingEnabler weavingEnabler = beanFactory.getBean(weavingEnablerBeanName, AspectJWeavingEnabler.class);
			weavingEnabler.postProcessBeanFactory(beanFactory);
			// Remove bean to prevent duplicate transformer registration
			beanFactory.removeBeanDefinition(weavingEnablerBeanName);
		}
	}


	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		// Do nothing
	}
}
