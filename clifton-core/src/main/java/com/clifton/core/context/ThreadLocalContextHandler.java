package com.clifton.core.context;


import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.function.Supplier;


/**
 * The <code>ThreadLocalContextHandler</code> class provides a simple implementation of {@link ContextHandler}
 * interface based on {@link ThreadLocal}.
 *
 * @author vgomelsky
 */
@Component("contextHandler")
public class ThreadLocalContextHandler implements ContextHandler {

	private final ThreadLocal<Context> context = new ThreadLocal<>();


	@Override
	public Object getBean(String beanName) {
		Context ctx = this.context.get();
		if (ctx == null) {
			return null;
		}
		return ctx.getBean(beanName);
	}


	@Override
	public <T> T getOrSupplyBean(String beanName, Supplier<T> beanSupplier) {
		return getOrSupplyBean(beanName, beanSupplier, null);
	}


	@Override
	public <T> T getOrSupplyBean(String beanName, Supplier<T> beanSupplier, Supplier<T> secondaryBeanSupplier) {
		@SuppressWarnings("unchecked")
		T bean = (T) getBean(beanName);
		if (bean == null) {
			bean = beanSupplier.get();
			if (bean == null && secondaryBeanSupplier != null) {
				bean = secondaryBeanSupplier.get();
			}
			if (bean != null) {
				setBean(beanName, bean);
			}
		}
		return bean;
	}


	@Override
	public void setBean(String beanName, Object bean) {
		Context ctx = this.context.get();
		if (ctx == null) {
			ctx = new ContextImpl();
			this.context.set(ctx);
		}
		ctx.setBean(beanName, bean);
	}


	@Override
	public void setBeanMap(Map<String, Object> beanMap) {
		Context ctx = this.context.get();
		if (ctx == null) {
			ctx = new ContextImpl();
			this.context.set(ctx);
		}
		ctx.setBeanMap(beanMap);
	}


	@Override
	public Object removeBean(String beanName) {
		Context ctx = this.context.get();
		if (ctx == null) {
			return null;
		}
		return ctx.removeBean(beanName);
	}


	@Override
	public void clear() {
		Context ctx = this.context.get();
		if (ctx != null) {
			ctx.clear();
		}
		this.context.remove();
	}


	@Override
	public String[] getBeanNames() {
		Context ctx = this.context.get();
		if (ctx == null) {
			return null;
		}
		return ctx.getBeanNames();
	}


	@Override
	@ValueIgnoringGetter
	public Map<String, Object> getBeanMap() {
		Context ctx = this.context.get();
		if (ctx == null) {
			return null;
		}
		return ctx.getBeanMap();
	}
}
