package com.clifton.core.context.spring;

import com.clifton.core.context.ExcludeFromComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableLoadTimeWeaving;


/**
 * The {@link SpringLoadTimeWeavingConfiguration} is a {@link Configuration} class strictly for enabling load-time weaving. This is intended to be used as a parent Spring context
 * in order to enable load-time weaving before classloading is performed in a child context.
 * <p>
 * This is used primarily to address the effect of the {@link EnableLoadTimeWeaving} annotation is not applied early enough for weaving to be performed on classes used in
 * {@link Configuration}-annotated classes.
 * <p>
 * In order to use this configuration class, it should be created as the parent context for the child context whose entities need to be woven:
 * <pre><code>
 * ApplicationContext rootContext = new AnnotationConfigApplicationContext(SpringLoadTimeWeavingConfiguration.class);
 * GenericApplicationContext childContext = new AnnotationConfigApplicationContext();
 * childContext.setParent(rootContext);
 * childContext.registerBean(InMemoryDatabaseDataExportConfig.class);
 * childContext.refresh();
 * </code></pre>
 *
 * <h2>Background</h2>
 * There are some subtle nuances between the effect of the {@link org.springframework.context.annotation.EnableLoadTimeWeaving EnableLoadTimeWeaving} annotation and the
 * {@link org.springframework.context.config.LoadTimeWeaverBeanDefinitionParser &lt;context:load-time-weaver&gt;} XML directive. The <code>EnableLoadTimeWeaving</code> annotation
 * results in an import of the {@link org.springframework.context.annotation.LoadTimeWeavingConfiguration LoadTimeWeavingConfiguration} class, while the
 * <code>&lt;context:load-time-weaver&gt;</code> directive is read by the {@link org.springframework.context.config.LoadTimeWeaverBeanDefinitionParser
 * LoadTimeWeaverBeanDefinitionParser}. Both of these methods of enabling load-time weaving revolve around registering a {@value
 * org.springframework.context.ConfigurableApplicationContext#LOAD_TIME_WEAVER_BEAN_NAME} bean and the {@link org.springframework.context.weaving.AspectJWeavingEnabler
 * AspectJWeavingEnabler} {@link org.springframework.beans.factory.config.BeanFactoryPostProcessor BeanFactoryPostProcessor} bean.
 * <p>
 * While the resulting effects of these methods are mostly the same, the <i>order</i> in which this execution occurs is different. The key difference is that the annotation-driven
 * configuration does <i>not</i> register the <code>BeanFactoryPostProcessor</code> until <i>after</i> the Spring context bean factory post processor phase is completed. By this
 * time, the <code>@Configuration</code>-annotated classes will have been loaded and their declared methods read, resulting in all referenced classes (both return types and
 * parameter types) being loaded into the current classloader. All of these classes will have been loaded too early for weaving to have taken place on them.
 * <p>
 * In contrast, the XML-driven configuration registers the <code>BeanFactoryPostProcessor</code> immediately during parsing of the XML file itself. This allows for a broader
 * weaving scope to be applied.
 * <p>
 * See <a href="http://blog.kezhuw.name/2017/08/31/spring-aspectj-load-time-weaving/#Custom-class-loader-to-apply-class-file-transformation">AspectJ Load-Time Weaving for Spring:
 * Custom class loader to apply class file transformation</a> for more information on this approach.
 *
 * @author MikeH
 */
@Configuration
@EnableLoadTimeWeaving
@ExcludeFromComponentScan
public class SpringLoadTimeWeavingConfiguration {
}
