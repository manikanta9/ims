package com.clifton.core.context.spring;


import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;


/**
 * The BeanDefinitionDescriptor contains information describing an application context managed bean definition.
 */
public class BeanDefinitionDescriptor implements Comparable<BeanDefinitionDescriptor> {

	private String beanName;
	private String beanClassName;
	/**
	 * The source from which this bean definition originated. This is often a context XML file, a class (e.g., via the {@link Component} annotation), or a method (often via the
	 * {@link Bean} annotation).
	 */
	private String definitionSource;
	/**
	 * The description of the bean definition resource, as per {@link BeanDefinition#getResourceDescription()}.
	 */
	private String resourceDescription;
	private boolean singleton;
	private boolean lazyInit;
	private boolean autowireCandidate;
	/**
	 * In rare cases, a Spring managed bean can be registered within the context directly and now have a definition.
	 */
	private boolean withoutDefinition;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean equals(Object o) {
		if (o instanceof BeanDefinitionDescriptor) {
			BeanDefinitionDescriptor d = (BeanDefinitionDescriptor) o;
			return getBeanName().equals(d.getBeanName());
		}
		return false;
	}


	@Override
	public int compareTo(BeanDefinitionDescriptor o) {
		return getBeanName().compareTo(o.getBeanName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanName() {
		return this.beanName;
	}


	public void setBeanName(String beanName) {
		this.beanName = beanName;
	}


	public String getBeanClassName() {
		return this.beanClassName;
	}


	public void setBeanClassName(String beanClassName) {
		this.beanClassName = beanClassName;
	}


	public String getDefinitionSource() {
		return this.definitionSource;
	}


	public void setDefinitionSource(String definitionSource) {
		this.definitionSource = definitionSource;
	}


	public String getResourceDescription() {
		return this.resourceDescription;
	}


	public void setResourceDescription(String resourceDescription) {
		this.resourceDescription = resourceDescription;
	}


	public boolean isSingleton() {
		return this.singleton;
	}


	public void setSingleton(boolean singleton) {
		this.singleton = singleton;
	}


	public boolean isLazyInit() {
		return this.lazyInit;
	}


	public void setLazyInit(boolean lazyInit) {
		this.lazyInit = lazyInit;
	}


	public boolean isAutowireCandidate() {
		return this.autowireCandidate;
	}


	public void setAutowireCandidate(boolean autowireCandidate) {
		this.autowireCandidate = autowireCandidate;
	}


	public boolean isWithoutDefinition() {
		return this.withoutDefinition;
	}


	public void setWithoutDefinition(boolean withoutDefinition) {
		this.withoutDefinition = withoutDefinition;
	}
}
