package com.clifton.core.context.spring;


import com.clifton.core.context.ContextConventionUtils;
import org.springframework.beans.factory.config.BeanDefinition;


/**
 * The <code>AnnotationUsingOurConventionBeanNameGenerator</code> class for beans that uses default names changes "ServiceImpl" suffix to "Service".
 *
 * @author vgomelsky
 */
public class AnnotationUsingOurConventionBeanNameGenerator extends org.springframework.context.annotation.AnnotationBeanNameGenerator {

	@Override
	protected String buildDefaultBeanName(BeanDefinition definition) {
		return ContextConventionUtils.getBeanNameFromClassName(definition.getBeanClassName());
	}
}
