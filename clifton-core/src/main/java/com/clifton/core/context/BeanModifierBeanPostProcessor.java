package com.clifton.core.context;

import com.clifton.core.util.AnnotationUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.BeanFactoryAware;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanPostProcessor;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.core.MethodParameter;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The {@link BeanModifierBeanPostProcessor} is used to apply all registered {@link BeanModifier} beans during the {@link BeanPostProcessor} phase. This applies post-initialization
 * bean customization logic to beans which are already registered, such as those registered by the Spring Framework, when we prefer to customize rather than override.
 * <p>
 * All modifiers are evaluated during {@link #postProcessAfterInitialization(Object, String)} calls. This class provides no guarantee that all {@link BeanModifier} beans are
 * executed; if no qualifying beans are initialized, then a modifier may never be triggered.
 *
 * @author MikeH
 * @see BeanModifier
 */
@Component
public class BeanModifierBeanPostProcessor implements BeanPostProcessor, BeanFactoryAware, InitializingBean {

	private DefaultListableBeanFactory defaultListableBeanFactory;

	/**
	 * The list of {@link BeanModifier} bean names. These are stored as bean names so that they can be lazily initialized, preventing eager initialization which would prevent this
	 * {@link BeanPostProcessor} from impacting beans depended on by {@link BeanModifier} instances.
	 */
	private List<String> beanModifierBeanNames = new ArrayList<>();
	/**
	 * The cache of {@link BeanModifier} instances to their matching bean name designations. This aims to reduce the performance impact of targeted bean name inference.
	 */
	private final Map<String, List<String>> beanModifierNameToBeanNameListCache = new HashMap<>();
	/**
	 * The cache of {@link BeanModifier} instances to their matching bean class designations. This aims to reduce the performance impact of targeted bean type inference.
	 */
	private final Map<String, List<Class<?>>> beanModifierNameToBeanClassListCache = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) {
		executePostProcessors(bean, beanName);
		return bean;
	}


	@Override
	public void afterPropertiesSet() {
		setBeanModifierBeanNames(Arrays.asList(getDefaultListableBeanFactory().getBeanNamesForType(BeanModifier.class, true, false)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T> void executePostProcessors(T bean, String beanName) {
		for (String modifierBeanName : getBeanModifierBeanNames()) {
			if (isBeanModifierMatch(modifierBeanName, bean, beanName)) {
				try {
					@SuppressWarnings("unchecked")
					BeanModifier<T> typedPostProcessor = (BeanModifier<T>) getDefaultListableBeanFactory().getBean(modifierBeanName, BeanModifier.class);
					typedPostProcessor.modifyBean(bean, beanName);
				}
				catch (Exception e) {
					throw new RuntimeException(String.format("An exception occurred while executing bean modifier [%s] for the bean [%s].", modifierBeanName, beanName), e);
				}
			}
		}
	}


	private boolean isBeanModifierMatch(String modifierBeanName, Object bean, String beanName) {
		// Evaluate bean name filter
		BeanDefinition modifierBeanDefinition = getDefaultListableBeanFactory().getMergedBeanDefinition(modifierBeanName);
		List<String> postProcessorBeanNameList = getBeanModifierNameToBeanNameListCache().computeIfAbsent(modifierBeanName, mbn -> {
			List<String> beanNameList = new ArrayList<>();
			// Discover properties from annotated factory method
			Object beanSource = modifierBeanDefinition.getResolvableType().getSource();
			if (beanSource instanceof MethodParameter) {
				MethodParameter beanFactoryMethod = (MethodParameter) beanSource;
				BeanModifierTarget beanModifierTarget = AnnotationUtils.getAnnotation(beanFactoryMethod.getAnnotatedElement(), BeanModifierTarget.class);
				if (beanModifierTarget != null) {
					beanNameList.addAll(Arrays.asList(beanModifierTarget.beanName()));
				}
			}
			return beanNameList;
		});
		if (!postProcessorBeanNameList.isEmpty() && !postProcessorBeanNameList.contains(beanName)) {
			return false;
		}

		// Evaluate bean type filter
		List<Class<?>> postProcessorBeanClassList = getBeanModifierNameToBeanClassListCache().computeIfAbsent(modifierBeanName, mbn -> {
			List<Class<?>> beanClassList = new ArrayList<>();
			// Infer target class from target type generics
			Class<?> inferredTargetClass = modifierBeanDefinition.getResolvableType().as(BeanModifier.class).resolveGeneric();
			if (inferredTargetClass != null) {
				beanClassList.add(inferredTargetClass);
			}
			// Discover properties from annotated factory method
			Object beanSource = modifierBeanDefinition.getResolvableType().getSource();
			if (beanSource instanceof MethodParameter) {
				MethodParameter beanFactoryMethod = (MethodParameter) beanSource;
				BeanModifierTarget beanModifierTarget = AnnotationUtils.getAnnotation(beanFactoryMethod.getAnnotatedElement(), BeanModifierTarget.class);
				if (beanModifierTarget != null) {
					beanClassList.addAll(Arrays.asList(beanModifierTarget.beanType()));
				}
			}
			return beanClassList;
		});
		if (!postProcessorBeanNameList.isEmpty() && postProcessorBeanClassList.stream().noneMatch(clazz -> clazz.isInstance(bean))) {
			return false;
		}
		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void setBeanFactory(BeanFactory beanFactory) throws BeansException {
		setDefaultListableBeanFactory((DefaultListableBeanFactory) beanFactory);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DefaultListableBeanFactory getDefaultListableBeanFactory() {
		return this.defaultListableBeanFactory;
	}


	public void setDefaultListableBeanFactory(DefaultListableBeanFactory defaultListableBeanFactory) {
		this.defaultListableBeanFactory = defaultListableBeanFactory;
	}


	public List<String> getBeanModifierBeanNames() {
		return this.beanModifierBeanNames;
	}


	public void setBeanModifierBeanNames(List<String> beanModifierBeanNames) {
		this.beanModifierBeanNames = beanModifierBeanNames;
	}


	public Map<String, List<String>> getBeanModifierNameToBeanNameListCache() {
		return this.beanModifierNameToBeanNameListCache;
	}


	public Map<String, List<Class<?>>> getBeanModifierNameToBeanClassListCache() {
		return this.beanModifierNameToBeanClassListCache;
	}
}
