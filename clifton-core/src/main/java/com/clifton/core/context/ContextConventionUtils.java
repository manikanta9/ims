package com.clifton.core.context;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.dao.NonPersistentObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.NoTableDAOConfig;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.MethodUtils;
import org.springframework.beans.BeansException;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.persistence.Table;
import java.beans.Introspector;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * The <code>ContextConventionUtils</code> is a utility class used
 * to consolidate various naming conventions used for Classes -> Spring Bean Names,
 * Methods to Urls, Getting default security permission from a method name, etc.
 *
 * @author manderson
 */
public class ContextConventionUtils {

	public static final String JSON_ROOT = "/json";
	public static final String API_ROOT = "/api";
	public static final String OPEN_API_ROOT = "/openapi";
	public static final String OPEN_API_WEBCLIENT_ROOT = "/openapi-webclient";

	public static final String URL_DEFAULT_EXTENSION = ".json";

	private static final String BEAN_NAME_REGEX = "(.*)(Impl|InSpringContext)";
	private static final Pattern BEAN_NAME_PATTERN = Pattern.compile(BEAN_NAME_REGEX);

	private static final String DATA_SOURCE_NAME_SUFFIX = "DataSource";

	public static final String PACKAGE_BASIC_PREFIX_CONVENTION = "com.clifton.";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the bean name that would be generated for the given class name
	 * In general this is just the Short Class Name with first letter lower case
	 * and if the class ends in Impl, that suffix is removed
	 */
	public static String getBeanNameFromClassName(String className) {
		String shortName = org.springframework.util.ClassUtils.getShortName(className);
		Matcher m = BEAN_NAME_PATTERN.matcher(shortName);
		if (m.find()) {
			shortName = m.replaceFirst("$1");
		}
		return Introspector.decapitalize(shortName);
	}


	/**
	 * Returns the bean name for the specified interface that uses the specified local data source.
	 * For local data source, based on our convention, a separate bean is created for each data source.
	 * <p>
	 * For example: "dataSource" and "MyService" => "myService"
	 * "integrationDataSource" and "MyService" => "integrationMyService"
	 */
	public static String getBeanNameForDataSource(String localDataSourceName, Class<?> interfaceClass) {

		String beanName = ContextConventionUtils.getBeanNameFromClassName(interfaceClass.getSimpleName());
		if (!StringUtils.isEmpty(localDataSourceName) && !DATA_SOURCE_NAME_SUFFIX.equalsIgnoreCase(localDataSourceName)) {
			beanName = StringUtils.substringBeforeLast(localDataSourceName, DATA_SOURCE_NAME_SUFFIX) + StringUtils.capitalize(beanName);
		}
		return beanName;
	}


	/**
	 * Returns the bean name for the specified interface that uses the specified external data source.
	 * For external data source, based on our convention, a separate bean is created for each data source.
	 * <p>
	 * For example: "dataSource" and "MyService" => "myService"
	 * "integrationDataSource" and "MyService" => "integrationApplicationMyService"
	 */
	public static String getExternalApplicationBeanNameForDataSource(String externalDataSourceName, Class<?> interfaceClass) {
		String beanName = ContextConventionUtils.getBeanNameFromClassName(interfaceClass.getSimpleName());
		if (!StringUtils.isEmpty(externalDataSourceName) && !DATA_SOURCE_NAME_SUFFIX.equalsIgnoreCase(externalDataSourceName)) {
			beanName = StringUtils.substringBeforeLast(externalDataSourceName, DATA_SOURCE_NAME_SUFFIX) + "Application" + StringUtils.capitalize(beanName);
		}
		return beanName;
	}


	/**
	 * Returns the name of the table for the specified DTO class.  Our naming conventions match Simple Class Name to corresponding Table name.
	 * To override this, we use {@link Table} annotation.
	 */
	public static String getTableNameFromClass(Class<?> tableClazz) {
		Table tableAnnotation = AnnotationUtils.getAnnotation(tableClazz, Table.class);
		if (tableAnnotation != null && !StringUtils.isEmpty(tableAnnotation.name())) {
			return tableAnnotation.name();
		}
		return tableClazz.getSimpleName();
	}


	/**
	 * Tries to extract table name from the method name using our naming convention.
	 * For example, getInvestmentSecurityBySymbol => InvestmentSecurity
	 */
	public static String getTableNameFromMethodName(String methodName) {
		String tableName = StringUtils.removeLeadingWord(methodName);

		tableName = StringUtils.truncateStringAfterWord(tableName, "By", false);
		tableName = StringUtils.truncateStringAfterWord(tableName, "For", false);
		if (tableName != null && tableName.endsWith("List")) {
			tableName = tableName.substring(0, tableName.length() - 4);
		}

		return tableName;
	}


	/**
	 * Tries to extract table name from the method ant its corresponding arguments/return types using our naming convention.
	 * Returns null if table name cannot be extracted.
	 *
	 * @param dtoClassHolder - allows to return discovered dtoClass back to caller
	 */
	@SuppressWarnings("unchecked")
	public static <T extends IdentityObject> String getTableNameFromMethod(Method handlerMethod, String methodName, ObjectWrapper<Class<T>> dtoClassHolder, DaoLocator daoLocator) {
		// try to determine table name
		String tableName = null;
		ParameterizedType dtoType = null; // when possible, use Type which has generic info in addition to the class
		Class<T> dtoClass = dtoClassHolder.getObject();
		if (dtoClass == null) {
			Class<?> returnType = handlerMethod.getReturnType();
			if (IdentityObject.class.isAssignableFrom(returnType)) {
				dtoClass = (Class<T>) returnType;
				if (handlerMethod.getGenericReturnType() instanceof ParameterizedType) {
					dtoType = (ParameterizedType) handlerMethod.getGenericReturnType();
				}
			}
			else if (List.class.isAssignableFrom(returnType)) {
				Type[] typeArgs = ((ParameterizedType) handlerMethod.getGenericReturnType()).getActualTypeArguments();
				Type type = (typeArgs != null && typeArgs.length == 1) ? typeArgs[0] : null;
				if ((type instanceof Class<?>) && IdentityObject.class.isAssignableFrom((Class<?>) type)) {
					dtoClass = (Class<T>) type;
				}
				else {
					tableName = getTableNameFromMethodName(methodName);
				}
			}
			else {
				Class<?>[] parameterTypes = handlerMethod.getParameterTypes();
				Class<?> parameterType = parameterTypes.length == 1 ? parameterTypes[0] : null;
				if (parameterType != null && IdentityObject.class.isAssignableFrom(parameterType)) {
					dtoClass = (Class<T>) parameterType;
				}
				else {
					tableName = getTableNameFromMethodName(methodName);
					// try to get DTO class if possible: more accurate than guessing table names
					try {
						ReadOnlyDAO<T> dao = daoLocator.locate(tableName);
						if (dao != null && !(dao.getConfiguration() instanceof NoTableDAOConfig<?>)) {
							dtoClass = dao.getConfiguration().getBeanClass();
						}
					}
					catch (BeansException e) {
						// ignore exception: try finding class

						try {
							dtoClass = (Class<T>) Class.forName(handlerMethod.getDeclaringClass().getPackage().getName() + "." + tableName);
							if (IdentityObject.class.isAssignableFrom(dtoClass)) {
								tableName = null;
							}
							else {
								dtoClass = null;
							}
						}
						catch (ClassNotFoundException e2) {
							// ignore exception: use tableName instead
						}
					}
				}
			}
		}

		if (tableName == null) {
			if (dtoClass != null && IdentityObject.class.isAssignableFrom(dtoClass) && !AnnotationUtils.isAnnotationPresent(dtoClass, NonPersistentObject.class)) {
				ReadOnlyDAO<T> dao = (dtoType == null) ? daoLocator.locate(dtoClass) : daoLocator.locate(dtoType);
				if (!(dao.getConfiguration() instanceof NoTableDAOConfig<?>)) {
					tableName = dao.getConfiguration().getTableName();
				}
			}
		}

		dtoClassHolder.setObject(dtoClass);
		return tableName;
	}


	/**
	 * Returns the name of the service method that retrieves objects of the specified DTO class by the primary key.
	 */
	public static String getGetMethodNameFromClass(Class<?> dtoClass) {
		return ContextMethodPrefixes.GET.getPrefix() + getTableNameFromClass(dtoClass);
	}


	/**
	 * Returns the name of the service method that saves objects of the specified DTO class.
	 */
	public static String getSaveMethodNameFromClass(Class<?> dtoClass) {
		return ContextMethodPrefixes.SAVE.getPrefix() + getTableNameFromClass(dtoClass);
	}


	/**
	 * Returns the name of the service method that deletes objects of the specified DTO class by the primary key.
	 */
	public static String getDeleteMethodNameFromClass(Class<?> dtoClass) {
		return ContextMethodPrefixes.DELETE.getPrefix() + getTableNameFromClass(dtoClass);
	}


	/**
	 * Returns the URL for the given class and method name.  Optionally can include the beginning forward slash
	 * <p>
	 * If class or method name is null/empty returns null.
	 * If the method doesn't exist, or there are multiple methods with the same name on the class, or the method is not accessible via a URL
	 * an exception will be thrown.
	 * <p>
	 * Checks explicit request mapping annotation first, otherwise uses our naming convention to get the URL based on the method name and parameters.
	 */
	public static String getUrlFromMethodClassAndName(Class<?> methodClass, String methodName, boolean includeBeginningForwardSlash) {
		if (methodClass != null && !StringUtils.isEmpty(methodName)) {
			Method method = MethodUtils.getMethod(methodClass, methodName, false);
			if (method == null) {
				throw new RuntimeException("Bean of Class [" + methodClass + "] is missing method [" + methodName + "] or method is not accessible.");
			}
			return getUrlFromMethod(method, includeBeginningForwardSlash);
		}
		return null;
	}


	public static String getUrlFromMethod(Method method, boolean includeBeginningForwardSlash) {
		String url = null;
		AssertUtils.assertNotNull(method, "Method cannot be null!");
		String methodName = method.getName();
		Class<?> methodClass = method.getDeclaringClass();
		if (AnnotationUtils.isAnnotationPresent(method, RequestMapping.class)) {
			RequestMapping mapping = AnnotationUtils.getAnnotation(method, RequestMapping.class);
			url = mapping.value()[0];
		}
		else if (isUrlFromMethodAllowed(method, methodClass)) {
			url = getUrlFromMethod(method);
		}
		if (url == null) {
			throw new RuntimeException("Unable to determine URL from Method [" + methodClass + "." + methodName + "].  Method is either not accessible from the URL or is missing explicit RequestMapping annotation.");
		}
		// If URL starts with / need to remove it
		else {
			if (url.startsWith("/")) {
				if (!includeBeginningForwardSlash) {
					url = url.substring(1);
				}
			}
			else if (includeBeginningForwardSlash) {
				url = "/" + url;
			}
			if (!url.endsWith(ContextConventionUtils.URL_DEFAULT_EXTENSION)) {
				url = url + ContextConventionUtils.URL_DEFAULT_EXTENSION;
			}
		}
		return url;
	}


	/**
	 * Converts the method name to a URL based on list of approved prefixes we use and their url convention.
	 * If the method contains a parameter whose class is annotated with {@link SearchForm} AND it returns a
	 * {@link List} or {@link DataTable} then append 'Find' to the url.
	 */
	public static String getUrlFromMethod(Method method) {
		if (method != null) {
			String methodName = method.getName();
			ContextMethodPrefixes methodPrefix = getContextMethodPrefixFromMethodName(methodName);
			if (methodPrefix != null) {
				StringBuilder url = new StringBuilder(32);
				url.append('/');
				url.append(Introspector.decapitalize(methodName.substring(methodPrefix.getPrefix().length())));
				if (methodPrefix.isAppendPrefixAsUrlSuffix()) {
					url.append(StringUtils.capitalize(methodPrefix.getPrefix()));
				}
				if (methodPrefix == ContextMethodPrefixes.GET && (List.class.isAssignableFrom(method.getReturnType()) || DataTable.class.isAssignableFrom(method.getReturnType()))) {
					for (Class<?> clazz : method.getParameterTypes()) {
						if (AnnotationUtils.isAnnotationPresent(clazz, SearchForm.class)) {
							url.append("Find");
							break;
						}
					}
				}
				url.append(URL_DEFAULT_EXTENSION);
				return url.toString();
			}
		}
		return null;
	}


	/**
	 * Returns true if the specified method on the specified class can be publicly exposed via URL.
	 * The method must not have DoNotAddRequestMapping annotation, must be declared on an interface that is under "com.clifton." package
	 * and cannot have parameters that are interfaces (cannot instantiate for binding).
	 */
	public static boolean isUrlFromMethodAllowed(Method method, Class<?> methodClass) {
		// Enforce annotation constraints
		//First check if we've enabled the api mapping explicitly on a 'DoNotAddRequestMapping' annotation
		if (AnnotationUtils.isTrue(method, DoNotAddRequestMapping.class, DoNotAddRequestMapping::allowAPI)) {
			return true;
		}

		if (AnnotationUtils.isAnnotationPresent(method, DoNotAddRequestMapping.class)) {
			return false;
		}

		// Enforce method declaration location constraints
		if (!methodClass.isInterface()
				&& !MethodUtils.isMethodDeclaredOnOurInterface(method, methodClass, "com.clifton.")) {
			return false;
		}

		// Enforce method signature constraints
		if (MethodUtils.isMethodWithInterfaceParameters(method)) {
			return false;
		}

		return true;
	}


	/**
	 * If possible, returns the key of the main object (usually result) in the model. Our UI relies on
	 * standard key name "data" so that we don't have to specify it for every service.
	 * <p>
	 * If there's only one key or 2 keys with one of the values annotated as NonPersistentObject, return that key.
	 */
	public static String getMainKeyFromModel(Map<String, Object> model) {
		int size = model.size();
		if (size == 1) {
			// replace the key with the rootNodeName
			return model.keySet().iterator().next();
		}
		else if (size == 2) {
			// if one value is annotated as NonPersistentObject and the other is a List or a FileWrapper, use rootNodeName for the list/file
			Iterator<Map.Entry<String, Object>> entries = model.entrySet().iterator();
			Map.Entry<String, Object> entry1 = entries.next();
			String key1 = entry1.getKey();
			Object value1 = entry1.getValue();
			Map.Entry<String, Object> entry2 = entries.next();
			String key2 = entry2.getKey();
			Object value2 = entry2.getValue();
			if (value1 != null && AnnotationUtils.isAnnotationPresent(value1.getClass(), NonPersistentObject.class)) {
				return key2;
			}
			else if (value2 != null && AnnotationUtils.isAnnotationPresent(value2.getClass(), NonPersistentObject.class)) {
				return key1;
			}
		}
		return null;
	}


	/**
	 * Returns the security permission required based on the method name.
	 * Security permissions for approved prefixes are mapped accordingly (i.e. get = Read, save = Write, fix = Full Control)
	 * <p>
	 * If the method prefix is not mapped to a security resource, Write permission will be returned to use as default
	 */
	public static Integer getRequiredPermissionFromMethodName(String methodName, Map<String, ?> parameterMap) {
		ContextMethodPrefixes methodPrefix = getContextMethodPrefixFromMethodName(methodName);
		if (methodPrefix != null) {
			return methodPrefix.calculateSecurityPermission(parameterMap);
		}
		return SecurityPermission.PERMISSION_WRITE;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static ContextMethodPrefixes getContextMethodPrefixFromMethodName(String methodName) {
		String prefix = StringUtils.getLeadingWord(methodName);
		if (prefix != null) {
			for (ContextMethodPrefixes methodPrefix : ContextMethodPrefixes.values()) {
				if (prefix.equals(methodPrefix.getPrefix())) {
					return methodPrefix;
				}
			}
		}
		return null;
	}
}
