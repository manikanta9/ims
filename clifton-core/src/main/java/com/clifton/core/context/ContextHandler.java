package com.clifton.core.context;


/**
 * The <code>ContextHandler</code> interface defines methods for working with {@link Context} objects.
 *
 * @author vgomelsky
 */
public interface ContextHandler extends Context {

	// empty
}
