package com.clifton.core.jmx;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.beans.MethodUtils;
import com.clifton.core.util.jmx.JmxUtils;

import javax.management.MBeanServer;
import javax.management.MXBean;
import javax.management.ObjectName;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.RuntimeMXBean;
import java.lang.management.ThreadMXBean;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * The {@link CoreJmxUtils} class provides static utility methods for accessing managed beans and their attributes from the locally registered {@link MBeanServer}.
 * <p>
 * Managed beans, such as {@link MXBean MXBeans}, may provide details about significant JVM services, objects, and attributes. Some examples include the {@link RuntimeMXBean},
 * which can be used to access JVM properties, the {@link MemoryMXBean}, which provides JVM memory details, and the {@link ThreadMXBean}, which provides details around the JVM
 * thread system. Other standard <i>MBeans</i> (a superset of {@link MXBean MXBeans} with less stringent constraints) may also be registered. These can be registered by various
 * applications. For example, Tomcat registers helpful MBeans to monitor host properties, such as servlets, configurations, and endpoints.
 * <p>
 * Although JMX does not provide a standard method of accessing entities referenced by MBeans, MBeans of type <code>org.apache.tomcat.util.modeler.BaseModelMBean</code> may be
 * dereferenced by the {@link #getMBeanResource(ObjectName)} method.
 * <p>
 * MBean and other JMX-based diagnostic information can be viewed through the <a href="org.apache.tomcat.util.modeler.BaseModelMBean">jconsole</a> graphical tool, which is provided
 * with some distributions of the JDK.
 *
 * @author MikeH
 * @see ManagementFactory
 */
public class CoreJmxUtils {

	private CoreJmxUtils() {
		// Private constructor
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Platform MBean Methods                          ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Retrieves the backing resource for the MBean of the given {@link ObjectName}. This backing resource may be any referenced object, such as a {@link
	 * java.util.concurrent.ThreadPoolExecutor} or other managed resource.
	 * <p>
	 * This method makes a number of assumptions where are not verifiable until runtime, chief of which is that the MBean for the given {@link ObjectName} is of type
	 * <code>org.apache.tomcat.util.modeler.BaseModelMBean</code>. Many MBean implementations extend this type, making this a frequently safe operation to use, but the type should
	 * be verified when attempting to target a new resource.
	 */
	public static Object getMBeanResource(ObjectName objectName) {
		try {
			// Get JMX registry-holder
			Class<?> jmxMBeanServerClass = CoreClassUtils.getClass("com.sun.jmx.mbeanserver.JmxMBeanServer");
			AssertUtils.assertTrue(jmxMBeanServerClass.isAssignableFrom(JmxUtils.getPlatformMBeanServer().getClass()), "MBean resource retrieval is only supported for MBeanServers of type [%s]. The current MBeanServer type is [%s].", jmxMBeanServerClass.getName(), JmxUtils.getPlatformMBeanServer().getClass().getName());
			Field mbsInterceptorField = ClassUtils.getClassField(JmxUtils.getPlatformMBeanServer().getClass(), "mbsInterceptor", false, false);
			mbsInterceptorField.setAccessible(true);
			MBeanServer mbsInterceptor = (MBeanServer) mbsInterceptorField.get(JmxUtils.getPlatformMBeanServer());

			// Get MBean
			Class<?> defaultMBeanServerInterceptorClass = CoreClassUtils.getClass("com.sun.jmx.interceptor.DefaultMBeanServerInterceptor");
			AssertUtils.assertTrue(defaultMBeanServerInterceptorClass.isAssignableFrom(mbsInterceptor.getClass()), "MBean resource retrieval is only supported for MBeanServers with an interceptor of type [%s]. The current MBeanServer interceptor type is [%s].", defaultMBeanServerInterceptorClass.getName(), mbsInterceptor.getClass().getName());
			Method getMBeanMethod = MethodUtils.getDeclaredMethod(mbsInterceptor.getClass(), "getMBean", ObjectName.class);
			getMBeanMethod.setAccessible(true);
			Object mBean = MethodUtils.invoke(getMBeanMethod, mbsInterceptor, objectName);

			// Get resource
			Class<?> baseModelMBeanClass = CoreClassUtils.getClass("org.apache.tomcat.util.modeler.BaseModelMBean");
			AssertUtils.assertTrue(baseModelMBeanClass.isAssignableFrom(mBean.getClass()), "MBean resource retrieval is only supported for MBeans of type [%s]. The type for the MBean of name [%s] is [%s].", baseModelMBeanClass.getName(), objectName, mBean.getClass().getName());
			Method getResourceMethod = MethodUtils.getMethod(mBean.getClass(), "getManagedResource");
			return MethodUtils.invoke(getResourceMethod, mBean);
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while attempting to retrieve the MBean instance for object name [%s].", objectName), e);
		}
	}
}
