package com.clifton.core.health;

/**
 * The <code>HealthCheckParameters</code> type encapsulates parameters used during health checks.
 *
 * @author MikeH
 * @see HealthCheckHandler
 */
public class HealthCheckParameters {

	/**
	 * The maximum time allowed for any single test.
	 */
	private final Long maxTimeAllowed;
	/**
	 * The name of the test to run, or <code>null</code> to run all tests.
	 */
	private final String testToRun;
	/**
	 * If <code>true</code>, then tests will be restricted to run only during their allowed intervals. Otherwise, tests will be excluded during disallowed intervals.
	 */
	private final boolean enableIntervals;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public HealthCheckParameters(Long maxTimeAllowed, String testToRun, Boolean enableIntervals) {
		this.maxTimeAllowed = maxTimeAllowed;
		this.testToRun = testToRun;
		this.enableIntervals = enableIntervals;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Long getMaxTimeAllowed() {
		return this.maxTimeAllowed;
	}


	public String getTestToRun() {
		return this.testToRun;
	}


	public boolean isEnableIntervals() {
		return this.enableIntervals;
	}
}
