package com.clifton.core.health;

import javax.servlet.http.HttpServletRequest;
import java.io.Writer;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;


/**
 * The <code>HealthCheckHandler</code> interface provides methods for producing health check information.
 * <p>
 * Methods declared in this type are intended to be used during view rendering. Clients may consume the rendered output in order to evaluate the reported system health.
 *
 * @author MikeH
 */
public interface HealthCheckHandler {

	public String getEnvironment();


	public HealthCheckParameters getHealthCheckPageParameters(HttpServletRequest request);


	public HealthCheckParameters getHealthCheckPageParameters(Map<String, String> urlParams);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void printSection(Writer writer, String sectionName, String... sectionItems);


	public void printSectionHeader(Writer writer, String sectionName);


	public void printSystemInformation(Writer writer);


	public void printHealthCheckParameterInformation(Writer writer, HealthCheckParameters healthCheckParameters);


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	void runTest(String testName, long maxAllowedTimeInMillis, List<String> disabledIntervalList, Writer writer, HealthCheckParameters healthCheckParameters, Runnable testFn);


	public void runTest(String testName, long maxAllowedTimeInMillis, List<String> disabledIntervalList, Writer writer, HealthCheckParameters healthCheckParameters, Supplier<String> testFn);
}
