package com.clifton.core.health;

import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.ObjectUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.TimeTrackedOperation;
import com.clifton.core.util.date.TimeUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.Writer;
import java.time.LocalTime;
import java.util.List;
import java.util.Map;
import java.util.function.Supplier;


/**
 * The standard implementation for {@link HealthCheckHandler}.
 *
 * @author MikeH
 */
@Component
public class HealthCheckHandlerImpl implements HealthCheckHandler {

	@Value("${application.environment.level:prod}")
	private String environment;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getEnvironment() {
		return this.environment;
	}


	@Override
	public HealthCheckParameters getHealthCheckPageParameters(HttpServletRequest request) {
		String maxTimeAllowedParam = request.getParameter("maxTimeAllowed");
		Long maxTimeAllowed = !StringUtils.isEmpty(maxTimeAllowedParam)
				? Long.parseLong(maxTimeAllowedParam)
				: null;
		String testToRun = request.getParameter("nameOfTestToRun");
		boolean enableTimeIntervals = Boolean.parseBoolean(ObjectUtils.coalesce(request.getParameter("enableTimeIntervals"), "true"));
		return new HealthCheckParameters(maxTimeAllowed, testToRun, enableTimeIntervals);
	}


	@Override
	public HealthCheckParameters getHealthCheckPageParameters(Map<String, String> urlParams) {
		String maxTimeAllowedParam = urlParams.get("maxTimeAllowed");
		Long maxTimeAllowed = !StringUtils.isEmpty(maxTimeAllowedParam)
				? Long.parseLong(maxTimeAllowedParam)
				: null;
		String testToRun = urlParams.get("nameOfTestToRun");
		boolean enableTimeIntervals = Boolean.parseBoolean(ObjectUtils.coalesce(urlParams.get("enableTimeIntervals"), "true"));
		return new HealthCheckParameters(maxTimeAllowed, testToRun, enableTimeIntervals);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void printSectionHeader(Writer writer, String sectionName) {
		try {
			writer.write("<b>");
			writer.write(sectionName);
			writer.write("</b><br/>");
			writer.flush();
		}
		catch (Exception e) {
			try {
				writer.write(String.format("An exception occurred while writing section header [%s]: %s", sectionName, ExceptionUtils.getDetailedMessage(e)));
				writer.write("<br/>");
				writer.flush();
				LogUtils.error(getClass(), String.format("An exception occurred while writing section header [%s].", sectionName), e);
			}
			catch (Exception e2) {
				throw new RuntimeException(e2);
			}
		}
	}


	@Override
	public void printSection(Writer writer, String sectionName, String... sectionItems) {
		try {
			printSectionHeader(writer, sectionName);
			writer.write(String.join("<br/>", sectionItems));
			writer.write("<br/><br/>");
			writer.flush();
		}
		catch (Exception e) {
			try {
				writer.write(String.format("An exception occurred while evaluating section [%s]: %s", sectionName, ExceptionUtils.getDetailedMessage(e)));
				writer.write("<br/>");
				writer.flush();
				LogUtils.error(getClass(), String.format("An exception occurred while evaluating section [%s].", sectionName), e);
			}
			catch (Exception e2) {
				throw new RuntimeException(e2);
			}
		}
	}


	@Override
	public void printSystemInformation(Writer writer) {
		printSection(writer,
				"SYSTEM INFORMATION",
				"Environment = " + getEnvironment()
		);
	}


	@Override
	public void printHealthCheckParameterInformation(Writer writer, HealthCheckParameters healthCheckParameters) {
		printSection(writer,
				"PAGE PARAMETERS",
				"maxTimeAllowed = " + healthCheckParameters.getMaxTimeAllowed(),
				"nameOfTestToRun = " + healthCheckParameters.getTestToRun(),
				"enableTimeIntervals = " + healthCheckParameters.isEnableIntervals()
		);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void runTest(String testName, long maxAllowedTimeInMillis, List<String> disabledIntervalList, Writer writer, HealthCheckParameters healthCheckParameters, Runnable testFn) {
		runTest(testName, maxAllowedTimeInMillis, disabledIntervalList, writer, healthCheckParameters, () -> {
			testFn.run();
			return null;
		});
	}


	@Override
	public void runTest(String testName, long maxAllowedTimeInMillis, List<String> disabledIntervalList, Writer writer, HealthCheckParameters healthCheckParameters, Supplier<String> testFn) {
		try {
			// Header
			writer.write(String.format("<b>%s</b>", testName));
			writer.flush();

			// Guard-clause: Test skipped
			if (skipTest(testName, maxAllowedTimeInMillis, healthCheckParameters)) {
				writer.write(" - SKIPPED");
				writer.write("<br/>");
				writer.flush();
				return;
			}

			// Guard-clause: Test disabled within interval
			final LocalTime currentTime = LocalTime.now();
			if (healthCheckParameters.isEnableIntervals() && disabledIntervalList != null && DateUtils.isWithinAnyInterval(currentTime, disabledIntervalList)) {
				writer.write(String.format(" - Test is disabled during the interval %s<br/>", DateUtils.getWithinIntervalList(currentTime, disabledIntervalList).toString()));
				writer.write("<br/>");
				writer.flush();
				return;
			}

			executeTest(testName, maxAllowedTimeInMillis, writer, testFn);
			writer.flush();
		}
		catch (Exception e) {
			try {
				writer.write(String.format("An exception occurred while running test [%s]: %s", testName, ExceptionUtils.getDetailedMessage(e)));
				writer.write("<br/>");
				writer.flush();
				LogUtils.error(getClass(), String.format("An exception occurred while running test [%s].", testName), e);
			}
			catch (Exception e2) {
				throw new RuntimeException(e2);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private boolean skipTest(String testName, long maxAllowedTimeInMillis, HealthCheckParameters healthCheckParameters) {
		return (healthCheckParameters.getMaxTimeAllowed() != null && healthCheckParameters.getMaxTimeAllowed() < maxAllowedTimeInMillis)
				|| (healthCheckParameters.getTestToRun() != null && !StringUtils.isEqual(healthCheckParameters.getTestToRun(), testName));
	}


	private void executeTest(String testName, long maxAllowedTimeInMillis, Writer writer, Supplier<String> function) {
		try {
			TimeTrackedOperation<String> testOperation = TimeUtils.getElapsedTime(function);
			String testOutput = testOperation.getResult();
			writer.write(String.format(" - OK (%dms)", testOperation.getElapsedMilliseconds()));
			if (testOperation.getElapsedMilliseconds() > maxAllowedTimeInMillis) {
				writer.write(String.format(" <mark>WARNING: Response time has exceeded threshold (%dms) and could be slow.</mark>", maxAllowedTimeInMillis));
			}
			if (testOutput != null) {
				writer.write(testOutput);
			}
			writer.write("<br/>");
			writer.flush();
		}
		catch (Exception e) {
			try {
				LogUtils.error(getClass(), String.format("An exception occurred while evaluating health check test [%s].", testName), e);
				writer.write(String.format(" - <mark>ERROR: %s</mark>", ExceptionUtils.getDetailedMessage(e)));
				writer.write("<br/>");
				writer.flush();
			}
			catch (Exception e2) {
				LogUtils.error(getClass(), String.format("An exception occurred while writing test exception output for test [%s].", testName), e2);
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setEnvironment(String environment) {
		this.environment = environment;
	}
}
