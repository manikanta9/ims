package com.clifton.core.comparison.field;

/**
 * @author manderson
 */
public class BeanFieldNotInValueListComparison extends BeanFieldInValueListComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
