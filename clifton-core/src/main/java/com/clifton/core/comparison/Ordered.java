package com.clifton.core.comparison;


/**
 * The <code>Ordered</code> interface is a marker for sorted objects.
 * Classes implementing this interface will be sorted according to the value returned by getOrder() method.
 *
 * @author vgomelsky
 */
public interface Ordered {

	/**
	 * Returns the relative order of this object as it compares to other objects in a list.
	 */
	public int getOrder();


	/**
	 * Returns true if an observer wants to ensure it is the only observer specified at a particular order value; default is false.
	 * Use cases for this include those that want to be first ({@link Integer#MIN_VALUE}) or last ({@link Integer#MAX_VALUE}).
	 * <p>
	 * This value is checked at the time of registering observers at startup and will throw an exception upon reaching a duplicate
	 * order for any observer returning true for this method.
	 */
	default public boolean isUniqueOrderingRequired() {
		return false;
	}
}
