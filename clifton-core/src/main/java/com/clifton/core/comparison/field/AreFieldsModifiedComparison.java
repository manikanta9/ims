package com.clifton.core.comparison.field;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;

import java.beans.PropertyDescriptor;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>AreFieldsModifiedComparison</code> takes an unsaved updated bean and compares it to the
 * original bean retrieved from the database.
 * <p>
 * Returns TRUE if ANY of the given fields have been modified.
 * Returns FALSE if NONE of the given fields have been modified.
 * NOTE: System Managed Fields (Record Stamp) are always excluded from comparison
 *
 * @author manderson
 */
public class AreFieldsModifiedComparison implements Comparison<IdentityObject> {

	private DaoLocator daoLocator;
	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////////

	/**
	 * Define specific fields to check
	 */
	private String beanFieldNames;


	/**
	 * Optionally check all fields
	 */
	private boolean allFields;


	/**
	 * When checking all fields define a list of fields to exclude
	 */
	private String exceptionBeanFieldNames;


	/**
	 * Optionally load related lists for the original entity identified by {@link com.clifton.core.dataaccess.dao.OneToManyEntity} annotation.
	 * Allows accurate comparison of dependent lists that maybe submitted by the caller (UI or other process).
	 */
	private boolean loadOneToManyDependencies;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		IdentityObject originalBean = getOriginalBean(bean);

		List<String> beanFields = getBeanFields(bean);
		List<String> notEqualFields = getNotEqualFields(beanFields, bean, originalBean);

		boolean result = (isReverse() ? CollectionUtils.isEmpty(notEqualFields) : !CollectionUtils.isEmpty(notEqualFields));

		if (context != null) {
			String message;
			if (!CollectionUtils.isEmpty(notEqualFields)) {
				if (notEqualFields.size() == 1) {
					message = "Field [" + notEqualFields.get(0) + "] has been modified.";
				}
				else {
					message = "Fields [" + StringUtils.collectionToDelimitedString(notEqualFields, ",") + "] have been modified.";
				}
			}
			else if (CollectionUtils.getSize(beanFields) == 1) {
				message = "Field [" + StringUtils.collectionToDelimitedString(beanFields, ",") + "] has not been modified.";
			}
			else {
				message = "None of the Fields [" + StringUtils.collectionToDelimitedString(beanFields, ",") + "] have been modified.";
			}
			if (result) {
				context.recordTrueMessage(message);
			}
			else {
				context.recordFalseMessage(message);
			}
		}
		return result;
	}


	protected List<String> getNotEqualFields(List<String> beanFields, IdentityObject bean, IdentityObject originalBean) {
		List<String> notEqualFields = new ArrayList<>();
		for (String fieldName : CollectionUtils.getIterable(beanFields)) {
			Object beanValue = BeanUtils.getPropertyValue(bean, fieldName, true);
			Object originalBeanValue = BeanUtils.getPropertyValue(originalBean, fieldName, true);
			if (CompareUtils.compare(beanValue, originalBeanValue) != 0) {
				// For Numeric Field Comparisons - If they aren't equal, check if they equal to the scale of the original value
				// Prevents failing when values are calculated but during saves they are rounded
				if (originalBeanValue instanceof BigDecimal) {
					beanValue = MathUtils.round((BigDecimal) beanValue, ((BigDecimal) originalBeanValue).scale());
					if (CompareUtils.compare(beanValue, originalBeanValue) != 0) {
						notEqualFields.add(fieldName);
					}
				}
				else {
					notEqualFields.add(fieldName);
				}
			}
		}
		return notEqualFields;
	}


	protected boolean isReverse() {
		return false;
	}


	private List<String> getBeanFields(IdentityObject bean) {
		// If all fields, set String beanFieldNames automatically, and filter out any exceptions
		if (isAllFields()) {
			Iterable<String> exceptions = StringUtils.delimitedStringToList(getExceptionBeanFieldNames(), ",");
			List<String> beanFields = new ArrayList<>();

			List<PropertyDescriptor> descriptors = BeanUtils.getPropertyDescriptors(bean.getClass());
			for (PropertyDescriptor desc : CollectionUtils.getIterable(descriptors)) {

				// Skip System Managed Fields
				if (BeanUtils.isSystemManagedField(desc.getName()) || desc.getWriteMethod() == null || "id".equals(desc.getName())) {
					continue;
				}
				boolean add = true;
				for (String except : CollectionUtils.getIterable(exceptions)) {
					if (desc.getName().equalsIgnoreCase(except)) {
						add = false;
						break;
					}
				}
				if (add) {
					beanFields.add(desc.getName());
				}
			}
			return beanFields;
		}
		return StringUtils.delimitedStringToList(getBeanFieldNames(), ",");
	}


	@SuppressWarnings("unchecked")
	protected IdentityObject getOriginalBean(final IdentityObject bean) {
		if (bean == null || bean.isNewBean()) {
			return null;
		}
		if (getDaoLocator() == null) {
			throw new IllegalStateException("DAO Locator is NULL");
		}
		final ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate((Class<IdentityObject>) bean.getClass());
		IdentityObject result = DaoUtils.executeDetached(true, () -> dao.findByPrimaryKey(bean.getIdentity()));

		if (result != null && isLoadOneToManyDependencies()) {
			getApplicationContextService().populateManySideDependencies(result);
		}

		return result;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public String getBeanFieldNames() {
		return this.beanFieldNames;
	}


	public void setBeanFieldNames(String beanFieldNames) {
		this.beanFieldNames = beanFieldNames;
	}


	public boolean isAllFields() {
		return this.allFields;
	}


	public void setAllFields(boolean allFields) {
		this.allFields = allFields;
	}


	public String getExceptionBeanFieldNames() {
		return this.exceptionBeanFieldNames;
	}


	public void setExceptionBeanFieldNames(String exceptionBeanFieldNames) {
		this.exceptionBeanFieldNames = exceptionBeanFieldNames;
	}


	public boolean isLoadOneToManyDependencies() {
		return this.loadOneToManyDependencies;
	}


	public void setLoadOneToManyDependencies(boolean loadOneToManyDependencies) {
		this.loadOneToManyDependencies = loadOneToManyDependencies;
	}
}
