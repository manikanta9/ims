package com.clifton.core.comparison.field;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.StringUtils;


/**
 * The <code>BeanFieldIsNullComparison</code> class compares field value of the specified bean to null value.
 * It evaluates to true if the field value is null.
 *
 * @author manderson
 */
public class BeanFieldIsNullComparison extends BaseBeanFieldComparison<IdentityObject> {

	/**
	 * Optional overrides to make the messages more user friendly
	 */
	private String trueMessageOverride;
	private String falseMessageOverride;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		boolean result = isReverse();
		if (bean == null) {
			result = !isReverse();
		}
		else {
			Object beanValue = BeanUtils.getPropertyValue(bean, getBeanFieldName());
			if (beanValue == null) {
				result = !isReverse();
			}
		}

		// record comparison result message
		if (context != null) {
			if (result) {
				context.recordTrueMessage(StringUtils.coalesce(true, getTrueMessageOverride(), getTrueMessage()));
			}
			else {
				context.recordFalseMessage(StringUtils.coalesce(true, getFalseMessageOverride(), getFalseMessage()));
			}
		}
		return result;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	protected String getTrueMessage() {
		return "(" + getBeanFieldName() + " is not populated)";
	}


	protected String getFalseMessage() {
		return "(" + getBeanFieldName() + " is populated)";
	}


	protected boolean isReverse() {
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                Getter and Setter Methods               ////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getTrueMessageOverride() {
		return this.trueMessageOverride;
	}


	public void setTrueMessageOverride(String trueMessageOverride) {
		this.trueMessageOverride = trueMessageOverride;
	}


	public String getFalseMessageOverride() {
		return this.falseMessageOverride;
	}


	public void setFalseMessageOverride(String falseMessageOverride) {
		this.falseMessageOverride = falseMessageOverride;
	}
}
