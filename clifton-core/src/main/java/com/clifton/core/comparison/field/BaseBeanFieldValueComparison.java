package com.clifton.core.comparison.field;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;


/**
 * The <code>BaseBeanFieldValueComparison</code> class provides fields as methods for performing bean comparisons based on a given field and a given value.
 * <p>
 * This class may be extended to provide comparison functionality, such as validating against an expected value or pattern. The value may also be provided as a
 * SQL select statement, in which case the value will be interpreted using the FreeMarker interpreter.
 */
public abstract class BaseBeanFieldValueComparison<T extends IdentityObject> extends BaseBeanFieldComparison<T> implements ValidationAware {

	private String value;

	/**
	 * Optionally specifies that the value is SQL that must be executed.
	 * SQL must return one column and row which will be used for comparison.
	 */
	private boolean valueIsSQL;

	private boolean valueIsBeanProperty;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() {
		if (isValueIsBeanProperty() && isValueIsSQL()) {
			throw new ValidationException("The Value cannot be Bean Property and SQL at the same time: only one, the other or neither.");
		}
	}


	/**
	 * Returns the value that bean's field is compared to. Can either be a static value or SQL based value.
	 */
	protected String getActualValue(T bean) {
		if (isValueIsBeanProperty()) {
			Object propertyValue = BeanUtils.getPropertyValue(bean, getValue());
			return (propertyValue == null) ? null : propertyValue.toString();
		}

		if (isValueIsSQL()) {
			// set SQL parameters using template converter
			TemplateConfig config = new TemplateConfig(getValue());
			config.addBeanToContext("bean", bean);
			String sql = getTemplateConverter().convert(config);

			DataTable result = getDataTableRetrievalHandlerLocator().locate(getDataSourceName()).findDataTable(sql);
			if (result != null) {
				ValidationUtils.assertTrue(result.getTotalRowCount() == 1, "One row must be returned by: " + sql);
				ValidationUtils.assertTrue(result.getColumnCount() == 1, "One column must be returned by: " + sql);
				Object v = result.getRow(0).getValue(0);
				if (v != null) {
					return v.toString();
				}
			}
			return null;
		}

		return getValue();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public boolean isValueIsSQL() {
		return this.valueIsSQL;
	}


	public void setValueIsSQL(boolean valueIsSQL) {
		this.valueIsSQL = valueIsSQL;
	}


	public boolean isValueIsBeanProperty() {
		return this.valueIsBeanProperty;
	}


	public void setValueIsBeanProperty(boolean valueIsBeanProperty) {
		this.valueIsBeanProperty = valueIsBeanProperty;
	}
}
