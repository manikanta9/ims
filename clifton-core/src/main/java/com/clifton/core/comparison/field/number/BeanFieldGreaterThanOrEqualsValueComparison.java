package com.clifton.core.comparison.field.number;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.MathUtils;


/**
 * The <code>BeanFieldGreaterThanOrEqualsValueComparison</code> ...
 *
 * @author manderson
 */
public class BeanFieldGreaterThanOrEqualsValueComparison extends BeanFieldNumberValueComparison<IdentityObject> {

	@Override
	public String getConditionString() {
		return ">=";
	}


	@Override
	public boolean evaluateCondition(Number n1, Number n2) {
		return MathUtils.isGreaterThan(n1, n2) || MathUtils.isEqual(n1, n2);
	}
}
