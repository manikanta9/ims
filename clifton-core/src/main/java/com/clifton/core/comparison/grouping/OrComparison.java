package com.clifton.core.comparison.grouping;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * The <code>OrComparison</code> class represents a grouping OR comparison that evaluates to true if at least one of it's child
 * comparisons evaluates to true.
 *
 * @author vgomelsky
 */
public class OrComparison<T extends IdentityObject> extends BaseGroupingComparison<T> {

	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		ValidationUtils.assertNotEmpty(getChildComparisonList(), "OR comparison must have at least one child.");

		OrComparisonContext orContext = new OrComparisonContext(context);
		boolean result = false;
		for (Comparison<T> comparison : getChildComparisonList()) {
			if (comparison.evaluate(bean, orContext)) {
				result = true;
				break;
			}
		}

		// record combined messages
		if (context != null) {
			orContext.setEvaluationResult(result);
			if (result) {
				context.recordTrueMessage(orContext.getTrueMessage());
			}
			else {
				context.recordFalseMessage(orContext.getFalseMessage());
			}
		}

		return result;
	}
}
