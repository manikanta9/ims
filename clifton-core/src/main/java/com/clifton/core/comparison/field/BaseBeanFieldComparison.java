package com.clifton.core.comparison.field;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;


/**
 * The <code>BaseBeanFieldComparison</code> class is the base class for performing comparisons based on a single bean field. This class provides fields for
 * defining the field name and the data source for the bean which shall be compared.
 *
 * @see BaseBeanFieldValueComparison
 */
public abstract class BaseBeanFieldComparison<T extends IdentityObject> implements Comparison<T> {

	private String beanFieldName;
	private String dataSourceName = "dataSource";

	private TemplateConverter templateConverter;
	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getBeanFieldName() {
		return this.beanFieldName;
	}


	public void setBeanFieldName(String beanFieldName) {
		this.beanFieldName = beanFieldName;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}
}
