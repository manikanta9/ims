package com.clifton.core.comparison.grouping;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.GroupingComparison;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BaseGroupingComparison</code> class is a base class with helper methods for grouping comparisons.
 *
 * @author vgomelsky
 */
public abstract class BaseGroupingComparison<T extends IdentityObject> implements GroupingComparison<T> {

	private List<Comparison<T>> childComparisonList;


	@Override
	public void addChildComparison(Comparison<T> comparison) {
		if (this.childComparisonList == null) {
			this.childComparisonList = new ArrayList<>();
		}
		this.childComparisonList.add(comparison);
	}


	/**
	 * @return the childComparisonList
	 */
	public List<Comparison<T>> getChildComparisonList() {
		return this.childComparisonList;
	}


	/**
	 * @param childComparisonList the childComparisonList to set
	 */
	public void setChildComparisonList(List<Comparison<T>> childComparisonList) {
		this.childComparisonList = childComparisonList;
	}
}
