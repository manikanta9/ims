package com.clifton.core.comparison.grouping;


import com.clifton.core.context.Context;


/**
 * The <code>AndComparisonContext</code> class implements ComparisonContext for the AndComparison.
 * It uses ' AND ' text to concatenate individual messages.
 *
 * @author vgomelsky
 */
public class AndComparisonContext extends BaseComparisonContext {

	public AndComparisonContext() {
		super();
	}


	public AndComparisonContext(Context context) {
		super(context);
	}


	@Override
	protected String getComparisonText() {
		return " AND ";
	}
}
