package com.clifton.core.comparison.grouping;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.Comparison;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.validation.ValidationUtils;


/**
 * The <code>AndComparison</code> class represents a grouping AND comparison that evaluates to true if all of it's child
 * comparisons evaluate to true.
 *
 * @author vgomelsky
 */
public class AndComparison<T extends IdentityObject> extends BaseGroupingComparison<T> {

	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		ValidationUtils.assertNotEmpty(getChildComparisonList(), "AND comparison must have at least one child.");

		AndComparisonContext andContext = new AndComparisonContext(context);

		boolean result = true;
		for (Comparison<T> comparison : getChildComparisonList()) {
			if (!comparison.evaluate(bean, andContext)) {
				result = false;
				break;
			}
		}

		// record combined messages
		if (context != null) {
			andContext.setEvaluationResult(result);
			context.recordFalseMessage(andContext.getFalseMessage());
			context.recordTrueMessage(andContext.getTrueMessage());
		}

		return result;
	}
}
