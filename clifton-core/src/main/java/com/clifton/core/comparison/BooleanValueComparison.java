package com.clifton.core.comparison;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>BooleanValueComparison</code> is used for comparisons that should
 * always return true or false independent of the {@link IdentityObject} itself.
 * <p/>
 * The value returned is the booleanValue set on the comparison object.
 *
 * @author manderson
 */
public class BooleanValueComparison implements Comparison<IdentityObject> {

	private boolean booleanValue;


	@Override
	@SuppressWarnings("unused")
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		boolean result = isBooleanValue();

		if (context != null) {
			if (result) {
				context.recordTrueMessage("TRUE");
			}
			else {
				context.recordFalseMessage("FALSE");
			}
		}
		return result;
	}


	public boolean isBooleanValue() {
		return this.booleanValue;
	}


	public void setBooleanValue(boolean booleanValue) {
		this.booleanValue = booleanValue;
	}
}
