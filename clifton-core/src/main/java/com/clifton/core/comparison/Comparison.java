package com.clifton.core.comparison;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>Comparison</code> interface defines comparison methods.
 *
 * @author vgomelsky
 */
public interface Comparison<T extends IdentityObject> {

	/**
	 * Evaluates this comparison for the specified bean and returns true if it evaluates to true.
	 * Returns false otherwise.
	 */
	public boolean evaluate(T bean, ComparisonContext context);
}
