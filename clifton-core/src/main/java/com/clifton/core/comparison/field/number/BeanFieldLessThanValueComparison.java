package com.clifton.core.comparison.field.number;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.MathUtils;


public class BeanFieldLessThanValueComparison extends BeanFieldNumberValueComparison<IdentityObject> {

	@Override
	public String getConditionString() {
		return "<";
	}


	@Override
	public boolean evaluateCondition(Number n1, Number n2) {
		return MathUtils.isLessThan(n1, n2);
	}
}
