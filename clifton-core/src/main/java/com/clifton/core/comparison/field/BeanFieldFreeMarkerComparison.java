package com.clifton.core.comparison.field;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import freemarker.core.Environment;
import freemarker.template.TemplateBooleanModel;
import freemarker.template.TemplateModel;
import freemarker.template.TemplateModelException;
import freemarker.template.TemplateScalarModel;


/**
 * The <code>BeanFieldFreeMarkerComparison</code> class allows for flexible comparison on a bean field using a FreeMarker template.
 * <p>
 * When this comparison is evaluated, the given FreeMarker template is evaluated with a populated context.
 * <p>
 * This comparison will evaluate to {@code true} under any of the following conditions:
 * <ul>
 * <li>The whitespace-trimmed textual result of the evaluated template is {@code true}
 * <li>The <u><tt>{@value #FREEMARKER_VARIABLE_SUCCESS}</tt></u> FreeMarker variable is set to {@code true}
 * </ul>
 * <p>
 * If the comparison evaluates to {@code false}, then the value of the <u><tt>{@value #FREEMARKER_VARIABLE_ERROR_MESSAGE}</tt></u> FreeMarker variable (if set)
 * will be appended to the result message.
 *
 * @author MikeH
 */
public class BeanFieldFreeMarkerComparison extends BaseBeanFieldComparison<IdentityObject> {

	private static final String FREEMARKER_VARIABLE_SUCCESS = "success";
	private static final String FREEMARKER_VARIABLE_ERROR_MESSAGE = "errorMessage";

	private String freeMarkerTemplate;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		// Retrieve field value
		Object propertyValue;
		try {
			propertyValue = BeanUtils.getPropertyValue(bean, getBeanFieldName());
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("Unable to retrieve value for field [%s].", getBeanFieldName()), e);
		}

		// Evaluate template
		TemplateConfig templateConfig = new TemplateConfig(getFreeMarkerTemplate());
		templateConfig.addBeanToContext("value", propertyValue);
		templateConfig.addBeanToContext("bean", bean);
		Environment environment = getTemplateConverter().processTemplate(templateConfig);
		String freeMarkerResult = environment.getOut().toString().trim();

		// Extract variables
		boolean successVariable;
		String errorMessage = "";
		try {
			TemplateModel successModel = environment.getVariable(FREEMARKER_VARIABLE_SUCCESS);
			TemplateModel errorMessageModel = environment.getVariable(FREEMARKER_VARIABLE_ERROR_MESSAGE);
			successVariable = successModel == TemplateBooleanModel.TRUE;
			if (errorMessageModel instanceof TemplateScalarModel) {
				errorMessage = " " + ((TemplateScalarModel) errorMessageModel).getAsString();
			}
		}
		catch (TemplateModelException e) {
			throw new ValidationException("An error occurred while attempting to retrieve evaluated FreeMarker template variables.", e);
		}

		// Handle results
		boolean matches = successVariable || StringUtils.isEqualIgnoreCase(freeMarkerResult, "true");
		if (context != null) {
			if (matches) {
				context.recordTrueMessage(String.format("Field [%s] is valid.", this.getBeanFieldName()));
			}
			else {
				context.recordFalseMessage(String.format("Invalid value for field [%s].%s", this.getBeanFieldName(), errorMessage));
			}
		}
		return matches;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFreeMarkerTemplate() {
		return this.freeMarkerTemplate;
	}


	public void setFreeMarkerTemplate(String freeMarkerTemplate) {
		this.freeMarkerTemplate = freeMarkerTemplate;
	}
}
