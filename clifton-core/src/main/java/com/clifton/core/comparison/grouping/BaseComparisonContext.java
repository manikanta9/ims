package com.clifton.core.comparison.grouping;


import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextImpl;


/**
 * The <code>BaseComparisonContext</code> abstract class implements ComparisonContext and is intended
 * to be extended by specific comparison implementation for AND and OR.
 *
 * @author vgomelsky
 */
public abstract class BaseComparisonContext extends ContextImpl implements ComparisonContext {

	private String trueMessage;
	private String falseMessage;
	private boolean evaluationResult;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BaseComparisonContext() {
	}


	public BaseComparisonContext(Context context) {
		super(context);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The method should return String representation of specific comparison implementation.
	 * For example, " AND ", " OR ", etc.
	 */
	protected abstract String getComparisonText();


	@Override
	public void clear() {
		super.clear();
		clearMessages();
	}


	@Override
	public void clearMessages() {
		this.trueMessage = null;
		this.falseMessage = null;
		this.evaluationResult = false;
	}


	@Override
	public void recordFalseMessage(String message) {
		if (this.falseMessage == null) {
			this.falseMessage = message;
		}
		else {
			this.falseMessage += getComparisonText() + message;
		}
	}


	@Override
	public void recordTrueMessage(String message) {
		if (this.trueMessage == null) {
			this.trueMessage = message;
		}
		else {
			this.trueMessage += getComparisonText() + message;
		}
	}


	@Override
	public String getTrueMessage() {
		if (this.trueMessage == null) {
			return null;
		}
		else if (!isEvaluationResult()) {
			return "COMPARISON EVALUATED TO FALSE";
		}
		return this.trueMessage;
	}


	@Override
	public String getFalseMessage() {
		if (this.falseMessage == null) {
			return null;
		}
		else if (isEvaluationResult()) {
			return "COMPARISON EVALUATED TO TRUE";
		}
		return this.falseMessage;
	}


	public boolean isEvaluationResult() {
		return this.evaluationResult;
	}


	public void setEvaluationResult(boolean evaluationResult) {
		this.evaluationResult = evaluationResult;
	}
}
