package com.clifton.core.comparison.field;


/**
 * The <code>BeanFieldIsNotNullComparison</code> class compares field value of the specified bean to null value.
 * It evaluates to true if the field value is NOT null.
 *
 * @author manderson
 */
public class BeanFieldIsNotNullComparison extends BeanFieldIsNullComparison {

	@Override
	protected boolean isReverse() {
		return true;
	}


	@Override
	protected String getTrueMessage() {
		return "(" + getBeanFieldName() + " is populated)";
	}


	@Override
	protected String getFalseMessage() {
		return "(" + getBeanFieldName() + " is required)";
	}
}
