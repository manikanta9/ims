package com.clifton.core.comparison.field;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>BeanFieldNoEqualsToValueComparison</code> class compares field value of the specified bean to the specified value
 * and returns true if they are not equal.
 *
 * @author manderson
 */
public class BeanFieldNotEqualsToValueComparison<T extends IdentityObject> extends BeanFieldEqualsToValueComparison<T> {

	@Override
	protected boolean isReverse() {
		return true;
	}
}
