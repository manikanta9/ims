package com.clifton.core.comparison;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.DataTableRetrievalHandlerLocator;
import com.clifton.core.util.StringUtils;


/**
 * The <code>SqlComparison</code> class represents a comparison that evaluates to true when
 * the specified query returns results. It evaluates to false otherwise.
 *
 * @author vgomelsky
 */
public class SqlComparison implements Comparison<IdentityObject> {

	private String sql;
	private String dataSourceName = "dataSource";
	private String trueMessage;
	private String falseMessage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private TemplateConverter templateConverter;
	private DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		// set SQL parameters using template converter
		TemplateConfig config = new TemplateConfig(getSql());
		config.addBeanToContext("bean", bean);
		String sqlToRun = getTemplateConverter().convert(config);

		DataTable result = getDataTableRetrievalHandlerLocator().locate(getDataSourceName()).findDataTable(sqlToRun);

		boolean answer = false;
		if (result != null && result.getTotalRowCount() > 0) {
			answer = true;
		}

		// record comparison result message
		if (context != null) {
			if (answer) {
				context.recordTrueMessage(StringUtils.isEmpty(getTrueMessage()) ? "The query return result" : getTrueMessage());
			}
			else {
				context.recordFalseMessage(StringUtils.isEmpty(getFalseMessage()) ? "The query returned no result" : getFalseMessage());
			}
		}

		return answer;
	}


	public String getSql() {
		return this.sql;
	}


	public void setSql(String sql) {
		this.sql = sql;
	}


	public String getDataSourceName() {
		return this.dataSourceName;
	}


	public void setDataSourceName(String dataSourceName) {
		this.dataSourceName = dataSourceName;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public DataTableRetrievalHandlerLocator getDataTableRetrievalHandlerLocator() {
		return this.dataTableRetrievalHandlerLocator;
	}


	public void setDataTableRetrievalHandlerLocator(DataTableRetrievalHandlerLocator dataTableRetrievalHandlerLocator) {
		this.dataTableRetrievalHandlerLocator = dataTableRetrievalHandlerLocator;
	}


	public String getTrueMessage() {
		return this.trueMessage;
	}


	public void setTrueMessage(String trueMessage) {
		this.trueMessage = trueMessage;
	}


	public String getFalseMessage() {
		return this.falseMessage;
	}


	public void setFalseMessage(String falseMessage) {
		this.falseMessage = falseMessage;
	}
}
