package com.clifton.core.comparison;


import com.clifton.core.context.Context;


/**
 * The <code>ComparisonContext</code> interface defines a comparison context that is passed
 * to each Comparison for evaluation.  It can be used to record true/false messages that
 * help understand results of the comparison.
 *
 * @author vgomelsky
 */
public interface ComparisonContext extends Context {

	/**
	 * Records the FALSE message which should be set when a Comparison evaluates to FALSE.
	 * The false message is a user friendly message that should help explain the result of this evaluation.
	 */
	public void recordFalseMessage(String message);


	/**
	 * Records the TRUE message which should be set when a Comparison evaluates to TRUE.
	 * The true message is a user friendly message that should help explain the result of this evaluation.
	 */
	public void recordTrueMessage(String message);


	/**
	 * Returns any message recorded for True results
	 */
	public String getTrueMessage();


	/**
	 * Returns any message recorded for False results
	 */
	public String getFalseMessage();


	/**
	 * Clears True and False Messages that may have been recorded for this context.
	 */
	public void clearMessages();
}
