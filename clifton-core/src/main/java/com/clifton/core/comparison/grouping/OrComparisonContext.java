package com.clifton.core.comparison.grouping;


import com.clifton.core.context.Context;


/**
 * The <code>OrComparisonContext</code> class implements ComparisonContext for the OrComparison.
 * It uses ' OR ' text to concatenate individual messages.
 *
 * @author vgomelsky
 */
public class OrComparisonContext extends BaseComparisonContext {

	public OrComparisonContext() {
		super();
	}


	public OrComparisonContext(Context context) {
		super(context);
	}


	@Override
	protected String getComparisonText() {
		return " OR ";
	}
}
