package com.clifton.core.comparison.field;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
public class BeanFieldInValueListComparison extends BaseBeanFieldComparison<IdentityObject> {

	private List<String> valueList;


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public boolean evaluate(IdentityObject bean, ComparisonContext context) {
		boolean result = isReverse();
		Object beanValue = null;
		if (bean != null) {
			beanValue = BeanUtils.getPropertyValue(bean, getBeanFieldName());
			result = (isReverse() ? !isBeanValueInList(beanValue) : isBeanValueInList(beanValue));
		}

		// record comparison result message
		if (context != null) {
			String msg;
			if (isReverse() ? !result : result) {
				msg = ("(" + getBeanFieldName() + ": " + beanValue + " IN " + StringUtils.collectionToCommaDelimitedString(getValueList()) + ")");
			}
			else {
				msg = ("(" + getBeanFieldName() + ": " + beanValue + " NOT IN " + StringUtils.collectionToCommaDelimitedString(getValueList()) + ")");
			}
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}


	private boolean isBeanValueInList(Object beanValue) {
		return !CollectionUtils.isEmpty(getValueList().stream().filter(value -> (isValueEqual(beanValue, value))).collect(Collectors.toList()));
	}


	private boolean isValueEqual(Object beanValue, String actualValue) {
		if (beanValue == null) {
			return (actualValue == null);
		}
		if (actualValue == null) {
			return false;
		}
		if (beanValue instanceof Number) {
			return MathUtils.isEqual((Number) beanValue, new BigDecimal(actualValue));
		}
		return (beanValue.toString().equals(actualValue));
	}


	protected boolean isReverse() {
		return false;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public List<String> getValueList() {
		return this.valueList;
	}


	public void setValueList(List<String> valueList) {
		this.valueList = valueList;
	}
}
