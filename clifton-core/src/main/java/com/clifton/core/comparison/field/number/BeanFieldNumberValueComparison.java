package com.clifton.core.comparison.field.number;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.comparison.field.BaseBeanFieldValueComparison;
import com.clifton.core.util.AssertUtils;

import java.math.BigDecimal;


/**
 * The <code>BeanFieldNumberValueComparison</code> is an abstract class
 * used for comparing number bean fields to stated values.
 * <p/>
 * Supports SQL based value.
 *
 * @author manderson
 */
public abstract class BeanFieldNumberValueComparison<T extends IdentityObject> extends BaseBeanFieldValueComparison<T> {

	/**
	 * To be overridden by child classes
	 *
	 * @param n1
	 * @param n2
	 */
	public abstract boolean evaluateCondition(Number n1, Number n2);


	public abstract String getConditionString();


	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		Object beanValue = BeanUtils.getPropertyValue(bean, getBeanFieldName());
		AssertUtils.assertTrue((beanValue == null || beanValue instanceof Number), "Bean Field [%s] selected must be a number to perform numeric comparisons.", getBeanFieldName());
		Number number = beanValue instanceof Number ? (Number) beanValue : null;
		String actualValue = getActualValue(bean);
		boolean result = evaluateCondition(number, new BigDecimal(actualValue));

		if (context != null) {
			if (result) {
				context.recordTrueMessage("(" + getBeanFieldName() + ": " + beanValue + " " + getConditionString() + " " + actualValue + ")");
			}
			else {
				context.recordFalseMessage("(" + getBeanFieldName() + ": " + beanValue + " is not " + getConditionString() + " " + actualValue + ")");
			}
		}

		return result;
	}
}
