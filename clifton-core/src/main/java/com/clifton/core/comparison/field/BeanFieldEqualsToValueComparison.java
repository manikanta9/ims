package com.clifton.core.comparison.field;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.comparison.ComparisonContext;
import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>BeanFieldEqualsToValueComparison</code> class compares field value of the specified bean to the specified value.
 * It evaluates to true if both values are equal.
 * <p/>
 * Supports SQL based value.
 *
 * @author vgomelsky
 */
public class BeanFieldEqualsToValueComparison<T extends IdentityObject> extends BaseBeanFieldValueComparison<T> {

	@Override
	public boolean evaluate(T bean, ComparisonContext context) {
		boolean result = isReverse();
		Object beanValue = null;
		String actualValue = getActualValue(bean);
		if (bean != null) {
			beanValue = BeanUtils.getPropertyValue(bean, getBeanFieldName());
			result = compareValues(beanValue, actualValue);
		}

		// record comparison result message
		if (context != null) {
			String msg;
			if (isReverse() ? !result : result) {
				msg = ("(" + getBeanFieldName() + " = " + actualValue + ")");
			}
			else {
				msg = ("(" + getBeanFieldName() + " = " + beanValue + " instead of " + actualValue + ")");
			}
			if (result) {
				context.recordTrueMessage(msg);
			}
			else {
				context.recordFalseMessage(msg);
			}
		}
		return result;
	}


	protected boolean compareValues(Object beanValue, String actualValue) {
		boolean result = isReverse();
		if (beanValue == null) {
			if (actualValue == null) {
				result = !isReverse();
			}
		}
		else if (actualValue != null) {
			if (beanValue instanceof Number) {
				if (MathUtils.isEqual((Number) beanValue, new BigDecimal(actualValue))) {
					result = !isReverse();
				}
			}
			else if (beanValue.toString().equals(actualValue)) {
				result = !isReverse();
			}
		}
		return result;
	}


	protected boolean isReverse() {
		return false;
	}
}
