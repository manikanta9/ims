package com.clifton.core.comparison;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextImpl;
import com.clifton.core.util.AssertUtils;


/**
 * The <code>SimpleComparisonContext</code> class is a basic implementation of ComparisonContext.
 * It supports only one true/false message.
 *
 * @author vgomelsky
 */
public class SimpleComparisonContext extends ContextImpl implements ComparisonContext {

	private String trueMessage;
	private String falseMessage;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SimpleComparisonContext() {
		super(); // used in tests when Context is not applicable
	}


	public SimpleComparisonContext(Context context) {
		if (context != null) {
			setBeanMap(context.getBeanMap());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void clear() {
		super.clear();
		clearMessages();
	}


	@Override
	public void clearMessages() {
		this.trueMessage = null;
		this.falseMessage = null;
	}


	@Override
	public void recordFalseMessage(String message) {
		AssertUtils.assertNull(this.falseMessage, "Cannot overwrite falseMessage '%1s' with '%2s'", this.falseMessage, this.trueMessage);
		this.falseMessage = message;
	}


	@Override
	public void recordTrueMessage(String message) {
		AssertUtils.assertNull(this.trueMessage, "Cannot overwrite trueMessage '%1s' with '%2s'", this.trueMessage, this.falseMessage);
		this.trueMessage = message;
	}


	@Override
	public String getTrueMessage() {
		return this.trueMessage;
	}


	@Override
	public String getFalseMessage() {
		return this.falseMessage;
	}
}
