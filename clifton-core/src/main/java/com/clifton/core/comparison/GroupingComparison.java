package com.clifton.core.comparison;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>GroupingComparison</code> interface is a {@link Comparison} that has child comparisons.
 *
 * @author vgomelsky
 */
public interface GroupingComparison<T extends IdentityObject> extends Comparison<T> {

	/**
	 * Adds the specified child {@link Comparison} to this grouping comparison.
	 *
	 * @param comparison
	 */
	public void addChildComparison(Comparison<T> comparison);
}
