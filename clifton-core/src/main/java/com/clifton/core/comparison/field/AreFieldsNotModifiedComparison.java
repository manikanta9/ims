package com.clifton.core.comparison.field;


/**
 * The <code>AreFieldsNotModifiedComparison</code> takes an unsaved updated bean and compares it to the
 * original bean retrieved from the database.
 * <p>
 * Returns TRUE if ALL of the given fields have NOT been modified.
 * Returns FALSE if ANY of the given fields have been modified.
 * NOTE: System Managed Fields (Record Stamp) are always excluded from comparison
 *
 * @author manderson
 */
public class AreFieldsNotModifiedComparison extends AreFieldsModifiedComparison {


	@Override
	protected boolean isReverse() {
		return true;
	}
}
