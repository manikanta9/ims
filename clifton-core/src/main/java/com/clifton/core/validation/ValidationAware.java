package com.clifton.core.validation;


import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>ValidationAware</code> interface should be implemented by objects that want to provide their own validation.
 * This validation can be used by other parts of the system.  For example, SystemBean instances can implement this interface
 * so that they are validated when saved.
 *
 * @author vgomelsky
 */
public interface ValidationAware {

	/**
	 * Validates object that implements this interface.  The method should throw ValidationException
	 * with appropriate message if validation fails.
	 */
	public void validate() throws ValidationException;
}
