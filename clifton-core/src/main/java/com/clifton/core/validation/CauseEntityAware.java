package com.clifton.core.validation;

import com.clifton.core.beans.IdentityObject;


/**
 * The CauseEntityAware interface can be used with exceptions that were caused by a specific entity.
 *
 * @author vgomelsky
 */
public interface CauseEntityAware {

	/**
	 * Returns the bean that was the cause of this exception.
	 */
	public IdentityObject getCauseEntity();
}
