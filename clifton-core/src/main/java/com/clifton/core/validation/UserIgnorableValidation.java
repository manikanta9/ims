package com.clifton.core.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * <code>UserIgnorableValidation</code> is an annotation that can be put on a public service method to decorate a thrown
 * {@link UserIgnorableValidationException} with the url to call back with validation ignored. The exception's url is generated
 * using {@link com.clifton.core.context.ContextConventionUtils#getUrlFromMethodClassAndName(Class, String, boolean)} for the
 * class and method name where the annotation is located, and url parameters defined by
 * {@link UserIgnorableValidationAspect#USER_IGNORABLE_REQUEST_PARAMETERS}.
 * <p>
 * Service methods are methods of a class annotated with {@link org.springframework.stereotype.Service}.
 * <p>
 * Method signatures using this annotation should have a final <code>boolean</code> argument for 'ignoreValidation'. This parameter
 * is automatically appending to the redirection URL for the service via {@link UserIgnorableValidationAspect}. Failing to add this
 * argument to the signature will result in a log warning and a standard validation exception that cannot be user overridden.
 *
 * @author nickk
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface UserIgnorableValidation {

}
