package com.clifton.core.validation;


import java.io.Serializable;


/**
 * The <code>FieldError</code> class represents an error for a bean field.
 *
 * @author vgomelsky
 */
public class FieldError implements Serializable {

	private String id;
	private String msg;


	public FieldError(String fieldId, String errorMessage) {
		this.id = fieldId;
		this.msg = errorMessage;
	}


	/**
	 * @return the id
	 */
	public String getId() {
		return this.id;
	}


	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}


	/**
	 * @return the msg
	 */
	public String getMsg() {
		return this.msg;
	}


	/**
	 * @param msg the msg to set
	 */
	public void setMsg(String msg) {
		this.msg = msg;
	}
}
