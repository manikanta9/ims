package com.clifton.core.validation;


import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>Validator</code> interface should be implemented by various validators.
 * It's validate method should throw instances of {@link ValidationException} whenever a validation violation is encountered.
 *
 * @param <T> the type of the bean to be validated
 * @param <C> optional configuration that can be used by the validator.
 * @author vgomelsky
 */
public interface Validator<T, C> {

	/**
	 * Validates the specified bean and throws an instance of {@link ValidationException} whenever a validation violation is encountered.
	 *
	 * @param bean
	 * @param config
	 */
	public void validate(T bean, C config) throws ValidationException;
}
