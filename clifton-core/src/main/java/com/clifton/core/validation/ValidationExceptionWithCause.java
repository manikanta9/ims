package com.clifton.core.validation;

import com.clifton.core.util.validation.ValidationException;


/**
 * ValidationExceptionWithCause is an extension of the ValidationException, but supports setting cause table/id information
 * This is used most commonly for Rules processing so we can save the validation exception as a violation and have links to the cause for
 * easy drill downs
 * <p>
 * Example - Price is missing message with link to the Investment Security
 *
 * @author manderson
 */
public class ValidationExceptionWithCause extends ValidationException {

	private String causeTableName;
	private Long causeFkFieldId;


	public ValidationExceptionWithCause(String message) {
		super(message);
	}


	public ValidationExceptionWithCause(String message, Throwable cause) {
		super(message, cause);
	}


	public ValidationExceptionWithCause(String causeTableName, Number causeFkFieldId, String message) {
		super(message);
		this.causeTableName = causeTableName;
		this.causeFkFieldId = (causeFkFieldId != null ? causeFkFieldId.longValue() : null);
	}


	public ValidationExceptionWithCause(String causeTableName, Number causeFkFieldId, String message, Throwable cause) {
		super(message, cause);
		this.causeTableName = causeTableName;
		this.causeFkFieldId = (causeFkFieldId != null ? causeFkFieldId.longValue() : null);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getCauseTableName() {
		return this.causeTableName;
	}


	public void setCauseTableName(String causeTableName) {
		this.causeTableName = causeTableName;
	}


	public Long getCauseFkFieldId() {
		return this.causeFkFieldId;
	}


	public void setCauseFkFieldId(Long causeFkFieldId) {
		this.causeFkFieldId = causeFkFieldId;
	}
}
