package com.clifton.core.validation.hierarchy;


import com.clifton.core.beans.hierarchy.HierarchicalObject;
import com.clifton.core.util.validation.FieldValidationException;


/**
 * The <code>SameHierarchyParentException</code> exception should be thrown when a {@link HierarchicalObject}
 * is trying to point to itself.
 *
 * @author vgomelsky
 */
public class SameHierarchyParentException extends FieldValidationException {

	/**
	 * Constructs a new exception using default field name "parent.id" and default message.
	 * This is the preferred constructor.
	 */
	public SameHierarchyParentException() {
		this("parent.id");
	}


	/**
	 * Constructs a new exception using the specified field name and default message.
	 * This is the second preferred constructor.
	 *
	 * @param fieldName
	 */
	public SameHierarchyParentException(String fieldName) {
		this(fieldName, "An entity cannot have itself as parent.");
	}


	/**
	 * Constructs a new exception using the specified arguments.
	 * This is the least preferred constructor.
	 *
	 * @param fieldName
	 * @param message
	 */
	public SameHierarchyParentException(String fieldName, String message) {
		super(message, fieldName);
	}
}
