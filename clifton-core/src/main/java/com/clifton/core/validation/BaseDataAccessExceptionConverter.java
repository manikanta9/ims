package com.clifton.core.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.UpdatableOnlyEntity;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.converter.string.ObjectToStringConverter;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import org.hibernate.StaleObjectStateException;
import org.hibernate.exception.DataException;
import org.hibernate.exception.GenericJDBCException;

import java.io.Serializable;
import java.sql.DataTruncation;
import java.sql.SQLException;
import java.sql.SQLTimeoutException;


/**
 * The <code>BaseDataAccessExceptionConverter</code> class is an abstract class that converts database exceptions to corresponding {$link ValidationException}
 * instances with user friendly error messages.
 * <p>
 * This class implements MS SQL Server 2005 specific exception conversion.
 * <p>
 * Note: This is currently overridden in system (for ims) and portal-2 (for client port) projects for correct end user application support
 *
 * @author manderson
 */
public abstract class BaseDataAccessExceptionConverter implements Converter<RuntimeException, RuntimeException> {

	public static final int ERROR_CODE_DUPLICATE_KEY = 2601;
	public static final int ERROR_CODE_REFERENCE_CONSTRAINT = 547;
	public static final int ERROR_CODE_NOT_NULL = 515;
	public static final int ERROR_CODE_ARITHMETIC_OVERFLOW_DATA_TRUNCATION = 8115;
	public static final int ERROR_CODE_DATA_TRUNCATION = 8152;

	private DaoLocator daoLocator;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public abstract LabeledObject getSecurityUser(short id);


	@SuppressWarnings("unchecked")
	@Override
	public RuntimeException convert(RuntimeException from) {
		Throwable original = ExceptionUtils.getOriginalException(from);
		if (original instanceof StaleObjectStateException) {
			// get information about the DTO that failed
			Serializable dtoIdentity;
			Class<?> dtoClass;
			if (from instanceof DaoException) {
				IdentityObject dto = ((DaoException) from).getCauseEntity();
				dtoIdentity = dto.getIdentity();
				dtoClass = dto.getClass();
			}
			else {
				StaleObjectStateException staleException = (StaleObjectStateException) original;
				dtoIdentity = staleException.getIdentifier();
				dtoClass = CoreClassUtils.getClass(staleException.getEntityName());
			}

			// generate user friendly exception
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate((Class<IdentityObject>) dtoClass);
			IdentityObject dtoUpdated = dao.findByPrimaryKey(dtoIdentity);
			if (dtoUpdated == null) {
				return new ValidationException("The object was deleted from the database and no longer exists.", from);
			}
			if (!(dtoUpdated instanceof UpdatableOnlyEntity)) {
				LogUtils.error(getClass(), "Encountered StaleObjectStateException, but the entity is not an UpdatableEntity", original);
				return new ValidationException("The object was updated, but unable to determine who last updated the object.  Reload the object and redo your changes.", from);
			}
			UpdatableOnlyEntity updatableDto = (UpdatableOnlyEntity) dtoUpdated;
			LabeledObject user = getSecurityUser(updatableDto.getUpdateUserId());
			String userLabel = (user == null) ? "UNKNOWN USER WITH ID = " + updatableDto.getUpdateUserId() : user.getLabel();
			// TODO: show updated fields? log as warn?
			return new ValidationException(dtoClass.getSimpleName() + " with id = " + dtoIdentity + " was updated by " + userLabel + " on " + DateUtils.fromDate(updatableDto.getUpdateDate()) + ". Reload the " + dtoClass.getSimpleName() + " to see the changes.", from);
		}

		if (original instanceof SQLException) {
			SQLException orig = (SQLException) original;
			String message = orig.getMessage();

			// a cleaner way would be to look up constraint by naming to find out the column, but this is good for now
			switch (orig.getErrorCode()) {
				case ERROR_CODE_DUPLICATE_KEY:
					return convertFromDuplicateKeyException(from, orig);

				case ERROR_CODE_REFERENCE_CONSTRAINT:
					return convertFromReferenceConstraintException(from, orig);

				// Note: JTDS Drivers used to return DataTruncation Error, now for numeric values, Arithmetic Overflow seems to return as generic SQLException with it's code populated
				case ERROR_CODE_ARITHMETIC_OVERFLOW_DATA_TRUNCATION:
					return convertFromDataTruncationException(from, orig);

				// Note: JTDS Drivers used to return DataTruncation Error, now seems to return as generic SQLException with it's code populated
				case ERROR_CODE_DATA_TRUNCATION:
					return convertFromDataTruncationException(from, orig);

				default:
					if (orig instanceof DataTruncation) {
						return convertFromDataTruncationException(from, orig);
					}
					if (orig instanceof SQLTimeoutException) {
						return new ValidationException("The query has timed out. Please contact administrator.", from);
					}

					if (from instanceof DaoException) {
						IdentityObject dto = ((DaoException) from).getCauseEntity();
						if (orig.getErrorCode() == ERROR_CODE_NOT_NULL) {
							// MESSAGE EXAMPLE: Cannot insert the value NULL into column 'FincadArgumentID', table 'CliftonIMS.dbo.FincadArgumentValue'; column does not allow nulls. INSERT fails.
							int start = message.indexOf('\'') + 1;
							int end = message.indexOf('\'', start);
							String columnName = message.substring(start, end);

							ReadOnlyDAO<?> dao = getDaoLocator().locate((Class<IdentityObject>) dto.getClass());
							String fieldName = dao.getConfiguration().getBeanFieldName(columnName);
							return new FieldValidationException("The field '" + fieldName + "' for '" + BeanUtils.getLabel(dto) + "' is required.", fieldName, from);
						}
					}
					if (from instanceof GenericJDBCException) {
						GenericJDBCException hibernateException = (GenericJDBCException) from;
						return new RuntimeException("Unhandled Hibernate exception for SQL: " + hibernateException.getSQL(), from);
					}
					return new RuntimeException("Unhandled SQL error code: " + orig.getErrorCode(), from);
			}
		}


		return from;
	}


	public String getDataSourceNameFromException(RuntimeException from) {
		if (from instanceof DaoException) {
			DaoException ex = (DaoException) from;
			IdentityObject obj = ex.getCauseEntity();
			@SuppressWarnings("unchecked")
			ReadOnlyDAO<IdentityObject> dao = getDaoLocator().locate((Class<IdentityObject>) obj.getClass());
			if (dao != null) {
				return dao.getConfiguration().getTable().getDataSource();
			}
		}
		return null;
	}


	protected RuntimeException convertFromDuplicateKeyException(RuntimeException exception, SQLException sqlException) {
		String message = sqlException.getMessage();
		// MESSAGE EXAMPLE: Cannot insert duplicate key row in object 'dbo.SystemAuditType' with unique index 'ux_SystemAuditType_AuditTypeName'.
		int end = message.lastIndexOf('\'');
		int start = message.lastIndexOf('\'', end - 1);
		String[] indexName = message.substring(start, end).split("_");
		if (indexName.length < 2) {
			return new RuntimeException("Duplicate Key error but cannot determine details because constraint does not follow our naming conventions. Message: '" + message + "'", exception);
		}
		String firstColumn = null;
		StringBuilder msg = new StringBuilder("The field ");
		for (int i = 2; i < indexName.length; i++) {
			String columnName = indexName[i];
			if (i == 2) {
				firstColumn = columnName;
				if (indexName.length > 3) {
					msg.append("combination ");
				}
			}
			msg.append('\'');
			msg.append(columnName);
			msg.append('\'');
			if (i < (indexName.length - 1)) {
				msg.append(", ");
			}
		}
		msg.append(" does not allow duplicate values.");
		if (exception instanceof DaoException) {
			IdentityObject dto = ((DaoException) exception).getCauseEntity();
			msg.append(" Entity: ");
			msg.append(BeanUtils.getLabel(dto));
		}
		return new FieldValidationException(msg.toString(), firstColumn);
	}


	protected RuntimeException convertFromReferenceConstraintException(RuntimeException exception, SQLException sqlException) {
		String message = sqlException.getMessage();
		throw new RuntimeException(message, exception);
	}


	protected RuntimeException convertFromDataTruncationException(RuntimeException exception, SQLException sqlException) {
		// Data Truncation Exception may be hiding Arithmetic Overflow Exception
		SQLException next = sqlException.getNextException();
		String message = sqlException.getMessage();
		if (next != null) {
			if (!StringUtils.isEmpty(next.getMessage())) {
				message = next.getMessage();
			}
		}

		StringBuilder dtoString = new StringBuilder(16);
		try {
			// Special Handling for DaoExceptions since we have access to the dto bean
			if (exception instanceof DaoException) {
				message = processDaoException((DaoException) exception, message, dtoString);
			}
			else {
				Throwable cause = exception.getCause();
				DaoException daoExceptionCause = ExceptionUtils.getCauseException(exception, DaoException.class);
				if (daoExceptionCause != null) {
					message = processDaoException(daoExceptionCause, message, dtoString);
				}
				else {
					if (cause instanceof DataException) {
						message = cause.getMessage() + ": " + message;
						dtoString.append(((DataException) cause).getSQL());
					}
					else {
						message = exception.getMessage() + ": " + message;
					}
					message = message + " " + "Unable to determine specific field that exceeds max length.";
				}
			}
		}
		finally {
			// Always Log the original Error - can't return it to user because DataTruncation throws a CastClassException
			// in the above case when calling getNextWarning() during PropertyUtils.describe
			// because a SqlException for getNextException is not necessarily a SqlWarning so casting doesn't work
			LogUtils.error(LogCommand.ofThrowableAndMessage(getClass(), exception, () -> !StringUtils.isEmpty(dtoString.toString()) ? " All Field Values [" + dtoString.toString() + "]" : ""));
		}

		return new ValidationException(message);
	}


	protected String processDaoException(DaoException exception, String message, StringBuilder dtoString) {
		StringBuilder dtoFailedString = new StringBuilder(16);
		message = exception.getMessage() + ": " + message;
		try {
			populateDTOStringsForDataTruncationException(exception, dtoFailedString, dtoString);
			if (!StringUtils.isEmpty(dtoFailedString.toString())) {
				message = message + " " + dtoFailedString.toString();
			}
			else {
				message = message + " " + "Unable to find a specific field that exceeds max length.";
			}
		}
		catch (Exception e) {
			dtoString.append("Exception while trying to get list of failed fields for bean with Data Truncation error [").append(e.getMessage()).append("]");
			LogUtils.error(getClass(), e.getMessage(), e);
		}
		return message;
	}


	protected void populateDTOStringsForDataTruncationException(DaoException daoException, StringBuilder dtoFailedString, StringBuilder dtoString) {
		IdentityObject dto = daoException.getCauseEntity();
		@SuppressWarnings("unchecked")
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate((Class<IdentityObject>) dto.getClass());
		ObjectToStringConverter converter = new ObjectToStringConverter();

		for (Column column : CollectionUtils.getIterable(dao.getConfiguration().getColumnList())) {
			Object value = BeanUtils.getPropertyValue(dto, column.getBeanPropertyName());
			String stringValue = converter.convert(value);
			if (stringValue != null) {
				if (column.getMaxLength() != 0) {
					if (DataTypeNames.STRING == column.getDataType().getTypeName()) {
						if (stringValue.length() > column.getMaxLength()) {
							dtoFailedString.append(column.getName()).append(" has max length of ").append(column.getMaxLength()).append(". Value length ").append(((String) value).length()).append(" is too long. [").append(value).append("]").append(StringUtils.NEW_LINE);
						}
					}
					// Decimals only care about the "integer" portion
					if (DataTypeNames.DECIMAL == column.getDataType().getTypeName()) {
						int maxLength = column.getMaxLength() - column.getDataType().getPrecision();
						int integerPlaces = (stringValue.indexOf('.') == -1) ? stringValue.length() : stringValue.indexOf('.');
						if (integerPlaces > maxLength) {
							dtoFailedString.append(column.getName()).append(" integer portion has max length of ").append(maxLength).append(".  Value with integer portion length ").append(integerPlaces).append(" is too large. [").append(stringValue).append("]").append(StringUtils.NEW_LINE);
						}
					}
				}
				dtoString.append(column.getName()).append(": ").append(stringValue).append("|");
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
