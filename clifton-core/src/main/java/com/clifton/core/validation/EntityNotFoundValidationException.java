package com.clifton.core.validation;


import com.clifton.core.util.validation.ValidationException;


/**
 * The <code>EntityNotFoundValidationException</code> class is a run time validation exception that
 * should be thrown when a lookup for an entity that must exist fails.
 *
 * @author vgomelsky
 */
public class EntityNotFoundValidationException extends ValidationException {

	private final String entityName;


	/**
	 * Constructs a new entity not found validation runtime exception. Preserves the specified cause.
	 *
	 * @param message    the detail message
	 * @param entityName usually the table/bean bean name that fails validation
	 * @param cause
	 */
	public EntityNotFoundValidationException(String message, String entityName, Throwable cause) {
		super(message, cause);
		this.entityName = entityName;
	}


	/**
	 * Constructs a new entity not found validation runtime exception.
	 *
	 * @param message    the detail message
	 * @param entityName usually the table/bean bean name that fails validation
	 */
	public EntityNotFoundValidationException(String message, String entityName) {
		super(message);
		this.entityName = entityName;
	}


	/**
	 * Returns the name of the entity that caused validation problem.
	 * Entity Name is usually corresponding table or bean name.
	 *
	 * @return the entityName
	 */
	public String getEntityName() {
		return this.entityName;
	}
}
