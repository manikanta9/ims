package com.clifton.core.validation;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * The <code>UserIgnorableValidationException</code> is an extension of the ValidationException, except that it allows the user to bypass the exception by
 * providing an alternative method.
 * <p>
 * For example, investment calendar only allows events after 5 business days in the past to be updated, however, if needed a user can explicitly say they want
 * to still save the old event.  These can be used for things that can rarely happen so we want to prevent it by default, but still have a bypass around the
 * issue
 * <p>
 * In the event the method doesn't exist, or is not accessible via URL, then that exception is logged, but the user will still only receive a validation
 * exception
 *
 * @author manderson
 */
public class UserIgnorableValidationException extends ValidationException {

	/**
	 * The save method url form submission should use if the user accepts option to bypass this validation
	 */
	private String ignoreExceptionUrl;

	/**
	 * The parameters that should be passed in the method url call if the user accepts option to bypass this validation
	 */
	private final Map<String, String> ignoreExceptionParameters = new HashMap<>();

	/**
	 * The confirmation message that should be displayed to the user asking if the exception should be ignored
	 */
	private String confirmationMessage = "Would you like to bypass this exception and continue?";


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	/**
	 * This constructor is necessary in order to properly deserialize the exception with Jackson when it is thrown by an external service call.
	 */
	public UserIgnorableValidationException(String message) {
		super(message);
		this.ignoreExceptionUrl = null;
	}


	public UserIgnorableValidationException(Class<?> ignoreExceptionMethodClass, String ignoreExceptionMethodName, String message) {
		super(message);
		setIgnoreExceptionUrl(ignoreExceptionMethodClass, ignoreExceptionMethodName);
	}


	public UserIgnorableValidationException setIgnoreExceptionUrl(Class<?> ignoreExceptionMethodClass, String ignoreExceptionMethodName) {
		String url = null;
		try {
			url = ContextConventionUtils.getUrlFromMethodClassAndName(ignoreExceptionMethodClass, ignoreExceptionMethodName, false);
		}
		// If there is an issue with getting the URL from the Bean/Method then we log the error, but don't throw it here
		// so users still get the original Validation Exception without the bypass option.
		catch (Throwable e) {
			LogUtils.error(getClass(), "Error occurred when getting URL From Method and ClassName. IgnoreExceptionMethodClass: " + ignoreExceptionMethodName + " IgnoreExceptionMethodName: " + ignoreExceptionMethodName + " Message: " + getMessage(), e);
		}
		this.ignoreExceptionUrl = url;
		return this;
	}


	public UserIgnorableValidationException addParameter(String parameterName, String parameterValue) {
		this.ignoreExceptionParameters.put(parameterName, parameterValue);
		return this;
	}


	public UserIgnorableValidationException addParameters(Map<String, String> parameterMap) {
		if (parameterMap != null) {
			this.ignoreExceptionParameters.putAll(parameterMap);
		}
		return this;
	}


	public String getIgnoreExceptionUrlWithParameters() {
		String url = getIgnoreExceptionUrl();
		if (!StringUtils.isEmpty(url) && !CollectionUtils.isEmpty(getIgnoreExceptionParameters())) {
			url = url + getIgnoreExceptionParameters().entrySet().stream()
					.map(entry -> String.join("=", entry.getKey(), entry.getValue()))
					.collect(Collectors.joining("&", "?", ""));
		}

		return url;
	}


	public UserIgnorableValidationException setConfirmationMessage(String confirmationMessage) {
		this.confirmationMessage = confirmationMessage;
		return this;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getIgnoreExceptionUrl() {
		return this.ignoreExceptionUrl;
	}


	public Map<String, String> getIgnoreExceptionParameters() {
		return this.ignoreExceptionParameters;
	}


	public String getConfirmationMessage() {
		return this.confirmationMessage;
	}



}
