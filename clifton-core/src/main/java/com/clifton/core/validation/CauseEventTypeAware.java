package com.clifton.core.validation;

import com.clifton.core.dataaccess.dao.event.DaoEventTypes;


/**
 * The CauseEventTypeAware interface can be used with exceptions that were caused by a specific DAO event type.
 *
 * @author vgomelsky
 */
public interface CauseEventTypeAware {

	/**
	 * Returns the cause event: INSERT, UPDATE, DELETE.
	 */
	public DaoEventTypes getCauseEvent();
}
