package com.clifton.core.validation.hierarchy;


import com.clifton.core.beans.hierarchy.HierarchicalObject;
import com.clifton.core.util.validation.FieldValidationException;


/**
 * The <code>ParentIsChildException</code> exception should be thrown when a {@link HierarchicalObject}
 * is trying to point to one of its children (parent cannot be a child).
 *
 * @author vgomelsky
 */
public class ParentIsChildException extends FieldValidationException {

	/**
	 * Constructs a new exception using default field name "parent.id" and default message.
	 * This is the preferred constructor.
	 */
	public ParentIsChildException() {
		this("parent.id");
	}


	/**
	 * Constructs a new exception using the specified field name and default message.
	 * This is the second preferred constructor.
	 *
	 * @param fieldName
	 */
	public ParentIsChildException(String fieldName) {
		this(fieldName, "Cannot select a child of this entity as its parent.");
	}


	/**
	 * Constructs a new exception using the specified arguments.
	 * This is the least preferred constructor.
	 *
	 * @param fieldName
	 * @param message
	 */
	public ParentIsChildException(String fieldName, String message) {
		super(message, fieldName);
	}
}
