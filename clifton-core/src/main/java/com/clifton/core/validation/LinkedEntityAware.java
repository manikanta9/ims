package com.clifton.core.validation;

import com.clifton.core.beans.IdentityObject;


/**
 * The LinkedEntityAware interface can be used with exceptions that were caused by a specific entity
 * that is linked to this entity. It is similar to {@link CauseEntityAware} and is usually used when
 * cause entity is a new bean that hasn't been created yet. Linked entity can provide additional information.
 *
 * @author vgomelsky
 */
public interface LinkedEntityAware {

	/**
	 * Returns the bean that was the link of this exception.
	 */
	public IdentityObject getLinkedEntity();
}
