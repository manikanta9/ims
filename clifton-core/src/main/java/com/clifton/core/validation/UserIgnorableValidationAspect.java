package com.clifton.core.validation;


import com.clifton.core.beans.CoreMethodUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.AfterThrowing;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.CodeSignature;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Supplier;


/**
 * <code>UserIgnorableValidationAspect</code> is an aspect for weaving behavior to service methods annotated
 * with {@link UserIgnorableValidation} when a {@link UserIgnorableValidationException} is thrown.
 * <p>
 * Since inheritance of annotations does not work the same way on methods as it does on classes, there are difference
 * advice entries for methods annotated directly vs those inherited. The functionality should be the same in either case.
 *
 * @author nickk
 * @see UserIgnorableValidation
 */
@Aspect
@Component
@Configurable
public class UserIgnorableValidationAspect {

	/**
	 * Url parameters to append to an annotated service upon validation exception. The parameters consist of <code>?ignoreValidation=true</code>.
	 */
	public static final String IGNORE_VALIDATION_URL_KEY = "ignoreValidation";
	public static final Map<String, String> USER_IGNORABLE_REQUEST_PARAMETERS = ((Supplier<Map<String, String>>) () -> {
		Map<String, String> map = new HashMap<>();
		map.put(IGNORE_VALIDATION_URL_KEY, Boolean.TRUE.toString());
		return Collections.unmodifiableMap(map);
	}).get();


	@Pointcut("@annotation(com.clifton.core.validation.UserIgnorableValidation)")
	public void userIgnorableValidationAnnotated() {
		// empty
	}


	@Pointcut("within(@org.springframework.stereotype.Service *) && execution(public * com.clifton..*.*(..))")
	public void publicCliftonServiceMethod() {
		// empty
	}


	@Pointcut("!userIgnorableValidationAnnotated() && publicCliftonServiceMethod()")
	public void publicCliftonServiceMethodNotAnnotatedWithUserIgnorableValidation() {
		// empty
	}


	@Pointcut("userIgnorableValidationAnnotated() && publicCliftonServiceMethod()")
	public void publicCliftonServiceMethodAnnotatedWithUserIgnorableValidation() {
		// empty
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@AfterThrowing(pointcut = "publicCliftonServiceMethodNotAnnotatedWithUserIgnorableValidation()", throwing = "exception")
	public void decorateUserIgnorableValidationException(JoinPoint pointcut, Throwable exception) {
		if (exception instanceof UserIgnorableValidationException) {
			Signature signature = pointcut.getSignature();
			Method method = CollectionUtils.getFirstElement(CoreMethodUtils.getMethodList(signature.getDeclaringType(), signature.getName()));
			if (method != null && AnnotationUtils.isAnnotationPresent(method, UserIgnorableValidation.class)) {
				setUserIgnorableValidationExceptionUrl((UserIgnorableValidationException) exception, signature);
			}
		}
	}


	@AfterThrowing(pointcut = "publicCliftonServiceMethodAnnotatedWithUserIgnorableValidation()", throwing = "exception")
	public void decorateUserIgnorableValidationExceptionAnnotated(JoinPoint pointcut, Throwable exception) {
		if (exception instanceof UserIgnorableValidationException) {
			setUserIgnorableValidationExceptionUrl((UserIgnorableValidationException) exception, pointcut.getSignature());
		}
	}


	/**
	 * Conditionally sets a {@link UserIgnorableValidationException} if the signature/method includes a boolean or Boolean argument with a name matching the value of {@link #IGNORE_VALIDATION_URL_KEY}
	 */
	private void setUserIgnorableValidationExceptionUrl(UserIgnorableValidationException exception, Signature signature) {
		if (StringUtils.isEmpty(exception.getIgnoreExceptionUrlWithParameters())) {
			if (signature instanceof CodeSignature) {
				CodeSignature codeSignature = (CodeSignature) signature;
				String[] parameterNames = codeSignature.getParameterNames();
				if (!ArrayUtils.isEmpty(parameterNames)) {
					int ignoreValidationIndex = 0;
					for (String parameterName : parameterNames) {
						if (IGNORE_VALIDATION_URL_KEY.equals(parameterName)) {
							break;
						}
						ignoreValidationIndex++;
					}
					if (ignoreValidationIndex < parameterNames.length) {
						// parameter names and types should be same size and order
						Class<?> parameterType = codeSignature.getParameterTypes()[ignoreValidationIndex];
						if (boolean.class.isAssignableFrom(parameterType) || Boolean.class.isAssignableFrom(parameterType)) {
							exception.addParameters(USER_IGNORABLE_REQUEST_PARAMETERS);
							exception.setIgnoreExceptionUrl(signature.getDeclaringType(), signature.getName());
						}
					}
				}
			}
		}
	}
}
