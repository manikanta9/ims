package com.clifton.core.validation.hierarchy;


import com.clifton.core.beans.LabeledObject;
import com.clifton.core.beans.hierarchy.HierarchicalObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.BaseDaoEventObserver;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.util.CollectionUtils;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.List;


/**
 * The <code>HierarchicalObjectObserver</code> class provides basic implementation for validating {@link HierarchicalObject}s.
 * Performs SameParentAsChild & Parent is Child validation to avoid circular dependencies
 *
 * @author vgomelsky
 */
@Component
public class HierarchicalObjectObserver<T extends HierarchicalObject<T>> extends BaseDaoEventObserver<T> {

	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<T> dao, @SuppressWarnings("unused") DaoEventTypes event, T bean) {
		// validate only existing entities with parents
		if (!bean.isNewBean() && bean.getParent() != null) {
			Serializable beanId = bean.getIdentity();
			Serializable parentId = bean.getParent().getIdentity();
			if (beanId.equals(parentId)) {
				if (bean instanceof LabeledObject) {
					throw new SameHierarchyParentException("parent.id", "Entity [" + ((LabeledObject) bean).getLabel() + "] cannot have itself as its parent.");
				}
				throw new SameHierarchyParentException();
			}
			// make sure parent is also not a child, grand child, etc of this entity
			validateHierarchyChildren(beanId, parentId, dao);
		}
	}


	/**
	 * Throws {@link ParentIsChildException} exception if specified bean's child, grand child, etc. is the same as the specified parent.
	 */
	private void validateHierarchyChildren(Serializable beanId, Serializable parentId, ReadOnlyDAO<T> dao) {
		List<T> childList = dao.findByField("parent.id", beanId);
		for (T child : CollectionUtils.getIterable(childList)) {
			if (child.getIdentity().equals(parentId)) {
				if (child instanceof LabeledObject) {
					throw new ParentIsChildException("parent.id", "Child [" + ((LabeledObject) child).getLabel() + "] entity is also referenced as a parent.");
				}
				throw new ParentIsChildException();
			}
			validateHierarchyChildren(child.getIdentity(), parentId, dao);
		}
	}
}
