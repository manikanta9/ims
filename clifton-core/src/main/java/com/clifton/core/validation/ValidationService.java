package com.clifton.core.validation;


import java.util.List;
import java.util.Map;


/**
 * The <code>ValidationService</code> interface defines methods for getting validation information.
 *
 * @author vgomelsky
 */
public interface ValidationService {

	/**
	 * Returns a List of field Maps for fields that need to be validated for a bean with the specified name.
	 * Each map represents a field with "name" attribute along with other attributes used for validation.
	 *
	 * @param beanName
	 */
	public List<Map<String, Object>> getValidationMetaData(String beanName);
}
