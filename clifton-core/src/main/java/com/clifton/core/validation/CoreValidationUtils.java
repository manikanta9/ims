package com.clifton.core.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.compare.CoreCompareUtils;
import com.clifton.core.util.validation.FieldValidationException;

import java.util.ArrayList;
import java.util.List;


/**
 * The {@link CoreValidationUtils} class provides validation utility/helper methods.
 *
 * @author vgomelsky
 */
public class CoreValidationUtils {


	/**
	 * Validates the list of modified fields between the two given entities to verify that all modified fields are allowed modifiable fields.
	 *
	 * @param bean             the modified bean
	 * @param originalBean     the original bean
	 * @param modifiableFields the list of modifiable field names
	 * @param <T>              the entity type
	 * @throws FieldValidationException on validation failure
	 * @see #assertDisallowedModifiedFields(IdentityObject, IdentityObject, String...)
	 */
	public static <T extends IdentityObject> void assertAllowedModifiedFields(T bean, T originalBean, String... modifiableFields) {
		List<String> disallowedModifiedFieldList = CoreCompareUtils.getNoEqualPropertiesWithSetters(bean, originalBean, false, modifiableFields);
		if (CollectionUtils.getSize(disallowedModifiedFieldList) > 0) {
			throw new FieldValidationException(String.format("Modifications to the given fields are not allowed: %s", CollectionUtils.toString(disallowedModifiedFieldList, 10)), disallowedModifiedFieldList.get(0));
		}
	}


	/**
	 * Validates the given list of fields between the two given entities to verify that the fields have not been modified.
	 *
	 * @param bean                the modified bean
	 * @param originalBean        the original bean
	 * @param nonModifiableFields the list of non-modifiable field names
	 * @param <T>                 the entity type
	 * @throws FieldValidationException on validation failure
	 * @see #assertAllowedModifiedFields(IdentityObject, IdentityObject, String...)
	 */
	public static <T extends IdentityObject> void assertDisallowedModifiedFields(T bean, T originalBean, String... nonModifiableFields) {
		List<String> violationList = new ArrayList<>();
		for (String field : nonModifiableFields) {
			Object newValue = BeanUtils.getPropertyValue(originalBean, field);
			Object originalValue = BeanUtils.getPropertyValue(bean, field);
			if (CompareUtils.compare(newValue, originalValue) != 0) {
				violationList.add(field);
			}
		}
		if (!violationList.isEmpty()) {
			throw new FieldValidationException(String.format("Modifications to the given fields are not allowed: %s", CollectionUtils.toString(violationList, 10)), violationList.get(0));
		}
	}
}
