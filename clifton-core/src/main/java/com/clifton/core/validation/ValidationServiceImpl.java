package com.clifton.core.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ValidationServiceImpl</code> class provides basic implementation for the {@link ValidationService} interface.
 *
 * @author vgomelsky
 */
@Service
public class ValidationServiceImpl implements ValidationService {

	private DaoLocator daoLocator;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SecureMethod(disableSecurity = true)
	public List<Map<String, Object>> getValidationMetaData(String beanName) {
		String tableName = StringUtils.capitalize(beanName);
		ReadOnlyDAO<? extends IdentityObject> dao = getDaoLocator().locate(tableName);
		DAOConfiguration<? extends IdentityObject> daoConfiguration = dao.getConfiguration();

		List<Map<String, Object>> result = new ArrayList<>();
		for (Column column : daoConfiguration.getColumnList()) {
			// skip validation for fields that are not updated
			if (BeanUtils.isSystemManagedField(column.getBeanPropertyName()) || (column.getColumnExcludedFromInserts() != null && column.getColumnExcludedFromInserts())
					|| (column.getColumnExcludedFromUpdates() != null && column.getColumnExcludedFromUpdates())) {
				continue;
			}
			Map<String, Object> fieldDefinition = new HashMap<>();
			if (column.getColumnRequiredForValidation() && DataTypes.BIT != column.getDataType()) {
				fieldDefinition.put("required", true);
			}
			if (column.getMaxLength() > 0) {
				fieldDefinition.put("maxLength", column.getMaxLength());
			}
			if (!fieldDefinition.isEmpty()) {
				fieldDefinition.put("name", column.getBeanPropertyName());
				result.add(fieldDefinition);
			}
		}

		return result;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
