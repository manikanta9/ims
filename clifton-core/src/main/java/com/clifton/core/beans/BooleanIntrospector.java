package com.clifton.core.beans;

import org.apache.commons.beanutils.BeanIntrospector;
import org.apache.commons.beanutils.IntrospectionContext;
import org.apache.commons.lang.WordUtils;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Method;


/**
 * This was created to address and issue with BeanUtils - the latest version 1.8.3 handles this;
 * however it is loaded as a dependency of another library right now so this was created as a workaround
 * this article explains the issue/solution:
 * <p>
 * http://stackoverflow.com/questions/5266531/java-beans-beanutils-and-the-boolean-wrapper-class
 *
 * @author stevenf on 8/13/2015
 */
public class BooleanIntrospector implements BeanIntrospector {

	@Override
	public void introspect(IntrospectionContext icontext) throws IntrospectionException {
		for (Method m : icontext.getTargetClass().getMethods()) {
			if (m.getName().startsWith("is") && Boolean.class.equals(m.getReturnType())) {
				String propertyName = getPropertyName(m);
				PropertyDescriptor pd = icontext.getPropertyDescriptor(propertyName);

				if (pd == null) {
					icontext.addPropertyDescriptor(new PropertyDescriptor(propertyName, m, getWriteMethod(icontext.getTargetClass(), propertyName)));
				}
				else if (pd.getReadMethod() == null) {
					pd.setReadMethod(m);
				}
			}
		}
	}


	private String getPropertyName(Method m) {
		return WordUtils.uncapitalize(m.getName().substring(2, m.getName().length()));
	}


	private Method getWriteMethod(Class<?> clazz, String propertyName) {
		try {
			return clazz.getMethod("get" + WordUtils.capitalize(propertyName));
		}
		catch (NoSuchMethodException e) {
			return null;
		}
	}
}
