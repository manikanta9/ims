package com.clifton.core.beans.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>ValueIgnoringGetter</code> marks a getter method that ignores the field value and may return a value that is not the same as the value of the corresponding field.
 *
 * This annotation can be used by various parts of the systems like unit tests that try to dynamically test setting/getting a property value (skip for this annotation)
 *
 * @author vgomelsky
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface ValueIgnoringGetter {
	// empty
}
