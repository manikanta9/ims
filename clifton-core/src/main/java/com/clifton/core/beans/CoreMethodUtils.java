package com.clifton.core.beans;


import com.clifton.core.util.CollectionUtils;

import java.lang.reflect.Method;
import java.util.List;


/**
 * The {@link CoreMethodUtils} class provides utility methods that simplify working with class methods.
 *
 * @author vgomelsky
 */
public class CoreMethodUtils {


	/**
	 * Get a complete list of methods for a given name
	 */
	public static List<Method> getMethodList(Class<?> clazz, String methodName) {
		return BeanUtils.filter(CollectionUtils.createList(clazz.getMethods()), Method::getName, methodName);
	}
}
