package com.clifton.core.beans.common;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.AddressUtils;


/**
 * The <code>Address</code> contains address information related fields.
 *
 * @author manderson
 */
public class AddressEntity extends BaseEntity<Integer> implements AddressObject {

	private String addressLine1;
	private String addressLine2;
	private String addressLine3;

	private String city;
	private String state;
	private String postalCode;
	private String country;


	@Override
	public String getAddressLabel() {
		return AddressUtils.getAddressLabel(this);
	}


	@Override
	public String getCityState() {
		return AddressUtils.getCityState(this);
	}


	@Override
	public String getCityStatePostalCode() {
		return AddressUtils.getCityStatePostalCode(this);
	}


	@Override
	public String getAddressLine1() {
		return this.addressLine1;
	}


	@Override
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}


	@Override
	public String getAddressLine2() {
		return this.addressLine2;
	}


	@Override
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}


	@Override
	public String getAddressLine3() {
		return this.addressLine3;
	}


	@Override
	public void setAddressLine3(String addressLine3) {
		this.addressLine3 = addressLine3;
	}


	@Override
	public String getCity() {
		return this.city;
	}


	@Override
	public void setCity(String city) {
		this.city = city;
	}


	@Override
	public String getState() {
		return this.state;
	}


	@Override
	public void setState(String state) {
		this.state = state;
	}


	@Override
	public String getPostalCode() {
		return this.postalCode;
	}


	@Override
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}


	@Override
	public String getCountry() {
		return this.country;
	}


	@Override
	public void setCountry(String country) {
		this.country = country;
	}
}
