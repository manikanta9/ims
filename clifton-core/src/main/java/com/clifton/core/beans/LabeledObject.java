package com.clifton.core.beans;


/**
 * The <code>LabeledObject</code> interface should be implemented by beans that can display
 * user friendly representation of their state.
 *
 * @author vgomelsky
 */
public interface LabeledObject {

	/**
	 * Returns user friendly String representation of this object.
	 */
	public String getLabel();
}
