package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.web.bind.WebBindingDataRetriever;

import java.beans.PropertyEditorSupport;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>JsonPropertyEditor</code> class converts JSON strings to corresponding {@link ArrayList}'s of beans or Strings.
 * Bean type can be specified by setting the <code>beanClassPropertyName</code> property which defaults to "class".
 * Otherwise, uses the <code>defaultBeanClass</code>
 *
 * @author vgomelsky
 */
public class JsonPropertyEditor<T extends IdentityObject> extends PropertyEditorSupport {

	private final JsonStringToMapConverter fromJsonConverter = new JsonStringToMapConverter();
	private Class<?> defaultBeanClass = SearchRestriction.class;
	private String beanClassPropertyName = "class";
	private final Map<Class<?>, PropertyEditorSupport> propertyEditorMap;

	private final WebBindingDataRetriever webBindingDataRetriever;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonPropertyEditor() {
		this(null);
	}


	public JsonPropertyEditor(Map<Class<?>, PropertyEditorSupport> propertyEditorMap) {
		this(propertyEditorMap, null);
	}


	public JsonPropertyEditor(Map<Class<?>, PropertyEditorSupport> propertyEditorMap, WebBindingDataRetriever webBindingDataRetriever) {
		super();
		this.propertyEditorMap = propertyEditorMap;
		this.webBindingDataRetriever = webBindingDataRetriever;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getAsText() {
		throw new IllegalArgumentException("To JSON conversion is not supported here.");
	}


	@Override
	@ValueChangingSetter
	public void setAsText(String text) {
		List<Object> result = null;
		if (text != null) {
			Map<String, Object> map = this.fromJsonConverter.convert(text);
			if (map != null && !map.isEmpty()) {
				if (map.size() > 1) {
					throw new IllegalArgumentException("Expected only one map element: " + map);
				}
				List<?> beanList = (List<?>) map.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
				int beanCount = CollectionUtils.getSize(beanList);
				if (beanCount > 0) {
					result = processEntityList(beanList);
				}
			}
		}
		setValue(result);
	}


	@SuppressWarnings("unchecked") // We construct the list in our JSON Converter so the type casts should be safe.
	private List<Object> processEntityList(List<?> beanList) {
		// check the type of the first element: create corresponding objects if map of property name/value pairs
		if (beanList.get(0) instanceof Map) {
			List<Object> result = new ArrayList<>();
			for (Map<String, Object> beanProps : (List<Map<String, Object>>) beanList) {
				Class<?> beanClass = getDefaultBeanClass();
				Object beanClassName = beanProps.remove(getBeanClassPropertyName());
				if (beanClassName != null) {
					beanClass = CoreClassUtils.getClass(beanClassName.toString());
				}
				Object bean = null;
				if (IdentityObject.class.isAssignableFrom(beanClass)) {
					// DTO: get existing instance and override only properties that are being updated
					Object primaryKeyValue = beanProps.remove("id");
					if (primaryKeyValue != null) {
						int primaryKey = Integer.parseInt(primaryKeyValue.toString());
						if (primaryKey > 0) {
							bean = getWebBindingDataRetriever().getEntity((Class<T>) beanClass, primaryKey);
						}
					}
				}
				if (bean == null) {
					bean = BeanUtils.newInstance(beanClass);
				}

				for (Map.Entry<String, Object> propertyEntry : beanProps.entrySet()) {
					String propertyName = propertyEntry.getKey();
					Object propertyValue = propertyEntry.getValue();
					// if DTO property then set to null if id == -1 and getDTO if existing DTO
					if (propertyName.endsWith(".id")) {
						String dtoProperty = propertyName.substring(0, propertyName.length() - 3);
						Class<?> dtoClass = BeanUtils.getPropertyType(beanClass, dtoProperty);
						if (IdentityObject.class.isAssignableFrom(dtoClass)) {
							int id = StringUtils.isEmpty((String) propertyValue) ? -1 : Integer.parseInt((String) propertyValue);
							propertyValue = null;
							propertyName = dtoProperty;
							if (id != -1) {
								propertyValue = getWebBindingDataRetriever().getEntity((Class<T>) dtoClass, id);
							}
						}
					}
					else if (propertyValue instanceof List) {
						List<Object> propertyList = (List<Object>) propertyValue;
						if (!CollectionUtils.isEmpty(propertyList)) {
							propertyValue = processEntityList(propertyList);
						}
					}
					BeanUtils.setPropertyValue(bean, propertyName, propertyValue, this.propertyEditorMap);
				}
				result.add(bean);
			}
			return result;
		}
		else {
			// an array of strings
			return (List<Object>) beanList;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public Class<?> getDefaultBeanClass() {
		return this.defaultBeanClass;
	}


	public void setDefaultBeanClass(Class<?> defaultBeanClass) {
		this.defaultBeanClass = defaultBeanClass;
	}


	public String getBeanClassPropertyName() {
		return this.beanClassPropertyName;
	}


	public void setBeanClassPropertyName(String beanClassPropertyName) {
		this.beanClassPropertyName = beanClassPropertyName;
	}


	public WebBindingDataRetriever getWebBindingDataRetriever() {
		return this.webBindingDataRetriever;
	}
}
