package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.StringUtils;

import java.beans.PropertyEditorSupport;


public class IntegerArrayPropertyEditor extends PropertyEditorSupport {

	@Override
	@ValueChangingSetter
	public void setAsText(String text) {
		if (StringUtils.isEmpty(text)) {
			setValue(null);
		}
		else {
			String[] fromElements = text.replaceAll("[\\[\\]]", "")
					.split(text.contains("::") ? "::" : ",");
			Integer[] result = new Integer[fromElements.length];
			for (int i = 0; i < fromElements.length; i++) {
				result[i] = Integer.parseInt(fromElements[i]);
			}
			setValue(result);
		}
	}
}
