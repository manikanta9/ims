package com.clifton.core.beans;


import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


/**
 * The <code>BatchEntityHolder</code> class accumulates entities that need to be inserted/updated/deleted.
 * It can be useful in batch jobs that rebuild objects.
 *
 * @param <T>
 * @author vgomelsky
 */
public class BatchEntityHolder<T extends IdentityObject> {

	private final List<T> insertList = new ArrayList<>();
	private final List<T> updateList = new ArrayList<>();
	private final List<T> deleteList = new ArrayList<>();

	private final String name;


	/**
	 * Constructs a new entity holder using the specified name.
	 * The name is used in toString method to create a summary that is user friendly.
	 */
	public BatchEntityHolder(String name) {
		this.name = name;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(128);
		if (this.name != null) {
			result.append(this.name);
			result.append(": ");
		}
		result.append(this.insertList.size());
		result.append(" inserts, ");
		result.append(this.updateList.size());
		result.append(" updates, ");
		result.append(this.deleteList.size());
		result.append(" deletes.");

		return result.toString();
	}


	/**
	 * Adds all insert/update/delete lists from the specified argument to the corresponding lists of this entity.
	 *
	 * @param batch
	 */
	public void addAll(BatchEntityHolder<T> batch) {
		addAllToInsert(batch.getInsertList());
		addAllToUpdate(batch.getUpdateList());
		addAllToDelete(batch.getDeleteList());
	}


	public void addToInsert(T bean) {
		this.insertList.add(bean);
	}


	public void addAllToInsert(Collection<T> beans) {
		this.insertList.addAll(beans);
	}


	public void addToUpdate(T bean) {
		this.updateList.add(bean);
	}


	public void addAllToUpdate(Collection<T> beans) {
		this.updateList.addAll(beans);
	}


	public void addToDelete(T bean) {
		this.deleteList.add(bean);
	}


	public void addAllToDelete(Collection<T> beans) {
		this.deleteList.addAll(beans);
	}


	public List<T> getInsertList() {
		return this.insertList;
	}


	public List<T> getUpdateList() {
		return this.updateList;
	}


	public List<T> getDeleteList() {
		return this.deleteList;
	}


	/**
	 * Returns combined insert and update lists.
	 */
	public List<T> getSaveList() {
		List<T> result = new ArrayList<>();
		result.addAll(this.insertList);
		result.addAll(this.updateList);
		return result;
	}
}
