package com.clifton.core.beans;


/**
 * The <code>NamedObject</code> interface defines an object with name/description properties that also has id and label
 *
 * @author vgomelsky
 */
public interface NamedObject extends IdentityObject, LabeledObject {

	public String getName();


	public void setName(String name);


	public String getDescription();


	public void setDescription(String description);
}
