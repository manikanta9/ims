package com.clifton.core.beans.hierarchy;


import com.clifton.core.beans.BaseEntity;

import java.io.Serializable;


/**
 * The <code>HierarchicalEntity</code> is a simple hierarchical DTO.
 *
 * @author vgomelsky
 */
public abstract class HierarchicalEntity<T extends HierarchicalEntity<T, ID>, ID extends Serializable> extends BaseEntity<ID> implements HierarchicalObject<T> {

	private T parent;


	@Override
	public T getParent() {
		return this.parent;
	}


	public void setParent(T parent) {
		this.parent = parent;
	}


	/**
	 * By default the max depth is null which
	 * implies no restriction.  Can be overridden
	 * by subclasses to return any actual restrictions.
	 */
	@Override
	public Integer getMaxDepth() {
		return null;
	}


	@Override
	public int getLevel() {
		int level = 1;
		if (getParent() != null) {
			return getParent().getLevel() + 1;
		}
		return level;
	}


	@Override
	public T getRootParent() {
		return getParentAtLevel(1);
	}


	@Override
	@SuppressWarnings("unchecked")
	public T getParentAtLevel(int level) {
		int currentLevel = getLevel();

		if (currentLevel <= level) {
			return (T) this;
		}

		T root = getParent();
		currentLevel--;

		while (currentLevel > level && root != null) {
			root = root.getParent();
			currentLevel--;
		}

		return root;
	}


	@Override
	public boolean isLeaf() {
		Integer maxDepth = getMaxDepth();
		if (maxDepth == null || maxDepth > getLevel()) {
			return false;
		}

		return true;
	}
}
