package com.clifton.core.beans;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>BaseEntity</code> class should be extended by all DTO objects that represent database entities.
 *
 * @author vgomelsky
 */
public abstract class BaseEntity<T extends Serializable> extends BaseUpdatableOnlyEntity<T> implements Entity<T> {

	private short createUserId;
	private Date createDate;


	@Override
	public short getCreateUserId() {
		return this.createUserId;
	}


	@Override
	public void setCreateUserId(short createUserId) {
		this.createUserId = createUserId;
	}


	@Override
	public Date getCreateDate() {
		return this.createDate;
	}


	@Override
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}
}
