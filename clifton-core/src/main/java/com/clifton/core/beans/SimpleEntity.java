package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>SimpleEntity</code> defines methods used by all DTO objects that represent database entities.
 * <p/>
 * NOTE: This interface does not implement {@link IdentityObject} because the {@link IdentityObject}
 * interface makes this interface unusable as hibernate proxy used on objects that can be lazily loaded.
 *
 * @param <T>
 * @author mwacker
 */
public interface SimpleEntity<T extends Serializable> {

	public T getIdentity();


	public boolean isNewBean();


	/**
	 * Returns id that uniquely identifies this object.
	 */
	public T getId();


	/**
	 * Sets id that uniquely identifies this object.
	 */
	public void setId(T id);
}
