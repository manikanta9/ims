package com.clifton.core.beans;


import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import java.io.Serializable;


/**
 * The <code>BaseSimpleEntity</code> class should be extended by all DTO objects that represent database entities.
 * <p>
 * The class is annotated with JsonIdentityInfo to facilitate dealing with circular references during json migrations.
 * This annotation tells jackson to add a 'jsonIdentity' field to this object (and all subclasses) that is used to
 * identity objects that are the same.  The second object encountered has the value set to the UUID that was generated
 * for the first object, this prevents circular references, and during deserialization jackson uses the reference
 * to set the object value back to the correct parent object.
 *
 * @author vgomelsky
 */
@JsonIdentityInfo(generator = ObjectIdGenerators.UUIDGenerator.class, property = "@jsonIdentity")
public abstract class BaseSimpleEntity<T extends Serializable> implements SimpleEntity<T>, IdentityObject {

	private T id;


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		if (this instanceof LabeledObject) {
			result.append(", label=");
			result.append(((LabeledObject) this).getLabel());
		}
		result.append('}');
		return result.toString();
	}


	/**
	 * Returns true of the specified object is an instance of the same class as this object
	 * and if they both have the same id.
	 */
	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || !o.getClass().equals(getClass())) {
			return false;
		}
		IdentityObject entity = (IdentityObject) o;
		if (getIdentity() == null) {
			return entity.getIdentity() == null;
		}
		return getIdentity().equals(entity.getIdentity());
	}


	@Override
	public int hashCode() {
		return (getId() == null) ? Integer.MIN_VALUE : getId().hashCode();
	}


	@Override
	public T getIdentity() {
		return getId();
	}


	@Override
	public boolean isNewBean() {
		return (this.id == null);
	}


	/**
	 * Returns id that uniquely identifies this object.
	 */
	@Override
	public T getId() {
		return this.id;
	}


	/**
	 * Sets id that uniquely identifies this object.
	 */
	@Override
	public void setId(T id) {
		this.id = id;
	}
}
