package com.clifton.core.beans.common;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author danielh
 */
@SearchForm(hasOrmDtoClass = false)
public class ContactSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private String firstName;

	@SearchField
	private String lastName;

	@SearchField
	private String emailAddress;

	@SearchField
	private String phoneNumber;

	@SearchField
	private String faxNumber;


	//////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////


	public String getFirstName() {
		return this.firstName;
	}


	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	public String getLastName() {
		return this.lastName;
	}


	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getFaxNumber() {
		return this.faxNumber;
	}


	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
}
