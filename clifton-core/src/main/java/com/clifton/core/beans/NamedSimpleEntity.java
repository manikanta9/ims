package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>NamedEntity</code> class can be extended by a lot of entities that have name/description properties.
 *
 * @author vgomelsky
 */
public abstract class NamedSimpleEntity<T extends Serializable> extends BaseSimpleEntity<T> implements NamedObject {

	private String name;
	private String label;
	private String description;


	public NamedSimpleEntity() {
		super();
	}


	public NamedSimpleEntity(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",label=");
		result.append(getLabel());
		result.append('}');
		return result.toString();
	}


	/**
	 * @return the name
	 */
	@Override
	public String getName() {
		return this.name;
	}


	/**
	 * @param name the name to set
	 */
	@Override
	public void setName(String name) {
		this.name = name;
	}


	/**
	 * @return the description
	 */
	@Override
	public String getDescription() {
		return this.description;
	}


	/**
	 * @param description the description to set
	 */
	@Override
	public void setDescription(String description) {
		this.description = description;
	}


	/**
	 * Returns the value of label field if it's not null.
	 * Otherwise returns the value of name field.
	 *
	 * @return the label
	 */
	@Override
	public String getLabel() {
		return (this.label == null) ? this.name : this.label;
	}


	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
}
