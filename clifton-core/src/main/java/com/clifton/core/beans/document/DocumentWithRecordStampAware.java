package com.clifton.core.beans.document;


import java.util.Date;


/**
 * The <code>DocumentWithRecordStampAware</code> extends the DocumentAware interface, and also defines record stamp
 * properties for the associated document to a bean which can be used for searching (list pages)
 *
 * @author manderson
 */
public interface DocumentWithRecordStampAware extends DocumentAware {

	public Date getDocumentUpdateDate();


	public void setDocumentUpdateDate(Date documentUpdateDate);


	public String getDocumentUpdateUser();


	public void setDocumentUpdateUser(String documentUpdateUser);
}
