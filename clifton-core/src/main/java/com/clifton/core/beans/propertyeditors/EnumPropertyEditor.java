package com.clifton.core.beans.propertyeditors;


import com.clifton.core.util.StringUtils;

import java.beans.PropertyEditorSupport;


/**
 * The <code>EnumPropertyEditor</code> handles special case of drop down support where value is set to -1 to
 * clear the field.
 *
 * @author manderson
 */
public class EnumPropertyEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) {
		// Override to handle -1 as blank value selection for drop downs
		if (StringUtils.isEmpty(text) || "-1".equals(text)) {
			setValue(null);
		}
		else {
			setValue(text);
		}
	}
}
