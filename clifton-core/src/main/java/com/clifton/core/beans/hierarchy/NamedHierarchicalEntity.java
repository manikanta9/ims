package com.clifton.core.beans.hierarchy;


import com.clifton.core.beans.NamedEntity;

import java.io.Serializable;


/**
 * The <code>NamedHierarchicalEntity</code> is a hierarchical DTO with name and description.
 *
 * @author vgomelsky
 */
public class NamedHierarchicalEntity<T extends NamedHierarchicalEntity<T, ID>, ID extends Serializable> extends NamedEntity<ID> implements HierarchicalObject<T> {

	public final static String HIERARCHY_DELIMITER = " / ";

	private T parent;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected String getDelimiter() {
		return HIERARCHY_DELIMITER;
	}


	public String getLabelExpanded() {
		StringBuilder result = getLabel() == null ? new StringBuilder() : new StringBuilder(getLabel());
		T bean = getParent();
		// avoid infinite loops with parent pointing to its own child even though this shouldn't be allowed
		int depth = 0;
		while (bean != null) {
			result.insert(0, bean.getLabel() + getDelimiter());
			bean = bean.getParent();
			depth++;
			if (depth > 20) {
				break;
			}
		}
		return result.toString();
	}


	public String getNameExpanded() {
		StringBuilder result = getName() == null ? new StringBuilder() : new StringBuilder(getName());
		T bean = getParent();
		// avoid infinite loops with parent pointing to its own child even though this shouldn't be allowed
		int depth = 0;
		while (bean != null) {
			result.insert(0, bean.getName() + getDelimiter());
			bean = bean.getParent();
			depth++;
			if (depth > 20) {
				break;
			}
		}
		return result.toString();
	}


	/**
	 * By default the max depth is null which
	 * implies no restriction.  Can be overridden
	 * by subclasses to return any actual restrictions.
	 */
	@Override
	public Integer getMaxDepth() {
		return null;
	}


	@Override
	public int getLevel() {
		int level = 1;
		T p = getParent();
		if (p != null && p != this) {
			return p.getLevel() + 1;
		}
		return level;
	}


	@Override
	public T getRootParent() {
		return getParentAtLevel(1);
	}


	@Override
	@SuppressWarnings("unchecked")
	public T getParentAtLevel(int level) {
		int currentLevel = getLevel();

		if (currentLevel <= level) {
			return (T) this;
		}

		T root = getParent();
		currentLevel--;

		while (currentLevel > level && root != null) {
			root = root.getParent();
			currentLevel--;
		}

		return root;
	}


	/**
	 * Returns true if the specified object is parent, grandparent, etc. of this object.
	 */
	public boolean isAncestor(T ancestor) {
		T p = getParent();
		while (p != null) {
			if (p.equals(ancestor)) {
				return true;
			}
			p = p.getParent();
		}
		return false;
	}


	/**
	 * Returns true if the specified object is equal to this object, is parent, grandparent, etc. of this object.
	 */
	public boolean isSelfOrAncestor(T ancestor) {
		@SuppressWarnings("unchecked")
		T p = (T) this;
		while (p != null) {
			if (p.equals(ancestor)) {
				return true;
			}
			p = p.getParent();
		}
		return false;
	}


	@Override
	public boolean isLeaf() {
		Integer maxDepth = getMaxDepth();
		if (maxDepth == null || maxDepth > getLevel()) {
			return false;
		}

		return true;
	}


	////////////////////////////////////////////////////////////////////////////
	//////////             Getter and Setter Methods                  //////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public T getParent() {
		return this.parent;
	}


	/**
	 * @param parent the parent to set
	 */
	public void setParent(T parent) {
		this.parent = parent;
	}
}
