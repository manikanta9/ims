package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.FormatUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;

import java.beans.PropertyEditorSupport;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * The <code>CustomFormatDatePropertyEditor</code> allows code to load date strings into properties when loading data from xml.  You can set
 * the date format by adding "f=format".  The default format is "MM/dd/yyyy".
 * <p/>
 * Examples:
 * p:minuteStartTime="8:00 am f=h:mm aaa"
 * p:minuteStartTime="10/1/2009 17:00:00  f=MM/dd/yyyy HH:mm:ss"
 * p:minuteStartTime="10/1/2009" is the same as p:minuteStartTime="10/1/2009 f=MM/dd/yyyy"
 *
 * @author mwacker
 */
public class CustomFormatDatePropertyEditor extends PropertyEditorSupport {

	private final boolean allowEmpty;


	public CustomFormatDatePropertyEditor() {
		this(true);
	}


	public CustomFormatDatePropertyEditor(boolean allowEmpty) {
		this.allowEmpty = allowEmpty;
	}


	/**
	 * Parse the Date from the given text, using the specified DateFormat.
	 */
	@Override
	@ValueChangingSetter
	public void setAsText(String text) throws IllegalArgumentException {
		SimpleDateFormat dateFormat;
		if (text.contains("f=")) {
			String[] items = text.split("f=");
			if (items.length != 2) {
				throw new IllegalArgumentException("Cannot convert '" + text + "' to a Date.");
			}
			text = items[0].trim();
			dateFormat = FormatUtils.getSimpleDateFormat(items[1].trim());
		}
		else {
			dateFormat = FormatUtils.getSimpleDateFormat(DateUtils.DATE_FORMAT_INPUT);
		}
		if (this.allowEmpty && StringUtils.isEmpty(text)) {
			// Treat empty String as null value.
			setValue(null);
		}
		else {
			try {
				setValue(dateFormat.parse(text));
			}
			catch (ParseException ex) {
				throw new IllegalArgumentException("Could not parse date: " + text, ex);
			}
		}
	}


	/**
	 * Format the Date as String, using the specified DateFormat.
	 */
	@Override
	public String getAsText() {
		Date value = (Date) getValue();
		SimpleDateFormat dateFormat = FormatUtils.getSimpleDateFormat(DateUtils.DATE_FORMAT_FULL);
		return (value != null ? dateFormat.format(value) : "");
	}
}
