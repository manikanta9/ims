package com.clifton.core.beans;

import com.clifton.core.concurrent.ConcurrentUtils;
import org.springframework.scheduling.concurrent.ForkJoinPoolFactoryBean;

import java.util.concurrent.ExecutorService;


/**
 * <code>ManagedForkJoinPoolFactoryBean</code> extends {@link ForkJoinPoolFactoryBean} and only overrides
 * the {@link #destroy()} method to use our {@link ConcurrentUtils#shutdownExecutorService(ExecutorService)} for
 * the singleton {@link java.util.concurrent.ForkJoinPool} created by the FJP FactoryBean.
 *
 * @author NickK
 */
public class ManagedForkJoinPoolFactoryBean extends ForkJoinPoolFactoryBean {

	@Override
	public void destroy() {
		ConcurrentUtils.shutdownExecutorService(getObject());
	}
}
