package com.clifton.core.beans;


import java.util.Date;


/**
 * The <code>UpdatableEntity</code> interface should be implemented by DTO's that have record stamp fields
 * which can be automatically managed by the corresponding DAO's.
 *
 * @author vgomelsky
 */
public interface UpdatableEntity extends UpdatableOnlyEntity {

	public short getCreateUserId();


	public void setCreateUserId(short createUserId);


	public Date getCreateDate();


	public void setCreateDate(Date createDate);
}
