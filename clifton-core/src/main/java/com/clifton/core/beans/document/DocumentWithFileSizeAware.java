package com.clifton.core.beans.document;

/**
 * The <code>DocumentWithFileSizeAware</code> extends the DocumentAware interface, and also defines a place holder for the file size
 * for the associated document to a bean which can be used for searching (list pages)
 *
 * @author manderson
 */
public interface DocumentWithFileSizeAware extends DocumentAware {

	public Long getFileSize();


	public void setFileSize(Long fileSize);
}
