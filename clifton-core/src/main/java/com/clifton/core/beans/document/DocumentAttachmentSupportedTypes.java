package com.clifton.core.beans.document;


/**
 * The <code>DocumentAttachmentSupportedTypes</code> enum can be used to select
 * what amount of attachments are supported for an entity for cases where these are configurable
 * See: System Note Types for an example
 *
 * @author manderson
 */
public enum DocumentAttachmentSupportedTypes {

	NONE, // NO ATTACHMENTS SUPPORTED
	SINGLE, // ONE DOCUMENT PER ENTITY SUPPORTED ONLY
	MULTIPLE // MULTIPLE DOCUMENTS "ATTACHMENTS" ALLOWED
}
