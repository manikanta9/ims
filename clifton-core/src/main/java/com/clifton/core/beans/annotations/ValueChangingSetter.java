package com.clifton.core.beans.annotations;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>ValueChangingSetter</code> marks a setter method that modifies the value before setting the field.
 *
 * This annotation can be used by various parts of the systems like unit tests that try to dynamically test setting/getting a property value (skip for this annotation)
 *
 * @author mwacker
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface ValueChangingSetter {
	// empty
}
