package com.clifton.core.beans.common;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.LabeledObject;


/**
 * The <code>Contact</code> class defines contact entities.
 *
 * @author vgomelsky
 */
public class Contact extends BaseEntity<Integer> implements LabeledObject {

	private String firstName;
	private String lastName;
	private String emailAddress;
	private String phoneNumber;
	private String faxNumber;


	@Override
	public String getLabel() {
		return this.firstName + " " + this.lastName;
	}


	public String getNameLabel() {
		return this.firstName + " " + this.lastName;
	}


	public String getLabelShort() {
		return this.firstName + " " + this.lastName;
	}


	/**
	 * @return the firstName
	 */
	public String getFirstName() {
		return this.firstName;
	}


	/**
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}


	/**
	 * @return the lastName
	 */
	public String getLastName() {
		return this.lastName;
	}


	/**
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}


	public String getEmailAddress() {
		return this.emailAddress;
	}


	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}


	public String getPhoneNumber() {
		return this.phoneNumber;
	}


	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}


	public String getFaxNumber() {
		return this.faxNumber;
	}


	public void setFaxNumber(String faxNumber) {
		this.faxNumber = faxNumber;
	}
}
