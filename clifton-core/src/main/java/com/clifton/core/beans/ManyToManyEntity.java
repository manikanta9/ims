package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>ManyToManyEntity</code> class represents a many to many database relationship.
 *
 * @author vgomelsky
 */
public abstract class ManyToManyEntity<T1 extends IdentityObject, T2 extends IdentityObject, ID extends Serializable> extends BaseEntity<ID> implements LabeledObject {

	private T1 referenceOne;
	private T2 referenceTwo;


	@Override
	public String getLabel() {
		StringBuilder result = new StringBuilder();
		result.append('[');
		if (this.referenceOne instanceof LabeledObject) {
			result.append(((LabeledObject) this.referenceOne).getLabel());
		}
		else {
			result.append("UNKNOWN");
		}
		result.append("] - [");
		if (this.referenceTwo instanceof LabeledObject) {
			result.append(((LabeledObject) this.referenceTwo).getLabel());
		}
		else {
			result.append("UNKNOWN");
		}
		result.append(']');
		return result.toString();
	}


	/**
	 * @return the referenceOne
	 */
	public T1 getReferenceOne() {
		return this.referenceOne;
	}


	/**
	 * @param referenceOne the referenceOne to set
	 */
	public void setReferenceOne(T1 referenceOne) {
		this.referenceOne = referenceOne;
	}


	/**
	 * @return the referenceTwo
	 */
	public T2 getReferenceTwo() {
		return this.referenceTwo;
	}


	/**
	 * @param referenceTwo the referenceTwo to set
	 */
	public void setReferenceTwo(T2 referenceTwo) {
		this.referenceTwo = referenceTwo;
	}
}
