package com.clifton.core.beans;

import com.clifton.core.beans.annotations.ValueChangingSetter;


/**
 * The <code>ImmutableObjectWrapper</code> is an object wrapper where the value object can never change. Useful for static NULL representations
 *
 * @author manderson
 */
public class ImmutableObjectWrapper<T> extends ObjectWrapper<T> {


	public ImmutableObjectWrapper(T obj) {
		super(obj);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@ValueChangingSetter
	@Override
	public void setObject(T obj) {
		throw new IllegalStateException("Cannot change object on ImmutableObjectWrapper");
	}
}
