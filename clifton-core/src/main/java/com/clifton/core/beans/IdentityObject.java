package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>IdentityObject</code> interface defines objects that can be uniquely identified by id. Most model objects
 * should implement this interface.
 * <p>
 * The class is annotated with JsonFilter to facilitate the jackson exclusion strategies.  Filters are applied by name
 * so various classes could be annotated with different names for the filters and thereby handled differently.  In our
 * case we use a single exclusion strategy to filter values from all IdentityObjects.
 *
 * @author vgomelsky
 */
public interface IdentityObject extends Serializable {

	/**
	 * Returns id that uniquely identifies this object.
	 */
	public Serializable getIdentity();


	/**
	 * Returns true if this object is new (doesn't exist in DB).
	 */
	public boolean isNewBean();
}
