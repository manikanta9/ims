package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.StringUtils;

import java.beans.PropertyEditorSupport;


/**
 * The {@link ByteArrayPropertyEditor} class converts a comma- or double-colon-delimited string of integer byte codes to the corresponding array of bytes.
 *
 * @author vgomelsky
 */
public class ByteArrayPropertyEditor extends PropertyEditorSupport {

	@Override
	@ValueChangingSetter
	public void setAsText(String text) {
		if (StringUtils.isEmpty(text)) {
			setValue(null);
		}
		else {
			String[] fromElements = text.replaceAll("[\\[\\]]", "")
					.split(text.contains("::") ? "::" : ",");
			byte[] result = new byte[fromElements.length];
			for (int i = 0; i < fromElements.length; i++) {
				result[i] = new Byte(fromElements[i]);
			}
			setValue(result);
		}
	}
}
