package com.clifton.core.beans.document;


/**
 * The <code>DocumentFilePropertyAware</code> is an extension of {@link DocumentFileAware} interface but also
 * supports file properties that can be updated on the bean itself in cases where there is only 1 file
 * example: System Note Attachments
 * This can be used by SystemNotes themselves are DocumentAware, but a specific note would only be one or the other.
 *
 * @author manderson
 */
public interface DocumentFilePropertyAware extends DocumentFileAware, DocumentAware {


	// NOTHING HERE
}
