package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>ObjectWrapperWithBooleanResult</code> class can be used when it's necessary to return an object and a boolean result
 * For example, DAO smart save would return the Object back as well as true if the save actually happened or false if it was skipped.
 *
 * @param <T>
 * @author manderson
 */
public class ObjectWrapperWithBooleanResult<T> implements Serializable {

	private final T object;

	private final boolean result;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	/**
	 * Sets the object and true for the result
	 */
	public static <T> ObjectWrapperWithBooleanResult<T> success(T obj) {
		return new ObjectWrapperWithBooleanResult<>(obj, true);
	}


	/**
	 * Sets the object and false for the result
	 */
	public static <T> ObjectWrapperWithBooleanResult<T> fail(T obj) {
		return new ObjectWrapperWithBooleanResult<>(obj, false);
	}


	private ObjectWrapperWithBooleanResult(T obj, boolean result) {
		this.object = obj;
		this.result = result;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		if (this.object == null) {
			return "Object is null, Result = " + isResult();
		}
		return this.object.toString() + ", Result = " + isResult();
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public T getObject() {
		return this.object;
	}


	public boolean isResult() {
		return this.result;
	}
}
