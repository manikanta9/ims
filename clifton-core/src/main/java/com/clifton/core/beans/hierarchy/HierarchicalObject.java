package com.clifton.core.beans.hierarchy;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>HierarchicalObject</code> interface should be implemented by DTO's that are hierarchical.
 *
 * @author vgomelsky
 */
public interface HierarchicalObject<T extends HierarchicalObject<T>> extends IdentityObject {

	/**
	 * Returns this object's parent object.
	 * Root nodes will return null.
	 */
	public T getParent();


	/**
	 * Returns this object's max allowable depth
	 */
	public Integer getMaxDepth();


	/**
	 * Returns this object's level
	 */
	public int getLevel();


	/**
	 * Returns true if this object is a leaf
	 * A leaf cannot have any children, therefore by default a leaf
	 * is determined by this object's level being equal to the max allowable depth
	 */
	public boolean isLeaf();


	/**
	 * Will return the top level parent of this hierarchical object.
	 *
	 * @return getParentAtLevel(1)
	 */
	public T getRootParent();


	/**
	 * Return the parent at the specified level
	 * <p/>
	 * If the designated level is lower than or equal to the current level of this object
	 * then return the current object.
	 *
	 * @param level
	 * @return the closest parent to the specified level
	 */
	public T getParentAtLevel(int level);
}
