package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>NamedEntityWithoutLabel</code> class can be extended by a lot of entities that have name/description properties.
 *
 * @author vgomelsky
 */
public class NamedEntityWithoutLabel<T extends Serializable> extends BaseEntity<T> implements NamedObject {

	private String name;
	private String description;


	public NamedEntityWithoutLabel() {
		super();
	}


	public NamedEntityWithoutLabel(String name, String description) {
		super();
		this.name = name;
		this.description = description;
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",label=");
		result.append(getLabel());
		result.append('}');
		return result.toString();
	}


	@Override
	public String getName() {
		return this.name;
	}


	@Override
	public void setName(String name) {
		this.name = name;
	}


	@Override
	public String getDescription() {
		return this.description;
	}


	@Override
	public void setDescription(String description) {
		this.description = description;
	}


	@Override
	public String getLabel() {
		return this.name;
	}
}
