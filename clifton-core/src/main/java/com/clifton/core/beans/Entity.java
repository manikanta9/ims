package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>Entity</code> defines methods used by all DTO objects that represent database entities.
 * <p/>
 * NOTE: This interface does not implement {@link IdentityObject} because the {@link IdentityObject}
 * interface makes this interface unusable as hibernate proxy used on objects that can be lazily loaded.
 *
 * @param <T>
 * @author mwacker
 */
public interface Entity<T extends Serializable> extends SimpleEntity<T>, UpdatableEntity {

	//
}
