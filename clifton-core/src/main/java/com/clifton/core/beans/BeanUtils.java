package com.clifton.core.beans;


import com.clifton.core.beans.propertyeditors.DatePropertyEditor;
import com.clifton.core.beans.propertyeditors.IntArrayPropertyEditor;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.validation.ValidationException;
import org.apache.commons.beanutils.PropertyUtils;
import org.springframework.beans.BeanInstantiationException;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeansException;
import org.springframework.beans.InvalidPropertyException;
import org.springframework.beans.NotReadablePropertyException;
import org.springframework.beans.NotWritablePropertyException;
import org.springframework.beans.NullValueInNestedPathException;
import org.springframework.beans.PropertyAccessorFactory;

import java.beans.PropertyDescriptor;
import java.beans.PropertyEditorSupport;
import java.io.Serializable;
import java.lang.annotation.Annotation;
import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.IdentityHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;


/**
 * The <code>BeanUtils</code> class provides utility methods for working with beans.
 *
 * @author vgomelsky
 */
public class BeanUtils {

	public static final String[] AUDIT_PROPERTIES = new String[]{"createUserId", "createDate", "updateUserId", "updateDate", "rv"};
	/**
	 * The annotations cache. This cache contains all previously-analyzed annotations for the fields and descriptors (getters) for any specific class and its
	 * superclasses.
	 * <p>
	 * Map hierarchy: Class -> Annotation Type -> Field/Descriptor Name -> Annotation Data
	 */
	private static final Map<Class<?>, Map<Class<? extends Annotation>, Map<String, ? extends Annotation>>> CLASS_PROPERTY_ANNOTATIONS = new ConcurrentHashMap<>();
	private static final Set<String> SYSTEM_MANAGED_FIELDS = Collections.unmodifiableSortedSet(new ConcurrentSkipListSet<>(Arrays.asList(AUDIT_PROPERTIES)));


	/*
	  Created by stevenf on 8/13/2015.
	  This was created to address and issue with BeanUtils - the latest version 1.8.3 handles this;
	  however it is loaded as a dependency of another library right now so this was created as a workaround
	  this article explains the issue/solution:
	  <p>
	  http://stackoverflow.com/questions/5266531/java-beans-beanutils-and-the-boolean-wrapper-class
	 */
	static {
		PropertyUtils.addBeanIntrospector(new BooleanIntrospector());
	}


	/**
	 * Returns true if the specified field is managed by the system (Example: record stamp field).
	 */
	public static boolean isSystemManagedField(String fieldName) {
		return SYSTEM_MANAGED_FIELDS.contains(fieldName);
	}


	public static String[] getSystemManagedFields() {
		return SYSTEM_MANAGED_FIELDS.toArray(new String[0]);
	}


	/**
	 * Creates a new instance of the specified class using the default constructor.
	 */
	public static <T> T newInstance(Class<T> clazz) {
		AssertUtils.assertNotNull(clazz, "Argument class cannot be null.");
		T result;
		try {
			result = clazz.newInstance();
		}
		catch (Exception e) {
			throw new RuntimeException("Error instantiating an object of type: " + clazz, e);
		}
		return result;
	}


	/**
	 * Creates a new instance of the same type as the provided object. This method is preferred over {@link #newInstance(Class)} when the object is given, and particularly when the
	 * object type is derived from a generic type parameter.
	 * <p>
	 * This utility differs from {@link #newInstance(Class)} in that it does not suffer from the inherent loss of compile-time type information derived from generic type
	 * parameters. For example, the compile-time return type of {@code newInstance(obj.getClass())} where {@code obj} is of generic type {@code T} is {@link Object}, while the
	 * compile-time return type of this method with the same argument is the generic type {@code T}.
	 */
	public static <T> T newInstance(T obj) {
		if (obj == null) {
			return null;
		}
		@SuppressWarnings("unchecked")
		T result = (T) newInstance(obj.getClass());
		return result;
	}


	/**
	 * Convenience method to instantiate a class using the given constructor.
	 * As this method doesn't try to load classes by name, it should avoid
	 * class-loading issues.
	 * <p>Note that this method tries to set the constructor accessible
	 * if given a non-accessible (that is, non-public) constructor.
	 *
	 * @param constructor the constructor to instantiate
	 * @param args        the constructor arguments to apply
	 * @return the new instance
	 * @throws BeanInstantiationException if the bean cannot be instantiated
	 */
	public static <T> T newInstance(Constructor<T> constructor, Object... args) {
		return org.springframework.beans.BeanUtils.instantiateClass(constructor, args);
	}


	/**
	 * Creates a new instance of the specified class calling the constructor matching the specified arguments.
	 */
	public static <T> T newInstance(Class<T> clazz, Object... args) {
		Class<?>[] types = new Class<?>[args.length];
		for (int i = 0; i < args.length; i++) {
			types[i] = args[i].getClass();
		}
		Constructor<T> constructor;
		try {
			constructor = clazz.getConstructor(types);
		}
		catch (Throwable e) {
			throw new IllegalArgumentException("Cannot find constructor for " + clazz, e);
		}
		return org.springframework.beans.BeanUtils.instantiateClass(constructor, args);
	}


	/**
	 * If the specified bean is an instance of {@link LabeledObject} then return the result of {@link LabeledObject#getLabel()}.
	 * Otherwise return {@link Object#toString()} representation of the bean.
	 */
	public static String getLabel(Object bean) {
		if (bean == null) {
			return "null";
		}
		if (bean instanceof LabeledObject) {
			return ((LabeledObject) bean).getLabel();
		}
		return bean.toString();
	}


	/**
	 * Similar to {@link #getLabel(Object)} but uses reflection to retrieve "label" or "labelShort" property value.
	 */
	public static String getLabelAdvanced(Object bean, boolean lookupShortLabelFirst) {
		if (bean == null) {
			return "null";
		}
		Object result = getPropertyValue(bean, lookupShortLabelFirst ? "labelShort" : "label", true);
		if (result == null) {
			result = getPropertyValue(bean, lookupShortLabelFirst ? "label" : "labelShort", true);
			if (result == null) {
				result = bean;
			}
		}
		return result.toString();
	}


	/**
	 * Sets the specified property of the argument bean to the specified value.
	 * Converts String values to corresponding property types where possible (enum, ???).
	 */
	public static void setPropertyValue(Object bean, String propertyName, Object value) {
		setPropertyValue(bean, propertyName, value, false);
	}


	/**
	 * Sets the specified property of the argument bean to the specified value.
	 * Converts String values to corresponding property types where possible (enum, ???).
	 *
	 * @param ignoreIfNotWritable - If true, will not throw an exception if the property is not writable
	 */
	public static void setPropertyValue(Object bean, String propertyName, Object value, boolean ignoreIfNotWritable) {
		// NOTE: do we need more standard editors?
		// Adding date support to standard editors
		Map<Class<?>, PropertyEditorSupport> standardEditors = new HashMap<>();
		standardEditors.put(int[].class, new IntArrayPropertyEditor());
		standardEditors.put(Date.class, new DatePropertyEditor());

		setPropertyValue(bean, propertyName, value, standardEditors, ignoreIfNotWritable);
	}


	/**
	 * Sets the specified property of the argument bean to the specified value if the bean does not currently have a value.
	 * Converts String values to corresponding property types where possible (enum, ???).
	 * <p>
	 * NOTE: This may not work correctly in all cases with primitive properties.
	 *
	 * @param ignoreIfNotWritable - If true, will not throw an exception if the property is not writable
	 * @see #isPropertyValueDefault(Object, String, boolean)
	 */
	public static void setPropertyValueIfNotSet(Object bean, String propertyName, Object value, boolean ignoreIfNotWritable) {
		if (isPropertyValueDefault(bean, propertyName, ignoreIfNotWritable)) {
			setPropertyValue(bean, propertyName, value, ignoreIfNotWritable);
		}
	}


	/**
	 * Sets the specified property of the argument bean to the specified value.
	 * Converts String values to corresponding property types where possible (enum, ???).
	 */
	public static void setPropertyValue(Object bean, String propertyName, Object value, Map<Class<?>, PropertyEditorSupport> propertyEditors) {
		setPropertyValue(bean, propertyName, value, propertyEditors, false);
	}


	/**
	 * Sets the specified property of the argument bean to the specified value.
	 * Converts String values to corresponding property types where possible (enum, ???).
	 *
	 * @param ignoreIfNotWritable - If true, will not throw an exception if the property is not writable
	 */
	public static void setPropertyValue(Object bean, String propertyName, Object value, Map<Class<?>, PropertyEditorSupport> propertyEditors, boolean ignoreIfNotWritable) {
		try {
			BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(bean);
			wrapper.setAutoGrowNestedPaths(true);
			if (propertyEditors != null) {
				for (Map.Entry<Class<?>, PropertyEditorSupport> classPropertyEditorSupportEntry : propertyEditors.entrySet()) {
					wrapper.registerCustomEditor(classPropertyEditorSupportEntry.getKey(), classPropertyEditorSupportEntry.getValue());
				}
			}
			wrapper.setPropertyValue(propertyName, value);
		}
		catch (NotWritablePropertyException e) {
			if (!ignoreIfNotWritable) {
				throw e;
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error setting property '" + propertyName + "' to " + value, e);
		}
	}


	/**
	 * Provides the ability to hydrate the bean list with objects based on a common "id" value
	 */
	public static <T, O, V> void hydrateBeanList(List<T> beanList, Function<T, V> identifierGetterFromBean, Function<V[], List<O>> objectListRetriever, Function<O, V> identifierGetterFromObject, BiConsumer<T, O> objectSetter, Class<V> propertyType) {
		if (!CollectionUtils.isEmpty(beanList)) {
			V[] ids = getPropertyValuesUniqueExcludeNull(beanList, identifierGetterFromBean, propertyType);
			if (ids.length > 0) {
				List<O> objectList = objectListRetriever.apply(ids);
				Map<V, O> objectMap = getBeanMap(objectList, identifierGetterFromObject);
				CollectionUtils.getStream(beanList).forEach(bean -> objectSetter.accept(bean, objectMap.get(identifierGetterFromBean.apply(bean))));
			}
		}
	}


	/**
	 * Returns the value of the specified field of the argument bean. Accesses the field directly and changes
	 * accessible if necessary.
	 */
	public static <T> T getFieldValue(Object bean, String fieldName) {
		try {
			Field privateField = ClassUtils.getClassField(bean.getClass(), fieldName, true, true);
			return getFieldValue(bean, privateField);
		}
		catch (Throwable e) {
			throw new RuntimeException("Error reading field '" + fieldName + "' for bean type: " + bean.getClass(), e);
		}
	}


	/**
	 * Returns the value of the specified field of the argument bean. Accesses the field directly and temporarily changes
	 * accessible if necessary.
	 */
	public static <T> T getFieldValue(Object bean, Field field) {
		try {
			boolean accessible = field.isAccessible();
			if (!accessible) {
				field.setAccessible(true);
			}
			Object result = field.get(bean);
			if (!accessible) {
				field.setAccessible(false);
			}
			@SuppressWarnings("unchecked")
			T typedResult = (T) result;
			return typedResult;
		}
		catch (Throwable e) {
			throw new RuntimeException("Error reading field '" + field.getName() + "' for bean type: " + bean.getClass(), e);
		}
	}


	/**
	 * Returns the value of the specified property of the argument bean.
	 * Supports nested properties.
	 */
	public static Object getPropertyValue(Object bean, String propertyName) {
		// Since now we use autoGrowNestedProperties = false, need to
		// all also allow returning null by default because if the property is nested within a null property
		// and we don't create the object and then look at it, get an error that the property
		// doesn't exist
		return getPropertyValue(bean, propertyName, true);
	}


	/**
	 * Returns the value of the specified property of the argument bean.
	 * Supports nested properties.
	 * <p>
	 * If nullIfPropertyMissing is true then will not throw an exception if the property is missing from
	 * the bean but will return null instead.
	 */
	public static Object getPropertyValue(Object bean, String propertyName, boolean nullIfPropertyMissing) {
		// Must be false for get property value because when true
		// for nested paths will actually convert null objects to
		// new instances.  Eventually if this gets back to hibernate
		// throws an error because the object is now an empty instance with no id value
		// Allowed to be passed as true because for getPropertyType, we need the actual objects
		return getPropertyValue(bean, propertyName, nullIfPropertyMissing, false);
	}


	/**
	 * Returns the value of the specified property of the argument bean. Supports nested properties.
	 * <p>
	 * If nullIfPropertyMissing is true then will not throw an exception if the property is missing from the bean but will return null instead.
	 * <p>
	 * If autoGrowNestedPaths = true will convert nulls to empty objects to lookup nested properties of that object. If false and the property is nested within
	 * a null property, will just return null (if nullIfPropertyMissing = true), otherwise throws NullValueInNestedPathException
	 */
	public static Object getPropertyValue(Object bean, String propertyName, boolean nullIfPropertyMissing, boolean autoGrowNestedPaths) {
		if (bean == null || (nullIfPropertyMissing && StringUtils.isEmpty(propertyName))) {
			return null;
		}
		try {
			BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(bean);
			wrapper.setAutoGrowNestedPaths(autoGrowNestedPaths);
			return wrapper.getPropertyValue(propertyName);
		}
		catch (NullValueInNestedPathException ex) {
			if (nullIfPropertyMissing) {
				return null;
			}
			// property is present but the value is null
			throw ex;
		}
		catch (NotReadablePropertyException | NotWritablePropertyException e) {
			if (nullIfPropertyMissing) {
				return null;
			}
			throw new RuntimeException("Error reading property '" + propertyName + "' for bean type: " + bean.getClass(), e);
		}
		catch (InvalidPropertyException ex) {
			if (nullIfPropertyMissing) {
				return null;
			}
			throw new RuntimeException("Property '" + propertyName + "' doesn't exist for bean type: " + bean.getClass());
		}
		catch (Exception e) {
			throw new RuntimeException("Error reading property '" + propertyName + "' for bean type: " + bean.getClass(), e);
		}
	}


	/**
	 * Returns true if the current value is the default value. For non-primitive, compares to null. For primitives
	 * compares to the corresponding Java primitive default.
	 */
	public static boolean isPropertyValueDefault(Object bean, String propertyName, boolean falseIfPropertyMissing) {
		try {
			Class<?> propertyType = getPropertyType(bean, propertyName);
			Object value = getPropertyValue(bean, propertyName);
			if (propertyType.isPrimitive()) {
				if (propertyType == Boolean.TYPE) {
					return !((Boolean) value);
				}
				else if (propertyType == Byte.TYPE) {
					return ((Byte) value) == 0;
				}
				else if (propertyType == Character.TYPE) {
					return ((Character) value) == 0;
				}
				else {
					return MathUtils.isEqual((Number) value, 0);
				}
			}
			return value == null;
		}
		catch (IllegalArgumentException e) {
			if (falseIfPropertyMissing) {
				return false;
			}
			throw new RuntimeException("Property '" + propertyName + "' doesn't exist for bean type: " + bean.getClass());
		}
	}


	/**
	 * Returns an array of property values for the specified fieldNames.
	 */
	public static Object[] getPropertyValues(Object object, String[] fieldNames) {
		Object[] fieldValues = new Object[fieldNames.length];
		for (int i = 0; i < fieldNames.length; i++) {
			String fieldName = fieldNames[i];
			fieldValues[i] = BeanUtils.getPropertyValue(object, fieldName);
		}
		return fieldValues;
	}


	/**
	 * Returns a map of field name to property value for the specified fieldNames.
	 */
	public static Map<String, Object> getPropertyValueMap(Object object, Collection<String> fieldNames) {
		Map<String, Object> results = new HashMap<>();
		for (String fieldName : CollectionUtils.getIterable(fieldNames)) {
			results.put(fieldName, BeanUtils.getPropertyValue(object, fieldName));
		}
		return results;
	}


	/**
	 * Returns a String of specified property values separated by the argument delimiter.
	 */
	public static <T> String getPropertyValues(List<T> list, String propertyName, String delimiter) {
		return getPropertyValues(list, propertyName, delimiter, null, null, null, Integer.MAX_VALUE);
	}


	/**
	 * Returns a String of specified property values separated by the argument delimiter.
	 *
	 * @param delimiter       - used to separate elements
	 * @param startMarker     - if not null, prepended before each element
	 * @param endMarker       - if not null, appended after each element
	 * @param remainingMarker - appends to the end of result when maxElements is exceeded.
	 * @param maxElements     - max number of elements to use. Skip remaining elements, if any, and append 'remainingMarker' in the end.
	 */
	public static <T> String getPropertyValues(List<T> list, String propertyName, String delimiter, String startMarker, String endMarker, String remainingMarker, int maxElements) {
		if (list == null) {
			return null;
		}
		int size = list.size();
		if (size == 0) {
			return null;
		}
		T element = list.get(0);
		StringBuilder result = new StringBuilder();
		try {
			if (startMarker != null) {
				result.append(startMarker);
			}
			result.append(getPropertyValue(element, propertyName));
			if (endMarker != null) {
				result.append(endMarker);
			}
			for (int i = 1; i < size; i++) {
				element = list.get(i);
				result.append(delimiter);
				if (startMarker != null) {
					result.append(startMarker);
				}
				result.append(getPropertyValue(element, propertyName));
				if (endMarker != null) {
					result.append(endMarker);
				}
				if (i >= (maxElements - 1)) {
					if (remainingMarker != null && (size - 1) > i) {
						result.append(remainingMarker);
					}
					break;
				}
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Error reading property '" + propertyName + "' from " + element, e);
		}
		return result.toString();
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.
	 * Returns an empty array if the argument beanList is null or empty. Excludes any properties that are null.
	 */
	public static <T, V> Object[] getPropertyValuesExcludeNull(List<T> beanList, Function<T, V> functionReturningPropertyValue) {
		return CollectionUtils.getStream(beanList).map(functionReturningPropertyValue).filter(Objects::nonNull).toArray();
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.
	 * Returns an empty array if the argument beanList is null or empty. Excludes any properties that are null.
	 */
	public static <T, V> V[] getPropertyValuesExcludeNull(List<T> beanList, Function<T, V> functionReturningPropertyValue, Class<V> propertyType) {
		return CollectionUtils.getStream(beanList)
				.map(functionReturningPropertyValue)
				.filter(Objects::nonNull)
				.toArray(value -> {
					@SuppressWarnings("unchecked")
					V[] array = (V[]) Array.newInstance(propertyType, value);
					return array;
				});
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.
	 * Returns an empty array if the argument beanList is null or empty. Excludes any properties that are null.
	 */
	public static <T, V> V[] getPropertyValuesUniqueExcludeNull(List<T> beanList, Function<T, V> functionReturningPropertyValue, Class<V> propertyType) {
		return CollectionUtils.getStream(beanList)
				.map(functionReturningPropertyValue)
				.filter(Objects::nonNull)
				.distinct()
				.toArray(value -> {
					@SuppressWarnings("unchecked")
					V[] array = (V[]) Array.newInstance(propertyType, value);
					return array;
				});
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.
	 * Returns an empty array if the argument beanList is null or empty.
	 */
	public static <T, V> Object[] getPropertyValues(List<T> beanList, Function<T, V> functionReturningPropertyValue) {
		return CollectionUtils.getStream(beanList)
				.map(functionReturningPropertyValue)
				.toArray();
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.  The resulting array is an
	 * array of items of type propertyType.
	 * Returns an empty array if the argument beanList is null or empty.
	 */
	public static <T, V> V[] getPropertyValues(List<T> beanList, Function<T, V> functionReturningPropertyValue, Class<V> propertyType) {
		return CollectionUtils.getStream(beanList)
				.map(functionReturningPropertyValue)
				.toArray(value -> {
					@SuppressWarnings("unchecked")
					V[] array = (V[]) Array.newInstance(propertyType, value);
					return array;
				});
	}


	/**
	 * Returns an Object[] of specified property values.  Throws an exception if the specified property is not found.
	 */
	public static <T> Object[] getPropertyValues(List<T> list, String propertyName) {
		return getPropertyValues(list, propertyName, false);
	}


	/**
	 * Returns an Object[] of specified property values
	 */
	public static <T> Object[] getPropertyValues(List<T> list, String propertyName, boolean nullIfPropertyMissing) {
		Object[] propertyList = new Object[CollectionUtils.getSize(list)];
		for (int i = 0; i < CollectionUtils.getSize(list); i++) {
			propertyList[i] = getPropertyValue(list.get(i), propertyName, nullIfPropertyMissing);
		}
		return propertyList;
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.  The resulting array is an
	 * array of items of type propertyType.
	 * Returns an empty array if the argument beanList is null or empty.
	 */
	public static <T> T[] getPropertyValues(List<?> list, String propertyName, Class<T> propertyType) {
		return getPropertyValues(list, propertyName, propertyType, false);
	}


	/**
	 * Returns array of the specified properties mapped from each of the specified beans.  The resulting array is an
	 * array of items of type propertyType.
	 * Returns an empty array if the argument beanList is null or empty.
	 */
	@SuppressWarnings("unchecked")
	public static <T> T[] getPropertyValues(List<?> list, String propertyName, Class<T> propertyType, boolean nullIfPropertyMissing) {
		if (CollectionUtils.isEmpty(list)) {
			return (T[]) Array.newInstance(propertyType, 0);
		}
		T[] propertyArray = (T[]) Array.newInstance(propertyType, list.size());
		int i = 0;
		for (Object element : CollectionUtils.getIterable(list)) {
			propertyArray[i++] = (T) getPropertyValue(element, propertyName, nullIfPropertyMissing);
		}

		return propertyArray;
	}


	/**
	 * Returns the data type of the specified property for the specified class.
	 * Supports nested properties.
	 */
	public static Class<?> getPropertyType(Class<?> clazz, String propertyName) {
		int dotIndex = propertyName.indexOf('.');
		if (dotIndex != -1) {
			String nestedPropertyName = propertyName.substring(0, dotIndex);
			PropertyDescriptor descriptor = getPropertyDescriptor(clazz, nestedPropertyName);
			if (descriptor == null) {
				throw new IllegalArgumentException("Cannot find property '" + propertyName + "' for " + clazz);
			}
			return getPropertyType(descriptor.getPropertyType(), propertyName.substring(dotIndex + 1));
		}

		PropertyDescriptor descriptor = getPropertyDescriptor(clazz, propertyName);
		if (descriptor == null) {
			throw new IllegalArgumentException("Cannot find property '" + propertyName + "' for " + clazz);
		}
		return descriptor.getPropertyType();
	}


	public static List<String> getNullProperties(Object bean) {
		List<String> nullProps = new ArrayList<>();
		for (String name : BeanUtils.getPropertyNames(bean.getClass(), false, false)) {
			if (BeanUtils.getPropertyValue(bean, name) == null) {
				nullProps.add(name);
			}
		}
		return nullProps;
	}


	public static List<String> getNonNullProperties(Object bean) {
		List<String> props = new ArrayList<>();
		for (String name : BeanUtils.getPropertyNames(bean.getClass(), false, false)) {
			if (BeanUtils.getPropertyValue(bean, name) != null) {
				props.add(name);
			}
		}
		return props;
	}


	/**
	 * Returns true if the specified property exists for the specified class.
	 * Supports nested properties.
	 */
	public static boolean isPropertyPresent(Class<?> clazz, String propertyName) {
		int dotIndex = propertyName.indexOf('.');
		if (dotIndex != -1) {
			String nestedPropertyName = propertyName.substring(0, dotIndex);
			PropertyDescriptor descriptor = getPropertyDescriptor(clazz, nestedPropertyName);
			if (descriptor == null) {
				return false;
			}
			return isPropertyPresent(descriptor.getPropertyType(), propertyName.substring(dotIndex + 1));
		}
		if (getPropertyDescriptor(clazz, propertyName) == null) {
			return false;
		}
		return true;
	}


	/**
	 * Returns a Map of argument class property names to corresponding data types.
	 */
	public static Map<String, PropertyDescriptor> getPropertyDataTypes(Class<?> clazz) {
		List<PropertyDescriptor> pdList = getPropertyDescriptors(clazz);

		Map<String, PropertyDescriptor> propertyMap = new HashMap<>();
		for (PropertyDescriptor pd : CollectionUtils.getIterable(pdList)) {
			propertyMap.put(pd.getName(), pd);
		}
		return propertyMap;
	}


	/**
	 * Returns the JavaBeans {@link PropertyDescriptor} for the specified property of the specified class.
	 */
	public static PropertyDescriptor getPropertyDescriptor(Class<?> clazz, String propertyName) {
		PropertyDescriptor pd = org.springframework.beans.BeanUtils.getPropertyDescriptor(clazz, propertyName);
		// Above getPropertyDescriptor does not check inherited interface properties - need to check these manually
		if (pd == null) {
			Class<?>[] interfaces = clazz.getInterfaces();
			if (interfaces != null && interfaces.length > 0) {
				for (Class<?> i : interfaces) {
					pd = org.springframework.beans.BeanUtils.getPropertyDescriptor(i, propertyName);
					if (pd != null) {
						break;
					}
				}
			}
		}
		return pd;
	}


	/**
	 * Returns the JavaBeans nested {@link PropertyDescriptor} for the specified property of the specified bean. Supports nested properties.
	 */
	public static PropertyDescriptor getNestedPropertyDescriptor(Object bean, String propertyName) {
		BeanWrapper wrapper = PropertyAccessorFactory.forBeanPropertyAccess(bean);
		wrapper.setAutoGrowNestedPaths(true);
		return wrapper.getPropertyDescriptor(propertyName);
	}


	/**
	 * Returns the JavaBeans {@link PropertyDescriptor} objects for the given class.
	 */
	public static List<PropertyDescriptor> getPropertyDescriptors(Class<?> clazz) {
		List<PropertyDescriptor> pdList = new ArrayList<>();
		Class<?>[] interfaces = clazz.getInterfaces();
		if (interfaces != null && interfaces.length > 0) {
			for (Class<?> i : interfaces) {
				pdList.addAll(Arrays.asList(org.springframework.beans.BeanUtils.getPropertyDescriptors(i)));
			}
		}
		pdList.addAll(Arrays.stream(org.springframework.beans.BeanUtils.getPropertyDescriptors(clazz)).filter(descriptor -> !StringUtils.isEqual(descriptor.getName(), "class")).collect(Collectors.toList()));
		return pdList;
	}


	/**
	 * Returns the collection of unique JavaBeans {@link PropertyDescriptor} names for the given class.
	 */
	public static List<String> getPropertyNames(Class<?> clazz, boolean requireGetter, boolean requireSetter) {
		return CollectionUtils.getStream(getPropertyDescriptors(clazz))
				.filter(descriptor -> !(requireGetter && descriptor.getReadMethod() == null))
				.filter(descriptor -> !(requireSetter && descriptor.getWriteMethod() == null))
				.map(PropertyDescriptor::getName)
				.distinct()
				.collect(Collectors.toList());
	}


	/**
	 * Returns a map of the bean's property names (determined via getters) to their corresponding values. Only properties with getters will be returned.
	 *
	 * @param bean the bean to describe
	 * @return the map of property names to their values
	 */
	public static Map<String, Object> describe(Object bean) {
		Map<String, Object> beanPropertyMap = null;
		try {
			if (bean != null) {
				beanPropertyMap = getPropertyValueMap(bean);
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error describing bean of " + bean.getClass(), e);
		}
		return beanPropertyMap;
	}


	/**
	 * Returns a map of the bean's property names (determined via getters) to their corresponding values. Only properties with both getters and setters will be
	 * returned.
	 *
	 * @see #describe(Object)
	 */
	public static Map<String, Object> describeWithRequiredSetter(Object bean) {
		Map<String, Object> beanPropertyMap = null;
		try {
			if (bean != null) {
				beanPropertyMap = getPropertyValueMap(bean);
				applyDescribeRequiredSetterRules(bean, beanPropertyMap);
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error describing bean of " + bean.getClass(), e);
		}
		return beanPropertyMap;
	}


	/**
	 * Returns a map of the bean's property names (determined via getters) to their corresponding values. Only properties which have getters and are not
	 * excluded from JSON serialization will be returned.
	 *
	 * @see #describe(Object)
	 */
	@SafeVarargs
	public static Map<String, Object> describeWithAnnotationFilters(Object bean, Class<? extends Annotation>... annotationClasses) {
		Map<String, Object> beanPropertyMap = null;
		try {
			if (bean != null) {
				beanPropertyMap = getPropertyValueMap(bean);
				for (Class<? extends Annotation> annotationClass : annotationClasses) {
					applyDescribeAnnotationFilter(bean, beanPropertyMap, annotationClass);
				}
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error describing bean of " + bean.getClass(), e);
		}
		return beanPropertyMap;
	}


	/**
	 * Returns a map of the bean's property names (determined via getters) to their corresponding values. Only properties which have both getters and setters
	 * and are not excluded from JSON serialization will be returned.
	 *
	 * @see #describe(Object)
	 */
	@SafeVarargs
	public static Map<String, Object> describeWithRequiredSetterAndAnnotationFilters(Object bean, Class<? extends Annotation>... annotationClasses) {
		Map<String, Object> beanPropertyMap = null;
		try {
			if (bean != null) {
				beanPropertyMap = getPropertyValueMap(bean);
				applyDescribeRequiredSetterRules(bean, beanPropertyMap);
				for (Class<? extends Annotation> annotationClass : annotationClasses) {
					applyDescribeAnnotationFilter(bean, beanPropertyMap, annotationClass);
				}
			}
		}
		catch (Throwable e) {
			throw new RuntimeException("Error describing bean of " + bean.getClass(), e);
		}
		return beanPropertyMap;
	}


	/**
	 * Gets the property values for the given bean.
	 */
	private static Map<String, Object> getPropertyValueMap(Object bean) throws Exception {
		return PropertyUtils.describe(bean);
	}


	/**
	 * Filters out all bean properties from the given result of the {@link #describe(Object)} method result which do not have setters.
	 */
	private static void applyDescribeRequiredSetterRules(Object bean, Map<String, Object> describeProperties) {
		for (PropertyDescriptor descriptor : PropertyUtils.getPropertyDescriptors(bean)) {
			if (descriptor.getWriteMethod() == null) {
				describeProperties.remove(descriptor.getName());
			}
		}
	}


	/**
	 * Filters out all bean properties from the given result of the {@link #describe(Object)} method result which do not have the given annotation class.
	 */
	private static void applyDescribeAnnotationFilter(Object bean, Map<String, Object> describeProperties, Class<? extends Annotation> annotationClass) {
		for (String excludedProperty : CollectionUtils.getKeys(getPropertyAnnotations(bean.getClass(), annotationClass))) {
			describeProperties.remove(excludedProperty);
		}
	}


	/**
	 * Gets the collection of retrievable property names for the given class, including all superclasses, which have the given annotation class. Fields and
	 * getters will both be analyzed for the given annotation.
	 * <p>
	 * When duplicate properties exist by shadowed getters, the annotation on the most specific getter for the class will be used.
	 * <p>
	 * Behavior is undefined when duplicate properties exist by shadowed fields or when multiple instances of a repeatable annotation exist on a property.
	 * <p>
	 * Results are cached after the first retrieval.
	 *
	 * @param beanClass       the class for which to retrieve excluded property names
	 * @param annotationClass the annotation type to search for
	 * @return the list of property names which have the given annotation class and qualify the given annotation filter
	 */
	public static <T extends Annotation> Map<String, T> getPropertyAnnotations(Class<?> beanClass, Class<T> annotationClass) {
		// Get all cached annotation maps for this class
		Map<Class<? extends Annotation>, Map<String, ? extends Annotation>> propertyAnnotationsByType = CLASS_PROPERTY_ANNOTATIONS.get(beanClass);
		if (propertyAnnotationsByType == null) {
			/*
			 * "computeIfAbsent" method is atomic but less performant than "get"; avoid execution if possible (similar to synchronization in double-checked
			 * locking). This should be fixed in Java 9.
			 *
			 * See http://jsr166-concurrency.10961.n7.nabble.com/Re-ConcurrentHashMap-computeIfAbsent-td11687.html for discussion.
			 */
			propertyAnnotationsByType = CLASS_PROPERTY_ANNOTATIONS.computeIfAbsent(beanClass, key -> new ConcurrentHashMap<>());
		}

		// Get the map of fields/descriptors to annotation data objects for this class
		Map<String, ? extends Annotation> propertyAnnotations = propertyAnnotationsByType.get(annotationClass);
		if (propertyAnnotations == null) {
			propertyAnnotations = propertyAnnotationsByType.computeIfAbsent(annotationClass, key -> {
				// Cache miss: Generate this annotation cache for class
				// Avoid using ClassUtils.getElementAnnotation throughout to prevent polluting annotation cache with unnecessary objects
				Map<String, T> newPropertyAnnotations = new HashMap<>();
				Target annotationTarget = AnnotationUtils.getAnnotation(annotationClass, Target.class);
				// Get annotated fields
				if (ArrayUtils.contains(annotationTarget.value(), ElementType.FIELD)) {
					for (Field field : ClassUtils.getClassFields(beanClass, true, false)) {
						T annotation = AnnotationUtils.getAnnotation(field, annotationClass);
						if (annotation != null) {
							newPropertyAnnotations.put(field.getName(), annotation);
						}
					}
				}
				// Get annotated descriptors
				if (ArrayUtils.contains(annotationTarget.value(), ElementType.METHOD)) {
					for (PropertyDescriptor descriptor : PropertyUtils.getPropertyDescriptors(beanClass)) {
						if (descriptor.getReadMethod() != null) {
							T annotation = AnnotationUtils.getAnnotation(descriptor.getReadMethod(), annotationClass);
							if (annotation != null) {
								newPropertyAnnotations.put(descriptor.getName(), annotation);
							}
						}
					}
				}
				return Collections.unmodifiableMap(newPropertyAnnotations);
			});
		}

		// Cast appropriately (type shall be guaranteed by computation logic above)
		@SuppressWarnings("unchecked")
		Map<String, T> classPropertyAnnotationsTyped = (Map<String, T>) propertyAnnotations;
		return classPropertyAnnotationsTyped;
	}


	/**
	 * Returns a sub-set of the specified list such that each element of the list has the result of the specified function equal the specified compareValue.
	 * Returns empty list when no matches are found or when the argument beanList is null or empty.
	 */
	public static <T, V> List<T> filter(List<T> beanList, Function<T, V> functionReturningCompareValue, V compareValue) {
		return CollectionUtils.getStream(beanList).filter(bean -> {
			V value = functionReturningCompareValue.apply(bean);
			return Objects.equals(compareValue, value);
		}).collect(Collectors.toList());
	}


	/**
	 * Returns a sub-set of the specified list such that each element of the list satisfies the provided predicate.
	 * Returns empty list when no matches are found or when the argument beanList is null or empty.
	 */
	public static <T> List<T> filter(List<T> beanList, Predicate<T> predicateToFilterWith) {
		return CollectionUtils.getStream(beanList).filter(predicateToFilterWith).collect(Collectors.toList());
	}


	/**
	 * Returns a sub-set of the specified list such that each element of the list has the result of the specified function not equal to null.
	 */
	public static <T, V> List<T> filterNotNull(List<T> beanList, Function<T, V> functionReturningCompareValue) {
		return CollectionUtils.getStream(beanList).filter(bean -> {
			V value = functionReturningCompareValue.apply(bean);
			return (value != null);
		}).collect(Collectors.toList());
	}


	/**
	 * Returns a subset List of the specified beanList such that all beans in returned
	 * List have the specified bean property equal one of the specified values.
	 * <p>
	 * NOTE: Should Use API Methods - This method should only be used for rare cases where we only have a string representation of the property name
	 *
	 * @param matchIfNotEquals      specifies whether EQUAL or NOT EQUAL comparison should be used
	 * @param nullIfPropertyMissing will consider missing properties to have a value of null and will not throw an exception
	 */
	public static <T> List<T> filterByPropertyName(List<T> beanList, String propertyName, Object[] values, boolean matchIfNotEquals, boolean nullIfPropertyMissing) {
		List<T> result = new ArrayList<>();
		if (beanList != null && values != null) {
			for (T bean : beanList) {
				Object beanValue = getPropertyValue(bean, propertyName, nullIfPropertyMissing);
				for (Object value : values) {
					if (matchIfNotEquals) {
						if ((beanValue == null && value != null) || (value == null && beanValue != null) || (value != null && !value.equals(beanValue))) {
							result.add(bean);
						}
					}
					else {
						if (CompareUtils.isEqualWithSmartLogic(beanValue, value)) {
							result.add(bean);
						}
					}
				}
			}
		}
		if (result.isEmpty()) {
			return null;
		}
		return result;
	}


	/**
	 * Returns the list sorted by the given property names & values
	 * in ascending or descending order.  Each property name MUST have a
	 * corresponding ascending flag.
	 * <p>
	 * null or empty lists will just return the given list.  It will not throw an exception.
	 * NOTE: SHOULD ONLY BE USED IN RARE CASES - USE SORT WITH FUNCTION(S) METHODS INSTEAD
	 */
	public static <T> List<T> sortWithPropertyNames(List<T> beanList, final List<String> propertyNames, final List<Boolean> ascending) {
		if (CollectionUtils.isEmpty(beanList)) {
			return beanList;
		}
		AssertUtils.assertNotEmpty(propertyNames, "Property Names to compare cannot be empty");
		AssertUtils.assertTrue(propertyNames.size() == ascending.size(), "Property Names & their corresponding order direction list must be the same size");

		beanList.sort(new Comparator<T>() {

			@Override
			public int compare(T o1, T o2) {
				for (int i = 0; i < propertyNames.size(); i++) {
					String propertyName = propertyNames.get(i);

					Object v1 = BeanUtils.getPropertyValue(o1, propertyName);
					Object v2 = BeanUtils.getPropertyValue(o2, propertyName);

					int comp = compareProperty(v1, v2);
					if (comp != 0) {
						return (ascending.get(i)) ? comp : comp * -1;
					}
				}
				return 0;
			}


			private int compareProperty(Object v1, Object v2) {
				return CompareUtils.compare(v1, v2);
			}
		});
		return beanList;
	}


	/**
	 * Returns the list sorted by the given function in ascending or descending order.
	 * <p>
	 * null or empty lists will just return the given list.  It will not throw an exception.
	 */
	public static <T> List<T> sortWithFunction(List<T> beanList, Function<T, Object> propertyFunction, boolean ascending) {
		return sortWithFunctions(beanList, CollectionUtils.createList(propertyFunction), CollectionUtils.createList(ascending));
	}


	/**
	 * Returns the list sorted by the given functions and associated ascending/descending value.
	 * Each function MUST have a corresponding ascending flag.
	 * <p>
	 * null or empty lists will just return the given list.  It will not throw an exception.
	 */
	public static <T> List<T> sortWithFunctions(List<T> beanList, final List<Function<T, Object>> propertyFunctions, final List<Boolean> ascending) {
		if (CollectionUtils.getSize(beanList) < 2) {
			return beanList;
		}
		AssertUtils.assertNotEmpty(propertyFunctions, "Property Functions to compare cannot be empty");
		AssertUtils.assertTrue(propertyFunctions.size() == CollectionUtils.getSize(ascending), "Property Functions & their corresponding order direction list must be the same size");

		beanList.sort(new Comparator<T>() {

			@Override
			public int compare(T o1, T o2) {
				for (int i = 0; i < propertyFunctions.size(); i++) {
					Function<T, Object> propertyFunction = propertyFunctions.get(i);

					Object v1 = propertyFunction.apply(o1);
					Object v2 = propertyFunction.apply(o2);

					int comp = compareProperty(v1, v2);
					if (comp != 0) {
						return (ascending.get(i)) ? comp : comp * -1;
					}
				}
				return 0;
			}


			private int compareProperty(Object v1, Object v2) {
				return CompareUtils.compare(v1, v2);
			}
		});
		return beanList;
	}


	/**
	 * Produces an index of the items from the given collection, using the given <tt>keyGeneratorFunction</tt> to generate the index keys.
	 * <p>
	 * <b>Note: If more than one item produces the same key, the final such item shall be used as determined by the order of the collection iterator.</b>
	 * <p>
	 * See {@link #getBeansMap(Collection, Function)} if more than one item is desired per key.
	 *
	 * @param itemCollection       the collection to index
	 * @param keyGeneratorFunction the function producing the index keys
	 * @param <K>                  the type of the resulting index key
	 * @param <V>                  the type of the collection to index
	 * @return a map of index keys to elements
	 */
	public static <K, V> Map<K, V> getBeanMap(Collection<V> itemCollection, Function<V, K> keyGeneratorFunction) {
		Map<K, V> result = new HashMap<>();
		for (V bean : CollectionUtils.getIterable(itemCollection)) {
			result.put(keyGeneratorFunction.apply(bean), bean);
		}
		return result;
	}


	/**
	 * Produces an index of the items from the given collection, using the given <tt>keyGeneratorFunction</tt> to generate the index keys.
	 * <p>
	 * <b>Note: This method does not allow key conflicts. If the same key is produced for two elements then an exception will be thrown.</b>
	 *
	 * @throws ValidationException on index key conflict
	 * @see #getBeanMap(Collection, Function)
	 */
	public static <K, V> Map<K, V> getBeanMapUnique(Collection<V> itemCollection, Function<V, K> keyGeneratorFunction) {
		return CollectionUtils.getStream(itemCollection).collect(Collectors.toMap(keyGeneratorFunction, Function.identity(),
				CollectionUtils.throwingMerger("Duplicate elements are not allowed. The following duplicate items were found: [%s], [%s]")));
	}


	/**
	 * Creates and returns a Map from the specified collection such that the specified property value of each bean in the collection is used as the key.  The
	 * value is the collection of beans that match that property value. Null keys are allowed.
	 */
	public static <K, V> Map<K, List<V>> getBeansMap(Collection<V> beanCollection, String keyPropertyName) {
		return getBeansMap(beanCollection, bean -> {
			@SuppressWarnings("unchecked")
			K key = (K) getPropertyValue(bean, keyPropertyName);
			return key;
		});
	}


	/**
	 * Creates and returns a Map from the specified collection such that the specified key value of each bean in the collection is used as the key.  The value
	 * is the List of beans that match that property value. Null keys are allowed.
	 */
	public static <K, V> Map<K, List<V>> getBeansMap(Collection<V> beanCollection, Function<V, K> functionReturningKeyValue) {
		// The groupingBy collector is not used here since it does not permit null keys
		Map<K, List<V>> result = new HashMap<>();
		for (V bean : CollectionUtils.getIterable(beanCollection)) {
			K key = functionReturningKeyValue.apply(bean);
			List<V> value = CollectionUtils.getValue(result, key, ArrayList::new);
			value.add(bean);
		}
		return result;
	}


	/**
	 * Similar to {@link #getBeansMap(Collection, String)}, returns a {@link MultiValueMap} that maps the specified property
	 * value of each bean in the List to a List of beans with the same property value.
	 */
	public static <K, V> MultiValueMap<K, V> getMultiValueMapFromProperty(List<V> list, String propertyName) {
		MultiValueMap<K, V> multiMap = new MultiValueHashMap<>(true);

		for (V obj : CollectionUtils.getIterable(list)) {
			@SuppressWarnings("unchecked")
			K key = (K) getPropertyValue(obj, propertyName);
			multiMap.put(key, obj);
		}

		return multiMap;
	}


	/**
	 * Returns a clone of the bean.  If deep is true, will also clone all properties that are IdentityObjects,
	 * otherwise those properties will be set to the same reference on the clone as it is on the original bean.
	 * <p>
	 * If you includeSystemManagedFields, will also copy the ID field & all system managed fields (createDate, rv, etc)
	 * otherwise these fields will remain null on the clone.
	 */
	public static <T extends IdentityObject> T cloneBean(T bean, boolean deep, boolean includeSystemManagedFields) {
		IdentityHashMap<Object, Object> visitedMap = new IdentityHashMap<>();
		return cloneBean(bean, deep, visitedMap, includeSystemManagedFields);
	}


	@SuppressWarnings({"unchecked"})
	private static <T extends IdentityObject> T cloneBean(T bean, boolean deep, IdentityHashMap<Object, Object> visitedMap, boolean includeSystemManagedFields) {
		if (bean == null) {
			return null;
		}
		if (visitedMap.containsKey(bean)) {
			return (T) visitedMap.get(bean);
		}

		T clonedBean = (T) newInstance(bean.getClass());
		visitedMap.put(bean, clonedBean);
		Field[] fields = ClassUtils.getClassFields(bean.getClass(), true, false);
		if (fields == null || fields.length == 0) {
			return clonedBean;
		}
		try {
			for (Field field : fields) {
				if (!includeSystemManagedFields && (isSystemManagedField(field.getName()) || "id".equals(field.getName()))) {
					continue;
				}
				if (!field.isAccessible()) {
					field.setAccessible(true);
				}
				Object value = field.get(bean);
				Object newValue = value;
				if (deep && value != null) {
					if (value instanceof List<?>) {
						newValue = cloneList((List<?>) value, deep, visitedMap, includeSystemManagedFields);
					}
					else if (value.getClass().isArray()) {
						Class<?> componentTypeClass = value.getClass().getComponentType();
						if (byte.class.equals(componentTypeClass)) {
							newValue = ArrayUtils.cloneArray((byte[]) value);
						}
						else if (int.class.equals(componentTypeClass)) {
							newValue = ArrayUtils.cloneArray((int[]) value);
						}
						else if (boolean.class.equals(componentTypeClass)) {
							newValue = ArrayUtils.cloneArray((boolean[]) value);
						}
						else {
							newValue = cloneArray((Object[]) value, deep, visitedMap, includeSystemManagedFields);
						}
					}
					else if (value instanceof Map) {
						newValue = cloneMap((Map<?, ?>) value, deep, visitedMap, includeSystemManagedFields);
					}
					else if (value instanceof IdentityObject) {
						if (visitedMap.containsKey(value)) {
							newValue = visitedMap.get(value);
						}
						else {
							newValue = cloneBean((IdentityObject) value, deep, visitedMap, includeSystemManagedFields);
						}
					}
				}
				field.set(clonedBean, newValue);
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Exception encountered while trying to clone a bean.", e);
		}
		return clonedBean;
	}


	@SuppressWarnings({"unchecked"})
	private static <T> List<T> cloneList(Collection<T> list, boolean deep, IdentityHashMap<Object, Object> visitedMap, boolean includeSystemManagedFields) {
		if (list == null) {
			return null;
		}
		List<T> clonedList = new ArrayList<>();
		for (T object : list) {
			T clonedObject = object;
			if (deep && object instanceof IdentityObject) {
				clonedObject = (T) cloneBean((IdentityObject) object, deep, visitedMap, includeSystemManagedFields);
			}
			clonedList.add(clonedObject);
		}
		return clonedList;
	}


	@SuppressWarnings({"unchecked"})
	private static <T> T[] cloneArray(T[] array, boolean deep, IdentityHashMap<Object, Object> visitedMap, boolean includeSystemManagedFields) {
		if (array == null) {
			return null;
		}
		// If not doing a deep clone, then use utility method
		if (!deep) {
			return ArrayUtils.cloneArray(array);
		}
		// If doing a deep clone, then manually iterate through array
		T[] clonedArray = null;
		for (T object : array) {
			T clonedObject = object;
			if (object instanceof IdentityObject) {
				clonedObject = (T) cloneBean((IdentityObject) object, deep, visitedMap, includeSystemManagedFields);
			}
			clonedArray = ArrayUtils.add(clonedArray, clonedObject);
		}
		return clonedArray;
	}


	@SuppressWarnings({"unchecked"})
	private static <K, V> Map<K, V> cloneMap(Map<K, V> map, boolean deep, IdentityHashMap<Object, Object> visitedMap, boolean includeSystemManagedFields) {
		if (map == null) {
			return null;
		}
		Map<K, V> clonedMap = new HashMap<>();
		for (Map.Entry<K, V> mapEntry : map.entrySet()) {
			K clonedKey = mapEntry.getKey();
			if (deep && mapEntry.getKey() instanceof IdentityObject) {
				clonedKey = (K) cloneBean((IdentityObject) mapEntry.getKey(), deep, visitedMap, includeSystemManagedFields);
			}
			V value = mapEntry.getValue();
			V clonedValue = value;
			if (deep && value instanceof IdentityObject) {
				clonedValue = (V) cloneBean((IdentityObject) value, deep, visitedMap, includeSystemManagedFields);
			}
			clonedMap.put(clonedKey, clonedValue);
		}
		return clonedMap;
	}


	/**
	 * Copy the property values of the given source bean into the target bean.
	 * <p>Note: The source and target classes do not have to match or even be derived
	 * from each other, as long as the properties match. Any bean properties that the
	 * source bean exposes but the target bean does not will silently be ignored.
	 *
	 * @throws BeansException if the copying failed
	 */
	public static void copyProperties(Object source, Object target) {
		org.springframework.beans.BeanUtils.copyProperties(source, target);
	}


	/**
	 * Copy the property values of the given source bean into the given target bean,
	 * only setting properties defined in the given "editable" class (or interface).
	 * <p>Note: The source and target classes do not have to match or even be derived
	 * from each other, as long as the properties match. Any bean properties that the
	 * source bean exposes but the target bean does not will silently be ignored.
	 *
	 * @param editable the class (or interface) to restrict property setting to
	 * @throws BeansException if the copying failed
	 */
	public static void copyProperties(Object source, Object target, Class<?> editable) {
		org.springframework.beans.BeanUtils.copyProperties(source, target, editable);
	}


	/**
	 * Copy the property values of the given source bean into the given target bean,
	 * ignoring the given "ignoreProperties".
	 * <p>Note: The source and target classes do not have to match or even be derived
	 * from each other, as long as the properties match. Any bean properties that the
	 * source bean exposes but the target bean does not will silently be ignored.
	 *
	 * @throws BeansException if the copying failed
	 */
	public static void copyProperties(Object source, Object target, String[] ignoreProperties) {
		org.springframework.beans.BeanUtils.copyProperties(source, target, ignoreProperties);
	}


	public static void copyPropertiesExceptAudit(Object source, Object target) {
		BeanUtils.copyProperties(source, target, AUDIT_PROPERTIES);
	}


	/**
	 * Copies the property identified by the specified propertyGetter and sets it using the specified propertySetter from the fromBean to the toBean.
	 * If the property value on the toBean already exists, only updates it if overwriteExistingValue is set to true.
	 *
	 * @return returns true if the property value on the toBean was updated
	 */
	public static <O, V> boolean copyProperty(O fromBean, O toBean, Function<O, V> propertyGetter, BiConsumer<O, V> propertySetter, boolean overwriteExistingValue) {
		V fromValue = propertyGetter.apply(fromBean);
		V toValue = propertyGetter.apply(toBean);

		// no need to copy: both values are already the same
		if (!Objects.equals(fromValue, toValue)) {
			if (overwriteExistingValue || toValue == null) {
				propertySetter.accept(toBean, propertyGetter.apply(fromBean));
				return true;
			}
		}

		return false;
	}


	/**
	 * Supports nested properties.  Also supports indexed properties.
	 * Will resolve nested properties such as 'items[0].value.names[0].id'
	 */
	public static Class<?> getPropertyType(Object bean, String propertyName) {
		int dotIndex = propertyName.indexOf('.');
		if (dotIndex != -1) //nested prop
		{
			String nestedPropertyName = propertyName.substring(0, propertyName.indexOf('.'));
			Object nestedParentBean = getPropertyValue(bean, nestedPropertyName, true, true);
			return getPropertyType(nestedParentBean, propertyName.substring(dotIndex + 1));
		}
		if (propertyName.lastIndexOf(']') == propertyName.length() - 1) {
			return getIndexedPropertyType(bean, propertyName);
		}

		return getPropertyType(bean.getClass(), propertyName);
	}


	/**
	 * Will get the type from a indexed property.
	 * Example: if a bean has a List<String> items as a property, then this method will return the Class java.lang.String
	 * for the property items[0]
	 */
	public static Class<?> getIndexedPropertyType(Object bean, String propertyName) {
		try {
			int firstIndex = propertyName.indexOf("[");
			Field field = bean.getClass().getDeclaredField(propertyName.substring(0, firstIndex));
			ParameterizedType type = (ParameterizedType) field.getGenericType();
			return (Class<?>) type.getActualTypeArguments()[0];
		}
		catch (SecurityException e) {
			throw new RuntimeException("SecurityException encountered while trying to access indexed property [" + propertyName + "] of  bean [" + bean.getClass() + "]", e);
		}
		catch (NoSuchFieldException e) {
			throw new RuntimeException("The field [" + propertyName + "] doesn't exist on the bean [" + bean.getClass() + "]", e);
		}
	}


	/**
	 * Convenience utility method that returns <code>null</code> if <code>entity == null</code>, otherwise returns <code>entity.getIdentity()</code>
	 * for any {@link SimpleEntity<T>}.
	 */
	public static <T extends Serializable, U extends SimpleEntity<T>> T getBeanIdentity(U entity) {
		return entity == null ? null : entity.getIdentity();
	}


	private static Serializable getBeanIdentity(IdentityObject identityObject) {
		return identityObject == null ? null : identityObject.getIdentity();
	}


	/**
	 * Returns null if the specified object or its identity is null.
	 * Returns Integer representation of numeric identity otherwise: converts short and long to Integer.
	 */
	public static Integer getIdentityAsInteger(IdentityObject object) {
		if (object != null) {
			Serializable identity = getBeanIdentity(object);
			if (identity instanceof Number) {
				return ((Number) identity).intValue();
			}
		}
		return null;
	}


	/**
	 * Returns null if the specified object or its identity is null.
	 * Returns Long representation of numeric identity otherwise: converts short and int to Long.
	 */
	public static Long getIdentityAsLong(IdentityObject object) {
		if (object != null) {
			Serializable identity = getBeanIdentity(object);
			if (identity instanceof Number) {
				return ((Number) identity).longValue();
			}
		}
		return null;
	}


	/**
	 * Returns a List of identities for a provided list of {@link IdentityObject}s. The list will never be null.
	 * <p>
	 * If a null or empty list is provided, the returned list will have a length of 0.
	 * If an entity in the list is null, or an entity has a null id, it is excluded from the returned list.
	 */
	@SuppressWarnings("unchecked")
	public static <T extends Serializable> List<T> getBeanIdentityList(Collection<? extends IdentityObject> entityList) {
		List<T> idList = new ArrayList<>();
		for (IdentityObject entity : CollectionUtils.getIterable(entityList)) {
			T id = (T) getBeanIdentity(entity);
			if (id != null) {
				idList.add(id);
			}
		}

		return idList;
	}


	/**
	 * Returns an array of identities for a provided list of {@link IdentityObject}s. The array will never be null.
	 * <p>
	 * If a null or empty list is provided, the returned array will have a length of 0.
	 * If an entity in the list is null, or an entity has a null id, it is excluded from the returned array.
	 */
	@SuppressWarnings({"SuspiciousToArrayCall", "unchecked"})
	public static <T extends Serializable> T[] getBeanIdentityArray(Collection<? extends IdentityObject> entityList, Class<T> idClass) {
		return CollectionUtils.getStream(entityList)
				.map(entity -> entity == null ? null : entity.getIdentity())
				.filter(Objects::nonNull)
				.toArray(size -> (T[]) Array.newInstance(idClass, size));
	}


	/**
	 * Creates a String key representation that identifies the list of beans in the form of K1-K2-K3...-Kx, where each K identifies the corresponding
	 * bean. For each bean, if it is an instance of {@link IdentityObject}, then the String representation of its identity is used for its key component,
	 * otherwise its <code>toString()</code> method is used.
	 * <p>
	 * This method can be used with maps to avoid needing to create boilerplate key classes.
	 *
	 * @return a String key representation of the beans, compromised of identifiers for each bean delimited by dashes
	 */
	public static String createKeyFromBeans(Object... beans) {
		String result = ArrayUtils.getStream(beans)
				.map(bean -> {
					if (bean instanceof IdentityObject) {
						Serializable identity = ((IdentityObject) bean).getIdentity();
						return identity == null ? "" : identity.toString();
					}
					else if (bean instanceof Collection) {
						return StringUtils.collectionToCommaDelimitedString((Collection<?>) bean);
					}
					else if (bean != null) {
						return bean.toString();
					}
					return "null";
				})
				.collect(Collectors.joining("-"));
		return result;
	}


	public interface BeanPropertyRetriever<T, R> {

		public R getPropertyValue(T bean);
	}
}
