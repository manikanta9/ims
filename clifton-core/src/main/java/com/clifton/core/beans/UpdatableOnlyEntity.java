package com.clifton.core.beans;

import java.util.Date;


/**
 * The <code>UpdatableOnlyEntity</code> interface should be implemented by DTO's that have record stamp fields
 * and update fields which can be automatically managed by the corresponding DAO's.
 *
 * @author theodorez
 */
public interface UpdatableOnlyEntity {

	public short getUpdateUserId();


	public void setUpdateUserId(short updateUserId);


	public Date getUpdateDate();


	public void setUpdateDate(Date updateDate);


	/**
	 * Returns the row version for this DTO. It's usually a database specific implementation
	 * of the column that cannot be modified directly but is updated by the database every
	 * time the row is modified (can be unique id from transaction log).
	 */
	public byte[] getRv();


	/**
	 * Sets the row version for this DTO. It's usually a database specific implementation
	 * of the column that cannot be modified directly but is updated by the database every
	 * time the row is modified (can be unique id from transaction log).
	 *
	 * @param rv
	 */
	public void setRv(byte[] rv);
}
