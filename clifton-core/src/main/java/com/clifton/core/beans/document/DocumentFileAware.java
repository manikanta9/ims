package com.clifton.core.beans.document;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>DocumentFileAware</code> interface should be implemented in order to use Document Management functionality for support of multiple files per entity
 * This means that there are entries in our "DocumentFile" table associated with the entity.
 * i.e. Billing Invoices can have multiple "attachments"
 * <p/>
 * Interface is in core so that projects that implement this functionality don't need to have their code base depend on document project
 * <p/>
 * Registers DAOs that implement this interface so when the related entity is deleted all of it's files are also deleted
 *
 * @author manderson
 */
public interface DocumentFileAware extends IdentityObject {

	// Nothing here
}
