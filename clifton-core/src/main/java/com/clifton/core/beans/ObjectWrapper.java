package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>ObjectWrapper</code> class can be used when it's necessary to pass a method argument
 * by reference. Its values can be set and retrieved.
 *
 * @param <T>
 * @author vgomelsky
 */
public class ObjectWrapper<T> implements Serializable {

	private T object;

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public ObjectWrapper() {
		super();
	}


	public ObjectWrapper(T obj) {
		this.object = obj;
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		if (this.object == null) {
			return "Object is null";
		}
		return this.object.toString();
	}


	public boolean isPresent() {
		return getObject() != null;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void setObject(T obj) {
		this.object = obj;
	}


	public T getObject() {
		return this.object;
	}
}
