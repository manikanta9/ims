package com.clifton.core.beans.common;


/**
 * The <code>AddressObject</code> class defines the methods
 * that an entity object that contains Address information should implement.
 *
 * @author manderson
 */
public interface AddressObject {

	public String getAddressLabel();


	public String getCityState();


	public String getCityStatePostalCode();


	//////////////////////////////////////


	public String getAddressLine1();


	public void setAddressLine1(String addressLine1);


	public String getAddressLine2();


	public void setAddressLine2(String addressLine2);


	public String getAddressLine3();


	public void setAddressLine3(String addressLine3);


	public String getCity();


	public void setCity(String city);


	public String getState();


	public void setState(String state);


	public String getPostalCode();


	public void setPostalCode(String postalCode);


	public String getCountry();


	public void setCountry(String country);
}
