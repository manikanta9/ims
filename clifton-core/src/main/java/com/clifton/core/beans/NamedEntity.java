package com.clifton.core.beans;


import java.io.Serializable;


/**
 * The <code>NamedEntity</code> class can be extended by a lot of entities that have name/description/label properties.
 *
 * @author vgomelsky
 */
public class NamedEntity<T extends Serializable> extends NamedEntityWithoutLabel<T> {

	private String label;


	public NamedEntity() {
		super();
	}


	public NamedEntity(String name, String description) {
		super(name, description);
	}


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(20);
		result.append("{id=");
		result.append(getId());
		result.append(",label=");
		result.append(getLabel());
		result.append('}');
		return result.toString();
	}


	/**
	 * Returns the value of label field if it's not null.
	 * Otherwise returns the value of name field.
	 *
	 * @return the label
	 */
	@Override
	public String getLabel() {
		return (this.label == null) ? this.getName() : this.label;
	}


	/**
	 * @param label the label to set
	 */
	public void setLabel(String label) {
		this.label = label;
	}
}
