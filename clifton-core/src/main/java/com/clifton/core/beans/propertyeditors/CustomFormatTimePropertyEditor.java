package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.Time;

import java.beans.PropertyEditorSupport;


/**
 * The <code>CustomFormatTimePropertyEditor</code> allows code to load date strings into properties when loading data from xml.  You can set
 * the time format by adding "f=format".  The default format is "HH:mm:ss".
 * <p/>
 * Examples:
 * p:minuteStartTime="8:00 am f=h:mm aaa"
 * p:minuteStartTime="17:00:00  f=HH:mm:ss"
 *
 * @author mwacker
 */
public class CustomFormatTimePropertyEditor extends PropertyEditorSupport {

	private final boolean allowEmpty;


	public CustomFormatTimePropertyEditor() {
		this(true);
	}


	public CustomFormatTimePropertyEditor(boolean allowEmpty) {
		this.allowEmpty = allowEmpty;
	}


	/**
	 * Parse the Date from the given text, using the specified DateFormat.
	 */
	@Override
	@ValueChangingSetter
	public void setAsText(String text) throws IllegalArgumentException {
		String format;
		if (text.contains("f=")) {
			String[] items = text.split("f=");
			if (items.length != 2) {
				throw new IllegalArgumentException("Cannot convert '" + text + "' to a Date.");
			}
			text = items[0].trim();
			format = items[1].trim();
		}
		else {
			format = Time.TIME_FORMAT_FULL;
		}
		if (this.allowEmpty && StringUtils.isEmpty(text)) {
			// Treat empty String as null value.
			setValue(null);
		}
		else {
			setValue(Time.parse(text, format));
		}
	}


	/**
	 * Format the Date as String, using the specified DateFormat.
	 */
	@Override
	public String getAsText() {
		Time value = (Time) getValue();
		return (value != null ? value.toString() : "");
	}
}
