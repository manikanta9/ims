package com.clifton.core.beans.document;


/**
 * The <code>DocumentFileCountAware</code> is an extension of {@link DocumentFileAware} interface but also
 * supports a file count on the related entity
 *
 * @author manderson
 */
public interface DocumentFileCountAware extends DocumentFileAware {

	/**
	 * Returns the total documents tied to the entity
	 */
	public Short getDocumentFileCount();


	/**
	 * Sets the total documents tied to the entity
	 */
	public void setDocumentFileCount(Short documentFileCount);
}
