package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.StringUtils;

import java.beans.PropertyEditorSupport;


public class ShortArrayPropertyEditor extends PropertyEditorSupport {

	@Override
	@ValueChangingSetter
	public void setAsText(String text) {
		if (StringUtils.isEmpty(text)) {
			setValue(null);
		}
		else {
			String[] fromElements = text.replaceAll("[\\[\\]]", "")
					.split(text.contains("::") ? "::" : ",");
			Short[] result = new Short[fromElements.length];
			for (int i = 0; i < fromElements.length; i++) {
				result[i] = Short.parseShort(fromElements[i]);
			}
			setValue(result);
		}
	}
}
