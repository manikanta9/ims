package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.date.TimeUtils;

import java.beans.PropertyEditorSupport;


/**
 * The <code>TimePropertyEditor</code> class converts strings to corresponding {@link Time}'s objects.
 *
 * @author vgomelsky
 */
public class TimePropertyEditor extends PropertyEditorSupport {

	@Override
	@ValueChangingSetter
	public void setAsText(String text) {
		super.setValue(TimeUtils.toTime(text));
	}


	@Override
	public String getAsText() {
		return super.getValue().toString();
	}
}
