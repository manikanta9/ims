package com.clifton.core.beans.document;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>DocumentAware</code> interface should be implemented in order to use Document Management functionality for records that can have
 * one document associated with the entity in the Document Management System.
 * i.e. Business Contract has one document associated with it.
 * <p>
 * Interface is in core so that projects that implement this functionality don't need to have their code base depend on document project
 * i.e. SystemNotes uses document management, but can't depend on document project because document project already depends on system
 *
 * @author manderson
 */
public interface DocumentAware extends IdentityObject {

	/**
	 * Returns the document type, i.e. pdf, doc, xls, etc.
	 */
	public String getDocumentFileType();


	/**
	 * Sets the document type, i.e. pdf, doc, xls, etc.
	 */
	public void setDocumentFileType(String documentType);
}
