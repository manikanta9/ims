package com.clifton.core.beans.propertyeditors;


import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;

import java.beans.PropertyEditorSupport;
import java.util.Date;


/**
 * The <code>DatePropertyEditor</code> class converts strings to corresponding {@link Date}'s objects.
 *
 * @author vgomelsky
 */
public class DatePropertyEditor extends PropertyEditorSupport {

	@Override
	@ValueChangingSetter
	public void setAsText(String text) {
		if (StringUtils.isEmpty(text)) {
			super.setValue(null);
			return;
		}
		String formatStr = DateUtils.DATE_FORMAT_INPUT;
		if (text.contains(":")) {
			if (!text.contains("/")) {
				formatStr = "";
			}
			else {
				formatStr += " ";
			}
			if (text.endsWith("AM") || text.endsWith("PM")) {
				formatStr += Time.TIME_FORMAT_SHORT;
			}
			else {
				formatStr += Time.TIME_FORMAT_FULL;
			}
		}
		try {
			super.setValue(DateUtils.toDate(text, formatStr));
		}
		catch (IllegalArgumentException e) {
			try {
				super.setValue(DateUtils.toDate(text));
			}
			catch (IllegalArgumentException ex) {
				throw new IllegalArgumentException(String.format("Unable to parse date field '%s' using any supported formats.", text));
			}
		}
	}


	@Override
	public String getAsText() {
		return DateUtils.fromDate((Date) super.getValue());
	}
}
