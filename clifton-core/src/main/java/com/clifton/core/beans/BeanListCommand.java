package com.clifton.core.beans;

import java.util.Collections;
import java.util.Iterator;
import java.util.List;


/**
 * <code>BeanListCommand</code> is a base command object responsible for holding a List of objects.
 * A primary use of this type is for aiding in binding a list of beans from the UI for service APIs.
 *
 * @author NickK
 */
public class BeanListCommand<T> implements Iterable<T> {

	private List<T> beanList;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public Iterator<T> iterator() {
		return getBeanList() == null ? Collections.<T>emptyList().iterator() : getBeanList().iterator();
	}

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public List<T> getBeanList() {
		return this.beanList;
	}


	public void setBeanList(List<T> beanList) {
		this.beanList = beanList;
	}
}
