package com.clifton.core.beans;

import java.io.Serializable;
import java.util.Date;


/**
 * The <code>BaseUpdatableOnlyEntity</code> class should be extended by all DTO objects that represent database entities
 * with rv and update attributes
 *
 * @author theodorez
 */
public abstract class BaseUpdatableOnlyEntity<T extends Serializable> extends BaseSimpleEntity<T> implements UpdatableOnlyEntity {

	private short updateUserId;
	private Date updateDate;
	private byte[] rv;


	@Override
	public short getUpdateUserId() {
		return this.updateUserId;
	}


	@Override
	public void setUpdateUserId(short updateUserId) {
		this.updateUserId = updateUserId;
	}


	@Override
	public Date getUpdateDate() {
		return this.updateDate;
	}


	@Override
	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}


	@Override
	public byte[] getRv() {
		return this.rv;
	}


	@Override
	public void setRv(byte[] rv) {
		this.rv = rv;
	}
}
