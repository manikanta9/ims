package com.clifton.core.converter.json.appenders;

import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.util.date.DateUtils;

import java.io.IOException;
import java.time.OffsetDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


/**
 * The {@link OffsetDateTimeValueAppender} class allows converting {@link OffsetDateTime} objects to a predefined string format.
 *
 * @author lnaylor
 */
public class OffsetDateTimeValueAppender implements JsonValueAppender {

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_FULL).withZone(ZoneId.systemDefault());

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else {
			OffsetDateTime offsetDateTimeValue = (OffsetDateTime) value;
			resultAppender.append('\"');
			resultAppender.append(offsetDateTimeValue.format(getFormatter()));
			resultAppender.append('\"');
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DateTimeFormatter getFormatter() {
		return this.formatter;
	}


	public void setFormatter(DateTimeFormatter formatter) {
		this.formatter = formatter;
	}
}
