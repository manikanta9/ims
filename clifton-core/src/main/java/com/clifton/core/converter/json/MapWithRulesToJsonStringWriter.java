package com.clifton.core.converter.json;


import com.clifton.core.converter.json.appenders.CollectionValueAppender;
import com.clifton.core.converter.json.appenders.ConstantStringAppender;
import com.clifton.core.converter.json.appenders.CustomJsonStringValueAppender;
import com.clifton.core.converter.json.appenders.DataTableValueAppender;
import com.clifton.core.converter.json.appenders.DateValueAppender;
import com.clifton.core.converter.json.appenders.EmptyValueAppender;
import com.clifton.core.converter.json.appenders.LocalDateValueAppender;
import com.clifton.core.converter.json.appenders.MapValueAppender;
import com.clifton.core.converter.json.appenders.OffsetDateTimeValueAppender;
import com.clifton.core.converter.json.appenders.PagingArrayListValueAppender;
import com.clifton.core.converter.json.appenders.StringValueAppender;
import com.clifton.core.converter.json.appenders.TimeValueAppender;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.json.custom.CustomJsonString;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.Time;
import org.hibernate.proxy.pojo.javassist.JavassistLazyInitializer;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;


/**
 * The {@link MapWithRulesToJsonStringWriter} type serializes {@link Map} objects as JSON strings.
 * <p>
 * This type incorporates filtering rules as configured through the {@link MapWithRules} passed into the {@link #write(Appendable, MapWithRules)} method. For example, the writer
 * can be configured to {@link JsonConfig#skipNullFields skip null value properties}, to {@link MapWithRules#maxAllowedDepth set a maximum allowed depth}, to {@link
 * JsonConfig#registerAppender(Class, JsonValueAppender) configure custom appenders}, or to {@link MapWithRules#propertiesToFilter set explicit properties to include}.
 *
 * @author vgomelsky
 */
public class MapWithRulesToJsonStringWriter {

	private JsonConfig jsonConfig;
	/**
	 * Optional appender mappings to add to the JsonConfig
	 */
	private Map<Class<?>, JsonValueAppender> additionalAppenderMap;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public <T extends Appendable> T write(T output, MapWithRules from) {
		Objects.requireNonNull(output, "The output parameter must be non-null.");
		try {
			// Guard-clause: Serialize empty/missing object
			if (from == null || from.getMap() == null) {
				output.append("{}");
				return output;
			}

			JsonConfig config = getJsonConfig(from.getJsonConfigInitializer());
			JsonValueAppender valueAppender = config.getAppender(from.getMap());
			// TODO: refactor the MapToJson to eliminate duplicate logic
			valueAppender.append(output, null, from.getMap(), config, new JsonSession(from.getMaxAllowedDepth(), from.isSkipExtraDepth(), from.getFilterStartPath(), from.getPropertiesToFilter(), from.getPropertiesToExcludeFilter(), from.getPropertiesToExcludeGloballyFilter(), from.getEntityConversionMap()));
			return output;
		}
		catch (IOException e) {
			throw new RuntimeException("An exception occurred while writing serialized content.", e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns initialized JsonConfig object.
	 */
	protected JsonConfig getJsonConfig(Consumer<JsonConfig> initializer) {
		if (this.jsonConfig == null) {
			JsonConfig config = new JsonConfig();
			config.registerAppender(String.class, new StringValueAppender());
			config.registerAppender(Date.class, new DateValueAppender());
			config.registerAppender(OffsetDateTime.class, new OffsetDateTimeValueAppender());
			config.registerAppender(LocalDate.class, new LocalDateValueAppender());
			config.registerAppender(Time.class, new TimeValueAppender());
			config.registerAppender(DataTable.class, new DataTableValueAppender());
			config.registerAppender(PagingArrayList.class, new PagingArrayListValueAppender());
			config.registerAppender(Map.class, new MapValueAppender());
			config.registerAppender(Collection.class, new CollectionValueAppender());
			// ignore the following properties (better to mark them with annotations?)
			config.registerAppender(MultipartFile.class, new ConstantStringAppender("MultipartFile"));
			// ignore the properties add by javassist
			config.registerAppender(JavassistLazyInitializer.class, new EmptyValueAppender());
			// String is already JSON so nothing to do
			config.registerAppender(CustomJsonString.class, new CustomJsonStringValueAppender());
			// add optional additional appenders
			addAdditionalAppenders(config);
			// skip properties with null values
			config.setSkipNullFields(true);

			if (initializer != null) {
				initializer.accept(config);
			}

			this.jsonConfig = config;
		}
		return this.jsonConfig;
	}


	private void addAdditionalAppenders(JsonConfig config) {
		if (this.additionalAppenderMap != null && !this.additionalAppenderMap.isEmpty()) {
			for (Map.Entry<Class<?>, JsonValueAppender> entry : this.additionalAppenderMap.entrySet()) {
				config.registerAppender(entry.getKey(), entry.getValue());
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<Class<?>, JsonValueAppender> getAdditionalAppenderMap() {
		return this.additionalAppenderMap;
	}


	public void setAdditionalAppenderMap(Map<Class<?>, JsonValueAppender> additionalAppenderMap) {
		this.additionalAppenderMap = additionalAppenderMap;
	}
}
