package com.clifton.core.converter.json.jackson;

import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.hibernate5.Hibernate5Module;

import java.util.TimeZone;


/**
 * @author StevenF
 */
public class JacksonObjectMapper extends ObjectMapper {

	public static final String EXCLUSION_FILTER_NAME = "ExclusionFilter";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JacksonObjectMapper() {
		init(new JacksonStrategy());
	}


	public JacksonObjectMapper(JacksonStrategy jacksonStrategy) {
		init(jacksonStrategy);
	}


	private void init(JacksonStrategy jacksonStrategy) {
		//Allow the strategy to implement a custom method in order to register custom adapters (e.g. Date serializer/deserializer)
		jacksonStrategy.registerModules(this);

		//We register our filter provider globally right now; this allows the object mapper to respect the
		//exclusion filter when used in other jackson implementations (e.g. bi-directional with Spring)
		this.setFilterProvider(jacksonStrategy.getFilterProvider());
		//This is a core add-on module that deals with hibernate data-types such as proxy classes, auto-magically.
		this.registerModule(new Hibernate5Module());
		// Support custom domain object properties
		this.addMixIn(IdentityObject.class, IdentityObjectMixin.class);
		//Only include non null values in serialization
		if (!jacksonStrategy.isIncludeNullValues()) {
			this.setSerializationInclusion(JsonInclude.Include.NON_NULL);
		}
		//Default is to turn off 'pretty printing' to reduce size of json data response to UI.
		this.configure(SerializationFeature.INDENT_OUTPUT, jacksonStrategy.isUsePrettyPrinting());
		//Set the timezone to CST (GMT-5) and use ISO date format so it is more
		//readable in case someone wants to tweak dates for tests after exporting.
		this.setTimeZone(TimeZone.getTimeZone("GMT-5"));
		this.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
		this.configure(SerializationFeature.WRITE_ENUMS_USING_TO_STRING, true);
		//Ensure values in JSON without setters will not cause deserialization to fail
		//e.g. computed getters that provided serialized values, but need not be deserialized.
		this.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
		this.configure(DeserializationFeature.READ_ENUMS_USING_TO_STRING, true);
		// Prevent Jackson from attempting to use and mutate the value returned from the getter when the corresponding setter to not exist
		this.configure(MapperFeature.USE_GETTERS_AS_SETTERS, false);
		if (jacksonStrategy.isMigration()) {
			//For migrations we don't care about computed getters (e.g. label, etc) - this configuration sets
			//serialization to be based off of field only, not getter/setter methods, etc.
			//This can significantly reduce the size of the JSON generated (I noticed a 66% reduction in one case)
			this.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
			this.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);
		}
		if (jacksonStrategy.isDisableAnnotations()) {
			//Disable jackson annotations - we only use one annotation currently 'JsonIdentityInfo':
			//It is used to prevent circular references during json migrations, max depth should
			//prevent circular references during basic serialization (once implemented) so at that
			//point the json identity info would simply be adding unnecessary json data.
			this.disable(MapperFeature.USE_ANNOTATIONS);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@JsonFilter(EXCLUSION_FILTER_NAME)
	public static interface IdentityObjectMixin {}
}
