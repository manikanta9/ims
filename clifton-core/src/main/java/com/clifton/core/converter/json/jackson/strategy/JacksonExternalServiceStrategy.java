package com.clifton.core.converter.json.jackson.strategy;

import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonPagingArrayListDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonPagingArrayListSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonTimeDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonTimeSerializer;
import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.date.Time;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.math.BigDecimal;
import java.util.Date;


/**
 * This simply keeps the annotations enabled (to prevent circular references
 * and allow for the Exclusion Filter code, potentially other customizations
 * will be added here as the external service json is used more.
 *
 * @author stevenf
 */
public class JacksonExternalServiceStrategy extends JacksonStrategy {

	@Override
	public void registerModules(ObjectMapper objectMapper) {
		SimpleModule module = new SimpleModule();

		module.addSerializer(BigDecimal.class, new JacksonBigDecimalSerializer());
		module.addSerializer(Date.class, new JacksonDateSerializer());
		module.addSerializer(Time.class, new JacksonTimeSerializer(Time.TIME_FORMAT_SHORT));
		module.addSerializer(PagingArrayList.class, new JacksonPagingArrayListSerializer());

		module.addDeserializer(BigDecimal.class, new JacksonBigDecimalDeserializer());
		module.addDeserializer(Date.class, new JacksonDateDeserializer());
		module.addDeserializer(Time.class, new JacksonTimeDeserializer());
		module.addDeserializer(PagingArrayList.class, new JacksonPagingArrayListDeserializer());

		objectMapper.registerModule(module);
	}
}
