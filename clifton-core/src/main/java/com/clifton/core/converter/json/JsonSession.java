package com.clifton.core.converter.json;


import com.clifton.core.util.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>JsonSession</code> class is serialization session specific and can be used for
 * session related data (keeping track of serialized beans in order to avoid circular references).
 * <p/>
 * <p/>
 * NOTE: Filtering logic is very complicated. I can't think of a better way to do this now. It works because of high test coverage.
 *
 * @author vgomelsky
 */
public class JsonSession {

	/**
	 * Maximum allowed depth (number of sub-elements from the root).
	 */
	private int maxAllowedDepth;
	/**
	 * When maxAllowedDepth is exceeded, skip remaining nodes or throw an exception?
	 */
	private boolean skipExtraDepth;
	/**
	 * A stack of visited parent beans for currently rendered bean in corresponding order.
	 */
	private List<Object> visitedBeansPath;
	/**
	 * The conversion map for converting source objects into target objects for serialization. This is similar to registering a custom serializer, but is differentiated by its
	 * applicability to only the instances which are registered in this map.
	 * <p>
	 * For example, if a specific entity should be excluded from serialization, then an entry with the entity as the key and a <code>null</code> value may be added to this map.
	 * <p>
	 * This can also be used in the event that an newer version of an entity is available. For example, automatically transitioned workflow-aware entities are added to this map
	 * when transitions are delayed. Since the original version of the entity is not mutated, the <code>entityConversionMap</code> must be used to transform all instances of the
	 * original entity to the new entity before serialization.
	 *
	 * @see #getSerializeEntity(Object)
	 */
	private final Map<Object, Object> entityConversionMap;

	private final String filterStartPath;
	/**
	 * If a filter start path is provided, create a replacement target to match and replace at construction to avoid creating it multiple times.
	 */
	private final String filterStartPathReplacementTarget;

	private Map<String, Map<String, ?>> propertiesToFilter;
	private List<String> propertiesToExclude;
	private List<String> propertiesToExcludeGlobally;

	// parameters used for filtering
	private String currentPathToFilter;
	private String currentPath;
	private boolean filteringEnabled;
	private boolean filterStartPathMatched;
	private boolean filterMatched;
	private Map<String, Map<String, ?>> currentlyMatchedNode;
	private Map<Integer, Map<String, Map<String, ?>>> matchedPathMap;
	private int afterMatchedDepthCount;

	private int currentDepth;
	private int currentlyMatchedNodeDepth;
	private boolean skipNextProperty;
	private int filterStartPathDepth;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JsonSession(int maxAllowedDepth, boolean skipExtraDepth, String filterStartPath, Map<String, Map<String, ?>> propertiesToFilter, List<String> propertiesToExclude, List<String> propertiesToExcludeGlobally, Map<Object, Object> entityConversionMap) {
		if (maxAllowedDepth < 1) {
			throw new IllegalArgumentException("maxAllowedDepth of " + maxAllowedDepth + " cannot be less than 1");
		}

		this.maxAllowedDepth = maxAllowedDepth;
		this.skipExtraDepth = skipExtraDepth;
		if (propertiesToFilter != null) {
			this.filteringEnabled = true;
			this.propertiesToFilter = propertiesToFilter;
		}
		this.filterStartPath = filterStartPath;
		this.filterStartPathReplacementTarget = this.filterStartPath == null ? null : this.filterStartPath + '.';

		this.propertiesToExclude = propertiesToExclude;
		this.propertiesToExcludeGlobally = propertiesToExcludeGlobally;
		this.entityConversionMap = entityConversionMap;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Starts visit of a node that may need to be filtered out.
	 */
	@SuppressWarnings("unchecked")
	public void pushFilterPathNode(String propertyName) {
		// keep track of the current path
		this.currentPath = (this.currentPath == null) ? propertyName : this.currentPath + '.' + propertyName;

		if (this.filteringEnabled) {
			this.currentDepth++;
			if (!this.filterStartPathMatched) {
				// check if "filterStartPath" is matched and, if so, enable "filterStartPathMatched"
				if (this.filterStartPath == null) {
					this.filterStartPathMatched = true;
				}
				else {
					this.currentPathToFilter = (this.currentPathToFilter == null) ? propertyName : this.currentPathToFilter + '.' + propertyName;
					if (this.filterStartPath.equals(this.currentPathToFilter)) {
						this.filterStartPathMatched = true;
						this.currentlyMatchedNode = this.propertiesToFilter;
						this.currentlyMatchedNodeDepth = this.currentDepth;
						this.filterStartPathDepth = this.currentDepth;
						return;
					}
				}
			}

			// filter is enabled only for properties that already matched the "filterStartPath"
			if (this.filterStartPathMatched) {
				this.skipNextProperty = false;
				if (this.filterMatched) {
					this.afterMatchedDepthCount++;
				}
				else {
					if (this.currentlyMatchedNode == null) {
						this.currentlyMatchedNode = this.propertiesToFilter;
						this.currentlyMatchedNodeDepth = this.currentDepth;
					}
					if (this.currentlyMatchedNode.containsKey(propertyName)) {
						Map<String, ?> nextNode = this.currentlyMatchedNode.get(propertyName);
						if (nextNode == null) {
							this.filterMatched = true;
						}
						else {
							if (this.matchedPathMap == null) {
								this.matchedPathMap = new HashMap<>();
							}
							this.matchedPathMap.put(this.currentDepth, this.currentlyMatchedNode);
							this.currentlyMatchedNode = (Map<String, Map<String, ?>>) nextNode;
							this.currentlyMatchedNodeDepth = this.currentDepth;
						}
					}
					else {
						this.skipNextProperty = true;
					}
				}
			}
		}
	}


	/**
	 * Resets filtering configuration after the node was visited.
	 */
	public void popFilterPathNode() {
		// keep track of the current path
		int ix = this.currentPath != null ? this.currentPath.lastIndexOf('.') : -1;
		this.currentPath = ix == -1 ? null : this.currentPath.substring(0, ix);

		if (this.filteringEnabled) {
			this.currentDepth--;
			if (this.filterMatched && this.afterMatchedDepthCount > 0) {
				this.afterMatchedDepthCount--;
			}
			else {
				if (!this.filterStartPathMatched || this.currentDepth < this.filterStartPathDepth) {
					int index = this.currentPathToFilter.lastIndexOf('.');
					if (index == -1) {
						this.currentPathToFilter = null;
					}
					else {
						this.currentPathToFilter = this.currentPathToFilter.substring(0, index);
					}
					this.filterStartPathMatched = false;
				}
				else {
					this.filterMatched = false;
					if (this.currentlyMatchedNodeDepth > this.currentDepth) {
						if (this.matchedPathMap == null || this.matchedPathMap.isEmpty()) {
							this.currentlyMatchedNode = null;
						}
						else {
							if (this.matchedPathMap.containsKey(this.currentDepth + 1)) {
								this.currentlyMatchedNode = this.matchedPathMap.get(this.currentDepth + 1);
								this.matchedPathMap.remove(this.currentDepth + 1);
							}
						}
					}
				}
			}
		}
	}


	/**
	 * Returns true when current property should be skipped.
	 */
	public boolean isFilteredOut() {
		if ((this.propertiesToExcludeGlobally != null)) {
			int lastPeriodIndex = this.currentPath.lastIndexOf('.');
			if (this.propertiesToExcludeGlobally.contains(lastPeriodIndex > -1 ? this.currentPath.substring(lastPeriodIndex + 1) : this.currentPath)) {
				return true;
			}
		}
		if (this.propertiesToExclude != null && this.filterStartPathReplacementTarget != null
				&& this.propertiesToExclude.contains(StringUtils.replace(this.currentPath, this.filterStartPathReplacementTarget, ""))) {
			return true;
		}
		if (!this.filteringEnabled || !this.filterStartPathMatched) {
			// don't filter out if filtering is disabled or filterStartPath hasn't been matched
			return false;
		}
		if (this.filterMatched) {
			return false;
		}
		if (this.skipNextProperty) {
			return true;
		}
		// filter hasn't been matched: keep if there's still a possibility
		return (this.currentlyMatchedNode == null);
	}


	/**
	 * Returns true if the specified bean was visited as specified beans parent, grandparent, etc.
	 * Returns false otherwise.
	 */
	public boolean isVisitedParent(Object bean) {
		if (this.visitedBeansPath == null) {
			return false;
		}
		if (this.visitedBeansPath.size() >= this.maxAllowedDepth) {
			if (this.skipExtraDepth) {
				return true;
			}

			// generate detailed message to help identify the problem
			StringBuilder details = new StringBuilder(1024);
			try {
				int maxSize = (this.visitedBeansPath.size() > 10) ? 10 : this.visitedBeansPath.size();
				for (int i = 0; i < maxSize; i++) {
					details.append(i);
					details.append(" = ");
					details.append(this.visitedBeansPath.get(this.visitedBeansPath.size() - i - 1));
					details.append("\n");
				}
			}
			catch (Throwable e) {
				details.append("ERROR: ").append(e.getMessage());
			}
			Class<?> lastClass = this.visitedBeansPath.get(this.visitedBeansPath.size() - 1).getClass();
			throw new IllegalStateException("Exceeded max allowed depth of " + this.maxAllowedDepth + " during JSON generation for " + lastClass + "\n\n" + details);
		}
		for (Object visitedBean : this.visitedBeansPath) {
			if (visitedBean == null) {
				return (bean == null);
			}
			else if (visitedBean.equals(bean)) {
				return true;
			}
		}
		return false;
	}


	/**
	 * Adds the specified bean to the END of the visited beans list.
	 */
	public void pushVisited(Object bean) {
		if (this.visitedBeansPath == null) {
			this.visitedBeansPath = new ArrayList<>();
		}
		this.visitedBeansPath.add(bean);
	}


	/**
	 * Removes last added bean from the list.
	 */
	public void popVisited() {
		int size = (this.visitedBeansPath == null) ? 0 : this.visitedBeansPath.size();
		if (size > 0) {
			this.visitedBeansPath.remove(size - 1);
		}
	}


	/**
	 * Gets the target object to serialize in place the given object. If a {@link #entityConversionMap conversion entity} is provided then that entity will be returned. Otherwise,
	 * the source entity will be returned.
	 */
	public Object getSerializeEntity(Object object) {
		return this.entityConversionMap != null
				? this.entityConversionMap.getOrDefault(object, object)
				: object;
	}
}
