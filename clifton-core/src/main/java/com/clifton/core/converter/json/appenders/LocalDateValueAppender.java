package com.clifton.core.converter.json.appenders;

import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.util.date.DateUtils;

import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;


/**
 * The {@link LocalDateValueAppender} class allows converting {@link java.time.LocalDate} objects to a predefined string format.
 *
 * @author lnaylor
 */
public class LocalDateValueAppender implements JsonValueAppender {

	private DateTimeFormatter formatter = DateTimeFormatter.ofPattern(DateUtils.DATE_FORMAT_ISO).withZone(ZoneId.systemDefault());

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else {
			LocalDate localDateValue = (LocalDate) value;
			resultAppender.append('\"');
			resultAppender.append(localDateValue.format(getFormatter()));
			resultAppender.append('\"');
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DateTimeFormatter getFormatter() {
		return this.formatter;
	}


	public void setFormatter(DateTimeFormatter formatter) {
		this.formatter = formatter;
	}
}
