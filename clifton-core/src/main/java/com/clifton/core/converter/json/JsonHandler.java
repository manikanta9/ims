package com.clifton.core.converter.json;

import java.io.InputStream;
import java.util.Collection;


/**
 * @author StevenF
 * @author michaelm
 * <p>
 * This inteface provides the wrapper implementation for any JSON serialization/deserialization libraries we may support.
 * There should only every be one implementation of this handler in the context.
 * The way de/serialization is handled is configured by the JsonStrategy object and is the resultant objectMapper
 * configuration is cached by strategyName.
 */
public interface JsonHandler<T extends JsonStrategy> {

	public String toJson(Object o);


	public String toJson(Object o, T strategy);


	public Object fromJson(String json, Class<?> clazz);


	/**
	 * This method exists so that the underlying implementation can correctly handle object references when deserializing lists of entities.
	 * This should be deleted after PortfolioTargetRunSerializationUpdateService is removed.
	 */
	public Object fromJson(String json, Class<? extends Collection> collectionClass, Class<?> clazz);


	public Object fromJson(InputStream json, Class<?> clazz);


	public Object fromJson(String json, Class<?> clazz, T strategy);


	public Object fromJson(InputStream inputStream, Class<?> clazz, T strategy);


	/**
	 * This method is useful when a JSON string has already been parsed, but has not been instantiated with the desired {@param clazz} yet.
	 * For example, when {@link com.clifton.core.json.custom.CustomJsonString}s are retrieved from the database, the {@link com.clifton.core.json.custom.CustomJsonString#jsonValue}
	 * is parsed to create the {@link com.clifton.core.json.custom.CustomJsonString#jsonValueMap}. Since class information is not saved in the json, any object within the json is
	 * represented as a generic {@link java.util.Map} of key/value pairs. That map can be passed into this method along with the desired class to create an instantiated object.
	 * It is important to use this JsonHandler method instead of our bean utilities to instantiate the object using properties because json often contains identifiers that
	 * reference other objects (e.g. @JsonIdentity) and these identifiers are not support by our bean utilities.
	 */
	public Object convertValue(Object o, Class<?> clazz);
}
