package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.dataaccess.PagingList;

import java.io.IOException;


/**
 * The <code>JsonDataTableValueProcessor</code> class converts objects that implement {@link DataTable}
 * interface into JSON format strings.
 * <p/>
 * Example:
 * <pre>
 * dataTable:{firstIndex:0,totalRows:3,rows:[[1,"apple","2009-01-01 10:00:00"],[2,"orange","2009-01-01 10:00:00"],[3,"peach","2009-01-01 10:00:00"]]}
 * </pre>
 *
 * @author vgomelsky
 */
public class DataTableValueAppender implements JsonValueAppender {

	@Override
	public void append(Appendable resultAppender, @SuppressWarnings("unused") String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("{}");
		}
		else {
			DataTable dataTable = (DataTable) value;
			int colCount = dataTable.getColumnCount();
			int rowCount = (dataTable instanceof PagingList) ? ((PagingList) dataTable).getCurrentPageElementCount() : dataTable.getTotalRowCount();
			resultAppender.append("{\"firstIndex\":");
			resultAppender.append(String.valueOf(dataTable instanceof PagingList ? ((PagingList) dataTable).getFirstElementIndex() : 0));
			resultAppender.append(",\"totalRows\":");
			resultAppender.append(String.valueOf(dataTable.getTotalRowCount()));
			resultAppender.append(",\"columns\":[");
			DataColumn[] columnList = dataTable.getColumnList();
			for (int c = 0; c < columnList.length; c++) {
				if (c != 0) {
					resultAppender.append(',');
				}
				String columnName = columnList[c].getColumnName();
				config.getAppender(columnName).append(resultAppender, null, columnName, config, session);
			}
			resultAppender.append("]");
			resultAppender.append(",\"rows\":[");
			for (int r = 0; r < rowCount; r++) {
				DataRow row = dataTable.getRow(r);
				if (r != 0) {
					resultAppender.append(",[");
				}
				else {
					resultAppender.append('[');
				}
				for (int c = 0; c < colCount; c++) {
					if (c != 0) {
						resultAppender.append(',');
					}
					Object v = row.getValue(c);
					config.getAppender(v).append(resultAppender, null, v, config, session);
				}
				resultAppender.append(']');
			}
			resultAppender.append("]}");
		}
	}
}
