package com.clifton.core.converter.template;


import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;


/**
 * The <code>TemplateBuilder</code> interface defines some standard methods for converting bean or {@link DataTable} results
 * to a template
 *
 * @author manderson
 */
public interface TemplateBuilder {

	/**
	 * Returns Template for the given {@link DataTable}
	 * <p/>
	 * Template will be in the form of an html table with the column names as the first
	 * row.
	 *
	 * @param dataTable
	 * @param dataTableEntityName
	 */
	public String buildDataTableTemplate(DataTable dataTable, String dataTableEntityName);


	/**
	 * Returns Template for the given {@link DataRow}
	 * <p/>
	 * Template will be in the form of a list of column names & property values for the given row
	 * <p/>
	 * i.e.
	 * Name: My name
	 * Amount: 500
	 *
	 * @param dataTable
	 * @param dataRowEntityName
	 */
	public String buildDataRowTemplate(DataTable dataTable, String dataRowEntityName);


	/**
	 * Returns Template for given {@link DAOConfiguration} config property.
	 * <p/>
	 * Template will contain all bean properties for the config and will be in the
	 * form of a list of column names & property values for the given row
	 * <p/>
	 * i.e.
	 * Name: My name
	 * Amount: 500
	 *
	 * @param config
	 * @param entityName
	 */
	public String buildDTOObjectTemplate(DAOConfiguration<?> config, String entityName);
}
