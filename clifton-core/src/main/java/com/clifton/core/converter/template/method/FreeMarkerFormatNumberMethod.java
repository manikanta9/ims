package com.clifton.core.converter.template.method;

import com.clifton.core.util.math.CoreMathUtils;
import freemarker.template.SimpleNumber;
import freemarker.template.TemplateMethodModelEx;
import freemarker.template.TemplateModelException;

import java.math.BigDecimal;
import java.util.List;


/**
 * @author stevenf
 * This class is an extension of the freemarker template functionality.
 * It allows us to add custom formatting to our violation messages that will be cleared for trading.
 * Additionally, positive values will be wrapped with a 'green' style, and negative values with a 'red' style.
 * This class is extended by others such as ${formatNumberMoney(value)} or ${formatNumberPercent(value)} to express as a percent.
 * The methods can take an optional second paramater, e.g. ${formatNumberMoney(value, "#,##0.0#")} in which case the override format will be
 * used with the CoreMathUtils.formatNumber(value, format) method rather than the overriden 'formatNumber' method of the implementing class.
 */
public abstract class FreeMarkerFormatNumberMethod implements TemplateMethodModelEx {

	@Override
	@SuppressWarnings("rawtypes")
	public Object exec(List args) throws TemplateModelException {
		if (args.isEmpty() || args.size() > 2) {
			throw new TemplateModelException("Wrong number of arguments");
		}
		BigDecimal value = BigDecimal.valueOf(((SimpleNumber) args.get(0)).getAsNumber().doubleValue());
		String displayValue;
		if (args.size() == 2) {
			displayValue = CoreMathUtils.formatNumber(value, String.valueOf(args.get(1)));
		}
		else {
			displayValue = formatNumber(value);
		}
		return formatDisplayValue(value, displayValue);
	}


	protected abstract String formatNumber(BigDecimal value);


	private String formatDisplayValue(BigDecimal value, String displayValue) {
		String className = "amountPositive";
		if (value.compareTo(BigDecimal.ZERO) < 0) {
			className = "amountNegative";
		}
		return "<span class='" + className + "'>" + displayValue + "</span>";
	}
}
