package com.clifton.core.converter.json.jackson.serializers;

import com.clifton.core.util.date.Time;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;


/**
 * The JacksonTimeSerializer  class converts a Time object to its corresponding String representation.
 * It uses date format specified in the constructor.
 * Defaults to {@link Time#TIME_FORMAT_SHORT}.
 *
 * @author vgomelsky
 */
public class JacksonTimeSerializer extends JsonSerializer<Time> {

	private String timeFormat;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JacksonTimeSerializer() {
		this(Time.TIME_FORMAT_SHORT);
	}


	public JacksonTimeSerializer(String timeFormat) {
		this.timeFormat = timeFormat;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void serialize(Time time, JsonGenerator generator, SerializerProvider provider) throws IOException {
		generator.writeString(time == null ? null : time.toString(this.timeFormat));
	}
}
