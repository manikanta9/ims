package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.util.date.Time;

import java.io.IOException;


/**
 * The <code>DateValueAppender</code> class allows converting Date objects to a predefined format.
 *
 * @author vgomelsky
 */
public class TimeValueAppender implements JsonValueAppender {

	private String format = Time.TIME_FORMAT_SHORT;


	@Override
	@SuppressWarnings("unused")
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else {
			Time dateValue = (Time) value;
			resultAppender.append('\"');
			resultAppender.append(dateValue.toString(getFormat()));
			resultAppender.append('\"');
		}
	}


	public String getFormat() {
		return this.format;
	}


	public void setFormat(String format) {
		this.format = format;
	}
}
