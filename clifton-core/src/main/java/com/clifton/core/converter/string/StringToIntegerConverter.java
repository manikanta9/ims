package com.clifton.core.converter.string;


import com.clifton.core.util.converter.Converter;


/**
 * The <code>StringToIntegerConverter</code> class converts String to corresponding Integer objects.
 * It throws {@link NumberFormatException} if conversion cannot be performed.
 *
 * @author vgomelsky
 */
public class StringToIntegerConverter implements Converter<String, Integer> {

	@Override
	public Integer convert(String from) {
		return Integer.parseInt(from);
	}
}
