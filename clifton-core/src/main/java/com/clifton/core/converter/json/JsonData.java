package com.clifton.core.converter.json;

/**
 * This class is a wrapper class for JSON serialization to the UI.
 * The jackson implementation checks for a JsonRootName annotation on the object
 * if none exists then it wraps the object in this class so the root name is 'data'
 *
 * @author StevenF on 10/4/2016.
 */
public class JsonData {

	private Object data;
	private boolean success;


	public JsonData(Object o) {
		this.data = o;
		this.success = true;
	}


	public JsonData(Object o, boolean success) {
		this.data = o;
		this.success = success;
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                 Getter and Setter Methods                ///////////
	////////////////////////////////////////////////////////////////////////////////


	public Object getData() {
		return this.data;
	}


	public boolean isSuccess() {
		return this.success;
	}
}
