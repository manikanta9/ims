package com.clifton.core.converter.json.jackson.serializers;

import com.clifton.core.util.date.Time;
import com.clifton.core.util.date.TimeUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;


/**
 * The JacksonTimeDeserializer class converts String representation of a time to the our Time object.
 *
 * @author vgomelsky
 */
public class JacksonTimeDeserializer extends JsonDeserializer<Time> {

	@Override
	public Time deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		return TimeUtils.toTime(parser.getText());
	}
}
