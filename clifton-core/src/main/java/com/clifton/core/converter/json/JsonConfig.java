package com.clifton.core.converter.json;


import com.clifton.core.converter.json.appenders.ObjectValueAppender;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>JsonConfig</code> class contains information about how object serialization should be performed.
 *
 * @author vgomelsky
 */
public class JsonConfig {

	private final Map<Class<?>, JsonValueAppender> appenderMap = new ConcurrentHashMap<>();
	private boolean skipNullFields;
	private JsonValueAppender defaultAppender = new ObjectValueAppender();


	/**
	 * Registers the specified {@link JsonValueAppender} to be used for serialization of
	 * instances of the specified class.
	 */
	public void registerAppender(Class<?> clazz, JsonValueAppender appender) {
		this.appenderMap.put(clazz, appender);
	}


	/**
	 * Returns {@link JsonValueAppender} that's registered for the class of the specified value.
	 * Returns the defaultAppender if one is not registered.
	 */
	public JsonValueAppender getAppender(Object value) {
		if (value != null) {
			Class<?> valueClass = value.getClass();
			// look for a direct match first
			JsonValueAppender appender = this.appenderMap.get(valueClass);
			if (appender != null) {
				return appender;
			}
			// then look for child classes
			for (Map.Entry<Class<?>, JsonValueAppender> clazzAppenderEntry : this.appenderMap.entrySet()) {
				if (clazzAppenderEntry.getKey().isAssignableFrom(valueClass)) {
					JsonValueAppender result = clazzAppenderEntry.getValue();
					registerAppender(valueClass, result); // register to speed up future calls
					return result;
				}
			}
			registerAppender(valueClass, this.defaultAppender); // register to speed up future calls
		}
		return this.defaultAppender;
	}


	/**
	 * Returns true if null value fields should be omitted from serialization.
	 */
	public boolean isSkipNullFields() {
		return this.skipNullFields;
	}


	public void setSkipNullFields(boolean skipNullFields) {
		this.skipNullFields = skipNullFields;
	}


	public JsonValueAppender getDefaultAppender() {
		return this.defaultAppender;
	}


	public void setDefaultAppender(JsonValueAppender defaultAppender) {
		this.defaultAppender = defaultAppender;
	}
}
