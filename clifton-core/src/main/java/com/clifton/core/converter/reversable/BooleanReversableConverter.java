package com.clifton.core.converter.reversable;


import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;

import java.util.ArrayList;
import java.util.List;


/**
 * The <code>BooleanReversableConverter</code> class is a ReversableConverter for Boolean data type.
 * It converts String variations for true and false to corresponding boolean value.
 * <p/>
 * For example, "B" or "Buy" for a Trade can be true while "S" or "Sell" can be false.
 *
 * @author manderson
 */
public class BooleanReversableConverter implements ReversableConverter<String, Boolean> {

	private List<String> trueValueList;
	private List<String> falseValueList;

	private String defaultTrueValue = "TRUE";
	private String defaultFalseValue = "FALSE";

	/**
	 * Instead of a specific list of false values, can always return false if true values don't match.
	 */
	private boolean falseIfValueUnmatched;


	@Override
	public Boolean convert(String from) {
		if (isValueInList(from, getValidTrueValues())) {
			return true;
		}
		if (isFalseIfValueUnmatched() || isValueInList(from, getValidFalseValues())) {
			return false;
		}
		throw new ValidationException("Unable to convert " + from + " to a boolean value because it is not set up as a true or false value.  Valid True Values ["
				+ StringUtils.collectionToCommaDelimitedString(getValidTrueValues()) + "], Valid False Values [" + StringUtils.collectionToCommaDelimitedString(getValidFalseValues()) + "].");
	}


	@Override
	public String reverseConvert(Boolean to) {
		if (to == null) {
			return null;
		}
		if (to) {
			return getDefaultTrueValue();
		}
		return getDefaultFalseValue();
	}


	private boolean isValueInList(String value, List<String> list) {
		for (String v : CollectionUtils.getIterable(list)) {
			if (v.equalsIgnoreCase(value)) {
				return true;
			}
		}
		return false;
	}


	private List<String> getValidTrueValues() {
		List<String> trueValues = getTrueValueList();
		if (getDefaultTrueValue() != null && !isValueInList(getDefaultTrueValue(), trueValues)) {
			if (trueValues == null) {
				trueValues = new ArrayList<>();
			}
			trueValues.add(getDefaultTrueValue());
		}
		return trueValues;
	}


	private List<String> getValidFalseValues() {
		List<String> falseValues = getFalseValueList();
		if (getDefaultFalseValue() != null && !isValueInList(getDefaultFalseValue(), falseValues)) {
			if (falseValues == null) {
				falseValues = new ArrayList<>();
			}
			falseValues.add(getDefaultFalseValue());
		}
		return falseValues;
	}


	public List<String> getTrueValueList() {
		return this.trueValueList;
	}


	public void setTrueValueList(List<String> trueValueList) {
		this.trueValueList = trueValueList;
	}


	public List<String> getFalseValueList() {
		return this.falseValueList;
	}


	public void setFalseValueList(List<String> falseValueList) {
		this.falseValueList = falseValueList;
	}


	public String getDefaultTrueValue() {
		return this.defaultTrueValue;
	}


	public void setDefaultTrueValue(String defaultTrueValue) {
		this.defaultTrueValue = defaultTrueValue;
	}


	public String getDefaultFalseValue() {
		return this.defaultFalseValue;
	}


	public void setDefaultFalseValue(String defaultFalseValue) {
		this.defaultFalseValue = defaultFalseValue;
	}


	public boolean isFalseIfValueUnmatched() {
		return this.falseIfValueUnmatched;
	}


	public void setFalseIfValueUnmatched(boolean falseIfValueUnmatched) {
		this.falseIfValueUnmatched = falseIfValueUnmatched;
	}
}
