package com.clifton.core.converter.json.jackson.serializers;

import com.clifton.core.util.dataaccess.PagingArrayList;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;


/**
 * Because the PagingArrayList extends ArrayList it requires a custom serializer and deserializer
 * in order to properly include the additional properties used for paging; default serialization
 * simply generates the json array and does not include start, total, or pagesize;
 * <p>
 * During serialization we also include the concrete className for any generic params so it is
 * available during deserialization.
 *
 * @author StevenF
 */
@SuppressWarnings("rawtypes")
public class JacksonPagingArrayListSerializer extends JsonSerializer<PagingArrayList> {

	@Override
	public void serialize(PagingArrayList list, JsonGenerator generator, SerializerProvider provider) throws IOException {
		if (list != null) {
			generator.writeStartObject();

			//TODO - make more efficient (See JacksonPagingArrayListDeserializer):
			//We manually write each row, because if we use writeObject to write the array as one
			//then references to the same object will be replaced via the jsonIdentityInfo functionality
			//normally this is fine and better for performance (less json data) however, since when we
			//deserialize this object we do so row by row (node by node), and I don't know a way around that.
			//Because of that, if we write as one object and read as multiple, then when we run into the
			//jsonIdentityInfo, the reference doesn't make sense at a single node level, so properties can
			//be skipped (essentially deserialized to null); so, processing row by row ensures data fidelity.
			//If we can fix deserialization, then we could serialize by simply doing the following:
//			generator.writeObjectField("rows", list);

			generator.writeFieldName("rows");
			generator.writeStartArray();
			for (Object o : list) {
				generator.writeObject(o);
			}
			generator.writeEndArray();

			generator.writeNumberField("start", list.getFirstElementIndex());
			generator.writeNumberField("total", list.getTotalElementCount());
			generator.writeNumberField("pageSize", list.getPageSize());
			if (!list.isEmpty()) {
				generator.writeStringField("listElementClassName", list.get(0).getClass().getName());
			}
			generator.writeEndObject();
		}
	}
}
