package com.clifton.core.converter.json.jackson.serializers;

import com.clifton.core.util.dataaccess.PagingArrayList;
import com.clifton.core.util.CoreClassUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;


/**
 * Because the PagingArrayList extends ArrayList it requires a custom serializer and deserializer
 * in order to properly include the additional properties used for paging; default serialization
 * simply generates the json array and does not include start, total, or pagesize;
 *
 * @author StevenF
 */
@SuppressWarnings("unchecked,rawtypes")
public class JacksonPagingArrayListDeserializer extends JsonDeserializer<PagingArrayList<Object>> {

	@Override
	public PagingArrayList deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		JsonNode node = parser.getCodec().readTree(parser);
		int start = node.get("start").asInt();
		int total = node.get("total").asInt();
		int pageSize = node.get("pageSize").asInt();

		final List<Object> rows = new ArrayList<>();
		JsonNode rowNodes = node.get("rows");
		if (rowNodes.isArray() && rowNodes.size() > 0) {
			//TODO - make more efficient (See JacksonPagingArrayListSerializer):
			//If rows can be deserialized in one call, rather than row by row
			//then we can update the serializer to write the array as a single call
			//and it would reduce the json written significantly for lists that share
			//similar objects (e.g. same PortalFileCategory for each row)
			Class<?> listElementClass = CoreClassUtils.getClass(node.get("listElementClassName").asText());
			for (JsonNode rowNode : node.get("rows")) {
				rows.add(parser.getCodec().treeToValue(rowNode, listElementClass));
			}
		}
		return new PagingArrayList<>(rows, start, total, pageSize);
	}
}
