package com.clifton.core.converter.string;


import com.clifton.core.util.converter.Converter;


/**
 * The <code>StringToIntArrayConverter</code> class converts String to corresponding int[] objects.
 * Valid strings are comma delimited lists of integers: "1", "1,2,3", "111,222".
 * It throws {@link NumberFormatException} if conversion cannot be performed.
 *
 * @author vgomelsky
 */
public class StringToIntArrayConverter implements Converter<String, int[]> {

	@Override
	public int[] convert(String from) {
		if (from == null) {
			return null;
		}
		String[] fromElements = from.split(",");
		int[] result = new int[fromElements.length];
		for (int i = 0; i < fromElements.length; i++) {
			result[i] = Integer.parseInt(fromElements[i]);
		}
		return result;
	}
}
