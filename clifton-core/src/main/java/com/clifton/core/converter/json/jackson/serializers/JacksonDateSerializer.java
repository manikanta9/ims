package com.clifton.core.converter.json.jackson.serializers;

import com.clifton.core.util.date.DateUtils;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.util.Date;


/**
 * The JacksonDateSerializer class converts a Date object to its corresponding String representation.
 * It uses date format specified in the constructor.
 * Defaults to {@link DateUtils#DATE_FORMAT_SQL_PRECISE}, most precise format that includes milliseconds.
 *
 * @author StevenF
 */
public class JacksonDateSerializer extends JsonSerializer<Date> {

	private String dateFormat;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JacksonDateSerializer() {
		this(DateUtils.DATE_FORMAT_SQL_PRECISE);
	}


	public JacksonDateSerializer(String dateFormat) {
		this.dateFormat = dateFormat;
	}


	@Override
	public void serialize(Date date, JsonGenerator generator, SerializerProvider provider) throws IOException {
		generator.writeString(DateUtils.fromDate(date, this.dateFormat));
	}
}
