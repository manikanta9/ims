package com.clifton.core.converter.template.method;

import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;


/**
 * @author stevenf
 * This class is an extension of the freemarker template functionality.
 * It provides default formatting for integer values.
 */
public class FreeMarkerFormatNumberIntegerMethod extends FreeMarkerFormatNumberMethod {

	@Override
	public String formatNumber(BigDecimal value) {
		return CoreMathUtils.formatNumberInteger(value);
	}
}
