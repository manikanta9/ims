package com.clifton.core.converter.json.appenders;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.ClassUtils;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;


/**
 * The ObjectUsingFieldsValueAppender class uses bean field values vs getters to describe the bean.
 * This maybe necessary for complex objects (Service, DAO, etc.) that have "get" methods that actually implement business logic.
 *
 * @author vgomelsky
 */
public class ObjectUsingFieldsValueAppender extends ObjectValueAppender {

	/**
	 * Optionally excludes all keys that start with the following prefixes.
	 */
	private String[] excludeKeyPrefixes;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public ObjectUsingFieldsValueAppender() {
		super();
	}


	public ObjectUsingFieldsValueAppender(String... excludeKeyPrefixes) {
		super();
		this.excludeKeyPrefixes = excludeKeyPrefixes;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	protected Map<String, Object> describeObject(Object bean) {
		Map<String, Object> result = new HashMap<>();
		// Use fields vs getters that may not be real getters but business logic implementation
		Field[] fields = ClassUtils.getClassFields(bean.getClass(), true, true);
		for (Field field : fields) {
			Object value = BeanUtils.getFieldValue(bean, field);
			if (value != null) {
				result.put(field.getName(), value);
			}
		}

		if (isIncludeClassProperty() && !result.containsKey("class")) {
			result.put("class", bean.getClass().getName());
		}

		return result;
	}


	@Override
	protected void removeProperties(Map<String, Object> beanMap) {
		super.removeProperties(beanMap);

		if (this.excludeKeyPrefixes != null) {
			beanMap.keySet().removeIf(key -> {
				for (String keyPrefix : this.excludeKeyPrefixes) {
					if (key.startsWith(keyPrefix)) {
						return true;
					}
				}
				return false;
			});
		}
	}
}
