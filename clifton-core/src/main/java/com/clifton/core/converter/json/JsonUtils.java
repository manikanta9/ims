package com.clifton.core.converter.json;

/**
 * @author vgomelsky
 */
public class JsonUtils {


	/**
	 * Sanitizes the given string as a JSON string value according to the syntax rules provided at http://www.json.org.
	 *
	 * @param value the value to be sanitized
	 * @return the JSON-sanitized value, which is suitable to exist within JSON string values
	 * @implNote This method takes measures to optimize sanitization performance. These measures include avoiding memory writes. Although this lends to
	 * lengthier code, it also reduces cost. Benchmarks show this method taking as little as 15% of the time taken by other methods, such as using regular
	 * expressions for each character sequence.
	 */
	public static String sanitizeJsonStringValue(String value) {
		// Get new string length
		boolean needsTransformation = false;
		int newStringLength = value.length();
		for (int i = 0; i < value.length(); i++) {
			switch (value.charAt(i)) {
				case '\\':
				case '\b':
				case '\f':
				case '\"':
				case '/': // Utility of escaping forward-slash is to avoid illegal "</" pattern within <script> tags
				case '\t':
					// Add escape before special characters
					needsTransformation = true;
					newStringLength++;
					break;
				case '\r':
					// Transform \r or \r\n to \n
					needsTransformation = true;
					if (i + 1 < value.length() && value.charAt(i + 1) == '\n') {
						i++;
					}
					else {
						newStringLength++;
					}
					break;
				case '\n':
					// Transform \n\r to \n
					needsTransformation = true;
					if (i + 1 < value.length() && value.charAt(i + 1) == '\r') {
						i++;
					}
					else {
						newStringLength++;
					}
					break;
				default:
					// Retain original character
					break;
			}
		}

		// Perform transformation if necessary
		String transformedValue = value;
		if (needsTransformation) {
			char[] transformedCharArray = new char[newStringLength];
			int transformIndex = 0;
			for (int i = 0; i < value.length(); i++) {
				char curChar = value.charAt(i);
				switch (curChar) {
					// Add escape before special characters
					case '\\':
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = '\\';
						break;
					case '\b':
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = 'b';
						break;
					case '\f':
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = 'f';
						break;
					case '\"':
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = '"';
						break;
					case '/':
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = '/';
						break;
					case '\t':
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = 't';
						break;
					case '\r':
						// Transform \r or \r\n to \n
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = 'n';
						if (i + 1 < value.length() && value.charAt(i + 1) == '\n') {
							i++;
						}
						break;
					case '\n':
						// Transform \n\r to \n
						transformedCharArray[transformIndex++] = '\\';
						transformedCharArray[transformIndex++] = 'n';
						if (i + 1 < value.length() && value.charAt(i + 1) == '\r') {
							i++;
						}
						break;
					default:
						// Retain original character
						transformedCharArray[transformIndex++] = curChar;
						break;
				}
			}
			transformedValue = String.valueOf(transformedCharArray);
		}
		return transformedValue;
	}
}
