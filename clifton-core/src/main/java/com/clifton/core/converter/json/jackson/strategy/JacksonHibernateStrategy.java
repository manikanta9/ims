package com.clifton.core.converter.json.jackson.strategy;

import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonTimeDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonTimeSerializer;
import com.clifton.core.util.date.Time;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.math.BigDecimal;
import java.util.Date;


/**
 * Provides default serialization strategy for Jackson, allows for specifying custom filters, de/serializers.
 *
 * @author StevenF on 10/4/2016.
 */
public class JacksonHibernateStrategy extends JacksonStrategy {

	@Override
	public void registerModules(ObjectMapper objectMapper) {
		SimpleModule module = new SimpleModule();

		module.addSerializer(BigDecimal.class, new JacksonBigDecimalSerializer());
		module.addSerializer(Date.class, new JacksonDateSerializer());
		module.addSerializer(Time.class, new JacksonTimeSerializer(Time.TIME_FORMAT_SHORT));

		module.addDeserializer(BigDecimal.class, new JacksonBigDecimalDeserializer());
		module.addDeserializer(Date.class, new JacksonDateDeserializer());
		module.addDeserializer(Time.class, new JacksonTimeDeserializer());

		objectMapper.registerModule(module);
	}
}
