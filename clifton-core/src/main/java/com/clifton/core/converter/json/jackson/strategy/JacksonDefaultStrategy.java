package com.clifton.core.converter.json.jackson.strategy;

import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonBigDecimalSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonDateSerializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonTimeDeserializer;
import com.clifton.core.converter.json.jackson.serializers.JacksonTimeSerializer;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The JacksonDefaultStrategy class uses our default serializers for various data types.
 * Defaults date format to the one that we use in UI (no milliseconds: less precise).
 *
 * @author vgomelsky
 */
public class JacksonDefaultStrategy extends JacksonStrategy {

	@Override
	public void registerModules(ObjectMapper objectMapper) {
		SimpleModule module = new SimpleModule();

		module.addSerializer(BigDecimal.class, new JacksonBigDecimalSerializer());
		module.addSerializer(Date.class, new JacksonDateSerializer(DateUtils.DATE_FORMAT_FULL));
		module.addSerializer(Time.class, new JacksonTimeSerializer(Time.TIME_FORMAT_SHORT));

		module.addDeserializer(BigDecimal.class, new JacksonBigDecimalDeserializer());
		module.addDeserializer(Date.class, new JacksonDateDeserializer(DateUtils.DATE_FORMAT_FULL));
		module.addDeserializer(Time.class, new JacksonTimeDeserializer());

		objectMapper.registerModule(module);
	}
}
