package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonUtils;
import com.clifton.core.converter.json.JsonValueAppender;

import java.io.IOException;


/**
 * The <code>StringValueAppender</code> class implements String serialization to JSON format.
 * It escapes quotes and new line characters and also adds quotes around the value.
 *
 * @author vgomelsky
 */
public class StringValueAppender implements JsonValueAppender {

	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else {
			resultAppender.append('\"');
			resultAppender.append(JsonUtils.sanitizeJsonStringValue((String) value));
			resultAppender.append('\"');
		}
	}
}
