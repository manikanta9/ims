package com.clifton.core.converter.json;


import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringJoiner;
import java.util.function.Consumer;


/**
 * The <code>MapWithRules</code> class defines a map of object name to object pairs that can be serialized
 * as well as the filtering rules for serialization.
 *
 * @author vgomelsky
 */
public class MapWithRules {

	public static final int DEFAULT_MAX_DEPTH = 50;

	////////////////////////////////////////////////////////////////////////////

	/**
	 * The Map to be serialized.
	 */
	private final Map<String, Object> map;
	/**
	 * The conversion map for converting source objects into target objects for serialization. This is similar to registering a custom serializer, but is differentiated by its
	 * applicability to only the objects provided.
	 *
	 * @see JsonSession#entityConversionMap
	 */
	private final Map<Object, Object> entityConversionMap;
	/**
	 * Optional starting path for the beans that should be filtered.
	 * Example: "data.rows" (there's "data" element in the map that has an object with "rows" property; the values of it will be filtered using propertiesToFilter)
	 */
	private final String filterStartPath;
	/**
	 * A Map of properties to serialize starting with the filterStartPath. All other bean properties will be ignored.
	 * Keys are property names. If there are not nested properties, then values are null, otherwise values are the maps of nested properties, etc.
	 */
	private final Map<String, Map<String, ?>> propertiesToFilter;

	/**
	 * A Map of properties to exclude from serialization starting with the filterStartPath. All other bean properties will be ignored.
	 * Keys are property names. If there are not nested properties, then values are null, otherwise values are the maps of nested properties, etc.
	 */
	private final List<String> propertiesToExcludeFilter;
	private final List<String> propertiesToExcludeGloballyFilter;
	/**
	 * The maximum depth of nested beans to process.  If 0 or negative, use default;
	 */
	private final int maxAllowedDepth;
	/**
	 * When maxAllowedDepth is exceeded, skip remaining nodes or throw an exception?
	 */
	private final boolean skipExtraDepth;

	private Consumer<JsonConfig> jsonConfigInitializer;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Constructs a new Map with rules object. Properties to filter is a | delimited list of property names.
	 * Example: "id|name|type.label"
	 */
	public MapWithRules(Map<String, Object> map, String filterStartPath, String propertiesToFilter) {
		this(map, null, filterStartPath, propertiesToFilter, null, null, DEFAULT_MAX_DEPTH, false);
	}


	/**
	 * Constructs a new Map with rules object. Properties to filter is a | delimited list of property names.
	 * Example: "id|name|type.label"
	 */
	public MapWithRules(Map<String, Object> map, String filterStartPath, String propertiesToFilter, String propertiesToExcludeFilter) {
		this(map, null, filterStartPath, propertiesToFilter, propertiesToExcludeFilter, null, DEFAULT_MAX_DEPTH, false);
	}


	public MapWithRules(Map<String, Object> map, int maxAllowedDepth, boolean skipExtraDepth) {
		this(map, null, null, null, null, null, maxAllowedDepth, skipExtraDepth);
	}


	/**
	 * Constructs a new Map with rules object. Properties to filter is a | delimited list of property names.
	 * Example: "id|name|type.label"
	 */
	public MapWithRules(Map<String, Object> map, Map<Object, Object> entityConversionMap, String filterStartPath, String propertiesToFilter, String propertiesToExcludeFilter, String propertiesToExcludeGloballyFilter, int maxAllowedDepth, boolean skipExtraDepth) {
		if (maxAllowedDepth < 1) {
			throw new IllegalArgumentException("maxAllowedDepth of " + maxAllowedDepth + " cannot be less than 1");
		}

		this.map = map;
		this.entityConversionMap = entityConversionMap;
		this.filterStartPath = filterStartPath;
		this.maxAllowedDepth = maxAllowedDepth;
		this.skipExtraDepth = skipExtraDepth;
		this.propertiesToFilter = propertiesToFilter != null ? readFilter(propertiesToFilter) : null;
		this.propertiesToExcludeFilter = propertiesToExcludeFilter != null ? Arrays.asList(propertiesToExcludeFilter.split("\\|")) : null;
		this.propertiesToExcludeGloballyFilter = propertiesToExcludeGloballyFilter != null ? Arrays.asList(propertiesToExcludeGloballyFilter.split("\\|")) : null;
	}


	@Override
	public String toString() {
		StringJoiner result = new StringJoiner("; ", "{", "}");
		try {
			result.add("filterStartPath=" + this.filterStartPath);
			result.add("maxAllowedDepth=" + this.maxAllowedDepth);
			result.add("skipExtraDepth=" + this.skipExtraDepth);
			if (this.propertiesToFilter != null) {
				result.add("propertiesToFilter=" + this.propertiesToFilter);
			}
			if (this.propertiesToExcludeFilter != null) {
				result.add("propertiesToExcludeFilter=" + this.propertiesToExcludeFilter);
			}
			if (this.propertiesToExcludeGloballyFilter != null) {
				result.add("propertiesToExcludeGloballyFilter=" + this.propertiesToExcludeGloballyFilter);
			}
			if (this.map != null) {
				result.add("map=" + this.map);
			}
		}
		catch (Throwable e) {
			result.add("ERROR=" + e.getMessage());
		}
		return result.toString();
	}


	@SuppressWarnings({"unchecked"})
	private Map<String, Map<String, ?>> readFilter(String filter) {
		Map<String, Map<String, ?>> result = new HashMap<>();
		String[] propertyNames = filter.split("\\|");
		for (String property : propertyNames) {
			String[] nestedProperties = property.split("\\.");
			if (nestedProperties.length == 1) {
				result.put(property, null);
			}
			else {
				Map<String, Map<String, ?>> currentMap = result;
				int index = 1;
				for (String nestedProperty : nestedProperties) {
					Map<String, Map<String, ?>> nestedMap = (Map<String, Map<String, ?>>) currentMap.get(nestedProperty);
					if (nestedMap == null) {
						if (index < nestedProperties.length) {
							nestedMap = new HashMap<>();
						}
						currentMap.put(nestedProperty, nestedMap);
					}
					currentMap = nestedMap;
					index++;
				}
			}
		}
		return result;
	}


	public MapWithRules withJsonConfigInitializer(Consumer<JsonConfig> jsonConfigInitializer) {
		this.setJsonConfigInitializer(jsonConfigInitializer);
		return this;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Map<String, Object> getMap() {
		return this.map;
	}


	public Map<Object, Object> getEntityConversionMap() {
		return this.entityConversionMap;
	}


	public String getFilterStartPath() {
		return this.filterStartPath;
	}


	public Map<String, Map<String, ?>> getPropertiesToFilter() {
		return this.propertiesToFilter;
	}


	public List<String> getPropertiesToExcludeFilter() {
		return this.propertiesToExcludeFilter;
	}


	public List<String> getPropertiesToExcludeGloballyFilter() {
		return this.propertiesToExcludeGloballyFilter;
	}


	public int getMaxAllowedDepth() {
		return this.maxAllowedDepth;
	}


	public boolean isSkipExtraDepth() {
		return this.skipExtraDepth;
	}


	public Consumer<JsonConfig> getJsonConfigInitializer() {
		return this.jsonConfigInitializer;
	}


	public void setJsonConfigInitializer(Consumer<JsonConfig> jsonConfigInitializer) {
		this.jsonConfigInitializer = jsonConfigInitializer;
	}
}
