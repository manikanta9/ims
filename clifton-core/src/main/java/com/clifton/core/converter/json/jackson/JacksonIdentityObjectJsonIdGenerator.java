package com.clifton.core.converter.json.jackson;

import com.clifton.core.beans.IdentityObject;
import com.fasterxml.jackson.annotation.ObjectIdGenerator;

import java.io.Serializable;
import java.util.UUID;


/**
 * <code>BaseSimpleEntityJsonIdGenerator</code> represents an {@link ObjectIdGenerator}
 * that generates a unique string based on an object's type and ID (e.g. Trade-123456).
 * <p>
 * This ID gernerator will function similar to {@link com.fasterxml.jackson.annotation.ObjectIdGenerators.StringIdGenerator}
 * or {@link com.fasterxml.jackson.annotation.ObjectIdGenerators.UUIDGenerator}, but generates
 * an ID that better identifies IMS entities.
 *
 * @author NickK
 */
public class JacksonIdentityObjectJsonIdGenerator extends ObjectIdGenerator<String> {

	private final Class<?> scope;


	public JacksonIdentityObjectJsonIdGenerator(Class<?> scope) {
		this.scope = scope;
	}


	public JacksonIdentityObjectJsonIdGenerator() {
		this(Object.class);
	}


	/**
	 * The scope of {@link Object} is returned. All objects are handled by this
	 * generator to generate unique IDs.
	 */
	@Override
	public Class<?> getScope() {
		return this.scope;
	}


	/**
	 * This object does not differentiate on object types (scopes), so the same instance is returned.
	 */
	@Override
	public ObjectIdGenerator<String> forScope(Class<?> scope) {
		return this;
	}


	/**
	 * This object does not have state, so the same instance is returned.
	 */
	@Override
	public ObjectIdGenerator<String> newForSerialization(Object context) {
		return this;
	}


	/**
	 * Returns a String ID for the provided object. The ID will be a string compose of:
	 * <br/>- {@link IdentityObject} type with the ID of the object (e.g. TRADE-123456). If the object does not
	 * yet have an ID, the ID will be a UUID string to make it unique.
	 * <br/>- A UUID string value for objects that are not IdentityObjects.
	 */
	@Override
	public String generateId(Object object) {
		if (object instanceof IdentityObject) {
			Serializable id = ((IdentityObject) object).getIdentity();
			if (id == null) {
				// use UUID for null objects to avoid duplication.
				id = UUID.randomUUID().toString();
			}
			return new StringBuilder(object.getClass().getSimpleName()).append('-').append(id).toString();
		}
		return UUID.randomUUID().toString();
	}


	/**
	 * Returns a {@link IdKey} for the provided object key. The key should be a string and will be used for matching
	 * keys to objects during deserialization of JSON to objects. This implementation is developed to also support
	 * the previous UUID generated JSON IDs as well.
	 */
	@Override
	public IdKey key(Object key) {
		if (key == null) {
			return null;
		}
		return new IdKey(getClass(), null, key);
	}


	@Override
	public boolean canUseFor(ObjectIdGenerator<?> generator) {
		return (generator instanceof JacksonIdentityObjectJsonIdGenerator);
	}
}
