package com.clifton.core.converter.template;


import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.config.DAOConfiguration;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.StringUtils;

import java.math.BigDecimal;
import java.sql.Types;
import java.util.Date;


/**
 * The <code>FreemarkerTemplateBuilder</code> ...
 *
 * @author manderson
 */
public class FreemarkerTemplateBuilder implements TemplateBuilder {

	private DaoLocator daoLocator;

	private static final String DATATABLE_TABLE_STYLE = "width: 100%; text-align: left; font-size: 11px; font-family: arial; border-collapse: collapse; padding: 5px 4px 5px 6px; border-style: solid; border-width: 1px;  border-color: #d0d0d0; background-color: white;";
	private static final String DATATABLE_HEADER_TR_STYLE = "padding: 5px 4px 5px 6px; border: 1px solid #d0d0d0; border-left-color: #eee; background-color: #ededed;";
	private static final String TABLE_STYLE = "width: 100%; text-align: left; font-size: 11px; font-family: arial; padding: 5px 4px 5px 6px;";


	@Override
	public String buildDataTableTemplate(DataTable dataTable, String dataTableEntityName) {
		if (dataTable == null) {
			return "";
		}
		StringBuilder template = new StringBuilder("");
		appendTableStart(template, DATATABLE_TABLE_STYLE);
		appendTRStart(template, DATATABLE_HEADER_TR_STYLE);

		StringBuilder columns = new StringBuilder();
		DataColumn[] columnList = dataTable.getColumnList();
		DataRow firstRow = (dataTable.getTotalRowCount() > 0 ? dataTable.getRow(0) : null);

		for (int i = 0; i < columnList.length; i++) {
			DataColumnImpl column = (DataColumnImpl) columnList[i];
			if (isSkipColumnName(column.getColumnName())) {
				continue;
			}
			DataTypes dataType = null;
			Class<?> clz = getClassForSQLDataType(column.getDataType());
			if (clz.equals(BigDecimal.class)) {
				// See if we have an actual value and figure out precision for formatting
				// If scale <= 2, use Money format, else use Decimal default
				BigDecimal firstValue = (firstRow == null ? null : (BigDecimal) firstRow.getValue(column));
				if (firstValue != null && firstValue.scale() <= 2) {
					dataType = DataTypes.MONEY;
				}
			}
			appendTD(template, column.getColumnName(), getAlignmentForSQLDataType(column.getDataType()), 0);
			appendTD(columns, getBeanValueString(dataTableEntityName, "row[i].getValue(" + i + ")", column.getColumnName(), clz, dataType));
		}
		appendTREnd(template);
		template.append(StringUtils.NEW_LINE);
		template.append("<#if dataTable.totalRowCount == 0>");
		appendTRStart(template);
		appendTD(template, "None", "center", columnList.length);
		appendTREnd(template);
		template.append(StringUtils.NEW_LINE);
		template.append("<#else>");
		template.append(StringUtils.NEW_LINE);
		template.append("<#assign rowCount=").append(dataTableEntityName).append(".totalRowCount-1/><#assign columnCount=").append(dataTableEntityName).append(".columnCount-1/>");
		template.append(StringUtils.NEW_LINE);
		template.append("<#list 0..rowCount as i>");
		appendTRStart(template);
		template.append(columns);
		appendTREnd(template);
		template.append(StringUtils.NEW_LINE);
		template.append("</#list>");
		template.append(StringUtils.NEW_LINE);
		template.append("</#if>");
		appendTableEnd(template);
		return template.toString();
	}


	@Override
	public String buildDataRowTemplate(DataTable dataTable, String dataRowEntityName) {
		if (dataTable == null) {
			return "";
		}
		StringBuilder template = new StringBuilder("");
		DataColumn[] columnList = dataTable.getColumnList();
		DataRow firstRow = (dataTable.getTotalRowCount() > 0 ? dataTable.getRow(0) : null);

		appendTableStart(template, TABLE_STYLE);
		for (int i = 0; i < columnList.length; i++) {
			DataColumnImpl column = (DataColumnImpl) columnList[i];
			if (isSkipColumnName(column.getColumnName())) {
				continue;
			}
			DataTypes dataType = null;
			Class<?> clz = getClassForSQLDataType(column.getDataType());
			if (clz.equals(BigDecimal.class)) {
				// See if we have an actual value and figure out precision for formatting
				// If scale <= 2, use Money format, else use Decimal default
				BigDecimal firstValue = (firstRow == null ? null : (BigDecimal) firstRow.getValue(column));
				if (firstValue != null && firstValue.scale() <= 2) {
					dataType = DataTypes.MONEY;
				}
			}
			appendTRStart(template);
			appendTD(template, "<b>" + column.getColumnName() + "</b>");
			appendTD(template, getBeanValueString(dataRowEntityName, "getValue(" + i + ")", column.getColumnName(), clz, dataType));
			appendTREnd(template);
		}
		appendTableEnd(template);
		return template.toString();
	}


	@Override
	public String buildDTOObjectTemplate(DAOConfiguration<?> config, String entityName) {
		StringBuilder template = new StringBuilder("");
		appendTableStart(template, TABLE_STYLE);
		for (Column column : config.getColumnList()) {
			if (isSkipColumnName(column.getName())) {
				continue;
			}
			appendTRStart(template);
			appendTD(template, "<b>" + column.getName() + "</b>");

			if (column.getFkTable() != null) {
				appendTD(template, getBeanValueString(entityName, column.getBeanPropertyName(), column.getName(), getDaoLocator().locate(column.getFkTable()).getConfiguration().getBeanClass(), null));
			}
			else {
				appendTD(template, getBeanValueString(entityName, column.getBeanPropertyName(), column.getName(), getClassForDataType(column.getDataType()), column.getDataType()));
			}
			appendTREnd(template);
		}
		appendTableEnd(template);
		return template.toString();
	}


	private String getAlignmentForSQLDataType(int dataType) {
		Class<?> columnClass = getClassForSQLDataType(dataType);
		if (columnClass.isAssignableFrom(Number.class)) {
			return "right";
		}
		else if (columnClass.isAssignableFrom(Date.class)) {
			return "center";
		}
		return "";
	}


	private Class<?> getClassForSQLDataType(int dataType) {
		if (Types.BIT == dataType) {
			return boolean.class;
		}
		if (Types.INTEGER == dataType) {
			return int.class;
		}
		if (Types.DECIMAL == dataType) {
			return BigDecimal.class;
		}
		if (Types.DATE == dataType || Types.TIMESTAMP == dataType) {
			return Date.class;
		}
		return String.class;
	}


	private Class<?> getClassForDataType(DataTypes dataType) {
		if (dataType != null && dataType.getTypeName() != null) {
			DataTypeNames typeName = dataType.getTypeName();
			if (DataTypeNames.BOOLEAN == typeName) {
				return boolean.class;
			}
			if (DataTypeNames.INTEGER == typeName) {
				return int.class;
			}
			if (DataTypeNames.DATE == typeName) {
				return Date.class;
			}
			if (DataTypeNames.DECIMAL == typeName) {
				return BigDecimal.class;
			}
		}
		return String.class;
	}


	/**
	 * Allows for additional dataType property to help with default formatting for decimals (i.e. MONEY, PERCENT, ETC)
	 *
	 * @param entityName
	 * @param beanFieldName
	 * @param columnName
	 * @param beanFieldClassName
	 * @param dataType
	 */
	private String getBeanValueString(String entityName, String beanFieldName, String columnName, Class<?> beanFieldClassName, DataTypes dataType) {
		if (beanFieldClassName.isAssignableFrom(LabeledObject.class)) {
			return "${(" + entityName + "." + beanFieldName + ".label)!}";
		}
		else if (beanFieldClassName.equals(Integer.class) || beanFieldClassName.equals(int.class)) {
			return "${((" + entityName + "." + beanFieldName + ")?int)!}";
		}
		else if (beanFieldClassName.equals(Date.class)) {
			return "${((" + entityName + "." + beanFieldName + ")?date?string(\"MM/dd/yyyy\"))!}";
		}
		else if (beanFieldClassName.equals(Boolean.class) || beanFieldClassName.equals(boolean.class)) {
			return "<#if " + entityName + "." + beanFieldName + ">Yes<#else>No</#if>";
		}
		else if (beanFieldClassName.equals(BigDecimal.class)) {
			// Two Decimals - Always
			if (dataType == DataTypes.MONEY) {
				return "${((" + entityName + "." + beanFieldName + ")?string(\"#,###.00\"))!}";
			}
			// Min Two Decimals - Always, up to 4 decimals
			if (dataType == DataTypes.MONEY4) {
				return "${((" + entityName + "." + beanFieldName + ")?string(\"#,###.00##\"))!}";
			}
			// Up to 5 Decimals with '%' appended
			if (dataType == DataTypes.PERCENT) {
				return "${((" + entityName + "." + beanFieldName + ")?string(\"#,###.#####\") + \" %\")!}";
			}
			// Max is Exchange Rate type, which is up 16 Decimals, since it doesn't pad all should be able to use the same format)
			return "${((" + entityName + "." + beanFieldName + ")?string(\"#,###.################\"))!}";
		}
		else if (!StringUtils.isEmpty(columnName) && columnName.endsWith("Date")) {
			// "Date" fields are returned as String, so attempt to still format it as a Date
			return "${((" + entityName + "." + beanFieldName + ")?date?string(\"MM/dd/yyyy\"))!}";
		}

		return "${(" + entityName + "." + beanFieldName + ")!}";
	}


	private boolean isSkipColumnName(String name) {
		if (name == null) {
			return true;
		}
		if ("ID".equalsIgnoreCase(name)) {
			return true;
		}
		if ("rv".equalsIgnoreCase(name)) {
			return true;
		}
		return false;
	}


	private void appendTD(StringBuilder sb, String value) {
		appendTD(sb, value, null, 0);
	}


	private void appendTD(StringBuilder sb, String value, String alignment, int colspan) {
		sb.append(StringUtils.NEW_LINE);
		sb.append(StringUtils.TAB);
		sb.append(StringUtils.TAB);
		sb.append("<td");
		if (!StringUtils.isEmpty(alignment)) {
			sb.append(" align=\"").append(alignment).append("\"");
		}
		if (colspan != 0) {
			sb.append(" colspan=\"").append(colspan).append("\"");
		}
		sb.append(">");
		sb.append(value);
		sb.append("</td>");
	}


	private void appendTRStart(StringBuilder sb) {
		appendTRStart(sb, null);
	}


	private void appendTRStart(StringBuilder sb, String style) {
		sb.append(StringUtils.NEW_LINE);
		sb.append(StringUtils.TAB);
		sb.append("<tr");
		if (!StringUtils.isEmpty(style)) {
			sb.append(" style=\"").append(style).append("\"");
		}
		sb.append(">");
	}


	private void appendTREnd(StringBuilder sb) {
		sb.append(StringUtils.NEW_LINE);
		sb.append(StringUtils.TAB);
		sb.append("</tr>");
	}


	private void appendTableStart(StringBuilder sb, String style) {
		sb.append("<table");
		if (!StringUtils.isEmpty(style)) {
			sb.append(" style=\"").append(style).append("\"");
		}
		sb.append(">");
	}


	private void appendTableEnd(StringBuilder sb) {
		sb.append(StringUtils.NEW_LINE);
		sb.append("</table>");
	}


	////////////////////////////////////////////////////////////////////////
	////////////            Getters & Setters                    ///////////
	////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}
}
