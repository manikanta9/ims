package com.clifton.core.converter.json.appenders;

import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;

import java.io.IOException;


/**
 * The <code>CustomJsonStringValueAppender</code> class implements Json String serialization to JSON format.
 * Because the String is already JSON - it just returns the string, nothing to escape
 *
 * @author manderson
 */
public class CustomJsonStringValueAppender implements JsonValueAppender {

	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else {
			// Already Json Format
			resultAppender.append(value.toString());
		}
	}
}
