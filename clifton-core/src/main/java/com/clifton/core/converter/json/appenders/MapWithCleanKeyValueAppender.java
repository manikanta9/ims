package com.clifton.core.converter.json.appenders;

/**
 * The MapWithCleanKeyValueAppender class will replace spaces and dots in each key with an underscore.
 * When using bean describe, this is not necessary because property names cannot have these symbols.
 * Avoiding this replacement also improves performance. However, for cases when a Map can have any key
 * (for example a Class), this becomes very important.
 *
 * @author vgomelsky
 */
public class MapWithCleanKeyValueAppender extends MapValueAppender {

	@Override
	protected String convertKey(Object key) {
		String result = super.convertKey(key);
		if (result != null) {
			// replaces all spaces and dots with underscore: to make a valid JSON key name
			result = result.replaceAll("[ .]", "_");
		}
		return result;
	}
}
