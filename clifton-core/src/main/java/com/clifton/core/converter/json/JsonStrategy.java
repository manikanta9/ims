package com.clifton.core.converter.json;

import java.util.Set;


/**
 * This class <code>JsonStrategy</code> is intended to be a placeholder for fields that will
 * be utilized by the exclusion strategy implementations for the various JSON libraries that will be tested.
 * It is not yet in use.
 *
 * @author stevenf on 9/7/2016.
 */
public interface JsonStrategy {

	public boolean isMigration();


	/**
	 * If true, JSON related annotations will be ignored when serializing an entity.
	 */
	public boolean isDisableAnnotations();


	/**
	 * If true, wrap the entity JSON in a root data object that can be consumed by the UI.
	 * <p/>
	 * Example:
	 * <br/>{
	 * <br/>data: {entity JSON}
	 * <br/>success: true
	 * <br/>}
	 */
	public boolean isWrapInDataResponse();


	/**
	 * If true, null property values will be serialized in the entity JSON.
	 */
	public boolean isIncludeNullValues();


	public boolean isUsePrettyPrinting();


	public String getName();


	public int getMaxDepth();


	public void setMaxDepth(int maxDepth);


	public Set<String> getPropertiesToExclude();


	public void setPropertiesToExclude(Set<String> propertiesToExclude);


	public Set<String> getPropertiesToInclude();


	public void setPropertiesToInclude(Set<String> propertiesToInclude);
}
