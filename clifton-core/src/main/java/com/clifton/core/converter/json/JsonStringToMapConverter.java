package com.clifton.core.converter.json;


import com.clifton.core.util.converter.Converter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>JsonStringToMapConverter</code> class converts string in JSON format to corresponding maps of name/value pairs and lists.
 * If the argument string represents an array, returns a {@link Map} with one element with "ROOT_ARRAY" key and corresponding list as a value.
 * <p/>
 * NOT A PRETTY IMPLEMENTATION (DESIGN-WISE). MAY WANT TO REFACTOR LATER. BUT IS FAST AND ACCURATE BECAUSE OF HIGH TEST COVERAGE.
 * <p/>
 * Argument (JSON format) examples:
 * []
 * {}
 * [{"type":"date","comparison":"lt","value":"08/12/2009","field":"auditDate"}]
 * {type:124}
 *
 * @author vgomelsky
 */
public class JsonStringToMapConverter implements Converter<String, Map<String, Object>> {

	public static final String ROOT_ARRAY_NAME = "ROOT_ARRAY";


	@Override
	public Map<String, Object> convert(String from) {
		Map<String, Object> result = new HashMap<>();
		if (from != null && !from.isEmpty()) {
			int start = 0;
			if (from.charAt(start) == '{') {
				// found an object: process map
				processMap(result, from, start);
			}
			else if (from.charAt(start) == '[') {
				// found an array: use special key
				List<Object> elements = new ArrayList<>();
				result.put(ROOT_ARRAY_NAME, elements);
				processArray(elements, from, start);
			}
			else {
				throw new IllegalArgumentException("Invalid first JSON character: " + from);
			}
		}
		return result;
	}


	/**
	 * Assumes a JSON array starting at arrayStartIndex position. Populates the specified {@link List} with
	 * corresponding elements and returns the index of next character after the end of this array.
	 */
	private int processArray(List<Object> result, String from, int arrayStartIndex) {
		assertCharacter(from, arrayStartIndex, '[');
		char ch = from.charAt(arrayStartIndex + 1);
		switch (ch) {
			case ']': // end of array - no elements
				return arrayStartIndex + 2;
			case '{': // object start
				do {
					Map<String, Object> map = new HashMap<>();
					result.add(map);
					int nextCharIndex = processMap(map, from, arrayStartIndex + 1);
					ch = from.charAt(nextCharIndex);
					if (ch == ']') {
						return nextCharIndex + 1;
					}
					else if (ch == ',') {
						// next object
						arrayStartIndex = nextCharIndex;
					}
					else {
						throw new IllegalArgumentException("Unexpected character \"" + ch + "\" in position: " + nextCharIndex + " for JSON string: " + from);
					}
				}
				while (true);
			case '[': // array of arrays
				throw new RuntimeException("array of arrays is under construction");
			default: // array of values
				int start = arrayStartIndex + 1;
				if (ch == '\"') {
					// handle quoted values
					start++;
					do {
						int end = from.indexOf('\"', start);
						if (end == -1) {
							throw new IllegalArgumentException("Cannot find closing quote for array element end in position " + start + " for JSON string: " + from);
						}
						result.add(from.substring(start, end));
						start = end + 1;
						if (from.charAt(start) == ']') {
							return start + 1;
						}
						assertCharacter(from, start, ',');
						start++;
						assertCharacter(from, start, '\"');
						start++;
					}
					while (true);
				}
				// unquoted values (numbers)
				int end = from.indexOf(']', start);
				if (end == -1) {
					throw new IllegalArgumentException("Cannot find closing of array in position " + start + " for JSON string: " + from);
				}
				String[] values = from.substring(start, end).split(",");
				Collections.addAll(result, values);
				return end + 1;
		}
		//return arrayStartIndex;
	}


	/**
	 * Assumes a JSON object starting at mapStartIndex position. Populates the specified {@link Map} with object's property/value pairs.
	 * Returns the index of the next character after the end of this object.
	 */
	private int processMap(Map<String, Object> result, String from, int mapStartIndex) {
		assertCharacter(from, mapStartIndex, '{');
		char ch = from.charAt(mapStartIndex + 1);
		switch (ch) {
			case '}': // end of object - no properties
				return mapStartIndex + 2;
			case '\"': // quoted property name
				mapStartIndex++;
				do {
					int nextIndex = from.indexOf('\"', mapStartIndex + 1);
					if (nextIndex == -1) {
						throw new IllegalArgumentException("Cannot find the end of a quoted property starting at " + mapStartIndex + " for JSON string: " + from);
					}
					String propertyName = from.substring(mapStartIndex + 1, nextIndex);
					// quoted values only for now
					mapStartIndex = nextIndex + 1;
					assertCharacter(from, mapStartIndex, ':');
					mapStartIndex++;

					Object propertyValue;
					ch = from.charAt(mapStartIndex);
					if (ch == '[') {
						// Array value
						List<Object> elements = new ArrayList<>();
						mapStartIndex = processArray(elements, from, mapStartIndex);
						propertyValue = elements;
					}
					else if (ch == '{') {
						Map<String, Object> childObject = new HashMap<>();
						mapStartIndex = processMap(childObject, from, mapStartIndex);
						propertyValue = childObject;
					}
					else if (ch == '\"') {
						// find ending quote: skip all quotes that are escaped within the string: \"
						nextIndex = mapStartIndex;
						int escapedQuoteCount = -1;
						do {
							nextIndex = from.indexOf('\"', nextIndex + 1);
							if (nextIndex == -1) {
								throw new IllegalArgumentException("Cannot find the end of a quoted property starting at " + mapStartIndex + " for JSON string: " + from);
							}
							escapedQuoteCount++;
						}
						while (from.charAt(nextIndex - 1) == '\\');
						propertyValue = from.substring(mapStartIndex + 1, nextIndex);
						if (escapedQuoteCount > 0) {
							// unescape quotes if any
							propertyValue = ((String) propertyValue).replaceAll("\\\\\"", "\"");
						}
						mapStartIndex = nextIndex + 1;
					}
					else {
						// non-quoted value (number, boolean, etc.)
						nextIndex = from.indexOf('}', mapStartIndex + 1);
						if (nextIndex == -1) {
							throw new IllegalArgumentException("Cannot find the end of an object starting at " + mapStartIndex + " for JSON string: " + from);
						}
						String stringValue = from.substring(mapStartIndex, nextIndex);
						int betterNextIndex = stringValue.indexOf(',');
						if (betterNextIndex != -1) {
							stringValue = stringValue.substring(0, betterNextIndex);
							nextIndex = mapStartIndex + betterNextIndex;
						}
						mapStartIndex = nextIndex;
						propertyValue = stringValue;
					}

					result.put(propertyName, propertyValue);
					ch = from.charAt(mapStartIndex);
					if (ch == '}') {
						return mapStartIndex + 1;
					}
					else if (ch == ',') {
						// next property
						mapStartIndex++;
					}
					else {
						throw new IllegalArgumentException("Unexpected end of property value character \"" + ch + "\" in position: " + mapStartIndex + ". for JSON string: " + from);
					}
				}
				while (true);
			default: // non-quoted property name
				throw new RuntimeException("under construction");
		}
		//return mapStartIndex;
	}


	private void assertCharacter(String str, int charIndex, char expectedChar) {
		char actualChar = str.charAt(charIndex);
		if (actualChar != expectedChar) {
			throw new IllegalArgumentException("Unexpected character \"" + actualChar + "\" in position: " + charIndex + ".\nExpected \"" + expectedChar + "\" for JSON string: " + str);
		}
	}
}
