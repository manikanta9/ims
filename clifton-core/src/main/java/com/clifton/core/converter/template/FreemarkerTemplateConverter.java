package com.clifton.core.converter.template;


import com.clifton.core.json.custom.CustomJsonStringUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import freemarker.cache.ClassTemplateLoader;
import freemarker.cache.FileTemplateLoader;
import freemarker.cache.TemplateLoader;
import freemarker.core.Environment;
import freemarker.ext.beans.BeansWrapperBuilder;
import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;
import freemarker.template.TemplateHashModel;
import freemarker.template.TemplateModelException;
import freemarker.template.Version;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.Writer;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


/**
 * The <code>FreemarkerTemplateConverterImpl</code> provides a basic implementation
 * of the TemplateConverter interface and uses the Freemarker library.
 *
 * @author manderson
 */
public class FreemarkerTemplateConverter implements TemplateConverter {

	/*
	 * This setting value is the FreeMarker version number where the not 100% backward compatible bug fixes and improvements
	 * that you want to enable were already implemented. In new projects you should set this to the FreeMarker version that you
	 * are actually using. In older projects it's also usually better to keep this high, however you better check the changes
	 * activated (find them below), at least if not only the 3rd version number (the micro version) of incompatibleImprovements
	 * is increased. Generally, as far as you only increase the last version number of this setting, the changes are always low risk.
	 * The default value is 2.3.0 to maximize backward compatibility, but that value isn't recommended.
	 *
	 * http://freemarker.incubator.apache.org/docs/api/index.html
	 */
	private static final Version FREEMARKER_INCOMPATIBLE_IMPROVEMENTS_VERSION = Configuration.VERSION_2_3_23;
	private static final String FREEMARKER_DEFAULT_PRECISION = "0.################";
	private static final String QUESTION_MARK = "?";
	private static final String DEFAULT_ALPHABETIC_PREFIX = "__";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String convertFromTemplateFile(String fullTemplatePath, Map<String, Object> beanMap) {
		Path path = FileSystems.getDefault().getPath(fullTemplatePath);
		String fileName = path.getFileName().toString();
		String folderPath = path.getParent().toString();

		FileTemplateLoader loader;
		try {
			loader = new FileTemplateLoader(new File(folderPath));
		}
		catch (IOException e) {
			throw new RuntimeException("Problem loading template file [" + fullTemplatePath + "]", e);
		}
		return convert(fileName, loader, beanMap);
	}


	@Override
	public String convertFromTemplateFile(String fullTemplatePath, Class<?> clazz, Map<String, Object> beanMap) {
		Path path = FileSystems.getDefault().getPath(fullTemplatePath);
		String fileName = path.getFileName().toString();
		String folderPath = path.getParent().toString();

		ClassTemplateLoader loader = new ClassTemplateLoader(clazz, folderPath);
		return convert(fileName, loader, beanMap);
	}


	/**
	 * Optionally can expose common static utility classes: {@link DateUtils}, {@link MathUtils}, {@link StringUtils}.
	 * Example, for tasks due on 11/10/2016, "Week Ending on ${DateUtils.addDays(task.dueDate, -6)?date?string.full}"
	 * will generate: "Week Ending on Friday, November 4, 2016"
	 */
	private Configuration createConfiguration(boolean exposeUtilsClasses) {
		Configuration config = new Configuration(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS_VERSION);
		config.setObjectWrapper(new DefaultObjectWrapper(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS_VERSION));
		config.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
		config.setNumberFormat(FREEMARKER_DEFAULT_PRECISION);

		if (exposeUtilsClasses) {
			TemplateHashModel staticModels = new BeansWrapperBuilder(FREEMARKER_INCOMPATIBLE_IMPROVEMENTS_VERSION).build().getStaticModels();
			for (Class<?> clazz : new Class[]{DateUtils.class, MathUtils.class, StringUtils.class, CustomJsonStringUtils.class}) {
				try {
					config.setSharedVariable(clazz.getSimpleName(), staticModels.get(clazz.getName()));
				}
				catch (TemplateModelException e) {
					throw new RuntimeException("Cannot load DateUtils: ", e);
				}
			}
		}

		return config;
	}


	private String convert(String filename, TemplateLoader loader, Map<String, Object> beanMap) {
		Configuration config = createConfiguration(false);
		config.setTemplateLoader(loader);

		try (Writer out = new StringWriter()) {
			Template template = config.getTemplate(filename);
			template.process(beanMap, out);
			return out.toString();
		}
		catch (Throwable e) {
			throw new ValidationException("Error generating formatted string from config [" + filename + "].  Error: [" + e.getMessage() + "]", e);
		}
	}


	@Override
	public String convert(TemplateConfig config) {
		if (config == null) {
			throw new IllegalArgumentException("Cannot generate a formatted string for a null Template Config.");
		}
		Configuration cfg = createConfiguration(config.isExposeUtilsClasses());

		handleNumericPrefixedVariables(config);

		try (Writer out = new StringWriter()) {
			Template template = new Template("TemporaryTemplate", new StringReader(config.getTemplate()), cfg);
			template.process(config.getContext().getBeanMap(), out);
			return out.toString();
		}
		catch (Throwable e) {
			throw new ValidationException(String.format("Error generating formatted string from config [%s] for context [%s]. Error: [%s]", config.getTemplate(), config.getContext(), e.getMessage()), e);
		}
	}


	@Override
	public Environment processTemplate(TemplateConfig config) {
		if (config == null) {
			throw new IllegalArgumentException("Cannot process null Template Config.");
		}

		handleNumericPrefixedVariables(config);

		try (Writer out = new StringWriter()) {
			Configuration cfg = createConfiguration(config.isExposeUtilsClasses());
			Template temp = new Template("TemporaryTemplate", new StringReader(config.getTemplate()), cfg);
			Environment environment = temp.createProcessingEnvironment(config.getContext().getBeanMap(), out);
			environment.process();
			return environment;
		}
		catch (Throwable e) {
			throw new ValidationException(String.format("Error processing template from config [%s] for context [%s]. Error: [%s]", config.getTemplate(), config.getContext(), e.getMessage()), e);
		}
	}


	@Override
	public Set<String> extractAllDataFields(String template) {
		Set<String> fields = new HashSet<>();

		// Use pattern regex matching parser to pull out external fields
		Pattern p0 = Pattern.compile("\\{(.*?)}");
		Matcher m = p0.matcher(template);

		while (m.find()) {
			String value = m.group(1);

			//inclusively remove everything after a question mark
			int index = value.indexOf(QUESTION_MARK);
			if (index > 0) {
				value = value.substring(0, index);
			}

			value = value.replaceAll(DEFAULT_ALPHABETIC_PREFIX, "");

			fields.add(value);
		}
		return fields;
	}


	@Override
	public Set<String> extractDataFields(String template, String[] excludeFieldsPattern) {
		Set<String> fields = extractAllDataFields(template);

		if (excludeFieldsPattern != null && excludeFieldsPattern.length > 0) {
			for (String excludePattern : excludeFieldsPattern) {
				for (String field : fields.toArray(new String[fields.size()])) {
					if (field.contains(excludePattern)) {
						fields.remove(field);
					}
				}
			}
		}

		return fields;
	}


	/**
	 * Convert a freemarker expression given supplied fields
	 */
	@Override
	public String convertExpression(String expression, Map<String, Object> fields) {
		TemplateConfig config = new TemplateConfig(expression);
		config.getContext().setBeanMap(fields);

		return convert(config);
	}


	@Override
	public String convertOverrides(String overrideExpression, Map<Enum<?>, TemplateValueRetriever> params) {
		//This will most likely never happen, just a sanity check
		if (params.isEmpty()) {
			throw new ValidationException("Issue converting override expression, no override parameters supplied");
		}

		Map<String, Object> fields = new HashMap<>();
		Enum<?> enumEntry = params.keySet().iterator().next(); //We need to grab a reference to our generic enum for valuation

		for (String override : extractAllDataFields(overrideExpression)) {
			Enum<?> variable = Enum.valueOf(enumEntry.getClass(), override);

			TemplateValueRetriever valueRetriever = params.get(variable);
			if (valueRetriever != null) {
				fields.put(override, valueRetriever.getValue());
			}
		}

		return this.convertExpression(overrideExpression, fields);
	}


	private void handleNumericPrefixedVariables(TemplateConfig config) {
		// Adding functionality to add prefix to fields that start with an ascii digit so that freemarker can process them
		List<String> keysToRemove = config.getContext().getBeanMap()
				.keySet()
				.stream()
				.filter(key -> Character.isDigit(key.charAt(0)))
				.collect(Collectors.toList());

		if (!CollectionUtils.isEmpty(keysToRemove)) {
			Map<String, Object> tempMap = config.getContext().getBeanMap();
			for (String key : keysToRemove) {
				Object val = tempMap.get(key);
				tempMap.put(DEFAULT_ALPHABETIC_PREFIX + key, val);
			}
			config.getContext().setBeanMap(tempMap);
		}
	}
}
