package com.clifton.core.converter.json.jackson;


import com.clifton.core.util.converter.Converter;
import com.fasterxml.jackson.databind.ObjectMapper;


public class JacksonObjectToJsonStringConverter implements Converter<Object, String> {

	@Override
	public String convert(Object from) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			return mapper.writeValueAsString(from);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to convert [" + from + "] to json.", e);
		}
	}
}
