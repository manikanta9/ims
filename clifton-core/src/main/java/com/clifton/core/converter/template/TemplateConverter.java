package com.clifton.core.converter.template;


import com.clifton.core.util.converter.Converter;
import freemarker.core.Environment;

import java.util.Map;
import java.util.Set;


/**
 * The <code>TemplateConverter</code> interface defines methods for converting a template
 * to a formatted String
 *
 * @author manderson
 */
public interface TemplateConverter extends Converter<TemplateConfig, String> {

	/**
	 * Reads a template file and converts to a String using the supplied inputs.
	 *
	 * @param fullTemplatePath full path to the template file
	 * @param beanMap          map of input names to their values
	 * @return the converted String
	 */
	public String convertFromTemplateFile(String fullTemplatePath, Map<String, Object> beanMap);


	/**
	 * @param fullTemplatePath full path to the template file
	 * @param clazz            the class to read resources on the classpath from
	 * @param beanMap          map of input names to their values
	 * @return the converted String
	 */
	public String convertFromTemplateFile(String fullTemplatePath, Class<?> clazz, Map<String, Object> beanMap);


	/**
	 * Processes the given template, returning the processed {@link Environment} object containing the output writer and all resulting variables.
	 *
	 * @param config the template configuration to process
	 * @return the processed environment
	 */
	public Environment processTemplate(TemplateConfig config);


	/**
	 * Given a Freemarker template, this method will return back a set of all unique interpolations from it.
	 * <p/>
	 * Ex. "${PX_LAST} + ${PX_MID} + ${investmentSecurity.instrument.priceMultiplier}" template
	 * would return back a set of "PX_LAST", "PX_MID", and "investmentSecurity.instrument.priceMultiplier"
	 */
	public Set<String> extractAllDataFields(String template);


	/**
	 * Given a Freemarker template, this method will return back a set of all unique interpolations from it.
	 * The result will exclude the fields that contain patters to be excluded.
	 * <p/>
	 * Ex. "${PX_LAST} + ${PX_MID} + ${investmentSecurity.instrument.priceMultiplier}" template
	 * would return back a set of "PX_LAST", and "PX_MID" but not the internally defined priceMultiplier
	 * if ["investmentSecurity"] exclusion is specified.
	 */
	public Set<String> extractDataFields(String template, String[] excludeFieldsPattern);


	/**
	 * Convert a Freemarker expression given supplied fields
	 */
	public String convertExpression(String expression, Map<String, Object> fields);


	/**
	 * Given an override expression this will invoke the required template value retrievers
	 */
	public String convertOverrides(String overrideExpression, Map<Enum<?>, TemplateValueRetriever> params);


	/**
	 * A simple interface to define a generic way to retrieve values for template expression variables
	 */
	interface TemplateValueRetriever {

		public String getValue();
	}
}
