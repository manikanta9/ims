package com.clifton.core.converter.json.jackson;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.converter.json.JsonData;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.util.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * This is the core de/serialization class for the Jackson JSON implementation
 * By implementing the JacksonStrategy it allows the use of the generic jsonHandler
 * while still being capable of using Jackson specific configurations (e.g. FilterProvider)
 *
 * @author StevenF
 * @author michaelm
 */
public class JacksonHandlerImpl implements JsonHandler<JacksonStrategy> {

	private static final JacksonStrategy DEFAULT_JACKSON_STRATEGY = new JacksonStrategy();

	private JacksonStrategy jacksonStrategy;
	/**
	 * This map will store object mappers that are configured using different strategies.
	 * It is a fairly heavy operation to create an object mapper so we want to cache them
	 * after being configured by a given strategy and reuse from then on.
	 */
	private Map<String, ObjectMapper> objectMapperMap;

	////////////////////////////////////////////////////////////////////////////////


	public JacksonHandlerImpl() {
		//Default constructor does nothing
	}


	public JacksonHandlerImpl(JacksonStrategy strategy) {
		this.jacksonStrategy = strategy;
	}

	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String toJson(Object o) {
		return toJson(o, getJacksonStrategy());
	}


	@Override
	public String toJson(Object o, JacksonStrategy strategy) {
		ObjectMapper mapper = getObjectMapper(strategy);
		try {
			//TODO - Ensure the per-request filtering still works after registering globally due to bi-directional spring integration changes
			return mapper.writer(strategy.getFilterProvider()).writeValueAsString(strategy.isWrapInDataResponse() ? new JsonData(o) : o);
		}
		catch (IOException ioe) {
			throw new RuntimeException("Failed to serialize object to json [" + o.getClass().getSimpleName() + "] using strategy [" + strategy.getName() + "]: " + ioe.getMessage(), ioe);
		}
	}


	@Override
	public Object fromJson(String json, Class<?> clazz) {
		return fromJson(json, clazz, getJacksonStrategy());
	}


	@Override
	public Object fromJson(String json, Class<? extends Collection> collectionClass, Class<?> clazz) {
		try {
			ObjectMapper mapper = getObjectMapper();
			return mapper.readValue(json, mapper.getTypeFactory().constructCollectionType(collectionClass, clazz));
		}
		catch (IOException ioe) {
			throw new RuntimeException("Failed to deserialize json to object using strategy [" + getJacksonStrategy().getName() + "]: " + ioe.getMessage() + "\nJSON: " + StringUtils.formatStringUpToNCharsWithDots(json, 10000), ioe);
		}
	}


	@Override
	public Object fromJson(InputStream inputStream, Class<?> clazz) {
		return fromJson(inputStream, clazz, getJacksonStrategy());
	}


	@Override
	public Object fromJson(String json, Class<?> clazz, JacksonStrategy strategy) {
		try {
			return getObjectMapper(strategy).readValue(json, clazz);
		}
		catch (IOException ioe) {
			throw new RuntimeException("Failed to deserialize json to object using strategy [" + strategy.getName() + "]: " + ioe.getMessage() + "\nJSON: " + StringUtils.formatStringUpToNCharsWithDots(json, 10000), ioe);
		}
	}


	@Override
	public Object fromJson(InputStream inputStream, Class<?> clazz, JacksonStrategy strategy) {
		try {
			return getObjectMapper(strategy).readValue(inputStream, clazz);
		}
		catch (IOException ioe) {
			throw new RuntimeException("Failed to deserialize json to object using strategy [" + strategy.getName() + "]: " + ioe.getMessage(), ioe);
		}
	}


	@Override
	public Object convertValue(Object o, Class<?> clazz) {
		ObjectMapper mapper = getObjectMapper();
		return mapper.convertValue(o, clazz);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The object mapper is configured once and the reused as it is a fairly heavy hit for initialization.
	 * This should suit our needs fine in the end we should be loading a jsonHandler for IMS initialized one way
	 * and a migrationJsonHandler initialized the other.
	 */
	public ObjectMapper getObjectMapper() {
		if (getJacksonStrategy() != null) {
			return getObjectMapper(getJacksonStrategy());
		}
		return getObjectMapper(DEFAULT_JACKSON_STRATEGY);
	}


	public ObjectMapper getObjectMapper(JacksonStrategy strategy) {
		ObjectMapper objectMapper = getObjectMapperMap().get(strategy.getName());
		if (objectMapper == null) {
			//TODO - look at using this for custom serialization/filtering: https://github.com/monitorjbl/json-view
			objectMapper = new JacksonObjectMapper(strategy);
			getObjectMapperMap().put(strategy.getName(), objectMapper);
		}
		return objectMapper;
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////               Getter and Setter Methods              //////////////
	////////////////////////////////////////////////////////////////////////////////


	@ValueIgnoringGetter
	public JacksonStrategy getJacksonStrategy() {
		return this.jacksonStrategy != null ? this.jacksonStrategy : DEFAULT_JACKSON_STRATEGY;
	}


	public void setJacksonStrategy(JacksonStrategy jacksonStrategy) {
		this.jacksonStrategy = jacksonStrategy;
	}


	@ValueIgnoringGetter
	public Map<String, ObjectMapper> getObjectMapperMap() {
		if (this.objectMapperMap == null) {
			this.objectMapperMap = new HashMap<>();
		}
		return this.objectMapperMap;
	}


	public void setObjectMapperMap(Map<String, ObjectMapper> objectMapperMap) {
		this.objectMapperMap = objectMapperMap;
	}
}
