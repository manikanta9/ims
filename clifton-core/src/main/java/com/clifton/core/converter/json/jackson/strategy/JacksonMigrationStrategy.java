package com.clifton.core.converter.json.jackson.strategy;

import com.clifton.core.util.CollectionUtils;

import java.util.Set;


/**
 * This class is here simply to demonstrate the use of a Jackson Module, here serializers, deserializers and mix-ins can be registered. A module could be
 * selected at run-time to provide various forms of serialization.
 *
 * @author StevenF on 10/4/2016.
 */
public class JacksonMigrationStrategy extends JacksonStrategy {

	private static final Set<String> EXCLUDED_MIGRATION_PROPERTIES = CollectionUtils.createHashSet("id", "createUserId", "createDate", "updateUserId", "updateDate", "rv");


	@Override
	public boolean isWrapInDataResponse() {
		return true;
	}


	@Override
	public boolean isMigration() {
		return true;
	}


	@Override
	public Set<String> getPropertiesToExclude() {
		Set<String> propertiesToExclude = super.getPropertiesToExclude();
		if (propertiesToExclude == null) {
			propertiesToExclude = EXCLUDED_MIGRATION_PROPERTIES;
		}
		else {
			propertiesToExclude.addAll(EXCLUDED_MIGRATION_PROPERTIES);
		}
		return propertiesToExclude;
	}
}
