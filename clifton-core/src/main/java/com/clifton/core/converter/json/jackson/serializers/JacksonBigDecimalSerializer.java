package com.clifton.core.converter.json.jackson.serializers;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import java.io.IOException;
import java.math.BigDecimal;


/**
 * @author StevenF
 */
public class JacksonBigDecimalSerializer extends JsonSerializer<BigDecimal> {

	@Override
	public void serialize(BigDecimal number, JsonGenerator generator, SerializerProvider provider) throws IOException {
		generator.writeString(number.stripTrailingZeros().toPlainString());
	}
}
