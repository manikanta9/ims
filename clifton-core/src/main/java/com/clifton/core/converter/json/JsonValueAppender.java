package com.clifton.core.converter.json;


import java.io.IOException;
import java.net.SocketTimeoutException;


/**
 * The <code>JsonValueAppender</code> interface should be implemented for various objects
 * that have different JSON representation.  Appenders are registered for {@link JsonConfig}
 * and are used for object serialization to JSON format.
 *
 * @author vgomelsky
 */
public interface JsonValueAppender {

	/**
	 * Appends JSON formatted value to the specified <code>resultAppender</code>.
	 * <p>
	 * While we tend to avoid checked exceptions by convention, this method signature declares an {@link IOException} to propagate errors that are triggered within the backing
	 * {@link Appendable}. Such cases may include a <code>ClientAbortException</code> or a {@link SocketTimeoutException}. In this way, the checked exception must be handled in the
	 * same way that it would be handled if writing directly to a backing {@link Appendable} capable of throwing such exceptions.
	 *
	 * @param resultAppender the {@link Appendable} to which the JSON value should be written
	 * @param key            the key under which the JSON value should be written
	 * @param value          the object to be serialized as a JSON value
	 * @param config         the {@link JsonConfig} specifying parameters to use during serialization
	 * @param session        the {@link JsonSession} which tracks the context of the current serialization process
	 * @throws IOException upon exception while writing to the <code>resultAppender</code>
	 */
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException;
}
