package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.util.collections.FlatMap;
import com.clifton.core.util.converter.string.ObjectToStringConverter;

import java.io.IOException;
import java.util.Map;


/**
 * The <code>MapValueAppender</code> class serializes {@link Map} implementations to JSON format.
 *
 * @author vgomelsky
 */
public class MapValueAppender implements JsonValueAppender {

	private ObjectToStringConverter stringConverter = new ObjectToStringConverter();


	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("{}");
		}
		else if (!(value instanceof Map)) {
			throw new IllegalArgumentException("Map is expected for key=" + key + " instead of " + value.getClass());
		}
		else {
			@SuppressWarnings("unchecked")
			Map<String, Object> valueMap = (Map<String, Object>) value;
			resultAppender.append('{');
			int count = 0;
			for (Map.Entry<String, Object> entry : valueMap.entrySet()) {
				String k = convertKey(entry.getKey());
				if (k != null) {
					Object v = entry.getValue();
					if (v == null && config.isSkipNullFields()) {
						continue;
					}
					if (v instanceof FlatMap) {
						for (Map.Entry<?, ?> entry1 : ((Map<?, ?>) v).entrySet()) {
							String k1 = convertKey(entry1.getKey());
							Object v1 = entry1.getValue();
							if (v1 == null && config.isSkipNullFields()) {
								continue;
							}
							// serialize only properties that are not filtered out
							session.pushFilterPathNode(k1);
							if (!session.isFilteredOut()) {
								// do not serialize parents of this bean (circular references)
								if (!session.isVisitedParent(v1)) {
									if (count > 0) {
										resultAppender.append(',');
									}
									resultAppender.append('"').append(k1).append("\":");
									JsonValueAppender appender = config.getAppender(v1);
									appender.append(resultAppender, k1, v1, config, session);
								}
							}
							session.popFilterPathNode();
						}
					}
					else {
						// serialize only properties that are not filtered out
						session.pushFilterPathNode(k);
						if (!session.isFilteredOut()) {
							// do not serialize parents of this bean (circular references)
							if (!session.isVisitedParent(v)) {
								if (count > 0) {
									resultAppender.append(',');
								}
								resultAppender.append('"').append(k).append("\":");
								JsonValueAppender appender = config.getAppender(v);
								appender.append(resultAppender, k, v, config, session);
								count++;
							}
						}
						session.popFilterPathNode();
					}
				}
			}
			resultAppender.append('}');
		}
	}


	protected String convertKey(Object key) {
		return this.stringConverter.convert(key);
	}
}
