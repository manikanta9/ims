package com.clifton.core.converter.reversable;


import com.clifton.core.util.MathUtils;

import java.math.BigDecimal;


/**
 * The <code>PercentReversableConverter</code> class is a ReversableConverter for data type name: Percent
 * When displayed in Excel, we use excel formatting for percentages, so the value must be divided by 100.  When importing from excel
 * we need to multiply the value by 100 to get the value.
 * <p/>
 * For example, in IMS we store 1.5 for 1.5%, the actual value that excel uses for this would be 0.015 with percent formatting
 * but if we were to upload 0.015 from excel, we need to convert back to 1.5 to save in IMS
 *
 * @author manderson
 */
public class PercentReversableConverter implements ReversableConverter<Object, BigDecimal> {

	@Override
	public BigDecimal convert(Object from) {
		if (from == null) {
			return null;
		}
		BigDecimal fromVal;
		if (from instanceof BigDecimal) {
			fromVal = (BigDecimal) from;
		}
		else {
			fromVal = new BigDecimal(from.toString());
		}
		return MathUtils.multiply(fromVal, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
	}


	@Override
	public BigDecimal reverseConvert(BigDecimal to) {
		if (to == null) {
			return null;
		}
		return MathUtils.divide(to, MathUtils.BIG_DECIMAL_ONE_HUNDRED);
	}
}
