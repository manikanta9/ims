package com.clifton.core.converter.template;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextImpl;
import com.clifton.core.util.StringUtils;

import java.util.Map;


/**
 * The <code>TemplateConfig</code> contains the configuration information
 * for converting a String & Context to a formatted String.
 *
 * @author manderson
 */
public class TemplateConfig {

	private String template;
	private Context context;

	/**
	 * Set to true to add common static utility classes to the context
	 */
	private boolean exposeUtilsClasses;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static TemplateConfig ofTemplate(String template) {
		return new TemplateConfig((template));
	}


	public static TemplateConfig ofTemplate(String template, Map<String, Object> params) {
		TemplateConfig result = new TemplateConfig(template);
		if (params != null) {
			result.getContext().setBeanMap(params);
		}
		return result;
	}


	public static TemplateConfig ofTemplateWithUtilsClasses(String template) {
		return ofTemplateWithUtilsClasses(template, null);
	}


	public static TemplateConfig ofTemplateWithUtilsClasses(String template, Map<String, Object> params) {
		TemplateConfig result = ofTemplate(template, params);
		result.exposeUtilsClasses = true;
		return result;
	}


	public TemplateConfig(String template) {
		if (StringUtils.isEmpty(template)) {
			throw new IllegalArgumentException("Template string is required.");
		}
		this.template = template;
		this.context = new ContextImpl();
	}


	/**
	 * Add an object to the context with the given name.
	 * Returns itself (this) to allow method chaining.
	 */
	public TemplateConfig addBeanToContext(String beanName, Object bean) {
		this.context.setBean(beanName, bean);
		return this;
	}


	public String getTemplate() {
		return this.template;
	}


	public Context getContext() {
		return this.context;
	}


	public boolean isExposeUtilsClasses() {
		return this.exposeUtilsClasses;
	}
}
