package com.clifton.core.converter.json.jackson.strategy;

/**
 * This class is here simply to demonstrate the use of a Jackson Module, here serializers, deserializers and mix-ins can be registered. A module could be
 * selected at run-time to provide various forms of serialization.
 *
 * @author StevenF on 10/4/2016.
 */
public class JacksonMigrationActionStrategy extends JacksonMigrationStrategy {

	@Override
	public boolean isWrapInDataResponse() {
		return false;
	}


	@Override
	public boolean isUsePrettyPrinting() {
		return true;
	}
}
