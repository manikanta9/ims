package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;

import java.io.IOException;
import java.util.Collection;


/**
 * The <code>CollectionValueAppender</code> class serializes {@link Collection} implementations to JSON format.
 *
 * @author vgomelsky
 */
public class CollectionValueAppender implements JsonValueAppender {

	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("[]");
		}
		else if (!(value instanceof Collection)) {
			throw new IllegalArgumentException("Collection is expected for key=" + key + " instead of " + value.getClass());
		}
		else {
			Collection<?> valueList = (Collection<?>) value;
			resultAppender.append('[');
			int count = 0;
			for (Object v : valueList) {
				if (v == null && config.isSkipNullFields()) {
					continue;
				}
				if (count > 0) {
					resultAppender.append(',');
				}
				JsonValueAppender appender = config.getAppender(v);
				appender.append(resultAppender, null, v, config, session);
				count++;
			}
			resultAppender.append(']');
		}
	}
}
