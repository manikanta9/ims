package com.clifton.core.converter.string;


import com.clifton.core.util.converter.Converter;

import java.math.BigDecimal;


/**
 * The <code>StringToBigDecimalConverter</code> class converts String to corresponding BigDecimal objects.
 * It throws {@link NumberFormatException} if conversion cannot be performed.
 *
 * @author manderson
 */
public class StringToBigDecimalConverter implements Converter<String, BigDecimal> {

	@Override
	public BigDecimal convert(String from) {
		return new BigDecimal(from);
	}
}
