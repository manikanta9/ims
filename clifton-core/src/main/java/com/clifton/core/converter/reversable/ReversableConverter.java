package com.clifton.core.converter.reversable;

import com.clifton.core.util.converter.Converter;


/**
 * The <code>ReversableConverter</code> extends the {@link Converter} interface, but also has a method to convert the To back to the From
 *
 * @author Mary Anderson
 */
public interface ReversableConverter<F, T> extends Converter<F, T> {

	/**
	 * Converts the To value back to a From value
	 */
	public F reverseConvert(T to);
}
