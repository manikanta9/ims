package com.clifton.core.converter.template.method;

import com.clifton.core.util.math.CoreMathUtils;

import java.math.BigDecimal;


/**
 * A FreeMarker number-formatter which formats a number according to its actual precision.
 *
 * @author MikeH
 */
public class FreeMarkerFormatNumberPreciseMethod extends FreeMarkerFormatNumberMethod {

	@Override
	public String formatNumber(BigDecimal value) {
		return CoreMathUtils.formatNumberDecimalMax(value);
	}
}
