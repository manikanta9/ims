package com.clifton.core.converter.string;


import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.date.DateUtils;

import java.util.Date;


/**
 * The <code>StringToDateConverter</code> class converts String to corresponding Date objects.
 * It throws {@link IllegalArgumentException} if conversion cannot be performed.
 *
 * @author vgomelsky
 */
public class StringToDateConverter implements Converter<String, Date> {

	@Override
	public Date convert(String from) {
		return DateUtils.toDate(from);
	}
}
