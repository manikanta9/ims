package com.clifton.core.converter.json.jackson.strategy;

import com.clifton.core.beans.annotations.ValueIgnoringGetter;
import com.clifton.core.converter.json.JsonStrategy;
import com.clifton.core.converter.json.jackson.JacksonObjectMapper;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;

import java.util.HashSet;
import java.util.Set;


/**
 * Provides default serialization strategy for Jackson, allows for specifying custom filters, de/serializers.
 *
 * @author StevenF on 10/4/2016.
 */
public class JacksonStrategy implements JsonStrategy {

	private int maxDepth;
	private Set<String> propertiesToExclude;
	private Set<String> propertiesToInclude;

	private FilterProvider filterProvider;


	/**
	 * Override this method to register custom serializers and/or deserializers.
	 */
	public void registerModules(ObjectMapper objectMapper) {
		//DO NOTHING
	}


	@Override
	public String getName() {
		return this.getClass().getSimpleName();
	}


	@Override
	public boolean isMigration() {
		return false;
	}


	@Override
	public boolean isDisableAnnotations() {
		return false;
	}


	@Override
	public boolean isWrapInDataResponse() {
		return false;
	}


	@Override
	public boolean isIncludeNullValues() {
		return false;
	}


	@Override
	public boolean isUsePrettyPrinting() {
		return false;
	}


	@Override
	@ValueIgnoringGetter
	public Set<String> getPropertiesToExclude() {
		if (this.propertiesToExclude == null) {
			this.propertiesToExclude = new HashSet<>();
		}
		return this.propertiesToExclude;
	}

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	/**
	 * Create default filter provider
	 * Example use of built in filter functionality.
	 * More robust filtering must be done using a custom filter, or more likely, a custom serializer.
	 * <p>
	 * This allows for custom handling of any given class by using additional annotations and/or custom filters.  In our case, we can likely make a single custom filter that will
	 * provide all the customization we need on a per request basis, for now I use a default exclusion filter.
	 */
	@ValueIgnoringGetter
	public FilterProvider getFilterProvider() {
		if (this.filterProvider == null) {
			if (getPropertiesToExclude() != null) {
				this.filterProvider = new SimpleFilterProvider()
						.addFilter(JacksonObjectMapper.EXCLUSION_FILTER_NAME, SimpleBeanPropertyFilter.SerializeExceptFilter.serializeAllExcept(getPropertiesToExclude()));
			}
		}
		return this.filterProvider;
	}


	public void setFilterProvider(FilterProvider filterProvider) {
		this.filterProvider = filterProvider;
	}


	@Override
	public int getMaxDepth() {
		return this.maxDepth;
	}


	@Override
	public void setMaxDepth(int maxDepth) {
		this.maxDepth = maxDepth;
	}


	@Override
	public void setPropertiesToExclude(Set<String> propertiesToExclude) {
		this.propertiesToExclude = propertiesToExclude;
	}


	@Override
	public Set<String> getPropertiesToInclude() {
		return this.propertiesToInclude;
	}


	@Override
	public void setPropertiesToInclude(Set<String> propertiesToInclude) {
		this.propertiesToInclude = propertiesToInclude;
	}
}
