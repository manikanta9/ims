package com.clifton.core.converter.json.appenders;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonUtils;
import com.clifton.core.converter.json.JsonValueAppender;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.Map;


/**
 * The <code>ObjectValueAppender</code> class implements object serialization to JSON format.
 *
 * @author vgomelsky
 */
public class ObjectValueAppender implements JsonValueAppender {

	/**
	 * Optionally restrict bean describe to objects that are instances of classes that start with the following path.
	 * It's dangerous to describe any class because they may often result in an error or infinite recursion.
	 * For example, use the following to limit describe to our classes: "com.clifton."
	 */
	private String describePathRestriction;

	/**
	 * When describing a bean, it maybe useful to include its "class" property to assist debugging complex object trees.  Exclude by default.
	 */
	private boolean includeClassProperty;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ObjectValueAppender includeClassProperty() {
		setIncludeClassProperty(true);
		return this;
	}


	@Override
	public void append(Appendable resultAppender, @SuppressWarnings("unused") String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else if (value instanceof Number || value instanceof Boolean) {
			resultAppender.append(String.valueOf(value));
		}
		else if (value instanceof Enum<?>) {
			resultAppender.append('\"');
			resultAppender.append(String.valueOf(value));
			resultAppender.append('\"');
		}
		else if (value instanceof Class<?>) {
			// classes have very long and complex circular dependencies so we use a simplified representation
			resultAppender.append('\"');
			resultAppender.append(((Class<?>) value).getName());
			resultAppender.append('\"');
		}
		else if (value.getClass().isArray()) {
			resultAppender.append('[');
			int count = 0;
			int size = Array.getLength(value);
			for (int i = 0; i < size; i++) {
				Object v = Array.get(value, i);
				if (v == null && config.isSkipNullFields()) {
					continue;
				}
				// do not serialize parents of this bean (circular references)
				if (!session.isVisitedParent(v)) {
					if (count > 0) {
						resultAppender.append(',');
					}
					JsonValueAppender appender = config.getAppender(v);
					appender.append(resultAppender, null, v, config, session);
					count++;
				}
			}
			resultAppender.append(']');
		}
		else {
			if (getDescribePathRestriction() == null || value.getClass().getName().startsWith(getDescribePathRestriction())) {
				// Describe the bean but remove system properties and ignored properties for serialization
				Object mappedValue = session.getSerializeEntity(value);
				Map<String, Object> beanMap = describeObject(mappedValue);
				removeProperties(beanMap);

				// mark this bean as visited to avoid circular references for its children, etc.
				session.pushVisited(value);

				JsonValueAppender appender = config.getAppender(beanMap);
				appender.append(resultAppender, null, beanMap, config, session);

				session.popVisited();
			}
			else {
				resultAppender.append('\"');
				resultAppender.append(JsonUtils.sanitizeJsonStringValue(value.toString()));
				resultAppender.append('\"');
			}
		}
	}


	protected Map<String, Object> describeObject(Object bean) {
		if (bean == null) {
			return null;
		}
		Map<String, Object> result = BeanUtils.describe(bean);
		if (isIncludeClassProperty() && !result.containsKey("class")) {
			result.put("class", bean.getClass().getName());
		}
		return result;
	}


	protected void removeProperties(Map<String, Object> beanMap) {
		if (!isIncludeClassProperty()) {
			beanMap.remove("class");
		}
		beanMap.remove("hibernateLazyInitializer");
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	public String getDescribePathRestriction() {
		return this.describePathRestriction;
	}


	public void setDescribePathRestriction(String describePathRestriction) {
		this.describePathRestriction = describePathRestriction;
	}


	public boolean isIncludeClassProperty() {
		return this.includeClassProperty;
	}


	public void setIncludeClassProperty(boolean includeClassProperty) {
		this.includeClassProperty = includeClassProperty;
	}
}
