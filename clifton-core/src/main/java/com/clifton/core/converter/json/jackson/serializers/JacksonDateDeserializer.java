package com.clifton.core.converter.json.jackson.serializers;

import com.clifton.core.util.date.DateUtils;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import java.io.IOException;
import java.util.Date;


/**
 * The JacksonDateDeserializer class converts String representation of a date to the java Date object.
 * It assumes the dates are in the format specified in the constructor.
 * Defaults to {@link DateUtils#DATE_FORMAT_SQL_PRECISE}, most precise format that includes milliseconds.
 *
 * @author StevenF
 */
public class JacksonDateDeserializer extends JsonDeserializer<Date> {

	private String dateFormat;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public JacksonDateDeserializer() {
		this(DateUtils.DATE_FORMAT_SQL_PRECISE);
	}


	public JacksonDateDeserializer(String dateFormat) {
		this.dateFormat = dateFormat;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Date deserialize(JsonParser parser, DeserializationContext context) throws IOException {
		return DateUtils.toDate(parser.getText(), this.dateFormat);
	}
}
