package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;

import java.io.IOException;


/**
 * The <code>ConstantStringAppender</code> class always appends the spring specified to the constructor.
 *
 * @author vgomelsky
 */
public class ConstantStringAppender implements JsonValueAppender {

	private final String valueToAppend;


	public ConstantStringAppender(String valueToAppend) {
		this.valueToAppend = valueToAppend;
	}


	@Override
	@SuppressWarnings("unused")
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		resultAppender.append('\"');
		resultAppender.append(this.valueToAppend);
		resultAppender.append('\"');
	}
}
