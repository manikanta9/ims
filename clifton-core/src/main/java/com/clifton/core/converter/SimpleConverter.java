package com.clifton.core.converter;


import com.clifton.core.util.converter.Converter;


/**
 * The <code>SimpleConverter</code> class doesn't do any conversion and returns unchanged argument.
 *
 * @param <T>
 * @author vgomelsky
 */
public class SimpleConverter<T> implements Converter<T, T> {

	@Override
	public T convert(T from) {
		return from;
	}
}
