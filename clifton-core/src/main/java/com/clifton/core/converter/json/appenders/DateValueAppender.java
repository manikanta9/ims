package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.util.date.DateUtils;

import java.io.IOException;
import java.util.Date;


/**
 * The <code>DateValueAppender</code> class allows converting Date objects to a predefined format.
 *
 * @author vgomelsky
 */
public class DateValueAppender implements JsonValueAppender {

	private String format = DateUtils.DATE_FORMAT_FULL;


	@Override
	@SuppressWarnings("unused")
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("null");
		}
		else {
			Date dateValue = (Date) value;
			resultAppender.append('\"');
			resultAppender.append(DateUtils.fromDate(dateValue, getFormat()));
			resultAppender.append('\"');
		}
	}


	public String getFormat() {
		return this.format;
	}


	public void setFormat(String format) {
		this.format = format;
	}
}
