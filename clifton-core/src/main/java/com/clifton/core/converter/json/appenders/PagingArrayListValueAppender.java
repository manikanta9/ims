package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;
import com.clifton.core.util.dataaccess.PagingArrayList;

import java.io.IOException;


/**
 * The <code>PagingArrayListValueAppender</code> class serializes {@link PagingArrayList} implementations to JSON format.
 *
 * @author vgomelsky
 */
public class PagingArrayListValueAppender implements JsonValueAppender {

	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		if (value == null) {
			resultAppender.append("{}");
		}
		else if (!(value instanceof PagingArrayList)) {
			throw new IllegalArgumentException("PagingArrayList is expected for key=" + key + " instead of " + value.getClass());
		}
		else {
			PagingArrayList<?> pagingList = (PagingArrayList<?>) value;
			int total = pagingList.getTotalElementCount();
			resultAppender.append("{\"rows\":[");
			int count = 0;
			session.pushFilterPathNode("rows");
			for (Object v : pagingList) {
				if (v == null && config.isSkipNullFields()) {
					total--;
					continue;
				}
				if (count > 0) {
					resultAppender.append(',');
				}
				JsonValueAppender appender = config.getAppender(v);
				appender.append(resultAppender, null, v, config, session);
				count++;
			}
			// append stats in the end because total may change for skipped rows
			resultAppender.append("],\"start\":");
			resultAppender.append(String.valueOf(pagingList.getFirstElementIndex()));
			resultAppender.append(",\"total\":");
			resultAppender.append(String.valueOf(total));
			resultAppender.append('}');
			session.popFilterPathNode();
		}
	}
}
