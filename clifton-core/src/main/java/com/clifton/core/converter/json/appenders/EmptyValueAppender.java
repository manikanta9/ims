package com.clifton.core.converter.json.appenders;


import com.clifton.core.converter.json.JsonConfig;
import com.clifton.core.converter.json.JsonSession;
import com.clifton.core.converter.json.JsonValueAppender;

import java.io.IOException;


public class EmptyValueAppender implements JsonValueAppender {

	@SuppressWarnings("unused")
	@Override
	public void append(Appendable resultAppender, String key, Object value, JsonConfig config, JsonSession session) throws IOException {
		resultAppender.append("{}");
	}
}
