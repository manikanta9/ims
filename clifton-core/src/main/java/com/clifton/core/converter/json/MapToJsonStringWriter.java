package com.clifton.core.converter.json;


import java.util.Map;
import java.util.function.Consumer;


/**
 * The {@link MapToJsonStringWriter} type is a facade for convenient usage of a backing {@link MapWithRulesToJsonStringWriter}.
 *
 * @author vgomelsky
 * @see MapWithRulesToJsonStringWriter
 */
public class MapToJsonStringWriter {

	private final MapWithRulesToJsonStringWriter writer = new MapWithRulesToJsonStringWriter();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public <T extends Appendable> T write(T output, Map<String, Object> from) {
		return this.writer.write(output, new MapWithRules(from, MapWithRules.DEFAULT_MAX_DEPTH, false));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns initialized JsonConfig object.
	 */
	protected JsonConfig getJsonConfig(Consumer<JsonConfig> initializer) {
		return this.writer.getJsonConfig(initializer);
	}
}
