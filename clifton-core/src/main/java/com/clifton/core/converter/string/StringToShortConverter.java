package com.clifton.core.converter.string;


import com.clifton.core.util.converter.Converter;


/**
 * The <code>StringToShortConverter</code> class converts String to corresponding Short objects.
 * It throws {@link NumberFormatException} if conversion cannot be performed.
 *
 * @author manderson
 */
public class StringToShortConverter implements Converter<String, Short> {

	@Override
	public Short convert(String from) {
		return Short.parseShort(from);
	}
}
