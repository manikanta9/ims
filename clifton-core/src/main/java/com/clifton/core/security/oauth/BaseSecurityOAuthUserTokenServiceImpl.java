package com.clifton.core.security.oauth;

import com.clifton.core.security.SecurityUser;
import com.clifton.core.security.oauth.client.connection.SecurityOAuth2TokenService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * The <code>BaseSecurityOAuthUserTokenServiceImpl</code> is an abstract class that should be extended with implementation to return real users from the application
 *
 * @author manderson
 */
public abstract class BaseSecurityOAuthUserTokenServiceImpl<T extends SecurityUser> implements SecurityOAuthUserTokenService {

	/**
	 * Tokens cached using key username_hostname
	 */
	private final Map<String, OAuth2AccessToken> userTokenCache = new ConcurrentHashMap<>();


	private SecurityOAuth2TokenService securityOAuth2TokenService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////
	//  These two methods are the only methods that should need to be implemented based on how users are set up in the system.


	protected abstract T getSecurityUserCurrent();


	protected abstract T getSecurityUserByName(String userName);

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTokenForCurrentUser(String endpointUrl) {
		return getTokenForSecurityUser(getSecurityUserCurrent(), endpointUrl, false, false);
	}


	@Override
	public String getTokenForCurrentUser(String endpointUrl, boolean clearTokenCache) {
		return getTokenForSecurityUser(getSecurityUserCurrent(), endpointUrl, clearTokenCache, false);
	}


	@Override
	public String getTokenForCurrentUserIfPresent(String endpointUrl, boolean clearTokenCache) {
		return getTokenForSecurityUser(getSecurityUserCurrent(), endpointUrl, clearTokenCache, true);
	}


	@Override
	public String getTokenForUsername(String username, String endpointUrl) {
		return getTokenForSecurityUser(getSecurityUserByName(username), endpointUrl, false, false);
	}


	@Override
	public String getTokenForUsername(String username, String endpointUrl, boolean clearTokenCache) {
		return getTokenForSecurityUser(getSecurityUserByName(username), endpointUrl, clearTokenCache, false);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	private String getTokenForSecurityUser(SecurityUser user, String endpointUrl, boolean clearTokenCache, boolean ignoreMissingUser) {
		if (clearTokenCache) {
			this.userTokenCache.clear();
		}
		if (user == null && ignoreMissingUser) {
			return null;
		}
		ValidationUtils.assertNotNull(user, "Cannot get OAuth2 Token for a null SecurityUser");
		OAuth2AccessToken token = this.userTokenCache.compute(getTokenCacheKey(user.getUserName(), endpointUrl), (key, existing) -> {
			//Check if there is a token in the token cache, if there is make sure the token will not expire within 10 seconds
			if (existing == null || DateUtils.isDateAfter(DateUtils.addSeconds(new Date(), -10), existing.getExpiration())) {
				return getSecurityOAuth2TokenService().getJwtBearerTokenForSecurityUser(user.getUserName(), endpointUrl);
			}
			return existing;
		});

		return token == null ? null : token.toString();
	}


	private String getTokenCacheKey(String username, String hostname) {
		return username + '_' + hostname;
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityOAuth2TokenService getSecurityOAuth2TokenService() {
		return this.securityOAuth2TokenService;
	}


	public void setSecurityOAuth2TokenService(SecurityOAuth2TokenService securityOAuth2TokenService) {
		this.securityOAuth2TokenService = securityOAuth2TokenService;
	}
}
