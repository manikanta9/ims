package com.clifton.core.security.password.passay;

import org.passay.CharacterData;


/**
 * The <code>PasswordGeneratorCharacterData</code> is used to define allowable characters for generating passwords
 * We are more strict on what characters we use for generating passwords to prevent confusion between zero, o, l, and ones
 * As well as removing all vowels from the allowed character set in order to prevent accidentally generating words that could be deemed inappropriate
 *
 * @author manderson
 */
public enum PasswordGeneratorCharacterData implements CharacterData {

	/**
	 * Lower case characters. exclude l, o and all vowels
	 */
	LOWER_CASE("INSUFFICIENT_LOWERCASE", "bcdfghjkmnpqrstvwxyz"),

	/**
	 * Upper case characters. exclude L, O and all vowels
	 */
	UPPER_CASE("INSUFFICIENT_UPPERCASE", "BCDFGHJKMNPQRSTVWXYZ"),


	/**
	 * Digit characters. Exclude 0 and 1
	 */
	DIGIT("INSUFFICIENT_DIGIT", "23456789");


	private final String errorCode;

	private final String characters;


	PasswordGeneratorCharacterData(final String code, final String charString) {
		this.errorCode = code;
		this.characters = charString;
	}


	@Override
	public String getErrorCode() {
		return this.errorCode;
	}


	@Override
	public String getCharacters() {
		return this.characters;
	}
}
