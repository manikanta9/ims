package com.clifton.core.security.oauth.client;

import com.clifton.core.beans.annotations.ValueChangingSetter;
import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.concurrent.ExecutableAction;
import com.clifton.core.util.concurrent.synchronize.SynchronizableBuilder;
import com.clifton.core.util.concurrent.synchronize.handler.SynchronizationHandler;
import com.clifton.core.util.token.JsonWebTokenUtils;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.NoSuchClientException;
import org.springframework.stereotype.Controller;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Service for handling our oauth clients.
 *
 * @author theodorez
 */
@Controller
public class SecurityOAuthClientDetailServiceImpl implements SecurityOAuthClientDetailService {

	/**
	 * Keyed by clientId
	 */
	private final Map<String, SecurityOAuthClientDetail> clientDetailsStore = new ConcurrentHashMap<>();

	private String clientConfigurationFile;

	/**
	 * The URL of THIS application / client
	 */
	private String applicationUrl;

	/**
	 * Folder that signature key files will be stored in
	 */
	private String signatureKeyFolder;


	private JsonHandler<JacksonStrategy> jsonHandler;

	private SynchronizationHandler synchronizationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ClientDetails loadClientByClientId(String clientId) {
		SecurityOAuthClientDetail details = getClientDetailsStore().computeIfAbsent(clientId, key -> {
			SecurityOAuthClientDetail lookedUp = null;
			try {
				lookedUp = getClientDetailConfig().getClient(clientId);
			}
			catch (Exception e) {
				LogUtils.warn(this.getClass(), "Failed to load OAuth details for client with ID: " + clientId, e);
			}
			if (lookedUp == null) {
				throw new NoSuchClientException("No client with requested id: " + clientId);
			}
			return lookedUp;
		});
		return details;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public SecurityOAuthClientDetail getClientDetailsFromFile(String id) {
		return getClientDetailConfig().getClient(id);
	}


	@Override
	public SecurityOAuthClientDetail saveClientDetailToFiles(SecurityOAuthClientDetail clientDetail) {
		//Remove the signature key from the client information for storage in separate file
		ValidationUtils.assertNotNull(clientDetail, "Cannot save null client details to files");
		addClientDetailKey(clientDetail);

		SecurityOAuthClientDetailConfig config = getClientDetailConfig();
		config.addClient(clientDetail);
		writeClientDetailConfig(config);

		getClientDetailsStore().put(clientDetail.getClientId(), clientDetail);
		return clientDetail;
	}


	@Override
	public SecurityOAuthClientDetail deleteClientDetailFromFiles(String id) {
		SecurityOAuthClientDetailConfig config = getClientDetailConfig();
		SecurityOAuthClientDetail client = config.removeClient(id);

		ValidationUtils.assertNotNull(client, "Invalid client ID: " + id);
		deleteClientDetailKey(client);

		writeClientDetailConfig(config);

		getClientDetailsStore().remove(client.getClientId());
		return client;
	}


	@Override
	public List<SecurityOAuthClientDetail> getClientDetailsList() {
		return new ArrayList<>(getClientDetailConfig().getClientDetailMap().values());
	}


	@Override
	public String getClientSignatureKey(SecurityOAuthClientDetail clientDetails) {
		String keyFileLocation = clientDetails.getSignatureKeyFileLocation();
		String checksum = clientDetails.getSignatureKeyFileChecksum();
		return JsonWebTokenUtils.getJsonWebTokenSignatureKey(keyFileLocation, checksum);
	}


	@Override
	public String getSenderClientId(String destinationApiUrl) {
		return getClientId(getApplicationUrl(), destinationApiUrl);
	}


	@Override
	public String getRecipientClientId(String senderApiUrl) {
		return getClientId(senderApiUrl, getApplicationUrl());
	}


	@Override
	public String getApplicationUrl() {
		String url = this.applicationUrl;
		if (url.endsWith("/")) {
			url = url.substring(0, url.length() - 1);
		}
		return url;
	}


	private String getClientId(String senderHost, String recipientHost) {
		return FileUtils.replaceInvalidCharacters(StringUtils.removeAll(senderHost + "-" + recipientHost, " "), "_");
	}


	private SecurityOAuthClientDetailConfig getClientDetailConfig() {
		return getSynchronizationHandler().call(SynchronizableBuilder.forLocking("OAUTH-CLIENT-DETAIL", "Reading Client Configuration", getClientConfigurationFile())
				.withMillisecondsToWaitIfBusy(2000)
				.buildWithCallableAction(() -> {
					File configFile = new File(getClientConfigurationFile());
					if (!configFile.exists()) {
						FileUtils.createDirectoryForFile(configFile);
						SecurityOAuthClientDetailConfig config = new SecurityOAuthClientDetailConfig();
						writeClientDetailConfig(config);
						return config;
					}
					else {
						return (SecurityOAuthClientDetailConfig) getJsonHandler().fromJson(FileUtils.readFileToString(configFile), SecurityOAuthClientDetailConfig.class);
					}
				}));
	}


	private void writeClientDetailConfig(SecurityOAuthClientDetailConfig config) {
		if (config != null) {
			//Write the config file out with synchronization
			getSynchronizationHandler().execute(SynchronizableBuilder.forLocking("OAUTH-CLIENT-DETAIL", "Saving Client Configuration", getClientConfigurationFile())
					.withMillisecondsToWaitIfBusy(2000)
					.buildWithExecutableAction(() -> FileUtils.writeStringToFile(getClientConfigurationFile(), getJsonHandler().toJson(config))));
		}
	}


	private void addClientDetailKey(SecurityOAuthClientDetail clientDetail) {
		String clientSignatureKeyFilePath = FileUtils.combinePaths(getSignatureKeyFolder(), clientDetail.getClientId().concat(".key"));
		updateClientDetailKey(clientSignatureKeyFilePath, () -> {
			clientDetail.setSignatureKeyFileLocation(clientSignatureKeyFilePath);
			FileUtils.writeStringToFile(clientSignatureKeyFilePath, String.valueOf(clientDetail.getSignatureKey()));
			clientDetail.setSignatureKey(null);
		});
	}


	private void deleteClientDetailKey(SecurityOAuthClientDetail clientDetail) {
		String keyFileLocation = clientDetail.getSignatureKeyFileLocation();
		updateClientDetailKey(keyFileLocation, () -> {
			if (FileUtils.fileExists(keyFileLocation)) {
				FileUtils.delete(new File(keyFileLocation));
			}
		});
	}


	private void updateClientDetailKey(String lockKey, ExecutableAction action) {
		if (lockKey != null && action != null) {
			getSynchronizationHandler().execute(SynchronizableBuilder.forLocking("OAUTH-CLIENT-DETAIL", "Updating Client Key", lockKey)
					.withMillisecondsToWaitIfBusy(2000)
					.buildWithExecutableAction(action));
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@ValueChangingSetter
	public void setClientDetailsStore(Map<String, SecurityOAuthClientDetail> clientDetailsStore) {
		this.clientDetailsStore.clear();
		if (!CollectionUtils.isEmpty(clientDetailsStore)) {
			this.clientDetailsStore.putAll(clientDetailsStore);
		}
	}


	public Map<String, SecurityOAuthClientDetail> getClientDetailsStore() {
		return this.clientDetailsStore;
	}


	public JsonHandler<JacksonStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JacksonStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public String getClientConfigurationFile() {
		return this.clientConfigurationFile;
	}


	public void setClientConfigurationFile(String clientConfigurationFile) {
		this.clientConfigurationFile = clientConfigurationFile;
	}


	public String getSignatureKeyFolder() {
		return this.signatureKeyFolder;
	}


	public void setSignatureKeyFolder(String signatureKeyFolder) {
		this.signatureKeyFolder = signatureKeyFolder;
	}


	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}


	public SynchronizationHandler getSynchronizationHandler() {
		return this.synchronizationHandler;
	}


	public void setSynchronizationHandler(SynchronizationHandler synchronizationHandler) {
		this.synchronizationHandler = synchronizationHandler;
	}
}
