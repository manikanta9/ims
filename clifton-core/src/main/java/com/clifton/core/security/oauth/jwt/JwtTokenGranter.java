package com.clifton.core.security.oauth.jwt;

import com.clifton.core.security.oauth.SecurityOAuth2GrantTypes;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetail;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetailService;
import com.clifton.core.util.token.JsonWebTokenUtils;
import com.clifton.core.web.token.JsonWebToken;
import com.clifton.core.web.token.JsonWebTokenClaimProperties;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.OAuth2RequestFactory;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.token.AbstractTokenGranter;
import org.springframework.security.oauth2.provider.token.AuthorizationServerTokenServices;


/**
 * Defines the grant type for jwt-bearer tokens
 * <p>
 * Per <a href="https://tools.ietf.org/html/rfc7523#section-2.1">IETF RFC7523</a>
 * <p>
 * Allows a client to exchange a valid authentication token for an access token.
 *
 * @author theodorez
 */
public class JwtTokenGranter extends AbstractTokenGranter {

	private final SecurityOAuthClientDetailService securityOAuthClientDetailService;

	private final UserDetailsService userDetailsService;

	private final OAuth2RequestFactory requestFactory;


	private static final String GRANT_TYPE = SecurityOAuth2GrantTypes.JWT_BEARER.getGrantTypeName();
	private static final String TOKEN_PARAMETER_NAME = "assertion";


	protected JwtTokenGranter(AuthorizationServerTokenServices tokenServices, ClientDetailsService clientDetailsService, UserDetailsService userDetailsService, OAuth2RequestFactory requestFactory) {
		super(tokenServices, clientDetailsService, requestFactory, GRANT_TYPE);
		this.requestFactory = requestFactory;
		this.userDetailsService = userDetailsService;
		this.securityOAuthClientDetailService = (SecurityOAuthClientDetailService) clientDetailsService;
	}


	@Override
	protected OAuth2Authentication getOAuth2Authentication(ClientDetails client, TokenRequest tokenRequest) {
		OAuth2Request storedOAuth2Request = this.requestFactory.createOAuth2Request(client, tokenRequest);
		return new OAuth2Authentication(storedOAuth2Request, getUserAuthenticationFromToken(client, tokenRequest));
	}


	private Authentication getUserAuthenticationFromToken(ClientDetails client, TokenRequest tokenRequest) {
		if (client instanceof SecurityOAuthClientDetail) {
			SecurityOAuthClientDetail securityOAuthClientDetail = (SecurityOAuthClientDetail) client;
			Object tokenParam = tokenRequest.getRequestParameters().get(TOKEN_PARAMETER_NAME);
			if (tokenParam != null) {
				JsonWebToken token = JsonWebTokenUtils.decodeJsonWebTokenWithValidation((String) tokenParam, this.securityOAuthClientDetailService.getClientSignatureKey(securityOAuthClientDetail), securityOAuthClientDetail.getSignatureAlgorithm());
				String username = JsonWebTokenUtils.getClaimAsString(JsonWebTokenClaimProperties.USERNAME.getClaimName(), token);
				return getUserAuthenticationFromUsername(username);
			}
		}
		return null;
	}


	private Authentication getUserAuthenticationFromUsername(String username) {
		if (username != null) {
			UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
			if (userDetails != null) {
				return new UsernamePasswordAuthenticationToken(userDetails.getUsername(), null, userDetails.getAuthorities());
			}
		}
		return null;
	}
}
