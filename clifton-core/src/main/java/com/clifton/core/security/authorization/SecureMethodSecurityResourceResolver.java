package com.clifton.core.security.authorization;


import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>SecureMethodTableResolver</code> interface should be used to provide custom security.
 * It is used when method security is not static (always tied to a specific resource) but can
 * change dynamically based on the argument passed (table name, etc.)
 *
 * @author manderson
 */
public interface SecureMethodSecurityResourceResolver {

	/**
	 * Returns the name of the security resource that user permissions are verified against.
	 *
	 * @param method the method that is invoked (can check name, annotations, etc.)
	 * @param config map of parameter name to values (usually request parameters as opposed to method parameters)
	 */
	public String getSecurityResourceName(Method method, Map<String, ?> config);
}
