package com.clifton.core.security.oauth.client.connection.converter;

import com.clifton.core.security.oauth.SecurityOAuth2GrantTypes;
import com.clifton.core.security.oauth.client.connection.SecurityOAuth2AccessTokenRequest;
import com.clifton.core.security.oauth.client.connection.token.JwtBearerResourceDetails;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.util.StringUtils;

import java.util.Arrays;


/**
 * Converts our object into a Spring Security OAuth Resource definition
 *
 * @author theodorez
 */
public class SecurityOAuth2JwtBearerConverter implements Converter<SecurityOAuth2AccessTokenRequest, OAuth2ProtectedResourceDetails> {

	@Override
	public OAuth2ProtectedResourceDetails convert(SecurityOAuth2AccessTokenRequest from) {
		ValidationUtils.assertTrue(from.getGrantType() == SecurityOAuth2GrantTypes.JWT_BEARER, "Cannot convert to JWT-Bearer grant type.");
		JwtBearerResourceDetails resource = new JwtBearerResourceDetails();
		resource.setScope(Arrays.asList(StringUtils.commaDelimitedListToStringArray(from.getScopes())));
		resource.setAccessTokenUri(from.getTokenUrl());
		resource.setClientId(from.getClientIdentifier());
		resource.setGrantType(SecurityOAuth2GrantTypes.JWT_BEARER.getGrantTypeName());
		resource.setSignatureKeyLocation(from.getSignatureKeyLocation());
		resource.setSignatureKeyChecksum(from.getSignatureKeyChecksum());
		/**
		 * if impersonation is allowed, set the username
		 */
		if (from.isImpersonationAllowed()) {
			resource.setUsername(from.getUsernameToImpersonate());
		}
		return resource;
	}
}
