package com.clifton.core.security.oauth.client.connection.token;

import com.clifton.core.security.oauth.SecurityOAuth2GrantTypes;
import org.springframework.security.oauth2.client.resource.BaseOAuth2ProtectedResourceDetails;


/**
 * Defines additional properties needed to create and issue a JWT access token
 *
 * @author theodorez
 */
public class JwtBearerResourceDetails extends BaseOAuth2ProtectedResourceDetails {

	private String username;

	private String signatureKeyLocation;

	private String signatureKeyChecksum;


	public JwtBearerResourceDetails() {
		super.setGrantType(SecurityOAuth2GrantTypes.JWT_BEARER.getGrantTypeName());
	}


	public String getUsername() {
		return username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getSignatureKeyLocation() {
		return this.signatureKeyLocation;
	}


	public void setSignatureKeyLocation(String signatureKeyLocation) {
		this.signatureKeyLocation = signatureKeyLocation;
	}


	public String getSignatureKeyChecksum() {
		return this.signatureKeyChecksum;
	}


	public void setSignatureKeyChecksum(String signatureKeyChecksum) {
		this.signatureKeyChecksum = signatureKeyChecksum;
	}
}
