package com.clifton.core.security.oauth.client.connection;

import org.springframework.security.oauth2.common.OAuth2AccessToken;


/**
 * @author theodorez
 */
public interface SecurityOAuth2TokenService {

	public OAuth2AccessToken getJwtBearerTokenForSecurityUser(String username, String url);
}
