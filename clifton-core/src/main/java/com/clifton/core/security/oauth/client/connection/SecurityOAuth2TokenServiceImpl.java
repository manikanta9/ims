package com.clifton.core.security.oauth.client.connection;

import com.clifton.core.security.oauth.SecurityOAuth2ClientRegistrationService;
import com.clifton.core.security.oauth.SecurityOAuth2GrantTypes;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetail;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetailService;
import com.clifton.core.util.converter.Converter;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.token.JsonWebTokenClaimProperties;
import org.springframework.security.oauth2.client.DefaultOAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestOperations;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenProviderChain;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.DefaultAccessTokenRequest;
import org.springframework.security.oauth2.common.OAuth2AccessToken;

import java.util.HashMap;
import java.util.Map;


/**
 * Service for getting tokens for a given user
 *
 * @author theodorez
 */
public class SecurityOAuth2TokenServiceImpl implements SecurityOAuth2TokenService {


	private String relativeAuthorizationPath;
	private String relativeTokenPath;

	private Map<String, Converter<SecurityOAuth2AccessTokenRequest, OAuth2ProtectedResourceDetails>> converterMap = new HashMap<>();

	private AccessTokenProviderChain accessTokenProviderChain;

	private SecurityOAuth2ClientRegistrationService securityOAuth2ClientRegistrationService;

	private SecurityOAuthClientDetailService securityOAuthClientDetailService;


	@Override
	public OAuth2AccessToken getJwtBearerTokenForSecurityUser(String username, String apiUrl) {
		SecurityOAuth2AccessTokenRequest connectionConfig = setupSecurityConfig(username, apiUrl, SecurityOAuth2GrantTypes.JWT_BEARER);
		return getAccessToken(getConverterMap().get(SecurityOAuth2GrantTypes.JWT_BEARER.name()), connectionConfig);
	}


	private OAuth2AccessToken getAccessToken(Converter<SecurityOAuth2AccessTokenRequest, OAuth2ProtectedResourceDetails> converter, SecurityOAuth2AccessTokenRequest connectionConfig) {
		return getRestTemplate(converter.convert(connectionConfig)).getAccessToken();
	}


	private OAuth2RestOperations getRestTemplate(OAuth2ProtectedResourceDetails resource) {
		AccessTokenRequest atr = new DefaultAccessTokenRequest();
		OAuth2RestTemplate template = new OAuth2RestTemplate(resource, new DefaultOAuth2ClientContext(atr));
		template.setAccessTokenProvider(getAccessTokenProviderChain());
		return template;
	}


	private SecurityOAuth2AccessTokenRequest setupSecurityConfig(String username, String apiUrl, SecurityOAuth2GrantTypes grantType) {
		String clientId = getSecurityOAuthClientDetailService().getSenderClientId(apiUrl);

		SecurityOAuthClientDetail clientDetail = getSecurityOAuthClientDetailService().getClientDetailsFromFile(clientId);
		ValidationUtils.assertNotNull(clientDetail, "Client details not found for client ID: " + clientId);
		ValidationUtils.assertFalse(clientDetail.getIncomingConnection(), "Cannot request an access token on behalf of an incoming client connection.");

		SecurityOAuth2AccessTokenRequest baseConfig = new SecurityOAuth2AccessTokenRequest();
		baseConfig.setGrantType(grantType);
		baseConfig.setAuthorizationUrl(apiUrl + getRelativeAuthorizationPath());
		baseConfig.setTokenUrl(apiUrl + getRelativeTokenPath());
		baseConfig.setSignatureKeyLocation(clientDetail.getSignatureKeyFileLocation());
		baseConfig.setClientIdentifier(clientId);
		baseConfig.setImpersonationAllowed(true);
		baseConfig.setScopes(JsonWebTokenClaimProperties.IMPERSONATION_USER_SCOPE.getClaimName());
		baseConfig.setUsernameToImpersonate(username);

		return baseConfig;
	}

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public String getRelativeAuthorizationPath() {
		return this.relativeAuthorizationPath;
	}


	public void setRelativeAuthorizationPath(String relativeAuthorizationPath) {
		this.relativeAuthorizationPath = relativeAuthorizationPath;
	}


	public String getRelativeTokenPath() {
		return this.relativeTokenPath;
	}


	public void setRelativeTokenPath(String relativeTokenPath) {
		this.relativeTokenPath = relativeTokenPath;
	}


	public Map<String, Converter<SecurityOAuth2AccessTokenRequest, OAuth2ProtectedResourceDetails>> getConverterMap() {
		return this.converterMap;
	}


	public void setConverterMap(Map<String, Converter<SecurityOAuth2AccessTokenRequest, OAuth2ProtectedResourceDetails>> converterMap) {
		this.converterMap = converterMap;
	}


	public AccessTokenProviderChain getAccessTokenProviderChain() {
		return this.accessTokenProviderChain;
	}


	public void setAccessTokenProviderChain(AccessTokenProviderChain accessTokenProviderChain) {
		this.accessTokenProviderChain = accessTokenProviderChain;
	}


	public SecurityOAuth2ClientRegistrationService getSecurityOAuth2ClientRegistrationService() {
		return this.securityOAuth2ClientRegistrationService;
	}


	public void setSecurityOAuth2ClientRegistrationService(SecurityOAuth2ClientRegistrationService securityOAuth2ClientRegistrationService) {
		this.securityOAuth2ClientRegistrationService = securityOAuth2ClientRegistrationService;
	}


	public SecurityOAuthClientDetailService getSecurityOAuthClientDetailService() {
		return this.securityOAuthClientDetailService;
	}


	public void setSecurityOAuthClientDetailService(SecurityOAuthClientDetailService securityOAuthClientDetailService) {
		this.securityOAuthClientDetailService = securityOAuthClientDetailService;
	}
}
