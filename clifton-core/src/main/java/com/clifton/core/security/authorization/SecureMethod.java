package com.clifton.core.security.authorization;


import com.clifton.core.beans.IdentityObject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>SecureMethod</code> annotation should be used to mark methods that must be secured.
 * It can explicitly identify permission(s) that current user needs in order to be able to execute this method.
 * Permissions are checked against a security resource which can either be defined explicitly using
 * <code>securityResource</code> property or by <code>table</code> property which identifies a table that can
 * be linked to a security resource or by a <code>dtoClass</code> property which identifies a DTO class that
 * is tied to a table which can be tied to a security resource.
 *
 * @author vgomelsky
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
@Inherited
public @interface SecureMethod {

	public static final String DEFAULT_STRING = "DEFAULT_STRING";

	/**
	 * When a URL being secured doesn't have a corresponding table or you require additional security restricted to power users,
	 * consider using this security resource name that must also be created in the application.
	 */
	public static final String RESOURCE_SYSTEM_MANAGEMENT = "System Management";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Setting this property to true, disables security check for this method and ignores all other settings.
	 */
	boolean disableSecurity() default false;


	/**
	 * Setting this property to true, will require the user to be Administrator.
	 */
	boolean adminSecurity() default false;


	/**
	 * Security permission(s) required to execute this method.  Use {@link SecurityPermission} permission and mask constants.
	 */
	int permissions() default 0;


	/**
	 * The name of context managed bean that implements SecureMethodPermissionResolver interface
	 * and returns corresponding permission level that can change based on method arguments.
	 * Used rarely, and utilizing default permissions based on the method or (permissions) attribute is sufficient
	 * Current Use Case: Workflow Transitions usually require write access to the table of the table that is being transitioned.  However, some transitions can allow READ access.
	 * <p>
	 */
	String permissionResolverBeanName() default DEFAULT_STRING;


	/**
	 * The name of security resource that permissions are checked against.
	 */
	String securityResource() default DEFAULT_STRING;


	/**
	 * The name of the table used to retrieve corresponding security resource.
	 * If securityResource property is defined, this property is ignored.
	 */
	String table() default DEFAULT_STRING;


	/**
	 * The name of context managed bean that implements SecureMethodTableResolver interface
	 * and returns corresponding table name that can change based on method arguments.
	 * <p>
	 * If 'securityResourceResolverBeanName' is defined and returns a valid security resource,
	 * then it takes precedence 'tableResolverBeanName'
	 */
	String tableResolverBeanName() default DEFAULT_STRING;


	/**
	 * The name of context managed bean that implements SecureMethodTableResolver interface
	 * and returns corresponding security resource name that can change based on method arguments.
	 */
	String securityResourceResolverBeanName() default DEFAULT_STRING;


	/**
	 * The DTO Class that manages the table used to retrieve corresponding security resource.
	 * If securityResource or table property is defined, this property is ignored.
	 * Defaults to IdentityObject class.
	 */
	Class<? extends IdentityObject> dtoClass() default IdentityObject.class;


	/**
	 * Set to true if this method name violates our naming convention: cannot lookup table name based on
	 * method signature. This is used in tests to make sure we don't have unnecessary annotations.
	 */
	boolean namingConventionViolation() default false;


	/**
	 * Can be used in cases where the name of the table is passed as a url parameter.  This should be used sparingly and only when the table name passed
	 * is also used to limit results or for the functionality of the method (i.e. not for security only).
	 */
	String dynamicTableNameUrlParameter() default DEFAULT_STRING;


	/**
	 * The name of the bean property that contains the table name to use for security.  This is used for "core" functionality components
	 * that are assigned to tables and the security is driven from the table name.
	 * Example Rule Assignments security is driven through the table name in : ruleDefinition.ruleCategory.categoryTable.name bean property
	 * If id property is present (for updates and deletes), we use DAO Locator to get the RuleAssignment from the database, and pull the table name from the defined property.
	 * If id property is NOT present (for inserts), we go one level deep, and locate that bean, i.e. get URL property for ruleDefinition.id and pull the table name from the defined property (after removing that level) i.e. ruleCategory.categoryTable.name bean property
	 * At this time, we support only going those 2 levels - there should not be any cases where that isn't sufficient.
	 */
	String dynamicTableNameBeanPath() default DEFAULT_STRING;


	/**
	 * The name of the bean property that contains the security resource name to use for security.  This can be used to override dynamic table name bean path if specified.
	 * Example: Custom Columns are associated with a Column Group where a Security Resource can be defined.  If not defined, then uses table name property
	 * <p>
	 * If id property is present (for updates and deletes), we use DAO Locator to get the entity from the database, and pull the security resource from the defined property.
	 * If id property is NOT present (for inserts), we go one level deep, and locate that bean, i.e. get URL property for columnGroup.id and pull the resource name from the defined property (after removing that level) i.e. columnGroup.securityResource.name bean property
	 * At this time, we support only going those 2 levels - there should not be any cases where that isn't sufficient.
	 */
	String dynamicSecurityResourceBeanPath() default DEFAULT_STRING;


	/**
	 * When using dynamicTableNameBeanPath, most cases, the "id" of the bean is just id.  However, in some cases, like link methods
	 * (i.e. saveSystemNoteLink, we pass noteId and fkFieldId, so we need to use noteId as the url parameter to lookup the note)
	 * <p>
	 * Used only if dynamicTableNameBeanPath is provided
	 */
	String dynamicIdUrlParameter() default "id";


	/**
	 * In some cases, like SystemNotes, users may not have access to the note table, i.e. InvestmentAccount, but they do have access to enter notes.  For these cases, where security
	 * is split, we've added a second resource that is the tableName + Note suffix.  If table name + suffix does not exist, then just the table name is used to determine security.
	 * <p>
	 * Used only if dynamicTableNameBeanPath is provided
	 */
	String dynamicSecurityResourceTableNameSuffix() default DEFAULT_STRING;


	/**
	 * The name of context managed bean that implements the {@link SecureMethodSecurityEvaluator} interface
	 * and performs custom security evaluation.
	 */
	String securityEvaluatorBeanName() default DEFAULT_STRING;
}
