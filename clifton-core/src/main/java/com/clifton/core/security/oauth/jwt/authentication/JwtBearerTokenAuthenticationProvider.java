package com.clifton.core.security.oauth.jwt.authentication;


import com.clifton.core.security.oauth.client.SecurityOAuthClientDetail;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetailService;
import com.clifton.core.util.oauth.OAuthClientDetailsUtils;
import com.clifton.core.util.token.JsonWebTokenUtils;
import com.clifton.core.web.token.JsonWebToken;
import com.clifton.core.web.token.JsonWebTokenClaimProperties;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.common.exceptions.OAuth2Exception;
import org.springframework.security.oauth2.provider.ClientDetails;


/**
 * This provider supports the use of JSON Web Tokens for authenticating requests.
 *
 * @author theodorez
 */
public class JwtBearerTokenAuthenticationProvider implements AuthenticationProvider {

	private UserDetailsService userDetailsService;

	private SecurityOAuthClientDetailService securityOAuthClientDetailService;

	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////


	@Override
	public Authentication authenticate(Authentication authentication) {
		if (authentication instanceof JwtAuthenticationToken) {
			JwtAuthenticationToken authToken = (JwtAuthenticationToken) authentication;

			try {
				JsonWebToken token = JsonWebTokenUtils.decodeJsonWebTokenNoValidation(authToken.getToken());

				//Get the clientID out of the token so we can look up the authentication signature key for the client
				String clientId = JsonWebTokenUtils.getClaimAsString(JsonWebTokenClaimProperties.CLIENT_ID.getClaimName(), token);

				ClientDetails clientDetails = getOAuthClientDetailsService().loadClientByClientId(clientId);
				if (clientDetails instanceof SecurityOAuthClientDetail) {
					SecurityOAuthClientDetail securityOAuthClientDetail = (SecurityOAuthClientDetail) clientDetails;
					token.setSignatureKey(getOAuthClientDetailsService().getClientSignatureKey(securityOAuthClientDetail));
					token.setAlgorithmName(securityOAuthClientDetail.getSignatureAlgorithm());
				}
				JsonWebTokenUtils.verifyJsonWebToken(token);

				OAuthClientDetailsUtils.validateClientScopes(clientDetails, token);
				OAuthClientDetailsUtils.validateClientImpersonationScope(clientDetails, token);

				String username = JsonWebTokenUtils.getClaimAsString(JsonWebTokenClaimProperties.USERNAME.getClaimName(), token);
				UserDetails userDetails = getUserDetailsService().loadUserByUsername(username);
				return new JwtAuthenticationToken(OAuthClientDetailsUtils.getGrantedAuthoritiesList(JsonWebTokenUtils.getClaimAsStringList(JsonWebTokenClaimProperties.AUTHORITIES.getClaimName(), token)), userDetails.getUsername(), authToken.getToken(), clientId);
			}
			catch (UsernameNotFoundException unfe) {
				throw new BadCredentialsException("Username is invalid", unfe);
			}
			catch (OAuth2Exception e) {
				throw new InsufficientAuthenticationException("Error occurred when authenticating with JWT.", e);
			}
		}
		return null;
	}


	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.isAssignableFrom(JwtAuthenticationToken.class);
	}


	//////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////


	public UserDetailsService getUserDetailsService() {
		return this.userDetailsService;
	}


	public void setUserDetailsService(UserDetailsService userDetailsService) {
		this.userDetailsService = userDetailsService;
	}


	public SecurityOAuthClientDetailService getOAuthClientDetailsService() {
		return this.securityOAuthClientDetailService;
	}


	public void setOAuthClientDetailsService(SecurityOAuthClientDetailService securityOAuthClientDetailService) {
		this.securityOAuthClientDetailService = securityOAuthClientDetailService;
	}
}

