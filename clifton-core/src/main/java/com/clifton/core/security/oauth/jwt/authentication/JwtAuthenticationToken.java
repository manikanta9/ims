package com.clifton.core.security.oauth.jwt.authentication;

import org.springframework.security.authentication.AbstractAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.AuthorityUtils;

import java.util.Collection;


/**
 * Defines and authentication token to be used with JWTs
 *
 * @author theodorez
 */
public class JwtAuthenticationToken extends AbstractAuthenticationToken {

	private final String token;
	private String username;
	private String clientId;


	public JwtAuthenticationToken(String token) {
		super(AuthorityUtils.NO_AUTHORITIES);
		this.token = token;
	}


	public JwtAuthenticationToken(Collection<? extends GrantedAuthority> authorities, String username, String token, String clientId) {
		super(authorities);
		this.username = username;
		this.token = token;
		this.clientId = clientId;
		setAuthenticated(true);
	}


	@Override
	public Object getCredentials() {
		return getToken();
	}


	@Override
	public Object getPrincipal() {
		return getClientId() != null ? getClientId() : getUsername();
	}


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof JwtAuthenticationToken)) {
			return false;
		}

		JwtAuthenticationToken test = (JwtAuthenticationToken) obj;

		if (getUsername() != null && !getUsername().equals(test.getUsername())) {
			return false;
		}

		if (getClientId() != null && !getClientId().equals(test.getClientId())) {
			return false;
		}

		if (getToken() != null && !getUsername().equals(test.getToken())) {
			return false;
		}


		return super.equals(obj);
	}


	@Override
	public int hashCode() {
		int code = super.hashCode();

		if (getUsername() != null) {
			code ^= this.getUsername().hashCode();
		}

		if (getClientId() != null) {
			code ^= this.getClientId().hashCode();
		}

		if (getToken() != null) {
			code ^= this.getToken().hashCode();
		}

		return code;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getToken() {
		return this.token;
	}


	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public String getClientId() {
		return this.clientId;
	}


	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
}
