package com.clifton.core.security.password.passay;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.security.password.SecurityPasswordValidator;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.passay.CharacterCharacteristicsRule;
import org.passay.CharacterRule;
import org.passay.CharacterSequence;
import org.passay.DictionarySubstringRule;
import org.passay.EnglishCharacterData;
import org.passay.EnglishSequenceData;
import org.passay.IllegalRegexRule;
import org.passay.IllegalSequenceRule;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordGenerator;
import org.passay.PasswordValidator;
import org.passay.PropertiesMessageResolver;
import org.passay.RepeatCharacterRegexRule;
import org.passay.Rule;
import org.passay.RuleResult;
import org.passay.RuleResultDetail;
import org.passay.SequenceData;
import org.passay.UsernameRule;
import org.passay.WhitespaceRule;
import org.passay.dictionary.ArrayWordList;
import org.passay.dictionary.WordListDictionary;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;


/**
 * The <code>PassayPasswordValidator</code>
 *
 * @author manderson
 */
public class PassayPasswordValidator implements SecurityPasswordValidator {


	private static final String USERNAME_SEQUENCE_ERROR_CODE = "ILLEGAL_USERNAME_SEQUENCE";


	@Override
	public void validatePassword(String username, String newPassword) {
		PasswordValidator validator = new PasswordValidator(getPasswordRules(username));
		PasswordData passwordData = new PasswordData(username, newPassword);
		RuleResult result = validator.validate(passwordData);
		validateResult(result);
	}


	@Override
	public String generateRandomPassword() {
		// For generating random passwords, only using Upper, Lower, and Digits
		// When using special characters, there are some really random ones like degree symbol that isn't easily available from keyboards
		// and would be frustrating for users.  Random passwords do not have to be fully validated against all the rules
		// and users are required to reset their password after successfully logging in.
		PasswordGenerator generator = new PasswordGenerator();
		return generator.generatePassword(10, Arrays.asList(
				new CharacterRule(PasswordGeneratorCharacterData.UPPER_CASE, 1),
				new CharacterRule(PasswordGeneratorCharacterData.LOWER_CASE, 1),
				new CharacterRule(PasswordGeneratorCharacterData.DIGIT, 1)
		));
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////                  Rules for Validation                  ////////////
	////////////////////////////////////////////////////////////////////////////////


	private List<Rule> getPasswordRules(String username) {
		// See: https://wiki.paraport.com/display/IT/Parametric+IT+Policies for Guidelines
		return Arrays.asList(
				// Passwords must be at least 8 characters long (cap at 30???)
				new LengthRule(8, 30),
				getCharacterCharacteristicsRule(),
				// Does not contain all or part of the user's account name.
				// Note: Checks All forward and backward
				new UsernameRule(true, true),
				// Also checks the username for partial sequences
				getUserNameSequenceRule(username),
				new IllegalSequenceRule(EnglishSequenceData.Alphabetical, 3, false),
				new IllegalSequenceRule(EnglishSequenceData.Numerical, 3, false),
				new IllegalSequenceRule(EnglishSequenceData.USQwerty, 3, false),
				new WhitespaceRule(),
				new DictionarySubstringRule(new WordListDictionary(new ArrayWordList(new String[]{"password"}, false))),
				// Cannot repeat the same character more than 3 times
				new RepeatCharacterRegexRule(4));
	}


	private CharacterCharacteristicsRule getCharacterCharacteristicsRule() {
		// Contains elements from three of the following four categories:
		// English uppercase characters (A through Z).
		// English lowercase characters (a through z).
		// Numeric characters (0 through 9).
		// Non-alphanumeric (for example, !, $, #, %). extended ASCII, symbolic, or linguistic characters.
		CharacterCharacteristicsRule characterCharacteristicsRule = new CharacterCharacteristicsRule();
		characterCharacteristicsRule.setNumberOfCharacteristics(3);
		characterCharacteristicsRule.setRules(Arrays.asList(
				new CharacterRule(EnglishCharacterData.UpperCase, 1),
				new CharacterRule(EnglishCharacterData.LowerCase, 1),
				new CharacterRule(EnglishCharacterData.Digit, 1),
				new CharacterRule(EnglishCharacterData.Special, 1)
		));
		return characterCharacteristicsRule;
	}


	private Rule getUserNameSequenceRule(String username) {
		CharacterSequence userNameSequence = new CharacterSequence(username);
		SequenceData userNameSequenceData = new SequenceData() {
			@Override
			public CharacterSequence[] getSequences() {
				return new CharacterSequence[]{userNameSequence};
			}


			@Override
			public String getErrorCode() {
				return USERNAME_SEQUENCE_ERROR_CODE;
			}
		};
		return new IllegalSequenceRule(userNameSequenceData, 4, false);
	}


	////////////////////////////////////////////////////////////////////////////////
	///////           Rule Result Parsing for User Friendly Messages         ///////
	////////////////////////////////////////////////////////////////////////////////


	private void validateResult(RuleResult result) {
		if (!result.isValid()) {
			List<String> messageList = new ArrayList<>();
			// Note: The Message Resolver looks at each result detail individually, but does not take into account the other details
			// So, we use the message resolver along with reviewing each detail as a part of the entire list of failures so we can give the user a friendlier message
			PropertiesMessageResolver messageResolver = new PropertiesMessageResolver();
			Map<String, List<RuleResultDetail>> errorCodeDetailListMap = BeanUtils.getBeansMap(result.getDetails(), RuleResultDetail::getErrorCode);
			for (String errorCode : errorCodeDetailListMap.keySet()) {
				appendErrorMessageForErrorCode(errorCode, errorCodeDetailListMap, messageResolver, messageList);
			}
			throw new ValidationException(StringUtils.collectionToCommaDelimitedString(messageList));
		}
	}


	protected void appendErrorMessageForErrorCode(String errorCode, Map<String, List<RuleResultDetail>> errorCodeDetailListMap, PropertiesMessageResolver messageResolver, List<String> messageList) {
		// Merge the Insufficient Characteristics Message with specific insufficient character messages
		if (isInsufficientCharacteristicsRuleError(errorCode)) {
			StringBuilder messageSb = new StringBuilder();
			String ruleResult = messageResolver.resolve(errorCodeDetailListMap.get(errorCode).get(0));
			messageSb.append(StringUtils.substringBeforeLast(ruleResult, "."));
			messageSb.append(". Missing: [");
			List<String> innerMessages = new ArrayList<>();
			for (Map.Entry<String, List<RuleResultDetail>> stringListEntry : errorCodeDetailListMap.entrySet()) {
				if (isInsufficientCharacterRuleError(stringListEntry.getKey())) {
					ruleResult = messageResolver.resolve(stringListEntry.getValue().get(0));
					innerMessages.add(StringUtils.substringBeforeLast(ruleResult, "."));
				}
			}
			String innerMessage = StringUtils.join(innerMessages, ", and/or ");
			messageSb.append(innerMessage).append("].");
			messageList.add(messageSb.toString());
		}
		else if (isInsufficientCharacterRuleError(errorCode)) {
			// DO NOTHING - HANDLED ABOVE
		}
		else if (isInvalidSequenceRuleError(errorCode)) {
			if (StringUtils.isEqual(errorCode, USERNAME_SEQUENCE_ERROR_CODE) && (errorCodeDetailListMap.containsKey(UsernameRule.ERROR_CODE) || errorCodeDetailListMap.containsKey(UsernameRule.ERROR_CODE_REVERSED))) {
				// DO NOTHING - DON'T GIVE USER ERROR MESSAGE ABOUT USERNAME SEQUENCE IF ALSO FAILED EXPLICIT USER NAME VIOLATION
			}
			else {
				String message = "Password contains the illegal " + getSequenceTypeLabel(errorCode) + " sequence" + (CollectionUtils.getSize(errorCodeDetailListMap.get(errorCode)) > 1 ? "s: " : ": ");
				message += StringUtils.collectionToCommaDelimitedString(errorCodeDetailListMap.get(errorCode), detail -> detail.getParameters().get("sequence"));
				message += ".";
				messageList.add(message);
			}
		}
		// Note: This is overridden to give a friendlier message
		else if (isIllegalDictionaryRuleError(errorCode)) {
			String message = "Password contains the illegal word" + (CollectionUtils.getSize(errorCodeDetailListMap.get(errorCode)) > 1 ? "s: " : ": ");
			message += StringUtils.collectionToCommaDelimitedString(errorCodeDetailListMap.get(errorCode), detail -> detail.getParameters().get("matchingWord"));
			message += ".";
			messageList.add(message);
		}
		// Note: If we ever add another regex rule, this won't work
		else if (StringUtils.isEqual(IllegalRegexRule.ERROR_CODE, errorCode)) {
			String message = "Password cannot contain the same character more than 3 times in succession.  Found the following violation" + (CollectionUtils.getSize(errorCodeDetailListMap.get(errorCode)) > 1 ? "s: " : ": ");
			message += StringUtils.collectionToCommaDelimitedString(errorCodeDetailListMap.get(errorCode), detail -> detail.getParameters().get("match"));
			message += ".";
			messageList.add(message);
		}
		else {
			for (RuleResultDetail detail : errorCodeDetailListMap.get(errorCode)) {
				messageList.add(messageResolver.resolve(detail));
			}
		}
	}


	private boolean isInsufficientCharacteristicsRuleError(String errorCode) {
		return StringUtils.isEqual(errorCode, CharacterCharacteristicsRule.ERROR_CODE);
	}


	private boolean isIllegalDictionaryRuleError(String errorCode) {
		return StringUtils.isEqual(errorCode, DictionarySubstringRule.ERROR_CODE);
	}


	private boolean isInsufficientCharacterRuleError(String errorCode) {
		for (EnglishCharacterData characterData : EnglishCharacterData.values()) {
			if (StringUtils.isEqual(errorCode, characterData.getErrorCode())) {
				return true;
			}
		}
		return false;
	}


	private boolean isInvalidSequenceRuleError(String errorCode) {
		if (StringUtils.isEqual(errorCode, USERNAME_SEQUENCE_ERROR_CODE)) {
			return true;
		}
		for (EnglishSequenceData sequenceData : EnglishSequenceData.values()) {
			if (StringUtils.isEqual(errorCode, sequenceData.getErrorCode())) {
				return true;
			}
		}
		return false;
	}


	private String getSequenceTypeLabel(String errorCode) {
		if (StringUtils.isEqual(errorCode, USERNAME_SEQUENCE_ERROR_CODE)) {
			return "username";
		}
		if (StringUtils.isEqual(errorCode, EnglishSequenceData.USQwerty.getErrorCode())) {
			return "QWERTY";
		}
		for (EnglishSequenceData sequenceData : EnglishSequenceData.values()) {
			if (StringUtils.isEqual(errorCode, sequenceData.getErrorCode())) {
				return sequenceData.name().toLowerCase();
			}
		}
		return null;
	}
}
