package com.clifton.core.security.authorization;


import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * The <code>SecureClass</code> annotation is used to indicate that all methods in the class exposed
 * via urls require admin security.
 * <p/>
 * Not actually used for security check, but tests use it to verify admin access only
 *
 * @author manderson
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
@Inherited
public @interface SecureClass {

	// NOTHING HERE FOR NOW
}
