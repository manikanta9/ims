package com.clifton.core.security.authorization;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * @author manderson
 */
public interface SecureMethodPermissionResolver {


	/**
	 * Returns the SecurityPermission level required for this method
	 * Used for cases where the level can change based on the method arguments
	 * <p>
	 * Example: Workflow Transition usually required WRITE access, but some Transitions require READ access Only
	 *
	 * @param method               the method that is invoked (can check name, annotations, etc.)
	 * @param config               map of parameter name to values (usually request parameters as opposed to method parameters)
	 * @param securityResourceName the security resource name of what we are getting permissions for. This can be useful for dynamic security when the permission level maybe different.  i.e. System Notes - if using TradeNote then we use CREATE / WRITE / DELETE, but if using Trade (for notes) then we'd use WRITE only
	 */
	public int getRequiredPermissions(Method method, Map<String, ?> config, String securityResourceName);
}
