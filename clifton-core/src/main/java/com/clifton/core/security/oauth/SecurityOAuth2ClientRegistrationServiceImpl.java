package com.clifton.core.security.oauth;

import com.clifton.core.converter.json.JsonHandler;
import com.clifton.core.converter.json.jackson.strategy.JacksonStrategy;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetail;
import com.clifton.core.security.oauth.client.SecurityOAuthClientDetailService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.encryption.EncryptionUtils;
import com.clifton.core.util.encryption.EncryptionUtilsAES;
import com.clifton.core.util.http.BadStatusException;
import com.clifton.core.util.http.CustomURIBuilder;
import com.clifton.core.util.http.HttpEntityFactory;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.core.util.http.RedirectStatusException;
import com.clifton.core.util.http.SimpleHttpClientFactory;
import com.clifton.core.util.http.SimpleHttpResponse;
import com.clifton.core.util.http.cookie.ImsCookieStoreFactory;
import com.clifton.core.util.token.JsonWebTokenUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.web.api.client.locator.ExternalServiceLocatorUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.protocol.HttpClientContext;
import org.apache.http.util.EntityUtils;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Controller;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


/**
 * Service that handles requests from external services to register themselves as clients.
 * <p>
 * Also handles issuing requests to register the app as a client
 *
 * @author theodorez
 */
@Controller
public class SecurityOAuth2ClientRegistrationServiceImpl implements SecurityOAuth2ClientRegistrationService {

	private int authenticationTokenValiditySeconds;

	private JsonHandler<JacksonStrategy> jsonHandler;

	private SecurityOAuthClientDetailService securityOAuthClientDetailService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Collection<String> getResourceRegistrationList() {
		return ExternalServiceLocatorUtils.getExternalServiceURLs();
	}


	@Override
	//TODO - remove ResponseBody or handle UI differently...maybe both.
	public boolean registerOAuth2ClientWithResource(String resourceAuthorizationUrl, String adminUserName, String adminPassword, String resourceRegistrationUrl, String apiHostname, String signatureAlgorithm, String scope, String grantType) {
		HttpClientContext context = login(resourceAuthorizationUrl, adminUserName, adminPassword);
		Map<String, String> params = new HashMap<>();
		params.put("clientUrl", getSecurityOAuthClientDetailService().getApplicationUrl());
		params.put("signatureAlgorithm", signatureAlgorithm);
		params.put("scope", scope);
		params.put("grantType", grantType);
		try {
			HttpPost post = new HttpPost(new CustomURIBuilder(resourceRegistrationUrl).build());
			post.setEntity(HttpEntityFactory.createHttpEntity(params));
			post.setHeader("X-XSRF-TOKEN", getCsrfToken(context));
			SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(post, context);

			//Response should be a SecurityOAuthClientDetail serialized to JSON
			String jsonResponse = EntityUtils.toString(response.getEntity(), "UTF-8");
			HttpUtils.closeResponse(response);

			SecurityOAuthClientDetail clientDetail = (SecurityOAuthClientDetail) getJsonHandler().fromJson(jsonResponse, SecurityOAuthClientDetail.class);
			ValidationUtils.assertNotNull(clientDetail.getClientId(), "ClientID was not returned. Response was: " + clientDetail.getAdditionalInformation());
			clientDetail.setIncomingConnection(Boolean.FALSE);
			getSecurityOAuthClientDetailService().saveClientDetailToFiles(clientDetail);
			return true;
		}
		catch (Exception e) {
			Map<String, String> parameterMap = new HashMap<>();
			parameterMap.put("resourceAuthorizationUrl", resourceAuthorizationUrl);
			parameterMap.put("adminUserName", adminUserName);
			parameterMap.put("adminPassword", "Is empty = " + StringUtils.isEmpty(adminPassword));
			parameterMap.put("resourceRegistrationUrl", resourceRegistrationUrl);
			parameterMap.put("apiHostname", apiHostname);
			parameterMap.put("signatureAlgorithm", signatureAlgorithm);
			parameterMap.put("scope", scope);
			parameterMap.put("grantType", grantType);
			throw new RuntimeException("Registration failed with following parameters:" + getJsonHandler().toJson(parameterMap), e);
		}
	}


	@Override
	public String registerOAuth2Client(String clientUrl, String signatureAlgorithm, String scope, String grantType) {
		ValidationUtils.assertNotNull(JsonWebTokenUtils.getAlgorithmClass(signatureAlgorithm), "Unsupported algorithm: " + signatureAlgorithm);
		String clientId = getSecurityOAuthClientDetailService().getRecipientClientId(clientUrl);

		SecurityOAuthClientDetail clientDetail = new SecurityOAuthClientDetail();
		clientDetail.setSignatureAlgorithm(signatureAlgorithm);
		clientDetail.setAccessTokenValiditySeconds(getAuthenticationTokenValiditySeconds());
		clientDetail.setAuthorizedGrantTypes(CollectionUtils.createList(grantType));
		clientDetail.setClientId(clientId);
		clientDetail.setScope(CollectionUtils.createList(scope));
		clientDetail.setAuthorities(CollectionUtils.createList(new SimpleGrantedAuthority("IS_AUTHENTICATED_FULLY")));
		clientDetail.addAdditionalInformation("clientUrl", clientUrl);
		clientDetail.addAdditionalInformation("resourceUrl", getSecurityOAuthClientDetailService().getApplicationUrl());

		String signatureKey = EncryptionUtils.byteArrayToHexString(EncryptionUtilsAES.generateKey());
		clientDetail.setSignatureKey(signatureKey);
		clientDetail.setSignatureKeyFileChecksum(EncryptionUtils.byteArrayToHexString(EncryptionUtilsAES.hashBytes(signatureKey.getBytes())));
		clientDetail.setIncomingConnection(Boolean.TRUE);
		getSecurityOAuthClientDetailService().saveClientDetailToFiles(clientDetail);
		//need to set the signature key again because it gets wiped in the save method
		clientDetail.setSignatureKey(signatureKey);
		return getJsonHandler().toJson(clientDetail);
	}


	private HttpClientContext login(String authorizationUrl, String username, String password) {
		HttpClientContext clientContext = new HttpClientContext();
		clientContext.setCookieStore(ImsCookieStoreFactory.newRamCookieStore());

		Map<String, String> params = new HashMap<>();
		params.put("j_username", username);
		params.put("j_password", password);

		try (SimpleHttpResponse ignored = SimpleHttpClientFactory.getHttpClient().post(new CustomURIBuilder(authorizationUrl).build(), HttpEntityFactory.createHttpEntity(params), clientContext)) {
			return clientContext;
		}
		catch (Exception e) {
			if ((e.getCause() instanceof BadStatusException)) {
				BadStatusException statusException = (BadStatusException) e.getCause();
				ValidationUtils.assertEquals(statusException.getStatusCode(), 403, "Status exception does not contain code 403.");
			}
			else if ((e.getCause() instanceof RedirectStatusException)) {
				RedirectStatusException statusException = (RedirectStatusException) e.getCause();
				ValidationUtils.assertEquals(statusException.getStatusCode(), 302, "Status exception does not contain code 302.");
			}
			else {
				throw new RuntimeException("Exception occurred while getting CSRF token.", e);
			}
		}

		try {
			HttpPost post = new HttpPost(new CustomURIBuilder(authorizationUrl).build());
			post.setEntity(HttpEntityFactory.createHttpEntity(params));
			post.setHeader("X-XSRF-TOKEN", getCsrfToken(clientContext));
			SimpleHttpResponse response = SimpleHttpClientFactory.getHttpClient().execute(post, clientContext);
			HttpUtils.closeResponse(response);
		}
		catch (Exception e) {
			//Silently catch exception because a redirect is expected here and we don't need to do anything with it.
			if (!(e.getCause() instanceof RedirectStatusException)) {
				throw new RuntimeException("Exception occurred while getting the JSESSIONID.", e);
			}
		}
		return clientContext;
	}


	private String getCsrfToken(HttpClientContext clientContext) {
		for (org.apache.http.cookie.Cookie cookie : clientContext.getCookieStore().getCookies()) {
			if (StringUtils.isEqual("XSRF-TOKEN", cookie.getName())) {
				return cookie.getValue();
			}
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public int getAuthenticationTokenValiditySeconds() {
		return this.authenticationTokenValiditySeconds;
	}


	public void setAuthenticationTokenValiditySeconds(int authenticationTokenValiditySeconds) {
		this.authenticationTokenValiditySeconds = authenticationTokenValiditySeconds;
	}


	public JsonHandler<JacksonStrategy> getJsonHandler() {
		return this.jsonHandler;
	}


	public void setJsonHandler(JsonHandler<JacksonStrategy> jsonHandler) {
		this.jsonHandler = jsonHandler;
	}


	public SecurityOAuthClientDetailService getSecurityOAuthClientDetailService() {
		return this.securityOAuthClientDetailService;
	}


	public void setSecurityOAuthClientDetailService(SecurityOAuthClientDetailService securityOAuthClientDetailService) {
		this.securityOAuthClientDetailService = securityOAuthClientDetailService;
	}
}
