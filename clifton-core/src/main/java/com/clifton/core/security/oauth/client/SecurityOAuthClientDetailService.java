package com.clifton.core.security.oauth.client;

import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;


/**
 * Adds support for getting the signature key for a client to support
 * token based authentication.
 *
 * @author theodorez
 */
public interface SecurityOAuthClientDetailService extends org.springframework.security.oauth2.provider.ClientDetailsService {

	@Override //Overridden simply to apply DoNotAddRequestMapping annotation
	@DoNotAddRequestMapping
	public ClientDetails loadClientByClientId(String clientId);


	@RequestMapping("securityOauthClientDetail")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public SecurityOAuthClientDetail getClientDetailsFromFile(String id);


	@DoNotAddRequestMapping
	public SecurityOAuthClientDetail saveClientDetailToFiles(SecurityOAuthClientDetail clientDetail);


	@RequestMapping(value = "securityOauthClientDetailDelete", method = RequestMethod.POST)
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public SecurityOAuthClientDetail deleteClientDetailFromFiles(String id);


	@RequestMapping("securityOauthClientDetailList")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public List<SecurityOAuthClientDetail> getClientDetailsList();


	@DoNotAddRequestMapping
	public String getClientSignatureKey(SecurityOAuthClientDetail clientDetails);


	@DoNotAddRequestMapping
	public String getSenderClientId(String destinationApiUrl);


	@DoNotAddRequestMapping
	public String getRecipientClientId(String senderApiUrl);


	@DoNotAddRequestMapping
	public String getApplicationUrl();
}
