package com.clifton.core.security.authorization;


import com.clifton.core.beans.hierarchy.NamedHierarchicalEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;

import java.security.SecurityPermission;


/**
 * The <code>SecurityResource</code> class represents security resources (elements that can be secured).
 * See {@link SecurityPermission} for bit definitions of allowedPermissionMask field.
 *
 * @author vgomelsky
 */
@CacheByName
public class SecurityResource extends NamedHierarchicalEntity<SecurityResource, Integer> {

	private int allowedPermissionMask;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public SecurityPermissionMask getAllowedSecurityPermissionMask() {
		return SecurityPermissionMask.getSecurityPermissionMask(this.allowedPermissionMask);
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public int getAllowedPermissionMask() {
		return this.allowedPermissionMask;
	}


	public void setAllowedPermissionMask(int allowedPermissionMask) {
		this.allowedPermissionMask = allowedPermissionMask;
	}
}
