package com.clifton.core.security.oauth.jwt.authentication;

import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.common.exceptions.BadClientCredentialsException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.RequestMatcher;
import org.springframework.web.HttpRequestMethodNotSupportedException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


/**
 * Authentication filter to support the use of JSON Web Tokens for authentication
 * <p>
 * Per <a href="https://tools.ietf.org/html/rfc7523#section-2.1">IETF RFC7523</a>
 *
 * @author theodorez
 */
public class JwtBearerTokenEndpointFilter extends AbstractAuthenticationProcessingFilter {

	private boolean allowOnlyPost = false;

	private AuthenticationEntryPoint authenticationEntryPoint;

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	public JwtBearerTokenEndpointFilter(String path) {
		super(path);
		setRequiresAuthenticationRequestMatcher(new JwtCredentialsRequestMatcher(path));
	}

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		if (isAllowOnlyPost() && !"POST".equalsIgnoreCase(request.getMethod())) {
			throw new HttpRequestMethodNotSupportedException(request.getMethod(), new String[]{"POST"});
		}

		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		if (authentication != null && authentication.isAuthenticated()) {
			return authentication;
		}

		String token = request.getParameter("assertion");

		if (token == null) {
			throw new BadCredentialsException("No jwt assertion presented for authentication.");
		}

		return this.getAuthenticationManager().authenticate(new JwtAuthenticationToken(token));
	}


	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response,
	                                        FilterChain chain, Authentication authResult) throws IOException, ServletException {
		super.successfulAuthentication(request, response, chain, authResult);
		chain.doFilter(request, response);
	}


	@Override
	public void afterPropertiesSet() {
		super.afterPropertiesSet();
		setAuthenticationFailureHandler((request, response, exception) -> {
			if (exception instanceof BadCredentialsException) {
				exception = new BadCredentialsException(exception.getMessage(), new BadClientCredentialsException());
			}
			getAuthenticationEntryPoint().commence(request, response, exception);
		});
		setAuthenticationSuccessHandler((request, response, authentication) -> {
			// no-op - just allow filter chain to continue to token endpoint
		});
	}

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////

	protected static class JwtCredentialsRequestMatcher implements RequestMatcher {

		private String path;


		public JwtCredentialsRequestMatcher(String path) {
			this.path = path;
		}


		@Override
		public boolean matches(HttpServletRequest request) {
			String uri = request.getRequestURI();
			int pathParamIndex = uri.indexOf(';');

			if (pathParamIndex > 0) {
				// strip everything after the first semi-colon
				uri = uri.substring(0, pathParamIndex);
			}

			String grantType = request.getParameter("grant_type");
			String token = request.getParameter("assertion");

			if (token == null || !"urn:ietf:params:oauth:grant-type:jwt-bearer".equals(grantType)) {
				return false;
			}

			if ("".equals(request.getContextPath())) {
				return uri.endsWith(this.path);
			}

			return uri.endsWith(request.getContextPath() + this.path);
		}
	}

	//////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////


	public boolean isAllowOnlyPost() {
		return this.allowOnlyPost;
	}


	public void setAllowOnlyPost(boolean allowOnlyPost) {
		this.allowOnlyPost = allowOnlyPost;
	}


	public AuthenticationEntryPoint getAuthenticationEntryPoint() {
		return this.authenticationEntryPoint;
	}


	public void setAuthenticationEntryPoint(AuthenticationEntryPoint authenticationEntryPoint) {
		this.authenticationEntryPoint = authenticationEntryPoint;
	}
}
