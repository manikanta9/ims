package com.clifton.core.security.authorization;


/**
 * The <code>AccessDeniedException</code> exception is a {@link RuntimeException} that should be
 * thrown when a user doesn't have access rights to requested security resource.
 * <p/>
 * TODO: 5. move permissions mapping logic somewhere else
 *
 * @author vgomelsky
 */
public class AccessDeniedException extends SecurityException {

	private final int permissions;
	private final String securityResourceName;


	/**
	 * Constructs a new instance of {@link AccessDeniedException} exception with the specified message.
	 * <p/>
	 * Use AccessDeniedException(String securityResourceName, int permissions) when possible.
	 *
	 * @param message
	 */
	public AccessDeniedException(String message) {
		super(message);
		this.permissions = 0;
		this.securityResourceName = null;
	}


	/**
	 * Constructs a new instance of {@link AccessDeniedException} exception for the specified argument.
	 *
	 * @param securityResourceName
	 * @param permissions
	 */
	public AccessDeniedException(String securityResourceName, int permissions) {
		super(buildExceptionMessage(securityResourceName, permissions));
		this.permissions = permissions;
		this.securityResourceName = securityResourceName;
	}


	private static String buildExceptionMessage(String securityResourceName, int permissions) {
		return "Access Denied. You do not have [" + getStringPermission(permissions) + "] permission(s) to [" + securityResourceName + "] security resource.";
	}


	private static String getStringPermission(int permissions) {
		switch (permissions) {
			case SecurityPermission.PERMISSION_READ:
				return "READ";
			case SecurityPermission.PERMISSION_WRITE:
				return "WRITE";
			case SecurityPermission.PERMISSION_CREATE:
				return "CREATE";
			case SecurityPermission.PERMISSION_DELETE:
				return "DELETE";
			case SecurityPermission.PERMISSION_EXECUTE:
				return "EXECUTE";
			case SecurityPermission.PERMISSION_FULL_CONTROL:
				return "FULL_CONTROL";
			default:
				return "UNKNOWN_PERMISSION=" + permissions;
		}
	}


	/**
	 * @return the permissions
	 */
	public int getPermissions() {
		return this.permissions;
	}


	/**
	 * @return the securityResourceName
	 */
	public String getSecurityResourceName() {
		return this.securityResourceName;
	}
}
