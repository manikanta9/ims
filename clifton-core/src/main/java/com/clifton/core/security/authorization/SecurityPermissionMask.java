package com.clifton.core.security.authorization;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * The <code>SecurityPermissionMask</code> class represents definitions of security permission masks (collections of permissions).
 *
 * @author vgomelsky
 */
public class SecurityPermissionMask implements Serializable {

	public static final SecurityPermissionMask MASK_NONE = new SecurityPermissionMask(0, "N/A", "No Permissions Defined");
	public static final SecurityPermissionMask MASK_R = new SecurityPermissionMask(SecurityPermission.PERMISSION_READ, "R", "Read Only");

	public static final SecurityPermissionMask MASK_RE = new SecurityPermissionMask(SecurityPermission.MASK_RE, "RE", "Read or Execute");

	public static final SecurityPermissionMask MASK_RW = new SecurityPermissionMask(SecurityPermission.MASK_RW, "RW", "Read or Write");
	public static final SecurityPermissionMask MASK_RWE = new SecurityPermissionMask(SecurityPermission.MASK_RWE, "RWE", "Read or Write or Execute");

	public static final SecurityPermissionMask MASK_RWC = new SecurityPermissionMask(SecurityPermission.MASK_RWC, "RWC", "Read or Write or Create");
	public static final SecurityPermissionMask MASK_RWCE = new SecurityPermissionMask(SecurityPermission.MASK_RWCE, "RWCE", "Read or Write or Create or Execute");

	public static final SecurityPermissionMask MASK_RWCD = new SecurityPermissionMask(SecurityPermission.MASK_RWCD, "RWCD", "Read or Write or Create or Delete");
	public static final SecurityPermissionMask MASK_RWCDE = new SecurityPermissionMask(SecurityPermission.MASK_RWCDE, "RWCDE", "Read or Write or Create or Delete or Execute");

	public static final SecurityPermissionMask MASK_RWD = new SecurityPermissionMask(SecurityPermission.MASK_RWD, "RWD", "Read or Write or Delete");
	public static final SecurityPermissionMask MASK_RWDE = new SecurityPermissionMask(SecurityPermission.MASK_RWDE, "RWDE", "Read or Write or Delete or Execute");
	public static final SecurityPermissionMask MASK_RWDEF = new SecurityPermissionMask(SecurityPermission.MASK_RWDEF, "RWDEF", "Read or Write or Delete or Execute or Full Control");
	public static final SecurityPermissionMask MASK_RWDF = new SecurityPermissionMask(SecurityPermission.MASK_RWDF, "RWDF", "Read or Write or Delete or Full Control");

	public static final SecurityPermissionMask MASK_RWCDF = new SecurityPermissionMask(SecurityPermission.MASK_RWCDF, "RWCDF", "Read or Write or Create or Delete or Full Control");
	public static final SecurityPermissionMask MASK_RWCDEF = new SecurityPermissionMask(SecurityPermission.MASK_RWCDEF, "RWCDEF", "Read or Write or Create or Delete or Execute or Full Control");
	private static final List<SecurityPermissionMask> allMasks;


	static {
		allMasks = new ArrayList<>();
		allMasks.add(MASK_R);
		allMasks.add(MASK_RW);
		allMasks.add(MASK_RE);
		allMasks.add(MASK_RWE);
		allMasks.add(MASK_RWC);
		allMasks.add(MASK_RWCE);
		allMasks.add(MASK_RWCD);
		allMasks.add(MASK_RWCDE);
		allMasks.add(MASK_RWCDF);
		allMasks.add(MASK_RWD);
		allMasks.add(MASK_RWDE);
		allMasks.add(MASK_RWDF);
		allMasks.add(MASK_RWDEF);
		allMasks.add(MASK_RWCDEF);
	}


	private final int mask;
	private final String name;
	private final String description;


	/**
	 * Constructs a new {@link SecurityPermissionMask} object using the specified arguments.
	 *
	 * @param mask
	 * @param name
	 * @param description
	 */
	private SecurityPermissionMask(int mask, String name, String description) {
		this.mask = mask;
		this.name = name;
		this.description = description;
	}


	/**
	 * Returns {@link SecurityPermissionMask} object that corresponds to the argument mask.
	 *
	 * @param mask
	 */
	public static SecurityPermissionMask getSecurityPermissionMask(int mask) {
		switch (mask) {
			case 0:
				return MASK_NONE;
			case SecurityPermission.PERMISSION_READ:
				return MASK_R;
			case SecurityPermission.MASK_RW:
				return MASK_RW;
			case SecurityPermission.MASK_RE:
				return MASK_RE;
			case SecurityPermission.MASK_RWE:
				return MASK_RWE;
			case SecurityPermission.MASK_RWC:
				return MASK_RWC;
			case SecurityPermission.MASK_RWCE:
				return MASK_RWCE;
			case SecurityPermission.MASK_RWD:
				return MASK_RWD;
			case SecurityPermission.MASK_RWDE:
				return MASK_RWDE;
			case SecurityPermission.MASK_RWCD:
				return MASK_RWCD;
			case SecurityPermission.MASK_RWCDE:
				return MASK_RWCDE;
			case SecurityPermission.MASK_RWDF:
				return MASK_RWDF;
			case SecurityPermission.MASK_RWDEF:
				return MASK_RWDEF;
			case SecurityPermission.MASK_RWCDF:
				return MASK_RWCDF;
			case SecurityPermission.MASK_RWCDEF:
				return MASK_RWCDEF;
		}
		throw new IllegalArgumentException("Unrecognized security mask: " + mask);
	}


	/**
	 * Returns a List of all {@link SecurityPermissionMask} objects.
	 */
	public static List<SecurityPermissionMask> getAllMasks() {
		return allMasks;
	}


	/**
	 * @return the mask
	 */
	public int getMask() {
		return this.mask;
	}


	/**
	 * @return the name
	 */
	public String getName() {
		return this.name;
	}


	/**
	 * @return the description
	 */
	public String getDescription() {
		return this.description;
	}
}
