package com.clifton.core.security.oauth.client.connection.token;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTCreationException;
import com.clifton.core.security.oauth.SecurityOAuth2GrantTypes;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.token.JsonWebTokenUtils;
import com.clifton.core.web.token.JsonWebTokenClaimProperties;
import org.springframework.http.HttpHeaders;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.AccessTokenProvider;
import org.springframework.security.oauth2.client.token.AccessTokenRequest;
import org.springframework.security.oauth2.client.token.OAuth2AccessTokenSupport;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.common.OAuth2RefreshToken;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;

import java.util.Date;
import java.util.List;


/**
 * Provider for obtaining an oauth2 access token by using a jwt-bearer token.
 *
 * @author theodorez
 */
public class JwtBearerAccessTokenProvider extends OAuth2AccessTokenSupport implements AccessTokenProvider {

	private final int tokenValidMillis;
	private final String issuerName;
	private final String algorithmName;
	private static final String GRANT_TYPE = SecurityOAuth2GrantTypes.JWT_BEARER.getGrantTypeName();


	public JwtBearerAccessTokenProvider(int tokenValidMillis, String issuerName, String algorithmName) {
		this.tokenValidMillis = tokenValidMillis;
		if (issuerName.endsWith("/")) {
			issuerName = issuerName.substring(0, issuerName.length() - 1);
		}
		this.issuerName = issuerName;
		this.algorithmName = algorithmName;
	}


	@Override
	public OAuth2AccessToken obtainAccessToken(OAuth2ProtectedResourceDetails details, AccessTokenRequest request) {
		JwtBearerResourceDetails resource = (JwtBearerResourceDetails) details;
		return super.retrieveToken(request, resource, getParametersForTokenRequest(resource), new HttpHeaders());
	}


	@Override
	public boolean supportsResource(OAuth2ProtectedResourceDetails resource) {
		return resource instanceof JwtBearerResourceDetails && GRANT_TYPE.equals(resource.getGrantType());
	}


	@Override
	public OAuth2AccessToken refreshAccessToken(OAuth2ProtectedResourceDetails resource, OAuth2RefreshToken refreshToken, AccessTokenRequest request) {
		return null;
	}


	@Override
	public boolean supportsRefresh(OAuth2ProtectedResourceDetails resource) {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private MultiValueMap<String, String> getParametersForTokenRequest(JwtBearerResourceDetails resource) {
		List<String> scopes = resource.getScope();

		String token;
		try {
			String signatureKey = JsonWebTokenUtils.getJsonWebTokenSignatureKey(resource.getSignatureKeyLocation(), resource.getSignatureKeyChecksum());
			Algorithm algorithm = JsonWebTokenUtils.getAlgorithmForKey(signatureKey, this.algorithmName);
			token = JWT.create()
					.withClaim(JsonWebTokenClaimProperties.CLIENT_ID.getClaimName(), resource.getClientId())
					.withArrayClaim(JsonWebTokenClaimProperties.SCOPE.getClaimName(), scopes.toArray(new String[0]))
					.withClaim(JsonWebTokenClaimProperties.USERNAME.getClaimName(), resource.getUsername())
					.withExpiresAt(DateUtils.addMilliseconds(new Date(), this.tokenValidMillis))
					.withIssuer(this.issuerName)
					.sign(algorithm);
		}
		catch (JWTCreationException exception) {
			//Invalid Signing configuration / Couldn't convert Claims.
			throw new RuntimeException("Could not create JWT", exception);
		}

		MultiValueMap<String, String> form = new LinkedMultiValueMap<>();
		form.set("grant_type", GRANT_TYPE);
		form.set("assertion", token);

		return form;
	}
}
