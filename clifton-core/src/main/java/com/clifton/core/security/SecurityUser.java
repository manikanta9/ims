package com.clifton.core.security;

/**
 * The <code>SecurityUser</code> interface defined core attributes of a security user that are needed for core security features.
 *
 * @author manderson
 */
public interface SecurityUser {


	public String getUserName();


	public String getEmailAddress();
}
