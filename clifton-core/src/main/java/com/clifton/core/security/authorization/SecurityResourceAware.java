package com.clifton.core.security.authorization;


import com.clifton.core.beans.IdentityObject;


/**
 * The <code>SecurityResourceAware</code> interface can be implemented by
 * entities that define the security resource to use to restrict access to
 * through a bean property on the class.
 * <p/>
 * Example: BusinessContract implements SecurityResourceAware and returns the
 * security resource defined on property:  company.type.contractSecurityResource
 * <p/>
 * When blank, default implementation uses the security resource associated with the table for the dtoClass
 *
 * @author manderson
 */
public interface SecurityResourceAware extends IdentityObject {

	public SecurityResource getSecurityResource();
}
