package com.clifton.core.security.oauth;

/**
 * Defines the standardized OAuth Grant Types
 * <p>
 * per https://tools.ietf.org/html/rfc6749
 *
 * @author theodorez
 */
public enum SecurityOAuth2GrantTypes {
	/**
	 * The traditional OAuth flow:
	 * The user is redirected to an authorization server,
	 * which in turn directs the user back to the server intending to use the resource
	 * with the appropriate authorization code (or token)
	 */
	AUTHORIZATION_CODE("authorization_code"),
	/**
	 * OAuth flow that is optimized for browser clients (the browser accesses the resource directly)
	 * Instead of issuing the client an authorization token, the client (browser in this case) is issued
	 * an access token directly.
	 */
	IMPLICIT("implicit"),
	/**
	 * Can be used when the authorization scope is limited to the protected resources under the control of the client,
	 * or to protected resources previously arranged with the authorization server.
	 * <p>
	 * This is commonly used for server-server requests (maintenance, batch jobs, etc).
	 */
	CLIENT_CREDENTIALS("client_credentials"),
	/**
	 * Username and Password are used directly to obtain an access token.
	 * <p>
	 * This should only be used if there is a high degree of trust between the user and the client.
	 */
	RESOURCE_OWNER("password"),
	/**
	 * This grant type is specified as part of RFC7523 - JSON Web Token Profile for OAuth 2.0 Client Authentication and Authorization Grants
	 * https://tools.ietf.org/html/rfc7523#section-2.1
	 * <p>
	 * The client is able to present a properly signed JWT in exchange for an access token.
	 */
	JWT_BEARER("urn:ietf:params:oauth:grant-type:jwt-bearer");

	private String grantTypeName;


	SecurityOAuth2GrantTypes(String name) {
		this.grantTypeName = name;
	}


	public String getGrantTypeName() {
		return this.grantTypeName;
	}
}
