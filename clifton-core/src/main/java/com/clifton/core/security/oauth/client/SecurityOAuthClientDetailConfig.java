package com.clifton.core.security.oauth.client;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Acts as a container for the detail configurations
 *
 * @author theodorez
 */
public class SecurityOAuthClientDetailConfig {

	private final Map<String, SecurityOAuthClientDetail> clientDetailMap = new ConcurrentHashMap<>();

	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	public void addClient(SecurityOAuthClientDetail client) {
		this.clientDetailMap.put(client.getClientId(), client);
	}


	public SecurityOAuthClientDetail getClient(String clientId) {
		return this.clientDetailMap.get(clientId);
	}


	public SecurityOAuthClientDetail removeClient(String clientId) {
		return this.clientDetailMap.remove(clientId);
	}


	public Map<String, SecurityOAuthClientDetail> getClientDetailMap() {
		return this.clientDetailMap;
	}
}
