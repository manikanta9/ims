package com.clifton.core.security.authorization;


import java.lang.reflect.Method;
import java.util.Map;


/**
 * The <code>SecureMethodTableResolver</code> interface should be used to provide custom security.
 * It is used when method security is not static (always tied to a specific resource) but can
 * change dynamically based on the argument passed (table name, etc.)
 *
 * @author vgomelsky
 */
public interface SecureMethodTableResolver {

	/**
	 * Returns the name of the table that user permissions are verified against.
	 *
	 * @param method the method that is invoked (can check name, annotations, etc.)
	 * @param config map of parameter name to values (usually request parameters as opposed to method parameters)
	 */
	public String getTableName(Method method, Map<String, ?> config);
}
