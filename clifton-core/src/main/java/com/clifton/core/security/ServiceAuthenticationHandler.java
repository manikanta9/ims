package com.clifton.core.security;

import org.springframework.stereotype.Component;


/**
 * Placeholder for properties needed for the system to be able to access windows fileshares from linux.
 *
 * @author theodorez
 */
@Component
public class ServiceAuthenticationHandler {

	private String serviceDomain;
	private String serviceUsername;
	private String servicePassword;


	public String getServiceDomain() {
		return this.serviceDomain;
	}


	public void setServiceDomain(String serviceDomain) {
		this.serviceDomain = serviceDomain;
	}


	public String getServiceUsername() {
		return this.serviceUsername;
	}


	public void setServiceUsername(String serviceUsername) {
		this.serviceUsername = serviceUsername;
	}


	public String getServicePassword() {
		return this.servicePassword;
	}


	public void setServicePassword(String servicePassword) {
		this.servicePassword = servicePassword;
	}
}
