package com.clifton.core.security.authorization;

import java.lang.reflect.Method;
import java.util.Map;


/**
 * The {@link SecureMethodSecurityEvaluator} interface should be used to provide custom security authorization logic. This should be used
 * when the logic in <code>MethodSecurityHandler#verifySecurityAccess</code> is insufficient - for example,
 * <code>SystemHierarchyAssignmentSecureMethodEvaluator</code> implements logic for checking permissions against multiple {@link SecurityResource} objects.
 *
 * @author lnaylor
 */
public interface SecureMethodSecurityEvaluator {

	/**
	 * Implements custom logic to evaluate security access after checking if admin access is required in <code>MethodSecurityHandler#verifySecurityAccess</code>.
	 *
	 * @param secureAnnotation the {@link SecureMethod} annotation associated with the method
	 * @param method           the method that is invoked (can check name, annotations, etc.)
	 * @param config           map of parameter name to values (usually request parameters as opposed to method parameters)
	 */
	public void checkPermissions(SecureMethod secureAnnotation, Method method, Map<String, ?> config);
}
