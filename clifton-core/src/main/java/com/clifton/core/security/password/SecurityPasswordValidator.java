package com.clifton.core.security.password;

/**
 * @author manderson
 */
public interface SecurityPasswordValidator {


	public void validatePassword(String username, String newPassword);


	public String generateRandomPassword();
}
