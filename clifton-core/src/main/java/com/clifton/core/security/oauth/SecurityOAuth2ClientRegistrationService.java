package com.clifton.core.security.oauth;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collection;


/**
 * @author theodorez
 */
public interface SecurityOAuth2ClientRegistrationService {

	/**
	 * Gets a list of resource servers that could be registered with.
	 */
	@RequestMapping("securityResourceRegistrationList")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public Collection<String> getResourceRegistrationList();


	/**
	 * Call this to setup a connection with one of our OAuth providers.
	 * <p>
	 * Works by logging into the remote server to verify that the specified user is an admin.
	 * <p>
	 * Then if the user is an admin will call the registration URL with the specified client information.
	 * <p>
	 * Stores the key in the appropriate file and then returns a checksum for the key.
	 * <p>
	 * Returns true if successful
	 */
	@ResponseBody
	@RequestMapping(value = "securityRegisterAsOAuth2ClientWithResource", method = RequestMethod.POST)
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public boolean registerOAuth2ClientWithResource(String resourceAuthorizationUrl, String adminUserName, String adminPassword, String resourceRegistrationUrl, String apiHostname, String signatureAlgorithm, String scope, String grantType);


	/**
	 * Meant to be called by a client that wishes to register as an oauth client.
	 * <p>
	 * Returns a key that is to be used to sign JWT's that will be used for authentication.
	 */
	@ResponseBody
	@RequestMapping(value = "securityRegisterOAuth2Client", method = RequestMethod.POST)
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public String registerOAuth2Client(String clientUrl, String signatureAlgorithm, String scope, String grantType);
}
