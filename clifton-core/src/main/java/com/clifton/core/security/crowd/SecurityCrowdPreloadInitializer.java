package com.clifton.core.security.crowd;

import com.atlassian.crowd.service.AuthenticationManager;
import com.clifton.core.context.CurrentContextApplicationListener;
import org.springframework.context.ApplicationContext;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;


/**
 * The {@link SecurityCrowdPreloadInitializer} reduces first-request delay by eagerly initializing the Crowd authentication manager session on application start-up, if the
 * authentication manager exists. Session initialization typically includes expensive operations, and eager initialization can prevent some timeouts or significant delays during
 * the initial post-startup request. This is particularly relevant for Spring Boot applications, where this initialization seems to take even longer.
 *
 * @author MikeH
 */
@Component
public class SecurityCrowdPreloadInitializer implements CurrentContextApplicationListener<ContextRefreshedEvent> {

	private ApplicationContext applicationContext;
	private AuthenticationManager crowdAuthenticationManager;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void onCurrentContextApplicationEvent(ContextRefreshedEvent event) {
		try {
			if (getCrowdAuthenticationManager() != null) {
				getCrowdAuthenticationManager().getSecurityServerClient().getDomain();
			}
		}
		catch (Exception e) {
			throw new RuntimeException(e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}


	public AuthenticationManager getCrowdAuthenticationManager() {
		return this.crowdAuthenticationManager;
	}


	public void setCrowdAuthenticationManager(AuthenticationManager crowdAuthenticationManager) {
		this.crowdAuthenticationManager = crowdAuthenticationManager;
	}
}
