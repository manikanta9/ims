package com.clifton.core.security.oauth.client;

import org.springframework.security.oauth2.provider.client.BaseClientDetails;

import java.util.HashSet;


/**
 * Defines an OAuth client for use on the authorization server with the additional parameters
 * required for jwt-bearer authentication
 *
 * @author theodorez
 */
public class SecurityOAuthClientDetail extends BaseClientDetails {

	private String signatureAlgorithm;
	/**
	 * NOTE: THIS FIELD SHOULD ONLY BE POPULATED WHEN THE CLIENT DETAILS ARE RETURNED FROM THE REGISTRATION SERVER
	 */
	private String signatureKey;
	private String signatureKeyFileLocation;
	private String signatureKeyFileChecksum;
	/**
	 * Identifies whether the client is incoming or outgoing
	 */
	private Boolean incomingConnection = Boolean.FALSE;

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	public SecurityOAuthClientDetail() {
		setScope(new HashSet<>());
	}


	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof SecurityOAuthClientDetail)) {
			return false;
		}

		SecurityOAuthClientDetail test = (SecurityOAuthClientDetail) obj;

		if (getSignatureAlgorithm() != null && !getSignatureAlgorithm().equals(test.getSignatureAlgorithm())) {
			return false;
		}

		if (getSignatureKeyFileLocation() != null && !getSignatureKeyFileLocation().equals(test.getSignatureKeyFileLocation())) {
			return false;
		}

		if (getSignatureKeyFileChecksum() != null && !getSignatureKeyFileChecksum().equals(test.getSignatureKeyFileChecksum())) {
			return false;
		}


		return super.equals(obj);
	}

	///////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////


	public String getSignatureAlgorithm() {
		return this.signatureAlgorithm;
	}


	public void setSignatureAlgorithm(String signatureAlgorithm) {
		this.signatureAlgorithm = signatureAlgorithm;
	}


	public String getSignatureKeyFileLocation() {
		return this.signatureKeyFileLocation;
	}


	public void setSignatureKeyFileLocation(String signatureKeyFileLocation) {
		this.signatureKeyFileLocation = signatureKeyFileLocation;
	}


	public String getSignatureKeyFileChecksum() {
		return this.signatureKeyFileChecksum;
	}


	public void setSignatureKeyFileChecksum(String signatureKeyFileChecksum) {
		this.signatureKeyFileChecksum = signatureKeyFileChecksum;
	}


	public Boolean getIncomingConnection() {
		return this.incomingConnection;
	}


	public void setIncomingConnection(Boolean incomingConnection) {
		this.incomingConnection = incomingConnection;
	}


	public String getSignatureKey() {
		return this.signatureKey;
	}


	public void setSignatureKey(String signatureKey) {
		this.signatureKey = signatureKey;
	}
}
