package com.clifton.core.security.oauth;

import com.clifton.core.context.DoNotAddRequestMapping;


/**
 * @author theodorez
 */
public interface SecurityOAuthUserTokenService {

	@DoNotAddRequestMapping
	public String getTokenForCurrentUser(String endpointUrl);


	@DoNotAddRequestMapping
	public String getTokenForCurrentUser(String endpointUrl, boolean clearTokenCache);


	@DoNotAddRequestMapping
	public String getTokenForCurrentUserIfPresent(String endpointUrl, boolean clearTokenCache);


	@DoNotAddRequestMapping
	public String getTokenForUsername(String username, String endpointUrl);


	@DoNotAddRequestMapping
	public String getTokenForUsername(String username, String endpointUrl, boolean clearTokenCache);
}
