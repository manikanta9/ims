package com.clifton.core.security.authorization;


import com.clifton.core.beans.annotations.ValueChangingSetter;

import java.io.Serializable;


/**
 * The <code>SecurityPermission</code> class defines instances of security permission objects.
 * A security permission objects specifies the type of permission(s) that a security principal
 * (SecurityUser or SecurityGroup) has to a specific {@link SecurityResource}
 * <p/>
 * Bit assignments:
 * 0 - read
 * 1 - write
 * 2 - create
 * 3 - delete
 * 4 - full control (implies all other permissions)
 *
 * @author vgomelsky
 */
public class SecurityPermission implements Serializable {

	public static final int PERMISSION_READ = 1;
	public static final int PERMISSION_WRITE = 2;
	public static final int PERMISSION_CREATE = 4;
	public static final int PERMISSION_DELETE = 8;
	public static final int PERMISSION_EXECUTE = 16;
	public static final int PERMISSION_FULL_CONTROL = 32;

	public static final int MASK_RW = SecurityPermission.PERMISSION_READ | SecurityPermission.PERMISSION_WRITE;
	public static final int MASK_RE = SecurityPermission.PERMISSION_READ | SecurityPermission.PERMISSION_EXECUTE;
	public static final int MASK_RWE = MASK_RW | SecurityPermission.PERMISSION_EXECUTE;
	public static final int MASK_RWC = MASK_RW | SecurityPermission.PERMISSION_CREATE;
	public static final int MASK_RWCE = MASK_RWC | SecurityPermission.PERMISSION_EXECUTE;
	public static final int MASK_RWD = MASK_RW | SecurityPermission.PERMISSION_DELETE;
	public static final int MASK_RWDE = MASK_RWD | SecurityPermission.PERMISSION_EXECUTE;
	public static final int MASK_RWCD = MASK_RWC | SecurityPermission.PERMISSION_DELETE;
	public static final int MASK_RWCDE = MASK_RWCD | SecurityPermission.PERMISSION_EXECUTE;
	public static final int MASK_RWDF = MASK_RWD | SecurityPermission.PERMISSION_FULL_CONTROL;
	public static final int MASK_RWDEF = MASK_RWDE | SecurityPermission.PERMISSION_FULL_CONTROL;
	public static final int MASK_RWCDF = MASK_RWCD | SecurityPermission.PERMISSION_FULL_CONTROL;
	public static final int MASK_RWCDEF = MASK_RWCDE | SecurityPermission.PERMISSION_FULL_CONTROL;

	private int permissionMask;


	@Override
	public String toString() {
		return "[mask = " + this.permissionMask + "]";
	}


	/**
	 * Creates a new SecurityPermission object with no permissions.
	 */
	public SecurityPermission() {
		this(0);
	}


	/**
	 * Creates a new SecurityPermission object with the specified mask.
	 * See bit assignments for more details.
	 *
	 * @param permissionMask
	 */
	public SecurityPermission(int permissionMask) {
		this.permissionMask = permissionMask;
	}


	public boolean isReadAllowed() {
		return ((this.permissionMask & PERMISSION_READ) > 0) || isFullControlAllowed();
	}


	public boolean isWriteAllowed() {
		return ((this.permissionMask & PERMISSION_WRITE) > 0) || isFullControlAllowed();
	}


	public boolean isCreateAllowed() {
		return ((this.permissionMask & PERMISSION_CREATE) > 0) || isFullControlAllowed();
	}


	public boolean isDeleteAllowed() {
		return ((this.permissionMask & PERMISSION_DELETE) > 0) || isFullControlAllowed();
	}


	public boolean isExecuteAllowed() {
		return ((this.permissionMask & PERMISSION_EXECUTE) > 0) || isFullControlAllowed();
	}


	public boolean isFullControlAllowed() {
		return (this.permissionMask & PERMISSION_FULL_CONTROL) > 0;
	}


	@ValueChangingSetter
	public void setReadAllowed(boolean allow) {
		if (allow) {
			this.permissionMask = this.permissionMask | PERMISSION_READ;
		}
		else {
			this.permissionMask = this.permissionMask & (MASK_RWCDEF ^ PERMISSION_READ);
		}
	}


	@ValueChangingSetter
	public void setWriteAllowed(boolean allow) {
		if (allow) {
			this.permissionMask = this.permissionMask | PERMISSION_WRITE;
		}
		else {
			this.permissionMask = this.permissionMask & (MASK_RWCDEF ^ PERMISSION_WRITE);
		}
	}


	@ValueChangingSetter
	public void setCreateAllowed(boolean allow) {
		if (allow) {
			this.permissionMask = this.permissionMask | PERMISSION_CREATE;
		}
		else {
			this.permissionMask = this.permissionMask & (MASK_RWCDEF ^ PERMISSION_CREATE);
		}
	}


	@ValueChangingSetter
	public void setDeleteAllowed(boolean allow) {
		if (allow) {
			this.permissionMask = this.permissionMask | PERMISSION_DELETE;
		}
		else {
			this.permissionMask = this.permissionMask & (MASK_RWCDEF ^ PERMISSION_DELETE);
		}
	}


	@ValueChangingSetter
	public void setExecuteAllowed(boolean allow) {
		if (allow) {
			this.permissionMask = this.permissionMask | PERMISSION_EXECUTE;
		}
		else {
			this.permissionMask = this.permissionMask & (MASK_RWCDEF ^ PERMISSION_EXECUTE);
		}
	}


	@ValueChangingSetter
	public void setFullControlAllowed(boolean allow) {
		if (allow) {
			this.permissionMask = MASK_RWCDEF; // implies all other permissions
		}
		else {
			this.permissionMask = this.permissionMask & (MASK_RWCDEF ^ PERMISSION_FULL_CONTROL);
		}
	}


	/**
	 * Returns true if this object's mask allows execution of all permissions in the specified mask.
	 *
	 * @param mask
	 */
	public boolean isMaskAllowed(int mask) {
		if (isFullControlAllowed()) {
			return true;
		}
		if ((mask & PERMISSION_FULL_CONTROL) > 0) {
			return false;
		}
		if ((mask & PERMISSION_EXECUTE) > 0 && !isExecuteAllowed()) {
			return false;
		}
		if ((mask & PERMISSION_WRITE) > 0 && !isWriteAllowed()) {
			return false;
		}
		if ((mask & PERMISSION_CREATE) > 0 && !isCreateAllowed()) {
			return false;
		}
		if ((mask & PERMISSION_DELETE) > 0 && !isDeleteAllowed()) {
			return false;
		}
		if ((mask & PERMISSION_READ) > 0 && !isReadAllowed()) {
			return false;
		}
		return true;
	}


	/**
	 * @return the permissionMask
	 */
	public int getPermissionMask() {
		return this.permissionMask;
	}


	/**
	 * @param permissionMask the permissionMask to set
	 */
	public void setPermissionMask(int permissionMask) {
		this.permissionMask = permissionMask;
	}
}
