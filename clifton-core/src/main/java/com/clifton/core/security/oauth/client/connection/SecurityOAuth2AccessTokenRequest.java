package com.clifton.core.security.oauth.client.connection;


import com.clifton.core.security.oauth.SecurityOAuth2GrantTypes;


/**
 * @author theodorez
 */
public class SecurityOAuth2AccessTokenRequest {

	/**
	 * The base hostname of the connection
	 */
	private String baseUrl;

	/**
	 * The URL that is used for authorization codes
	 */
	private String authorizationUrl;

	/**
	 * The URL that tokens can be requested from
	 */
	private String tokenUrl;

	/**
	 * The type of grant that the connection allows (how you get a token)
	 */
	private SecurityOAuth2GrantTypes grantType;

	/**
	 * The identifier assigned to the client by the resource server (what you're connecting to)
	 */
	private String clientIdentifier;

	/**
	 * Comma delimited list of scopes for a given client
	 */
	private String scopes;


	private boolean impersonationAllowed;

	private String usernameToImpersonate;

	private String signatureKeyLocation;

	private String signatureKeyChecksum;


	public String getBaseUrl() {
		return this.baseUrl;
	}


	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}


	public String getAuthorizationUrl() {
		return this.authorizationUrl;
	}


	public void setAuthorizationUrl(String authorizationUrl) {
		this.authorizationUrl = authorizationUrl;
	}


	public String getTokenUrl() {
		return this.tokenUrl;
	}


	public void setTokenUrl(String tokenUrl) {
		this.tokenUrl = tokenUrl;
	}


	public SecurityOAuth2GrantTypes getGrantType() {
		return this.grantType;
	}


	public void setGrantType(SecurityOAuth2GrantTypes grantType) {
		this.grantType = grantType;
	}


	public boolean isImpersonationAllowed() {
		return this.impersonationAllowed;
	}


	public void setImpersonationAllowed(boolean impersonationAllowed) {
		this.impersonationAllowed = impersonationAllowed;
	}


	public String getClientIdentifier() {
		return this.clientIdentifier;
	}


	public void setClientIdentifier(String clientIdentifier) {
		this.clientIdentifier = clientIdentifier;
	}


	public String getScopes() {
		return this.scopes;
	}


	public void setScopes(String scopes) {
		this.scopes = scopes;
	}


	public String getSignatureKeyLocation() {
		return this.signatureKeyLocation;
	}


	public void setSignatureKeyLocation(String signatureKeyLocation) {
		this.signatureKeyLocation = signatureKeyLocation;
	}


	public String getUsernameToImpersonate() {
		return this.usernameToImpersonate;
	}


	public void setUsernameToImpersonate(String usernameToImpersonate) {
		this.usernameToImpersonate = usernameToImpersonate;
	}


	public String getSignatureKeyChecksum() {
		return this.signatureKeyChecksum;
	}


	public void setSignatureKeyChecksum(String signatureKeyChecksum) {
		this.signatureKeyChecksum = signatureKeyChecksum;
	}
}
