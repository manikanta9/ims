package com.clifton.core.shared.dataaccess;


/**
 * The <code>DataTypes</code> enumeration defines supported database data types.
 * Instead of using very specific DB data types, we try to standardize DB design by standardizing the data types.
 * For example, NAME is used to identify a 50 character NVARCHAR, etc.
 *
 * @author vgomelsky
 */
public enum DataTypes {

	IDENTITY_STRING(DataTypesBuilder.of(DataTypeNames.STRING).withIdentity(true)),

	// Integers
	IDENTITY(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(true)),
	NATURAL_ID(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(false).withNaturalKey(true)),
	INT(DataTypesBuilder.of(DataTypeNames.INTEGER)), //

	// Longs/BigInt
	IDENTITY_LONG(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(true)),
	NATURAL_ID_LONG(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(false).withNaturalKey(true)),
	LONG(DataTypesBuilder.of(DataTypeNames.INTEGER)), //

	// Shorts/SmallInt
	IDENTITY_SHORT(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(true)),
	NATURAL_ID_SHORT(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(false).withNaturalKey(true)),
	SHORT(DataTypesBuilder.of(DataTypeNames.INTEGER)), //

	// TinyInt
	IDENTITY_TINY(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(true)),
	NATURAL_ID_TINY(DataTypesBuilder.of(DataTypeNames.INTEGER).withIdentity(false).withNaturalKey(true)),
	TINY(DataTypesBuilder.of(DataTypeNames.INTEGER)), //

	// Decimals (Precision of up to 9 takes 5 bytes, up to 19 takes 9 bytes, up to 28 takes 13 bytes, up to 38 takes 17 bytes)
	MONEY(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(19).withPrecision(2)), MONEY4(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(19).withPrecision(4)), DECIMAL_PRECISE(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(19).withPrecision(10)), //
	PRICE(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(28).withPrecision(15)), QUANTITY(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(28).withPrecision(10)), //
	EXCHANGE_RATE(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(28).withPrecision(16)), //

	// Percentages
	PERCENT(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(9).withPrecision(5).withPercent(true)), PERCENT_PRECISE(DataTypesBuilder.of(DataTypeNames.DECIMAL).withLength(19).withPrecision(15).withPercent(true)), //

	// String/Text
	// TEXT was removed in SQL Server and it is recommended to use varchar(max), nvarchar(max), or varbinary(max)
	// varchar - for ascii data and uses a single byte per character, storage size = = actual length + 2 bytes, allows length value 1-8000, max = 2^31-1 (2GB)
	// nvarchar - for unicode data and uses 2 bytes per character, storage size = actual length * 2 + 2 bytes, allows length value 1-4000, max = storage size of 2^31-1 (2GB), data size (2^31-1/2)(1GB)
	NAME(DataTypesBuilder.of(DataTypeNames.STRING).withLength(50)), NAME_LONG(DataTypesBuilder.of(DataTypeNames.STRING).withLength(100)), //
	DESCRIPTION(DataTypesBuilder.of(DataTypeNames.STRING).withLength(500)), DESCRIPTION_LONG(DataTypesBuilder.of(DataTypeNames.STRING).withLength(4000)), //
	CUSTOM_JSON_STRING(DataTypesBuilder.of(DataTypeNames.STRING).withLength(Integer.MAX_VALUE)), // Uses NVARCHAR(MAX)
	STRING3(DataTypesBuilder.of(DataTypeNames.STRING).withLength(3)), STRING10(DataTypesBuilder.of(DataTypeNames.STRING).withLength(10)), TEXT(DataTypesBuilder.of(DataTypeNames.TEXT).withLength(Integer.MAX_VALUE)), //
	ENUM(DataTypesBuilder.of(DataTypeNames.STRING).withLength(50)), //

	// Boolean
	BIT(DataTypesBuilder.of(DataTypeNames.BOOLEAN)), //

	// Date/Times
	DATE(DataTypesBuilder.of(DataTypeNames.DATE)), DATETIME(DataTypesBuilder.of(DataTypeNames.DATE)), TIME(DataTypesBuilder.of(DataTypeNames.INTEGER)), //

	// RV
	ROWVERSION(DataTypesBuilder.of(DataTypeNames.STRING)), //

	//SECRET
	// uses varbinary data type for binary data.
	// usage examples: SECRET_SHORT - InitializationVector, SECRET - EncryptionKey, SECRET_MAX - SecretValue (PKI Data)
	SECRET_SHORT(DataTypesBuilder.of(DataTypeNames.SECRET).withLength(64)), SECRET(DataTypesBuilder.of(DataTypeNames.SECRET).withLength(512)), SECRET_MAX(DataTypesBuilder.of(DataTypeNames.SECRET)),

	// Stores text in a compressed field
	COMPRESSED_TEXT(DataTypesBuilder.of(DataTypeNames.BINARY).withCompressed(true)),

	// Blank placeholder data type
	NONE(DataTypesBuilder.of(null));

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final int length;
	private final int precision;
	private final DataTypeNames typeName;

	private final boolean identity;
	private final boolean naturalKey;

	private final boolean percent;

	private final boolean compressed;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	DataTypes(DataTypesBuilder builder) {
		this.typeName = builder.typeName;
		this.length = builder.length;
		this.precision = builder.precision;
		this.identity = builder.identity;
		this.naturalKey = builder.naturalKey;
		this.percent = builder.percent;
		this.compressed = builder.compressed;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class DataTypesBuilder {

		private int length;
		private int precision;
		private DataTypeNames typeName;

		private boolean identity;
		private boolean naturalKey;

		private boolean percent;

		private boolean compressed;


		private DataTypesBuilder() {
		}


		public static DataTypesBuilder of(DataTypeNames typeName) {
			DataTypesBuilder result = new DataTypesBuilder();
			result.typeName = typeName;
			return result;
		}


		public DataTypesBuilder withLength(int length) {
			this.length = length;
			return this;
		}


		public DataTypesBuilder withPrecision(int precision) {
			this.precision = precision;
			return this;
		}


		public DataTypesBuilder withIdentity(boolean identity) {
			this.identity = identity;
			return this;
		}


		public DataTypesBuilder withNaturalKey(boolean naturalKey) {
			this.naturalKey = naturalKey;
			return this;
		}


		public DataTypesBuilder withPercent(boolean percent) {
			this.percent = percent;
			return this;
		}


		public DataTypesBuilder withCompressed(boolean compressed) {
			this.compressed = compressed;
			return this;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the length of a data type (number of characters in a string, number of digits in a number, etc.).
	 * Returns 0 when length doesn't apply to a data type.
	 */
	public int getLength() {
		return this.length;
	}


	/**
	 * Returns the precision of a data type (number of digits used for precision in decimals).
	 * Returns 0 when length doesn't apply to a data type.
	 */
	public int getPrecision() {
		return this.precision;
	}


	/**
	 * Returns the name of this DataTypes instance (more generic categorization)
	 */
	public DataTypeNames getTypeName() {
		return this.typeName;
	}


	public boolean isIdentity() {
		return this.identity;
	}


	public boolean isNaturalKey() {
		return this.naturalKey;
	}


	public boolean isPercent() {
		return this.percent;
	}


	public boolean isCompressed() {
		return this.compressed;
	}
}
