package com.clifton.core.shared.dataaccess;


import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>DataTypeNames</code> enumeration defines supported database data type names.
 *
 * @author vgomelsky
 */
public enum DataTypeNames {

	INTEGER(Integer.class, true, false),
	DECIMAL(BigDecimal.class, true, false),
	STRING(String.class, false, true),
	DATE(Date.class, false, false),
	TIME(Date.class, false, false),
	BOOLEAN(Boolean.class, false, false),
	TEXT(String.class, false, true),
	SECRET(String.class, false, false),
	BINARY(null, false, false),
	TABLE(null, false, false);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private final Class<?> dataType;

	private final boolean numeric;

	private final boolean supportEncoding;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	DataTypeNames(Class<?> dataType, boolean numeric, boolean supportEncoding) {
		this.dataType = dataType;
		this.numeric = numeric;
		this.supportEncoding = supportEncoding;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Class<?> getDataType() {
		return this.dataType;
	}


	public boolean isNumeric() {
		return this.numeric;
	}


	public boolean isSupportEncoding() {
		return this.supportEncoding;
	}
}
