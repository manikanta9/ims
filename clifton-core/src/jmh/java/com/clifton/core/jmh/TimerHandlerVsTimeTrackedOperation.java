package com.clifton.core.jmh;

import com.clifton.core.util.date.TimeTrackedOperation;
import com.clifton.core.util.date.TimeUtils;
import com.clifton.core.util.timer.ThreadLocalTimerHandler;
import com.clifton.core.util.timer.TimerHandler;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;


/**
 * Tests the performance of {@link TimerHandler} against {@link TimeTrackedOperation}.
 * <p>
 * Results:
 * <pre>
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_201, Java HotSpot(TM) 64-Bit Server VM, 25.201-b09
 * # VM invoker: C:\Program Files\Java\jdk1.8.0_201\jre\bin\java.exe
 * # VM options: -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant
 * # Warmup: 2 iterations, 5 s each
 * # Measurement: 4 iterations, 5 s each
 * Benchmark                                                Mode  Cnt   Score   Error  Units
 * TimerHandlerVsTimeTrackedOperation.timeTrackedOperation  avgt    4  52.761 � 1.078  ns/op
 * TimerHandlerVsTimeTrackedOperation.timerHandler          avgt    4  62.765 � 3.726  ns/op
 * </pre>
 * <p>
 * The difference is insignificant. {@link TimeTrackedOperation} can be used without worry of hurting performance.
 *
 * @author lnaylor
 */
public class TimerHandlerVsTimeTrackedOperation extends BaseJmhBenchmark {

	private TimerHandler timerHandler = new ThreadLocalTimerHandler();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Benchmark
	public void timerHandler(Blackhole bh) {
		getTimerHandler().startTimer();
		bh.consume(someOperation());
		getTimerHandler().stopTimer();
		bh.consume(getTimerHandler().getDurationNano());
		getTimerHandler().reset();
	}


	@Benchmark
	public void timeTrackedOperation(Blackhole bh) {
		TimeTrackedOperation<Void> elapsedTime = TimeUtils.getElapsedTime(() -> bh.consume(someOperation()));
		bh.consume(elapsedTime.getElapsedNanoseconds());
	}


	private int someOperation() {
		return 1;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public TimerHandler getTimerHandler() {
		return this.timerHandler;
	}


	public void setTimerHandler(TimerHandler timerHandler) {
		this.timerHandler = timerHandler;
	}
}
