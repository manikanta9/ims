package com.clifton.core.jmh;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Warmup;


/**
 * Tests the performance of {@link String#contains(CharSequence)} against {@link String#matches(String)}.
 * <p>
 * Results:
 * <pre>
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_221, Java HotSpot(TM) 64-Bit Server VM, 25.221-b11
 * # VM options: -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant
 * # Warmup: 5 iterations, 10 s each
 * # Measurement: 5 iterations, 10 s each
 * Benchmark                          Mode  Cnt         Score        Error  Units
 * ContainsVsMatches.stringContains  thrpt    5  92794973.477   324380.123  ops/s
 * ContainsVsMatches.stringMatches   thrpt    5   1994804.471    10574.317  ops/s
 * </pre>
 * <p>
 * The difference is significant. The cost of NFA generation and execution in {@link String#matches(String)} is likely to be the blame.
 *
 * @author MikeH
 */
@Warmup(iterations = 0)
@Measurement(iterations = 1)
public class ContainsVsMatches extends BaseJmhBenchmark {

	// Intentionally non-final to prevent JVM optimizations
	private String testString = "mystring";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Benchmark
	public void stringContains() {
		if (this.testString.contains("abc") || this.testString.contains("def") || this.testString.contains("ghi")) {
			// Do nothing
		}
	}


	@Benchmark
	public void stringMatches() {
		if (this.testString.matches(".*(abc|def|ghi).*")) {
			// Do nothing
		}
	}
}
