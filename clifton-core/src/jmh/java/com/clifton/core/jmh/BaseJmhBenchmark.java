package com.clifton.core.jmh;

import org.openjdk.jmh.Main;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.runner.options.CommandLineOptions;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;


/**
 * The base type for creating <a href="https://openjdk.java.net/projects/code-tools/jmh/">JMH</a> benchmarks.
 * <p>
 * This type provides an agnostic {@code public static void main} entry point. When using this entry point, the class targeted via the command line will be used as the set of
 * benchmarks to execute. Additionally, this type provides default benchmark settings (such as {@link BenchmarkMode} and {@link OutputTimeUnit}) which are inherited by children.
 * These can be overridden if necessary.
 * <p>
 * JMH resources:
 * <ul>
 * <li>JMH home page: https://openjdk.java.net/projects/code-tools/jmh/
 * <li>JMH samples: https://hg.openjdk.java.net/code-tools/jmh/file/tip/jmh-samples/src/main/java/org/openjdk/jmh/samples/
 * <li>JMH tutorial: http://tutorials.jenkov.com/java-performance/jmh.html
 * </ul>
 *
 * @author MikeH
 */
/* Default benchmark settings */
@State(Scope.Benchmark)
@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Fork(value = 1, warmups = 0)
/*
 * Rapid benchmarks: Testing only
 */
// @Warmup(iterations = 0)
// @Measurement(iterations = 1, time = 1)
/*
 * Quick benchmarks: Low accuracy
 */
// @Warmup(iterations = 1, time = 2)
// @Measurement(iterations = 1, time = 3)
/*
 * Standard benchmarks: Medium accuracy
 */
@Warmup(iterations = 2, time = 5)
@Measurement(iterations = 4, time = 5)
/*
 * Full benchmarks: High accuracy
 */
// @Warmup(iterations = 5, time = 10)
// @Measurement(iterations = 5, time = 10)
public abstract class BaseJmhBenchmark {

	public static void main(String[] args) {
		try {
			// Pre-process arguments
			List<String> arguments = new ArrayList<>(Arrays.asList(args));
			CommandLineOptions options = new CommandLineOptions(args);
			if (options.getIncludes().isEmpty()) {
				// Only run benchmarks for the main class; this allows us to easily run individual benchmark classes
				String mainClassName = getMainClassName();
				arguments.add(Pattern.quote(mainClassName));
			}
			// Execute benchmarks
			Main.main(arguments.toArray(new String[0]));
		}
		catch (Exception e) {
			throw new RuntimeException(String.format("An error occurred while processing benchmarks. Command line: [%s]. Arguments: %s.", getCommandline(), Arrays.asList(args)), e);
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static String getCommandline() {
		return System.getProperty("sun.java.command");
	}


	private static String getMainClassName() {
		String commandline = getCommandline();
		if (commandline == null) {
			throw new RuntimeException("Unable to determine the targeted class for benchmark execution.");
		}
		// Strip command line arguments
		int delimiterIndex = commandline.indexOf(' ');
		return delimiterIndex > -1
				? commandline.substring(0, delimiterIndex)
				: commandline;
	}
}
