package com.clifton.core.jmh;

import com.clifton.core.util.CollectionUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.TearDown;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


/**
 * Tests various methods of seeking threads within the JVM. This is intended to be used to evaluate the costs associated with these methods in order to determine the fastest way
 * to locate threads given an identifier, such as a name or ID.
 * <p>
 * Results:
 * <pre>
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_252, OpenJDK 64-Bit Server VM, 25.252-b09
 * # VM invoker: C:\Users\mikeh\.jdks\adopt-openjdk-1.8.0_252\jre\bin\java.exe
 * # VM options: -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant
 * # Warmup: 2 iterations, 5 s each
 * # Measurement: 4 iterations, 5 s each
 * Benchmark                                                                    Mode  Cnt         Score         Error  Units
 * ThreadRetrievalByStackTraceVsThreadGroup.threadByGroupRetrievalNested        avgt    4    281265.184      4383.017  ns/op
 * ThreadRetrievalByStackTraceVsThreadGroup.threadByGroupRetrievalShallow       avgt    4     43404.409       338.329  ns/op
 * ThreadRetrievalByStackTraceVsThreadGroup.threadByStackTraceRetrievalNested   avgt    4  25442128.680    114419.112  ns/op
 * ThreadRetrievalByStackTraceVsThreadGroup.threadByStackTraceRetrievalShallow  avgt    4   3944197.835     36636.245  ns/op
 * </pre>
 * <p>
 * Retrieving threads using the root {@link ThreadGroup} method is significantly faster than when calling {@link Thread#getAllStackTraces()}. The "nested" and "shallow" results
 * should be compared independently. This difference may be explained by the cost of dumping the stack traces for each thread during retrieval.
 * <p>
 * There are some race conditions which may be worth considering when identifying threads via the {@link ThreadGroup} method. See {@link ThreadGroup#enumerate(java.lang.Thread[],
 * boolean)} for more information.
 *
 * @author MikeH
 */
@SuppressWarnings({"ALL", "java:S3014"})
public class ThreadRetrievalByStackTraceVsThreadGroup extends BaseJmhBenchmark {

	private static final long THREAD_SLEEP_MS = 10 * 60 * 1000; // 10 minutes

	// Number of threads for "nested" scenarios: 2^6 = 64 threads
	private static final int NESTED_THREAD_GROUP_DEPTH = 5;
	private static final int NESTED_THREAD_GROUP_BREADTH_COUNT = 2;
	private static final int NESTED_THREAD_PER_GROUP_COUNT = 2;
	private List<Thread> nestedThreadList;
	private List<Long> nestedThreadIdList;

	private static final int SHALLOW_THREAD_COUNT = 10;
	private List<Thread> shallowThreadList;
	private List<Long> shallowThreadIdList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Setup(Level.Trial)
	public void setup() {
		// Create nested thread group hierarchy
		this.nestedThreadList = createThreadGroupHierarchy(Thread.currentThread().getThreadGroup(), NESTED_THREAD_GROUP_DEPTH);
		this.nestedThreadIdList = CollectionUtils.getConverted(this.nestedThreadList, Thread::getId);
		this.nestedThreadList.forEach(Thread::start);

		// Create shallow thread list
		this.shallowThreadList = IntStream.range(0, SHALLOW_THREAD_COUNT)
				.mapToObj(i -> new Thread(new TrialRunnable()))
				.collect(Collectors.toList());
		this.shallowThreadIdList = CollectionUtils.getConverted(this.shallowThreadList, Thread::getId);
		this.shallowThreadList.forEach(Thread::start);
	}


	@TearDown(Level.Trial)
	public void teardown() {
		for (Thread thread : this.nestedThreadList) {
			thread.interrupt();
		}
		for (Thread thread : this.shallowThreadList) {
			thread.interrupt();
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private List<Thread> createThreadGroupHierarchy(ThreadGroup parentThreadGroup, int remainingDepth) {
		if (remainingDepth > 0) {
			return IntStream.range(0, NESTED_THREAD_GROUP_BREADTH_COUNT)
					.mapToObj(i -> createThreadGroupHierarchy(new ThreadGroup(parentThreadGroup, parentThreadGroup.getName() + "-" + i), remainingDepth - 1))
					.flatMap(Collection::stream)
					.collect(Collectors.toList());
		}
		else {
			return IntStream.range(0, NESTED_THREAD_PER_GROUP_COUNT)
					.mapToObj(i -> new Thread(parentThreadGroup, new TrialRunnable(), parentThreadGroup.getName() + "-" + i))
					.collect(Collectors.toList());
		}
	}


	private class TrialRunnable implements Runnable {

		@Override
		public void run() {
			try {
				Thread.sleep(THREAD_SLEEP_MS);
			}
			catch (InterruptedException ie) {
				Thread.currentThread().interrupt();
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Benchmark
	public void threadByStackTraceRetrievalShallow(Blackhole bh) {
		for (Long id : this.shallowThreadIdList) {
			bh.consume(getThreadByStackTraceRetrieval(id));
		}
	}


	@Benchmark
	public void threadByStackTraceRetrievalNested(Blackhole bh) {
		for (Long id : this.nestedThreadIdList) {
			bh.consume(getThreadByStackTraceRetrieval(id));
		}
	}


	@Benchmark
	public void threadByGroupRetrievalShallow(Blackhole bh) {
		for (Long id : this.shallowThreadIdList) {
			bh.consume(getThreadByGroupRetrieval(id));
		}
	}


	@Benchmark
	public void threadByGroupRetrievalNested(Blackhole bh) {
		for (Long id : this.nestedThreadIdList) {
			bh.consume(getThreadByGroupRetrieval(id));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Simulates gathering thread details by stack trace information.
	 */
	private Thread getThreadByStackTraceRetrieval(long id) {
		// Find expected thread
		Thread seekThread = null;
		for (Thread thread : Thread.getAllStackTraces().keySet()) {
			if (thread != null && id == thread.getId()) {
				seekThread = thread;
				break;
			}
		}
		if (seekThread == null) {
			throw new RuntimeException(String.format("Thread with ID [%d] could not be found.", id));
		}
		return seekThread;
	}


	/**
	 * Simulates gathering thread details by navigating thread group information.
	 */
	private Thread getThreadByGroupRetrieval(long id) {
		// Identify root thread group
		ThreadGroup tg = Thread.currentThread().getThreadGroup();
		while (tg.getParent() != null) {
			tg = tg.getParent();
		}

		// Populate all threads; see org.apache.commons.lang3.ThreadUtils.findThreads(java.lang.ThreadGroup, boolean, org.apache.commons.lang3.ThreadUtils.ThreadPredicate)
		int activeCount = tg.activeCount();
		Thread[] threads;
		do {
			threads = new Thread[activeCount + (activeCount / 2) + 1];
			activeCount = tg.enumerate(threads);
		}
		while (activeCount >= threads.length);

		// Find expected thread
		Thread seekThread = null;
		for (Thread thread : threads) {
			if (thread != null && id == thread.getId()) {
				seekThread = thread;
				break;
			}
		}
		if (seekThread == null) {
			throw new RuntimeException(String.format("Thread with ID [%d] could not be found.", id));
		}
		return seekThread;
	}
}
