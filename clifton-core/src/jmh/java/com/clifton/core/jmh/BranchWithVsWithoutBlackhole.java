package com.clifton.core.jmh;

import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;


/**
 * Tests branches with and without the use of {@link Blackhole} objects. This intends to test whether conditions on identical branches in these cases are removed by the JVM.
 * <p>
 * Results:
 * <pre>
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_221, Java HotSpot(TM) 64-Bit Server VM, 25.221-b11
 * # VM options: -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant
 * # Warmup: 5 iterations, 10 s each
 * # Measurement: 5 iterations, 10 s each
 * Benchmark                                             Mode  Cnt        Score      Error  Units
 * BranchWithVsWithoutBlackhole.branchWithBlackhole     thrpt    5  1713565.494   3755.746  ops/s
 * BranchWithVsWithoutBlackhole.branchWithoutBlackhole  thrpt    5  1698288.384   4884.721  ops/s
 * </pre>
 * <p>
 * The difference is insignificant. The branch conditions are likely not removed by the JVM due to possible side effects.
 *
 * @author MikeH
 */
public class BranchWithVsWithoutBlackhole extends BaseJmhBenchmark {

	// Intentionally non-final to prevent JVM optimizations
	private String testString = "mystring";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Benchmark
	public void branchWithoutBlackhole(Blackhole bh) {
		if (this.testString.matches("(?!test123).*") && this.testString.matches(".*")) {
			// Do nothing
		}
	}


	@Benchmark
	public void branchWithBlackhole(Blackhole bh) {
		if (this.testString.matches("(?!test123).*") && this.testString.matches(".*")) {
			bh.consume(false);
		}
	}
}
