package com.clifton.core.jmh;

import com.clifton.core.util.CollectionUtils;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Tests various procedures using {@link Pattern} objects. This intends to be used to evaluate the costs associated with the use of {@link Pattern} objects.
 * <p>
 * Results:
 * <pre>
 * # JMH version: 1.23
 * # VM version: JDK 1.8.0_221, Java HotSpot(TM) 64-Bit Server VM, 25.221-b11
 * # VM invoker: C:\Program Files\Java\jdk1.8.0_221\jre\bin\java.exe
 * # VM options: -Dfile.encoding=UTF-8 -Duser.country=US -Duser.language=en -Duser.variant
 * # Warmup: 2 iterations, 5 s each
 * # Measurement: 4 iterations, 5 s each
 * Benchmark                                  Mode  Cnt     Score     Error  Units
 * PatternPerformance.patternCache            avgt    4    20.098     2.686  ns/op
 * PatternPerformance.patternCompile          avgt    4  3781.424    31.673  ns/op
 * PatternPerformance.patternCompileAndMatch  avgt    4  4196.000   114.635  ns/op
 * PatternPerformance.patternMatch            avgt    4   400.273     1.350  ns/op
 * PatternPerformance.startsWithStringMatch   avgt    4   112.397     0.463  ns/op
 * </pre>
 * <p>
 * Compilation tends to be the most expensive activity by an order of magnitude when using {@link Pattern} objects. The cost of the match operation is much lesser, but is still
 * greater than simpler operations like {@link String#startsWith(String)}.
 *
 * @author MikeH
 */
public class PatternPerformance extends BaseJmhBenchmark {

	// Types are intentionally non-final to prevent JVM optimizations
	private List<Class<?>> classList = Arrays.asList(
			PatternPerformance.class,
			BaseJmhBenchmark.class
	);
	private List<String> startsWithStringList = Arrays.asList(
			"com.clifton.core.jmh.test-pattern-1.",
			"com.clifton.core.jmh.test-pattern-2.",
			"com.clifton.core.jmh.PatternPerformance"
	);
	private List<String> patternStringList = CollectionUtils.getConverted(this.startsWithStringList, str -> Pattern.quote(str) + ".*");
	private List<Pattern> patternList = CollectionUtils.getConverted(this.patternStringList, Pattern::compile);
	private Map<Class<?>, Pattern> patternResultCache = new HashMap<>();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Setup(Level.Trial)
	public void setup() {
		// Populate pattern result cache
		this.patternResultCache.clear();
		for (Class<?> clazz : this.classList) {
			for (Pattern pattern : this.patternList) {
				if (pattern.matcher(clazz.getName()).matches()) {
					System.out.println("matching " + clazz.getName() + " with pattern " + pattern);
					this.patternResultCache.put(clazz, pattern);
					break;
				}
			}
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Benchmark
	public void patternCompile(Blackhole bh) {
		// Simulates compiling patterns only to help illustrate the incremental cost of pattern compilation
		for (Class<?> clazz : this.classList) {
			bh.consume(clazz);
			for (String patternStr : this.patternStringList) {
				bh.consume(Pattern.compile(patternStr));
			}
		}
	}


	@Benchmark
	public void patternCompileAndMatch(Blackhole bh) {
		// Simulates matching patterns which are not pre-compiled
		for (Class<?> clazz : this.classList) {
			bh.consume(clazz);
			for (String patternStr : this.patternStringList) {
				Pattern pattern = Pattern.compile(patternStr);
				Matcher matcher = pattern.matcher(clazz.getName());
				bh.consume(matcher.find());
			}
		}
	}


	@Benchmark
	public void patternMatch(Blackhole bh) {
		// Simulates matching patterns which are pre-compiled
		for (Class<?> clazz : this.classList) {
			bh.consume(clazz);
			for (Pattern pattern : this.patternList) {
				Matcher matcher = pattern.matcher(clazz.getName());
				bh.consume(matcher.find());
			}
		}
	}


	@Benchmark
	public void startsWithStringMatch(Blackhole bh) {
		// Simulates using String#startsWith instead of the equivalent patterns
		for (Class<?> clazz : this.classList) {
			bh.consume(clazz);
			for (String startsWithString : this.startsWithStringList) {
				bh.consume(clazz.getName().startsWith(startsWithString));
			}
		}
	}


	@Benchmark
	public void patternCache(Blackhole bh) {
		// Simulates retrieving pattern match results which are already cached
		for (Class<?> clazz : this.classList) {
			bh.consume(clazz);
			bh.consume(this.patternResultCache.get(clazz));
		}
	}
}
