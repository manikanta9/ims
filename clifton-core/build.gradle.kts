import com.clifton.gradle.plugin.build.registerVariant


val shared = registerVariant("shared", upstreamForMain = true)

val jmhSourceSet = cliftonJmh.enable()

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api("com.amazonaws:aws-java-sdk-s3:1.11.832")
	// For testing AWS S3
	testImplementation("com.adobe.testing:s3mock-junit5:2.1.26") {
		exclude("org.apache.logging.log4j", "log4j-to-slf4j")
		exclude("ch.qos.logback", "logback-classic")
		exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
	}

	// Tomcat URL rewrite filter - forked version that allows 'drop-cookies=false' param on rule and adds the "X-Forwarded-For" header.
	// Fork source: https://github.com/onehippo/urlrewritefilter/tree/master
	api("org.tuckey:urlrewritefilter:4.0.4-h9-beta-4")

	runtimeOnly("com.microsoft.sqlserver:mssql-jdbc:8.2.2.jre8") // Spring Boot 2.3.0 requires 7.4.1.jre8; upgrade for enhanced driver features
	runtimeOnly("net.sourceforge.jtds:jtds:1.3.1")

	// Used by Spring bean postprocessor
	runtimeOnly("commons-fileupload:commons-fileupload:1.3.3")
	// Bean utility libraries used by spring
	runtimeOnly("org.apache.xbean:xbean-spring:3.7")

	// Log4j2 and bridge dependencies - Bridges ensure third party dependency logging are funneled properly
	api("org.apache.logging.log4j:log4j-web")
	api("org.apache.logging.log4j:log4j-api")
	api("org.apache.logging.log4j:log4j-jcl")
	api("org.apache.logging.log4j:log4j-slf4j-impl")
	api("org.apache.logging.log4j:log4j-jul")

	api("biz.paluch.logging:logstash-gelf:1.11.2")

	// Hibernate - pulls in hibernate-core
	api("org.hibernate:hibernate-ehcache")
	// The following is used for migrations and jdbc pool beans. Ideally, the version should match the Tomcat runtime version
	// but a mismatched dependency version does seem to work across Tomcat runtime versions.
	api("org.apache.tomcat:tomcat-jdbc")

	// Spring - springVersion is configured in gradle.properties so it can be used globally
	api("org.springframework:spring-webmvc")
	api("org.springframework:spring-jdbc")
	api("org.springframework:spring-orm")
	api("org.springframework:spring-context-support")
	api("org.springframework:spring-aspects")
	api("org.springframework:spring-web")

	// Security libraries
	api("com.atlassian.crowd:crowd-integration-client:clifton-v2-2.6.5")
	api("com.auth0:java-jwt:3.1.0")
	api("org.bouncycastle:bcpg-jdk15on:1.64")
	api("org.bouncycastle:bcprov-ext-jdk15on:1.64")
	// springSecurityVersion is configured in gradle.properties so it can be used globally
	api("org.springframework.security:spring-security-config")
	api("org.springframework.security:spring-security-web")
	api("org.springframework.security.oauth:spring-security-oauth2:2.4.0.RELEASE")
	api("org.springframework.security:spring-security-jwt:1.1.0.RELEASE")

	// Servlet library
	api("javax.servlet:javax.servlet-api")

	// Warning: Updating the httpcomponents library to version 4.4 or higher may cause issues.
	// If the version needs to be updated, you may need to configure it to not validate domain suffixes
	api("org.apache.httpcomponents:httpclient")
	api("org.apache.httpcomponents:httpmime")

	// JSON libraries
	api("com.fasterxml.jackson.core:jackson-core")
	api("com.fasterxml.jackson.core:jackson-databind")
	api("com.fasterxml.jackson.core:jackson-annotations")
	api("com.fasterxml.jackson.datatype:jackson-datatype-hibernate5")
	api("com.fasterxml.jackson.datatype:jackson-datatype-jsr310")
	api("com.monitorjbl:json-view:1.0.1")

	// Apache Commons libraries
	api("commons-lang:commons-lang:2.4")
	api("commons-beanutils:commons-beanutils:1.9.4")
	api("commons-validator:commons-validator:1.4.0")
	api("commons-net:commons-net:3.1")
	api("commons-io:commons-io:2.2")
	api("commons-collections:commons-collections:3.2.2")
	api("org.apache.commons:commons-csv:1.7")

	// Document related libraries
	api("org.freemarker:freemarker")
	api("org.apache.poi:poi-ooxml:3.17")
	api("com.itextpdf:itextpdf:5.5.0")
	api("com.itextpdf.tool:xmlworker:5.5.0")
	api("com.jcraft:jsch:0.1.50")
	api("com.thoughtworks.xstream:xstream:1.4.11.1")
	api("javax.mail:mail:1.4.7")

	// Used in MathUtils to evaluate string mathematical expressions: Ex. "2+3+5" will evaluate to 10
	api("org.apfloat:apfloat-calc:1.8.0")

	// Class utilities for classpath and class scanning
	api("org.reflections:reflections:0.9.10")

	// Used for transferring files to/from network shares
	api("com.hierynomus:smbj:0.9.1")

	// Password validation - used for Portal and other apps when using disabled security
	api("org.passay:passay:1.2.0")

	compileOnlyApi("org.jetbrains:annotations:20.1.0")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////
	api(project(":clifton-core:clifton-core-utils"))
	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	// In memory DB
	// TODO: While this should be a runtime-only dependency, an issue with Gradle Test Fixtures does not allow it to propagate to downstream projects.
	//  See https://github.com/gradle/gradle/issues/11336.
	// testFixturesRuntimeOnly("org.hsqldb:hsqldb")
	// Upgraded to 2.5.1 since it addresses this transactional issue: https://sourceforge.net/p/hsqldb/discussion/73674/thread/1441bdff47/
	testFixturesApi("org.hsqldb:hsqldb:2.5.1")

	testFixturesApi("org.springframework:spring-test")
	testFixturesApi("org.springframework:spring-jms") // Needed for standard BasicProjectTests constraints

	// JUnit 5
	testFixturesApi("org.junit.jupiter:junit-jupiter-api")
	testFixturesApi("org.junit.jupiter:junit-jupiter-engine")
	testFixturesApi("org.junit.jupiter:junit-jupiter-params")
	testFixturesApi("org.junit.platform:junit-platform-engine")
	testFixturesApi("org.junit.platform:junit-platform-launcher")
	testFixturesApi("org.mockito:mockito-junit-jupiter")
	testFixturesApi("org.hamcrest:hamcrest")
	testFixturesApi("org.mockito:mockito-core")

	// EmailHandler
	testFixturesApi("net.kemitix:wiser-assertions:0.4.0")
}
