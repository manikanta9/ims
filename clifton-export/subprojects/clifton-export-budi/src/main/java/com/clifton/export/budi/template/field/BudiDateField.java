package com.clifton.export.budi.template.field;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;

import java.util.Date;


/**
 * @author TerryS
 */
public class BudiDateField extends BudiField {

	public BudiDateField(String name, Visibility visibility) {
		super(name, visibility);
	}


	@Override
	public String format(Object value) {
		if (getVisibility() == Visibility.MANDATORY && value == null) {
			throw new RuntimeException("A mandatory field value is missing " + getName());
		}
		if (value != null) {
			ValidationUtils.assertTrue(value instanceof Date, "The provided value must be a date.");
			return DateUtils.fromDate((Date) value, "yyyyMMdd");
		}
		else {
			return null;
		}
	}
}
