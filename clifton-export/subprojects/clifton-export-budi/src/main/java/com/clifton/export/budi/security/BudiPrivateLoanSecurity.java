package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiPrivateLoanSecurity extends BudiBaseSecurity {

	private String activity;
	private BigDecimal allIn;
	private BigDecimal amount;
	private BigDecimal baseRate;
	private String borrower;
	private BigDecimal borrowingBaseAmount;
	private Boolean callProtection;
	private BigDecimal ceiling;
	private BigDecimal commitmentFee;
	private String contractID;
	private String countryCode;
	private Boolean covenantLite;
	private String currency;
	private String cusip;
	private Date date;
	private String dayCountBasis;
	private String dealIdentifier;
	private String dealName;
	private Date effectiveDate;
	private String fitchRating;
	private BigDecimal floor;
	private Date fundingDate;
	private Date fxFixingDate;
	private BigDecimal fxRate;
	private BigDecimal globalAmount;
	private String index;
	private BigDecimal indexFloor;
	private Date indexFloorExpirationDate;
	private BigDecimal interest;
	private String interestFrequency;
	private String isin;
	private BigDecimal issuedOID;
	private BigDecimal issueSize;
	private Date launchDate;
	private String lcs;
	private Boolean leveraged;
	private String loanType;
	private BigDecimal margin;
	private Date maturityDate;
	private BigDecimal minimumTransferAmount;
	private String moodysRating;
	private Date nextPayDate;
	private BigDecimal outstandingSize;
	private BigDecimal pik;
	private BigDecimal principle;
	private String purpose;
	private String rank;
	private String resetFrequency;
	private String sandPRating;
	private String sector;
	private Date signingDate;
	private String sourceContractID;
	private Date startDate;
	private String status;
	private BigDecimal targetAmount;
	private String targetContractID;
	private String ticker;
	private BigDecimal trancheCommitAfter;
	private BigDecimal trancheCommitBefore;
	private BigDecimal trancheFundedAfter;
	private BigDecimal trancheFundedBefore;
	private String trancheLetter;
	private String transactionID;
	private BigDecimal transferFee;
	private String type;
	private String xRefContext;


	public BudiPrivateLoanSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.LOANS_PPCR, instrumentType);
	}


	public String getActivity() {
		return this.activity;
	}


	public void setActivity(String activity) {
		this.activity = activity;
	}


	public BigDecimal getAllIn() {
		return this.allIn;
	}


	public void setAllIn(BigDecimal allIn) {
		this.allIn = allIn;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public BigDecimal getBaseRate() {
		return this.baseRate;
	}


	public void setBaseRate(BigDecimal baseRate) {
		this.baseRate = baseRate;
	}


	public String getBorrower() {
		return this.borrower;
	}


	public void setBorrower(String borrower) {
		this.borrower = borrower;
	}


	public BigDecimal getBorrowingBaseAmount() {
		return this.borrowingBaseAmount;
	}


	public void setBorrowingBaseAmount(BigDecimal borrowingBaseAmount) {
		this.borrowingBaseAmount = borrowingBaseAmount;
	}


	public Boolean getCallProtection() {
		return this.callProtection;
	}


	public void setCallProtection(Boolean callProtection) {
		this.callProtection = callProtection;
	}


	public BigDecimal getCeiling() {
		return this.ceiling;
	}


	public void setCeiling(BigDecimal ceiling) {
		this.ceiling = ceiling;
	}


	public BigDecimal getCommitmentFee() {
		return this.commitmentFee;
	}


	public void setCommitmentFee(BigDecimal commitmentFee) {
		this.commitmentFee = commitmentFee;
	}


	public String getContractID() {
		return this.contractID;
	}


	public void setContractID(String contractID) {
		this.contractID = contractID;
	}


	public String getCountryCode() {
		return this.countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public Boolean getCovenantLite() {
		return this.covenantLite;
	}


	public void setCovenantLite(Boolean covenantLite) {
		this.covenantLite = covenantLite;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getDayCountBasis() {
		return this.dayCountBasis;
	}


	public void setDayCountBasis(String dayCountBasis) {
		this.dayCountBasis = dayCountBasis;
	}


	public String getDealIdentifier() {
		return this.dealIdentifier;
	}


	public void setDealIdentifier(String dealIdentifier) {
		this.dealIdentifier = dealIdentifier;
	}


	public String getDealName() {
		return this.dealName;
	}


	public void setDealName(String dealName) {
		this.dealName = dealName;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public String getFitchRating() {
		return this.fitchRating;
	}


	public void setFitchRating(String fitchRating) {
		this.fitchRating = fitchRating;
	}


	public BigDecimal getFloor() {
		return this.floor;
	}


	public void setFloor(BigDecimal floor) {
		this.floor = floor;
	}


	public Date getFundingDate() {
		return this.fundingDate;
	}


	public void setFundingDate(Date fundingDate) {
		this.fundingDate = fundingDate;
	}


	public Date getFxFixingDate() {
		return this.fxFixingDate;
	}


	public void setFxFixingDate(Date fxFixingDate) {
		this.fxFixingDate = fxFixingDate;
	}


	public BigDecimal getFxRate() {
		return this.fxRate;
	}


	public void setFxRate(BigDecimal fxRate) {
		this.fxRate = fxRate;
	}


	public BigDecimal getGlobalAmount() {
		return this.globalAmount;
	}


	public void setGlobalAmount(BigDecimal globalAmount) {
		this.globalAmount = globalAmount;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	public BigDecimal getIndexFloor() {
		return this.indexFloor;
	}


	public void setIndexFloor(BigDecimal indexFloor) {
		this.indexFloor = indexFloor;
	}


	public Date getIndexFloorExpirationDate() {
		return this.indexFloorExpirationDate;
	}


	public void setIndexFloorExpirationDate(Date indexFloorExpirationDate) {
		this.indexFloorExpirationDate = indexFloorExpirationDate;
	}


	public BigDecimal getInterest() {
		return this.interest;
	}


	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}


	public String getInterestFrequency() {
		return this.interestFrequency;
	}


	public void setInterestFrequency(String interestFrequency) {
		this.interestFrequency = interestFrequency;
	}


	public String getIsin() {
		return this.isin;
	}


	public void setIsin(String isin) {
		this.isin = isin;
	}


	public BigDecimal getIssuedOID() {
		return this.issuedOID;
	}


	public void setIssuedOID(BigDecimal issuedOID) {
		this.issuedOID = issuedOID;
	}


	public BigDecimal getIssueSize() {
		return this.issueSize;
	}


	public void setIssueSize(BigDecimal issueSize) {
		this.issueSize = issueSize;
	}


	public Date getLaunchDate() {
		return this.launchDate;
	}


	public void setLaunchDate(Date launchDate) {
		this.launchDate = launchDate;
	}


	public String getLcs() {
		return this.lcs;
	}


	public void setLcs(String lcs) {
		this.lcs = lcs;
	}


	public Boolean getLeveraged() {
		return this.leveraged;
	}


	public void setLeveraged(Boolean leveraged) {
		this.leveraged = leveraged;
	}


	public String getLoanType() {
		return this.loanType;
	}


	public void setLoanType(String loanType) {
		this.loanType = loanType;
	}


	public BigDecimal getMargin() {
		return this.margin;
	}


	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public BigDecimal getMinimumTransferAmount() {
		return this.minimumTransferAmount;
	}


	public void setMinimumTransferAmount(BigDecimal minimumTransferAmount) {
		this.minimumTransferAmount = minimumTransferAmount;
	}


	public String getMoodysRating() {
		return this.moodysRating;
	}


	public void setMoodysRating(String moodysRating) {
		this.moodysRating = moodysRating;
	}


	public Date getNextPayDate() {
		return this.nextPayDate;
	}


	public void setNextPayDate(Date nextPayDate) {
		this.nextPayDate = nextPayDate;
	}


	public BigDecimal getOutstandingSize() {
		return this.outstandingSize;
	}


	public void setOutstandingSize(BigDecimal outstandingSize) {
		this.outstandingSize = outstandingSize;
	}


	public BigDecimal getPik() {
		return this.pik;
	}


	public void setPik(BigDecimal pik) {
		this.pik = pik;
	}


	public BigDecimal getPrinciple() {
		return this.principle;
	}


	public void setPrinciple(BigDecimal principle) {
		this.principle = principle;
	}


	public String getPurpose() {
		return this.purpose;
	}


	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}


	public String getRank() {
		return this.rank;
	}


	public void setRank(String rank) {
		this.rank = rank;
	}


	public String getResetFrequency() {
		return this.resetFrequency;
	}


	public void setResetFrequency(String resetFrequency) {
		this.resetFrequency = resetFrequency;
	}


	public String getSandPRating() {
		return this.sandPRating;
	}


	public void setSandPRating(String sandPRating) {
		this.sandPRating = sandPRating;
	}


	public String getSector() {
		return this.sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public Date getSigningDate() {
		return this.signingDate;
	}


	public void setSigningDate(Date signingDate) {
		this.signingDate = signingDate;
	}


	public String getSourceContractID() {
		return this.sourceContractID;
	}


	public void setSourceContractID(String sourceContractID) {
		this.sourceContractID = sourceContractID;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public String getStatus() {
		return this.status;
	}


	public void setStatus(String status) {
		this.status = status;
	}


	public BigDecimal getTargetAmount() {
		return this.targetAmount;
	}


	public void setTargetAmount(BigDecimal targetAmount) {
		this.targetAmount = targetAmount;
	}


	public String getTargetContractID() {
		return this.targetContractID;
	}


	public void setTargetContractID(String targetContractID) {
		this.targetContractID = targetContractID;
	}


	public String getTicker() {
		return this.ticker;
	}


	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	public BigDecimal getTrancheCommitAfter() {
		return this.trancheCommitAfter;
	}


	public void setTrancheCommitAfter(BigDecimal trancheCommitAfter) {
		this.trancheCommitAfter = trancheCommitAfter;
	}


	public BigDecimal getTrancheCommitBefore() {
		return this.trancheCommitBefore;
	}


	public void setTrancheCommitBefore(BigDecimal trancheCommitBefore) {
		this.trancheCommitBefore = trancheCommitBefore;
	}


	public BigDecimal getTrancheFundedAfter() {
		return this.trancheFundedAfter;
	}


	public void setTrancheFundedAfter(BigDecimal trancheFundedAfter) {
		this.trancheFundedAfter = trancheFundedAfter;
	}


	public BigDecimal getTrancheFundedBefore() {
		return this.trancheFundedBefore;
	}


	public void setTrancheFundedBefore(BigDecimal trancheFundedBefore) {
		this.trancheFundedBefore = trancheFundedBefore;
	}


	public String getTrancheLetter() {
		return this.trancheLetter;
	}


	public void setTrancheLetter(String trancheLetter) {
		this.trancheLetter = trancheLetter;
	}


	public String getTransactionID() {
		return this.transactionID;
	}


	public void setTransactionID(String transactionID) {
		this.transactionID = transactionID;
	}


	public BigDecimal getTransferFee() {
		return this.transferFee;
	}


	public void setTransferFee(BigDecimal transferFee) {
		this.transferFee = transferFee;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
