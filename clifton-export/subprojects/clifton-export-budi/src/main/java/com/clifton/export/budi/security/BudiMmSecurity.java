package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiMmSecurity extends BudiBaseSecurity {

	private String action;
	private Date announcementDate;
	private BigDecimal calcType;
	private String classification;
	private BigDecimal collateralPercent;
	private String counterparty;
	private String countryOfIssuance;
	private String couponFreq;
	private String creditEnhancement;
	private String currency;
	private String dayCountBasis;
	private Date settlementDate;
	private Date effectiveDate;
	private Date firstAccrualDate;
	private Date firstCouponDate;
	private String floatingIndex;
	private String floatingIndexTenor;
	private String guaranteeType;
	private BigDecimal haircutPercent;
	private String industry;
	private Boolean isOpenTrade;
	private Boolean isRoll;
	private BigDecimal issueAmount;
	private BigDecimal issuePrice;
	private String issuerName;
	private BigDecimal margin;
	private Date maturityDate;
	private BigDecimal notional;
	private String notionalAmountMultiplier;
	private BigDecimal price;
	private String program;
	private BigDecimal rate;
	private String rateType;
	private BigDecimal repoRate;
	private String resetFrequencey;
	private BigDecimal spread;
	private String style;
	private Integer subordinationType;
	private String ticker;
	private String underlyingSecurity;
	private String underlyingSecurityType;
	private String userSeries;
	private String xRefContext;


	public BudiMmSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.MONEY_MARKETS, instrumentType);
	}


	public String getAction() {
		return this.action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public Date getAnnouncementDate() {
		return this.announcementDate;
	}


	public void setAnnouncementDate(Date announcementDate) {
		this.announcementDate = announcementDate;
	}


	public BigDecimal getCalcType() {
		return this.calcType;
	}


	public void setCalcType(BigDecimal calcType) {
		this.calcType = calcType;
	}


	public String getClassification() {
		return this.classification;
	}


	public void setClassification(String classification) {
		this.classification = classification;
	}


	public BigDecimal getCollateralPercent() {
		return this.collateralPercent;
	}


	public void setCollateralPercent(BigDecimal collateralPercent) {
		this.collateralPercent = collateralPercent;
	}


	public String getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}


	public String getCountryOfIssuance() {
		return this.countryOfIssuance;
	}


	public void setCountryOfIssuance(String countryOfIssuance) {
		this.countryOfIssuance = countryOfIssuance;
	}


	public String getCouponFreq() {
		return this.couponFreq;
	}


	public void setCouponFreq(String couponFreq) {
		this.couponFreq = couponFreq;
	}


	public String getCreditEnhancement() {
		return this.creditEnhancement;
	}


	public void setCreditEnhancement(String creditEnhancement) {
		this.creditEnhancement = creditEnhancement;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getDayCountBasis() {
		return this.dayCountBasis;
	}


	public void setDayCountBasis(String dayCountBasis) {
		this.dayCountBasis = dayCountBasis;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getFirstAccrualDate() {
		return this.firstAccrualDate;
	}


	public void setFirstAccrualDate(Date firstAccrualDate) {
		this.firstAccrualDate = firstAccrualDate;
	}


	public Date getFirstCouponDate() {
		return this.firstCouponDate;
	}


	public void setFirstCouponDate(Date firstCouponDate) {
		this.firstCouponDate = firstCouponDate;
	}


	public String getFloatingIndex() {
		return this.floatingIndex;
	}


	public void setFloatingIndex(String floatingIndex) {
		this.floatingIndex = floatingIndex;
	}


	public String getFloatingIndexTenor() {
		return this.floatingIndexTenor;
	}


	public void setFloatingIndexTenor(String floatingIndexTenor) {
		this.floatingIndexTenor = floatingIndexTenor;
	}


	public String getGuaranteeType() {
		return this.guaranteeType;
	}


	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}


	public BigDecimal getHaircutPercent() {
		return this.haircutPercent;
	}


	public void setHaircutPercent(BigDecimal haircutPercent) {
		this.haircutPercent = haircutPercent;
	}


	public String getIndustry() {
		return this.industry;
	}


	public void setIndustry(String industry) {
		this.industry = industry;
	}


	public Boolean getOpenTrade() {
		return this.isOpenTrade;
	}


	public void setOpenTrade(Boolean openTrade) {
		this.isOpenTrade = openTrade;
	}


	public Boolean getRoll() {
		return this.isRoll;
	}


	public void setRoll(Boolean roll) {
		this.isRoll = roll;
	}


	public BigDecimal getIssueAmount() {
		return this.issueAmount;
	}


	public void setIssueAmount(BigDecimal issueAmount) {
		this.issueAmount = issueAmount;
	}


	public BigDecimal getIssuePrice() {
		return this.issuePrice;
	}


	public void setIssuePrice(BigDecimal issuePrice) {
		this.issuePrice = issuePrice;
	}


	public String getIssuerName() {
		return this.issuerName;
	}


	public void setIssuerName(String issuerName) {
		this.issuerName = issuerName;
	}


	public BigDecimal getMargin() {
		return this.margin;
	}


	public void setMargin(BigDecimal margin) {
		this.margin = margin;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public String getNotionalAmountMultiplier() {
		return this.notionalAmountMultiplier;
	}


	public void setNotionalAmountMultiplier(String notionalAmountMultiplier) {
		this.notionalAmountMultiplier = notionalAmountMultiplier;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public String getProgram() {
		return this.program;
	}


	public void setProgram(String program) {
		this.program = program;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public String getRateType() {
		return this.rateType;
	}


	public void setRateType(String rateType) {
		this.rateType = rateType;
	}


	public BigDecimal getRepoRate() {
		return this.repoRate;
	}


	public void setRepoRate(BigDecimal repoRate) {
		this.repoRate = repoRate;
	}


	public String getResetFrequencey() {
		return this.resetFrequencey;
	}


	public void setResetFrequencey(String resetFrequencey) {
		this.resetFrequencey = resetFrequencey;
	}


	public BigDecimal getSpread() {
		return this.spread;
	}


	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}


	public String getStyle() {
		return this.style;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	public Integer getSubordinationType() {
		return this.subordinationType;
	}


	public void setSubordinationType(Integer subordinationType) {
		this.subordinationType = subordinationType;
	}


	public String getTicker() {
		return this.ticker;
	}


	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	public String getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(String underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public String getUnderlyingSecurityType() {
		return this.underlyingSecurityType;
	}


	public void setUnderlyingSecurityType(String underlyingSecurityType) {
		this.underlyingSecurityType = underlyingSecurityType;
	}


	public String getUserSeries() {
		return this.userSeries;
	}


	public void setUserSeries(String userSeries) {
		this.userSeries = userSeries;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
