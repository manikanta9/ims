package com.clifton.export.budi.file;

import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;
import com.clifton.export.budi.security.BudiBondOptionSecurity;
import com.clifton.export.budi.security.BudiCdsSecurity;
import com.clifton.export.budi.security.BudiCmdtySecurity;
import com.clifton.export.budi.security.BudiEquityOptionSecurity;
import com.clifton.export.budi.security.BudiFiPpcrSecurity;
import com.clifton.export.budi.security.BudiFiSecurity;
import com.clifton.export.budi.security.BudiFxSecurity;
import com.clifton.export.budi.security.BudiIrSecurity;
import com.clifton.export.budi.security.BudiMmSecurity;
import com.clifton.export.budi.security.BudiPlaceholderSecurity;
import com.clifton.export.budi.security.BudiPrivateLoanSecurity;
import com.clifton.export.budi.security.BudiSecurity;
import com.clifton.export.budi.security.NonBudiSecurity;

import java.security.SecureRandom;
import java.util.Date;
import java.util.function.Function;


/**
 * UDI - User Defined Instruments
 * PPCR - is the standard private securities creation template for fixed income securities in bloomberg from bonds, to corps, govis, fixed and floating bonds etc
 * IP - Intellectual property
 *
 * @author TerryS
 */
public enum BudiFileFormats {
	INTEREST_RATE_DERIVATIVES(BudiInstruments.INTEREST_RATES, "IR", BudiIrSecurity::new),
	CREDIT_DERIVATIVES(BudiInstruments.CREDIT_DERIVATIVES, "CD", BudiCdsSecurity::new),
	FX_AND_FX_OPTIONS(BudiInstruments.FOREIGN_EXCHANGE, "FX", BudiFxSecurity::new),
	COMMODITY_INSTRUMENTS(BudiInstruments.COMMODITY, "CMDTY", BudiCmdtySecurity::new),
	MONEY_MARKETS(BudiInstruments.MONEY_MARKETS, "MM", BudiMmSecurity::new),
	FIXED_INCOME_PPCR_UDI(BudiInstruments.FIXED_INCOME_INSTRUMENTS, "FI_UDI", BudiFiSecurity::new),
	FIXED_INCOME_PPCR_ORIGINAL(BudiInstruments.BONDS_PPCR_ORIGINAL, "FI_PPRC", BudiFiPpcrSecurity::new),
	LOANS(BudiInstruments.LOANS_PPCR, "LOAN_PPRC", BudiPrivateLoanSecurity::new),
	BOND_OPTIONS(BudiInstruments.BOND_OPTIONS, "FI_BOND_OPT", BudiBondOptionSecurity::new),
	EQUITY_AND_IR_FUTURE_OPTIONS(BudiInstruments.EQUITY_IR_FUTURE_OPTIONS, "EQTY_IR", BudiEquityOptionSecurity::new),

	NON_BUDI(BudiInstruments.NON_BUDI, "NON_BUDI", NonBudiSecurity::new),
	PLACEHOLDER(BudiInstruments.PLACEHOLDER, "UNKWN", BudiPlaceholderSecurity::new);

	private static final String BUDI_VERSION = "BUDI_v1_03";
	private static final SecureRandom random = new SecureRandom();

	private final BudiInstruments instrument;
	private final String abbreviation;
	private final Function<BudiInstrumentTypes, BudiSecurity> securitySupplier;


	BudiFileFormats(BudiInstruments instrument, String abbreviation, Function<BudiInstrumentTypes, BudiSecurity> securitySupplier) {
		this.instrument = instrument;
		this.abbreviation = abbreviation;
		this.securitySupplier = securitySupplier;
	}


	public BudiSecurity getSecurityInstance(BudiInstrumentTypes type) {
		return this.securitySupplier.apply(type);
	}


	public BudiInstruments getInstrument() {
		return this.instrument;
	}


	public String getAbbreviation() {
		return this.abbreviation;
	}


	/**
	 * The filename is case insensitive
	 * The prefix is at the discretion of the client and must not be more than 20 characters in length (PPA_CDS_RANDOM SHORT_)
	 * The BUDI CSV version is fixed length of 10 characters and must contain the version number and conform to the format below (BUDI_v1_03_)
	 * Suffix: Fixed length of 18 characters including file extension and must conform to the specified format below (yyMMdd_HHmmss)
	 * The filename should not exceed the total limit of up to 48 characters (PPA_CDS_BUDI_v1_03_YYMMDD_HHMMSS.csv)
	 */
	public String getFileName(String companyAlias, Date date) {
		StringBuilder builder = new StringBuilder();
		if (!StringUtils.isEmpty(companyAlias)) {
			builder.append(companyAlias);
			builder.append('_');
		}
		builder.append(getAbbreviation());
		builder.append('_');
		builder.append(random.nextInt(Short.MAX_VALUE + 1));
		builder.append('_');
		builder.append(BUDI_VERSION);
		builder.append('_');
		Date timestamp = new Date();
		if (date != null) {
			timestamp = new Date(date.getTime());
			timestamp = DateUtils.setTime(timestamp, new Date());
		}
		builder.append(DateUtils.fromDate(timestamp, "yyMMdd_HHmmss"));
		builder.append(".csv");
		return builder.toString();
	}
}
