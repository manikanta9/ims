package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstruments;
import com.clifton.export.budi.asset.BudiInstrumentTypes;


/**
 * @author TerryS
 */
public interface BudiSecurity {

	public BudiInstruments getInstrument();


	public BudiInstrumentTypes getInstrumentType();
}
