package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiBondOptionSecurity extends BudiBaseSecurity {

	private String barrierDirection;
	private Date barrierEndDate;
	private String barrierLevel;
	private BigDecimal barrierRebate;
	private String barrierSettlement;
	private Date barrierStartDate;
	private String barrierType;
	private BigDecimal capStrike;
	private String currency;
	private String exerciseStyle;
	private Date expiryDate;
	private BigDecimal optionStrike;
	private String optionType;
	private String payoffType;
	private BigDecimal rate;
	private Date settlementDate;
	private BigDecimal sharesPerWarrant;
	private String side;
	private BigDecimal shares;
	private String strategy;
	private String style;
	private String ticker;
	private String underlyingSecurity;
	private String underlyingSecurityType;
	private String warrantType;
	private String xRefContext;


	public BudiBondOptionSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.BOND_OPTIONS, instrumentType);
	}


	public String getBarrierDirection() {
		return this.barrierDirection;
	}


	public void setBarrierDirection(String barrierDirection) {
		this.barrierDirection = barrierDirection;
	}


	public Date getBarrierEndDate() {
		return this.barrierEndDate;
	}


	public void setBarrierEndDate(Date barrierEndDate) {
		this.barrierEndDate = barrierEndDate;
	}


	public String getBarrierLevel() {
		return this.barrierLevel;
	}


	public void setBarrierLevel(String barrierLevel) {
		this.barrierLevel = barrierLevel;
	}


	public BigDecimal getBarrierRebate() {
		return this.barrierRebate;
	}


	public void setBarrierRebate(BigDecimal barrierRebate) {
		this.barrierRebate = barrierRebate;
	}


	public String getBarrierSettlement() {
		return this.barrierSettlement;
	}


	public void setBarrierSettlement(String barrierSettlement) {
		this.barrierSettlement = barrierSettlement;
	}


	public Date getBarrierStartDate() {
		return this.barrierStartDate;
	}


	public void setBarrierStartDate(Date barrierStartDate) {
		this.barrierStartDate = barrierStartDate;
	}


	public String getBarrierType() {
		return this.barrierType;
	}


	public void setBarrierType(String barrierType) {
		this.barrierType = barrierType;
	}


	public BigDecimal getCapStrike() {
		return this.capStrike;
	}


	public void setCapStrike(BigDecimal capStrike) {
		this.capStrike = capStrike;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getExerciseStyle() {
		return this.exerciseStyle;
	}


	public void setExerciseStyle(String exerciseStyle) {
		this.exerciseStyle = exerciseStyle;
	}


	public Date getExpiryDate() {
		return this.expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public BigDecimal getOptionStrike() {
		return this.optionStrike;
	}


	public void setOptionStrike(BigDecimal optionStrike) {
		this.optionStrike = optionStrike;
	}


	public String getOptionType() {
		return this.optionType;
	}


	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}


	public String getPayoffType() {
		return this.payoffType;
	}


	public void setPayoffType(String payoffType) {
		this.payoffType = payoffType;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BigDecimal getSharesPerWarrant() {
		return this.sharesPerWarrant;
	}


	public void setSharesPerWarrant(BigDecimal sharesPerWarrant) {
		this.sharesPerWarrant = sharesPerWarrant;
	}


	public String getSide() {
		return this.side;
	}


	public void setSide(String side) {
		this.side = side;
	}


	public BigDecimal getShares() {
		return this.shares;
	}


	public void setShares(BigDecimal shares) {
		this.shares = shares;
	}


	public String getStrategy() {
		return this.strategy;
	}


	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}


	public String getStyle() {
		return this.style;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	public String getTicker() {
		return this.ticker;
	}


	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	public String getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(String underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public String getUnderlyingSecurityType() {
		return this.underlyingSecurityType;
	}


	public void setUnderlyingSecurityType(String underlyingSecurityType) {
		this.underlyingSecurityType = underlyingSecurityType;
	}


	public String getWarrantType() {
		return this.warrantType;
	}


	public void setWarrantType(String warrantType) {
		this.warrantType = warrantType;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
