package com.clifton.export.budi.template.field;

/**
 * @author TerryS
 */
public class BudiFieldValue {

	private final String header;
	private final Object value;


	public BudiFieldValue(String header, Object value) {
		this.header = header;
		this.value = value;
	}


	public String getHeader() {
		return this.header;
	}


	public Object getValue() {
		return this.value;
	}
}
