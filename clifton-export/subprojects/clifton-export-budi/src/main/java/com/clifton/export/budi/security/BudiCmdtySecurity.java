package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiCmdtySecurity extends BudiBaseSecurity {

	private String action;
	private String barrierDirection;
	private Date barrierEndDate;
	private String barrierLevel;
	private String barrierRebate;
	private Date barrierStartDate;
	private String counterparty;
	private String dealTypePath;
	private Date deliveryDate;
	private Date getDeliveryDate2;
	private String delivery;
	private Boolean digital;
	private Date effectiveDate;
	private Date endDate;
	private String exerciseStyle;
	private Date expiryDate;
	private String frequency;
	private BigDecimal notional;
	private BigDecimal notional2;
	private String optionCurrency;
	private BigDecimal optionStrike;
	private BigDecimal getOptionStrike2;
	private String optionType;
	private String optionType2;
	private String payCondition;
	private BigDecimal rate;
	private BigDecimal rate2;
	private String rateSet;
	private String payOffSide;
	private String settlementCurrency;
	private Date startDate;
	private String style;
	private String style2;
	private String swapType;
	private Date tradeDate;
	private String underlyingSecurity;
	private String underlyingSecurityType;
	private String xRefContext;


	public BudiCmdtySecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.COMMODITY, instrumentType);
	}


	public String getAction() {
		return this.action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getBarrierDirection() {
		return this.barrierDirection;
	}


	public void setBarrierDirection(String barrierDirection) {
		this.barrierDirection = barrierDirection;
	}


	public Date getBarrierEndDate() {
		return this.barrierEndDate;
	}


	public void setBarrierEndDate(Date barrierEndDate) {
		this.barrierEndDate = barrierEndDate;
	}


	public String getBarrierLevel() {
		return this.barrierLevel;
	}


	public void setBarrierLevel(String barrierLevel) {
		this.barrierLevel = barrierLevel;
	}


	public String getBarrierRebate() {
		return this.barrierRebate;
	}


	public void setBarrierRebate(String barrierRebate) {
		this.barrierRebate = barrierRebate;
	}


	public Date getBarrierStartDate() {
		return this.barrierStartDate;
	}


	public void setBarrierStartDate(Date barrierStartDate) {
		this.barrierStartDate = barrierStartDate;
	}


	public String getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}


	public String getDealTypePath() {
		return this.dealTypePath;
	}


	public void setDealTypePath(String dealTypePath) {
		this.dealTypePath = dealTypePath;
	}


	public Date getDeliveryDate() {
		return this.deliveryDate;
	}


	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public Date getGetDeliveryDate2() {
		return this.getDeliveryDate2;
	}


	public void setGetDeliveryDate2(Date getDeliveryDate2) {
		this.getDeliveryDate2 = getDeliveryDate2;
	}


	public String getDelivery() {
		return this.delivery;
	}


	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}


	public Boolean getDigital() {
		return this.digital;
	}


	public void setDigital(Boolean digital) {
		this.digital = digital;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getExerciseStyle() {
		return this.exerciseStyle;
	}


	public void setExerciseStyle(String exerciseStyle) {
		this.exerciseStyle = exerciseStyle;
	}


	public Date getExpiryDate() {
		return this.expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public String getFrequency() {
		return this.frequency;
	}


	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public BigDecimal getNotional2() {
		return this.notional2;
	}


	public void setNotional2(BigDecimal notional2) {
		this.notional2 = notional2;
	}


	public String getOptionCurrency() {
		return this.optionCurrency;
	}


	public void setOptionCurrency(String optionCurrency) {
		this.optionCurrency = optionCurrency;
	}


	public BigDecimal getOptionStrike() {
		return this.optionStrike;
	}


	public void setOptionStrike(BigDecimal optionStrike) {
		this.optionStrike = optionStrike;
	}


	public BigDecimal getGetOptionStrike2() {
		return this.getOptionStrike2;
	}


	public void setGetOptionStrike2(BigDecimal getOptionStrike2) {
		this.getOptionStrike2 = getOptionStrike2;
	}


	public String getOptionType() {
		return this.optionType;
	}


	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}


	public String getOptionType2() {
		return this.optionType2;
	}


	public void setOptionType2(String optionType2) {
		this.optionType2 = optionType2;
	}


	public String getPayCondition() {
		return this.payCondition;
	}


	public void setPayCondition(String payCondition) {
		this.payCondition = payCondition;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public BigDecimal getRate2() {
		return this.rate2;
	}


	public void setRate2(BigDecimal rate2) {
		this.rate2 = rate2;
	}


	public String getRateSet() {
		return this.rateSet;
	}


	public void setRateSet(String rateSet) {
		this.rateSet = rateSet;
	}


	public String getPayOffSide() {
		return this.payOffSide;
	}


	public void setPayOffSide(String payOffSide) {
		this.payOffSide = payOffSide;
	}


	public String getSettlementCurrency() {
		return this.settlementCurrency;
	}


	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public String getStyle() {
		return this.style;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	public String getStyle2() {
		return this.style2;
	}


	public void setStyle2(String style2) {
		this.style2 = style2;
	}


	public String getSwapType() {
		return this.swapType;
	}


	public void setSwapType(String swapType) {
		this.swapType = swapType;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public String getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(String underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public String getUnderlyingSecurityType() {
		return this.underlyingSecurityType;
	}


	public void setUnderlyingSecurityType(String underlyingSecurityType) {
		this.underlyingSecurityType = underlyingSecurityType;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
