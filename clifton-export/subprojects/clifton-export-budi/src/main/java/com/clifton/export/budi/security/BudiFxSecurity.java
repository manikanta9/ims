package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiFxSecurity extends BudiBaseSecurity {

	private String action;
	private String barrierDirection;
	private Date barrierEndDate;
	private BigDecimal barrierLevel;
	private BigDecimal barrierRebate;
	private Date barrierStartDate;
	private String barrierType;
	private String counterparty;
	private String currency;
	private String dealTypePath;
	private Date deliveryDate;
	private Date deliveryDate2;
	private String delivery;
	private Boolean digital;
	private String exerciseStyle;
	private Date expiryDate;
	private Date fixingEndDate;
	private Date fixingStartDate;
	private String flags;
	private String multiLegId;
	private BigDecimal notional;
	private BigDecimal notional2;
	private BigDecimal optionStrike;
	private BigDecimal optionStrike2;
	private String optionType;
	private String payCondition;
	private String payFrequency;
	private BigDecimal rate;
	private BigDecimal rate2;
	private String rateSet;
	private String settlementCurrency;
	private String payOffSide;
	private Date tradeDate;
	private String underlyingSecurity;
	private String xRefContext;


	public BudiFxSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.FOREIGN_EXCHANGE, instrumentType);
	}


	public String getAction() {
		return this.action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getBarrierDirection() {
		return this.barrierDirection;
	}


	public void setBarrierDirection(String barrierDirection) {
		this.barrierDirection = barrierDirection;
	}


	public Date getBarrierEndDate() {
		return this.barrierEndDate;
	}


	public void setBarrierEndDate(Date barrierEndDate) {
		this.barrierEndDate = barrierEndDate;
	}


	public BigDecimal getBarrierLevel() {
		return this.barrierLevel;
	}


	public void setBarrierLevel(BigDecimal barrierLevel) {
		this.barrierLevel = barrierLevel;
	}


	public BigDecimal getBarrierRebate() {
		return this.barrierRebate;
	}


	public void setBarrierRebate(BigDecimal barrierRebate) {
		this.barrierRebate = barrierRebate;
	}


	public Date getBarrierStartDate() {
		return this.barrierStartDate;
	}


	public void setBarrierStartDate(Date barrierStartDate) {
		this.barrierStartDate = barrierStartDate;
	}


	public String getBarrierType() {
		return this.barrierType;
	}


	public void setBarrierType(String barrierType) {
		this.barrierType = barrierType;
	}


	public String getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getDealTypePath() {
		return this.dealTypePath;
	}


	public void setDealTypePath(String dealTypePath) {
		this.dealTypePath = dealTypePath;
	}


	public Date getDeliveryDate() {
		return this.deliveryDate;
	}


	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public Date getDeliveryDate2() {
		return this.deliveryDate2;
	}


	public void setDeliveryDate2(Date deliveryDate2) {
		this.deliveryDate2 = deliveryDate2;
	}


	public String getDelivery() {
		return this.delivery;
	}


	public void setDelivery(String delivery) {
		this.delivery = delivery;
	}


	public Boolean getDigital() {
		return this.digital;
	}


	public void setDigital(Boolean digital) {
		this.digital = digital;
	}


	public String getExerciseStyle() {
		return this.exerciseStyle;
	}


	public void setExerciseStyle(String exerciseStyle) {
		this.exerciseStyle = exerciseStyle;
	}


	public Date getExpiryDate() {
		return this.expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Date getFixingEndDate() {
		return this.fixingEndDate;
	}


	public void setFixingEndDate(Date fixingEndDate) {
		this.fixingEndDate = fixingEndDate;
	}


	public Date getFixingStartDate() {
		return this.fixingStartDate;
	}


	public void setFixingStartDate(Date fixingStartDate) {
		this.fixingStartDate = fixingStartDate;
	}


	public String getFlags() {
		return this.flags;
	}


	public void setFlags(String flags) {
		this.flags = flags;
	}


	public String getMultiLegId() {
		return this.multiLegId;
	}


	public void setMultiLegId(String multiLegId) {
		this.multiLegId = multiLegId;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public BigDecimal getNotional2() {
		return this.notional2;
	}


	public void setNotional2(BigDecimal notional2) {
		this.notional2 = notional2;
	}


	public BigDecimal getOptionStrike() {
		return this.optionStrike;
	}


	public void setOptionStrike(BigDecimal optionStrike) {
		this.optionStrike = optionStrike;
	}


	public BigDecimal getOptionStrike2() {
		return this.optionStrike2;
	}


	public void setOptionStrike2(BigDecimal optionStrike2) {
		this.optionStrike2 = optionStrike2;
	}


	public String getOptionType() {
		return this.optionType;
	}


	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}


	public String getPayCondition() {
		return this.payCondition;
	}


	public void setPayCondition(String payCondition) {
		this.payCondition = payCondition;
	}


	public String getPayFrequency() {
		return this.payFrequency;
	}


	public void setPayFrequency(String payFrequency) {
		this.payFrequency = payFrequency;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public BigDecimal getRate2() {
		return this.rate2;
	}


	public void setRate2(BigDecimal rate2) {
		this.rate2 = rate2;
	}


	public String getRateSet() {
		return this.rateSet;
	}


	public void setRateSet(String rateSet) {
		this.rateSet = rateSet;
	}


	public String getSettlementCurrency() {
		return this.settlementCurrency;
	}


	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}


	public String getPayOffSide() {
		return this.payOffSide;
	}


	public void setPayOffSide(String payOffSide) {
		this.payOffSide = payOffSide;
	}


	public Date getTradeDate() {
		return this.tradeDate;
	}


	public void setTradeDate(Date tradeDate) {
		this.tradeDate = tradeDate;
	}


	public String getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(String underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
