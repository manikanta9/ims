package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiFiPpcrSecurity extends BudiBaseSecurity {

	private Date announcementDate;
	private BigDecimal ask;
	private Boolean autoUpdate;
	private BigDecimal askBidSpread;
	private BigDecimal benchmarkMultiplier;
	private BigDecimal bid;
	private Boolean bVALEnabledPricing;
	private BigDecimal calcType;
	private String calendar;
	private Boolean cashPriced;
	private String cashType;
	private String collateralType;
	private String country;
	private BigDecimal coupon;
	private String couponType;
	private String currency;
	private Date date;
	private String dateCountBasis;
	private Boolean discrete;
	private Date endDate;
	private String estRating;
	private Date firstCouponDate;
	private Date firstSettleDate;
	private String fixFrequency;
	private BigDecimal floaterCap;
	private BigDecimal floaterFloor;
	private String index;
	private String industryCode;
	private Date interestAccrualDate;
	private BigDecimal issrBbid;
	private BigDecimal issuePrice;
	private String issuerIndustry;
	private BigDecimal issueSize;
	private String issueType;
	private BigDecimal issueYield;
	private Date lastCouponDate;
	private Date maturityDate;
	private String maturityType;
	private Integer maximumNotificationDays;
	private BigDecimal minimumIncrement;
	private BigDecimal minimumNotificationDays;
	private BigDecimal minimumPiece;
	private Date nextCouponDate;
	private String otcTicker;
	private BigDecimal outstandingSize;
	private BigDecimal parAmount;
	private Integer payDay;
	private String paymentTerm;
	private BigDecimal principal;
	private BigDecimal pubAmount;
	private BigDecimal rate;
	private BigDecimal redemptionValue;
	private Integer refixday;
	private Date refundingDate;
	private Date restructureDate;
	private String series;
	private String shortName;
	private BigDecimal sinkAmount;
	private String sinkFrequency;
	private String sinkType;
	private BigDecimal spread;
	private Date startDate;
	private BigDecimal taxRate;
	private Boolean tradedFlat;
	private String type;
	private Boolean unitTraded;
	private BigDecimal voluntaryFactor;
	private String xRefContext;


	public BudiFiPpcrSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.FIXED_INCOME_INSTRUMENTS, instrumentType);
	}


	public Date getAnnouncementDate() {
		return this.announcementDate;
	}


	public void setAnnouncementDate(Date announcementDate) {
		this.announcementDate = announcementDate;
	}


	public BigDecimal getAsk() {
		return this.ask;
	}


	public void setAsk(BigDecimal ask) {
		this.ask = ask;
	}


	public Boolean getAutoUpdate() {
		return this.autoUpdate;
	}


	public void setAutoUpdate(Boolean autoUpdate) {
		this.autoUpdate = autoUpdate;
	}


	public BigDecimal getAskBidSpread() {
		return this.askBidSpread;
	}


	public void setAskBidSpread(BigDecimal askBidSpread) {
		this.askBidSpread = askBidSpread;
	}


	public BigDecimal getBenchmarkMultiplier() {
		return this.benchmarkMultiplier;
	}


	public void setBenchmarkMultiplier(BigDecimal benchmarkMultiplier) {
		this.benchmarkMultiplier = benchmarkMultiplier;
	}


	public BigDecimal getBid() {
		return this.bid;
	}


	public void setBid(BigDecimal bid) {
		this.bid = bid;
	}


	public Boolean getbVALEnabledPricing() {
		return this.bVALEnabledPricing;
	}


	public void setbVALEnabledPricing(Boolean bVALEnabledPricing) {
		this.bVALEnabledPricing = bVALEnabledPricing;
	}


	public BigDecimal getCalcType() {
		return this.calcType;
	}


	public void setCalcType(BigDecimal calcType) {
		this.calcType = calcType;
	}


	public String getCalendar() {
		return this.calendar;
	}


	public void setCalendar(String calendar) {
		this.calendar = calendar;
	}


	public Boolean getCashPriced() {
		return this.cashPriced;
	}


	public void setCashPriced(Boolean cashPriced) {
		this.cashPriced = cashPriced;
	}


	public String getCashType() {
		return this.cashType;
	}


	public void setCashType(String cashType) {
		this.cashType = cashType;
	}


	public String getCollateralType() {
		return this.collateralType;
	}


	public void setCollateralType(String collateralType) {
		this.collateralType = collateralType;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public BigDecimal getCoupon() {
		return this.coupon;
	}


	public void setCoupon(BigDecimal coupon) {
		this.coupon = coupon;
	}


	public String getCouponType() {
		return this.couponType;
	}


	public void setCouponType(String couponType) {
		this.couponType = couponType;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getDateCountBasis() {
		return this.dateCountBasis;
	}


	public void setDateCountBasis(String dateCountBasis) {
		this.dateCountBasis = dateCountBasis;
	}


	public Boolean getDiscrete() {
		return this.discrete;
	}


	public void setDiscrete(Boolean discrete) {
		this.discrete = discrete;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getEstRating() {
		return this.estRating;
	}


	public void setEstRating(String estRating) {
		this.estRating = estRating;
	}


	public Date getFirstCouponDate() {
		return this.firstCouponDate;
	}


	public void setFirstCouponDate(Date firstCouponDate) {
		this.firstCouponDate = firstCouponDate;
	}


	public Date getFirstSettleDate() {
		return this.firstSettleDate;
	}


	public void setFirstSettleDate(Date firstSettleDate) {
		this.firstSettleDate = firstSettleDate;
	}


	public String getFixFrequency() {
		return this.fixFrequency;
	}


	public void setFixFrequency(String fixFrequency) {
		this.fixFrequency = fixFrequency;
	}


	public BigDecimal getFloaterCap() {
		return this.floaterCap;
	}


	public void setFloaterCap(BigDecimal floaterCap) {
		this.floaterCap = floaterCap;
	}


	public BigDecimal getFloaterFloor() {
		return this.floaterFloor;
	}


	public void setFloaterFloor(BigDecimal floaterFloor) {
		this.floaterFloor = floaterFloor;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	public String getIndustryCode() {
		return this.industryCode;
	}


	public void setIndustryCode(String industryCode) {
		this.industryCode = industryCode;
	}


	public Date getInterestAccrualDate() {
		return this.interestAccrualDate;
	}


	public void setInterestAccrualDate(Date interestAccrualDate) {
		this.interestAccrualDate = interestAccrualDate;
	}


	public BigDecimal getIssrBbid() {
		return this.issrBbid;
	}


	public void setIssrBbid(BigDecimal issrBbid) {
		this.issrBbid = issrBbid;
	}


	public BigDecimal getIssuePrice() {
		return this.issuePrice;
	}


	public void setIssuePrice(BigDecimal issuePrice) {
		this.issuePrice = issuePrice;
	}


	public String getIssuerIndustry() {
		return this.issuerIndustry;
	}


	public void setIssuerIndustry(String issuerIndustry) {
		this.issuerIndustry = issuerIndustry;
	}


	public BigDecimal getIssueSize() {
		return this.issueSize;
	}


	public void setIssueSize(BigDecimal issueSize) {
		this.issueSize = issueSize;
	}


	public String getIssueType() {
		return this.issueType;
	}


	public void setIssueType(String issueType) {
		this.issueType = issueType;
	}


	public BigDecimal getIssueYield() {
		return this.issueYield;
	}


	public void setIssueYield(BigDecimal issueYield) {
		this.issueYield = issueYield;
	}


	public Date getLastCouponDate() {
		return this.lastCouponDate;
	}


	public void setLastCouponDate(Date lastCouponDate) {
		this.lastCouponDate = lastCouponDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public String getMaturityType() {
		return this.maturityType;
	}


	public void setMaturityType(String maturityType) {
		this.maturityType = maturityType;
	}


	public Integer getMaximumNotificationDays() {
		return this.maximumNotificationDays;
	}


	public void setMaximumNotificationDays(Integer maximumNotificationDays) {
		this.maximumNotificationDays = maximumNotificationDays;
	}


	public BigDecimal getMinimumIncrement() {
		return this.minimumIncrement;
	}


	public void setMinimumIncrement(BigDecimal minimumIncrement) {
		this.minimumIncrement = minimumIncrement;
	}


	public BigDecimal getMinimumNotificationDays() {
		return this.minimumNotificationDays;
	}


	public void setMinimumNotificationDays(BigDecimal minimumNotificationDays) {
		this.minimumNotificationDays = minimumNotificationDays;
	}


	public BigDecimal getMinimumPiece() {
		return this.minimumPiece;
	}


	public void setMinimumPiece(BigDecimal minimumPiece) {
		this.minimumPiece = minimumPiece;
	}


	public Date getNextCouponDate() {
		return this.nextCouponDate;
	}


	public void setNextCouponDate(Date nextCouponDate) {
		this.nextCouponDate = nextCouponDate;
	}


	public String getOtcTicker() {
		return this.otcTicker;
	}


	public void setOtcTicker(String otcTicker) {
		this.otcTicker = otcTicker;
	}


	public BigDecimal getOutstandingSize() {
		return this.outstandingSize;
	}


	public void setOutstandingSize(BigDecimal outstandingSize) {
		this.outstandingSize = outstandingSize;
	}


	public BigDecimal getParAmount() {
		return this.parAmount;
	}


	public void setParAmount(BigDecimal parAmount) {
		this.parAmount = parAmount;
	}


	public Integer getPayDay() {
		return this.payDay;
	}


	public void setPayDay(Integer payDay) {
		this.payDay = payDay;
	}


	public String getPaymentTerm() {
		return this.paymentTerm;
	}


	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}


	public BigDecimal getPrincipal() {
		return this.principal;
	}


	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}


	public BigDecimal getPubAmount() {
		return this.pubAmount;
	}


	public void setPubAmount(BigDecimal pubAmount) {
		this.pubAmount = pubAmount;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public BigDecimal getRedemptionValue() {
		return this.redemptionValue;
	}


	public void setRedemptionValue(BigDecimal redemptionValue) {
		this.redemptionValue = redemptionValue;
	}


	public Integer getRefixday() {
		return this.refixday;
	}


	public void setRefixday(Integer refixday) {
		this.refixday = refixday;
	}


	public Date getRefundingDate() {
		return this.refundingDate;
	}


	public void setRefundingDate(Date refundingDate) {
		this.refundingDate = refundingDate;
	}


	public Date getRestructureDate() {
		return this.restructureDate;
	}


	public void setRestructureDate(Date restructureDate) {
		this.restructureDate = restructureDate;
	}


	public String getSeries() {
		return this.series;
	}


	public void setSeries(String series) {
		this.series = series;
	}


	public String getShortName() {
		return this.shortName;
	}


	public void setShortName(String shortName) {
		this.shortName = shortName;
	}


	public BigDecimal getSinkAmount() {
		return this.sinkAmount;
	}


	public void setSinkAmount(BigDecimal sinkAmount) {
		this.sinkAmount = sinkAmount;
	}


	public String getSinkFrequency() {
		return this.sinkFrequency;
	}


	public void setSinkFrequency(String sinkFrequency) {
		this.sinkFrequency = sinkFrequency;
	}


	public String getSinkType() {
		return this.sinkType;
	}


	public void setSinkType(String sinkType) {
		this.sinkType = sinkType;
	}


	public BigDecimal getSpread() {
		return this.spread;
	}


	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public BigDecimal getTaxRate() {
		return this.taxRate;
	}


	public void setTaxRate(BigDecimal taxRate) {
		this.taxRate = taxRate;
	}


	public Boolean getTradedFlat() {
		return this.tradedFlat;
	}


	public void setTradedFlat(Boolean tradedFlat) {
		this.tradedFlat = tradedFlat;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public Boolean getUnitTraded() {
		return this.unitTraded;
	}


	public void setUnitTraded(Boolean unitTraded) {
		this.unitTraded = unitTraded;
	}


	public BigDecimal getVoluntaryFactor() {
		return this.voluntaryFactor;
	}


	public void setVoluntaryFactor(BigDecimal voluntaryFactor) {
		this.voluntaryFactor = voluntaryFactor;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
