package com.clifton.export.budi.transaction;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.security.BudiSecurity;


/**
 * @author TerryS
 */
public interface BudiSecurityPopulator<T> {

	public BudiSecurity populate(BudiInstrumentTypes type, T transaction);
}
