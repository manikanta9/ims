package com.clifton.export.budi.template;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.template.block.BudiBlock;
import com.clifton.export.budi.template.block.BudiBlockTypes;
import com.clifton.export.budi.template.field.BudiDateField;
import com.clifton.export.budi.template.field.BudiField;
import com.clifton.export.budi.template.field.BudiNumericField;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * @author TerryS
 */
public class BudiTemplateFactory {

	private static final Map<BudiInstrumentTypes, BudiTemplate> TEMPLATE_CACHE = new ConcurrentHashMap<>();
	private static final Map<String, Map<BudiField.Visibility, BudiField>> FIELD_CACHE = new ConcurrentHashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BudiTemplate buildTemplate(BudiInstrumentTypes type) {
		return TEMPLATE_CACHE.computeIfAbsent(type, BudiTemplateFactory::doBuildTemplate);
	}


	public static void flush() {
		TEMPLATE_CACHE.clear();
		FIELD_CACHE.clear();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@SuppressWarnings("DuplicatedCode")
	private static BudiTemplate doBuildTemplate(BudiInstrumentTypes type) {
		BudiTemplate results;
		switch (type) {
			case FX_MULTILEG:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_CASH:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_SPOT:
			case FX_FORWARD:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getNumericField("Rate", BudiField.Visibility.MANDATORY),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getDateField("DeliveryDate", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_FORWARD_AVG:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.MANDATORY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.OPTIONAL),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getDateField("FixingStartDate", BudiField.Visibility.MANDATORY),
								getDateField("FixingEndDate", BudiField.Visibility.MANDATORY),
								getStringField("RateSet", BudiField.Visibility.MANDATORY),
								getStringField("PayFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("DealTypePath", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_OPTION_VANILLA:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.CONDITIONALLY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getStringField("ExerciseStyle", BudiField.Visibility.OPTIONAL),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getStringField("Digital", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_ACCRUAL:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.OPTIONAL),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getDateField("FixingStartDate", BudiField.Visibility.MANDATORY),
								getDateField("FixingEndDate", BudiField.Visibility.MANDATORY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("PayFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("DealTypePath", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_ACCUMULATOR:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.OPTIONAL),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getDateField("FixingStartDate", BudiField.Visibility.MANDATORY),
								getDateField("FixingEndDate", BudiField.Visibility.MANDATORY),
								getStringField("BarrierDirection", BudiField.Visibility.CONDITIONALLY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("PayFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("DealTypePath", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_TOUCH:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getStringField("BarrierDirection", BudiField.Visibility.CONDITIONALLY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getDateField("BarrierStartDate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierEndDate", BudiField.Visibility.OPTIONAL),
								getStringField("PayCondition", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_NO_TOUCH:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getStringField("BarrierDirection", BudiField.Visibility.CONDITIONALLY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getDateField("BarrierStartDate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierEndDate", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_BARRIER_DBL:
			case FX_BARRIER:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getStringField("BarrierDirection", BudiField.Visibility.MANDATORY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("BarrierType", BudiField.Visibility.OPTIONAL),
								getNumericField("BarrierRebate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierStartDate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierEndDate", BudiField.Visibility.OPTIONAL),
								getStringField("Digital", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_AVERAGE:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.OPTIONAL),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getDateField("FixingStartDate", BudiField.Visibility.MANDATORY),
								getDateField("FixingEndDate", BudiField.Visibility.MANDATORY),
								getStringField("RateSet", BudiField.Visibility.OPTIONAL),
								getStringField("PayFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("DealTypePath", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_PARTICIPATING_FWD:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getNumericField("Notional2", BudiField.Visibility.MANDATORY),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getStringField("ExerciseStyle", BudiField.Visibility.OPTIONAL),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_FWD_EXTRA:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getStringField("ExerciseStyle", BudiField.Visibility.OPTIONAL),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getStringField("BarrierDirection", BudiField.Visibility.MANDATORY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("BarrierType", BudiField.Visibility.OPTIONAL),
								getNumericField("BarrierRebate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierStartDate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierEndDate", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_EAKO:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("MultilegId", BudiField.Visibility.CONDITIONALLY),
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("PayCondition", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_DCD:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("Rate", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_FADER:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Flags", BudiField.Visibility.CONDITIONALLY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getDateField("FixingStartDate", BudiField.Visibility.MANDATORY),
								getDateField("FixingEndDate", BudiField.Visibility.MANDATORY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("PayFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("DealTypePath", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case FX_STRADDLE:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getStringField("ExerciseStyle", BudiField.Visibility.OPTIONAL),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_STRANGLE:
			case FX_COLLAR:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike2", BudiField.Visibility.MANDATORY),
								getStringField("ExerciseStyle", BudiField.Visibility.OPTIONAL),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_ENHANCED_COLLAR:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getStringField("OptionType", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike", BudiField.Visibility.MANDATORY),
								getNumericField("OptionStrike2", BudiField.Visibility.MANDATORY),
								getStringField("ExerciseStyle", BudiField.Visibility.OPTIONAL),
								getDateField("ExpiryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate", BudiField.Visibility.OPTIONAL),
								getStringField("BarrierDirection", BudiField.Visibility.MANDATORY),
								getNumericField("BarrierLevel", BudiField.Visibility.MANDATORY),
								getStringField("BarrierType", BudiField.Visibility.MANDATORY),
								getNumericField("BarrierRebate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierStartDate", BudiField.Visibility.OPTIONAL),
								getDateField("BarrierEndDate", BudiField.Visibility.OPTIONAL),
								getStringField("Digital", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case FX_SWAP:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.CONCATENATE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Delivery", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("TradeDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING,
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getNumericField("Rate", BudiField.Visibility.MANDATORY),
								getNumericField("Rate2", BudiField.Visibility.MANDATORY),
								getStringField("UnderlyingSecurity", BudiField.Visibility.MANDATORY),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.OPTION,
								getDateField("DeliveryDate", BudiField.Visibility.MANDATORY),
								getDateField("DeliveryDate2", BudiField.Visibility.MANDATORY)
						)
				);
				break;
			case IR_FLFL:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.REFERENCE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Action", BudiField.Visibility.MANDATORY),
								getStringField("XrefContext", BudiField.Visibility.OPTIONAL),
								getStringField("OTCTicker", BudiField.Visibility.OPTIONAL),
								getStringField("CentralCounterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Counterparty", BudiField.Visibility.OPTIONAL),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getDateField("EffectiveDate", BudiField.Visibility.OPTIONAL),
								getDateField("MaturityDate", BudiField.Visibility.MANDATORY),
								getStringField("PrincipalExchange", BudiField.Visibility.OPTIONAL),
								getStringField("DealCurrency", BudiField.Visibility.OPTIONAL),
								getNumericField("DealNotional", BudiField.Visibility.OPTIONAL),
								getStringField("SettlementCurrency", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.FEES, true,
								getStringField("Identifier", BudiField.Visibility.OPTIONAL),
								getDateField("Date", BudiField.Visibility.OPTIONAL),
								getStringField("PayOffSide", BudiField.Visibility.OPTIONAL),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getNumericField("Amount", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING, true,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("PayOffSide", BudiField.Visibility.MANDATORY),
								getNumericField("Notional", BudiField.Visibility.MANDATORY),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getNumericField("Spread", BudiField.Visibility.OPTIONAL),
								getStringField("Index", BudiField.Visibility.MANDATORY),
								getNumericField("Leverage", BudiField.Visibility.OPTIONAL),
								getStringField("PayFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("DayCountBasis", BudiField.Visibility.MANDATORY),
								getDateField("FirstPaymentDate", BudiField.Visibility.OPTIONAL),
								getDateField("NextLastPaymentDate", BudiField.Visibility.OPTIONAL),
								getStringField("ResetFrequency", BudiField.Visibility.OPTIONAL),
								getStringField("IsArrears", BudiField.Visibility.OPTIONAL),
								getNumericField("DaysBeforeAccrual", BudiField.Visibility.OPTIONAL),
								getStringField("AccrualDayAdjustment", BudiField.Visibility.OPTIONAL),
								getStringField("BusinessDayAdjustment", BudiField.Visibility.OPTIONAL),
								getStringField("RollDateConvention", BudiField.Visibility.OPTIONAL),
								getStringField("CustomDateGen", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.SCHEDULE, true,
								getStringField("Identifier", BudiField.Visibility.OPTIONAL),
								getStringField("Type", BudiField.Visibility.OPTIONAL),
								getDateField("StartDate", BudiField.Visibility.OPTIONAL),
								getDateField("EndDate", BudiField.Visibility.OPTIONAL),
								getDateField("PaymentDate", BudiField.Visibility.OPTIONAL),
								getNumericField("Amount", BudiField.Visibility.OPTIONAL),
								getNumericField("Rate", BudiField.Visibility.OPTIONAL),
								getNumericField("Balance", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
			case UNKNOWN:
			default:
				results = new BudiTemplate(type, BudiBlock.BlockFormat.REFERENCE,
						new BudiBlock(type, BudiBlockTypes.HEADER,
								getStringField("Identifier", BudiField.Visibility.MANDATORY),
								getStringField("Description", BudiField.Visibility.OPTIONAL),
								getStringField("InstType", BudiField.Visibility.MANDATORY),
								getStringField("RiskSecurityType", BudiField.Visibility.MANDATORY),
								getStringField("Country", BudiField.Visibility.OPTIONAL),
								getStringField("Ticker", BudiField.Visibility.OPTIONAL),
								getDateField("DatedDate", BudiField.Visibility.OPTIONAL),
								getDateField("MaturityDate", BudiField.Visibility.OPTIONAL)
						),
						new BudiBlock(type, BudiBlockTypes.UNDERLYING, true,
								getStringField("CurrencyOfRisk", BudiField.Visibility.OPTIONAL),
								getStringField("Class4", BudiField.Visibility.OPTIONAL),
								getNumericField("EquityIndustryName", BudiField.Visibility.OPTIONAL),
								getStringField("Currency", BudiField.Visibility.OPTIONAL),
								getNumericField("Rate", BudiField.Visibility.OPTIONAL),
								getStringField("MoodysRating", BudiField.Visibility.OPTIONAL),
								getNumericField("SandPRating", BudiField.Visibility.OPTIONAL),
								getStringField("FitchRating", BudiField.Visibility.OPTIONAL)
						)
				);
				break;
		}
		return results;
	}


	private static BudiField getStringField(String name, BudiField.Visibility visibility) {
		Map<BudiField.Visibility, BudiField> visibilityMap = FIELD_CACHE.computeIfAbsent(name, s -> new ConcurrentHashMap<>());
		return visibilityMap.computeIfAbsent(visibility, v -> new BudiField(name, v));
	}


	private static BudiField getDateField(String name, BudiField.Visibility visibility) {
		Map<BudiField.Visibility, BudiField> visibilityMap = FIELD_CACHE.computeIfAbsent(name, s -> new ConcurrentHashMap<>());
		return visibilityMap.computeIfAbsent(visibility, v -> new BudiDateField(name, v));
	}


	private static BudiField getNumericField(String name, BudiField.Visibility visibility) {
		Map<BudiField.Visibility, BudiField> visibilityMap = FIELD_CACHE.computeIfAbsent(name, s -> new ConcurrentHashMap<>());
		return visibilityMap.computeIfAbsent(visibility, v -> new BudiNumericField(name, v));
	}
}
