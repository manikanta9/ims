package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;
import com.clifton.export.budi.template.block.BudiBlockTypes;
import com.clifton.export.budi.template.block.BudiRepeatingBlock;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;


/**
 * @author TerryS
 */
public class BudiIrSecurity extends BudiBaseSecurity {

	private String accrualDayAdjustment;
	private String action;
	private BigDecimal amount;
	private String asset;
	private BigDecimal balance;
	private String businessDayAdjustment;
	private String centralCounterparty;
	private String compoundingFrequency;
	private String compoundingType;
	private String counterparty;
	private String currency;
	private String customDateGen;
	private Date date;
	private String dayCountBasis;
	private Integer daysBeforeAccrual;
	private Integer daysToNotify;
	private String dealCurrency;
	private BigDecimal dealNotional;
	private String deliveryMethod;
	private Date effectiveDate;
	private Date endDate;
	private Date firstPaymentDate;
	private BigDecimal fvNotional;
	private String index;
	private String isArrears;
	private Integer lag;
	private BigDecimal leverage;
	private Date maturityDate;
	private String multilegId;
	private Date nextLastPaymentDate;
	private BigDecimal notional;
	private String optionCurrency;
	private String optionDirection;
	private String optionExerciseFrequency;
	private Date optionExpiryDate;
	private Date optionFirstExercise;
	private BigDecimal optionNotional;
	private BigDecimal optionStrike;
	private BigDecimal getOptionStrike2;
	private String optionStyle;
	private String otcTicker;
	private String payFrequency;
	private Date paymentDate;
	private String payoffSide;
	private BigDecimal premiumAtExpiry;
	private String principalExchange;
	private BigDecimal rate;
	private String resetFrequency;
	private String rollDateConvention;
	private String settlementCurrency;
	private BigDecimal spread;
	private Date startDate;
	private String type;
	private String xRefContext;

	@BudiRepeatingBlock(blockType = BudiBlockTypes.UNDERLYING)
	private List<BudiIrSecurity> underlying;

	@BudiRepeatingBlock(blockType = BudiBlockTypes.FEES)
	private List<BudiIrSecurity> fees;

	@BudiRepeatingBlock(blockType = BudiBlockTypes.SCHEDULE)
	private List<BudiIrSecurity> schedule;


	public BudiIrSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.INTEREST_RATES, instrumentType);
	}


	public String getAccrualDayAdjustment() {
		return this.accrualDayAdjustment;
	}


	public void setAccrualDayAdjustment(String accrualDayAdjustment) {
		this.accrualDayAdjustment = accrualDayAdjustment;
	}


	public String getAction() {
		return this.action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public BigDecimal getAmount() {
		return this.amount;
	}


	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}


	public String getAsset() {
		return this.asset;
	}


	public void setAsset(String asset) {
		this.asset = asset;
	}


	public BigDecimal getBalance() {
		return this.balance;
	}


	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}


	public String getBusinessDayAdjustment() {
		return this.businessDayAdjustment;
	}


	public void setBusinessDayAdjustment(String businessDayAdjustment) {
		this.businessDayAdjustment = businessDayAdjustment;
	}


	public String getCentralCounterparty() {
		return this.centralCounterparty;
	}


	public void setCentralCounterparty(String centralCounterparty) {
		this.centralCounterparty = centralCounterparty;
	}


	public String getCompoundingFrequency() {
		return this.compoundingFrequency;
	}


	public void setCompoundingFrequency(String compoundingFrequency) {
		this.compoundingFrequency = compoundingFrequency;
	}


	public String getCompoundingType() {
		return this.compoundingType;
	}


	public void setCompoundingType(String compoundingType) {
		this.compoundingType = compoundingType;
	}


	public String getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public String getCustomDateGen() {
		return this.customDateGen;
	}


	public void setCustomDateGen(String customDateGen) {
		this.customDateGen = customDateGen;
	}


	public String getDayCountBasis() {
		return this.dayCountBasis;
	}


	public void setDayCountBasis(String dayCountBasis) {
		this.dayCountBasis = dayCountBasis;
	}


	public Integer getDaysBeforeAccrual() {
		return this.daysBeforeAccrual;
	}


	public void setDaysBeforeAccrual(Integer daysBeforeAccrual) {
		this.daysBeforeAccrual = daysBeforeAccrual;
	}


	public Integer getDaysToNotify() {
		return this.daysToNotify;
	}


	public void setDaysToNotify(Integer daysToNotify) {
		this.daysToNotify = daysToNotify;
	}


	public String getDealCurrency() {
		return this.dealCurrency;
	}


	public void setDealCurrency(String dealCurrency) {
		this.dealCurrency = dealCurrency;
	}


	public BigDecimal getDealNotional() {
		return this.dealNotional;
	}


	public void setDealNotional(BigDecimal dealNotional) {
		this.dealNotional = dealNotional;
	}


	public String getDeliveryMethod() {
		return this.deliveryMethod;
	}


	public void setDeliveryMethod(String deliveryMethod) {
		this.deliveryMethod = deliveryMethod;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public Date getFirstPaymentDate() {
		return this.firstPaymentDate;
	}


	public void setFirstPaymentDate(Date firstPaymentDate) {
		this.firstPaymentDate = firstPaymentDate;
	}


	public BigDecimal getFvNotional() {
		return this.fvNotional;
	}


	public void setFvNotional(BigDecimal fvNotional) {
		this.fvNotional = fvNotional;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	public String getIsArrears() {
		return this.isArrears;
	}


	public void setIsArrears(String isArrears) {
		this.isArrears = isArrears;
	}


	public Integer getLag() {
		return this.lag;
	}


	public void setLag(Integer lag) {
		this.lag = lag;
	}


	public BigDecimal getLeverage() {
		return this.leverage;
	}


	public void setLeverage(BigDecimal leverage) {
		this.leverage = leverage;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public String getMultilegId() {
		return this.multilegId;
	}


	public void setMultilegId(String multilegId) {
		this.multilegId = multilegId;
	}


	public Date getNextLastPaymentDate() {
		return this.nextLastPaymentDate;
	}


	public void setNextLastPaymentDate(Date nextLastPaymentDate) {
		this.nextLastPaymentDate = nextLastPaymentDate;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public String getOptionCurrency() {
		return this.optionCurrency;
	}


	public void setOptionCurrency(String optionCurrency) {
		this.optionCurrency = optionCurrency;
	}


	public String getOptionDirection() {
		return this.optionDirection;
	}


	public void setOptionDirection(String optionDirection) {
		this.optionDirection = optionDirection;
	}


	public String getOptionExerciseFrequency() {
		return this.optionExerciseFrequency;
	}


	public void setOptionExerciseFrequency(String optionExerciseFrequency) {
		this.optionExerciseFrequency = optionExerciseFrequency;
	}


	public Date getOptionExpiryDate() {
		return this.optionExpiryDate;
	}


	public void setOptionExpiryDate(Date optionExpiryDate) {
		this.optionExpiryDate = optionExpiryDate;
	}


	public Date getOptionFirstExercise() {
		return this.optionFirstExercise;
	}


	public void setOptionFirstExercise(Date optionFirstExercise) {
		this.optionFirstExercise = optionFirstExercise;
	}


	public BigDecimal getOptionNotional() {
		return this.optionNotional;
	}


	public void setOptionNotional(BigDecimal optionNotional) {
		this.optionNotional = optionNotional;
	}


	public BigDecimal getOptionStrike() {
		return this.optionStrike;
	}


	public void setOptionStrike(BigDecimal optionStrike) {
		this.optionStrike = optionStrike;
	}


	public BigDecimal getGetOptionStrike2() {
		return this.getOptionStrike2;
	}


	public void setGetOptionStrike2(BigDecimal getOptionStrike2) {
		this.getOptionStrike2 = getOptionStrike2;
	}


	public String getOptionStyle() {
		return this.optionStyle;
	}


	public void setOptionStyle(String optionStyle) {
		this.optionStyle = optionStyle;
	}


	public String getOtcTicker() {
		return this.otcTicker;
	}


	public void setOtcTicker(String otcTicker) {
		this.otcTicker = otcTicker;
	}


	public String getPayFrequency() {
		return this.payFrequency;
	}


	public void setPayFrequency(String payFrequency) {
		this.payFrequency = payFrequency;
	}


	public Date getPaymentDate() {
		return this.paymentDate;
	}


	public void setPaymentDate(Date paymentDate) {
		this.paymentDate = paymentDate;
	}


	public String getPayoffSide() {
		return this.payoffSide;
	}


	public void setPayoffSide(String payoffSide) {
		this.payoffSide = payoffSide;
	}


	public BigDecimal getPremiumAtExpiry() {
		return this.premiumAtExpiry;
	}


	public void setPremiumAtExpiry(BigDecimal premiumAtExpiry) {
		this.premiumAtExpiry = premiumAtExpiry;
	}


	public String getPrincipalExchange() {
		return this.principalExchange;
	}


	public void setPrincipalExchange(String principalExchange) {
		this.principalExchange = principalExchange;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public String getResetFrequency() {
		return this.resetFrequency;
	}


	public void setResetFrequency(String resetFrequency) {
		this.resetFrequency = resetFrequency;
	}


	public String getRollDateConvention() {
		return this.rollDateConvention;
	}


	public void setRollDateConvention(String rollDateConvention) {
		this.rollDateConvention = rollDateConvention;
	}


	public String getSettlementCurrency() {
		return this.settlementCurrency;
	}


	public void setSettlementCurrency(String settlementCurrency) {
		this.settlementCurrency = settlementCurrency;
	}


	public BigDecimal getSpread() {
		return this.spread;
	}


	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}


	public List<BudiIrSecurity> getUnderlying() {
		return this.underlying;
	}


	public void setUnderlying(List<BudiIrSecurity> underlying) {
		this.underlying = underlying;
	}


	public List<BudiIrSecurity> getFees() {
		return this.fees;
	}


	public void setFees(List<BudiIrSecurity> fees) {
		this.fees = fees;
	}


	public List<BudiIrSecurity> getSchedule() {
		return this.schedule;
	}


	public void setSchedule(List<BudiIrSecurity> schedule) {
		this.schedule = schedule;
	}
}
