package com.clifton.export.budi.asset;


import com.clifton.export.budi.file.BudiFileFormats;


/**
 * InstType field - also corresponds to template
 *
 * @author TerryS
 */
public enum BudiInstrumentTypes {

	FI_BOND_ZERO(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Zero Bond", "Use for Zero Coupon Bonds"),
	FI_BOND_FIXED(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Fix Rate Bond", "Use for Fixed Rate Bonds"),
	FI_BOND_FLOATING(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Floating Bond", "Use for Floating Rate Bonds"),
	FI_BOND_CALL(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Callable Bond", "Use for Callable Bonds"),
	FI_BOND_PUT(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Putable Bond", "Use for Puttable Bond"),
	FI_BOND_INFLATION(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Inflation Bond", "Used for Inflation Indexed Bonds"),
	FI_PREFERRED(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Preferred Bond", "Used for fi preferred"),
	FI_MUNI(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Bond Muni", "Use for Municipal Bonds"),
	FI_BOND_STEP(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Step Bond", "Use for Step-up Bonds"),
	FI_BOND_SINK(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Sink Bond", "Use for Bonds with a sinking schedule"),
	FI_BOND_LOAN(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Fixed Bond", "Use for fixed income loan (ppcr)"),
	FI_BOND_REPO(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Repo Bond", "Use for fixed income repos (ppcr)"),
	FI_PREF_STOCK(BudiFileFormats.FIXED_INCOME_PPCR_UDI, "Preferred Stock Bond", "Use for Preferred Stock"),

	FI_SPOT(BudiFileFormats.BOND_OPTIONS, "Spot", "Use for fixed income Spot"),
	FI_FORWARD(BudiFileFormats.BOND_OPTIONS, "Forward", "Use for fixed income Forward"),
	FI_OPTION_VANILLA(BudiFileFormats.BOND_OPTIONS, "Vanilla", "Use for fixed Vanilla Option"),
	FI_WARRANT(BudiFileFormats.BOND_OPTIONS, "Warrant", "Use for fixed income Warrant"),
	FI_TOUCH(BudiFileFormats.BOND_OPTIONS, "Touch", "Use for fixed income Touch"),
	FI_NO_TOUCH(BudiFileFormats.BOND_OPTIONS, "No Touch", "Use for fixed income No Touch"),
	FI_BARRIER(BudiFileFormats.BOND_OPTIONS, "Barrier", "Use for fixed income Barrier"),

	IR_MULTILEG(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Multi-Leg", "Use for multi-leg derivatives."),
	IR_ZERO_COUPON(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Zero Coupon", "Use for Zero Coupon Interest Swap"),
	IR_FIFL(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Fixed Float Interest Rate Swap", "Interest Rate Swap Fixed to Float"),
	IR_FLFL(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Float Float Interest Rate Swap", "Interest Rate Swap Float to Float"),
	IR_FIFI(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Fixed Fixed Interest Rate Swap", "Interest Rate Swap Fixed to Fixed"),
	IR_OIS(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Interest Rate Overnight Swap", "Use for Interest Rate OIS"),
	IR_SWAPTION(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Swaption", "Interest Rate Swaption"),
	IR_XCY_FIFI(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Cross-currency Fix to Fix", "Use for Cross-currency Interest Rate Swap Fix to Fix"),
	IR_XCY_FLFL(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Cross-currency Float to Float", "Use for Cross-currency Interest Rate Swap Float to Float"),
	IR_XCY_FIFL(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Cross-currency Fix to Float", "Use for Cross-currency Interest Rate Swap Fix to Float"),
	IR_INFLATION(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Inflation Indexed Swap", "Use for Inflation Indexed Interest Rate Swap"),
	IR_TRS(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Total Return Swap", "Use for Total Return Swap"),
	IR_TRS_FI(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Fixed Total Return Swap", "Use for Total Return Swap fixed"),
	IR_STRADDLE(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Straddle", "Use for Interest Rate Cap Straddle"),
	IR_CANCELLABLESWAP(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Cancellable Swap", "Use for Interest Rate cancellable swap"),
	IR_CAP(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Interest Rate Cap", "Use for Interest Rate Cap"),
	IR_CAPSPREAD(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Cap Spread", "Use for Interest Rate Cap Spread"),
	IR_FLOOR(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Interest Rate Flooer", "Use for Interest Rate Floor"),
	IR_FLOORSPREAD(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Floor Spread", "Use for Interest Rate Floor Spread"),
	IR_COLLAR(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Rate Collar", "Use for Interest Rate Collar"),
	IR_FRA(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Rate Agreements", "Use for Interest Rate Forward Rate Agreement"),
	IR_LOAN(BudiFileFormats.INTEREST_RATE_DERIVATIVES, "Load Derivative", "Use for load derivatives."),

	CDS_SINGLE(BudiFileFormats.CREDIT_DERIVATIVES, "Single", "Use for Single Name Credit Default Swap"),
	CDS_INDEX(BudiFileFormats.CREDIT_DERIVATIVES, "Index", "Use for Credit Default Index Swap"),
	CDS_SINGLE_OPT(BudiFileFormats.CREDIT_DERIVATIVES, "Single Swaption", "Use for Single Name CDS Swaption"),
	CDS_INDEX_OPT(BudiFileFormats.CREDIT_DERIVATIVES, "Index Swaption", "Use for Credit Default Index Swaption"),

	FI_LOAN(BudiFileFormats.LOANS, "Loan", "Use for Loans"),

	EQ_FORWARD(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Forwards", "Use for Equity Forwards"),
	EQ_SPOT(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Spot", "Use for Equity Spot"),
	EQ_DIVIDEND_SWAP(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Dividend", "Use for Equity Dividend Swap"),
	EQ_OPTION_VANILLA(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Vanilla", "Use for Equity vanilla option"),
	EQ_ACCUMULATOR(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Accumulator", "Use for Equity Accumulator"),
	EQ_WARRANT(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Warrant", "Use for Equity Warrant"),
	EQ_CFD(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Contract for Differences", "Use for Contract for Differences"),
	EQ_CLIQUET(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Cliquet", "Use for Equity Cliquet"),
	EQ_LOOPBACK_OPTION(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Lookback", "Use for Equity Lookback option"),
	EQ_FORWARD_START(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Forward Start", "Use for Equity Forward Start"),
	EQ_FADER(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Fader", "Use for Equity Fader"),
	EQ_TOUCH(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Touch", "Use for Equity Touch"),
	EQ_NO_TOUCH(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "No Touch", "Use for Equity No Touch"),
	EQ_ACCRUAL(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Accrual", "Use for Equity Accrual"),
	EQ_AVERAGE(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Average", "Use for Equity average"),
	EQ_DIGITAL_ACCRUAL(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Digital Accrual", "Use for Equity Digital Accrual"),
	EQ_BARRIER(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Barrier", "Use for Equity Barrier"),
	EQ_VOLATILITY_SWAP(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Volatility", "Use for Equity Volatility Swap"),
	EQ_VARIANCE_SWAP(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Variance", "Use for Equity variance Swap"),
	EQ_STOCK(BudiFileFormats.EQUITY_AND_IR_FUTURE_OPTIONS, "Stock", "Use for Equity stock private"),

	MM_REPO(BudiFileFormats.MONEY_MARKETS, "Repos", "Use for Repos and Reverse Repos"),
	MM_INSTRUMENT(BudiFileFormats.MONEY_MARKETS, "Non-Repos", "Use for Money Markets (non Repos)"),

	FX_FORWARD(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Forward", "Use for FX Forward"),
	FX_SPOT(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Spot", "Use for FX Spot"),
	FX_SWAP(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Swap", "Use for FX Swap"),
	FX_TOUCH(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Touch", "Use for FX Touch"),
	FX_NO_TOUCH(BudiFileFormats.FX_AND_FX_OPTIONS, "FX No Touch", "Use for FX No Touch"),
	FX_ACCRUAL(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Accrual", "Use for FX Accrual"),
	FX_OPTION_VANILLA(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Vanilla Option", "Use for FX Option"),
	FX_BARRIER(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Barrier", "Use for FX single barriers"),
	FX_FADER(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Fader", "Use for FX Fader"),
	FX_BARRIER_DBL(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Barrier Double", "Use for FX Barriers double"),
	FX_COLLAR(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Collar", "Use for FX Collar"),
	FX_ENHANCED_COLLAR(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Enhanced Collar", "Use for FX Enhanced Collar"),
	FX_ACCUMULATOR(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Accumulator", "Use for FX Accumulator"),
	FX_STRADDLE(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Straddle", "Use for FX Straddle"),
	FX_AVERAGE(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Average", "Use for FX Average(Asian)"),
	FX_FWD_EXTRA(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Forward Extra", "Use for FX Forward Extra"),
	FX_DCD(BudiFileFormats.FX_AND_FX_OPTIONS, "FX DCD", "Use for FX Dual Currency Instrument"),
	FX_EAKO(BudiFileFormats.FX_AND_FX_OPTIONS, "FX EAKO", "Use for FX Euro-American KO"),
	FX_FORWARD_AVG(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Average Forward", "Use for FX Average Forward"),
	FX_PARTICIPATING_FWD(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Participating Forward", "Use for FX Participating Forward"),
	FX_STRANGLE(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Strangle", "Use for FX Strangle"),
	FX_MULTILEG(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Multi Leg", "Use for Multi-Leg."),
	FX_CASH(BudiFileFormats.FX_AND_FX_OPTIONS, "FX Cash", "Use for cash."),

	CMDTY_FORWARD(BudiFileFormats.COMMODITY_INSTRUMENTS, "Forward", "Use for Commodity Forward"),
	CMDTY_SWAP(BudiFileFormats.COMMODITY_INSTRUMENTS, "Swap", "Use for Commodity Swap"),
	CMDTY_TOUCH(BudiFileFormats.COMMODITY_INSTRUMENTS, "Touch", "Use for Commodity Touch"),
	CMDTY_NO_TOUCH(BudiFileFormats.COMMODITY_INSTRUMENTS, "No Touch", "Use for Commodity No Touch"),
	CMDTY_OPTION_VANILLA(BudiFileFormats.COMMODITY_INSTRUMENTS, "Vanilla", "Use for Commodity Option"),
	CMDTY_BARRIER(BudiFileFormats.COMMODITY_INSTRUMENTS, "Barrier", "Use for Commodity Barriers"),
	CMDTY_BARRIER_DBL(BudiFileFormats.COMMODITY_INSTRUMENTS, "Double Barrier", "Use for Commodity Double Barriers"),
	CMDTY_COLLAR(BudiFileFormats.COMMODITY_INSTRUMENTS, "Collar", "Use for Commodity Collar"),
	CMDTY_ACCUMULATOR(BudiFileFormats.COMMODITY_INSTRUMENTS, "Accumulator", "Use for Commodity Accumulator"),
	CMDTY_STRADDLE(BudiFileFormats.COMMODITY_INSTRUMENTS, "Straddle", "Use for Commodity Straddle"),
	CMDTY_AVERAGE(BudiFileFormats.COMMODITY_INSTRUMENTS, "Average", "Use for Commodity Average(Asian)"),
	CMDTY_ACCRUAL(BudiFileFormats.COMMODITY_INSTRUMENTS, "Accrual", "Use for Commodity Accrual"),
	CMDTY_FWD_EXTRA(BudiFileFormats.COMMODITY_INSTRUMENTS, "Forward Extra", "Use for Commodity Forward Extra"),
	CMDTY_ENHANCED_COLLAR(BudiFileFormats.COMMODITY_INSTRUMENTS, "Collar", "Use for Commodity Enhanced Collar"),
	CMDTY_PARTICIPATING_FWD(BudiFileFormats.COMMODITY_INSTRUMENTS, "Participating Forward", "Use for Commodity Participating Forward"),
	CMDTY_STRANGLE(BudiFileFormats.COMMODITY_INSTRUMENTS, "Strangle", "Use for Commodity Strangle"),

	NON_BUDI(BudiFileFormats.NON_BUDI, "Not a Budi", "Use for OTC securities that are not defined in BUDI formatted files."),
	UNKNOWN(BudiFileFormats.PLACEHOLDER, "UNKNOWN", "Use for security is not mapped to a specific BUDI instrument template.");


	private final BudiFileFormats format;
	private final String label;
	private final String description;


	BudiInstrumentTypes(BudiFileFormats format, String label, String description) {
		this.format = format;
		this.label = label == null ? name().toLowerCase() : label;
		this.description = description;
	}


	public BudiFileFormats getFormat() {
		return this.format;
	}


	public String getLabel() {
		return this.label;
	}


	public String getDescription() {
		return this.description;
	}
}
