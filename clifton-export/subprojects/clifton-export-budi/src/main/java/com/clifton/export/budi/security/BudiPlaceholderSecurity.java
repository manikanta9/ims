package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiPlaceholderSecurity extends BudiBaseSecurity {

	private String class4;
	private String country;
	private String currency;
	private String currencyOfRisk;
	private Date datedDate;
	private String equityIndustryName;
	private String fitchRating;
	private Date maturityDate;
	private String moodysRating;
	private String riskSecurityType;
	private BigDecimal rate;
	private String sandPRating;
	private String sector;
	private String ticker;


	public BudiPlaceholderSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.PLACEHOLDER, instrumentType);
	}


	public String getRiskSecurityType() {
		return this.riskSecurityType;
	}


	public void setRiskSecurityType(String riskSecurityType) {
		this.riskSecurityType = riskSecurityType;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public String getTicker() {
		return this.ticker;
	}


	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	public Date getDatedDate() {
		return this.datedDate;
	}


	public void setDatedDate(Date datedDate) {
		this.datedDate = datedDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public String getCurrencyOfRisk() {
		return this.currencyOfRisk;
	}


	public void setCurrencyOfRisk(String currencyOfRisk) {
		this.currencyOfRisk = currencyOfRisk;
	}


	public String getClass4() {
		return this.class4;
	}


	public void setClass4(String class4) {
		this.class4 = class4;
	}


	public String getEquityIndustryName() {
		return this.equityIndustryName;
	}


	public void setEquityIndustryName(String equityIndustryName) {
		this.equityIndustryName = equityIndustryName;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getSector() {
		return this.sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public String getMoodysRating() {
		return this.moodysRating;
	}


	public void setMoodysRating(String moodysRating) {
		this.moodysRating = moodysRating;
	}


	public String getSandPRating() {
		return this.sandPRating;
	}


	public void setSandPRating(String sandPRating) {
		this.sandPRating = sandPRating;
	}


	public String getFitchRating() {
		return this.fitchRating;
	}


	public void setFitchRating(String fitchRating) {
		this.fitchRating = fitchRating;
	}
}
