package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiFiSecurity extends BudiBaseSecurity {

	private BigDecimal baseCPI;
	private String bondType;
	private String classification;
	private String country;
	private BigDecimal coupon;
	private String creditEnhancement;
	private String currency;
	private String cusip;
	private Date date;
	private Date datedDate;
	private String dayCountBasis;
	private String dayMinimus;
	private Integer daysPrior;
	private BigDecimal dealSize;
	private String dualTaxState;
	private Date exDividendDate;
	private String fitchRating;
	private Date floaterCouponDate;
	private BigDecimal floaterCap;
	private BigDecimal floaterFloor;
	private String guaranteeType;
	private String index;
	private String inflationQuoteType;
	private BigDecimal interest;
	private String iSIN;
	private Date issueDate;
	private String issueName;
	private BigDecimal issrBdid;
	private BigDecimal issueSize;
	private BigDecimal lag;
	private Date lastCouponDate;
	private Date maturityDate;
	private BigDecimal mDCutPrc;
	private BigDecimal mDCutYld;
	private String MdFlag;
	private BigDecimal MdSpr;
	private BigDecimal minimumPiece;
	private BigDecimal minimumIncrement;
	private String moodysRating;
	private BigDecimal notional;
	private String obligor;
	private BigDecimal originalYield;
	private BigDecimal outstandingSize;
	private String oTCTicker;
	private BigDecimal parAmount;
	private String paymentTerm;
	private String placementType;
	private String pool;
	private BigDecimal prepaymentFactor;
	private String prepaymentSpeed;
	private BigDecimal price;
	private BigDecimal principal;
	private String purposeCode;
	private String purposeType;
	private BigDecimal rate;
	private String sandPRating;
	private String sector;
	private BigDecimal sinkAmount;
	private BigDecimal spread;
	private String state;
	private Integer subordinationType;
	private String taxStatus;
	private Boolean cashPriced;
	private String type;
	private BigDecimal wac;
	private BigDecimal wal;
	private BigDecimal wala;
	private BigDecimal wam;
	private String xrefContext;
	private BigDecimal ytm;


	public BudiFiSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.FIXED_INCOME_INSTRUMENTS, instrumentType);
	}


	public BigDecimal getBaseCPI() {
		return this.baseCPI;
	}


	public void setBaseCPI(BigDecimal baseCPI) {
		this.baseCPI = baseCPI;
	}


	public String getBondType() {
		return this.bondType;
	}


	public void setBondType(String bondType) {
		this.bondType = bondType;
	}


	public String getClassification() {
		return this.classification;
	}


	public void setClassification(String classification) {
		this.classification = classification;
	}


	public String getCountry() {
		return this.country;
	}


	public void setCountry(String country) {
		this.country = country;
	}


	public BigDecimal getCoupon() {
		return this.coupon;
	}


	public void setCoupon(BigDecimal coupon) {
		this.coupon = coupon;
	}


	public String getCreditEnhancement() {
		return this.creditEnhancement;
	}


	public void setCreditEnhancement(String creditEnhancement) {
		this.creditEnhancement = creditEnhancement;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getCusip() {
		return this.cusip;
	}


	public void setCusip(String cusip) {
		this.cusip = cusip;
	}


	public Date getDate() {
		return this.date;
	}


	public void setDate(Date date) {
		this.date = date;
	}


	public Date getDatedDate() {
		return this.datedDate;
	}


	public void setDatedDate(Date datedDate) {
		this.datedDate = datedDate;
	}


	public String getDayCountBasis() {
		return this.dayCountBasis;
	}


	public void setDayCountBasis(String dayCountBasis) {
		this.dayCountBasis = dayCountBasis;
	}


	public String getDayMinimus() {
		return this.dayMinimus;
	}


	public void setDayMinimus(String dayMinimus) {
		this.dayMinimus = dayMinimus;
	}


	public Integer getDaysPrior() {
		return this.daysPrior;
	}


	public void setDaysPrior(Integer daysPrior) {
		this.daysPrior = daysPrior;
	}


	public BigDecimal getDealSize() {
		return this.dealSize;
	}


	public void setDealSize(BigDecimal dealSize) {
		this.dealSize = dealSize;
	}


	public String getDualTaxState() {
		return this.dualTaxState;
	}


	public void setDualTaxState(String dualTaxState) {
		this.dualTaxState = dualTaxState;
	}


	public Date getExDividendDate() {
		return this.exDividendDate;
	}


	public void setExDividendDate(Date exDividendDate) {
		this.exDividendDate = exDividendDate;
	}


	public String getFitchRating() {
		return this.fitchRating;
	}


	public void setFitchRating(String fitchRating) {
		this.fitchRating = fitchRating;
	}


	public Date getFloaterCouponDate() {
		return this.floaterCouponDate;
	}


	public void setFloaterCouponDate(Date floaterCouponDate) {
		this.floaterCouponDate = floaterCouponDate;
	}


	public BigDecimal getFloaterCap() {
		return this.floaterCap;
	}


	public void setFloaterCap(BigDecimal floaterCap) {
		this.floaterCap = floaterCap;
	}


	public BigDecimal getFloaterFloor() {
		return this.floaterFloor;
	}


	public void setFloaterFloor(BigDecimal floaterFloor) {
		this.floaterFloor = floaterFloor;
	}


	public String getGuaranteeType() {
		return this.guaranteeType;
	}


	public void setGuaranteeType(String guaranteeType) {
		this.guaranteeType = guaranteeType;
	}


	public String getIndex() {
		return this.index;
	}


	public void setIndex(String index) {
		this.index = index;
	}


	public String getInflationQuoteType() {
		return this.inflationQuoteType;
	}


	public void setInflationQuoteType(String inflationQuoteType) {
		this.inflationQuoteType = inflationQuoteType;
	}


	public BigDecimal getInterest() {
		return this.interest;
	}


	public void setInterest(BigDecimal interest) {
		this.interest = interest;
	}


	public String getiSIN() {
		return this.iSIN;
	}


	public void setiSIN(String iSIN) {
		this.iSIN = iSIN;
	}


	public Date getIssueDate() {
		return this.issueDate;
	}


	public void setIssueDate(Date issueDate) {
		this.issueDate = issueDate;
	}


	public String getIssueName() {
		return this.issueName;
	}


	public void setIssueName(String issueName) {
		this.issueName = issueName;
	}


	public BigDecimal getIssrBdid() {
		return this.issrBdid;
	}


	public void setIssrBdid(BigDecimal issrBdid) {
		this.issrBdid = issrBdid;
	}


	public BigDecimal getIssueSize() {
		return this.issueSize;
	}


	public void setIssueSize(BigDecimal issueSize) {
		this.issueSize = issueSize;
	}


	public BigDecimal getLag() {
		return this.lag;
	}


	public void setLag(BigDecimal lag) {
		this.lag = lag;
	}


	public Date getLastCouponDate() {
		return this.lastCouponDate;
	}


	public void setLastCouponDate(Date lastCouponDate) {
		this.lastCouponDate = lastCouponDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public BigDecimal getmDCutPrc() {
		return this.mDCutPrc;
	}


	public void setmDCutPrc(BigDecimal mDCutPrc) {
		this.mDCutPrc = mDCutPrc;
	}


	public BigDecimal getmDCutYld() {
		return this.mDCutYld;
	}


	public void setmDCutYld(BigDecimal mDCutYld) {
		this.mDCutYld = mDCutYld;
	}


	public String getMdFlag() {
		return this.MdFlag;
	}


	public void setMdFlag(String mdFlag) {
		this.MdFlag = mdFlag;
	}


	public BigDecimal getMdSpr() {
		return this.MdSpr;
	}


	public void setMdSpr(BigDecimal mdSpr) {
		this.MdSpr = mdSpr;
	}


	public BigDecimal getMinimumPiece() {
		return this.minimumPiece;
	}


	public void setMinimumPiece(BigDecimal minimumPiece) {
		this.minimumPiece = minimumPiece;
	}


	public BigDecimal getMinimumIncrement() {
		return this.minimumIncrement;
	}


	public void setMinimumIncrement(BigDecimal minimumIncrement) {
		this.minimumIncrement = minimumIncrement;
	}


	public String getMoodysRating() {
		return this.moodysRating;
	}


	public void setMoodysRating(String moodysRating) {
		this.moodysRating = moodysRating;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public String getObligor() {
		return this.obligor;
	}


	public void setObligor(String obligor) {
		this.obligor = obligor;
	}


	public BigDecimal getOriginalYield() {
		return this.originalYield;
	}


	public void setOriginalYield(BigDecimal originalYield) {
		this.originalYield = originalYield;
	}


	public BigDecimal getOutstandingSize() {
		return this.outstandingSize;
	}


	public void setOutstandingSize(BigDecimal outstandingSize) {
		this.outstandingSize = outstandingSize;
	}


	public String getoTCTicker() {
		return this.oTCTicker;
	}


	public void setoTCTicker(String oTCTicker) {
		this.oTCTicker = oTCTicker;
	}


	public BigDecimal getParAmount() {
		return this.parAmount;
	}


	public void setParAmount(BigDecimal parAmount) {
		this.parAmount = parAmount;
	}


	public String getPaymentTerm() {
		return this.paymentTerm;
	}


	public void setPaymentTerm(String paymentTerm) {
		this.paymentTerm = paymentTerm;
	}


	public String getPlacementType() {
		return this.placementType;
	}


	public void setPlacementType(String placementType) {
		this.placementType = placementType;
	}


	public String getPool() {
		return this.pool;
	}


	public void setPool(String pool) {
		this.pool = pool;
	}


	public BigDecimal getPrepaymentFactor() {
		return this.prepaymentFactor;
	}


	public void setPrepaymentFactor(BigDecimal prepaymentFactor) {
		this.prepaymentFactor = prepaymentFactor;
	}


	public String getPrepaymentSpeed() {
		return this.prepaymentSpeed;
	}


	public void setPrepaymentSpeed(String prepaymentSpeed) {
		this.prepaymentSpeed = prepaymentSpeed;
	}


	public BigDecimal getPrice() {
		return this.price;
	}


	public void setPrice(BigDecimal price) {
		this.price = price;
	}


	public BigDecimal getPrincipal() {
		return this.principal;
	}


	public void setPrincipal(BigDecimal principal) {
		this.principal = principal;
	}


	public String getPurposeCode() {
		return this.purposeCode;
	}


	public void setPurposeCode(String purposeCode) {
		this.purposeCode = purposeCode;
	}


	public String getPurposeType() {
		return this.purposeType;
	}


	public void setPurposeType(String purposeType) {
		this.purposeType = purposeType;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public String getSandPRating() {
		return this.sandPRating;
	}


	public void setSandPRating(String sandPRating) {
		this.sandPRating = sandPRating;
	}


	public String getSector() {
		return this.sector;
	}


	public void setSector(String sector) {
		this.sector = sector;
	}


	public BigDecimal getSinkAmount() {
		return this.sinkAmount;
	}


	public void setSinkAmount(BigDecimal sinkAmount) {
		this.sinkAmount = sinkAmount;
	}


	public BigDecimal getSpread() {
		return this.spread;
	}


	public void setSpread(BigDecimal spread) {
		this.spread = spread;
	}


	public String getState() {
		return this.state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public Integer getSubordinationType() {
		return this.subordinationType;
	}


	public void setSubordinationType(Integer subordinationType) {
		this.subordinationType = subordinationType;
	}


	public String getTaxStatus() {
		return this.taxStatus;
	}


	public void setTaxStatus(String taxStatus) {
		this.taxStatus = taxStatus;
	}


	public Boolean getCashPriced() {
		return this.cashPriced;
	}


	public void setCashPriced(Boolean cashPriced) {
		this.cashPriced = cashPriced;
	}


	public String getType() {
		return this.type;
	}


	public void setType(String type) {
		this.type = type;
	}


	public BigDecimal getWac() {
		return this.wac;
	}


	public void setWac(BigDecimal wac) {
		this.wac = wac;
	}


	public BigDecimal getWal() {
		return this.wal;
	}


	public void setWal(BigDecimal wal) {
		this.wal = wal;
	}


	public BigDecimal getWala() {
		return this.wala;
	}


	public void setWala(BigDecimal wala) {
		this.wala = wala;
	}


	public BigDecimal getWam() {
		return this.wam;
	}


	public void setWam(BigDecimal wam) {
		this.wam = wam;
	}


	public String getXrefContext() {
		return this.xrefContext;
	}


	public void setXrefContext(String xrefContext) {
		this.xrefContext = xrefContext;
	}


	public BigDecimal getYtm() {
		return this.ytm;
	}


	public void setYtm(BigDecimal ytm) {
		this.ytm = ytm;
	}
}
