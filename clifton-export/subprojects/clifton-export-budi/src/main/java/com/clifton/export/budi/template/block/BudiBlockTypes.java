package com.clifton.export.budi.template.block;

/**
 * @author TerryS
 */
public enum BudiBlockTypes {

	HEADER("Header"),
	OPTION("Option"),
	FEES("Deal Fees"),
	UNDERLYING("Underlying"),
	SCHEDULE("Schedule"),
	SINK("Sink"),
	CASHFLOW("Cashflow"),
	DEAL("Deal"),
	TRANCHE("Tranche"),
	CONTRACT_AND_TRANSACTION("Contract and Transaction");

	private final String label;


	BudiBlockTypes(String label) {
		this.label = label;
	}


	public String getLabel() {
		return this.label;
	}
}
