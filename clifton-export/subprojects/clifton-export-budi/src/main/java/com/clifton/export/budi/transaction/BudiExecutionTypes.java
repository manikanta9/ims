package com.clifton.export.budi.transaction;

/**
 * @author TerryS
 */
public enum BudiExecutionTypes {

	NEW,
	CANCEL,
	CORRECT
}
