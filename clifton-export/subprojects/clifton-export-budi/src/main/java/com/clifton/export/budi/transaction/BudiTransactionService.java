package com.clifton.export.budi.transaction;

import java.util.List;


/**
 * @author TerryS
 */
public interface BudiTransactionService<T, F extends BudiTransactionFilter> {

	public List<T> getTransactionList(F filters);
}
