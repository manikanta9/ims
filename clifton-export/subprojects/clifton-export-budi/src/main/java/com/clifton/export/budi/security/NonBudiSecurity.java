package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;


/**
 * @author TerryS
 */
public class NonBudiSecurity implements BudiSecurity {

	private final BudiInstrumentTypes budiInstrumentType;
	private final BudiInstruments budiInstrument;


	public NonBudiSecurity(BudiInstrumentTypes budiInstrumentType) {
		this.budiInstrumentType = budiInstrumentType;
		this.budiInstrument = BudiInstruments.NON_BUDI;
	}


	@Override
	public BudiInstruments getInstrument() {
		return this.budiInstrument;
	}


	@Override
	public BudiInstrumentTypes getInstrumentType() {
		return this.budiInstrumentType;
	}
}
