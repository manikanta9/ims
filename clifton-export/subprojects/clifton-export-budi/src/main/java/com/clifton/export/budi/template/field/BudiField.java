package com.clifton.export.budi.template.field;


import java.util.Objects;


/**
 * @author TerryS
 */
public class BudiField {

	public enum Visibility {
		MANDATORY,
		CONDITIONALLY,
		OPTIONAL
	}

	private final String name;
	private final Visibility visibility;


	public BudiField(String name, Visibility visibility) {
		this.name = name;
		this.visibility = visibility;
	}


	public String format(Object value) {
		if (this.visibility == Visibility.MANDATORY && (value == null || (value instanceof String && value.toString().isEmpty()))) {
			throw new RuntimeException("A mandatory field value is missing " + this.name);
		}
		return value == null ? null : value.toString();
	}


	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}
		BudiField budiField = (BudiField) o;
		return Objects.equals(this.name, budiField.name) &&
				this.visibility == budiField.visibility;
	}


	@Override
	public int hashCode() {
		return Objects.hash(this.name, this.visibility);
	}


	protected boolean isFound(Enum<?>[] enumeration, String value) {
		boolean found = false;
		for (Enum<?> possible : enumeration) {
			if (possible.toString().equals(value)) {
				found = true;
				break;
			}
		}
		return found;
	}


	public String getName() {
		return this.name;
	}


	public Visibility getVisibility() {
		return this.visibility;
	}
}
