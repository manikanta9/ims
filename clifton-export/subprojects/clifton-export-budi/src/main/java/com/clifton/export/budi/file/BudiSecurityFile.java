package com.clifton.export.budi.file;

import com.clifton.export.budi.security.BudiSecurity;
import com.clifton.export.budi.template.BudiTemplate;
import com.clifton.export.budi.template.BudiTemplateFactory;
import com.clifton.export.budi.template.block.BudiBlock;
import com.clifton.export.budi.template.block.BudiBlockValues;
import com.clifton.export.budi.template.field.BudiFieldValue;
import org.apache.commons.csv.CSVFormat;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;


/**
 * @author TerryS
 */
public class BudiSecurityFile {

	private final BudiFileFormats fileFormat;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BudiSecurityFile(BudiFileFormats fileFormat) {
		this.fileFormat = fileFormat;
	}


	public void format(List<BudiSecurity> securityList, Writer writer) {
		List<BudiBlockValues> blocks = new ArrayList<>();

		for (BudiSecurity security : securityList) {
			if (security.getInstrument() == this.fileFormat.getInstrument()) {
				BudiTemplate template = BudiTemplateFactory.buildTemplate(security.getInstrumentType());
				List<BudiBlockValues> values = template.getValues(security);
				blocks.addAll(values);
			}
		}

		int maximumColumnCount = getMaximumColumnCount(blocks);
		for (BudiBlockValues block : blocks) {
			output(block, writer, maximumColumnCount);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int getMaximumColumnCount(List<BudiBlockValues> blocks) {
		int results = 0;

		for (BudiBlockValues block : blocks) {
			if (block.getMaximumColumnCount() > results) {
				results = block.getMaximumColumnCount();
			}
		}

		return results;
	}


	private void output(BudiBlockValues blockValues, Writer writer, int columnCount) {
		try {
			CSVFormat format = CSVFormat.DEFAULT.withQuote(null).withAutoFlush(true);
			String[] headers = getHeaders(blockValues, columnCount);
			boolean headerPrinted = false;
			for (int index = 0; index < blockValues.getRowCount(); index++) {
				Object[] values = getValues(blockValues.getRow(index), columnCount);
				if (!headerPrinted && blockValues.hasHeader(index)) {
					format.printRecord(writer, (Object[]) headers);
					headerPrinted = true;
				}
				format.printRecord(writer, values);
			}
		}
		catch (IOException e) {
			throw new RuntimeException("could not output block ", e);
		}
	}


	private String[] getHeaders(BudiBlockValues block, int columnCount) {
		List<BudiFieldValue> values = block.getLongestRow();
		return Stream.concat(values.stream().map(BudiFieldValue::getHeader), Stream.generate(() -> BudiBlock.EMPTY).limit(columnCount - values.size())).toArray(String[]::new);
	}


	private Object[] getValues(List<BudiFieldValue> values, int columnCount) {
		return Stream.concat(values.stream().map(BudiFieldValue::getValue), Stream.generate(() -> BudiBlock.EMPTY).limit(columnCount - values.size())).toArray();
	}
}
