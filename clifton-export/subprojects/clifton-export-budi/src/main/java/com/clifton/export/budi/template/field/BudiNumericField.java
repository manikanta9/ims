package com.clifton.export.budi.template.field;

import com.clifton.core.util.validation.ValidationUtils;

import java.math.BigDecimal;


/**
 * @author TerryS
 */
public class BudiNumericField extends BudiField {

	public BudiNumericField(String name, Visibility visibility) {
		super(name, visibility);
	}


	@Override
	public String format(Object value) {
		if (getVisibility() == Visibility.MANDATORY && value == null) {
			throw new RuntimeException("A mandatory field value is missing " + getName());
		}
		if (value != null) {
			ValidationUtils.assertTrue(value instanceof Number, "The provided value must be a numeric on field " + this.getName());
			return (value instanceof BigDecimal) ? ((BigDecimal) value).toPlainString() : value.toString();
		}
		else {
			return null;
		}
	}
}
