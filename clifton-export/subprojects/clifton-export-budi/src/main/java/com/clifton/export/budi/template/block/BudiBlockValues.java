package com.clifton.export.budi.template.block;

import com.clifton.export.budi.template.field.BudiFieldValue;

import java.util.ArrayList;
import java.util.List;


/**
 * @author TerryS
 */
public class BudiBlockValues {

	private final List<List<BudiFieldValue>> rows = new ArrayList<>();
	private final List<Integer> noHeaderRows = new ArrayList<>();


	public void addRow(List<BudiFieldValue> row) {
		addRow(row, true);
	}


	public void addRow(List<BudiFieldValue> row, boolean header) {
		this.rows.add(row);
		if (!header) {
			this.noHeaderRows.add(this.rows.size() - 1);
		}
	}


	public List<List<BudiFieldValue>> getRows() {
		return this.rows;
	}


	public List<BudiFieldValue> getRow(int index) {
		if (index < this.rows.size()) {
			return this.rows.get(index);
		}
		else {
			throw new IndexOutOfBoundsException("Index larger then row count " + index);
		}
	}


	public int getMaximumColumnCount() {
		return this.rows.stream().map(List::size).max(Integer::compareTo).orElse(0);
	}


	public List<BudiFieldValue> getLongestRow() {
		int maxSize = 0;
		int maxIndex = 0;
		for (int index = 0; index < this.rows.size(); index++) {
			if (!this.noHeaderRows.contains(index)) {
				if (this.rows.get(index).size() > maxSize) {
					maxSize = this.rows.get(index).size();
					maxIndex = index;
				}
			}
		}
		return this.rows.get(maxIndex);
	}


	public boolean hasHeader(int index) {
		return !this.noHeaderRows.contains(index);
	}


	public int getRowCount() {
		return this.rows.size();
	}
}
