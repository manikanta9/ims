package com.clifton.export.budi.template;

import com.clifton.core.util.AssertUtils;
import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.security.BudiSecurity;
import com.clifton.export.budi.template.block.BudiBlock;
import com.clifton.export.budi.template.block.BudiBlockValues;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
public class BudiTemplate {

	private final BudiInstrumentTypes instrumentType;
	private final BudiBlock.BlockFormat outputFormat;
	private final List<BudiBlock> blockList;

	////////////////////////////////////////////////////////////////////////////


	public BudiTemplate(BudiInstrumentTypes instrumentType, BudiBlock.BlockFormat outputFormat, BudiBlock... blocks) {
		this.instrumentType = instrumentType;
		this.outputFormat = outputFormat;
		this.blockList = Arrays.asList(blocks);
		AssertUtils.assertEmpty(this.blockList.stream().map(BudiBlock::getInstrumentType).filter(t -> instrumentType != t).collect(Collectors.toList()), "Block template mismatch.");
	}


	public BudiBlock.BlockFormat getOutputFormat() {
		return this.outputFormat;
	}


	public List<BudiBlock> getBlockList() {
		return this.blockList;
	}


	public BudiBlock[] getBlockArray() {
		return this.blockList.toArray(new BudiBlock[0]);
	}


	public List<BudiBlockValues> getValues(BudiSecurity security) {
		List<BudiBlockValues> results = new ArrayList<>();

		AssertUtils.assertEquals(security.getInstrumentType(), getInstrumentType(), "Template security type mismatch.");
		if (BudiBlock.BlockFormat.CONCATENATE == getOutputFormat()) {
			BudiBlockValues values = BudiBlock.getConcatenatedBlockValues(security, getBlockList());
			results.add(values);
		}
		else if (BudiBlock.BlockFormat.REFERENCE == getOutputFormat()) {
			for (BudiBlock block : getBlockList()) {
				BudiBlockValues values = BudiBlock.getReferenceBlockValues(security, block);
				results.add(values);
			}
		}
		else {
			throw new RuntimeException("output format not supported " + getOutputFormat());
		}

		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BudiInstrumentTypes getInstrumentType() {
		return this.instrumentType;
	}
}
