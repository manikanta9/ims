package com.clifton.export.budi.template.block;

import java.lang.annotation.ElementType;
import java.lang.annotation.Inherited;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * @author TerryS
 */
@Inherited
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface BudiRepeatingBlock {

	BudiBlockTypes blockType() default BudiBlockTypes.FEES;
}

