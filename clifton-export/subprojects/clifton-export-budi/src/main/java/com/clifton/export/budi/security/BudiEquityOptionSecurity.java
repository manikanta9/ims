package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiEquityOptionSecurity extends BudiBaseSecurity {

	private String barrierDirection;
	private Date barrierEndDate;
	private String barrierLevel;
	private String barrierRebate;
	private String barrierSettlement;
	private Date barrierStartDate;
	private String barrierType;
	private String busDayAdjustment;
	private String counterparty;
	private String currency;
	private String dealTypePath;
	private Date effectiveDate;
	private String exerciseStyle;
	private Date expiryDate;
	private Date fixingEndDate;
	private Date fixingStartDate;
	private String frequency;
	private BigDecimal localCap;
	private BigDecimal localFloor;
	private BigDecimal globalCap;
	private BigDecimal globalFloor;
	private BigDecimal notional;
	private String optionExerciseFrequency;
	private Date OptionFirstExercise;
	private BigDecimal optionStrike;
	private String optionType;
	private String otcTicker;
	private String payoffType;
	private BigDecimal rate;
	private String rollConvention;
	private Date settlementDate;
	private BigDecimal sharesPerWarrant;
	private String side;
	private BigDecimal shares;
	private String stockDilution;
	private String strategy;
	private BigDecimal strikePercent;
	private String strikeType;
	private String style;
	private String ticker;
	private String underlyingSecurity;
	private String underlyingSecurityType;
	private String warrantType;
	private String xRefContext;


	public BudiEquityOptionSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.EQUITY_IR_FUTURE_OPTIONS, instrumentType);
	}


	public String getBarrierDirection() {
		return this.barrierDirection;
	}


	public void setBarrierDirection(String barrierDirection) {
		this.barrierDirection = barrierDirection;
	}


	public Date getBarrierEndDate() {
		return this.barrierEndDate;
	}


	public void setBarrierEndDate(Date barrierEndDate) {
		this.barrierEndDate = barrierEndDate;
	}


	public String getBarrierLevel() {
		return this.barrierLevel;
	}


	public void setBarrierLevel(String barrierLevel) {
		this.barrierLevel = barrierLevel;
	}


	public String getBarrierRebate() {
		return this.barrierRebate;
	}


	public void setBarrierRebate(String barrierRebate) {
		this.barrierRebate = barrierRebate;
	}


	public String getBarrierSettlement() {
		return this.barrierSettlement;
	}


	public void setBarrierSettlement(String barrierSettlement) {
		this.barrierSettlement = barrierSettlement;
	}


	public Date getBarrierStartDate() {
		return this.barrierStartDate;
	}


	public void setBarrierStartDate(Date barrierStartDate) {
		this.barrierStartDate = barrierStartDate;
	}


	public String getBarrierType() {
		return this.barrierType;
	}


	public void setBarrierType(String barrierType) {
		this.barrierType = barrierType;
	}


	public String getBusDayAdjustment() {
		return this.busDayAdjustment;
	}


	public void setBusDayAdjustment(String busDayAdjustment) {
		this.busDayAdjustment = busDayAdjustment;
	}


	public String getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getDealTypePath() {
		return this.dealTypePath;
	}


	public void setDealTypePath(String dealTypePath) {
		this.dealTypePath = dealTypePath;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public String getExerciseStyle() {
		return this.exerciseStyle;
	}


	public void setExerciseStyle(String exerciseStyle) {
		this.exerciseStyle = exerciseStyle;
	}


	public Date getExpiryDate() {
		return this.expiryDate;
	}


	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}


	public Date getFixingEndDate() {
		return this.fixingEndDate;
	}


	public void setFixingEndDate(Date fixingEndDate) {
		this.fixingEndDate = fixingEndDate;
	}


	public Date getFixingStartDate() {
		return this.fixingStartDate;
	}


	public void setFixingStartDate(Date fixingStartDate) {
		this.fixingStartDate = fixingStartDate;
	}


	public String getFrequency() {
		return this.frequency;
	}


	public void setFrequency(String frequency) {
		this.frequency = frequency;
	}


	public BigDecimal getLocalCap() {
		return this.localCap;
	}


	public void setLocalCap(BigDecimal localCap) {
		this.localCap = localCap;
	}


	public BigDecimal getLocalFloor() {
		return this.localFloor;
	}


	public void setLocalFloor(BigDecimal localFloor) {
		this.localFloor = localFloor;
	}


	public BigDecimal getGlobalCap() {
		return this.globalCap;
	}


	public void setGlobalCap(BigDecimal globalCap) {
		this.globalCap = globalCap;
	}


	public BigDecimal getGlobalFloor() {
		return this.globalFloor;
	}


	public void setGlobalFloor(BigDecimal globalFloor) {
		this.globalFloor = globalFloor;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public String getOptionExerciseFrequency() {
		return this.optionExerciseFrequency;
	}


	public void setOptionExerciseFrequency(String optionExerciseFrequency) {
		this.optionExerciseFrequency = optionExerciseFrequency;
	}


	public Date getOptionFirstExercise() {
		return this.OptionFirstExercise;
	}


	public void setOptionFirstExercise(Date optionFirstExercise) {
		this.OptionFirstExercise = optionFirstExercise;
	}


	public BigDecimal getOptionStrike() {
		return this.optionStrike;
	}


	public void setOptionStrike(BigDecimal optionStrike) {
		this.optionStrike = optionStrike;
	}


	public String getOptionType() {
		return this.optionType;
	}


	public void setOptionType(String optionType) {
		this.optionType = optionType;
	}


	public String getOtcTicker() {
		return this.otcTicker;
	}


	public void setOtcTicker(String otcTicker) {
		this.otcTicker = otcTicker;
	}


	public String getPayoffType() {
		return this.payoffType;
	}


	public void setPayoffType(String payoffType) {
		this.payoffType = payoffType;
	}


	public BigDecimal getRate() {
		return this.rate;
	}


	public void setRate(BigDecimal rate) {
		this.rate = rate;
	}


	public String getRollConvention() {
		return this.rollConvention;
	}


	public void setRollConvention(String rollConvention) {
		this.rollConvention = rollConvention;
	}


	public Date getSettlementDate() {
		return this.settlementDate;
	}


	public void setSettlementDate(Date settlementDate) {
		this.settlementDate = settlementDate;
	}


	public BigDecimal getSharesPerWarrant() {
		return this.sharesPerWarrant;
	}


	public void setSharesPerWarrant(BigDecimal sharesPerWarrant) {
		this.sharesPerWarrant = sharesPerWarrant;
	}


	public String getSide() {
		return this.side;
	}


	public void setSide(String side) {
		this.side = side;
	}


	public BigDecimal getShares() {
		return this.shares;
	}


	public void setShares(BigDecimal shares) {
		this.shares = shares;
	}


	public String getStockDilution() {
		return this.stockDilution;
	}


	public void setStockDilution(String stockDilution) {
		this.stockDilution = stockDilution;
	}


	public String getStrategy() {
		return this.strategy;
	}


	public void setStrategy(String strategy) {
		this.strategy = strategy;
	}


	public BigDecimal getStrikePercent() {
		return this.strikePercent;
	}


	public void setStrikePercent(BigDecimal strikePercent) {
		this.strikePercent = strikePercent;
	}


	public String getStrikeType() {
		return this.strikeType;
	}


	public void setStrikeType(String strikeType) {
		this.strikeType = strikeType;
	}


	public String getStyle() {
		return this.style;
	}


	public void setStyle(String style) {
		this.style = style;
	}


	public String getTicker() {
		return this.ticker;
	}


	public void setTicker(String ticker) {
		this.ticker = ticker;
	}


	public String getUnderlyingSecurity() {
		return this.underlyingSecurity;
	}


	public void setUnderlyingSecurity(String underlyingSecurity) {
		this.underlyingSecurity = underlyingSecurity;
	}


	public String getUnderlyingSecurityType() {
		return this.underlyingSecurityType;
	}


	public void setUnderlyingSecurityType(String underlyingSecurityType) {
		this.underlyingSecurityType = underlyingSecurityType;
	}


	public String getWarrantType() {
		return this.warrantType;
	}


	public void setWarrantType(String warrantType) {
		this.warrantType = warrantType;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
