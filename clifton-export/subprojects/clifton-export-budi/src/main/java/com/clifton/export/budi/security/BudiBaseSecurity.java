package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;


/**
 * @author TerryS
 */
public abstract class BudiBaseSecurity implements BudiSecurity {

	private String description;
	private String identifier;
	private String instType;

	public final BudiInstruments instrument;
	private final BudiInstrumentTypes instrumentType;


	protected BudiBaseSecurity(BudiInstruments instrument, BudiInstrumentTypes instrumentType) {
		this.instrument = instrument;
		this.instrumentType = instrumentType;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getIdentifier() {
		return this.identifier;
	}


	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}


	public String getInstType() {
		return this.instType;
	}


	public void setInstType(String instType) {
		this.instType = instType;
	}


	@Override
	public BudiInstruments getInstrument() {
		return this.instrument;
	}


	@Override
	public BudiInstrumentTypes getInstrumentType() {
		return this.instrumentType;
	}
}
