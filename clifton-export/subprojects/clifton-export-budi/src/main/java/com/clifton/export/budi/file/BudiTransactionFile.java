package com.clifton.export.budi.file;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;


/**
 * @author TerryS
 */
public class BudiTransactionFile {

	private final File file;


	public BudiTransactionFile(File file) {
		this.file = file;
	}


	public void write(Iterable<Object[]> iterable, String[] headers) {
		try (FileWriter writer = new FileWriter(this.file); CSVPrinter csvFilePrinter = new CSVPrinter(writer, CSVFormat.DEFAULT.withHeader(headers))) {
			for (Object[] values : iterable) {
				csvFilePrinter.printRecord(values);
			}
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to generate export file.", e);
		}
	}
}
