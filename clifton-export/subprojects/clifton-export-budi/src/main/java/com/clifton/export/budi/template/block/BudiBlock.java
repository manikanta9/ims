package com.clifton.export.budi.template.block;

import com.clifton.core.beans.BeanUtils;
import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.security.BudiSecurity;
import com.clifton.export.budi.template.field.BudiField;
import com.clifton.export.budi.template.field.BudiFieldValue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
public class BudiBlock {

	public static final String EMPTY = "";

	public enum BlockFormat {
		CONCATENATE,
		REFERENCE
	}

	private final BudiInstrumentTypes instrumentType;
	private final BudiBlockTypes blockType;
	private final boolean repeating;
	private final List<BudiField> fieldList;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BudiBlock(BudiInstrumentTypes instrumentType, BudiBlockTypes blockType, BudiField... fields) {
		this(instrumentType, blockType, false, fields);
	}


	public BudiBlock(BudiInstrumentTypes instrumentType, BudiBlockTypes blockType, boolean repeating, BudiField... fields) {
		this.instrumentType = instrumentType;
		this.blockType = blockType;
		this.repeating = repeating;
		this.fieldList = Arrays.asList(fields);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static BudiBlockValues getReferenceBlockValues(BudiSecurity security, BudiBlock block) {
		BudiBlockValues results = new BudiBlockValues();
		List<BudiFieldValue> fieldValueList;
		if (block.isRepeating()) {
			results.addRow(Collections.singletonList(new BudiFieldValue(EMPTY, "#" + block.getBlockType().getLabel())), false);
			List<BudiSecurity> blockSecurities = getBlockSecurities(security, block.blockType);
			for (BudiSecurity budiSecurity : blockSecurities) {
				fieldValueList = BudiBlock.getFieldValues(budiSecurity, block.getFieldList());
				results.addRow(fieldValueList);
			}
		}
		else {
			if (block.blockType == BudiBlockTypes.HEADER) {
				results.addRow(Collections.singletonList(new BudiFieldValue(EMPTY, "#" + block.getInstrumentType().getLabel())), false);
			}
			results.addRow(Collections.singletonList(new BudiFieldValue(EMPTY, "#" + block.getBlockType().getLabel())), false);
			fieldValueList = BudiBlock.getFieldValues(security, block.getFieldList());
			results.addRow(fieldValueList);
		}
		results.addRow(Collections.singletonList(new BudiFieldValue(EMPTY, EMPTY)), false);
		return results;
	}


	public static BudiBlockValues getConcatenatedBlockValues(BudiSecurity security, List<BudiBlock> blocks) {
		BudiBlockValues results = new BudiBlockValues();
		if (!blocks.isEmpty()) {
			List<BudiField> fieldList = blocks.stream().flatMap(b -> b.getFieldList().stream()).collect(Collectors.toList());
			results.addRow(Collections.singletonList(new BudiFieldValue(EMPTY, "#" + blocks.get(0).getInstrumentType().getLabel())), false);
			results.addRow(BudiBlock.getFieldValues(security, fieldList));
			results.addRow(Collections.singletonList(new BudiFieldValue(EMPTY, EMPTY)), false);
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static List<BudiFieldValue> getFieldValues(BudiSecurity security, List<BudiField> fields) {
		List<BudiFieldValue> results = new ArrayList<>();
		Map<String, Object> values = BeanUtils.describeWithAnnotationFilters(security, BudiRepeatingBlock.class).entrySet().stream()
				.collect(HashMap::new, (m, e) -> m.put(e.getKey().toUpperCase(), e.getValue()), HashMap::putAll);
		for (BudiField field : fields) {
			String key = field.getName().toUpperCase();
			if (values.containsKey(key)) {
				Object value = values.get(key);
				String formatted = field.format(value);
				if (formatted != null) {
					results.add(new BudiFieldValue(field.getName(), formatted));
				}
			}
			else {
				throw new RuntimeException("Field is expected in the security object " + field.getName());
			}
		}
		return results;
	}


	@SuppressWarnings("unchecked")
	private static List<BudiSecurity> getBlockSecurities(BudiSecurity security, BudiBlockTypes blockType) {
		List<BudiSecurity> results = new ArrayList<>();

		List<String> typeProperties = BeanUtils.getPropertyAnnotations(security.getClass(), BudiRepeatingBlock.class).entrySet().stream()
				.filter(e -> e.getValue().blockType() == blockType)
				.map(Map.Entry::getKey)
				.collect(Collectors.toList());
		if (typeProperties.size() == 1) {
			String property = typeProperties.get(0);
			Object value = BeanUtils.getPropertyValue(security, property);
			if (value instanceof List) {
				results.addAll((Collection<? extends BudiSecurity>) value);
			}
		}
		else {
			results.add(security);
		}

		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public BudiInstrumentTypes getInstrumentType() {
		return this.instrumentType;
	}


	public BudiBlockTypes getBlockType() {
		return this.blockType;
	}


	public boolean isRepeating() {
		return this.repeating;
	}


	public List<BudiField> getFieldList() {
		return this.fieldList;
	}
}
