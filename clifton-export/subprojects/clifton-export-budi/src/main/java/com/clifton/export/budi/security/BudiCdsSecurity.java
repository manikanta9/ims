package com.clifton.export.budi.security;

import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.asset.BudiInstruments;

import java.math.BigDecimal;
import java.util.Date;


/**
 * @author TerryS
 */
public class BudiCdsSecurity extends BudiBaseSecurity {

	private String action;
	private String businessDayAdjustment;
	private String contractType;
	private String centralCounterparty;
	private String counterparty;
	private Integer coupon;
	private String currency;
	private String dateGenerationMethod;
	private String dayCount;
	private Date effectiveDate;
	private Date maturityDate;
	private BigDecimal notional;
	private String OptionDirection;
	private Date optionExpirationDate;
	private Date optionStartDate;
	private BigDecimal optionStrikePrice;
	private String otcTicker;
	private String payFrequency;
	private String payoffSide;
	private String rEDCode;
	private String refObligOrIndex;
	private String restructuringType;
	private Integer tradeSpread;
	private BigDecimal upfrontPoints;
	private String xRefContext;


	public BudiCdsSecurity(BudiInstrumentTypes instrumentType) {
		super(BudiInstruments.CREDIT_DERIVATIVES, instrumentType);
	}


	public String getAction() {
		return this.action;
	}


	public void setAction(String action) {
		this.action = action;
	}


	public String getBusinessDayAdjustment() {
		return this.businessDayAdjustment;
	}


	public void setBusinessDayAdjustment(String businessDayAdjustment) {
		this.businessDayAdjustment = businessDayAdjustment;
	}


	public String getContractType() {
		return this.contractType;
	}


	public void setContractType(String contractType) {
		this.contractType = contractType;
	}


	public String getCentralCounterparty() {
		return this.centralCounterparty;
	}


	public void setCentralCounterparty(String centralCounterparty) {
		this.centralCounterparty = centralCounterparty;
	}


	public String getCounterparty() {
		return this.counterparty;
	}


	public void setCounterparty(String counterparty) {
		this.counterparty = counterparty;
	}


	public Integer getCoupon() {
		return this.coupon;
	}


	public void setCoupon(Integer coupon) {
		this.coupon = coupon;
	}


	public String getCurrency() {
		return this.currency;
	}


	public void setCurrency(String currency) {
		this.currency = currency;
	}


	public String getDateGenerationMethod() {
		return this.dateGenerationMethod;
	}


	public void setDateGenerationMethod(String dateGenerationMethod) {
		this.dateGenerationMethod = dateGenerationMethod;
	}


	public String getDayCount() {
		return this.dayCount;
	}


	public void setDayCount(String dayCount) {
		this.dayCount = dayCount;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public Date getMaturityDate() {
		return this.maturityDate;
	}


	public void setMaturityDate(Date maturityDate) {
		this.maturityDate = maturityDate;
	}


	public BigDecimal getNotional() {
		return this.notional;
	}


	public void setNotional(BigDecimal notional) {
		this.notional = notional;
	}


	public String getOptionDirection() {
		return this.OptionDirection;
	}


	public void setOptionDirection(String optionDirection) {
		this.OptionDirection = optionDirection;
	}


	public Date getOptionExpirationDate() {
		return this.optionExpirationDate;
	}


	public void setOptionExpirationDate(Date optionExpirationDate) {
		this.optionExpirationDate = optionExpirationDate;
	}


	public Date getOptionStartDate() {
		return this.optionStartDate;
	}


	public void setOptionStartDate(Date optionStartDate) {
		this.optionStartDate = optionStartDate;
	}


	public BigDecimal getOptionStrikePrice() {
		return this.optionStrikePrice;
	}


	public void setOptionStrikePrice(BigDecimal optionStrikePrice) {
		this.optionStrikePrice = optionStrikePrice;
	}


	public String getOtcTicker() {
		return this.otcTicker;
	}


	public void setOtcTicker(String otcTicker) {
		this.otcTicker = otcTicker;
	}


	public String getPayFrequency() {
		return this.payFrequency;
	}


	public void setPayFrequency(String payFrequency) {
		this.payFrequency = payFrequency;
	}


	public String getPayoffSide() {
		return this.payoffSide;
	}


	public void setPayoffSide(String payoffSide) {
		this.payoffSide = payoffSide;
	}


	public String getrEDCode() {
		return this.rEDCode;
	}


	public void setrEDCode(String rEDCode) {
		this.rEDCode = rEDCode;
	}


	public String getRefObligOrIndex() {
		return this.refObligOrIndex;
	}


	public void setRefObligOrIndex(String refObligOrIndex) {
		this.refObligOrIndex = refObligOrIndex;
	}


	public String getRestructuringType() {
		return this.restructuringType;
	}


	public void setRestructuringType(String restructuringType) {
		this.restructuringType = restructuringType;
	}


	public Integer getTradeSpread() {
		return this.tradeSpread;
	}


	public void setTradeSpread(Integer tradeSpread) {
		this.tradeSpread = tradeSpread;
	}


	public BigDecimal getUpfrontPoints() {
		return this.upfrontPoints;
	}


	public void setUpfrontPoints(BigDecimal upfrontPoints) {
		this.upfrontPoints = upfrontPoints;
	}


	public String getxRefContext() {
		return this.xRefContext;
	}


	public void setxRefContext(String xRefContext) {
		this.xRefContext = xRefContext;
	}
}
