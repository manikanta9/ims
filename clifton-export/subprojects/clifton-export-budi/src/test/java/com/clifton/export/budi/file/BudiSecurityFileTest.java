package com.clifton.export.budi.file;

import com.clifton.core.util.date.DateUtils;
import com.clifton.export.budi.asset.BudiInstrumentTypes;
import com.clifton.export.budi.security.BudiFxSecurity;
import com.clifton.export.budi.security.BudiIrSecurity;
import com.clifton.export.budi.security.BudiSecurity;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.core.io.ClassPathResource;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;


/**
 * @author TerryS
 */
class BudiSecurityFileTest {

	private File fx;
	private File ir;
	private File output;


	@BeforeEach
	public void loadMessageFile() throws IOException {
		ClassPathResource classPathResource = new ClassPathResource("com/clifton/export/budi/file/fx_test1_65_BUDI_v1_00_200818_124500.csv");
		this.fx = classPathResource.getFile();
		classPathResource = new ClassPathResource("com/clifton/export/budi/file/IR_test1_5_BUDI_v1_00_210127_124500.csv");
		this.ir = classPathResource.getFile();
	}


	@AfterEach
	public void deleteFiles() {
		Optional.ofNullable(this.output).ifPresent(File::delete);
	}


	@Test
	public void fxTest() throws IOException {
		this.output = File.createTempFile("fx_", null);
		BudiSecurityFile securityFile = new BudiSecurityFile(BudiFileFormats.FX_AND_FX_OPTIONS);
		List<BudiSecurity> securityList = Arrays.asList(
				createFxNoTouch(),
				createFxForward(),
				createFxForwardExtra()
		);
		try (FileWriter writer = new FileWriter(this.output, true)) {
			securityFile.format(securityList, writer);
		}
		List<String> generated = Files.readAllLines(this.output.toPath());
		List<String> expected = Files.readAllLines(this.fx.toPath());

		Assertions.assertEquals(expected.size(), generated.size());
		for (int i = 0; i < expected.size(); i++) {
			String expectedLine = expected.get(i);
			String generatedLine = generated.get(i);

			String[] expectedValues = expectedLine.split(",");
			String[] generatedValues = generatedLine.split(",");
			Assertions.assertEquals(expectedValues.length, generatedValues.length, "Line " + (i + 1));

			List<String> expectedValueList = Arrays.asList(expectedValues);
			List<String> generatedValueList = Arrays.asList(generatedValues);
			List<String> remaining = new ArrayList<>(expectedValueList);
			remaining.removeAll(generatedValueList);
			Assertions.assertEquals(0, remaining.size(), remaining.toString() + " for line " + (i + 1));
		}
	}


	@Test
	public void irTest() throws IOException {
		this.output = File.createTempFile("ir_", null);
		BudiSecurityFile securityFile = new BudiSecurityFile(BudiFileFormats.INTEREST_RATE_DERIVATIVES);
		List<BudiSecurity> securityList = Collections.singletonList(
				createFloatFloatInterestRateSwap()
		);
		try (FileWriter writer = new FileWriter(this.output, true)) {
			securityFile.format(securityList, writer);
		}
		List<String> generated = Files.readAllLines(this.output.toPath());
		List<String> expected = Files.readAllLines(this.ir.toPath());

		Assertions.assertEquals(expected.size(), generated.size());
		for (int i = 0; i < expected.size(); i++) {
			String expectedLine = expected.get(i);
			String generatedLine = generated.get(i);

			String[] expectedValues = expectedLine.split(",");
			String[] generatedValues = generatedLine.split(",");
			Assertions.assertEquals(expectedValues.length, generatedValues.length, "Line " + (i + 1));

			List<String> expectedValueList = Arrays.asList(expectedValues);
			List<String> generatedValueList = Arrays.asList(generatedValues);
			List<String> remaining = new ArrayList<>(expectedValueList);
			remaining.removeAll(generatedValueList);
			Assertions.assertEquals(0, remaining.size(), remaining.toString() + " for line " + (i + 1));
		}
	}


	public BudiSecurity createFxNoTouch() {
		BudiFxSecurity security = new BudiFxSecurity(BudiInstrumentTypes.FX_NO_TOUCH);

		security.setIdentifier("FX1");
		security.setAction("AddUpdate");
		security.setxRefContext("Firm");
		security.setInstType(BudiInstrumentTypes.FX_NO_TOUCH.toString().toLowerCase());
		security.setDescription("NT.FX");
		security.setUnderlyingSecurity("CADUSD");
		security.setPayOffSide("Buy");
		security.setNotional(new BigDecimal("1200000"));
		security.setCurrency("CAD");
		security.setExpiryDate(DateUtils.toDate("11/20/2020"));
		security.setBarrierDirection("DownAndOut");
		security.setBarrierLevel(new BigDecimal("0.7129"));
		security.setBarrierStartDate(DateUtils.toDate("08/20/2020"));
		security.setBarrierEndDate(DateUtils.toDate("11/20/2020"));

		return security;
	}


	public BudiSecurity createFxForward() {
		BudiFxSecurity security = new BudiFxSecurity(BudiInstrumentTypes.FX_FORWARD);

		security.setIdentifier("FX2");
		security.setAction("AddUpdate");
		security.setxRefContext("Firm");
		security.setDelivery("Cash");
		security.setDescription("FWD.FX");
		security.setInstType(BudiInstrumentTypes.FX_FORWARD.toString().toLowerCase());
		security.setDeliveryDate(DateUtils.toDate("08/01/2020"));
		security.setPayOffSide("Buy");
		security.setNotional(new BigDecimal("10000000"));
		security.setRate(new BigDecimal("0.7533"));
		security.setUnderlyingSecurity("CADUSD");
		security.setCurrency("CAD");
		security.setSettlementCurrency("CAD");

		return security;
	}


	public BudiSecurity createFxForwardExtra() {
		BudiFxSecurity security = new BudiFxSecurity(BudiInstrumentTypes.FX_FWD_EXTRA);

		security.setIdentifier("FX3");
		security.setAction("AddUpdate");
		security.setxRefContext("Firm");
		security.setDelivery("Physical");
		security.setDescription("FX Forward Extra Example");
		security.setInstType(BudiInstrumentTypes.FX_FWD_EXTRA.toString().toLowerCase());
		security.setTradeDate(DateUtils.toDate("08/19/2020"));
		security.setUnderlyingSecurity("CADUSD");
		security.setPayOffSide("Buy");
		security.setNotional(new BigDecimal("1500000"));
		security.setDeliveryDate(DateUtils.toDate("11/20/2020"));
		security.setSettlementCurrency("CAD");
		security.setExpiryDate(DateUtils.toDate("11/19/2020"));
		security.setOptionStrike(new BigDecimal("0.7723"));
		security.setExerciseStyle("AMERICAN");
		security.setOptionType("Call");
		security.setBarrierDirection("DownAndIn");
		security.setBarrierType("Continuous");
		security.setBarrierStartDate(DateUtils.toDate("08/19/2020"));
		security.setBarrierEndDate(DateUtils.toDate("11/19/2020"));
		security.setBarrierLevel(new BigDecimal("0.7151"));
		security.setBarrierRebate(new BigDecimal("1"));
		// option currency in sample file but not referenced anywhere in fx section.

		return security;
	}


	public BudiSecurity createFloatFloatInterestRateSwap() {
		BudiIrSecurity security = new BudiIrSecurity(BudiInstrumentTypes.IR_FLFL);

		// header
		security.setIdentifier("BUDI_IR_FLFL_ANYSOFR_0_1");
		security.setAction("AddUpdate");
		security.setxRefContext("Firm");
		security.setOtcTicker("FLFLA");
		security.setCentralCounterparty("OTC");
		security.setCounterparty("SWAP CNTRPARTY");
		security.setDescription("Interest Rate FLFL");
		security.setInstType(BudiInstrumentTypes.IR_FLFL.toString().toUpperCase());
		security.setEffectiveDate(DateUtils.toDate("09/26/2019"));
		security.setMaturityDate(DateUtils.toDate("09/26/2024"));
		security.setPrincipalExchange("EffectiveAndMaturity");
		security.setDealCurrency("USD");
		security.setDealNotional(new BigDecimal("10000000"));

		// Deal Fees
		List<BudiIrSecurity> fees = new ArrayList<>();
		BudiIrSecurity fee = createFloatFloatInterestRateSwapFee();
		fees.add(fee);
		fee = createFloatFloatInterestRateSwapFee();
		fee.setPayoffSide("Receive");
		fee.setAmount(new BigDecimal("150"));
		fees.add(fee);
		security.setFees(fees);

		// Underlying
		List<BudiIrSecurity> underlyings = new ArrayList<>();
		BudiIrSecurity underlying = createFloatFloatInterestRateSwapUnderlying();
		underlyings.add(underlying);
		underlying = createFloatFloatInterestRateSwapUnderlying();
		underlying.setPayoffSide("Receive");
		underlying.setIndex("SOFRRATE");
		underlying.setPayFrequency("Quarterly");
		underlying.setResetFrequency("Daily");
		underlying.setAccrualDayAdjustment("PaymentDates");
		underlying.setCustomDateGen("");
		underlyings.add(underlying);
		security.setUnderlying(underlyings);

		// Schedule
		List<BudiIrSecurity> schedules = new ArrayList<>();
		BudiIrSecurity schedule = createFloatFloatInterestRateSwapSchedule();
		schedules.add(schedule);
		schedule = createFloatFloatInterestRateSwapSchedule();
		schedule.setStartDate(DateUtils.toDate("12/27/2019"));
		schedule.setEndDate(DateUtils.toDate("03/26/2020"));
		schedule.setPaymentDate(DateUtils.toDate("03/26/2020"));
		schedule.setAmount(new BigDecimal("1000000"));
		schedules.add(schedule);
		schedule = createFloatFloatInterestRateSwapSchedule();
		schedule.setStartDate(DateUtils.toDate("03/26/2020"));
		schedule.setEndDate(DateUtils.toDate("06/26/2020"));
		schedule.setPaymentDate(DateUtils.toDate("06/26/2020"));
		schedule.setAmount(new BigDecimal("1000000"));
		schedules.add(schedule);
		security.setSchedule(schedules);

		return security;
	}


	private BudiIrSecurity createFloatFloatInterestRateSwapFee() {
		BudiIrSecurity fee = new BudiIrSecurity(BudiInstrumentTypes.IR_FLFL);
		fee.setIdentifier("BUDI_IR_FLFL_ANYSOFR_0_1");
		fee.setDate(DateUtils.toDate("09/26/2019"));
		fee.setPayoffSide("Pay");
		fee.setCurrency("USD");
		fee.setAmount(new BigDecimal("200"));
		return fee;
	}


	private BudiIrSecurity createFloatFloatInterestRateSwapUnderlying() {
		BudiIrSecurity underlying = new BudiIrSecurity(BudiInstrumentTypes.IR_FLFL);
		underlying.setIdentifier("BUDI_IR_FLFL_ANYSOFR_0_1");
		underlying.setPayoffSide("Pay");
		underlying.setNotional(new BigDecimal("10000000"));
		underlying.setCurrency("USD");
		underlying.setSpread(new BigDecimal("10"));
		underlying.setIndex("US0012M");
		underlying.setLeverage(new BigDecimal("1"));
		underlying.setPayFrequency("Annual");
		underlying.setDayCountBasis("ACT/360");
		underlying.setFirstPaymentDate(DateUtils.toDate("12/26/2019"));
		underlying.setNextLastPaymentDate(DateUtils.toDate("06/26/2024"));
		underlying.setResetFrequency("Annual");
		underlying.setDaysBeforeAccrual(2);
		underlying.setAccrualDayAdjustment("PaymentAndAccrual");
		underlying.setBusinessDayAdjustment("ModifiedFollowing");
		underlying.setRollDateConvention("Backward");
		underlying.setCustomDateGen("None");
		return underlying;
	}


	private BudiIrSecurity createFloatFloatInterestRateSwapSchedule() {
		BudiIrSecurity schedule = new BudiIrSecurity(BudiInstrumentTypes.IR_FLFL);
		schedule.setIdentifier("BUDI_IR_FLFL_ANYSOFR_0_1");
		schedule.setType("ReceiveLeg");
		schedule.setStartDate(DateUtils.toDate("09/26/2019"));
		schedule.setEndDate(DateUtils.toDate("12/27/2019"));
		schedule.setPaymentDate(DateUtils.toDate("12/27/2019"));
		schedule.setAmount(new BigDecimal("0"));
		return schedule;
	}
}
