package com.clifton.export.file;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.run.ExportRunHistory;

import java.util.Date;


/**
 * Utils for dealing with ExportFiles
 *
 * @author theodorez
 */
public class ExportFileUtils {

	private static final String UNIQUE_IDENTIFIER_APPENDED_TO_FILES = "__RunID-%s_%s";


	/**
	 * Appends a unique identifier to the filename
	 */
	public static String appendUniqueIdentifier(String fileName, ExportRunHistory exportRunHistory) {
		Integer runId = exportRunHistory != null ? exportRunHistory.getId() : null;
		return appendUniqueRunIdentifier(fileName, runId);
	}


	/**
	 * Appends a unique identifier to the filename
	 */
	public static String appendUniqueRunIdentifier(String fileName, Integer runId) {
		String runString = runId != null ? runId.toString() : "none";
		String date = DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE);
		String toAppend = String.format(UNIQUE_IDENTIFIER_APPENDED_TO_FILES, runString, date);
		return FileUtils.appendToFileName(toAppend, fileName);
	}


	/**
	 * Removes the unique identifier from the filename
	 */
	public static String removeUniqueIdentifier(String fileName) {
		if (!StringUtils.isEmpty(fileName)) {
			String fileNameWithoutExtension = FileUtils.getFileNameWithoutExtension(fileName);

			int index = fileNameWithoutExtension.lastIndexOf("__RunID");
			if (index != -1) {
				return fileNameWithoutExtension.substring(0, index) + "." + FileUtils.getFileExtension(fileName);
			}
		}

		return fileName;
	}
}
