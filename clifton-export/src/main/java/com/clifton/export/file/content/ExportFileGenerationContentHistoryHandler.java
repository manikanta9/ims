package com.clifton.export.file.content;

import com.clifton.core.dataaccess.datatable.DataTable;


/**
 * The <code>ExportFileGenerationContentHistoryHandler</code> defines the methods needed to filter and save export content history.
 *
 * @author mwacker
 */
public interface ExportFileGenerationContentHistoryHandler {

	public DataTable filterAndSaveExportHistory(ExportFileGenerationContentHistoryCommand command);
}
