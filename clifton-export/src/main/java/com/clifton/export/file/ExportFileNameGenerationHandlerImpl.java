package com.clifton.export.file;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;
import org.springframework.stereotype.Component;


@Component
public class ExportFileNameGenerationHandlerImpl implements ExportFileNameGenerationHandler {

	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String formatFileName(ExportTemplateCommand<ExportDefinitionContent> templateCommand) {
		ValidationUtils.assertNotNull(templateCommand.getDtoObject(), "Cannot generate file name because no ExportDefinitionContent object was supplied.");

		String fileNameTemplate = templateCommand.getDtoObject().getFileNameTemplate();
		String fileNameSuffixTemplate = templateCommand.getDtoObject().getDefinition().getFileNameSuffixTemplate();

		String fullFileNameTemplate;
		if (fileNameSuffixTemplate == null) {
			fullFileNameTemplate = fileNameTemplate;
		}
		else {
			String fileNameWithoutExtension = FileUtils.getFileNameWithoutExtension(fileNameTemplate);
			String fileExtension = FileUtils.getFileExtension(fileNameTemplate);

			StringBuilder sb = new StringBuilder();
			sb.append(fileNameWithoutExtension);
			sb.append(fileNameSuffixTemplate);
			if (fileExtension != null) {
				sb.append('.');
				sb.append(fileExtension);
			}

			fullFileNameTemplate = sb.toString();
		}

		return formatFileName(fullFileNameTemplate, templateCommand);
	}


	@Override
	public String formatFileName(String fileNameTemplate, ExportTemplateCommand<ExportDefinitionContent> templateCommand) {
		return getExportTemplateGenerationHandler().processTemplate(fileNameTemplate, templateCommand);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}
}
