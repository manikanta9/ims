package com.clifton.export.file.content;

import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.dataaccess.sql.SqlHandler;
import com.clifton.core.dataaccess.sql.SqlSelectCommand;
import com.clifton.core.dataaccess.sql.SqlUpdateCommand;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionContentHistory;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunStatus;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@Component
public class ExportFileGenerationContentHistoryHandlerImpl implements ExportFileGenerationContentHistoryHandler {

	private SqlHandler sqlHandler;

	private ExportRunService exportRunService;
	private ExportDefinitionService exportDefinitionService;

	////////////////////////////////////////////////////////////////////////////


	private static final String HISTORY_QUERY = "SELECT HistoryPKFieldID\n" +
			"FROM ExportDefinitionContentHistory h\n" +
			"\tINNER JOIN ExportRunHistory r ON r.ExportRunHistoryID = h.ExportRunHistoryID\n" +
			"WHERE h.ExportDefinitionContentID = ? AND r.ExportRunStatusID NOT IN (%s)";


	private static final String HISTORY_DELETE = "DELETE h FROM ExportDefinitionContentHistory h\n" +
			"\tINNER JOIN ExportRunHistory r ON r.ExportRunHistoryID = h.ExportRunHistoryID\n" +
			"WHERE h.ExportDefinitionContentID = ? AND r.ScheduledDate < ?";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable filterAndSaveExportHistory(ExportFileGenerationContentHistoryCommand command) {
		ExportDefinitionContent content = command.getDefinitionContent();
		if (!content.isContentHistorySupported()) {
			return command.getDataTable();
		}
		DataTable sourceData = command.getDataTable();
		int historyPKColumnIndex = sourceData.getColumnIndex(content.getHistoryPKColumnName());

		// clear old data if needed
		clearOldHistoryData(command);

		// get the list of id's for the already added items
		Set<String> contentHistories = getContentHistory(command);

		DataTable resultData = new PagingDataTableImpl(sourceData.getColumnList());

		List<ExportDefinitionContentHistory> newExportDefinitionContentHistoryList = new ArrayList<>();
		Set<String> foundObjects = new HashSet<>();
		for (int rowIndex = 0; rowIndex < sourceData.getTotalRowCount(); rowIndex++) {
			DataRow row = sourceData.getRow(rowIndex);
			// always add skipped rows to output but do not add to content history.
			if (command.getSkipLineCount() > 0 && rowIndex < command.getSkipLineCount()) {
				resultData.addRow(new DataRowImpl(resultData, ((DataRowImpl) row).getData()));
				continue;
			}
			String historyPKValue = getHistoryPKValueFromRow(row, historyPKColumnIndex, content.getHistoryPKColumnName());

			if (!contentHistories.contains(historyPKValue)) {
				resultData.addRow(new DataRowImpl(resultData, ((DataRowImpl) row).getData()));

				// only create the history entry once, but don't filter the duplicate out of the file
				if (!foundObjects.contains(historyPKValue)) {
					newExportDefinitionContentHistoryList.add(createContentHistory(historyPKValue, command));
					foundObjects.add(historyPKValue);
				}
			}
		}

		if (!command.isPreview()) {
			getExportDefinitionService().saveExportDefinitionContentHistoryList(newExportDefinitionContentHistoryList);
		}

		return resultData;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////                 Helper Methods                   //////////////
	////////////////////////////////////////////////////////////////////////////


	private ExportDefinitionContentHistory createContentHistory(String historyPKValue, ExportFileGenerationContentHistoryCommand command) {
		ExportDefinitionContentHistory history = new ExportDefinitionContentHistory();
		history.setExportDefinitionContent(command.getDefinitionContent());
		history.setExportRunHistory(command.getExportRunHistory());
		history.setHistoryPKFieldId(historyPKValue);
		return history;
	}


	private String getHistoryPKValueFromRow(DataRow row, int historyPKColumnIndex, String historyPKColumnName) {
		Object historyPKValue = row.getValue(historyPKColumnIndex);
		ValidationUtils.assertNotNull(historyPKValue, "Data row is missing value for the history PK column [" + historyPKColumnName + "].");

		return historyPKValue.toString();
	}


	private Set<String> getContentHistory(ExportFileGenerationContentHistoryCommand command) {
		Object[] values = ExportRunStatus.ExportRunStatusNames.getExportRunStatusNameListFailed().stream().map(status -> getExportRunService().getExportRunStatusByName(status).getId()).toArray();
		String statusString = ArrayUtils.toString(values);

		String sqlString = HISTORY_QUERY;
		Date fromDate = null;
		if (command.getDefinitionContent().getHistoryDaysToInclude() != null) {
			fromDate = DateUtils.addDays(DateUtils.clearTime(new Date()), -command.getDefinitionContent().getHistoryDaysToInclude().intValue());
			sqlString += "\n\tAND r.ScheduledDate >= ?";
		}

		SqlSelectCommand sql = new SqlSelectCommand(String.format(sqlString, statusString));
		sql.addIntegerParameterValue(command.getDefinitionContent().getId());

		if (fromDate != null) {
			sql.addDateParameterValue(fromDate);
		}

		ResultSetExtractor<Set<String>> resultSetExtractor = resultSet -> {
			Set<String> result = new HashSet<>();
			while (resultSet.next()) {
				result.add(resultSet.getString(1));
			}
			return result;
		};
		return getSqlHandler().executeSelect(sql, resultSetExtractor);
	}


	private void clearOldHistoryData(ExportFileGenerationContentHistoryCommand command) {
		if (!command.isPreview() && command.getDefinitionContent().getHistoryDaysToKeep() != null) {
			Date startDate = DateUtils.addDays(DateUtils.clearTime(new Date()), -command.getDefinitionContent().getHistoryDaysToKeep().intValue());

			// using an SQL command here to avoid execution of lookup query and then a delete
			SqlUpdateCommand sql = new SqlUpdateCommand(HISTORY_DELETE);
			sql.addIntegerParameterValue(command.getDefinitionContent().getId());
			sql.addDateParameterValue(startDate);
			getSqlHandler().executeUpdate(sql);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SqlHandler getSqlHandler() {
		return this.sqlHandler;
	}


	public void setSqlHandler(SqlHandler sqlHandler) {
		this.sqlHandler = sqlHandler;
	}


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}
}
