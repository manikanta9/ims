package com.clifton.export.file.content;

import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;


/**
 * The <code>ExportFileGenerationContentHistoryCommand</code> contains all items needed to filter and create the content export history.
 *
 * @author mwacker
 */
public class ExportFileGenerationContentHistoryCommand {

	private DataTable dataTable;

	private ExportDefinitionContent definitionContent;

	private ExportRunHistory exportRunHistory;

	private boolean preview;

	private short skipLineCount;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DataTable getDataTable() {
		return this.dataTable;
	}


	public void setDataTable(DataTable dataTable) {
		this.dataTable = dataTable;
	}


	public ExportDefinitionContent getDefinitionContent() {
		return this.definitionContent;
	}


	public void setDefinitionContent(ExportDefinitionContent definitionContent) {
		this.definitionContent = definitionContent;
	}


	public ExportRunHistory getExportRunHistory() {
		return this.exportRunHistory;
	}


	public void setExportRunHistory(ExportRunHistory exportRunHistory) {
		this.exportRunHistory = exportRunHistory;
	}


	public boolean isPreview() {
		return this.preview;
	}


	public void setPreview(boolean preview) {
		this.preview = preview;
	}


	public short getSkipLineCount() {
		return this.skipLineCount;
	}


	public void setSkipLineCount(short skipLineCount) {
		this.skipLineCount = skipLineCount;
	}
}
