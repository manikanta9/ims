package com.clifton.export.file;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.content.transformation.ExportTransformationGenerator;
import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.context.ExportContextGenerator;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.file.content.ExportFileGenerationContentHistoryCommand;
import com.clifton.export.file.content.ExportFileGenerationContentHistoryHandler;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;
import com.clifton.export.transformation.ExportTransformation;
import com.clifton.export.transformation.ExportTransformationService;
import com.clifton.export.transformation.search.ExportTransformationSearchForm;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Component
@SuppressWarnings("rawtypes")
public class ExportFileGenerationHandlerImpl implements ExportFileGenerationHandler {

	////////////////////////////////////////////////////////////////////////////

	private ExportTransformationService exportTransformationService;
	private ExportFileNameGenerationHandler exportFileNameGenerationHandler;
	private ExportFileGenerationContentHistoryHandler exportFileGenerationContentHistoryHandler;
	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;

	private final SecureRandom secureRandom = new SecureRandom();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@SuppressWarnings("unchecked")
	public List<FileWrapper> generateTempExportFileWrapperList(List<ExportDefinitionContent> contentList, ExportRunHistory runHistory, Date previewDate, Map<String, Object> configurationMap) {
		List<FileWrapper> exportFiles = new ArrayList<>();


		Map<String, Object> exportFileNameContextMap = new HashMap<>();

		if (previewDate != null) {
			exportFileNameContextMap.put(ExportTemplateGenerationHandler.PREVIEW_DATE_KEY, previewDate);
		}

		for (ExportDefinitionContent content : CollectionUtils.getIterable(contentList)) {
			if (!content.isDisabled() && !content.isNotGenerateFile()) {
				ExportContentGenerator generator = getExportContentGenerator(content);
				Object data = generateAndTransformData(generator, content, runHistory, true, previewDate, configurationMap);
				//If there is no data and we are skipping, then no point in processing further
				if (data == null && content.isSkipIfNoContentGenerated()) {
					continue;
				}
				if (data instanceof FileWrapper) {
					exportFiles.add((FileWrapper) data);
				}
				else if (data instanceof List && !CollectionUtils.isEmpty((List) data) && ((List) data).get(0) instanceof FileWrapper) {
					exportFiles.addAll((List<FileWrapper>) data);
				}
				else {
					//We don't append a unique identifier to the file in this case since it isn't necessary - we aren't copying the temp files the export folder
					String filename = getExportFileNameGenerationHandler().formatFileName(new ExportTemplateCommand<>(generator, content, content.getDefinition(), exportFileNameContextMap));
					List<FileWrapper> fileWrapperList = generator.writeContent(data, filename, content, runHistory, previewDate);
					if (!CollectionUtils.isEmpty(fileWrapperList)) {
						exportFiles.addAll(fileWrapperList);
					}
				}
			}
		}

		return exportFiles;
	}


	@Override
	public ExportFileGenerationResult generateExportFileList(ExportDefinition definition, ExportRunHistory runHistory, String rootDirectory) {
		return generateExportFileList(definition, runHistory, rootDirectory, null);
	}


	@Override
	@SuppressWarnings("unchecked")
	public ExportFileGenerationResult generateExportFileList(ExportDefinition definition, ExportRunHistory runHistory, String rootDirectory, Map<String, Object> configurationMap) {
		List<ExportDefinitionContent> contents = definition.getContentList();

		List<ExportFile> exportFiles = new ArrayList<>();
		List<ExportDefinitionContent> contentsThatGeneratedFiles = new ArrayList<>();
		List<ExportDefinitionContent> contentsThatDidNotGenerateFiles = new ArrayList<>(contents); //initialize

		Map<ExportDefinitionContent, List<FileWrapper>> generatedTempFiles = new HashMap<>();
		Map<ExportDefinitionContent, Map<ExportContentContextMapKeys, Object>> contentContexts = new HashMap<>();

		try {
			//Generate all of the export files
			Map<String, Object> exportFileNameContextMap = new HashMap<>();
			for (ExportDefinitionContent content : contents) {
				if (content.isDisabled()) {
					continue;
				}

				if (content.getContentReadySystemCondition() != null) {
					EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(content.getContentReadySystemCondition(), content);
					ValidationUtils.assertTrue(result.isResult(), result.getMessage());
				}

				ExportContentGenerator generator = getExportContentGenerator(content);
				Object data = generateAndTransformData(generator, content, runHistory, configurationMap);
				//If there is no data and we are skipping, then no point in processing further
				if (data == null && content.isSkipIfNoContentGenerated()) {
					continue;
				}

				if (content.getContextSystemBean() != null) {
					ExportContextGenerator contextGenerator = (ExportContextGenerator) getSystemBeanService().getBeanInstance(content.getContextSystemBean());
					contentContexts.put(content, contextGenerator.getContext(content, data));
				}

				if (data instanceof FileWrapper) {
					generatedTempFiles.computeIfAbsent(content, k -> new ArrayList<>()).add((FileWrapper) data);
				}
				else if (data instanceof List && !CollectionUtils.isEmpty((List) data) && ((List) data).get(0) instanceof FileWrapper) {
					generatedTempFiles.computeIfAbsent(content, k -> new ArrayList<>()).addAll((List<FileWrapper>) data);
				}
				else {
					if (content.isNotGenerateFile()) {
						exportFiles.add(new ExportFile(StringUtils.EMPTY_STRING, content, contentContexts.get(content)));
					}
					else {
						String fileName = formatFileNameWithUniqueIdentifier(runHistory, new ExportTemplateCommand<>(generator, content, content.getDefinition(), exportFileNameContextMap));
						List<FileWrapper> fileWrapperList = generator.writeContent(data, fileName, content, runHistory);
						if (!CollectionUtils.isEmpty(fileWrapperList)) {
							generatedTempFiles.computeIfAbsent(content, k -> new ArrayList<>()).addAll(fileWrapperList);
						}
					}
				}
				contentsThatDidNotGenerateFiles.remove(content);
				contentsThatGeneratedFiles.add(content);
			}


			//If zipping is enabled, then zip the files to the export directory (if there were any files generated)
			if (definition.isZipFilesForExport()) {
				if (!CollectionUtils.isEmpty(generatedTempFiles)) {
					//Set the "original" file names for each generated temp file (the name of the file without the unique identifier).
					setOriginalFileNames(generatedTempFiles.values());
					String fileName = formatFileNameWithUniqueIdentifier(definition.getZippedFileNameTemplate(), runHistory, new ExportTemplateCommand<>(null, null, definition, exportFileNameContextMap));
					String fullPath = FileUtils.combinePaths(rootDirectory, fileName);
					ZipUtils.zipFiles(new ArrayList<>(generatedTempFiles.values().stream().collect(ArrayList::new, ArrayList::addAll, ArrayList::addAll)), fullPath);
					exportFiles.add(new ExportFile(fullPath, null));
				}
			}
			//Otherwise, copy the files to the export directory
			else {
				for (Map.Entry<ExportDefinitionContent, List<FileWrapper>> exportDefinitionContentListEntry : generatedTempFiles.entrySet()) {
					List<FileWrapper> fileList = exportDefinitionContentListEntry.getValue();
					if (!CollectionUtils.isEmpty(fileList)) {
						fileList.forEach(file -> {
							String destinationFileNameWithPath = FileUtils.combinePaths(rootDirectory, file.getFileName());
							try {
								FileUtils.copyFileCreatePathAndOverwrite(file.getFile(), FilePath.forPath(destinationFileNameWithPath));
								exportFiles.add(new ExportFile(destinationFileNameWithPath, exportDefinitionContentListEntry.getKey(), contentContexts.get(exportDefinitionContentListEntry.getKey())));
							}
							catch (IOException e) {
								// if one file fails in any content, delete what was copied and error the entire export
								deleteExportedFiles(exportFiles);
								throw new RuntimeException("Unable to copy file: " + file.getFileName(), e);
							}
						});
					}
				}
			}
		}
		//Delete all of the temp files
		finally {
			for (Map.Entry<ExportDefinitionContent, List<FileWrapper>> exportDefinitionContentListEntry : generatedTempFiles.entrySet()) {
				List<FileWrapper> fileList = exportDefinitionContentListEntry.getValue();
				fileList.forEach(file -> {
					if (file.isTempFile()) {
						try {
							if (FileUtils.fileExists(file.getFile())) {
								FileUtils.delete(file.getFile());
							}
						}
						catch (Exception e) {
							LogUtils.error(getClass(), "Failed to delete file [" + file.getFile().getPath() + "].", e);
						}
					}
				});
			}
		}

		return new ExportFileGenerationResult(exportFiles, contentsThatGeneratedFiles, contentsThatDidNotGenerateFiles, definition.isZipFilesForExport());
	}


	private void deleteExportedFiles(List<ExportFile> exportFileList) {
		for (ExportFile file : CollectionUtils.getIterable(exportFileList)) {
			try {
				FileUtils.delete(new File(file.getPath()));
			}
			catch (Throwable e) {
				LogUtils.error(getClass(), String.format("Error occurred while attempting to delete the exported files due to an error running the export definition - could not delete file: %s", file.getPath()));
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private DataTable filterAndSaveExportHistory(DataTable dataTable, ExportDefinitionContent content, boolean preview, ExportRunHistory exportRunHistory) {
		if (content.isContentHistorySupported()) {
			ExportFileGenerationContentHistoryCommand contentHistoryCommand = new ExportFileGenerationContentHistoryCommand();
			contentHistoryCommand.setDataTable(dataTable);
			contentHistoryCommand.setDefinitionContent(content);
			contentHistoryCommand.setPreview(preview);
			contentHistoryCommand.setExportRunHistory(exportRunHistory);
			short skipCount = content.getNumberOfRowsToSkip() != null && content.getNumberOfRowsToSkip() > 0 ? content.getNumberOfRowsToSkip() : 0;
			contentHistoryCommand.setSkipLineCount(skipCount);

			return getExportFileGenerationContentHistoryHandler().filterAndSaveExportHistory(contentHistoryCommand);
		}
		return dataTable;
	}


	private Object generateAndTransformData(ExportContentGenerator generator, ExportDefinitionContent content, ExportRunHistory exportRunHistory, Map<String, Object> configurationMap) {
		return generateAndTransformData(generator, content, exportRunHistory, false, null, configurationMap);
	}


	@SuppressWarnings("unchecked")
	private Object generateAndTransformData(ExportContentGenerator generator, ExportDefinitionContent content, ExportRunHistory exportRunHistory, boolean preview, Date previewDate, Map<String, Object> configurationMap) {
		Object data = generator.generateContentData(content, previewDate, configurationMap);

		// filter to data that hasn't been exported yet
		if (data instanceof DataTable) {
			DataTable table = (DataTable) data;
			table = filterAndSaveExportHistory(table, content, preview, exportRunHistory);
			//if empty then there is no data, don't process further
			short skipLines = content.getNumberOfRowsToSkip() != null && content.getNumberOfRowsToSkip() > 0 ? content.getNumberOfRowsToSkip() : 0;
			if (table.getTotalRowCount() <= skipLines && content.isSkipIfNoContentGenerated()) {
				return null;
			}
			data = table;
		}

		Map<String, Object> contextMap = generator.getTemplateBeanMap(content, previewDate);

		//NOTE: if there is no data and processing continues, the transformation generators could potentially generate static content
		List<ExportTransformation> transformationList = getTransformationList(content);
		for (ExportTransformation transformation : CollectionUtils.getIterable(transformationList)) {
			ExportTransformationGenerator transformationGenerator = getExportTransformationGenerator(transformation);
			if (data == null) {
				throw new RuntimeException("Cannot apply transformation " + transformationGenerator.getClass() + " because the input data is null.");
			}
			else if (transformationGenerator.getInputClass().isAssignableFrom(data.getClass())) {
				// use random integer when using preview
				Integer runId = exportRunHistory == null ? this.secureRandom.nextInt() : exportRunHistory.getId();
				data = transformationGenerator.generateTransformation(data, runId, content, contextMap);
			}
			else {
				throw new RuntimeException("Cannot apply transformation " + transformationGenerator.getClass() + " because the input class is not valid: " + data.getClass());
			}
		}
		return data;
	}


	private List<ExportTransformation> getTransformationList(ExportDefinitionContent content) {
		ExportTransformationSearchForm searchForm = new ExportTransformationSearchForm();
		searchForm.setOrderBy("order");
		searchForm.setDefinitionContentId(content.getId());
		return getExportTransformationService().getExportTransformationList(searchForm);
	}


	private String formatFileNameWithUniqueIdentifier(ExportRunHistory exportRunHistory, ExportTemplateCommand<ExportDefinitionContent> templateCommand) {
		String fileName = getExportFileNameGenerationHandler().formatFileName(templateCommand);
		return ExportFileUtils.appendUniqueIdentifier(fileName, exportRunHistory);
	}


	public String formatFileNameWithUniqueIdentifier(String fileNameTemplate, ExportRunHistory exportRunHistory, ExportTemplateCommand<ExportDefinitionContent> templateCommand) {
		String fileName = getExportFileNameGenerationHandler().formatFileName(fileNameTemplate, templateCommand);
		return ExportFileUtils.appendUniqueIdentifier(fileName, exportRunHistory);
	}


	private void setOriginalFileNames(Collection<List<FileWrapper>> filesList) {
		filesList.forEach(fileList -> fileList.forEach(file -> file.setFileName(ExportFileUtils.removeUniqueIdentifier(file.getFileName()))));
	}


	private ExportContentGenerator getExportContentGenerator(ExportDefinitionContent content) {
		return (ExportContentGenerator) getSystemBeanService().getBeanInstance(content.getContentSystemBean());
	}


	private ExportTransformationGenerator getExportTransformationGenerator(ExportTransformation transformation) {
		return (ExportTransformationGenerator) getSystemBeanService().getBeanInstance(transformation.getTransformationSystemBean());
	}


	////////////////////////////////////////////////////////////////////////////
	//////                  Getters and Setters                           //////
	////////////////////////////////////////////////////////////////////////////


	public ExportFileNameGenerationHandler getExportFileNameGenerationHandler() {
		return this.exportFileNameGenerationHandler;
	}


	public void setExportFileNameGenerationHandler(ExportFileNameGenerationHandler exportFileNameGenerationHandler) {
		this.exportFileNameGenerationHandler = exportFileNameGenerationHandler;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public ExportTransformationService getExportTransformationService() {
		return this.exportTransformationService;
	}


	public void setExportTransformationService(ExportTransformationService exportTransformationService) {
		this.exportTransformationService = exportTransformationService;
	}


	public ExportFileGenerationContentHistoryHandler getExportFileGenerationContentHistoryHandler() {
		return this.exportFileGenerationContentHistoryHandler;
	}


	public void setExportFileGenerationContentHistoryHandler(ExportFileGenerationContentHistoryHandler exportFileGenerationContentHistoryHandler) {
		this.exportFileGenerationContentHistoryHandler = exportFileGenerationContentHistoryHandler;
	}
}
