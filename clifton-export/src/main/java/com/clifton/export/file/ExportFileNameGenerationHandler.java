package com.clifton.export.file;


import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.template.ExportTemplateCommand;


/**
 * The <code>ExportFileNameGenerationHandler</code> handles the generation of file names for export files.
 *
 * @author jgommels
 */
public interface ExportFileNameGenerationHandler {

	/**
	 * Build the standard file name template and then return the result of the Freemarker replacement.
	 */
	public String formatFileName(ExportTemplateCommand<ExportDefinitionContent> templateCommand);


	/**
	 * Apply the Freemarker replacement to the filename template.
	 */
	public String formatFileName(String fileNameTemplate, ExportTemplateCommand<ExportDefinitionContent> templateCommand);
}
