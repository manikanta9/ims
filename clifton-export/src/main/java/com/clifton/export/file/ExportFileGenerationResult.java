package com.clifton.export.file;


import com.clifton.export.definition.ExportDefinitionContent;

import java.util.List;


/**
 * The <code>ExportFileGenerationResult</code> contains the results from generating export files.
 *
 * @author jgommels
 */
public class ExportFileGenerationResult {

	/**
	 * The files to be exported. May contain a single zip file if zipping is enabled.
	 */
	private final List<ExportFile> exportFiles;

	/**
	 * The list of {@link ExportDefinitionContent}s that generated files.
	 */
	private final List<ExportDefinitionContent> contentsThatGeneratedFiles;

	/**
	 * The list of {@link ExportDefinitionContent}s that did not generate files. For example, if the content was disabled or if it had
	 * no results at the time the export ran.
	 */
	private final List<ExportDefinitionContent> contentsThatDidNotGenerateFiles;

	/**
	 * Whether the files were zipped. If this is the case, {@link #getExportFiles()} will contain one file.
	 */
	private final boolean filesZipped;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportFileGenerationResult(List<ExportFile> exportFiles, List<ExportDefinitionContent> contentsThatGeneratedFiles, List<ExportDefinitionContent> contentsThatDidNotGenerateFiles,
	                                  boolean filesZipped) {
		this.exportFiles = exportFiles;
		this.contentsThatGeneratedFiles = contentsThatGeneratedFiles;
		this.contentsThatDidNotGenerateFiles = contentsThatDidNotGenerateFiles;
		this.filesZipped = filesZipped;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<ExportFile> getExportFiles() {
		return this.exportFiles;
	}


	public List<ExportDefinitionContent> getContentsThatGeneratedFiles() {
		return this.contentsThatGeneratedFiles;
	}


	public List<ExportDefinitionContent> getContentsThatDidNotGenerateFiles() {
		return this.contentsThatDidNotGenerateFiles;
	}


	public boolean isFilesZipped() {
		return this.filesZipped;
	}
}
