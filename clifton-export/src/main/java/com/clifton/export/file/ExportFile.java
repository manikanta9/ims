package com.clifton.export.file;


import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.Map;


/**
 * The <code>ExportFile</code> represents a file that is to be exported.
 *
 * @author jgommels
 */
public class ExportFile {

	/**
	 * The path to the file to be exported.
	 */
	private final String path;

	/**
	 * The {@link ExportDefinitionContent} that generated the file to be exported. May be <code>null</code> if no single specific content is associated with the
	 * file (e.g. if the file is a zipped file containing several files.)
	 */
	private final ExportDefinitionContent exportContent;

	private final Map<ExportContentContextMapKeys, Object> fileContext;


	public ExportFile(String path, ExportDefinitionContent exportContent) {
		this(path, exportContent, null);
	}


	public ExportFile(String path, ExportDefinitionContent exportContent, Map<ExportContentContextMapKeys, Object> fileContext) {
		this.path = path;
		this.exportContent = exportContent;
		this.fileContext = fileContext;
	}


	/**
	 * @return {@link #path}
	 */

	public String getPath() {
		return this.path;
	}


	/**
	 * @return {@link #exportContent}
	 */

	public ExportDefinitionContent getExportContent() {
		return this.exportContent;
	}


	public Map<ExportContentContextMapKeys, Object> getFileContext() {
		return this.fileContext;
	}
}
