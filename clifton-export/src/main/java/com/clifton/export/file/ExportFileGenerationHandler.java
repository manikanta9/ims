package com.clifton.export.file;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>ExportFileGenerationHandler</code> generates files that are to be exported.
 *
 * @author jgommels
 */
public interface ExportFileGenerationHandler {

	/**
	 * Generates and returns a list of temp files generated from the specified contents. Does not copy the files to the final
	 * export folder.
	 *
	 * @param contents    the contents to generate files for
	 * @param runHistory  the {@link ExportRunHistory} associated with the contents. May be <code>null</code> if there is no actual run
	 *                    associated (e.g. if this is a 'preview').
	 * @param previewDate - a preview date value that, when defined, is used to generate the file names (that may include dates) and data for that date.
	 */
	public List<FileWrapper> generateTempExportFileWrapperList(List<ExportDefinitionContent> contents, ExportRunHistory runHistory, Date previewDate, Map<String, Object> configurationMap);


	/**
	 * Generates the export file for each {@link  ExportDefinitionContent}, then copies it to {@code rootDirectory}.
	 *
	 * @param definition    used to lookup the contents to generate files for
	 * @param runHistory    the {@link ExportRunHistory} associated with the contents. May be <code>null</code> if there is no actual run
	 *                      associated (e.g. if this is a 'preview').
	 * @param rootDirectory the root directory that the files should be copied to
	 * @return the list of {@link ExportFile}s, which wrap the file path and the associated {@link ExportDefinitionContent}.
	 */
	public ExportFileGenerationResult generateExportFileList(ExportDefinition definition, ExportRunHistory runHistory, String rootDirectory);


	/**
	 * Generates the export file for each {@link  ExportDefinitionContent}, then copies it to {@code rootDirectory}.
	 *
	 * @param definition       used to lookup the contents to generate files for
	 * @param runHistory       the {@link ExportRunHistory} associated with the contents. May be <code>null</code> if there is no actual run
	 *                         associated (e.g. if this is a 'preview').
	 * @param rootDirectory    the root directory that the files should be copied to
	 * @param configurationMap
	 * @return the list of {@link ExportFile}s, which wrap the file path and the associated {@link ExportDefinitionContent}.
	 */
	public ExportFileGenerationResult generateExportFileList(ExportDefinition definition, ExportRunHistory runHistory, String rootDirectory, Map<String, Object> configurationMap);
}
