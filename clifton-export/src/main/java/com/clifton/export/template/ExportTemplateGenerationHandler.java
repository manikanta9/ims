package com.clifton.export.template;

import com.clifton.core.beans.IdentityObject;


/**
 * Defines and object used to convert a Freemarker template to the resulting string.  The command object
 * is used to build the context object needed for the string replacement in the Freemarker template.
 *
 * @author theodorez
 */
public interface ExportTemplateGenerationHandler {

	public final String PREVIEW_DATE_KEY = "PREVIEW_DATE_KEY";


	public <T extends IdentityObject> String processTemplate(String template, ExportTemplateCommand<T> command);
}
