package com.clifton.export.template;

import com.clifton.core.beans.IdentityObject;
import com.clifton.export.definition.ExportDefinition;

import java.util.HashMap;
import java.util.Map;


/**
 * Command for applying Freemarker templates to strings.
 *
 * @author mwacker
 */
public class ExportTemplateCommand<T extends IdentityObject> {

	private final ExportTemplateGenerator<T> exportTemplateGenerator;
	private final T dtoObject;
	private final ExportDefinition definition;
	private final Map<String, Object> contextMap;


	public ExportTemplateCommand(ExportTemplateGenerator<T> exportTemplateGenerator, T dtoObject, ExportDefinition definition, Map<String, Object> contextMap) {
		this.exportTemplateGenerator = exportTemplateGenerator;
		this.dtoObject = dtoObject;
		this.definition = definition;
		this.contextMap = contextMap == null ? new HashMap<>() : contextMap;
	}


	public ExportTemplateGenerator<T> getExportTemplateGenerator() {
		return this.exportTemplateGenerator;
	}


	public T getDtoObject() {
		return this.dtoObject;
	}


	public ExportDefinition getDefinition() {
		return this.definition;
	}


	public Map<String, Object> getContextMap() {
		return this.contextMap;
	}
}
