package com.clifton.export.template;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.search.ExportRunHistorySearchForm;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * @author theodorez
 */
@Component
public class ExportTemplateGenerationHandlerImpl implements ExportTemplateGenerationHandler {

	public static final String EXPORT_VARIABLE_DATE = "DATE";
	public static final String EXPORT_VARIABLE_EFFECTIVE_DATE = "EFFECTIVE_DATE";
	public static final String EXPORT_VARIABLE_DAILY_RUN_COUNT = "DAILY_RUN_COUNT";


	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private ExportRunService exportRunService;
	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public <T extends IdentityObject> String processTemplate(String template, ExportTemplateCommand<T> command) {
		Map<String, Object> contextMap = command.getContextMap();
		if (command.getExportTemplateGenerator() != null) {
			Map<String, Object> dtoObjectVariables = command.getExportTemplateGenerator().getTemplateBeanMap(command.getDtoObject(), null);
			if (dtoObjectVariables != null) {
				contextMap.putAll(dtoObjectVariables);
			}
		}
		contextMap = appendCommonTemplateItems(template, command);

		TemplateConfig config = new TemplateConfig(template);
		if (!CollectionUtils.isEmpty(contextMap)) {
			for (Map.Entry<String, Object> entry : CollectionUtils.getIterable(contextMap.entrySet())) {
				config.addBeanToContext(StringUtils.formatFreemarkerVariable(entry.getKey()), entry.getValue());
			}
		}

		return getTemplateConverter().convert(config);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private <T extends IdentityObject> Map<String, Object> appendCommonTemplateItems(String template, ExportTemplateCommand<T> command) {
		Date dateWithTime = new Date();
		Date date = DateUtils.clearTime(dateWithTime);

		Map<String, Object> contextMap = command.getContextMap();

		if (!contextMap.containsKey(EXPORT_VARIABLE_DATE)) {
			contextMap.put(EXPORT_VARIABLE_DATE, dateWithTime);
		}

		if (contextMap.containsKey(EXPORT_VARIABLE_DATE) && contextMap.containsKey(ExportTemplateGenerationHandler.PREVIEW_DATE_KEY)) {
			contextMap.put(EXPORT_VARIABLE_DATE, contextMap.get(ExportTemplateGenerationHandler.PREVIEW_DATE_KEY));
		}

		if (command.getDefinition() != null) {

			// If a preview date is specified in the context, use that date for the effective date
			if (contextMap.containsKey(ExportTemplateGenerationHandler.PREVIEW_DATE_KEY)) {
				Date previewDate = (Date) contextMap.get(ExportTemplateGenerationHandler.PREVIEW_DATE_KEY);
				ValidationUtils.assertNotNull(previewDate, "Invalid preview date found in export context map.");
				contextMap.put(EXPORT_VARIABLE_EFFECTIVE_DATE, previewDate);
			}
			else if (template.contains(EXPORT_VARIABLE_EFFECTIVE_DATE) && !contextMap.containsKey(EXPORT_VARIABLE_EFFECTIVE_DATE)) {
				DateGenerationOptions dateGenerationOption = command.getDefinition().getEffectiveDateGenerationOption();

				if (dateGenerationOption == null) {
					throw new RuntimeException("'" + EXPORT_VARIABLE_EFFECTIVE_DATE + "' variable was specified, but was not configured on the Definition.");
				}

				Date effectiveDate = getCalendarDateGenerationHandler().generateDate(dateGenerationOption, command.getDefinition().getEffectiveDateDaysFromToday());
				contextMap.put(EXPORT_VARIABLE_EFFECTIVE_DATE, effectiveDate);
			}

			if (template.contains(EXPORT_VARIABLE_DAILY_RUN_COUNT) && !contextMap.containsKey(EXPORT_VARIABLE_DAILY_RUN_COUNT)) {
				ExportRunHistorySearchForm searchForm = new ExportRunHistorySearchForm();
				searchForm.setDefinitionId(command.getDefinition().getId());
				searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, date));
				searchForm.addSearchRestriction(new SearchRestriction("startDate", ComparisonConditions.LESS_THAN, DateUtils.addDays(date, 1)));

				List<ExportRunHistory> runHistoryList = getExportRunService().getExportRunHistoryList(searchForm);

				/*
				 * The run count should be equal the number of ExportRunHistory records that ran on the current day (this list will include the already-saved
				 * ExportRunHistory record for this run).
				 *
				 * TODO If a unique run count needs to be guaranteed for each file, then we'll need to consider using a different solution such as storing
				 * a sequence number. (As is, if two exports run at the same time, they may generate the same run count.)
				 */
				int runCount = CollectionUtils.getSize(runHistoryList);
				contextMap.put(EXPORT_VARIABLE_DAILY_RUN_COUNT, runCount);
			}
		}
		return contextMap;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}
}
