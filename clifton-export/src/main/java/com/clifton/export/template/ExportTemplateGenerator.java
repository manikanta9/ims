package com.clifton.export.template;

import com.clifton.core.beans.IdentityObject;

import java.util.Date;
import java.util.Map;


/**
 * Defines an object that will provide a map to be used in a Freemarker conversion.
 *
 * @author theodorez
 */
public interface ExportTemplateGenerator<T extends IdentityObject> {

	/**
	 * Generate the variable name to object map used by Freemarker to format the file name from the template. May return null if there
	 * are no template variables specific to this content generator.  If previewDate is defined, implementations should
	 * use the previewDate instead of the current date for data generation.
	 */
	default public Map<String, Object> getTemplateBeanMap(T dtoObject, Date previewDate) {
		return null;
	}
}
