package com.clifton.export.run.runner;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.AbstractStatusAwareRunner;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.dynamic.ExportDefinitionDynamicConfigurationGenerator;
import com.clifton.export.file.ExportFile;
import com.clifton.export.file.ExportFileGenerationHandler;
import com.clifton.export.file.ExportFileGenerationResult;
import com.clifton.export.messaging.ExportMessagingMessageConverterService;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunSourceTypes;
import com.clifton.export.run.ExportRunStatus;
import com.clifton.export.run.ExportRunStatus.ExportRunStatusNames;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ExportRunner</code> class represents a Runner for a specific run of an Export Definition. It will manage updates to definition's status as well as
 * corresponding history record.
 * <p>
 * The run will acquire the Export Destination and Contents defined by Export Definition and will put it on a JMS queue for the Integration Export service to
 * actually execute it.
 */
public class ExportRunner extends AbstractStatusAwareRunner {

	public static final String RUNNER_TYPE = "EXPORT";

	////////////////////////////////////////////////////////////////////////////

	private final int exportDefinitionId;
	private final ExportRunSourceTypes exportRunSourceType;
	private final SecurityUser runAsUser;
	/**
	 * Run Context can be used to share data among job steps and to return results
	 * ("result" key) back to the runner which will be stored in job history description.
	 */
	private final Map<String, Object> runContext = new HashMap<>();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ContextHandler contextHandler;
	private ExportDefinitionService exportDefinitionService;
	private ExportRunnerFactory exportRunnerFactory;
	private ExportMessagingMessageConverterService exportMessagingMessageConverterService;
	private ExportRunnerService exportRunnerService;
	private ExportRunService exportRunService;
	private ExportFileGenerationHandler exportFileGenerationHandler;
	private SystemBeanService systemBeanService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SecurityUserService securityUserService;

	/**
	 * The sender used for putting the message on a queue which will then be
	 * picked up by the Integration Export service to send the export.
	 */
	private AsynchronousMessageHandler exportClientAsynchronousMessageHandler;

	/**
	 * where the secured FTP files are going to be located
	 */
	private String rootDirectory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	ExportRunner(Date runDate, int exportDefinitionId, ExportRunSourceTypes exportRunSourceType, SecurityUser runAsUser) {
		super(RUNNER_TYPE, Integer.toString(exportDefinitionId), runDate);
		this.exportDefinitionId = exportDefinitionId;
		this.exportRunSourceType = exportRunSourceType;
		this.runAsUser = runAsUser;
	}


	@Override
	public void run() {
		ExportDefinition exportDefinition = getExportDefinitionService().getExportDefinitionPopulated(this.exportDefinitionId);
		ValidationUtils.assertTrue(exportDefinition.isActive(), "Cannot run export [" + exportDefinition.getLabel() + "] because it is not active as of [" + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_INPUT) + "].");

		if (ExportRunStatusNames.RUNNING == exportDefinition.getStatus().getExportRunStatusName()) {
			// do not start another instance of the same job if the job is still running
			LogUtils.info(getClass(), "Skipping Export " + exportDefinition.getLabel() + " run because its previous run is still in progress");
			return;
		}

		// Set the current user the 'runAsUser'
		Object currentUser = getContextHandler().getBean(Context.USER_BEAN_NAME);
		try {
			// set the Run As User as current user
			getContextHandler().setBean(Context.USER_BEAN_NAME, this.runAsUser);

			getStatus().setMessage("Sending Export Definition '" + exportDefinition.getLabel() + "' to on ActiveMQ queue on '"
					+ (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)) + "'.");
			LogUtils.info(getClass(), getStatus().getMessage());
			short retryAttempt = 0;
			if (this.exportRunSourceType == ExportRunSourceTypes.RETRY) {
				//Retrieve last run history to get the current retry count
				ExportRunHistory latestHistory = getExportDefinitionService().getExportDefinitionRunLastHistory(this.exportDefinitionId);

				if (latestHistory != null && latestHistory.getRetryAttemptNumber() != null) {
					retryAttempt = (short) (latestHistory.getRetryAttemptNumber() + 1);
				}
			}

			boolean success = doRun(exportDefinition, retryAttempt);

			//If the export failed and this was not a Manual run, then reschedule the export if the retry count has not been reached.
			if (!success && this.exportRunSourceType != ExportRunSourceTypes.MANUAL && exportDefinition.getRetryCount() != null && retryAttempt < exportDefinition.getRetryCount()) {
				rescheduleExport(exportDefinition);
			}
		}
		finally {
			// restore the current user
			if (currentUser == null) {
				getContextHandler().removeBean(Context.USER_BEAN_NAME);
			}
			else {
				getContextHandler().setBean(Context.USER_BEAN_NAME, currentUser);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void rescheduleExport(ExportDefinition exportDefinition) {
		ExportRunStatus statusWaiting = getExportRunService().getExportRunStatusByName(ExportRunStatusNames.WAITING_FOR_RETRY);
		exportDefinition = getExportDefinitionService().updateExportDefinitionStatus(this.exportDefinitionId, statusWaiting);

		Date runOnDate = DateUtils.addSeconds(new Date(), exportDefinition.getRetryDelayInSeconds());
		getStatus().setMessage("Rescheduling Export Definition '" + exportDefinition.getLabel() + "' to on ActiveMQ queue on '" + DateUtils.fromDate(runOnDate, DateUtils.DATE_FORMAT_SHORT) + "'.");
		LogUtils.info(getClass(), getStatus().getMessage());
		//schedule for running after the wait period
		getExportRunnerService().rescheduleExport(this.exportDefinitionId, runOnDate);
	}


	private boolean doRun(ExportDefinition exportDefinition, short retryAttempt) {
		ExportRunResult result = null;
		ExportRunHistory exportRunHistory = createAndInitializeExportRunHistory(exportDefinition, retryAttempt);
		try {
			getExportDefinitionService().validateDefinitionSendable(exportDefinition);
			initExportRun(exportDefinition);

			List<ExportDefinitionContent> contents = exportDefinition.getContentList();

			verifyContentReady(contents);
			verifyActiveDestinationsAndContents(exportDefinition);
			result = doRunAttempt(exportDefinition, exportRunHistory);
		}
		catch (ExportContentNotReadyException e) {
			result = handleRunError(e, exportDefinition, ExportRunStatusNames.FAILED_NOT_READY);
		}
		catch (Exception e) {
			result = handleRunError(e, exportDefinition, ExportRunStatusNames.FAILED_ERROR);
		}
		finally {
			ExportRunStatusNames runStatus = result != null ? result.getCompletedStatus() : ExportRunStatusNames.FAILED_ERROR;
			ExportRunStatus finalStatus = getExportRunService().getExportRunStatusByName(runStatus);
			String completedMessage = result != null ? result.getCompletedMessage() : runStatus.name();

			//At this point it is safe to bypass the Modify Condition in order to update the status on the definition
			exportDefinition = getExportDefinitionService().updateExportDefinitionStatus(this.exportDefinitionId, finalStatus);

			exportRunHistory.setStatus(finalStatus);
			exportRunHistory.setEndDate(new Date());
			exportRunHistory.setDescription(formatCompletedMessage(completedMessage, retryAttempt, finalStatus.getExportRunStatusName()));
			getExportRunService().saveExportRunHistory(exportRunHistory);
		}

		return result != null && !result.getCompletedStatus().isFailed();
	}


	private ExportRunResult handleRunError(Exception e, ExportDefinition exportDefinition, ExportRunStatusNames completedStatus) {
		LogUtils.info(getClass(), "Error running Export " + exportDefinition.getLabel(), e);
		String completedMessage = buildErrorMessage(e);
		return new ExportRunResult(completedStatus, completedMessage);
	}


	private ExportRunResult doRunAttempt(ExportDefinition exportDefinition, ExportRunHistory runHistory) {
		List<Map<String, Object>> configurationList = new ArrayList<>();
		if (exportDefinition.getDynamicConfigurationSystemBean() != null) {
			ExportDefinitionDynamicConfigurationGenerator configurationGenerator = (ExportDefinitionDynamicConfigurationGenerator) getSystemBeanService().getBeanInstance(exportDefinition.getDynamicConfigurationSystemBean());
			String configurationExceptionResultString = "Dynamic Configuration Generator did not produce a valid configuration. Aborting Run Attempt";
			try {
				configurationList = configurationGenerator.getConfigurationList(exportDefinition);
			}
			catch (ValidationException ve) {
				configurationExceptionResultString = ve.getMessage();
			}

			if (CollectionUtils.isEmpty(configurationList)) {
				return new ExportRunResult(exportDefinition.isDataExpected() ? ExportRunStatusNames.FAILED_NO_DATA : ExportRunStatusNames.COMPLETED_NO_DATA, configurationExceptionResultString);
			}
		}
		ExportRunResult runResult;
		if (CollectionUtils.isEmpty(configurationList)) {
			runResult = doRunAttemptForConfiguration(exportDefinition, runHistory, new HashMap<>());
		}
		else {
			ExportRunStatusNames status = ExportRunStatusNames.NONE;
			StringBuilder completedMessages = new StringBuilder();

			for (Map<String, Object> configuration : configurationList) {
				ExportRunResult configRunResult = doRunAttemptForConfiguration(exportDefinition, runHistory, configuration);
				//If the status is still NONE it has not been set yet
				if (status == ExportRunStatusNames.NONE && configRunResult.getCompletedStatus().isFailed()) {
					status = ExportRunStatusNames.FAILED_ERROR;
				}
				completedMessages.append("\n").append(configRunResult.getCompletedMessage());
			}
			//If we get all the way through and the status is still the same, then we have not had a failure
			if (status == ExportRunStatusNames.NONE) {
				status = ExportRunStatusNames.COMPLETED;
			}

			runResult = new ExportRunResult(status, completedMessages.toString());
		}
		return runResult;
	}


	private ExportRunResult doRunAttemptForConfiguration(ExportDefinition exportDefinition, ExportRunHistory runHistory, Map<String, Object> configurationMap) {
		//Process the export contents and generate the files to send
		ExportFileGenerationResult fileResults = this.exportFileGenerationHandler.generateExportFileList(exportDefinition, runHistory, this.rootDirectory, configurationMap);
		List<ExportFile> filesToSend = fileResults.getExportFiles();

		ExportRunStatusNames newStatus;
		if (!CollectionUtils.isEmpty(filesToSend)) {
			//Send the message to the queue
			boolean messageSent = prepareAndSendExportMessage(runHistory.getId(), exportDefinition, filesToSend, configurationMap);

			if (getExportRunService().getExportRunHistory(runHistory.getId()).getStatus().getExportRunStatusName() == ExportRunStatusNames.COMPLETED) {
				newStatus = ExportRunStatusNames.COMPLETED;
			}
			else if (messageSent) {
				newStatus = ExportRunStatusNames.SENDING;
			}
			else {
				newStatus = ExportRunStatusNames.COMPLETED_NO_DATA;
			}
		}
		else {
			if (exportDefinition.isDataExpected()) {
				newStatus = ExportRunStatusNames.FAILED_NO_DATA;
			}
			else {
				newStatus = ExportRunStatusNames.COMPLETED_NO_DATA;
			}
		}

		LogUtils.info(getClass(), getStatus().getMessage());
		String completedMessage = buildExportCompletionDescription(exportDefinition, fileResults);
		return new ExportRunResult(newStatus, completedMessage);
	}


	private ExportRunHistory createAndInitializeExportRunHistory(ExportDefinition exportDefinition, short retryAttempt) {
		ExportRunStatus newStatus = getExportRunService().getExportRunStatusByName(ExportRunStatusNames.RUNNING);

		ExportRunHistory exportRunHistory = new ExportRunHistory();
		exportRunHistory.setDefinition(exportDefinition);
		exportRunHistory.setExportRunSourceType(this.exportRunSourceType);
		exportRunHistory.setStatus(newStatus);
		exportRunHistory.setScheduledDate(getRunDate());
		exportRunHistory.setStartDate(new Date());
		exportRunHistory.setDescription(newStatus.getLabel());
		exportRunHistory.setRetryAttemptNumber(retryAttempt);
		exportRunHistory = getExportRunService().saveExportRunHistory(exportRunHistory);

		this.runContext.put("exportRunHistory-" + exportDefinition.getId(), exportRunHistory);

		return exportRunHistory;
	}


	private void initExportRun(ExportDefinition exportDefinition) {
		getExportDefinitionService().updateExportDefinitionStatus(this.exportDefinitionId, getExportRunService().getExportRunStatusByName(ExportRunStatusNames.RUNNING));
		this.runContext.put("currentExportDefinitionId", exportDefinition.getId());
	}


	private String formatCompletedMessage(String message, short retryAttempt, ExportRunStatusNames status) {
		if (message == null) {
			message = status.toString();
		}

		if (retryAttempt > 0) {
			message = "Retry Attempt " + retryAttempt + ":" + System.lineSeparator() + message;
		}

		return StringUtils.formatStringUpToNCharsWithDots(message, DataTypes.DESCRIPTION_LONG.getLength(), true);
	}


	private String buildErrorMessage(Throwable e) {
		StringBuilder errorMsgBuilder = new StringBuilder("Error occurred while processing export: '" + e.getMessage() + "'");

		//If this isn't a simple ValidationException or ExportContentNotReadyException, add more info to the message, including the stack trace.
		if (!(e instanceof ValidationException) && !(e instanceof ExportContentNotReadyException)) {
			Throwable rootCause = e;
			while (rootCause.getCause() != null && rootCause.getCause() != rootCause) {
				rootCause = rootCause.getCause();
				String message = rootCause.getMessage();
				if (!StringUtils.isEmpty(message)) {
					errorMsgBuilder.append(System.lineSeparator());
					errorMsgBuilder.append("CAUSED BY: ");
					errorMsgBuilder.append(message);
				}
			}
			errorMsgBuilder.append(System.lineSeparator());
			errorMsgBuilder.append("Stack Trace:");
			errorMsgBuilder.append(System.lineSeparator());
			errorMsgBuilder.append(ExceptionUtils.getFullStackTrace(e));
		}

		errorMsgBuilder.append(System.lineSeparator());
		return errorMsgBuilder.toString();
	}


	private void verifyContentReady(List<ExportDefinitionContent> contentList) throws ExportContentNotReadyException {
		for (ExportDefinitionContent content : CollectionUtils.getIterable(contentList)) {
			if (content.getContentReadySystemCondition() != null) {
				EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(content.getContentReadySystemCondition(), content);
				if (!result.isResult()) {
					throw new ExportContentNotReadyException(result.getMessage());
				}
			}
		}
	}


	private void verifyActiveDestinationsAndContents(ExportDefinition exportDefinition) {
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(exportDefinition.getDestinationList()), "There are no active Destinations for Export Definition: [" + exportDefinition.getLabel() + "]");

		ValidationUtils.assertTrue(containsActiveContents(exportDefinition.getContentList()),
				"There are no active Contents for Export Definition: [" + exportDefinition.getLabel() + "]");
	}


	private String buildExportCompletionDescription(ExportDefinition exportDefinition, ExportFileGenerationResult fileResults) {
		StringBuilder completedMessageBuilder = new StringBuilder();

		if (!CollectionUtils.isEmpty(fileResults.getExportFiles())) {
			int numFiles = fileResults.getContentsThatGeneratedFiles().size();
			if (exportDefinition.isZipFilesForExport()) {
				completedMessageBuilder.append("A zipped file was generated containing ").append(numFiles).append(" files.");
			}
			else {
				completedMessageBuilder.append(numFiles).append(" files were generated.");
			}
		}

		completedMessageBuilder.append(System.lineSeparator());

		List<ExportDefinitionContent> contentsWithoutFiles = fileResults.getContentsThatDidNotGenerateFiles();
		if (!CollectionUtils.isEmpty(contentsWithoutFiles)) {
			completedMessageBuilder.append("The following Content definitions did not generate any files, possibly because there was no data to send: ");
			completedMessageBuilder.append(System.lineSeparator());
			for (ExportDefinitionContent content : CollectionUtils.getIterable(contentsWithoutFiles)) {
				completedMessageBuilder.append("- [");
				completedMessageBuilder.append(content.getContentSystemBean().getNameWithoutAutoGeneratedPrefix());
				completedMessageBuilder.append(']');
				completedMessageBuilder.append(System.lineSeparator());
			}
		}

		return completedMessageBuilder.toString();
	}


	private boolean prepareAndSendExportMessage(Integer exportRunHistoryId, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		// put the stuff on queue
		ExportMessagingMessage messagingMessage = getExportMessagingMessageConverterService().toExportMessagingMessage(exportRunHistoryId, exportDefinition, files, configurationMap);
		//Do not send a message via JMS if there are no destinations
		if (!CollectionUtils.isEmpty(messagingMessage.getDestinationList())) {
			getExportClientAsynchronousMessageHandler().send(messagingMessage);
			getStatus().setMessage("Sent Export Definition '" + exportDefinition.getLabel() + "' to on ActiveMQ queue on '"
					+ (getRunDate() == null ? "" : DateUtils.fromDate(getRunDate(), DateUtils.DATE_FORMAT_SHORT)) + "'.");
			return true;
		}
		return false;
	}


	private boolean containsActiveContents(List<ExportDefinitionContent> contents) {
		for (ExportDefinitionContent content : CollectionUtils.getIterable(contents)) {
			if (!content.isDisabled()) {
				return true;
			}
		}

		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private static class ExportRunResult {

		private final ExportRunStatusNames completedStatus;
		private final String completedMessage;


		public ExportRunResult(ExportRunStatusNames completedStatus, String completedMessage) {
			this.completedStatus = completedStatus;
			this.completedMessage = completedMessage;
		}


		public ExportRunStatusNames getCompletedStatus() {
			return this.completedStatus;
		}


		public String getCompletedMessage() {
			return this.completedMessage;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public Map<String, Object> getRunContext() {
		return this.runContext;
	}


	public ExportRunnerFactory getExportRunnerFactory() {
		return this.exportRunnerFactory;
	}


	public void setExportRunnerFactory(ExportRunnerFactory exportRunnerFactory) {
		this.exportRunnerFactory = exportRunnerFactory;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	/**
	 * @return location where files are going to be located
	 */
	public String getRootDirectory() {
		return this.rootDirectory;
	}


	/**
	 * @param rootDirectory where files are going to be located
	 */
	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public AsynchronousMessageHandler getExportClientAsynchronousMessageHandler() {
		return this.exportClientAsynchronousMessageHandler;
	}


	public void setExportClientAsynchronousMessageHandler(AsynchronousMessageHandler exportClientAsynchronousMessageHandler) {
		this.exportClientAsynchronousMessageHandler = exportClientAsynchronousMessageHandler;
	}


	public ExportMessagingMessageConverterService getExportMessagingMessageConverterService() {
		return this.exportMessagingMessageConverterService;
	}


	public void setExportMessagingMessageConverterService(ExportMessagingMessageConverterService exportMessagingMessageConverterService) {
		this.exportMessagingMessageConverterService = exportMessagingMessageConverterService;
	}


	public ExportRunnerService getExportRunnerService() {
		return this.exportRunnerService;
	}


	public void setExportRunnerService(ExportRunnerService exportRunnerService) {
		this.exportRunnerService = exportRunnerService;
	}


	public ExportFileGenerationHandler getExportFileGenerationHandler() {
		return this.exportFileGenerationHandler;
	}


	public void setExportFileGenerationHandler(ExportFileGenerationHandler exportFileGenerationHandler) {
		this.exportFileGenerationHandler = exportFileGenerationHandler;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}
}
