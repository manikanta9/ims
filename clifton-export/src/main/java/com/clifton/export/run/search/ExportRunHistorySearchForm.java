package com.clifton.export.run.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithTimeSearchForm;
import com.clifton.export.run.ExportRunSourceTypes;

import java.util.Date;


public class ExportRunHistorySearchForm extends BaseAuditableEntityDateRangeWithTimeSearchForm {

	@SearchField(searchField = "definition.id", sortField = "definition.name")
	private Integer definitionId;

	@SearchField(searchField = "definition.name")
	private String exportDefinitionName;

	@SearchField(searchField = "type.id", searchFieldPath = "definition")
	private Short exportDefinitionTypeId;

	@SearchField
	private ExportRunSourceTypes exportRunSourceType;

	@SearchField(searchField = "exportRunSourceType", comparisonConditions = ComparisonConditions.IN)
	private ExportRunSourceTypes[] exportRunSourceTypeList;

	@SearchField(searchField = "status.id", sortField = "status.name")
	private Short statusId;

	@SearchField(searchField = "status.name")
	private String statusName;

	@SearchField(searchField = "status.name", comparisonConditions = ComparisonConditions.IN)
	private String[] statusNameList;

	@SearchField(searchField = "ownerUser.id", searchFieldPath = "definition")
	Short ownerUserId;

	@SearchField(searchField = "ownerUser.userName", searchFieldPath = "definition")
	String ownerUserName;

	@SearchField(searchField = "ownerGroup.id", searchFieldPath = "definition")
	Short ownerGroupId;

	@SearchField(searchField = "ownerGroup.name", searchFieldPath = "definition")
	String ownerGroupName;

	@SearchField(searchField = "definition.ownerUser.displayName,definition.ownerUser.userName,definition.ownerGroup.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String ownerUserOrGroupName;

	@SearchField
	private Date scheduledDate;

	@SearchField
	private String description;

	@SearchField(searchFieldPath = "definition")
	private Integer maxExecutionTimeMillis;

	/**
	 * Customized search field that returns run history entries with ExportDefinitions that have an ownerUserId that matches the ownedByUserId or a ownerGroupId that falls within the security groups assigned
	 * the user.
	 */
	private Short ownedByUserId;


	@SearchField(searchField = "priority.id", searchFieldPath = "definition")
	private Short priorityId;

	@SearchField(searchFieldPath = "definition")
	private Boolean failureMonitored;

	@SearchField(searchFieldPath = "definition")
	private Boolean dataExpected;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public String getExportDefinitionName() {
		return this.exportDefinitionName;
	}


	public void setExportDefinitionName(String exportDefinitionName) {
		this.exportDefinitionName = exportDefinitionName;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public String getStatusName() {
		return this.statusName;
	}


	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}


	public String[] getStatusNameList() {
		return this.statusNameList;
	}


	public void setStatusNameList(String[] statusNameList) {
		this.statusNameList = statusNameList;
	}


	public Short getOwnerUserId() {
		return this.ownerUserId;
	}


	public void setOwnerUserId(Short ownerUserId) {
		this.ownerUserId = ownerUserId;
	}


	public String getOwnerUserName() {
		return this.ownerUserName;
	}


	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}


	public Short getOwnerGroupId() {
		return this.ownerGroupId;
	}


	public void setOwnerGroupId(Short ownerGroupId) {
		this.ownerGroupId = ownerGroupId;
	}


	public String getOwnerGroupName() {
		return this.ownerGroupName;
	}


	public void setOwnerGroupName(String ownerGroupName) {
		this.ownerGroupName = ownerGroupName;
	}


	public String getOwnerUserOrGroupName() {
		return this.ownerUserOrGroupName;
	}


	public void setOwnerUserOrGroupName(String ownerUserOrGroupName) {
		this.ownerUserOrGroupName = ownerUserOrGroupName;
	}


	public ExportRunSourceTypes getExportRunSourceType() {
		return this.exportRunSourceType;
	}


	public void setExportRunSourceType(ExportRunSourceTypes exportRunSourceType) {
		this.exportRunSourceType = exportRunSourceType;
	}


	public ExportRunSourceTypes[] getExportRunSourceTypeList() {
		return this.exportRunSourceTypeList;
	}


	public void setExportRunSourceTypeList(ExportRunSourceTypes[] exportRunSourceTypeList) {
		this.exportRunSourceTypeList = exportRunSourceTypeList;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getExportDefinitionTypeId() {
		return this.exportDefinitionTypeId;
	}


	public void setExportDefinitionTypeId(Short exportDefinitionTypeId) {
		this.exportDefinitionTypeId = exportDefinitionTypeId;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}


	public Short getOwnedByUserId() {
		return this.ownedByUserId;
	}


	public void setOwnedByUserId(Short ownedByUserId) {
		this.ownedByUserId = ownedByUserId;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Boolean getFailureMonitored() {
		return this.failureMonitored;
	}


	public void setFailureMonitored(Boolean failureMonitored) {
		this.failureMonitored = failureMonitored;
	}


	public Boolean getDataExpected() {
		return this.dataExpected;
	}


	public void setDataExpected(Boolean dataExpected) {
		this.dataExpected = dataExpected;
	}
}
