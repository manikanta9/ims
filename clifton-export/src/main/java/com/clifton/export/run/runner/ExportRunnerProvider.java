package com.clifton.export.run.runner;


import com.clifton.calendar.schedule.runner.AbstractScheduleAwareRunnerProvider;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Runner;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.search.ExportDefinitionSearchForm;
import com.clifton.export.run.ExportRunSourceTypes;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * The <code>ExportRunnerProvider</code> class returns ExportDefinition Runner instances that are scheduled for the specified time period.
 */
@Component
public class ExportRunnerProvider extends AbstractScheduleAwareRunnerProvider<ExportDefinition> {

	private ExportDefinitionService exportDefinitionService;
	private ExportRunnerFactory exportRunnerFactory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional(readOnly = true)
	public List<Runner> getOccurrencesBetween(Date startDateTime, Date endDateTime) {
		long startMillis = System.currentTimeMillis();
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Starting getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE)));
		List<Runner> results = new ArrayList<>();

		ExportDefinitionSearchForm searchForm = new ExportDefinitionSearchForm();
		searchForm.setActiveOnDate(startDateTime);
		List<ExportDefinition> activeDefinitions = getExportDefinitionService().getExportDefinitionList(searchForm);
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Active Export Definition list compiled at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		for (ExportDefinition definition : CollectionUtils.getIterable(activeDefinitions)) {
			List<Runner> newDefinitionRunners = getOccurrencesBetweenForEntity(definition, startDateTime, endDateTime);

			if (!CollectionUtils.isEmpty(newDefinitionRunners)) {
				results.addAll(newDefinitionRunners);
			}
		}
		LogUtils.trace(LogCommand.ofMessage(getClass(), "Ending getOccurrencesBetween for " + getClass() + " at " + DateUtils.fromDate(new Date(), DateUtils.DATE_FORMAT_FILE_PRECISE) + " Elapsed Milliseconds: " + (System.currentTimeMillis() - startMillis)));
		return results;
	}


	@Override
	public Runner createRunnerForEntityAndDate(ExportDefinition entity, Date runnerDate) {
		return getExportRunnerFactory().createExportRunner(runnerDate, entity.getId(), ExportRunSourceTypes.SCHEDULE);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	public ExportRunnerFactory getExportRunnerFactory() {
		return this.exportRunnerFactory;
	}


	public void setExportRunnerFactory(ExportRunnerFactory exportRunnerFactory) {
		this.exportRunnerFactory = exportRunnerFactory;
	}
}
