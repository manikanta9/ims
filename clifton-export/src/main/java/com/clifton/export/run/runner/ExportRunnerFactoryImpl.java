package com.clifton.export.run.runner;


import com.clifton.core.context.ApplicationContextService;
import com.clifton.export.run.ExportRunSourceTypes;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.stereotype.Component;

import java.util.Date;


@Component
public class ExportRunnerFactoryImpl implements ExportRunnerFactory {

	/**
	 * This user will be used in case runAsUser is not set on the export job.
	 */
	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	/**
	 * where the secured FTP files are going to be located
	 */
	private String rootDirectory;


	private ApplicationContextService applicationContextService;
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportRunner createExportRunner(Date runDate, int jobId, ExportRunSourceTypes exportRunSourceType) {
		return createExportRunner(runDate, jobId, exportRunSourceType, null);
	}


	@Override
	public ExportRunner createExportRunner(Date runDate, int jobId, ExportRunSourceTypes exportRunSourceType, SecurityUser runAsUser) {
		if (runAsUser == null) {
			runAsUser = getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
		}

		ExportRunner runner = new ExportRunner(runDate, jobId, exportRunSourceType, runAsUser);
		getApplicationContextService().autowireBean(runner);
		runner.setRootDirectory(getRootDirectory());

		return runner;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	@Override
	public String getRootDirectory() {
		return this.rootDirectory;
	}


	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}
}
