package com.clifton.export.run;


import com.clifton.core.beans.BaseEntity;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;

import java.util.Date;


/**
 * The <code>ExportRunHistory</code> class is a history of a single completed export run with corresponding details.
 *
 * @author vgomelsky
 */
public class ExportRunHistory extends BaseEntity<Integer> {

	/**
	 * Either the definition or destination must be populated.
	 */
	private ExportDefinition definition;

	/**
	 * Destination MUST be a shared destination
	 */
	private ExportDefinitionDestination destination;
	private ExportRunStatus status;
	private ExportRunSourceTypes exportRunSourceType;
	private Date scheduledDate;
	private Date startDate;
	private Date endDate;

	/**
	 * If the content is not ready (contentReadySystemCondition returns false) and export definition
	 * is configured to retry, this field will have the number of retry attempt: 1, 2, 3, etc.
	 */
	private Short retryAttemptNumber;
	/**
	 * Contains error message for failed runs or run specific information.
	 */
	private String description;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String toString() {
		StringBuilder result = new StringBuilder(50);
		result.append("{id=");
		result.append(getId());
		result.append(", ");
		result.append(getStatus());
		result.append(", ");
		result.append(getDescription());
		result.append('}');
		return result.toString();
	}


	public String getExecutionTimeFormatted() {
		if (getStartDate() == null) {
			return null;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getTimeDifferenceShort(toDate, getStartDate());
	}


	public long getExecutionTimeInMillis() {
		if (getStartDate() == null) {
			return 0;
		}
		// If still in this state, then give time in state up to now
		Date toDate = getEndDate();
		if (toDate == null) {
			toDate = new Date();
		}
		return DateUtils.getMillisecondsDifference(toDate, getStartDate());
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinition getDefinition() {
		return this.definition;
	}


	public void setDefinition(ExportDefinition definition) {
		this.definition = definition;
	}


	public ExportDefinitionDestination getDestination() {
		return this.destination;
	}


	public void setDestination(ExportDefinitionDestination destination) {
		this.destination = destination;
	}


	public ExportRunStatus getStatus() {
		return this.status;
	}


	public void setStatus(ExportRunStatus status) {
		this.status = status;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public Date getStartDate() {
		return this.startDate;
	}


	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}


	public Date getEndDate() {
		return this.endDate;
	}


	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public ExportRunSourceTypes getExportRunSourceType() {
		return this.exportRunSourceType;
	}


	public void setExportRunSourceType(ExportRunSourceTypes exportRunSourceType) {
		this.exportRunSourceType = exportRunSourceType;
	}


	public Short getRetryAttemptNumber() {
		return this.retryAttemptNumber;
	}


	public void setRetryAttemptNumber(Short retryAttemptNumber) {
		this.retryAttemptNumber = retryAttemptNumber;
	}
}
