package com.clifton.export.run;


import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.run.runner.ExportRunnerService;

import java.util.Map;


/**
 * The <code>ExportRunJob</code> runs the specified {@link ExportDefinition}.
 *
 * @author jgommels
 */
public class ExportRunJob implements Task {

	private Integer exportDefinitionId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ExportRunnerService exportRunnerService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(@SuppressWarnings("unused") Map<String, Object> context) {
		return getExportRunnerService().runExport(getExportDefinitionId(), ExportRunSourceTypes.BATCH_JOB);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Getters and Setters                           //////
	////////////////////////////////////////////////////////////////////////////


	public Integer getExportDefinitionId() {
		return this.exportDefinitionId;
	}


	public void setExportDefinitionId(Integer exportDefinitionId) {
		this.exportDefinitionId = exportDefinitionId;
	}


	public ExportRunnerService getExportRunnerService() {
		return this.exportRunnerService;
	}


	public void setExportRunnerService(ExportRunnerService exportRunnerService) {
		this.exportRunnerService = exportRunnerService;
	}
}
