package com.clifton.export.run;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.dao.event.cache.DaoNamedEntityCache;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchRestriction;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.search.ExportDefinitionSearchForm;
import com.clifton.export.run.search.ExportRunHistorySearchForm;
import com.clifton.export.run.search.ExportRunHistorySearchFormConfigurer;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
public class ExportRunServiceImpl implements ExportRunService {

	private ExportDefinitionService exportDefinitionService;
	private SecurityAuthorizationService securityAuthorizationService;

	private AdvancedUpdatableDAO<ExportRunStatus, Criteria> exportRunStatusDAO;
	private AdvancedUpdatableDAO<ExportRunHistory, Criteria> exportRunHistoryDAO;

	private DaoNamedEntityCache<ExportRunStatus> exportRunStatusCache;

	private static final int RUN_AGE_MINUTES = 30;

	////////////////////////////////////////////////////////////////////////////
	///////             Export Run Status Business Methods             /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportRunStatus getExportRunStatusByName(ExportRunStatus.ExportRunStatusNames name) {
		if (name == null) {
			throw new ValidationException("getExportRunStatusByName requires ExportRunStatusNames name parameter");
		}
		return getExportRunStatusCache().getBeanForKeyValueStrict(getExportRunStatusDAO(), name.name());
	}


	@Override
	public List<ExportRunStatus> getExportRunStatusList() {
		return getExportRunStatusDAO().findAll();
	}


	@Override
	public List<ExportRunStatus> getExportRunStatusList(ExportDefinitionSearchForm searchForm) {
		return getExportRunStatusDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Export Run History Business Methods        /////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportRunHistory getExportRunHistory(int id) {
		return getExportRunHistoryDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ExportRunHistory> getExportRunHistoryList(ExportRunHistorySearchForm searchForm) {
		return getExportRunHistoryDAO().findBySearchCriteria(new ExportRunHistorySearchFormConfigurer(searchForm));
	}


	@Override
	public ExportRunHistory getExportRunHistoryLatestSuccessful(int exportDefinitionId) {
		ExportRunHistorySearchForm searchForm = new ExportRunHistorySearchForm();
		searchForm.setDefinitionId(exportDefinitionId);
		searchForm.setStatusNameList(ExportRunStatus.ExportRunStatusNames.getExportRunStatusNameListSuccessful().stream().map(ExportRunStatus.ExportRunStatusNames::name).toArray(String[]::new));
		searchForm.setOrderBy("endDate:desc");
		searchForm.setLimit(1);
		return CollectionUtils.getOnlyElement(getExportRunHistoryList(searchForm));
	}


	@Override
	public List<ExportRunHistory> getExportRunHistoryListByDate(int definitionId, Date scheduledDate) {
		scheduledDate = DateUtils.clearTime(scheduledDate);
		ExportRunHistorySearchForm searchForm = new ExportRunHistorySearchForm();
		searchForm.setDefinitionId(definitionId);

		searchForm.addSearchRestriction(new SearchRestriction("scheduledDate", ComparisonConditions.GREATER_THAN_OR_EQUALS, scheduledDate));
		searchForm.addSearchRestriction(new SearchRestriction("scheduledDate", ComparisonConditions.LESS_THAN, DateUtils.addDays(scheduledDate, 1)));

		return getExportRunHistoryList(searchForm);
	}


	@Override
	public ExportRunHistory saveExportRunHistory(ExportRunHistory bean) {
		ValidationUtils.assertFalse(bean.getDefinition() != null && bean.getDestination() != null, "Export Run History can have a Definition or Destination, but not both simultaneously.");
		if (bean.getDestination() != null) {
			ValidationUtils.assertTrue(bean.getDestination().isSharedDestination(), "Export Run History can only be linked to Shared Destinations. Current destination is not shared.");
		}
		else if (bean.getDefinition() == null) {
			throw new ValidationException("Export Run History does not have a Definition or Destination specified.");
		}
		return getExportRunHistoryDAO().save(bean);
	}


	@Override
	public void deleteExportRunHistoryForDefinition(int definitionId) {
		ExportRunHistorySearchForm historySearchForm = new ExportRunHistorySearchForm();
		historySearchForm.setDefinitionId(definitionId);

		List<ExportRunHistory> historyList = getExportRunHistoryList(historySearchForm);
		ValidationUtils.assertTrue(historyList.size() < 100, "Cannot delete export definitions with more than 100 history entries.");

		getExportRunHistoryDAO().deleteList(historyList);
	}


	@Override
	@Transactional
	public void fixExportRunHistory(Integer[] runHistoryIds) {
		boolean isAdmin = getSecurityAuthorizationService().isSecurityUserAdmin();
		Throwable firstCause = null;
		for (Integer runHistoryId : runHistoryIds) {
			try {
				ExportRunHistory runHistory = getExportRunHistory(runHistoryId);
				ValidationUtils.assertNotNull(runHistory, "Cannot find integration run with id = " + runHistoryId);
				if (!isAdmin) {
					long minutes = DateUtils.getMinutesDifference(new Date(), runHistory.getStartDate());
					ValidationUtils.assertTrue(minutes >= RUN_AGE_MINUTES, "Run History [" + runHistoryId + "] is [" + minutes + "] minutes old and it cannot be fixed until it is at least [" + RUN_AGE_MINUTES
							+ "] minutes old.");
				}
				ValidationUtils.assertTrue(ExportRunStatus.ExportRunStatusNames.RUNNING == runHistory.getStatus().getExportRunStatusName() || ExportRunStatus.ExportRunStatusNames.SENDING == runHistory.getStatus().getExportRunStatusName(), "Cannot fix run histories in statuses other than RUNNING or SENDING!");

				runHistory.setStatus(getExportRunStatusByName(ExportRunStatus.ExportRunStatusNames.FAILED_ERROR));
				runHistory.setDescription("This run history was determined to be stuck in RUNNING status.  Status manually changed from RUNNING to FAILED_ERROR.");
				runHistory.setEndDate(new Date());
				runHistory = saveExportRunHistory(runHistory);
				getExportDefinitionService().updateExportDefinitionStatus(runHistory.getDefinition().getId(), runHistory.getStatus());
			}
			catch (ValidationException e) {
				if (firstCause == null) {
					firstCause = e;
				}
			}
		}

		if (firstCause != null) {
			throw new ValidationException("Error fixing one or more Export Run Histories:  " + firstCause.getMessage());
		}
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	public AdvancedUpdatableDAO<ExportRunStatus, Criteria> getExportRunStatusDAO() {
		return this.exportRunStatusDAO;
	}


	public void setExportRunStatusDAO(AdvancedUpdatableDAO<ExportRunStatus, Criteria> exportRunStatusDAO) {
		this.exportRunStatusDAO = exportRunStatusDAO;
	}


	public AdvancedUpdatableDAO<ExportRunHistory, Criteria> getExportRunHistoryDAO() {
		return this.exportRunHistoryDAO;
	}


	public void setExportRunHistoryDAO(AdvancedUpdatableDAO<ExportRunHistory, Criteria> exportRunHistoryDAO) {
		this.exportRunHistoryDAO = exportRunHistoryDAO;
	}


	public DaoNamedEntityCache<ExportRunStatus> getExportRunStatusCache() {
		return this.exportRunStatusCache;
	}


	public void setExportRunStatusCache(DaoNamedEntityCache<ExportRunStatus> exportRunStatusCache) {
		this.exportRunStatusCache = exportRunStatusCache;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}
}
