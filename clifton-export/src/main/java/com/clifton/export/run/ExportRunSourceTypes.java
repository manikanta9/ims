package com.clifton.export.run;


import com.clifton.export.definition.ExportDefinition;


/**
 *
 */
public enum ExportRunSourceTypes {
	/**
	 * Indicates the export run originated from a manual execution.
	 */
	MANUAL,

	/**
	 * Indicates the export run originated from an automated schedule.
	 */
	SCHEDULE,

	/**
	 * Indicates that the export run originated from a batch job.
	 */
	BATCH_JOB,

	/**
	 * Indicates the export run originated from an automated retry. Retries (if configured on the {@link ExportDefinition}) are triggered
	 * when a run that originated from a schedule fails.
	 */
	RETRY
}
