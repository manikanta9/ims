package com.clifton.export.run;

import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.export.definition.search.ExportDefinitionSearchForm;
import com.clifton.export.run.search.ExportRunHistorySearchForm;

import java.util.Date;
import java.util.List;


/**
 * The <code>ExportRunService</code> interface defines methods for working with ExportRunHistory
 * and related objects.
 *
 * @author mwacker
 */
public interface ExportRunService {

	////////////////////////////////////////////////////////////////////////////
	///////          Export Definition Status Business Methods         /////////
	////////////////////////////////////////////////////////////////////////////


	public ExportRunStatus getExportRunStatusByName(ExportRunStatus.ExportRunStatusNames name);


	public List<ExportRunStatus> getExportRunStatusList();


	public List<ExportRunStatus> getExportRunStatusList(ExportDefinitionSearchForm searchForm);

	////////////////////////////////////////////////////////////////////////////
	////////            Export Run History Business Methods        ///////////
	////////////////////////////////////////////////////////////////////////////


	public ExportRunHistory getExportRunHistory(int id);


	public List<ExportRunHistory> getExportRunHistoryListByDate(int definitionId, Date scheduledDate);


	public List<ExportRunHistory> getExportRunHistoryList(ExportRunHistorySearchForm searchForm);


	public ExportRunHistory saveExportRunHistory(ExportRunHistory bean);


	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void deleteExportRunHistoryForDefinition(int definitionId);


	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_WRITE)
	public void fixExportRunHistory(Integer[] runHistoryIds);


	/**
	 * Retrieves the most recent successful ExportDefinition run
	 */
	public ExportRunHistory getExportRunHistoryLatestSuccessful(int exportDefinitionId);
}
