package com.clifton.export.run.runner;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.definition.ExportDefinitionDestinationExportDefinition;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.definition.dynamic.ExportDefinitionDynamicConfigurationGenerator;
import com.clifton.export.definition.search.ExportDefinitionContentSearchForm;
import com.clifton.export.definition.search.ExportDefinitionDestinationExportDefinitionSearchForm;
import com.clifton.export.definition.search.ExportDefinitionSearchForm;
import com.clifton.export.file.ExportFileGenerationHandler;
import com.clifton.export.messaging.ExportMessagingMessageConverterService;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunSourceTypes;
import com.clifton.export.run.ExportRunStatus;
import com.clifton.export.run.ExportRunStatus.ExportRunStatusNames;
import com.clifton.export.run.search.ExportRunHistorySearchForm;
import com.clifton.security.user.SecurityUser;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;


@Service
public class ExportRunnerServiceImpl implements ExportRunnerService {

	private ExportDefinitionService exportDefinitionService;
	private ExportRunService exportRunService;
	private ExportFileGenerationHandler exportFileGenerationHandler;
	private ExportRunnerFactory exportRunnerFactory;
	private ExportMessagingMessageConverterService exportMessagingMessageConverterService;
	private AsynchronousMessageHandler exportClientAsynchronousMessageHandler;
	private SystemBeanService systemBeanService;

	private RunnerHandler runnerHandler;
	private ContextHandler contextHandler;


	private static final String MANUAL_EXPORT_DEFINITION = "Manual";

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	@Override
	public Status runManualExport(int id) {
		return runExport(id, ExportRunSourceTypes.MANUAL);
	}


	@Override
	public Status runExport(int id, ExportRunSourceTypes runSource) {
		SecurityUser currentUser = (SecurityUser) getContextHandler().getBean(Context.USER_BEAN_NAME);
		validateExportIsRunnable(id);
		ExportRunner runner = getExportRunnerFactory().createExportRunner(new Date(), id, runSource, currentUser);
		//TODO: handle multiple runners?
		getRunnerHandler().runNow(runner);
		return runner.getStatus();
	}


	@Override
	public void sendCustomExportFileUpload(FileUploadWrapper file, Integer destinationId) {
		FileContainer fileContainer = getFileContainerForUploadFile(file);
		doSendCustomExportFile(fileContainer, destinationId);
	}


	@Override
	public void sendSingleCustomExportFile(FileWrapper fileWrapper, Integer destinationId) {
		FileContainer fileContainer = getFileContainerForFileWrapper(fileWrapper);
		doSendCustomExportFile(fileContainer, destinationId);
	}


	@Override
	public FileWrapper downloadExportPreview(int id, boolean exceptionIfNoResults, Date previewDate) {
		ExportDefinition exportDefinition = getExportDefinitionService().getExportDefinitionPopulated(id);
		List<Map<String, Object>> configurationList = new ArrayList<>();
		if (exportDefinition.getDynamicConfigurationSystemBean() != null) {
			ExportDefinitionDynamicConfigurationGenerator configurationGenerator = (ExportDefinitionDynamicConfigurationGenerator) getSystemBeanService().getBeanInstance(exportDefinition.getDynamicConfigurationSystemBean());
			configurationList = configurationGenerator.getConfigurationList(exportDefinition);
			if (CollectionUtils.isEmpty(configurationList)) {
				throw new ValidationException("Dynamic Configuration Generator did not produce a valid configuration. Aborting Run attempt.");
			}
		}

		List<FileWrapper> files = new ArrayList<>();
		if (CollectionUtils.isEmpty(configurationList)) {
			files.addAll(getExportFileGenerationHandler().generateTempExportFileWrapperList(exportDefinition.getContentList(), null, previewDate, null));
		}
		else {
			for (Map<String, Object> configuration : configurationList) {
				files.addAll(getExportFileGenerationHandler().generateTempExportFileWrapperList(exportDefinition.getContentList(), null, previewDate, configuration));
			}
		}

		FileWrapper fileToReturn;

		if (CollectionUtils.isEmpty(files)) {
			if (exceptionIfNoResults) {
				throw new ValidationException("Export Preview did not generate any files.");
			}

			fileToReturn = null;
		}
		else if (CollectionUtils.getSize(files) == 1) {
			fileToReturn = files.get(0);
		}
		else {
			Date dateForFilename = previewDate != null ? previewDate : new Date();
			StringBuilder fileNameBuilder = new StringBuilder();
			fileNameBuilder.append(exportDefinition.getName().replaceAll(" ", "_"));
			fileNameBuilder.append("_PREVIEW_");
			fileNameBuilder.append(DateUtils.fromDate(dateForFilename, DateUtils.DATE_FORMAT_FILE));
			fileNameBuilder.append(".zip");
			String zippedFileName = fileNameBuilder.toString();
			FileContainer tempFile;
			try {
				tempFile = FileContainerFactory.getFileContainer(File.createTempFile(zippedFileName, null));
			}
			catch (IOException e) {
				throw new RuntimeException("Failed to create a temporary Zip file during export preview", e);
			}

			FilePath zippedFile = FilePath.forPath(ZipUtils.zipFiles(files, tempFile).getPath());
			fileToReturn = new FileWrapper(zippedFile, zippedFileName, true);
		}

		return fileToReturn;
	}


	@Override
	public void rescheduleExport(int id, Date runOnDate) {
		ExportRunner runner = getExportRunnerFactory().createExportRunner(runOnDate, id, ExportRunSourceTypes.RETRY);
		getRunnerHandler().rescheduleRunner(runner);
	}


	@Override
	public List<ExportDefinition> getExportDefinitionListInvalid() {
		List<ExportDefinition> invalidList = null;

		// 1. get all running batch jobs
		ExportDefinitionSearchForm searchForm = new ExportDefinitionSearchForm();
		searchForm.setStatusId(getExportRunService().getExportRunStatusByName(ExportRunStatusNames.RUNNING).getId());
		List<ExportDefinition> runningList = getExportDefinitionService().getExportDefinitionList(searchForm);

		if (CollectionUtils.getSize(runningList) > 0) {
			for (ExportDefinition job : runningList) {
				if (!isExportDefinitionRunning(job)) {
					// SHOULD NEVER HAVE AN EXPORT IN "RUNNING" STATUS THAT IS NOT ACTUALLY RUNNING
					// this could happen if the server is killed while the job is running
					if (invalidList == null) {
						invalidList = new ArrayList<>();
					}
					invalidList.add(job);
				}
			}
		}

		return invalidList;
	}


	@Override
	@Transactional
	public void fixExportDefinition(int id) {
		// validate current export state to make sure we're not fixing a good job
		ExportDefinition def = getExportDefinitionService().getExportDefinition(id);
		ValidationUtils.assertNotNull(def, "Cannot find export definition with id = " + id);
		ValidationUtils.assertTrue(ExportRunStatusNames.RUNNING == def.getStatus().getExportRunStatusName() || ExportRunStatusNames.SENDING == def.getStatus().getExportRunStatusName(), "Cannot fix export definition that is not in RUNNING or SENDING status: " + def.getStatus());
		ValidationUtils.assertFalse(isExportDefinitionRunning(def), "Cannot fix the export with id = \" + id + \" because it maybe currently running.");

		// set job to FAILED and update history
		getExportDefinitionService().updateExportDefinitionStatus(def.getId(), getExportRunService().getExportRunStatusByName(ExportRunStatusNames.FAILED_ERROR));

		// find last history
		ExportRunHistorySearchForm searchForm = new ExportRunHistorySearchForm();
		searchForm.setDefinitionId(id);
		searchForm.setLimit(1);
		searchForm.setOrderBy("id:desc");
		List<ExportRunHistory> historyList = getExportRunService().getExportRunHistoryList(searchForm);
		ExportRunHistory history = CollectionUtils.getFirstElement(historyList);
		if (history != null && ExportRunStatusNames.RUNNING == history.getStatus().getExportRunStatusName()) {
			history.setStatus(getExportRunService().getExportRunStatusByName(ExportRunStatusNames.FAILED_ERROR));
			history.setEndDate(new Date());
			history.setDescription("Fixed export in RUNNING status that was not running (server restart?)");
			getExportRunService().saveExportRunHistory(history);
		}
	}


	///////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////


	private void doSendCustomExportFile(FileContainer fileContainer, Integer destinationId) {

		FileWrapper fileWrapper = new FileWrapper(FilePath.forPath(fileContainer.getPath()), fileContainer.getName(), false);

		ExportDefinitionDestination destination = getExportDefinitionService().getExportDefinitionDestination(destinationId);
		ExportDefinition definition = getExportDefinition(destination);
		definition.setDestinationList(Collections.singletonList(destination));
		getExportDefinitionService().validateDefinitionSendable(definition);

		ExportRunHistory runHistory = createAndInitializeExportRunHistory(definition);
		ExportRunStatusNames runStatus = ExportRunStatusNames.COMPLETED;
		StringBuilder resultBuilder = new StringBuilder();
		try {
			ExportMessagingMessage message = getExportMessagingMessageConverterService().toExportMessagingMessageIncludeDisabledDestinations(
					definition,
					Collections.singletonList(fileWrapper));
			message.setSourceSystemIdentifier(runHistory.getId().toString());
			if (!CollectionUtils.isEmpty(message.getDestinationList())) {
				getExportClientAsynchronousMessageHandler().send(message);
			}
			resultBuilder.append(runStatus);
			resultBuilder.append(" - Manually sent file: ");
			resultBuilder.append(fileWrapper.getFileName());
			resultBuilder.append("\n");
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to export message.", e);

			runStatus = ExportRunStatusNames.FAILED_ERROR;
			resultBuilder.append(runStatus);
			resultBuilder.append("\n");
			resultBuilder.append(e.getMessage());
			resultBuilder.append("\n");
		}
		finally {
			runHistory.setStatus(getExportRunService().getExportRunStatusByName(runStatus));
			runHistory.setEndDate(new Date());
			runHistory.setDescription(resultBuilder.toString());
			getExportRunService().saveExportRunHistory(runHistory);
		}
	}


	/**
	 * Converts a FileUploadWrapper to a FileContainer.
	 */
	private FileContainer getFileContainerForUploadFile(FileUploadWrapper file) {
		FileContainer fileContainer = FileContainerFactory.getFileContainer(FileUtils.combinePath(getExportRunnerFactory().getRootDirectory(), file.getFile().getOriginalFilename()));
		try {
			if (fileContainer.exists()) {
				FileUtils.delete(fileContainer);
			}
			FileUtils.transferFromInputToOutput(file.getFile().getInputStream(), fileContainer.getOutputStream());
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
		return fileContainer;
	}


	/**
	 * Converts a FileWrapper to a FileContainer.
	 */
	private FileContainer getFileContainerForFileWrapper(FileWrapper file) {
		FileContainer fileContainer = FileContainerFactory.getFileContainer(FileUtils.combinePath(getExportRunnerFactory().getRootDirectory(), file.getFileName()));
		try {
			if (fileContainer.exists()) {
				FileUtils.delete(fileContainer);
			}
			FileUtils.transferFromInputToOutput(file.getFile().toFileContainer().getInputStream(), fileContainer.getOutputStream());
			if (file.isTempFile()) {
				FileUtils.delete(file.getFile());
			}
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
		return fileContainer;
	}


	private ExportDefinition getExportDefinition(ExportDefinitionDestination destination) {
		if (!destination.isSharedDestination()) {
			ExportDefinitionDestinationExportDefinitionSearchForm searchForm = new ExportDefinitionDestinationExportDefinitionSearchForm();
			searchForm.setExportDefinitionDestinationId(destination.getId());
			ExportDefinitionDestinationExportDefinition m2mDestination = CollectionUtils.getFirstElementStrict(getExportDefinitionService().getExportDefinitionDestinationExportDefinitionList(searchForm));
			return m2mDestination.getReferenceTwo();
		}
		else {
			ExportDefinitionSearchForm searchForm = new ExportDefinitionSearchForm();
			searchForm.setName(MANUAL_EXPORT_DEFINITION);
			List<ExportDefinition> results = getExportDefinitionService().getExportDefinitionList(searchForm);
			ValidationUtils.assertNotEmpty(results, "Cannot find a definition to assign the run for this destination.");
			return CollectionUtils.getFirstElementStrict(results);
		}
	}


	private ExportRunHistory createAndInitializeExportRunHistory(ExportDefinition exportDefinition) {
		ExportRunStatus newStatus = getExportRunService().getExportRunStatusByName(ExportRunStatusNames.RUNNING);

		ExportRunHistory exportRunHistory = new ExportRunHistory();
		exportRunHistory.setDefinition(exportDefinition);
		exportRunHistory.setExportRunSourceType(ExportRunSourceTypes.MANUAL);
		exportRunHistory.setStatus(newStatus);
		exportRunHistory.setScheduledDate(new Date());
		exportRunHistory.setStartDate(new Date());
		exportRunHistory.setDescription(newStatus.getLabel());
		exportRunHistory.setRetryAttemptNumber((short) 0);
		exportRunHistory = getExportRunService().saveExportRunHistory(exportRunHistory);

		return exportRunHistory;
	}


	private void validateExportIsRunnable(int definitionId) {
		getExportDefinitionService().validateDefinitionSendable(getExportDefinitionService().getExportDefinition(definitionId));
		ExportDefinitionDestinationExportDefinitionSearchForm destinationExportDefinitionSearchForm = new ExportDefinitionDestinationExportDefinitionSearchForm();
		destinationExportDefinitionSearchForm.setExportDefinitionId(definitionId);
		destinationExportDefinitionSearchForm.setDestinationDisabled(false);
		destinationExportDefinitionSearchForm.setLimit(1);
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(getExportDefinitionService().getExportDefinitionDestinationExportDefinitionList(destinationExportDefinitionSearchForm)), "There must be at least one active destination linked to an Export Definition to run it.");

		ExportDefinitionContentSearchForm contentSearchForm = new ExportDefinitionContentSearchForm();
		contentSearchForm.setDefinitionId(definitionId);
		contentSearchForm.setDisabled(false);
		contentSearchForm.setLimit(1);
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(getExportDefinitionService().getExportDefinitionContentList(contentSearchForm)), "There must be at least one active piece of content for an Export Definition to run it.");
	}


	private boolean isExportDefinitionRunning(ExportDefinition definition) {
		return getRunnerHandler().isRunnerWithTypeAndIdActive("EXPORT", Integer.toString(definition.getId()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	public ExportRunnerFactory getExportRunnerFactory() {
		return this.exportRunnerFactory;
	}


	public void setExportRunnerFactory(ExportRunnerFactory exportRunnerFactory) {
		this.exportRunnerFactory = exportRunnerFactory;
	}


	public ExportFileGenerationHandler getExportFileGenerationHandler() {
		return this.exportFileGenerationHandler;
	}


	public void setExportFileGenerationHandler(ExportFileGenerationHandler exportFileGenerationHandler) {
		this.exportFileGenerationHandler = exportFileGenerationHandler;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public ExportMessagingMessageConverterService getExportMessagingMessageConverterService() {
		return this.exportMessagingMessageConverterService;
	}


	public void setExportMessagingMessageConverterService(ExportMessagingMessageConverterService exportMessagingMessageConverterService) {
		this.exportMessagingMessageConverterService = exportMessagingMessageConverterService;
	}


	public AsynchronousMessageHandler getExportClientAsynchronousMessageHandler() {
		return this.exportClientAsynchronousMessageHandler;
	}


	public void setExportClientAsynchronousMessageHandler(AsynchronousMessageHandler exportClientAsynchronousMessageHandler) {
		this.exportClientAsynchronousMessageHandler = exportClientAsynchronousMessageHandler;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
