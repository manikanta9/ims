package com.clifton.export.run;


import com.clifton.core.beans.NamedEntity;
import com.clifton.core.dataaccess.dao.event.cache.impl.name.CacheByName;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;


/**
 * The <code>ExportRunStatus</code> class defines available export run statuses:
 * NONE, WAITING_FOR_RETRY, RUNNING, SENDING, FAILED_ERROR, FAILED_NOT_READY, COMPLETED, COMPLETED_NO_DATA
 *
 * @author vgomelsky
 */
@CacheByName
public class ExportRunStatus extends NamedEntity<Short> {

	public ExportRunStatusNames getExportRunStatusName() {
		return ExportRunStatusNames.valueOf(getName());
	}


	public enum ExportRunStatusNames {

		NONE(false, false), WAITING_FOR_RETRY(false, false),
		RUNNING(false, false), SENDING(false, false),
		FAILED_ERROR(true, true), FAILED_NOT_READY(true, true), FAILED_NO_DATA(true, true),
		COMPLETED(true, false), COMPLETED_NO_DATA(true, false);


		ExportRunStatusNames(boolean completed, boolean failed) {
			this.completed = completed;
			this.failed = failed;
		}


		private final boolean completed;
		private final boolean failed;

		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


		public boolean isCompleted() {
			return this.completed;
		}


		public boolean isCompletedWithoutErrors() {
			return isCompleted() && !isFailed();
		}


		public boolean isFailed() {
			return this.failed;
		}


		public static List<ExportRunStatusNames> getExportRunStatusNameListSuccessful() {
			return Arrays.stream(ExportRunStatusNames.values()).filter(ExportRunStatusNames::isCompletedWithoutErrors).collect(Collectors.toList());
		}


		public static List<ExportRunStatusNames> getExportRunStatusNameListFailed() {
			return Arrays.stream(ExportRunStatusNames.values()).filter(ExportRunStatusNames::isFailed).collect(Collectors.toList());
		}
	}
}
