package com.clifton.export.run.runner;

import com.clifton.export.definition.ExportDefinitionContent;


/**
 * The ExportContentNotReadyException class is used when {@link ExportDefinitionContent}
 * has content ready SystemCondition set that evaluates to false.
 *
 * @author jgommels
 */
public class ExportContentNotReadyException extends Exception {

	public ExportContentNotReadyException() {
		super();
	}


	public ExportContentNotReadyException(String message) {
		super(message);
	}
}
