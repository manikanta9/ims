package com.clifton.export.run.search;

import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.security.user.SecurityUserGroup;
import org.hibernate.Criteria;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.hibernate.criterion.Subqueries;
import org.hibernate.sql.JoinType;


/**
 * A SearchFormConfigurer for an ExportRunHistorySearchForm which facilitates searching for ExportRunHistory entities with ExportDefinitions
 * that belong to a particular user or that user's security group.
 *
 * @author davidi
 */
public class ExportRunHistorySearchFormConfigurer extends HibernateSearchFormConfigurer {

	private ExportRunHistorySearchForm searchForm;

	public ExportRunHistorySearchFormConfigurer(ExportRunHistorySearchForm searchForm) {
		super(searchForm);
		this.searchForm = searchForm;
	}


	@Override
	public void configureCriteria(Criteria criteria) {
		super.configureCriteria(criteria);

		if (this.searchForm.getOwnedByUserId() != null) {
			DetachedCriteria sub = DetachedCriteria.forClass(SecurityUserGroup.class, "sug");
			sub.setProjection(Projections.id());
			sub.add(Restrictions.eq("sug.referenceOne.id", this.searchForm.getOwnedByUserId()));
			sub.add(Restrictions.eqProperty("sug.referenceTwo.id", getPathAlias("definition", criteria, JoinType.LEFT_OUTER_JOIN) + ".ownerGroup.id"));
			criteria.add(Restrictions.or(Restrictions.eq(getPathAlias("definition", criteria) + ".ownerUser.id", this.searchForm.getOwnedByUserId()), Subqueries.exists(sub)));
		}
	}
}
