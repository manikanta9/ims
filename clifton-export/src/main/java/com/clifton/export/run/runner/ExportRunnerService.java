package com.clifton.export.run.runner;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.dataaccess.file.FileUploadWrapper;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.util.status.Status;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.run.ExportRunSourceTypes;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Date;
import java.util.List;


/**
 * The <code>ExportRunnerService</code> defines methods used to handle action related to export runs.
 *
 * @author Masood Siddiqui
 */
public interface ExportRunnerService {

	/**
	 * Schedules ExportDefinition with the specified id to run immediately with run type manual.
	 * The call is asynchronous and the job will be executed in a different thread.
	 */
	@SecureMethod(dtoClass = ExportDefinition.class)
	@RequestMapping("exportRunManual")
	public Status runManualExport(int id);


	/**
	 * Schedules ExportDefinition with the specified id to run immediately.
	 * The call is asynchronous and the job will be executed in a different thread.
	 */
	@DoNotAddRequestMapping
	public Status runExport(int id, ExportRunSourceTypes runSource);


	/**
	 * Sends a custom file to the destination
	 */
	@SecureMethod(dtoClass = ExportDefinition.class)
	@RequestMapping("exportSendCustomFileUpload")
	public void sendCustomExportFileUpload(FileUploadWrapper file, Integer destinationId);


	/**
	 * Sends a single (non-multipart) custom file to the destination given a FileWrapper and destinationId.
	 */
	@DoNotAddRequestMapping
	public void sendSingleCustomExportFile(FileWrapper fileWrapper, Integer destinationId);


	/**
	 * Previews the export without actually sending the file(s). If there is one file generated, then that file is returned.
	 * If there are multiple files generated, then a zipped file containing those files is returned. If no files are generated,
	 * <code>null</code> is returned. An optional previewDate parameter can be specified to generate the data for the provided date,
	 * overriding the export's scheduled date.
	 */
	@SecureMethod(dtoClass = ExportDefinition.class, permissions = SecurityPermission.PERMISSION_EXECUTE)
	public FileWrapper downloadExportPreview(int id, boolean exceptionIfNoResults, Date previewDate);


	/**
	 * Schedules ExportDefinition with the specified id to run immediately.
	 * The call is asynchronous and the job will be executed in a different thread.
	 */
	@RequestMapping("exportReschedule")
	@SecureMethod(dtoClass = ExportDefinition.class)
	public void rescheduleExport(int id, Date runOnDate);


	/**
	 * Returns a list of ExportDefinition objects that are in invalid state: RUNNING status but there's nothing running.
	 * This could happen if the server is killed while the job is running.
	 * <p>
	 * Job status must be updated so that the job can start again.
	 */
	public List<ExportDefinition> getExportDefinitionListInvalid();


	/**
	 * Fixes invalid export definition (see getExportDefinitionListInvalid) by setting its status to FAILED.
	 */
	@RequestMapping("exportDefinitionBatchFix")
	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_WRITE)
	public void fixExportDefinition(int id);
}
