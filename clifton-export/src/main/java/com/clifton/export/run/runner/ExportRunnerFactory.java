package com.clifton.export.run.runner;


import com.clifton.export.run.ExportRunSourceTypes;
import com.clifton.security.user.SecurityUser;

import java.util.Date;


/**
 * The <code>ExportRunnerFactory</code> interface defines methods for creating ExportRunner instances.
 */
public interface ExportRunnerFactory {

	/**
	 * Create a top level export job runner.
	 */
	public ExportRunner createExportRunner(Date runDate, int jobId, ExportRunSourceTypes exportRunSourceType);


	public ExportRunner createExportRunner(Date runDate, int jobId, ExportRunSourceTypes exportRunSourceType, SecurityUser runAsUser);


	/**
	 * Returns the location where files are going to be located
	 */
	public String getRootDirectory();
}
