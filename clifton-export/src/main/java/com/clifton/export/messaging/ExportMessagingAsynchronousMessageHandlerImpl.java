package com.clifton.export.messaging;

import com.clifton.core.messaging.asynchronous.AsynchronousMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandlerImpl;
import com.clifton.core.util.StringUtils;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.messaging.message.ExportMessagingMessageList;
import com.clifton.security.encryption.transport.client.SecurityRSATransportEncryptionClientHandler;
import org.springframework.stereotype.Component;

import java.util.Map;


/**
 * @author theodorez
 */
@Component
public class ExportMessagingAsynchronousMessageHandlerImpl extends AsynchronousMessageHandlerImpl {

	private SecurityRSATransportEncryptionClientHandler securityRSATransportEncryptionClientHandler;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	@Override
	public void send(AsynchronousMessage requestMessage) {
		send(requestMessage, null);
	}


	@Override
	public void send(AsynchronousMessage message, String correlationId) {
		if (message instanceof ExportMessagingMessage) {
			processMessage((ExportMessagingMessage) message);
		}
		else if (message instanceof ExportMessagingMessageList) {
			for (ExportMessagingMessage exportMessage : ((ExportMessagingMessageList) message).getMessageList()) {
				processMessage(exportMessage);
			}
		}
		super.send(message, correlationId);
	}


	private void processMessage(ExportMessagingMessage message) {
		for (ExportMessagingDestination destination : message.getDestinationList()) {
			for (Map.Entry<ExportMapKeys, String> propertyEntry : destination.getPropertyList().entrySet()) {
				if (propertyEntry.getKey().isValueEncrypted() && !StringUtils.isEmpty(propertyEntry.getValue())) {
					propertyEntry.setValue(getSecurityRSATransportEncryptionClientHandler().encryptValueForTransport(propertyEntry.getValue()));
				}
			}
		}
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SecurityRSATransportEncryptionClientHandler getSecurityRSATransportEncryptionClientHandler() {
		return this.securityRSATransportEncryptionClientHandler;
	}


	public void setSecurityRSATransportEncryptionClientHandler(SecurityRSATransportEncryptionClientHandler securityRSATransportEncryptionClientHandler) {
		this.securityRSATransportEncryptionClientHandler = securityRSATransportEncryptionClientHandler;
	}
}
