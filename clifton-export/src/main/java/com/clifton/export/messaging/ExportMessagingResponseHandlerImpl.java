package com.clifton.export.messaging;

import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.export.definition.ExportDefinitionService;
import com.clifton.export.messaging.message.ExportMessagingStatusMessage;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunStatus;


/**
 * Handles incoming export status messages from the integration server.
 *
 * @author mwacker
 */
public class ExportMessagingResponseHandlerImpl implements ExportMessagingResponseHandler {

	private ExportDefinitionService exportDefinitionService;
	private ExportRunService exportRunService;

	private String sourceSystemName;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void process(ExportMessagingStatusMessage message) {
		if (ExportStatuses.RECEIVED == message.getStatus()) {
			return;
		}
		if (!StringUtils.isEmpty(message.getSourceSystemIdentifier())) {
			int runHistoryId = Integer.parseInt(message.getSourceSystemIdentifier());
			ExportRunHistory runHistory = getExportRunService().getExportRunHistory(runHistoryId);
			AssertUtils.assertNotNull(runHistory, String.format("The run history [%s] could not be found on source system [%s].", runHistoryId, message.getSourceSystemName()));
			ExportRunStatus status = getExportRunStatus(message);
			if (status.getExportRunStatusName().isFailed() || !runHistory.getStatus().getExportRunStatusName().isCompleted() || status.getExportRunStatusName().isCompleted()) {
				runHistory.setDescription(StringUtils.formatStringUpToNCharsWithDots(runHistory.getDescription() + "\nINTEGRATION RESULT:\n" + message.getMessage(), DataTypes.DESCRIPTION_LONG.getLength(), true));
				if (!runHistory.getStatus().getExportRunStatusName().isFailed()) {
					runHistory.setStatus(status);
				}
				getExportRunService().saveExportRunHistory(runHistory);
				getExportDefinitionService().updateExportDefinitionStatus(runHistory.getDefinition().getId(), status);
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ExportRunStatus getExportRunStatus(ExportMessagingStatusMessage message) {
		ExportRunStatus result = null;
		switch (message.getStatus()) {
			case FAILED:
				result = getExportRunService().getExportRunStatusByName(ExportRunStatus.ExportRunStatusNames.FAILED_ERROR);
				break;
			case PROCESSED:
			case REPROCESSED:
				result = getExportRunService().getExportRunStatusByName(ExportRunStatus.ExportRunStatusNames.COMPLETED);
				break;
			default:
				break;
		}
		return result;
	}


	public ExportDefinitionService getExportDefinitionService() {
		return this.exportDefinitionService;
	}


	public void setExportDefinitionService(ExportDefinitionService exportDefinitionService) {
		this.exportDefinitionService = exportDefinitionService;
	}


	@Override
	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}
}
