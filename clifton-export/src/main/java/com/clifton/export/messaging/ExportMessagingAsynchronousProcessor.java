package com.clifton.export.messaging;

import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.StringUtils;
import com.clifton.export.messaging.message.ExportMessagingStatusMessage;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/**
 * Used to handle export status messages returned from Integration.
 *
 * @author mwacker
 */
public class ExportMessagingAsynchronousProcessor implements AsynchronousProcessor<ExportMessagingStatusMessage> {

	private Map<String, ExportMessagingResponseHandler> responseHandlerMap = new ConcurrentHashMap<>();

	/**
	 * A default user need to run the process so there is a user for database updates and inserts.
	 */
	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;
	private ContextHandler contextHandler;
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	@Transactional
	public void process(ExportMessagingStatusMessage message, @SuppressWarnings("unused") AsynchronousMessageHandler handler) {
		if (message == null) {
			return;
		}
		String sourceSystemName = getSourceSystemName(message);
		ExportMessagingResponseHandler messagingResponseHandler = getExportMessagingResponseHandler(sourceSystemName);
		if (messagingResponseHandler == null) {
			throw new RuntimeException("Cannot process message the message because no handler for source system [" + sourceSystemName + "] is registered.  Message: [" + message + "]");
		}
		// set the user for database
		setRunAsUser();
		messagingResponseHandler.process(message);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private ExportMessagingResponseHandler getExportMessagingResponseHandler(String sourceSystemName) {
		if (StringUtils.isEmpty(sourceSystemName)) {
			return null;
		}
		return getResponseHandlerMap().get(sourceSystemName);
	}


	private void setRunAsUser() {
		if ((getContextHandler() != null) && getContextHandler().getBean(Context.USER_BEAN_NAME) == null) {
			SecurityUser result = getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
			getContextHandler().setBean(Context.USER_BEAN_NAME, result);
		}
	}


	private String getSourceSystemName(ExportMessagingStatusMessage message) {
		if (!StringUtils.isEmpty(message.getSourceSystemName())) {
			return message.getSourceSystemName();
		}

		return message.getProperties().containsKey("sourceSystemName") ? message.getProperties().get("sourceSystemName") : null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public Map<String, ExportMessagingResponseHandler> getResponseHandlerMap() {
		return this.responseHandlerMap;
	}


	public void setResponseHandlerMap(Map<String, ExportMessagingResponseHandler> responseHandlerMap) {
		this.responseHandlerMap = responseHandlerMap;
	}
}
