package com.clifton.export.messaging;


import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.CSVFileToDataTableConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.destination.ExportDestinationGenerator;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.message.ExportMessagingContent;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunStatus;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanProperty;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


public class ExportMessagingMessageConverterServiceImpl implements ExportMessagingMessageConverterService {

	private SystemBeanService systemBeanService;
	private SecuritySecretService securitySecretService;

	private ExportRunService exportRunService;


	/**
	 * Unique name of the source system. In our case it is IMS.
	 */
	private String sourceSystemName;

	/**
	 * The location of the share
	 */
	private String rootDirectory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportMessagingMessage toExportMessagingMessage(Integer exportRunHistoryId, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		ExportMessagingMessage exportMessage = buildExportMessage(exportRunHistoryId, exportDefinition);
		populateExportContents(exportMessage, files);
		populateExternalAndProcessInternalExportDestinations(exportMessage, exportDefinition, files, false, configurationMap);
		return exportMessage;
	}


	@Override
	public ExportMessagingMessage toExportMessagingMessage(ExportDefinition definition, List<FileWrapper> files) {
		return toExportMessagingMessage(definition, files, false);
	}


	@Override
	public ExportMessagingMessage toExportMessagingMessageIncludeDisabledDestinations(ExportDefinition definition, List<FileWrapper> files) {
		return toExportMessagingMessage(definition, files, true);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ExportMessagingMessage toExportMessagingMessage(ExportDefinition definition, List<FileWrapper> files, boolean includeDisabledDestinations) {
		ExportMessagingMessage message = buildExportMessage(null, definition);
		for (FileWrapper file : CollectionUtils.getIterable(files)) {
			addExportContent(message, buildExportContent(file.getFile().toFileContainer().getAbsolutePath()));
		}
		populateExternalAndProcessInternalExportDestinations(message, definition, null, includeDisabledDestinations, null);
		return message;
	}


	/**
	 * Will populate the exportMessage with the destinations generated by external destination generators. Will also process internal destinations.
	 */
	private void populateExternalAndProcessInternalExportDestinations(ExportMessagingMessage exportMessage, ExportDefinition exportDefinition, List<ExportFile> files, boolean includeDisabledDestinations, Map<String, Object> configurationMap) {
		//Gather the destinations
		for (ExportDefinitionDestination destination : CollectionUtils.getIterable(exportDefinition.getDestinationList())) {
			//Skip disabled destinations
			if (destination.isDisabled() && !includeDisabledDestinations) {
				continue;
			}

			//Get the destination generator system bean from the destination
			ExportDestinationGenerator systemBean = (ExportDestinationGenerator) getSystemBeanService().getBeanInstance(destination.getDestinationSystemBean());
			//If a destination is external it will be sent via JMS
			if (systemBean.getType().isExternalDestination()) {
				if (systemBean.getType() == ExportDestinationTypes.BY_ROW_EMAIL) {
					List<ExportMessagingDestination> destinationList = buildByRowEmailDestinations(files, exportDefinition, destination, systemBean);
					exportMessage.getDestinationList().addAll(destinationList);
				}
				else {
					Map<ExportMapKeys, String> map = systemBean.generateDestination(destination, exportDefinition, files, configurationMap);
					ExportMessagingDestination exportDestination = new ExportMessagingDestination();
					exportDestination.setType(systemBean.getType());
					exportDestination.getPropertyList().putAll(map);
					exportMessage.getDestinationList().add(exportDestination);
				}
			}
			else {
				//If the destination is internal it is capable of sending on its own
				systemBean.processInternalDestination(files, configurationMap);
				if (exportDefinition.getDestinationList().size() == 1) {
					ExportRunHistory runHistory = getExportRunService().getExportRunHistory(Integer.parseInt(exportMessage.getSourceSystemIdentifier()));
					runHistory.setStatus(getExportRunService().getExportRunStatusByName(ExportRunStatus.ExportRunStatusNames.COMPLETED));
					getExportRunService().saveExportRunHistory(runHistory);
				}
			}
		}
	}


	private List<ExportMessagingDestination> buildByRowEmailDestinations(List<ExportFile> files, ExportDefinition exportDefinition, ExportDefinitionDestination destination, ExportDestinationGenerator systemBean) {
		List<ExportMessagingDestination> results = new ArrayList<>();

		CSVFileToDataTableConverter converter = new CSVFileToDataTableConverter();
		for (ExportFile exportFile : CollectionUtils.getIterable(files)) {
			if (isCsvExportFormatWithHeader(exportFile)) {
				DataTable dataTable = converter.convert(new File(exportFile.getPath()));
				for (DataRow row : CollectionUtils.getIterable(dataTable.getRowList())) {
					Map<String, Object> valueMap = row.getRowValueMap();
					Map<ExportContentContextMapKeys, Object> context = exportFile.getFileContext();
					if (context == null) {
						context = new EnumMap<>(ExportContentContextMapKeys.class);
						exportFile = new ExportFile(exportFile.getPath(), exportFile.getExportContent(), context);
					}
					populateExportContentReferenceMap(exportFile, valueMap);
					Map<ExportMapKeys, String> map = systemBean.generateDestination(destination, exportDefinition, Collections.singletonList(exportFile), valueMap);
					ExportMessagingDestination exportDestination = new ExportMessagingDestination();
					exportDestination.setType(systemBean.getType());
					exportDestination.getPropertyList().putAll(map);
					results.add(exportDestination);
				}
			}
		}
		return results;
	}


	private boolean isCsvExportFormatWithHeader(ExportFile exportFile) {
		AssertUtils.assertNotNull(exportFile.getExportContent(), "Expected an export content.");
		AssertUtils.assertNotNull(exportFile.getExportContent().getContentSystemBean(), "Expected export content system bean.");

		SystemBean contentSystemBean = exportFile.getExportContent().getContentSystemBean();
		if (CollectionUtils.isEmpty(contentSystemBean.getPropertyList())) {
			contentSystemBean = getSystemBeanService().getSystemBean(contentSystemBean.getId());
		}
		boolean csv = false;
		boolean headers = false;
		for (SystemBeanProperty property : CollectionUtils.getIterable(contentSystemBean.getPropertyList())) {
			SystemBeanPropertyType type = property.getType();
			if ("format".equals(type.getSystemPropertyName()) && "CSV".equalsIgnoreCase(property.getValue())) {
				csv = true;
			}
			else if ("suppressHeaderRow".equals(type.getSystemPropertyName()) && "false".equalsIgnoreCase(property.getValue())) {
				headers = true;
			}
		}
		return csv && headers;
	}


	private void populateExportContentReferenceMap(ExportFile exportFile, Map<String, Object> queryMap) {
		Map<ExportContentContextMapKeys, Object> context = exportFile.getFileContext();
		if (!context.containsKey(ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP)) {
			context.put(ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP, queryMap);
		}
		else if (context.get(ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP) instanceof Map) {
			@SuppressWarnings("unchecked")
			Map<String, Object> exportContentContextMap = (Map<String, Object>) context.get(ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP);
			exportContentContextMap.putAll(queryMap);
		}
		else {
			throw new RuntimeException("Expected a context map for key " + ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP);
		}
	}


	private ExportMessagingMessage buildExportMessage(Integer exportRunHistoryId, ExportDefinition exportDefinition) {
		ExportMessagingMessage exportMessage = new ExportMessagingMessage();

		exportMessage.setRetryCount(exportDefinition.getRetryCount());
		exportMessage.setRetryDelayInSeconds(exportDefinition.getRetryDelayInSeconds());

		if (exportRunHistoryId != null) {
			//set the export definition run history id as the source system identifier
			exportMessage.setSourceSystemIdentifier(exportRunHistoryId.toString());
		}

		exportMessage.setSourceSystemDefinitionName(exportDefinition.getName());

		//set the unique source system name of IMS
		exportMessage.setSourceSystemName(getSourceSystemName());

		exportMessage.setBaseArchivePath(exportDefinition.getType().getName());
		exportMessage.setArchiveStrategyName(exportDefinition.getArchiveStrategyName());

		return exportMessage;
	}


	private ExportMessagingContent buildExportContent(String fileNameWithPath) {
		ExportMessagingContent exportContent = new ExportMessagingContent();
		//Strip export location off file path - leaving remaining relative path
		exportContent.setFileNameWithPath(FileUtils.getRelativePath(fileNameWithPath, getRootDirectory()));
		exportContent.setDeleteSourceFile(true);

		return exportContent;
	}


	private void populateExportContents(ExportMessagingMessage exportMessage, List<ExportFile> files) {
		for (ExportFile file : CollectionUtils.getIterable(files)) {
			ExportMessagingContent exportContent = buildExportContent(file.getPath());

			if (file.getExportContent() != null && file.getExportContent().getEncryptionSecret() != null) {
				exportContent.setPassword(getSecuritySecretService().decryptSecuritySecret(file.getExportContent().getEncryptionSecret()).getSecretString());
			}

			addExportContent(exportMessage, exportContent);
		}
	}


	private void addExportContent(ExportMessagingMessage exportMessage, ExportMessagingContent content) {
		exportMessage.getContentList().add(content);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	/**
	 * @return Unique name of the source system. In our case it is IMS.
	 */
	public String getSourceSystemName() {
		return this.sourceSystemName;
	}


	public void setSourceSystemName(String sourceSystemName) {
		this.sourceSystemName = sourceSystemName;
	}


	public String getRootDirectory() {
		return this.rootDirectory;
	}


	public void setRootDirectory(String rootDirectory) {
		this.rootDirectory = rootDirectory;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}
}
