package com.clifton.export.messaging;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.message.ExportMessagingMessage;

import java.util.List;
import java.util.Map;


/**
 * The <code>ExportMessagingMessageConverterService</code> is for converting
 * Export/IMS related domain objects to JMS related objects to be put on queue.
 *
 * @author msiddiqui
 */
public interface ExportMessagingMessageConverterService {

	/**
	 * Creates and returns message that can be sent through JMS which contains the export information.
	 *
	 * @param exportRunHistoryId the ID of the {@link com.clifton.export.run.ExportRunHistory} associated with the export
	 * @param exportDefinition   the export being run
	 * @param files              the list of files to send
	 * @param configurationMap   the dynamic configuration map to use
	 */
	public ExportMessagingMessage toExportMessagingMessage(Integer exportRunHistoryId, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap);


	/**
	 * Creates and returns message that can be sent through JMS which contains the export information. Does not included disabled destinations.
	 */
	public ExportMessagingMessage toExportMessagingMessage(ExportDefinition definition, List<FileWrapper> files);


	/**
	 * Creates and returns a message that can be sent through JMS. Includes disabled destinations.
	 */
	public ExportMessagingMessage toExportMessagingMessageIncludeDisabledDestinations(ExportDefinition definition, List<FileWrapper> files);
}
