package com.clifton.export.messaging;

import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.PropertyValue;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.config.TypedStringValue;
import org.springframework.beans.factory.support.ManagedMap;

import java.util.Map;
import java.util.Optional;


/**
 * The <code>ExportMessagingResponseHandlerPostProcessor</code> is a post processor that will register ExportMessagingResponseHandler objects
 * with the ExportMessagingAsynchronousProcessor based on the source system name.  The ExportMessagingResponseHandlers are used to process
 * status messages received from the integration server when exports either complete or fail.
 *
 * @author mwacker
 */
public class ExportMessagingResponseHandlerPostProcessor implements BeanFactoryPostProcessor {

	@Override
	public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
		if (!beanFactory.containsBean("exportMessagingAsynchronousProcessor")) {
			return;
		}

		// Populate source system name -> response handler mappings
		String[] beanNames = beanFactory.getBeanNamesForType(ExportMessagingResponseHandler.class, true, false);
		Map<String, RuntimeBeanReference> responseHandlerMap = new ManagedMap<>();
		for (String beanName : CollectionUtils.createList(beanNames)) {
			// Reference response handler by source system name
			BeanDefinition responseHandlerBeanDefinition = beanFactory.getBeanDefinition(beanName);
			PropertyValue sourceSystemNameProp = responseHandlerBeanDefinition.getPropertyValues().getPropertyValue("sourceSystemName");
			AssertUtils.assertNotNull(sourceSystemNameProp, "Could not find property 'sourceSystemName' for response handler");
			String sourceSystemName = Optional.ofNullable((String) sourceSystemNameProp.getConvertedValue())
					.orElseGet(() -> (AssertUtils.assertNotNull((TypedStringValue) sourceSystemNameProp.getValue(), "sourceSystemName value was null")).getValue());
			responseHandlerMap.put(sourceSystemName, new RuntimeBeanReference(beanName));
		}

		// Modify messaging processor bean definition to attach response handler implementors
		BeanDefinition processorBeanDefinition = beanFactory.getBeanDefinition("exportMessagingAsynchronousProcessor");
		processorBeanDefinition.getPropertyValues().addPropertyValue("responseHandlerMap", responseHandlerMap);
	}
}
