package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.file.ExportFileNameGenerationHandler;
import com.clifton.export.template.ExportTemplateCommand;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * Generate a file using a Freemarker template for each row of a data table.
 *
 * @author theodorez
 */
public class ExportTransformationDataTableToFileUsingFreemarkerGenerator extends ExportTransformationBaseDataTableFreemarkerGenerator<FileWrapper> {


	private ExportFileNameGenerationHandler exportFileNameGenerationHandler;

	////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		FileWrapper fileWrapper;
		BufferedWriter bufferedWriter = null;
		String fileName = getExportFileNameGenerationHandler().formatFileName(content.getFileNameTemplate(), prepareExportTemplateCommand(content, new HashMap<>()));
		try {
			File tempFile = File.createTempFile(FileUtils.getFileNameWithoutExtension(fileName), "." + FileUtils.getFileExtension(fileName));
			bufferedWriter = new BufferedWriter(new FileWriter(tempFile));
			writeHeader(content, input, bufferedWriter, context);
			writeBody(content, input, bufferedWriter, context);
			writeFooter(content, input, bufferedWriter, context);
			fileWrapper = new FileWrapper(tempFile, fileName, true);
		}
		catch (Throwable e) {
			throw new RuntimeException("Unable to transform file during export of file: " + fileName, e);
		}
		finally {
			FileUtils.close(bufferedWriter);
		}
		return fileWrapper;
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void writeHeader(ExportDefinitionContent content, DataTable table, BufferedWriter writer, Map<String, Object> context) throws IOException {
		if (!StringUtils.isEmpty(getHeaderTemplate())) {
			Map<String, Object> headerContext = getHeaderContext(content, table, context);
			writeLine(writer, getHeaderTemplate(), prepareExportTemplateCommand(content, headerContext));
		}
	}


	private void writeBody(ExportDefinitionContent content, DataTable table, BufferedWriter writer, Map<String, Object> context) throws IOException {
		DataColumn[] columns = table.getColumnList();
		for (int i = 0; i < table.getTotalRowCount(); i++) {
			Map<String, Object> rowContext = getRowContext(content, table, context, i, columns);
			ExportTemplateCommand<ExportDefinitionContent> command = prepareExportTemplateCommand(content, rowContext);
			writeLine(writer, getRowTemplate(), command);
		}
	}


	private void writeFooter(ExportDefinitionContent content, DataTable table, BufferedWriter writer, Map<String, Object> context) throws IOException {
		if (!StringUtils.isEmpty(getFooterTemplate())) {
			Map<String, Object> footerContext = getFooterContext(content, table, context);
			writeLine(writer, getFooterTemplate(), prepareExportTemplateCommand(content, footerContext));
		}
	}


	private void writeLine(BufferedWriter writer, String template, ExportTemplateCommand<ExportDefinitionContent> command) throws IOException {
		writer.write(applyTemplate(template, command) + System.getProperty("line.separator"));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportFileNameGenerationHandler getExportFileNameGenerationHandler() {
		return this.exportFileNameGenerationHandler;
	}


	public void setExportFileNameGenerationHandler(ExportFileNameGenerationHandler exportFileNameGenerationHandler) {
		this.exportFileNameGenerationHandler = exportFileNameGenerationHandler;
	}
}
