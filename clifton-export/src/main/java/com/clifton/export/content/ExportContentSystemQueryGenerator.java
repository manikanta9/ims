package com.clifton.export.content;


import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.FileFormats;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.template.ExportTemplateGenerationHandler;
import com.clifton.system.query.BaseSystemQueryBeanPropertyProvider;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.execute.SystemQueryExecutionService;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A system bean that will generate an export file for a system query.
 *
 * @author mwacker
 */
public class ExportContentSystemQueryGenerator extends BaseSystemQueryBeanPropertyProvider implements ExportContentGenerator<DataTable> {

	/**
	 * The file format for the export.
	 */
	private FileFormats format;

	/**
	 * Omit outputting the header row.
	 */
	private boolean suppressHeaderRow;

	/**
	 * Query timeout in seconds, default is -1, indicating to use the JDBC driver's default
	 */
	private Integer queryTimeoutSeconds = -1;

	////////////////////////////////////////////////////////////////////////////

	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;
	private SystemQueryExecutionService systemQueryExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable generateContentData(ExportDefinitionContent content, Date previewDate, Map<String, Object> configurationMap) {
		List<SystemQueryParameterValue> propertyList = getPropertyList(null);

		// TODO: move the date look up logic to a central location.
		for (SystemQueryParameterValue value : CollectionUtils.getIterable(propertyList)) {
			LogUtils.debug(LogCommand.ofMessage(getClass(), "System Query Parameter Name: ", value.getParameter().getName(), " Value: ", value.getValue(), " Text: ", value.getText()));

			if (DataTypeNames.DATE == DataTypeNames.valueOf(value.getParameter().getDataType().getName())) {
				String dateString = DateUtils.fromDate(previewDate != null ? previewDate : getCalendarDateGenerationHandler().generateDate(value.getValue()), DateUtils.DATE_FORMAT_INPUT);
				value.setValue(dateString);
				value.setText(dateString);
			}
		}

		//If the map is present and not empty, there may be overrides
		if (configurationMap != null && !configurationMap.isEmpty()) {
			Map<String, SystemQueryParameterValue> valueMap = BeanUtils.getBeanMap(propertyList, value -> value.getParameter().getName());
			List<SystemQueryParameter> availableParameters = getSystemQueryService().getSystemQueryParameterListByQuery(getSystemQueryId());
			//if there are overrides present for parameters that are valid for the query, construct new parameter values
			for (SystemQueryParameter parameter : availableParameters) {
				String parameterName = parameter.getName();
				Object parameterOverride = configurationMap.get(parameterName);
				if (parameterOverride != null) {
					SystemQueryParameterValue value = new SystemQueryParameterValue();
					value.setParameter(parameter);
					value.setValue((String) parameterOverride);
					value.setText((String) parameterOverride);
					LogUtils.debug(LogCommand.ofMessage(getClass(), "System Query Parameter Name: ", parameterName, " Override found: ", value.getValue()));
					//replaces existing parameter values that are stored on the bean
					valueMap.put(parameterName, value);
				}
			}
			propertyList = new ArrayList<>(valueMap.values());
		}

		getSystemQuery().setParameterValueList(propertyList);

		return getSystemQueryExecutionService().getSystemQueryResultWithTimeout(getSystemQuery(), getQueryTimeoutSeconds());
	}


	@Override
	public List<FileWrapper> writeContent(DataTable data, String fileName, @SuppressWarnings("unused") ExportDefinitionContent content,
	                                      @SuppressWarnings("unused") ExportRunHistory runHistory, @SuppressWarnings("unused") Date previewDate) {
		DataTableFileConfig config = new DataTableFileConfig(data);
		config.setSuppressHeaderRow(isSuppressHeaderRow());
		File file = getFormat().getDataTableConverter().convert(config);
		if (file != null) {
			return CollectionUtils.createList(new FileWrapper(file, fileName, true));
		}
		else {
			return Collections.emptyList();
		}
	}


	@Override
	public String getContentLabel() {
		StringBuilder resultSb = new StringBuilder(100);

		if (getSystemQuery() != null) {
			resultSb.append(getSystemQuery().getName());
			resultSb.append(' ');
		}

		if (getFormat() != null) {
			resultSb.append('(').append(getFormat()).append(')');
		}

		return resultSb.toString();
	}


	@Override
	public Map<String, Object> getTemplateBeanMap(ExportDefinitionContent content, Date previewDate) {
		Map<String, Object> map = new HashMap<>();

		List<SystemQueryParameterValue> propertyList = getPropertyList(null);
		if (!CollectionUtils.isEmpty(propertyList)) {
			for (SystemQueryParameterValue value : propertyList) {
				String key = value.getParameter().getName().concat("_parameter");
				Object parameterValue = value.getValue();
				if (DataTypeNames.DATE == DataTypeNames.valueOf(value.getParameter().getDataType().getName())) {
					parameterValue = DateUtils.fromDate(getCalendarDateGenerationHandler().generateDate(value.getValue()), DateUtils.DATE_FORMAT_INPUT);
				}
				map.put(key, parameterValue);
			}
		}
		return map;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FileFormats getFormat() {
		return this.format;
	}


	public void setFormat(FileFormats format) {
		this.format = format;
	}


	public boolean isSuppressHeaderRow() {
		return this.suppressHeaderRow;
	}


	public void setSuppressHeaderRow(boolean suppressHeaderRow) {
		this.suppressHeaderRow = suppressHeaderRow;
	}


	public Integer getQueryTimeoutSeconds() {
		return this.queryTimeoutSeconds;
	}


	public void setQueryTimeoutSeconds(Integer queryTimeoutSeconds) {
		this.queryTimeoutSeconds = queryTimeoutSeconds;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}


	public SystemQueryExecutionService getSystemQueryExecutionService() {
		return this.systemQueryExecutionService;
	}


	public void setSystemQueryExecutionService(SystemQueryExecutionService systemQueryExecutionService) {
		this.systemQueryExecutionService = systemQueryExecutionService;
	}
}
