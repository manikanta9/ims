package com.clifton.export.content;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.template.ExportTemplateGenerator;

import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * The <code>ExportContentGenerator</code> defines methods used to generate data and output files that will be exported.
 */
public interface ExportContentGenerator<C> extends ExportTemplateGenerator<ExportDefinitionContent> {

	/**
	 * Returns the data that can be transformed before generating the file for the export.
	 *
	 * @param content - an instance of the ExportDefinitionContent class used in data generation.
	 * @return data that can be transformed
	 */
	default public C generateContentData(@SuppressWarnings("unused") ExportDefinitionContent content) {
		return generateContentData(content, null, null);
	}


	/**
	 * Returns the data that can be transformed before generating the file for the export.
	 *
	 * @param content          - an instance of the ExportDefinitionContent class used in data generation.
	 * @param previewDate      - an optional date value that, if defined, is used to generate the data for that date.
	 * @param configurationMap
	 * @return data that can be transformed
	 */
	default public C generateContentData(@SuppressWarnings("unused") ExportDefinitionContent content, Date previewDate, Map<String, Object> configurationMap) {
		return null;
	}


	/**
	 * Returns the file with the newly generated content.
	 */
	default public List<FileWrapper> writeContent(C data, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory) {
		return writeContent(data, fileName, content, runHistory, null);
	}


	/**
	 * Returns the file with the newly generated content. If previewDate is specified, it will be used for data generation.
	 */
	public List<FileWrapper> writeContent(C data, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory, Date previewDate);


	/**
	 * Returns user friendly label for this generator.  For example, report name, account number, etc.
	 */
	public String getContentLabel();
}
