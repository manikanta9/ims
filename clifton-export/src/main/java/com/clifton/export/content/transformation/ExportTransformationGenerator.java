package com.clifton.export.content.transformation;


import com.clifton.export.definition.ExportDefinitionContent;

import java.util.Map;


/**
 * Transforms export content from the input type to the output type.
 */
public interface ExportTransformationGenerator<I, O> {

	public O generateTransformation(I input, Integer runId, ExportDefinitionContent content, Map<String, Object> context);


	public Class<I> getInputClass();
}
