package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.Map;


/**
 * Transformation generator to manipulate column presence and order for a data table.
 *
 * @author MikeH
 */
public class ExportTransformationColumnManipulationGenerator implements ExportTransformationGenerator<DataTable, DataTable> {

	/**
	 * The column names to be included in the resulting table, in the order in which they are desired.
	 */
	private String[] includedColumnNames;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		// 1. Validate required parameters
		if (ArrayUtils.isEmpty(getIncludedColumnNames())) {
			throw new ValidationException("Unable to process data table re-mappings: No columns to include have been specified.");
		}

		// 2. Get column objects
		DataColumn[] includedColumns = getColumns(input, getIncludedColumnNames());

		// 4. Get table with no duplicates
		return getDataTableWithManipulatedColumns(input, includedColumns);
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the array of column objects in the given data table matching the list of specified column names, in order.
	 *
	 * @param input       the data table from which to retrieve the column objects
	 * @param columnNames the column names for which to retrieve the column objects
	 * @return the column objects in the order of the column names provided
	 */
	private static DataColumn[] getColumns(DataTable input, String... columnNames) {
		DataColumn[] columns = new DataColumn[columnNames.length];
		for (int columnIndex = 0; columnIndex < columnNames.length; columnIndex++) {
			String columnName = columnNames[columnIndex];
			DataColumn column = ArrayUtils.getStream(input.getColumnList())
					.filter(col -> StringUtils.isEqual(columnName, col.getColumnName())).findFirst()
					.orElseThrow(() -> new ValidationException("Unable to find column [" + columnName + "] within the data table."));
			columns[columnIndex] = column;
		}
		return columns;
	}


	/**
	 * Gets the transformation of the data table with manipulated columns.
	 * <p>
	 * The result of this transformation includes the given columns in the order provided. Any columns not given shall not be included in the resulting table.
	 *
	 * @param table           the table to convert
	 * @param includedColumns the columns to include, in the order desired
	 * @return the transformed data table
	 */
	private static DataTable getDataTableWithManipulatedColumns(DataTable table, DataColumn[] includedColumns) {
		// Get column indices, in order
		int[] includedColumnIndices = ArrayUtils.getStream(includedColumns)
				.mapToInt(column -> ArrayUtils.indexOf(table.getColumnList(), column))
				.toArray();

		// Build row data from indices
		DataTable manipulatedTable = new PagingDataTableImpl(includedColumns);
		for (int rowIndex = 0; rowIndex < table.getTotalRowCount(); rowIndex++) {
			DataRow row = table.getRow(rowIndex);
			Object[] manipulatedRowData = new Object[includedColumnIndices.length];
			for (int newColumnIndex = 0; newColumnIndex < manipulatedRowData.length; newColumnIndex++) {
				int originalColumnIndex = includedColumnIndices[newColumnIndex];
				manipulatedRowData[newColumnIndex] = row.getValue(originalColumnIndex);
			}
			manipulatedTable.addRow(new DataRowImpl(manipulatedTable, manipulatedRowData));
		}
		return manipulatedTable;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getIncludedColumnNames() {
		return this.includedColumnNames;
	}


	public void setIncludedColumnNames(String[] includedColumnNames) {
		this.includedColumnNames = includedColumnNames;
	}
}
