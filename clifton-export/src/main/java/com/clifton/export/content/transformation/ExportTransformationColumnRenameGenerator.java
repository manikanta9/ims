package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author KellyJ
 */
public class ExportTransformationColumnRenameGenerator implements ExportTransformationGenerator<DataTable, DataTable> {

	/**
	 * A mapping of original column names to an alias.
	 * The string comes in as a string array in the format: [OriginalValue==NewValue,OriginalValueTwo==NewValueTwo]
	 */
	private String[] columnNameToAliasMapping;

	private static final String MAPPING_DELIMITER = "==";


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		// Validate required parameters
		if (ArrayUtils.isEmpty(getColumnNameToAliasMapping())) {
			throw new ValidationException("Unable to process data table re-mappings: No columns to rename have been specified.");
		}

		// Get the renamed columns
		DataColumn[] dataColumns = getColumns(input, convertToMap(this.columnNameToAliasMapping));

		// Get table with renamed columns
		DataTable result = getDataTableWithRenamedColumns(input, dataColumns);

		return result;
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the array of column objects in the given data table matching the list of specified column names, in order.
	 *
	 * @param input       the data table from which to retrieve the column objects
	 * @param columnNames the column names for which to retrieve the column objects
	 * @return the column objects in the order of the column names provided
	 */
	private static DataColumn[] getColumns(DataTable input, Map<String, String> columnNames) {
		DataColumn[] result = new DataColumn[columnNames.size()];
		if (columnNames != null) {
			List<DataColumn> columns = ArrayUtils.getStream(input.getColumnList())
					.peek(dataColumn -> {
						String alias = columnNames.get(dataColumn.getColumnName());
						if (!StringUtils.isEmpty(alias)) {
							dataColumn.setColumnName(alias);
						}
					})
					.collect(Collectors.toList());

			if (columns != null) {
				result = columns.toArray(result);
			}
		}
		else {
			return input.getColumnList();
		}
		return result;
	}


	/**
	 * Gets the transformation of the data table with manipulated columns.
	 * <p>
	 * The result of this transformation includes the given columns in the order provided. Any columns not given shall not be included in the resulting table.
	 *
	 * @param table              the table to convert
	 * @param transformedColumns the columns to include, in the order desired
	 * @return the transformed data table
	 */
	private static DataTable getDataTableWithRenamedColumns(DataTable table, DataColumn[] transformedColumns) {
		// Get column indices, in order
		int[] columnIndices = ArrayUtils.getStream(transformedColumns)
				.mapToInt(column -> ArrayUtils.indexOf(table.getColumnList(), column))
				.toArray();

		// Build row data from indices
		DataTable manipulatedTable = new PagingDataTableImpl(transformedColumns);
		for (int rowIndex = 0; rowIndex < table.getTotalRowCount(); rowIndex++) {
			DataRow row = table.getRow(rowIndex);
			Object[] manipulatedRowData = new Object[columnIndices.length];
			for (int newColumnIndex = 0; newColumnIndex < manipulatedRowData.length; newColumnIndex++) {
				int originalColumnIndex = columnIndices[newColumnIndex];
				manipulatedRowData[newColumnIndex] = row.getValue(originalColumnIndex);
			}
			manipulatedTable.addRow(new DataRowImpl(manipulatedTable, manipulatedRowData));
		}
		return manipulatedTable;
	}


	private static Map<String, String> convertToMap(String[] columns) {
		Map<String, String> columnNames = new HashMap<>();
		Arrays.stream(columns).forEach(col -> {
			List<String> columnList = StringUtils.delimitedStringToList(col, MAPPING_DELIMITER);
			if (columnList != null && columnList.size() > 1) {
				columnNames.put(columnList.get(0), columnList.get(1));
			}
		});
		return columnNames;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getColumnNameToAliasMapping() {
		return this.columnNameToAliasMapping;
	}


	public void setColumnNameToAliasMapping(String[] columnNameToAliasMapping) {
		this.columnNameToAliasMapping = columnNameToAliasMapping;
	}
}
