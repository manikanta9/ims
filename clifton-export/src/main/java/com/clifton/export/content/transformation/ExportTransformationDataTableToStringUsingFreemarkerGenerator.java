package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.StringUtils;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.Map;


/**
 * Converts the DataTable passed in and applies the given header, body, footer templates
 * and and then returns the transformed output as a String
 *
 * @author theodorez
 */
public class ExportTransformationDataTableToStringUsingFreemarkerGenerator extends ExportTransformationBaseDataTableFreemarkerGenerator<String> {


	@Override
	public String generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		StringBuilder result = new StringBuilder();
		appendHeader(result, content, input, context);
		appendBody(result, content, input, context);
		appendFooter(result, content, input, context);
		return result.toString();
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void appendHeader(StringBuilder builder, ExportDefinitionContent content, DataTable table, Map<String, Object> context) {
		if (!StringUtils.isEmpty(getHeaderTemplate())) {
			Map<String, Object> headerContext = getHeaderContext(content, table, context);
			builder.append(applyTemplate(getHeaderTemplate(), prepareExportTemplateCommand(content, headerContext)));
		}
	}


	private void appendBody(StringBuilder builder, ExportDefinitionContent content, DataTable table, Map<String, Object> context) {
		DataColumn[] columns = table.getColumnList();
		for (int i = 0; i < table.getTotalRowCount(); i++) {
			Map<String, Object> rowContext = getRowContext(content, table, context, i, columns);
			builder.append(applyTemplate(getRowTemplate(), prepareExportTemplateCommand(content, rowContext)));
		}
	}


	private void appendFooter(StringBuilder builder, ExportDefinitionContent content, DataTable table, Map<String, Object> context) {
		if (!StringUtils.isEmpty(getFooterTemplate())) {
			Map<String, Object> headerContext = getFooterContext(content, table, context);
			builder.append(applyTemplate(getFooterTemplate(), prepareExportTemplateCommand(content, headerContext)));
		}
	}
}
