package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * Transformation generator to filter table content based on column values.
 *
 * @author MikeH
 */
public class ExportTransformationColumnValueFilterGenerator implements ExportTransformationGenerator<DataTable, DataTable> {

	private static final String FORMAT_DATETIME = "yyyy-MM-dd HH:mm:ss";
	private static final String FORMAT_DATE = "yyyy-MM-dd";
	private static final String FORMAT_TIME = "HH:mm:ss";

	/**
	 * The name of the column to use for the filter.
	 */
	private String columnName;

	/**
	 * The valid column values for which the filter will be satisfied.
	 */
	private List<String> columnValueList;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		DataColumn column = getColumn(input);
		List<String> valueList = getFormattedColumnValueList(input, column);
		return getFilteredResults(input, column, valueList);
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}


	/**
	 * Gets the specified column object within the given data table.
	 *
	 * @param input the data table
	 * @return the column object
	 * @throws ValidationException if the column does not exist within the data table
	 */
	private DataColumn getColumn(DataTable input) {
		return ArrayUtils.getStream(input.getColumnList())
				.filter(col -> StringUtils.isEqual(getColumnName(), col.getColumnName())).findFirst()
				.orElseThrow(() -> new ValidationException("Unable to find column [" + getColumnName() + "] within the data table."));
	}


	/**
	 * Gets the column value list. The values are converted if necessary for matching.
	 *
	 * @param input  the data table
	 * @param column the column containing the values
	 */
	private List<String> getFormattedColumnValueList(DataTable input, DataColumn column) {
		List<String> valueList = new ArrayList<>();
		if (input.getTotalRowCount() > 0
				&& input.getRow(0).getValue(column) instanceof Date) {
			// Standardize dates by parsing and then re-converting to strings
			for (String columnValue : getColumnValueList()) {
				Date dateValue = DateUtils.toDate(columnValue, FORMAT_DATETIME, FORMAT_DATE, FORMAT_TIME);
				valueList.add(String.valueOf(dateValue));
			}
		}
		else {
			valueList.addAll(getColumnValueList());
		}
		return valueList;
	}


	/**
	 * Gets the matching rows from the given data table.
	 *
	 * @param input     the data table
	 * @param column    the column containing the values
	 * @param valueList the list of valid values
	 * @return the filtered data table
	 */
	private DataTable getFilteredResults(DataTable input, DataColumn column, List<String> valueList) {
		DataTable filteredResults = new PagingDataTableImpl(input.getColumnList());
		for (int rowIndex = 0; rowIndex < input.getTotalRowCount(); rowIndex++) {
			DataRow row = input.getRow(rowIndex);
			Object value = row.getValue(column);
			if (value instanceof Date) {
				// Convert SQL types to standard java.util.Date object to standardize toString format
				value = DateUtils.asUtilDate(value);
			}
			if (value != null && CollectionUtils.contains(valueList, String.valueOf(value))) {
				filteredResults.addRow(row);
			}
		}
		return filteredResults;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getColumnName() {
		return this.columnName;
	}


	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}


	public List<String> getColumnValueList() {
		return this.columnValueList;
	}


	public void setColumnValueList(List<String> columnValueList) {
		this.columnValueList = columnValueList;
	}
}
