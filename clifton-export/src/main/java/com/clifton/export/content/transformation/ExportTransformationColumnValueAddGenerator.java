package com.clifton.export.content.transformation;

import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.definition.ExportDefinitionContent;

import java.sql.Types;
import java.util.Map;


/**
 * Transformation generator to add a column to the data table at a given index and with a provided name
 * and a default value if so desired.
 *
 * @author JasonS
 */
public class ExportTransformationColumnValueAddGenerator implements ExportTransformationGenerator<DataTable, DataTable>, ValidationAware {

	/**
	 * The column header and column index are required, if a default value is included, it will be added.
	 */
	private String columnHeader;
	private Integer columnIndex;
	private String value;

	private TemplateConverter templateConverter;
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		// 1. Set up the index target
		int columnIndexTarget = computeColumnIndexTarget(input);

		// 2. Get column objects
		DataColumn[] includedColumns = getColumns(input, columnIndexTarget);

		// 3. Get table with no duplicates
		return getDataTableWithManipulatedColumns(input, includedColumns, columnIndexTarget);
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}


	@Override
	public void validate() {
		if (StringUtils.isEmpty(getColumnHeader())) {
			throw new ValidationException("Unable to process data table re-mappings: No new column header has been specified.");
		}
		if (StringUtils.isEmpty(getValue())) {
			throw new ValidationException("Some value must be provided, either a string or a Freemarker compatible calculation.");
		}
	}
	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * If an index is null, negative or beyond the max size, assume it should be appended to the end of the table. Otherwise, set the target as presented.
	 */
	private int computeColumnIndexTarget(DataTable input) {
		if (MathUtils.isNullOrZero(getColumnIndex())) {
			if (MathUtils.isZeroOrOne(getColumnIndex())) {
				return getColumnIndex();
			}
			else {
				return input.getColumnCount();
			}
		}
		return (MathUtils.isNegative(getColumnIndex()) || MathUtils.isGreaterThan(getColumnIndex(), input.getColumnCount())) ?
				input.getColumnCount() : getColumnIndex();
	}


	/**
	 * Gets the array of column objects in the given data table matching the list of specified column names, in order.
	 *
	 * @param input the data table from which to retrieve the column objects
	 * @return the column objects with the new column object added
	 */
	private DataColumn[] getColumns(DataTable input, int columnIndexTarget) {
		DataColumn[] columns;
		DataColumn columnInsert = new DataColumnImpl(getColumnHeader(), Types.VARCHAR);

		columns = ArrayUtils.add(input.getColumnList(), columnInsert, columnIndexTarget);

		return columns;
	}


	/**
	 * Gets the transformation of the data table with manipulated columns.
	 * <p>
	 * The result of this transformation includes the given columns in the order provided. Any columns not given shall not be included in the resulting table.
	 *
	 * @param table           the table to convert
	 * @param includedColumns the columns to include, in the order desired
	 * @return the transformed data table
	 */
	private DataTable getDataTableWithManipulatedColumns(DataTable table, DataColumn[] includedColumns, int columnIndexTarget) {
		// Build row data from indices
		DataTable manipulatedTable = new PagingDataTableImpl(includedColumns);
		for (int rowIndex = 0; rowIndex < table.getTotalRowCount(); rowIndex++) {
			DataRow row = table.getRow(rowIndex);
			Object[] manipulatedRowData = new Object[includedColumns.length];
			for (int newColumnIndex = 0; newColumnIndex < manipulatedRowData.length; newColumnIndex++) {
				if (newColumnIndex == columnIndexTarget) {
					manipulatedRowData[newColumnIndex] = getTransformedValue(row);
				}
				else {
					manipulatedRowData[newColumnIndex] = row.getValue(includedColumns[newColumnIndex].getColumnName());
				}
			}
			manipulatedTable.addRow(new DataRowImpl(manipulatedTable, manipulatedRowData));
		}
		return manipulatedTable;
	}


	/**
	 * Adds all the row data to the context and translates the FreeMarker template into a value to be added to the table
	 *
	 * @param row
	 * @return the default value string or the computed string from the row data.
	 */
	private String getTransformedValue(DataRow row) {
		TemplateConfig templateConfig = new TemplateConfig(getValue());
		row.getRowValueMap(true).forEach(templateConfig::addBeanToContext);
		return getTemplateConverter().convert(templateConfig);
	}

	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String getColumnHeader() {
		return this.columnHeader;
	}


	public void setColumnHeader(String columnHeader) {
		this.columnHeader = columnHeader;
	}


	public Integer getColumnIndex() {
		return this.columnIndex;
	}


	public void setColumnIndex(Integer columnIndex) {
		this.columnIndex = columnIndex;
	}


	public String getValue() {
		return this.value;
	}


	public void setValue(String value) {
		this.value = value;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
