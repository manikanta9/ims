package com.clifton.export.content;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;

import java.util.Date;
import java.util.List;


/**
 * Will return a file from a network path to be exported.  The path to the file can be a Freemarker template.
 * <p>
 * For example:
 * <p>
 * "\\some_server\path\${DATE?string("yyyyMMdd")}_filename.xls" will on 2/25/2015 resolve to "\\some_server\path\20150225_filename.xls"
 *
 * @author theodorez
 */
public class ExportContentNetworkFileGenerator implements ExportContentGenerator<Object> {

	/**
	 * The network file path, can also be e Freemarker template such as "\\some_server\path\${DATE?string("yyyyMMdd")}_filename.xls"
	 */
	private String fileLocation;
	/**
	 * Flag to indicate if the source file should be deleted after sending it
	 */
	private Boolean deleteAfterSend;

	////////////////////////////////////////////////////////////////////////////

	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<FileWrapper> writeContent(@SuppressWarnings("unused") Object data, String fileName, ExportDefinitionContent content,
	                                      @SuppressWarnings("unused") ExportRunHistory runHistory, @SuppressWarnings("unused") Date previewDate) {
		//Freemarker command template
		ExportTemplateCommand<ExportDefinitionContent> command = new ExportTemplateCommand<>(this, content, content.getDefinition(), null);

		// Replace date with preview date if defined
		if (previewDate != null) {
			command.getContextMap().put("DATE", previewDate);
		}

		//apply template to the file location
		String processedFileLocation = applyTemplate(getFileLocation(), command);
		FileContainer file = FileContainerFactory.getFileContainer(processedFileLocation);

		//apply template to the filename
		String processedFileName = applyTemplate(fileName, command);

		if (!file.exists()) {
			throw new ValidationException("File does not exist: " + file.getAbsolutePath());
		}

		FileWrapper fileWrapper = new FileWrapper(FilePath.forPath(file.getPath()), processedFileName, false);

		if (getDeleteAfterSend() != null) {
			//setting the temp file to true will delete the file in the directory after it is sent
			fileWrapper.setTempFile(getDeleteAfterSend());
		}

		return CollectionUtils.createList(fileWrapper);
	}


	@Override
	public String getContentLabel() {
		return getFileLocation();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String applyTemplate(String field, ExportTemplateCommand<ExportDefinitionContent> command) {
		return getExportTemplateGenerationHandler().processTemplate(field, command);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFileLocation() {
		return this.fileLocation;
	}


	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}


	public void setDeleteAfterSend(Boolean deleteAfterSend) {
		this.deleteAfterSend = deleteAfterSend;
	}


	public Boolean getDeleteAfterSend() {
		return this.deleteAfterSend;
	}


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}
}
