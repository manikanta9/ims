package com.clifton.export.content;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.aws.s3.AWSS3Handler;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;

import java.util.Date;
import java.util.List;


/**
 * @author BrandonC, rtschumper
 */
public class ExportContentAWSS3Generator implements ExportContentGenerator<Object> {

	private AWSS3Handler awsS3Handler;

	private String awsBucketName;

	private String awsRegionName;

	private String awsFileName;

	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;

	private String fileLocation;


	@Override
	public List<FileWrapper> writeContent(Object data, String fileName, ExportDefinitionContent content, ExportRunHistory runHistory, Date previewDate) {
		//Freemarker command template
		ExportTemplateCommand<ExportDefinitionContent> command = new ExportTemplateCommand<>(this, content, content.getDefinition(), null);

		// Replace date with preview date if defined
		if (previewDate != null) {
			command.getContextMap().put("DATE", previewDate);
		}

		setFileLocation(getAwsS3Handler().retrieve(awsRegionName, awsBucketName, awsFileName, null));

		String processedFileLocation = applyTemplate(getFileLocation(), command);
		FileContainer file = FileContainerFactory.getFileContainer(processedFileLocation);

		//apply template to the filename
		String processedFileName = applyTemplate(fileName, command);

		if (!file.exists()) {
			throw new ValidationException("File does not exist: " + file.getAbsolutePath());
		}

		FileWrapper fileWrapper = new FileWrapper(FilePath.forPath(file.getPath()), processedFileName, false);

		// Deleting the file in the network - no need to keep it stored there if its on S3
		fileWrapper.setTempFile(true);

		return CollectionUtils.createList(fileWrapper);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////

	@Override
	public String getContentLabel() {
		return getAwsBucketName() + "/" + getFileLocation();
	}


	private String applyTemplate(String field, ExportTemplateCommand<ExportDefinitionContent> command) {
		return getExportTemplateGenerationHandler().processTemplate(field, command);
	}


	public void setAwsBucketName(String awsBucketName) {
		this.awsBucketName = awsBucketName;
	}


	public String getAwsBucketName() {
		return this.awsBucketName;
	}


	public void setAwsRegionName(String awsRegionName) {
		this.awsRegionName = awsRegionName;
	}


	public String getAwsRegionName() {
		return this.awsRegionName;
	}


	public String getAwsFileName() {
		return this.awsFileName;
	}


	public void setAwsFileName(String awsFileName) {
		this.awsFileName = awsFileName;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setFileLocation(String fileLocation) {
		this.fileLocation = fileLocation;
	}


	public String getFileLocation() {
		return this.fileLocation;
	}


	public AWSS3Handler getAwsS3Handler() {
		return this.awsS3Handler;
	}


	public void setAwsS3Handler(AWSS3Handler awsS3Handler) {
		this.awsS3Handler = awsS3Handler;
	}
}
