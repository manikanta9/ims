package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.file.ExportFileNameGenerationHandler;
import com.clifton.export.file.ExportFileUtils;
import com.clifton.export.template.ExportTemplateCommand;

import java.util.HashMap;
import java.util.Map;


/**
 * @author TerryS
 */
public abstract class ExportTransformationToFileGenerator<I> implements ExportTransformationGenerator<I, FileWrapper> {

	private ExportFileNameGenerationHandler exportFileNameGenerationHandler;

	////////////////////////////////////////////////////////////////////////////


	protected String getOutputFileName(ExportDefinitionContent content, Integer runId) {
		String fileName = getExportFileNameGenerationHandler().formatFileName(content.getFileNameTemplate(), prepareExportTemplateCommand(content, new HashMap<>()));
		return ExportFileUtils.appendUniqueRunIdentifier(fileName, runId);
	}


	protected ExportTemplateCommand<ExportDefinitionContent> prepareExportTemplateCommand(ExportDefinitionContent content, Map<String, Object> contextMap) {
		return new ExportTemplateCommand<>(null, content, content.getDefinition(), contextMap);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportFileNameGenerationHandler getExportFileNameGenerationHandler() {
		return this.exportFileNameGenerationHandler;
	}


	public void setExportFileNameGenerationHandler(ExportFileNameGenerationHandler exportFileNameGenerationHandler) {
		this.exportFileNameGenerationHandler = exportFileNameGenerationHandler;
	}
}
