package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.util.StringUtils;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author theodorez
 */
public abstract class ExportTransformationBaseDataTableFreemarkerGenerator<T> implements ExportTransformationGenerator<DataTable, T> {

	/**
	 * The template for the header row
	 */
	private String headerTemplate;
	/**
	 * The template for the body rows
	 */
	private String rowTemplate;
	/**
	 * The template for the footer row
	 */
	private String footerTemplate;

	////////////////////////////////////////////////////////////////////////////

	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected Map<String, Object> getHeaderContext(ExportDefinitionContent content, DataTable table, Map<String, Object> context) {
		Map<String, Object> headerContext = new HashMap<>();
		List<String> columnHeaders = new ArrayList<>();
		if (!StringUtils.isEmpty(getHeaderTemplate())) {
			StringBuilder columnInformation = new StringBuilder();
			columnInformation.append("COLUMN INFORMATION:\n");
			DataColumn[] columns = table.getColumnList();
			for (int i = 0; i < columns.length; i++) {
				DataColumn column = columns[i];
				columnInformation.append("Column[");
				columnInformation.append(i);
				columnInformation.append("] Name -> \"");
				columnInformation.append(column.getColumnName());
				columnInformation.append("\" Freemarker Variable -> \"");
				columnInformation.append(StringUtils.formatFreemarkerVariable(column.getColumnName()));
				columnInformation.append("\"\n");
				headerContext.put(column.getColumnName(), column);
				columnHeaders.add(column.getColumnName());
			}
			headerContext.put("allColumnInformation", columnInformation.toString());
			headerContext.put("numberOfRows", table.getTotalRowCount());
			headerContext.put("numberOfColumns", table.getColumnCount());
			headerContext.put("allColumns", columnHeaders);
		}
		headerContext.putAll(context);
		return headerContext;
	}


	protected Map<String, Object> getRowContext(ExportDefinitionContent content, DataTable table, Map<String, Object> context, int rowNumber, DataColumn[] columns) {
		Map<String, Object> rowContext = getRowValueMap(columns, table.getRow(rowNumber));
		rowContext.putAll(context);
		rowContext.put("rowNumber", rowNumber);
		return rowContext;
	}


	protected Map<String, Object> getFooterContext(ExportDefinitionContent content, DataTable table, Map<String, Object> context) {
		Map<String, Object> footerContext = new HashMap<>();
		footerContext.put("numberOfRows", table.getTotalRowCount());
		footerContext.putAll(context);
		return footerContext;
	}


	protected Map<String, Object> getRowValueMap(DataColumn[] columns, DataRow row) {
		Map<String, Object> result = new HashMap<>();
		List<Object> rowValues = new ArrayList<>();
		for (int i = 0; i < columns.length; i++) {
			Object rowValue = row.getValue(i);
			if (rowValue != null) {
				result.put(columns[i].getColumnName(), rowValue);
			}
			rowValues.add(rowValue);
		}
		result.put("rowValues", rowValues);
		return result;
	}


	protected ExportTemplateCommand<ExportDefinitionContent> prepareExportTemplateCommand(ExportDefinitionContent content, Map<String, Object> contextMap) {
		return new ExportTemplateCommand<>(null, content, content.getDefinition(), contextMap);
	}


	protected String applyTemplate(String field, ExportTemplateCommand<ExportDefinitionContent> command) {
		return getExportTemplateGenerationHandler().processTemplate(field, command);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getHeaderTemplate() {
		return this.headerTemplate;
	}


	public void setHeaderTemplate(String headerTemplate) {
		this.headerTemplate = headerTemplate;
	}


	public String getRowTemplate() {
		return this.rowTemplate;
	}


	public void setRowTemplate(String rowTemplate) {
		this.rowTemplate = rowTemplate;
	}


	public String getFooterTemplate() {
		return this.footerTemplate;
	}


	public void setFooterTemplate(String footerTemplate) {
		this.footerTemplate = footerTemplate;
	}


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}
}
