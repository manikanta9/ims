package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreCollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.export.definition.ExportDefinitionContent;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.function.BinaryOperator;
import java.util.function.UnaryOperator;
import java.util.stream.Collectors;


/**
 * Transformation generator to filter out similar rows from a data table based on provided criteria for uniqueness.
 *
 * @author MikeH
 */
public class ExportTransformationColumnValueGroupingGenerator implements ExportTransformationGenerator<DataTable, DataTable> {

	/**
	 * The SQL column types for which column aggregation is supported.
	 */
	private static final Integer[] SUPPORTED_AGGREGATION_TYPES = {
			// String types
			java.sql.Types.VARCHAR, java.sql.Types.NVARCHAR, java.sql.Types.LONGVARCHAR, java.sql.Types.LONGNVARCHAR,
			java.sql.Types.CHAR, java.sql.Types.NCHAR, java.sql.Types.CLOB, java.sql.Types.NCLOB,

			// Integer types
			java.sql.Types.TINYINT, java.sql.Types.SMALLINT, java.sql.Types.INTEGER, java.sql.Types.BIGINT,

			// Decimal types
			java.sql.Types.FLOAT, java.sql.Types.DOUBLE, java.sql.Types.NUMERIC, java.sql.Types.DECIMAL,

			// Time types
			java.sql.Types.TIME, java.sql.Types.TIME_WITH_TIMEZONE
	};

	/**
	 * The column names on which each row must be unique.
	 */
	private String[] groupingColumnNames;

	/**
	 * The column names which should be aggregated.
	 */
	private String[] aggregatedColumnNames;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public DataTable generateTransformation(DataTable input, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		// 1. Validate required parameters
		if (ArrayUtils.isEmpty(getGroupingColumnNames())) {
			throw new ValidationException("Unable to process data table groupings: No columns have been specified on which to group items.");
		}

		// 2. Get column objects
		List<DataColumn> groupingColumnList = getColumns(input, getGroupingColumnNames());
		List<DataColumn> aggregatedColumnList = getColumns(input, getAggregatedColumnNames());

		// 3. Validate parameter column rules
		for (DataColumn column : aggregatedColumnList) {
			if (!ArrayUtils.contains(SUPPORTED_AGGREGATION_TYPES, column.getDataType())) {
				throw new ValidationException(String.format("Unable to perform aggregation on column [%s]. Aggregation is not supported for columns of type [%s].",
						column.getColumnName(), java.sql.JDBCType.valueOf(column.getDataType())));
			}
		}

		// 4. Get table with no duplicates
		return getDataTableWithNoDuplicates(input, groupingColumnList, aggregatedColumnList);
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Gets the list of column objects in the given data table matching the list of specified column names, in order.
	 *
	 * @param input       the data table from which to retrieve the column objects
	 * @param columnNames the list of column names for which to retrieve the column objects
	 * @return the collection of column objects in the order of the column names provided
	 */
	private static List<DataColumn> getColumns(DataTable input, String... columnNames) {
		List<DataColumn> columns = new ArrayList<>();
		for (String columnName : CollectionUtils.createList(columnNames)) {
			DataColumn column = ArrayUtils.getStream(input.getColumnList())
					.filter(col -> StringUtils.isEqual(columnName, col.getColumnName())).findFirst()
					.orElseThrow(() -> new ValidationException("Unable to find column [" + columnName + "] within the data table."));
			columns.add(column);
		}
		return columns;
	}


	/**
	 * Gets the aggregated result of the two provided values. This result will be calculated based on the SQL column type provided.
	 * <p>
	 * The result will be of the appropriate type for aggregation, using wrapper classes to avoid overflow issues.
	 *
	 * @param existingValue the existing value, if one exists
	 * @param addedValue    the value to be added to the existing value
	 * @param sqlValueType  the SQL type for the column from which the values were extracted (see {@link java.sql.Types})
	 * @return the result of the given values added together
	 * @see #SUPPORTED_AGGREGATION_TYPES
	 */
	private static Object getAggregatedColumnValue(Object existingValue, Object addedValue, int sqlValueType) {
		Object newValue;
		// Handle trivial cases: No aggregation necessary
		if (existingValue == null && addedValue == null) {
			newValue = null;
		}
		else {
			// Evaluate reduction function based on column type
			UnaryOperator<Object> typeConverter;
			BinaryOperator<Object> valueAggregator;
			switch (sqlValueType) {
				case java.sql.Types.VARCHAR:
				case java.sql.Types.NVARCHAR:
				case java.sql.Types.LONGVARCHAR:
				case java.sql.Types.LONGNVARCHAR:
				case java.sql.Types.CHAR:
				case java.sql.Types.NCHAR:
				case java.sql.Types.CLOB:
				case java.sql.Types.NCLOB:
					typeConverter = val -> val == null ? null : val instanceof BigDecimal ? (BigDecimal) val : new BigDecimal((String) val);
					valueAggregator = (val1, val2) -> ((BigDecimal) val1).add((BigDecimal) val2);
					break;
				case java.sql.Types.TINYINT:
				case java.sql.Types.SMALLINT:
				case java.sql.Types.INTEGER:
				case java.sql.Types.BIGINT:
					typeConverter = val -> val == null ? null : val instanceof BigInteger ? (BigInteger) val : BigInteger.valueOf(((Number) val).longValue());
					valueAggregator = (val1, val2) -> ((BigInteger) val1).add((BigInteger) val2);
					break;
				case java.sql.Types.FLOAT:
				case java.sql.Types.DOUBLE:
				case java.sql.Types.NUMERIC:
				case java.sql.Types.DECIMAL:
					typeConverter = val -> val == null ? null : val instanceof BigDecimal ? (BigDecimal) val : BigDecimal.valueOf(((Number) val).doubleValue());
					valueAggregator = (val1, val2) -> ((BigDecimal) val1).add((BigDecimal) val2);
					break;
				case java.sql.Types.TIME:
				case java.sql.Types.TIME_WITH_TIMEZONE:
					typeConverter = UnaryOperator.identity();
					valueAggregator = (val1, val2) -> Date.from(Instant.ofEpochMilli(((Date) val1).getTime() + ((Date) val2).getTime()));
					break;
				default:
					throw new ValidationException(String.format("Aggregation is not supported for columns with type value [%d].", sqlValueType));
			}
			try {
				// Convert to appropriate type for aggregation
				Object addedValueConverted = typeConverter.apply(addedValue);
				if (existingValue == null) {
					newValue = addedValueConverted;
				}
				else if (addedValueConverted == null) {
					newValue = existingValue;
				}
				else {
					newValue = valueAggregator.apply(existingValue, addedValueConverted);
				}
			}
			catch (Exception e) {
				throw new ValidationException(String.format("Unable to perform numeric/time-based aggregation for values [%s] and [%s].", existingValue, addedValue), e);
			}
		}
		return newValue;
	}


	/**
	 * Converts the table into a data list. The elements in this list correspond to the rows and the columns of the given table, in order.
	 *
	 * @param table                   the table to convert
	 * @param columnDataTypes         the SQL data type integers for all columns, in order
	 * @param aggregatedColumnIndices the column indices whose values should be normalized to their aggregation-friendly counterparts
	 * @return the data list
	 */
	private List<List<Object>> getDataListFromTable(DataTable table, List<Integer> columnDataTypes, Collection<Integer> aggregatedColumnIndices) {
		List<List<Object>> rowDataList = new ArrayList<>();
		for (int rowIndex = 0; rowIndex < table.getTotalRowCount(); rowIndex++) {
			DataRow row = table.getRow(rowIndex);
			List<Object> rowData = new ArrayList<>();
			for (int columnIndex = 0; columnIndex < table.getColumnCount(); columnIndex++) {
				// Perform aggregation type transformation if necessary
				Object rowValue = aggregatedColumnIndices.contains(columnIndex)
						? getAggregatedColumnValue(null, row.getValue(columnIndex), columnDataTypes.get(columnIndex))
						: row.getValue(columnIndex);
				rowData.add(rowValue);
			}
			rowDataList.add(rowData);
		}
		return rowDataList;
	}


	/**
	 * Gets the list of distinct rows from the given data set, grouping and performing aggregation on the given column indices.
	 *
	 * @param rowDataList             the data set
	 * @param columnDataTypes         the SQL data type integers for all columns, in order
	 * @param groupingColumnIndices   the indices of the columns on which rows will be evaluated for duplication
	 * @param aggregatedColumnIndices the indices of the columns on which rows will be aggregated for duplicate rows
	 * @return the list of distinct rows, with aggregation performed on the given columns for all grouped rows
	 */
	private List<List<Object>> getDistinctDataList(List<List<Object>> rowDataList, List<Integer> columnDataTypes, Collection<Integer> groupingColumnIndices, Collection<Integer> aggregatedColumnIndices) {
		return CollectionUtils.getDistinct(rowDataList,
				// Group by grouping column values
				rowData -> CollectionUtils.getStream(groupingColumnIndices)
						.map(rowData::get)
						.collect(Collectors.toList()),

				// Reduce according to aggregators
				(currentRowData, newRowData) -> {
					// Guard-clause: Trivial case; no aggregated columns exist
					if (CollectionUtils.isEmpty(aggregatedColumnIndices)) {
						return currentRowData;
					}

					// Aggregate values
					List<Object> aggregatedRowData = CoreCollectionUtils.clone(currentRowData);
					for (Integer aggregatedColumnIndex : aggregatedColumnIndices) {
						try {
							Object aggregatedValue = getAggregatedColumnValue(
									currentRowData.get(aggregatedColumnIndex),
									newRowData.get(aggregatedColumnIndex),
									columnDataTypes.get(aggregatedColumnIndex));
							aggregatedRowData.set(aggregatedColumnIndex, aggregatedValue);
						}
						catch (Exception e) {
							throw new ValidationException(String.format("Aggregation failed while processing row [%d] at column [%s] with value [%s]. Reason: [%s].",
									rowDataList.indexOf(newRowData), aggregatedColumnIndex, newRowData.get(aggregatedColumnIndex), e.getMessage()), e);
						}
					}
					return aggregatedRowData;
				});
	}


	/**
	 * Generates a new data table from the given input table without duplicate rows.
	 * <p>
	 * Rows are evaluated as <i>duplicate</i> based only on the given list of columns. If more than one row has the same set of values for the given list of
	 * columns, then only the first such row will exist in the resulting collection.
	 * <p>
	 * For any columns provided in the aggregated column list, the sum of the values in that column for all duplicate rows will be placed in the resulting row.
	 *
	 * @param table                the input data table
	 * @param groupingColumnList   the list of columns on which rows will be evaluated for duplication
	 * @param aggregatedColumnList the list of columns which will be aggregated for duplicate rows
	 * @return a new data table containing the rows of the input data table, in order, with no duplicates
	 */
	private DataTable getDataTableWithNoDuplicates(DataTable table, List<DataColumn> groupingColumnList, List<DataColumn> aggregatedColumnList) {
		// Get indices and data types
		Collection<Integer> groupingColumnIndices = CollectionUtils.getStream(groupingColumnList)
				.map(groupingColumn -> ArrayUtils.indexOf(table.getColumnList(), groupingColumn))
				.collect(Collectors.toList());
		Collection<Integer> aggregatedColumnIndices = CollectionUtils.getStream(aggregatedColumnList)
				.map(aggregatedColumn -> ArrayUtils.indexOf(table.getColumnList(), aggregatedColumn))
				.collect(Collectors.toSet());
		List<Integer> columnDataTypes = ArrayUtils.getStream(table.getColumnList())
				.map(DataColumn::getDataType)
				.collect(Collectors.toList());

		// Get unique rows according to constraints, aggregating values between duplicate rows as necessary
		List<List<Object>> rowDataList = getDataListFromTable(table, columnDataTypes, aggregatedColumnIndices);
		List<List<Object>> uniqueRowDataList = getDistinctDataList(rowDataList, columnDataTypes, groupingColumnIndices, aggregatedColumnIndices);

		// Build table from rows
		DataTable reducedTable = new PagingDataTableImpl(ArrayUtils.cloneArray(table.getColumnList()));
		for (List<Object> rowValueList : uniqueRowDataList) {
			reducedTable.addRow(new DataRowImpl(reducedTable, rowValueList.toArray(new Object[0])));
		}
		return reducedTable;
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Getter and Setter Methods                       ////////
	////////////////////////////////////////////////////////////////////////////


	public String[] getGroupingColumnNames() {
		return this.groupingColumnNames;
	}


	public void setGroupingColumnNames(String[] groupingColumnNames) {
		this.groupingColumnNames = groupingColumnNames;
	}


	public String[] getAggregatedColumnNames() {
		return this.aggregatedColumnNames;
	}


	public void setAggregatedColumnNames(String[] aggregatedColumnNames) {
		this.aggregatedColumnNames = aggregatedColumnNames;
	}
}
