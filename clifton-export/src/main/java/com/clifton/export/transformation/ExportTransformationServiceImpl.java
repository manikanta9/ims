package com.clifton.export.transformation;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.export.transformation.search.ExportTransformationSearchForm;
import org.hibernate.Criteria;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ExportTransformationServiceImpl implements ExportTransformationService {

	private AdvancedUpdatableDAO<ExportTransformation, Criteria> exportTransformationDAO;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportTransformation getExportTransformation(int id) {
		return getExportTransformationDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ExportTransformation> getExportTransformationList(ExportTransformationSearchForm searchForm) {
		return getExportTransformationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ExportTransformation saveExportTransformation(ExportTransformation bean) {
		return getExportTransformationDAO().save(bean);
	}


	@Override
	public void deleteExportTransformation(int id) {
		getExportTransformationDAO().delete(id);
	}

	////////////////////////////////////////////////////////////////////////////
	//////                  Getters and Setters                           //////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ExportTransformation, Criteria> getExportTransformationDAO() {
		return this.exportTransformationDAO;
	}


	public void setExportTransformationDAO(AdvancedUpdatableDAO<ExportTransformation, Criteria> exportTransformationDAO) {
		this.exportTransformationDAO = exportTransformationDAO;
	}
}
