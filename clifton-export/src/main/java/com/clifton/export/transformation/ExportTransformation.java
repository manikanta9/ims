package com.clifton.export.transformation;


import com.clifton.core.beans.BaseEntity;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.system.bean.SystemBean;


/**
 * The <code>ExportTransformation</code> class applies optional transformation to
 * export content.  For example, we can use QueryExport to generate Excel spreadsheet
 * and then apply ExportExcelColumnTransformationGenerator to it to re-map a specific
 * column from one value to another using user defined mapping.
 *
 * @author vgomelsky
 */
public class ExportTransformation extends BaseEntity<Integer> {

	private ExportDefinitionContent content;
	private SystemBean transformationSystemBean;

	/**
	 * Multiple transformations can be applied to same content file in the following order.
	 */
	private Integer order;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionContent getContent() {
		return this.content;
	}


	public void setContent(ExportDefinitionContent content) {
		this.content = content;
	}


	public SystemBean getTransformationSystemBean() {
		return this.transformationSystemBean;
	}


	public void setTransformationSystemBean(SystemBean transformationSystemBean) {
		this.transformationSystemBean = transformationSystemBean;
	}


	public Integer getOrder() {
		return this.order;
	}


	public void setOrder(Integer order) {
		this.order = order;
	}
}
