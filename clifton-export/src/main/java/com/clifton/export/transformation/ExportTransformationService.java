package com.clifton.export.transformation;

import com.clifton.export.transformation.search.ExportTransformationSearchForm;

import java.util.List;


/**
 * Defines the service methods for ExportTransformation objects.
 * <p>
 *
 * @author mwacker
 */
public interface ExportTransformationService {

	public ExportTransformation getExportTransformation(int id);


	public List<ExportTransformation> getExportTransformationList(ExportTransformationSearchForm searchForm);


	public ExportTransformation saveExportTransformation(ExportTransformation bean);


	public void deleteExportTransformation(int id);
}
