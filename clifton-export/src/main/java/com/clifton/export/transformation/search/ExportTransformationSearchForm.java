package com.clifton.export.transformation.search;

import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ExportTransformationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "content.id")
	private Integer definitionContentId;


	public Integer getDefinitionContentId() {
		return this.definitionContentId;
	}


	public void setDefinitionContentId(Integer definitionContentId) {
		this.definitionContentId = definitionContentId;
	}
}
