package com.clifton.export.definition;

import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.event.DaoEventTypes;
import com.clifton.core.dataaccess.dao.event.SelfRegisteringDaoObserver;
import com.clifton.core.util.runner.RunnerHandler;
import com.clifton.core.util.runner.RunnerUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.run.runner.ExportRunnerProvider;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.SystemConditionService;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import org.springframework.stereotype.Component;


/**
 * Observer that will check the Export Definition upon modification and reschedule it if the schedule has been changed
 *
 * @author theodorez
 */
@Component
public class ExportDefinitionObserver extends SelfRegisteringDaoObserver<ExportDefinition> {

	private ExportRunnerProvider exportRunnerProvider;
	private RunnerHandler runnerHandler;
	private SystemConditionService systemConditionService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;


	@Override
	public DaoEventTypes getRegisteredDaoEventTypes() {
		return DaoEventTypes.MODIFY;
	}


	@Override
	protected void beforeMethodCallImpl(ReadOnlyDAO<ExportDefinition> dao, DaoEventTypes event, ExportDefinition bean) {
		if (event.isInsert() || event.isUpdate()) {
			SystemCondition permissionCondition = getSystemConditionService().getSystemConditionByName("Export Definition Priority Modify Condition");
			if (permissionCondition != null) {
				EvaluationResult result = getSystemConditionEvaluationHandler().evaluateCondition(permissionCondition, bean);
				ValidationUtils.assertTrue(result.isTrue(), result.getMessage());
			}
		}
	}


	@Override
	@SuppressWarnings("unused")
	public void afterMethodCallImpl(ReadOnlyDAO<ExportDefinition> dao, DaoEventTypes event, ExportDefinition bean, Throwable e) {
		// If job is changed to enabled, or schedule changes, reschedule jobs
		if (e == null && isRescheduleNecessary(dao, event, bean)) {
			RunnerUtils.rescheduleRunnersForEntity(bean, s -> s.isEnabled() && s.getSchedule() != null, Integer.toString(bean.getId()), getExportRunnerProvider(), getRunnerHandler());
		}
	}


	private boolean isRescheduleNecessary(ReadOnlyDAO<ExportDefinition> dao, DaoEventTypes event, ExportDefinition bean) {
		// Reschedule for Inserts/Deletes only if the batch job is enabled.
		if (event.isInsert() || event.isDelete()) {
			return bean.isActive();
		}

		// Batch Job Updates
		ExportDefinition original = getOriginalBean(dao, bean);

		// Enable/Disable
		if (original.isActive() && !bean.isActive() && original.isSchedulePopulated()) {
			return true;
		}
		else if (!original.isActive() && bean.isActive() && bean.isSchedulePopulated()) {
			return true;
		}

		// Schedule Change
		if (!original.isSchedulePopulated()) {
			return bean.isSchedulePopulated();
		}
		if (!bean.isSchedulePopulated()) {
			return true;
		}
		return !original.getSchedule().equals(bean.getSchedule());
	}


	////////////////////////////////////////////////////////////////////////////
	/////////               Getter and Setter Methods                 //////////
	////////////////////////////////////////////////////////////////////////////


	public RunnerHandler getRunnerHandler() {
		return this.runnerHandler;
	}


	public void setRunnerHandler(RunnerHandler runnerHandler) {
		this.runnerHandler = runnerHandler;
	}


	public ExportRunnerProvider getExportRunnerProvider() {
		return this.exportRunnerProvider;
	}


	public void setExportRunnerProvider(ExportRunnerProvider exportRunnerProvider) {
		this.exportRunnerProvider = exportRunnerProvider;
	}


	public SystemConditionService getSystemConditionService() {
		return this.systemConditionService;
	}


	public void setSystemConditionService(SystemConditionService systemConditionService) {
		this.systemConditionService = systemConditionService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}
}
