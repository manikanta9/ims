package com.clifton.export.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


/**
 * @author theodorez
 */
public class ExportDefinitionDestinationExportDefinitionSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "referenceTwo.name,referenceOne.destinationName,referenceOne.destinationSystemBean.name,referenceOne.destinationSystemBean.type.name")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "referenceOne.id")
	private Integer exportDefinitionDestinationId;

	@SearchField(searchField = "referenceTwo.id")
	private Integer exportDefinitionId;

	@SearchField(searchField = "referenceOne.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] exportDefinitionDestinationIds;

	@SearchField(searchField = "referenceTwo.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] exportDefinitionIds;

	@SearchField(searchField = "referenceOne.disabled")
	private Boolean destinationDisabled;

	@SearchField(searchField = "referenceOne.sharedDestination")
	private Boolean sharedDestination;

	@SearchField(searchField = "referenceTwo.name")
	private String definitionName;

	@SearchField(searchField = "referenceOne.destinationName,referenceOne.destinationSystemBean.name")
	private String destinationNameOrBeanName;

	@SearchField(searchField = "referenceOne.description")
	private String destinationDescription;

	@SearchField(searchField = "referenceOne.destinationSystemBean.type.id")
	private Integer destinationTypeId;

	@SearchField(searchField = "referenceOne.entityModifyCondition.name")
	private String destinationModifyConditionName;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getExportDefinitionDestinationId() {
		return this.exportDefinitionDestinationId;
	}


	public void setExportDefinitionDestinationId(Integer exportDefinitionDestinationId) {
		this.exportDefinitionDestinationId = exportDefinitionDestinationId;
	}


	public Integer getExportDefinitionId() {
		return this.exportDefinitionId;
	}


	public void setExportDefinitionId(Integer exportDefinitionId) {
		this.exportDefinitionId = exportDefinitionId;
	}


	public Integer[] getExportDefinitionDestinationIds() {
		return this.exportDefinitionDestinationIds;
	}


	public void setExportDefinitionDestinationIds(Integer[] exportDefinitionDestinationIds) {
		this.exportDefinitionDestinationIds = exportDefinitionDestinationIds;
	}


	public Integer[] getExportDefinitionIds() {
		return this.exportDefinitionIds;
	}


	public void setExportDefinitionIds(Integer[] exportDefinitionIds) {
		this.exportDefinitionIds = exportDefinitionIds;
	}


	public Boolean getDestinationDisabled() {
		return this.destinationDisabled;
	}


	public void setDestinationDisabled(Boolean destinationDisabled) {
		this.destinationDisabled = destinationDisabled;
	}


	public Boolean getSharedDestination() {
		return this.sharedDestination;
	}


	public void setSharedDestination(Boolean sharedDestination) {
		this.sharedDestination = sharedDestination;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public String getDestinationNameOrBeanName() {
		return this.destinationNameOrBeanName;
	}


	public void setDestinationNameOrBeanName(String destinationNameOrBeanName) {
		this.destinationNameOrBeanName = destinationNameOrBeanName;
	}


	public String getDestinationDescription() {
		return this.destinationDescription;
	}


	public void setDestinationDescription(String destinationDescription) {
		this.destinationDescription = destinationDescription;
	}


	public Integer getDestinationTypeId() {
		return this.destinationTypeId;
	}


	public void setDestinationTypeId(Integer destinationTypeId) {
		this.destinationTypeId = destinationTypeId;
	}


	public String getDestinationModifyConditionName() {
		return this.destinationModifyConditionName;
	}


	public void setDestinationModifyConditionName(String destinationModifyConditionName) {
		this.destinationModifyConditionName = destinationModifyConditionName;
	}
}
