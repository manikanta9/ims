package com.clifton.export.definition;

import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.system.usedby.softlink.SoftLinkField;


/**
 * The <code>ExportDefinitionContent</code> class stores a history of entities that have been sent in prior exports.
 * This is used in cases where we need to track what was already sent to avoid sending the same data more than once.
 *
 * @author mwacker
 */
public class ExportDefinitionContentHistory extends BaseSimpleEntity<Long> {

	private ExportDefinitionContent exportDefinitionContent;

	private ExportRunHistory exportRunHistory;

	@SoftLinkField(tableBeanPropertyName = "exportDefinitionContent.historySystemTable", propertyDataType = DataTypes.NAME_LONG)
	private String historyPKFieldId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionContent getExportDefinitionContent() {
		return this.exportDefinitionContent;
	}


	public void setExportDefinitionContent(ExportDefinitionContent exportDefinitionContent) {
		this.exportDefinitionContent = exportDefinitionContent;
	}


	public ExportRunHistory getExportRunHistory() {
		return this.exportRunHistory;
	}


	public void setExportRunHistory(ExportRunHistory exportRunHistory) {
		this.exportRunHistory = exportRunHistory;
	}


	public String getHistoryPKFieldId() {
		return this.historyPKFieldId;
	}


	public void setHistoryPKFieldId(String historyPKFieldId) {
		this.historyPKFieldId = historyPKFieldId;
	}
}
