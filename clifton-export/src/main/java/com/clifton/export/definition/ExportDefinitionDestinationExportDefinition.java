package com.clifton.export.definition;

import com.clifton.core.beans.ManyToManyEntity;


/**
 * Many-to-many link between Export Definition Destinations and Export Definitions.
 * <p>
 * A destination may be global and assigned to many definitions, or it may not be global and only assigned to one definition.
 *
 * @author theodorez
 */
public class ExportDefinitionDestinationExportDefinition extends ManyToManyEntity<ExportDefinitionDestination, ExportDefinition, Integer> {

	//Nothing here
}
