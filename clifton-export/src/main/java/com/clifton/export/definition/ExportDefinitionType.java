package com.clifton.export.definition;


import com.clifton.core.beans.NamedEntity;
import com.clifton.system.condition.SystemCondition;


/**
 * The <code>ExportDefinitionType</code> class allows classification of ExportDefinition.
 * For example, each department can have its own export definitions.
 * <p/>
 * We can also optionally secure ExportDefinition entries for a specific type:
 * only Operations staff can add/update operations export definitions, etc.
 *
 * @author vgomelsky
 */
public class ExportDefinitionType extends NamedEntity<Short> {

	/**
	 * For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit definitions of this type.
	 */
	private SystemCondition entityModifyCondition;

	private String emailSignature;

	private SystemCondition sendCondition;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public SystemCondition getEntityModifyCondition() {
		return this.entityModifyCondition;
	}


	public void setEntityModifyCondition(SystemCondition entityModifyCondition) {
		this.entityModifyCondition = entityModifyCondition;
	}


	public String getEmailSignature() {
		return this.emailSignature;
	}


	public void setEmailSignature(String emailSignature) {
		this.emailSignature = emailSignature;
	}


	public SystemCondition getSendCondition() {
		return this.sendCondition;
	}


	public void setSendCondition(SystemCondition sendCondition) {
		this.sendCondition = sendCondition;
	}
}
