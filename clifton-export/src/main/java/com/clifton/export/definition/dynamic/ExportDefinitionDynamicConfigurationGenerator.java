package com.clifton.export.definition.dynamic;

import com.clifton.export.definition.ExportDefinition;

import java.util.List;
import java.util.Map;


/**
 * @author theodorez
 */
public interface ExportDefinitionDynamicConfigurationGenerator {

	public List<Map<String, Object>> getConfigurationList(ExportDefinition exportDefinition);


	public default String getConfigurationLabel() {
		return "Export Definition Dynamic Configuration Generator";
	}
}
