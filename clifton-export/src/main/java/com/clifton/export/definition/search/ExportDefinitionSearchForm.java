package com.clifton.export.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntityDateRangeWithoutTimeSearchForm;


public class ExportDefinitionSearchForm extends BaseAuditableEntityDateRangeWithoutTimeSearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "name,description", sortField = "name")
	private String searchPattern;

	@SearchField
	private String name;

	@SearchField
	private String description;

	@SearchField(searchField = "type.id", sortField = "type.name")
	private Short typeId;

	@SearchField(searchField = "recurrenceSchedule.id", sortField = "recurrenceSchedule.name")
	private Integer recurrenceScheduleId;

	@SearchField(searchField = "status.id", sortField = "status.name")
	private Short statusId;

	@SearchField(searchField = "status.name", comparisonConditions = ComparisonConditions.IN)
	private String[] statusNameList;

	@SearchField(searchField = "destinationList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer destinationId;

	@SearchField
	private String archiveStrategyName;

	@SearchField
	private Boolean dataExpected;

	@SearchField
	private Integer maxExecutionTimeMillis;

	@SearchField(searchField = "ownerUser.id")
	private Short ownerUserId;

	@SearchField(searchField = "userName", searchFieldPath = "ownerUser")
	private String ownerUserName;

	@SearchField(searchField = "ownerGroup.id")
	private Short ownerGroupId;

	@SearchField(searchField = "name", searchFieldPath = "ownerGroup")
	private String ownerGroupName;

	@SearchField(searchField = "ownerUser.displayName,ownerUser.userName,ownerGroup.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String ownerUserOrGroupName;

	/**
	 * Customized search field that returns entities that have an ownerUserId that matches the ownedByUserId or a ownerGroupId that falls within the security groups assigned
	 * the user.
	 */
	private Short ownedByUserId;

	@SearchField(searchField = "dynamicConfigurationSystemBean.name")
	private String dynamicConfigurationBeanName;



	@SearchField(searchField = "supportUser.id")
	private Short supportUserId;

	@SearchField(searchField = "userName", searchFieldPath = "supportUser")
	private String supportUserName;

	@SearchField(searchField = "supportGroup.id")
	private Short supportGroupId;

	@SearchField(searchField = "name", searchFieldPath = "supportGroup")
	private String supportGroupName;

	@SearchField(searchField = "supportUser.displayName,supportUser.userName,supportGroup.name", leftJoin = true, searchFieldCustomType = SearchFieldCustomTypes.COALESCE)
	private String supportUserOrGroupName;

	/**
	 * Customized search field that returns entities that have an supportUserId that matches the ownedByUserId or a supportGroupId that falls within the security groups assigned
	 * the user.
	 */
	private Short supportedByUserId;

	@SearchField(searchField = "priority.id")
	private Short priorityId;

	@SearchField
	private Boolean failureMonitored;


	@SearchField
	private String originName;

	@SearchField
	private String destinationName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public String getName() {
		return this.name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public Short getTypeId() {
		return this.typeId;
	}


	public void setTypeId(Short typeId) {
		this.typeId = typeId;
	}


	public Integer getRecurrenceScheduleId() {
		return this.recurrenceScheduleId;
	}


	public void setRecurrenceScheduleId(Integer recurrenceScheduleId) {
		this.recurrenceScheduleId = recurrenceScheduleId;
	}


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Short getStatusId() {
		return this.statusId;
	}


	public void setStatusId(Short statusId) {
		this.statusId = statusId;
	}


	public String[] getStatusNameList() {
		return this.statusNameList;
	}


	public void setStatusNameList(String[] statusNameList) {
		this.statusNameList = statusNameList;
	}


	public Integer getDestinationId() {
		return this.destinationId;
	}


	public void setDestinationId(Integer destinationId) {
		this.destinationId = destinationId;
	}


	public String getArchiveStrategyName() {
		return this.archiveStrategyName;
	}


	public void setArchiveStrategyName(String archiveStrategyName) {
		this.archiveStrategyName = archiveStrategyName;
	}


	public Boolean getDataExpected() {
		return this.dataExpected;
	}


	public void setDataExpected(Boolean dataExpected) {
		this.dataExpected = dataExpected;
	}


	public Integer getMaxExecutionTimeMillis() {
		return this.maxExecutionTimeMillis;
	}


	public void setMaxExecutionTimeMillis(Integer maxExecutionTimeMillis) {
		this.maxExecutionTimeMillis = maxExecutionTimeMillis;
	}


	public Short getOwnerUserId() {
		return this.ownerUserId;
	}


	public void setOwnerUserId(Short ownerUserId) {
		this.ownerUserId = ownerUserId;
	}


	public String getOwnerUserName() {
		return this.ownerUserName;
	}


	public void setOwnerUserName(String ownerUserName) {
		this.ownerUserName = ownerUserName;
	}


	public Short getOwnerGroupId() {
		return this.ownerGroupId;
	}


	public void setOwnerGroupId(Short ownerGroupId) {
		this.ownerGroupId = ownerGroupId;
	}


	public String getOwnerGroupName() {
		return this.ownerGroupName;
	}


	public void setOwnerGroupName(String ownerGroupName) {
		this.ownerGroupName = ownerGroupName;
	}


	public String getOwnerUserOrGroupName() {
		return this.ownerUserOrGroupName;
	}


	public void setOwnerUserOrGroupName(String ownerUserOrGroupName) {
		this.ownerUserOrGroupName = ownerUserOrGroupName;
	}


	public Short getOwnedByUserId() {
		return this.ownedByUserId;
	}


	public void setOwnedByUserId(Short ownedByUserId) {
		this.ownedByUserId = ownedByUserId;
	}


	public String getDynamicConfigurationBeanName() {
		return this.dynamicConfigurationBeanName;
	}


	public void setDynamicConfigurationBeanName(String dynamicConfigurationBeanName) {
		this.dynamicConfigurationBeanName = dynamicConfigurationBeanName;
	}


	public Short getPriorityId() {
		return this.priorityId;
	}


	public void setPriorityId(Short priorityId) {
		this.priorityId = priorityId;
	}


	public Boolean getFailureMonitored() {
		return this.failureMonitored;
	}


	public void setFailureMonitored(Boolean failureMonitored) {
		this.failureMonitored = failureMonitored;
	}


	public Short getSupportUserId() {
		return this.supportUserId;
	}


	public void setSupportUserId(Short supportUserId) {
		this.supportUserId = supportUserId;
	}


	public String getSupportUserName() {
		return this.supportUserName;
	}


	public void setSupportUserName(String supportUserName) {
		this.supportUserName = supportUserName;
	}


	public Short getSupportGroupId() {
		return this.supportGroupId;
	}


	public void setSupportGroupId(Short supportGroupId) {
		this.supportGroupId = supportGroupId;
	}


	public String getSupportGroupName() {
		return this.supportGroupName;
	}


	public void setSupportGroupName(String supportGroupName) {
		this.supportGroupName = supportGroupName;
	}


	public String getSupportUserOrGroupName() {
		return this.supportUserOrGroupName;
	}


	public void setSupportUserOrGroupName(String supportUserOrGroupName) {
		this.supportUserOrGroupName = supportUserOrGroupName;
	}


	public Short getSupportedByUserId() {
		return this.supportedByUserId;
	}


	public void setSupportedByUserId(Short supportedByUserId) {
		this.supportedByUserId = supportedByUserId;
	}


	public String getOriginName() {
		return this.originName;
	}


	public void setOriginName(String originName) {
		this.originName = originName;
	}


	public String getDestinationName() {
		return this.destinationName;
	}


	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}
}
