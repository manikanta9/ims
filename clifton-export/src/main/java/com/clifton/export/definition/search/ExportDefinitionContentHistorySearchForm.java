package com.clifton.export.definition.search;

import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseEntitySearchForm;

import java.util.Date;


/**
 * @author mwacker
 */
public class ExportDefinitionContentHistorySearchForm extends BaseEntitySearchForm {

	@SearchField(searchField = "exportDefinitionContent.id")
	private Integer definitionContentId;

	@SearchField(searchField = "definition.id", searchFieldPath = "exportDefinitionContent", sortField = "definition.name")
	private Integer exportDefinitionId;

	@SearchField(searchField = "definition.name", searchFieldPath = "exportDefinitionContent")
	private String exportDefinitionName;

	@SearchField(searchField = "type.id", searchFieldPath = "exportDefinitionContent.definition")
	private Short exportDefinitionTypeId;

	@SearchField(searchField = "exportRunHistory.id")
	private Integer exportRunHistoryId;

	@SearchField(searchField = "exportRunHistory.status.id", comparisonConditions = ComparisonConditions.NOT_IN)
	private Short[] excludeRunHistoryStatusIds;

	@SearchField(searchField = "exportDefinitionContent.historySystemTable.id")
	private Short historySystemTableId;

	@SearchField(searchField = "exportDefinitionContent.historySystemTable.name")
	private String historySystemTableName;

	@SearchField(searchField = "historyPKFieldId")
	private String historyPKFieldId;

	@SearchField(searchField = "scheduledDate", searchFieldPath = "exportRunHistory")
	private Date scheduledDate;

	@SearchField(searchField = "scheduledDate", searchFieldPath = "exportRunHistory", comparisonConditions = ComparisonConditions.LESS_THAN)
	private Date beforeRunScheduleDate;

	@SearchField(searchField = "endDate", searchFieldPath = "exportRunHistory", comparisonConditions = ComparisonConditions.GREATER_THAN)
	private Date endDateAfter;


	@SearchField(searchField = "exportRunHistory.destination.destinationName")
	private String destinationName;

	@SearchField(searchField = "exportRunHistory.status.name")
	private String statusName;

	@SearchField(searchField = "exportRunHistory.exportRunSourceType")
	private String exportRunSourceType;

	/////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getDefinitionContentId() {
		return this.definitionContentId;
	}


	public void setDefinitionContentId(Integer definitionContentId) {
		this.definitionContentId = definitionContentId;
	}


	public Integer getExportDefinitionId() {
		return this.exportDefinitionId;
	}


	public void setExportDefinitionId(Integer exportDefinitionId) {
		this.exportDefinitionId = exportDefinitionId;
	}


	public String getExportDefinitionName() {
		return this.exportDefinitionName;
	}


	public void setExportDefinitionName(String exportDefinitionName) {
		this.exportDefinitionName = exportDefinitionName;
	}


	public Short getExportDefinitionTypeId() {
		return this.exportDefinitionTypeId;
	}


	public void setExportDefinitionTypeId(Short exportDefinitionTypeId) {
		this.exportDefinitionTypeId = exportDefinitionTypeId;
	}


	public Integer getExportRunHistoryId() {
		return this.exportRunHistoryId;
	}


	public void setExportRunHistoryId(Integer exportRunHistoryId) {
		this.exportRunHistoryId = exportRunHistoryId;
	}


	public Short[] getExcludeRunHistoryStatusIds() {
		return this.excludeRunHistoryStatusIds;
	}


	public void setExcludeRunHistoryStatusIds(Short[] excludeRunHistoryStatusIds) {
		this.excludeRunHistoryStatusIds = excludeRunHistoryStatusIds;
	}


	public Short getHistorySystemTableId() {
		return this.historySystemTableId;
	}


	public void setHistorySystemTableId(Short historySystemTableId) {
		this.historySystemTableId = historySystemTableId;
	}


	public String getHistorySystemTableName() {
		return this.historySystemTableName;
	}


	public void setHistorySystemTableName(String historySystemTableName) {
		this.historySystemTableName = historySystemTableName;
	}


	public String getHistoryPKFieldId() {
		return this.historyPKFieldId;
	}


	public void setHistoryPKFieldId(String historyPKFieldId) {
		this.historyPKFieldId = historyPKFieldId;
	}


	public Date getBeforeRunScheduleDate() {
		return this.beforeRunScheduleDate;
	}


	public void setBeforeRunScheduleDate(Date beforeRunScheduleDate) {
		this.beforeRunScheduleDate = beforeRunScheduleDate;
	}


	public Date getEndDateAfter() {
		return this.endDateAfter;
	}


	public void setEndDateAfter(Date endDateAfter) {
		this.endDateAfter = endDateAfter;
	}


	public Date getScheduledDate() {
		return this.scheduledDate;
	}


	public void setScheduledDate(Date scheduledDate) {
		this.scheduledDate = scheduledDate;
	}


	public String getDestinationName() {
		return this.destinationName;
	}


	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}


	public String getStatusName() {
		return this.statusName;
	}


	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}


	public String getExportRunSourceType() {
		return this.exportRunSourceType;
	}


	public void setExportRunSourceType(String exportRunSourceType) {
		this.exportRunSourceType = exportRunSourceType;
	}
}
