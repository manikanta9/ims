package com.clifton.export.definition;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.DaoUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchFormConfigurer;
import com.clifton.core.dataaccess.search.hibernate.expression.ActiveExpressionForDates;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.content.ExportContentGenerator;
import com.clifton.export.context.ExportContextGenerator;
import com.clifton.export.definition.dynamic.ExportDefinitionDynamicConfigurationGenerator;
import com.clifton.export.definition.search.ExportDefinitionContentHistorySearchForm;
import com.clifton.export.definition.search.ExportDefinitionContentSearchForm;
import com.clifton.export.definition.search.ExportDefinitionDestinationExportDefinitionSearchForm;
import com.clifton.export.definition.search.ExportDefinitionDestinationSearchForm;
import com.clifton.export.definition.search.ExportDefinitionSearchForm;
import com.clifton.export.definition.search.ExportDefinitionTypeSearchForm;
import com.clifton.export.destination.ExportDestinationGenerator;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunService;
import com.clifton.export.run.ExportRunStatus;
import com.clifton.export.run.ExportRunStatus.ExportRunStatusNames;
import com.clifton.export.run.search.ExportRunHistorySearchForm;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.condition.SystemCondition;
import com.clifton.system.condition.evaluator.EvaluationResult;
import com.clifton.system.condition.evaluator.SystemConditionEvaluationHandler;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareObserver;
import org.hibernate.Criteria;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;


@Service
public class ExportDefinitionServiceImpl implements ExportDefinitionService {

	private AdvancedUpdatableDAO<ExportDefinition, Criteria> exportDefinitionDAO;
	private AdvancedUpdatableDAO<ExportDefinitionContent, Criteria> exportDefinitionContentDAO;
	private AdvancedUpdatableDAO<ExportDefinitionContentHistory, Criteria> exportDefinitionContentHistoryDAO;
	private AdvancedUpdatableDAO<ExportDefinitionDestination, Criteria> exportDefinitionDestinationDAO;
	private AdvancedUpdatableDAO<ExportDefinitionDestinationExportDefinition, Criteria> exportDefinitionDestinationExportDefinitionDAO;
	private AdvancedUpdatableDAO<ExportDefinitionType, Criteria> exportDefinitionTypeDAO;

	private SystemBeanService systemBeanService;
	private ExportRunService exportRunService;
	private SystemConditionEvaluationHandler systemConditionEvaluationHandler;
	private SecuritySecretService securitySecretService;
	private SecurityUserService securityUserService;

	////////////////////////////////////////////////////////////////////////////
	//////           Export Definition Business Methods                   //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportDefinition getExportDefinition(int id) {
		return getExportDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public ExportDefinition getExportDefinitionPopulated(int id) {
		ExportDefinition result = getExportDefinition(id);
		if (result != null) {
			ExportDefinitionDestinationSearchForm searchForm = new ExportDefinitionDestinationSearchForm();
			searchForm.setDisabled(false);
			searchForm.setDefinitionId(id);
			//Get all of the destinations off of the m2m objects
			result.setDestinationList(getExportDefinitionDestinationList(searchForm));

			ExportDefinitionContentSearchForm csf = new ExportDefinitionContentSearchForm();
			csf.setDefinitionId(result.getId());
			result.setContentList(getExportDefinitionContentList(csf));

			SystemBean configurationBean = result.getDynamicConfigurationSystemBean();
			if (configurationBean != null) {
				configurationBean.setPropertyList(getSystemBeanService().getSystemBeanPropertyListByBeanId(configurationBean.getId()));
			}
		}
		return result;
	}


	@Override
	public ExportRunHistory getExportDefinitionRunLastHistory(int id) {
		ExportRunHistorySearchForm searchForm = new ExportRunHistorySearchForm();
		searchForm.setDefinitionId(id);
		searchForm.setOrderBy("startDate:desc");
		searchForm.setLimit(1);

		return CollectionUtils.getFirstElement(getExportRunService().getExportRunHistoryList(searchForm));
	}


	@Override
	public List<ExportDefinition> getExportDefinitionListActiveOnDate(final Date date) {
		HibernateSearchConfigurer searchConfigurer = criteria -> {
			Date startDate = DateUtils.clearTime(date);
			criteria.createCriteria("recurrenceSchedule", "s")
					.add(Restrictions.le("s.startDate", startDate))
					.add(Restrictions.or(Restrictions.isNull("s.endDate"), Restrictions.ge("s.endDate", startDate)));
		};

		return getExportDefinitionDAO().findBySearchCriteria(searchConfigurer);
	}


	@Override
	public List<ExportDefinition> getExportDefinitionList(ExportDefinitionSearchForm searchForm) {

		HibernateSearchFormConfigurer exportDefinitionListSearchFormConfigurer = new HibernateSearchFormConfigurer(searchForm) {
			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				ExportDefinitionSearchForm searchForm = (ExportDefinitionSearchForm) getSortableSearchForm();

				if (searchForm.getOwnedByUserId() != null) {
					List<Short> userGroupIdList = BeanUtils.getBeanIdentityList(getSecurityUserService().getSecurityGroupListByUser(searchForm.getOwnedByUserId()));
					criteria.add(Restrictions.or(Restrictions.eq("ownerUser.id", searchForm.getOwnedByUserId()), Restrictions.in("ownerGroup.id", userGroupIdList)));
				}

				if (searchForm.getSupportedByUserId() != null) {
					List<Short> userGroupIdList = BeanUtils.getBeanIdentityList(getSecurityUserService().getSecurityGroupListByUser(searchForm.getSupportedByUserId()));
					criteria.add(Restrictions.or(Restrictions.eq("supportUser.id", searchForm.getSupportedByUserId()), Restrictions.in("supportGroup.id", userGroupIdList)));
				}
			}
		};

		return getExportDefinitionDAO().findBySearchCriteria(exportDefinitionListSearchFormConfigurer);
	}


	@Override
	public ExportDefinition saveExportDefinition(ExportDefinition bean) {
		if (bean.isNewBean()) {
			// assign the initial default status
			bean.setStatus(getExportRunService().getExportRunStatusByName(ExportRunStatusNames.NONE));
		}

		if (bean.isZipFilesForExport() && StringUtils.isEmpty(bean.getZippedFileNameTemplate())) {
			throw new ValidationException("'Zipped File Name Template' must be specified if 'Zip Export Files' is enabled.");
		}

		if (bean.getRetryCount() != null || bean.getRetryDelayInSeconds() != null) {
			ValidationUtils.assertTrue(bean.getRetryCount() != null && bean.getRetryDelayInSeconds() != null, "The retry count and retry delay must both be specified or not at all");
			ValidationUtils.assertTrue((1 <= bean.getRetryCount()) && (bean.getRetryCount() <= 100), "The retry count must be between 1 and 100");
			ValidationUtils.assertTrue((60 <= bean.getRetryDelayInSeconds()) && (bean.getRetryDelayInSeconds() <= 14400), "The retry delay must be between 60 and 14400 seconds (4 hours)");
		}

		// save the bean first: use system generated but user friendly bean name
		SystemBean dynamicConfigurationBean = bean.getDynamicConfigurationSystemBean();
		if (dynamicConfigurationBean != null) {
			boolean newBean = dynamicConfigurationBean.isNewBean();
			if (!newBean && CollectionUtils.isEmpty(dynamicConfigurationBean.getPropertyList())) {
				dynamicConfigurationBean = getSystemBeanService().getSystemBean(dynamicConfigurationBean.getId());
			}
			ExportDefinitionDynamicConfigurationGenerator configurationGenerator = (ExportDefinitionDynamicConfigurationGenerator) getSystemBeanService().getBeanInstance(dynamicConfigurationBean, dynamicConfigurationBean.getPropertyList());
			if (newBean) {
				dynamicConfigurationBean.setName(StringUtils.formatStringUpToNCharsWithDots(configurationGenerator.getConfigurationLabel(), DataTypes.NAME_LONG.getLength(), true));
			}
			else {
				dynamicConfigurationBean.setAutoGeneratedName(configurationGenerator.getConfigurationLabel());
			}
			getSystemBeanService().saveSystemBean(dynamicConfigurationBean);
			if (newBean) {
				// didn't have id on create so need to rename
				getSystemBeanService().renameSystemBean(dynamicConfigurationBean.getId(), dynamicConfigurationBean.getAutoGeneratedName(configurationGenerator.getConfigurationLabel()));
			}
		}

		return getExportDefinitionDAO().save(bean);
	}


	/**
	 * Updates just the status of the export definition.  In case other properties are updated by a user while a job is running, this will prevent
	 * any conflicts for versioning
	 * <p>
	 */
	@Override
	public ExportDefinition updateExportDefinitionStatus(int id, ExportRunStatus status) {
		ExportDefinition definition = getExportDefinition(id);
		definition.setStatus(status);
		return DaoUtils.executeWithSpecificObserversDisabled(() -> getExportDefinitionDAO().save(definition), SystemEntityModifyConditionAwareObserver.class);
	}


	@Override
	@Transactional
	public void deleteExportDefinitionAndRunHistory(int id) {
		ExportDefinition definition = getExportDefinition(id);

		// only all deleting the export if there is no history
		ExportRunHistorySearchForm historySearchForm = new ExportRunHistorySearchForm();
		historySearchForm.setDefinitionId(id);

		List<ExportRunHistory> historyList = getExportRunService().getExportRunHistoryList(historySearchForm);
		ValidationUtils.assertEmpty(historyList, "Cannot delete [" + definition.getLabel() + "] because there are [" + historyList.size() + "] history entries.");

		ExportDefinitionDestinationSearchForm destinationSearchForm = new ExportDefinitionDestinationSearchForm();
		destinationSearchForm.setDefinitionId(id);
		List<ExportDefinitionDestination> destinationList = getExportDefinitionDestinationList(destinationSearchForm);

		ExportDefinitionContentSearchForm contentSearchForm = new ExportDefinitionContentSearchForm();
		contentSearchForm.setDefinitionId(id);
		List<ExportDefinitionContent> contentList = getExportDefinitionContentList(contentSearchForm);

		getExportDefinitionDestinationDAO().deleteList(destinationList);
		getExportDefinitionContentDAO().deleteList(contentList);

		deleteExportDefinition(id);
	}


	@Override
	public void deleteExportDefinition(int id) {
		ExportDefinition definition = getExportDefinitionDAO().findByPrimaryKey(id);
		if (definition.getDynamicConfigurationSystemBean() != null) {
			getSystemBeanService().deleteSystemBean(definition.getDynamicConfigurationSystemBean().getId());
		}
		getExportDefinitionDAO().delete(id);
	}


	@Override
	public void validateDefinitionSendable(ExportDefinition definition) {
		ExportDefinitionType type = definition.getType();
		ValidationUtils.assertNotNull(type.getSendCondition(), "No send condition set on the Export Definition " + definition.getLabel());

		//Validate the send condition on the type
		EvaluationResult typeSendableResult = getSystemConditionEvaluationHandler().evaluateCondition(type.getSendCondition(), type);
		ValidationUtils.assertTrue(typeSendableResult.isResult(), "Export Definition Type Send Condition did not evaluate to True: \n" + typeSendableResult.getMessage());

		//Validate the send condition on the definition (using the condition from the type)
		EvaluationResult definitionSendableResult = getSystemConditionEvaluationHandler().evaluateCondition(type.getSendCondition(), definition);
		ValidationUtils.assertTrue(definitionSendableResult.isResult(), "Export Definition Send Condition did not evaluate to True: \n" + definitionSendableResult.getMessage());

		for (ExportDefinitionDestination destination : CollectionUtils.getIterable(definition.getDestinationList())) {
			SystemCondition sendCondition = destination.getSendCondition();
			if (sendCondition != null) {
				EvaluationResult destinationSendableResult = getSystemConditionEvaluationHandler().evaluateCondition(destination.getSendCondition(), destination);
				ValidationUtils.assertTrue(destinationSendableResult.isResult(), "Export Definition Destination Send Condition did not evaluate to True: \n" + destinationSendableResult.getMessage());
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////        Export Definition Destination Business Methods          //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportDefinitionDestination getExportDefinitionDestination(int id) {
		ExportDefinitionDestination result = getExportDefinitionDestinationDAO().findByPrimaryKey(id);
		if (result != null) {
			SystemBean bean = result.getDestinationSystemBean();
			if (bean != null) {
				bean.setPropertyList(getSystemBeanService().getSystemBeanPropertyListByBeanId(bean.getId()));
			}

			if (!result.isSharedDestination()) {
				//populate the definition attribute on the destination
				ExportDefinitionDestinationExportDefinitionSearchForm searchForm = new ExportDefinitionDestinationExportDefinitionSearchForm();
				searchForm.setSharedDestination(false);
				searchForm.setExportDefinitionDestinationId(result.getId());
				ExportDefinitionDestinationExportDefinition exportDefinitionDestinationExportDefinition = CollectionUtils.getOnlyElement(getExportDefinitionDestinationExportDefinitionList(searchForm));

				if (exportDefinitionDestinationExportDefinition != null) {
					result.setDefinition(exportDefinitionDestinationExportDefinition.getReferenceTwo());
				}
			}
		}
		return result;
	}


	@Override
	public List<ExportDefinitionDestination> getExportDefinitionDestinationList(ExportDefinitionDestinationSearchForm searchForm) {
		return getExportDefinitionDestinationDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	@Transactional
	public ExportDefinitionDestination saveExportDefinitionDestination(ExportDefinitionDestination destination) {
		if (destination.isSharedDestination()) {
			ValidationUtils.assertTrue(!StringUtils.isEmpty(destination.getDestinationName()), "Destination Name must be populated for Shared Destinations");
			ValidationUtils.assertTrue(destination.getSendCondition() != null, "A Send Condition is required for Shared Destinations.");
		}
		else {
			ValidationUtils.assertTrue(destination.getDefinition() != null, "Destinations that are not shared must be assigned to a definition.");
		}

		// save the bean first: use system generated but user friendly bean name
		SystemBean bean = destination.getDestinationSystemBean();
		if (bean != null) {
			ExportDestinationGenerator destinationGenerator = (ExportDestinationGenerator) getSystemBeanService().getBeanInstance(bean, bean.getPropertyList());
			boolean newBean = bean.isNewBean();
			if (newBean) {
				bean.setName(StringUtils.formatStringUpToNCharsWithDots(destinationGenerator.getDestinationLabel(), DataTypes.NAME_LONG.getLength(), true));
			}
			else {
				bean.setAutoGeneratedName(destinationGenerator.getDestinationLabel());
			}
			getSystemBeanService().saveSystemBean(bean);
			if (newBean) {
				// didn't have id on create so need to rename
				getSystemBeanService().renameSystemBean(bean.getId(), bean.getAutoGeneratedName(destinationGenerator.getDestinationLabel()));
			}
		}
		int definitionId = destination.getDefinition() != null ? destination.getDefinition().getId() : 0;
		destination = getExportDefinitionDestinationDAO().save(destination);
		createExportDefinitionDestinationExportDefinitionLink(destination, definitionId);
		return destination;
	}


	@Override
	@Transactional
	public void deleteExportDefinitionDestination(int id) {
		SystemBean bean = getExportDefinitionDestination(id).getDestinationSystemBean();
		getExportDefinitionDestinationDAO().delete(id);
		getSystemBeanService().deleteSystemBean(bean.getId());
	}


	@Override
	@Transactional
	public void saveExportDefinitionDestinationLink(int destinationId, int definitionId) {
		ExportDefinitionDestination destination = getExportDefinitionDestination(destinationId);
		if (definitionId > 0) {
			//delete the specific destination & definition m2m
			createExportDefinitionDestinationExportDefinitionLink(destination, definitionId);
		}
	}


	@Override
	@Transactional
	public void deleteExportDefinitionDestinationAndLink(int destinationId, int definitionId) {
		ExportDefinitionDestination destination = getExportDefinitionDestination(destinationId);
		if (definitionId > 0) {
			//delete the specific destination & definition m2m
			deleteExportDefinitionDestinationExportDefinitionLink(destination, definitionId);

			if (!destination.isSharedDestination()) {
				deleteExportDefinitionDestination(destinationId);
			}
		}
	}


	@Override
	@Transactional
	public void deleteExportDefinitionDestinationCommon(int id) {
		ExportDefinitionDestinationExportDefinitionSearchForm destinationDefinitionSearchForm = new ExportDefinitionDestinationExportDefinitionSearchForm();
		destinationDefinitionSearchForm.setExportDefinitionDestinationId(id);
		List<ExportDefinitionDestinationExportDefinition> exportDefinitionDestinationExportDefinitionList = getExportDefinitionDestinationExportDefinitionList(destinationDefinitionSearchForm);
		//Delete the many to many entries for the destination
		for (ExportDefinitionDestinationExportDefinition exportDefinitionDestinationExportDefinition : exportDefinitionDestinationExportDefinitionList) {
			deleteExportDefinitionDestinationExportDefinitionLink(exportDefinitionDestinationExportDefinition.getReferenceOne(), exportDefinitionDestinationExportDefinition.getReferenceTwo().getId());
		}

		deleteExportDefinitionDestination(id);
	}


	////////////////////////////////////////////////////////////////////////////////
	//////        Export Definition Destination Many-To-Many Methods          //////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportDefinitionDestinationExportDefinition getExportDefinitionDestinationExportDefinition(int id) {
		return getExportDefinitionDestinationExportDefinitionDAO().findByPrimaryKey(id);
	}


	@Override
	public List<ExportDefinitionDestinationExportDefinition> getExportDefinitionDestinationExportDefinitionList(ExportDefinitionDestinationExportDefinitionSearchForm searchForm) {
		return getExportDefinitionDestinationExportDefinitionDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ExportDefinitionDestinationExportDefinition saveExportDefinitionDestinationExportDefinition(ExportDefinitionDestinationExportDefinition bean) {
		return getExportDefinitionDestinationExportDefinitionDAO().save(bean);
	}


	@Override
	public void deleteExportDefinitionDestinationExportDefinition(int id) {
		getExportDefinitionDestinationExportDefinitionDAO().delete(id);
	}


	private void createExportDefinitionDestinationExportDefinitionLink(ExportDefinitionDestination destination, int definitionId) {
		if (definitionId != 0) {
			//Get the m2m's given the destination and definition
			ExportDefinitionDestinationExportDefinitionSearchForm searchForm = new ExportDefinitionDestinationExportDefinitionSearchForm();
			searchForm.setExportDefinitionDestinationId(destination.getId());
			searchForm.setExportDefinitionId(definitionId);
			//There can never be more than one m2m existing for a given combination of destination and definition
			ExportDefinitionDestinationExportDefinition definitionLink = CollectionUtils.getOnlyElement(getExportDefinitionDestinationExportDefinitionList(searchForm));
			//If there is no existing link, create a new one
			if (definitionLink == null) {
				definitionLink = new ExportDefinitionDestinationExportDefinition();
				definitionLink.setReferenceOne(destination);
				definitionLink.setReferenceTwo(getExportDefinition(definitionId));
				saveExportDefinitionDestinationExportDefinition(definitionLink);
			}
			else if (!destination.isSharedDestination()) {
				// even though there was no change, update non-shared destination links anyway
				// first, it will update record stamp making it easier to see that there was a change
				// second, it will allow us to lock changes via workflow task (the task can only be associated with the link table)
				saveExportDefinitionDestinationExportDefinition(definitionLink);
			}
		}
	}


	private void deleteExportDefinitionDestinationExportDefinitionLink(ExportDefinitionDestination destination, int definitionId) {
		if (definitionId != 0) {
			ExportDefinitionDestinationExportDefinitionSearchForm searchForm = new ExportDefinitionDestinationExportDefinitionSearchForm();
			searchForm.setExportDefinitionDestinationId(destination.getId());
			searchForm.setExportDefinitionId(definitionId);

			ExportDefinitionDestinationExportDefinition definitionLink = CollectionUtils.getOnlyElement(getExportDefinitionDestinationExportDefinitionList(searchForm));
			ValidationUtils.assertNotNull(definitionLink, "No link found for destination: " + destination.getDestinationName() + "[" + destination.getId() + "]  and definition[" + definitionId + "]");
			deleteExportDefinitionDestinationExportDefinition(definitionLink.getId());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////        Export Definition Content Business Methods          ////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportDefinitionContent getExportDefinitionContent(int id) {
		ExportDefinitionContent result = getExportDefinitionContentDAO().findByPrimaryKey(id);
		if (result != null) {
			SystemBean contentBean = result.getContentSystemBean();
			if (contentBean != null) {
				contentBean.setPropertyList(getSystemBeanService().getSystemBeanPropertyListByBean(contentBean));
			}
			SystemBean contextBean = result.getContextSystemBean();
			if (contextBean != null) {
				contextBean.setPropertyList(getSystemBeanService().getSystemBeanPropertyListByBean(contextBean));
			}
		}
		return result;
	}


	@Override
	public List<ExportDefinitionContent> getExportDefinitionContentList(ExportDefinitionContentSearchForm searchForm) {

		HibernateSearchFormConfigurer config = new HibernateSearchFormConfigurer(searchForm) {

			@Override
			public void configureCriteria(Criteria criteria) {
				super.configureCriteria(criteria);

				if (searchForm.getActiveDefinition() != null) {
					String alias = getPathAlias("definition", criteria);
					criteria.add(ActiveExpressionForDates.forActiveWithAlias(searchForm.getActiveDefinition(), alias));
				}
			}
		};

		return getExportDefinitionContentDAO().findBySearchCriteria(config);
	}


	@Override
	@Transactional
	@SuppressWarnings("rawtypes")
	public ExportDefinitionContent saveExportDefinitionContent(ExportDefinitionContent content) {
		validateExportDefinitionContent(content);

		content.setEncryptionSecret(getSecuritySecretService().saveSecuritySecret(content.getEncryptionSecret()));
		// save the bean first: use system generated but user friendly bean name
		SystemBean contentBean = content.getContentSystemBean();
		if (contentBean != null) {
			boolean newBean = contentBean.isNewBean();
			if (!newBean && CollectionUtils.isEmpty(contentBean.getPropertyList())) {
				contentBean = getSystemBeanService().getSystemBean(contentBean.getId());
			}
			ExportContentGenerator contentGenerator = (ExportContentGenerator) getSystemBeanService().getBeanInstance(contentBean, contentBean.getPropertyList());
			if (newBean) {
				contentBean.setName(StringUtils.formatStringUpToNCharsWithDots(contentGenerator.getContentLabel(), DataTypes.NAME_LONG.getLength(), true));
			}
			else {
				contentBean.setAutoGeneratedName(contentGenerator.getContentLabel());
			}
			getSystemBeanService().saveSystemBean(contentBean);
			if (newBean) {
				// didn't have id on create so need to rename
				getSystemBeanService().renameSystemBean(contentBean.getId(), contentBean.getAutoGeneratedName(contentGenerator.getContentLabel()));
			}
		}

		SystemBean contextBean = content.getContextSystemBean();
		if (contextBean != null) {
			boolean newBean = contextBean.isNewBean();
			if (!newBean && CollectionUtils.isEmpty(contextBean.getPropertyList())) {
				contextBean = getSystemBeanService().getSystemBean(contextBean.getId());
			}
			ExportContextGenerator contextGenerator = (ExportContextGenerator) getSystemBeanService().getBeanInstance(contextBean, contextBean.getPropertyList());
			if (newBean) {
				contextBean.setName(StringUtils.formatStringUpToNCharsWithDots(contextGenerator.getContextLabel(), DataTypes.NAME_LONG.getLength(), true));
			}
			else {
				contextBean.setAutoGeneratedName(contextGenerator.getContextLabel());
			}
			getSystemBeanService().saveSystemBean(contextBean);
			if (newBean) {
				// didn't have id on create so need to rename
				getSystemBeanService().renameSystemBean(contextBean.getId(), contextBean.getAutoGeneratedName(contextGenerator.getContextLabel()));
			}
		}
		return getExportDefinitionContentDAO().save(content);
	}


	@Override
	public void deleteExportDefinitionContent(int id) {
		ExportDefinitionContent content = getExportDefinitionContent(id);
		SystemBean contentBean = content.getContentSystemBean();
		SystemBean contextBean = content.getContextSystemBean();
		getExportDefinitionContentDAO().delete(id);
		if (contentBean != null) {
			getSystemBeanService().deleteSystemBean(contentBean.getId());
		}
		if (contextBean != null) {
			getSystemBeanService().deleteSystemBean(contextBean.getId());
		}
	}


	private void validateExportDefinitionContent(ExportDefinitionContent content) {
		if (FileUtils.getFileExtension(content.getFileNameTemplate()) == null) {
			throw new FieldValidationException("The file name must include a file extension at the end (e.g. 'my_file.pdf' rather than just 'my_file')", "fileNameTemplate");
		}

		if (content.getHistorySystemTable() != null) {
			ValidationUtils.assertFalse(StringUtils.isEmpty(content.getHistoryPKColumnName()), "History column name is required when a history table is selected.", "historyPKColumnName");
		}
		if (!StringUtils.isEmpty(content.getHistoryPKColumnName())) {
			ValidationUtils.assertTrue(content.getHistorySystemTable() != null, "History table is required when a history column name is populated.", "historySystemTable");
		}
		if (content.getHistoryDaysToInclude() != null) {
			ValidationUtils.assertTrue(content.getHistoryDaysToInclude().intValue() >= 0, "Days to Include from history must be greater than or equal to 0.", "historyDaysToInclude");
		}
		if (content.getHistoryDaysToKeep() != null) {
			ValidationUtils.assertTrue(content.getHistoryDaysToKeep().intValue() > 0, "Days to Keep from history must be greater than 0.", "historyDaysToKeep");
		}
	}

	////////////////////////////////////////////////////////////////////////////
	//////          Export Content Business Methods History               //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<ExportDefinitionContentHistory> getExportDefinitionContentHistoryList(ExportDefinitionContentHistorySearchForm searchForm) {
		return getExportDefinitionContentHistoryDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ExportDefinitionContentHistory saveExportDefinitionContentHistory(ExportDefinitionContentHistory contentHistory) {
		return getExportDefinitionContentHistoryDAO().save(contentHistory);
	}


	@Override
	public void saveExportDefinitionContentHistoryList(List<ExportDefinitionContentHistory> contentHistoryList) {
		getExportDefinitionContentHistoryDAO().saveList(contentHistoryList);
	}

	////////////////////////////////////////////////////////////////////////////
	//////           Export Definition Type Business Methods              //////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public ExportDefinitionType getExportDefinitionType(short id) {
		return getExportDefinitionTypeDAO().findByPrimaryKey(id);
	}


	@Override
	public ExportDefinitionType getExportDefinitionTypeByName(String name) {
		return getExportDefinitionTypeDAO().findOneByField("name", name);
	}


	@Override
	public List<ExportDefinitionType> getExportDefinitionTypeList(ExportDefinitionTypeSearchForm searchForm) {
		return getExportDefinitionTypeDAO().findBySearchCriteria(new HibernateSearchFormConfigurer(searchForm));
	}


	@Override
	public ExportDefinitionType saveExportDefinitionType(ExportDefinitionType bean) {
		ValidationUtils.assertNotNull(bean.getSendCondition(), "Export Definition Types are required to have a Send Condition");
		return getExportDefinitionTypeDAO().save(bean);
	}


	@Override
	public void deleteExportDefinitionType(short id) {
		getExportDefinitionTypeDAO().delete(id);
	}


	////////////////////////////////////////////////////////////////////////////
	//////                  Getters and Setters                           //////
	////////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<ExportDefinition, Criteria> getExportDefinitionDAO() {
		return this.exportDefinitionDAO;
	}


	public void setExportDefinitionDAO(AdvancedUpdatableDAO<ExportDefinition, Criteria> exportDefinitionDAO) {
		this.exportDefinitionDAO = exportDefinitionDAO;
	}


	public AdvancedUpdatableDAO<ExportDefinitionDestination, Criteria> getExportDefinitionDestinationDAO() {
		return this.exportDefinitionDestinationDAO;
	}


	public void setExportDefinitionDestinationDAO(AdvancedUpdatableDAO<ExportDefinitionDestination, Criteria> exportDefinitionDestinationDAO) {
		this.exportDefinitionDestinationDAO = exportDefinitionDestinationDAO;
	}


	public AdvancedUpdatableDAO<ExportDefinitionDestinationExportDefinition, Criteria> getExportDefinitionDestinationExportDefinitionDAO() {
		return this.exportDefinitionDestinationExportDefinitionDAO;
	}


	public void setExportDefinitionDestinationExportDefinitionDAO(AdvancedUpdatableDAO<ExportDefinitionDestinationExportDefinition, Criteria> exportDefinitionDestinationExportDefinitionDAO) {
		this.exportDefinitionDestinationExportDefinitionDAO = exportDefinitionDestinationExportDefinitionDAO;
	}


	public AdvancedUpdatableDAO<ExportDefinitionContent, Criteria> getExportDefinitionContentDAO() {
		return this.exportDefinitionContentDAO;
	}


	public void setExportDefinitionContentDAO(AdvancedUpdatableDAO<ExportDefinitionContent, Criteria> exportDefinitionContentDAO) {
		this.exportDefinitionContentDAO = exportDefinitionContentDAO;
	}


	public AdvancedUpdatableDAO<ExportDefinitionType, Criteria> getExportDefinitionTypeDAO() {
		return this.exportDefinitionTypeDAO;
	}


	public void setExportDefinitionTypeDAO(AdvancedUpdatableDAO<ExportDefinitionType, Criteria> exportDefinitionTypeDAO) {
		this.exportDefinitionTypeDAO = exportDefinitionTypeDAO;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public ExportRunService getExportRunService() {
		return this.exportRunService;
	}


	public void setExportRunService(ExportRunService exportRunService) {
		this.exportRunService = exportRunService;
	}


	public SystemConditionEvaluationHandler getSystemConditionEvaluationHandler() {
		return this.systemConditionEvaluationHandler;
	}


	public void setSystemConditionEvaluationHandler(SystemConditionEvaluationHandler systemConditionEvaluationHandler) {
		this.systemConditionEvaluationHandler = systemConditionEvaluationHandler;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public AdvancedUpdatableDAO<ExportDefinitionContentHistory, Criteria> getExportDefinitionContentHistoryDAO() {
		return this.exportDefinitionContentHistoryDAO;
	}


	public void setExportDefinitionContentHistoryDAO(AdvancedUpdatableDAO<ExportDefinitionContentHistory, Criteria> exportDefinitionContentHistoryDAO) {
		this.exportDefinitionContentHistoryDAO = exportDefinitionContentHistoryDAO;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
