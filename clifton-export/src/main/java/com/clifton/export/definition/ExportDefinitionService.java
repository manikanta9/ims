package com.clifton.export.definition;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.export.definition.search.ExportDefinitionContentHistorySearchForm;
import com.clifton.export.definition.search.ExportDefinitionContentSearchForm;
import com.clifton.export.definition.search.ExportDefinitionDestinationExportDefinitionSearchForm;
import com.clifton.export.definition.search.ExportDefinitionDestinationSearchForm;
import com.clifton.export.definition.search.ExportDefinitionSearchForm;
import com.clifton.export.definition.search.ExportDefinitionTypeSearchForm;
import com.clifton.export.run.ExportRunHistory;
import com.clifton.export.run.ExportRunStatus;

import java.util.Date;
import java.util.List;


/**
 * The <code>ExportDefinitionService</code> interface defines methods for working with ExportDefinition
 * and related objects.
 *
 * @author vgomelsky
 */
public interface ExportDefinitionService {

	////////////////////////////////////////////////////////////////////////////
	//////           Export Definition Business Methods                   //////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinition getExportDefinition(int id);


	/**
	 * Return the ExportDefinition populated with the ExportDefinitionContents and ONLY the active ExportDefinitionDestinations.
	 */
	public ExportDefinition getExportDefinitionPopulated(int id);


	public ExportRunHistory getExportDefinitionRunLastHistory(int id);


	public List<ExportDefinition> getExportDefinitionListActiveOnDate(final Date date);


	public List<ExportDefinition> getExportDefinitionList(ExportDefinitionSearchForm searchForm);


	public ExportDefinition saveExportDefinition(ExportDefinition bean);


	/**
	 * Updates just the status of the export definition.  In case other properties are updated by a user while a job is running, this will prevent
	 * any conflicts for versioning
	 * <p>
	 */
	@DoNotAddRequestMapping
	public ExportDefinition updateExportDefinitionStatus(int id, ExportRunStatus status);


	@SecureMethod(securityResource = SecureMethod.RESOURCE_SYSTEM_MANAGEMENT, permissions = SecurityPermission.PERMISSION_FULL_CONTROL)
	public void deleteExportDefinitionAndRunHistory(int id);


	public void deleteExportDefinition(int id);


	@DoNotAddRequestMapping
	public void validateDefinitionSendable(ExportDefinition definition);

	////////////////////////////////////////////////////////////////////////////
	//////           Export Definition Type Business Methods              //////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionType getExportDefinitionType(short id);


	public ExportDefinitionType getExportDefinitionTypeByName(String name);


	public List<ExportDefinitionType> getExportDefinitionTypeList(ExportDefinitionTypeSearchForm searchForm);


	public ExportDefinitionType saveExportDefinitionType(ExportDefinitionType bean);


	public void deleteExportDefinitionType(short id);

	////////////////////////////////////////////////////////////////////////////
	//////           Export Destination Business Methods                   //////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionDestination getExportDefinitionDestination(int id);


	public List<ExportDefinitionDestination> getExportDefinitionDestinationList(ExportDefinitionDestinationSearchForm searchForm);


	public ExportDefinitionDestination saveExportDefinitionDestination(ExportDefinitionDestination destination);


	public void deleteExportDefinitionDestination(int id);


	@SecureMethod(dtoClass = ExportDefinitionDestination.class)
	public void saveExportDefinitionDestinationLink(int destinationId, int definitionId);


	@SecureMethod(dtoClass = ExportDefinitionDestination.class)
	public void deleteExportDefinitionDestinationAndLink(int destinationId, int definitionId);


	@SecureMethod(dtoClass = ExportDefinitionDestination.class)
	public void deleteExportDefinitionDestinationCommon(int id);

	////////////////////////////////////////////////////////////////////////////////
	//////        Export Definition Destination Many-To-Many Methods          //////
	////////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionDestinationExportDefinition getExportDefinitionDestinationExportDefinition(int id);


	public List<ExportDefinitionDestinationExportDefinition> getExportDefinitionDestinationExportDefinitionList(ExportDefinitionDestinationExportDefinitionSearchForm searchForm);


	public ExportDefinitionDestinationExportDefinition saveExportDefinitionDestinationExportDefinition(ExportDefinitionDestinationExportDefinition bean);


	public void deleteExportDefinitionDestinationExportDefinition(int id);

	////////////////////////////////////////////////////////////////////////////
	//////            Export Content Business Methods                     //////
	////////////////////////////////////////////////////////////////////////////


	public ExportDefinitionContent getExportDefinitionContent(int id);


	public List<ExportDefinitionContent> getExportDefinitionContentList(ExportDefinitionContentSearchForm searchForm);


	public ExportDefinitionContent saveExportDefinitionContent(ExportDefinitionContent content);


	public void deleteExportDefinitionContent(int id);

	////////////////////////////////////////////////////////////////////////////
	//////          Export Content Business Methods History               //////
	////////////////////////////////////////////////////////////////////////////


	public List<ExportDefinitionContentHistory> getExportDefinitionContentHistoryList(ExportDefinitionContentHistorySearchForm searchForm);


	@DoNotAddRequestMapping
	public ExportDefinitionContentHistory saveExportDefinitionContentHistory(ExportDefinitionContentHistory contentHistory);


	@DoNotAddRequestMapping
	public void saveExportDefinitionContentHistoryList(List<ExportDefinitionContentHistory> contentHistoryList);
}
