package com.clifton.export.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ExportDefinitionContentSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField
	private Integer id;

	@SearchField(searchField = "fileNameTemplate,contentSystemBean.name,definition.name,contentSystemBean.type.name")
	private String searchPattern;

	@SearchField(searchField = "definition.id")
	private Integer definitionId;

	@SearchField(searchField = "definition.id", comparisonConditions = ComparisonConditions.IN)
	private Integer[] definitionIds;

	@SearchField(searchFieldPath = "definition", searchField = "name")
	private String definitionName;

	@SearchField
	private Boolean disabled;

	@SearchField(searchField = "contentSystemBean.id")
	private Integer contentSystemBeanId;

	@SearchField
	private String fileNameTemplate;

	@SearchField(searchField = "notGenerateFile", comparisonConditions = ComparisonConditions.NOT_EQUALS)
	private Boolean generateFile;

	@SearchField(searchField = "contentReadySystemCondition.name")
	private String contentReadySystemConditionName;

	@SearchField(searchField = "contentSystemBean.type.id")
	private Integer contentSystemBeanTypeId;

	@SearchField(searchField = "contentEntityModifyCondition.name")
	private String modifyCondition;

	@SearchField(searchField = "contextSystemBean.name")
	private String contextBeanName;

	@SearchField(searchField = "historySystemTable.name")
	private String historyTable;

	@SearchField
	private String historyPKColumnName;

	@SearchField
	private Short historyDaysToInclude;

	@SearchField
	private Short historyDaysToKeep;

	@SearchField
	private Short numberOfRowsToSkip;

	/**
	 * Customized search field that returns entities for which the definition is active, if set to true, or inactive, if set to false.
	 */
	private Boolean activeDefinition;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Integer[] getDefinitionIds() {
		return this.definitionIds;
	}


	public void setDefinitionIds(Integer[] definitionIds) {
		this.definitionIds = definitionIds;
	}


	public String getDefinitionName() {
		return this.definitionName;
	}


	public void setDefinitionName(String definitionName) {
		this.definitionName = definitionName;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Integer getContentSystemBeanId() {
		return this.contentSystemBeanId;
	}


	public void setContentSystemBeanId(Integer contentSystemBeanId) {
		this.contentSystemBeanId = contentSystemBeanId;
	}


	public String getFileNameTemplate() {
		return this.fileNameTemplate;
	}


	public void setFileNameTemplate(String fileNameTemplate) {
		this.fileNameTemplate = fileNameTemplate;
	}


	public Boolean getGenerateFile() {
		return this.generateFile;
	}


	public void setGenerateFile(Boolean generateFile) {
		this.generateFile = generateFile;
	}


	public String getContentReadySystemConditionName() {
		return this.contentReadySystemConditionName;
	}


	public void setContentReadySystemConditionName(String contentReadySystemConditionName) {
		this.contentReadySystemConditionName = contentReadySystemConditionName;
	}


	public Integer getContentSystemBeanTypeId() {
		return this.contentSystemBeanTypeId;
	}


	public void setContentSystemBeanTypeId(Integer contentSystemBeanTypeId) {
		this.contentSystemBeanTypeId = contentSystemBeanTypeId;
	}


	public String getModifyCondition() {
		return this.modifyCondition;
	}


	public void setModifyCondition(String modifyCondition) {
		this.modifyCondition = modifyCondition;
	}


	public String getContextBeanName() {
		return this.contextBeanName;
	}


	public void setContextBeanName(String contextBeanName) {
		this.contextBeanName = contextBeanName;
	}


	public String getHistoryTable() {
		return this.historyTable;
	}


	public void setHistoryTable(String historyTable) {
		this.historyTable = historyTable;
	}


	public String getHistoryPKColumnName() {
		return this.historyPKColumnName;
	}


	public void setHistoryPKColumnName(String historyPKColumnName) {
		this.historyPKColumnName = historyPKColumnName;
	}


	public Short getHistoryDaysToInclude() {
		return this.historyDaysToInclude;
	}


	public void setHistoryDaysToInclude(Short historyDaysToInclude) {
		this.historyDaysToInclude = historyDaysToInclude;
	}


	public Short getHistoryDaysToKeep() {
		return this.historyDaysToKeep;
	}


	public void setHistoryDaysToKeep(Short historyDaysToKeep) {
		this.historyDaysToKeep = historyDaysToKeep;
	}


	public Boolean getActiveDefinition() {
		return this.activeDefinition;
	}


	public void setActiveDefinition(Boolean activeDefinition) {
		this.activeDefinition = activeDefinition;
	}


	public Short getNumberOfRowsToSkip() {
		return this.numberOfRowsToSkip;
	}


	public void setNumberOfRowsToSkip(Short numberOfRowsToSkip) {
		this.numberOfRowsToSkip = numberOfRowsToSkip;
	}
}
