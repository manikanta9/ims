package com.clifton.export.definition.search;


import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.form.entity.BaseAuditableEntitySearchForm;


public class ExportDefinitionDestinationSearchForm extends BaseAuditableEntitySearchForm {

	@SearchField(searchField = "destinationName,description")
	private String searchPattern;

	@SearchField
	private Integer id;

	@SearchField(searchField = "definitionList.id", comparisonConditions = ComparisonConditions.EXISTS)
	private Integer definitionId;

	@SearchField
	private Boolean disabled;

	@SearchField
	private Boolean sharedDestination;

	@SearchField(searchField = "destinationName", comparisonConditions = ComparisonConditions.LIKE)
	private String destinationName;

	@SearchField(searchField = "description", comparisonConditions = ComparisonConditions.LIKE)
	private String description;

	@SearchField(searchFieldPath = "destinationSystemBean.type", searchField = "name")
	private String destinationTypeName;

	/////////////////////////////////////////////////////////////////////////////
	////////////            Getter and Setter Methods            ////////////////
	/////////////////////////////////////////////////////////////////////////////


	public Integer getId() {
		return this.id;
	}


	public void setId(Integer id) {
		this.id = id;
	}


	public Integer getDefinitionId() {
		return this.definitionId;
	}


	public void setDefinitionId(Integer definitionId) {
		this.definitionId = definitionId;
	}


	public Boolean getDisabled() {
		return this.disabled;
	}


	public void setDisabled(Boolean disabled) {
		this.disabled = disabled;
	}


	public Boolean getSharedDestination() {
		return this.sharedDestination;
	}


	public void setSharedDestination(Boolean sharedDestination) {
		this.sharedDestination = sharedDestination;
	}


	public String getDestinationName() {
		return this.destinationName;
	}


	public void setDestinationName(String destinationName) {
		this.destinationName = destinationName;
	}


	public String getDescription() {
		return this.description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getDestinationTypeName() {
		return this.destinationTypeName;
	}


	public void setDestinationTypeName(String destinationTypeName) {
		this.destinationTypeName = destinationTypeName;
	}


	public String getSearchPattern() {
		return this.searchPattern;
	}


	public void setSearchPattern(String searchPattern) {
		this.searchPattern = searchPattern;
	}
}
