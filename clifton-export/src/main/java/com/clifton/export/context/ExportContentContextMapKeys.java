package com.clifton.export.context;

/**
 * @author theodorez
 */
public enum ExportContentContextMapKeys {
	REPORT_POSTING_FILE_CONFIG, EXPORT_CONTENT_REFERENCE_MAP
}
