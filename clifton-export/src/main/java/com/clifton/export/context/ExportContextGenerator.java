package com.clifton.export.context;

import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.export.template.ExportTemplateGenerator;

import java.util.Map;


/**
 * @author theodorez
 */
public interface ExportContextGenerator extends ExportTemplateGenerator<ExportDefinitionContent> {

	/**
	 * Returns a context map that will be passed to the destination generators
	 */
	public Map<ExportContentContextMapKeys, Object> getContext(ExportDefinitionContent content, Object generatedData);


	public String getContextLabel();
}
