package com.clifton.export.destination;


import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.util.ExportMessagingUtils;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;

import java.util.List;
import java.util.Map;


/**
 * The <code>ExportDestinationFtpGenerator</code> implements {@link ExportDestinationGenerator} to generate fax data for export.
 *
 * @author msiddiqui
 */
public class ExportDestinationFtpGenerator extends FtpConfig implements ExportDestinationGenerator {


	private SecuritySecretService securitySecretService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private SecuritySecret passwordSecret;

	private SecuritySecret sshPrivateKeySecret;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generateDestination(@SuppressWarnings("unused") ExportDefinitionDestination destination, @SuppressWarnings("unused") ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		return ExportMessagingUtils.generateFtpDestinationMap(this, getPasswordSecretString(), getSshPrivateKeySecretString());
	}


	@Override
	public Integer getPasswordSecretId() {
		if (getPasswordSecret() != null) {
			return getPasswordSecret().getId();
		}
		return null;
	}


	@Override
	public Integer getSshPrivateKeySecretId() {
		if (getSshPrivateKeySecret() != null) {
			return getSshPrivateKeySecret().getId();
		}
		return null;
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.FTP;
	}


	@Override
	public String getDestinationLabel() {
		return getUrl() + ((getPort() != null) ? ":" + getPort() : "");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String getPasswordSecretString() {
		return getPasswordSecret() != null ? getSecuritySecretService().decryptSecuritySecret(getSecuritySecretService().getSecuritySecret(getPasswordSecret().getId())).getSecretString() : null;
	}


	private String getSshPrivateKeySecretString() {
		return getSshPrivateKeySecret() != null ? getSecuritySecretService().decryptSecuritySecret(getSecuritySecretService().getSecuritySecret(getSshPrivateKeySecret().getId())).getSecretString() : null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public SecuritySecret getPasswordSecret() {
		return this.passwordSecret;
	}


	public void setPasswordSecret(SecuritySecret passwordSecret) {
		this.passwordSecret = passwordSecret;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public SecuritySecret getSshPrivateKeySecret() {
		return this.sshPrivateKeySecret;
	}


	public void setSshPrivateKeySecret(SecuritySecret sshPrivateKeySecret) {
		this.sshPrivateKeySecret = sshPrivateKeySecret;
	}
}
