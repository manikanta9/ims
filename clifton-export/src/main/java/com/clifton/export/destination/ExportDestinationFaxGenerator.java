package com.clifton.export.destination;


import com.clifton.business.contact.BusinessContactService;
import com.clifton.core.beans.common.Contact;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ExportDestinationFaxGenerator</code> implements {@link ExportDestinationGenerator} to generate fax data for export.
 *
 * @author theodorez
 */
public class ExportDestinationFaxGenerator implements ExportDestinationGenerator, ValidationAware {

	/**
	 * The id of the business contact the will have the fax number we need to send to.
	 */
	private Integer recipientContactId;

	/**
	 * The id of the business contact that will the fax will be sent by
	 */
	private Integer senderContactId;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private BusinessContactService businessContactService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generateDestination(@SuppressWarnings("unused") ExportDefinitionDestination destination, @SuppressWarnings("unused") ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		Map<ExportMapKeys, String> result = new EnumMap<>(ExportMapKeys.class);
		populateRecipientInformation(result);
		populateSenderInformation(result);
		populateMessageBody(result);
		return result;
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.PHONE;
	}


	@Override
	public String getDestinationLabel() {
		return getRecipientContact().getNameLabel();
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotNull(this.recipientContactId, "Cannot find businessContact with id [" + this.recipientContactId + "]");
		Contact contact = getRecipientContact();
		ValidationUtils.assertNotNull(contact.getNameLabel(), "Name is required for contact: " + this.recipientContactId);
		ValidationUtils.assertNotNull(contact.getFaxNumber(), "Fax number is required for contact: " + this.recipientContactId);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void populateMessageBody(Map<ExportMapKeys, String> result) {
		result.put(ExportMapKeys.MESSAGE_SUBJECT, getType() + " to " + result.get(ExportMapKeys.FAX_RECIPIENT));
		result.put(ExportMapKeys.MESSAGE_TEXT, getType() + " to " + result.get(ExportMapKeys.FAX_RECIPIENT));
	}


	private void populateSenderInformation(Map<ExportMapKeys, String> result) {
		Contact contact = getSenderContact();
		if (contact != null) {
			result.put(ExportMapKeys.FAX_SENDER_NUMBER, contact.getFaxNumber());
			result.put(ExportMapKeys.FAX_SENDER_NAME, contact.getNameLabel());
			result.put(ExportMapKeys.FAX_SENDER, contact.getEmailAddress());
		}
	}


	private void populateRecipientInformation(Map<ExportMapKeys, String> result) {
		Contact contact = getRecipientContact();
		result.put(ExportMapKeys.FAX_RECIPIENT, contact.getFaxNumber());
		result.put(ExportMapKeys.FAX_RECIPIENT_NAME, contact.getNameLabel());
	}


	private Contact getRecipientContact() {
		return getBusinessContactService().getBusinessContact(getRecipientContactId());
	}


	private Contact getSenderContact() {
		if (getSenderContactId() != null) {
			return getBusinessContactService().getBusinessContact(getSenderContactId());
		}
		return null;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRecipientContactId() {
		return this.recipientContactId;
	}


	public void setRecipientContactId(Integer recipientContactId) {
		this.recipientContactId = recipientContactId;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setbusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public Integer getSenderContactId() {
		return this.senderContactId;
	}


	public void setSenderContactId(Integer senderContactId) {
		this.senderContactId = senderContactId;
	}
}
