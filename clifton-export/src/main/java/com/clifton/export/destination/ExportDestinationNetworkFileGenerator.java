package com.clifton.export.destination;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ExportDestinationNetworkFileGenerator</code> implements {@link ExportDestinationGenerator} to generate a network file export.
 *
 * @author mwacker
 */
public class ExportDestinationNetworkFileGenerator implements ExportDestinationGenerator, ValidationAware {

	/**
	 * The path to the network folder where the file will be exported to.
	 */
	private String destinationNetworkPath;

	/**
	 * Whether or not the destination file should be overwritten if a file with the same name already exists
	 */
	private boolean overwriteFile;

	/**
	 * Whether or not a .write extension will be added to the file while being copied.
	 */
	private boolean writeExtension;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generateDestination(@SuppressWarnings("unused") ExportDefinitionDestination destination, @SuppressWarnings("unused") ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		Map<ExportMapKeys, String> result = new EnumMap<>(ExportMapKeys.class);
		result.put(ExportMapKeys.NETWORK_PATH, getDestinationNetworkPath());
		result.put(ExportMapKeys.NETWORK_OVERWRITE, StringUtils.toString(isOverwriteFile()));
		result.put(ExportMapKeys.NETWORK_WRITE_EXTENSION, StringUtils.toString(isWriteExtension()));
		return result;
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.NETWORK_FILE;
	}


	@Override
	public void validate() {
		ValidationUtils.assertTrue(FileUtils.fileExists(getDestinationNetworkPath()), "The from path [" + getDestinationNetworkPath() + "] does not exist.");
	}


	@Override
	public String getDestinationLabel() {
		return getDestinationNetworkPath();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getDestinationNetworkPath() {
		return this.destinationNetworkPath;
	}


	public void setDestinationNetworkPath(String destinationNetworkPath) {
		this.destinationNetworkPath = destinationNetworkPath;
	}


	public boolean isOverwriteFile() {
		return this.overwriteFile;
	}


	public void setOverwriteFile(boolean overwriteFile) {
		this.overwriteFile = overwriteFile;
	}


	public boolean isWriteExtension() {
		return this.writeExtension;
	}


	public void setWriteExtension(boolean writeExtension) {
		this.writeExtension = writeExtension;
	}
}
