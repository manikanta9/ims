package com.clifton.export.destination;


import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.template.ExportTemplateGenerator;

import java.util.List;
import java.util.Map;


/**
 * The <code>ExportDestinationGenerator</code> defines methods used to generate destination data.
 */
public interface ExportDestinationGenerator extends ExportTemplateGenerator<ExportDefinitionDestination> {


	/**
	 * Returns a map of data that represents everything needed to send the data to this destination.
	 * <p>
	 * The key value of the map should be the name of a ExportMapKeys entry and the value is needed data.
	 * <p>
	 * NOTE: To be used by external destinations only! (I.e. when being sent to Integration via JMS)
	 */
	public Map<ExportMapKeys, String> generateDestination(ExportDefinitionDestination destination, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap);


	/**
	 * Used to send items to internal destinations (i.e. portal)
	 */
	default public void processInternalDestination(List<ExportFile> files, Map<String, Object> configurationMap) {
		//do nothing
	}


	public ExportDestinationTypes getType();


	/**
	 * Returns user friendly label for this generator.  For example, email subject, fax number, etc.
	 */
	public String getDestinationLabel();
}
