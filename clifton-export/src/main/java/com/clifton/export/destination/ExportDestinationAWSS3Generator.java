package com.clifton.export.destination;

import com.amazonaws.services.s3.AmazonS3URI;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.aws.s3.AWSS3Handler;
import com.clifton.core.util.aws.s3.AWSS3HandlerImpl;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.security.credential.SecurityCredential;
import com.clifton.security.credential.SecurityCredentialService;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;

import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import static com.clifton.core.util.aws.s3.AWSS3Utils.getAmazonS3Uri;


/**
 * The <code>ExportDestinationAWSS3Generator</code> implements {@link ExportDestinationGenerator} to generate a network file export.
 *
 * @author rtschumper
 */
public class ExportDestinationAWSS3Generator implements ExportDestinationGenerator, ValidationAware {
	private SecurityCredentialService securityCredentialService;

	private SecuritySecretService securitySecretService;
	/**
	 * The unique S3 bucket name and its corresponding region name (i.e. us-east-1).
	 */
	private String bucket;
	private String region;

	private int awsUser;

	/**
	 * Used for testing purposes ONLY
	 */
	private String awsS3Endpoint;


	@Override
	public Map<ExportMapKeys, String> generateDestination(ExportDefinitionDestination destination, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		Map<ExportMapKeys, String> result = new EnumMap<>(ExportMapKeys.class);

		result.put(ExportMapKeys.AWS_S3_BUCKET, getBucket());
		result.put(ExportMapKeys.AWS_S3_REGION, getRegion());

		SecurityCredential securityCredential = getSecurityCredentialService().getSecurityCredential((short)getAwsUser());
		result.put(ExportMapKeys.AWS_S3_ACCESS_KEY_ID, securityCredential.getUserName());
		result.put(ExportMapKeys.AWS_S3_SECRET_KEY, decrypt(securityCredential));

		if (!StringUtils.isEmpty(getAwsS3Endpoint())) {
			result.put(ExportMapKeys.AWS_S3_ENDPOINT_OVERRIDE, getAwsS3Endpoint());
		}

		return result;
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.AWS_S3;
	}


	@Override
	public String getDestinationLabel() {
		return bucket + region;
	}

	@Override
	public void validate() {
		AWSS3Handler handler = new AWSS3HandlerImpl();
		AmazonS3URI s3Uri = getAmazonS3Uri(this.bucket);
		handler.validateBucketName(s3Uri.getBucket());
	}

	private String decrypt(SecurityCredential securityCredential){
		SecuritySecret securitySecret = securityCredential.getPasswordSecret();
		securitySecret = getSecuritySecretService().decryptSecuritySecret(securitySecret);
		return securitySecret.getSecretString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////

	public String getBucket() {
		return this.bucket;
	}


	public void setBucket(String bucket) {
		this.bucket = bucket;
	}


	public String getRegion() {
		return this.region;
	}


	public void setRegion(String region) {
		this.region = region;
	}


	public int getAwsUser() {
		return this.awsUser;
	}


	public void setAwsUser(int awsUser) {
		this.awsUser = awsUser;
	}


	public String getAwsS3Endpoint() {
		return this.awsS3Endpoint;
	}


	public void setAwsS3Endpoint(String awsS3Endpoint) {
		this.awsS3Endpoint = awsS3Endpoint;
	}

	public SecurityCredentialService getSecurityCredentialService() {
		return this.securityCredentialService;
	}


	public void setSecurityCredentialService(SecurityCredentialService securityCredentialService) {
		this.securityCredentialService = securityCredentialService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
