package com.clifton.export.destination;


import com.clifton.business.contact.BusinessContactService;
import com.clifton.core.beans.common.Contact;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportConstants;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.template.ExportTemplateCommand;
import com.clifton.export.template.ExportTemplateGenerationHandler;
import com.clifton.security.authorization.SecurityAuthorizationService;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ExportDestinationEmailGenerator</code> implements {@link ExportDestinationGenerator} to generate email data for export.
 */
public class ExportDestinationEmailGenerator implements ExportDestinationGenerator, ValidationAware {

	private BusinessContactService businessContactService;
	private ExportTemplateGenerationHandler exportTemplateGenerationHandler;

	private SecurityAuthorizationService securityAuthorizationService;

	////////////////////////////////////////////////////////////////////////////

	private List<Integer> toContactIds;
	private List<Integer> ccContactIds;
	private List<Integer> bccContactIds;

	private Integer fromContactId;
	/**
	 * Used to send messages on behalf of the from contact
	 */
	private Integer senderContactId;

	private String subject;
	private String text;

	/**
	 * Used for testing purposes ONLY
	 */
	private String smtpHostUrl;
	private Integer smtpHostPort;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportMapKeys, String> generateDestination(ExportDefinitionDestination destination, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		Map<ExportMapKeys, String> result = new EnumMap<>(ExportMapKeys.class);

		//Generate From email address
		Contact fromContact = getFromContact();
		validateAndAddContactAddress(fromContact, result, ExportMapKeys.EMAIL_FROM);
		validateAndAddContactName(fromContact, result, ExportMapKeys.EMAIL_FROM_NAME);

		Contact senderContact = getSenderContact();
		validateAndAddContactAddress(senderContact, result, ExportMapKeys.EMAIL_SENDER);
		validateAndAddContactName(senderContact, result, ExportMapKeys.EMAIL_SENDER_NAME);

		ExportTemplateCommand<ExportDefinitionDestination> templateCommand = prepareExportTemplateCommand(this, destination, exportDefinition, fromContact, senderContact, files);
		populateSubjectAndBody(templateCommand, result);

		addEmailAddresses(getToContactIds(), ExportMapKeys.EMAIL_TO, result);
		addEmailAddresses(getCcContactIds(), ExportMapKeys.EMAIL_CC, result);
		addEmailAddresses(getBccContactIds(), ExportMapKeys.EMAIL_BCC, result);

		if (!StringUtils.isEmpty(getSmtpHostUrl())) {
			result.put(ExportMapKeys.SMTP_HOSTNAME, getSmtpHostUrl());
		}

		if (getSmtpHostPort() != null) {
			result.put(ExportMapKeys.SMTP_PORT, getSmtpHostPort().toString());
		}

		return result;
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.EMAIL;
	}


	@Override
	public String getDestinationLabel() {
		StringBuilder result = new StringBuilder(100);
		int toCount = CollectionUtils.getSize(getToContactIds());
		if (toCount > 0) {
			result.append(getBusinessContactService().getBusinessContact(getToContactIds().get(0)).getLabelShort());
			if (toCount > 1) {
				result.append(", ...: ");
			}
			else {
				result.append(": ");
			}
		}
		result.append(getSubject());
		return result.toString();
	}


	@Override
	public void validate() {
		if (!StringUtils.isEmpty(getSmtpHostUrl()) || getSmtpHostPort() != null) {
			ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityUserAdmin(), "Only administrators may specify a non-standard SMTP Host or Port");
		}

		validateContacts(getToContactIds());
		validateContacts(getCcContactIds());
		validateContacts(getBccContactIds());

		if (getFromContactId() != null) {
			validateContactById(getFromContactId());
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected void populateSubjectAndBody(ExportTemplateCommand<ExportDefinitionDestination> templateCommand, Map<ExportMapKeys, String> result) {
		String emailSubject = applyTemplate(getSubject(), templateCommand);
		result.put(ExportMapKeys.MESSAGE_SUBJECT, emailSubject);

		String body = applyTemplate(buildEmailBody(templateCommand.getDefinition()), templateCommand);
		int maxEmailSize = DataTypes.TEXT.getLength();
		if (body.length() > maxEmailSize) {
			throw new ValidationException("E-mail body (message and signature) has a length of [" + body.length() + "], " +
					"which exceeds maximum length of [" + maxEmailSize + "]");
		}
		result.put(ExportMapKeys.MESSAGE_TEXT, body);
	}


	private String applyTemplate(String field, ExportTemplateCommand<ExportDefinitionDestination> command) {
		return getExportTemplateGenerationHandler().processTemplate(field, command);
	}


	protected ExportTemplateCommand<ExportDefinitionDestination> prepareExportTemplateCommand(ExportDestinationGenerator generator, ExportDefinitionDestination destination, ExportDefinition definition, Contact fromContact, Contact senderContact, List<ExportFile> files) {
		// TODO: may apply Freemarker transformation to both subject and text: add contacts/etc. to context?
		Map<String, Object> contextMap = new HashMap<>();
		if (fromContact != null) {
			contextMap.put("fromContact", fromContact);
			if (senderContact != null) {
				contextMap.put("senderContact", senderContact);
			}
			else {
				//To support existing transformations that use Sender Contact when referring to the From Contact
				contextMap.put("senderContact", fromContact);
			}
		}
		contextMap.putAll(getContentContextReferenceMap(files));

		return new ExportTemplateCommand<>(generator, destination, definition, contextMap);
	}


	private Map<String, Object> getContentContextReferenceMap(List<ExportFile> files) {
		Map<String, Object> contextFreemarkerMap = new HashMap<>();
		for (ExportFile file : files) {
			Map<ExportContentContextMapKeys, Object> context = file.getFileContext();
			if (context != null) {
				Object contentValue = context.get(ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP);
				if (contentValue instanceof Map) {
					contextFreemarkerMap.putAll(((Map<String, Object>) contentValue));
				}
			}
		}
		return contextFreemarkerMap;
	}


	private Contact getFromContact() {
		//Generate From email address
		Contact fromContact = null;
		if (getFromContactId() != null) {
			fromContact = getBusinessContactService().getBusinessContact(getFromContactId());
			ValidationUtils.assertNotNull(fromContact, "Cannot find Contact with id = " + getFromContactId());
		}


		return fromContact;
	}


	private Contact getSenderContact() {
		//Generate Sender email address
		Contact senderContact = null;
		if (getSenderContactId() != null) {
			senderContact = getBusinessContactService().getBusinessContact(getSenderContactId());
			ValidationUtils.assertNotNull(senderContact, "Cannot find Contact with id = " + getFromContactId());
		}

		return senderContact;
	}


	private void validateAndAddContactAddress(Contact contact, Map<ExportMapKeys, String> result, ExportMapKeys key) {
		if (contact != null) {
			ValidationUtils.assertNotNull(contact.getEmailAddress(), "Email address is required for contact: " + contact);
			result.put(key, contact.getEmailAddress());
		}
	}


	private void validateAndAddContactName(Contact contact, Map<ExportMapKeys, String> result, ExportMapKeys key) {
		if (contact != null) {
			ValidationUtils.assertNotNull(contact.getNameLabel(), "Name label is required for contact: " + contact);
			result.put(key, contact.getNameLabel());
		}
	}


	protected void validateContacts(List<Integer> contactIds) {
		for (int contactId : CollectionUtils.getIterable(contactIds)) {
			validateContactById(contactId);
		}
	}


	protected void validateContactById(Integer contactId) {
		Contact contact = getBusinessContactService().getBusinessContact(contactId);
		ValidationUtils.assertNotNull(contact, "Cannot find Contact with id = " + contactId);
		ValidationUtils.assertNotNull(contact.getEmailAddress(), "Email address is required for " + contact.getLabel());
	}


	private String buildEmailBody(ExportDefinition definition) {
		String emailSignature = definition.getType().getEmailSignature();

		StringBuilder emailBody = new StringBuilder();
		emailBody.append(getText());

		if (!StringUtils.isEmpty(emailSignature)) {
			emailBody.append(System.lineSeparator());
			emailBody.append(System.lineSeparator());
			emailBody.append(definition.getType().getEmailSignature());
		}
		return emailBody.toString();
	}


	protected void addEmailAddresses(List<Integer> contactIds, ExportMapKeys key, Map<ExportMapKeys, String> resultMap) {
		if (!CollectionUtils.isEmpty(contactIds) && resultMap != null) {
			//Get the existing email address list
			StringBuilder to = (resultMap.get(key) != null) ? new StringBuilder(resultMap.get(key)) : new StringBuilder();
			for (Integer contactId : contactIds) {
				Contact contact = getBusinessContactService().getBusinessContact(contactId);
				if (contact != null && !StringUtils.isEmpty(contact.getEmailAddress())) {
					if (to.length() > 0) {
						to.append(ExportConstants.STRING_DELIMITER);
					}
					to.append(contact.getEmailAddress());
				}
			}
			resultMap.put(key, to.toString());
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public List<Integer> getToContactIds() {
		return this.toContactIds;
	}


	public void setToContactIds(List<Integer> toContactIds) {
		this.toContactIds = toContactIds;
	}


	public List<Integer> getCcContactIds() {
		return this.ccContactIds;
	}


	public void setCcContactIds(List<Integer> ccContactIds) {
		this.ccContactIds = ccContactIds;
	}


	public List<Integer> getBccContactIds() {
		return this.bccContactIds;
	}


	public void setBccContactIds(List<Integer> bccContactIds) {
		this.bccContactIds = bccContactIds;
	}


	public BusinessContactService getBusinessContactService() {
		return this.businessContactService;
	}


	public void setBusinessContactService(BusinessContactService businessContactService) {
		this.businessContactService = businessContactService;
	}


	public Integer getFromContactId() {
		return this.fromContactId;
	}


	public void setFromContactId(Integer fromContactId) {
		this.fromContactId = fromContactId;
	}


	public Integer getSenderContactId() {
		return this.senderContactId;
	}


	public void setSenderContactId(Integer senderContactId) {
		this.senderContactId = senderContactId;
	}


	public String getSubject() {
		return this.subject;
	}


	public void setSubject(String subject) {
		this.subject = subject;
	}


	public String getText() {
		return this.text;
	}


	public void setText(String text) {
		this.text = text;
	}


	public ExportTemplateGenerationHandler getExportTemplateGenerationHandler() {
		return this.exportTemplateGenerationHandler;
	}


	public void setExportTemplateGenerationHandler(ExportTemplateGenerationHandler exportTemplateGenerationHandler) {
		this.exportTemplateGenerationHandler = exportTemplateGenerationHandler;
	}


	public SecurityAuthorizationService getSecurityAuthorizationService() {
		return this.securityAuthorizationService;
	}


	public void setSecurityAuthorizationService(SecurityAuthorizationService securityAuthorizationService) {
		this.securityAuthorizationService = securityAuthorizationService;
	}


	public String getSmtpHostUrl() {
		return this.smtpHostUrl;
	}


	public void setSmtpHostUrl(String smtpHostUrl) {
		this.smtpHostUrl = smtpHostUrl;
	}


	public Integer getSmtpHostPort() {
		return this.smtpHostPort;
	}


	public void setSmtpHostPort(Integer smtpHostPort) {
		this.smtpHostPort = smtpHostPort;
	}
}
