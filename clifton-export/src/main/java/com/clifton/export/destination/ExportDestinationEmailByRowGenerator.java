package com.clifton.export.destination;

import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.definition.ExportDefinition;
import com.clifton.export.definition.ExportDefinitionDestination;
import com.clifton.export.file.ExportFile;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.template.ExportTemplateCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;


/**
 * @author TerryS
 */
public class ExportDestinationEmailByRowGenerator extends ExportDestinationEmailGenerator {

	private static final List<ExportMapKeys> emailKeys = Arrays.asList(
			ExportMapKeys.EMAIL_BCC,
			ExportMapKeys.EMAIL_CC,
			ExportMapKeys.EMAIL_FROM,
			ExportMapKeys.EMAIL_FROM_NAME,
			ExportMapKeys.EMAIL_SENDER,
			ExportMapKeys.EMAIL_SENDER_NAME,
			ExportMapKeys.EMAIL_TO
	);


	@Override
	public Map<ExportMapKeys, String> generateDestination(ExportDefinitionDestination destination, ExportDefinition exportDefinition, List<ExportFile> files, Map<String, Object> configurationMap) {
		Map<ExportMapKeys, String> result = new EnumMap<>(ExportMapKeys.class);

		for (ExportMapKeys exportMapKey : emailKeys) {
			String key = exportMapKey.toString();
			if (configurationMap.containsKey(key)) {
				String value = StringUtils.toNullableString(configurationMap.get(key));
				if (!StringUtils.isEmpty(value)) {
					result.put(exportMapKey, value);
				}
			}
		}

		ExportTemplateCommand<ExportDefinitionDestination> templateCommand = prepareExportTemplateCommand(this, destination, exportDefinition, null, null, files);
		populateSubjectAndBody(templateCommand, result);

		if (!StringUtils.isEmpty(getSmtpHostUrl())) {
			result.put(ExportMapKeys.SMTP_HOSTNAME, getSmtpHostUrl());
		}

		if (getSmtpHostPort() != null) {
			result.put(ExportMapKeys.SMTP_PORT, getSmtpHostPort().toString());
		}

		List<String> missing = runtimeValidation(result);
		if (!CollectionUtils.isEmpty(missing)) {
			LogUtils.error(LogCommand.ofMessageSupplier(ExportDestinationEmailByRowGenerator.class, () -> String.format("The support email for [%s] may not send due to missing information %s.", exportDefinition.getName(), missing)));
		}

		return result;
	}


	@Override
	public ExportDestinationTypes getType() {
		return ExportDestinationTypes.BY_ROW_EMAIL;
	}


	@Override
	public String getDestinationLabel() {
		StringBuilder result = new StringBuilder(100);
		result.append(getSubject());
		return result.toString();
	}


	public List<String> runtimeValidation(Map<ExportMapKeys, String> exportMap) {
		List<String> results = new ArrayList<>();

		if (!StringUtils.isEmpty(getSmtpHostUrl()) || getSmtpHostPort() != null) {
			ValidationUtils.assertTrue(getSecurityAuthorizationService().isSecurityUserAdmin(), "Only administrators may specify a non-standard SMTP Host or Port");
		}
		if (!exportMap.containsKey(ExportMapKeys.EMAIL_TO) || StringUtils.isEmpty(exportMap.get(ExportMapKeys.EMAIL_TO))) {
			results.add(ExportMapKeys.EMAIL_TO.toString());
		}
		if (!exportMap.containsKey(ExportMapKeys.EMAIL_FROM) || StringUtils.isEmpty(exportMap.get(ExportMapKeys.EMAIL_FROM))) {
			results.add(ExportMapKeys.EMAIL_FROM.toString());
		}
		return results;
	}
}
