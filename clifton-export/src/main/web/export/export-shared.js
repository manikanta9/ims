Ext.ns('Clifton.export', 'Clifton.export.definition', 'Clifton.export.destination', 'Clifton.export.content', 'Clifton.export.content.transformation');


Clifton.export.TradeExportFileFormatTypes = [
	['J.P. Morgan Export', 'JP_MORGAN', 'The trade file format required by J.P. Morgan.'],
	['Standard Futures Export', 'STANDARD_FUTURES', 'The standard format for future trades.']
];

Clifton.export.DisplayOptions = [
	['ALL', 'All', 'Display all export runs'],
	['OWNED_BY_ME', 'Owned By Me', 'Display export runs where current user is the Owner User or belongs to the Owner Group.'],
	['COMPLETED', 'Completed', 'Display only completed export runs'],
	['FAILED', 'Failed', 'Display only Failed export runs'],
	['RUNNING', 'Running', 'Display only running export runs']
];

Clifton.export.getDisplayFilter = function(value) {
	let statusFilter = null;
	let ownerFilter = null;
	if (TCG.isEquals(value, 'FAILED')) {
		statusFilter = ['FAILED_ERROR', 'FAILED_NOT_READY', 'FAILED_NO_DATA'];
	}
	else if (TCG.isEquals(value, 'COMPLETED')) {
		statusFilter = ['COMPLETED', 'COMPLETED_NO_DATA'];
	}
	else if (TCG.isEquals(value, 'RUNNING')) {
		statusFilter = ['NONE', 'RUNNING', 'SENDING', 'WAITING_FOR_RETRY'];
	}
	else if (TCG.isEquals(value, 'OWNED_BY_ME')) {
		const currentUser = TCG.getCurrentUser();
		ownerFilter = currentUser.id;
	}
	return {'statusFilter': statusFilter, 'ownerFilter': ownerFilter};
};

Clifton.export.StatusOptions = [
	['COMPLETED', 'Completed'],
	['COMPLETED_NO_DATA', 'Completed (No Data)'],
	['FAILED_ERROR', 'Failed (Error)'],
	['FAILED_NOT_READY', 'Failed (Not Ready)'],
	['FAILED_NO_DATA', 'Failed (No Data)'],
	['NONE', 'None'],
	['RUNNING', 'Running'],
	['SENDING', 'Sending'],
	['WAITING_FOR_RETRY', 'Waiting for Retry']
];

Clifton.export.renderStatus = function(v) {
	if (v === 'WAITING_FOR_RETRY') {
		return '<div class="amountNegative">Waiting for Retry</div>';
	}
	else if (v === 'RUNNING') {
		return '<div class="amountPositive">Running</div>';
	}
	else if (v === 'SENDING') {
		return '<div class="amountPositive">Sending</div>';
	}
	else if (v === 'FAILED_ERROR') {
		return '<div class="amountNegative">Failed (Error)</div>';
	}
	else if (v === 'FAILED_NOT_READY') {
		return '<div class="amountAdjusted">Failed (Not Ready)</div>';
	}
	else if (v === 'FAILED_NO_DATA') {
		return '<div class="amountAdjusted">Failed (No Data)</div>';
	}
	else if (v === 'COMPLETED') {
		return '<div class="important">Completed</div>';
	}
	else if (v === 'COMPLETED_NO_DATA') {
		return '<div class="important">Completed (No Data)</div>';
	}

	return v;
};

Clifton.export.ExportRunSourceTypes = [['MANUAL', 'Manual'], ['SCHEDULE', 'Schedule'], ['BATCH_JOB', 'Batch Job'], ['RETRY', 'Retry']];

Clifton.export.renderExportRunSourceType = function(v) {
	if (v === 'MANUAL') {
		return 'Manual';
	}
	if (v === 'SCHEDULE') {
		return 'Schedule';
	}
	if (v === 'BATCH_JOB') {
		return 'Batch Job';
	}
	if (v === 'RETRY') {
		return 'Retry';
	}
	return v;
};

Clifton.export.ExportDefinitionContentHistoryGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'exportDefinitionContentHistoryListFind',
	xtype: 'gridpanel',
	instructions: 'A list of entities that have been sent in prior exports for selected Export Definition. Used in cases where we need to track what was already sent to avoid sending the same data more than once.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Export Definition', dataIndex: 'exportDefinitionContent.definition.name', width: 200, filter: {searchFieldName: 'exportDefinitionName'}, hidden: true},
		{header: 'Scheduled Date', dataIndex: 'exportRunHistory.scheduledDate', width: 70, filter: {searchFieldName: 'scheduledDate'}},
		{header: 'Destination', dataIndex: 'exportRunHistory.destination.name', width: 60, filter: {searchFieldName: 'destinationName'}, hidden: true},
		{header: 'Status', dataIndex: 'exportRunHistory.status.name', width: 60, filter: {searchFieldName: 'statusName'}},
		{header: 'Source Type', dataIndex: 'exportRunHistory.exportRunSourceType', width: 60, filter: {searchFieldName: 'exportRunSourceType'}},

		{header: 'System Table', width: 80, dataIndex: 'exportDefinitionContent.historySystemTable.name', filter: {searchFieldName: 'historySystemTableId', type: 'combo', url: 'systemTableListFind.json', displayField: 'nameExpanded'}},
		{header: 'Identifier', dataIndex: 'historyPKFieldId', width: 100, doNotFormat: true, defaultSortColumn: true, defaultSortDirection: 'DESC'},
		{
			header: 'Link', width: 40, dataIndex: 'exportDefinitionContent.historySystemTable.detailScreenClass', filter: false, sortable: false,
			renderer: function(clz, args, r) {
				if (TCG.isNotBlank(clz) && TCG.isNotBlank(r.json.historyPKFieldId)) {
					return TCG.renderActionColumn('view', 'View', 'View ' + TCG.getValue('exportDefinitionContent.historySystemTable.name', r.json) + ' that is linked to this record');
				}
				return 'N/A';
			},
			eventListeners: {
				click: function(column, grid, rowIndex, event) {
					const row = grid.getStore().getAt(rowIndex);
					const className = row.json.exportDefinitionContent.historySystemTable.detailScreenClass;
					const id = row.json.historyPKFieldId;
					TCG.createComponent(className, {
						id: TCG.getComponentId(className, id),
						params: {id: id},
						openerCt: grid.ownerGridPanel
					});
				}
			}
		}
	]
});
Ext.reg('export-definition-content-history-grid', Clifton.export.ExportDefinitionContentHistoryGrid);


Clifton.export.ExportDefinitionGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'exportDefinitionListFind',
	xtype: 'gridpanel',
	pageSize: 500,
	instructions: 'A list of all export definitions. Each definition packages content file(s) and sends them to the specified destination(s) (Email, FTP, FAX, etc.) on scheduled basis or can be triggered manually.',
	wikiPage: 'IT/Export+Management',
	groupField: 'type.label',
	topToolbarSearchParameter: 'searchPattern',
	additionalPropertiesToRequest: 'definition.priority.cssStyle',
	getTopToolbarFilters: function(toolbar) {
		const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.call(this, arguments);
		filters.push({fieldLabel: 'Export Type', xtype: 'toolbar-combo', width: 150, url: 'exportDefinitionTypeListFind.json', linkedFilter: 'type.label'});
		filters.push({
			fieldLabel: 'Display', xtype: 'toolbar-combo', name: 'status', width: 110, minListWidth: 150, forceSelection: true, mode: 'local', value: 'ALL',
			store: {
				xtype: 'arraystore',
				data: Clifton.export.DisplayOptions
			}
		});
		return filters;
	},
	defaultViewName: 'Grouped by Type View',
	viewNames: ['List View', 'Grouped by Type View'],
	switchToViewBeforeReload: function(viewName) {
		let grpPropertyName = 'type.label';
		if (viewName === 'List View') {
			grpPropertyName = null;
		}
		this.grid.getStore().groupBy(grpPropertyName, false);
	},
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Export Type', width: 70, dataIndex: 'type.label', filter: {searchFieldName: 'typeId', type: 'combo', url: 'exportDefinitionTypeListFind.json'}, hidden: true, viewNames: ['List View']},
		{header: 'Export Name', width: 150, dataIndex: 'name', defaultSortColumn: true, filter: {searchFieldName: 'searchPattern'}},
		{header: 'Export Description', width: 300, dataIndex: 'description', hidden: true},
		{header: 'Archive Strategy', width: 100, dataIndex: 'archiveStrategyName', hidden: true},
		{header: 'Modify Condition', width: 150, dataIndex: 'definitionEntityModifyCondition.name', tooltip: 'For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit definitions of this type.', hidden: true},
		{header: 'Schedule', width: 100, dataIndex: 'recurrenceSchedule.name', filter: {searchFieldName: 'recurrenceScheduleId', type: 'combo', url: 'calendarScheduleListFind.json?calendarScheduleTypeName=Standard Schedules&systemDefined=false'}},
		{header: 'Schedule Calendar', width: 100, dataIndex: 'recurrenceSchedule.calendar.name', filter: false, hidden: true},
		{
			header: 'Status', width: 50, dataIndex: 'status.name', filter: {type: 'list', options: Clifton.export.StatusOptions, searchFieldName: 'statusNameList'},
			renderer: Clifton.export.renderStatus
		},
		{
			header: 'Priority', width: 25, dataIndex: 'priority.name', filter: {searchFieldName: 'priorityId', type: 'combo', url: 'systemPriorityListFind.json'},
			renderer: function(v, c, r) {
				const style = r.json.priority ? r.json.priority.cssStyle : '';
				if (TCG.isNotBlank(style)) {
					c.attr = 'style="' + style + '"';
				}
				return v;
			}
		},
		{header: 'Email Subject', dataIndex: 'subject', width: 100, hidden: true},
		{header: 'Email Text', dataIndex: 'text', width: 300, hidden: true},
		{header: 'Owner', dataIndex: 'ownerLabel', filter: {searchFieldName: 'ownerUserOrGroupName'}, sortable: false, width: 50},
		{header: 'Owner User', dataIndex: 'ownerUser.label', width: 50, idDataIndex: 'ownerUser.id', filter: {searchFieldName: 'ownerUserId', displayField: 'label', type: 'combo', url: 'securityUserListFind.json'}, hidden: true},
		{header: 'Owner Group', dataIndex: 'ownerGroup.label', width: 50, idDataIndex: 'ownerGroup.id', filter: {searchFieldName: 'ownerGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
		{header: 'Support', dataIndex: 'supportLabel', filter: {searchFieldName: 'supportUserOrGroupName'}, sortable: false, width: 50},
		{header: 'Support User', dataIndex: 'supportUser.label', width: 50, idDataIndex: 'supportUser.id', filter: {searchFieldName: 'supportUserId', displayField: 'label', type: 'combo', url: 'securityUserListFind.json'}, hidden: true},
		{header: 'Support Group', dataIndex: 'supportGroup.label', width: 50, idDataIndex: 'supportGroup.id', filter: {searchFieldName: 'supportGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
		{header: 'Retry Count', dataIndex: 'retryCount', width: 30, type: 'int', title: 'Number of times to retry in case of failure', useNull: true},
		{header: 'Retry Delay In Seconds', dataIndex: 'retryDelayInSeconds', width: 60, type: 'int', title: 'Time in seconds before a retry is attempted again', hidden: true, useNull: true},
		{header: 'Max Execution Time (ms)', width: 60, dataIndex: 'maxExecutionTimeMillis', type: 'int', useNull: true, hidden: true, qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
		{header: 'Start Date', width: 70, dataIndex: 'startDate', type: 'date', hidden: true},
		{header: 'End Date', width: 70, dataIndex: 'endDate', type: 'date', hidden: true},
		{header: 'Active', dataIndex: 'active', width: 20, type: 'boolean', sortable: false},
		{header: 'Data Expected', dataIndex: 'dataExpected', width: 20, type: 'boolean', hidden: true},
		{header: 'Origin Name', width: 100, dataIndex: 'originName', hidden: true},
		{header: 'Destination Name', width: 100, dataIndex: 'destinationName', hidden: true},
		{header: 'Dynamic Configuration', dataIndex: 'dynamicConfigurationSystemBean.name', width: 100, filter: {searchFieldName: 'dynamicConfigurationBeanName'}, hidden: true},
		{header: 'Failure Monitored', dataIndex: 'failureMonitored', width: 20, type: 'boolean', hidden: true}
	],
	getTopToolbarInitialLoadParams: function(firstLoad) {
		const toolBarInitialLoadParams = {};
		if (firstLoad) {
			this.setFilterValue('active', true);
			this.grid.filters.getFilter('active').setActive(true, true);
			if (this.defaultViewName) {
				this.setDefaultView(this.defaultViewName); // sets it as "checked"
				this.switchToView(this.defaultViewName, true);
			}
		}

		const t = this.getTopToolbar();
		const status = TCG.getChildByName(t, 'status');
		const v = status.getValue();
		const displayFilter = Clifton.export.getDisplayFilter(v);
		if (TCG.isNull(displayFilter.statusFilter)) {
			this.clearFilter('status.name', true);
		}
		else {
			this.setFilterValue('status.name', displayFilter.statusFilter, false, true);
		}
		if (TCG.isNotBlank(displayFilter.ownerFilter)) {
			toolBarInitialLoadParams.ownedByUserId = displayFilter.ownerFilter;
		}
		return toolBarInitialLoadParams;
	},
	editor: {
		detailPageClass: 'Clifton.export.definition.ExportDefinitionWindow',
		deleteRow: function(deleteHistory) {
			const editor = this;
			const grid = this.grid;
			const sm = grid.getSelectionModel();
			if (sm.getCount() === 0) {
				TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
			}
			else if (sm.getCount() !== 1) {
				TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
			}
			else {
				const message = !deleteHistory ? 'Would you like to delete selected row?' : 'Would you like to delete selected definition and run history?';
				Ext.Msg.confirm('Delete Selected Row', message, function(a) {
					if (a === 'yes') {
						const loader = new TCG.data.JsonLoader({
							waitTarget: grid.ownerCt,
							waitMsg: 'Deleting...',
							params: editor.getDeleteParams(sm),
							conf: {rowId: sm.getSelected().id},
							onLoad: function(record, conf) {

								if (editor.reloadGridAfterDelete && editor.reloadGridAfterDelete === true) {
									editor.getGridPanel().reload();
								}
								else {
									grid.getStore().remove(sm.getSelected());
									grid.ownerCt.updateCount();
								}
							}
						});
						const url = !deleteHistory ? editor.getDeleteURL() : 'exportDefinitionAndRunHistoryDelete.json';
						loader.load(url);
					}
				});
			}
		},
		addToolbarDeleteButton: function(toolBar) {
			toolBar.add({
				text: 'Remove',
				xtype: 'splitbutton',
				tooltip: 'Remove selected item',
				iconCls: 'remove',
				scope: this,
				handler: function() {
					this.deleteRow(false);
				},
				menu: {
					items: [{
						text: 'FORCE Delete (Admin Only)',
						tooltip: 'Will delete the contents, destinations and definition (Admin Only).  NOTE: The run history must be deleted before you can use this delete.',
						iconCls: 'remove',
						scope: this,
						handler: function() {
							this.deleteRow(true);
						}
					}]
				}
			});
			toolBar.add('-');
		}
	}
});
Ext.reg('export-definition-grid', Clifton.export.ExportDefinitionGrid);

Clifton.export.ExportDefinitionContentGrid = Ext.extend(TCG.grid.GridPanel, {
	name: 'exportDefinitionContentListFind',
	xtype: 'gridpanel',
	instructions: 'Content defines/configures what are the contents (e.g. files/reports) that are to be exported/sent. The content is usually a file that is sent to the destination(s) directly or as an email attachment.  The content can also be used to generate dynamic string via transformations and embed it into the email body.',
	columns: [
		{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
		{header: 'Export Name', dataIndex: 'definition.name', width: 150, hidden: true, filter: {searchFieldName: 'definitionName'}},
		{header: 'File Name Template', dataIndex: 'fileNameTemplate', width: 200},
		{header: 'Ready Condition', dataIndex: 'contentReadySystemCondition.name', width: 150, filter: {searchFieldName: 'contentReadySystemConditionName'}},
		{header: 'Modify Condition', width: 150, dataIndex: 'contentEntityModifyCondition.name', filter: {searchFieldName: 'modifyCondition'}, tooltip: 'For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit definitions of this type.', hidden: true},
		{header: 'Content Type', dataIndex: 'contentSystemBean.type.name', width: 80, filter: {type: 'combo', searchFieldName: 'contentSystemBeanTypeId', displayField: 'name', url: 'systemBeanTypeListFind.json'}},
		{header: 'Export Content', dataIndex: 'contentSystemBean.nameWithoutAutoGeneratedPrefix', width: 200, filter: {type: 'combo', searchFieldName: 'contentSystemBeanId', displayField: 'nameWithoutAutoGeneratedPrefix', url: 'systemBeanListFind.json'}},
		{header: 'Skipped Lines', dataIndex: 'numberOfRowsToSkip', width: 80, type: 'int', useNull: true, hidden: true},
		{header: 'Disabled', dataIndex: 'disabled', type: 'boolean', width: 35},
		{
			header: 'Generate File', dataIndex: 'notGenerateFile', type: 'boolean', width: 50, filter: {searchFieldName: 'generateFile'},
			convert: v => !v
		},
		{header: 'Context Bean', dataIndex: 'contextSystemBean.name', width: 100, filter: {searchFieldName: 'contextBeanName'}, hidden: true},
		{header: 'Active', dataIndex: 'definition.active', width: 20, type: 'boolean', sortable: false, filter: false, hidden: true},

		{header: 'History Table', dataIndex: 'historySystemTable.name', width: 80, filter: {searchFieldName: 'historyTable'}, hidden: true},
		{header: 'History PK Column', dataIndex: 'historyPKColumnName', width: 80, hidden: true},
		{header: 'History Days To Include', dataIndex: 'historyDaysToInclude', width: 80, type: 'int', useNull: true, hidden: true},
		{header: 'History Days To Keep', dataIndex: 'historyDaysToKeep', width: 80, type: 'int', useNull: true, hidden: true}

	]
});
Ext.reg('export-definition-content-grid', Clifton.export.ExportDefinitionContentGrid);

Clifton.export.SendCustomFileWindow = Ext.extend(TCG.app.DetailWindow, {
	title: 'Send Custom File to Destination',
	iconCls: 'import',
	modal: true,
	enableShowInfo: false,
	hideApplyButton: true,
	height: 250,
	width: 600,
	items: [{
		xtype: 'formpanel',
		loadValidation: false,
		fileUpload: true,
		labelWidth: 100,
		instructions: 'Select a file to send to this destination.<p>The result of the send can be found in <b>Tools > Integration System > Outgoing Integration</b>.',
		items: [
			{name: 'destinationId', xtype: 'integerfield', hidden: true},
			{fieldLabel: 'Destination', name: 'destinationName', readOnly: true, submitValue: false},
			{fieldLabel: 'External File', name: 'file', xtype: 'fileuploadfield'}
		],

		getSaveURL: function() {
			return 'exportSendCustomFileUpload.json';
		}
	}]
});
