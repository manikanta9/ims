Clifton.export.content.transformation.ExportTransformationWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Export Transformation',
	iconCls: 'export',
	//width: 750,
	//height: 500,

	items: [{
		xtype: 'formpanel',
		instructions: 'Export Types are used for classification purposes to group definitions into categories.',
		url: 'exportTransformation.json',
		labelWidth: 130,
		items: [
			{fieldLabel: 'Export Content', name: 'content.fileNameTemplate', detailIdField: 'content.id', xtype: 'linkfield', detailPageClass: 'Clifton.export.content.ExportContentWindow'},
			{
				fieldLabel: 'Transformation Type', name: 'transformationSystemBean.type.name', hiddenName: 'transformationSystemBean.type.id', xtype: 'combo', url: 'systemBeanTypeListFind.json?groupName=Export Transformation Generator',
				detailPageClass: 'Clifton.system.bean.TypeWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Export Transformation Generator', alias: 'Transformation Type'}}};
				},
				listeners: {
					select: function(combo, record, index) {
						const f = combo.getParentForm().getForm();
						f.findField('transformationSystemBean.id').clearAndReset();
					}
				}
			},
			{
				fieldLabel: 'Transformation Bean', name: 'transformationSystemBean.name', hiddenName: 'transformationSystemBean.id', xtype: 'combo', url: 'systemBeanListFind.json?groupName=Export Transformation Generator',
				detailPageClass: 'Clifton.system.bean.BeanWindow',
				getDefaultData: function() {
					return {type: {group: {name: 'Export Transformation Generator', alias: 'Transformation Generator'}}};
				},
				listeners: {
					beforequery: function(queryEvent) {
						const f = queryEvent.combo.getParentForm().getForm();
						const transformationTypeId = f.findField('transformationSystemBean.type.id').value;
						if (!transformationTypeId) {
							f.findField('transformationSystemBean.id').clearAndReset();
						}

						queryEvent.combo.store.baseParams = {
							typeId: transformationTypeId
						};
					},
					select: function(combo, record, index) {
						combo.getParentForm().setFormValue('transformationSystemBean.description', record.json.description, true);
					}
				}
			},
			{name: 'transformationSystemBean.description', xtype: 'textarea', readOnly: true},
			{fieldLabel: 'Execution Order', name: 'order', xtype: 'spinnerfield'}
		]
	}]
});
