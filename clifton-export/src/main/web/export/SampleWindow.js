Clifton.export.SampleWindow = function(conf) {
	this.win = new TCG.app.Window(Ext.apply({
		title: 'Export Sample',
		width: 600,
		height: 400,
		buttons: [{
			text: 'Close',
			tooltip: 'Close this window',
			handler: function() {
				this.ownerCt.ownerCt.closeWindow();
			}
		}],

		closeWindow: function() {
			const win = this;
			win.closing = true;
			win.close();
		},

		items: [{
			xtype: 'formpanel',
			loadValidation: false,
			instructions: 'The following is a sample export generated for selected Export Definition, Subject, & Text templates.',
			url: 'exportSample.json',
			items: [
				{fieldLabel: 'Definition', name: 'definition.label', xtype: 'displayfield'},

				{fieldLabel: 'Export Date', name: 'createDate', xtype: 'displayfield'},
				{fieldLabel: 'Subject', name: 'subject', xtype: 'displayfield'},
				{fieldLabel: 'Text', name: 'text', xtype: 'displayfield'}
			]
		}]
	}, conf));
};
