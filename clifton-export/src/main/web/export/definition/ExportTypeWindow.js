Clifton.export.definition.ExportTypeWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Export Type',
	iconCls: 'export',
	width: 750,
	height: 500,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [{
			title: 'Type',
			items: [{
				xtype: 'formpanel',
				instructions: 'Export Types are used for classification purposes to group definitions into categories.',
				url: 'exportDefinitionType.json',
				labelWidth: 130,
				items: [
					{fieldLabel: 'Type Name', name: 'name'},
					{fieldLabel: 'Description', name: 'description', xtype: 'textarea'},
					{fieldLabel: 'Modify Condition', name: 'entityModifyCondition.label', hiddenName: 'entityModifyCondition.id', displayField: 'name', xtype: 'system-condition-combo', qtip: 'For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit definitions of this type.'},
					{fieldLabel: 'Send Condition', name: 'sendCondition.label', hiddenName: 'sendCondition.id', displayField: 'name', xtype: 'system-condition-combo', qtip: 'Must evaluate to true prior to sending an export for the definition of this type'},
					{
						fieldLabel: 'E-mail Signature', name: 'emailSignature', xtype: 'htmleditor', height: 150,
						plugins: [new TCG.form.HtmlEditorFreemarker()]
					}
				]
			}]
		}]
	}]
});
