Clifton.export.definition.RunHistoryWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Export Run History',
	height: 500,
	width: 1000,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Overview',
				items: [{
					xtype: 'formpanel',
					url: 'exportRunHistory.json',
					labelFieldName: 'definition.name',
					readOnly: true,

					items: [
						{fieldLabel: 'Export Definition', name: 'definition.name', xtype: 'linkfield', detailPageClass: 'Clifton.export.definition.ExportDefinitionWindow', detailIdField: 'definition.id'},
						{fieldLabel: 'Run Type', name: 'exportRunSourceType'},
						{fieldLabel: 'Status', name: 'status.exportRunStatusName'},
						{fieldLabel: 'Scheduled Time', name: 'scheduledDate'},
						{fieldLabel: 'Start Time', name: 'startDate'},
						{fieldLabel: 'End Time', name: 'endDate'},
						{fieldLabel: 'Retry Attempt', name: 'retryAttemptNumber'},
						{fieldLabel: 'Message', name: 'description', xtype: 'textarea', anchor: '-35 -200'}
					]
				}]
			},


			{
				title: 'Details',
				items: [{
					xtype: 'integration-outgoing-exportGrid',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Source Definition', dataIndex: 'sourceSystemDefinitionName', width: 60},
						{header: 'Export Source', width: 50, dataIndex: 'integrationSource.label', filter: {searchFieldName: 'integrationSourceId', type: 'combo', url: 'integrationSourceListFind.json'}},
						{header: 'Source Identifier', dataIndex: 'sourceSystemIdentifier', width: 40, hidden: true},
						{header: 'Destination Type', dataIndex: 'destinationType.name', width: 50},
						{
							header: 'Status', width: 60, dataIndex: 'status.name', filter: {type: 'list', options: [['RECEIVED', 'RECEIVED'], ['PROCESSED', 'PROCESSED'], ['REPROCESSED', 'REPROCESSED'], ['FAILED', 'FAILED']]},
							renderer: function(v, p, r) {
								return (Clifton.integration.outgoing.renderStatus(v));
							}
						},
						{header: 'Message Subject', dataIndex: 'messageSubject', width: 100},
						{header: 'Retry Count', dataIndex: 'retryCount', width: 60, type: 'int', title: 'Number of times to retry in case of failure', hidden: true, useNull: true},
						{header: 'Retry Delay In Seconds', dataIndex: 'retryDelayInSeconds', width: 60, type: 'int', title: 'Time in seconds before a retry is attempted again', hidden: true, useNull: true},
						{header: 'Received Date', width: 70, dataIndex: 'receivedDate', type: 'date', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Sent Date', width: 70, dataIndex: 'sentDate', type: 'date'}
					],
					getLoadParams: function() {
						return {'integrationSourceName': 'IMS', 'sourceSystemIdentifier': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Content History',
				items: [{
					xtype: 'export-definition-content-history-grid',
					instructions: 'A list of entities that have been sent as part of this export run. Used in cases where we need to track what was already sent to avoid sending the same data more than once.',
					getLoadParams: function() {
						return {'exportRunHistoryId': this.getWindow().getMainFormId()};
					}
				}]
			}
		]
	}]
});
