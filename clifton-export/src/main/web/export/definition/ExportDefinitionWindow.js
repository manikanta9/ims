Clifton.export.definition.ExportDefinitionWindow = Ext.extend(TCG.app.DetailWindow, {
	titlePrefix: 'Export Definition',
	iconCls: 'export',
	width: 1200,
	height: 800,
	enableRefreshWindow: true,

	items: [{
		xtype: 'tabpanel',
		requiredFormIndex: 0,
		items: [
			{
				title: 'General',
				tbar: [
					{
						text: 'Send Now',
						tooltip: 'Generate and Send Export Now',
						iconCls: 'run',
						getWindow: function() {
							let result = this.findParentByType(Ext.Window);
							if (TCG.isNull(result)) {
								result = this.findParentBy(function(o) {
									return o.baseCls === 'x-window';
								});
							}
							return result;
						},
						handler: function() {
							const w = this.getWindow();
							const f = w.getMainFormPanel();
							if (w.isModified() || !w.isMainFormSaved()) {
								TCG.showError('Please save your changes before running the export.');
								return false;
							}

							Ext.Msg.confirm('Confirm Export Run', 'Are you sure you want to run this export?', function(a) {
								if (a === 'yes') {
									const params = {id: w.getMainFormId()};
									const loader = new TCG.data.JsonLoader({
										waitTarget: f,
										waitMsg: 'Running...',
										timeout: 60000,
										params: params,
										conf: params,
										onLoad: function(record, conf) {
											Ext.Msg.alert('Status', 'Export has been scheduled to run now and will be completed shortly.');
										}
									});
									loader.load('exportRunManual.json');
								}
							});
						}
					},
					'-',
					{
						text: 'Preview File(s) On',
						tooltip: 'Generates the export file(s) and downloads them through the browser without actually sending them.<p><p>' +
							'<b>If a preview date is specified, the DATA will be generated for the selected date.</b><p><p>' +
							'<b>Note:</b> Some of the data in previewed export files may differ from the actual export files when they are sent, such as message IDs and timestamps.',
						iconCls: 'run',
						handler: function() {
							const w = this.getWindow();
							const previewDate = TCG.getChildByName(this.ownerCt, 'previewDate');
							let formattedPreviewDate = '';
							if (TCG.isNotBlank(previewDate.getValue())) {
								formattedPreviewDate = previewDate.getValue().format('m/d/Y');
							}
							TCG.downloadFile('exportPreviewDownload.json', {id: w.getMainFormId(), exceptionIfNoResults: true, previewDate: formattedPreviewDate}, this);
						},
						getWindow: function() {
							let result = this.findParentByType(Ext.Window);
							if (TCG.isNull(result)) {
								result = this.findParentBy(function(o) {
									return o.baseCls === 'x-window';
								});
							}
							return result;
						}
					},
					{xtype: 'toolbar-datefield', name: 'previewDate'}
				],
				items: {
					xtype: 'formpanel',
					url: 'exportDefinition.json',
					labelWidth: 150,
					listeners: {
						afterload: function(panel) {
							const form = panel.getForm();
							const strategyName = form.findField('archiveStrategyName').value;
							if (strategyName) {
								//Unique index on strategy name - should never return more than one
								const strategies = TCG.data.getData('integrationFileArchiveStrategyListFind.json?name=' + strategyName, panel);
								if (strategies) {
									form.findField('archiveStrategy.name').setValue(strategies[0].name);
									form.findField('archiveStrategy.id').setValue(strategies[0].id);
								}
							}
						},
						afterRender: function(panel) {
							console.log('abc');
							const monitored = TCG.getChildByName(panel, 'failureMonitored').getValue();
							TCG.getChildByName(panel, 'priority.name').allowBlank = !monitored;
							TCG.getChildByName(panel, 'description').allowBlank = !monitored;
						}
					},
					items: [
						{fieldLabel: 'Export Type', name: 'type.name', hiddenName: 'type.id', xtype: 'combo', displayField: 'name', url: 'exportDefinitionTypeListFind.json', detailPageClass: 'Clifton.export.definition.ExportTypeWindow'},
						{fieldLabel: 'Export Name', name: 'name'},
						{fieldLabel: 'Description', name: 'description', xtype: 'textarea', height: 50, grow: true, allowBlank: false},
						{
							xtype: 'system-entity-modify-condition-combo',
							name: 'definitionEntityModifyCondition.name',
							hiddenName: 'definitionEntityModifyCondition.id'
						},
						{fieldLabel: 'Schedule', name: 'recurrenceSchedule.name', hiddenName: 'recurrenceSchedule.id', xtype: 'combo', url: 'calendarScheduleListFind.json?calendarScheduleTypeName=Standard Schedules&systemDefined=false', detailPageClass: 'Clifton.calendar.schedule.ScheduleWindow'},
						{fieldLabel: 'Status', name: 'status.name', xtype: 'displayfield'},
						{xtype: 'label', html: '<hr/>'},
						{
							xtype: 'panel', layout: 'column', defaults: {layout: 'form'},
							items: [
								{
									columnWidth: .50,
									labelWidth: 150,
									defaults: {anchor: '0'},
									items: [
										{fieldLabel: 'Start Date', name: 'startDate', xtype: 'datefield'},
										{fieldLabel: 'End Date', name: 'endDate', xtype: 'datefield'},
										{fieldLabel: 'Origin Name', name: 'originName', xtype: 'textfield', qtip: 'A free text name of the origin of the export used for alerting if there is a file failure.  For example, IMS Security Master or APX Positions.'},
										{fieldLabel: 'Destination Name', name: 'destinationName', xtype: 'textfield', qtip: 'A free text name of the destination of the export used for alerting if there is a file failure.  For example, MSIM Position Data or AWS Position Service.'},
										{
											boxLabel: 'Monitor Failures (notify support team)', name: 'failureMonitored', xtype: 'checkbox', qtip: 'Notify support team of failed runs for this export.',
											onClick: function(event) {
												if (event.type === 'change') {
													const formPanel = TCG.getParentFormPanel(this);
													const priorityField = TCG.getChildByName(formPanel, 'priority.name');
													const descriptionField = TCG.getChildByName(formPanel, 'description');
													priorityField.allowBlank = !this.getValue();
													descriptionField.allowBlank = !this.getValue();
													formPanel.fireEvent('change', formPanel);
												}
											}
										},
										{boxLabel: 'Data Expected (if no content is generated, treat the run as a Failure)', name: 'dataExpected', xtype: 'checkbox', qtip: 'The export should expect content every time it is run. If no content is generated then it will be treated as a failure.'}
									]
								},
								{
									columnWidth: .50,
									labelWidth: 130,
									defaults: {anchor: '0'},
									style: {paddingLeft: '20px'},
									items: [
										{fieldLabel: 'Export Priority', name: 'priority.name', hiddenName: 'priority.id', xtype: 'combo', url: 'systemPriorityListFind.json', allowBlank: false},
										{fieldLabel: 'Owner User', name: 'ownerUser.label', hiddenName: 'ownerUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json', detailPageClass: 'Clifton.security.user.UserWindow'},
										{fieldLabel: 'Owner Group', name: 'ownerGroup.label', hiddenName: 'ownerGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json', detailPageClass: 'Clifton.security.user.GroupWindow'},
										{fieldLabel: 'Support User', name: 'supportUser.label', hiddenName: 'supportUser.id', xtype: 'combo', displayField: 'label', url: 'securityUserListFind.json', detailPageClass: 'Clifton.security.user.UserWindow', qtip: 'The user responsible for supporting the export in case of failures.'},
										{fieldLabel: 'Support Group', name: 'supportGroup.label', hiddenName: 'supportGroup.id', xtype: 'combo', displayField: 'label', url: 'securityGroupListFind.json', detailPageClass: 'Clifton.security.user.GroupWindow', qtip: 'The user group responsible for supporting the export in case of failures.'}
									]
								}
							]
						},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Archive Strategy', name: 'archiveStrategy.name', hiddenName: 'archiveStrategy.id', xtype: 'combo', displayField: 'name', url: 'integrationFileArchiveStrategyListFind.json', detailPageClass: 'Clifton.integration.file.FileArchiveStrategyWindow', qtip: 'The name of the File Archive Strategy that should be applied to this definition.', disableAddNewItem: true, submitValue: false,
							listeners: {
								change: function(field) {
									const fp = field.getParentForm();
									const strategyNameField = fp.getForm().findField('archiveStrategyName');
									const val = field.getValueObject();
									if (val) {
										strategyNameField.setValue(val.name);
									}
									else {
										strategyNameField.setValue('');
									}
								}
							}
						},
						{fieldLabel: 'Archive Strategy Name', name: 'archiveStrategyName', hidden: true},
						{xtype: 'label', html: '<hr/>'},
						{fieldLabel: 'Retry Count', name: 'retryCount', xtype: 'integerfield', qtip: 'If the content is not ready (Content Ready condition evaluates to FALSE), then one can optionally specify how many times to retry checking if the content is ready and how long to wait.'},
						{fieldLabel: 'Retry Delay in Seconds', name: 'retryDelayInSeconds', xtype: 'integerfield'},
						{fieldLabel: 'Max Execution Time (ms)', name: 'maxExecutionTimeMillis', xtype: 'integerfield', qtip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'File Name Suffix', name: 'fileNameSuffixTemplate', qtip: 'Optional global suffix that will be appended to file names of files generated from this Export Definition. ' +
								'Supports FreeMarker templating. \'DATE\' and \'DAILY_RUN_COUNT\' are currently the only available variables.'
						},
						{boxLabel: 'Zip all files', name: 'zipFilesForExport', xtype: 'checkbox', qtip: 'If enabled, indicates that the output files will be zipped into a single archive before being sent to the destination(s).'},
						{fieldLabel: 'Zipped File Name', name: 'zippedFileNameTemplate', qtip: 'The file name pattern for the zipped file. Supports FreeMarker templating. \'DATE\' and \'DAILY_RUN_COUNT\' are currently the only available variables.'},
						{xtype: 'label', html: '<hr/>'},
						{
							fieldLabel: 'Effective Date Generation', name: 'effectiveDateGenerationOption', displayField: 'name', hiddenName: 'effectiveDateGenerationOption', valueField: 'value', xtype: 'combo', mode: 'local',
							store: {xtype: 'arraystore', fields: ['name', 'value', 'description'], data: Clifton.calendar.date.DATE_GENERATION_OPTIONS},
							qtip: 'Optional setting that specifies how to calculate the value of the \'EFFECTIVE_DATE\' variable. Only required if \'EFFECTIVE_DATE\' variable is used.'
						},
						{fieldLabel: 'Days From Today', name: 'effectiveDateDaysFromToday', xtype: 'integerfield', qtip: 'The number associated with \'Effective Date Generation Option\' (if applicable).'}
					]
				}
			},


			{
				title: 'Destinations',
				items: [{
					xtype: 'gridpanel',
					name: 'exportDefinitionDestinationListFind',
					instructions: 'Destinations define/configure where Exports are to be sent: Email, FTP, FAX, etc.  The same content can be sent to more than one destination.',
					additionalPropertiesToRequest: 'destinationName',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{
							header: 'Export Destination', dataIndex: 'destinationSystemBean.nameWithoutAutoGeneratedPrefix', width: 250, filter: {sortFieldName: 'destinationName', searchFieldName: 'destinationName'},
							renderer: function(v, c, r) {
								if (r.data.sharedDestination) {
									return r.json.destinationName;
								}
								return v;
							}
						},
						{header: 'Description', dataIndex: 'description', width: 250, hidden: true},
						{header: 'Destination Type', dataIndex: 'destinationSystemBean.type.name', width: 70},
						{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', tooltip: 'For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit definitions of this type.', hidden: true},
						{header: 'Disabled', dataIndex: 'disabled', type: 'boolean', width: 40},
						{header: 'Shared', dataIndex: 'sharedDestination', type: 'boolean', width: 40}
					],
					getLoadParams: function(firstLoad) {
						return {definitionId: this.getWindow().getMainFormId()};
					},
					showCustomFileSendWindow: function(destination) {
						TCG.createComponent('Clifton.export.SendCustomFileWindow', {
							defaultData: {
								destinationId: destination.data['id'],
								destinationName: destination.data['destinationSystemBean.nameWithoutAutoGeneratedPrefix']
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.export.destination.DestinationWindow',
						getDefaultData: function(gridPanel) {
							const definition = gridPanel.getWindow().getMainForm().formValues;
							return {
								definition: {id: definition.id, name: definition.name}
							};
						},
						getDeleteURL: function() {
							return 'exportDefinitionDestinationAndLinkDelete.json';
						},
						getDeleteParams: function(selectionModel) {
							const destinationId = selectionModel.getSelected().id;
							const definitionId = this.getWindow().getMainForm().formValues.id;

							return {
								destinationId: destinationId,
								definitionId: definitionId
							};
						},
						addEditButtons: function(t, gp) {
							const sm = gp.grid.getSelectionModel();
							t.add(new TCG.form.ComboBox({
								name: 'destinationName',
								displayField: 'destinationName',
								url: 'exportDefinitionDestinationListFind.json?sharedDestination=true',
								emptyText: '< Select Shared Destination >',
								width: 200,
								listWidth: 250
							}));
							t.add({
								text: 'Add Selected',
								xtype: 'button',
								tooltip: 'Add a destination',
								iconCls: 'add',
								scope: this,
								handler: function() {
									const sharedDestination = TCG.getChildByName(t, 'destinationName');
									const sharedDestinationId = sharedDestination.getValue();
									if (TCG.isBlank(sharedDestinationId)) {
										TCG.showError('You must first select a shared destination from the list.');
									}
									else {
										const definitionId = gp.getWindow().getMainFormId();
										const loader = new TCG.data.JsonLoader({
											waitTarget: gp,
											waitMsg: 'Adding...',
											params: {destinationId: sharedDestinationId, definitionId: definitionId},
											onLoad: function(record, conf) {
												gp.reload();
												TCG.getChildByName(t, 'destinationName').reset();
											}
										});
										loader.load('exportDefinitionDestinationLinkSave.json');
									}
								}
							});
							t.add('-');
							TCG.grid.GridEditor.prototype.addToolbarAddButton.apply(this, arguments);
							TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
							t.add({
								text: 'Send File',
								tooltip: 'Send a custom file to the selected destination. This feature should only be used in special cases, such as when testing that a file can be sent to a destination.',
								iconCls: 'upload',
								scope: this,
								handler: function() {
									const selection = sm.getSelections()[0];
									if (selection) {
										if (selection.data['disabled']) {
											Ext.Msg.confirm('Confirm File Send (Destination is disabled)', 'The selected destination is currently disabled on this export. ' +
												'Are you sure you want to proceed with selecting a custom file to send to this destination?', function(a) {
												if (a === 'yes') {
													gp.showCustomFileSendWindow(selection);
												}
											});
										}
										else {
											gp.showCustomFileSendWindow(selection);
										}
									}
									else {
										TCG.showError('Please select a destination to send the file to.', 'Incorrect Selection');
									}
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Content',
				items: [{
					xtype: 'export-definition-content-grid',
					getLoadParams: function(firstLoad) {
						return {definitionId: this.getWindow().getMainFormId()};
					},
					editor: {
						detailPageClass: 'Clifton.export.content.ExportContentWindow',
						getDefaultData: function(gridPanel) {
							const definition = gridPanel.getWindow().getMainForm().formValues;
							return {
								definition: {id: definition.id, name: definition.name}
							};
						}
					}
				}]
			},


			{
				title: 'Dynamic Configuration',
				items: [{
					xtype: 'formwithdynamicfields',
					url: 'exportDefinition.json',
					instructions: 'Optionally create a custom System Bean that will dynamically generate configuration(s) for a specific export run.  For example, when exporting trades, we may need to create a separate file for each Client Account and Trade Type.  Etc.  If this is necessary, select Configuration Type from the list of available options and populate corresponding parameters.',
					labelFieldName: 'dynamicConfigurationSystemBean.nameWithoutAutoGeneratedPrefix',
					labelWidth: 170,

					dynamicTriggerFieldName: 'dynamicConfigurationSystemBean.type.name',
					dynamicTriggerValueFieldName: 'dynamicConfigurationSystemBean.type.id',
					dynamicFieldTypePropertyName: 'type',
					dynamicFieldListPropertyName: 'dynamicConfigurationSystemBean.propertyList',
					dynamicFieldsUrl: 'systemBeanPropertyTypeListByBean.json',
					dynamicFieldsUrlParameterName: 'type.id',

					dynamicFieldsUrlIdFieldName: 'dynamicConfigurationSystemBean.id',
					dynamicFieldsUrlListParameterName: 'propertyList',
					getLoadParams: function(firstLoad) {
						return {id: this.getWindow().getMainFormId()};
					},
					getLoadURL: function() {
						return 'exportDefinitionPopulated.json';
					},
					appendSubmitParams: function(submitParams) {
						if (submitParams && !submitParams.id) {
							submitParams.id = this.getWindow().getMainFormId();
						}
						return submitParams;
					},
					items: [
						{
							fieldLabel: 'Configuration Type', name: 'dynamicConfigurationSystemBean.type.name', hiddenName: 'dynamicConfigurationSystemBean.type.id', xtype: 'combo', allowBlank: false,
							url: 'systemBeanTypeListFind.json?groupName=Export Definition Dynamic Configuration Generator', detailPageClass: 'Clifton.system.bean.TypeWindow',
							listeners: {
								// reset object bean property fields on changes to bean type (triggerField)
								select: function(combo, record, index) {
									combo.ownerCt.resetDynamicFields(combo, false);
								}
							}
						}
					]
				}]
			},


			{
				title: 'Content History',
				items: [{
					xtype: 'export-definition-content-history-grid',
					getLoadParams: function() {
						return {'exportDefinitionId': this.getWindow().getMainFormId()};
					}
				}]
			},


			{
				title: 'Run History',
				items: [{
					name: 'exportRunHistoryListFind',
					xtype: 'gridpanel',
					instructions: 'History of runs for batched exports for selected definition.',
					additionalPropertiesToRequest: 'id|definition.maxExecutionTimeMillis',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Scheduled', width: 80, dataIndex: 'scheduledDate', hidden: true},
						{header: 'Started', width: 80, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Ended', width: 80, dataIndex: 'endDate'},
						{
							header: 'Duration', width: 50, dataIndex: 'executionTimeFormatted', filter: {type: 'int', sortFieldName: 'executionTimeInSeconds', searchFieldName: 'executionTimeInSeconds'},
							renderer: function(v, metaData, r) {
								return TCG.renderMaxSLA(v, TCG.getValue('executionTimeInMillis', r.json), TCG.getValue('definition.maxExecutionTimeMillis', r.json), metaData);
							}
						},
						{header: 'Duration In Millis', width: 60, dataIndex: 'executionTimeInMillis', type: 'int', hidden: true},
						{
							header: 'Run Type', width: 70, dataIndex: 'exportRunSourceType', filter: {type: 'list', options: Clifton.export.ExportRunSourceTypes},
							renderer: Clifton.export.renderExportRunSourceType
						},
						{
							header: 'Status', width: 75, dataIndex: 'status.name', filter: {searchFieldName: 'statusNameList', type: 'list', options: Clifton.export.StatusOptions},
							renderer: function(v, p, r) {
								return (Clifton.export.renderStatus(v));
							}
						},
						{header: 'Retry', width: 40, dataIndex: 'retryAttemptNumber', type: 'int', useNull: true},
						{header: 'Message', width: 250, dataIndex: 'description'}
					],
					getLoadParams: function(firstLoad) {
						if (firstLoad) {
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -90)});
						}
						return {definitionId: this.getWindow().getMainFormId()};
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.export.definition.RunHistoryWindow',
						addEditButtons: function(t) {
							const definitionId = this.getWindow().getMainFormId();
							t.add({
								text: 'Clear History (Admin Only)',
								tooltip: 'Delete all export history entries.',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const editor = this;
									const grid = this.grid;
									Ext.Msg.confirm('Delete Selected Row', 'Would you like to clear the run history?', function(a) {
										if (a === 'yes') {
											const loader = new TCG.data.JsonLoader({
												waitTarget: grid.ownerCt,
												waitMsg: 'Deleting...',
												params: {definitionId: definitionId},
												onLoad: function(record, conf) {
													editor.getGridPanel().reload();
												}
											});
											const url = 'exportRunHistoryForDefinitionDelete.json';
											loader.load(url);
										}
									});
								}
							});
							t.add('-');
							t.add({
								text: 'Fix Runs',
								tooltip: 'Set selected runs to ERROR status',
								iconCls: 'tools',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const rows = grid.getSelectionModel().getSelections();
									if (rows.length === 0) {
										TCG.showError('Please select a run history to fix.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Fix Selected Run Histories', 'Would you like to set the selected run histories to FAILED_ERROR status?', function(a) {
											if (a === 'yes') {
												const ids = [];
												for (let i = 0; i < rows.length; i++) {
													ids.push(rows[i].data.id);
												}
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Fixing...',
													params: {runHistoryIds: ids},
													onLoad: function(record, conf) {
														grid.ownerCt.reload();
													},
													onFailure: function() {
														grid.ownerCt.reload();
													}
												});
												loader.load('exportRunHistoryFix.json');
											}
										});
									}
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Tasks',
				items: [{
					xtype: 'workflow-task-grid',
					tableName: 'ExportDefinition'
				}]
			}
		]
	}]
});
