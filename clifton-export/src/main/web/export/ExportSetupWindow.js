Clifton.export.ExportSetupWindow = Ext.extend(TCG.app.Window, {
	id: 'exportSetupWindow',
	title: 'Export Setup',
	iconCls: 'export',
	width: 1500,
	height: 700,

	items: [{
		xtype: 'tabpanel',
		items: [
			{
				title: 'Export Definitions',
				items: [{
					xtype: 'export-definition-grid'
				}]
			},


			{
				title: 'Run History',
				items: [{
					name: 'exportRunHistoryListFind',
					xtype: 'gridpanel',
					additionalPropertiesToRequest: 'id|definition.id|definition.type.name|definition.priority.name|definition.priority.cssStyle|definition.maxExecutionTimeMillis',
					instructions: 'History of runs for sent exports for all export definitions. Each run can be sent manually or when scheduled.',
					getTopToolbarFilters: function(toolbar) {
						return [
							{fieldLabel: 'Export Type', xtype: 'toolbar-combo', name: 'exportDefinitionTypeId', width: 150, url: 'exportDefinitionTypeListFind.json'},
							{
								fieldLabel: 'Display', xtype: 'combo', name: 'status', width: 110, minListWidth: 150, forceSelection: true, mode: 'local', value: 'ALL',
								store: {
									xtype: 'arraystore',
									data: Clifton.export.DisplayOptions
								},
								listeners: {
									select: function(field) {
										TCG.getParentByClass(field, Ext.Panel).reload();
									}
								}
							}];
					},
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Export Definition', width: 200, dataIndex: 'definition.name', filter: {searchFieldName: 'exportDefinitionName'}},
						{header: 'Export Type', width: 90, dataIndex: 'definition.type.name', filter: {type: 'combo', searchFieldName: 'exportDefinitionTypeId', url: 'exportDefinitionTypeListFind.json'}, hidden: true},
						{header: 'Scheduled', width: 80, dataIndex: 'scheduledDate', hidden: true},
						{header: 'Started', width: 80, dataIndex: 'startDate', defaultSortColumn: true, defaultSortDirection: 'DESC'},
						{header: 'Ended', width: 80, dataIndex: 'endDate', hidden: true},
						{
							header: 'Duration', width: 60, dataIndex: 'executionTimeFormatted', filter: {type: 'int', sortFieldName: 'executionTimeInSeconds', searchFieldName: 'executionTimeInSeconds'},
							renderer: function(v, metaData, r) {
								return TCG.renderMaxSLA(v, TCG.getValue('executionTimeInMillis', r.json), TCG.getValue('definition.maxExecutionTimeMillis', r.json), metaData);
							}
						},
						{header: 'Duration In Millis', width: 60, dataIndex: 'executionTimeInMillis', type: 'int', hidden: true},
						{header: 'Max Time Millis', width: 60, dataIndex: 'definition.maxExecutionTimeMillis', type: 'int', useNull: true, filter: {searchFieldName: 'maxExecutionTimeMillis'}, hidden: true, tooltip: 'Defines optional SLA execution time in milliseconds. Corresponding UI will highlight exceptions.'},
						{
							header: 'Run Type', width: 60, dataIndex: 'exportRunSourceType', filter: {type: 'list', options: Clifton.export.ExportRunSourceTypes},
							renderer: Clifton.export.renderExportRunSourceType
						},
						{
							header: 'Status', width: 80, dataIndex: 'status.name', filter: {searchFieldName: 'statusNameList', type: 'list', options: Clifton.export.StatusOptions},
							renderer: Clifton.export.renderStatus
						},
						{
							header: 'Priority', width: 25, dataIndex: 'definition.priority.name', filter: {searchFieldName: 'priorityId', type: 'combo', url: 'systemPriorityListFind.json'},
							renderer: function(v, c, r) {
								const style = r.json.definition.priority ? r.json.definition.priority.cssStyle : '';
								if (TCG.isNotBlank(style)) {
									c.attr = 'style="' + style + '"';
								}
								return v;
							}
						},

						{header: 'Owner', dataIndex: 'definition.ownerLabel', filter: {searchFieldName: 'ownerUserOrGroupName'}, sortable: false, width: 50},
						{header: 'Owner User', dataIndex: 'definition.ownerUser.label', width: 50, idDataIndex: 'definition.ownerUser.id', filter: {searchFieldName: 'ownerUserId', displayField: 'label', type: 'combo', url: 'securityUserListFind.json'}, hidden: true},
						{header: 'Owner Group', dataIndex: 'definition.ownerGroup.label', width: 50, idDataIndex: 'definition.ownerGroup.id', filter: {searchFieldName: 'ownerGroupId', type: 'combo', url: 'securityGroupListFind.json'}, hidden: true},
						{header: 'Retry Attempt', width: 50, dataIndex: 'retryAttemptNumber', type: 'int', useNull: true},
						{header: 'Origin Name', width: 150, dataIndex: 'definition.originName', hidden: true},
						{header: 'Destination Name', width: 150, dataIndex: 'definition.destinationName', hidden: true},
						{header: 'Data Expected', dataIndex: 'definition.dataExpected', filter: {searchFieldName: 'dataExpected'}, width: 20, type: 'boolean', hidden: true},
						{header: 'Failure Monitored', dataIndex: 'definition.failureMonitored', filter: {searchFieldName: 'failureMonitored'}, width: 20, type: 'boolean', hidden: true},
						{header: 'Message', width: 200, dataIndex: 'description'}
					],
					getLoadParams: function(firstLoad) {
						const loadParams = {};
						if (firstLoad) {
							this.setFilterValue('startDate', {'after': new Date().add(Date.DAY, -30)});
						}

						const t = this.getTopToolbar();

						const status = TCG.getChildByName(t, 'status');
						const v = status.getValue();
						const displayFilter = Clifton.export.getDisplayFilter(v);

						if (TCG.isNull(displayFilter.statusFilter)) {
							this.clearFilter('status.name', true);
						}
						else {
							this.setFilterValue('status.name', displayFilter.statusFilter, false, true);
						}

						if (TCG.isNotBlank(displayFilter.ownerFilter)) {
							loadParams.ownedByUserId = displayFilter.ownerFilter;
						}

						const exportType = TCG.getChildByName(t, 'exportDefinitionTypeId');
						if (TCG.isNotNull(exportType.getValue())) {
							this.setFilterValue('definition.type.name', exportType.getValue(), false, true);
						}
						else {
							this.clearFilter('definition.type.name', true);
						}

						return loadParams;
					},
					editor: {
						drillDownOnly: true,
						detailPageClass: 'Clifton.export.definition.RunHistoryWindow',
						addEditButtons: function(t) {
							t.add('-');
							t.add({
								text: 'Fix Runs',
								tooltip: 'Set selected runs to ERROR status',
								iconCls: 'tools',
								scope: this,
								handler: function() {
									const grid = this.grid;
									const rows = grid.getSelectionModel().getSelections();
									if (rows.length === 0) {
										TCG.showError('Please select a run history to fix.', 'No Row(s) Selected');
									}
									else {
										Ext.Msg.confirm('Fix Selected Run Histories', 'Would you like to set the selected run histories to FAILED_ERROR status?', function(a) {
											if (a === 'yes') {
												const ids = [];
												for (let i = 0; i < rows.length; i++) {
													ids.push(rows[i].data.id);
												}
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Fixing...',
													params: {runHistoryIds: ids},
													onLoad: function(record, conf) {
														grid.ownerCt.reload();
													},
													onFailure: function() {
														grid.ownerCt.reload();
													}
												});
												loader.load('exportRunHistoryFix.json');
											}
										});
									}
								}
							});
							t.add('-');
						}
					}
				}]
			},


			{
				title: 'Scheduled Runs',
				items: [{
					xtype: 'core-scheduled-runner-grid',
					typeName: 'EXPORT'
				}]
			},


			{
				title: 'Export Types',
				items: [{
					name: 'exportDefinitionTypeListFind',
					xtype: 'gridpanel',
					instructions: 'Export Definition Types categorise export definitions and can also specify defaults as well as restrict access to all definitions of a specific type.',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Type Name', width: 75, dataIndex: 'name'},
						{header: 'Description', width: 200, dataIndex: 'description', hidden: true},
						{header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name', tooltip: 'For Non-Admin users - specific condition that must evaluate to true in order for a user to be able to edit definitions of this type.'},
						{header: 'Send Condition', width: 150, dataIndex: 'sendCondition.name', tooltip: 'Must evaluate to true prior to sending an export for the definition of this type'},
						{header: 'Email Signature', width: 70, dataIndex: 'emailSignature'}
					],
					editor: {
						detailPageClass: 'Clifton.export.definition.ExportTypeWindow'
					}
				}]
			},


			{
				title: 'Shared Destinations',
				items: [{
					name: 'exportDefinitionDestinationListFind',
					xtype: 'gridpanel',
					instructions: 'Shared Destinations can be used in multiple exports',
					additionalPropertiesToRequest: 'destinationName',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 15, dataIndex: 'id', hidden: true},
						{header: 'Destination Name', dataIndex: 'destinationName', width: 200},
						{header: 'Destination Description', dataIndex: 'description', width: 250, hidden: true},
						{header: 'Destination Type', dataIndex: 'destinationSystemBean.type.name', width: 50},
						{header: 'Disabled', dataIndex: 'disabled', type: 'boolean', width: 40},
						{header: 'Shared Destination', dataIndex: 'sharedDestination', type: 'boolean', width: 50, hidden: true},
						{
							header: 'Modify Condition', width: 150, dataIndex: 'entityModifyCondition.name',
							tooltip: 'This condition must evaluate to true before content can be sent to the destination. The condition will be evaluated AFTER the Send Condition on the Definition Type evaluates to true.'
						},
						{
							header: 'Send Condition', width: 150, dataIndex: 'sendCondition.name',
							tooltip: 'This condition must evaluate to true before content can be sent to the destination. The condition will be evaluated AFTER the Send Condition on the Definition Type evaluates to true.'
						}
					],
					getTopToolbarInitialLoadParams: function(firstLoad) {
						return {sharedDestination: true};
					},
					showCustomFileSendWindow: function(destination) {
						TCG.createComponent('Clifton.export.SendCustomFileWindow', {
							defaultData: {
								destinationId: destination.data['id'],
								destinationName: destination.data['destinationSystemBean.nameWithoutAutoGeneratedPrefix']
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.export.destination.DestinationWindow',
						getDeleteURL: function() {
							return 'exportDefinitionDestinationCommonDelete.json';
						},
						getDefaultData: function(win) {
							return {
								sharedDestination: true
							};
						},
						addEditButtons: function(toolBar, gp) {
							const sm = gp.grid.getSelectionModel();
							TCG.grid.GridEditor.prototype.addToolbarAddButton.apply(this, arguments);
							toolBar.add({
								text: 'Remove',
								tooltip: 'Remove selected item(s)',
								iconCls: 'remove',
								scope: this,
								handler: function() {
									const editor = this;
									const grid = this.grid;
									if (sm.getCount() === 0) {
										TCG.showError('Please select a row to be deleted.', 'No Row(s) Selected');
									}
									else if (sm.getCount() !== 1) {
										TCG.showError('Multi-selection deletes are not supported yet.  Please select one row.', 'NOT SUPPORTED');
									}
									else {
										Ext.Msg.confirm('Delete Selected Shared Destination', 'Would you like to delete selected destination? This will remove it from all export definitions.', function(a) {
											if (a === 'yes') {
												const loader = new TCG.data.JsonLoader({
													waitTarget: grid.ownerCt,
													waitMsg: 'Deleting...',
													params: editor.getDeleteParams(sm),
													conf: {rowId: sm.getSelected().id},
													onLoad: function(record, conf) {
														if (editor.reloadGridAfterDelete && editor.reloadGridAfterDelete === true) {
															editor.getGridPanel().reload();
														}
														else {
															grid.getStore().remove(sm.getSelected());
															grid.ownerCt.updateCount();
														}
													}
												});
												loader.load(editor.getDeleteURL());
											}
										});
									}
								}
							});
							toolBar.add('-');
							toolBar.add({
								text: 'Send File',
								tooltip: 'Send a custom file to the selected destination. This feature should only be used in special cases, such as when testing that a file can be sent to a destination.',
								iconCls: 'upload',
								scope: this,
								handler: function() {
									const selection = sm.getSelections()[0];
									if (selection) {
										if (selection.data['disabled']) {
											Ext.Msg.confirm('Confirm File Send (Destination is disabled)', 'The selected destination is currently disabled on this export. ' +
												'Are you sure you want to proceed with selecting a custom file to send to this destination?', function(a) {
												if (a === 'yes') {
													gp.showCustomFileSendWindow(selection);
												}
											});
										}
										else {
											gp.showCustomFileSendWindow(selection);
										}
									}
									else {
										TCG.showError('Please select a destination to send the file to.', 'Incorrect Selection');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},

			{
				title: 'Export Destinations',
				items: [{
					name: 'exportDefinitionDestinationExportDefinitionListFind',
					xtype: 'gridpanel',
					instructions: 'All Export Destinations that have an Export Definition',
					additionalPropertiesToRequest: 'referenceOne.destinationName|referenceTwo.id',
					topToolbarSearchParameter: 'searchPattern',
					columns: [
						{header: 'ID', width: 20, dataIndex: 'id', hidden: true},
						{header: 'Destination ID', width: 35, dataIndex: 'referenceOne.id', type: 'int', filter: {searchFieldName: 'exportDefinitionDestinationId'}, hidden: true},
						{header: 'Export Definition', dataIndex: 'referenceTwo.name', width: 150, filter: {searchFieldName: 'definitionName'}, defaultSortColumn: true},
						{
							header: 'Export Destination', dataIndex: 'referenceOne.destinationSystemBean.nameWithoutAutoGeneratedPrefix', width: 200,
							filter: {searchFieldName: 'destinationNameOrBeanName'}, sortable: false,
							renderer: function(v, c, r) {
								if (r.data['referenceOne.sharedDestination']) {
									return r.json.referenceOne.destinationName;
								}
								return v;
							}
						},
						{header: 'Destination Description', dataIndex: 'referenceOne.description', width: 250, hidden: true, filter: {searchFieldName: 'destinationDescription'}},
						{
							header: 'Destination Type', dataIndex: 'referenceOne.destinationSystemBean.type.name', width: 50,
							filter: {type: 'combo', searchFieldName: 'destinationTypeId', displayField: 'name', url: 'systemBeanTypeListFind.json'}
						},
						{
							header: 'Modify Condition', width: 150, dataIndex: 'referenceOne.entityModifyCondition.name', filter: {searchFieldName: 'destinationModifyConditionName'},
							tooltip: 'This condition must evaluate to true before content can be sent to the destination. The condition will be evaluated AFTER the Send Condition on the Definition Type evaluates to true.'
						},
						{header: 'Disabled', dataIndex: 'referenceOne.disabled', type: 'boolean', width: 30, filter: {searchFieldName: 'destinationDisabled'}},
						{header: 'Shared', dataIndex: 'referenceOne.sharedDestination', type: 'boolean', width: 30, filter: {searchFieldName: 'sharedDestination'}}
					],
					showCustomFileSendWindow: function(destination) {
						TCG.createComponent('Clifton.export.SendCustomFileWindow', {
							defaultData: {
								destinationId: destination.data['referenceOne.id'],
								destinationName: destination.data['referenceOne.destinationSystemBean.nameWithoutAutoGeneratedPrefix']
							}
						});
					},
					editor: {
						detailPageClass: 'Clifton.export.destination.DestinationWindow',
						deleteURL: 'exportDefinitionDestinationAndLinkDelete.json',
						getDeleteParams: function(selectionModel) {
							const row = selectionModel.getSelected();
							return {destinationId: row.json.referenceOne.id, definitionId: row.json.referenceTwo.id};
						},
						getDetailPageId: function(gridPanel, row) {
							return row.json.referenceOne.id;
						},
						addEditButtons: function(toolBar, gp) {
							const sm = gp.grid.getSelectionModel();
							toolBar.add({name: 'exportDefinition', xtype: 'combo', url: 'exportDefinitionListFind.json', width: 175, listWidth: 200, emptyText: '< Select Export Definition >'});
							toolBar.add({
								text: 'Add',
								tooltip: 'Create a new Export Destination for the selected Export Definition',
								iconCls: 'add',
								handler: () => {
									const exportDefinition = TCG.getChildByName(toolBar, 'exportDefinition');
									const exportDefinitionId = exportDefinition.getValue();
									if (TCG.isBlank(exportDefinitionId)) {
										TCG.showError('You must first select a desired Export Definition from the list.');
									}
									else {
										const cmpId = TCG.getComponentId(this.getDetailPageClass());
										TCG.createComponent(this.getDetailPageClass(), {
											id: cmpId,
											defaultData: {definition: {'id': exportDefinitionId, 'name': exportDefinition.getRawValue()}},
											openerCt: gp,
											defaultIconCls: gp.getWindow().iconCls
										});
									}
								}
							});
							toolBar.add('-');
							TCG.grid.GridEditor.prototype.addToolbarDeleteButton.apply(this, arguments);
							toolBar.add({
								text: 'Send File',
								tooltip: 'Send a custom file to the selected destination. This feature should only be used in special cases, such as when testing that a file can be sent to a destination.',
								iconCls: 'upload',
								handler: () => {
									const selection = sm.getSelections()[0];
									if (selection) {
										if (selection.data['referenceOne.disabled']) {
											Ext.Msg.confirm('Confirm File Send (Destination is disabled)', 'The selected destination is currently disabled on this export. ' +
												'Are you sure you want to proceed with selecting a custom file to send to this destination?', function(a) {
												if (a === 'yes') {
													gp.showCustomFileSendWindow(selection);
												}
											});
										}
										else {
											gp.showCustomFileSendWindow(selection);
										}
									}
									else {
										TCG.showError('Please select a destination to send the file to.', 'Incorrect Selection');
									}
								}
							});
							toolBar.add('-');
						}
					}
				}]
			},


			{
				title: 'Export Content',
				items: [{
					xtype: 'export-definition-content-grid',
					columnOverrides: [
						{dataIndex: 'definition.name', hidden: false}
					],
					topToolbarSearchParameter: 'searchPattern',
					getLoadParams: function() {
						const params = TCG.grid.GridPanel.prototype.getLoadParams.apply(this, arguments);
						const definitionActiveFilterValue = TCG.getChildByName(this.getTopToolbar(), 'activeDefinition').getValue();
						if (definitionActiveFilterValue === 'ACTIVE') {
							params['activeDefinition'] = true;
						}
						else if (definitionActiveFilterValue === 'INACTIVE') {
							params['activeDefinition'] = false;
						}
						return params;
					},
					getTopToolbarFilters: function(toolbar) {
						const filters = TCG.grid.GridPanel.prototype.getTopToolbarFilters.apply(this, arguments);
						filters.push({
							fieldLabel: 'Export Definition', xtype: 'toolbar-combo', name: 'activeDefinition', width: 75, minListWidth: 75, mode: 'local', value: 'ACTIVE',
							store: {
								xtype: 'arraystore',
								data: [
									['ACTIVE', 'Active', 'View content with active definitions'],
									['INACTIVE', 'Inactive', 'View content with inactive definitions'],
									['ALL', 'All', 'View all content']
								]
							}
						});
						return filters;
					},
					editor: {
						detailPageClass: 'Clifton.export.content.ExportContentWindow',
						drillDownOnly: true
					}
				}]
			}
		]
	}]
});
