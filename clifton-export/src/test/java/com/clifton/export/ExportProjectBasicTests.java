package com.clifton.export;


import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.List;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ExportProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "export";
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("budi");
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("javax.jms");
		imports.add("org.springframework.jms");
		imports.add("org.springframework.beans.");
		imports.add("org.springframework.jdbc.core.ResultSetExtractor");
		imports.add("java.security.SecureRandom");
		imports.add("java.sql");
		imports.add("org.apache.commons.csv.CSVFormat");
		imports.add("org.apache.commons.csv.CSVPrinter");
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		String methodName = method.getName();
		Set<String> skipMethods = new HashSet<>();
		skipMethods.add("getExportRunStatusList");
		skipMethods.add("saveExportDefinitionType");
		skipMethods.add("saveExportDefinitionContent");
		skipMethods.add("deleteExportDefinitionContent");
		skipMethods.add("saveExportDefinitionDestination");
		skipMethods.add("deleteExportDefinitionDestination");
		skipMethods.add("saveExportRunHistory");
		return skipMethods.contains(methodName);
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveExportDefinitionDestinationLink");
		return ignoredVoidSaveMethodSet;
	}
}
