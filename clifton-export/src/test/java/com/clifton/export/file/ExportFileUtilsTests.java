package com.clifton.export.file;

import com.clifton.core.util.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author theodorez
 */
public class ExportFileUtilsTests {

	@Test
	public void testAppendUniqueIdentifier() {
		String fileName = "test.txt";
		String formattedFileName = ExportFileUtils.appendUniqueIdentifier(fileName, null);

		Assertions.assertTrue(formattedFileName.startsWith("test"));
		Assertions.assertTrue(formattedFileName.endsWith(".txt"));
	}


	@Test
	public void testRemoveUniqueIdentifier() {
		String fileName = "test.txt";
		String formattedFileName = ExportFileUtils.appendUniqueIdentifier(fileName, null);
		Assertions.assertTrue(StringUtils.isEqual(ExportFileUtils.removeUniqueIdentifier(formattedFileName), fileName));
	}
}
