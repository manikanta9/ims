package com.clifton.export.definition;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class ExportDefinitionServiceImplTests {

	@Resource
	private ExportDefinitionService exportDefinitionService;


	////////////////////////////////////////////////////////////////////////////
	////////                   Export Definition Tests              ////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveExportDefinition_RetryCountNotPopulatedWhenRetryDelayIsPopulated() {
		TestUtils.expectException(ValidationException.class, () -> {
			ExportDefinition exportDefinition = new ExportDefinition();
			exportDefinition.setRetryDelayInSeconds(60);
			this.exportDefinitionService.saveExportDefinition(exportDefinition);
		}, "The retry count and retry delay must both be specified or not at all");
	}


	@Test
	public void testSaveExportDefinition_RetryDelayNotPopulatedWhenRetryCountIsPopulated() {
		TestUtils.expectException(ValidationException.class, () -> {
			ExportDefinition exportDefinition = new ExportDefinition();
			exportDefinition.setRetryCount(1);
			this.exportDefinitionService.saveExportDefinition(exportDefinition);
		}, "The retry count and retry delay must both be specified or not at all");
	}


	@Test
	public void testSaveExportDefinition_RetryCountIsOutOfBounds() {
		TestUtils.expectException(ValidationException.class, () -> {
			ExportDefinition exportDefinition = new ExportDefinition();
			exportDefinition.setRetryDelayInSeconds(60);
			exportDefinition.setRetryCount(110);
			this.exportDefinitionService.saveExportDefinition(exportDefinition);
		}, "The retry count must be between 1 and 100");
	}


	@Test
	public void testSaveExportDefinition_RetryDelayIsOutOfBounds() {
		TestUtils.expectException(ValidationException.class, () -> {
			ExportDefinition exportDefinition = new ExportDefinition();
			exportDefinition.setRetryDelayInSeconds(30);
			exportDefinition.setRetryCount(100);
			this.exportDefinitionService.saveExportDefinition(exportDefinition);
		}, "The retry delay must be between 60 and 14400 seconds (4 hours)");
	}
}
