package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * Test class for {@link ExportTransformationColumnValueGroupingGenerator}.
 *
 * @author MikeH
 */
public class ExportTransformationColumnValueGroupingGeneratorTests {


	private ExportTransformationColumnValueGroupingGenerator generator = new ExportTransformationColumnValueGroupingGenerator();


	////////////////////////////////////////////////////////////////////////////
	////////            Initialize Sample Data                          ////////
	////////////////////////////////////////////////////////////////////////////


	private DataTable sampleDataTable;
	private Object[][] columnValuesArrays;

	private static final String COLUMN_00 = "Column #00";
	private static final String COLUMN_01 = "Column #01";
	private static final String COLUMN_02 = "Column #02";
	private static final String COLUMN_03 = "Column #03";
	private static final String COLUMN_04 = "Column #04";
	private static final String COLUMN_05 = "Column #05";
	private static final String COLUMN_06 = "Column #06";
	private static final String COLUMN_07 = "Column #07";
	private static final String COLUMN_08 = "Column #08";
	private static final String COLUMN_09 = "Column #09";
	private static final String COLUMN_10 = "Column #10";
	private static final String COLUMN_11 = "Column #11";
	private static final String COLUMN_12 = "Column #12";
	private static final String COLUMN_13 = "Column #13";
	private static final String COLUMN_14 = "Column #14";

	private static final String COLUMN_00_GROUP_A = "A";
	private static final String COLUMN_01_GROUP_A = "A";
	private static final String COLUMN_01_GROUP_B = "B";
	private static final String COLUMN_01_GROUP_C = "C";
	private static final String COLUMN_01_GROUP_D = "D";
	private static final String COLUMN_01_GROUP_E = "E";
	private static final String COLUMN_02_GROUP_A = "A";
	private static final String COLUMN_02_GROUP_B = "B";
	private static final Integer COLUMN_03_GROUP_A = 1;
	private static final Integer COLUMN_03_GROUP_B = 2;
	private static final Boolean COLUMN_04_GROUP_A = true;
	private static final Boolean COLUMN_04_GROUP_B = false;
	private static final Date COLUMN_05_GROUP_A = Date.from(Instant.parse("2014-01-01T00:00:00.00Z"));
	private static final Date COLUMN_05_GROUP_B = Date.from(Instant.parse("2014-01-01T12:00:00.00Z"));
	private static final Double COLUMN_06_GROUP_A = 1.0d;
	private static final Double COLUMN_06_GROUP_B = -1.0d;
	private static final Short COLUMN_07_GROUP_A = 1;
	private static final Short COLUMN_07_GROUP_B = 2;
	private static final Long COLUMN_08_GROUP_A = 1L;
	private static final Long COLUMN_08_GROUP_B = 2L;
	private static final BigInteger COLUMN_09_GROUP_A = BigInteger.valueOf(1L);
	private static final BigInteger COLUMN_09_GROUP_B = BigInteger.valueOf(2L);
	private static final Float COLUMN_10_GROUP_A = 1.0f;
	private static final Float COLUMN_10_GROUP_B = -1.0f;
	private static final Double COLUMN_11_GROUP_A = 1.0D;
	private static final Double COLUMN_11_GROUP_B = -1.0d;
	private static final BigDecimal COLUMN_12_GROUP_A = BigDecimal.valueOf(1.0d);
	private static final BigDecimal COLUMN_12_GROUP_B = BigDecimal.valueOf(-1.0d);
	private static final Date COLUMN_13_GROUP_A = Date.from(Instant.ofEpochMilli(1000L));
	private static final Date COLUMN_13_GROUP_B = Date.from(Instant.ofEpochMilli(2000L));
	private static final String COLUMN_14_GROUP_A = "1";
	private static final String COLUMN_14_GROUP_B = "2";
	private static final Object COLUMN_N_NULLVAL = null;


	@BeforeEach
	public void setup() {
		/*
		 * Sample data:
		 *  STR STR STR INT BOO DAT DEC SHR LNG BGI FLT DBL BGD TIM STR
		 *   A   A   B   A   A   A   A   A   A   A   A   A   A   A   A
		 *   A   B   A   B   A   A   A   A   A   A   A   A   A   A   A
		 *   A   C   A   A   B   A   A   A   A   A   A   A   A   A   A
		 *   A   D   A   A   A   B   A   B   B   B   B   B   B   B   B
		 *   A   E   A   A   A   A   B   B   B   B   B   B   B   B   B
		 *   X   X   X   X   X   X   X   X   X   X   X   X   X   X   B
		 */
		DataColumn[] columns = new DataColumn[]{
				new DataColumnImpl(COLUMN_00, java.sql.Types.VARCHAR),
				new DataColumnImpl(COLUMN_01, java.sql.Types.VARCHAR),
				new DataColumnImpl(COLUMN_02, java.sql.Types.VARCHAR),
				new DataColumnImpl(COLUMN_03, java.sql.Types.INTEGER),
				new DataColumnImpl(COLUMN_04, java.sql.Types.BOOLEAN),
				new DataColumnImpl(COLUMN_05, java.sql.Types.DATE),
				new DataColumnImpl(COLUMN_06, java.sql.Types.DECIMAL),
				new DataColumnImpl(COLUMN_07, java.sql.Types.TINYINT),
				new DataColumnImpl(COLUMN_08, java.sql.Types.BIGINT),
				new DataColumnImpl(COLUMN_09, java.sql.Types.DECIMAL),
				new DataColumnImpl(COLUMN_10, java.sql.Types.DECIMAL),
				new DataColumnImpl(COLUMN_11, java.sql.Types.DECIMAL),
				new DataColumnImpl(COLUMN_12, java.sql.Types.DECIMAL),
				new DataColumnImpl(COLUMN_13, java.sql.Types.TIME),
				new DataColumnImpl(COLUMN_14, java.sql.Types.VARCHAR)
		};
		this.columnValuesArrays = new Object[][]{
				{COLUMN_00_GROUP_A, COLUMN_01_GROUP_A, COLUMN_02_GROUP_B, COLUMN_03_GROUP_A, COLUMN_04_GROUP_A, COLUMN_05_GROUP_A, COLUMN_06_GROUP_A, COLUMN_07_GROUP_A, COLUMN_08_GROUP_A, COLUMN_09_GROUP_A, COLUMN_10_GROUP_A, COLUMN_11_GROUP_A, COLUMN_12_GROUP_A, COLUMN_13_GROUP_A, COLUMN_14_GROUP_A},
				{COLUMN_00_GROUP_A, COLUMN_01_GROUP_B, COLUMN_02_GROUP_A, COLUMN_03_GROUP_B, COLUMN_04_GROUP_A, COLUMN_05_GROUP_A, COLUMN_06_GROUP_A, COLUMN_07_GROUP_A, COLUMN_08_GROUP_A, COLUMN_09_GROUP_A, COLUMN_10_GROUP_A, COLUMN_11_GROUP_A, COLUMN_12_GROUP_A, COLUMN_13_GROUP_A, COLUMN_14_GROUP_A},
				{COLUMN_00_GROUP_A, COLUMN_01_GROUP_C, COLUMN_02_GROUP_A, COLUMN_03_GROUP_A, COLUMN_04_GROUP_B, COLUMN_05_GROUP_A, COLUMN_06_GROUP_A, COLUMN_07_GROUP_A, COLUMN_08_GROUP_A, COLUMN_09_GROUP_A, COLUMN_10_GROUP_A, COLUMN_11_GROUP_A, COLUMN_12_GROUP_A, COLUMN_13_GROUP_A, COLUMN_14_GROUP_A},
				{COLUMN_00_GROUP_A, COLUMN_01_GROUP_D, COLUMN_02_GROUP_A, COLUMN_03_GROUP_A, COLUMN_04_GROUP_A, COLUMN_05_GROUP_B, COLUMN_06_GROUP_A, COLUMN_07_GROUP_B, COLUMN_08_GROUP_B, COLUMN_09_GROUP_B, COLUMN_10_GROUP_B, COLUMN_11_GROUP_B, COLUMN_12_GROUP_B, COLUMN_13_GROUP_B, COLUMN_14_GROUP_B},
				{COLUMN_00_GROUP_A, COLUMN_01_GROUP_E, COLUMN_02_GROUP_A, COLUMN_03_GROUP_A, COLUMN_04_GROUP_A, COLUMN_05_GROUP_A, COLUMN_06_GROUP_B, COLUMN_07_GROUP_B, COLUMN_08_GROUP_B, COLUMN_09_GROUP_B, COLUMN_10_GROUP_B, COLUMN_11_GROUP_B, COLUMN_12_GROUP_B, COLUMN_13_GROUP_B, COLUMN_14_GROUP_B},
				// Unique (all-nulls) row
				{COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL, COLUMN_N_NULLVAL}
		};
		this.sampleDataTable = new PagingDataTableImpl(columns);
		for (Object[] columnValues : this.columnValuesArrays) {
			this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, columnValues));
		}
		Assertions.assertEquals(this.sampleDataTable.getTotalRowCount(), this.columnValuesArrays.length);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Transformation Results                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testOneColumnNoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_01}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testOneColumnOneGrouping() {
		performAndAssertTransformation(new String[]{COLUMN_00}, new Integer[]{0});
	}


	@Test
	public void testOneColumnTwoGroupingsString() {
		performAndAssertTransformation(new String[]{COLUMN_02}, new Integer[]{0, 1});
	}


	@Test
	public void testOneColumnTwoGroupingsInteger() {
		performAndAssertTransformation(new String[]{COLUMN_03}, new Integer[]{0, 1});
	}


	@Test
	public void testOneColumnTwoGroupingsBoolean() {
		performAndAssertTransformation(new String[]{COLUMN_04}, new Integer[]{0, 2});
	}


	@Test
	public void testOneColumnTwoGroupingsDate() {
		performAndAssertTransformation(new String[]{COLUMN_05}, new Integer[]{0, 3});
	}


	@Test
	public void testOneColumnTwoGroupingsDecimal() {
		performAndAssertTransformation(new String[]{COLUMN_06}, new Integer[]{0, 4});
	}


	@Test
	public void testTwoColumnsNoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_01}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testTwoColumnsTwoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_02}, new Integer[]{0, 1});
	}


	@Test
	public void testTwoColumnsThreeGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_02, COLUMN_03}, new Integer[]{0, 1, 2});
	}


	@Test
	public void testThreeColumnsNoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_01, COLUMN_02}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testThreeColumnsThreeGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_02, COLUMN_03}, new Integer[]{0, 1, 2});
	}


	@Test
	public void testThreeColumnsFourGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_02, COLUMN_03, COLUMN_04}, new Integer[]{0, 1, 2, 3});
	}


	@Test
	public void testFourColumnsNoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_01, COLUMN_02, COLUMN_03}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testFourColumnsFourGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_02, COLUMN_03, COLUMN_04}, new Integer[]{0, 1, 2, 3});
	}


	@Test
	public void testFourColumnsFiveGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_02, COLUMN_03, COLUMN_04, COLUMN_05}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testFiveColumnsNoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_01, COLUMN_02, COLUMN_03, COLUMN_04}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testFiveColumnsFiveGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_02, COLUMN_03, COLUMN_04, COLUMN_05}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testSixColumnsFiveGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_02, COLUMN_03, COLUMN_04, COLUMN_05, COLUMN_06}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testSevenColumnsNoGroupings() {
		performAndAssertTransformation(new String[]{COLUMN_00, COLUMN_01, COLUMN_02, COLUMN_03, COLUMN_04, COLUMN_05, COLUMN_06}, new Integer[]{0, 1, 2, 3, 4});
	}


	@Test
	public void testAggregateInvalidVarchar() {
		TestUtils.expectException(ValidationException.class, () -> {
			Object aggregateValue = convertToAggregateType(COLUMN_00_GROUP_A, COLUMN_00);
			performAndAssertTransformation(new String[]{COLUMN_00}, new Integer[]{0}, new String[]{COLUMN_00}, new Object[][]{{aggregateValue}});
		}, String.format("Unable to perform numeric/time-based aggregation for values [%s] and [%s].", null, COLUMN_00_GROUP_A));
	}


	@Test
	public void testAggregateBoolean() {
		TestUtils.expectException(ValidationException.class, () -> {
			Object aggregateValueA = convertToAggregateType(COLUMN_04_GROUP_A, COLUMN_04);
			Object aggregateValueB = convertToAggregateType(COLUMN_04_GROUP_B, COLUMN_04);
			performAndAssertTransformation(new String[]{COLUMN_04}, new Integer[]{0, 2}, new String[]{COLUMN_04}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
		}, String.format("Unable to perform aggregation on column [%s]. Aggregation is not supported for columns of type [%s].", COLUMN_04, "BOOLEAN"));
	}


	@Test
	public void testAggregateInteger() {
		Object aggregateValueA = convertToAggregateType(COLUMN_03_GROUP_A * 4, COLUMN_03);
		Object aggregateValueB = convertToAggregateType(COLUMN_03_GROUP_B, COLUMN_03);
		performAndAssertTransformation(new String[]{COLUMN_03}, new Integer[]{0, 1}, new String[]{COLUMN_03}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateShort() {
		Object aggregateValueA = convertToAggregateType(COLUMN_07_GROUP_A * 3, COLUMN_07);
		Object aggregateValueB = convertToAggregateType(COLUMN_07_GROUP_B * 2, COLUMN_07);
		performAndAssertTransformation(new String[]{COLUMN_07}, new Integer[]{0, 3}, new String[]{COLUMN_07}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateLong() {
		Object aggregateValueA = convertToAggregateType(COLUMN_08_GROUP_A * 3, COLUMN_08);
		Object aggregateValueB = convertToAggregateType(COLUMN_08_GROUP_B * 2, COLUMN_08);
		performAndAssertTransformation(new String[]{COLUMN_08}, new Integer[]{0, 3}, new String[]{COLUMN_08}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateBigInteger() {
		Object aggregateValueA = convertToAggregateType(COLUMN_09_GROUP_A.multiply(BigInteger.valueOf(3L)), COLUMN_09);
		Object aggregateValueB = convertToAggregateType(COLUMN_09_GROUP_B.multiply(BigInteger.valueOf(2L)), COLUMN_09);
		performAndAssertTransformation(new String[]{COLUMN_09}, new Integer[]{0, 3}, new String[]{COLUMN_09}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateFloat() {
		Object aggregateValueA = convertToAggregateType(COLUMN_10_GROUP_A * 3, COLUMN_10);
		Object aggregateValueB = convertToAggregateType(COLUMN_10_GROUP_B * 2, COLUMN_10);
		performAndAssertTransformation(new String[]{COLUMN_10}, new Integer[]{0, 3}, new String[]{COLUMN_10}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateDouble() {
		Object aggregateValueA = convertToAggregateType(COLUMN_11_GROUP_A * 3, COLUMN_11);
		Object aggregateValueB = convertToAggregateType(COLUMN_11_GROUP_B * 2, COLUMN_11);
		performAndAssertTransformation(new String[]{COLUMN_11}, new Integer[]{0, 3}, new String[]{COLUMN_11}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateBigDecimal() {
		Object aggregateValueA = convertToAggregateType(COLUMN_12_GROUP_A.add(COLUMN_12_GROUP_A).add(COLUMN_12_GROUP_A), COLUMN_12);
		Object aggregateValueB = convertToAggregateType(COLUMN_12_GROUP_B.add(COLUMN_12_GROUP_B), COLUMN_12);
		performAndAssertTransformation(new String[]{COLUMN_12}, new Integer[]{0, 3}, new String[]{COLUMN_12}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateDate() {
		TestUtils.expectException(ValidationException.class, () -> {
			Object aggregateValueA = convertToAggregateType(Date.from(Instant.ofEpochMilli(COLUMN_05_GROUP_A.getTime() * 4)), COLUMN_05);
			Object aggregateValueB = convertToAggregateType(COLUMN_05_GROUP_B, COLUMN_05);
			performAndAssertTransformation(new String[]{COLUMN_05}, new Integer[]{0, 3}, new String[]{COLUMN_05}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
		}, String.format("Unable to perform aggregation on column [%s]. Aggregation is not supported for columns of type [%s].", COLUMN_05, "DATE"));
	}


	@Test
	public void testAggregateTime() {
		Object aggregateValueA = convertToAggregateType(Date.from(Instant.ofEpochMilli(COLUMN_13_GROUP_A.getTime() * 3)), COLUMN_13);
		Object aggregateValueB = convertToAggregateType(Date.from(Instant.ofEpochMilli(COLUMN_13_GROUP_B.getTime() * 2)), COLUMN_13);
		performAndAssertTransformation(new String[]{COLUMN_13}, new Integer[]{0, 3}, new String[]{COLUMN_13}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	@Test
	public void testAggregateMultiple() {
		Object aggregateValueInteger1 = convertToAggregateType(COLUMN_03_GROUP_A * 3 + COLUMN_03_GROUP_B, COLUMN_03);
		Object aggregateValueInteger2 = convertToAggregateType(COLUMN_03_GROUP_A, COLUMN_03);
		Object aggregateValueShort1 = convertToAggregateType(COLUMN_07_GROUP_A * 2 + COLUMN_07_GROUP_B * 2, COLUMN_07);
		Object aggregateValueShort2 = convertToAggregateType(COLUMN_07_GROUP_A, COLUMN_07);
		performAndAssertTransformation(new String[]{COLUMN_04}, new Integer[]{0, 2}, new String[]{COLUMN_03, COLUMN_07}, new Object[][]{{aggregateValueInteger1, aggregateValueShort1}, {aggregateValueInteger2, aggregateValueShort2}});
	}


	@Test
	public void testAggregateStringNumber() {
		BigDecimal valueA = new BigDecimal(COLUMN_14_GROUP_A);
		BigDecimal valueB = new BigDecimal(COLUMN_14_GROUP_B);
		Object aggregateValueA = convertToAggregateType(valueA.multiply(new BigDecimal(2)).add(valueB.multiply(new BigDecimal(2))), COLUMN_14);
		Object aggregateValueB = convertToAggregateType(valueA, COLUMN_14);
		performAndAssertTransformation(new String[]{COLUMN_04}, new Integer[]{0, 2}, new String[]{COLUMN_14}, new Object[][]{{aggregateValueA}, {aggregateValueB}});
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Edge Cases                                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidColumnName() {
		final String columnName = "Some non-existent column";
		TestUtils.expectException(ValidationException.class, () -> {
			this.generator.setGroupingColumnNames(new String[]{columnName});
			DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		}, String.format("Unable to find column [%s] within the data table.", columnName));
	}


	@Test
	public void testInvalidNoColumns() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.generator.setGroupingColumnNames(new String[]{});
			DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		}, "Unable to process data table groupings: No columns have been specified on which to group items.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Converts the given value to the aggregate type for the given column.
	 *
	 * @param value      the value to convert
	 * @param columnName the name of the column which has the type to which the value should be converted
	 * @return the converted value
	 */
	private Object convertToAggregateType(Object value, String columnName) {
		final Object aggregateValue;
		DataColumn column = ArrayUtils.getStream(this.sampleDataTable.getColumnList())
				.filter(tableColumn -> StringUtils.isEqual(tableColumn.getColumnName(), columnName))
				.findFirst()
				.orElseThrow(() -> new RuntimeException(String.format("No column of name [%s] exists within the test data table.", columnName)));
		switch (column.getDataType()) {
			case java.sql.Types.TINYINT:
			case java.sql.Types.SMALLINT:
			case java.sql.Types.INTEGER:
			case java.sql.Types.BIGINT:
				aggregateValue = value == null ? null : value instanceof BigInteger ? (BigInteger) value : BigInteger.valueOf(((Number) value).longValue());
				break;
			case java.sql.Types.FLOAT:
			case java.sql.Types.DOUBLE:
			case java.sql.Types.NUMERIC:
			case java.sql.Types.DECIMAL:
				aggregateValue = value == null ? null : value instanceof BigDecimal ? (BigDecimal) value : BigDecimal.valueOf(((Number) value).doubleValue());
				break;
			case java.sql.Types.TIME:
			case java.sql.Types.TIME_WITH_TIMEZONE:
				// No conversion necessary
				aggregateValue = value;
				break;
			default:
				aggregateValue = value;
				break;
		}
		return aggregateValue;
	}


	private void performAndAssertTransformation(String[] groupingColumnNames, Integer[] expectedRowIndices) {
		performAndAssertTransformation(groupingColumnNames, expectedRowIndices, null, null);
	}


	/**
	 * Performs the transformation and asserts the expected results.
	 *
	 * @param groupingColumnNames         the column names on which grouping should be performed
	 * @param expectedRowIndices          the expected rows from the source table which should remain in the transformed table
	 * @param aggregatedColumnNames       the column names on which aggregation should be performed
	 * @param expectedAggregatedRowValues the expected row/column values for each aggregated column, where the column index given corresponds to the aggregated
	 *                                    column specified at the same index in {@code aggregatedColumnIndices} (for example, aggregated column indices
	 *                                    <code>{0, 2}</code> and values <code>{{4, 6}}</code> could correspond to the rows <code>{{4, 0, 0}, {0, 0,
	 *                                    6}}</code>)
	 */
	private void performAndAssertTransformation(String[] groupingColumnNames, Integer[] expectedRowIndices, String[] aggregatedColumnNames, Object[][] expectedAggregatedRowValues) {
		// Run transformation
		this.generator.setGroupingColumnNames(groupingColumnNames);
		this.generator.setAggregatedColumnNames(aggregatedColumnNames);
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);

		// Verify transformation
		Integer[] aggregatedColumnIndices = validateAndGetAggregatedColumnIndices(output, expectedRowIndices, aggregatedColumnNames, expectedAggregatedRowValues);
		verifyTransformationContents(output, expectedRowIndices, aggregatedColumnIndices, expectedAggregatedRowValues);
	}


	/**
	 * Validates the column indices array against the aggregated values array, checking for consistent lengths, and returns an array of the indices of the
	 * aggregated columns, in order.
	 */
	private Integer[] validateAndGetAggregatedColumnIndices(DataTable transformedTable, Integer[] expectedRowIndices, String[] aggregatedColumnNames, Object[][] expectedAggregatedRowValues) {// Configure aggregation columns
		Integer[] aggregatedColumnIndices = null;
		if (aggregatedColumnNames != null || expectedAggregatedRowValues != null) {
			// Validate values
			AssertUtils.assertFalse(aggregatedColumnNames == null || expectedAggregatedRowValues == null, "The aggregated column names and aggregated row values parameters must either be both non-null or both null.");
			AssertUtils.assertEquals(expectedRowIndices.length, expectedAggregatedRowValues.length, "The matching rows array and the aggregated row values array must be of equal length. Found matching rows length [%d] and aggregated row values length [%d].",
					expectedRowIndices.length, expectedAggregatedRowValues.length);
			for (int rowIndex = 0; rowIndex < expectedAggregatedRowValues.length; rowIndex++) {
				Object[] aggregatedValuesForRow = expectedAggregatedRowValues[rowIndex];
				AssertUtils.assertEquals(aggregatedValuesForRow.length, aggregatedColumnNames.length, "Invalid number of values in row [%d] of the aggregated row values array. An equal number of values must exist in the column names array and in each row of the aggregated row values array. Expected [%d]. Found [%d].",
						rowIndex, aggregatedValuesForRow.length, aggregatedColumnNames.length);
			}

			// Build index map from aggregated row value column indices to actual column indices
			List<String> tableColumnNameList = ArrayUtils.getStream(transformedTable.getColumnList())
					.map(DataColumn::getColumnName)
					.collect(Collectors.toList());
			aggregatedColumnIndices = new Integer[aggregatedColumnNames.length];
			for (int aggregatedColumnIndex = 0; aggregatedColumnIndex < aggregatedColumnIndices.length; aggregatedColumnIndex++) {
				String aggregatedColumnName = aggregatedColumnNames[aggregatedColumnIndex];
				int foundAggregatedColumnIndex = tableColumnNameList.indexOf(aggregatedColumnName);
				AssertUtils.assertNotEquals(-1, foundAggregatedColumnIndex, "Unable to find the specified aggregation column [%s] within the data table.", aggregatedColumnName);
				aggregatedColumnIndices[aggregatedColumnIndex] = foundAggregatedColumnIndex;
			}
		}
		return aggregatedColumnIndices;
	}


	/**
	 * Verifies the contents of the transformed table against the expected values.
	 */
	private void verifyTransformationContents(DataTable transformedTable, Integer[] expectedRowIndices, Integer[] aggregatedColumnIndices, Object[][] expectedAggregatedRowValues) {
		// Add null-values row if not present (this unique row will always add an additional grouping)
		Integer nullValueRow = this.columnValuesArrays.length - 1;
		Integer[] expectedRowIndicesWithNullRow = expectedRowIndices;
		if (!ArrayUtils.contains(expectedRowIndicesWithNullRow, nullValueRow)) {
			expectedRowIndicesWithNullRow = ArrayUtils.add(expectedRowIndicesWithNullRow, nullValueRow);
		}
		Object[][] expectedAggregatedRowValuesWithNullRow = expectedAggregatedRowValues;
		if (aggregatedColumnIndices != null) {
			Object[] nullAggregatedRowValues = new Object[aggregatedColumnIndices.length];
			expectedAggregatedRowValuesWithNullRow = ArrayUtils.addAll(expectedAggregatedRowValuesWithNullRow, new Object[][]{nullAggregatedRowValues});
		}

		// Verify correct number and contents of rows
		AssertUtils.assertEquals(expectedRowIndicesWithNullRow.length, transformedTable.getTotalRowCount(), "The number of rows in the resulting table does not match the expected number of rows. Expected [%d]. Found [%d].", expectedRowIndicesWithNullRow.length, transformedTable.getTotalRowCount());
		for (int rowNumber = 0; rowNumber < transformedTable.getTotalRowCount(); rowNumber++) {
			// Get actual row data
			DataRow row = transformedTable.getRow(rowNumber);
			Object[] rowValues = new Object[transformedTable.getColumnCount()];
			for (int columnNumber = 0; columnNumber < rowValues.length; columnNumber++) {
				rowValues[columnNumber] = row.getValue(columnNumber);
			}

			// Get expected row data
			Object[] expectedRowValues = ArrayUtils.cloneArray(this.columnValuesArrays[expectedRowIndicesWithNullRow[rowNumber]]);
			if (aggregatedColumnIndices != null) {
				for (int sourceColumnIndex = 0; sourceColumnIndex < aggregatedColumnIndices.length; sourceColumnIndex++) {
					int mappedColumnIndex = aggregatedColumnIndices[sourceColumnIndex];
					expectedRowValues[mappedColumnIndex] = expectedAggregatedRowValuesWithNullRow[rowNumber][sourceColumnIndex];
				}
			}

			// Perform assertion
			Assertions.assertArrayEquals(expectedRowValues, rowValues);
		}
	}
}
