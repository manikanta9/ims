package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.IntStream;


/**
 * Test class for {@link ExportTransformationColumnRenameGenerator}.
 *
 * @author KellyJ
 */
public class ExportTransformationColumnRenameGeneratorTests {

	private ExportTransformationColumnRenameGenerator generator = new ExportTransformationColumnRenameGenerator();


	////////////////////////////////////////////////////////////////////////////
	////////            Initialize Sample Data                          ////////
	////////////////////////////////////////////////////////////////////////////


	private static final int NUM_COLUMNS = 4;
	private static final int NUM_ROWS = 4;

	private DataTable sampleDataTable;


	@BeforeEach
	public void setup() {
		// Build sample data table with the original column names (ex:  'Column[0]','Column[1]',etc)
		DataColumn[] columns = new DataColumn[NUM_COLUMNS];
		for (int colNum = 0; colNum < columns.length; colNum++) {
			columns[colNum] = new DataColumnImpl(getOriginalColumnName(colNum), java.sql.Types.VARCHAR);
		}

		this.sampleDataTable = new PagingDataTableImpl(columns);
		for (int rowNum = 0; rowNum < NUM_ROWS; rowNum++) {
			Object[] rowValues = new Object[columns.length];
			for (int colNum = 0; colNum < rowValues.length; colNum++) {
				rowValues[colNum] = getExpectedRowValue(rowNum, colNum);
			}
			this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, rowValues));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Transformation Results                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRenameAllColumns() {
		performAndAssertTransformation(IntStream.range(0, this.sampleDataTable.getColumnCount()).toArray());
	}


	@Test
	public void testRenameFirstColumn() {
		performAndAssertTransformation(0);
	}


	@Test
	public void testRenameMiddleColumn() {
		performAndAssertTransformation(2);
	}


	@Test
	public void testRenameLastColumn() {
		performAndAssertTransformation(NUM_COLUMNS - 1);
	}


	@Test
	public void testRenameMultipleColumns() {
		performAndAssertTransformation(0, 1);
	}


	@Test
	public void testRenameMultipleRandomColumns() {
		performAndAssertTransformation(3, 0, 2);
	}


	@Test
	public void testInvalidColumnName() {
		final String columnName = "Some non-existent column==My some non-existent column";
		this.generator.setColumnNameToAliasMapping(new String[]{columnName});
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		validateData(output);
	}


	@Test
	public void testRenameNoColumns() {
		TestUtils.expectException(ValidationException.class, () -> {
			performAndAssertTransformation();
		}, "Unable to process data table re-mappings: No columns to rename have been specified.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	private static String[] getOriginalColumnNames(int... columnIndices) {
		String[] columnNames = new String[columnIndices.length];
		for (int i = 0; i < columnNames.length; i++) {
			columnNames[i] = getOriginalColumnName(columnIndices[i]);
		}
		return columnNames;
	}


	private static String getOriginalColumnName(int column) {
		return String.format("Column [%d]", column);
	}


	private static String[] getExpectedColumnNames(int... renamedColumnIndices) {
		String[] columnNames = new String[NUM_COLUMNS];
		List<Integer> renamedColumns = new ArrayList<>();
		for (int col : renamedColumnIndices) {
			renamedColumns.add(col);
		}
		for (int i = 0; i < columnNames.length; i++) {
			if (renamedColumns.contains(i)) {
				columnNames[i] = getExpectedRenamedColumnName(i);
			}
			else {
				columnNames[i] = getOriginalColumnName(i);
			}
		}
		return columnNames;
	}


	private static String getExpectedColumnNameAsInput(int column) {
		return String.format("Column [%1$d]==MyColumn [%1$d]", column);
	}


	private static String getExpectedRenamedColumnName(int column) {
		return String.format("MyColumn [%d]", column);
	}


	private static String[] getExpectedRowValues(int row, int... columnIndices) {
		String[] rowValues = new String[columnIndices.length];
		for (int i = 0; i < rowValues.length; i++) {
			rowValues[i] = getExpectedRowValue(row, columnIndices[i]);
		}
		return rowValues;
	}


	private static String getExpectedRowValue(int row, int column) {
		return String.format("Row [%d] Column [%d]", row, column);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void performAndAssertTransformation(int... expectedColumns) {
		// Prepare transformation data
		String[] columnNames = IntStream.of(expectedColumns)
				.mapToObj(ExportTransformationColumnRenameGeneratorTests::getExpectedColumnNameAsInput)
				.toArray(String[]::new);
		this.generator.setColumnNameToAliasMapping(columnNames);


		// Run transformation
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);

		// validate the test data
		validateData(output, expectedColumns);
	}


	private void validateData(DataTable output, int... expectedColumns) {
		validateColumnData(output, expectedColumns);
		validateRowData(output);
	}


	private void validateRowData(DataTable output) {
		// Verify correct number of rows
		AssertUtils.assertEquals(this.sampleDataTable.getTotalRowCount(), output.getTotalRowCount(), "The number of rows in the resulting table does not match the expected number of rows. Expected [%d]. Found [%d].", this.sampleDataTable.getTotalRowCount(), output.getTotalRowCount());

		// Verify correct content of rows for each row
		for (int rowNumber = 0; rowNumber < output.getTotalRowCount(); rowNumber++) {
			DataRow row = output.getRow(rowNumber);
			Object[] actualRowValues = new Object[output.getColumnCount()];
			for (int columnNumber = 0; columnNumber < actualRowValues.length; columnNumber++) {
				actualRowValues[columnNumber] = row.getValue(columnNumber);
			}
			Object[] expectedRowValues = getExpectedRowValues(rowNumber, IntStream.range(0, NUM_COLUMNS).toArray());
			Assertions.assertArrayEquals(expectedRowValues, actualRowValues);
		}
	}


	private void validateColumnData(DataTable output, int... renamedColumns) {
		// Verify correct number of rows
		AssertUtils.assertEquals(this.sampleDataTable.getColumnCount(), output.getColumnCount(), "The number of columns in the resulting table does not match the expected number of columns. Expected [%d]. Found [%d].", this.sampleDataTable.getColumnCount(), output.getColumnCount());

		// Verify correct column names
		String[] expectedColumnNames = getExpectedColumnNames(renamedColumns);
		String[] actualColumnNames = Arrays.stream(output.getColumnList()).map(DataColumn::getColumnName).toArray(String[]::new);
		Assertions.assertArrayEquals(expectedColumnNames, actualColumnNames);
	}
}
