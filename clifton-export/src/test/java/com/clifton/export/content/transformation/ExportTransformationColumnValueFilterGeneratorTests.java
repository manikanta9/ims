package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.util.Collections;
import java.util.Date;


/**
 * Test class for {@link ExportTransformationColumnValueFilterGenerator}.
 *
 * @author MikeH
 */
public class ExportTransformationColumnValueFilterGeneratorTests {

	private ExportTransformationColumnValueFilterGenerator generator = new ExportTransformationColumnValueFilterGenerator();


	////////////////////////////////////////////////////////////////////////////
	////////            Initialize Sample Data                          ////////
	////////////////////////////////////////////////////////////////////////////


	private DataTable sampleDataTable;

	private static final String COLUMN_0 = "Column #0";
	private static final String COLUMN_2 = "Column #1";
	private static final String COLUMN_1 = "Column #2";
	private static final String COLUMN_3 = "Column #3";
	private static final String COLUMN_4 = "Column #4";

	private static final Date DATE_0 = Date.from(Instant.parse("2014-01-01T00:00:00.00Z"));
	private static final Date DATE_1 = Date.from(Instant.parse("2015-01-01T12:00:00.00Z"));
	private static final Date DATE_2 = Date.from(Instant.parse("2016-01-01T12:00:00.00Z"));
	private static final Date DATE_3 = Date.from(Instant.parse("2017-01-01T23:59:59.99Z"));
	private static final Date DATE_4 = Date.from(Instant.parse("2018-01-01T23:59:59.99Z"));

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";


	{
		DataColumn[] columns = new DataColumn[]{
				new DataColumnImpl(COLUMN_0, java.sql.Types.VARCHAR),
				new DataColumnImpl(COLUMN_1, java.sql.Types.INTEGER),
				new DataColumnImpl(COLUMN_2, java.sql.Types.BOOLEAN),
				new DataColumnImpl(COLUMN_3, java.sql.Types.DATE),
				new DataColumnImpl(COLUMN_4, java.sql.Types.DECIMAL)
		};
		this.sampleDataTable = new PagingDataTableImpl(columns);
		this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, new Object[]{"Entity #0", 0, true, DATE_0, 0.0}));
		this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, new Object[]{"Entity #1", 1, true, DATE_1, 0.1}));
		this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, new Object[]{"Entity #2", 2, true, DATE_2, 0.2}));
		this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, new Object[]{"Entity #3", 3, false, DATE_3, 0.3}));
		this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, new Object[]{null, null, null, null, null}));
		Assertions.assertEquals(this.sampleDataTable.getTotalRowCount(), 5);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Filter Results                             ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testStringNoResult() throws Exception {
		this.generator.setColumnName(COLUMN_0);
		this.generator.setColumnValueList(CollectionUtils.createList("null"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 0);
	}


	@Test
	public void testStringSingleResult() throws Exception {
		this.generator.setColumnName(COLUMN_0);
		this.generator.setColumnValueList(CollectionUtils.createList("Entity #2"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 1);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(2)));
	}


	@Test
	public void testStringMultipleResults() throws Exception {
		this.generator.setColumnName(COLUMN_0);
		this.generator.setColumnValueList(CollectionUtils.createList("Entity #0", "Entity #1", "Entity #3"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 3);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(0)));
		Assertions.assertTrue(compareDataRows(output.getRow(1), this.sampleDataTable.getRow(1)));
		Assertions.assertTrue(compareDataRows(output.getRow(2), this.sampleDataTable.getRow(3)));
	}


	@Test
	public void testIntegerNoResult() throws Exception {
		this.generator.setColumnName(COLUMN_1);
		this.generator.setColumnValueList(CollectionUtils.createList("5"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 0);
	}


	@Test
	public void testIntegerSingleResult() throws Exception {
		this.generator.setColumnName(COLUMN_1);
		this.generator.setColumnValueList(CollectionUtils.createList("3"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 1);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(3)));
	}


	@Test
	public void testIntegerMultipleResults() throws Exception {
		this.generator.setColumnName(COLUMN_1);
		this.generator.setColumnValueList(CollectionUtils.createList("0", "1"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 2);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(0)));
		Assertions.assertTrue(compareDataRows(output.getRow(1), this.sampleDataTable.getRow(1)));
	}


	@Test
	public void testBooleanNoResult() throws Exception {
		this.generator.setColumnName(COLUMN_2);
		this.generator.setColumnValueList(CollectionUtils.createList("Not a boolean"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 0);
	}


	@Test
	public void testBooleanSingleResult() throws Exception {
		this.generator.setColumnName(COLUMN_2);
		this.generator.setColumnValueList(CollectionUtils.createList("false"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 1);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(3)));
	}


	@Test
	public void testBooleanMultipleResults() throws Exception {
		this.generator.setColumnName(COLUMN_2);
		this.generator.setColumnValueList(CollectionUtils.createList("true"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 3);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(0)));
		Assertions.assertTrue(compareDataRows(output.getRow(1), this.sampleDataTable.getRow(1)));
		Assertions.assertTrue(compareDataRows(output.getRow(2), this.sampleDataTable.getRow(2)));
	}


	@Test
	public void testDateNoResult() throws Exception {
		this.generator.setColumnName(COLUMN_3);
		this.generator.setColumnValueList(CollectionUtils.createList(DateUtils.fromDate(Date.from(Instant.EPOCH), DATE_FORMAT)));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 0);
	}


	@Test
	public void testDateSingleResult() throws Exception {
		this.generator.setColumnName(COLUMN_3);
		this.generator.setColumnValueList(CollectionUtils.createList(DateUtils.fromDate(DATE_0, DATE_FORMAT)));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 1);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(0)));
	}


	@Test
	public void testDateMultipleResults() throws Exception {
		this.generator.setColumnName(COLUMN_3);
		this.generator.setColumnValueList(CollectionUtils.createList(
				DateUtils.fromDate(DATE_0, DATE_FORMAT),
				DateUtils.fromDate(DATE_1, DATE_FORMAT),
				DateUtils.fromDate(DATE_2, DATE_FORMAT),
				DateUtils.fromDate(DATE_3, DATE_FORMAT)));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 4);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(0)));
		Assertions.assertTrue(compareDataRows(output.getRow(1), this.sampleDataTable.getRow(1)));
		Assertions.assertTrue(compareDataRows(output.getRow(2), this.sampleDataTable.getRow(2)));
		Assertions.assertTrue(compareDataRows(output.getRow(3), this.sampleDataTable.getRow(3)));
	}


	@Test
	public void testDecimalNoResult() throws Exception {
		this.generator.setColumnName(COLUMN_4);
		this.generator.setColumnValueList(CollectionUtils.createList("-0.0"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 0);
	}


	@Test
	public void testDecimalSingleResult() throws Exception {
		this.generator.setColumnName(COLUMN_4);
		this.generator.setColumnValueList(CollectionUtils.createList("0.0"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 1);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(0)));
	}


	@Test
	public void testDecimalMultipleResults() throws Exception {
		this.generator.setColumnName(COLUMN_4);
		this.generator.setColumnValueList(CollectionUtils.createList("0.1", "0.2", "0.3"));
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		Assertions.assertEquals(output.getTotalRowCount(), 3);
		Assertions.assertTrue(compareDataRows(output.getRow(0), this.sampleDataTable.getRow(1)));
		Assertions.assertTrue(compareDataRows(output.getRow(1), this.sampleDataTable.getRow(2)));
		Assertions.assertTrue(compareDataRows(output.getRow(2), this.sampleDataTable.getRow(3)));
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Edge Cases                                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidColumnName() throws Exception {
		Assertions.assertThrows(ValidationException.class, () -> {
			this.generator.setColumnName("Some invalid name");
			this.generator.setColumnValueList(Collections.emptyList());
			DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		});
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Compares the given data rows for equality.
	 *
	 * @param source the source data row
	 * @param target the target data row
	 * @return <code>true</code> if the data rows are equal, or <code>false</code> otherwise
	 */
	private static boolean compareDataRows(DataRow source, DataRow target) {
		return source.getRowValueMap().equals(target.getRowValueMap());
	}
}
