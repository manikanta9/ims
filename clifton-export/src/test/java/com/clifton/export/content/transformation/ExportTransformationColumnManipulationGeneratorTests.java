package com.clifton.export.content.transformation;

import com.clifton.core.dataaccess.datatable.DataColumn;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.datatable.impl.DataColumnImpl;
import com.clifton.core.dataaccess.datatable.impl.DataRowImpl;
import com.clifton.core.dataaccess.datatable.impl.PagingDataTableImpl;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.validation.ValidationException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.stream.IntStream;


/**
 * Test class for {@link ExportTransformationColumnManipulationGenerator}.
 *
 * @author MikeH
 */
public class ExportTransformationColumnManipulationGeneratorTests {

	private ExportTransformationColumnManipulationGenerator generator = new ExportTransformationColumnManipulationGenerator();


	////////////////////////////////////////////////////////////////////////////
	////////            Initialize Sample Data                          ////////
	////////////////////////////////////////////////////////////////////////////


	private static final int NUM_COLUMNS = 4;
	private static final int NUM_ROWS = 4;

	private DataTable sampleDataTable;


	@BeforeEach
	public void setup() {
		// Build sample data table
		DataColumn[] columns = new DataColumn[NUM_COLUMNS];
		for (int colNum = 0; colNum < columns.length; colNum++) {
			columns[colNum] = new DataColumnImpl(getExpectedColumnName(colNum), java.sql.Types.VARCHAR);
		}

		this.sampleDataTable = new PagingDataTableImpl(columns);
		for (int rowNum = 0; rowNum < NUM_ROWS; rowNum++) {
			Object[] rowValues = new Object[columns.length];
			for (int colNum = 0; colNum < rowValues.length; colNum++) {
				rowValues[colNum] = getExpectedRowValue(rowNum, colNum);
			}
			this.sampleDataTable.addRow(new DataRowImpl(this.sampleDataTable, rowValues));
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Transformation Results                     ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRetainAllColumns() {
		int[] columnIndices = IntStream.range(0, this.sampleDataTable.getColumnCount()).toArray();
		performAndAssertTransformation(columnIndices);
	}


	@Test
	public void testRetainOneColumn() {
		performAndAssertTransformation(0);
	}


	@Test
	public void testRetainMultipleColumns() {
		performAndAssertTransformation(0, 1);
	}


	@Test
	public void testReorderSomeColumns() {
		performAndAssertTransformation(1, 0);
	}


	@Test
	public void testReorderAllColumns() {
		performAndAssertTransformation(2, 1, 0);
	}


	@Test
	public void testExcludeOneColumn() {
		performAndAssertTransformation(0, 2);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Test Edge Cases                                 ////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testInvalidColumnName() {
		final String columnName = "Some non-existent column";
		TestUtils.expectException(ValidationException.class, () -> {
			this.generator.setIncludedColumnNames(new String[]{columnName});
			DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		}, String.format("Unable to find column [%s] within the data table.", columnName));
	}


	@Test
	public void testInvalidNoColumns() {
		TestUtils.expectException(ValidationException.class, () -> {
			this.generator.setIncludedColumnNames(new String[]{});
			DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0, null, null);
		}, "Unable to process data table re-mappings: No columns to include have been specified.");
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Utility Methods                                 ////////
	////////////////////////////////////////////////////////////////////////////


	private static String[] getExpectedColumnNames(int... columnIndices) {
		String[] columnNames = new String[columnIndices.length];
		for (int i = 0; i < columnNames.length; i++) {
			columnNames[i] = getExpectedColumnName(columnIndices[i]);
		}
		return columnNames;
	}


	private static String getExpectedColumnName(int column) {
		return String.format("Column [%d]", column);
	}


	private static String[] getExpectedRowValues(int row, int... columnIndices) {
		String[] rowValues = new String[columnIndices.length];
		for (int i = 0; i < rowValues.length; i++) {
			rowValues[i] = getExpectedRowValue(row, columnIndices[i]);
		}
		return rowValues;
	}


	private static String getExpectedRowValue(int row, int column) {
		return String.format("Row [%d] Column [%d]", row, column);
	}


	private void performAndAssertTransformation(int... expectedColumns) {
		// Run transformation
		String[] columnNames = IntStream.of(expectedColumns)
				.mapToObj(ExportTransformationColumnManipulationGeneratorTests::getExpectedColumnName)
				.toArray(String[]::new);
		this.generator.setIncludedColumnNames(columnNames);
		DataTable output = this.generator.generateTransformation(this.sampleDataTable, 0,null, null);

		// Verify correct number and contents of rows
		AssertUtils.assertEquals(this.sampleDataTable.getTotalRowCount(), output.getTotalRowCount(), "The number of rows in the resulting table does not match the expected number of rows. Expected [%d]. Found [%d].", this.sampleDataTable.getTotalRowCount(), output.getTotalRowCount());
		for (int rowNumber = 0; rowNumber < output.getTotalRowCount(); rowNumber++) {
			DataRow row = output.getRow(rowNumber);
			Object[] actualRowValues = new Object[output.getColumnCount()];
			for (int columnNumber = 0; columnNumber < actualRowValues.length; columnNumber++) {
				actualRowValues[columnNumber] = row.getValue(columnNumber);
			}
			Object[] expectedRowValues = getExpectedRowValues(rowNumber, expectedColumns);
			Assertions.assertArrayEquals(expectedRowValues, actualRowValues);
		}
	}
}
