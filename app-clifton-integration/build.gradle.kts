import com.clifton.gradle.plugin.artifact.CliftonSchemaPlugin
import com.clifton.gradle.plugin.artifact.MigrationExec

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	// Used by Crowd
	runtimeOnly("org.codehaus.xfire:xfire-aegis:1.2.6") {
		exclude(module = "XmlSchema")
		exclude(module = "jaxen")
		exclude(module = "junit")
	}

	// JSP API library
	implementation("javax.servlet.jsp:jsp-api:2.2")
	//For Pentaho
	implementation("org.apache.logging.log4j:log4j-1.2-api")

	// for Windows service
	implementation("winrun4j:winrun4j:0.4.5")

	implementation("pentaho:kettle-core:6.1.0.1-196")
	implementation("pentaho:kettle-dbdialog:6.1.0.1-196")
	implementation("pentaho:kettle-engine:6.1.0.1-196")
	implementation("pentaho:kettle5-log4j-plugin:6.1.0.1-196")

	implementation("pentaho:commons-vfs:2-2.1-20150824")
	implementation("pentaho:jxl:2.6.12")
	implementation("pentaho:js:1.7R3")
	implementation("pentaho:janino:2.5.16")
	implementation("pentaho:jug-lgpl:2.0.0")
	implementation("jaxen:jaxen:1.1.1")

	implementation("pentaho:metastore:6.1.0.1-196")
	implementation("pentaho:libformula:6.1.0.1-196")
	implementation("pentaho:libbase:6.1.0.1-196")
	//Integration breaks if you use 1.0.3; 1.0.2 needed for pentaho XML transformations (advent files)
	implementation("org.scannotation:scannotation:1.0.2")

	//We zip the plugins directory in pentaho and store in nexus; large (500+ megs), but allows for all
	//plugins to be available and loaded as needed through the pluginList params of the pentahoTransformer
	javascript("pentaho:plugins:6.1.0.1-196@zip")

	implementation("net.sourceforge.jtds:jtds:1.3.1")

	// springIntegrationVersion is configured in gradle.properties so it can be used globally
	implementation("org.springframework.integration:spring-integration-core")
	implementation("org.springframework.integration:spring-integration-file")
	implementation("org.springframework.integration:spring-integration-sftp")


	// WebJars
	// implementation("org.webjars:extjs:3.3.0") // TODO: Switch to official webjars distribution
	implementation("sencha:extjs:3.3.0:clifton-webjar")
	implementation("sencha:extjs-printer:1.0.0:webjar")
	implementation("org.webjars.npm:jsoneditor:5.5.7")
	implementation("org.webjars:google-diff-match-patch:20121119-1")

	testImplementation("org.mockftpserver:MockFtpServer:2.8.0")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	javascript(project(":clifton-core"))
	javascript(project(":clifton-core:clifton-core-messaging"))
	javascript(project(":clifton-security"))
	javascript(project(":clifton-system"))
	javascript(project(":clifton-system:clifton-system-search"))
	javascript(project(":clifton-system:clifton-system-statistic"))
	javascript(project(":clifton-system:clifton-system-note"))
	javascript(project(":clifton-system:clifton-system-navigation"))
	javascript(project(":clifton-system:clifton-system-priority"))
	javascript(project(":clifton-system:clifton-system-query"))
	javascript(project(":clifton-business"))
	javascript(project(":clifton-business:clifton-business-contact"))
	javascript(project(":clifton-calendar"))
	javascript(project(":clifton-calendar:clifton-calendar-schedule"))
	javascript(project(":clifton-batch"))
	javascript(project(":clifton-document"))
	javascript(project(":clifton-export"))
	javascript(project(":clifton-fax"))
	javascript(project(":clifton-notification"))
	javascript(project(":clifton-workflow"))
	javascript(project(":clifton-websocket"))
	javascript(project(":clifton-integration"))

	implementation(project(":clifton-core"))
	implementation(project(":clifton-core:clifton-core-messaging"))
	implementation(project(":clifton-security"))
	implementation(project(":clifton-system"))
	implementation(project(":clifton-system:clifton-system-search"))
	implementation(project(":clifton-system:clifton-system-statistic"))
	implementation(project(":clifton-system:clifton-system-note"))
	implementation(project(":clifton-system:clifton-system-navigation"))
	implementation(project(":clifton-system:clifton-system-priority"))
	implementation(project(":clifton-system:clifton-system-query"))
	implementation(project(":clifton-business"))
	implementation(project(":clifton-business:clifton-business-contact"))
	implementation(project(":clifton-calendar"))
	implementation(project(":clifton-calendar:clifton-calendar-schedule"))
	implementation(project(":clifton-batch"))
	implementation(project(":clifton-document"))
	implementation(project(":clifton-export"))
	implementation(project(":clifton-fax"))
	implementation(project(":clifton-notification"))
	implementation(project(":clifton-workflow"))
	implementation(project(":clifton-websocket"))
	implementation(project(":clifton-integration"))

	testFixturesApi(testFixtures(project(":clifton-core")))
}

/*
 * Enforce database restore order when running parallel restores. Some databases reference other databases (such as the IMS database) during the restore process and will fail if
 * executed while any of the referenced databases are in a restoring state. This only affects builds where multiple database restores are simultaneously requested.
 */
tasks.named(CliftonSchemaPlugin.DB_MIGRATE_RESTORE_TASK_NAME, MigrationExec::class) { mustRunAfter(":app-clifton-ims:${CliftonSchemaPlugin.DB_MIGRATE_RESTORE_TASK_NAME}") }
