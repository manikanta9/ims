package com.clifton.integration;

import com.clifton.core.test.BasicProjectTests;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;


/**
 * The {@link IntegrationAppProjectBasicTests} are disabled due to the current package structure. The {@link BasicProjectTests}
 * find relevant beans for testing by using the package name - i.e. com.clifton.integration, which is identical to the package name
 * used in the clifton-integration module. These tests may currently fail, or may fail after future updates, because of beans from
 * the clifton-integration module. Rather than adding skipMethods/skipServices, we'll leave this disabled until the package structure
 * can be updated (For example, using com.clifton.integration.app)
 *
 * @author lnaylor
 */
@ContextConfiguration
@ExtendWith(SpringExtension.class)
@Disabled
public class IntegrationAppProjectBasicTests extends BasicProjectTests {


	@Override
	public String getProjectPrefix() {
		return "integration";
	}


	@Override
	protected void configureApprovedClassImports(List<String> imports) {
		imports.add("java.security.SecureRandom");
		imports.add("org.apache.commons.");
		imports.add("org.springframework.integration.file.");
		imports.add("org.springframework.messaging.");
		imports.add("com.atlassian.crowd.integration.springsecurity.");
		imports.add("org.pentaho.di.");
		imports.add("org.springframework.security.core.");
		imports.add("org.springframework.util.ResourceUtils");
		imports.add("java.sql.SQLException");
	}


	@Override
	protected void configureApprovedTestClassImports(List<String> imports) {
		super.configureApprovedTestClassImports(imports);
		imports.add("org.mockftpserver.fake.");
		imports.add("org.apache.xbean.spring.context.FileSystemXmlApplicationContext");
		imports.add("org.springframework.integration.channel.AbstractMessageChannel");
		imports.add("org.springframework.messaging.support.ChannelInterceptor");
		imports.add("org.pentaho.di.core.plugins.");
	}


	@Override
	protected List<String> getAllowedContextManagedBeanSuffixNames() {
		List<String> basicProjectAllowedSuffixNamesList = super.getAllowedContextManagedBeanSuffixNames();

		basicProjectAllowedSuffixNamesList.add("Job");
		basicProjectAllowedSuffixNamesList.add("integrationExportHandlerAWSS3");
		basicProjectAllowedSuffixNamesList.add("integrationExportHandlerFax");
		basicProjectAllowedSuffixNamesList.add("Enabler");
		basicProjectAllowedSuffixNamesList.add("Interceptor");
		return basicProjectAllowedSuffixNamesList;
	}
}
