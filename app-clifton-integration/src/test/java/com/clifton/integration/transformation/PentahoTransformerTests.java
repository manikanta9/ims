package com.clifton.integration.transformation;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;
import com.clifton.integration.incoming.transformation.IntegrationTransformationTypes;
import com.clifton.integration.incoming.transformation.IntegrationTransformationUtilHandlerImpl;
import com.clifton.integration.incoming.transformer.FileTransformerCommand;
import com.clifton.integration.incoming.transformer.PentahoTransformer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.pentaho.di.core.plugins.PluginFolder;
import org.pentaho.di.core.plugins.StepPluginType;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author mwacker
 */
public class PentahoTransformerTests {

	private final Path basePath = Paths.get("src/test/java/com/clifton/integration/transformation");
	// assumes gradle root directory is at app-clifton-integration
	private final Path pluginPath = Paths.get("build/dist/app-clifton-integration/plugins/kettle-xml-plugin");


	@Test
	public void testTransformation() throws IOException {
		File outFile1 = Files.createFile(Paths.get(this.basePath.toString(), "TestFileOutput.csv")).toFile();
		File outFile2 = Files.createFile(Paths.get(this.basePath.toString(), "TestFileOutput2.csv")).toFile();
		try {
			doTransformations(outFile1, outFile2);

			Assertions.assertTrue(outFile1.exists());
			Assertions.assertTrue(outFile2.exists());

			String output1 = FileUtils.readFileToString(outFile1.getAbsolutePath());
			Assertions.assertEquals("Name,Description,Value,Symbol,AdjustedValue\r\n" +
					"Account1,The first account, 523100.23,ESM5,5231002.3\r\n" +
					"Account2,The second account, 1250010.52,ESM5,12500105.2", output1.trim());

			String output2 = FileUtils.readFileToString(outFile2.getAbsolutePath());
			Assertions.assertEquals("Name,Description,Value,Symbol,AdjustedValue\r\n" +
					"Account1,The first account, 523100.23,ESM5,10462004.6\r\n" +
					"Account2,The second account, 1250010.52,ESM5,25000210.4", output2.trim());
		}
		finally {
			if (outFile1.exists()) {
				FileUtils.delete(outFile1);
			}
			if (outFile2.exists()) {
				FileUtils.delete(outFile2);
			}
		}
	}


	@Test
	public void testXmlTransformation() {
		File outFile = this.basePath.resolve("TestXmlFileOutput.csv").toFile();
		try {
			doTransformations(outFile);

			Assertions.assertTrue(outFile.exists());

			String output = FileUtils.readFileToString(outFile.getAbsolutePath());
			Assertions.assertEquals("xml_element_id,PortfolioCode,PostCode,TransactionCode,TransactionCodeDesc,SecurityType,SecurityTypeDesc,SecurityISOCode,SecurityIdentifier,TradeDate,SettlementDate,Quantity,DestinationSecurityType,DestinationISOCode,DestinationSecurityIdentifier,TradeDateExchangeRate,NetTradeAmount,OriginalCostDate,OriginalCostDate1,OriginalCostAmount,OriginalCostAmount1,OriginalFace,OriginalExchangeRate,Location,Ticker,Cusip,Sedol,ISIN,QUIK,BloombergTicker,RIC,SwapContractID,GMISecurityIdentifier,SwapFinancing,SecurityDescription,AssetType,UnderlyingTicker,UnderlyingCusip,UnderlyingSEDOL,UnderlyingISIN,UnderlyingBloombergTicker,UnderlyingRIC,UnderlyingSecurityDescription,SecurityCountryCode,SettlementISOCode,ExchangeNameorCode,MarketOrEndOfTheDayPrice,PriceFactor,PriceInBaseCurrency,SettlementDatePrice,TradeDatePricePercentageDisplay,SettlementDatePricePercentageDisplay,StrikePrice,AmericanOrEuropeanOptionType,ValueAddedTax,AccruedInterestInBaseCurrency,AccruedInterest,CouponRate,MaturityDate,LastTradeDate,ExchangeRateLocalToSettle,NetTradeAmountIn,GrossTradeAmount,OpenTradeEquity,LotLocation,PortfolioName,AccountISOCode,SubAccountNumber,SubAccountName,ClientReferenceNumber,CustodianCode,CustodianName,CustodianBrokerIdentificationCode,RoutingCode,APSpecName,APInternalID,APRecordNumber\r\n" +
					" 6,941845136,,li,Deliver In (Long),tb,Treasury Bill,USD,912796MK2,20180605,,2372000,aw,USD,client,,2366900.2,,,,,2372000,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,912796MK2,\r\n" +
					" 7,941845136,,li,Deliver In (Long),tb,Treasury Bill,USD,912796NZ8,20180605,,2380000,aw,USD,client,,2363554.2,,,,,2380000,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,912796NZ8,\r\n" +
					" 8,941845136,,li,Deliver In (Long),tn,Treasury Note ,USD,912828B33,20180605,,2364000,aw,USD,client,,2353196.52,,,,,2364000,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,,912828B33,", output.trim());
		}
		finally {
			if (outFile.exists()) {
				FileUtils.delete(outFile);
			}
		}
	}


	private void doTransformations(File outFile) {
		String normalizedRootPath = Paths.get("META-INF", "pentaho").toString();
		IntegrationTransformationUtilHandlerImpl<Object> utilHandler = new IntegrationTransformationUtilHandlerImpl<>();
		utilHandler.setRelativeJobAndSettingsRootPath(normalizedRootPath);
		utilHandler.setTransformationRootPath(normalizedRootPath);
		PentahoTransformer transformer = new PentahoTransformer();
		StepPluginType.getInstance().getPluginFolders().add(new PluginFolder(this.pluginPath.toFile().getAbsolutePath(), true, true));
		transformer.setIntegrationTransformationUtilHandler(utilHandler);

		IntegrationImportRun run = new IntegrationImportRun();
		run.setId(-10);

		File testFile = this.basePath.resolve("TestFile.xml").toFile();

		List<IntegrationTransformationFile> transformationFileList = new ArrayList<>();
		IntegrationTransformationFile file = new IntegrationTransformationFile();

		file.setFileName(this.basePath.resolve("AdventTransformation_All.ktr").toAbsolutePath().toString());
		file.setTransformationFileNameFullPath(true);
		IntegrationTransformation transformation = new IntegrationTransformation();
		transformation.setType(IntegrationTransformationTypes.DATA_TRANSFORMATION);
		file.setTransformation(transformation);
		transformationFileList.add(file);

		Map<String, String> parameters = new HashMap<>();
		parameters.put("OUTPUT_FILE_NAME", FileUtils.getFileNameWithoutExtension(outFile.getAbsolutePath()));

		FileTransformerCommand fileTransformerCommand = new FileTransformerCommand(run.getId(), testFile, transformationFileList, parameters);
		transformer.doTransform(fileTransformerCommand);
	}


	private void doTransformations(File outFile1, File outFile2) {
		String normalizedRootPath = Paths.get("META-INF", "pentaho").toString();
		IntegrationTransformationUtilHandlerImpl<Object> utilHandler = new IntegrationTransformationUtilHandlerImpl<>();
		utilHandler.setRelativeJobAndSettingsRootPath(normalizedRootPath);
		utilHandler.setTransformationRootPath(normalizedRootPath);
		PentahoTransformer transformer = new PentahoTransformer();
		transformer.setIntegrationTransformationUtilHandler(utilHandler);

		IntegrationImportRun run = new IntegrationImportRun();
		run.setId(-10);

		File testFile = this.basePath.resolve("TestFile.csv").toFile();

		List<IntegrationTransformationFile> transformationFileList = new ArrayList<>();
		IntegrationTransformationFile file = new IntegrationTransformationFile();

		file.setFileName(this.basePath.resolve("TestFileTransformation.ktr").toAbsolutePath().toString());
		file.setTransformationFileNameFullPath(true);
		IntegrationTransformation transformation = new IntegrationTransformation();
		transformation.setType(IntegrationTransformationTypes.DATA_TRANSFORMATION);
		file.setTransformation(transformation);
		transformationFileList.add(file);

		IntegrationTransformationFile fileToFinal = new IntegrationTransformationFile();
		fileToFinal.setFileName(this.basePath.resolve("TestFileTransformationToFinal.ktr").toAbsolutePath().toString());
		fileToFinal.setTransformationFileNameFullPath(true);
		transformation = new IntegrationTransformation();
		transformation.setType(IntegrationTransformationTypes.DATA_TRANSFORMATION);
		fileToFinal.setTransformation(transformation);
		transformationFileList.add(fileToFinal);


		Map<String, String> parameters = new HashMap<>();
		parameters.put("OUTPUT_FILENAME", FileUtils.getFileNameWithoutExtension(outFile1.getAbsolutePath()));
		parameters.put("OUTPUT_FILENAME_TO_FINAL", FileUtils.getFileNameWithoutExtension(outFile2.getAbsolutePath()));

		FileTransformerCommand fileTransformerCommand = new FileTransformerCommand(run.getId(), testFile, transformationFileList, parameters);
		transformer.doTransform(fileTransformerCommand);
	}
}
