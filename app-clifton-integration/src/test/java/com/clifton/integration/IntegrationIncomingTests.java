package com.clifton.integration;

import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.file.IntegrationDirectory;
import com.clifton.integration.file.IntegrationFileServiceImpl;
import com.clifton.integration.incoming.file.IntegrationFileGateway;
import org.apache.xbean.spring.context.FileSystemXmlApplicationContext;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.integration.channel.AbstractMessageChannel;
import org.springframework.messaging.support.ChannelInterceptor;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * This class utilizes the gateways defined in the loaded context files.
 * The test manually triggers the gateway to send a string payload of files to a splitter which then is sent
 * to the respective file processing change.
 * Processing continues from there and comments in the context file itself explain the process further.
 *
 * @author stevenf
 */
public class IntegrationIncomingTests {

	private static final String WORKING_DIR = "working";
	private static final String COM_CLIFTON_INTEGRATION = "com/clifton/integration/";


	@BeforeEach
	public void initializeTests() throws IOException {
		//Clean the working dir in case of previous test failure.
		File workingDir = new File(WORKING_DIR);
		if (workingDir.exists()) {
			delete(new File(WORKING_DIR).getAbsoluteFile());
		}
	}


	@Test
	public void testFileProcessing() throws IOException {
		ConfigurableApplicationContext context = new FileSystemXmlApplicationContext("classpath:" + COM_CLIFTON_INTEGRATION + "service-clifton-integration-tests-context.xml");
		final IntegrationFileGateway fileListGateway = context.getBean("integrationFileListGateway", IntegrationFileGateway.class);

		Resource x = new ClassPathResource(COM_CLIFTON_INTEGRATION + "incoming/files/manual/");
		File[] fileArray = copyFilesToWorkingDir(FileUtils.getFilesFromDirectory(FilePath.forFile(x.getFile()), true).keySet().stream().map(f -> new File(f.getPath())).toArray(File[]::new));
		fileListGateway.sendFileListToChannel(Arrays.asList(fileArray));

		doValidationAndCleanup(context);

		context.close();
	}


	@Test
	public void testSftpFileProcessing() throws IOException {
		ConfigurableApplicationContext context = new FileSystemXmlApplicationContext("classpath:" + COM_CLIFTON_INTEGRATION + "service-clifton-integration-tests-context.xml");
		final IntegrationFileGateway fileListGateway = context.getBean("sftpFileListGateway", IntegrationFileGateway.class);

		//Disable interceptor so it doesn't try to remove file from SFTP server.
		AbstractMessageChannel channel = context.getBean("sftpProcessFileChannel", AbstractMessageChannel.class);
		for (ChannelInterceptor interceptor : channel.getInterceptors()) {
			channel.removeInterceptor(interceptor);
		}

		Resource x = new ClassPathResource(COM_CLIFTON_INTEGRATION + "incoming/files/sftp/");
		File[] fileArray = copyFilesToWorkingDir(FileUtils.getFilesFromDirectory(FilePath.forFile(x.getFile()), true).keySet().stream().map(f -> new File(f.getPath())).toArray(File[]::new));
		fileListGateway.sendFileListToChannel(Arrays.asList(fileArray));

		doValidationAndCleanup(context);

		context.close();
	}


	private void doValidationAndCleanup(ConfigurableApplicationContext context) throws IOException {
		IntegrationFileServiceImpl integrationFileService = (IntegrationFileServiceImpl) context.getBean("integrationFileService");
		String processedDirRoot = integrationFileService.getIntegrationSystemFilePathMap().get(IntegrationDirectory.PROCESSED);

		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/Citigroup/rosecgmiBalancesCOB_rosemary2_20160210.csv").getAbsoluteFile().isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/Citigroup/rosecgmiBalancesCOB_rosemary2_20160211.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/JPMorgan/DataFeed_ExchangeRate_20160215.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/JPMorgan/DataFeed_ExchangeRate_20160216.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/Morgan Stanley/20160208.clifton.cashJrnlTxn.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/Morgan Stanley/20160208.clifton.collateral.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/UBS/Clifton_DailyStatement_20120814.pdf").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/broker/daily/UBS/CliftonCollateral_20160208.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/manager/Amalgamated/clifton_hold_20160211.txt").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/manager/Amalgamated/clifton_hold_20160212.txt").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/manager/Northern/SRD513_CSM_20160212_233745.txt").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/manager/Northern/SRD513_CSM_20160215_223658.txt").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/manager/WellsFargo/20160325.wf3.csv").isFile());
		Assertions.assertTrue(new File(processedDirRoot + "/archive/manager/WellsFargo/DAYPND_325735.CSV.20160209.csv").isFile());

		File processedDir = new File(processedDirRoot.substring(0, processedDirRoot.indexOf('/')));
		//Ensure only expected number files are contained in the directory before deleting.
		Assertions.assertEquals(14, listFiles(processedDir).size());
		delete(processedDir.getAbsoluteFile());
		delete(new File(WORKING_DIR).getAbsoluteFile());
	}


	private File[] copyFilesToWorkingDir(File[] files) throws IOException {
		File[] returnVal = new File[files.length];
		for (int i = 0; i < files.length; i++) {
			File sourceFile = files[i];
			File targetFile = Paths.get(WORKING_DIR, sourceFile.getParentFile().getName(), sourceFile.getName()).toFile().getAbsoluteFile();
			FileUtils.copyFileCreatePath(sourceFile, targetFile);
			returnVal[i] = targetFile;
		}
		return returnVal;
	}


	private List<File> listFiles(File file) {
		List<File> files = new ArrayList<>();
		if (file != null) {
			if (file.isDirectory()) {
				for (File f : CollectionUtils.createList(file.listFiles())) {
					files.addAll(listFiles(f));
				}
			}
			else {
				files.add(file);
			}
		}
		return files;
	}


	private void delete(File file) throws IOException {
		if (file != null) {
			if (file.isDirectory()) {
				for (File f : CollectionUtils.createList(file.listFiles())) {
					delete(f);
				}
			}
			if (!file.delete()) {
				throw new FileNotFoundException("Failed to delete file: " + file);
			}
		}
	}
}
