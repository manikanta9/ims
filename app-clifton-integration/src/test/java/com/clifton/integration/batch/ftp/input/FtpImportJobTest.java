package com.clifton.integration.batch.ftp.input;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.ftp.FtpProtocols;
import com.clifton.core.util.status.Status;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.io.TempDir;
import org.mockftpserver.fake.FakeFtpServer;
import org.mockftpserver.fake.UserAccount;
import org.mockftpserver.fake.filesystem.DirectoryEntry;
import org.mockftpserver.fake.filesystem.FileEntry;
import org.mockftpserver.fake.filesystem.FileSystem;
import org.mockftpserver.fake.filesystem.UnixFakeFileSystem;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.File;
import java.nio.file.Paths;
import java.util.HashMap;


/**
 * @author TerryS
 */
@Disabled("Build server doesn't allow connection to localhost.")
@ExtendWith(MockitoExtension.class)
class FtpImportJobTest {

	private static final String homeDirectory = "/ftp/directory";

	@Mock
	public SecuritySecretService securitySecretService;

	@Spy
	@InjectMocks
	public FtpImportJob ftpImportJob;

	@TempDir
	public File targetDirectory;

	private FakeFtpServer fakeFtpServer;

	private FileSystem fileSystem;


	@BeforeEach
	public void setup() {
		UserAccount userAccount = new UserAccount("user", "password", homeDirectory);
		this.fakeFtpServer = new FakeFtpServer();
		this.fakeFtpServer.addUserAccount(userAccount);

		this.fileSystem = new UnixFakeFileSystem();
		this.fileSystem.add(new DirectoryEntry(homeDirectory));
		this.fakeFtpServer.setFileSystem(this.fileSystem);

		this.fakeFtpServer.start();

		this.ftpImportJob.setPort(this.fakeFtpServer.getServerControlPort());
		this.ftpImportJob.setUsername(userAccount.getUsername());

		SecuritySecret securitySecret = new SecuritySecret();
		securitySecret.setSecretString(userAccount.getPassword());
		securitySecret.setSecretValue(userAccount.getPassword().getBytes());
		this.ftpImportJob.setPasswordSecret(securitySecret);

		this.ftpImportJob.setUrl("localhost");
		this.ftpImportJob.setFtpProtocol(FtpProtocols.FTP);

		this.ftpImportJob.setFilterRegex(".*");
		this.ftpImportJob.setReformatFileNameEffectiveDate(false);
		this.ftpImportJob.setTargetPath(this.targetDirectory.getPath());
		this.ftpImportJob.setTemplateConverter(new FreemarkerTemplateConverter());

		Mockito.doReturn(securitySecret).when(this.securitySecretService).decryptSecuritySecret(ArgumentMatchers.any());
	}


	@AfterEach
	public void teardown() {
		this.fakeFtpServer.stop();
	}


	@Test
	public void basicTest() {
		Mockito.doReturn(DateUtils.toDate("06/01/2021")).when(this.ftpImportJob).getReportingDate();
		this.fileSystem.add(new FileEntry(homeDirectory + "/" + "file1.txt", "basicTest"));
		Status status = this.ftpImportJob.run(new HashMap<>());
		Assertions.assertEquals(1, status.getDetailList().size());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());
		Assertions.assertEquals("file1.txt", status.getDetailList().get(0).getNote());

		String[] files = this.targetDirectory.list();
		Assertions.assertNotNull(files);
		Assertions.assertEquals(1, files.length);
		Assertions.assertEquals("file1.txt", files[0]);
		String contents = FileUtils.readFileToString(Paths.get(this.targetDirectory.getAbsolutePath(), files[0]).toFile());

		Assertions.assertEquals("basicTest", contents);
	}


	@Test
	public void writingExtensionTest() {
		Mockito.doReturn(DateUtils.toDate("06/01/2021")).when(this.ftpImportJob).getReportingDate();
		this.fileSystem.add(new FileEntry(homeDirectory + "/" + "file1.txt", "basicTest"));
		this.ftpImportJob.setWriteExtension(true);

		Status status = this.ftpImportJob.run(new HashMap<>());
		Assertions.assertEquals(1, status.getDetailList().size());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());
		Assertions.assertEquals("file1.txt", status.getDetailList().get(0).getNote());

		String[] files = this.targetDirectory.list();
		Assertions.assertNotNull(files);
		Assertions.assertEquals(1, files.length);
		Assertions.assertEquals("file1.txt", files[0]);
		String contents = FileUtils.readFileToString(Paths.get(this.targetDirectory.getAbsolutePath(), files[0]).toFile());

		Assertions.assertEquals("basicTest", contents);
	}


	@Test
	public void fileMappingTest() {
		Mockito.doReturn(DateUtils.toDate("06/01/2021")).when(this.ftpImportJob).getReportingDate();
		this.fileSystem.add(new FileEntry(homeDirectory + "/" + "file1.txt", "fileMappingTest"));
		this.ftpImportJob.setDestinationFileNameMap("[{\"file1.txt\":\"fileMappingTest.txt\"}]");

		Status status = this.ftpImportJob.run(new HashMap<>());
		Assertions.assertEquals(1, status.getDetailList().size());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());
		Assertions.assertEquals("file1.txt", status.getDetailList().get(0).getNote());

		String[] files = this.targetDirectory.list();
		Assertions.assertNotNull(files);
		Assertions.assertEquals(1, files.length);
		Assertions.assertEquals("fileMappingTest.txt", files[0]);
		String contents = FileUtils.readFileToString(Paths.get(this.targetDirectory.getAbsolutePath(), files[0]).toFile());

		Assertions.assertEquals("fileMappingTest", contents);
	}


	@Test
	public void freemarkerFileMappingTest() {
		Mockito.doReturn(DateUtils.toDate("06/01/2021")).when(this.ftpImportJob).getReportingDate();
		this.fileSystem.add(new FileEntry(homeDirectory + "/" + "file1.txt", "freemarkerFileMappingTest"));
		this.ftpImportJob.setDestinationFileNameMap("[{\"file1.txt\":\"freemarkerFileMappingTest_${DATE?string('yyyyMMdd')}.txt\"}]");

		Status status = this.ftpImportJob.run(new HashMap<>());
		Assertions.assertEquals(1, status.getDetailList().size());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());
		Assertions.assertEquals("file1.txt", status.getDetailList().get(0).getNote());

		String[] files = this.targetDirectory.list();
		Assertions.assertNotNull(files);
		Assertions.assertEquals(1, files.length);
		Assertions.assertEquals("freemarkerFileMappingTest_20210601.txt", files[0]);
		String contents = FileUtils.readFileToString(Paths.get(this.targetDirectory.getAbsolutePath(), files[0]).toFile());

		Assertions.assertEquals("freemarkerFileMappingTest", contents);
	}


	@Test
	public void freemarkerFolderNameTest() {
		Mockito.doReturn(DateUtils.toDate("06/01/2021")).when(this.ftpImportJob).getReportingDate();
		this.fileSystem.add(new FileEntry(homeDirectory + "/20210601/" + "file1.txt", "freemarkerFolderNameTest"));
		Status status = this.ftpImportJob.run(new HashMap<>());
		Assertions.assertEquals(0, status.getDetailList().size());

		this.ftpImportJob.setFtpServerFolder("${DATE?string(\"yyyyMMdd\")}");
		status = this.ftpImportJob.run(new HashMap<>());
		Assertions.assertEquals(1, status.getDetailList().size());
		Assertions.assertEquals("Success", status.getDetailList().get(0).getCategory());
		Assertions.assertEquals("file1.txt", status.getDetailList().get(0).getNote());

		String[] files = this.targetDirectory.list();
		Assertions.assertNotNull(files);
		Assertions.assertEquals(1, files.length);
		Assertions.assertEquals("file1.txt", files[0]);
		String contents = FileUtils.readFileToString(Paths.get(this.targetDirectory.getAbsolutePath(), files[0]).toFile());

		Assertions.assertEquals("freemarkerFolderNameTest", contents);
	}
}
