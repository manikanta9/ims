package com.clifton.integration.incoming.file.batch;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileServiceImpl;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.integration.source.IntegrationSourceService;
import org.hibernate.Criteria;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Random;


/**
 * @author TerryS
 */
@ExtendWith(MockitoExtension.class)
@Disabled
class IntegrationFileTransferJobTest {

	//This is a shared network location where files will be placed for pickup by the integration server
	private static final String FILE_BASE_PATH = "//MSP-730-02.paraport.com/Development/Integration Testing/temp/";
	private final Random random = new SecureRandom();

	@Mock
	public CalendarDateGenerationHandler calendarDateGenerationHandler;

	@Mock
	public IntegrationSourceService integrationSourceService;

	@Mock
	public AdvancedUpdatableDAO<IntegrationFileDefinition, Criteria> integrationFileDefinitionDAO;

	@Mock
	public AdvancedUpdatableDAO<IntegrationFile, Criteria> integrationFileDAO;

	@Spy
	public IntegrationFileService integrationFileService = new IntegrationFileServiceImpl();

	@Spy
	public TemplateConverter templateConverter = new FreemarkerTemplateConverter();

	@Spy
	@InjectMocks
	public IntegrationFileTransferJob integrationFileTransferJob = new IntegrationFileTransferJob();

	public FileContainer testDirectory;

	public FileContainer sourceDirectory;
	public FileContainer targetDirectory;

	private FileContainer sourceFile;
	private FileContainer targetFile;

	private final String directorySuffix = Integer.toString(this.random.nextInt(Short.MAX_VALUE + 1));


	@BeforeEach
	public void setup() {
		this.testDirectory = FileContainerFactory.getFileContainer(FILE_BASE_PATH);
		Assertions.assertTrue(this.testDirectory.exists());

		this.sourceDirectory = FileContainerFactory.getFileContainer(Paths.get(this.testDirectory.getPath(), "Source_" + this.directorySuffix).toAbsolutePath().toString());
		FileUtils.createDirectory(this.sourceDirectory.getAbsolutePath());
		this.targetDirectory = FileContainerFactory.getFileContainer(Paths.get(this.testDirectory.getPath(), "Target_" + this.directorySuffix).toAbsolutePath().toString());
		FileUtils.createDirectory(this.targetDirectory.getAbsolutePath());

		this.integrationFileTransferJob.setFromPath(this.sourceDirectory.getAbsolutePath());
		this.integrationFileTransferJob.setToPath(this.targetDirectory.getAbsolutePath());
		this.integrationFileTransferJob.setIntegrationFolderDestination(true);
		this.integrationFileTransferJob.setDateGenerationOption(DateGenerationOptions.TODAY);
		this.integrationFileTransferJob.setFilterPattern("*-${DATE?string(\"yyyyMMdd\")}*");
		this.integrationFileTransferJob.setFileNameRegex("(?i)^([a-zA-Z0-9]{2}.)(${DATE?string(\"yyyyMMdd\")}).*(PSB|LPB|PSX|LPX|TRB|TRX)$");
		this.integrationFileTransferJob.setDestinationExtensionMap("[{\"PSB\":\"seattle.PSX\",\"TRB\":\"seattle.TRX\",\"LPB\":\"LPX\",\"PSX\":\"seattle.PSX\",\"TRX\":\"seattle.TRX\",\"LPX\":\"LPX\"}]");
		this.integrationFileTransferJob.setDaysFromToday(0);

		((IntegrationFileServiceImpl) this.integrationFileService).setIntegrationFileDefinitionDAO(this.integrationFileDefinitionDAO);
		((IntegrationFileServiceImpl) this.integrationFileService).setIntegrationFileDAO(this.integrationFileDAO);

		String sourceFileName = "8F-" + DateUtils.fromDate(new Date(), "yyyyMMdd") + "-TRN-0001.TRX";
		this.sourceFile = FileContainerFactory.getFileContainer(Paths.get(this.sourceDirectory.getAbsolutePath(), sourceFileName).toFile());
		FileUtils.writeStringToFile(this.sourceFile, "Testing Source File");
		Assertions.assertTrue(this.sourceFile.exists());

		Mockito.when(this.calendarDateGenerationHandler.generateDate(DateGenerationOptions.TODAY, 0)).thenReturn(new Date());
		IntegrationSource integrationSource = new IntegrationSource();
		integrationSource.setId(53);
		Mockito.when(this.integrationSourceService.getIntegrationSourceByName(this.targetDirectory.getName())).thenReturn(integrationSource);

		IntegrationFileDefinition integrationFileDefinition = new IntegrationFileDefinition();
		Mockito.when(this.integrationFileDefinitionDAO.findBySearchCriteria(ArgumentMatchers.any())).thenReturn(Collections.singletonList(integrationFileDefinition));
		Mockito.when(this.integrationFileDAO.findBySearchCriteria(ArgumentMatchers.any())).thenReturn(Collections.emptyList());
	}


	@AfterEach
	public void teardown() {
		FileUtils.delete(this.targetFile);
		FileUtils.delete(this.sourceFile);
		FileUtils.delete(this.targetDirectory);
		FileUtils.delete(this.sourceDirectory);
	}


	@Test
	public void targetFileExists() {
		String targetFileName = "8F-" + DateUtils.fromDate(new Date(), "yyyyMMdd") + "-TRN-0001.seattle.TRX";
		this.targetFile = FileContainerFactory.getFileContainer(Paths.get(this.targetDirectory.getAbsolutePath(), targetFileName).toFile());
		FileUtils.writeStringToFile(this.targetFile, "Testing Target File");
		Assertions.assertTrue(this.targetFile.exists());

		Status status = this.integrationFileTransferJob.run(new HashMap<>());
		Assertions.assertEquals(1, status.getErrorCount(), status.getMessage());

		FileContainer targetWritingFile = FileContainerFactory.getFileContainer(Paths.get(this.targetDirectory.getAbsolutePath(), targetFileName + "." + IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION).toFile());
		Assertions.assertFalse(targetWritingFile.exists());
		Assertions.assertTrue(this.targetFile.exists());
		Assertions.assertTrue(this.sourceFile.exists());
	}
}
