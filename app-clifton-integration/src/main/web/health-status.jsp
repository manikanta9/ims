<%@ page import="com.clifton.batch.job.BatchJob" %>
<%@ page import="com.clifton.batch.runner.BatchRunnerService" %>
<%@ page import="com.clifton.core.beans.BaseSimpleEntity" %>
<%@ page import="com.clifton.core.context.Context" %>
<%@ page import="com.clifton.core.context.ContextHandler" %>
<%@ page import="com.clifton.core.health.HealthCheckHandler" %>
<%@ page import="com.clifton.core.health.HealthCheckParameters" %>
<%@ page import="com.clifton.core.messaging.MessageStatus" %>
<%@ page import="com.clifton.core.util.CollectionUtils" %>
<%@ page import="com.clifton.core.util.StringUtils" %>
<%@ page import="com.clifton.core.util.date.TimeTrackedOperation" %>
<%@ page import="com.clifton.core.util.date.TimeUtils" %>
<%@ page import="com.clifton.core.util.validation.ValidationException" %>
<%@ page import="com.clifton.core.util.validation.ValidationUtils" %>
<%@ page import="com.clifton.integration.incoming.server.IntegrationServerStatusProcessorService" %>
<%@ page import="com.clifton.security.secret.SecuritySecretService" %>
<%@ page import="com.clifton.security.user.SecurityUser" %>
<%@ page import="com.clifton.security.user.SecurityUserService" %>
<%@ page import="com.clifton.security.user.search.SecurityUserSearchForm" %>
<%@ page import="org.apache.tomcat.jdbc.pool.DataSourceProxy" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="java.io.Writer" %>
<%@ page import="java.sql.Connection" %>
<%@ page import="java.sql.SQLException" %>
<%@ page import="java.sql.Statement" %>
<%@ page import="java.util.Arrays" %>
<%@ page import="java.util.Collections" %>
<%@ page import="java.util.Comparator" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Set" %>
<body style="FONT-FAMILY: arial,serif; FONT-SIZE: 12px;">
<%!
	final Set<String> messageExclusions = CollectionUtils.createHashSet(MessageStatus.ENVIRONMENT, MessageStatus.DATABASE_NAME, MessageStatus.DATA_SOURCE_URL);


	//Appends the given message status to the builder in a standard format IF it is not in the exclusion list
	void appendMessageStatus(StringBuilder builder, MessageStatus messageStatus) {
		if (!this.messageExclusions.contains(messageStatus.getResponseName())) {
			builder.append(messageStatus.getResponseName()).append(" : ").append(messageStatus.getResponseStatus()).append("<br />");
		}
	}
%><%
	final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletConfig().getServletContext());
	final HealthCheckHandler healthCheckHandler = applicationContext.getBean("healthCheckHandler", HealthCheckHandler.class);
	final ContextHandler contextHandler = applicationContext.getBean("contextHandler", ContextHandler.class);
	final Writer writer = out;

	// Impersonate user for page
	SecurityUser pageUser = new SecurityUser();
	pageUser.setId((short) 999);
	pageUser.setUserName(SecurityUser.SYSTEM_USER);
	contextHandler.setBean(Context.USER_BEAN_NAME, pageUser);

	// Write header information
	HealthCheckParameters healthCheckParameters = healthCheckHandler.getHealthCheckPageParameters(request);
	healthCheckHandler.printSystemInformation(writer);
	healthCheckHandler.printHealthCheckParameterInformation(writer, healthCheckParameters);

	// Execute tests
	healthCheckHandler.printSectionHeader(writer, "TESTS");
	TimeTrackedOperation<Void> healthStatusOperation = TimeUtils.getElapsedTime(() -> {
		/*
		 * CliftonIntegration database access: systemuser should never be deleted
		 * Database maintenance tasks window: 11:00 PM - 12:00 AM
		 */
		healthCheckHandler.runTest("CliftonIntegration DB", 500, Collections.singletonList("11:00 PM - 12:00 AM"), writer, healthCheckParameters, () -> {
			SecurityUserService securityUserService = applicationContext.getBean("securityUserService", SecurityUserService.class);
			SecurityUserSearchForm securityUserSearchForm = new SecurityUserSearchForm();
			securityUserSearchForm.setLimit(1);
			securityUserSearchForm.setUserNameEquals("systemuser");
			SecurityUser user = CollectionUtils.getFirstElementStrict(securityUserService.getSecurityUserList(securityUserSearchForm));
			ValidationUtils.assertNotNull(user, "Cannot find security user: " + SecurityUser.SYSTEM_USER);
		});

		/*
		 * FTP Status
		 */
		healthCheckHandler.runTest("FTP Status", 1500, null, writer, healthCheckParameters, () -> {
			IntegrationServerStatusProcessorService integrationStatusProcessor = applicationContext.getBean("integrationServerStatusProcessorService", IntegrationServerStatusProcessorService.class);
			MessageStatus status = integrationStatusProcessor.getIntegrationFtpStatus();
			StringBuilder statusMessages = new StringBuilder();
			if (status.isError()) {
				throw new ValidationException("Status check returned an error: " + status.getException().getMessage());
			}
			statusMessages.append("<br />");
			appendMessageStatus(statusMessages, status);
			return statusMessages.toString();
		});

		/*
		 * Transformation Status
		 */
		healthCheckHandler.runTest("Transformation", 15000, Arrays.asList("6:40 PM - 7:20 PM", "7:40 PM - 8:20 PM", "8:40 PM - 9:20 PM", "9:40 PM - 10:20 PM", "10:40 PM - 11:50 PM", "12:10 AM - 5:50 AM", "6:10 AM - 7:00 AM", "7:20 AM - 11:50 AM", "12:10 PM - 6:20 PM"), writer, healthCheckParameters, () -> {
			IntegrationServerStatusProcessorService integrationStatusProcessor = applicationContext.getBean("integrationServerStatusProcessorService", IntegrationServerStatusProcessorService.class);
			MessageStatus status = integrationStatusProcessor.getIntegrationTransformationStatus();
			StringBuilder statusMessages = new StringBuilder();
			if (status.isError()) {
				throw new ValidationException("Status check returned an error: " + status.getException().getMessage());
			}
			statusMessages.append("<br />");
			appendMessageStatus(statusMessages, status);
			return statusMessages.toString();
		});

		/*
		 * Database Secret Encryption/Decryption: Checks that a secret can be encrypted and decrypted to/from the database.
		 */
		healthCheckHandler.runTest("Security Secret Status", 3000, null, writer, healthCheckParameters, () -> {
			SecuritySecretService securitySecretService = applicationContext.getBean("securitySecretService", SecuritySecretService.class);
			if (!securitySecretService.doHealthCheck()) {
				throw new RuntimeException("Error occurred conducting Security Secret health check. Review the logs for further details");
			}
			return "<br />";
		});

		/*
		 * Check the data sources.
		 */
		healthCheckHandler.runTest("Data Source Status", 3000, null, writer, healthCheckParameters, () -> {
			StringBuilder statusMessages = new StringBuilder();
			String[] dataSources = applicationContext.getBeanNamesForType(org.apache.tomcat.jdbc.pool.DataSourceProxy.class);
			for (String dataSource : dataSources) {
				statusMessages.append("<br />");
				statusMessages.append(dataSource);
				statusMessages.append(":  ");
				DataSourceProxy db = applicationContext.getBean(dataSource, org.apache.tomcat.jdbc.pool.DataSourceProxy.class);
				String verification = db.getValidationQuery();
				try (Connection connection = db.getConnection(); Statement statement = connection.createStatement()) {
					statement.execute(verification);
					statusMessages.append("OK");
				}
				catch (SQLException e) {
					statusMessages.append("ERROR  ");
					statusMessages.append("<br />");
					String message = e.getMessage();
					message = StringUtils.replace(message, db.getUsername(), "*******");
					statusMessages.append(message);
				}
			}
			return statusMessages.toString();
		});

		/*
		 * Invalid Batch Job Check: Identifies batch jobs that have a RUNNING status but are actually not running
		 */
		healthCheckHandler.runTest("Batch Job Status", 3000, null, writer, healthCheckParameters, () -> {
			final String messageTemplate = "&nbsp;&nbsp;&nbsp;&nbsp;Batch Job Name: %s, ID: %d";
			BatchRunnerService batchRunnerService = applicationContext.getBean("batchRunnerService", BatchRunnerService.class);
			List<BatchJob> invalidBatchJobList = batchRunnerService.getBatchJobListInvalid();
			if (CollectionUtils.isEmpty(invalidBatchJobList)) {
				return;
			}
			invalidBatchJobList.sort(Comparator.comparing(BaseSimpleEntity::getId));
			StringBuilder stringBuilder = new StringBuilder("Batch Jobs with status 'RUNNING' that are not currently running:");
			for (BatchJob invalidBatchJob : invalidBatchJobList) {
				stringBuilder.append("<br />").append(String.format(messageTemplate, invalidBatchJob.getName(), invalidBatchJob.getId()));
			}
			throw new RuntimeException(stringBuilder.toString());
		});
	});

	writer.write(String.format("<br/><b>PROCESSING TIME: %d milliseconds</b>", healthStatusOperation.getElapsedMilliseconds()));
	writer.flush();
%>
</body>
