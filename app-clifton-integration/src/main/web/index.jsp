<%@ page import="com.clifton.core.util.AssertUtils" %>
<%@ page import="org.springframework.beans.factory.config.ConfigurableListableBeanFactory" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%!
	// version is reset every time the server is restarted and should avoid browser caching
	private static final long version = System.currentTimeMillis();


%><%
	// ask the browser not to cache this page
	response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate");
	response.addHeader("Cache-Control", "post-check=0, pre-check=0");
	response.setHeader("Pragma", "no-cache");
	response.setDateHeader("Expires", 0);


	final WebApplicationContext applicationContext = WebApplicationContextUtils.getRequiredWebApplicationContext(getServletConfig().getServletContext());
	final ConfigurableListableBeanFactory beanFactory = (ConfigurableListableBeanFactory) applicationContext.getAutowireCapableBeanFactory();
	final String extjsRootUrl = beanFactory.resolveEmbeddedValue("${application.extjs.rootUrl}");

%>
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title><%=beanFactory.resolveEmbeddedValue("${application.region}")%> Integration System</title>

	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>


	<link rel="SHORTCUT ICON" href="core/images/favicon.ico"/>

	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/ux/gridfilters/css/GridFilters.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/ux/gridfilters/css/RangeMenu.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/resources/css/ext-all.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="<%=extjsRootUrl%>/ux/css/ux-all.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="core/css/application.css?version=<%=version%>" media="screen"/>

	<%-- Used for JSON migration functionality --%>
	<link rel="stylesheet" type="text/css" href="webjars/jsoneditor/5.5.7/dist/jsoneditor.css?version=<%=version%>"/>
	<link rel="stylesheet" type="text/css" href="core/css/jsoneditor-ims-theme.css?version=<%=version%>"/>

	<script type="text/javascript" src="webjars/stomp__stompjs/4.0.3/lib/stomp.min.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/sockjs-client/1.1.4/dist/sockjs.min.js?version=<%=version%>"></script>

	<script type="text/javascript" src="<%=extjsRootUrl%>/adapter/ext/ext-base-debug.js?version=<%=version%>"></script>
	<script type="text/javascript" src="<%=extjsRootUrl%>/ext-all-debug.js?version=<%=version%>"></script>
	<script type="text/javascript" src="<%=extjsRootUrl%>/ux/ux-all-debug.js?version=<%=version%>"></script>
	<script type="text/javascript" src="<%=extjsRootUrl%>/ux/FieldReplicator.js?version=<%=version%>"></script>
	<script type="text/javascript" src="webjars/extjs-printer/1.0.0/Printer-all.js?version=<%=version%>"></script>

	<%-- Used for JSON migration functionality --%>
	<script type="text/javascript" src="webjars/jsoneditor/5.5.7/dist/jsoneditor.min.js?version=<%=version%>"></script>

	<%-- Used for differences in audit trail values --%>
	<script type="text/javascript" src="webjars/google-diff-match-patch/20121119-1/diff_match_patch.js?version=<%=version%>"></script>


	<script type="text/javascript">
		Ext.ns('TCG');
		TCG.ExtRootUrl = '<%=extjsRootUrl%>';
		TCG.PageConfiguration = {
			applicationName: '<%=AssertUtils.assertNotNull(beanFactory.resolveEmbeddedValue("${application.name}"), "Property application.name is null").toLowerCase()%>',
			applicationRegion: '<%=beanFactory.resolveEmbeddedValue("${application.region}")%>',
			externalApplication: {
				names: '<%=AssertUtils.assertNotNull(beanFactory.resolveEmbeddedValue("${application.external.names}"), "Property application.external.names is null").toLowerCase()%>'
						.split(',')
						.filter(name => !!name), // Remove empty values
				rootUri: '<%=beanFactory.resolveEmbeddedValue("${application.external.root.uri}")%>'
			},
			instance: {
				text: '<%=beanFactory.resolveEmbeddedValue("${application.instance.text}")%>',
				color: '<%=beanFactory.resolveEmbeddedValue("${application.instance.text.color}")%>',
				size: '<%=beanFactory.resolveEmbeddedValue("${application.instance.text.size}")%>',
				version: '<%=beanFactory.resolveEmbeddedValue("${application.instance.version}")%>',
				theme: '<%=beanFactory.resolveEmbeddedValue("${application.instance.theme}")%>'
			},
			graylog: {
				apiScheme: '<%=beanFactory.resolveEmbeddedValue("${graylog.api.scheme}")%>',
				apiHost: '<%=beanFactory.resolveEmbeddedValue("${graylog.api.host}")%>'
			},
			websocket: {
				enabled: '<%=beanFactory.resolveEmbeddedValue("${application.websocket.enabled}")%>' === 'true',
				// Use relative path for non-root contexts; path must be normalized (remove redundant slashes) to avoid Spring Security constraints on missing URI path elements
				endpoint: './<%=beanFactory.resolveEmbeddedValue("${application.websocket.servlet.path}")%>/<%=beanFactory.resolveEmbeddedValue("${application.websocket.stomp.path}")%>'.replace(/\/+(?=\/)|\/+$/g, '')
			}
		};
	</script>

	<script type="text/javascript" src="core/js/tcg.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-app.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-cache.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-data.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-file.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-form.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-grid.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-gridfilters.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-gridsummary.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-toolbar.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/js/tcg-tree.js?version=<%=version%>"></script>

	<script type="text/javascript" src="core/core-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="core/core-messaging-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-search-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-statistic-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-navigation-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-note-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-priority-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="system/system-query-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="security/security-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="websocket/websocket-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="websocket/websocket-alert.js?version=<%=version%>"></script>
	<script type="text/javascript" src="calendar/calendar-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="calendar/calendar-schedule-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="batch/batch-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="business/business-contact-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="document/document-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="export/export-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="fax/fax-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="notification/notification-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="workflow/workflow-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="integration/integration-shared.js?version=<%=version%>"></script>
	<script type="text/javascript" src="integration/outgoing/integration-outgoing-shared.js?version=<%=version%>"></script>

	<%-- Application Bootstrap --%>
	<script type="text/javascript" src="app-integration/app-integration-bootstrap.js"></script>

</head>

<body>
<div id="officePluginHolder"></div>
</body>
</html>
