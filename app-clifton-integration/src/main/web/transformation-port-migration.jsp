<%@ page import="com.clifton.core.context.Context" %>
<%@ page import="static org.apache.logging.log4j.web.WebLoggerContextUtils.getServletContext" %>
<%@ page import="com.clifton.core.context.ContextHandler" %>
<%@ page import="com.clifton.core.dataaccess.file.FileWrapper" %>
<%@ page import="com.clifton.core.dataaccess.file.container.FileContainer" %>
<%@ page import="com.clifton.core.dataaccess.file.container.FileContainerFactory" %>
<%@ page import="com.clifton.core.web.multipart.MultipartFileImpl" %>
<%@ page import="com.clifton.document.DocumentManagementService" %>
<%@ page import="com.clifton.document.DocumentRecord" %>
<%@ page import="com.clifton.integration.incoming.transformation.IntegrationTransformationFile" %>
<%@ page import="com.clifton.integration.incoming.transformation.IntegrationTransformationService" %>
<%@ page import="com.clifton.integration.incoming.transformation.search.IntegrationTransformationFileSearchForm" %>
<%@ page import="com.clifton.security.user.SecurityUser" %>
<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.multipart.MultipartFile" %>
<%@ page import="org.w3c.dom.Document" %>
<%@ page import="org.w3c.dom.Node" %>
<%@ page import="org.w3c.dom.NodeList" %>
<%@ page import="javax.xml.parsers.DocumentBuilder" %>
<%@ page import="javax.xml.parsers.DocumentBuilderFactory" %>
<%@ page import="javax.xml.transform.Transformer" %>
<%@ page import="javax.xml.transform.TransformerFactory" %>
<%@ page import="javax.xml.transform.dom.DOMSource" %>
<%@ page import="javax.xml.transform.stream.StreamResult" %>
<%@ page import="javax.xml.xpath.XPath" %>
<%@ page import="javax.xml.xpath.XPathConstants" %>
<%@ page import="javax.xml.xpath.XPathExpressionException" %>
<%@ page import="javax.xml.xpath.XPathFactory" %>
<%@ page import="java.io.IOException" %>
<%@ page import="java.io.InputStream" %>
<%@ page import="java.io.OutputStream" %>
<%@ page import="java.util.ArrayList" %>
<%@ page import="java.util.List" %>
<%@ page import="java.util.Objects" %>

<%!
	private static final String TRANSFORMATION_FILE_TABLE_NAME = "IntegrationTransformationFile";

	private static final DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();

	WebApplicationContext applicationContext = null;


	public void transform(JspWriter out) throws IOException {
		List<DocumentRecord> documentRecordList = getTransformationDocumentRecordList();
		for (DocumentRecord documentRecord : documentRecordList) {
			out.print("Document " + documentRecord.getName() + "<br />");
			try {
				FileWrapper fileWrapper = downloadDocument(documentRecord.getDocumentId());
				out.print("Downloaded " + fileWrapper.getFileName() + " <br />");

				Document document = getDomDocument(fileWrapper);
				out.print("Done Parsing" + " <br />");

				boolean containsPort = containsConnectionPort(document);
				out.print("Contains Port 1433 " + containsPort + " <br />");

				boolean containsAttribute = containsAttributeConnectionPort(document);
				out.print("Contains Attribute Port 1433 " + containsAttribute + " <br />");

				if (containsPort || containsAttribute) {
					DocumentRecord checkedOutDocumentRecord = null;
					try {
						checkedOutDocumentRecord = checkoutDocument(documentRecord);
						out.print("Checked out Document " + " <br />");

						boolean successfullyUpdatedPort = false;
						if (containsPort) {
							successfullyUpdatedPort = replaceConnectionPort(document);
							if (successfullyUpdatedPort) {
								out.print("Replaced Connection Port 1433 " + " <br />");
							}
							else {
								out.print("ERROR:  Replaced Connection Port 1433 " + " <br />");
							}
						}
						boolean successfullyUpdatedAttribute = false;
						if (containsAttribute) {
							successfullyUpdatedAttribute = replaceConnectionAttribute(document);
							if (successfullyUpdatedAttribute) {
								out.print("Replaced Attribute Port 1433 " + " <br />");
							}
							else {
								out.print("ERROR:  Replaced Attribute Port 1433 " + " <br />");
							}
						}
						if (successfullyUpdatedPort || successfullyUpdatedAttribute) {
							outputTransformedDom(document, fileWrapper);
							out.print("Update file on disk " + fileWrapper.getFileName() + " <br />");

							MultipartFile uploadFile = new MultipartFileImpl(fileWrapper.getFileName(), fileWrapper.getFile().toFile());

							DocumentRecord uploadDocument = new DocumentRecord();
							uploadDocument.setDocumentId(checkedOutDocumentRecord.getVersionSeriesCheckedOutId());
							uploadDocument.setFile(uploadFile);
							uploadDocument.setTableName(checkedOutDocumentRecord.getTableName());
							uploadDocument.setFkFieldId(checkedOutDocumentRecord.getFkFieldId());

							checkedOutDocumentRecord = uploadDocument(uploadDocument);
							out.print("upload file" + " <br />");
						}
					}
					finally {
						if (checkedOutDocumentRecord != null) {
							checkinDocument(checkedOutDocumentRecord);
							out.print("checked in file" + " <br />");
						}
					}
				}
			}
			catch (Exception e) {
				out.print("Could not process file " + documentRecord.getName() + " because of error " + e.getMessage());
			}
		}
	}


	public List<DocumentRecord> getTransformationDocumentRecordList() {
		List<DocumentRecord> documentRecordList = new ArrayList<>();

		try {
			setSystemUser();

			IntegrationTransformationService integrationTransformationService = (IntegrationTransformationService) this.applicationContext.getBean("integrationTransformationService");
			IntegrationTransformationFileSearchForm searchForm = new IntegrationTransformationFileSearchForm();
			searchForm.setLimit(500);
			List<IntegrationTransformationFile> integrationTransformationFileList = integrationTransformationService.getIntegrationTransformationFileList(searchForm);

			DocumentManagementService documentManagementService = (DocumentManagementService) this.applicationContext.getBean("documentManagementService");
			for (IntegrationTransformationFile integrationTransformationFile : integrationTransformationFileList) {
				DocumentRecord documentRecord = null;
				documentRecord = documentManagementService.getDocumentRecord(TRANSFORMATION_FILE_TABLE_NAME, integrationTransformationFile.getId());
				documentRecordList.add(documentRecord);
			}
		}
		finally {
			clearUser();
		}
		return documentRecordList;
	}


	public FileWrapper downloadDocument(String documentId) {
		try {
			setSystemUser();
			DocumentManagementService documentManagementService = (DocumentManagementService) this.applicationContext.getBean("documentManagementService");
			return documentManagementService.downloadDocumentRecord(documentId);
		}
		finally {
			clearUser();
		}
	}


	public DocumentRecord uploadDocument(DocumentRecord document) {
		try {
			setSystemUser();
			DocumentManagementService documentManagementService = (DocumentManagementService) this.applicationContext.getBean("documentManagementService");
			return documentManagementService.uploadDocumentRecord(document);
		}
		finally {
			clearUser();
		}
	}


	public DocumentRecord checkoutDocument(DocumentRecord documentRecord) {
		try {
			setSystemUser();
			DocumentManagementService documentManagementService = (DocumentManagementService) this.applicationContext.getBean("documentManagementService");
			return documentManagementService.checkoutDocumentRecord(documentRecord.getDocumentId(), documentRecord.getTableName(), documentRecord.getFkFieldId());
		}
		finally {
			clearUser();
		}
	}


	public DocumentRecord checkinDocument(DocumentRecord documentRecord) {
		try {
			setSystemUser();
			DocumentManagementService documentManagementService = (DocumentManagementService) this.applicationContext.getBean("documentManagementService");
			documentRecord.setCheckedOutUser(SecurityUser.SYSTEM_USER);
			documentRecord.setCheckinComment("Automatic update of transformation port number.");
			return documentManagementService.checkinDocumentRecord(documentRecord);
		}
		finally {
			clearUser();
		}
	}


	public void outputTransformedDom(Document document, FileWrapper outputFile) throws Exception {
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource source = new DOMSource(document);

		FileContainer fileContainer = FileContainerFactory.getFileContainer(outputFile.getFile());
		try (OutputStream output = fileContainer.getOutputStream()) {
			StreamResult result = new StreamResult(output);
			transformer.transform(source, result);
		}
	}


	public Document getDomDocument(FileWrapper inputFile) throws Exception {
		DocumentBuilder db = dbf.newDocumentBuilder();
		FileContainer fileContainer = FileContainerFactory.getFileContainer(inputFile.getFile());
		try (InputStream inputStream = fileContainer.getInputStream()) {
			return db.parse(inputStream);
		}
	}


	public boolean containsConnectionPort(Document document) throws XPathExpressionException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = (NodeList) xPath.compile("//connection[port = 1433]").evaluate(document, XPathConstants.NODESET);
		return nodeList != null && nodeList.getLength() > 0;
	}


	public boolean containsAttributeConnectionPort(Document document) throws XPathExpressionException {
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = (NodeList) xPath.compile("//connection/attributes/attribute[code='PORT_NUMBER'][attribute=1433]").evaluate(document, XPathConstants.NODESET);
		return nodeList != null && nodeList.getLength() > 0;
	}


	public boolean replaceConnectionPort(Document document) throws XPathExpressionException {
		boolean results = false;
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = (NodeList) xPath.compile("//connection[port = 1433]/port").evaluate(document, XPathConstants.NODESET);
		if (nodeList != null) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (Objects.equals("1433", node.getTextContent())) {
					node.setTextContent("${DB_PORT}");
					results = true;
				}
			}
		}
		return results;
	}


	public boolean replaceConnectionAttribute(Document document) throws XPathExpressionException {
		boolean results = false;
		XPath xPath = XPathFactory.newInstance().newXPath();
		NodeList nodeList = (NodeList) xPath.compile("//connection/attributes/attribute[code='PORT_NUMBER'][attribute=1433]/attribute").evaluate(document, XPathConstants.NODESET);
		if (nodeList != null) {
			for (int i = 0; i < nodeList.getLength(); i++) {
				Node node = nodeList.item(i);
				if (Objects.equals("1433", node.getTextContent())) {
					node.setTextContent("${DB_PORT}");
					results = true;
				}
			}
		}
		return results;
	}


	void setSystemUser() {
		ContextHandler contextHandler = (ContextHandler) this.applicationContext.getBean("contextHandler");
		SecurityUser systemUser = new SecurityUser();
		systemUser.setId((short) 999);
		systemUser.setUserName(SecurityUser.SYSTEM_USER);
		contextHandler.setBean(Context.USER_BEAN_NAME, systemUser);
	}


	void clearUser() {
		ContextHandler contextHandler = (ContextHandler) this.applicationContext.getBean("contextHandler");
		contextHandler.removeBean(Context.USER_BEAN_NAME);
	}
%>
<%
	this.applicationContext = org.springframework.web.context.support.WebApplicationContextUtils.getRequiredWebApplicationContext(getServletContext());

	long time = System.currentTimeMillis();

	out.print("Starting transformation migration <br /><br />");

	transform(out);

	time = System.currentTimeMillis() - time;
	out.print("<br />PROCESSING TIME: " + time + " milliseconds");
%>