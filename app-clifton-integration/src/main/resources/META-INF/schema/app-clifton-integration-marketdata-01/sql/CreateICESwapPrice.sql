USE [CliftonIntegration]


/****** Object:  Table [source].[MarketDataICESwapPrice]    Script Date: 11/03/2010 10:22:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[MarketDataICESwapPrice]') AND type in (N'U'))
DROP TABLE [source].[MarketDataICESwapPrice]


USE [CliftonIntegration]


/****** Object:  Table [source].[MarketDataICESwapPrice]    Script Date: 11/03/2010 10:22:31 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[MarketDataICESwapPrice](
	[SettlementDate] [datetime] NULL,
	[PairClip] [varchar](100) NULL,
	[ReferenceEntityClip] [varchar](100) NULL,
	[ICECode] [varchar](100) NULL,
	[ReferenceEntityName] [varchar](100) NULL,
	[SChedTermDate] [datetime] NULL,
	[DebtLevel] [varchar](100) NULL,
	[FixedSpread] decimal(28,16) NULL,
	[CloseSpread] decimal(28,16) NULL,
	[ClosePrice] decimal(28,16) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[MarketDataICESwapPrice]  WITH CHECK ADD  CONSTRAINT [FK_MarketDataICESwapPrice_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[MarketDataICESwapPrice] CHECK CONSTRAINT [FK_MarketDataICESwapPrice_IntegrationImportRun]

