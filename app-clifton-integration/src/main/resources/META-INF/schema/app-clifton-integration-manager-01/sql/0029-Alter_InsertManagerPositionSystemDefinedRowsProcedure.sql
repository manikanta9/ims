-- exec InsertManagerPositionSystemDefinedValues 10718,'10/10/11'

ALTER PROCEDURE [dbo].[InsertManagerPositionSystemDefinedValues]
	@IntegrationImportRunID INT
AS
BEGIN
	
SET NOCOUNT ON;
	
	DECLARE @IntegrationManagerAssetClassGroupID INT, @RunDate DateTime

	SELECT
		@IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
		,@RunDate = iir.EffectiveDate
	FROM IntegrationImportDefinition iid
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		inner join IntegrationImportRun iir on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		where iir.IntegrationImportRunID = @IntegrationImportRunID

		
    -- Add system rows
    DECLARE @SystemDefinedTypeID INT
		
	SELECT @SystemDefinedTypeID = IntegrationManagerPositionTypeID FROM IntegrationManagerPositionType WHERE PositionTypeName='SYSTEM_DEFINED'
	
	DELETE imph FROM IntegrationManagerPositionHistory imph
		inner join IntegrationImportRun iir on iir.IntegrationImportRunID = imph.IntegrationImportRunID
		inner join IntegrationImportDefinition iid on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID 
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationManagerPositionTypeID = @SystemDefinedTypeID AND imacg.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND iir.EffectiveDate = @RunDate
	
	DECLARE @SystemDefinedIntegrationManagerPositionSecurityID INT
	SET @SystemDefinedIntegrationManagerPositionSecurityID = 
	(
		SELECT IntegrationManagerPositionSecurityID FROM IntegrationManagerPositionSecurity 
		WHERE AssetClass = 'SYSTEM DEFINED SECURITY VALUE - (Negative cash value in the ManagerValue column to balance totals)'
			AND [SourceAssetClass] IS NULL
			AND [SecurityIdentifier] IS NULL
			AND [SecurityIdentifierType] IS NULL
			AND [SecurityDescription] IS NULL
	)
	
	IF @SystemDefinedIntegrationManagerPositionSecurityID IS NULL
	BEGIN
		INSERT INTO IntegrationManagerPositionSecurity(AssetClass)
		VALUES('SYSTEM DEFINED SECURITY VALUE - (Negative cash value in the ManagerValue column to balance totals)')
		
		SET @SystemDefinedIntegrationManagerPositionSecurityID = @@IDENTITY
	END
	
	INSERT INTO IntegrationManagerPositionHistory
	(
		IntegrationManagerPositionBankID,
		ManagerValue,
		IntegrationManagerPositionSecurityID,
		CashValue,
		SecurityValue,
		IntegrationManagerPositionTypeID,
		IntegrationImportRunID
	)
	Select * from 
	(
		SELECT
			IntegrationManagerPositionBankID,
			-(ISNULL(AssetValue,0) + ISNULL(LiabilityValue,0) - ISNULL(LiabilityNegativeValue,0) + ISNULL(SecurityValue,0)) as ManagerValue,
			@SystemDefinedIntegrationManagerPositionSecurityID as IntegrationManagerPositionSecurityID,
			0 as CashValue,
			TotalValue - (ISNULL(AssetValue,0) + ISNULL(LiabilityValue,0) - ISNULL(LiabilityNegativeValue,0)+ ISNULL(SecurityValue,0)) as SystemSecurityValue,
			@SystemDefinedTypeID as IntegrationManagerPositionTypeID,
			@IntegrationImportRunID as IntegrationImportRunID
		FROM IntegrationManagerPosition p 
		WHERE p.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND EffectiveDate = @RunDate
	) t1 where t1.SystemSecurityValue <>0
END
