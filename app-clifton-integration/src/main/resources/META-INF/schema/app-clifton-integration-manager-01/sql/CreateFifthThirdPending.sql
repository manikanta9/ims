USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerFifthThirdPending]    Script Date: 12/08/2010 16:13:21 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerFifthThirdPending]') AND type in (N'U'))
DROP TABLE [source].[ManagerFifthThirdPending]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerFifthThirdPending]    Script Date: 12/08/2010 16:13:21 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerFifthThirdPending](
	[AccountName] [varchar](100) NULL,
	[TransactionTypeDescription] [varchar](100) NULL,
	[DescriptionLine1] [varchar](500) NULL,
	[AccountNumber] [varchar](100) NULL,
	[Cash] float NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerFifthThirdPending]  WITH CHECK ADD  CONSTRAINT [FK_ManagerFifthThirdPending_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerFifthThirdPending] CHECK CONSTRAINT [FK_ManagerFifthThirdPending_IntegrationImportRun]

