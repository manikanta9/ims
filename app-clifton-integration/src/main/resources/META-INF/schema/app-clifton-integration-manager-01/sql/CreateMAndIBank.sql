USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerMAndIHoldings_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerMAndIHoldings]'))
ALTER TABLE [source].[ManagerMAndIHoldings] DROP CONSTRAINT [FK_ManagerMAndIHoldings_IntegrationImportRun]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerMAndIHoldings]') AND type in (N'U'))
DROP TABLE [source].[ManagerMAndIHoldings]

SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerMAndIHoldings](
	[SecurityNumber] [varchar](100) NULL,
	[ClassificationCode] [varchar](100) NULL,
	[UnitsHeld] float NULL,
	[CarryValueTaxCost] float NULL,
	[MarketValue] float NULL,
	[OriginalFaceValue] float NULL,
	[RegistrationCode] [varchar](100) NULL,
	[QualityRatingCode] [varchar](100) NULL,
	[Price] float NULL,
	[PriceDate] [datetime] NULL,
	[Ticker] [varchar](100) NULL,
	[AnnualIncomeAmount] float NULL,
	[AccruedIncome] float NULL,
	[IncomeAssetInd] [varchar](100) NULL,
	[UnrealizedGL] float NULL,
	[YieldToMarket] float NULL,
	[MaturityDate] [datetime] NULL,
	[IncomeFrequency] [varchar](100) NULL,
	[PaymentDate] [datetime] NULL,
	[RecordDate] [datetime] NULL,
	[ExDate] [datetime] NULL,
	[StateCode] [varchar](100) NULL,
	[DescriptionLine1] [varchar](100) NULL,
	[DescriptionLine2] [varchar](100) NULL,
	[DescriptionLine3] [varchar](100) NULL,
	[DescriptionLine4] [varchar](100) NULL,
	[DescriptionLine5] [varchar](100) NULL,
	[AccountNumber] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerMAndIHoldings]  WITH CHECK ADD  CONSTRAINT [FK_ManagerMAndIHoldings_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerMAndIHoldings] CHECK CONSTRAINT [FK_ManagerMAndIHoldings_IntegrationImportRun]



