USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerAmalgamated]') AND type in (N'U'))
DROP TABLE [source].[ManagerAmalgamated]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerAmalgamated]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON



CREATE TABLE [source].[ManagerAmalgamated](
	[OwningPortfolio] [varchar](50) NULL,
	[CUSIP] [varchar](50) NULL,
	[Description] [varchar](150) NULL,
	[MaturityDate] [datetime] NULL,
	[Units] float NULL,
	[Cost] float NULL,
	[MarketValue] float NULL,
	[Price] float NULL,
	[PriceDate] [datetime] NULL,
	[AccruedIncome] float NULL,
	[UnitCost] float NULL,
	[UnrealizedGLCapital] float NULL,
	[MoodysRating] [varchar](50) NULL,
	[SandPRating] [varchar](50) NULL,
	[AssetClass] [varchar](50) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerAmalgamated]  WITH CHECK ADD  CONSTRAINT [FK_ManagerAmalgamated_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerAmalgamated] CHECK CONSTRAINT [FK_ManagerAmalgamated_IntegrationImportRun]


