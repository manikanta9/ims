USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerWellsCashBalance]    Script Date: 11/03/2010 10:28:15 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerWellsCashBalance]') AND type in (N'U'))
DROP TABLE [source].[ManagerWellsCashBalance]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerWellsCashBalance]    Script Date: 11/03/2010 10:28:15 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerWellsCashBalance](
	[AccountNumber] [bigint] NULL,
	[AccountShortName] [varchar](36) NULL,
	[AssetId] [varchar](9) NULL,
	[DescriptionLine1] [varchar](35) NULL,
	[DescriptionLine2] [varchar](31) NULL,
	[MarketValue] [varchar](11) NULL,
	[IncomeCash] [varchar](11) NULL,
	[PrincipalCash] [varchar](12) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerWellsCashBalance]  WITH CHECK ADD  CONSTRAINT [FK_ManagerWellsCashBalance_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerWellsCashBalance] CHECK CONSTRAINT [FK_ManagerWellsCashBalance_IntegrationImportRun]



