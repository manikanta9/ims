-- exec UpdateManagerPositionType 2,'12/6/10'

ALTER PROCEDURE UpdateManagerPositionValues
	@IntegrationImportRunID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @IntegrationImportDefinitionID INT
	SELECT
		@IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	FROM IntegrationImportRun iir
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	WHERE IntegrationImportRunID = @IntegrationImportRunID
    
    -- Update the Cash and Security value columns
    UPDATE mph SET
		CashValue =
			CASE WHEN mpt.IsIgnore = 0 AND mpt.IsPending = 0 AND mpt.IsCash = 1 THEN
				CASE WHEN mpt.IsNegative = 1 THEN -1 * ManagerValue ELSE ManagerValue END
			WHEN mpt.IsIgnore = 0 AND mpt.IsPending = 1 AND mpt.IsCash = 1 THEN
				CASE WHEN mpt.IsNegative = 1 THEN -1 * ManagerValue ELSE ManagerValue END
			WHEN mpt.IsIgnore = 0 AND mpt.IsPending = 1 AND mpt.IsCash = 0 THEN
				CASE WHEN mpt.IsNegative = 1 THEN ManagerValue ELSE -1 * ManagerValue END
			ELSE 0 END,
		SecurityValue =
			CASE WHEN mpt.IsIgnore = 0 AND mpt.IsPending = 0 AND mpt.IsSecurity = 1 THEN
				CASE WHEN mpt.IsNegative = 1 THEN -1 * ManagerValue ELSE ManagerValue END
			WHEN mpt.IsIgnore = 0 AND mpt.IsPending = 1 AND mpt.IsSecurity = 1 THEN
				CASE WHEN mpt.IsNegative = 1 THEN -1 * ManagerValue ELSE ManagerValue END
			WHEN mpt.IsIgnore = 0 AND mpt.IsPending = 1 AND mpt.IsSecurity = 0 THEN
				CASE WHEN mpt.IsNegative = 1 THEN ManagerValue ELSE -1 * ManagerValue END
			ELSE 0 END,
		ManagerValue =
			CASE WHEN mpt.IsIgnore = 0 AND mpt.IsTotal = 1 AND mpt.IsNegative = 1 THEN -1 * ManagerValue ELSE ManagerValue END
	FROM IntegrationManagerPositionHistory mph
		INNER JOIN IntegrationManagerPositionType mpt ON mpt.IntegrationManagerPositionTypeID = mph.IntegrationManagerPositionTypeID
	WHERE mph.IntegrationImportRunID = @IntegrationImportRunID

END


