USE [CliftonIntegration]
GO
/****** Object:  StoredProcedure [dbo].[InsertManagerPositionSystemDefinedValues]    Script Date: 04/11/2011 12:08:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec InsertManagerPositionSystemDefinedValues 10718,'10/10/11'

ALTER PROCEDURE [dbo].[InsertManagerPositionSystemDefinedValues]
	@IntegrationImportRunID INT,
	@PositionDate DATETIME
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @IntegrationManagerAssetClassGroupID INT, @RunDate DateTime

	SELECT
		@IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
		,@RunDate = iir.EffectiveDate
	FROM IntegrationImportDefinition iid
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		inner join IntegrationImportRun iir on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		where iir.IntegrationImportRunID = @IntegrationImportRunID

		
    -- Add system rows
    DECLARE @SystemDefinedTypeID INT
		
	SELECT @SystemDefinedTypeID = IntegrationManagerPositionTypeID FROM IntegrationManagerPositionType WHERE PositionTypeName='SYSTEM_DEFINED'
	
	DELETE imph FROM IntegrationManagerPositionHistory imph
		inner join IntegrationImportRun iir on iir.IntegrationImportRunID = imph.IntegrationImportRunID
		inner join IntegrationImportDefinition iid on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID 
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationManagerPositionTypeID = @SystemDefinedTypeID AND imacg.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND PositionDate = @RunDate
	 
	
	INSERT INTO IntegrationManagerPositionHistory
	(
		ManagerBankCode,
		ManagerValue,
		AssetClass,
		PositionDate,
		CashValue,
		SecurityValue,
		IntegrationManagerPositionTypeID,
		IntegrationImportRunID
	)
	Select * from 
	(
	SELECT
		ManagerBankCode,
		-(ISNULL(AssetValue,0) + ISNULL(LiabilityValue,0) - ISNULL(LiabilityNegativeValue,0) + ISNULL(SecurityValue,0)) as ManagerValue,
		'SYSTEM DEFINED SECURITY VALUE - (Negative cash value in the ManagerValue column to balance totals)' as AssetClass,
		PositionDate,
		0 as CashValue,
		TotalValue - (ISNULL(AssetValue,0) + ISNULL(LiabilityValue,0) - ISNULL(LiabilityNegativeValue,0)+ ISNULL(SecurityValue,0)) as SystemSecurityValue,
		@SystemDefinedTypeID as IntegrationManagerPositionTypeID,
		@IntegrationImportRunID as IntegrationImportRunID
	FROM IntegrationManagerPosition p 
	WHERE p.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND PositionDate = @RunDate
	) t1 where t1.SystemSecurityValue <>0
END
