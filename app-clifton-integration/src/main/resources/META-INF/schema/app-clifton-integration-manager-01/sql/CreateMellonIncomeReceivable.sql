USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerMellonIncomeReceivable]') AND type in (N'U'))
DROP TABLE [source].[ManagerMellonIncomeReceivable]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerMellonIncomeReceivable](
	[Shares] float NULL,
	[Receivable] float NULL,
	[SheetName] [varchar](250) NULL,
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[SourceAccountNumber] [varchar](100) NULL,
	[SourceAccountName] [varchar](100) NULL,
	[AccountBaseCurrency] [varchar](100) NULL,
	[AsofDate] [datetime] NULL,
	[DividendInterestType] [varchar](100) NULL,
	[DividendRate] float NULL,
	[AssetCode] [varchar](100) NULL,
	[AssetCategoryName] [varchar](100) NULL,
	[SecurityID] [varchar](100) NULL,
	[SecurityCrossReferenceType] [varchar](100) NULL,
	[SecurityCrossReference] [varchar](100) NULL,
	[SecurityDescription1] [varchar](100) NULL,
	[SecurityDescription2] [varchar](100) NULL,
	[LocalCurrencyName] [varchar](100) NULL,
	[ExchangeRate] float NULL,
	[ExDate] [datetime] NULL,
	[MaturityDate] [varchar](100) NULL,
	[PayableDate] [datetime] NULL,
	[SettlementDate] [varchar](100) NULL,
	[LocalNetAmount] float NULL,
	[LocalTaxPayable] float NULL,
	[LocalReclaimAmount] float NULL,
	[BaseNetAmount] float NULL,
	[BaseTaxPayable] float NULL,
	[BaseReclaimAmount] float NULL,
	[NetGainLoss] float NULL,
	[TaxGainLoss] float NULL,
	[ReclaimGainLoss] float NULL,
	[TotalGainLoss] float NULL,
	[TotalBaseAmount] float NULL,
	[TotalLocalAmount] float NULL,
	[NetPercent] float NULL,
	[TaxPercent] float NULL,
	[ReclaimPercent] float NULL,
	[NetPartialIndicator] [varchar](100) NULL,
	[TaxPartialIndicator] [varchar](100) NULL,
	[ReclaimPartialIndicator] [varchar](100) NULL,
	[PendingFlag] [varchar](100) NULL,
	[LocalBaseBothCurrencyType] [varchar](100) NULL,
	[ReportID] [varchar](100) NULL,
	[ReportingAccountStatus] [varchar](100) NULL,
	[SourceAccountStatus] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerMellonIncomeReceivable]  WITH CHECK ADD  CONSTRAINT [FK_ManagerMellonIncomeReceivable_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerMellonIncomeReceivable] CHECK CONSTRAINT [FK_ManagerMellonIncomeReceivable_IntegrationImportRun]


