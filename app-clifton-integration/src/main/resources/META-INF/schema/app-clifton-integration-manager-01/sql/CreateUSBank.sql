USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerUSBank_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerUSBank]'))
ALTER TABLE [source].[ManagerUSBank] DROP CONSTRAINT [FK_ManagerUSBank_IntegrationImportRun]


/****** Object:  Table [source].[ManagerUSBank]    Script Date: 11/10/2010 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerUSBank]') AND type in (N'U'))
DROP TABLE [source].[ManagerUSBank]


/****** Object:  Table [source].[ManagerUSBank]    Script Date: 11/10/2010 16:24:27 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerUSBank](
	[AccountNumber] [varchar](50) NULL,
	[MarketValue] [decimal](18, 2) NULL,
	[SecurityName] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerUSBank]  WITH CHECK ADD  CONSTRAINT [FK_ManagerUSBank_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerUSBank] CHECK CONSTRAINT [FK_ManagerUSBank_IntegrationImportRun]



