-- exec MoveManagerPositionsToFinalTable 48115 WITH RECOMPILE
-- exec MoveManagerPositionsToFinalTable 48125 WITH RECOMPILE

ALTER PROCEDURE MoveManagerPositionsToFinalTable
	@IntegrationImportRunID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	INSERT INTO IntegrationManagerPositionSecurity(AssetClass,SourceAssetClass,SecurityIdentifier,SecurityIdentifierType,SecurityDescription)
	SELECT ph.[AssetClass],ph.[SourceAssetClass],ph.[SecurityID],ph.[SecurityIDType],ph.[SecurityDescription]
	FROM IntegrationManagerPositionHistory_TEMP ph
		LEFT JOIN IntegrationManagerPositionSecurity s ON ISNULL(s.AssetClass,1) = ISNULL(ph.AssetClass,1)
			AND ISNULL(s.SourceAssetClass,1) = ISNULL(ph.SourceAssetClass,1)
			AND ISNULL(s.SecurityIdentifier,1) = ISNULL(ph.SecurityID,1)
			AND ISNULL(s.SecurityIdentifierType,1) = ISNULL(ph.SecurityIDType,1)
			AND ISNULL(s.SecurityDescription,1) = ISNULL(ph.SecurityDescription,1)
	WHERE s.IntegrationManagerPositionSecurityID IS NULL AND ph.IntegrationImportRunID = @IntegrationImportRunID
	GROUP BY ph.[AssetClass],ph.[SourceAssetClass],ph.[SecurityID],ph.[SecurityIDType],ph.[SecurityDescription]

	INSERT INTO IntegrationManagerPositionBank(ManagerBankCode)
	SELECT ph.ManagerBankCode
	FROM IntegrationManagerPositionHistory_TEMP ph
		LEFT JOIN IntegrationManagerPositionBank b ON b.ManagerBankCode = ph.ManagerBankCode
	WHERE b.IntegrationManagerPositionBankID IS NULL AND ph.IntegrationImportRunID = @IntegrationImportRunID
	GROUP BY ph.ManagerBankCode

	INSERT INTO IntegrationManagerPositionHistory
	(
		IntegrationManagerPositionBankID,
		ManagerValue,
		IntegrationManagerPositionSecurityID,
		CashValue,
		SecurityValue,
		IntegrationImportRunID
	)
	SELECT
		b.IntegrationManagerPositionBankID,ph.ManagerValue,s.IntegrationManagerPositionSecurityID,ph.CashValue,ph.SecurityValue,ph.IntegrationImportRunID
	FROM IntegrationManagerPositionHistory_TEMP ph
		INNER JOIN IntegrationManagerPositionSecurity s ON ISNULL(s.AssetClass,1) = ISNULL(ph.AssetClass,1)
			AND ISNULL(s.SourceAssetClass,1) = ISNULL(ph.SourceAssetClass,1)
			AND ISNULL(s.SecurityIdentifier,1) = ISNULL(ph.SecurityID,1)
			AND ISNULL(s.SecurityIdentifierType,1) = ISNULL(ph.SecurityIDType,1)
			AND ISNULL(s.SecurityDescription,1) = ISNULL(ph.SecurityDescription,1)
		INNER JOIN IntegrationManagerPositionBank b ON b.ManagerBankCode = ph.ManagerBankCode
	WHERE ph.IntegrationImportRunID = @IntegrationImportRunID
			
	DELETE FROM IntegrationManagerPositionHistory_TEMP WHERE IntegrationImportRunID = @IntegrationImportRunID
END


