USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAccountingCurrency]    Script Date: 11/03/2010 10:21:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerBNYMellonPRIM]') AND type in (N'U'))
DROP TABLE [source].[ManagerBNYMellonPRIM]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBNYMellonPRIM]    Script Date: 06/15/2011 10:02:12 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerBNYMellonPRIM](
	[IntegrationImportRunID] [int] NOT NULL,
	ReportingAccountNumber [varchar](100) NULL,
	ReportingAccountName [varchar](100) NULL,
	AsofDate [datetime] NULL,
	SecurityID [varchar](100) NULL,
	SecurityCrossReferenceType [varchar](100) NULL,
	SecurityCrossReference [varchar](100) NULL,
	Description1 [varchar](100) NULL,
	Description2 [varchar](100) NULL,
	AssetTypeCode [varchar](100) NULL,
	AssetCategory [varchar](100) NULL,
	SectorName [varchar](100) NULL,
	BaseCurrencyCode [varchar](100) NULL,
	AlternateBaseCurrency [varchar](100) NULL,
	PositionType [varchar](100) NULL,
	AlternateBaseCurrencyExchangeRate [float] NULL,
	ExchangeRate [float] NULL,
	LocalCurrencyCode [varchar](100) NULL,
	LocalCurrency [varchar](100) NULL,
	CountryCode [varchar](100) NULL,
	CountryName [varchar](100) NULL,
	SharesPar [float] NULL,
	BaseCost [float] NULL,
	LocalCost [float] NULL,
	BasePrice [float] NULL,
	LocalPrice [float] NULL,
	BaseMarketValue [float] NULL,
	LocalMarketValue [float] NULL,
	BaseEstimatedAnnualIncome [float] NULL,
	CouponRate [varchar](100) NULL,
	MaturityDate [varchar](100) NULL,
	BaseUnrealizedInvGainLoss [float] NULL,
	LocalUnrealizedInvGainLoss [float] NULL,
	BaseUnrealizedCurrencyGainLoss [float] NULL,
	AccountStatus [varchar](100) NULL,
	ReportID [varchar](100) NULL,
	LocalBase [varchar](100) NULL,
	GLAccount [varchar](100) NULL,
	SORT [varchar](100) NULL,
	SORT1 [varchar](100) NULL,
	SORT2 [varchar](100) NULL,
	SORT3 [varchar](100) NULL,
	SORT4 [varchar](100) NULL,
	AcctgStatusUpdateDate [datetime] NULL,
	AcctgStatusUpdateTimeEDT [varchar](100) NULL,
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]


ALTER TABLE [source].[ManagerBNYMellonPRIM]  WITH CHECK ADD  CONSTRAINT [FK_ManagerBNYMellonPRIM_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerBNYMellonPRIM] CHECK CONSTRAINT [FK_ManagerBNYMellonPRIM_IntegrationImportRun]




