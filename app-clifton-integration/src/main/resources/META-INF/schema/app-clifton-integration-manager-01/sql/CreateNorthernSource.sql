USE [CliftonIntegration]


IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerNorthernTrust_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerNorthernTrust]'))
ALTER TABLE [source].[ManagerNorthernTrust] DROP CONSTRAINT [FK_ManagerNorthernTrust_IntegrationImportRun]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerNorthernTrust]    Script Date: 12/02/2010 15:28:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerNorthernTrust]') AND type in (N'U'))
DROP TABLE [source].[ManagerNorthernTrust]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerNorthernTrust]    Script Date: 12/02/2010 15:28:59 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerNorthernTrust](
	[AccountNumber] [varchar](100) NULL,
	[AssetSuperCategory] [varchar](100) NULL,
	[AssetSubCategory] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL,
	[AccountName] [varchar](31) NULL,
	[MarketValue] float NULL,
	[MarketPrice] float NULL,
	[AccruedIncExp] float NULL,
	[AssetDescription] [varchar](100) NULL,
	[CUSIP] [varchar](50) NULL,
	[Symbol] [varchar](50) NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerNorthernTrust]  WITH CHECK ADD  CONSTRAINT [FK_ManagerNorthernTrust_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerNorthernTrust] CHECK CONSTRAINT [FK_ManagerNorthernTrust_IntegrationImportRun]


