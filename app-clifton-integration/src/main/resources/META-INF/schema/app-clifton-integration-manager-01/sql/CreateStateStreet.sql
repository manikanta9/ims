USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerStateStreetNAV]    Script Date: 12/01/2010 14:36:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerStateStreetNAV]') AND type in (N'U'))
DROP TABLE [source].[ManagerStateStreetNAV]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerStateStreetNAV]    Script Date: 12/01/2010 14:36:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerStateStreetNAV](
[IntegrationImportRunID] [int] NOT NULL,
	[Fund] [varchar](4) NULL,
	[TotalNetAssetValue] float NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerStateStreetTBL]    Script Date: 12/01/2010 14:37:05 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerStateStreetTBL]') AND type in (N'U'))
DROP TABLE [source].[ManagerStateStreetTBL]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerStateStreetTBL]    Script Date: 12/01/2010 14:37:05 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerStateStreetTBL](
	[IntegrationImportRunID] [int] NOT NULL,
	[Fund] [varchar](4) NULL,
	[LineDescription] [varchar](100) NULL,
	[Balance] float NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerStateStreetNAV]  WITH CHECK ADD  CONSTRAINT [FK_ManagerStateStreetNAV_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerStateStreetNAV] CHECK CONSTRAINT [FK_ManagerStateStreetNAV_IntegrationImportRun]

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerStateStreetTBL]  WITH CHECK ADD  CONSTRAINT [FK_ManagerStateStreetTBL_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerStateStreetTBL] CHECK CONSTRAINT [FK_ManagerStateStreetTBL_IntegrationImportRun]


