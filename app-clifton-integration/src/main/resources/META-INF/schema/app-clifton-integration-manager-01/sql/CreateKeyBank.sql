USE [CliftonIntegration]
GO

/****** Object:  Table [source].[ManagerKeyBank]    Script Date: 01/14/2011 16:36:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerKeyBank]') AND type in (N'U'))
DROP TABLE [source].[ManagerKeyBank]
GO

USE [CliftonIntegration]
GO

/****** Object:  Table [source].[ManagerKeyBank]    Script Date: 01/14/2011 16:36:53 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerKeyBank](
	[IntegrationImportRunID] [int] NULL,
	[AccountNumber] [varchar](100) NULL,
	[MarketValue] float NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



ALTER TABLE [source].[ManagerKeyBank]  WITH CHECK ADD  CONSTRAINT [FK_ManagerKeyBank_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerKeyBank] CHECK CONSTRAINT [FK_ManagerKeyBank_IntegrationImportRun]



