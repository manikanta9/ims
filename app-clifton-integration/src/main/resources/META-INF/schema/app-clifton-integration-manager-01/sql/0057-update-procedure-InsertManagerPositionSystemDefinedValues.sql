USE [CliftonIntegration]
GO
/****** Object:  StoredProcedure [dbo].[InsertManagerPositionSystemDefinedValues]    Script Date: 3/3/2016 2:39:39 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- exec InsertManagerPositionSystemDefinedValues 324529,'10/10/11'

ALTER PROCEDURE [dbo].[InsertManagerPositionSystemDefinedValues]
	@IntegrationImportRunID INT
AS
BEGIN

SET NOCOUNT ON;

	DECLARE @IntegrationManagerAssetClassGroupID INT, @RunDate DateTime

	SELECT
		@IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
		,@RunDate = iir.EffectiveDate
	FROM IntegrationImportDefinition iid
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		inner join IntegrationImportRun iir on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		where iir.IntegrationImportRunID = @IntegrationImportRunID

	DELETE imph FROM IntegrationManagerPositionHistory imph
		INNER JOIN IntegrationManagerPositionType t ON t.IntegrationManagerPositionTypeID = imph.IntegrationManagerPositionTypeID
		INNER JOIN IntegrationImportRun iir on iir.IntegrationImportRunID = imph.IntegrationImportRunID
		INNER JOIN IntegrationImportDefinition iid on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE t.IsSystemGenerated = 1 AND imacg.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND iir.EffectiveDate = @RunDate


	-- Process ASSET adjustment entries
	DECLARE @SystemDefinedIntegrationManagerCashSecurityID INT
	SET @SystemDefinedIntegrationManagerCashSecurityID =
	(
		SELECT IntegrationManagerSecurityID FROM IntegrationManagerSecurity
		WHERE AssetClass = 'SYSTEM DEFINED CASH VALUE'
			AND [SourceAssetClass] IS NULL
			AND [SecurityIdentifier] IS NULL
			AND [SecurityIdentifierType] IS NULL
			AND [SecurityDescription] IS NULL
	)

	IF @SystemDefinedIntegrationManagerCashSecurityID IS NULL
	BEGIN
		INSERT INTO IntegrationManagerSecurity(AssetClass)
		VALUES('SYSTEM DEFINED CASH VALUE')

		SET @SystemDefinedIntegrationManagerCashSecurityID = @@IDENTITY
	END


	INSERT INTO IntegrationManagerPositionHistory
	(
		IntegrationManagerBankID,
		ManagerValue,
		IntegrationManagerSecurityID,
		CashValue,
		SecurityValue,
		IntegrationManagerPositionTypeID,
		IntegrationImportRunID
	)
	SELECT IntegrationManagerBankID,
		p.ManagerValue as ManagerValue,  -- TOTAL_ASSET_ADJUSTMENT value has already been negated by a previous step
		@SystemDefinedIntegrationManagerCashSecurityID,
		p.ManagerValue as CashValue,
		0 as SystemSecurityValue,
		(SELECT IntegrationManagerPositionTypeID FROM IntegrationManagerPositionType WHERE PositionTypeName = 'SYSTEM_DEFINED_ASSET') as IntegrationManagerPositionTypeID,
		@IntegrationImportRunID as IntegrationImportRunID
	FROM IntegrationManagerPositionHistory p
		INNER JOIN IntegrationImportRun r ON r.IntegrationImportRunID = p.IntegrationImportRunID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition dmkg ON dmkg.IntegrationImportDefinitionID = r.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup mkg ON mkg.IntegrationManagerAssetClassGroupID = dmkg.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerPositionType t ON t.IntegrationManagerPositionTypeID = p.IntegrationManagerPositionTypeID
	WHERE mkg.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND EffectiveDate = @RunDate AND PositionTypeName = 'TOTAL_ASSET_ADJUSTMENT'


 -- Add system rows
    DECLARE @SystemDefinedTypeID INT

	SELECT @SystemDefinedTypeID = IntegrationManagerPositionTypeID FROM IntegrationManagerPositionType WHERE PositionTypeName='SYSTEM_DEFINED'

	DECLARE @SystemDefinedIntegrationManagerSecurityID INT
	SET @SystemDefinedIntegrationManagerSecurityID =
	(
		SELECT IntegrationManagerSecurityID FROM IntegrationManagerSecurity
		WHERE AssetClass = 'SYSTEM DEFINED SECURITY VALUE - (Negative cash value in the ManagerValue column to balance totals)'
			AND [SourceAssetClass] IS NULL
			AND [SecurityIdentifier] IS NULL
			AND [SecurityIdentifierType] IS NULL
			AND [SecurityDescription] IS NULL
	)

	IF @SystemDefinedIntegrationManagerSecurityID IS NULL
	BEGIN
		INSERT INTO IntegrationManagerSecurity(AssetClass)
		VALUES('SYSTEM DEFINED SECURITY VALUE - (Negative cash value in the ManagerValue column to balance totals)')

		SET @SystemDefinedIntegrationManagerSecurityID = @@IDENTITY
	END

	INSERT INTO IntegrationManagerPositionHistory
	(
		IntegrationManagerBankID,
		ManagerValue,
		IntegrationManagerSecurityID,
		CashValue,
		SecurityValue,
		IntegrationManagerPositionTypeID,
		IntegrationImportRunID
	)
	Select * from
	(
		SELECT
			IntegrationManagerBankID,
			-(ISNULL(AssetValue,0) + ISNULL(LiabilityValue,0) - ISNULL(LiabilityNegativeValue,0) + ISNULL(SecurityValue,0)) as ManagerValue,
			@SystemDefinedIntegrationManagerSecurityID as IntegrationManagerSecurityID,
			0 as CashValue,
			TotalValue - (ISNULL(AssetValue,0) + ISNULL(LiabilityValue,0) - ISNULL(LiabilityNegativeValue,0)+ ISNULL(SecurityValue,0)) as SystemSecurityValue,
			@SystemDefinedTypeID as IntegrationManagerPositionTypeID,
			@IntegrationImportRunID as IntegrationImportRunID
		FROM IntegrationManagerPosition p
		WHERE p.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND EffectiveDate = @RunDate AND TotalValue <> 0
	) t1 where t1.SystemSecurityValue <>0
END
