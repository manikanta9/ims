
ALTER PROCEDURE [dbo].[UpdateManagerPositionType]
	@IntegrationImportRunID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	
	-- Update position types
	DECLARE @IntegrationImportDefinitionID INT, @UseUnmappedType BIT, @UnmappedTypeID INT
	SELECT
		@IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID,
		@UseUnmappedType = CASE WHEN UnmappedPositionTypeID IS NULL THEN 0 ELSE 1 END,
		@UnmappedTypeID = UnmappedPositionTypeID
	FROM IntegrationImportRun iir
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationImportRunID = @IntegrationImportRunID
	
	
	--UPDATE ALL WITH AN ASSET CLASS, MANAGER BANK CODE, AND SECURITY DESCRIPTION PATTERN MAPPED 
	UPDATE imph SET 
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph 
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
		INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE '%' + imac.SecurityDescriptionPattern + '%'
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID 
	
	
	
	--UPDATE ALL WITH AN ASSET CLASS AND MANAGER BANK CODE MAPPED
	UPDATE imph SET 
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph 
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
		INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			--FILTER FOR RELEVANT ROWS FROM THE IntegrationManagerAssetClass TABLE
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode = impb.ManagerBankCode AND imac.SecurityDescriptionPattern IS NULL
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID 
		--EXCLUDE ROWS FROM THE FILE THAT HAVE ALREADY BEEN MAPPED
		AND NOT EXISTS(
			SELECT TOP 1 * 
			FROM IntegrationManagerAssetClass c 
			WHERE c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
				AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE '%' + c.SecurityDescriptionPattern + '%'
		)
	
	
	
	--UPDATE ALL WITH AN ASSET CLASS AND SECURITY DESCRIPTION PATTERN MAPPED 
	UPDATE imph SET 
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph 
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
		INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			--FILTER FOR RELEVANT ROWS FROM THE IntegrationManagerAssetClass TABLE
			AND imac.AssetClass = imps.AssetClass AND imps.SecurityDescription LIKE '%' + imac.SecurityDescriptionPattern + '%' AND imac.ManagerBankCode IS NULL
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID 
		--EXCLUDE ROWS FROM THE FILE THAT HAVE ALREADY BEEN MAPPED
		AND NOT EXISTS(
			SELECT TOP 1 * 
			FROM IntegrationManagerAssetClass c 
			WHERE (c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
				AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE '%' + c.SecurityDescriptionPattern + '%')
			OR
				(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
				AND c.ManagerBankCode = impb.ManagerBankCode AND c.SecurityDescriptionPattern IS NULL)
		)
	 
	

	--UPDATE ALL WITH AN ASSET CLASS MAPPED
	UPDATE imph SET 
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph 
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
		INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			--FILTER FOR RELEVANT ROWS FROM THE IntegrationManagerAssetClass TABLE
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode IS NULL AND imac.SecurityDescriptionPattern IS NULL
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID 
		--EXCLUDE ROWS FROM THE FILE THAT HAVE ALREADY BEEN MAPPED
		AND NOT EXISTS(
			SELECT TOP 1 * 
			FROM IntegrationManagerAssetClass c 
			WHERE (c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
				AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE '%' + c.SecurityDescriptionPattern + '%')
			OR
				(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
				AND c.ManagerBankCode = impb.ManagerBankCode AND c.SecurityDescriptionPattern IS NULL)
			OR
				(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
				AND imps.SecurityDescription LIKE '%' + c.SecurityDescriptionPattern + '%' AND c.ManagerBankCode IS NULL)
		)
	
	
	IF @UseUnmappedType = 1
	BEGIN
	
		--IF UseUnmappedType, UPDATE ALL WITH NO ASSET CLASS MAPPED
		UPDATE imph SET 
			IntegrationManagerPositionTypeID = @UnmappedTypeID
		FROM IntegrationManagerPositionHistory imph 
			INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
			INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
			INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
			INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
			LEFT JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
				AND imac.AssetClass = imps.AssetClass
				AND EXISTS(
					SELECT TOP 1 * 
					FROM IntegrationManagerAssetClass c
					WHERE (c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
						AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE '%' + c.SecurityDescriptionPattern + '%')
					OR
						(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
						AND c.ManagerBankCode = impb.ManagerBankCode AND c.SecurityDescriptionPattern IS NULL)
					OR
						(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
						AND imps.SecurityDescription LIKE '%' + c.SecurityDescriptionPattern + '%' AND c.ManagerBankCode IS NULL)
					OR
						(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass 
						 AND imac.ManagerBankCode IS NULL AND imac.SecurityDescriptionPattern IS NULL)
				) 
		WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND imac.AssetClass IS NULL
		
	END
	
END

