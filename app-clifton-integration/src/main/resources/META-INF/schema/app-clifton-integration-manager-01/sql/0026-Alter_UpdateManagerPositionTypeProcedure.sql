-- exec UpdateManagerPositionType 4,'11/29/10'

ALTER PROCEDURE UpdateManagerPositionType
	@IntegrationImportRunID INT,
	@PositionDate DATE
AS
BEGIN
	
	SET NOCOUNT ON;
	
	-- Update position dates
	UPDATE imph SET 
		PositionDate = @PositionDate
	FROM IntegrationManagerPositionHistory imph
	WHERE imph.IntegrationImportRunID = @IntegrationImportRunID
	
	
	-- Update position types
	DECLARE @IntegrationImportDefinitionID INT, @UseUnmappedType BIT, @UnmappedTypeID INT
	SELECT
		@IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID,
		@UseUnmappedType = CASE WHEN UnmappedPositionTypeID IS NULL THEN 0 ELSE 1 END,
		@UnmappedTypeID = UnmappedPositionTypeID
	FROM IntegrationImportRun iir
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationImportRunID = @IntegrationImportRunID
	
	IF @UseUnmappedType = 0
	BEGIN
		UPDATE imph SET 
			IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
		FROM IntegrationManagerPositionHistory imph
			INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
			INNER JOIN IntegrationImportRun iir ON iir.IntegrationImportRunID = imph.IntegrationImportRunID
			INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
			INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
				AND imac.AssetClass = imps.AssetClass
		WHERE imph.IntegrationImportRunID = @IntegrationImportRunID
	END
	ELSE
	BEGIN
		UPDATE imph SET 
			IntegrationManagerPositionTypeID = ISNULL(imac.IntegrationManagerPositionTypeID, @UnmappedTypeID)
		FROM IntegrationManagerPositionHistory imph
			INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
			INNER JOIN IntegrationImportRun iir ON iir.IntegrationImportRunID = imph.IntegrationImportRunID
			INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
			LEFT JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
				AND imac.AssetClass = imps.AssetClass
		WHERE imph.IntegrationImportRunID = @IntegrationImportRunID
	END
	
END


