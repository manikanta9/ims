USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerUnionBank]') AND type in (N'U'))
DROP TABLE [source].[ManagerUnionBank]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerUnionBank]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON



CREATE TABLE [source].[ManagerUnionBank](
	[OwningAccountNumber] [varchar](100) NULL,
	[OwningAccountName] [varchar](100) NULL,
	[AssetType] [varchar](100) NULL,
	[AssetName] [varchar](100) NULL,
	[Pending] [varchar](100) NULL,
	[CUSIP] [varchar](100) NULL,
	[ISIN] [varchar](100) NULL,
	[SharesUnits] float NULL,
	[Price] float NULL,
	[Currency_Price] [varchar](100) NULL,
	[DatePriced] [datetime] NULL,
	[CostBasis] float NULL,
	[Currency_CostBasis] [varchar](100) NULL,
	[MarketValue] float NULL,
	[Currency_MarketValue] [varchar](100) NULL,
	[NetUnrealizedGainLoss] float NULL,
	[Currency_NetUnrealizedGainLoss] [varchar](100) NULL,
	[Portfolio] [varchar](100) NULL,
	[MaturityDate] [datetime] NULL,
	[Location] [varchar](100) NULL,
	[Sedol] [varchar](100) NULL,
	[PriceUnrealizedGainLoss] float NULL,
	[Currency_PriceUnrealizedGainLoss] [varchar](100) NULL,
	[LocalCurrencyPrice] float NULL,
	[Currency_LocalCurrencyPrice] [varchar](100) NULL,
	[AnnualYield] float NULL,
	[LocalCurrencyMarketValue] float NULL,
	[Currency_LocalCurrencyMarketValue] [varchar](100) NULL,
	[ExchangeRate] float NULL,
	[Currency_ExchangeRate] [varchar](100) NULL,
	[PercentofTotalMarketValue] float NULL,
	[Ticker] [varchar](100) NULL,
	[StateofIssue] [varchar](100) NULL,
	[IssuersCountry] [varchar](100) NULL,
	[EstimatedAnnualIncome] float NULL,
	[Currency_EstimatedAnnualIncome] [varchar](100) NULL,
	[ExchangeRateDate] [datetime] NULL,
	[Industry] [varchar](100) NULL,
	[CountryofIssue] [varchar](100) NULL,
	[OriginalFaceValue] float NULL,
	[LocalCurrencyCostBasis] float NULL,
	[Currency_LocalCurrencyCostBasis] [varchar](100) NULL,
	[CurrencyUnrealizedGainLoss] float NULL,
	[Currency_CurrencyUnrealizedGainLoss] [varchar](100) NULL,
	[MoodysRating] [varchar](100) NULL,
	[SandPRating] [varchar](100) NULL,
	[PoolNumber] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerUnionBank]  WITH CHECK ADD  CONSTRAINT [FK_ManagerUnionBank_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerUnionBank] CHECK CONSTRAINT [FK_ManagerUnionBank_IntegrationImportRun]


