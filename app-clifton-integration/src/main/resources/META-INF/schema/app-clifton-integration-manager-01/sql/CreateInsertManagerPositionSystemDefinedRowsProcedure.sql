-- exec UpdateManagerPositionType 4,'11/29/10'

CREATE PROCEDURE InsertManagerPositionSystemDefinedValues
	@IntegrationImportRunID INT,
	@PositionDate DATETIME
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @IntegrationManagerAssetClassGroupID INT, @RunDate DateTime

	SELECT
		@IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
		,@RunDate = iir.EffectiveDate
	FROM IntegrationImportDefinition iid
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		inner join IntegrationImportRun iir on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		where iir.IntegrationImportRunID = @IntegrationImportRunID

		
    -- Add system rows
     DECLARE @ManagerBankCode nvarchar(50),
		@PDate datetime,
		@SecurityValue decimal(19, 2),
		@AssetValue decimal(19, 2),
		@LiabilityValue decimal(19, 2),
		@LiabilityNegativeValue decimal(19, 2),
		@TotalValue decimal(19, 2),
		@SystemDefinedTypeID INT
		
	SELECT @SystemDefinedTypeID = IntegrationManagerPositionTypeID FROM IntegrationManagerPositionType WHERE PositionTypeName='SYSTEM_DEFINED'
	
	DELETE imph FROM IntegrationManagerPositionHistory imph
	inner join IntegrationImportRun iir on iir.IntegrationImportRunID = imph.IntegrationImportRunID
	inner join IntegrationImportDefinition iid on iir.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID 
	INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON iid.IntegrationImportDefinitionID = imacgid.IntegrationImportDefinitionID
	INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationManagerPositionTypeID = @SystemDefinedTypeID AND imacg.IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND PositionDate = @RunDate
	 
	
	
	
	DECLARE total_cursor CURSOR local forward_only FOR 
	SELECT
		ManagerBankCode,
		PositionDate = PositionDate,
		SecurityValue,
		AssetValue,
		LiabilityValue,
		LiabilityNegativeValue,
		TotalValue
	FROM IntegrationManagerPosition mp
	WHERE IntegrationManagerAssetClassGroupID = @IntegrationManagerAssetClassGroupID AND TotalValue IS NOT NULL AND TotalValue <> 0 AND PositionDate = @RunDate;
    
    OPEN total_cursor;

	FETCH NEXT FROM total_cursor 
	INTO @ManagerBankCode, @Pdate,@SecurityValue,@AssetValue,@LiabilityValue,@LiabilityNegativeValue,@TotalValue;
		
	WHILE @@FETCH_STATUS = 0
	BEGIN
	
		INSERT INTO IntegrationManagerPositionHistory
		(
			ManagerBankCode,
			ManagerValue,
			AssetClass,
			PositionDate,
			CashValue,
			SecurityValue,
			IntegrationManagerPositionTypeID,
			IntegrationImportRunID
		)
		VALUES
		(
			@ManagerBankCode,
			-(ISNULL(@AssetValue,0) + ISNULL(@LiabilityValue,0) - ISNULL(@LiabilityNegativeValue,0) + ISNULL(@SecurityValue,0)),
			'SYSTEM DEFINED SECURITY VALUE - (Negative cash value in the ManagerValue column to balance totals)',
			@Pdate,
			0,
			@TotalValue - (ISNULL(@AssetValue,0) + ISNULL(@LiabilityValue,0) - ISNULL(@LiabilityNegativeValue,0)+ ISNULL(@SecurityValue,0)),
			@SystemDefinedTypeID,
			@IntegrationImportRunID
		)
		
		FETCH NEXT FROM total_cursor 
		INTO @ManagerBankCode, @Pdate,@SecurityValue,@AssetValue,@LiabilityValue,@LiabilityNegativeValue,@TotalValue;	
	END
	CLOSE total_cursor;
    DEALLOCATE total_cursor;

END
