USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerGoldmanMoneyMarket_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerGoldmanMoneyMarket]'))
ALTER TABLE [source].[ManagerGoldmanMoneyMarket] DROP CONSTRAINT [FK_ManagerGoldmanMoneyMarket_IntegrationImportRun]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerGoldmanMoneyMarket]') AND type in (N'U'))
DROP TABLE [source].[ManagerGoldmanMoneyMarket]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerGoldmanSDIAccounts_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerGoldmanSDIAccounts]'))
ALTER TABLE [source].[ManagerGoldmanSDIAccounts] DROP CONSTRAINT [FK_ManagerGoldmanSDIAccounts_IntegrationImportRun]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerGoldmanSDIAccounts]') AND type in (N'U'))
DROP TABLE [source].[ManagerGoldmanSDIAccounts]


SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerGoldmanMoneyMarket](
	[Fund] [varchar](50) NULL,
	[FundName] [varchar](100) NULL,
	[Account] [varchar](100) NULL,
	[SubAccount] [varchar](100) NULL,
	[OpeningShares] float NULL,
	[Currency] [varchar](100) NULL,
	[NAV] float NULL,
	[OpeningValue] float NULL,
	[NetActivity] float NULL,
	[MTDDivAccrual] float NULL,
	[CurrentValue] float NULL,
	[Rep] [varchar](100) NULL,
	[Entity] [varchar](100) NULL,
	[EntityName] [varchar](100) NULL,
	[FundFamily] [varchar](100) NULL,
	[ISIN] [varchar](100) NULL,
	[CUSIP] [varchar](100) NULL,
	[ReportDate] [datetime] NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]

CREATE TABLE [source].[ManagerGoldmanSDIAccounts](
	[COBDate] [varchar](100) NULL,
	[GSAccountNumber] [varchar](100) NULL,
	[ClientAccountName] [varchar](100) NULL,
	[AccountType] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[BeginningAccountBalance] float NULL,
	[EndingAccountBalance] float NULL,
	[GrossRealizedPL] float NULL,
	[NetRealizedPL] float NULL,
	[CommissionAndFees] float NULL,
	[OpenTradeEquity] float NULL,
	[TotalEquity] float NULL,
	[InitialMarginRequirement] float NULL,
	[MaintenanceMarginRequirement] float NULL,
	[ExcessDeficit] float NULL,
	[SecurityOnDeposit] float NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerGoldmanMoneyMarket]  WITH CHECK ADD  CONSTRAINT [FK_ManagerGoldmanMoneyMarket_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerGoldmanMoneyMarket] CHECK CONSTRAINT [FK_ManagerGoldmanMoneyMarket_IntegrationImportRun]

ALTER TABLE [source].[ManagerGoldmanSDIAccounts]  WITH CHECK ADD  CONSTRAINT [FK_ManagerGoldmanSDIAccounts_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerGoldmanSDIAccounts] CHECK CONSTRAINT [FK_ManagerGoldmanSDIAccounts_IntegrationImportRun]



