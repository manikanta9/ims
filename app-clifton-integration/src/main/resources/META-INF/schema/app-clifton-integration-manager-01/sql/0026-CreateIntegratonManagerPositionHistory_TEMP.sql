CREATE TABLE [dbo].[IntegrationManagerPositionHistory_TEMP](
	[IntegrationManagerPositionHistoryID] [int] IDENTITY(1,1) NOT NULL,
	[IntegrationImportRunID] [int] NOT NULL,
	[ManagerBankCode] [nvarchar](50) NOT NULL,
	[AssetClass] [nvarchar](500) NULL,
	[SourceAssetClass] [nvarchar](500) NULL,
	[SecurityID] [nvarchar](50) NULL,
	[SecurityIDType] [nvarchar](50) NULL,
	[SecurityDescription] [nvarchar](500) NULL,
	[ManagerValue] [decimal](19, 2) NOT NULL,
	[IntegrationManagerPositionTypeID] [int] NULL,
	[CashValue] [decimal](19, 2) NULL,
	[SecurityValue] [decimal](19, 2) NULL,
	[PositionDate] [datetime] NULL,
 CONSTRAINT [PK_IntegrationManagerPositionHistory_TEMP] PRIMARY KEY CLUSTERED 
(
	[IntegrationManagerPositionHistoryID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[IntegrationManagerPositionHistory_TEMP]  WITH CHECK ADD  CONSTRAINT [FK_IntegrationManagerPositionHistory_TEMP_IntegrationImportRun_IntegrationImportRunID] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [dbo].[IntegrationManagerPositionHistory_TEMP] CHECK CONSTRAINT [FK_IntegrationManagerPositionHistory_TEMP_IntegrationImportRun_IntegrationImportRunID]

ALTER TABLE [dbo].[IntegrationManagerPositionHistory_TEMP]  WITH CHECK ADD  CONSTRAINT [FK_IntegrationManagerPositionHistory_TEMP_IntegrationManagerPositionType_IntegrationManagerPositionTypeID] FOREIGN KEY([IntegrationManagerPositionTypeID])
REFERENCES [dbo].[IntegrationManagerPositionType] ([IntegrationManagerPositionTypeID])

ALTER TABLE [dbo].[IntegrationManagerPositionHistory_TEMP] CHECK CONSTRAINT [FK_IntegrationManagerPositionHistory_TEMP_IntegrationManagerPositionType_IntegrationManagerPositionTypeID]
CREATE INDEX [ix_IntegrationManagerPositionHistory_TEMP_IntegrationImportRunID] ON IntegrationManagerPositionHistory_TEMP(IntegrationImportRunID)
