CREATE TABLE [dbo].[IntegrationManagerTransaction_TEMP](
	[IntegrationManagerTransactionID] [int] IDENTITY(1,1) NOT NULL,
	[IntegrationImportRunID] [int] NOT NULL,
	[ManagerBankCode] [nvarchar](50) NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[TransactionAmount] [decimal](19, 2) NOT NULL,
	[TransactionDescription] [nvarchar](500) NULL,
 CONSTRAINT [PK_IntegrationManagerTransaction_TEMP] PRIMARY KEY CLUSTERED
(
	[IntegrationManagerTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


ALTER TABLE [dbo].[IntegrationManagerTransaction_TEMP]  WITH CHECK ADD  CONSTRAINT [FK_IntegrationManagerTransaction_TEMP_IntegrationImportRun_IntegrationImportRunID] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [dbo].[IntegrationManagerTransaction_TEMP] CHECK CONSTRAINT [FK_IntegrationManagerTransaction_TEMP_IntegrationImportRun_IntegrationImportRunID]
