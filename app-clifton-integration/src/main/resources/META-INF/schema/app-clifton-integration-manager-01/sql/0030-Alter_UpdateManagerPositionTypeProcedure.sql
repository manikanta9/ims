-- exec UpdateManagerPositionType 4,'11/29/10'

ALTER PROCEDURE [dbo].[UpdateManagerPositionType]
	@IntegrationImportRunID INT
AS
BEGIN
	
	SET NOCOUNT ON;

	
	
	-- Update position types
	DECLARE @IntegrationImportDefinitionID INT, @UseUnmappedType BIT, @UnmappedTypeID INT
	SELECT
		@IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID,
		@UseUnmappedType = CASE WHEN UnmappedPositionTypeID IS NULL THEN 0 ELSE 1 END,
		@UnmappedTypeID = UnmappedPositionTypeID
	FROM IntegrationImportRun iir
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationImportRunID = @IntegrationImportRunID
	
	
	UPDATE imph SET 
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	--ASSET CLASS AND BANK CODE MAPPED
	FROM IntegrationManagerPositionHistory imph 
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
		INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode = impb.ManagerBankCode
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID 
		
	
	IF @UseUnmappedType = 0
	BEGIN
	
		UPDATE imph SET 
			IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
		--ASSET CLASS MAPPED, BANK CODE NOT MAPPED
		FROM IntegrationManagerPositionHistory imph 
			INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
			INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
			INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
			INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
			INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
				AND imac.AssetClass = imps.AssetClass 
		WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND imac.ManagerBankCode IS NULL 
			AND NOT EXISTS(
				SELECT TOP 1 * 
				FROM IntegrationManagerAssetClass c 
				WHERE c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
					AND c.AssetClass = imps.AssetClass AND c.ManagerBankCode = impb.ManagerBankCode
			)
		
	END
	ELSE
	BEGIN
		UPDATE imph SET 
			IntegrationManagerPositionTypeID = ISNULL(imac.IntegrationManagerPositionTypeID, @UnmappedTypeID)
		--UNMAPPED ASSET CLASSES INCLUDED
		FROM IntegrationManagerPositionHistory imph 
			INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
			INNER JOIN IntegrationManagerPositionSecurity imps ON imph.IntegrationManagerPositionSecurityID = imps.IntegrationManagerPositionSecurityID
			INNER JOIN IntegrationManagerPositionBank impb ON imph.IntegrationManagerPositionBankID = impb.IntegrationManagerPositionBankID
			INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
			LEFT JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
				AND imac.AssetClass = imps.AssetClass 
		WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND imac.ManagerBankCode IS NULL 
			AND NOT EXISTS(
				SELECT TOP 1 * 
				FROM IntegrationManagerAssetClass c 
				WHERE c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
					AND c.AssetClass = imps.AssetClass AND c.ManagerBankCode = impb.ManagerBankCode
			)
	END
	
END


