USE [CliftonIntegration]
GO
/****** Object:  StoredProcedure [dbo].[DeleteDailyHistoryData]    Script Date: 04/11/2011 13:34:44 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[DeleteDailyHistoryData]
	@IntegrationImportRunID INT,
	@Table nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	exec('DELETE FROM ' + @Table + '
	WHERE IntegrationImportRunID IN
	(
		SELECT IntegrationImportRunID
		FROM IntegrationImportRun
		WHERE EffectiveDate = (SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = ' + @IntegrationImportRunID + ')
			AND IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = ' + @IntegrationImportRunID + ')
	)');
	
END
