USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBNYMellonCIBC]    Script Date: 11/03/2010 10:22:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerBNYMellonCIBC]') AND type in (N'U'))
DROP TABLE [source].[ManagerBNYMellonCIBC]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBNYMellonCIBC]    Script Date: 11/03/2010 10:22:31 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerBNYMellonCIBC](
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[AsOfDate] [datetime] NULL,
	[AssetCategory] [varchar](100) NULL,
	[SectorName] [varchar](100) NULL,
	[BaseCurrencyCode] [varchar](100) NULL,
	[AlternateBaseCurrency] [varchar](100) NULL,
	[ExchangeRate] decimal(28,16) NULL,
	[LocalCurrencyCode] [varchar](100) NULL,
	[LocalCurrency] [varchar](100) NULL,
	[CountryCode] [varchar](100) NULL,
	[CountryName] [varchar](100) NULL,
	[Shares] decimal(28,16) NULL,
	[BaseCost] decimal(28,16) NULL,
	[LocalCost] decimal(28,16) NULL,
	[BaseMarketValue] decimal(28,16) NULL,
	[LocalMarketValue] decimal(28,16) NULL,
	[BaseEstimatedAnnualIncome] decimal(28,16) NULL,
	[BaseUnrealizedInvGainLoss] decimal(28,16) NULL,
	[LocalUnrealizedInvGainLoss] decimal(28,16) NULL,
	[BaseUnrealizedCurrencyGainLoss] decimal(28,16) NULL,
	[AccountStatus] [varchar](100) NULL,
	[ReportID] [varchar](100) NULL,
	[LocalBase] [varchar](100) NULL,
	[SheetName] [varchar](250) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerBNYMellonCIBC]  WITH CHECK ADD  CONSTRAINT [FK_ManagerBNYMellonCIBC_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerBNYMellonCIBC] CHECK CONSTRAINT [FK_ManagerBNYMellonCIBC_IntegrationImportRun]

