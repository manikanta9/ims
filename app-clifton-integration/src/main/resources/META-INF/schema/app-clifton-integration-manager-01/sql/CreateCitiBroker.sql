USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerCitiBroker_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerCitiBroker]'))
ALTER TABLE [source].[ManagerCitiBroker] DROP CONSTRAINT [FK_ManagerCitiBroker_IntegrationImportRun]


/****** Object:  Table [source].[ManagerUSBank]    Script Date: 11/10/2010 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerCitiBroker]') AND type in (N'U'))
DROP TABLE [source].[ManagerCitiBroker]


/****** Object:  Table [source].[ManagerUSBank]    Script Date: 11/10/2010 16:24:27 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerCitiBroker](
	[COB_Date] [datetime] NULL,
	[Account_Ref] [varchar] NULL,
	[Account_Name] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[Total_Cash_Equity] float NULL,
	[FWD_Open_Trade_Equity] float NULL,
	[Initial_Margin] float NULL,
	[Collateral_Margin_Value] float NULL,
	[Excess_Deficit_Collateral] float NULL,
	[Excess_Deficit_Cash] float NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerCitiBroker]  WITH CHECK ADD  CONSTRAINT [FK_ManagerCitiBroker_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerCitiBroker] CHECK CONSTRAINT [FK_ManagerCitiBroker_IntegrationImportRun]



