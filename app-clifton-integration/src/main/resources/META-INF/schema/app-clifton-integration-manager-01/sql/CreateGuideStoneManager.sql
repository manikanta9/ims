USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerGuidestone_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerGuidestone]'))
ALTER TABLE [source].[ManagerGuidestone] DROP CONSTRAINT [FK_ManagerGuidestone_IntegrationImportRun]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerGuidestone]') AND type in (N'U'))
DROP TABLE [source].[ManagerGuidestone]

SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerGuidestone](
	[ManagerAccountNumber] [varchar](100) NULL,
	[ManagerName] [varchar](100) NULL,
	[AssetClass] [varchar](100) NULL,
	[NetAssetValue] [varchar](100) NULL,
	[CliftonCash] float NULL,
	[ManagerCash] float NULL,
	[SecurityAmount] float NULL,
	[FinalAmount] float NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]

SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerGuidestone]  WITH CHECK ADD  CONSTRAINT [FK_ManagerGuidestone_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerGuidestone] CHECK CONSTRAINT [FK_ManagerGuidestone_IntegrationImportRun]



