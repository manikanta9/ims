USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAccountingCurrency]    Script Date: 11/03/2010 10:21:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerBNYMellonMMBB]') AND type in (N'U'))
DROP TABLE [source].[ManagerBNYMellonMMBB]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBNYMellonMMBB]    Script Date: 06/15/2011 10:02:12 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerBNYMellonMMBB](
	[IntegrationImportRunID] [int] NOT NULL,
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[SourceAccountNumber] [varchar](100) NULL,
	[SourceAccountName] [varchar](100) NULL,
	[ReportingAccountBaseCurrency] [varchar](100) NULL,
	[ReportingAccountFXRate] float NULL,
	[SourceAccountBaseCurrency] [varchar](100) NULL,
	[SourceAccountFXRate] [varchar](100) NULL,
	[SourceAccountToReportingAccountFXRate] [varchar](100) NULL,
	[VendorRate] [varchar](100) NULL,
	[AsofDate] [datetime] NULL,
	[SecurityID] [varchar](100) NULL,
	[SecurityCrossReferenceType] [varchar](100) NULL,
	[SecurityCrossReference] [varchar](100) NULL,
	[Description1] [varchar](100) NULL,
	[Description2] [varchar](100) NULL,
	[AssetCode] [varchar](100) NULL,
	[AssetType] [varchar](100) NULL,
	[SectorDescription] [varchar](100) NULL,
	[LocalCurrency] [varchar](100) NULL,
	[LocalCurrencyName] [varchar](100) NULL,
	[CountryCode] [varchar](100) NULL,
	[CountryName] [varchar](100) NULL,
	[Shares] float NULL,
	[BaseCost] float NULL,
	[LocalCost] float NULL,
	[BasePrice] float NULL,
	[LocalPrice] float NULL,
	[BaseMarketValue] float NULL,
	[LocalMarketValue] float NULL,
	[CouponRate] [varchar](100) NULL,
	[MaturityDate] [varchar](100) NULL,
	[BaseUnrealizedInvestmentGainLoss] float NULL,
	[LocalUnrealizedInvestmentGainLoss] float NULL,
	[BaseUnrealizedCurrencyGainLoss] float NULL,
	[ReportingAccountStatus] [varchar](100) NULL,
	[PositionType] [varchar](100) NULL,
	[ReportID] [varchar](100) NULL,
	[LocalBaseBoth] [varchar](100) NULL,
	[GLAccount] [varchar](100) NULL,
	[ConsolidateExplode] [varchar](100) NULL,
	[AlternateBaseCurrency] [varchar](100) NULL,
	[SourceAccountStatus] [varchar](100) NULL,
	[AssetMixCategoryCode] [varchar](100) NULL,
	[AssetMixCategoryDescription] [varchar](100) NULL,
	[Form5500CategoryCode] [varchar](100) NULL,
	[Form5500ClassCodeDescription] [varchar](100) NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF




ALTER TABLE [source].[ManagerBNYMellonMMBB]  WITH CHECK ADD  CONSTRAINT [FK_ManagerBNYMellonMMBB_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerBNYMellonMMBB] CHECK CONSTRAINT [FK_ManagerBNYMellonMMBB_IntegrationImportRun]

CREATE INDEX [ix_ManagerBNYMellonMMBB_IntegrationImportDefinitionID] ON source.ManagerBNYMellonMMBB (IntegrationImportRunID)


