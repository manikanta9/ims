USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAccountingCurrency]    Script Date: 11/03/2010 10:21:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerSchwab]') AND type in (N'U'))
DROP TABLE [source].[ManagerSchwab]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerSchwab]    Script Date: 06/15/2011 10:02:12 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerSchwab](
	[IntegrationImportRunID] [int] NOT NULL,
	AsOfBusinessDate [datetime] NULL,
	AccountNumber [varchar](100) NULL,
	AccountName [varchar](100) NULL,
	Symbol [varchar](100) NULL,
	SecurityType [varchar](100) NULL,
	SecurityDescription [varchar](100) NULL,
	CashMargin [varchar](100) NULL,
	NumberOfShares [float] NULL,
	LongShort [varchar](100) NULL,
	Price [float] NULL,
	DateOfPrice [datetime] NULL,
	MarketValue [float] NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]


ALTER TABLE [source].[ManagerSchwab]  WITH CHECK ADD  CONSTRAINT [FK_ManagerSchwab_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerSchwab] CHECK CONSTRAINT [FK_ManagerSchwab_IntegrationImportRun]




