USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerWellsMarketCost]    Script Date: 11/03/2010 10:28:51 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerWellsMarketCost]') AND type in (N'U'))
DROP TABLE [source].[ManagerWellsMarketCost]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerWellsMarketCost]    Script Date: 11/03/2010 10:28:51 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerWellsMarketCost](
	[AccountNumber] [varchar](50) NULL,
	[MajorAssetGroup] [varchar](50) NULL,
	[MinorAssetClass] [varchar](50) NULL,
	[IndustryClass] [varchar](50) NULL,
	[AssetID] [varchar](50) NULL,
	[AccountShortName] [varchar](50) NULL,
	[Units] [decimal](18, 2) NULL,
	[ActivityPendingIndicator] [char](1) NULL,
	[FedTaxCost] [decimal](18, 2) NULL,
	[EstAnnualIncome] [decimal](18, 2) NULL,
	[MarketValue] [decimal](18, 2) NULL,
	[Price] [decimal](18, 2) NULL,
	[PriceDate] [date] NULL,
	[SPRating] [varchar](50) NULL,
	[MoodyRating] [varchar](50) NULL,
	[TradedUnits] [varchar](50) NULL,
	[FreeReceiptDeliveryUnitsPending] [bigint] NULL,
	[CapitActionUnitsPending] [bigint] NULL,
	[EncumberedUnits] [bigint] NULL,
	[MaturityDate] [date] NULL,
	[InterestRate] [varchar](50) NULL,
	[AssetDescription1] [varchar](150) NULL,
	[AssetDescription2] [varchar](150) NULL,
	[AssetDescription3] [varchar](100) NULL,
	[AssetDescription4] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerWellsMarketCost]  WITH CHECK ADD  CONSTRAINT [FK_ManagerWellsMarketCost_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerWellsMarketCost] CHECK CONSTRAINT [FK_ManagerWellsMarketCost_IntegrationImportRun]



