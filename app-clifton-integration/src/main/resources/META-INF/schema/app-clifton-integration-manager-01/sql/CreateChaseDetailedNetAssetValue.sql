USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerChaseDetailedNetAssetValue]') AND type in (N'U'))
DROP TABLE [source].[ManagerChaseDetailedNetAssetValue]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerChaseDetailedNetAssetValue]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON



CREATE TABLE [source].[ManagerChaseDetailedNetAssetValue](
	[AccountNumber] [nvarchar](50) NULL,
	[ReportCurrencyCode] [nvarchar](50) NULL,
	[NetAssetMarketValue] float NULL,
	[AccountName] [nvarchar](150) NULL,
	[AccountStatus] [nvarchar](50) NULL,
	[BaseCurrencyCode] [nvarchar](50) NULL,
	[SecurityID] [nvarchar](50) NULL,
	[SecurityDescription1] [nvarchar](150) NULL,
	[SecurityDescription2] [nvarchar](150) NULL,
	[ExchangeRate] float NULL,
	[Quantity] float NULL,
	[CostLocal] float NULL,
	[MarketPrice] float NULL,
	[MarketValueLocal] float NULL,
	[CostBase] float NULL,
	[MarketValueBase] float NULL,
	[AccruedIncomeBase] float NULL,
	[UnrealizedGainLossBase] float NULL,
	[MarketValuePlusAccruedIncomeBase] float NULL,
	[PercentofFund] float NULL,
	[SegregatedCash] float NULL,
	[SegregatedCashPercentofFund] float NULL,
	[SubscriptionsReceivable] float NULL,
	[SubscriptionsReceivablePercentofFund] float NULL,
	[RedemptionsPayable] float NULL,
	[RedemptionsPayablePercentofFund] float NULL,
	[Segment] [nvarchar](50) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerChaseDetailedNetAssetValue]  WITH CHECK ADD  CONSTRAINT [FK_ManagerChaseDetailedNetAssetValue_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerChaseDetailedNetAssetValue] CHECK CONSTRAINT [FK_ManagerChaseDetailedNetAssetValue_IntegrationImportRun]


