USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[source].[FK_ManagerBrotherhoodBank_IntegrationImportRun]') AND parent_object_id = OBJECT_ID(N'[source].[ManagerBrotherhoodBank]'))
ALTER TABLE [source].[ManagerBrotherhoodBank] DROP CONSTRAINT [FK_ManagerBrotherhoodBank_IntegrationImportRun]


/****** Object:  Table [source].[ManagerBrotherhoodBank]    Script Date: 11/10/2010 16:24:27 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerBrotherhoodBank]') AND type in (N'U'))
DROP TABLE [source].[ManagerBrotherhoodBank]


/****** Object:  Table [source].[ManagerBrotherhoodBank]    Script Date: 11/10/2010 16:24:27 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerBrotherhoodBank](
	[AccountNumber] [varchar](50) NULL,
	[MarketValue] [decimal](18, 2) NULL,
	[SecurityName] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[ManagerBrotherhoodBank]  WITH CHECK ADD  CONSTRAINT [FK_ManagerBrotherhoodBank_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerBrotherhoodBank] CHECK CONSTRAINT [FK_ManagerBrotherhoodBank_IntegrationImportRun]



