USE [CliftonIntegration]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerNorthernTrustCombinedAccount]') AND type in (N'U'))
DROP TABLE [source].[ManagerNorthernTrustCombinedAccount]

SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE source.ManagerNorthernTrustCombinedAccount
(
	ManagerKeyBankID INT IDENTITY(1,1) NOT NULL,
	Consolidation NVARCHAR(100),
	AccountNumber NVARCHAR(100),
	AccountName NVARCHAR(100),
	AsOfDate DATE NULL,
	MarketPrice FLOAT,
	MarketValueLocal FLOAT,
	MarketValueBase FLOAT,
	A_ADJ_BAS FLOAT,
	A_ADJ_BAS_BSE FLOAT,
	UnrealizedMarketGainLoss FLOAT,
	UnrealizedTranslationGainLoss FLOAT,
	UnrealizedTotalGainLoss FLOAT,
	IncomeReceivableBase FLOAT,
	ShareParValue FLOAT,
	CurrencyCode NVARCHAR(100),
	CurrencyCash NVARCHAR(100),
	CurrencyDescription NVARCHAR(100),
	CountryName NVARCHAR(100),
	I_INV_SUP_CATG NVARCHAR(100),
	AssetSuperCategoryName NVARCHAR(100),
	I_INV_SUB_CATG NVARCHAR(100),
	AssetSubCategoryName NVARCHAR(100),
	AssetDescription NVARCHAR(100),
	C_HLDG_TYPE INT,
	AssetIdentifier NVARCHAR(100),
	IncomeReceivableLocal FLOAT,
	C_VALN_TYPE_O NVARCHAR(100),
	ErrorCode NVARCHAR(100),
	C_ASSET_LIAB NVARCHAR(100),
	ConsolidationAuditIndicatorFlag NVARCHAR(8),
	OriginalFaceValue FLOAT,
	FinancialInstitutionRoleType NVARCHAR(100),
	PriceType NVARCHAR(100),
	PriceQuoteMethodType NVARCHAR(100),
	IntegrationImportRunID INT
)  ON [PRIMARY]


SET ANSI_PADDING OFF



USE [CliftonIntegration]


ALTER TABLE source.ManagerNorthernTrustCombinedAccount WITH CHECK ADD CONSTRAINT [FK_ManagerNorthernTrustCombinedAccount_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerNorthernTrustCombinedAccount] CHECK CONSTRAINT [FK_ManagerNorthernTrustCombinedAccount_IntegrationImportRun]




