USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAssetSummary]    Script Date: 11/03/2010 10:22:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerMellonAssetSummary]') AND type in (N'U'))
DROP TABLE [source].[ManagerMellonAssetSummary]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAssetSummary]    Script Date: 11/03/2010 10:22:31 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerMellonAssetSummary](
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[AsOfDate] [datetime] NULL,
	[AssetCategory] [varchar](100) NULL,
	[SectorName] [varchar](100) NULL,
	[BaseCurrencyCode] [varchar](100) NULL,
	[AlternateBaseCurrency] [varchar](100) NULL,
	[ExchangeRate] float NULL,
	[LocalCurrencyCode] [varchar](100) NULL,
	[LocalCurrency] [varchar](100) NULL,
	[CountryCode] [varchar](100) NULL,
	[CountryName] [varchar](100) NULL,
	[Shares] float NULL,
	[BaseCost] float NULL,
	[LocalCost] float NULL,
	[BaseMarketValue] float NULL,
	[LocalMarketValue] float NULL,
	[BaseEstimatedAnnualIncome] float NULL,
	[BaseUnrealizedInvGainLoss] float NULL,
	[LocalUnrealizedInvGainLoss] float NULL,
	[BaseUnrealizedCurrencyGainLoss] float NULL,
	[AccountStatus] [varchar](100) NULL,
	[ReportID] [varchar](100) NULL,
	[LocalBase] [varchar](100) NULL,
	[SheetName] [varchar](250) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerMellonAssetSummary]  WITH CHECK ADD  CONSTRAINT [FK_ManagerMellonAssetSummary_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerMellonAssetSummary] CHECK CONSTRAINT [FK_ManagerMellonAssetSummary_IntegrationImportRun]

