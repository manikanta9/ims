USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonPendingTransactions]    Script Date: 11/03/2010 10:23:26 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerMellonPendingTransactions]') AND type in (N'U'))
DROP TABLE [source].[ManagerMellonPendingTransactions]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonPendingTransactions]    Script Date: 11/03/2010 10:23:26 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerMellonPendingTransactions](
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[SourceAccount] [varchar](100) NULL,
	[SourceAccountName] [varchar](100) NULL,
	[SecurityID] [varchar](100) NULL,
	[BaseCurrency] [varchar](100) NULL,
	[LocalCurrencyCode] [varchar](100) NULL,
	[LocalCurrencyName] [varchar](100) NULL,
	[AsOfDate] [datetime] NULL,
	[SecurityXRefType] [varchar](100) NULL,
	[SecurityXRef] [varchar](100) NULL,
	[SecurityDescription1] [varchar](100) NULL,
	[SecurityDescription2] [varchar](100) NULL,
	[TransactionCategoryCode] [varchar](100) NULL,
	[TransactionDescription] [varchar](100) NULL,
	[TradeDate] [datetime] NULL,
	[SettleDate] [datetime] NULL,
	[ContractSettleDate] [datetime] NULL,
	[BasePrice] float NULL,
	[LocalPrice] float NULL,
	[BaseCommissionsFees] float NULL,
	[LocalCommissionsFees] float NULL,
	[SharesPar] float NULL,
	[BrokerCode] [varchar](100) NULL,
	[BrokerName] [varchar](100) NULL,
	[BasePrincipalAmount] float NULL,
	[BaseIncomeAmount] float NULL,
	[BaseSettlementAmount] float NULL,
	[LocalPrincipalAmount] float NULL,
	[LocalIncomeAmount] float NULL,
	[LocalSettlementAmount] float NULL,
	[UnrealizedGainLoss] float NULL,
	[LocalBase] [varchar](100) NULL,
	[TransactionCategory] [varchar](100) NULL,
	[TransactionReferenceNumber] [varchar](100) NULL,
	[TransactionStatus] [varchar](100) NULL,
	[ReportID] [varchar](100) NULL,
	[ReportingAccountStatus] [varchar](100) NULL,
	[SourceAccountStatus] [varchar](100) NULL,
	[SheetName] [varchar](250) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerMellonPendingTransactions]  WITH CHECK ADD  CONSTRAINT [FK_ManagerMellonPendingTransactions_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerMellonPendingTransactions] CHECK CONSTRAINT [FK_ManagerMellonPendingTransactions_IntegrationImportRun]


