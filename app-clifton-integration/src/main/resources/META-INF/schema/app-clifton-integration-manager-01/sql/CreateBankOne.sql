USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBankOne]    Script Date: 12/02/2010 12:07:10 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerBankOne]') AND type in (N'U'))
DROP TABLE [source].[ManagerBankOne]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBankOne]    Script Date: 12/02/2010 12:07:11 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerBankOne](
	[IntegrationImportRunID] [int] NOT NULL,
	[AccountID] [bigint] NULL,
	[CUSIP] [varchar](10) NULL,
	[AssetClassCode] [varchar](4) NULL,
	[AssetClassName] [varchar](30) NULL,
	[SharesParValueQty] float NULL,
	[AnnualPerUnitIncome] float NULL,
	[MaturityDate] [datetime] NULL,
	[EOMPrice] float NULL,
	[CurrentPrice] float NULL,
	[CurrentPricingDate] [datetime] NULL,
	[AssetName] [varchar](36) NULL,
	[PortfolioNum] [bigint] NULL,
	[IssueDate] [datetime] NULL,
	[StateID] [varchar](2) NULL,
	[AccrualDayCode] [varchar](5) NULL,
	[PaymentsPerYear] [varchar](5) NULL,
	[RecordDate] [datetime] NULL,
	[IncomeDate] [datetime] NULL,
	[IncomeDate2] [datetime] NULL,
	[FedTaxCost] float NULL,
	[FedTaxAcqDate] [datetime] NULL,
	[MarketValue] float NULL,
	[AsOfDate] [datetime] NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerBankOne]  WITH CHECK ADD  CONSTRAINT [FK_ManagerBankOne_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerBankOne] CHECK CONSTRAINT [FK_ManagerBankOne_IntegrationImportRun]



