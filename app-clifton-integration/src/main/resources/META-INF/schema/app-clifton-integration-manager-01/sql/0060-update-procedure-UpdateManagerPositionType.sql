ALTER PROCEDURE [dbo].[UpdateManagerPositionType]
	@IntegrationImportRunID INT
AS
BEGIN

	SET NOCOUNT ON;


	-- Update position types
	DECLARE @IntegrationImportDefinitionID INT, @UseUnmappedType BIT, @UnmappedTypeID INT
	SELECT
		@IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID,
		@UseUnmappedType = CASE WHEN UnmappedPositionTypeID IS NULL THEN 0 ELSE 1 END,
		@UnmappedTypeID = UnmappedPositionTypeID
	FROM IntegrationImportRun iir
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
	WHERE IntegrationImportRunID = @IntegrationImportRunID


	--UPDATE ALL WITH AN ASSET CLASS, MANAGER BANK CODE, AND SECURITY DESCRIPTION PATTERN MAPPED
	UPDATE imph SET
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerSecurity imps ON imph.IntegrationManagerSecurityID = imps.IntegrationManagerSecurityID
		INNER JOIN IntegrationManagerBank impb ON imph.IntegrationManagerBankID = impb.IntegrationManagerBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE (CASE WHEN imac.IsSecurityDescriptionPatternExactMatch = 1 THEN imac.SecurityDescriptionPattern ELSE '%' + imac.SecurityDescriptionPattern + '%' END)
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID



	--UPDATE ALL WITH AN ASSET CLASS AND MANAGER BANK CODE MAPPED
	UPDATE imph SET
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerSecurity imps ON imph.IntegrationManagerSecurityID = imps.IntegrationManagerSecurityID
		INNER JOIN IntegrationManagerBank impb ON imph.IntegrationManagerBankID = impb.IntegrationManagerBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			--FILTER FOR RELEVANT ROWS FROM THE IntegrationManagerAssetClass TABLE
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode = impb.ManagerBankCode AND imac.SecurityDescriptionPattern IS NULL
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		--EXCLUDE ROWS FROM THE FILE THAT HAVE ALREADY BEEN MAPPED
		AND NOT EXISTS(
			SELECT TOP 1 *
			FROM IntegrationManagerAssetClass c
			WHERE c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
				AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE (CASE WHEN c.IsSecurityDescriptionPatternExactMatch = 1 THEN c.SecurityDescriptionPattern ELSE '%' + c.SecurityDescriptionPattern + '%' END)
		)



	--UPDATE ALL WITH AN ASSET CLASS AND SECURITY DESCRIPTION PATTERN MAPPED
	UPDATE imph SET
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerSecurity imps ON imph.IntegrationManagerSecurityID = imps.IntegrationManagerSecurityID
		INNER JOIN IntegrationManagerBank impb ON imph.IntegrationManagerBankID = impb.IntegrationManagerBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			--FILTER FOR RELEVANT ROWS FROM THE IntegrationManagerAssetClass TABLE
			AND imac.AssetClass = imps.AssetClass AND imps.SecurityDescription LIKE (CASE WHEN imac.IsSecurityDescriptionPatternExactMatch = 1 THEN imac.SecurityDescriptionPattern ELSE '%' + imac.SecurityDescriptionPattern + '%' END) AND imac.ManagerBankCode IS NULL
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		--EXCLUDE ROWS FROM THE FILE THAT HAVE ALREADY BEEN MAPPED
		AND NOT EXISTS(
			SELECT TOP 1 *
			FROM IntegrationManagerAssetClass c
			WHERE (c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
				AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE (CASE WHEN c.IsSecurityDescriptionPatternExactMatch = 1 THEN c.SecurityDescriptionPattern ELSE '%' + c.SecurityDescriptionPattern + '%' END))
			OR
				(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
				AND c.ManagerBankCode = impb.ManagerBankCode AND c.SecurityDescriptionPattern IS NULL)
		)



	--UPDATE ALL WITH AN ASSET CLASS MAPPED
	UPDATE imph SET
		IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
	FROM IntegrationManagerPositionHistory imph
		INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
		INNER JOIN IntegrationManagerSecurity imps ON imph.IntegrationManagerSecurityID = imps.IntegrationManagerSecurityID
		INNER JOIN IntegrationManagerBank impb ON imph.IntegrationManagerBankID = impb.IntegrationManagerBankID
		INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
		INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
		INNER JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
			--FILTER FOR RELEVANT ROWS FROM THE IntegrationManagerAssetClass TABLE
			AND imac.AssetClass = imps.AssetClass AND imac.ManagerBankCode IS NULL AND imac.SecurityDescriptionPattern IS NULL
	WHERE iir.IntegrationImportRunID = @IntegrationImportRunID
		--EXCLUDE ROWS FROM THE FILE THAT HAVE ALREADY BEEN MAPPED
		AND NOT EXISTS(
			SELECT TOP 1 *
			FROM IntegrationManagerAssetClass c
			WHERE (c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
				AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE (CASE WHEN c.IsSecurityDescriptionPatternExactMatch = 1 THEN c.SecurityDescriptionPattern ELSE '%' + c.SecurityDescriptionPattern + '%' END))
			OR
				(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
				AND c.ManagerBankCode = impb.ManagerBankCode AND c.SecurityDescriptionPattern IS NULL)
			OR
				(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
				AND imps.SecurityDescription LIKE (CASE WHEN c.IsSecurityDescriptionPatternExactMatch = 1 THEN c.SecurityDescriptionPattern ELSE '%' + c.SecurityDescriptionPattern + '%' END) AND c.ManagerBankCode IS NULL)
		)


	IF @UseUnmappedType = 1
	BEGIN

		--IF UseUnmappedType, UPDATE ALL WITH NO ASSET CLASS MAPPED
		UPDATE imph SET
			IntegrationManagerPositionTypeID = @UnmappedTypeID
		FROM IntegrationManagerPositionHistory imph
			INNER JOIN IntegrationImportRun iir ON imph.IntegrationImportRunID = iir.IntegrationImportRunID
			INNER JOIN IntegrationManagerSecurity imps ON imph.IntegrationManagerSecurityID = imps.IntegrationManagerSecurityID
			INNER JOIN IntegrationManagerBank impb ON imph.IntegrationManagerBankID = impb.IntegrationManagerBankID
			INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroupImportDefinition imacgid ON imacgid.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
			INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imacgid.IntegrationManagerAssetClassGroupID
			LEFT JOIN IntegrationManagerAssetClass imac ON imac.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID
				AND imac.AssetClass = imps.AssetClass
				AND EXISTS(
					SELECT TOP 1 *
					FROM IntegrationManagerAssetClass c
					WHERE (c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
						AND c.ManagerBankCode = impb.ManagerBankCode AND imps.SecurityDescription LIKE (CASE WHEN c.IsSecurityDescriptionPatternExactMatch = 1 THEN c.SecurityDescriptionPattern ELSE '%' + c.SecurityDescriptionPattern + '%' END))
					OR
						(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
						AND c.ManagerBankCode = impb.ManagerBankCode AND c.SecurityDescriptionPattern IS NULL)
					OR
						(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
						AND imps.SecurityDescription LIKE (CASE WHEN c.IsSecurityDescriptionPatternExactMatch = 1 THEN c.SecurityDescriptionPattern ELSE '%' + c.SecurityDescriptionPattern + '%' END) AND c.ManagerBankCode IS NULL)
					OR
						(c.IntegrationManagerAssetClassGroupID = imacg.IntegrationManagerAssetClassGroupID AND c.AssetClass = imps.AssetClass
						 AND imac.ManagerBankCode IS NULL AND imac.SecurityDescriptionPattern IS NULL)
				)
		WHERE iir.IntegrationImportRunID = @IntegrationImportRunID AND imac.AssetClass IS NULL

	END

END

