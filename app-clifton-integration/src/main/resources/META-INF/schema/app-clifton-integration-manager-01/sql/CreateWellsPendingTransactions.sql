USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerWellsPendingTransactions]    Script Date: 11/03/2010 10:29:31 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerWellsPendingTransactions]') AND type in (N'U'))
DROP TABLE [source].[ManagerWellsPendingTransactions]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerWellsPendingTransactions]    Script Date: 11/03/2010 10:29:31 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerWellsPendingTransactions](
	[AssetID] [varchar](50) NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountShortName] [varchar](50) NULL,
	[TranDate] [date] NULL,
	[TradeDate] [date] NULL,
	[Units] [int] NULL,
	[IncomeCash] [decimal](18, 2) NULL,
	[PrincipalCash] [decimal](18, 2) NULL,
	[TranType] [varchar](50) NULL,
	[TranCode] [bigint] NULL,
	[TranDesc] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerWellsPendingTransactions]  WITH CHECK ADD  CONSTRAINT [FK_ManagerWellsPendingTransactions_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerWellsPendingTransactions] CHECK CONSTRAINT [FK_ManagerWellsPendingTransactions_IntegrationImportRun]



