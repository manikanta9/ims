USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAccountingCurrency]    Script Date: 11/03/2010 10:21:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerStateStreetFund]') AND type in (N'U'))
DROP TABLE [source].[ManagerStateStreetFund]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerStateStreetFund]    Script Date: 06/15/2011 10:02:12 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerStateStreetFund](
	AssetClass [varchar](100) NULL
	, BaseCurrencyName [varchar](100) NULL
	, BaseMarketValue [float] NULL
	, Fund [varchar](100) NULL
	, FundName [varchar](100) NULL
	, InvestmentTypeName [varchar](100) NULL
	, MajorIndustryName [varchar](100) NULL
	, ManagerName [varchar](100) NULL
	, SecurityName [varchar](100) NULL
	, IntegrationImportRunID [int] not NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]


ALTER TABLE [source].[ManagerStateStreetFund]  WITH CHECK ADD  CONSTRAINT [FK_ManagerStateStreetFund_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerStateStreetFund] CHECK CONSTRAINT [FK_ManagerStateStreetFund_IntegrationImportRun]




