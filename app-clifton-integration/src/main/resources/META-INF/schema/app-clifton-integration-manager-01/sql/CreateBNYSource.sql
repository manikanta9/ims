USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBNY]    Script Date: 11/29/2010 15:12:29 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerBNY]') AND type in (N'U'))
DROP TABLE [source].[ManagerBNY]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerBNY]    Script Date: 11/29/2010 15:12:29 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerBNY](
	[IntegrationImportRunID] [int] NOT NULL,
	[AccountNumber] [int] NULL,
	[RelationshipNumber] [int] NULL,
	[AccountTitle] [varchar](50) NULL,
	[AssetNumber] [varchar](50) NULL,
	[AssetLongDescription] [varchar](100) NULL,
	[MaturityDate] [datetime] NULL,
	[EvaluationDate] [datetime] NULL,
	[CurrencyOfTradeCode] [varchar](50) NULL,
	[Price] float NULL,
	[Quantity] float NULL,
	[TotalAmountBase] float NULL,
	[TotalAmountLocal] float NULL,
	[MarketValueBase] float NULL,
	[MarketValueLocal] float NULL,
	[RatingSAndP] [varchar](50) NULL,
	[PriceSource] [int] NULL,
	[PriceDate] [datetime] NULL,
	[QuantityAmortized] float NULL,
	[IncomeEntitlementQuantity] float NULL,
	[IncomePaymentPreviousDate] [datetime] NULL,
	[IncomePaymentNextDate] [datetime] NULL,
	[EstimatedAnnualIncomeRate] float NULL,
	[EstimatedAnnualIncomeLocal] float NULL,
	[AccruedIncomeBase] float NULL,
	[AccruedIncomeLocal] float NULL,
	[GainLossMarketBase] float NULL,
	[GainLossMarketLocal] float NULL,
	[GainLossCurrencyBase] float NULL,
	[GainLossTotalBase] float NULL,
	[SharesOutstanding] float NULL,
	[YieldAtMarketValue] float NULL,
	[YieldAtOriginalCost] float NULL,
	[YieldAtBookValue] float NULL,
	[RatingMoody] [varchar](50) NULL,
	[Cusip] [varchar](50) NULL,
	[Isin] [varchar](50) NULL,
	[Sedol] [varchar](50) NULL,
	[TickerSymbol] [varchar](50) NULL,
	[AssetShortDescription] [varchar](50) NULL,
	[CurrencyOfOriginCode] [varchar](3) NULL,
	[CountryOfIssueCode] [varchar](2) NULL,
	[InterestRate] float NULL,
	[CurrencyOfTrade] [varchar](50) NULL,
	[CountryOfOriginName] [varchar](50) NULL,
	[CountryOfIssueName] [varchar](50) NULL,
	[AssetClassificationNumber] [varchar](4) NULL,
	[BookValueBase] float NULL,
	[BookValueLocal] float NULL,
	[CurrencyOfIncomeCode] [varchar](3) NULL,
	[CurrencyOfIncome] [varchar](50) NULL,
	[CurrencyOfIssueCode] [varchar](3) NULL,
	[CurrencyOfIssue] [varchar](50) NULL,
	[CountryOfOriginCode] [varchar](2) NULL,
	[CurrencyOfOrigin] [varchar](50) NULL,
	[EuroAmortizedQuantity] [varchar](50) NULL,
	[EuroIncomeEntitlementQuantity] [varchar](50) NULL,
	[EuroQuantity] [varchar](50) NULL,
	[FactorCurrent] float NULL,
	[FactorDateCurrent] [datetime] NULL,
	[IssueDate] [datetime] NULL,
	[MaturityDateEffective] [datetime] NULL,
	[PoolNumber] [varchar](50) NULL,
	[RedenominationIndicator] [varchar](50) NULL,
	[TotalMarketValueBase] float NULL,
	[TotalMarketValueLocal] float NULL,
	[AssetClassCodeLevel1] [int] NULL,
	[AssetClassCodeLevel2] [int] NULL,
	[AssetClassCodeLevel3] [int] NULL,
	[AssetClassCodeLevel4] [int] NULL,
	[AssetClassDescriptionLevel1] [varchar](50) NULL,
	[AssetClassDescriptionLevel2] [varchar](50) NULL,
	[AssetClassDescriptionLevel3] [varchar](50) NULL,
	[AssetClassDescriptionLevel4] [varchar](50) NULL,
	[AssetClassificationCategoryCode] [int] NULL,
	[AssetClassificationCategoryName] [varchar](50) NULL,
	[TradeDateSettlementDateIndicator] [varchar](1) NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerBNY]  WITH CHECK ADD  CONSTRAINT [FK_ManagerBNY_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerBNY] CHECK CONSTRAINT [FK_ManagerBNY_IntegrationImportRun]

