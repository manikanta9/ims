USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAccountingCurrency]    Script Date: 11/03/2010 10:21:09 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerMellonAccountingCurrency]') AND type in (N'U'))
DROP TABLE [source].[ManagerMellonAccountingCurrency]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonAccountingCurrency]    Script Date: 11/03/2010 10:21:09 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerMellonAccountingCurrency](
	[SourceAccountStatus] [varchar](100) NULL,
	[SheetName] [varchar](250) NULL,
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[SourceAccountNumber] [varchar](100) NULL,
	[SourceAccountName] [varchar](100) NULL,
	[AsOfDate] [datetime] NULL,
	[SecurityID] [varchar](100) NULL,
	[BaseCurrencyCode] [varchar](100) NULL,
	[CurrencyType] [varchar](100) NULL,
	[LocalCurrencyCode] [varchar](100) NULL,
	[LocalCurrency] [varchar](100) NULL,
	[LocalCost] float NULL,
	[BaseCost] float NULL,
	[LocalMarketValue] float NULL,
	[BaseMarketValue] float NULL,
	[UnrealizedGainLossBase] float NULL,
	[ReportID] [varchar](100) NULL,
	[ReportingAccountStatus] [varchar](100) NULL,
	[ConsolidatedExplode] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerMellonAccountingCurrency]  WITH CHECK ADD  CONSTRAINT [FK_ManagerMellonAccountingCurrency_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerMellonAccountingCurrency] CHECK CONSTRAINT [FK_ManagerMellonAccountingCurrency_IntegrationImportRun]

