USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerChasePendingTransactionsSummary]') AND type in (N'U'))
DROP TABLE [source].[ManagerChasePendingTransactionsSummary]


USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON



CREATE TABLE [source].[ManagerChasePendingTransactionsSummary](
	[AccountNumber] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL,
	[CashAccountCCYCode] [varchar](100) NULL,
	[CashAccountNumber] [varchar](100) NULL,
	[TransactionGroupDescription] [varchar](100) NULL,
	[TransactionStatusDescription] [varchar](100) NULL,
	[ProjectedDate] [datetime] NULL,
	[CashAccountName] [varchar](100) NULL,
	[TransactionCategoryDescription] [varchar](100) NULL,
	[TransactionTypeDescription] [varchar](100) NULL,
	[TransactionNumber] [varchar](100) NULL,
	[PayableDate] [varchar](100) NULL,
	[NetAmount] float NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerChasePendingTransactionsSummary]  WITH CHECK ADD  CONSTRAINT [FK_ManagerChasePendingTransactionsSummary_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerChasePendingTransactionsSummary] CHECK CONSTRAINT [FK_ManagerChasePendingTransactionsSummary_IntegrationImportRun]


