SELECT
'<data table="IntegrationManagerAssetClass">
	<value column="IntegrationManagerAssetClassGroupID">(SELECT IntegrationManagerAssetClassGroupID FROM IntegrationManagerAssetClassGroup WHERE AssetClassGroupName = ''' + imacg.AssetClassGroupName + ''')</value>
	<value column="AssetClass">' + imac.AssetClass + '</value>
	<value column="IntegrationManagerPositionTypeID">(SELECT IntegrationManagerPositionTypeID FROM IntegrationManagerPositionType WHERE PositionTypeName = ''' + impt.PositionTypeName + ''')</value>
</data>'
FROM IntegrationManagerAssetClass imac
	INNER JOIN IntegrationManagerAssetClassGroup imacg ON imacg.IntegrationManagerAssetClassGroupID = imac.IntegrationManagerAssetClassGroupID
	INNER JOIN IntegrationManagerPositionType impt ON impt.IntegrationManagerPositionTypeID = imac.IntegrationManagerPositionTypeID
WHERE imacg.AssetClassGroupName = 'Amalgamated Bank'
