USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerUnionBank_USBank]') AND type in (N'U'))
DROP TABLE [source].[ManagerUnionBank_USBank]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerUnionBank_USBank]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerUnionBank_USBank](
	  AsOfDate DATETIME
	, AccountID [varchar](100) NULL
	, AccountName [varchar](100) NULL
	, AssetCategoryName [varchar](100) NULL
	, AssetClassName [varchar](100) NULL
	, CUSIPId [varchar](100) NULL
	, TickerSymbol [varchar](100) NULL
	, SEDOL [varchar](100) NULL
	, AssetShortName [varchar](100) NULL
	, IssueDate DATETIME
	, MaturityDate DATETIME
	, AnnualPerUnitIncomeAmt [decimal](28,16) NULL
	, CurrentPrice [decimal](28,16) NULL
	, SharesPar [decimal](28,16) NULL
	, FederalTaxCostAmt [decimal](28,16) NULL
	, MarketValue [decimal](28,16) NULL
	, UnrealizedGainLossAmt [decimal](28,16) NULL
	, Yield [decimal](28,16) NULL
	, AvgUnitCost [decimal](28,16) NULL
	, EstAnnualIncomeAmt [decimal](28,16) NULL
	, IntegrationImportRunID INT
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerUnionBank_USBank]  WITH CHECK ADD  CONSTRAINT [FK_ManagerUnionBank_USBank_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerUnionBank_USBank] CHECK CONSTRAINT [FK_ManagerUnionBank_USBank_IntegrationImportRun]


