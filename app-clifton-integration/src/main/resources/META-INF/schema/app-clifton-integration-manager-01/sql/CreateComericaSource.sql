USE [CliftonIntegration]


/****** Object:  Table [source].[MellonIncomeReceivable]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerComerica]') AND type in (N'U'))
DROP TABLE [source].[ManagerComerica]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerComerica]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON



CREATE TABLE [source].[ManagerComerica](
	[IntegrationImportRunID] [int] NOT NULL,
	[AcctNum] [varchar](100) NULL,
	[AcctName] [varchar](100) NULL,
	[MarketValue] float NULL,
	[CashBalance] float NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF



USE [CliftonIntegration]

ALTER TABLE [source].[ManagerComerica]  WITH CHECK ADD  CONSTRAINT [FK_ManagerComerica_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerComerica] CHECK CONSTRAINT [FK_ManagerComerica_IntegrationImportRun]


