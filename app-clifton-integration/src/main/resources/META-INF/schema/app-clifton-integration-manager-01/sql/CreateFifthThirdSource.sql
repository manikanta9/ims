USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerFifthThird]    Script Date: 11/30/2010 10:50:59 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerFifthThird]') AND type in (N'U'))
DROP TABLE [source].[ManagerFifthThird]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerFifthThird]    Script Date: 11/30/2010 10:51:00 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerFifthThird](
	[IntegrationImportRunID] [int] NOT NULL,
	[AccountNumber] [varchar](50) NULL,
	[AccountName] [varchar](100) NULL,
	[AssetClassDescription] [varchar](50) NULL,
	[DescriptionLine1] [varchar](200) NULL,
	[MarketValue] float
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerFifthThird]  WITH CHECK ADD  CONSTRAINT [FK_ManagerFifthThird_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerFifthThird] CHECK CONSTRAINT [FK_ManagerFifthThird_IntegrationImportRun]

