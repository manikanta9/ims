USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonInvestmentDetail]    Script Date: 11/03/2010 10:23:08 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerMellonInvestmentDetail]') AND type in (N'U'))
DROP TABLE [source].[ManagerMellonInvestmentDetail]


USE [CliftonIntegration]


/****** Object:  Table [source].[ManagerMellonInvestmentDetail]    Script Date: 11/03/2010 10:23:08 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerMellonInvestmentDetail](
	[ReportingAccountNumber] [varchar](100) NULL,
	[ReportingAccountName] [varchar](100) NULL,
	[SourceAccountNumber] [varchar](100) NULL,
	[SourceAccountName] [varchar](100) NULL,
	[ReportingAccountBaseCurrency] [varchar](100) NULL,
	[ReportingAccountFXRate] float NULL,
	[SourceAccountBaseCurrency] [varchar](100) NULL,
	[SourceAccountFXRate] [varchar](100) NULL,
	[SourceAccountToReportingAccountFXRate] [varchar](100) NULL,
	[VendorRate] [varchar](100) NULL,
	[AsOfDate] [datetime] NULL,
	[SecurityID] [varchar](100) NULL,
	[SecurityCrossReferenceType] [varchar](100) NULL,
	[SecurityCrossReference] [varchar](100) NULL,
	[Description1] [varchar](100) NULL,
	[Description2] [varchar](100) NULL,
	[AssetCode] [varchar](100) NULL,
	[AssetType] [varchar](100) NULL,
	[SectorDescription] [varchar](100) NULL,
	[LocalCurrency] [varchar](100) NULL,
	[LocalCurrencyName] [varchar](100) NULL,
	[CountryCode] [varchar](100) NULL,
	[CountryName] [varchar](100) NULL,
	[Shares] float NULL,
	[BaseCost] float NULL,
	[LocalCost] float NULL,
	[BasePrice] float NULL,
	[LocalPrice] float NULL,
	[BaseMarketValue] float NULL,
	[LocalMarketValue] float NULL,
	[CouponRate] [varchar](100) NULL,
	[MaturityDate] [varchar](100) NULL,
	[BaseUnrealizedInvestmentGainLoss] float NULL,
	[LocalUnrealizedInvestmentGainLoss] float NULL,
	[BaseUnrealizedCurrencyGainLoss] float NULL,
	[ReportingAccountStatus] [varchar](100) NULL,
	[PositionType] [varchar](100) NULL,
	[ReportID] [varchar](100) NULL,
	[LocalBaseBoth] [varchar](100) NULL,
	[GLAccount] [varchar](100) NULL,
	[ConsolidateExplode] [varchar](100) NULL,
	[AlternateBaseCurrency] [varchar](100) NULL,
	[SourceAccountStatus] [varchar](100) NULL,
	[AssetMixCategoryCode] [varchar](100) NULL,
	[AssetMixCategoryDescription] [varchar](100) NULL,
	[SheetName] [varchar](250) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

USE [CliftonIntegration]

ALTER TABLE [source].[ManagerMellonInvestmentDetail]  WITH CHECK ADD  CONSTRAINT [FK_ManagerMellonInvestmentDetail_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[ManagerMellonInvestmentDetail] CHECK CONSTRAINT [FK_ManagerMellonInvestmentDetail_IntegrationImportRun]




