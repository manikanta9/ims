ALTER VIEW [dbo].[IntegrationManagerPosition]
AS
SELECT
	IntegrationManagerAssetClassGroupID,
	EffectiveDate,
	IntegrationManagerPositionBankID,
	SecurityValue = [SECURITY],
	AssetValue = [ASSET],
	LiabilityValue = [LIABILITY],
	LiabilityNegativeValue = [LIABILITY_NEGATIVE],
	TotalValue = [TOTAL] 
FROM
(SELECT
	IntegrationManagerPositionBankID, ManagerValue, PositionTypeName, EffectiveDate, mkg.IntegrationManagerAssetClassGroupID
FROM IntegrationManagerPositionHistory mph
	INNER JOIN IntegrationImportRun iir ON iir.IntegrationImportRunID = mph.IntegrationImportRunID
	INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	INNER JOIN IntegrationManagerPositionType mpt ON mpt.IntegrationManagerPositionTypeID = mph.IntegrationManagerPositionTypeID
	INNER JOIN IntegrationManagerAssetClassGroupImportDefinition dmkg ON dmkg.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
	INNER JOIN IntegrationManagerAssetClassGroup mkg ON mkg.IntegrationManagerAssetClassGroupID = dmkg.IntegrationManagerAssetClassGroupID) p
PIVOT (
	SUM(ManagerValue)
	FOR PositionTypeName IN ([ASSET], [LIABILITY],[LIABILITY_NEGATIVE], [SECURITY], [TOTAL])
) p2
