USE [CliftonIntegration]
GO

/****** Object:  Table [source].[ManagerKeyBank]    Script Date: 01/14/2011 16:36:53 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[ManagerKeyBank]') AND type in (N'U'))
DROP TABLE [source].[ManagerKeyBank]
GO

USE [CliftonIntegration]
GO

/****** Object:  Table [source].[ManagerKeyBank]    Script Date: 01/14/2011 16:36:53 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[ManagerKeyBank](
	[ManagerKeyBankID] int IDENTITY(1,1)NOT NULL,
	[IntegrationImportRunID] [int] NULL,
	[AsOfDate] [datetime] NULL,
	[AccountNumber] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[PortfolioNumber] [varchar](100) NULL,
	[PortfolioName] [varchar](100) NULL,
	[PortfolioType] [varchar](100) NULL,
	[AcctBaseCurrencyName] [varchar](100) NULL,
	[AcctBaseCurrencyCode] [varchar](100) NULL,
	[Ticker] [varchar](100) NULL,
	[SecurityID] [varchar](100) NULL,
	[Shares] [float] NULL,
	[AssetTitle] [varchar](100) NULL,
	[BasePrice] [float] NULL,
	[BaseMarketValue] [float] NULL,
	[PercentOfTotalHoldings] [float] NULL,
	[BaseBookValue] [float] NULL,
	[BaseCostFed] [float] NULL,
	[BaseAverageCost] [float] NULL,
	[BaseUnrealizedGLBook] [float] NULL,
	[BaseUnrealizedGLFed] [float] NULL,
	[AnnualIncomeRate] [float] NULL,
	[BaseEstimatedAnnualIncome] [float] NULL,
	[BaseAccruedIncome] [float] NULL,
	[LocalCountryName] [varchar](100) NULL,
	[LocalCountryCode] [varchar](100) NULL,
	[LocalPrice] [float] NULL,
	[LastPriceDate] [varchar](100) NULL,
	[LocalCurrencyName] [varchar](100) NULL,
	[LocalCurrencyCode] [varchar](100) NULL,
	[LocalMarketValue] [float] NULL,
	[LocalCost] [float] NULL,
	[LocalUnrealizedGL] [float] NULL,
	[AssetType] [varchar](100) NULL,
	[MajorAssetType] [varchar](100) NULL,
	[MinorAssetType] [varchar](100) NULL,
	[IndustryMajor] [varchar](100) NULL,
	[IndustryMinor] [varchar](100) NULL,
	[PriceSourceCode] [varchar](100) NULL,
	[PriceSourceDescription] [varchar](100) NULL,
	[EarningsPerShare] [float] NULL,
	[PERatio] [float] NULL,
	[CouponRate] [float] NULL,
	[BookYieldValue] [float] NULL,
	[YieldOnMarket] [float] NULL,
	[YieldToMaturity] [float] NULL,
	[Duration] [float] NULL,
	[NextPaymentDate] [varchar](100) NULL,
	[MaturityDate] [varchar](100) NULL,
	[CallDate] [varchar](100) NULL,
	[LastPurchasedDate] [varchar](100) NULL,
	[OriginalFaceAmount] [float] NULL,
	[IssuerID] [varchar](100) NULL,
	[StateofIssuer] [varchar](100) NULL,
	[ExchangeRate] [float] NULL,
	[OriginalStrikePrice] [float] NULL,
	[CurrencyStrikePrice] [float] NULL,
	[MoodysRating] [varchar](100) NULL,
	[FairValueDefaultLevel] [varchar](100) NULL,
	[FairValueOverride] [varchar](100) NULL,
	[ISIN] [varchar](100) NULL,
	[Sedol] [varchar](100) NULL,
	[UnderlyingSecID] [varchar](100) NULL,
	[YieldCurrent] [float] NULL,
	[ReportRunDate] [datetime] NULL,
	CONSTRAINT PK_ManagerKeyBank PRIMARY KEY CLUSTERED (ManagerKeyBankID)
) ON [PRIMARY]



SET ANSI_PADDING OFF



ALTER TABLE [source].[ManagerKeyBank]  WITH CHECK ADD  CONSTRAINT [FK_ManagerKeyBank_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[ManagerKeyBank] CHECK CONSTRAINT [FK_ManagerKeyBank_IntegrationImportRun]

CREATE INDEX [ix_ManagerKeyBank_IntegrationImportRunID] ON source.ManagerKeyBank (IntegrationImportRunID)


