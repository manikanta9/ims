ALTER VIEW [dbo].[IntegrationManagerPosition]
AS
SELECT
	IntegrationManagerAssetClassGroupID,
	EffectiveDate,
	IntegrationManagerPositionBankID,
	SecurityValue = [SECURITY],
	AssetValue = ISNULL([ASSET],0) + ISNULL([SYSTEM_DEFINED_ASSET],0),
	LiabilityValue = [LIABILITY],
	LiabilityNegativeValue = [LIABILITY_NEGATIVE],
	TotalValue = [TOTAL] + ISNULL([TOTAL_ASSET_ADJUSTMENT],0) + ISNULL([TOTAL_NEGATIVE],0)
FROM
(SELECT
	IntegrationManagerPositionBankID, ManagerValue, PositionTypeName, EffectiveDate, mkg.IntegrationManagerAssetClassGroupID
FROM IntegrationManagerPositionHistory mph
	INNER JOIN IntegrationImportRun iir ON iir.IntegrationImportRunID = mph.IntegrationImportRunID
	INNER JOIN IntegrationImportDefinition iid ON iid.IntegrationImportDefinitionID = iir.IntegrationImportDefinitionID
	INNER JOIN IntegrationManagerPositionType mpt ON mpt.IntegrationManagerPositionTypeID = mph.IntegrationManagerPositionTypeID
	INNER JOIN IntegrationManagerAssetClassGroupImportDefinition dmkg ON dmkg.IntegrationImportDefinitionID = iid.IntegrationImportDefinitionID
	INNER JOIN IntegrationManagerAssetClassGroup mkg ON mkg.IntegrationManagerAssetClassGroupID = dmkg.IntegrationManagerAssetClassGroupID) p
PIVOT (
	SUM(ManagerValue)
	FOR PositionTypeName IN ([ASSET], [SYSTEM_DEFINED_ASSET], [LIABILITY],[LIABILITY_NEGATIVE], [SECURITY], [TOTAL], [TOTAL_NEGATIVE], [TOTAL_ASSET_ADJUSTMENT])
) p2
