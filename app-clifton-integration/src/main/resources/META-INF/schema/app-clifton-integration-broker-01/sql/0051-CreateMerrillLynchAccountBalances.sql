
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerMerrillLynchAccountBalances') AND type in (N'U'))
	DROP TABLE source.BrokerMerrillLynchAccountBalances
	
CREATE TABLE source.BrokerMerrillLynchAccountBalances (
	BrokerMerrillLynchAccountBalancesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	Firm varchar(100) NULL,
	Office varchar(100) NULL,
	Account varchar(100) NULL,
	AccountType varchar(100) NULL,
	PreviousAccountBalance decimal(28,16) NULL,
	AccountBalance decimal(28,16) NULL,
	OpenTradeEquity decimal(28,16) NULL,
	TotalEquity decimal(28,16) NULL,
	SpanAdjEquity decimal(28,16) NULL,
	AccountValueAtMarket decimal(28,16) NULL,
	FutInitialMarginRequirement decimal(28,16) NULL,
	EqtInitialMarginRequirement decimal(28,16) NULL,
	TotalAccountRequirements decimal(28,16) NULL,
	AccountBaseCurrency varchar(100) NULL,
	ConversionRate decimal(28,16) NULL,
	ConvertedAccountValue decimal(28,16) NULL,
	Comissions decimal(28,16) NULL,
	TotalFees decimal(28,16) NULL,
	OptionPremium decimal(28,16) NULL,
	GrossProfitOrLoss decimal(28,16) NULL,
	NetProfitLossFromTrades decimal(28,16) NULL,
	NetMemoOptionPremium decimal(28,16) NULL,
	TotalCash decimal(28,16) NULL,
	Currency varchar(100) NULL,
	NetOptionValue decimal(28,16) NULL,
	LongOptionValue decimal(28,16) NULL,
	ShortOptionValue decimal(28,16) NULL,
	MaintenanceMarginRequirement decimal(28,16) NULL,
	MarginExcessDeficit decimal(28,16) NULL,
	LegalEntityName varchar(100) NULL,
	RunDate datetime null,
	CONSTRAINT PK_BrokerMerrillLynchAccountBalances PRIMARY KEY CLUSTERED (BrokerMerrillLynchAccountBalancesID),
	CONSTRAINT FK_BrokerMerrillLynchAccountBalances_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
