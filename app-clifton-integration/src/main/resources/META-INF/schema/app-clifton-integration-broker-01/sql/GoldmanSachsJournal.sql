USE [CliftonIntegration]


IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsJournal]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsJournal]

USE [CliftonIntegration]

/****** Object:  Table [source].[BrokerGoldmanSachsJournal]    Script Date: 05/18/2011 16:48:01 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[BrokerGoldmanSachsJournal](
	[IntegrationImportRunID] [int] NOT NULL,
	[COBDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[AcctName] [varchar](100) NULL,
	[Curr] [varchar](100) NULL,
	[CashAmt] decimal(28,16) NULL,
	[Description] [varchar](100) NULL,
	[AnotherDate] [datetime] NULL,
	[AccountType] [int] NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[BrokerGoldmanSachsJournal]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsJournal_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[BrokerGoldmanSachsJournal] CHECK CONSTRAINT [FK_BrokerGoldmanSachsJournal_IntegrationImportRun]
