
CREATE PROCEDURE [dbo].[LoadReconcileM2MDaily]
	@IntegrationImportRunID INT
AS
BEGIN
	DECLARE @M2MLocalAccountList TABLE(InvestmentAccountID INT,  AccountNumber NVARCHAR(50))

	INSERT INTO @M2MLocalAccountList(InvestmentAccountID, AccountNumber)
	SELECT
		x.InvestmentAccountID,
		ia.AccountNumber
	FROM
	(
		SELECT
			InvestmentAccountID,
			CONVERT(BIT,[M2M in Local Currency]) AS IsM2MInLocalCurrency
		FROM
		(
			SELECT
				scv.FKFieldID as InvestmentAccountID,
				sc.ColumnName,
				scv.ColumnValue
			FROM CliftonIMS.dbo.SystemColumnValue scv
				INNER JOIN CliftonIMS.dbo.SystemColumn sc ON sc.SystemColumnID = scv.SystemColumnID
				INNER JOIN CliftonIMS.dbo.SystemColumnGroup scg ON scg.SystemColumnGroupID = sc.SystemColumnGroupID
			WHERE scg.ColumnGroupName = 'Holding Account Fields'
		) as x
		PIVOT ( MAX(ColumnValue) FOR ColumnName IN ([M2M in Local Currency])) as t
	)x
		INNER JOIN CliftonIMS.dbo.InvestmentAccount ia ON ia.InvestmentAccountID = x.InvestmentAccountID
	WHERE IsM2MInLocalCurrency = 1

	SELECT
		uuid = NEWID(),
		IntegrationImportRunID,
		AccountNumber,
		PositionDate,
		SUM(ExpectedTransferAmount) as ExpectedTransferAmount,
		CurrencySymbolLocal,
		CurrencySymbolBase
	FROM
	(
		SELECT [IntegrationReconcileM2MDailyID]
			  ,[IntegrationImportRunID]
			  ,r.[AccountNumber]
			  ,[PositionDate]
			  ,[ExpectedTransferAmount]
			  ,CASE WHEN m.AccountNumber IS NULL THEN NULL ELSE [CurrencySymbolLocal] END AS [CurrencySymbolLocal]
			  ,CASE WHEN m.AccountNumber IS NULL THEN NULL ELSE [CurrencySymbolBase] END AS [CurrencySymbolBase]
		FROM CliftonIntegration.dbo.IntegrationReconcileM2MDaily r
			LEFT JOIN @M2MLocalAccountList m ON r.AccountNumber = m.AccountNumber
		WHERE IntegrationImportRunID = @IntegrationImportRunID
	) loadResults
	GROUP BY IntegrationImportRunID, AccountNumber, PositionDate, CurrencySymbolLocal, CurrencySymbolBase
END
