IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerCitigroupExchangeRates') AND type in (N'U'))
	DROP TABLE source.BrokerCitigroupExchangeRates

CREATE TABLE source.BrokerCitigroupExchangeRates (
	BrokerCitigroupExchangeRatesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	SpotRate decimal(28,16) NULL,
	Curr varchar(100) NULL,
	CurrName varchar(100) NULL,
	FXRatesDate datetime NULL,
	CONSTRAINT PK_BrokerCitigroupExchangeRates PRIMARY KEY CLUSTERED (BrokerCitigroupExchangeRatesID),
	CONSTRAINT FK_BrokerCitigroupExchangeRates_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
