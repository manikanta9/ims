USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsSecuritiesOnDeposit]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsSecuritiesOnDeposit]

USE [CliftonIntegration]

/****** Object:  Table [source].[BrokerGoldmanSachsSecuritiesOnDeposit]    Script Date: 05/18/2011 17:24:19 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[BrokerGoldmanSachsSecuritiesOnDeposit](
	[IntegrationImportRunID] [int] NOT NULL,
	[BusinessDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[AcctName] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[SegNonSeg] [varchar](100) NULL,
	[CUSIP] [int] NULL,
	[Description] [varchar](100) NULL,
	[FaceValue] [int] NULL,
	[CollateralValue] decimal(28,16) NULL,
	[InitialMargin] [varchar](100) NULL,
	[CollateralExcessDeficit] [varchar](100) NULL,
	[FaceDeficit] [varchar](100) NULL,
	[SharesDeficit] [varchar](100) NULL,
	[MaturityDate] [datetime] NULL,
	[InceptionDate] [datetime] NULL,
	[AccountType] [varchar](100) NULL,
	[CollateralPrice] decimal(28,16) NULL,
	[EquityAccount] [int] NULL,
	[HaircutFrom] [varchar](100) NULL,
	[Haircut] decimal(28,16) NULL,
	[ISIN] [varchar](100) NULL,
	[InterestRate] decimal(28,16) NULL,
	[MarketPrice] decimal(28,16) NULL,
	[MarketValue] decimal(28,16) NULL,
	[NoofDaystoMaturity] [int] NULL,
	[ProductID] [int] NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[BrokerGoldmanSachsSecuritiesOnDeposit]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsSecuritiesOnDeposit_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[BrokerGoldmanSachsSecuritiesOnDeposit] CHECK CONSTRAINT [FK_BrokerGoldmanSachsSecuritiesOnDeposit_IntegrationImportRun]



