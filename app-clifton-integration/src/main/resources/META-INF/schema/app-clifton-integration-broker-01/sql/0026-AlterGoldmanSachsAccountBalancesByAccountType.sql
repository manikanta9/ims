ALTER TABLE [source].[BrokerGoldmanSachsAccountBalancesByAccountType]
ADD 
	[InitialMarginRequirementLocal] decimal(28,16) NULL,
	[InitialMarginRequirementBase] decimal(28,16) NULL,
	[SecurityOnDepositLocal] decimal(28,16) NULL,
	[SecurityOnDepositBase] decimal(28,16) NULL;
	
IF EXISTS (SELECT * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IntegrationReconcileM2MDaily')
BEGIN
	EXEC sp_rename 'IntegrationReconcileM2MDaily', 'IntegrationReconcileAccountBalancesByAccountType'

	EXEC sp_rename 'IntegrationReconcileAccountBalancesByAccountType.IntegrationReconcileM2MDailyID', 'IntegrationReconcileAccountBalancesByAccountTypeID', 'COLUMN'

	ALTER TABLE [dbo].[IntegrationReconcileAccountBalancesByAccountType]
	ADD 
		[CollateralRequirementLocal] [decimal](19,2) NULL,
		[CollateralRequirementBase] [decimal](19,2) NULL,
		[CollateralAmountLocal] [decimal](19,2) NULL,
		[CollateralAmountBase] [decimal](19,2) NULL;
		

END
