USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsIntradayTrades]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsIntradayTrades]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsIntradayTrades]

CREATE TABLE [source].[BrokerGoldmanSachsIntradayTrades](
	[BrokerGoldmanSachsIntradayTradesID] [int] IDENTITY(1,1) NOT NULL,
	[UniqueTradeId] [varchar](100) NULL,
	[BusinessDate] [datetime] NULL,
	[GSAccountNumber] [varchar](100) NULL,
	[AccountBaseCurrency] [varchar](100) NULL,
	[ClientAccountNumber] [varchar](100) NULL,
	[ClientAccountName] [varchar](100) NULL,
	[ContractCode] [varchar](100) NULL,
	[ClientContractCode] [varchar](100) NULL,
	[ContractDescription] [varchar](100) NULL,
	[ProductDescription] [varchar](100) NULL,
	[ContractTypeIndicator] [varchar](100) NULL,
	[Month] [int] NULL,
	[Year] [int] NULL,
	[PromptDay] decimal(28,16) NULL,
	[PutCallIndicator] [varchar](100) NULL,
	[StrikePrice] decimal(28,16) NULL,
	[BuySellIndicator] [varchar](100) NULL,
	[Quantity] decimal(28,16) NULL,
	[ContractMultiplier] decimal(28,16) NULL,
	[TradePrice] decimal(28,16) NULL,
	[TradePriceDisplay] decimal(28,16) NULL,
	[RoundedAveragePrice] decimal(28,16) NULL,
	[AveragePriceResidual] decimal(28,16) NULL,
	[ContractCurrency] [varchar](100) NULL,
	[TradeDate]  [datetime] NULL,
	[LastTradingDate]  [datetime] NULL,
	[ExecutionMethod] [varchar](100) NULL,
	[TradeType] [varchar](100) NULL,
	[ExchangeCode] [varchar](50) NULL,
	[CommodityCode] [varchar](100) NULL,
	[ExchangeNotation] [varchar](100) NULL,
	[ExecutingBroker] [varchar](100) NULL,
	[ClearingBroker] [varchar](100) NULL,
	[MatchStatus] [varchar](100) NULL,
	[DeltaIndicator] [varchar](100) NULL,
	[Filler1] [varchar](100) NULL,
	[Filler2] [varchar](100) NULL,
	[Filler3] [varchar](100) NULL,
	[Filler4] [varchar](100) NULL,
	[Filler5] [varchar](100) NULL,
	[Filler6] [varchar](100) NULL,
	[Filler7] [varchar](100) NULL,
	[Filler8] [varchar](100) NULL,
	[Filler9] [varchar](100) NULL,
	[Filler10] [varchar](100) NULL,
	[Filler11] [varchar](100) NULL,
	[Filler12] [varchar](100) NULL,
	[Filler13] [varchar](100) NULL,
	[Filler14] [varchar](100) NULL,
	[Filler15] [varchar](100) NULL,
	[Filler16] [varchar](100) NULL,
	[Filler17] [varchar](100) NULL,
	[Filler18] [varchar](100) NULL,
	[Filler19] [varchar](100) NULL,
	[Filler20] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
);
ALTER TABLE [source].[BrokerGoldmanSachsIntradayTrades] ADD CONSTRAINT [PK_BrokerGoldmanSachsIntradayTrades]
	PRIMARY KEY CLUSTERED (BrokerGoldmanSachsIntradayTradesID);
   

ALTER TABLE [source].[BrokerGoldmanSachsIntradayTrades]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsIntradayTrades] CHECK CONSTRAINT [FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun]

CREATE INDEX [ix_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun] ON [source].[BrokerGoldmanSachsIntradayTrades] (IntegrationImportRunID);



