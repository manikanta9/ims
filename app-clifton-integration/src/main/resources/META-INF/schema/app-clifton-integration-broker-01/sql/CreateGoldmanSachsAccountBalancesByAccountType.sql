USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsAccountBalancesByAccountType]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsAccountBalancesByAccountType]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsAccountBalancesByAccountType]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsAccountBalancesByAccountType]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON
CREATE TABLE source.BrokerGoldmanSachsAccountBalancesByAccountType
(
  COBDate VARCHAR(100)
, GSAccountNumber VARCHAR(100)
, TotalEquityBase FLOAT
, TotalEquityLocal FLOAT
, IntegrationImportRunID INT NOT NULL
);

SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerGoldmanSachsAccountBalancesByAccountType]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsAccountBalancesByAccountType_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsAccountBalancesByAccountType] CHECK CONSTRAINT [FK_BrokerGoldmanSachsAccountBalancesByAccountType_IntegrationImportRun]
