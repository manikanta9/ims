USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsOTCAccountBalances]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsOTCAccountBalances]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsOTCAccountBalances]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsOTCAccountBalances]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON
CREATE TABLE source.BrokerGoldmanSachsOTCAccountBalances
(
  BrokerGoldmanSachsOTCAccountBalancesID INT IDENTITY(1,1) NOT NULL
, GSAccountName VARCHAR(100)
, ClientAccount VARCHAR(100)
, CCP VARCHAR(100)
, Instrument VARCHAR(100)
, Currency VARCHAR(100)
, OpeningBalanceLocal DECIMAL(28, 16)
, EndingBalanceLocal DECIMAL(28, 16)
, NPVLocal DECIMAL(28, 16)
, PAILocal DECIMAL(28, 16)
, IMCollateralBalanceLocal DECIMAL(28, 16)
, AccountValueLocal DECIMAL(28, 16)
, AccountValueIMCollateralLocal DECIMAL(28, 16)
, InitialMarginRequirementLocal DECIMAL(28, 16)
, ExcessDeficitLocal DECIMAL(28, 16)
, IMExcessDeficitBase DECIMAL(28, 16)
, VMExcessDeficitLocal DECIMAL(28, 16)
, AccountValueBase DECIMAL(28, 16)
, COBDate VARCHAR(100)
, GSAccountNumber VARCHAR(100)
, InitialMarginRequirementBase DECIMAL(28, 16)
, IMCollateralBalanceBase DECIMAL(28, 16)
, IntegrationImportRunID INT NOT NULL
);
ALTER TABLE [source].[BrokerGoldmanSachsOTCAccountBalances] ADD CONSTRAINT [PK_BrokerGoldmanSachsOTCAccountBalances]
	PRIMARY KEY CLUSTERED (BrokerGoldmanSachsOTCAccountBalancesID);

SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerGoldmanSachsOTCAccountBalances]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsOTCAccountBalances_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsOTCAccountBalances] CHECK CONSTRAINT [FK_BrokerGoldmanSachsOTCAccountBalances_IntegrationImportRun]
