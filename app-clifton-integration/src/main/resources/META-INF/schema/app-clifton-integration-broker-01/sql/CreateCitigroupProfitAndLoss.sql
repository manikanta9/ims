USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupProfitAndLoss]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerCitigroupProfitAndLoss]') AND type in (N'U'))
DROP TABLE [source].[BrokerCitigroupProfitAndLoss]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupProfitAndLoss]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON
CREATE TABLE source.BrokerCitigroupProfitAndLoss
(
	[COBDate] [datetime] NULL,
	[COB1Date] [datetime] NULL,
	[ACRef] [varchar](100) NULL,
	[ACName] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[Exchange] [varchar](100) NULL,
	[Contract] [varchar](100) NULL,
	[Delivery] [varchar](100) NULL,
	[Type] [varchar](100) NULL,
	[PutCall] [varchar](100) NULL,
	[Strike] [varchar](100) NULL,
	[RIC] [varchar](100) NULL,
	[Bloomberg] [varchar](100) NULL,
	[Long] decimal(28,16) NULL,
	[Short] decimal(28,16) NULL,
	[Net] decimal(28,16) NULL,
	[PreviousSettPrice] decimal(28,16) NULL,
	[SettPrice] decimal(28,16) NULL,
	[OTEMovement] decimal(28,16) NULL,
	[RealizedPandL] decimal(28,16) NULL,
	[OptionPremium] decimal(28,16) NULL,
	[GrossPandLMovement] decimal(28,16) NULL,
	[TotalFees] decimal(28,16) NULL,
	[NetPandL] decimal(28,16) NULL,
	[OptionValuationMovement] decimal(28,16) NULL,
	[LotsMovement] decimal(28,16) NULL,
	[IntegrationImportRunID] [int] NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerCitigroupProfitAndLoss]  WITH CHECK ADD  CONSTRAINT [FK_BrokerCitigroupProfitAndLoss_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerCitigroupProfitAndLoss] CHECK CONSTRAINT [FK_BrokerCitigroupProfitAndLoss_IntegrationImportRun]


