IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerJPMorganExchangeRates') AND type in (N'U'))
	DROP TABLE source.BrokerJPMorganExchangeRates

CREATE TABLE source.BrokerJPMorganExchangeRates (
	BrokerJPMorganExchangeRatesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	Rate decimal(28,16) NULL,
	FromCurrency varchar(100) NULL,
	ToCurrency varchar(100) NULL,
	RateDate datetime NULL,
	CONSTRAINT PK_BrokerJPMorganExchangeRates PRIMARY KEY CLUSTERED (BrokerJPMorganExchangeRatesID),
	CONSTRAINT FK_BrokerJPMorganExchangeRates_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
