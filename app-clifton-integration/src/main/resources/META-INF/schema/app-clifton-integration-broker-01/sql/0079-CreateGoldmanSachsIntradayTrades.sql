USE [CliftonIntegration];

IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE COLUMN_NAME = 'PromptDay' AND TABLE_NAME = 'BrokerGoldmanSachsIntradayTrades' AND TABLE_SCHEMA = 'SOURCE')
BEGIN
	EXEC SP_RENAME '[source].[BrokerGoldmanSachsIntradayTrades]','BrokerGoldmanSachsIntradayTrades_OLD'
	
	DROP INDEX [ix_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun] ON source.BrokerGoldmanSachsIntradayTrades_OLD
	ALTER TABLE source.BrokerGoldmanSachsIntradayTrades_OLD DROP CONSTRAINT PK_BrokerGoldmanSachsIntradayTrades
	
	IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.CONSTRAINT_TABLE_USAGE WHERE CONSTRAINT_NAME = 'FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun')
	BEGIN
		ALTER TABLE source.BrokerGoldmanSachsIntradayTrades_OLD DROP CONSTRAINT FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun
	END
	
	ALTER TABLE source.BrokerGoldmanSachsIntradayTrades_OLD  ADD CONSTRAINT [PK_BrokerGoldmanSachsIntradayTrades_OLD] PRIMARY KEY CLUSTERED (BrokerGoldmanSachsIntradayTradesID)
	
	ALTER TABLE source.BrokerGoldmanSachsIntradayTrades_OLD  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun_OLD] FOREIGN KEY([IntegrationImportRunID])
	REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

	CREATE INDEX [ix_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun_OLD] ON [source].[BrokerGoldmanSachsIntradayTrades_OLD] (IntegrationImportRunID);
END



/****** Object:  Table [source].[BrokerGoldmanSachsIntradayTrades]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsIntradayTrades]') AND type in (N'U'))
	DROP TABLE [source].[BrokerGoldmanSachsIntradayTrades]


CREATE TABLE [source].[BrokerGoldmanSachsIntradayTrades](
	[BrokerGoldmanSachsIntradayTradesID] [int] IDENTITY(1,1) NOT NULL
	, ActivityDate DATETIME null
	, ClearingMatchStatus VARCHAR(100) NULL
	, TradeDate DATETIME null
	, TradeType VARCHAR(100) NULL
	, Account VARCHAR(100) NULL
	, AccountName VARCHAR(500) NULL
	, BS VARCHAR(100) NULL
	, Qty decimal(28,16) NULL
	, ExchangeShortName VARCHAR(100) NULL
	, GSProductDescription VARCHAR(100) NULL
	, "MONTH" VARCHAR(100) NULL
	, "YEAR" VARCHAR(100) NULL
	, PC VARCHAR(100) NULL
	, Strike VARCHAR(100) NULL
	, TradePrice decimal(28,16) NULL
	, Currency VARCHAR(100) NULL
	, ExecutingBroker VARCHAR(100) NULL
	, BloombergCode VARCHAR(100) NULL
	, APSFlag VARCHAR(100) NULL
	, APSResidualAmount VARCHAR(100) NULL
	, AccountBaseCurrency VARCHAR(100) NULL
	, AccruedClearingCommissionBase decimal(28,16) NULL
	, AccruedClearingCommissionLocal decimal(28,16) NULL
	, AccruedExecutionCommissionBase decimal(28,16) NULL
	, AccruedExecutionCommissionLocal decimal(28,16) NULL
	, AccruedTotalFeesBase decimal(28,16) NULL
	, AccruedTotalFeesLocal decimal(28,16) NULL
	, AccruedClearingExecutionCommissionBase decimal(28,16) NULL
	, AccruedClearingAndExecutionCommissionLocal decimal(28,16) NULL
	, Age VARCHAR(100) NULL
	, AllocationSource VARCHAR(100) NULL
	, BloombergUniqueCode VARCHAR(100) NULL
	, ClearingAndExecutionCommissionBase decimal(28,16) NULL
	, ClearingAndExecutionCommissionLocal decimal(28,16) NULL
	, ClearingAndExecutionCommissionRate decimal(28,16) NULL
	, ClearingBroker VARCHAR(100) NULL
	, ClearingPrice decimal(28,16) NULL
	, ClearingTime VARCHAR(100) NULL
	, ClearingCommissionBase decimal(28,16) NULL
	, ClearingCommissionLocal decimal(28,16) NULL
	, ClearingCommissionRate decimal(28,16) NULL
	, ClearingFeeBase decimal(28,16) NULL
	, ClearingFeeLocal decimal(28,16) NULL
	, ClearingFeeCurrency VARCHAR(100) NULL
	, ClientAccountNumber VARCHAR(100) NULL
	, ClientAccountMapped VARCHAR(100) NULL
	, ClientAllocationID VARCHAR(100) NULL
	, ClientCreatorID VARCHAR(100) NULL
	, ClientOrderNumber VARCHAR(100) NULL
	, ClientReference4 VARCHAR(100) NULL
	, ClientReference5 VARCHAR(100) NULL
	, ClientReferenceID VARCHAR(100) NULL
	, ClosingFillQty decimal(28,16) NULL
	, ConsumptionTaxBase decimal(28,16) NULL
	, ConsumptionTaxLocal decimal(28,16) NULL
	, ConsumptionTaxCurrency VARCHAR(100) NULL
	, ContractKey VARCHAR(100) NULL
	, ContractDay VARCHAR(100) NULL
	, ContractMultiplier decimal(28,16) NULL
	, CreatedTime VARCHAR(100) NULL
	, CustomPrice decimal(28,16) NULL
	, DeltaIndicator VARCHAR(100) NULL
	, ExchangeElectronicPlatform VARCHAR(100) NULL
	, ExchangeSymbol VARCHAR(100) NULL
	, ExchangeFeeBase decimal(28,16) NULL
	, ExchangeFeeLocal decimal(28,16) NULL
	, ExchangeFeeCurrency VARCHAR(100) NULL
	, ExecutionCommissionBase decimal(28,16) NULL
	, ExecutionCommissionLocal decimal(28,16) NULL
	, ExecutionCommissionRate decimal(28,16) NULL
	, FXRateToAccountBaseCurrency decimal(28,16) NULL
	, FlexibleCode VARCHAR(100) NULL
	, FractionalTradePrice VARCHAR(100) NULL
	, GMIExchangeCode VARCHAR(100) NULL
	, GMIProductCode VARCHAR(100) NULL
	, GiveInGiveOut VARCHAR(100) NULL
	, ISIN VARCHAR(100) NULL
	, KerberosID VARCHAR(100) NULL
	, LatestSettlementPrice decimal(28,16) NULL
	, ModifiedTime VARCHAR(100) NULL
	, NFAFeesBase decimal(28,16) NULL
	, NFAFeesLocal decimal(28,16) NULL
	, NFAFeesCurrency VARCHAR(100) NULL
	, NotionalValueOrOptPrem decimal(28,16) NULL
	, OpenFillQty decimal(28,16) NULL
	, OpenCloseIndicator VARCHAR(100) NULL
	, OrderTrade VARCHAR(100) NULL
	, OrderChannel VARCHAR(100) NULL
	, OtherFeesBase decimal(28,16) NULL
	, OtherFeesLocal decimal(28,16) NULL
	, OtherFeesCurrency VARCHAR(100) NULL
	, PriorTradePrice decimal(28,16) NULL
	, ProductID VARCHAR(100) NULL
	, ProductType VARCHAR(100) NULL
	, RateBS VARCHAR(100) NULL
	, ReutersCode VARCHAR(100) NULL
	, RoundedAveragePrice decimal(28,16) NULL
	, SECFeeBase decimal(28,16) NULL
	, SECFeeLocal decimal(28,16) NULL
	, SecFeeCurrency VARCHAR(100) NULL
	, "SESSION" VARCHAR(100) NULL
	, SettlementDate DATETIME NULL
	, StrategyCode VARCHAR(100) NULL
	, TAS VARCHAR(100) NULL
	, TotalFeesBase decimal(28,16) NULL
	, TotalFeesLocal decimal(28,16) NULL
	, TradeTypeCode VARCHAR(100) NULL
	, TradedRate decimal(28,16) NULL
	, UniqueTradeId VARCHAR(100) COLLATE Latin1_General_CS_AS NOT NULL
	, [IntegrationImportRunID] [int] NOT NULL
);
ALTER TABLE [source].[BrokerGoldmanSachsIntradayTrades] ADD CONSTRAINT [PK_BrokerGoldmanSachsIntradayTrades]
	PRIMARY KEY CLUSTERED (BrokerGoldmanSachsIntradayTradesID);
   

ALTER TABLE [source].[BrokerGoldmanSachsIntradayTrades]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsIntradayTrades] CHECK CONSTRAINT [FK_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun]

CREATE INDEX [ix_BrokerGoldmanSachsIntradayTrades_IntegrationImportRun] ON [source].[BrokerGoldmanSachsIntradayTrades] (IntegrationImportRunID);



