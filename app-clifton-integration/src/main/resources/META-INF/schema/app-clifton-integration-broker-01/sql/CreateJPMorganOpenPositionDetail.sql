USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerJPMorganOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerJPMorganOpenPositionDetail]') AND type in (N'U'))
DROP TABLE [source].[BrokerJPMorganOpenPositionDetail]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerJPMorganOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerJPMorganOpenPositionDetail](
	[BrokerJPMorganOpenPositionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[AcountNumber] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[CustomAccount] [varchar](100) NULL,
	[TraceId] [varchar](100) NULL,
	[PositionDate] [datetime] NULL,
	[TradeDate] [datetime] NULL,
	[TradeSettlementDate] [datetime] NULL,
	[BBGCode] [varchar](100) NULL,
	[SymbolType] [varchar](100) NULL,
	[ProductCurr] [varchar](100) NULL,
	[AccountCurrency] [varchar](100) NULL,
	[FXRate] decimal(28,16) NULL,
	[Quantity] decimal(28,16) NULL,
	[BuySell] [varchar](100) NULL,
	[TradePrice] decimal(28,16) NULL,
	[Settlement] decimal(28,16) NULL,
	[NotionalFutures] decimal(28,16) NULL,
	[NotionalFuturesByFXrate] decimal(28,16) NULL,
	[MarketValue] decimal(28,16) NULL,
	[MarketValueBase] decimal(28,16) NULL,
	[OptionValue] decimal(28,16) NULL,
	[OTE] decimal(28,16) NULL,
	[OTEBase] decimal(28,16) NULL,
	[IntegrationImportRunID] [int] NOT NULL
 CONSTRAINT [PK_BrokerJPMorganOpenPositionDetail] PRIMARY KEY CLUSTERED 
(
	[BrokerJPMorganOpenPositionDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerJPMorganOpenPositionDetail]  WITH CHECK ADD  CONSTRAINT [FK_BrokerJPMorganOpenPositionDetail_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerJPMorganOpenPositionDetail] CHECK CONSTRAINT [FK_BrokerJPMorganOpenPositionDetail_IntegrationImportRun]


