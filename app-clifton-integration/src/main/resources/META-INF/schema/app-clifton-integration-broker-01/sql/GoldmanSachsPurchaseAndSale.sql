USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsPurchaseAndSale]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsPurchaseAndSale]


USE [CliftonIntegration]

/****** Object:  Table [source].[BrokerGoldmanSachsPurchaseAndSale]    Script Date: 05/18/2011 17:17:29 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[BrokerGoldmanSachsPurchaseAndSale](
	[IntegrationImportRunID] [int] NOT NULL,
	[COBDate] [datetime] NULL,
	[TradeDate] [varchar](100) NULL,
	[Account] [varchar](100) NULL,
	[Accountname] [varchar](100) NULL,
	[TradeType] [varchar](100) NULL,
	[BuySell] [varchar](100) NULL,
	[Quantity] decimal(28,16) NULL,
	[Market] [varchar](100) NULL,
	[Product] [varchar](100) NULL,
	[ContractMonth] [int] NULL,
	[ContractYear] [int] NULL,
	[PutCallIndicator] [varchar](100) NULL,
	[StrikePrice] [varchar](100) NULL,
	[ContractCurrency] [varchar](100) NULL,
	[TradePrice] decimal(28,16) NULL,
	[RealizedPLlocal] decimal(28,16) NULL,
	[NetRealizedPLlocal] decimal(28,16) NULL,
	[ClearingExecutionCommissionlocal] decimal(28,16) NULL,
	[TotalFeeslocal] decimal(28,16) NULL,
	[APSStyle] [varchar](100) NULL,
	[AccountBaseCurrency] [varchar](100) NULL,
	[AccountCountry] [varchar](100) NULL,
	[AccountType] [int] NULL,
	[BloombergCode] [varchar](100) NULL,
	[BloombergUniqueCode] [varchar](100) NULL,
	[ClearingExecutionCommissionbase] decimal(28,16) NULL,
	[ClearingExecutionCommissionCurrency] [varchar](100) NULL,
	[ClearingCommissionbase] decimal(28,16) NULL,
	[ClearingCommissionlocal] decimal(28,16) NULL,
	[ClearingCommissionCurrency] [varchar](100) NULL,
	[ClearingFeebase] decimal(28,16) NULL,
	[ClearingFeelocal] decimal(28,16) NULL,
	[ClearingFeeCurrency] [varchar](100) NULL,
	[ClientAccountNumber] [int] NULL,
	[ClientProductCode] [varchar](100) NULL,
	[ConsumptionTaxbase] decimal(28,16) NULL,
	[ConsumptionTaxlocal] decimal(28,16) NULL,
	[ConsumptionTaxCurrency] [varchar](100) NULL,
	[ContractDescription] [varchar](100) NULL,
	[ContractMultiplier] decimal(28,16) NULL,
	[CouponRate] [varchar](100) NULL,
	[DerivedClearingCommissionRate] decimal(28,16) NULL,
	[DerivedExecutionClearingCommissionRate] decimal(28,16) NULL,
	[DerivedExecutionCommissionRate] decimal(28,16) NULL,
	[ExchangeElectronicPlatform] [varchar](100) NULL,
	[ExchangeFeebase] decimal(28,16) NULL,
	[ExchangeFeelocal] decimal(28,16) NULL,
	[ExchangeFeeCurrency] [varchar](100) NULL,
	[ExecutionCommissionbase] decimal(28,16) NULL,
	[ExecutionCommissionlocal] decimal(28,16) NULL,
	[ExecutionCommissionCurrency] [varchar](100) NULL,
	[FXRatetoAccountBaseCurrency] decimal(28,16) NULL,
	[Firm] [varchar](100) NULL,
	[FlexibleCode] [varchar](100) NULL,
	[FuturesEquityCode] [varchar](100) NULL,
	[GMIExchangeCode] [varchar](100) NULL,
	[GMIOriginalTracer] [varchar](100) NULL,
	[GMIProductCode] [varchar](100) NULL,
	[GMISubExchangeCode] [varchar](100) NULL,
	[GMIUniqueSequence] [varchar](100) NULL,
	[LongQuantity] decimal(28,16) NULL,
	[MarginGroupCode] [varchar](100) NULL,
	[MemoOptionPremiumbase] decimal(28,16) NULL,
	[MemoOptionPremiumlocal] decimal(28,16) NULL,
	[NCMFlag] [char](1) NULL,
	[NFACurrency] [varchar](100) NULL,
	[NFAFeebase] decimal(28,16) NULL,
	[NFAFeelocal] decimal(28,16) NULL,
	[NetMemoOptionPremiumbase] decimal(28,16) NULL,
	[NetMemoOptionPremiumlocal] decimal(28,16) NULL,
	[NetRealizedPLbase] decimal(28,16) NULL,
	[NetQuantity] decimal(28,16) NULL,
	[NotionalQuantity] decimal(28,16) NULL,
	[OpenCloseIndicator] [varchar](100) NULL,
	[OptionStyle] [varchar](100) NULL,
	[OrderChannel] [varchar](100) NULL,
	[OtherFeesbase] decimal(28,16) NULL,
	[OtherFeeslocal] decimal(28,16) NULL,
	[OtherFeesCurrency] [varchar](100) NULL,
	[PLPostingIndicator] [varchar](100) NULL,
	[PremiumStyle] [varchar](100) NULL,
	[ProductCategory] [varchar](100) NULL,
	[ProductID] [int] NULL,
	[ProductTypeIndicator] [varchar](100) NULL,
	[ProductCountryCode] [varchar](100) NULL,
	[PromptDay] [varchar](100) NULL,
	[RealizedPLbase] decimal(28,16) NULL,
	[RegNonRegIndicator] [varchar](100) NULL,
	[ReutersCode] [varchar](100) NULL,
	[SECCurrency] [varchar](100) NULL,
	[SECFeebase] decimal(28,16) NULL,
	[SECFeelocal] decimal(28,16) NULL,
	[SettleDate] [datetime] NULL,
	[ShortQuantity] decimal(28,16) NULL,
	[StrategyCode] [varchar](100) NULL,
	[StrikePricePrintable] decimal(28,16) NULL,
	[Ticker] [varchar](100) NULL,
	[TotalFeesbase] decimal(28,16) NULL,
	[TotalFeeCurrency] [varchar](100) NULL,
	[TradePricePrintable] decimal(28,16) NULL
) ON [PRIMARY]



SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerGoldmanSachsPurchaseAndSale]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsPurchaseAndSale_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[BrokerGoldmanSachsPurchaseAndSale] CHECK CONSTRAINT [FK_BrokerGoldmanSachsPurchaseAndSale_IntegrationImportRun]



