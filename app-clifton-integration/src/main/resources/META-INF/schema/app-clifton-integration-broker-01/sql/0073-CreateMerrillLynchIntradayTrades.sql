
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerMerrillLynchIntradayTrades') AND type in (N'U'))
	DROP TABLE source.BrokerMerrillLynchIntradayTrades
	
CREATE TABLE source.BrokerMerrillLynchIntradayTrades (
	BrokerMerrillLynchIntradayTradesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	RECORDID varchar(100) NULL,
	FIRM varchar(100) NULL,
	OFFICE varchar(100) NULL,
	ACCOUNT varchar(100) NULL,
	ACCOUNTTYPE varchar(100) NULL,
	TRADEDATE datetime NULL,
	BUYSELLCODE varchar(100) NULL,
	QUANTITY decimal(28,16) NULL,
	TRADEDESCRIPTION varchar(100) NULL,
	MULTFACTOR decimal(28,16) NULL,
	TRADEPRICE decimal(28,16) NULL,
	PRINTABLETRADEPRICE varchar(100) NULL,
	CONTRACTID varchar(100) NULL,
	EXCHANGE varchar(100) NULL,
	PRODUCTCODE varchar(100) NULL,
	CONTRACTYEARMONTH varchar(100) NULL,
	PUTCALL varchar(100) NULL,
	STRIKEPRICE decimal(28,16) NULL,
	GIVEUPCODE varchar(100) NULL,
	BROKERNAME varchar(100) NULL,
	CURRENCY varchar(100) NULL,
	CUSIP varchar(100) NULL,
	CUSIP2 varchar(100) NULL,
	PROMPTDAY varchar(100) NULL,
	EXPIRYDATE varchar(100) NULL,
	LASTTRADEDATE varchar(100) NULL,
	CARDNO varchar(100) NULL,
	TRADERID varchar(100) NULL,
	BROKERFINDER varchar(100) NULL,
	BBGCODE varchar(100) NULL,
	TRADEEXECUTIONTIME varchar(100) NULL,
	COMMENTCODE1 varchar(100) NULL,
	CONSTRAINT PK_BrokerMerrillLynchIntradayTrades PRIMARY KEY CLUSTERED (BrokerMerrillLynchIntradayTradesID),
	CONSTRAINT FK_BrokerMerrillLynchTrades_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
