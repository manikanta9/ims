
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerMerrillLynchOpenPositionDetail') AND type in (N'U'))
	DROP TABLE source.BrokerMerrillLynchOpenPositionDetail
	
CREATE TABLE source.BrokerMerrillLynchOpenPositionDetail (
	BrokerMerrillLynchOpenPositionDetailID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	RecordID varchar(50) NULL,
	TransactionType varchar(50) NULL,
	Firm varchar(50) NULL,
	Office varchar(50) NULL,
	Account varchar(50) NULL,
	AccountType varchar(50) NULL,
	TradeDate datetime NULL,
	BuySellCode int NULL,
	Quantity decimal(28,16) NULL,
	TradeDescription varchar(100) NULL,
	MultFactor decimal(28,16) NULL,
	TradePrice decimal(28,16) NULL,
	PrintableTradePrice varchar(100) NULL,
	ContractID varchar(100) NULL,
	OpenClose varchar(50) NULL,
	Exchange varchar(50) NULL,
	ProductCode varchar(50) NULL,
	ContractYearMonth varchar(50) NULL,
	PutCall varchar(50) NULL,
	StrikePrice decimal(28,16) NULL,
	OptionPremium decimal(28,16) NULL,
	OpenTradeEquity decimal(28,16) NULL,
	SettleToMktValue decimal(28,16) NULL,
	ClosingPrice decimal(28,16) NULL,
	PrevClosePrice decimal(28,16) NULL,
	Commissions decimal(28,16) NULL,
	ClearingFee decimal(28,16) NULL,
	ExchangeFee decimal(28,16) NULL,
	NFAFee decimal(28,16) NULL,
	GiveUpCharge decimal(28,16) NULL,
	BrokerageCharge decimal(28,16) NULL,
	ElecExecCharge  decimal(28,16) NULL,
	OtherCharges decimal(28,16) NULL,
	Currency varchar(50) NULL,
	Cusip varchar(50) NULL,
	Cusip2 varchar(50) NULL,
	PromptDay varchar(50) NULL,
	ExpiryDate varchar(50) NULL,
	LastTradeDate datetime NULL,
	BBGCode varchar(50) NULL,
	BBGYellowKey varchar(50) NULL,
	COBDate datetime NULL,
	SettlementDate datetime NULL,
	FXRate decimal(28,16) NULL,
	CostBasisLocal decimal(28,16) NULL,
	CostBasisBase decimal(28,16) NULL,
	MarketValueLocal decimal(28,16) NULL,
	MarketValueBase decimal(28,16) NULL,
	OpenTradeEquityBase decimal(28,16) NULL,
	CONSTRAINT PK_BrokerMerrillLynchOpenPositionDetail PRIMARY KEY CLUSTERED (BrokerMerrillLynchOpenPositionDetailID),
	CONSTRAINT FK_BrokerMerrillLynchOpenPositionDetail_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
