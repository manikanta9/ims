
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerUBSAccountBalances') AND type in (N'U'))
	DROP TABLE source.BrokerUBSAccountBalances
	
CREATE TABLE source.BrokerUBSAccountBalances (
	BrokerUBSAccountBalancesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	AccountNumber varchar(100) NULL,
	AccountName varchar(200) NULL,
	BalanceDate datetime NULL,
	AccountBaseCCY varchar(100) NULL,
	BeginningBalance decimal(28,16) NULL,
	CashMovements decimal(28,16) NULL,
	CommissionAndFees decimal(28,16) NULL,
	Tax decimal(28,16) NULL,
	OptionPremium decimal(28,16) NULL,
	UpfrontFees decimal(28,16) NULL,
	DailyPAI varchar(100) NULL,
	RealizedProfitLoss decimal(28,16) NULL,
	EndingBalance decimal(28,16) NULL,
	FuturesVariationMargin decimal(28,16) NULL,
	OptionVariationMargin decimal(28,16) NULL,
	SwapVariationMargin decimal(28,16) NULL,
	TotalEquity decimal(28,16) NULL,
	ForwardCash decimal(28,16) NULL,
	TotalEquityIncFwdCash decimal(28,16) NULL,
	LongOptionValue decimal(28,16) NULL,
	ShortOptionValue decimal(28,16) NULL,
	InitialMargin decimal(28,16) NULL,
	CollateralSecurities decimal(28,16) NULL,
	CashCollateral decimal(28,16) NULL,
	NetEquity decimal(28,16) NULL,
	NetEquityIncFwdCash decimal(28,16) NULL,
	ForwardValueExcludingLME decimal(28,16) NULL,
	LMEForwardValue decimal(28,16) NULL,
	LiquidationValue decimal(28,16) NULL,
	TotalEquityLessCashCollateralPosted decimal(28,16) NULL,
	CONSTRAINT PK_BrokerUBSAccountBalances PRIMARY KEY CLUSTERED (BrokerUBSAccountBalancesID),
	CONSTRAINT FK_BrokerUBSAccountBalances_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
