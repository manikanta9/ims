USE [CliftonIntegration]
GO

SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

SET ANSI_PADDING ON
GO

CREATE TABLE source.BrokerMorganStanleyTrades
	(
	BrokerMorganStanleyTradesID int NOT NULL IDENTITY (1, 1),
	IntegrationImportRunID int NOT NULL,
	StatementDate datetime NULL,
	Account varchar(100) NULL,
	SecuredAccount varchar(100) NULL,
	AccountName varchar(100) NULL,
	TradeDate datetime NULL,
	Exchange varchar(100) NULL,
	ProductDescription varchar(100) NULL,
	ContractMonth varchar(100) NULL,
	ContractYear varchar(100) NULL,
	StrikePrice varchar(100) NULL,
	PutCallInd varchar(100) NULL,
	Quantity decimal(28, 16) NULL,
	BuySellInd varchar(100) NULL,
	Currency varchar(100) NULL,
	TradePrice decimal(28, 16) NULL,
	DisplayPriceDecimal varchar(100) NULL,
	DisplayCode varchar(100) NULL,
	SpreadInd varchar(100) NULL,
	AveragePriceInd varchar(100) NULL,
	NightInd varchar(100) NULL,
	BlockExecutionInd varchar(100) NULL,
	OffExchangeInd varchar(100) NULL,
	OffsetType varchar(100) NULL,
	OptnPremAmt decimal(28, 16) NULL,
	OptnPremDrCrInd varchar(100) NULL,
	Commission decimal(28, 16) NULL,
	CommissionDrCrInd varchar(100) NULL,
	CommissionCurrency varchar(100) NULL,
	Fees decimal(28, 16) NULL,
	FeesDrCrInd varchar(100) NULL,
	FeesCurrency varchar(100) NULL,
	TradeReferenceNumber varchar(100) NULL,
	TradeVersion decimal(28, 16) NULL,
	Cusip varchar(100) NULL,
	MSSymbol varchar(100) NULL,
	ExchangeContractCode varchar(100) NULL,
	RIC varchar(100) NULL,
	ExpiryDate datetime NULL,
	ProductType varchar(100) NULL,
	OptnStyleCode varchar(100) NULL,
	BloombergTicker varchar(100) NULL,
	BrokerCode varchar(100) NULL,
	ExecutionCommission decimal(28, 16) NULL,
	ExecutionCommissionDbCrInd varchar(100) NULL,
	ExecutionCommissionCurrency varchar(100) NULL,
	ClearingCommission decimal(28, 16) NULL,
	ClearingCommDbCrInd varchar(100) NULL,
	ClearingCommissionCurrency varchar(100) NULL,
	BloombergIdentifier varchar(100) NULL,
	BloombergUnique varchar(100) NULL,
	BloombergExchange varchar(100) NULL,
	OldNewInd varchar(100) NULL,
	CONSTRAINT PK_BrokerMorganStanleyTrades PRIMARY KEY CLUSTERED
		(
	BrokerMorganStanleyTradesID ASC
	) WITH( PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 95) ON [PRIMARY]
	)  ON [PRIMARY]

GO

SET ANSI_PADDING OFF
GO



