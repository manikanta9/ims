USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsNetPositions]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsNetPositions]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsNetPositions]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsNetPositions]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerGoldmanSachsNetPositions](
	[COBDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[AcctName] [varchar](100) NULL,
	[Long] decimal(28,16) NULL,
	[Short] decimal(28,16) NULL,
	[NetQty] [varchar](100) NULL,
	[Product] [varchar](100) NULL,
	[Month] [varchar](100) NULL,
	[Year] decimal(28,16) NULL,
	[ContractDay] decimal(28,16) NULL,
	[PC] [datetime] NULL,
	[StrikePrice] [datetime] NULL,
	[Market] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[GMIExchProdCode] [varchar](100) NULL,
	[StlPrice] [varchar](100) NULL,
	[Details] [varchar](100) NULL,
	[AccountBaseCurrency] [varchar](100) NULL,
	[AccountType] [varchar](100) NULL,
	[AverageTradePrice] [varchar](100) NULL,
	[BloombergCode] [varchar](100) NULL,
	[BloombergUniqueCode] [varchar](100) NULL,
	[ClientAccountNumber] [varchar](100) NULL,
	[ClientProductCode] [varchar](100) NULL,
	[ContractDescription] [varchar](100) NULL,
	[ContractMult] decimal(28,16) NULL,
	[Country] [varchar](100) NULL,
	[CouponRate] decimal(28,16) NULL,
	[DiscountedVariationMargin] [varchar](100) NULL,
	[DiscountFactor] [varchar](100) NULL,
	[DisplayStlPrice] [varchar](100) NULL,
	[DisplayStrikePrice] [varchar](100) NULL,
	[EODExchOptionDelta] [varchar](100) NULL,
	[EquitySymbol] [varchar](100) NULL,
	[ExposureBase] decimal(28,16) NULL,
	[ExposureBaseAbsolute] decimal(28,16) NULL,
	[ExposureLocal] decimal(28,16) NULL,
	[ExposureLocalAbsolute] decimal(28,16) NULL,
	[FXRate] decimal(28,16) NULL,
	[FirstDeliveryDate] [datetime] NULL,
	[FirstNoticeDate] [datetime] NULL,
	[FlexibleCode] [varchar](100) NULL,
	[GMIExchCode] [varchar](100) NULL,
	[GMIProdCode] [varchar](100) NULL,
	[GMISubExchCode] [varchar](100) NULL,
	[ImpliedMarketValue] decimal(28,16) NULL,
	[InstrumentCode] [varchar](100) NULL,
	[LastDeliveryDate] [datetime] NULL,
	[LastNoticeDate] [datetime] NULL,
	[LastTradeDate] [datetime] NULL,
	[LongAverageTradePrice] decimal(28,16) NULL,
	[LongOptionMarketValueBase] decimal(28,16) NULL,
	[LongOptionMarketValueLocal] decimal(28,16) NULL,
	[MarginGroupCode] [varchar](100) NULL,
	[NotionalValueBase] decimal(28,16) NULL,
	[NotionalValueLocal] decimal(28,16) NULL,
	[OpenTradeEquityBase] decimal(28,16) NULL,
	[OpenTradeEquityLocal] decimal(28,16) NULL,
	[OptionMarketValueBase] decimal(28,16) NULL,
	[OptionMarketValueLocal] decimal(28,16) NULL,
	[OptionStyle] [varchar](100) NULL,
	[OptionsFuturesEquivalent] [varchar](100) NULL,
	[OptionExpirationDate] [datetime] NULL,
	[OptionPremiumBase] decimal(28,16) NULL,
	[OptionPremiumLocal] decimal(28,16) NULL,
	[ProductCategory] [varchar](100) NULL,
	[ProductID] [varchar](100) NULL,
	[ProductTypeCode] [varchar](100) NULL,
	[ReutersCode] [varchar](100) NULL,
	[SettlementStyle] [varchar](100) NULL,
	[ShortAverageTradePrice] decimal(28,16) NULL,
	[ShortOptionMarketValueBase] [varchar](100) NULL,
	[ShortOptionMarketValueLocal] [varchar](100) NULL,
	[Ticker] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerGoldmanSachsNetPositions]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsNetPositions_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsNetPositions] CHECK CONSTRAINT [FK_BrokerGoldmanSachsNetPositions_IntegrationImportRun]


