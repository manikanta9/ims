CREATE TABLE source.BrokerCitigroupDailyCash (
	  BrokerCitigroupDailyCashID INT IDENTITY(1,1) NOT NULL 
	, IntegrationImportRunID INT NOT NULL
	, PostingDate DATE NOT NULL
	, ACRef VARCHAR(50) NOT NULL
	, ACName VARCHAR(100) NULL
	, SegType VARCHAR(50) NULL
	, Curr VARCHAR(50) NULL
	, PostingType VARCHAR(50) NULL
	, PostingNarrative VARCHAR(100) NOT NULL
	, PostingNo VARCHAR(100) NULL
	, Contract VARCHAR(100) NULL
	, Delivery VARCHAR(50) NULL
	, Amount DECIMAL(19,2) NOT NULL
	, Strike VARCHAR(100) NULL
	, Lots DECIMAL(19,2) NULL
	, RIC VARCHAR(50) NULL
	, TradeType VARCHAR(100) NULL
	, CONSTRAINT PK_BrokerCitigroupDailyCash PRIMARY KEY CLUSTERED (BrokerCitigroupDailyCashID)
	, CONSTRAINT FK_BrokerCitigroupDailyCash_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
);
