ALTER PROCEDURE [dbo].[DeleteDailyIntradayCumulativeDate]
	@IntegrationImportRunID INT
AS
BEGIN
	
	SET NOCOUNT ON;
	
	DECLARE @TradeDate DATE;
	SET @TradeDate = CONVERT(DATE, (SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID))

	
	DELETE FROM IntegrationTradeIntraday
	WHERE IntegrationImportRunID IN
	(
		SELECT IntegrationImportRunID FROM IntegrationTradeIntraday
		WHERE IntegrationImportRunID IN 
		(
			SELECT IntegrationImportRunID
			FROM IntegrationImportRun r
				INNER JOIN IntegrationFile f ON f.IntegrationFileID = r.IntegrationFileID
			WHERE IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID)
				AND f.IntegrationFileDefinitionID = 
				(
					SELECT f.IntegrationFileDefinitionID
					FROM IntegrationImportRun r
						INNER JOIN IntegrationFile f ON f.IntegrationFileID = r.IntegrationFileID
					WHERE IntegrationImportRunID = @IntegrationImportRunID
				)
				AND CONVERT(DATE, r.EffectiveDate) = CONVERT(DATE,(SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID))
		)
		--AND TradeDate >= CONVERT(DATE,(SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID))
		GROUP BY IntegrationImportRunID
	)
	
	
	DELETE FROM t
	FROM IntegrationTradeIntraday t
		INNER JOIN IntegrationImportRun r ON r.IntegrationImportRunID = t.IntegrationImportRunID
	WHERE TradeDate = @TradeDate
		AND IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID)
		AND r.IntegrationImportRunID NOT IN 
		(
			SELECT IntegrationImportRunID
			FROM IntegrationImportRun r
				INNER JOIN IntegrationFile f ON f.IntegrationFileID = r.IntegrationFileID
			WHERE IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID)
				AND f.IntegrationFileDefinitionID = 
				(
					SELECT f.IntegrationFileDefinitionID
					FROM IntegrationImportRun r
						INNER JOIN IntegrationFile f ON f.IntegrationFileID = r.IntegrationFileID
					WHERE IntegrationImportRunID = @IntegrationImportRunID
				)
				AND CONVERT(DATE, r.EffectiveDate) = CONVERT(DATE,(SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID))
		)
	
END
