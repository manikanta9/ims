USE [CliftonIntegration]

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsConfirmedTrades]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsConfirmedTrades]

USE [CliftonIntegration]

/****** Object:  Table [source].[BrokerGoldmanSachsConfirmedTrades]    Script Date: 05/18/2011 16:34:04 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON


CREATE TABLE [source].[BrokerGoldmanSachsConfirmedTrades](
	[IntegrationImportRunID] [int] NOT NULL,
	[COBDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[TradeType] [varchar](100) NULL,
	[BuySell] [varchar](100) NULL,
	[Quantity] [int] NULL,
	[Market] [varchar](100) NULL,
	[Product] [varchar](100) NULL,
	[ContractMonth] [int] NULL,
	[ContractYear] [int] NULL,
	[PC] [varchar](100) NULL,
	[StrikePrice] [varchar](100) NULL,
	[ContractCurrency] [varchar](100) NULL,
	[TradePrice] decimal(28,16) NULL,
	[ExecutingBroker] [varchar](100) NULL,
	[ClearingExecutionCommissionlocal] decimal(28,16) NULL,
	[TotalFeeslocal] decimal(28,16) NULL,
	[OptionPremiumlocal] decimal(28,16) NULL,
	[TradeDate] [datetime] NULL,
	[APSStyle] [varchar](100) NULL,
	[AccountBaseCurrency] [varchar](100) NULL,
	[AccountCountry] [varchar](100) NULL,
	[AccountType] [int] NULL,
	[AccruedClearingExecutionCommissionbase] decimal(28,16) NULL,
	[AccruedClearingExecutionCommissionlocal] decimal(28,16) NULL,
	[AccruedClearingCommissionbase] decimal(28,16) NULL,
	[AccruedClearingCommissionlocal] decimal(28,16) NULL,
	[AccruedExecutionCommissionbase] decimal(28,16) NULL,
	[AccruedExecutionCommissionlocal] decimal(28,16) NULL,
	[AccruedTotalFeesbase] decimal(28,16) NULL,
	[AccruedTotalFeeslocal] decimal(28,16) NULL,
	[BloombergCode] [varchar](100) NULL,
	[BloombergUniqueCode] [varchar](100) NULL,
	[ClearedQuantity] [int] NULL,
	[ClearingExecutionCommissionbase] decimal(28,16) NULL,
	[ClearingExecutionCommissionCurrency] [varchar](100) NULL,
	[ClearingCommissionbase] decimal(28,16) NULL,
	[ClearingCommissionlocal] decimal(28,16) NULL,
	[ClearingCommissionCurrency] [varchar](100) NULL,
	[ClearingFeebase] decimal(28,16) NULL,
	[ClearingFeelocal] decimal(28,16) NULL,
	[ClearingFeeCurrency] [varchar](100) NULL,
	[ClientAccountNumber] [int] NULL,
	[ClientProductCode] [varchar](100) NULL,
	[ClosingPriceofUnderlyingProductforEquityOptions] decimal(28,16) NULL,
	[ClosingQuantity] decimal(28,16) NULL,
	[ConsumptionTaxbase] decimal(28,16) NULL,
	[ConsumptionTaxlocal] decimal(28,16) NULL,
	[ConsumptionTaxCurrency] [varchar](100) NULL,
	[ContractDescription] [varchar](100) NULL,
	[ContractKey] [int] NULL,
	[ContractMultiplier] decimal(28,16) NULL,
	[ContractValue] decimal(28,16) NULL,
	[ContractValuelocal] decimal(28,16) NULL,
	[CouponRate] [varchar](100) NULL,
	[DealSpread] [varchar](100) NULL,
	[DerivedClearingCommissionRate] decimal(28,16) NULL,
	[DerivedExecutionClearingCommissionRate] decimal(28,16) NULL,
	[DerivedExecutionCommissionRate] decimal(28,16) NULL,
	[EODExchOptionDelta] [varchar](100) NULL,
	[ExchangeElectronicPlatform] [varchar](100) NULL,
	[ExchangeFeebase] decimal(28,16) NULL,
	[ExchangeFeelocal] decimal(28,16) NULL,
	[ExchangeFeeCurrency] [varchar](100) NULL,
	[ExecutionCommissionbase] decimal(28,16) NULL,
	[ExecutionCommissionlocal] decimal(28,16) NULL,
	[ExecutionCommissionCurrency] [varchar](100) NULL,
	[FXRatetoAccountBaseCurrency] decimal(28,16) NULL,
	[Firm] [varchar](100) NULL,
	[FlexibleCode] [varchar](100) NULL,
	[FuturesEquityCode] [varchar](100) NULL,
	[GMIExchangeCode] [int] NULL,
	[GMIOriginalTracer] [varchar](100) NULL,
	[GMIProductCode] [varchar](100) NULL,
	[GMISubExchangeCode] [varchar](100) NULL,
	[GMIUniqueSequenceNum] [int] NULL,
	[GiveinGiveoutIndicator] [varchar](100) NULL,
	[InitialCoupon] decimal(28,16) NULL,
	[LastTradeDate] [datetime] NULL,
	[LongQuantity] decimal(28,16) NULL,
	[MarginGroupCode] [varchar](100) NULL,
	[MarginStyle] [varchar](100) NULL,
	[NCMFlag] [varchar](100) NULL,
	[NFACurrency] [varchar](100) NULL,
	[NFAFeebase] decimal(28,16) NULL,
	[NFAFeelocal] decimal(28,16) NULL,
	[NetNotionalValuebase] decimal(28,16) NULL,
	[NetNotionalValuelocal] decimal(28,16) NULL,
	[NetQuantity] decimal(28,16) NULL,
	[NumOfDaysSettle] [int] NULL,
	[NotionalQuantity] decimal(28,16) NULL,
	[NotionalValuebase] decimal(28,16) NULL,
	[NotionalValuelocal] decimal(28,16) NULL,
	[OTE] decimal(28,16) NULL,
	[OpenCloseIndicator] [varchar](100) NULL,
	[OpeningQuantity] decimal(28,16) NULL,
	[OptionsFuturesEquivalent] [varchar](100) NULL,
	[OptionExerciseStyle] [varchar](100) NULL,
	[OptionExpirationDate] [datetime] NULL,
	[OptionPremiumbase] decimal(28,16) NULL,
	[OrderChannel] [varchar](100) NULL,
	[OtherFeesbase] decimal(28,16) NULL,
	[OtheFeeslocal] decimal(28,16) NULL,
	[OtherFeesCurrency] [varchar](100) NULL,
	[ProductCategory] [varchar](100) NULL,
	[ProductID] [varchar](100) NULL,
	[ProductCountryCode] [varchar](100) NULL,
	[ProductType] [varchar](100) NULL,
	[PromptDay] [varchar](100) NULL,
	[RegNonRegCode] [varchar](100) NULL,
	[ReutersCode] [varchar](100) NULL,
	[SECCurrency] [varchar](100) NULL,
	[SECFeebase] decimal(28,16) NULL,
	[SECFeelocal] decimal(28,16) NULL,
	[SettlementPrice] decimal(28,16) NULL,
	[SettlementStyle] [varchar](100) NULL,
	[SettleDate] [datetime] NULL,
	[ShortQuantity] decimal(28,16) NULL,
	[StrategyCode] [varchar](100) NULL,
	[Ticker] [varchar](100) NULL,
	[TotalFeesbase] decimal(28,16) NULL,
	[TotalFeeCurrency] [varchar](100) NULL,
	[TradePricePrintable] decimal(28,16) NULL,
	[TradeDateAsString] [varchar](100) NULL,
	[UpfrontFee] decimal(28,16) NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF


ALTER TABLE [source].[BrokerGoldmanSachsConfirmedTrades]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsConfirmedTrades_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[BrokerGoldmanSachsConfirmedTrades] CHECK CONSTRAINT [FK_BrokerGoldmanSachsConfirmedTrades_IntegrationImportRun]

