IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerGoldmanSachsExchangeRates') AND type in (N'U'))
	DROP TABLE source.BrokerGoldmanSachsExchangeRates

CREATE TABLE source.BrokerGoldmanSachsExchangeRates (
	BrokerGoldmanSachsExchangeRatesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	Rate decimal(28,16) NULL,
	FromCurrencyCode varchar(100) NULL,
	ToCurrencyCode varchar(100) NULL,
	COBDate datetime NULL,
	InverseRate decimal(28,16) NULL,
	CONSTRAINT PK_BrokerGoldmanSachsExchangeRates PRIMARY KEY CLUSTERED (BrokerGoldmanSachsExchangeRatesID),
	CONSTRAINT FK_BrokerGoldmanSachsExchangeRates_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
