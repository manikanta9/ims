
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerMorganStanleyProfitAndLoss]') AND type in (N'U'))
DROP TABLE [source].[BrokerMorganStanleyProfitAndLoss]


CREATE TABLE [source].[BrokerMorganStanleyProfitAndLoss](
	[BrokerMorganStanleyProfitAndLossID] [int] IDENTITY(1,1) NOT NULL,
	[IntegrationImportRunID] [int] NOT NULL,
	[StatementDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[SecuredAccount] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[StatusInd] [varchar](100) NULL,
	[TradeDate] [datetime] NULL,
	[SettlementDate] [datetime] NULL,
	[Exchange] [varchar](100) NULL,
	[ProductDescription] [varchar](100) NULL,
	[ContractDate] [datetime] NULL,
	[ContractMonth] [varchar](100) NULL,
	[ContractYear] [varchar](100) NULL,
	[StrikePrice] [varchar](100) NULL,
	[PutCallInd] [varchar](100) NULL,
	[Quantity] decimal(28,16) NULL,
	[BuySellInd] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[TradePrice] decimal(28,16) NULL,
	[DisplayPriceDecimal] decimal(28,16) NULL,
	[DisplayCode] [varchar](100) NULL,
	[SpreadInd] [varchar](100) NULL,
	[AvgeragePriceInd] [varchar](100) NULL,
	[NightInd] [varchar](100) NULL,
	[BlockExecutionInd] [varchar](100) NULL,
	[OffExchangeInd] [varchar](100) NULL,
	[GrossAmt] decimal(28,16) NULL,
	[GrossAmtDrCrInd] [varchar](100) NULL,
	[Commission] decimal(28,16) NULL,
	[CommissionDrCrInd] [varchar](100) NULL,
	[CommissionCurrency] [varchar](100) NULL,
	[Fees] decimal(28,16) NULL,
	[FeesDrCrInd] [varchar](100) NULL,
	[FeesCurrency] [varchar](100) NULL,
	[NetAmt] decimal(28,16) NULL,
	[NetAmtDrCrInd] [varchar](100) NULL,
	[NetAmtCurrency] [varchar](100) NULL,
	[TradeReferenceNumber] [varchar](100) NULL,
	[Cusip] [varchar](100) NULL,
	[MSSymbol] [varchar](100) NULL,
	[ExchangeContractCode] [varchar](100) NULL,
	[RIC] [varchar](100) NULL,
	[ExpiryDate] [datetime] NULL,
	[ProductType] [varchar](100) NULL,
	[OptionType] [varchar](100) NULL,
	[SegregatedInd] [varchar](100) NULL,
	[OptnStyleCode] [varchar](100) NULL,
	[BloombergTicker] [varchar](100) NULL,
	[BloombergIdentifier] [varchar](100) NULL,
	[BloombergUnique] [varchar](100) NULL,
	[BloombergExchange] [varchar](100) NULL,
	[DailyContractDate] [varchar](100) NULL,
	[ClientReference] [varchar](100) NULL,
	[FxRate] decimal(28,16) NULL,
	[ConvertedComissions] decimal(28,16) NULL,
	[ConvertedFees] decimal(28,16) NULL,
	[ConvertedGrossAmount] decimal(28,16) NULL,
) ON [PRIMARY]

ALTER TABLE [source].[BrokerMorganStanleyProfitAndLoss] ADD CONSTRAINT [PK_BrokerMorganStanleyProfitAndLoss]
	PRIMARY KEY CLUSTERED (BrokerMorganStanleyProfitAndLossID);

ALTER TABLE [source].[BrokerMorganStanleyProfitAndLoss]  WITH CHECK ADD CONSTRAINT [FK_BrokerMorganStanleyProfitAndLoss_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
	REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[BrokerMorganStanleyProfitAndLoss] CHECK CONSTRAINT [FK_BrokerMorganStanleyProfitAndLoss_IntegrationImportRun]
