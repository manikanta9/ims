USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsDailyPNL]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsDailyPNL]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsDailyPNL]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsDailyPNL]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerGoldmanSachsDailyPNL](
	[COBDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[ContractCurrency] [varchar](100) NULL,
	[Market] [varchar](100) NULL,
	[ProductDescription] [varchar](100) NULL,
	[ContractYear] [int] NULL,
	[ContractMonth] [varchar](100) NULL,
	[PutCall] [varchar](100) NULL,
	[StrikePrice] decimal(28,16) NULL,
	[PriorNetPosition] decimal(28,16) NULL,
	[CurrentNetPosition] decimal(28,16) NULL,
	[ChangeInNetPosition] decimal(28,16) NULL,
	[AverageTradePriceOfPositions] decimal(28,16) NULL,
	[SettlementPrice] decimal(28,16) NULL,
	[OpenTradeEquity] decimal(28,16) NULL,
	[PriorOpenTradeEquity] decimal(28,16) NULL,
	[UnrealizedPandL_ChangeinOTE] decimal(28,16) NULL,
	[RealizedPandL] decimal(28,16) NULL,
	[OptionPremium] decimal(28,16) NULL,
	[GrossPandL_and_ChangeinOTELocal] decimal(28,16) NULL,
	[TotalCommissionandFees] decimal(28,16) NULL,
	[NetPandL_and_ChangeinOTELocal] decimal(28,16) NULL,
	[AccountBaseCurrency] [varchar](100) NULL,
	[AccountType] [varchar](100) NULL,
	[AveragePriceOfCurrentLongPosition] decimal(28,16) NULL,
	[AveragePriceOfCurrentShortPosition] decimal(28,16) NULL,
	[AveragePriceOfPriorLongPosition] decimal(28,16) NULL,
	[AveragePriceOfPriorShortPosition] decimal(28,16) NULL,
	[AverageTradePriceOfClosingTrades] decimal(28,16) NULL,
	[AverageTradePriceOfNewTrades] decimal(28,16) NULL,
	[AverageTradePriceOfOpeningTrades] decimal(28,16) NULL,
	[BaseBrokerage] decimal(28,16) NULL,
	[BaseCommissions] decimal(28,16) NULL,
	[BaseFees] decimal(28,16) NULL,
	[BaseMTMClosingTrades] decimal(28,16) NULL,
	[BaseMTMOpeningTrades] decimal(28,16) NULL,
	[BaseMTMPositionsCarriedForward] decimal(28,16) NULL,
	[BaseMTMTotalActivityandPositions] decimal(28,16) NULL,
	[BaseMarketValueOfOptions] decimal(28,16) NULL,
	[BaseOpenTradeEquity] decimal(28,16) NULL,
	[BaseOptionPremium] decimal(28,16) NULL,
	[BasePriorOpenTradeEquity] decimal(28,16) NULL,
	[BaseRealizedPandL] decimal(28,16) NULL,
	[BaseTotalCommissionandFees] decimal(28,16) NULL,
	[BaseUnrealizedPandL_ChangeinOTE] decimal(28,16) NULL,
	[BloombergCode] [varchar](100) NULL,
	[BloombergUniqueCode] [varchar](100) NULL,
	[Brokerage] decimal(28,16) NULL,
	[ChangeinMarketValueofOptions] decimal(28,16) NULL,
	[ChangeInLongPosition] decimal(28,16) NULL,
	[ChangeInShortPosition] decimal(28,16) NULL,
	[ClientAccountNumber] [varchar](100) NULL,
	[ClientProductCode] [varchar](100) NULL,
	[ClosingTradeQuantity] decimal(28,16) NULL,
	[Commissions] decimal(28,16) NULL,
	[ContractDescription] [varchar](100) NULL,
	[CurrentLongPosition] decimal(28,16) NULL,
	[CurrentShortPosition] decimal(28,16) NULL,
	[EODExchOptionDelta] decimal(28,16) NULL,
	[FXratetoBaseCurrency] decimal(28,16) NULL,
	[Fees] decimal(28,16) NULL,
	[FlexibleCode] [varchar](100) NULL,
	[GMIExchange] [varchar](100) NULL,
	[GMIProductCode] [varchar](100) NULL,
	[GMISubExchange] [varchar](100) NULL,
	[GrossPandLandChangeinOTEBase] decimal(28,16) NULL,
	[MTMClosingTrades] decimal(28,16) NULL,
	[MTMOpeningTrades] decimal(28,16) NULL,
	[MTMPositionsCarriedForward] decimal(28,16) NULL,
	[MTMTotalActivityandPositions] decimal(28,16) NULL,
	[MarginStyle] [varchar](100) NULL,
	[MarketValueOfOptions] [varchar](100) NULL,
	[Multiplier] decimal(28,16) NULL,
	[NetPandL_and_ChangeinOTEBase] decimal(28,16) NULL,
	[NetRealizedProceedsBase] decimal(28,16) NULL,
	[NetRealizedProceedsLocal] decimal(28,16) NULL,
	[OpeningTradeQuantity] decimal(28,16) NULL,
	[OptionPremium_plus_ChangeinMarketValueofOptionsBase] decimal(28,16) NULL,
	[OptionPremium_plus_ChangeinMarketValueofOptionsLocal] decimal(28,16) NULL,
	[OptionsFuturesEquivalent] decimal(28,16) NULL,
	[PrintableStrikePrice] [varchar](100) NULL,
	[PriorLongPosition] decimal(28,16) NULL,
	[PriorMarketValueofOptions] decimal(28,16) NULL,
	[PriorSettlementPrice] decimal(28,16) NULL,
	[PriorShortPosition] decimal(28,16) NULL,
	[ProductCategory] [varchar](100) NULL,
	[ProductID] [varchar](100) NULL,
	[ProductType] [varchar](100) NULL,
	[PromptDay] [varchar](100) NULL,
	[RealizedProceedsBase] decimal(28,16) NULL,
	[RealizedProceedsLocal] decimal(28,16) NULL,
	[ReutersCode] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerGoldmanSachsDailyPNL]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsDailyPNL_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsDailyPNL] CHECK CONSTRAINT [FK_BrokerGoldmanSachsDailyPNL_IntegrationImportRun]


