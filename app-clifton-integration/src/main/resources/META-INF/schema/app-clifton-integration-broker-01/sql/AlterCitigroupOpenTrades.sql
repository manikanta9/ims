

ALTER TABLE [source].[BrokerCitigroupOpenTrades]
ADD 
	[AccountBase] [varchar](100) NULL,
	[FXRate] decimal(28,16) NULL,
	[OpenTradeEquityBase] decimal(28,16) NULL,
	[MarketValueLocal] decimal(28,16) NULL,
	[MarketValueBase] decimal(28,16) NULL,
	[OptionMarketValueLocal] decimal(28,16) NULL,
	[OptionMarketValueBase] decimal(28,16) NULL,
	[NotionalValueBase] decimal(28,16) NULL;
