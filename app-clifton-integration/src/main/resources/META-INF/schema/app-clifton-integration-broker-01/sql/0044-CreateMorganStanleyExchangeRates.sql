IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerMorganStanleyExchangeRates') AND type in (N'U'))
	DROP TABLE source.BrokerMorganStanleyExchangeRates

CREATE TABLE source.BrokerMorganStanleyExchangeRates (
	BrokerMorganStanleyExchangeRatesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	Rate decimal(28,16) NULL,
	FromCurrency varchar(100) NULL,
	RateDate datetime NULL,
	CONSTRAINT PK_BrokerMorganStanleyExchangeRates PRIMARY KEY CLUSTERED (BrokerMorganStanleyExchangeRatesID),
	CONSTRAINT FK_BrokerMorganStanleyExchangeRates_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
