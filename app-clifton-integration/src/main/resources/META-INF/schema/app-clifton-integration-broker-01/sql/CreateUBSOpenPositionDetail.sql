USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerUBSOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerUBSOpenPositionDetail]') AND type in (N'U'))
DROP TABLE [source].[BrokerUBSOpenPositionDetail]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerUBSOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerUBSOpenPositionDetail](
	[BrokerUBSOpenPositionDetailID] [int] IDENTITY(1,1) NOT NULL,	
	[AccountNumber] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[Market] [varchar](100) NULL,
	[CCY] [varchar](100) NULL,
	[Product] [varchar](100) NULL,
	[ProductName] [varchar](100) NULL,
	[Prompt] [varchar](100) NULL,
	[SerialNumber] [varchar](100) NULL,
	[TradeType] [varchar](100) NULL,
	[ExpiryDate] [datetime] NULL,
	[OpenClosed] [varchar](100) NULL,
	[LastTradeDate] [datetime] NULL,
	[ValueDate] [datetime] NULL,
	[Contracts] decimal(28,16) NULL,
	[ClosedContracts] decimal(28,16) NULL,
	[StrikePrice] decimal(28,16) NULL,
	[VariationMargin] decimal(28,16) NULL,
	[INOUT] [varchar](100) NULL,
	[InOutValue] [varchar](100) NULL,
	[Price] decimal(28,16) NULL,
	[Tax] decimal(28,16) NULL,
	[CashValueDate] [datetime] NULL,
	[MarketRate] decimal(28,16) NULL,
	[TradeDate] [datetime] NULL,
	[OriginalCurrency] [varchar](100) NULL,
	[SeriesVersion] [varchar](100) NULL,
	[Covered] [varchar](100) NULL,
	[PayHold] [varchar](100) NULL,
	[RunDate] [datetime] NULL,
	[PremiumRate] decimal(28,16) NULL,
	[PremiumValue] decimal(28,16) NULL,
	[BSMarketPrice] decimal(28,16) NULL,
	[UnderlyingValue] decimal(28,16) NULL,
	[UnderlyingPrice] decimal(28,16) NULL,
	[UnderlyingType] [varchar](100) NULL,
	[ExerciseValue] decimal(28,16) NULL,
	[trader_id] [varchar](100) NULL,
	[ClientSerialNumber] [varchar](100) NULL,
	[SourceAccount] [varchar](100) NULL,
	[ExecutionBroker] [varchar](100) NULL,
	[LinkNumber] [varchar](100) NULL,
	[QuotedInMajorUnits] [varchar](100) NULL,
	[UnitsPerLot] decimal(28,16) NULL,
	[ClientNumber] decimal(28,16) NULL,
	[TradeComments] [varchar](100) NULL,
	[ExecutionType] [varchar](100) NULL,
	[ProductSeries] [varchar](100) NULL,
	[IsInterim] [varchar](100) NULL,
	[FirstNoticeDate] [datetime] NULL,
	[FirmNumber] [varchar](100) NULL,
	[GroupCode1] [varchar](100) NULL,
	[GroupCode2] [varchar](100) NULL,
	[GroupCode3] [varchar](100) NULL,
	[GroupCode4] [varchar](100) NULL,
	[GroupCode5] [varchar](100) NULL,
	[BrokerAccount] [varchar](100) NULL,
	[OtherSideAccount] [varchar](100) NULL,
	[CollateralValue] decimal(28,16) NULL,
	[CouponRate] decimal(28,16) NULL,
	[NextCouponDate] [datetime] NULL,
	[Coupon] decimal(28,16) NULL,
	[Haircut] decimal(28,16) NULL,
	[BBGBackOfficeTicker] [varchar](100) NULL,
	[BBGExchangeCode] [varchar](100) NULL,
	[BBGYellowKey] [varchar](100) NULL,
	[Ledger] [varchar](100) NULL,
	[ContractUnits] decimal(28,16) NULL,
	[GMIProduct] [varchar](100) NULL,
	[GMIMarket] decimal(28,16) NULL,
	[GMILibrary] [varchar](100) NULL,
	[GMIFirm] [varchar](100) NULL,
	[GMIOffice] [varchar](100) NULL,
	[GMIAccount] [varchar](100) NULL,
	[GMIAccountClass] [varchar](100) NULL,
	[GMIAccountSubClass] [varchar](100) NULL,
	[GMISalesman] [varchar](100) NULL,
	[GMIMultiplier] decimal(28,16) NULL,
	[DecimalPrice] decimal(28,16) NULL,
	[DecimalUnderlyingPrice] decimal(28,16) NULL,
	[DecimalMarketRate] decimal(28,16) NULL,
	[OriginalSerial] decimal(28,16) NULL,
	[PitBroker] [varchar](100) NULL,
	[UBS_BSMarketPrice] decimal(28,16) NULL,
	[UBS_MarketRate] decimal(28,16) NULL,
	[UBS_PremiumRate] decimal(28,16) NULL,
	[UBS_Price] decimal(28,16) NULL,
	[UBS_StrikePrice] decimal(28,16) NULL,
	[UBS_UnderlyingPrice] decimal(28,16) NULL,
	[ISIN] [varchar](100) NULL,
	[RoundTurnCommission] [varchar](100) NULL,
	[DeltaFactor] decimal(28,16) NULL,
	[DeltaQuantity] decimal(28,16) NULL,
	[BBGFrontOfficeTicker] [varchar](100) NULL,
	[RIC] [varchar](100) NULL,
	[RootRIC] [varchar](100) NULL,
	[SEDOL] [varchar](100) NULL,
	[InitialCoupon] [varchar](100) NULL,
	[UpfrontFee] [varchar](100) NULL,
	[RedCode] [varchar](100) NULL,
	[TradingSymbol] [varchar](100) NULL,
	[CDSType] [varchar](100) NULL,
	[PreviousCouponDate] [datetime] NULL,
	[FinalCouponDate] [datetime] NULL,
	[DaysInCoupon] [varchar](100) NULL,
	[RestructuringCode] [varchar](100) NULL,
	[DebtType] [varchar](100) NULL,
	[AccruedCoupon] decimal(28,16) NULL,
	[MarkToMarket] decimal(28,16) NULL,
	[CDSFactor] decimal(28,16) NULL,
	[ExchangeProductCode] [varchar](100) NULL,
	[BBGUniqueID] [varchar](100) NULL,
	[SecurityType] [varchar](100) NULL,
	[BaseCCY] [varchar](100) NULL,
	[BusinessSeriesKey] [varchar](100) NULL,
	[DealSpread] [varchar](100) NULL,
	[DealerId] [varchar](100) NULL,
	[TradeExecutionId] [varchar](100) NULL,
	[Tenor] [varchar](100) NULL,
	[SwapType] [datetime] NULL,
	[PAI] decimal(28,16) NULL,
	[PIFactor] decimal(28,16) NULL,
	[EffectiveDate] [datetime] NULL,
	[ClearingDate] [datetime] NULL,
	[IntegrationImportRunID] [int] NOT NULL
 CONSTRAINT [PK_BrokerUBSOpenPositionDetail] PRIMARY KEY CLUSTERED 
(
	[BrokerUBSOpenPositionDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerUBSOpenPositionDetail]  WITH CHECK ADD  CONSTRAINT [FK_BrokerUBSOpenPositionDetail_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerUBSOpenPositionDetail] CHECK CONSTRAINT [FK_BrokerUBSOpenPositionDetail_IntegrationImportRun]


