IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerUBSExchangeRates') AND type in (N'U'))
	DROP TABLE source.BrokerUBSExchangeRates

CREATE TABLE source.BrokerUBSExchangeRates (
	BrokerUBSExchangeRatesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	COBDate datetime NULL,
	FromCCYSymbol varchar(100) NULL,
	ToCCYSymbol varchar(100) NULL,
	FXRate decimal(28,16) NULL,
	CONSTRAINT PK_BrokerUBSExchangeRates PRIMARY KEY CLUSTERED (BrokerUBSExchangeRatesID),
	CONSTRAINT FK_BrokerUBSExchangeRates_IntegrationImportRun_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
