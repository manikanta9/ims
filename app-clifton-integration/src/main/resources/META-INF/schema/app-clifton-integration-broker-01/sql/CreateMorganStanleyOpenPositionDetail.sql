USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerMorganStanleyOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerMorganStanleyOpenPositionDetail]') AND type in (N'U'))
DROP TABLE [source].[BrokerMorganStanleyOpenPositionDetail]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerMorganStanleyOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerMorganStanleyOpenPositionDetail](
	[BrokerMorganStanleyOpenPositionDetailID] [int] IDENTITY(1,1) NOT NULL,	
	[StatementDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[SecuredAccount] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[TradeDate] [datetime] NULL,
	[Exchange] [varchar](100) NULL,
	[ProductDescription] [varchar](100) NULL,
	[ContractMonth] decimal(28,16) NULL,
	[ContractYear] decimal(28,16) NULL,
	[StrikePrice] decimal(28,16) NULL,
	[PutCallInd] [varchar](100) NULL,
	[Quantity] decimal(28,16) NULL,
	[LongShortInd] [varchar](100) NULL,
	[Currency] [varchar](100) NULL,
	[TradePrice] decimal(28,16) NULL,
	[DisplayPriceDecimal] decimal(28,16) NULL,
	[DisplayCode] [varchar](100) NULL,
	[SpreadInd] [varchar](100) NULL,
	[AveragePriceInd] [varchar](100) NULL,
	[NightInd] [varchar](100) NULL,
	[BlockExecutionInd] [varchar](100) NULL,
	[OffExchangeInd] [varchar](100) NULL,
	[MarketValue] decimal(28,16) NULL,
	[MarketValueDrCrInd] [varchar](100) NULL,
	[SettlementDate] [datetime] NULL,
	[SettlementPrice] decimal(28,16) NULL,
	[TradeReferenceNumber] [varchar](100) NULL,
	[TradeVersion] [varchar](100) NULL,
	[Cusip] [varchar](100) NULL,
	[MSSymbol] [varchar](100) NULL,
	[ExchangeContractCode] [varchar](100) NULL,
	[RIC] [varchar](100) NULL,
	[ExpiryDate] [datetime] NULL,
	[ProductType] [varchar](100) NULL,
	[OptionType] [varchar](100) NULL,
	[SegregatedInd] [varchar](100) NULL,
	[OptnStyleCode] [varchar](100) NULL,
	[BloombergTicker] [varchar](100) NULL,
	[BloombergIdentifier] [varchar](100) NULL,
	[BloombergUnique] [varchar](100) NULL,
	[BloombergExchange] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
 CONSTRAINT [PK_BrokerMorganStanleyOpenPositionDetail] PRIMARY KEY CLUSTERED 
(
	[BrokerMorganStanleyOpenPositionDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerMorganStanleyOpenPositionDetail]  WITH CHECK ADD  CONSTRAINT [FK_BrokerMorganStanleyOpenPositionDetail_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerMorganStanleyOpenPositionDetail] CHECK CONSTRAINT [FK_BrokerMorganStanleyOpenPositionDetail_IntegrationImportRun]


