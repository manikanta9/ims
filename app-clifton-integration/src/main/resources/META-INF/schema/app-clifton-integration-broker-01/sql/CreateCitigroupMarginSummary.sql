USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupMarginSummary]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerCitigroupMarginSummary]') AND type in (N'U'))
DROP TABLE [source].[BrokerCitigroupMarginSummary]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupMarginSummary]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON
CREATE TABLE source.BrokerCitigroupMarginSummary
(
	  COBDate DATETIME
	, ACRef VARCHAR(100)
	, ACName VARCHAR(100)
	, Currency VARCHAR(100)
	, TotalCashEquity FLOAT
	, FWDOpenTradeEquity FLOAT
	, InitialMargin FLOAT
	, CollateralMarginValue FLOAT
	, ExcessDeficitCollateral FLOAT
	, ExcessDeficitCash FLOAT
	, IntegrationImportRunID INT NOT NULL
);

SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerCitigroupMarginSummary]  WITH CHECK ADD  CONSTRAINT [FK_BrokerCitigroupMarginSummary_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerCitigroupMarginSummary] CHECK CONSTRAINT [FK_BrokerCitigroupMarginSummary_IntegrationImportRun]
