USE [CliftonIntegration]

ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail ADD
	OTE DECIMAL(28,16) NULL,
	Multiplier DECIMAL(28,16) NULL

