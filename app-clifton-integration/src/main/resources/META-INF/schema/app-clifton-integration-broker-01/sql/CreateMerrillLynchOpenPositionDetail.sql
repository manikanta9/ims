USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerMorganStanleyOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerMerrillLynchOpenPositionDetail](
	[BrokerMerrillLynchOpenPositionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[AccountNumber] [varchar](100) NULL,
	[AccountName] [varchar](100) NULL,
	[ProductDesc] [varchar](100) NULL,
	[Long] decimal(28,16) NULL,
	[Short] decimal(28,16) NULL,
	[TradeDate] [datetime] NULL,
	[TradePrice] [varchar](100) NULL,
	[SettlePrice] [varchar](100) NULL,
	[OTE] decimal(28,16) NULL,
	[Currency] [varchar](100) NULL,
	[MarketValue] [varchar](100) NULL,
	[ContractMultiplier] decimal(28,16) NULL,
	[IntegrationImportRunID] [int] NOT NULL,
 CONSTRAINT [PK_BrokerMerrillLynchOpenPositionDetail] PRIMARY KEY CLUSTERED 
(
	[BrokerMerrillLynchOpenPositionDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



SET ANSI_PADDING OFF


ALTER TABLE [source].[BrokerMerrillLynchOpenPositionDetail]  WITH CHECK ADD  CONSTRAINT [FK_BrokerMerrillLynchOpenPositionDetail_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])


ALTER TABLE [source].[BrokerMerrillLynchOpenPositionDetail] CHECK CONSTRAINT [FK_BrokerMerrillLynchOpenPositionDetail_IntegrationImportRun]


