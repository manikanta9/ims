
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerUBSIntradayTrades') AND type in (N'U'))
	DROP TABLE source.BrokerUBSIntradayTrades
	
CREATE TABLE source.BrokerUBSIntradayTrades (
	BrokerUBSIntradayTradesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	AccountNumber varchar(100) NULL,
	AccountName varchar(100) NULL,
	TradeDate datetime NULL,
	ActivityType1 varchar(100) NULL,
	BloombergBtickSymbol varchar(100) NULL,
	BloombergYellowKey varchar(100) NULL,
	PutCall varchar(100) NULL,
	Strike decimal(28,16) NULL,
	BS varchar(100) NULL,
	Quantity decimal(28,16) NULL,
	TradePrice decimal(28,16) NULL,
	ActivityType2 varchar(100) NULL,
	UniqueTradePositionID varchar(100) NULL,
	ExecutingBroker varchar(100) NULL,
	ExecutionType varchar(100) NULL,
	CONSTRAINT PK_BrokerUBSIntradayTrades PRIMARY KEY CLUSTERED (BrokerUBSIntradayTradesID),
	CONSTRAINT FK_BrokerUBSIntradayTrades_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
