USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupOpenTrades]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerCitigroupOpenTrades]') AND type in (N'U'))
DROP TABLE [source].[BrokerCitigroupOpenTrades]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupOpenTrades]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerCitigroupOpenTrades](
	[TradeDate] [datetime] NULL,
	[ACRef] [varchar](100) NULL,
	[ACName] [varchar](100) NULL,
	[TradeType] [varchar](100) NULL,
	[BS] [varchar](100) NULL,
	[Lots] decimal(28,16) NULL,
	[Exchange] [varchar](100) NULL,
	[Contract] [varchar](100) NULL,
	[Delivery] [varchar](100) NULL,
	[Strike] [varchar](100) NULL,
	[RIC] [varchar](100) NULL,
	[ContractSymbol] [varchar](100) NULL,
	[Type] [varchar](100) NULL,
	[CallPut] [varchar](100) NULL,
	[AmerEuro] [varchar](100) NULL,
	[Price] decimal(28,16) NULL,
	[Curr] [varchar](100) NULL,
	[OpenTradeEquity] decimal(28,16) NULL,
	[OptionValuation] decimal(28,16) NULL,
	[KnockInOut] [varchar](100) NULL,
	[Barrier1] [varchar](100) NULL,
	[Barrier2] [varchar](100) NULL,
	[PositionDate] [datetime] NOT NULL,
	[Bloomberg] [varchar](100) NULL,
	[SettlementPrice] decimal(28,16) NULL,
	[NotionalValue] decimal(28,16) NULL,
	[IntegrationImportRunID] [int] NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerCitigroupOpenTrades]  WITH CHECK ADD  CONSTRAINT [FK_BrokerCitigroupOpenTrades_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerCitigroupOpenTrades] CHECK CONSTRAINT [FK_BrokerCitigroupOpenTrades_IntegrationImportRun]


