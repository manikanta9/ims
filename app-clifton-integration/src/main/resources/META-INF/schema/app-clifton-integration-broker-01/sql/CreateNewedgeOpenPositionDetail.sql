USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerNewedgeOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerNewedgeOpenPositionDetail]') AND type in (N'U'))
DROP TABLE [source].[BrokerNewedgeOpenPositionDetail]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerNewedgeOpenPositionDetail]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerNewedgeOpenPositionDetail](
	[BrokerNewedgeOpenPositionDetailID] [int] IDENTITY(1,1) NOT NULL,
	[TradeDate] [datetime] NULL,
	[SettlementDate] [datetime] NULL,
	[BuySellSide] [varchar](100) NULL,
	[ExecutingBrokerName] [varchar](100) NULL,
	[PositionIdentifier] [varchar](100) NULL,
	[BrokerUsageCode] [varchar](100) NULL,
	[BrokerName] [varchar](100) NULL,
	[OptionsEquiv] decimal(28,16) NULL,
	[AccountCurrencySymbol] [varchar](100) NULL,
	[ProductCurrencySymbol] [varchar](100) NULL,
	[BloombergTickerSymbol] [varchar](100) NULL,
	[BloombergKey] [varchar](100) NULL,
	[Strike] decimal(28,16) NULL,
	[OptionType] [varchar](100) NULL,
	[Coupon] decimal(28,16) NULL,
	[MarketPrice] decimal(28,16) NULL,
	[Office] [varchar](100) NULL,
	[AccountNumber] [varchar](100) NULL,
	[Notional] decimal(28,16) NULL,
	[OTE] decimal(28,16) NULL,
	[OOTE] decimal(28,16) NULL,
	[LOTE] decimal(28,16) NULL,
	[TradePrice] decimal(28,16) NULL,
	[ClosePrice] decimal(28,16) NULL,
	[MarketValue] decimal(28,16) NULL,
	[CommissionAmount] decimal(28,16) NULL,
	[FXRate] decimal(28,16) NULL,
	[QuantitySigned] decimal(28,16) NULL,
	[LastBusinessDate] [datetime] NULL,
	[IntegrationImportRunID] [int] NOT NULL
 CONSTRAINT [PK_BrokerNewedgeOpenPositionDetail] PRIMARY KEY CLUSTERED 
(
	[BrokerNewedgeOpenPositionDetailID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerNewedgeOpenPositionDetail]  WITH CHECK ADD  CONSTRAINT [FK_BrokerNewedgeOpenPositionDetail_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerNewedgeOpenPositionDetail] CHECK CONSTRAINT [FK_BrokerNewedgeOpenPositionDetail_IntegrationImportRun]


