USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerCitigroupIntradayTrades]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerCitigroupIntradayTrades]') AND type in (N'U'))
DROP TABLE [source].[BrokerCitigroupIntradayTrades]

CREATE TABLE [source].[BrokerCitigroupIntradayTrades](
	[BrokerCitigroupIntradayTradesID] [int] IDENTITY(1,1) NOT NULL
	, TradeDate VARCHAR(100)
	, ACRef VARCHAR(100)
	, ACName VARCHAR(100)
	, TradeType VARCHAR(100)
	, BS VARCHAR(100)
	, Lots INT
	, Exchange VARCHAR(100)
	, Contract VARCHAR(100)
	, Delivery VARCHAR(100)
	, Strike VARCHAR(100)
	, Type VARCHAR(100)
	, CallPut VARCHAR(100)
	, AmerEuro VARCHAR(100)
	, Price FLOAT
	, Curr VARCHAR(100)
	, RIC VARCHAR(100)
	, ExchContractSymbol VARCHAR(100)
	, USESTTimestamp DATETIME
	, BrokerCode VARCHAR(100)
	, BrokerName VARCHAR(100)
	, ExecFirmCode VARCHAR(100)
	, ExecFirmName VARCHAR(100)
	, TradeOK VARCHAR(100)
	, LastChangedBy VARCHAR(100)
	, TradeID VARCHAR(100)
	, Bloomberg VARCHAR(100)
	, BBGYellowKey VARCHAR(100)
	, IntegrationImportRunID INT NOT NULL
);
ALTER TABLE [source].[BrokerCitigroupIntradayTrades] ADD CONSTRAINT [PK_BrokerCitigroupIntradayTrades]
	PRIMARY KEY CLUSTERED (BrokerCitigroupIntradayTradesID);
   

ALTER TABLE [source].[BrokerCitigroupIntradayTrades]  WITH CHECK ADD  CONSTRAINT [FK_BrokerCitigroupIntradayTrades_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerCitigroupIntradayTrades] CHECK CONSTRAINT [FK_BrokerCitigroupIntradayTrades_IntegrationImportRun]

CREATE INDEX [ix_BrokerCitigroupIntradayTrades_IntegrationImportRun] ON [source].[BrokerCitigroupIntradayTrades] (IntegrationImportRunID);



