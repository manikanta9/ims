
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerUBSOpenPositionDetail') AND type in (N'U'))
	DROP TABLE source.BrokerUBSOpenPositionDetail
	
CREATE TABLE source.BrokerUBSOpenPositionDetail (
	BrokerUBSOpenPositionDetailID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	AccountNumber varchar(50) null,
	AccountName varchar(200) null,
	TradeDate datetime null,
	PositionDateCOBDate datetime null,
	BloombergBtickSymbol varchar(50) null,
	BloombergYellowKey varchar(50) null,
	PutCall varchar(100) null,
	Strike decimal(28, 16) null,
	BS varchar(50) null,
	Quantity decimal(28, 16) null,
	TradePrice decimal(28, 16) null,
	COBPrice decimal(28, 16) null,
	NotionalLocal decimal(28, 16) null,
	NotionalBase decimal(28, 16) null,
	OpenMarketValueLocal decimal(28, 16) null,
	OpenMarketValueBase decimal(28, 16) null,
	OpenTradeEquityLocal decimal(28, 16) null,
	OpenTradeEquityBase decimal(28, 16) null,
	OptionPremium decimal(28, 16) null,
	OptionMarketValue decimal(28, 16) null,
	UniqueTradePositionID varchar(50) null,
	AccountBaseCCYSymbol varchar(50) null,
	PositionCCYSymbol varchar(50) null,
	FXRate decimal(28, 16) null,
	ContractPriceMultiplier decimal(28, 16) null,
	Exchange varchar(50) null,
	ExecutingBroker varchar(50) null,
	ExecutionType varchar(50) null,
	CONSTRAINT PK_BrokerUBSOpenPositionDetail PRIMARY KEY CLUSTERED (BrokerUBSOpenPositionDetailID),
	CONSTRAINT FK_BrokerUBSOpenPositionDetail_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
