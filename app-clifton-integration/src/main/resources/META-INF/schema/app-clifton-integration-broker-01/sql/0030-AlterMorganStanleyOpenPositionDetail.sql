USE [CliftonIntegration]

DELETE FROM source.BrokerMorganStanleyOpenPositionDetail

DELETE FROM IntegrationImportRun
	WHERE IntegrationImportRunID IN (SELECT DISTINCT IntegrationImportRunID FROM source.BrokerMorganStanleyOpenPositionDetail)

	
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	DROP COLUMN OTE
	
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	ADD PrincipalCostBasisColumn varchar(100) null
	
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	ADD MktValSettlementPriceValue varchar(100) null
	
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	ADD ConvertedMarketValue DECIMAL(28,16) null
					
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	ADD ConvertedPrincipalValue DECIMAL(28,16) null
	
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	ADD ConvertedSettlementPrice DECIMAL(28,16) null
	
ALTER TABLE source.BrokerMorganStanleyOpenPositionDetail
	ADD FxRate DECIMAL(28,16) null
