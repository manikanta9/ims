	EXEC sp_rename 'source.BrokerCitigroupOpenTrades', 'BrokerCitigroupOpenPositionDetail'
	EXEC sp_rename 'source.BrokerGoldmanSachsOpenPositions', 'BrokerGoldmanSachsOpenPositionDetail'
	
	
	ALTER TABLE source.BrokerGoldmanSachsOpenPositionDetail ADD BrokerGoldmanSachsOpenPositionDetailID int IDENTITY(1,1) NOT NULL

	ALTER TABLE source.BrokerGoldmanSachsOpenPositionDetail ADD CONSTRAINT PK_BrokerGoldmanSachsOpenPositionDetail
	      PRIMARY KEY CLUSTERED (BrokerGoldmanSachsOpenPositionDetailID);
	
	
	ALTER TABLE source.BrokerCitigroupOpenPositionDetail ADD BrokerCitigroupOpenPositionDetailID int IDENTITY(1,1) NOT NULL

	ALTER TABLE source.BrokerCitigroupOpenPositionDetail ADD CONSTRAINT PK_BrokerCitigroupOpenPositionDetail
	      PRIMARY KEY CLUSTERED (BrokerCitigroupOpenPositionDetailID);


	UPDATE dbo.IntegrationImportDefinition 
		SET ImportDefinitionName = 'CitigroupOpenPositionDetailImport' 
		WHERE ImportDefinitionName = 'CitigroupOpenTradesImport';
		
	UPDATE dbo.IntegrationImportDefinition 
		SET ImportDefinitionName = 'GoldmanSachsOpenPositionDetailImport' 
		WHERE ImportDefinitionName = 'GoldmanSachsOpenPositionsImport';
