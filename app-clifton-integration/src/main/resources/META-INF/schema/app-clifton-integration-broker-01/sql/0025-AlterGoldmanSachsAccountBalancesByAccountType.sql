IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IntegrationReconcileM2MDaily')
BEGIN
	EXEC sp_rename 'IntegrationReconcileM2MDaily.ExpectedTransferAmount', 'ExpectedTransferAmountBase', 'COLUMN'

	ALTER TABLE IntegrationReconcileM2MDaily ADD ExpectedTransferAmountLocal decimal(19, 2) NULL
END
