
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerUBSProfitAndLoss') AND type in (N'U'))
	DROP TABLE source.BrokerUBSProfitAndLoss
	
CREATE TABLE source.BrokerUBSProfitAndLoss (
	BrokerUBSProfitAndLossID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	AccountNumber varchar(100) NULL,
	AccountName varchar(100) NULL,
	TradeDate datetime NULL,
	ActivityType1 varchar(100) NULL,
	BloombergBtickSymbol varchar(100) NULL,
	BloombergYellowKey varchar(100) NULL,
	PutCall varchar(100) NULL,
	Strike decimal(28,16) NULL,
	BS varchar(100) NULL,
	Quantity decimal(28,16) NULL,
	TradePrice decimal(28,16) NULL,
	ActivityType2 varchar(100) NULL,
	UniqueTradePositionID varchar(100) NULL,
	ExecutingBroker varchar(100) NULL,
	ExecutionType varchar(100) NULL,
	NumContractsRealized decimal(28,16) NULL,
	PurchaseSaleRealizedAmountLocal decimal(28,16) NULL,
	PurchaseSaleRealizedAmountBase decimal(28,16) NULL,
	CommissionsLocal decimal(28,16) NULL,
	CommissionsBase decimal(28,16) NULL,
	ExecutionFeesLocal decimal(28,16) NULL,
	ExecutionFeesBase decimal(28,16) NULL,
	NFATaxesLocal decimal(28,16) NULL,
	NFATaxesBase decimal(28,16) NULL,
	AnyOtherFeeLocal decimal(28,16) NULL,
	AnyOtherFeeBase decimal(28,16) NULL,
	TotalCommsFeesLocal decimal(28,16) NULL,
	TotalCommsFeesBase decimal(28,16) NULL,
	FXRate decimal(28,16) NULL,
	Exchange varchar(100) NULL,
	CONSTRAINT PK_BrokerUBSProfitAndLoss PRIMARY KEY CLUSTERED (BrokerUBSProfitAndLossID),
	CONSTRAINT FK_BrokerUBSProfitAndLoss_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
