USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsOpenPositions]    Script Date: 11/03/2010 10:22:49 ******/
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[source].[BrokerGoldmanSachsOpenPositions]') AND type in (N'U'))
DROP TABLE [source].[BrokerGoldmanSachsOpenPositions]


USE [CliftonIntegration]


/****** Object:  Table [source].[BrokerGoldmanSachsOpenPositions]    Script Date: 11/03/2010 10:22:49 ******/
SET ANSI_NULLS ON


SET QUOTED_IDENTIFIER ON


SET ANSI_PADDING ON

CREATE TABLE [source].[BrokerGoldmanSachsOpenPositions](
	[COBDate] [datetime] NULL,
	[Account] [varchar](100) NULL,
	[AccountName] [varchar](250) NULL,
	[Market] [varchar](100) NULL,
	[TradeDate] [datetime] NULL,
	[LongQty] [int] NULL,
	[ShortQty] [int] NULL,
	[Curr] [varchar](100) NULL,
	[GMIExchCode] [int] NULL,
	[GMIProdCode] [varchar](100) NULL,
	[Product] [varchar](100) NULL,
	[MONTH] [int] NULL,
	[YEAR] [int] NULL,
	[ContractDay] [int] NULL,
	[PC] [varchar](100) NULL,
	[StrikePrice] decimal(28,16) NULL,
	[TradePrice] decimal(28,16) NULL,
	[StlPrice] decimal(28,16) NULL,
	[AcctBaseCurr] [varchar](100) NULL,
	[AcctType] [int] NULL,
	[AverageTradePrice] decimal(28,16) NULL,
	[BloombergCode] [varchar](100) NULL,
	[BloombergUniqueCode] [varchar](100) NULL,
	[BuySell] [varchar](100) NULL,
	[ClientAccountNumber] [varchar](100) NULL,
	[ClientProductCode] [varchar](100) NULL,
	[ContractKey] [varchar](100) NULL,
	[ContractMult] decimal(28,16) NULL,
	[Country] [varchar](100) NULL,
	[CouponRate] decimal(28,16) NULL,
	[DiscountedVariationMargin] decimal(28,16) NULL,
	[DiscountFactor] decimal(28,16) NULL,
	[DisplayStlPrice] [varchar](100) NULL,
	[DisplayStrikePrice] decimal(28,16) NULL,
	[DisplayTradePrice] decimal(28,16) NULL,
	[EODExchOptionDelta] decimal(28,16) NULL,
	[EquitySymbol] [varchar](100) NULL,
	[ExposureBase] decimal(28,16) NULL,
	[ExposureBaseAbsolute] decimal(28,16) NULL,
	[ExposureLocal] decimal(28,16) NULL,
	[ExposureLocalAbsolute] decimal(28,16) NULL,
	[FXRate] decimal(28,16) NULL,
	[FirstDeliveryDate] [datetime] NULL,
	[FirstNoticeDate] [datetime] NULL,
	[FlexibleCode] [varchar](100) NULL,
	[GMISubExchCode] [varchar](100) NULL,
	[ImpliedMarketValue] decimal(28,16) NULL,
	[InstrumentCode] [varchar](100) NULL,
	[LastDeliveryDate] [datetime] NULL,
	[LastNoticeDate] [datetime] NULL,
	[LastTradeDate] [datetime] NULL,
	[LongAverageTradePrice] decimal(28,16) NULL,
	[MarginGroupCode] [varchar](100) NULL,
	[NetQty] decimal(28,16) NULL,
	[NotionalValueBase] decimal(28,16) NULL,
	[NotionalValueLocal] decimal(28,16) NULL,
	[OpenTradeEquityBase] decimal(28,16) NULL,
	[OpenTradeEquityLocal] decimal(28,16) NULL,
	[OptionMarketValueBase] decimal(28,16) NULL,
	[OptionMarketValueLocal] decimal(28,16) NULL,
	[OptionStyle] [varchar](100) NULL,
	[OptionsFuturesEquivalent] decimal(28,16) NULL,
	[OptionExpirationDate] [datetime] NULL,
	[OptionPremiumBase] decimal(28,16) NULL,
	[OptionPremiumLocal] decimal(28,16) NULL,
	[ProductCategory] [varchar](100) NULL,
	[ProductID] [int] NULL,
	[ProductTypeCode] [varchar](100) NULL,
	[ReutersCode] [varchar](100) NULL,
	[SettlementStyle] [varchar](100) NULL,
	[ShortAverageTradePrice] decimal(28,16) NULL,
	[SpreadCode] [varchar](100) NULL,
	[StlDate] [datetime] NULL,
	[Ticker] [varchar](100) NULL,
	[UniquePositionID] [varchar](100) NULL,
	[UnsettledPandSCode] [varchar](100) NULL,
	[IntegrationImportRunID] [int] NOT NULL
) ON [PRIMARY]


SET ANSI_PADDING OFF

ALTER TABLE [source].[BrokerGoldmanSachsOpenPositions]  WITH CHECK ADD  CONSTRAINT [FK_BrokerGoldmanSachsOpenPositions_IntegrationImportRun] FOREIGN KEY([IntegrationImportRunID])
REFERENCES [dbo].[IntegrationImportRun] ([IntegrationImportRunID])

ALTER TABLE [source].[BrokerGoldmanSachsOpenPositions] CHECK CONSTRAINT [FK_BrokerGoldmanSachsOpenPositions_IntegrationImportRun]


