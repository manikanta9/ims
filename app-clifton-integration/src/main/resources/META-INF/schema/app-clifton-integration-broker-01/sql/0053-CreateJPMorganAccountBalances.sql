
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'source.BrokerJPMorganAccountBalances') AND type in (N'U'))
	DROP TABLE source.BrokerJPMorganAccountBalances

CREATE TABLE source.BrokerJPMorganAccountBalances (
	BrokerJPMorganAccountBalancesID int IDENTITY(1,1)NOT NULL,
	IntegrationImportRunID int NOT NULL,
	CustomAccount varchar(100) NULL,
	JPMAccount varchar(100) NULL,
	StatementDate datetime NULL,
	Currency varchar(100) NULL,
	OpeningBalance decimal(28,16) NULL,
	Commission decimal(28,16) NULL,
	OptionPremium decimal(28,16) NULL,
	JournalItems decimal(28,16) NULL,
	GrossRealisedProfitLoss decimal(28,16) NULL,
	ClosingBalance decimal(28,16) NULL,
	OpenTradeEquity decimal(28,16) NULL,
	TotalEquity decimal(28,16) NULL,
	NetMarketValueOfOptions decimal(28,16) NULL,
	InitialMarginRequirement decimal(28,16) NULL,
	MaintenanceMarginRequirement decimal(28,16) NULL,
	MarginValueSecurities decimal(28,16) NULL,
	AFRExcessCashEquity decimal(28,16) NULL,
	NetLiquidatingValue decimal(28,16) NULL,
	ConversionRate decimal(28,16) NULL,
	ConvNetLiquidatingValue decimal(28,16) NULL,
	AccountType varchar(100) NULL,
	CustomAccountName varchar(100) NULL,
	JapaneseInitialRequirement decimal(28,16) NULL,
	JpmAccountName varchar(100) NULL,
	StmntRecapCatDescription varchar(100) NULL,
	TransactionSourceType varchar(100) NULL,
	PendingCashTotal decimal(28,16) NULL,
	PendingTradeDateBalance decimal(28,16) NULL,
	PendingIncrementalExcDef decimal(28,16) NULL,
	CONSTRAINT PK_BrokerJPMorganAccountBalances PRIMARY KEY CLUSTERED (BrokerJPMorganAccountBalancesID),
	CONSTRAINT FK_BrokerJPMorganAccountBalances_IntegrationImportRunID FOREIGN KEY (IntegrationImportRunID)
		REFERENCES dbo.IntegrationImportRun(IntegrationImportRunID)
)
