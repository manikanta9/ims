IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'db_executor')
	BEGIN
		CREATE ROLE db_executor
		GRANT EXECUTE TO db_executor
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'PARAPORT\MPLS SQL QA Readers')
	BEGIN
		CREATE USER [PARAPORT\MPLS SQL QA Readers] FOR LOGIN [PARAPORT\MPLS SQL QA Readers];
	END
EXEC sp_addrolemember 'db_datareader', 'PARAPORT\MPLS SQL QA Readers';
EXEC sp_addrolemember 'db_executor', 'PARAPORT\MPLS SQL QA Readers';


-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group - note that the group is assumed to exist and will not be created
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'
UNION
-- PW: password2
SELECT 'imstestuser2', 'Test User 2', NULL
UNION
-- PW: password3
SELECT 'imstestuser3', 'Test User 3', NULL
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', 'Analysts|'
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', 'Documentation Team|'
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|Compliance Violation Support|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trade Confirmation|Analysts|Portfolio Managers|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', 'Client Admins|Investment Account Admins|Reporting Admins|Operations|'

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)


INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================


UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';

UPDATE SystemDataSource
SET DatabaseName  = 'dummyMarkitDataSource',
	ConnectionUrl = ''
WHERE DataSourceName = 'markitDataSource';

UPDATE SystemDataSource
SET DatabaseName  = 'dummyMarkitEdmDataSource',
	ConnectionUrl = ''
WHERE DataSourceName = 'markitEDMDataSource';

UPDATE SystemDataSource
SET DatabaseName  = 'dummyUnionDataSource',
	ConnectionUrl = ''
WHERE DataSourceName = 'unionDataSource';

UPDATE SystemDataSource
SET DatabaseName  = 'dummyApxDataSource',
	ConnectionUrl = ''
WHERE DataSourceName = 'apxDataSource';

UPDATE SystemDataSource
SET DatabaseName  = 'dummySscDataSource',
	ConnectionUrl = ''
WHERE DataSourceName = 'sscReconDataSource';

UPDATE SystemDataSource
SET DatabaseName  = 'dummyMagnetDataSource',
	ConnectionUrl = ''
WHERE DataSourceName = 'magnetDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@imsDataSource.databaseName@',
	ConnectionUrl = '@imsDataSource.url@'
WHERE DataSourceName = 'imsDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@deltaDataSource.databaseName@',
	ConnectionUrl = '@deltaDataSource.url@'
WHERE DataSourceName = 'deltaDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@corpDataSource.databaseName@',
	ConnectionUrl = '@corpDataSource.url@'
WHERE DataSourceName = 'corpDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@munisDataSource.databaseName@',
	ConnectionUrl = '@munisDataSource.url@'
WHERE DataSourceName = 'munisDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@corpLdiDataSource.databaseName@',
	ConnectionUrl = '@corpLdiDataSource.url@'
WHERE DataSourceName = 'corpLdiDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@axiomDataSource.databaseName@',
	ConnectionUrl = '@axiomDataSource.url@'
WHERE DataSourceName = 'axiomDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@crdDataSource.databaseName@',
	ConnectionUrl = '@crdDataSource.url@'
WHERE DataSourceName = 'crdDataSource';


-- DISABLE ALL BATCH JOBS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'BatchJob')
	BEGIN
		UPDATE BatchJob
		SET IsEnabled = 0;
	END

-- DISABLE ALL NOTIFICATIONS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'NotificationDefinition')
	BEGIN
		UPDATE NotificationDefinition
		SET IsActive = 0
	END

-- DISABLE ALL WORKFLOW TASK DEFINITIONS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'WorkflowTaskDefinition')
	BEGIN
		UPDATE WorkflowTaskDefinition
		SET IsActive = 0
	END

-- DISABLE ALL EXPORTS and remove schedules from all Exports
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'ExportDefinition')
	BEGIN
		UPDATE ExportDefinition
		SET StartDate = DATEADD(DAY, -10, CONVERT(DATE, GETDATE())),
			EndDate   = DATEADD(DAY, -10, CONVERT(DATE, GETDATE()))

		UPDATE ExportDefinition
		SET RecurrenceScheduleID = NULL
	END


--Disable all Export destinations (Need to check because of PIOS test db)
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.COLUMNS
		  WHERE TABLE_NAME = 'ExportDefinitionDestination'
			AND COLUMN_NAME = 'IsDisabled')
	BEGIN
		EXEC ('UPDATE ExportDefinitionDestination SET IsDisabled = 1')
	END





