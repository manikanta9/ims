IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'db_executor')
	BEGIN
		CREATE ROLE db_executor
		GRANT EXECUTE TO db_executor
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'PARAPORT\MPLS SQL QA Readers')
	BEGIN
		CREATE USER [PARAPORT\MPLS SQL QA Readers] FOR LOGIN [PARAPORT\MPLS SQL QA Readers];
	END
EXEC sp_addrolemember 'db_datareader', 'PARAPORT\MPLS SQL QA Readers';
EXEC sp_addrolemember 'db_executor', 'PARAPORT\MPLS SQL QA Readers';


-- ==============================================================
-- ==============================================================
-- Test Users Begin
-- The following users can be used to log into the system.  If a group with the given group name (separated by |) exists, then that test user will be assigned to that group - note that the group is assumed to exist and will not be created
-- ==============================================================
-- ==============================================================
DECLARE @TestUsers TABLE (
	UserName    NVARCHAR(50),
	DisplayName NVARCHAR(50),
	GroupName   NVARCHAR(500)
)
INSERT INTO @TestUsers
	-- PW: password1
SELECT 'imstestuser1', 'Test Admin User', 'Administrators|'
UNION
-- PW: password2
SELECT 'imstestuser2', 'Test User 2', NULL
UNION
-- PW: password3
SELECT 'imstestuser3', 'Test User 3', NULL
UNION
-- PW: test_pw2
SELECT 'imsanalysttestuser', 'Test Analyst User', 'Analysts|'
UNION
-- PW: test_pw2
SELECT 'imsdocumenttestuser', 'Test Documentation User', 'Documentation Team|'
UNION
-- PW: test_pw2
SELECT 'imscompliancetestuser', 'Test Compliance User', 'Compliance|Compliance Violation Support|'
UNION
-- PW: test_pw2
SELECT 'imstradertestuser', 'Trading Test User', 'Trade Confirmation|Analysts|Portfolio Managers|'
UNION
-- PW: test_pw3
SELECT 'imsnopermissionuser', 'No Permission Test User', NULL
UNION
-- PW: pw_imsclientadminuser
SELECT 'imsclientadminuser', 'Client Admin Test User', 'Client Admins|Investment Account Admins|Reporting Admins|Operations|'

INSERT INTO SecurityUser (SecurityUserName, IntegrationPseudonym, DisplayName, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT x.UserName, x.UserName, x.DisplayName, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
WHERE NOT EXISTS(SELECT * FROM SecurityUser u WHERE u.SecurityUserName = x.UserName)


INSERT INTO SecurityUserGroup (SecurityUserID, SecurityGroupID, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
SELECT u.SecurityUserID, g.SecurityGroupID, 0, GETDATE(), 0, GETDATE()
FROM @TestUsers x
	 INNER JOIN SecurityUser u ON x.UserName = u.SecurityUserName
	 INNER JOIN SecurityGroup g ON x.GroupName LIKE '%' + g.SecurityGroupName + '|%'
WHERE NOT EXISTS(SELECT * FROM SecurityUserGroup ug WHERE u.SecurityUserID = ug.SecurityUserID AND g.SecurityGroupID = ug.SecurityGroupID)

-- ==============================================================
-- ==============================================================
-- Test Users End
-- ==============================================================
-- ==============================================================


UPDATE SystemDataSource
SET DatabaseName  = '@dataSource.databaseName@',
	ConnectionUrl = '@dataSource.url@'
WHERE DataSourceName = 'dataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@markitDataSource.databaseName@',
	ConnectionUrl = '@markitDataSource.url@'
WHERE DataSourceName = 'markitDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@markitEDMDataSource.databaseName@',
	ConnectionUrl = '@markitEDMDataSource.url@'
WHERE DataSourceName = 'markitEDMDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@unionDataSource.databaseName@',
	ConnectionUrl = '@unionDataSource.url@'
WHERE DataSourceName = 'unionDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@apxDataSource.databaseName@',
	ConnectionUrl = '@apxDataSource.url@'
WHERE DataSourceName = 'apxDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@sscReconDataSource.databaseName@',
	ConnectionUrl = '@sscReconDataSource.url@'
WHERE DataSourceName = 'sscReconDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@magnetDataSource.databaseName@',
	ConnectionUrl = '@magnetDataSource.url@'
WHERE DataSourceName = 'magnetDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@imsDataSource.databaseName@',
	ConnectionUrl = '@imsDataSource.url@'
WHERE DataSourceName = 'imsDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@deltaDataSource.databaseName@',
	ConnectionUrl = '@deltaDataSource.url@'
WHERE DataSourceName = 'deltaDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@corpDataSource.databaseName@',
	ConnectionUrl = '@corpDataSource.url@'
WHERE DataSourceName = 'corpDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@munisDataSource.databaseName@',
	ConnectionUrl = '@munisDataSource.url@'
WHERE DataSourceName = 'munisDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@corpLdiDataSource.databaseName@',
	ConnectionUrl = '@corpLdiDataSource.url@'
WHERE DataSourceName = 'corpLdiDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@axiomDataSource.databaseName@',
	ConnectionUrl = '@axiomDataSource.url@'
WHERE DataSourceName = 'axiomDataSource';

UPDATE SystemDataSource
SET DatabaseName  = '@crdDataSource.databaseName@',
	ConnectionUrl = '@crdDataSource.url@'
WHERE DataSourceName = 'crdDataSource';

-- DISABLE ALL BATCH JOBS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'BatchJob')
	BEGIN
		UPDATE BatchJob
		SET IsEnabled = 0;
	END

-- DISABLE ALL NOTIFICATIONS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'NotificationDefinition')
	BEGIN
		UPDATE NotificationDefinition
		SET IsActive = 0
	END

-- DISABLE ALL WORKFLOW TASK DEFINITIONS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'WorkflowTaskDefinition')
	BEGIN
		UPDATE WorkflowTaskDefinition
		SET IsActive = 0
	END

-- DISABLE ALL EXPORTS
IF EXISTS(SELECT *
		  FROM INFORMATION_SCHEMA.TABLES
		  WHERE TABLE_NAME = 'ExportDefinition')
	BEGIN
		UPDATE ExportDefinition
		SET StartDate = DATEADD(DAY, -10, CONVERT(DATE, GETDATE())),
			EndDate   = DATEADD(DAY, -10, CONVERT(DATE, GETDATE()))
	END

-- IN ORDER TO MAKE RESTORES MORE EFFICIENT
-- ACTUAL SP HAS BEEN ADDED TO PRODUCTION
-- CREATE PROC [dbo].[spManagerDownloadUpdateFromProduction]
-- HOWEVER THERE IS A CHECK ON THE SERVER NAME THAT ENSURE IT'S NEVER
-- ACTUALLY EXECUTED ON PRODUCTION
--	IF (@@SERVERNAME = 'TCGIMSSQL')
--	BEGIN
--		RAISERROR('CANNOT EXECUTE spManagerDownloadUpdateFromProduction ON PRODUCTION', 16, 1)
--		RETURN
--	END
-- THE BATCH JOB AND SYSTEM BEAN THAT CALL THAT SP ARE ADDED AFTER EACH RESTORE


DECLARE
	@BeanTypeID INT,
	@BeanID INT,
	@Name NVARCHAR(50) = '** QA Copy Manager Downloads from Production',
	@Description NVARCHAR(100) = '** QA copies Manager Download Data from Production for past 2 business days'

DECLARE @BatchJobCategoryID INT
SELECT @BatchJobCategoryID = BatchJobCategoryID
FROM BatchJobCategory
WHERE CategoryName = 'Other'

IF NOT EXISTS(SELECT *
			  FROM BatchJob
			  WHERE JobName = @Name)
	BEGIN
		SELECT @BeanTypeID = SystemBeanTypeID
		FROM SystemBeanType
		WHERE SystemBeanTypeName = 'Stored Procedure Executor'

		INSERT INTO SystemBean (SystemBeanTypeID, SystemBeanName, SystemBeanDescription, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanTypeID, @Name, @Description, 0, GETDATE(), 0, GETDATE())


		SELECT @BeanID = SystemBeanID
		FROM SystemBean
		WHERE SystemBeanName = @Name

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Stored Procedure Name'), 'spManagerDownloadUpdateFromProduction',
				'spManagerDownloadUpdateFromProduction', 0, GETDATE(), 0, GETDATE())

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Data Source Name'), 'dataSource', 'dataSource', 0, GETDATE(),
				0, GETDATE())

		INSERT INTO SystemBeanProperty (SystemBeanID, SystemBeanPropertyTypeID, Value, Text, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BeanID, (SELECT SystemBeanPropertyTypeID
						  FROM SystemBeanPropertyType
						  WHERE SystemBeanTypeID = @BeanTypeID
							AND SystemBeanPropertyTypeName = 'Transaction Timeout (Seconds)'), '300', '300', 0, GETDATE(), 0, GETDATE())


		INSERT INTO BatchJob (BatchJobCategoryID, SystemBeanID, CalendarScheduleID, JobName, JobDescription, IsEnabled, CreateUserID, CreateDate, UpdateUserID, UpdateDate)
		VALUES (@BatchJobCategoryID, @BeanID, (SELECT CalendarScheduleID
											   FROM CalendarSchedule
											   WHERE ScheduleName = 'Every Day at 8:00 AM'), @Name, @Description, 1, 0, GETDATE(), 0, GETDATE())
	END
