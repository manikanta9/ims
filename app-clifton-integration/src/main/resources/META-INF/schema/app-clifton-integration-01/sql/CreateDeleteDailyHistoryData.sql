-- exec UpdateManagerPositionType 4,'11/29/10'

CREATE PROCEDURE DeleteDailyHistoryData
	@IntegrationImportRunID INT,
	@Table nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	exec('DELETE FROM dbo.' + @Table + '
	WHERE IntegrationImportRunID IN
	(
		SELECT IntegrationImportRunID
		FROM IntegrationImportRun
		WHERE EffectiveDate = (SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = ' + @IntegrationImportRunID + ')
			AND IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = ' + @IntegrationImportRunID + ')
	)');
	
END


