CREATE FUNCTION GetDailyRunList
(
	@IntegrationImportRunID INT
)
RETURNS TABLE
AS
RETURN 
(
	SELECT IntegrationImportRunID
	FROM IntegrationImportRun r
		INNER JOIN IntegrationFile f ON f.IntegrationFileID = r.IntegrationFileID
	WHERE r.EffectiveDate = (SELECT EffectiveDate FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID)
		AND IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID)
		AND f.IntegrationFileDefinitionID = 
		(
			SELECT f.IntegrationFileDefinitionID
			FROM IntegrationImportRun r
				INNER JOIN IntegrationFile f ON f.IntegrationFileID = r.IntegrationFileID
			WHERE IntegrationImportRunID = @IntegrationImportRunID
		)
)
