-- exec UpdateManagerPositionType 7,'11/18/10'

CREATE PROCEDURE SetPreviousImportRunStatus
	@IntegrationImportRunID INT,
	@EffectiveDate DATETIME,
	@Status nvarchar(50)
AS
BEGIN
	
	SET NOCOUNT ON;
	
	UPDATE IntegrationImportRun SET
	 [Status]=@Status
	WHERE IntegrationImportRunID <> @IntegrationImportRunID
		AND IntegrationImportDefinitionID = (SELECT IntegrationImportDefinitionID FROM IntegrationImportRun WHERE IntegrationImportRunID = @IntegrationImportRunID)
		AND EffectiveDate = @EffectiveDate
		AND [Status] <> 'ERROR'
    
END


