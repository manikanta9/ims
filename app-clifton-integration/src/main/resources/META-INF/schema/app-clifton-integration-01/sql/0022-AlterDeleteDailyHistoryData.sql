ALTER PROCEDURE [dbo].[DeleteDailyHistoryData]
	@IntegrationImportRunID INT,
	@Table nvarchar(250),
	@IsSourceTable BIT = 0 
AS
BEGIN
	
	SET NOCOUNT ON;
	
	exec('DELETE FROM ' + @Table + '
	WHERE IntegrationImportRunID IN (SELECT * FROM GetDailyRunList(' + @IntegrationImportRunID + '))');
	
	
END
