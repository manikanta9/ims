USE [CliftonIntegration]
GO

/****** Object:  UserDefinedFunction [Calendar].[GetBusinessDayFrom]    Script Date: 7/27/2021 11:39:55 AM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO



CREATE FUNCTION [Calendar].[GetBusinessDayFrom]	(

	 @Date DATETIME,
	 @CalendarID INT,
	 @DaysFrom INT
	

) RETURNS DATETIME AS


BEGIN
	DECLARE @BusinessDayDate DATETIME
	
	IF @CalendarID IS NULL
	BEGIN
		SET @CalendarID = (SELECT TOP 1 CalendarID FROM Calendar WHERE IsDefaultSystemCalendar = 1)
	END
	
	IF (@DaysFrom = 0)
	BEGIN
		SET @BusinessDayDate = @Date
	END
	ELSE 
	BEGIN
		DECLARE @Increment INT
		SELECT @Increment = CASE WHEN @DaysFrom > 0 THEN 1 ELSE -1 END
	
		DECLARE @CurrentDays INT
		SET @CurrentDays = 0
		
		SET @BusinessDayDate = @Date
		WHILE (@DaysFrom > @CurrentDays OR @DaysFrom < @CurrentDays)
		BEGIN
			SET @BusinessDayDate = DATEADD(day, @Increment, @BusinessDayDate)
			IF (Calendar.IsBusinessDay(@BusinessDayDate, @CalendarID) = 1)
			BEGIN
				SET @CurrentDays += @Increment
			END
		END
	END
	
	
RETURN
	 
	@BusinessDayDate
 
END



GO


