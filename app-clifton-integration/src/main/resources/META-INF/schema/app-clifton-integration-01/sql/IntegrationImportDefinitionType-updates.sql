IF NOT EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IntegrationImportDefinitionType' AND COLUMN_NAME = 'PathToSubFolder')
	ALTER TABLE IntegrationImportDefinitionType ADD PathToSubFolder NVARCHAR(50) NULL;
	
IF NOT EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IntegrationImportDefinitionType' AND COLUMN_NAME = 'DestinationTableName')
	ALTER TABLE IntegrationImportDefinitionType ADD DestinationTableName NVARCHAR(50) NULL;
