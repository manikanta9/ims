IF EXISTS (SELECT TOP 1 * FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = 'IntegrationImportDefinitionType' AND COLUMN_NAME = 'PathToSubFolder')
BEGIN

	IF EXISTS (SELECT TOP 1 * FROM IntegrationImportDefinitionType WHERE ImportDefinitionTypeName = 'Manager Download')
		UPDATE IntegrationImportDefinitionType SET
			PathToSubFolder = 'manager',
			DestinationTableName = 'IntegrationManagerPositionHistory'
		WHERE ImportDefinitionTypeName = 'Manager Download'
	
	IF EXISTS (SELECT TOP 1 * FROM IntegrationImportDefinitionType WHERE ImportDefinitionTypeName = 'Broker Import Position')
		UPDATE IntegrationImportDefinitionType SET
			PathToSubFolder = 'broker',
			DestinationTableName = 'IntegrationReconcilePositionHistory'
		WHERE ImportDefinitionTypeName = 'Broker Import Position'
		
	IF EXISTS (SELECT TOP 1 * FROM IntegrationImportDefinitionType WHERE ImportDefinitionTypeName = 'Broker Import')
		UPDATE IntegrationImportDefinitionType SET
			PathToSubFolder = 'broker'
		WHERE ImportDefinitionTypeName = 'Broker Import Position'
		
	
	ALTER TABLE IntegrationImportDefinitionType ALTER COLUMN PathToSubFolder NVARCHAR(50) NOT NULL
END
