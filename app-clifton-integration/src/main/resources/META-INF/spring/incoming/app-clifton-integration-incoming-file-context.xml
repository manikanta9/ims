<?xml version="1.0" encoding="UTF-8"?>
<beans default-lazy-init="true" default-autowire="byName"
       xmlns="http://www.springframework.org/schema/beans"
       xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
       xmlns:int="http://www.springframework.org/schema/integration"
       xmlns:file="http://www.springframework.org/schema/integration/file"
       xsi:schemaLocation="http://www.springframework.org/schema/beans
		http://www.springframework.org/schema/beans/spring-beans.xsd
		http://www.springframework.org/schema/integration
		http://www.springframework.org/schema/integration/spring-integration.xsd
		http://www.springframework.org/schema/integration/file
		http://www.springframework.org/schema/integration/file/spring-integration-file.xsd">


	<!-- FILE NAME GENERATORS -->
	<bean id="removeFileReadyTagNameGenerator" class="com.clifton.integration.incoming.file.RemoveFileReadyTagNameGenerator" />


	<!--*************************************START FILE INPUT PROCESSING****************************************-->
	<!--
		The inbound channel adapter is configured to poll the FileReadingMessageSource every 5 seconds by calling the 'receive()'
		method using a SpEL bean expression.  The FileReadingMessageSource will return a payload of 'Message<File>' that will be
		sent to the 'integrationFileInputChannel' for handling.
	-->
	<int:inbound-channel-adapter
			id="integrationFileInputChannelAdapter"
			channel="integrationFileInputChannel"
			expression="@integrationFileReadingMessageSource.receive()">
		<int:poller fixed-delay="5000" />
	</int:inbound-channel-adapter>

	<!--
		The FileReadingMessageSource is the starting point of the file integrations.  It defines the root directory to monitor and
		the scanner to use to locate eligible files for processing
	-->
	<bean id="integrationFileReadingMessageSource" class="com.clifton.integration.incoming.file.IntegrationFileReadingMessageSource">
		<property name="directory" value="file:${integration.file.input.path}" />
		<property name="scanner" ref="integrationFileSourceDirectoryScanner" />
	</bean>

	<bean id="integrationFileSourceDirectoryScanner" class="com.clifton.integration.incoming.file.IntegrationFileSourceDirectoryScanner">
		<property name="filter" ref="integrationFileFilter" />
		<property name="acceptAllFiles" value="true" />
	</bean>
	<bean id="integrationFileFilter" class="com.clifton.integration.incoming.file.filter.ImportFileListFilter">
		<property name="excludeExtensionList">
			<list>
				<value>zip</value>
				<value>write</value>
			</list>
		</property>
	</bean>

	<!-- move the file to processing folder -->
	<file:outbound-gateway id="integrationFileInputChannelGateway"
	                       request-channel="integrationFileInputChannel"
	                       reply-channel="integrationFileProcessingChannel"
	                       directory="file:${integration.file.inprocess.path}"
	                       filename-generator="removeFileReadyTagNameGenerator"
	                       delete-source-files="true" />
	<!-- process and move to output folder -->
	<int:chain input-channel="integrationFileProcessingChannel">
		<int:header-enricher>
			<int:header name="file_originalFile" expression="payload" />
			<int:error-channel ref="integrationFileErrorChannel" />
		</int:header-enricher>
		<int:service-activator ref="integrationFileHandler" method="handleFile" />
	</int:chain>
	<bean id="integrationFileHandler" class="com.clifton.integration.incoming.file.handler.IntegrationFileHandler">
		<property name="fileSourceType" value="MANUAL" />
	</bean>
	<bean id="integrationFileErrorHandler" class="com.clifton.integration.incoming.file.handler.IntegrationFileHandlerError">
		<property name="outputFolder" value="${integration.file.error.path}" />
	</bean>

	<int:service-activator input-channel="errorChannel" ref="integrationFileErrorHandler" method="handleFile" />

	<!--*************************************END FILE INPUT PROCESSING****************************************-->

	<!--*********************************START BATCH FILE INPUT PROCESSING************************************-->
	<!--
		The inbound channel adapter is configured to poll the BatchFileReadingMessageSource every 5 seconds by calling the 'receive()'
		method using a SpEL bean expression.  The FileReadingMessageSource will return a payload of 'Message<File>' that will be
		sent to the 'integrationFileInputChannel' for handling.
	-->
	<int:inbound-channel-adapter
			id="integrationBatchFileInputChannelAdapter"
			channel="integrationBatchFileInputChannel"
			expression="@integrationBatchFileReadingMessageSource.receive()">
		<int:poller fixed-delay="5000" />
	</int:inbound-channel-adapter>

	<!--
		The FileReadingMessageSource is the starting point of the file integrations.  It defines the root directory to monitor and
		the scanner to use to locate eligible files for processing
	-->
	<bean id="integrationBatchFileReadingMessageSource" class="com.clifton.integration.incoming.file.IntegrationFileReadingMessageSource">
		<property name="directory" value="file:${integration.file.input.batch.path}" />
		<property name="scanner" ref="integrationBatchFileSourceDirectoryScanner" />
	</bean>

	<bean id="integrationBatchFileSourceDirectoryScanner" class="com.clifton.integration.incoming.file.IntegrationFileSourceDirectoryScanner">
		<property name="filter" ref="integrationBatchFileFilter" />
		<property name="acceptAllFiles" value="true" />
	</bean>
	<bean id="integrationBatchFileFilter" class="com.clifton.integration.incoming.file.filter.ImportFileListFilter">
		<property name="excludeExtensionList">
			<list>
				<value>zip</value>
				<value>write</value>
			</list>
		</property>
	</bean>

	<!-- move the batch files to the processing folder -->
	<file:outbound-gateway id="integrationBatchFileInputChannelGateway"
	                       request-channel="integrationBatchFileInputChannel"
	                       reply-channel="integrationBatchFileProcessingChannel"
	                       directory="file:${integration.file.inprocess.path}"
	                       filename-generator="removeFileReadyTagNameGenerator"
	                       delete-source-files="true" />
	<!-- process and move to output folder -->
	<int:chain input-channel="integrationBatchFileProcessingChannel">
		<int:header-enricher>
			<int:header name="file_originalFile" expression="payload" />
			<int:error-channel ref="integrationFileErrorChannel" />
		</int:header-enricher>
		<int:service-activator ref="integrationBatchFileHandler" method="handleFile" />
	</int:chain>
	<bean id="integrationBatchFileHandler" class="com.clifton.integration.incoming.file.handler.IntegrationFileHandler">
		<property name="fileSourceType" value="BATCH" />
	</bean>
	<bean id="integrationBatchFileErrorHandler" class="com.clifton.integration.incoming.file.handler.IntegrationFileHandlerError">
		<property name="outputFolder" value="${integration.file.error.path}" />
	</bean>
	<!--*********************************END BATCH FILE INPUT PROCESSING************************************-->

	<!--*********************************START ZIP FILE INPUT PROCESSING************************************-->
	<!--
		The inbound channel adapter is configured to poll the FileReadingMessageSource every 5 seconds by calling the 'receive()'
		method using a SpEL bean expression.  The FileReadingMessageSource will return a payload of 'Message<File>' that will be
		sent to the 'integrationFileInputChannel' for handling.
	-->
	<int:inbound-channel-adapter
			id="integrationZipFileInputChannelAdapter"
			channel="integrationZipFileInputChannel"
			expression="@integrationZipFileReadingMessageSource.receive()">
		<int:poller fixed-delay="5000" />
	</int:inbound-channel-adapter>

	<bean id="integrationZipFileReadingMessageSource" class="com.clifton.integration.incoming.file.IntegrationFileReadingMessageSource">
		<property name="directory" value="file:${integration.file.input.path}" />
		<property name="scanner" ref="integrationZipFileSourceDirectoryScanner" />
	</bean>
	<bean id="integrationZipFileSourceDirectoryScanner" class="com.clifton.integration.incoming.file.IntegrationFileSourceDirectoryScanner">
		<property name="filter" ref="integrationZipFileFilter" />
		<property name="acceptAllFiles" value="true" />
	</bean>
	<bean id="integrationZipFileFilter" class="com.clifton.integration.incoming.file.filter.ImportFileListFilter">
		<property name="includeExtensionList">
			<list>
				<value>zip</value>
			</list>
		</property>
	</bean>
	<int:chain input-channel="integrationZipFileInputChannel">
		<int:header-enricher>
			<int:header name="file_originalFile" expression="payload" />
			<int:error-channel ref="integrationFileErrorChannel" />
		</int:header-enricher>
		<int:service-activator ref="integrationZipFileHandler" method="handleFile" />
	</int:chain>
	<bean id="integrationZipFileHandler" class="com.clifton.integration.incoming.file.handler.IntegrationZipFileHandler" />

	<!-- handle error and move to error folder -->
	<int:chain input-channel="integrationFileErrorChannel">
		<int:service-activator ref="integrationFileErrorHandler" method="handleFile" />
	</int:chain>
	<!--*********************************START BATCH ZIP FILE INPUT PROCESSING************************************-->
	<!--
		The inbound channel adapter is configured to poll the FileReadingMessageSource every 5 seconds by calling the 'receive()'
		method using a SpEL bean expression.  The FileReadingMessageSource will return a payload of 'Message<File>' that will be
		sent to the 'integrationFileInputChannel' for handling.
	-->
	<int:inbound-channel-adapter
			id="integrationBatchZipFileInputChannelAdapter"
			channel="integrationBatchZipFileInputChannel"
			expression="@integrationBatchZipFileReadingMessageSource.receive()">
		<int:poller fixed-delay="5000" />
	</int:inbound-channel-adapter>

	<bean id="integrationBatchZipFileReadingMessageSource" class="com.clifton.integration.incoming.file.IntegrationFileReadingMessageSource">
		<property name="directory" value="file:${integration.file.input.batch.path}" />
		<property name="scanner" ref="integrationBatchZipFileSourceDirectoryScanner" />
	</bean>
	<bean id="integrationBatchZipFileSourceDirectoryScanner" class="com.clifton.integration.incoming.file.IntegrationFileSourceDirectoryScanner">
		<property name="filter" ref="integrationBatchZipFileFilter" />
		<property name="acceptAllFiles" value="true" />
	</bean>
	<bean id="integrationBatchZipFileFilter" class="com.clifton.integration.incoming.file.filter.ImportFileListFilter">
		<property name="includeExtensionList">
			<list>
				<value>zip</value>
			</list>
		</property>
	</bean>
	<int:chain input-channel="integrationBatchZipFileInputChannel">
		<int:header-enricher>
			<int:error-channel ref="integrationBatchFileErrorChannel" />
		</int:header-enricher>
		<int:service-activator ref="integrationZipFileHandler" method="handleFile" />
	</int:chain>
	<bean id="integrationBatchZipFileHandler" class="com.clifton.integration.incoming.file.handler.IntegrationZipFileHandler" />

	<!-- handle error and move to error folder -->
	<int:chain input-channel="integrationBatchFileErrorChannel">
		<int:service-activator ref="integrationBatchFileErrorHandler" method="handleFile" />
	</int:chain>
	<!--*********************************END ZIP FILE INPUT PROCESSING************************************-->


	<!--*************************************START ADVENT FILE INPUT PROCESSING****************************************-->
	<!--
		The advent directory is simply used so we can only process PSX, SCX, and TRX files; we don't want integration to pick
		up the index.txt and motd.htm files that are used by advent for internal tracking.  This channel simply grabs the
		PSX and TRX files and copies to the the normal file processing folder.

		The inbound channel adapter is configured to poll the FileReadingMessageSource every 5 seconds by calling the 'receive()'
		method using a SpEL bean expression.  The FileReadingMessageSource will return a payload of 'Message<File>' that will be
		sent to the 'integrationFileInputChannel' for handling.
	-->
	<int:inbound-channel-adapter
			id="integrationAdventFileInputChannelAdapter"
			channel="integrationAdventFileInputChannel"
			expression="@integrationAdventFileReadingMessageSource.receive()">
		<int:poller fixed-delay="5000" />
	</int:inbound-channel-adapter>

	<!--
		The FileReadingMessageSource is the starting point of the file integrations.  It defines the root directory to monitor and
		the scanner to use to locate eligible files for processing
	-->
	<bean id="integrationAdventFileReadingMessageSource" class="com.clifton.integration.incoming.file.IntegrationFileReadingMessageSource">
		<property name="directory" value="file:${integration.file.advent.path}" />
		<property name="scanner" ref="integrationAdventFileSourceDirectoryScanner" />
	</bean>

	<bean id="integrationAdventFileSourceDirectoryScanner" class="com.clifton.integration.incoming.file.IntegrationFileSourceDirectoryScanner">
		<property name="filter" ref="integrationAdventFileFilter" />
		<property name="acceptAllFiles" value="true" />
	</bean>
	<bean id="integrationAdventFileFilter" class="com.clifton.integration.incoming.file.filter.ImportFileListFilter">
		<property name="includeExtensionList">
			<list>
				<value>psx</value>
				<value>scx</value>
				<value>trx</value>
			</list>
		</property>
	</bean>

	<!-- move the file to processing folder -->
	<file:outbound-gateway id="integrationAdventFileInputChannelGateway"
	                       request-channel="integrationAdventFileInputChannel"
	                       reply-channel="integrationFileProcessingChannel"
	                       directory="file:${integration.file.inprocess.path}"
	                       filename-generator="removeFileReadyTagNameGenerator"
	                       delete-source-files="true" />

	<!--*************************************END ADVENT FILE INPUT PROCESSING****************************************-->


	<!-- processing channel -->

	<int:channel id="integrationFileInputChannel" />
	<int:channel id="integrationAdventFileInputChannel" />
	<int:channel id="integrationBatchFileInputChannel" />
	<int:channel id="integrationZipFileInputChannel" />
	<int:channel id="integrationBatchZipFileInputChannel" />

	<int:channel id="integrationFileProcessingChannel" />
	<int:channel id="integrationBatchFileProcessingChannel" />
	<int:channel id="integrationZipFileProcessingChannel" />

	<int:channel id="integrationFileErrorChannel" />
	<int:channel id="integrationBatchFileErrorChannel" />
	<int:channel id="integrationZipFileErrorChannel" />
</beans>
