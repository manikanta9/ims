package com.clifton.integration.incoming.file;


import com.clifton.integration.incoming.definition.IntegrationImportRun;


/**
 * The <code>IntegrationFileProcessor</code> defines a file processor that will perform the transformation and pre/post transformation actions.
 *
 * @author mwacker
 */
public interface IntegrationFileProcessor {

	/**
	 * Do the file processing
	 */
	public IntegrationImportRun processIntegrationFile(IntegrationFileProcessorCommand command);
}
