package com.clifton.integration.incoming.file.handler;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.ZipUtils;
import org.springframework.messaging.Message;

import java.io.File;


public class IntegrationZipFileHandler {

	public void handleFile(Message<File> message) {
		File input = message.getPayload();
		ZipUtils.unZipFiles(input);
		FileUtils.delete(input);
	}
}
