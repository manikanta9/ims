package com.clifton.integration.incoming.action;

import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.manager.IntegrationManagerTransaction;
import org.hibernate.Criteria;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;


/**
 * The <code>ManagerTransactionPostTransformationAction</code> custom action for manager transaction imports (cashflows)
 * that executes stored queries to build the final transaction data.
 *
 * @author theodorez
 */
public class ManagerTransactionPostTransformationAction implements TransformationAction {

	private AdvancedUpdatableDAO<IntegrationManagerTransaction, Criteria> integrationManagerTransactionDAO;


	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	@Override
	public void doAction(Map<String, Object> actionData, Map<String, String> transformationData) {
		if (actionData != null) {
			IntegrationImportRun run = (IntegrationImportRun) actionData.get("IntegrationImportRun");

			// move the data to the final table
			executeNamedQuery("managerMoveTransactionsToFinalTable", run.getId());
		}
	}


	@Transactional(timeout = 180)
	protected void executeNamedQuery(String queryName, Integer runId) {
		getIntegrationManagerTransactionDAO().executeNamedQuery(queryName, runId);
	}

	////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////


	public AdvancedUpdatableDAO<IntegrationManagerTransaction, Criteria> getIntegrationManagerTransactionDAO() {
		return this.integrationManagerTransactionDAO;
	}


	public void setIntegrationManagerTransactionDAO(AdvancedUpdatableDAO<IntegrationManagerTransaction, Criteria> integrationManagerTransactionDAO) {
		this.integrationManagerTransactionDAO = integrationManagerTransactionDAO;
	}
}
