package com.clifton.integration.incoming.server.event;


import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.target.IntegrationTargetApplication;


public interface IntegrationEventService {

	@DoNotAddRequestMapping
	public void sendIntegrationEventMessage(IntegrationImportRunEvent runEvent, String integrationEventName, IntegrationTargetApplication targetApplication);
}
