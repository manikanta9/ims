package com.clifton.integration.incoming.server.event;


import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.messages.IntegrationDataEventRequestMessage;
import com.clifton.integration.target.IntegrationTargetApplication;
import org.springframework.stereotype.Service;


@Service
public class IntegrationEventServiceImpl implements IntegrationEventService {

	private AsynchronousMessageHandler integrationEventJmsMessageHandler;


	@Override
	public void sendIntegrationEventMessage(IntegrationImportRunEvent runEvent, String integrationEventName, IntegrationTargetApplication targetApplication) {
		IntegrationDataEventRequestMessage requestMessage = new IntegrationDataEventRequestMessage();
		requestMessage.setIntegrationImportRunEventId(runEvent.getId());
		requestMessage.setImportDefinitionTypeName(runEvent.getImportRun().getIntegrationImportDefinition().getType().getName());
		requestMessage.setIntegrationEventName(integrationEventName);
		requestMessage.getProperties().put(MessagingMessage.TARGET_APPLICATION, targetApplication.getName());
		getIntegrationEventJmsMessageHandler().send(requestMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	//////////              Getter & Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public AsynchronousMessageHandler getIntegrationEventJmsMessageHandler() {
		return this.integrationEventJmsMessageHandler;
	}


	public void setIntegrationEventJmsMessageHandler(AsynchronousMessageHandler integrationEventJmsMessageHandler) {
		this.integrationEventJmsMessageHandler = integrationEventJmsMessageHandler;
	}
}
