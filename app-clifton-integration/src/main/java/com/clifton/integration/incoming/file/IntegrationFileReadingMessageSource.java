package com.clifton.integration.incoming.file;


import org.springframework.integration.file.FileReadingMessageSource;


/**
 * Filters the directory file scanner using a list of sub folders loaded for integration sources.
 */
public class IntegrationFileReadingMessageSource extends FileReadingMessageSource {

	@Override
	protected void onInit() {
		//Do nothing as there is a bug in the onInit method that prevents the use of a custom scanner completely.
	}
}
