package com.clifton.integration.incoming.file.handler;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileSourceType;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.file.archive.IntegrationFileArchiveParameters;
import com.clifton.integration.file.archive.IntegrationFileArchiveService;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.file.IntegrationFileProcessor;
import com.clifton.integration.incoming.file.IntegrationFileProcessorCommand;
import com.clifton.integration.source.IntegrationSourceService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.integration.file.FileHeaders;
import org.springframework.messaging.Message;

import java.io.File;
import java.io.StringWriter;
import java.util.Date;
import java.util.List;


/**
 * The <code>IntegrationFileHandler</code> gets the IntegrationImportDefinition for the file, looks up the correct file processor
 * based on the definition type and executed the processor.
 *
 * @author mwacker
 */
public class IntegrationFileHandler {

	private IntegrationFileProcessor integrationImportFileProcessor;

	private IntegrationImportService integrationImportService;
	private IntegrationSourceService integrationSourceService;
	private IntegrationFileService integrationFileService;
	private IntegrationFileArchiveService integrationFileArchiveService;

	public static final String UNKNOWN_FILE_NAME = "UNKNOWN";

	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;

	private ContextHandler contextHandler;
	private SecurityUserService securityUserService;

	private IntegrationFileSourceType fileSourceType = IntegrationFileSourceType.MANUAL;


	public void handleFile(Message<File> message) {
		IntegrationFile integrationFile = (IntegrationFile) message.getHeaders().get("IntegrationFile");
		try {
			getContextHandler().setBean(Context.USER_BEAN_NAME, getRunAsUser());

			File input = message.getPayload();
			if (IntegrationFileUtils.isTimeStampPresent(input.getName()) && !message.getHeaders().containsKey("IntegrationFile")) {
				// rename the file with out the time stamps
				File newInput = new File(IntegrationFileUtils.removeTimeStamp(input.getAbsolutePath()));
				if (input.renameTo(newInput)) {
					input = newInput;
				}
			}

			Date effectiveDate = IntegrationFileUtils.getEffectiveDateFromFileName(input.getName());
			String originalFilename = IntegrationFileUtils.getOriginalFileName(input.getName(), effectiveDate != null);

			// get the IntegrationFile from the message or archive the file that received and create a new entity
			if (integrationFile != null) {
				integrationFile.setErrorOnLoad(false);

				// rerun the decryption if needed
				getIntegrationFileArchiveService().decryptArchivedIntegrationFile(integrationFile);
				// look up the correct file definition and move the file
				getIntegrationFileArchiveService().rearchiveExistingIntegrationFile(integrationFile);
			}
			else {
				IntegrationFileArchiveParameters params = new IntegrationFileArchiveParameters(input, originalFilename, getFileSourceType(), effectiveDate);
				// get the directory of the original file that we will use to find the source.
				if (getFileSourceType().isFtp()) {
					String remoteDirectory = new File(String.valueOf(message.getHeaders().get(FileHeaders.REMOTE_DIRECTORY))).getName();
					if ("null".equals(remoteDirectory)) {
						//It may be null because of the integration tests in which case pull the source off the input file.
						remoteDirectory = input.getParentFile().getName();
					}
					params.setImportSource(getIntegrationSourceService().getIntegrationSourceByFtpRootFolder(remoteDirectory));
				}
				else {
					File originalFile = ((File) message.getHeaders().get(FileHeaders.ORIGINAL_FILE));
					AssertUtils.assertNotNull(originalFile, "Could not find original file from message headers");
					String inputDirectory = new File(originalFile.getParent()).getName();
					params.setImportSource(getIntegrationSourceService().getIntegrationSourceByName(inputDirectory));
				}
				integrationFile = getIntegrationFileArchiveService().archiveIntegrationFile(params);
			}

			if (integrationFile.isErrorOnLoad()) {
				// do not process the file since it failed to archive correctly
				return;
			}

			// find the import definitions and process the file
			if (!UNKNOWN_FILE_NAME.equals(integrationFile.getFileDefinition().getFileName())) {
				List<IntegrationImportDefinition> definitionList = getIntegrationImportService().getIntegrationImportDefinitionListByFileDefinition(integrationFile.getFileDefinition().getId());
				//If this is coming from a reprocess, we want to only reprocess the appropriate run
				Boolean limitToImportRun = (Boolean) message.getHeaders().get("LimitToImportRun");
				IntegrationImportRun integrationImportRun = (IntegrationImportRun) message.getHeaders().get("IntegrationImportRun");

				if (limitToImportRun != null && limitToImportRun && integrationImportRun != null) {
					definitionList = BeanUtils.filter(definitionList, (integrationImportDefinition -> integrationImportRun.getIntegrationImportDefinition().equals(integrationImportDefinition)));
				}
				definitionList = BeanUtils.sortWithFunction(definitionList, IntegrationImportDefinition::getOrder, true);
				BuildErrorHolder errorHolder = new BuildErrorHolder();
				for (IntegrationImportDefinition definition : CollectionUtils.getIterable(definitionList)) {

					if (!definition.isDisabled()) {
						processIntegrationFile(new IntegrationFileProcessorCommand(message, integrationFile, definition, integrationFile.getEffectiveDate()), errorHolder);
					}
					else {
						LogUtils.warn(getClass(), "Not running IntegrationImportDefinition [" + definition.getName() + "] because it's disabled.");
					}
				}
				if (errorHolder.getErrors() != null) {
					throw new RuntimeException(errorHolder.getErrors().toString(), errorHolder.getFirstCause());
				}
			}
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Exception occurred in the Integration File Handler when handling message: " + message, e);

			StringWriter sw = new StringWriter();

			// if the file was archived then set the error on the file, otherwise throw the error so spring integration will move it to the errors folder.
			if (integrationFile != null) {

				integrationFile.setErrorOnLoad(true);
				integrationFile.setError(e.getMessage() + "\n" + sw);

				getIntegrationFileService().saveIntegrationFile(integrationFile);
			}
			else {
				throw new RuntimeException(sw.toString());
			}
		}
	}


	private void processIntegrationFile(IntegrationFileProcessorCommand command, BuildErrorHolder errorHolder) {
		try {
			IntegrationFile integrationFile = command.getIntegrationFile();
			IntegrationImportRun run = getIntegrationImportFileProcessor().processIntegrationFile(command);
			// update the effective if not already populated.
			if (command.getIntegrationFile().getEffectiveDate() == null) {
				integrationFile.setEffectiveDate(integrationFile.getFileDefinition().isOnePerDay() ? DateUtils.clearTime(run.getEffectiveDate()) : run.getEffectiveDate());
				getIntegrationFileService().saveIntegrationFile(integrationFile);
			}
		}
		catch (Exception e) {
			// full log for first error and only causes for consecutive errors
			boolean firstError = false;
			if (errorHolder.getErrors() == null) {
				firstError = true;
				errorHolder.setFirstCause(e);
				errorHolder.setErrors(new StringBuilder(1024));
			}
			else {
				errorHolder.getErrors().append("\n\n");
			}
			errorHolder.getErrors().append("Failed to run import definition [").append(command.getImportDefinition().getName()).append("].");
			if (!firstError) {
				errorHolder.getErrors().append("\nCAUSED BY ");
				errorHolder.getErrors().append(ExceptionUtils.getDetailedMessage(e));
			}
		}
	}


	private class BuildErrorHolder {

		private StringBuilder errors;
		private Exception firstCause;


		public StringBuilder getErrors() {
			return this.errors;
		}


		public void setErrors(StringBuilder errors) {
			this.errors = errors;
		}


		public Exception getFirstCause() {
			return this.firstCause;
		}


		public void setFirstCause(Exception firstCause) {
			this.firstCause = firstCause;
		}
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////
	private SecurityUser getRunAsUser() {
		return getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public IntegrationFileSourceType getFileSourceType() {
		return this.fileSourceType;
	}


	public void setFileSourceType(IntegrationFileSourceType fileSourceType) {
		this.fileSourceType = fileSourceType;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	public IntegrationFileArchiveService getIntegrationFileArchiveService() {
		return this.integrationFileArchiveService;
	}


	public void setIntegrationFileArchiveService(IntegrationFileArchiveService integrationFileArchiveService) {
		this.integrationFileArchiveService = integrationFileArchiveService;
	}


	public IntegrationFileProcessor getIntegrationImportFileProcessor() {
		return this.integrationImportFileProcessor;
	}


	public void setIntegrationImportFileProcessor(IntegrationFileProcessor integrationImportFileProcessor) {
		this.integrationImportFileProcessor = integrationImportFileProcessor;
	}
}
