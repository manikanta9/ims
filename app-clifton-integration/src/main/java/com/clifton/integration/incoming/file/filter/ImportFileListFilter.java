package com.clifton.integration.incoming.file.filter;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.file.IntegrationFileUtils;
import org.springframework.integration.file.filters.AbstractFileListFilter;

import java.io.File;
import java.util.List;


/**
 * @author mwacker
 */
public class ImportFileListFilter extends AbstractFileListFilter<File> {


	/**
	 * NOTE: Extensions should always be in lower case.
	 */
	private List<String> excludeExtensionList;
	private List<String> includeExtensionList;


	@Override
	public boolean accept(File file) {
		ValidationUtils.assertFalse(this.includeExtensionList != null && this.excludeExtensionList != null, "Cannot have both include and exclude lists populated.");
		String extension = FileUtils.getFileExtension(removeReadyExtension(file).toLowerCase());
		if (this.excludeExtensionList != null) {
			return !this.excludeExtensionList.contains(extension);
		}
		else if (this.includeExtensionList != null) {
			return this.includeExtensionList.contains(extension);
		}
		return true;
	}


	private String removeReadyExtension(File file) {
		String fileName = file.getName();
		if (fileName.endsWith(IntegrationFileUtils.FILE_NAME_READY_EXTENSION)) {
			return FileUtils.getFileNameWithoutExtension(fileName);
		}
		return fileName;
	}


	public void setExcludeExtensionList(List<String> excludeExtensionList) {
		this.excludeExtensionList = excludeExtensionList;
	}


	public void setIncludeExtensionList(List<String> includeExtensionList) {
		this.includeExtensionList = includeExtensionList;
	}
}
