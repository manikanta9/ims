package com.clifton.integration.incoming.action;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.MappingException;
import com.clifton.integration.incoming.manager.IntegrationManagerAssetClassGroup;
import com.clifton.integration.incoming.manager.IntegrationManagerPositionType;
import com.clifton.integration.incoming.manager.IntegrationManagerService;
import org.hibernate.Criteria;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Map;


/**
 * The <code>ManagerPositionPostTransformationAction</code> custom action for manager imports
 * that executes the procedures that build the final manager data.
 *
 * @author mwacker
 */
public class ManagerPositionPostTransformationAction implements TransformationAction {

	private AdvancedUpdatableDAO<IntegrationManagerPositionType, Criteria> integrationManagerPositionTypeDAO;

	private IntegrationManagerService integrationManagerService;


	@Override
	public void doAction(Map<String, Object> actionData, @SuppressWarnings("unused") Map<String, String> transformationParameters) {
		if (actionData != null) {
			IntegrationImportRun run = (IntegrationImportRun) actionData.get("IntegrationImportRun");

			// move the data to the final table and populate the position securities
			executeNamedQuery("managerMovePositionsToFinalTable", run.getId());
			// update manager broker codes
			executeNamedQuery("updateManagerBrokerCodes", run.getId());
			// update the position types
			executeNamedQuery("managerPositionTypeUpdate", run.getId());
			// update the position values
			executeNamedQuery("managerPositionValueUpdate", run.getId());
			// insert system defined rows
			executeNamedQuery("managerInsertPositionSystemDefinedValues", run.getId());

			getIntegrationManagerService().setIntegrationDataSourceName("dataSource");

			IntegrationManagerAssetClassGroup group = getIntegrationManagerService().getIntegrationManagerAssetClassGroupByDefinition(run.getIntegrationImportDefinition().getId());
			if (group != null && group.isRequireAllAssetClassMappings()) {
				List<String> unmappedAssetClassList = getIntegrationManagerService().getIntegrationUnmappedAssetClassesForGroup(run.getId());
				if ((unmappedAssetClassList != null) && (!unmappedAssetClassList.isEmpty())) {
					StringBuilder sb = new StringBuilder();
					for (String unmappedAssetClass : CollectionUtils.getIterable(unmappedAssetClassList)) {
						sb.append("'").append(unmappedAssetClass).append("' asset class found but it does not have a mapping.\n");
					}
					throw new MappingException(sb.toString());
				}
			}
		}
	}


	@Transactional(timeout = 180)
	protected void executeNamedQuery(String queryName, Integer runId) {
		getIntegrationManagerPositionTypeDAO().executeNamedQuery(queryName, runId);
	}


	public AdvancedUpdatableDAO<IntegrationManagerPositionType, Criteria> getIntegrationManagerPositionTypeDAO() {
		return this.integrationManagerPositionTypeDAO;
	}


	public void setIntegrationManagerPositionTypeDAO(AdvancedUpdatableDAO<IntegrationManagerPositionType, Criteria> integrationManagerPositionTypeDAO) {
		this.integrationManagerPositionTypeDAO = integrationManagerPositionTypeDAO;
	}


	public IntegrationManagerService getIntegrationManagerService() {
		return this.integrationManagerService;
	}


	public void setIntegrationManagerService(IntegrationManagerService integrationManagerService) {
		this.integrationManagerService = integrationManagerService;
	}
}
