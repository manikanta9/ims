package com.clifton.integration.incoming.file.handler;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.file.archive.IntegrationFileArchiveParameters;
import com.clifton.integration.file.archive.IntegrationFileArchiveService;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessagingException;

import java.io.File;
import java.util.Date;


/**
 * The <code>IntegrationFileErrorHandlerImpl</code> when an un-handled processing error occurs before an import run
 * is created, this handler logs the error and moves the file to the error directory.
 *
 * @author mwacker
 */
public class IntegrationFileHandlerError {

	private String outputFolder;
	private IntegrationFileArchiveService integrationFileArchiveService;


	public void handleFile(Message<MessagingException> message) {
		LogUtils.error(getClass(), message.getPayload().toString());

		@SuppressWarnings("unchecked")
		Message<File> originalMessage = (Message<File>) message.getPayload().getFailedMessage();
		AssertUtils.assertNotNull(originalMessage, "Cannot handle messaging exception because the failed message is null");
		File file = originalMessage.getPayload();
		try {
			FileUtils.moveFileCreatePathAndOverwrite(file, FileUtils.combinePath(getOutputFolder(), generateFileName(originalMessage)));
		}
		catch (Throwable e) {
			LogUtils.error(getClass(), "Failed to archive file [" + file.getName() + "].", e);
		}
	}


	public String generateFileName(Message<?> message) {
		File input = (File) message.getPayload();
		try {
			Date effectiveDate = IntegrationFileUtils.getEffectiveDateFromFileName(input.getName());
			String originalFilename = IntegrationFileUtils.getOriginalFileName(input.getName(), effectiveDate != null);

			return getIntegrationFileArchiveService().getIntegrationFileRelativeArchiveFilePath(new IntegrationFileArchiveParameters(input, originalFilename, null));
		}
		catch (Throwable e) {
			// don't throw an error because we are trying to handle an error, so just return the original file name
			LogUtils.error(getClass(), "Failed to generate name for error archive with file [" + input.getName() + "].", e);
		}
		return ((File) message.getPayload()).getName();
	}


	public String getOutputFolder() {
		return this.outputFolder;
	}


	public void setOutputFolder(String outputFolder) {
		this.outputFolder = outputFolder;
	}


	public IntegrationFileArchiveService getIntegrationFileArchiveService() {
		return this.integrationFileArchiveService;
	}


	public void setIntegrationFileArchiveService(IntegrationFileArchiveService integrationFileArchiveService) {
		this.integrationFileArchiveService = integrationFileArchiveService;
	}
}
