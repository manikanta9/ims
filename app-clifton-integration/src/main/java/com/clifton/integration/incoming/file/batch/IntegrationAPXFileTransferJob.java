package com.clifton.integration.incoming.file.batch;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.file.IntegrationFileUtils;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVPrinter;
import org.apache.commons.csv.CSVRecord;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * <code>IntegrationFileTransferJob</code> does not iterate into sub-directories.
 *
 * @author TerryS
 */
public class IntegrationAPXFileTransferJob extends IntegrationFileTransferJob {

	private static final char DEFAULT_SEPARATOR = ',';
	private static final char DEFAULT_QUOTE = '"';

	private String tempToPath;
	private String tempToPathFileNameSuffix;


	////////////////////////////////////////////////////////////////////////////


	public String getTempToPathFileNameSuffix() {
		return this.tempToPathFileNameSuffix != null ? this.tempToPathFileNameSuffix : "";
	}

	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processFile(String fileName, String targetFilename, FileCopyResult fileCopyResult) throws IOException {

		FileUtils.createDirectory(getTempToPath());

		String sourceAbsolutePath = FileUtils.combinePath(getFromPath(), fileName);
		FileContainer sourceFileContainer = FileContainerFactory.getFileContainer(sourceAbsolutePath);

		String tempTargetAbsolutePath = FileUtils.combinePaths(getTempToPath(), targetFilename);
		FileContainer tempTargetFileContainer = FileContainerFactory.getFileContainer(tempTargetAbsolutePath);
		try {
			moveOrCopyFile(sourceFileContainer, tempTargetFileContainer, fileCopyResult);
			for (FileContainer fileContainer : CollectionUtils.getIterable(splitFile(tempTargetFileContainer))) {
				String targetWriteAbsolutePath = FileUtils.combinePaths(getToPath(), fileContainer.getName() + "." + IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION);
				FileContainer targetWriteFileContainer = FileContainerFactory.getFileContainer(targetWriteAbsolutePath);
				FileUtils.moveFile(fileContainer, targetWriteFileContainer);
				fileCopyResult.incrementCopiedCount();

				if (isIntegrationFolderDestination()) {
					String targetAbsolutePath = FileUtils.combinePaths(getToPath(), fileContainer.getName());
					FileContainer targetFileContainer = FileContainerFactory.getFileContainer(targetAbsolutePath);
					boolean successful = targetWriteFileContainer.renameToFile(targetFileContainer);
					if (!successful) {
						fileCopyResult.addErrorAndIncrement(String.format("Could not rename [%s] to [%s].", targetWriteFileContainer.getAbsolutePath(), targetFileContainer.getAbsolutePath()));
					}
				}
			}
		}
		catch (Exception e) {
			LogUtils.error(this.getClass(), "Failed to process file [" + fileName + "]", e);
		}
		finally {
			//Cleanup temp dir
			if (tempTargetFileContainer.exists()) {
				for (String tempFileName : FileContainerFactory.getFileContainer(getTempToPath()).list()) {
					FileUtils.delete(FileContainerFactory.getFileContainer(FileUtils.combinePaths(getTempToPath(), tempFileName)));
				}
			}
		}
	}


	public List<FileContainer> splitFile(FileContainer fileContainer) {
		Set<String> splitFileNameList = new HashSet<>();
		Map<String, String> headerWrittenMap = new HashMap<>();
		Map<String, Boolean> custodialCodeMap = new HashMap<>();
		try (final CSVParser parser = getParser(fileContainer.getInputStream(), ',', '"')) {
			BufferedWriter bw = null;
			CSVPrinter csvPrinter = null;
			String currentCustodialCode = null;
			for (final CSVRecord record : parser) {
				if (record.size() > 1) {
					String custodialCode = record.get("Custodial Code").toUpperCase();
					String fileName = FileUtils.combinePaths(getTempToPath(), custodialCode + "-" + getEffectiveDate(fileContainer.getName()) + getTempToPathFileNameSuffix() + ".txt");
					boolean process = custodialCodeMap.computeIfAbsent(custodialCode, processCustodialCode -> isReprocess() || process(fileName, null));
					if (process) {
						if (!custodialCode.equals(currentCustodialCode)) {
							if (bw != null) {
								bw.flush();
								bw.close();
								csvPrinter.close();
							}
							splitFileNameList.add(fileName);
							bw = Files.newBufferedWriter(
									Paths.get(FileContainerFactory.getFileContainer(fileName).getAbsolutePath()),
									StandardOpenOption.APPEND,
									StandardOpenOption.CREATE);
							csvPrinter = new CSVPrinter(bw, CSVFormat.DEFAULT);
							if (headerWrittenMap.get(custodialCode) == null) {
								csvPrinter.printRecord(parser.getHeaderNames());
								headerWrittenMap.put(custodialCode, custodialCode);
							}
						}
						csvPrinter.printRecord(record);
						currentCustodialCode = custodialCode;
					}
				}
			}
			if (bw != null) {
				bw.flush();
				bw.close();
				csvPrinter.close();
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Failed to process csv file.", e);
		}
		List<FileContainer> fileContainerList = new ArrayList<>();
		for (String fileName : CollectionUtils.getIterable(splitFileNameList)) {
			fileContainerList.add(FileContainerFactory.getFileContainer(fileName));
		}
		return fileContainerList;
	}


	private String getEffectiveDate(String fileName) {
		return fileName.substring(0, fileName.indexOf('_'));
	}


	private static CSVParser getParser(InputStream inputStream, char separators, char customQuote) throws IOException {
		if (customQuote == ' ') {
			customQuote = DEFAULT_QUOTE;
		}
		if (separators == ' ') {
			separators = DEFAULT_SEPARATOR;
		}
		return CSVParser.parse(inputStream, Charset.defaultCharset(), CSVFormat.newFormat(separators).withQuote(customQuote).withTrim().withFirstRecordAsHeader());
	}


	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTempToPath() {
		return this.tempToPath;
	}


	public void setTempToPath(String tempToPath) {
		this.tempToPath = tempToPath;
	}


	public void setTempToPathFileNameSuffix(String tempToPathFileNameSuffix) {
		this.tempToPathFileNameSuffix = tempToPathFileNameSuffix;
	}
}
