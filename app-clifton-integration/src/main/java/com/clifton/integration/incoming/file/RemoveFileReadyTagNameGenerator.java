package com.clifton.integration.incoming.file;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.integration.file.IntegrationFileUtils;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.messaging.Message;

import java.io.File;


/**
 * The <code>RemoveFileReadyTagNameGenerator</code> removes the extension that was added to the file to determine if it was ready to be moved.
 *
 * @author mwacker
 */
public class RemoveFileReadyTagNameGenerator implements FileNameGenerator {

	@Override
	public String generateFileName(Message<?> message) {
		String fileName = ((File) message.getPayload()).getName();
		if (fileName.endsWith(IntegrationFileUtils.FILE_NAME_READY_EXTENSION)) {
			return FileUtils.getFileNameWithoutExtension(fileName);
		}
		return fileName;
	}
}
