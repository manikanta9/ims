package com.clifton.integration.incoming.file;

import com.clifton.core.messaging.MessagingMessage;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.transformation.IntegrationImportTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;
import com.clifton.integration.target.IntegrationTargetApplication;
import org.springframework.messaging.Message;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * @author mwacker
 */
public class IntegrationFileProcessorCommand {

	public static final String COMMAND_EFFECTIVE_DATE = "EffectiveDate";
	//The actual 'File' object, converted from an Integration File
	public static final String COMMAND_INPUT_FILE = "InputFile";
	//The 'IntegrationFile' reference, used in messages, and later converted to a physical file.
	public static final String COMMAND_INTEGRATION_FILE = "IntegrationFile";
	public static final String COMMAND_IMPORT_RUN = "IntegrationImportRun";
	public static final String COMMAND_TARGET_APPLICATION = MessagingMessage.TARGET_APPLICATION;

	private final Message<File> message;
	private final IntegrationFile integrationFile;

	private IntegrationImportRun run;
	private IntegrationImportRunEvent runEvent;
	private IntegrationImportDefinition importDefinition;
	private IntegrationTargetApplication targetApplication;
	private IntegrationTransformationFile effectiveDateTransformationFile;

	private List<IntegrationTransformationFile> transformationFileList;
	private List<IntegrationTransformationFile> fileToFileTransformationFileList;
	private List<IntegrationTransformationFile> dataFileList;
	private List<IntegrationImportTransformation> systemBeanTransformationList;
	private Date effectiveDate;
	private File inputFile;
	private boolean deleteFileWhenComplete;

	////////////////////////////////////////////////////////////////////////////


	public IntegrationFileProcessorCommand(Message<File> message, IntegrationFile integrationFile, IntegrationImportDefinition importDefinition) {
		this(message, integrationFile, importDefinition, null);
	}


	public IntegrationFileProcessorCommand(Message<File> message, IntegrationFile integrationFile, IntegrationImportDefinition importDefinition, Date effectiveDate) {
		this.message = message;
		this.integrationFile = integrationFile;
		this.importDefinition = importDefinition;
		this.effectiveDate = effectiveDate;
	}


	public List<IntegrationTransformationFile> getAllTransformationFiles() {
		List<IntegrationTransformationFile> fileList = new ArrayList<>();
		if (!CollectionUtils.isEmpty(getTransformationFileList())) {
			fileList.addAll(getTransformationFileList());
		}
		if (!CollectionUtils.isEmpty(getFileToFileTransformationFileList())) {
			fileList.addAll(getFileToFileTransformationFileList());
		}
		if (!CollectionUtils.isEmpty(getDataFileList())) {
			fileList.addAll(getDataFileList());
		}
		if (getEffectiveDateTransformationFile() != null) {
			fileList.add(getEffectiveDateTransformationFile());
		}
		return fileList;
	}


	/**
	 * Returns true (meaning the default effective date should be used) if there is no effective date and no effective date transformation.
	 */
	public boolean isDefaultEffectiveDateNeeded() {
		return ((getEffectiveDate() == null) || (getIntegrationFile().getFileDefinition().getSource().isForceEffectiveDateLoadFromJobFile() && (getEffectiveDateTransformationFile() != null)))
				&& ((getImportDefinition() == null) || (getEffectiveDateTransformationFile() == null));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationImportRun getRun() {
		return this.run;
	}


	public void setRun(IntegrationImportRun run) {
		this.run = run;
	}


	public IntegrationImportRunEvent getRunEvent() {
		return this.runEvent;
	}


	public void setRunEvent(IntegrationImportRunEvent runEvent) {
		this.runEvent = runEvent;
	}


	public Message<File> getMessage() {
		return this.message;
	}


	public IntegrationFile getIntegrationFile() {
		return this.integrationFile;
	}


	public IntegrationTargetApplication getTargetApplication() {
		return this.targetApplication;
	}


	public IntegrationImportDefinition getImportDefinition() {
		return this.importDefinition;
	}


	public IntegrationTransformationFile getEffectiveDateTransformationFile() {
		return this.effectiveDateTransformationFile;
	}


	public void setEffectiveDateTransformationFile(IntegrationTransformationFile effectiveDateTransformationFile) {
		this.effectiveDateTransformationFile = effectiveDateTransformationFile;
	}


	public Date getEffectiveDate() {
		return this.effectiveDate;
	}


	public void setEffectiveDate(Date effectiveDate) {
		this.effectiveDate = effectiveDate;
	}


	public File getInputFile() {
		return this.inputFile;
	}


	public void setInputFile(File inputFile) {
		this.inputFile = inputFile;
	}


	public void setImportDefinition(IntegrationImportDefinition importDefinition) {
		this.importDefinition = importDefinition;
	}


	public void setTargetApplication(IntegrationTargetApplication targetApplication) {
		this.targetApplication = targetApplication;
	}


	public List<IntegrationTransformationFile> getTransformationFileList() {
		return this.transformationFileList;
	}


	public void setTransformationFileList(List<IntegrationTransformationFile> transformationFileList) {
		this.transformationFileList = transformationFileList;
	}


	public List<IntegrationTransformationFile> getDataFileList() {
		return this.dataFileList;
	}


	public void setDataFileList(List<IntegrationTransformationFile> dataFileList) {
		this.dataFileList = dataFileList;
	}


	public List<IntegrationTransformationFile> getFileToFileTransformationFileList() {
		return this.fileToFileTransformationFileList;
	}


	public void setFileToFileTransformationFileList(List<IntegrationTransformationFile> fileToFileTransformationFileList) {
		this.fileToFileTransformationFileList = fileToFileTransformationFileList;
	}


	public List<IntegrationImportTransformation> getSystemBeanTransformationList() {
		return this.systemBeanTransformationList;
	}


	public void setSystemBeanTransformationList(List<IntegrationImportTransformation> systemBeanTransformationList) {
		this.systemBeanTransformationList = systemBeanTransformationList;
	}


	public boolean isDeleteFileWhenComplete() {
		return this.deleteFileWhenComplete;
	}


	public void setDeleteFileWhenComplete(boolean deleteFileWhenComplete) {
		this.deleteFileWhenComplete = deleteFileWhenComplete;
	}
}
