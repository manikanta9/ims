package com.clifton.integration.incoming.transformer;

import com.clifton.core.util.status.Status;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;

import java.io.File;
import java.util.List;
import java.util.Map;


/**
 * @author TerryS
 */
public class FileTransformerCommand {

	private final Integer runId;
	private final File file;
	private final List<IntegrationTransformationFile> transformationFiles;
	private final Map<String, String> transformationParameters;
	private final boolean captureRunBuffer;

	private Status status = new Status();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FileTransformerCommand(Integer runId, File file, List<IntegrationTransformationFile> transformationFiles, Map<String, String> transformationParameters) {
		this(runId, file, transformationFiles, transformationParameters, false);
	}


	public FileTransformerCommand(Integer runId, File file, List<IntegrationTransformationFile> transformationFiles, Map<String, String> transformationParameters, boolean captureRunBuffer) {
		this.runId = runId;
		this.file = file;
		this.transformationFiles = transformationFiles;
		this.transformationParameters = transformationParameters;
		this.captureRunBuffer = captureRunBuffer;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Integer getRunId() {
		return this.runId;
	}


	public File getFile() {
		return this.file;
	}


	public List<IntegrationTransformationFile> getTransformationFiles() {
		return this.transformationFiles;
	}


	public Map<String, String> getTransformationParameters() {
		return this.transformationParameters;
	}


	public boolean isCaptureRunBuffer() {
		return this.captureRunBuffer;
	}


	public Status getStatus() {
		return this.status;
	}


	public void setStatus(Status status) {
		this.status = status;
	}
}
