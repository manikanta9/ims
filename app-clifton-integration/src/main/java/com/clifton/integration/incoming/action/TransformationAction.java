package com.clifton.integration.incoming.action;


import java.util.Map;


/**
 * The <code>TransformationAction</code> defines a transformation action.
 *
 * @author mwacker
 */
public interface TransformationAction {

	/**
	 * Executed the transformation action.
	 *
	 * @param actionData         - contains the import data i.e. IntegrationImportRun, EffectiveDate and InputFile
	 * @param transformationData - use to add parameter that will sent to the transformations
	 */
	public void doAction(Map<String, Object> actionData, Map<String, String> transformationData);
}
