package com.clifton.integration.incoming.transformer;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;
import com.clifton.integration.incoming.transformation.IntegrationTransformationUtilHandler;
import org.apache.commons.vfs2.FileSystemManager;
import org.apache.commons.vfs2.impl.StandardFileSystemManager;
import org.pentaho.di.core.Const;
import org.pentaho.di.core.KettleEnvironment;
import org.pentaho.di.core.exception.KettleException;
import org.pentaho.di.core.exception.KettleXMLException;
import org.pentaho.di.core.logging.CentralLogStore;
import org.pentaho.di.core.logging.KettleLogStore;
import org.pentaho.di.core.plugins.PluginFolder;
import org.pentaho.di.core.plugins.StepPluginType;
import org.pentaho.di.core.util.EnvUtil;
import org.pentaho.di.core.vfs.KettleVFS;
import org.pentaho.di.job.Job;
import org.pentaho.di.job.JobMeta;
import org.springframework.core.io.FileSystemResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.util.ResourceUtils;

import javax.annotation.PreDestroy;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;


public class PentahoTransformer implements FileTransformer {

	private static final String ORIGINAL_INPUT_FILE_NAME_PARAMETER = "ORIGINAL_INPUT_FILE_NAME";
	private static final String INPUT_FILE_NAME_PARAMETER = "INPUT_FILE_NAME";
	private static final String INPUT_RUN_ID_PARAMETER = "INPUT_RUN_ID";
	private static final String TRANSFORMATION_FILE_PARAMETER = "TRANSFORMATION_FILE";
	/**
	 * Created during FILE_TO_FILE transformations and is a temporary file used later
	 * in DATA_TRANSFORMATION, then automatically cleaned up.
	 */
	private static final String TRANSFORMATION_OUTPUT_FILE_NAME_PARAMETER = "OUTPUT_FILE_NAME";
	/**
	 * Used by transformation when they control the output file name;
	 * simply provides the based path that should be used for output.
	 */
	private static final String TRANSFORMATION_OUTPUT_FILE_PATH_PARAMETER = "OUTPUT_FILE_PATH";

	private static final String PROP_KETTLE_HOME = "KETTLE_HOME";
	private static final String FOLDER_KETTLE_HOME = "kettle_home";
	private static final String KETTLE_PLUGINS_RELATIVE_DIR = "classpath:../../plugins/";
	private static final String JOB_FILENAME = "StandardJob.kjb";
	private static final long JOB_TIMEOUT_5_MIN = Duration.ofMinutes(5).toMillis();

	private String transformationResultOutputFilePath;

	private List<String> transformationPluginList;
	private IntegrationTransformationUtilHandler integrationTransformationUtilHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public File doTransform(FileTransformerCommand fileTransformerCommand) {
		boolean error = false;
		TransformationExecutionCommand command = null;
		try {
			setKettleHome();

			initializeKettleEnvironment();
			command = new TransformationExecutionCommand(new JobMeta(getJobFilename(), null), fileTransformerCommand);

			for (IntegrationTransformationFile transformationFile : CollectionUtils.getIterable(fileTransformerCommand.getTransformationFiles())) {
				executeTransformation(transformationFile, command);
			}
			File appended = appendTemporaryFiles(command);
			if (appended != null) {
				command.setOutputFile(appended);
			}

			return command.getOutputFile();
		}
		catch (KettleException e) {
			error = true;
			throw new RuntimeException("Error starting kettle.", e);
		}
		catch (Throwable e) {
			error = true;
			throw new RuntimeException("Failed to run transformation.", e);
		}
		finally {
			if (command != null) {
				removeTemporaryFiles(command.getTemporaryFileList());
				if (error && !StringUtils.isEmpty(command.getTransformationResultFileName())) {
					try {
						FileUtils.delete(new File(command.getTransformationResultFileName()));
					}
					catch (Exception e) {
						// do not suppress the original exception.
						LogUtils.error(this.getClass(), String.format("Could not delete transformation results file [%s].", command.getTransformationResultFileName()) + e.getMessage());
					}
				}
				if (fileTransformerCommand.isCaptureRunBuffer()) {
					fileTransformerCommand.setStatus(command.getStatus());
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void executeTransformation(IntegrationTransformationFile transformationFile, TransformationExecutionCommand command) throws KettleXMLException {
		if (transformationFile.isRunnable()) {
			addPreviousTransformationResultToTemporaryFileList(transformationFile, command);

			File newFile;
			if (transformationFile.getFileName().toLowerCase().endsWith(".kjb")) {
				newFile = runJob(transformationFile, command);
			}
			else {
				newFile = runTransformation(null, transformationFile, command);
			}

			// use the new file for the next transformation
			if (newFile != null) {
				command.setFile(newFile);
				if (transformationFile.isAppendFile() && !command.getAppendedFileList().contains(newFile)) {
					command.getAppendedFileList().add(newFile);
				}
			}
		}
	}


	private void addPreviousTransformationResultToTemporaryFileList(IntegrationTransformationFile transformationFile, TransformationExecutionCommand command) {
		if (transformationFile.getTransformation() != null && transformationFile.getTransformation().getType().isFileReturned()) {
			ValidationUtils.assertTrue(transformationFile.getTransformation().getType().getRequiredTransformationFileType() == transformationFile.getFileType(), "Transformation files of type [" + transformationFile.getFileType() + "] are not supported for [" + transformationFile.getTransformation().getType() + "] transformations.");
			if (!StringUtils.isEmpty(command.getTransformationResultFileName())) {
				File previousFile = new File(command.getTransformationResultFileName());
				if (!command.getTemporaryFileList().contains(previousFile)) {
					command.getTemporaryFileList().add(previousFile);
				}
			}
			command.setTransformationResultFileName(createNewTempFileName(command));
		}
	}


	private File appendTemporaryFiles(TransformationExecutionCommand command) {
		if (!CollectionUtils.isEmpty(command.getAppendedFileList())) {
			File results = new File(createNewTempFileName(command));
			try {
				for (File fileToAppend : command.getAppendedFileList()) {
					try (FileOutputStream outputStream = new FileOutputStream(results, true);
					     FileInputStream inputStream = new FileInputStream(fileToAppend)) {
						FileUtils.transferFromInputToOutput(inputStream, outputStream);
					}
				}
			}
			catch (Exception e) {
				command.getTemporaryFileList().add(results);
				throw new RuntimeException("Could not append files.");
			}
			if (!command.getTemporaryFileList().contains(command.getOutputFile())) {
				command.getTemporaryFileList().add(command.getOutputFile());
			}
			return results;
		}
		return null;
	}


	private void removeTemporaryFiles(List<File> temporaryFileList) {
		for (File tempFile : CollectionUtils.getIterable(temporaryFileList)) {
			try {
				FileUtils.delete(tempFile);
			}
			catch (Exception e) {
				LogUtils.error(getClass(), "Failed to delete temporary integration file: " + tempFile, e);
			}
		}
	}


	private File runJob(IntegrationTransformationFile transformationFile, TransformationExecutionCommand command) throws KettleXMLException {
		String jobFileNameAndPath = getIntegrationTransformationUtilHandler().getTransformationFilePath(transformationFile);
		JobMeta jobMeta = new JobMeta(jobFileNameAndPath, null);
		return runTransformation(jobMeta, transformationFile, command);
	}


	private File runTransformation(JobMeta jobMeta, IntegrationTransformationFile integrationTransformationFile, TransformationExecutionCommand command) {
		Job job = null;
		command.setOutputFile(null);
		try {
			File transformationFile = getIntegrationTransformationUtilHandler().getIntegrationTransformationFile(integrationTransformationFile);
			if (command.getFile().exists()) {
				job = new Job(null, jobMeta != null ? jobMeta : command.getJobMeta());
				job.setVariable(ORIGINAL_INPUT_FILE_NAME_PARAMETER, command.getOriginalInputFile().getAbsolutePath());
				job.setVariable(INPUT_FILE_NAME_PARAMETER, command.getFile().getAbsolutePath());
				job.setVariable(INPUT_RUN_ID_PARAMETER, command.getRunId().toString());
				job.setVariable(TRANSFORMATION_FILE_PARAMETER, transformationFile.getAbsolutePath());
				job.setVariable(TRANSFORMATION_OUTPUT_FILE_NAME_PARAMETER, command.getTransformationResultFileName());
				job.setVariable(TRANSFORMATION_OUTPUT_FILE_PATH_PARAMETER, getTransformationResultOutputFilePath());
				for (Map.Entry<String, String> transformationParameter : command.getTransformationParameters().entrySet()) {
					job.setVariable(transformationParameter.getKey(), transformationParameter.getValue());
				}
				job.start();
				job.waitUntilFinished(JOB_TIMEOUT_5_MIN);
				handleJobErrors(job);
			}
		}
		finally {
			if (job != null) {
				command.addTransformationBuffer(integrationTransformationFile, getLoggerOutput(job, true));
				CentralLogStore.discardLines(job.getLogChannelId(), false);
				//Always expect the output file to be set in 'OUTPUT_FILE_NAME'
				if (job.getVariable(TRANSFORMATION_OUTPUT_FILE_NAME_PARAMETER) != null) {
					command.setOutputFile(new File(job.getVariable(TRANSFORMATION_OUTPUT_FILE_NAME_PARAMETER)));
				}
				else if (!StringUtils.isEmpty(command.getTransformationResultFileName())) {
					command.setOutputFile(new File(command.getTransformationResultFileName()));
				}
			}
		}
		return command.getOutputFile();
	}


	private void handleJobErrors(Job job) {
		if (job.getErrors() > 0) {
			String formattedErrors = getLoggerOutput(job, false);
			if (!StringUtils.isEmpty(formattedErrors)) {
				throw new RuntimeException(formattedErrors);
			}
		}
		if (!job.isFinished()) {
			String formattedErrors = KettleLogStore.getAppender().getBuffer(job.getLogChannelId(), true).toString();
			throw new RuntimeException("The job did not finish after " + Duration.ofMillis(JOB_TIMEOUT_5_MIN).toMinutes() + " minutes. " + formattedErrors);
		}
	}


	private String getLoggerOutput(Job job, boolean includeGeneral) {
		return KettleLogStore.getAppender().getBuffer(job.getLogChannelId(), includeGeneral).toString();
	}


	// For running in IDEs - check to see if KETTLE_HOME system property is set, if not set it programmatically.
	private void setKettleHome() throws IOException {
		if (System.getProperty(PROP_KETTLE_HOME) == null) {
			System.setProperty(PROP_KETTLE_HOME, getIntegrationTransformationUtilHandler().getTransformationSettingsRootPath() + Const.FILE_SEPARATOR + FOLDER_KETTLE_HOME);
			CentralLogStore.init();
		}
	}


	private synchronized void initializeKettleEnvironment() {
		try {
			ResourceLoader resourceLoader = new FileSystemResourceLoader();
			for (String transformationPlugin : CollectionUtils.getIterable(getTransformationPluginList())) {
				StepPluginType.getInstance().getPluginFolders().add(new PluginFolder(resourceLoader.getResource(KETTLE_PLUGINS_RELATIVE_DIR + transformationPlugin).getFile().getAbsolutePath(), true, true));
			}
			KettleEnvironment.init();
			EnvUtil.environmentInit();
		}
		catch (Exception e) {
			throw new RuntimeException("Error starting kettle.", e);
		}
	}


	@PreDestroy
	private void close() {
		FileSystemManager fileSystemManager = KettleVFS.getInstance().getFileSystemManager();
		if (fileSystemManager instanceof StandardFileSystemManager) {
			((StandardFileSystemManager) fileSystemManager).close();
		}
		if (KettleEnvironment.isInitialized()) {
			KettleEnvironment.shutdown();
		}
	}


	private String getJobFilename() throws IOException {
		ResourceLoader resourceLoader = new FileSystemResourceLoader();
		Resource resource = resourceLoader.getResource(ResourceUtils.FILE_URL_PREFIX + getIntegrationTransformationUtilHandler().getTransformationJobRootPath() + Const.FILE_SEPARATOR + JOB_FILENAME);
		return resource.getFile().getAbsolutePath();
	}


	private String createNewTempFileName(TransformationExecutionCommand command) {
		File file = command.getOriginalInputFile();
		return FileUtils.getFileNameWithoutExtension(file.getAbsolutePath()) + "." + UUID.randomUUID() + "." + FileUtils.getFileExtension(file.getName());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static class TransformationExecutionCommand {

		private final JobMeta jobMeta;
		private final Integer runId;
		private final File originalInputFile;
		private final Map<String, String> transformationParameters;
		/**
		 * File passed between transformations;
		 */
		private File file;
		/**
		 * File created by 'FILE_TO_FILE_TRANSFORMATION' transformations, or from transformations that explicitly set the TRANSFORMATION_OUTPUT_FILE_NAME_PARAMETER
		 * These files are sent back through integration when returned from a DATA_TRANSFORMATION and set the parentImportRunId on the newly created run.
		 */
		private File outputFile;

		private String transformationResultFileName;
		private final List<File> temporaryFileList;
		private final List<File> appendedFileList;

		private final boolean captureRunBuffer;
		private final Status status = new Status();


		private TransformationExecutionCommand(JobMeta jobMeta, FileTransformerCommand fileTransformerCommand) {
			this.jobMeta = jobMeta;
			this.runId = fileTransformerCommand.getRunId();
			this.originalInputFile = fileTransformerCommand.getFile();
			this.file = fileTransformerCommand.getFile();
			this.transformationParameters = fileTransformerCommand.getTransformationParameters();
			this.temporaryFileList = new ArrayList<>();
			this.appendedFileList = new ArrayList<>();
			this.captureRunBuffer = fileTransformerCommand.isCaptureRunBuffer();
		}


		public void addTransformationBuffer(IntegrationTransformationFile integrationTransformationFile, String buffer) {
			if (isCaptureRunBuffer() && !StringUtils.isEmpty(buffer)) {
				List<String> lines = StringUtils.splitOnDelimiter(buffer, System.lineSeparator());
				String category = integrationTransformationFile.getFileName();
				for (String line : lines) {
					getStatus().addDetail(category, line);
				}
			}
		}


		public JobMeta getJobMeta() {
			return this.jobMeta;
		}


		public Integer getRunId() {
			return this.runId;
		}


		public List<File> getTemporaryFileList() {
			return this.temporaryFileList;
		}


		public List<File> getAppendedFileList() {
			return this.appendedFileList;
		}


		public File getFile() {
			return this.file;
		}


		public void setFile(File file) {
			this.file = file;
		}


		public File getOutputFile() {
			return this.outputFile;
		}


		public void setOutputFile(File outputFile) {
			this.outputFile = outputFile;
		}


		public File getOriginalInputFile() {
			return this.originalInputFile;
		}


		public Map<String, String> getTransformationParameters() {
			return this.transformationParameters;
		}


		public String getTransformationResultFileName() {
			return this.transformationResultFileName;
		}


		public void setTransformationResultFileName(String transformationResultFileName) {
			this.transformationResultFileName = transformationResultFileName;
		}


		public boolean isCaptureRunBuffer() {
			return this.captureRunBuffer;
		}


		public Status getStatus() {
			return this.status;
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTransformationResultOutputFilePath() {
		return this.transformationResultOutputFilePath;
	}


	public void setTransformationResultOutputFilePath(String transformationResultOutputFilePath) {
		this.transformationResultOutputFilePath = transformationResultOutputFilePath;
	}


	public List<String> getTransformationPluginList() {
		return this.transformationPluginList;
	}


	public void setTransformationPluginList(List<String> transformationPluginList) {
		this.transformationPluginList = transformationPluginList;
	}


	public IntegrationTransformationUtilHandler getIntegrationTransformationUtilHandler() {
		return this.integrationTransformationUtilHandler;
	}


	public void setIntegrationTransformationUtilHandler(IntegrationTransformationUtilHandler integrationTransformationUtilHandler) {
		this.integrationTransformationUtilHandler = integrationTransformationUtilHandler;
	}
}
