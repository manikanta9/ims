package com.clifton.integration.incoming.file;


import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.integration.source.IntegrationSourceService;
import com.clifton.integration.source.search.IntegrationSourceSearchForm;
import org.springframework.integration.file.DefaultDirectoryScanner;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Filters the directory file scanner using a list of sub folders loaded for integration sources.
 */
public class IntegrationFileSourceDirectoryScanner extends DefaultDirectoryScanner {

	private IntegrationSourceService integrationSourceService;

	/**
	 * If true the folderNameList will not be used and all files will be accepted.
	 */
	private boolean acceptAllFiles = false;
	/**
	 * A list of sub folder to pull files from.  The underlying scanner will find all files in the source directory,
	 * but this class will filter to only the files in the folders listed.
	 */
	private Set<String> sourceFolderNameList;
	/**
	 * The counter for the re-query.
	 */
	private int queryCount = 0;
	/**
	 * The number of times the listEligibleFiles method can be with out resetting the folder list.
	 * <p>
	 * If the poller is setup to poll every 1 seconds, then setting this to 60 will re-query the list every minute.
	 */
	private int requeryFolderListCount = 60;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	protected File[] listEligibleFiles(File directory) throws IllegalArgumentException {
		initializeSourceFolderList(directory.toPath());
		File[] fileArray = new File[0];
		try {
			List<File> readyFileList = new ArrayList<>();
			Map<FilePath, BasicFileAttributes> fileMap = FileUtils.getFilesFromDirectory(FilePath.forFile(directory), true, this.sourceFolderNameList);
			for (FilePath filePath : fileMap.keySet()) {
				readyFileList.add(filePath.toFile());
			}
			fileArray = readyFileList.toArray(new File[0]);
		}
		catch (Exception e) {
			LogUtils.error(getClass(), "Failed to list files: " + e.getMessage(), e);
		}
		return fileArray;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private synchronized void initializeSourceFolderList(Path directory) {
		if (!isAcceptAllFiles()) {
			if (this.sourceFolderNameList == null || this.queryCount >= this.requeryFolderListCount) {
				this.sourceFolderNameList = new HashSet<>();
				this.sourceFolderNameList.add(directory.getFileName().toString().toLowerCase());
				List<IntegrationSource> sourceList = getIntegrationSourceList();
				for (IntegrationSource source : CollectionUtils.getIterable(sourceList)) {
					this.sourceFolderNameList.add(source.getFtpRootFolder().toLowerCase());
				}
			}
			this.queryCount = 0;
		}
	}


	private List<IntegrationSource> getIntegrationSourceList() {
		IntegrationSourceSearchForm searchForm = new IntegrationSourceSearchForm();
		searchForm.setFtpRootFolderNotNull(true);
		searchForm.setFtpWatchingEnabled(true);
		return getIntegrationSourceService().getIntegrationSourceList(searchForm);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public void setRequeryFolderListCount(int requeryListCount) {
		this.requeryFolderListCount = requeryListCount;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	public boolean isAcceptAllFiles() {
		return this.acceptAllFiles;
	}


	public void setAcceptAllFiles(boolean acceptAllFiles) {
		this.acceptAllFiles = acceptAllFiles;
	}
}
