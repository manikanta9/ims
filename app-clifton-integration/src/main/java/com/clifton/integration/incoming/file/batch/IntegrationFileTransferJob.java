package com.clifton.integration.incoming.file.batch;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileDefinition;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.file.search.IntegrationFileSearchForm;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.integration.source.IntegrationSourceService;

import java.io.File;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * <code>IntegrationFileTransferJob</code> does not iterate into sub-directories.
 *
 * @author TerryS
 */
public class IntegrationFileTransferJob implements Task, ValidationAware {

	/**
	 * The path to the network location where we will be pulling files from.
	 */
	private String fromPath;

	/**
	 * The network path where the files are going to.
	 */
	private String toPath;

	/**
	 * The filter pattern.
	 */
	private String filterPattern;

	/**
	 * File name pattern regex.
	 */
	private String fileNameRegex;

	/**
	 * Delete the files from the source directory after they have been moved.
	 */
	private boolean deleteSourceFiles;

	/**
	 * Number of days to go back to calculate the DATE variable for the filterRegex template.
	 */
	private Integer daysFromToday = 1;

	/**
	 * The date generation strategy used to calculate the DATE variable for the filterRegex template.
	 */
	private DateGenerationOptions dateGenerationOption;

	/**
	 * The file is being copied to an integration monitored folder, copy the file with the .write extension and remove it upon copy complete.
	 */
	private boolean integrationFolderDestination;

	/**
	 * The JSON string representation of the map of source to destination file extension mappings.
	 */
	private String destinationExtensionMap;

	/**
	 * The map built from the JSON representation in destinationExtensionMap
	 */
	private Map<String, String> translatedDestinationExtensionMap;

	/**
	 * The same file can be reprocessed.
	 */
	private boolean reprocess;


	////////////////////////////////////////////////////////////////////////////

	private TemplateConverter templateConverter;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;
	private IntegrationFileService integrationFileService;
	private IntegrationSourceService integrationSourceService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		// expensive stuff once
		this.translatedDestinationExtensionMap = buildTranslatedDestinationExtensionMap();

		String remoteFilter = doFreemarkerReplacements(getFilterPattern());
		Pattern localPredicate = evaluatePattern(getFileNameRegex());

		final FileCopyResult fileCopyResult = new FileCopyResult();
		String finalFromPath = doFreemarkerReplacements(getFromPath());
		FileContainer rootFileContainer = FileContainerFactory.getFileContainer(finalFromPath);
		try (Stream<String> fileStream = rootFileContainer.list(remoteFilter, f -> fileMatchesRegex(f, localPredicate, fileCopyResult))) {
			fileStream.forEach(fileName -> {
				try {
					Path fileNamePath = Paths.get(fileName);
					String targetFilename = getTargetFileName(fileNamePath);
					processFile(fileName, targetFilename, fileCopyResult);
				}
				catch (Exception e) {
					fileCopyResult.addErrorAndIncrement("Error Copying File:  " + fileName + ExceptionUtils.getDetailedMessage(e));
				}
			});
		}
		catch (IOException e) {
			throw new RuntimeException("Failure copying between network shares ", e);
		}

		fileCopyResult.setMessage(fileCopyResult.toString());
		if (fileCopyResult.getCopiedCount() == 0) {
			fileCopyResult.setActionPerformed(false);
		}
		return fileCopyResult.getStatus();
	}


	public void processFile(String fileName, String targetFilename, FileCopyResult fileCopyResult) throws IOException {
		String finalFromPath = doFreemarkerReplacements(getFromPath());
		String finalToPath = doFreemarkerReplacements(getToPath());

		String targetAbsolutePath = FileUtils.combinePaths(finalToPath, targetFilename);
		if (!isIntegrationFolderDestination() || isReprocess() || process(targetFilename, getIntegrationSourceForTargetPath(targetAbsolutePath))) {
			String targetWriteAbsolutePath = FileUtils.combinePaths(finalToPath, targetFilename + "." + IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION);

			FileContainer targetFileContainer = FileContainerFactory.getFileContainer(targetAbsolutePath);
			FileContainer targetWriteFileContainer = FileContainerFactory.getFileContainer(targetWriteAbsolutePath);
			FileContainer newFileContainer = isIntegrationFolderDestination() ? targetWriteFileContainer : targetFileContainer;

			String sourceAbsolutePath = FileUtils.combinePath(finalFromPath, fileName);
			FileContainer sourceFileContainer = FileContainerFactory.getFileContainer(sourceAbsolutePath);
			// will throw an exception if the destination file exists
			moveOrCopyFile(sourceFileContainer, newFileContainer, fileCopyResult);
			if (isIntegrationFolderDestination()) {
				boolean successful = targetWriteFileContainer.renameToFile(targetFileContainer);
				if (!successful) {
					fileCopyResult.addErrorAndIncrement(String.format("Could not rename [%s] to [%s].", targetWriteFileContainer.getAbsolutePath(), targetFileContainer.getAbsolutePath()));
					// only delete the writing file if the original exists
					if (!isDeleteSourceFiles()) {
						// delete writing file if rename operation fails
						targetWriteFileContainer.deleteFile();
					}
				}
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void validate() throws ValidationException {
		String finalFromPath = doFreemarkerReplacements(getFromPath());
		String finalToPath = doFreemarkerReplacements(getToPath());

		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(finalFromPath).isDirectory(), "The from path [" + finalFromPath + "] does not exist.");
		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(finalToPath).isDirectory(), "The to path [" + finalToPath + "] does not exist.");
	}


	private boolean fileMatchesRegex(String fileName, Pattern filenamePredicate, FileCopyResult fileCopyResult) {
		fileCopyResult.addServerFilteredFileName(fileName);
		boolean fileNameMatches = filenamePredicate.matcher(fileName).matches();
		if (fileNameMatches) {
			fileCopyResult.addClientFilteredFileName(fileName);
		}
		return fileNameMatches;
	}


	private String getTargetFileName(Path filePath) {
		String targetFilename = filePath.getFileName().toString();
		if (!StringUtils.isEmpty(getDestinationExtensionMap())) {
			String extension = Optional.ofNullable(FileUtils.getFileExtension(targetFilename)).orElse("");
			if (getTranslatedDestinationExtensionMap().containsKey(extension.toUpperCase())) {
				targetFilename = FileUtils.getFileNameWithoutExtension(targetFilename) + "." + getTranslatedDestinationExtensionMap().get(extension.toUpperCase());
			}
			// special pattern for always adding an extension to a file:  [{"+":"txt"}]
			else if (getTranslatedDestinationExtensionMap().containsKey("+")) {
				targetFilename = targetFilename + "." + getTranslatedDestinationExtensionMap().get("+");
			}
		}
		return targetFilename;
	}


	protected IntegrationSource getIntegrationSourceForTargetPath(String targetAbsolutePath) {
		if (!StringUtils.isEmpty(targetAbsolutePath)) {
			File targetAbsoluteFile = new File(targetAbsolutePath);
			File parentAbsoluteDirectory = targetAbsoluteFile.getParentFile();
			if (parentAbsoluteDirectory != null) {
				String inputDirectory = parentAbsoluteDirectory.getName();
				return getIntegrationSourceService().getIntegrationSourceByName(inputDirectory);
			}
		}
		return null;
	}


	protected boolean process(String targetFileName, IntegrationSource integrationSource) {
		IntegrationFileDefinition integrationFileDefinition = getIntegrationFileService().getIntegrationDefinitionForFile(new File(targetFileName), getReportingDate(), integrationSource);
		if (integrationFileDefinition != null && !IntegrationFileService.UNKNOWN_FILE_DEFINITION_FILE_NAME.equals(integrationFileDefinition.getFileName())) {
			IntegrationFileSearchForm integrationFileSearchForm = new IntegrationFileSearchForm();
			integrationFileSearchForm.setExternalFileName(integrationFileDefinition.getExternalFileName());
			integrationFileSearchForm.setEffectiveDate(getReportingDate());
			List<IntegrationFile> integrationFiles = getIntegrationFileService().getIntegrationFileList(integrationFileSearchForm);
			return integrationFiles.isEmpty();
		}
		return false;
	}


	protected void moveOrCopyFile(FileContainer sourceFileContainer, FileContainer destinationFileContainer, FileCopyResult fileCopyResult) throws IOException {
		if (isDeleteSourceFiles()) {
			FileUtils.moveFile(sourceFileContainer, destinationFileContainer);
		}
		else {
			FileUtils.copyFile(sourceFileContainer, destinationFileContainer);
		}
		fileCopyResult.incrementCopiedCount();
	}


	private Date getReportingDate() {
		if (getDateGenerationOption() != null) {
			return getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
		}
		return getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.PREVIOUS_BUSINESS_DAY);
	}


	private Pattern evaluatePattern(String pattern) {
		if (!StringUtils.isEmpty(pattern)) {
			String freeMarker = doFreemarkerReplacements(pattern);
			if (!StringUtils.isEmpty(freeMarker)) {
				return Pattern.compile(freeMarker);
			}
		}
		return Pattern.compile(".*");
	}


	private String doFreemarkerReplacements(String filter) {
		if (!StringUtils.isEmpty(filter)) {
			TemplateConfig config = new TemplateConfig(filter);
			config.addBeanToContext("DATE", DateUtils.clearTime(getReportingDate()));
			return getTemplateConverter().convert(config);
		}
		return "*";
	}


	@SuppressWarnings("unchecked")
	private Map<String, String> buildTranslatedDestinationExtensionMap() {
		if (!StringUtils.isEmpty(getDestinationExtensionMap())) {
			Map<String, Object> objectMap = (new JsonStringToMapConverter()).convert(getDestinationExtensionMap());
			if (objectMap.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME)) {
				List<Object> arr = (List<Object>) objectMap.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
				if (!CollectionUtils.isEmpty(arr)) {
					Map<String, String> strMap = (Map<String, String>) arr.get(0);
					return strMap.entrySet().stream()
							.collect(Collectors.toMap(e -> e.getKey().toUpperCase(), Map.Entry::getValue));
				}
			}
		}
		return Collections.emptyMap();
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public String getFromPath() {
		return this.fromPath;
	}


	public void setFromPath(String fromPath) {
		this.fromPath = fromPath;
	}


	public String getToPath() {
		return this.toPath;
	}


	public void setToPath(String toPath) {
		this.toPath = toPath;
	}


	public String getFilterPattern() {
		return this.filterPattern;
	}


	public void setFilterPattern(String filterPattern) {
		this.filterPattern = filterPattern;
	}


	public String getFileNameRegex() {
		return this.fileNameRegex;
	}


	public void setFileNameRegex(String fileNameRegex) {
		this.fileNameRegex = fileNameRegex;
	}


	public boolean isDeleteSourceFiles() {
		return this.deleteSourceFiles;
	}


	public void setDeleteSourceFiles(boolean deleteSourceFiles) {
		this.deleteSourceFiles = deleteSourceFiles;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public boolean isIntegrationFolderDestination() {
		return this.integrationFolderDestination;
	}


	public void setIntegrationFolderDestination(boolean integrationFolderDestination) {
		this.integrationFolderDestination = integrationFolderDestination;
	}


	public boolean isReprocess() {
		return this.reprocess;
	}


	public void setReprocess(boolean reprocess) {
		this.reprocess = reprocess;
	}


	public String getDestinationExtensionMap() {
		return this.destinationExtensionMap;
	}


	public void setDestinationExtensionMap(String destinationExtensionMap) {
		this.destinationExtensionMap = destinationExtensionMap;
	}


	public Map<String, String> getTranslatedDestinationExtensionMap() {
		return this.translatedDestinationExtensionMap;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	/**
	 * Used to hold the results of the folder copy.  Needed because the
	 * lambda loop can only access final objects.
	 */
	protected static class FileCopyResult {

		private final Status status;

		private int errorCount = 0;
		private int filteredServerCount = 0;
		private int filteredClientCount = 0;
		private int copiedCount = 0;


		public FileCopyResult() {
			this.status = Status.ofEmptyMessage();
		}


		@Override
		public String toString() {
			return "File Transfer Counts:\n" +
					"Error Count=" + this.errorCount + "\n" +
					"Server Filtered Count=" + this.filteredServerCount + "\n" +
					"Client Filtered Count=" + this.filteredClientCount + "\n" +
					"Copied Count=" + this.copiedCount + "\n" +
					this.status.getDetailList().stream().map(d -> d.getCategory() + ": " + d.getNote()).collect(Collectors.joining("\n"));
		}


		public void setActionPerformed(boolean actionPerformed) {
			this.status.setActionPerformed(actionPerformed);
		}


		public void setMessage(String message) {
			this.status.setMessage(message);
		}


		public void addErrorAndIncrement(String errorMessage) {
			this.status.addError(errorMessage);
			incrementErrorCount();
		}


		public void incrementErrorCount() {
			this.errorCount++;
		}


		public void addServerFilteredFileName(String fileName) {
			this.status.addDetail("SERVER FILTERED", fileName);
			this.filteredServerCount++;
		}


		public void addClientFilteredFileName(String fileName) {
			this.status.addDetail("CLIENT FILTERED", fileName);
			this.filteredClientCount++;
		}


		public void incrementCopiedCount() {
			this.copiedCount++;
		}


		public int getCopiedCount() {
			return this.copiedCount;
		}


		public Status getStatus() {
			return this.status;
		}
	}
}
