package com.clifton.integration.incoming.action;


import com.clifton.core.cache.TimedEvictionCache;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.integration.incoming.file.IntegrationFileProcessorCommand;
import com.clifton.integration.incoming.server.event.IntegrationEventService;
import com.clifton.integration.target.IntegrationTargetApplication;
import com.clifton.integration.target.IntegrationTargetApplicationService;
import com.clifton.integration.target.search.IntegrationTargetApplicationSearchForm;

import java.time.Duration;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationActionEventPostTransformationAction</code> runs all events that are defined for the definition.
 *
 * @author mwacker
 */
public class IntegrationActionEventPostTransformationAction implements TransformationAction {

	private IntegrationImportService integrationImportService;
	private IntegrationEventService integrationEventService;
	private IntegrationTargetApplicationService integrationTargetApplicationService;

	private TimedEvictionCache<IntegrationImportRunEvent> integrationRunEventTimedEvictionCache;


	@Override
	public void doAction(Map<String, Object> actionData, @SuppressWarnings("unused") Map<String, String> transformationData) {
		if (actionData != null) {
			IntegrationImportRun run = (IntegrationImportRun) actionData.get(IntegrationFileProcessorCommand.COMMAND_IMPORT_RUN);
			if (run.getIntegrationImportDefinition().isRaiseExternalEvent()) {
				ValidationUtils.assertNotNull(StringUtils.isEmpty(run.getIntegrationImportDefinition().getType().getEventName()), "Cannot raise event for ["
						+ run.getIntegrationImportDefinition().getName() + "] because event name for type [" + run.getIntegrationImportDefinition().getType().getName() + "] is not defined.");

				//If explicit application target was specified; only process it; otherwise run for all targets
				List<IntegrationTargetApplication> targetApplicationList;
				IntegrationTargetApplication explicitTargetApplication = (IntegrationTargetApplication) actionData.get(IntegrationFileProcessorCommand.COMMAND_TARGET_APPLICATION);
				if (explicitTargetApplication != null) {
					targetApplicationList = Collections.singletonList(explicitTargetApplication);
				}
				else {
					IntegrationTargetApplicationSearchForm searchForm = new IntegrationTargetApplicationSearchForm();
					searchForm.setDefinitionTypeId(run.getIntegrationImportDefinition().getType().getId());
					targetApplicationList = getIntegrationTargetApplicationService().getIntegrationTargetApplicationList(searchForm);
				}

				for (IntegrationTargetApplication targetApplication : CollectionUtils.getIterable(targetApplicationList)) {
					IntegrationImportRunEvent runEvent = createImportRunEvent(run, targetApplication);
					//Add to timed eviction cache; cache is checked every ten seconds and will evict if requestTimeout has been reached and event is still running
					getIntegrationEventService().sendIntegrationEventMessage(runEvent, run.getIntegrationImportDefinition().getType().getEventName(), targetApplication);
					getIntegrationRunEventTimedEvictionCache().add(runEvent, Duration.ofMillis(targetApplication.getRequestTimeout()));
				}
			}
		}
	}


	private IntegrationImportRunEvent createImportRunEvent(IntegrationImportRun run, IntegrationTargetApplication targetApplication) {
		// create a new run even
		IntegrationImportRunEvent runEvent = new IntegrationImportRunEvent();
		runEvent.setImportRun(run);
		runEvent.setTargetApplication(targetApplication);
		runEvent.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING));
		return getIntegrationImportService().saveIntegrationImportRunEvent(runEvent);
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public TimedEvictionCache<IntegrationImportRunEvent> getIntegrationRunEventTimedEvictionCache() {
		return this.integrationRunEventTimedEvictionCache;
	}


	public void setIntegrationRunEventTimedEvictionCache(TimedEvictionCache<IntegrationImportRunEvent> integrationRunEventTimedEvictionCache) {
		this.integrationRunEventTimedEvictionCache = integrationRunEventTimedEvictionCache;
	}


	public IntegrationTargetApplicationService getIntegrationTargetApplicationService() {
		return this.integrationTargetApplicationService;
	}


	public void setIntegrationTargetApplicationService(IntegrationTargetApplicationService integrationTargetApplicationService) {
		this.integrationTargetApplicationService = integrationTargetApplicationService;
	}


	public IntegrationEventService getIntegrationEventService() {
		return this.integrationEventService;
	}


	public void setIntegrationEventService(IntegrationEventService integrationEventService) {
		this.integrationEventService = integrationEventService;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}
}
