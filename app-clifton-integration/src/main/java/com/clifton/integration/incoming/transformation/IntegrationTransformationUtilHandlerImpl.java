package com.clifton.integration.incoming.transformation;

import com.atlassian.crowd.integration.springsecurity.CrowdSSOAuthenticationToken;
import com.atlassian.crowd.integration.springsecurity.user.CrowdUserDetails;
import com.clifton.core.dataaccess.file.DocumentContentStream;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.TempFilePath;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.document.DocumentManagementProvider;
import com.clifton.document.DocumentRecord;
import com.clifton.document.converters.DocumentToDocumentRecordConverter;
import org.pentaho.di.core.Const;
import org.springframework.core.io.ContextResource;
import org.springframework.core.io.DefaultResourceLoader;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.io.File;
import java.io.InputStream;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * @author mwacker
 */
public class IntegrationTransformationUtilHandlerImpl<T> implements IntegrationTransformationUtilHandler {

	private static final String TRANSFORMATION_FILE_TABLE_NAME = "IntegrationTransformationFile";
	private static final String PROP_TRANSFORMATION_HOME = "transformations";


	private IntegrationTransformationService integrationTransformationService;
	private DocumentManagementProvider<T> documentManagementProvider;
	private DocumentToDocumentRecordConverter<T> documentToDocumentRecordConverter;


	/**
	 * Relative path to the pentaho root folder
	 * This path will be relative to the working directory 'user.dir'
	 */
	private String relativeJobAndSettingsRootPath;

	/**
	 * Path to the transformation root folder
	 */
	private String transformationRootPath;

	/**
	 * The Username for accessing the document server.
	 */
	private String documentServerUserName;

	/**
	 * The resolved {@link #getTransformationSettingsRootPath() transformation settings root path}.
	 */
	private volatile String resolvedTransformationSettingsRootPath;
	private final Object resolvedTransformationRootPathLock = new Object();


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getTransformationFilePath(IntegrationTransformationFile transformationFile) {
		if (transformationFile.isTransformationFileNameFullPath()) {
			return transformationFile.getFileName();
		}
		String transformationFolder = transformationFile.getTransformation() == null || StringUtils.isEmpty(transformationFile.getTransformation().getDefinitionType().getTransformationFolder()) ? "" : transformationFile.getTransformation().getDefinitionType().getTransformationFolder() + Const.FILE_SEPARATOR;
		return FileUtils.combinePath(getTransformationFileRootPath(), transformationFolder) + transformationFile.getFileName();
	}


	@Override
	public String getTransformationJobRootPath() {
		return getTransformationSettingsRootPath() + Const.FILE_SEPARATOR + PROP_TRANSFORMATION_HOME;
	}


	@Override
	public String getTransformationSettingsRootPath() {
		String resolvedPath = getResolvedTransformationSettingsRootPath();
		// Double-checked locking
		if (resolvedPath == null) {
			synchronized (getResolvedTransformationSettingsRootPathLock()) {
				resolvedPath = getResolvedTransformationSettingsRootPath();
				if (resolvedPath == null) {
					resolvedPath = resolveTransformationSettingsRootPath();
					setResolvedTransformationSettingsRootPath(resolvedPath);
				}
			}
		}
		return resolvedPath;
	}


	@Override
	public String getTransformationFileRootPath() {
		return getTransformationRootPath();
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Resolves the {@link #getTransformationSettingsRootPath() transformation settings root path}. If the configured path is accessible on the class path but not accessible on the
	 * file system then a temporary directory will be created to host the class path resources.
	 */
	private String resolveTransformationSettingsRootPath() {
		// Find root path on class path
		ResourceLoader resourceLoader = new DefaultResourceLoader();
		Resource resource = resourceLoader.getResource(getRelativeJobAndSettingsRootPath());
		final String rootPath;
		try {
			if (resource.isFile()) {
				// Path is on filesystem; use path directly
				rootPath = resource.getFile().getAbsolutePath();
			}
			else if (resource instanceof ContextResource) {
				// Path is not on filesystem (typically inside a JAR); make externally accessible by copying resources to a temporary directory
				try (FileSystem resourceFileSystem = FileSystems.newFileSystem(resource.getURI(), Collections.emptyMap())) {
					Path contextPath = resourceFileSystem.getPath(((ContextResource) resource).getPathWithinContext()).toAbsolutePath();
					TempFilePath tempDir = TempFilePath.createTempDirectory("kettle-home-");
					try (Stream<Path> contextPathStream = Files.walk(contextPath)) {
						List<Path> fileList = contextPathStream
								.skip(1) // Skip root directory
								.collect(Collectors.toList());
						Path tempDirPath = tempDir.getTempFile().toPath();
						for (Path file : fileList) {
							Path newPath = tempDirPath.resolve(contextPath.relativize(file).toString());
							Files.copy(file, newPath, StandardCopyOption.REPLACE_EXISTING);
						}
					}
					rootPath = tempDir.toString();
				}
			}
			else {
				throw new RuntimeException(String.format("Unable to derive a directory name for resources of type [%s]. Resource: [%s].", resource.getClass(), resource));
			}
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to resolve transformation settings root path.", e);
		}
		return rootPath;
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public File getIntegrationTransformationFile(IntegrationTransformationFile transformationFile) {
		String fileNameAndPath = getTransformationFilePath(transformationFile);
		if (!FileUtils.fileExists(fileNameAndPath) || transformationFile.isUpdateRequired()) {
			downloadIntegrationTransformationFile(transformationFile);
		}
		return new File(fileNameAndPath);
	}


	@Override
	public void updateIntegrationTransformationFile(IntegrationTransformationFile transformationFile) {
		getIntegrationTransformationFile(transformationFile);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void downloadIntegrationTransformationFile(IntegrationTransformationFile transformationFile) {
		try {
			setDocServerUser();
			DocumentRecord documentRecord = getTransformationDocument(transformationFile);
			downloadDocumentRecordToTarget(transformationFile, documentRecord);

			transformationFile.setUpdateRequired(false);
			getIntegrationTransformationService().saveIntegrationTransformationFile(transformationFile);
		}
		finally {
			removeDocServerUser();
		}
	}


	private void downloadDocumentRecordToTarget(IntegrationTransformationFile transformationFile, DocumentRecord documentRecord) {
		String targetFilePath = getTransformationFilePath(transformationFile);
		try (InputStream stream = downloadDocumentRecord(documentRecord).getStream()) {
			FileUtils.createDirectoryForFile(new File(targetFilePath));
			FileUtils.convertInputStreamToFile(targetFilePath, stream);
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to copy document [" + (documentRecord != null ? documentRecord.getName() : "(documentRecord is null)") + "] to file [" + targetFilePath + "].", e);
		}
	}


	private DocumentRecord getTransformationDocument(IntegrationTransformationFile transformationFile) {
		try {
			return getDocumentRecord(TRANSFORMATION_FILE_TABLE_NAME, transformationFile.getId());
		}
		catch (Throwable e) {
			throw new RuntimeException("Failed to download [" + (transformationFile != null ? transformationFile.getFileName() : "(transformationFile is null)") + "] for transformation [" + transformationFile.getTransformation().getName() + "] from document server.", e);
		}
	}


	private DocumentRecord getDocumentRecord(String tableName, int fkFieldId) {
		DocumentRecord record = null;
		try {
			record = getDocumentRecordForDocument(getDocumentByTableAndFkField(tableName, fkFieldId));
		}
		catch (Exception e) {
			handleDocumentException(e, "Error retrieving document record from system");
		}
		return record;
	}


	public DocumentContentStream downloadDocumentRecord(DocumentRecord record) {
		return getDocumentManagementProvider().getDocumentContentStream(record.getDocumentId());
	}


	private void removeDocServerUser() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		securityContext.setAuthentication(null);
	}


	private void setDocServerUser() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		GrantedAuthority[] authorities = new GrantedAuthority[0];
		CrowdUserDetails principal = new CrowdUserDetails(new com.atlassian.crowd.integration.soap.SOAPPrincipal(getDocumentServerUserName()), authorities);
		CrowdSSOAuthenticationToken token = new CrowdSSOAuthenticationToken(principal, null, authorities);
		securityContext.setAuthentication(token);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private T getDocumentByTableAndFkField(String tableName, int fkFieldId) {
		return getDocumentManagementProvider().getDocumentByTableAndFkField(tableName, fkFieldId);
	}


	private DocumentRecord getDocumentRecordForDocument(T document) {
		return getDocumentToDocumentRecordConverter().convert(document);
	}


	private void handleDocumentException(Exception e, String userFriendlyMessage) {
		// If not a ValidationException - Log the Error
		userFriendlyMessage = userFriendlyMessage + " [" + (e == null ? null : e.getMessage()) + "]";
		LogUtils.errorOrInfo(getClass(), userFriendlyMessage, e);
		throw new ValidationException(userFriendlyMessage, e);
	}


	////////////////////////////////////////////////////////////////////////////
	////////            Custom Getters/Setters                          ////////
	////////////////////////////////////////////////////////////////////////////


	private String getResolvedTransformationSettingsRootPath() {
		// Private property
		return this.resolvedTransformationSettingsRootPath;
	}


	private void setResolvedTransformationSettingsRootPath(String resolvedTransformationSettingsRootPath) {
		// Private property
		this.resolvedTransformationSettingsRootPath = resolvedTransformationSettingsRootPath;
	}


	private Object getResolvedTransformationSettingsRootPathLock() {
		// Private property
		return this.resolvedTransformationRootPathLock;
	}


	public void setRelativeJobAndSettingsRootPath(String relativeJobAndSettingsRootPath) {
		this.relativeJobAndSettingsRootPath = relativeJobAndSettingsRootPath;
		// Clear resolved root path
		setResolvedTransformationSettingsRootPath(null);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DocumentManagementProvider<T> getDocumentManagementProvider() {
		return this.documentManagementProvider;
	}


	public void setDocumentManagementProvider(DocumentManagementProvider<T> documentManagementProvider) {
		this.documentManagementProvider = documentManagementProvider;
	}


	public DocumentToDocumentRecordConverter<T> getDocumentToDocumentRecordConverter() {
		return this.documentToDocumentRecordConverter;
	}


	public void setDocumentToDocumentRecordConverter(DocumentToDocumentRecordConverter<T> documentToDocumentRecordConverter) {
		this.documentToDocumentRecordConverter = documentToDocumentRecordConverter;
	}


	public IntegrationTransformationService getIntegrationTransformationService() {
		return this.integrationTransformationService;
	}


	public void setIntegrationTransformationService(IntegrationTransformationService integrationTransformationService) {
		this.integrationTransformationService = integrationTransformationService;
	}


	public String getDocumentServerUserName() {
		return this.documentServerUserName;
	}


	public void setDocumentServerUserName(String documentServerUserName) {
		this.documentServerUserName = documentServerUserName;
	}


	public String getTransformationRootPath() {
		return this.transformationRootPath;
	}


	public void setTransformationRootPath(String transformationRootPath) {
		this.transformationRootPath = transformationRootPath;
	}


	public String getRelativeJobAndSettingsRootPath() {
		return this.relativeJobAndSettingsRootPath;
	}
}


