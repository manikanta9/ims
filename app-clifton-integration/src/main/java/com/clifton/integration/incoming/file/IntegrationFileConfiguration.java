package com.clifton.integration.incoming.file;

import com.clifton.integration.incoming.action.TransformationAction;

import java.util.List;


/**
 * @author mwacker
 */
public class IntegrationFileConfiguration {

	private List<TransformationAction> preTransformationActionList;
	private List<TransformationAction> postTransformationActionList;


	public List<TransformationAction> getPreTransformationActionList() {
		return this.preTransformationActionList;
	}


	public void setPreTransformationActionList(List<TransformationAction> preTransformationActionList) {
		this.preTransformationActionList = preTransformationActionList;
	}


	public List<TransformationAction> getPostTransformationActionList() {
		return this.postTransformationActionList;
	}


	public void setPostTransformationActionList(List<TransformationAction> postTransformationActionList) {
		this.postTransformationActionList = postTransformationActionList;
	}
}
