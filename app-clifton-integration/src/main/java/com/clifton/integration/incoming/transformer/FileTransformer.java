package com.clifton.integration.incoming.transformer;


import java.io.File;


public interface FileTransformer {

	/**
	 * Does the transformation and returns any action data needed for post transformation actions.  The resulting file is null for data transformations and the final file
	 * for file to file transformations.
	 */
	public File doTransform(FileTransformerCommand command);
}
