package com.clifton.integration.incoming.transformation;

import java.io.File;
import java.io.IOException;


/**
 * @author mwacker
 */
public interface IntegrationTransformationUtilHandler {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getTransformationFilePath(IntegrationTransformationFile transformationFile);


	/**
	 * Returns the path the folder with the default job file and the settings files.
	 */
	public String getTransformationJobRootPath() throws IOException;


	/**
	 * Returns the path the folder were the settings files or folder should be located.
	 */
	public String getTransformationSettingsRootPath() throws IOException;


	/**
	 * Returns the path the transformation folder.
	 */
	public String getTransformationFileRootPath();

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Update all files for a given transformation.
	 *
	 * @param transformationFile
	 */
	public File getIntegrationTransformationFile(IntegrationTransformationFile transformationFile);


	public void updateIntegrationTransformationFile(IntegrationTransformationFile transformationFile);
}
