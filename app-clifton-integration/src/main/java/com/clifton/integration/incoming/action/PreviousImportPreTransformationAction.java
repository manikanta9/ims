package com.clifton.integration.incoming.action;


import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatus;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunEventSearchForm;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.incoming.file.IntegrationFileProcessorCommand;
import com.clifton.integration.incoming.manager.IntegrationManagerPositionType;
import com.clifton.integration.target.IntegrationTargetApplication;
import org.hibernate.Criteria;

import java.util.Date;
import java.util.Map;


/**
 * The <code>PreviousImportPreTransformationAction</code> finds the previous import for the given run and sets the status to REPROCESSED.
 *
 * @author mwacker
 */
public class PreviousImportPreTransformationAction implements TransformationAction {

	private AdvancedUpdatableDAO<IntegrationManagerPositionType, Criteria> integrationManagerPositionTypeDAO;

	private IntegrationImportService integrationImportService;


	@Override
	public void doAction(Map<String, Object> actionData, @SuppressWarnings("unused") Map<String, String> transformationParameters) {
		if (actionData != null) {
			IntegrationImportStatus reprocessedStatus = getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.REPROCESSED);

			IntegrationImportRun run = (IntegrationImportRun) actionData.get(IntegrationFileProcessorCommand.COMMAND_IMPORT_RUN);
			Date effectiveDate = (Date) actionData.get(IntegrationFileProcessorCommand.COMMAND_EFFECTIVE_DATE);
			IntegrationTargetApplication targetApplication = (IntegrationTargetApplication) actionData.get(IntegrationFileProcessorCommand.COMMAND_TARGET_APPLICATION);

			//First find the run
			IntegrationImportRunSearchForm runSearchForm = new IntegrationImportRunSearchForm();
			runSearchForm.setEffectiveDate(effectiveDate);
			runSearchForm.setIntegrationImportDefinitionId(run.getIntegrationImportDefinition().getId());
			runSearchForm.setStatusIdNotEquals(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR).getId());
			for (IntegrationImportRun importRun : CollectionUtils.getIterable(getIntegrationImportService().getIntegrationImportRunList(runSearchForm))) {
				//We don't set runs that were in an error status to reprocessed.
				if (importRun != null && importRun.getId().intValue() != run.getId().intValue() && !importRun.getStatus().getIntegrationImportStatusType().isError()) {
					//Now locate runEvents, if any.
					IntegrationImportRunEventSearchForm runEventSearchForm = new IntegrationImportRunEventSearchForm();
					runEventSearchForm.setImportRunId(importRun.getId());
					for (IntegrationImportRunEvent importRunEvent : CollectionUtils.getIterable(getIntegrationImportService().getIntegrationImportRunEventList(runEventSearchForm))) {
						//Only update the status to REPROCESSED for the targetApplication (if exists) and only when the prior runEvent status is not an error.
						if ((targetApplication == null || (targetApplication.getId().equals(importRunEvent.getTargetApplication().getId())) && !importRunEvent.getStatus().getIntegrationImportStatusType().isError())) {
							importRunEvent.setStatus(reprocessedStatus);
							getIntegrationImportService().saveIntegrationImportRunEvent(importRunEvent);
						}
					}
					importRun.setStatus(reprocessedStatus);
					getIntegrationImportService().saveIntegrationImportRun(importRun);
				}
			}
		}
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public AdvancedUpdatableDAO<IntegrationManagerPositionType, Criteria> getIntegrationManagerPositionTypeDAO() {
		return this.integrationManagerPositionTypeDAO;
	}


	public void setIntegrationManagerPositionTypeDAO(AdvancedUpdatableDAO<IntegrationManagerPositionType, Criteria> integrationManagerPositionTypeDAO) {
		this.integrationManagerPositionTypeDAO = integrationManagerPositionTypeDAO;
	}
}
