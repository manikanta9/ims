package com.clifton.integration.incoming.file;

import java.io.File;
import java.util.Collection;


/**
 * @author stevenf
 */
public interface IntegrationFileGateway {

	public void sendFileListToChannel(Collection<File> files);
}
