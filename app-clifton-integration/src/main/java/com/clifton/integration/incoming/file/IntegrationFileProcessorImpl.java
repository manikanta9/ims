package com.clifton.integration.incoming.file;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.Calendar;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.archive.IntegrationFileArchiveService;
import com.clifton.integration.incoming.action.TransformationAction;
import com.clifton.integration.incoming.definition.IntegrationImportDefinition;
import com.clifton.integration.incoming.definition.IntegrationImportDefinitionColumnMapping;
import com.clifton.integration.incoming.definition.IntegrationImportRun;
import com.clifton.integration.incoming.definition.IntegrationImportRunEvent;
import com.clifton.integration.incoming.definition.IntegrationImportService;
import com.clifton.integration.incoming.definition.IntegrationImportStatus;
import com.clifton.integration.incoming.definition.IntegrationImportStatusTypes;
import com.clifton.integration.incoming.definition.MappingException;
import com.clifton.integration.incoming.definition.TransformationDataTypes;
import com.clifton.integration.incoming.definition.search.IntegrationImportRunSearchForm;
import com.clifton.integration.incoming.file.handler.IntegrationFileHandler;
import com.clifton.integration.incoming.transformation.IntegrationImportTransformation;
import com.clifton.integration.incoming.transformation.IntegrationImportTransformationService;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFileTypes;
import com.clifton.integration.incoming.transformation.IntegrationTransformationService;
import com.clifton.integration.incoming.transformation.IntegrationTransformationUtilHandler;
import com.clifton.integration.incoming.transformation.generator.ImportTransformationGenerator;
import com.clifton.integration.incoming.transformation.parameter.IntegrationTransformationParameterHandler;
import com.clifton.integration.incoming.transformation.parameter.IntegrationTransformationParameterService;
import com.clifton.integration.incoming.transformation.search.IntegrationImportTransformationSearchForm;
import com.clifton.integration.incoming.transformation.search.IntegrationTransformationFileSearchForm;
import com.clifton.integration.incoming.transformer.FileTransformer;
import com.clifton.integration.incoming.transformer.FileTransformerCommand;
import com.clifton.integration.target.IntegrationTargetApplication;
import com.clifton.integration.target.IntegrationTargetApplicationService;
import com.clifton.integration.target.search.IntegrationTargetApplicationSearchForm;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.integration.file.FileHeaders;
import org.springframework.integration.file.FileNameGenerator;
import org.springframework.messaging.Message;
import org.springframework.messaging.support.MessageBuilder;

import java.io.File;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>DefaultFileHandlerProcessor</code> default implementation of IntegrationFileProcessor.
 *
 * @author mwacker
 */
public class IntegrationFileProcessorImpl implements IntegrationFileProcessor {

	private FileTransformer transformer;

	private IntegrationFileArchiveService integrationFileArchiveService;
	private IntegrationFileHandler integrationFileHandler;
	private IntegrationFileService integrationFileService;
	private IntegrationImportService integrationImportService;
	private IntegrationTargetApplicationService integrationTargetApplicationService;
	private IntegrationTransformationService integrationTransformationService;
	private IntegrationTransformationParameterService integrationTransformationParameterService;
	private IntegrationTransformationUtilHandler integrationTransformationUtilHandler;
	private IntegrationImportTransformationService integrationImportTransformationService;
	private SystemBeanService systemBeanService;
	private IntegrationTransformationParameterHandler integrationTransformationParameterHandler;


	private Map<String, IntegrationFileConfiguration> integrationFileConfigurationMap;

	private static final String UNKNOWN_FILE_JOB_NAME = "UNKNOWN FILE";

	/**
	 * Used only to get the name that will be used to save it to the run object.
	 */
	private FileNameGenerator postProcessingFileNameGenerator;

	private CalendarSetupService calendarSetupService;
	private CalendarBusinessDayService calendarBusinessDayService;
	private SecuritySecretService securitySecretService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationImportRun processIntegrationFile(IntegrationFileProcessorCommand command) {
		populateCommand(command);
		validateCommand(command);

		IntegrationImportRun importRun = null;
		//Only used if the definition does not raise external events; otherwise runEvents are created prior to issuing events.
		IntegrationImportRunEvent importRunEvent = null;
		try {
			importRun = createImportRun(command);
			if (!importRun.getIntegrationImportDefinition().isRaiseExternalEvent()) {
				importRunEvent = createImportRunEvent(importRun);
			}
			importRun = runEffectiveDateTransformation(command, importRun);

			ValidationUtils.assertNotNull(importRun.getEffectiveDate(), "EffectiveDate is required.");

			updateTransformationFiles(command);
			executeTransformations(command, importRun);
		}
		catch (MappingException e) {
			handleError(importRun, importRunEvent, getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.MAPPING_ERROR), e);
		}
		catch (Throwable e) {
			handleError(importRun, importRunEvent, getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.ERROR), e);
		}
		finally {
			if (importRun != null) {
				//If no external events are raised, then we can set the end date here; otherwise it will be handled when the last event is processed/timed-out.
				if (!importRun.getIntegrationImportDefinition().isRaiseExternalEvent()) {
					importRun.setEndProcessDate(new Date());
					if (importRunEvent != null && importRunEvent.getError() == null) {
						importRunEvent.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL));
						getIntegrationImportService().saveIntegrationImportRunEvent(importRunEvent);
					}
					getIntegrationImportService().saveIntegrationImportRun(importRun);
				}
				getIntegrationFileService().saveIntegrationFile(importRun.getFile());
			}
			if (command.isDeleteFileWhenComplete()) {
				try {
					FileUtils.delete(command.getInputFile());
				}
				catch (Exception ex) {
					// swallow the error and log it in order to not hide the original error
					LogUtils.error(getClass(), "Failed to delete the temporary file generated by the transformations [" + command.getInputFile() + "].", ex);
				}
			}
		}
		return importRun;
	}

	////////////////////////////////////////////////////////////////////////////
	////////                    Helper Methods                     /////////////
	////////////////////////////////////////////////////////////////////////////


	private IntegrationImportRun createImportRun(IntegrationFileProcessorCommand command) {
		// create a new run
		IntegrationImportRun importRun = new IntegrationImportRun(command.getImportDefinition(), new Date());
		importRun.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING));
		importRun.setEffectiveDate(command.getIntegrationFile().getFileDefinition().isOnePerDay() ? DateUtils.clearTime(command.getEffectiveDate()) : command.getEffectiveDate());
		importRun.setFile(command.getIntegrationFile());
		getIntegrationImportService().saveIntegrationImportRun(importRun);
		return importRun;
	}


	private IntegrationImportRunEvent createImportRunEvent(IntegrationImportRun run) {
		// create a new run even
		IntegrationImportRunEvent runEvent = new IntegrationImportRunEvent();
		runEvent.setImportRun(run);
		runEvent.setTargetApplication(getIntegrationTargetApplicationService().getIntegrationTargetApplicationByName(IntegrationTargetApplication.TARGET_APPLICATION_INTEGRATION));
		runEvent.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.RUNNING));
		return getIntegrationImportService().saveIntegrationImportRunEvent(runEvent);
	}


	private IntegrationImportRun runEffectiveDateTransformation(IntegrationFileProcessorCommand command, IntegrationImportRun importRun) {
		// run the effective date transformation
		if ((command.getEffectiveDateTransformationFile() != null) && ((command.getEffectiveDate() == null) || command.getIntegrationFile().getFileDefinition().getSource().isForceEffectiveDateLoadFromJobFile())) {
			Map<String, String> transformationParameters = new HashMap<>();

			// run the transformation to update the effective date
			FileTransformerCommand fileTransformerCommand = new FileTransformerCommand(importRun.getId(), command.getInputFile(), CollectionUtils.createList(command.getEffectiveDateTransformationFile()), transformationParameters);
			getTransformer().doTransform(fileTransformerCommand);
			// reload the run from the database to get the updated effective date
			importRun = getIntegrationImportService().getIntegrationImportRun(importRun.getId());
			importRun.getFile().setEffectiveDate(importRun.getEffectiveDate());
			getIntegrationFileService().saveIntegrationFile(importRun.getFile());
		}
		return importRun;
	}


	/**
	 * Download all files that need updating from the document server.
	 */
	private void updateTransformationFiles(IntegrationFileProcessorCommand command) {
		for (IntegrationTransformationFile transformationFile : CollectionUtils.getIterable(command.getAllTransformationFiles())) {
			getIntegrationTransformationUtilHandler().updateIntegrationTransformationFile(transformationFile);
		}
	}


	private IntegrationImportRun executeTransformations(IntegrationFileProcessorCommand command, IntegrationImportRun importRun) {
		IntegrationFileConfiguration fileConfig = getIntegrationFileConfig(command.getImportDefinition().getType().getName());

		// populate the action data for the pre and post transformation actions
		Map<String, Object> actionData = new HashMap<>();
		actionData.put(IntegrationFileProcessorCommand.COMMAND_IMPORT_RUN, importRun);
		actionData.put(IntegrationFileProcessorCommand.COMMAND_EFFECTIVE_DATE, importRun.getEffectiveDate());
		actionData.put(IntegrationFileProcessorCommand.COMMAND_INPUT_FILE, command.getInputFile());
		actionData.put(IntegrationFileProcessorCommand.COMMAND_TARGET_APPLICATION, command.getTargetApplication());

		Map<String, String> transformationParameters = new HashMap<>();

		if (command.getImportDefinition().getTransformation() != null) {
			IntegrationTransformation integrationTransformation = command.getImportDefinition().getTransformation();
			if (integrationTransformation.isTemplateTransformation()) {
				configureTransformationTemplateParams(command.getImportDefinition(), transformationParameters);
			}
			else {
				Map<String, String> tmp = getIntegrationTransformationParameterHandler().getIntegrationTransformationParameterMap(integrationTransformation);
				transformationParameters.putAll(tmp);
			}
		}
		configureTransformationParams(command, transformationParameters);

		// run the pre transformation actions
		for (TransformationAction action : CollectionUtils.getIterable(fileConfig.getPreTransformationActionList())) {
			action.doAction(actionData, transformationParameters);
		}

		// run the file to file transformations
		doFileToFileTransformation(importRun, command, transformationParameters);

		// run the system bean based transformations
		doSystemBeanTransformations(importRun, command, transformationParameters);

		// do the main transformation
		doDataTransformation(importRun, command, transformationParameters);

		// run the post transformation actions
		for (TransformationAction action : CollectionUtils.getIterable(fileConfig.getPostTransformationActionList())) {
			action.doAction(actionData, transformationParameters);
		}

		//Only set to success if no external events are raised; otherwise success is dependent on their processing.
		if (!importRun.getIntegrationImportDefinition().isRaiseExternalEvent()) {
			importRun.setStatus(getIntegrationImportService().getIntegrationImportStatusByStatusType(IntegrationImportStatusTypes.SUCCESSFUL));
		}
		importRun.getFile().setSourceDataLoaded(true);
		return importRun;
	}


	private void doFileToFileTransformation(IntegrationImportRun importRun, IntegrationFileProcessorCommand command, Map<String, String> transformationParameters) {
		if (!CollectionUtils.isEmpty(command.getFileToFileTransformationFileList())) {
			FileTransformerCommand fileTransformerCommand = new FileTransformerCommand(importRun.getId(), command.getInputFile(), command.getFileToFileTransformationFileList(), transformationParameters);
			File newFile = getTransformer().doTransform(fileTransformerCommand);
			if (newFile != null && newFile.exists()) {
				command.setInputFile(newFile);
				command.setDeleteFileWhenComplete(true);
			}
		}
	}


	@SuppressWarnings("unchecked")
	private void doSystemBeanTransformations(IntegrationImportRun importRun, IntegrationFileProcessorCommand command, Map<String, String> transformationParameters) {
		for (IntegrationImportTransformation transformation : CollectionUtils.getIterable(command.getSystemBeanTransformationList())) {
			ImportTransformationGenerator<?, ?> transformationGenerator = (ImportTransformationGenerator<?, ?>) getSystemBeanService().getBeanInstance(transformation.getTransformationSystemBean());
			if (transformationGenerator.getInputClass().isAssignableFrom(File.class) && transformationGenerator.getOutputClass().isAssignableFrom(File.class)) {
				ImportTransformationGenerator<File, File> fileTransformationGenerator = (ImportTransformationGenerator<File, File>) transformationGenerator;
				File newFile = fileTransformationGenerator.generateTransformation(command.getInputFile(), importRun.getId(), importRun.getIntegrationImportDefinition(), transformationParameters);
				if (newFile != null && newFile.exists()) {
					command.setInputFile(newFile);
					command.setDeleteFileWhenComplete(true);
				}
			}
			else {
				throw new RuntimeException("Only supports file-to-file transformations.");
			}
		}
	}


	private void doDataTransformation(IntegrationImportRun importRun, IntegrationFileProcessorCommand command, Map<String, String> transformationParameters) {
		// do the main transformation
		if (!CollectionUtils.isEmpty(command.getTransformationFileList())) {
			FileTransformerCommand fileTransformerCommand = new FileTransformerCommand(importRun.getId(), command.getInputFile(), command.getTransformationFileList(), transformationParameters);
			File outputFile = getTransformer().doTransform(fileTransformerCommand);
			if (outputFile != null && outputFile.exists()) {
				//Send the output file back through integration.
				Message<File> message = MessageBuilder.withPayload(outputFile)
						.setHeader(FileHeaders.ORIGINAL_FILE, command.getMessage().getHeaders().get(FileHeaders.ORIGINAL_FILE))
						.setHeader("EffectiveDate", importRun != null ? importRun.getEffectiveDate() : null)
						.setHeader("ParentImportRun", importRun)
						.build();
				getIntegrationFileHandler().handleFile(message);
			}
		}
	}


	private void handleError(IntegrationImportRun importRun, IntegrationImportRunEvent importRunEvent, IntegrationImportStatus status, Throwable e) {
		if (importRun != null) {
			importRun.setStatus(status);
			String error = ExceptionUtils.getOriginalMessage(e) + "\n\n" + ExceptionUtils.toString(e);
			//Truncate the error message if necessary
			if (error.length() > DataTypes.DESCRIPTION_LONG.getLength()) {
				error = error.substring(0, DataTypes.DESCRIPTION_LONG.getLength() - 4).concat("...");
			}
			importRun.setError(error);
			importRun.setEndProcessDate(new Date());
			getIntegrationImportService().saveIntegrationImportRun(importRun);
			//If this raises events we want to manufacture them and set to error since they haven't been raised yet.
			if (importRun.getIntegrationImportDefinition().isRaiseExternalEvent()) {
				IntegrationTargetApplicationSearchForm searchForm = new IntegrationTargetApplicationSearchForm();
				searchForm.setDefinitionTypeId(importRun.getIntegrationImportDefinition().getType().getId());
				for (IntegrationTargetApplication targetApplication : CollectionUtils.getIterable(getIntegrationTargetApplicationService().getIntegrationTargetApplicationList(searchForm))) {
					IntegrationImportRunEvent runEvent = new IntegrationImportRunEvent();
					runEvent.setImportRun(importRun);
					runEvent.setTargetApplication(targetApplication);
					runEvent.setStatus(status);
					runEvent.setError(error);
					getIntegrationImportService().saveIntegrationImportRunEvent(runEvent);
				}
			}
			else {
				//If this didn't raise external events then the local runEvent needs to be updated with the status and error.
				if (importRunEvent != null) {
					importRunEvent.setStatus(status);
					importRunEvent.setError(error);
					getIntegrationImportService().saveIntegrationImportRunEvent(importRunEvent);
				}
			}
		}
		LogUtils.error(getClass(), "Error occurred in integration import run: " + (importRun != null ? importRun.getId() : null), e);
	}


	private Date getEffectiveDate() {
		Date runDate = DateUtils.clearTime(new Date());
		Calendar calendar = getCalendarSetupService().getDefaultCalendar();
		return getCalendarBusinessDayService().getPreviousBusinessDay(CalendarBusinessDayCommand.forDate(runDate, calendar.getId()));
	}


	private void configureTransformationParams(IntegrationFileProcessorCommand command, Map<String, String> transformationParameter) {
		for (IntegrationTransformationFile dataFile : CollectionUtils.getIterable(command.getDataFileList())) {
			transformationParameter.put(dataFile.getFileName(), getIntegrationTransformationUtilHandler().getTransformationFilePath(dataFile));
		}
	}


	private void configureTransformationTemplateParams(IntegrationImportDefinition importDefinition, Map<String, String> transformationParameters) {
		String[] columnNames = importDefinition.getFileDefinition().getFileColumnNamesArray();
		List<IntegrationImportDefinitionColumnMapping> columnMappings = importDefinition.getColumnMappings();

		if (ArrayUtils.isEmpty(columnNames)) {
			throw new RuntimeException("Cannot run template transformation since no column names were specified on the file definition.");
		}

		if (CollectionUtils.isEmpty(columnMappings)) {
			throw new RuntimeException("Cannot run template transformation since no column mappings were specified on the import definition.");
		}

		StringBuilder outputParamBuilder = new StringBuilder();

		//Create a mapping between source column names and their associated data types
		Map<String, TransformationDataTypes> columnDataTypes = new HashMap<>();
		for (IntegrationImportDefinitionColumnMapping mapping : columnMappings) {
			columnDataTypes.put(mapping.getSourceColumnName(), mapping.getDestinationColumn().getDataType());

			if (outputParamBuilder.length() > 0) {
				outputParamBuilder.append(';');
			}
			outputParamBuilder.append(mapping.getSourceColumnName());
			outputParamBuilder.append(',');
			outputParamBuilder.append(mapping.getDestinationColumn().getName());
		}

		outputParamBuilder.append(";IntegrationImportRunID,IntegrationImportRunID");

		StringBuilder inputParamBuilder = new StringBuilder();
		for (String columnName : columnNames) {
			if (inputParamBuilder.length() > 0) {
				inputParamBuilder.append(';');
			}
			inputParamBuilder.append(columnName);
			inputParamBuilder.append(',');
			inputParamBuilder.append(columnDataTypes.containsKey(columnName) ? columnDataTypes.get(columnName).getName() : "String");
		}

		transformationParameters.put("TEMPLATE_INPUT_PARAMS", inputParamBuilder.toString());
		transformationParameters.put("TEMPLATE_OUTPUT_PARAMS", outputParamBuilder.toString());

		transformationParameters.put("OUTPUT_SCHEMA", "dbo");

		String outputTable = importDefinition.getType().getDestinationTableName();
		if (importDefinition.getType().isTempTableUsed()) {
			outputTable = outputTable + "_TEMP";
		}
		transformationParameters.put("OUTPUT_TABLE", outputTable);
	}


	public IntegrationFileConfiguration getIntegrationFileConfig(String typeName) {
		if (!getIntegrationFileConfigurationMap().containsKey(typeName)) {
			throw new RuntimeException("No FileConfiguration defined for IntegrationImportDefinitionType '" + typeName + "'.");
		}
		return getIntegrationFileConfigurationMap().get(typeName);
	}


	private void validateCommand(IntegrationFileProcessorCommand command) {
		if (command.getInputFile() == null) {
			throw new IllegalArgumentException("Input file is required.");
		}

		// validate that in import is not running
		if (!UNKNOWN_FILE_JOB_NAME.equals(command.getImportDefinition().getName()) && isImportRunning(command)) {
			throw new RuntimeException("An import is currently running for '" + command.getImportDefinition().getName() + "'.  Cannot start another import until the process is completed.");
		}
	}


	private void populateCommand(IntegrationFileProcessorCommand command) {
		command.setInputFile(new File(getIntegrationFileService().getIntegrationFileAbsolutePath(command.getIntegrationFile())));
		populateCommandEffectiveDateTransformation(command);
		populateCommandTransformationFileList(command);
		populateCommandTransformationFileToFileList(command);
		populateCommandEffectiveDate(command);
		populateCommandSystemBeanTransformationList(command);
		populateCommandImportDefinition(command);
		populateCommandTargetApplication(command);
	}


	private void populateCommandImportDefinition(IntegrationFileProcessorCommand command) {
		// if no definition is found use the unknown job
		if (command.getImportDefinition() == null) {
			LogUtils.error(getClass(), "No download definition exists for file '" + command.getIntegrationFile().getFileDefinition().getFileName() + "'.");
			command.setImportDefinition(getIntegrationImportService().getIntegrationImportDefinitionByName(UNKNOWN_FILE_JOB_NAME));
		}
	}


	private void populateCommandEffectiveDateTransformation(IntegrationFileProcessorCommand command) {
		if (command.getEffectiveDateTransformationFile() == null) {
			command.setEffectiveDateTransformationFile(command.getImportDefinition().getEffectiveDateTransformationFile());
		}
	}


	private void populateCommandTransformationFileList(IntegrationFileProcessorCommand command) {
		if (CollectionUtils.isEmpty(command.getTransformationFileList())) {
			command.setTransformationFileList(populateCommandTransformationFileList(command.getImportDefinition().getTransformation(), IntegrationTransformationFileTypes.DATA_TRANSFORMATION, null));
		}
	}


	private void populateCommandTransformationFileToFileList(IntegrationFileProcessorCommand command) {
		if (CollectionUtils.isEmpty(command.getFileToFileTransformationFileList())) {
			command.setFileToFileTransformationFileList(populateCommandTransformationFileList(command.getImportDefinition().getFileTransformation(), IntegrationTransformationFileTypes.FILE_TO_FILE_TRANSFORMATION, null));
		}
	}


	private void populateCommandSystemBeanTransformationList(IntegrationFileProcessorCommand command) {
		IntegrationImportTransformationSearchForm searchForm = new IntegrationImportTransformationSearchForm();
		searchForm.setDefinitionId(command.getImportDefinition().getId());
		searchForm.setOrderBy("order");
		List<IntegrationImportTransformation> importTransformations = getIntegrationImportTransformationService().getIntegrationImportTransformationList(searchForm);
		command.setSystemBeanTransformationList(importTransformations);
	}


	private List<IntegrationTransformationFile> populateCommandTransformationFileList(IntegrationTransformation transformation, IntegrationTransformationFileTypes fileType, Boolean runnable) {
		if (transformation != null) {
			IntegrationTransformationFileSearchForm searchForm = new IntegrationTransformationFileSearchForm();
			searchForm.setTransformationId(transformation.getId());
			if (runnable != null) {
				searchForm.setRunnable(runnable);
			}
			searchForm.setOrderBy("order:asc");
			searchForm.setFileType(fileType);
			return getIntegrationTransformationService().getIntegrationTransformationFileList(searchForm);
		}
		return null;
	}


	private void populateCommandEffectiveDate(IntegrationFileProcessorCommand command) {

		Message<File> message = command.getMessage();
		// get the correct effective date
		if (message.getHeaders().containsKey(IntegrationFileProcessorCommand.COMMAND_EFFECTIVE_DATE) && (message.getHeaders().get(IntegrationFileProcessorCommand.COMMAND_EFFECTIVE_DATE) != null)) {
			command.setEffectiveDate((Date) message.getHeaders().get(IntegrationFileProcessorCommand.COMMAND_EFFECTIVE_DATE));
		}
		else if (command.isDefaultEffectiveDateNeeded()) {
			command.setEffectiveDate(getEffectiveDate());
		}
	}


	private void populateCommandTargetApplication(IntegrationFileProcessorCommand command) {
		Message<File> message = command.getMessage();
		// get the correct effective date
		if (message.getHeaders().containsKey(IntegrationFileProcessorCommand.COMMAND_TARGET_APPLICATION) && (message.getHeaders().get(IntegrationFileProcessorCommand.COMMAND_TARGET_APPLICATION) != null)) {
			command.setTargetApplication((IntegrationTargetApplication) message.getHeaders().get(IntegrationFileProcessorCommand.COMMAND_TARGET_APPLICATION));
		}
	}


	private boolean isImportRunning(IntegrationFileProcessorCommand command) {
		// check if another import is running
		if (command.getImportDefinition() != null) {
			IntegrationImportRunSearchForm searchForm = new IntegrationImportRunSearchForm();
			searchForm.setIntegrationImportDefinitionId(command.getImportDefinition().getId());
			searchForm.setStatusName(IntegrationImportStatusTypes.RUNNING.name());
			searchForm.setEffectiveDate(command.getEffectiveDate());

			List<IntegrationImportRun> runList = getIntegrationImportService().getIntegrationImportRunList(searchForm);

			if (!runList.isEmpty()) {
				return true;
			}
		}
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////


	public FileTransformer getTransformer() {
		return this.transformer;
	}


	public void setTransformer(FileTransformer fileTransformer) {
		this.transformer = fileTransformer;
	}


	public IntegrationImportService getIntegrationImportService() {
		return this.integrationImportService;
	}


	public void setIntegrationImportService(IntegrationImportService integrationImportService) {
		this.integrationImportService = integrationImportService;
	}


	public FileNameGenerator getPostProcessingFileNameGenerator() {
		return this.postProcessingFileNameGenerator;
	}


	public void setPostProcessingFileNameGenerator(FileNameGenerator postProcessingFileNameGenerator) {
		this.postProcessingFileNameGenerator = postProcessingFileNameGenerator;
	}


	public CalendarSetupService getCalendarSetupService() {
		return this.calendarSetupService;
	}


	public void setCalendarSetupService(CalendarSetupService calendarSetupService) {
		this.calendarSetupService = calendarSetupService;
	}


	public CalendarBusinessDayService getCalendarBusinessDayService() {
		return this.calendarBusinessDayService;
	}


	public void setCalendarBusinessDayService(CalendarBusinessDayService calendarBusinessDayService) {
		this.calendarBusinessDayService = calendarBusinessDayService;
	}


	public IntegrationFileHandler getIntegrationFileHandler() {
		return this.integrationFileHandler;
	}


	public void setIntegrationFileHandler(IntegrationFileHandler integrationFileHandler) {
		this.integrationFileHandler = integrationFileHandler;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public IntegrationFileArchiveService getIntegrationFileArchiveService() {
		return this.integrationFileArchiveService;
	}


	public void setIntegrationFileArchiveService(IntegrationFileArchiveService integrationFileArchiveService) {
		this.integrationFileArchiveService = integrationFileArchiveService;
	}


	public Map<String, IntegrationFileConfiguration> getIntegrationFileConfigurationMap() {
		return this.integrationFileConfigurationMap;
	}


	public void setIntegrationFileConfigurationMap(Map<String, IntegrationFileConfiguration> integrationFileConfigurationMap) {
		this.integrationFileConfigurationMap = integrationFileConfigurationMap;
	}


	public IntegrationTargetApplicationService getIntegrationTargetApplicationService() {
		return this.integrationTargetApplicationService;
	}


	public void setIntegrationTargetApplicationService(IntegrationTargetApplicationService integrationTargetApplicationService) {
		this.integrationTargetApplicationService = integrationTargetApplicationService;
	}


	public IntegrationTransformationService getIntegrationTransformationService() {
		return this.integrationTransformationService;
	}


	public void setIntegrationTransformationService(IntegrationTransformationService integrationTransformationService) {
		this.integrationTransformationService = integrationTransformationService;
	}


	public IntegrationTransformationParameterService getIntegrationTransformationParameterService() {
		return this.integrationTransformationParameterService;
	}


	public IntegrationImportTransformationService getIntegrationImportTransformationService() {
		return this.integrationImportTransformationService;
	}


	public void setIntegrationImportTransformationService(IntegrationImportTransformationService integrationImportTransformationService) {
		this.integrationImportTransformationService = integrationImportTransformationService;
	}


	public void setIntegrationTransformationParameterService(IntegrationTransformationParameterService integrationTransformationParameterService) {
		this.integrationTransformationParameterService = integrationTransformationParameterService;
	}


	public IntegrationTransformationUtilHandler getIntegrationTransformationUtilHandler() {
		return this.integrationTransformationUtilHandler;
	}


	public void setIntegrationTransformationUtilHandler(IntegrationTransformationUtilHandler integrationTransformationUtilHandler) {
		this.integrationTransformationUtilHandler = integrationTransformationUtilHandler;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public IntegrationTransformationParameterHandler getIntegrationTransformationParameterHandler() {
		return this.integrationTransformationParameterHandler;
	}


	public void setIntegrationTransformationParameterHandler(IntegrationTransformationParameterHandler integrationTransformationParameterHandler) {
		this.integrationTransformationParameterHandler = integrationTransformationParameterHandler;
	}
}
