package com.clifton.integration.validation;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.beans.LabeledObject;
import com.clifton.core.dataaccess.dao.DaoException;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.validation.BaseDataAccessExceptionConverter;
import com.clifton.security.user.SecurityUserService;

import java.sql.SQLException;


/**
 * The <code>DataAccessExceptionConverter</code> class converts database exceptions to corresponding {$link ValidationException}
 * instances with user friendly error messages.
 * <p>
 * This class implements MS SQL Server 2005 specific exception conversion.
 *
 * @author vgomelsky
 */
public class DataAccessExceptionConverter extends BaseDataAccessExceptionConverter {

	private SecurityUserService securityUserService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public LabeledObject getSecurityUser(short id) {
		return getSecurityUserService().getSecurityUser(id);
	}


	@Override
	protected RuntimeException convertFromDuplicateKeyException(RuntimeException exception, SQLException sqlException) {
		String message = sqlException.getMessage();
		// MESSAGE EXAMPLE: Cannot insert duplicate key row in object 'dbo.SystemAuditType' with unique index 'ux_SystemAuditType_AuditTypeName'.
		int end = message.lastIndexOf('\'');
		int start = message.lastIndexOf('\'', end - 1);
		String indexName = message.substring(start, end);
		String[] indexElements = indexName.split("_");
		if (indexElements.length < 2) {
			return new RuntimeException("Duplicate Key error but cannot determine details because constraint does not follow our naming conventions. Message: '" + message + "'", exception);
		}
		String firstColumn = null;
		StringBuilder msg = new StringBuilder("The field ");
		for (int i = 2; i < indexElements.length; i++) {
			String columnName = indexElements[i];
			msg.append('\'');
			msg.append(columnName);
			msg.append('\'');
			if (i < (indexElements.length - 1)) {
				msg.append(", ");
			}
		}
		msg.append(" does not allow duplicate values.");
		if (exception instanceof DaoException) {
			IdentityObject dto = ((DaoException) exception).getCauseEntity();
			msg.append(" Entity: ");
			msg.append(BeanUtils.getLabel(dto));
		}
		return new FieldValidationException(msg.toString(), firstColumn);
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}
}
