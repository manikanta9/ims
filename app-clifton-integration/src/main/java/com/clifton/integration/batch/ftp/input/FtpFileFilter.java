package com.clifton.integration.batch.ftp.input;


public class FtpFileFilter {

	private String ftpServerFolder;
	private String filterRegex;
	private String dateFormat;
	private String targetPath;
	private boolean writeExtension;


	public String getFtpServerFolder() {
		return this.ftpServerFolder;
	}


	public void setFtpServerFolder(String ftpServerFolder) {
		this.ftpServerFolder = ftpServerFolder;
	}


	public String getFilterRegex() {
		return this.filterRegex;
	}


	public void setFilterRegex(String filterRegex) {
		this.filterRegex = filterRegex;
	}


	public String getDateFormat() {
		return this.dateFormat;
	}


	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}


	public String getTargetPath() {
		return this.targetPath;
	}


	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}


	public boolean isWriteExtension() {
		return this.writeExtension;
	}


	public void setWriteExtension(boolean writeExtension) {
		this.writeExtension = writeExtension;
	}
}
