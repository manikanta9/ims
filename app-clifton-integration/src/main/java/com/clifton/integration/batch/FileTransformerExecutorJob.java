package com.clifton.integration.batch;

import com.clifton.batch.runner.BatchRunner;
import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusHolderObject;
import com.clifton.core.util.status.StatusHolderObjectAware;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;
import com.clifton.integration.incoming.transformation.IntegrationTransformationService;
import com.clifton.integration.incoming.transformation.parameter.IntegrationTransformationParameterHandler;
import com.clifton.integration.incoming.transformer.FileTransformer;
import com.clifton.integration.incoming.transformer.FileTransformerCommand;

import java.io.File;
import java.security.SecureRandom;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
public class FileTransformerExecutorJob implements Task, ValidationAware, StatusHolderObjectAware<Status> {

	public static final SecureRandom random = new SecureRandom();

	private Integer transformationId;
	private StatusHolderObject<Status> statusHolder;
	private boolean captureRunBuffer;

	private FileTransformer fileTransformer;
	private IntegrationTransformationService integrationTransformationService;
	private IntegrationTransformationParameterHandler integrationTransformationParameterHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static FileTransformerExecutorJob create(Integer transformationId, StatusHolderObject<Status> statusHolder, ApplicationContextService applicationContextService, boolean captureRunBuffer) {
		FileTransformerExecutorJob fileTransformerExecutorJob = new FileTransformerExecutorJob();
		applicationContextService.autowireBean(fileTransformerExecutorJob);
		fileTransformerExecutorJob.setTransformationId(transformationId);
		fileTransformerExecutorJob.setStatusHolderObject(statusHolder);
		fileTransformerExecutorJob.setCaptureRunBuffer(captureRunBuffer);
		return fileTransformerExecutorJob;
	}

	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		Status status = this.statusHolder.getStatus();
		status.setMessage(null);
		FileTransformerCommand command = null;
		try {
			validate();

			File file = File.createTempFile("Batch", null);
			file.deleteOnExit();

			IntegrationTransformation transformation = getIntegrationTransformationService().getIntegrationTransformation(getTransformationId());

			Map<String, String> transformationParameters = getIntegrationTransformationParameterHandler().getIntegrationTransformationParameterMap(transformation);

			command = new FileTransformerCommand(getJobId(context), file, getIntegrationTransformationFileList(), transformationParameters, isCaptureRunBuffer());
			getFileTransformer().doTransform(command);
		}
		catch (ValidationException v) {
			status.setMessage("FAILED setup validation.");
			status.addError(getErrorMessage(v));
		}
		catch (Exception e) {
			status.setMessage("FAILED transformation.");
			status.addError(getErrorMessage(e));
		}
		finally {
			if (command != null && command.isCaptureRunBuffer() && !CollectionUtils.isEmpty(command.getStatus().getDetailList())) {
				command.getStatus().getDetailList().forEach(d -> status.addDetail(d.getCategory(), d.getNote()));
			}
		}

		return status;
	}


	private String getErrorMessage(Exception e) {
		String errorMessage = ExceptionUtils.getOriginalMessage(e) + "\n\n" + ExceptionUtils.toString(e);
		//Truncate the error message if necessary
		if (errorMessage.length() > DataTypes.DESCRIPTION_LONG.getLength()) {
			errorMessage = errorMessage.substring(0, DataTypes.DESCRIPTION_LONG.getLength() - 4).concat("...");
		}

		return errorMessage;
	}


	@Override
	public void validate() throws ValidationException {
		ValidationUtils.assertNotNull(getTransformationId(), "The transformation cannot be empty.");

		IntegrationTransformation integrationTransformation = getIntegrationTransformationService().getIntegrationTransformation(getTransformationId());
		ValidationUtils.assertNotNull(integrationTransformation, String.format("The Integration Transformation [%s] could not be found.", getTransformationId()));

		List<IntegrationTransformationFile> integrationTransformationFileList = getIntegrationTransformationFileList();
		ValidationUtils.assertNotEmpty(integrationTransformationFileList, String.format("No File Transformations found for Integration Transformation [%s]", getTransformationId()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private int getJobId(Map<String, Object> context) {
		if (context.containsKey(BatchRunner.CURRENT_BATCH_JOB_ID)) {
			Object tmp = context.get(BatchRunner.CURRENT_BATCH_JOB_ID);
			if (tmp != null && MathUtils.isNumber(tmp.toString())) {
				return Integer.parseInt(tmp.toString());
			}
		}
		return random.nextInt();
	}


	private List<IntegrationTransformationFile> getIntegrationTransformationFileList() {
		return CollectionUtils.getStream(getIntegrationTransformationService().getIntegrationTransformationFileListForTransformation(getTransformationId()))
				.sorted(Comparator.comparing(IntegrationTransformationFile::getOrder))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                  ///////////
	////////////////////////////////////////////////////////////////////////////


	public StatusHolderObject<Status> getStatusHolder() {
		return this.statusHolder;
	}


	@Override
	public void setStatusHolderObject(StatusHolderObject<Status> statusHolderObject) {
		this.statusHolder = statusHolderObject;
	}


	public Integer getTransformationId() {
		return this.transformationId;
	}


	public void setTransformationId(Integer transformationId) {
		this.transformationId = transformationId;
	}


	public boolean isCaptureRunBuffer() {
		return this.captureRunBuffer;
	}


	public void setCaptureRunBuffer(boolean captureRunBuffer) {
		this.captureRunBuffer = captureRunBuffer;
	}


	public FileTransformer getFileTransformer() {
		return this.fileTransformer;
	}


	public void setFileTransformer(FileTransformer fileTransformer) {
		this.fileTransformer = fileTransformer;
	}


	public IntegrationTransformationService getIntegrationTransformationService() {
		return this.integrationTransformationService;
	}


	public void setIntegrationTransformationService(IntegrationTransformationService integrationTransformationService) {
		this.integrationTransformationService = integrationTransformationService;
	}


	public IntegrationTransformationParameterHandler getIntegrationTransformationParameterHandler() {
		return this.integrationTransformationParameterHandler;
	}


	public void setIntegrationTransformationParameterHandler(IntegrationTransformationParameterHandler integrationTransformationParameterHandler) {
		this.integrationTransformationParameterHandler = integrationTransformationParameterHandler;
	}
}
