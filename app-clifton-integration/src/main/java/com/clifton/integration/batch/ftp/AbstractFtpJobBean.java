package com.clifton.integration.batch.ftp;


import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ftp.FtpProtocols;
import com.clifton.core.util.runner.Task;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


public abstract class AbstractFtpJobBean implements Task {

	/**
	 * The url of the external FTP server.
	 */
	private String url;
	/**
	 * The port for the external FTP server.
	 */
	private Integer port;
	/**
	 * The username for the external FTP server.
	 */
	private String username;

	/**
	 * The Secret the password is stored in.
	 */
	private SecuritySecret passwordSecret;

	/**
	 * The FTP protocol for the external server.  FTP, FTPS or SFTP.
	 */
	private FtpProtocols ftpProtocol;
	/**
	 * The SSH private key for an sftp connection.
	 */
	private SecuritySecret sftpPrivateKeySecret;

	/**
	 * The SSH public key for an sftp connection.
	 */
	private String sftpPublicKey;
	/**
	 * The bean used to create the list of files to be exported.
	 */
	private SystemBean systemBean;

	/**
	 * Number of days to go back to run the FTP export.
	 */
	private Integer daysFromToday = 1;
	/**
	 * The date generation strategy.
	 */
	private DateGenerationOptions dateGenerationOption;
	/**
	 * The folder path on the remote FTP server.
	 */
	private String ftpServerFolder;
	/**
	 * The express for filtering remote folder files.
	 */
	private String filterRegex;
	/**
	 * The simple date format for converting a date into a string to match the date portion in a file name.
	 */
	private String dateFormat;
	/**
	 * Destination file path on the local file system.
	 */
	private String targetPath;
	/**
	 * If checked, the file will have a .write file extension while its being copied to the network path.
	 * The file extension will be removed after its successfully copied.
	 */
	private Boolean writeExtension;
	/**
	 * The JSON string representation of the map of destination file name mappings.
	 */
	private String destinationFileNameMap;
	/**
	 * The map built from the JSON representation in destinationFileNameMap
	 */
	private Map<String, String> translatedDestinationFileNameMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	private SecuritySecretService securitySecretService;

	private SystemBeanService systemBeanService;

	private TemplateConverter templateConverter;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected SecuritySecret decryptSecuritySecret(SecuritySecret secret) {
		return getSecuritySecretService().decryptSecuritySecret(getSecuritySecretService().getSecuritySecretPopulated(secret));
	}


	@SuppressWarnings("unchecked")
	protected Map<String, String> buildTranslatedDestinationFileNameMap() {
		if (!StringUtils.isEmpty(getDestinationFileNameMap())) {
			Map<String, Object> objectMap = (new JsonStringToMapConverter()).convert(getDestinationFileNameMap());
			if (objectMap.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME)) {
				List<Object> arr = (List<Object>) objectMap.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
				if (!CollectionUtils.isEmpty(arr)) {
					Map<String, String> strMap = (Map<String, String>) arr.get(0);
					return strMap.entrySet().stream()
							.collect(Collectors.toMap(e -> e.getKey().toUpperCase(), Map.Entry::getValue));
				}
			}
		}
		return Collections.emptyMap();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public String getUrl() {
		return this.url;
	}


	public void setUrl(String url) {
		this.url = url;
	}


	public String getUsername() {
		return this.username;
	}


	public void setUsername(String username) {
		this.username = username;
	}


	public SecuritySecret getPasswordSecret() {
		return this.passwordSecret;
	}


	public void setPasswordSecret(SecuritySecret passwordSecret) {
		this.passwordSecret = passwordSecret;
	}


	public FtpProtocols getFtpProtocol() {
		return this.ftpProtocol;
	}


	public void setFtpProtocol(FtpProtocols ftpProtocol) {
		this.ftpProtocol = ftpProtocol;
	}


	public SystemBean getSystemBean() {
		return this.systemBean;
	}


	public void setSystemBean(SystemBean systemBean) {
		this.systemBean = systemBean;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}


	public Integer getPort() {
		return this.port;
	}


	public void setPort(Integer port) {
		this.port = port;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public String getFtpServerFolder() {
		return this.ftpServerFolder;
	}


	public void setFtpServerFolder(String ftpServerFolder) {
		this.ftpServerFolder = ftpServerFolder;
	}


	public String getFilterRegex() {
		return this.filterRegex;
	}


	public void setFilterRegex(String filterRegex) {
		this.filterRegex = filterRegex;
	}


	public String getDateFormat() {
		return this.dateFormat;
	}


	public void setDateFormat(String dateFormat) {
		this.dateFormat = dateFormat;
	}


	public String getTargetPath() {
		return this.targetPath;
	}


	public void setTargetPath(String targetPath) {
		this.targetPath = targetPath;
	}


	public Boolean getWriteExtension() {
		return this.writeExtension;
	}


	public void setWriteExtension(Boolean writeExtension) {
		this.writeExtension = writeExtension;
	}


	public String getDestinationFileNameMap() {
		return this.destinationFileNameMap;
	}


	public void setDestinationFileNameMap(String destinationFileNameMap) {
		this.destinationFileNameMap = destinationFileNameMap;
	}


	public Map<String, String> getTranslatedDestinationFileNameMap() {
		if (this.translatedDestinationFileNameMap == null) {
			this.translatedDestinationFileNameMap = buildTranslatedDestinationFileNameMap();
		}
		return this.translatedDestinationFileNameMap;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public String getSftpPublicKey() {
		return this.sftpPublicKey;
	}


	public void setSftpPublicKey(String sftpPublicKey) {
		this.sftpPublicKey = sftpPublicKey;
	}


	public SecuritySecret getSftpPrivateKeySecret() {
		return this.sftpPrivateKeySecret;
	}


	public void setSftpPrivateKeySecret(SecuritySecret sftpPrivateKeySecret) {
		this.sftpPrivateKeySecret = sftpPrivateKeySecret;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}
}
