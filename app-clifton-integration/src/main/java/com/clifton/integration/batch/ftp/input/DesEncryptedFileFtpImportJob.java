package com.clifton.integration.batch.ftp.input;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.util.BooleanUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ZipUtils;
import com.clifton.core.util.encryption.EncryptionUtilsDES;
import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.core.util.ftp.FtpResult;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.security.secret.SecuritySecret;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 * FTP Import Job to import files that were encrypted using the DES.exe program (libdes)
 *
 * @author theodorez
 */
public class DesEncryptedFileFtpImportJob extends FtpImportJob {

	/**
	 * Determines whether to attempt to decrypt all of the files selected using the file filter
	 */
	private Boolean decryptFiles;
	/**
	 * The secret that will be used to decrypt the files using the DES algorithm
	 */
	private SecuritySecret decryptionSecret;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		FtpFileFilter filter = getFtpFileFilter();
		Date reportingDate = getReportingDate();
		int fileCount = 0;
		int errorCount = 0;
		FtpConfig ftpConfig = new FtpConfig(getUrl(), getPort(), getFtpProtocol(), getUsername(), getPasswordSecret() != null ? decryptSecuritySecret(getPasswordSecret()).getSecretString() : null);
		Status status = Status.ofMessage("Starting Bloomberg FTP Import from: " + getUrl());
		ftpConfig.setSshPrivateKey(getSftpPrivateKeySecret() != null ? decryptSecuritySecret(getSftpPrivateKeySecret()).getSecretString() : null);
		try {
			if (BooleanUtils.isTrue(getDecryptFiles())) {
				String finalPath = filter.getTargetPath();
				//Set the target path to the temp directory
				filter.setTargetPath(FileUtils.JAVA_TEMP_DIRECTORY);
				List<FtpResult> ftpResults = processFtpFileFilter(filter, ftpConfig, reportingDate);
				String decryptionKey = decryptSecuritySecret(getDecryptionSecret()).getSecretString();
				for (FtpResult ftpResult : ftpResults) {
					if (ftpResult.isSuccess()) {
						decryptAndUnGzipFile(ftpResult.getFileName(), finalPath, decryptionKey);
						fileCount += 1;
					}
					else {
						errorCount++;
						status.addError(ExceptionUtils.getDetailedMessage(ftpResult.getError()));
					}
					if (ftpResult.hasWarning()) {
						status.addMessage(ftpResult.getWarning());
					}
				}
			}
			else {
				fileCount += processFtpFileFilter(filter, ftpConfig, reportingDate).size();
			}
		}
		catch (Throwable e) {
			errorCount++;
			status.addError(ExceptionUtils.getDetailedMessage(e));
		}

		status.setMessage("File Download Count: " + fileCount + ". Error Count: " + errorCount);
		status.setActionPerformed(fileCount != 0);

		return status;
	}


	/**
	 * Decrypts and unzips the file
	 *
	 * @param fileName      The name of the file to operate on in the temp directory
	 * @param finalPath     The location the unencrypted and unzipped file will be stored in
	 * @param decryptionKey The key used to decrypt the file
	 */
	private void decryptAndUnGzipFile(String fileName, String finalPath, String decryptionKey) throws IOException {
		String encryptedFilePath = FileUtils.combinePath(FileUtils.JAVA_TEMP_DIRECTORY, fileName);
		File encryptedFile = new File(encryptedFilePath);
		File decryptedFile = new File(StringUtils.removeAll(encryptedFilePath, "(\\.enc)"));
		try (
				FileOutputStream fileOutputStream = new FileOutputStream(decryptedFile);
				BufferedOutputStream bos = new BufferedOutputStream(fileOutputStream)
		) {
			ValidationUtils.assertTrue(FileUtils.fileExists(encryptedFilePath), "File to decrypt does not exist at: " + encryptedFilePath);
			bos.write(EncryptionUtilsDES.decrypt(decryptionKey, FileUtils.readFileBytes(encryptedFile)));
		}
		//Unzips the file into the input directory
		ZipUtils.unGzipFile(decryptedFile, finalPath, StringUtils.removeAll(decryptedFile.getName(), "(\\.gz)"));
		if (encryptedFile.exists()) {
			FileUtils.delete(encryptedFile);
		}
		if (decryptedFile.exists()) {
			FileUtils.delete(decryptedFile);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Boolean getDecryptFiles() {
		return this.decryptFiles;
	}


	public void setDecryptFiles(Boolean decryptFiles) {
		this.decryptFiles = decryptFiles;
	}


	public SecuritySecret getDecryptionSecret() {
		return this.decryptionSecret;
	}


	public void setDecryptionSecret(SecuritySecret decryptionSecret) {
		this.decryptionSecret = decryptionSecret;
	}
}
