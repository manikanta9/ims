package com.clifton.integration.transformation;

import com.clifton.core.util.status.Status;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;


/**
 * @author TerryS
 */
public class IntegrationTransformationResults {

	private final IntegrationTransformation integrationTransformation;
	private final Status results;


	public IntegrationTransformationResults(IntegrationTransformation integrationTransformation, Status results) {
		this.integrationTransformation = integrationTransformation;
		this.results = results;
	}


	public IntegrationTransformation getIntegrationTransformation() {
		return this.integrationTransformation;
	}


	public Status getResults() {
		return this.results;
	}
}
