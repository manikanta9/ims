package com.clifton.integration.transformation;


import com.clifton.core.security.authorization.SecureMethod;
import org.springframework.web.bind.annotation.RequestMapping;


/**
 * @author TerryS
 */
public interface IntegrationTransformationExecutionService {

	@RequestMapping("integrationTransformationExecute")
	@SecureMethod(securityResource = "Transformation")
	public IntegrationTransformationResults executeIntegrationTransformation(int transformationId, boolean showRunDetails);
}
