package com.clifton.integration.transformation;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.integration.batch.FileTransformerExecutorJob;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationService;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
@Service
public class IntegrationTransformationExecutionServiceImpl implements IntegrationTransformationExecutionService {

	private IntegrationTransformationService integrationTransformationService;
	private ApplicationContextService applicationContextService;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public IntegrationTransformationResults executeIntegrationTransformation(int transformationId, boolean showRunDetails) {
		ValidationUtils.assertNotNull(transformationId, "The transformation is required");

		IntegrationTransformation integrationTransformation = getIntegrationTransformationService().getIntegrationTransformation(transformationId);
		ValidationUtils.assertNotNull(integrationTransformation, "The transformation is required");

		FileTransformerExecutorJob fileTransformerExecutorJob = FileTransformerExecutorJob.create(
				transformationId,
				new StatusHolder("Executing Transformation " + integrationTransformation.getLabel()),
				getApplicationContextService(),
				showRunDetails
		);
		Status results = fileTransformerExecutorJob.run(new HashMap<>());
		results.setMessage(getStatusMessage(results, showRunDetails));
		return new IntegrationTransformationResults(integrationTransformation, results);
	}


	private String getStatusMessage(Status results, boolean showRunDetails) {
		StringBuilder stringBuilder = new StringBuilder();
		if (results.getErrorCount() > 0) {
			stringBuilder.append(results.getMessageWithErrors());
		}
		else if (!StringUtils.isEmpty(results.getMessage())) {
			stringBuilder.append(results.getMessage());
		}
		if (showRunDetails) {
			stringBuilder.append(System.lineSeparator());
			List<String> notes = CollectionUtils.getStream(results.getDetailList())
					.filter(d -> !Objects.equals(d.getCategory(), StatusDetail.CATEGORY_ERROR))
					.map(StatusDetail::getNote)
					.filter(s -> !StringUtils.isEmpty(s))
					.collect(Collectors.toList());
			stringBuilder.append(notes.stream().collect(Collectors.joining(System.lineSeparator())));
		}
		return stringBuilder.toString();
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////////           Getter and Setter Methods            /////////////////
	////////////////////////////////////////////////////////////////////////////////


	public IntegrationTransformationService getIntegrationTransformationService() {
		return this.integrationTransformationService;
	}


	public void setIntegrationTransformationService(IntegrationTransformationService integrationTransformationService) {
		this.integrationTransformationService = integrationTransformationService;
	}


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}


	public void setApplicationContextService(ApplicationContextService applicationContextService) {
		this.applicationContextService = applicationContextService;
	}
}
