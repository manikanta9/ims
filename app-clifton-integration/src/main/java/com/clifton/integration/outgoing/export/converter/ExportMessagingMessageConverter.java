package com.clifton.integration.outgoing.export.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationType;
import com.clifton.integration.source.IntegrationSource;


/**
 * @author mwacker
 */
public interface ExportMessagingMessageConverter {

	/**
	 * Convert the incoming export message to local IntegrationExport.
	 *
	 * @param exportMessagingMessage
	 * @param exportMessagingDestination
	 * @param destinationType
	 * @param source
	 */
	public IntegrationExport convertToExport(ExportMessagingMessage exportMessagingMessage, ExportMessagingDestination exportMessagingDestination, IntegrationExportDestinationType destinationType, IntegrationSource source);


	/**
	 * Update IntegrationExportDestination destination properties.
	 *
	 * @param integrationExportDestination
	 * @param exportMessagingDestination
	 * @param destinationType
	 */
	public IntegrationExportDestination createOrUpdateIntegrationExportDestination(IntegrationExportDestination integrationExportDestination, ExportMessagingDestination exportMessagingDestination, IntegrationExportDestinationType destinationType, String subDestinationValue, ExportMapKeys key);


	/**
	 * Returns the destination value that will be used for IntegrationExportDestination destinationValue field.
	 */
	default public String getDestinationValue(ExportMessagingDestination exportMessagingDestination, String subDestinationValue, ExportMapKeys key) {
		return subDestinationValue;
	}


	default public String getDestinationText(ExportMessagingDestination exportMessagingDestination) {
		return null;
	}

	default public IntegrationExportDestination copyProperties(IntegrationExportDestination existingIntegrationExportDestination, IntegrationExportDestination integrationExportDestination){
		BeanUtils.copyProperties(existingIntegrationExportDestination, integrationExportDestination);
		return integrationExportDestination;
	}
}
