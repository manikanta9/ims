package com.clifton.integration.outgoing.export;

import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.logging.LogCommand;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.retry.RetryContext;
import com.clifton.core.util.retry.RetryFuture;
import com.clifton.core.util.retry.RetryJob;
import com.clifton.core.util.retry.RetryUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportConstants;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.ExportStatuses;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.outgoing.export.converter.ExportMessagingMessageConverter;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationService;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationSubType;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationType;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;
import com.clifton.integration.outgoing.export.server.IntegrationExportRunner;
import com.clifton.integration.outgoing.export.server.IntegrationExportRunnerResult;
import com.clifton.integration.outgoing.export.server.IntegrationExportStatusHandler;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.security.encryption.transport.server.SecurityRSATransportEncryptionServerHandler;
import com.clifton.security.secret.SecuritySecret;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PreDestroy;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Executors;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.atomic.AtomicInteger;


/**
 * @author theodorez
 */
@Component
public class IntegrationExportProcessingHandlerImpl implements IntegrationExportProcessingHandler {

	//Creates a new pool so we don't use up the common thread pool
	private static final ScheduledExecutorService SCHEDULER = Executors.newScheduledThreadPool(Runtime.getRuntime().availableProcessors() * 4);
	private static final BlockingQueue<RetryFuture<IntegrationExportRunnerResult>> QUEUE = new LinkedBlockingQueue<>();
	private static final int MAX_RETRIES = 5;
	private static final int RETRY_DELAY_SECONDS = 5 * 60;
	//Holder for the future monitoring thread
	private final ScheduledFuture<?> retryFutureMonitor = RetryUtils.getFutureMonitoringFuture(SCHEDULER, QUEUE, this::processRunnerResultsAndSendStatus, 5);


	/**
	 * Will have an entry for EMAIL, FTP and PHONE
	 */
	private Map<String, ExportMessagingMessageConverter> exportMessagingMessageConverterMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private ContextHandler contextHandler;

	private IntegrationExportDestinationService integrationExportDestinationService;
	private IntegrationExportRunner integrationExportRunner;
	private IntegrationExportService integrationExportService;
	private IntegrationExportStatusHandler integrationExportStatusHandler;

	private SecurityRSATransportEncryptionServerHandler securityRSATransportEncryptionServerHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void processIntegrationExport(IntegrationExportBuildCommand command) {
		Map<String, Object> contextBeans = getContextHandler().getBeanMap();
		List<CompletableFuture<IntegrationExportRunnerResult>> completableFutureList = new ArrayList<>();

		// get the initial export status
		IntegrationExportStatus status = getIntegrationExportService().getIntegrationExportStatusByName(ExportStatuses.RECEIVED.name());
		for (ExportMessagingDestination destination : CollectionUtils.getIterable(command.getExportMessagingExport().getDestinationList())) {
			decryptDestinationProperties(destination, command.getSource());
			IntegrationExportBuildCommand destinationCommand = new IntegrationExportBuildCommand(command, status, destination);
			IntegrationExport export = createIntegrationExport(destinationCommand);
			//Atomic int needed here to increment the retry count on the run history
			AtomicInteger retryCounter = new AtomicInteger(1);

			RetryJob<IntegrationExportRunnerResult> job = new RetryJob<>(RetryContext.RetryContextBuilder.getInstanceForActionAndSuccessCondition(() -> {
						try {
							//Sets the context on each run
							getContextHandler().setBeanMap(contextBeans);
							//Passes the current retry count to the runner via the export object
							export.setRetryCount(retryCounter.getAndIncrement());
							return getIntegrationExportRunner().runIntegrationExport(export);
						}
						catch (Exception e) {
							LogUtils.warn(getClass(), "Failed to run scheduled export.", e);
							IntegrationExportHistory runHistory = getIntegrationExportErrorHistory(export, e);
							return new IntegrationExportRunnerResult(export, runHistory.getExportStatus(), runHistory);
						}
					},
					IntegrationExportRunnerResult::success
			)
					.withScheduler(SCHEDULER)
					.withMaxRetries(MAX_RETRIES)
					.withRetryDelayInSeconds(RETRY_DELAY_SECONDS)
					.build());
			//Submits the job to be run immediately and adds the future returned to the list
			LogUtils.info(LogCommand.ofMessageSupplier(this.getClass(), () -> String.format("Queue size before submission [%s].", Optional.of(SCHEDULER).filter(s -> SCHEDULER instanceof ScheduledThreadPoolExecutor).map(s -> ((ScheduledThreadPoolExecutor) s).getQueue()).map(Collection::size).orElse(-1))));
			completableFutureList.add(job.submit());
		}

		if (!CollectionUtils.isEmpty(completableFutureList)) {
			LogUtils.info(this.getClass(), String.format("Adding [%s] completable futures to the queue.", completableFutureList.size()));
			QUEUE.add(new RetryFuture<>(completableFutureList));
		}
		else {
			getIntegrationExportStatusHandler().sendIntegrationExportStatus(command.getExportMessagingExport().getSourceSystemIdentifier(), command.getSource().getName(), ExportStatuses.PROCESSED, "No exports were run because no completable futures were generated.");
			LogUtils.warn(LogCommand.ofMessage(getClass(),
					"Completable Future List is Empty for Source: ",
					command.getSource(),
					" Destinations ",
					command.getExportMessagingExport().getDestinationList(),
					" Source System Identifier",
					command.getExportMessagingExport().getSourceSystemIdentifier(),
					" Source System Definition: ",
					command.getExportMessagingExport().getSourceSystemDefinitionName())
			);
		}
	}


	@Override
	public void reprocessIntegrationExport(IntegrationExport export) {
		export.setRetryCount(0);
		IntegrationExportRunnerResult runnerResult = runIntegrationExport(export);
		ValidationUtils.assertNotNull(runnerResult, "IntegrationRunnerResult cannot be null");
		getIntegrationExportStatusHandler().sendIntegrationExportStatus(export.getSourceSystemIdentifier(), export.getIntegrationSource().getName(), ExportStatuses.valueOf(runnerResult.getExportStatus().getName()), runnerResult.getExportRunHistory().getDescription());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Transactional
	protected void saveExportFiles(IntegrationExport export, List<IntegrationFile> fileList) {
		for (IntegrationFile file : CollectionUtils.getIterable(fileList)) {
			getIntegrationExportService().linkIntegrationExportIntegrationFile(export, file, IntegrationExportFileStatus.IntegrationExportFileStatusNames.PROCESSING);
		}
	}


	@PreDestroy
	public void stop() {
		this.retryFutureMonitor.cancel(true);
		ConcurrentUtils.shutdownExecutorService(SCHEDULER);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	private void processRunnerResultsAndSendStatus(List<IntegrationExportRunnerResult> resultList) {
		ValidationUtils.assertFalse(CollectionUtils.isEmpty(resultList), "List of IntegrationExportRunnerResult[s] cannot be empty");
		IntegrationExportRunnerResult result = resultList.get(0);
		ValidationUtils.assertNotNull(result, "IntegrationRunnerResult cannot be null");

		//Process the first result
		IntegrationExport export = result.getExport();
		IntegrationExportStatus finalStatus = result.getExportStatus();
		//All results should have the same source identifier
		String sourceSystemIdentifier = export.getSourceSystemIdentifier();
		IntegrationSource integrationSource = export.getIntegrationSource();
		StringBuilder messageBuilder = new StringBuilder(result.getExportRunHistory().getDescription());

		//Process subsequent results
		for (int i = 1; i < resultList.size(); i++) {
			result = resultList.get(i);
			ValidationUtils.assertNotNull(result, "IntegrationRunnerResult cannot be null");
			messageBuilder.append("\n\n");
			messageBuilder.append(result.getExportRunHistory().getDescription());
			if (!result.success()) {
				finalStatus = result.getExportStatus();
			}
		}
		getIntegrationExportStatusHandler().sendIntegrationExportStatus(sourceSystemIdentifier, integrationSource.getName(), ExportStatuses.valueOf(finalStatus.getName()), messageBuilder.toString());
	}


	private IntegrationExport createIntegrationExport(IntegrationExportBuildCommand command) {
		IntegrationExport export = null;
		try {
			IntegrationExportDestinationType destinationType = this.integrationExportDestinationService.getIntegrationExportDestinationTypeByName(command.getDestination().getType().name());
			ExportMessagingMessageConverter converter = getExportMessagingMessageConverterMap().get(command.getDestination().getType().name());

			// get the IntegrationExport from the message, set the status and save it
			export = converter.convertToExport(command.getExportMessagingExport(), command.getDestination(), destinationType, command.getSource());
			export.setStatus(command.getStatus());
			export = getIntegrationExportService().saveIntegrationExport(export);

			// link the files to the new export
			saveExportFiles(export, command.getFilesToExportList());

			// resolve and save the destinations
			List<IntegrationExportIntegrationExportDestination> linkedDestinationList = resolveAndSaveIntegrationExportDestination(export, command.getDestination());

			// populate the export
			export.setIntegrationFileList(command.getFilesToExportList());
			export.setLinkedDestinationList(linkedDestinationList);
		}
		catch (Exception e) {
			if (!handleIntegrationExportCreationError(command, export, e)) {
				throw e;
			}
		}
		return export;
	}


	/**
	 * Handles Errors that occur when creating Integration Exports.  Return true if the error was handled and false if not.
	 */
	private boolean handleIntegrationExportCreationError(IntegrationExportBuildCommand command, IntegrationExport export, Exception e) {
		boolean result = true;
		// if there is an error creating the export create a history entry with the error.
		if ((export != null) && !export.isNewBean()) {
			IntegrationExportStatus status = getIntegrationExportService().getIntegrationExportStatusByName(ExportStatuses.FAILED.name());
			export.setStatus(status);
			getIntegrationExportService().saveIntegrationExport(export);

			IntegrationExportHistory history = getIntegrationExportErrorHistory(export, e);

			getIntegrationExportStatusHandler().sendIntegrationExportStatus(export.getSourceSystemIdentifier(), export.getIntegrationSource().getName(), status.getIntegrationExportStatusName(), history.getDescription());
		}
		else if (command != null) {
			handleBasicError(e, command.getExportMessagingExport().getSourceSystemIdentifier(), command.getExportMessagingExport().getSourceSystemName());
		}
		else {
			result = false;
		}
		return result;
	}


	private List<IntegrationExportIntegrationExportDestination> resolveAndSaveIntegrationExportDestination(IntegrationExport integrationExport, ExportMessagingDestination destination) {
		List<IntegrationExportIntegrationExportDestination> result = new ArrayList<>();
		//Gather the destinations
		for (Map.Entry<ExportMapKeys, String> propertyEntry : destination.getPropertyList().entrySet()) {
			String subDestinationValue = propertyEntry.getValue();
			if (!StringUtils.isEmpty(subDestinationValue)) {
				List<String> subDestinationValueList = getExportDestinationSubTypeValues(subDestinationValue);
				for (String value : CollectionUtils.getIterable(subDestinationValueList)) {
					IntegrationExportDestinationSubType integrationExportDestinationSubType = getIntegrationExportDestinationSubType(propertyEntry.getKey(), value);
					if (integrationExportDestinationSubType != null) {
						IntegrationExportDestinationType integrationExportDestinationType = integrationExportDestinationSubType.getDestinationType();

						// Check if the destination value exists
						IntegrationExportDestination integrationExportDestination = createOrUpdateIntegrationExportDestination(integrationExportDestinationType, destination, value, propertyEntry.getKey());

						// create a new destination link
						IntegrationExportIntegrationExportDestination integrationExportIntegrationExportDestination = createDestinationToExportLink(integrationExportDestinationSubType, integrationExport, integrationExportDestination);

						result.add(integrationExportIntegrationExportDestination);
					}
				}
			}
		}
		return result;
	}


	private List<String> getExportDestinationSubTypeValues(String value) {
		String[] emailArray = StringUtils.tokenizeToStringArray(value, ExportConstants.STRING_DELIMITER);
		if (emailArray != null) {
			return CollectionUtils.createList(emailArray);
		}
		return CollectionUtils.createList(value);
	}


	private IntegrationExportDestinationSubType getIntegrationExportDestinationSubType(ExportMapKeys key, String value) {
		String subTypeName = key.getSubTypeName(value);
		if (!StringUtils.isEmpty(subTypeName)) {
			return getIntegrationExportDestinationService().getIntegrationExportDestinationSubTypeByName(subTypeName);
		}
		return null;
	}


	private IntegrationExportIntegrationExportDestination createDestinationToExportLink(IntegrationExportDestinationSubType integrationExportDestinationSubType, IntegrationExport integrationExport, IntegrationExportDestination integrationExportDestination) {
		// create a new destination link
		IntegrationExportIntegrationExportDestination integrationExportIntegrationExportDestination = new IntegrationExportIntegrationExportDestination();
		integrationExportIntegrationExportDestination.setDestinationSubType(integrationExportDestinationSubType);
		integrationExportIntegrationExportDestination.setReferenceOne(integrationExport);
		integrationExportIntegrationExportDestination.setReferenceTwo(integrationExportDestination);

		//save the link table
		return getIntegrationExportDestinationService().saveIntegrationExportIntegrationExportDestination(
				integrationExportIntegrationExportDestination);
	}


	private IntegrationExportDestination createOrUpdateIntegrationExportDestination(IntegrationExportDestinationType integrationExportDestinationType, ExportMessagingDestination destination, String value, ExportMapKeys key) {
		ExportMessagingMessageConverter converter = getExportMessagingMessageConverterMap().get(destination.getType().name());

		IntegrationExportDestination integrationExportDestination = converter.createOrUpdateIntegrationExportDestination(null, destination, integrationExportDestinationType, value, key);

		// Check if the destination value exists
		IntegrationExportDestination existingIntegrationExportDestination = getIntegrationExportDestinationService().getIntegrationExportDestinationByValueAndFlags(integrationExportDestination);

		//Merge the existing bean into the new one if it exists
		if (existingIntegrationExportDestination != null) {
			SecuritySecret newPasswordSecret = integrationExportDestination.getDestinationPasswordSecuritySecret();
			SecuritySecret newSshPrivateKeySecret = integrationExportDestination.getDestinationSshPrivateKeySecuritySecret();

			integrationExportDestination = converter.copyProperties(existingIntegrationExportDestination, integrationExportDestination);

			if (newPasswordSecret != null) {
				if (integrationExportDestination.getDestinationPasswordSecuritySecret() == null) {
					integrationExportDestination.setDestinationPasswordSecuritySecret(newPasswordSecret);
				}
				else {
					integrationExportDestination.getDestinationPasswordSecuritySecret().setSecretString(newPasswordSecret.getSecretString());
				}
			}
			if (newSshPrivateKeySecret != null) {
				if (integrationExportDestination.getDestinationSshPrivateKeySecuritySecret() == null) {
					integrationExportDestination.setDestinationSshPrivateKeySecuritySecret(newSshPrivateKeySecret);
				}
				else {
					integrationExportDestination.getDestinationSshPrivateKeySecuritySecret().setSecretString(newSshPrivateKeySecret.getSecretString());
				}
			}
		}
		return getIntegrationExportDestinationService().saveIntegrationExportDestination(integrationExportDestination);
	}


	private IntegrationExportRunnerResult runIntegrationExport(IntegrationExport export) {
		try {
			// run the export
			return getIntegrationExportRunner().runIntegrationExport(export);
		}
		catch (Exception e) {
			IntegrationExportHistory runHistory = getIntegrationExportErrorHistory(export, e);
			return new IntegrationExportRunnerResult(export, runHistory.getExportStatus(), runHistory);
		}
	}


	private IntegrationExportHistory getIntegrationExportErrorHistory(IntegrationExport export, Exception e) {
		IntegrationExportHistory history = new IntegrationExportHistory();
		history.setStartDate(export.getReceivedDate());
		history.setEndDate(new Date());
		history.setDescription(e.getMessage() + "\nCAUSED BY: " + ExceptionUtils.getFullStackTrace(e));
		history.setExportStatus(getIntegrationExportService().getIntegrationExportStatusByName(ExportStatuses.FAILED.name()));
		history.setExport(export);
		if (export.getRetryCount() != null) {
			history.setRetryAttemptNumber(export.getRetryCount().shortValue());
		}
		return getIntegrationExportService().saveIntegrationExportHistory(history);
	}


	private void decryptDestinationProperties(ExportMessagingDestination destination, IntegrationSource source) {
		if (source.isEncryptedDataSendingEnabled()) {
			for (Map.Entry<ExportMapKeys, String> propertyEntry : destination.getPropertyList().entrySet()) {
				if (propertyEntry.getKey().isValueEncrypted() && !StringUtils.isEmpty(propertyEntry.getValue())) {
					propertyEntry.setValue(getSecurityRSATransportEncryptionServerHandler().decryptValueFromTransport(propertyEntry.getValue()));
				}
			}
		}
	}


	private void handleBasicError(Exception e, String sourceSystemIdentifier, String sourceSystemName) {
		String errorMessage = e.getMessage() + "\nCAUSED BY: " + ExceptionUtils.getFullStackTrace(e);
		getIntegrationExportStatusHandler().sendIntegrationExportStatus(sourceSystemIdentifier, sourceSystemName
				, ExportStatuses.FAILED, errorMessage);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public IntegrationExportRunner getIntegrationExportRunner() {
		return this.integrationExportRunner;
	}


	public void setIntegrationExportRunner(IntegrationExportRunner integrationExportRunner) {
		this.integrationExportRunner = integrationExportRunner;
	}


	public IntegrationExportService getIntegrationExportService() {
		return this.integrationExportService;
	}


	public void setIntegrationExportService(IntegrationExportService integrationExportService) {
		this.integrationExportService = integrationExportService;
	}


	public IntegrationExportStatusHandler getIntegrationExportStatusHandler() {
		return this.integrationExportStatusHandler;
	}


	public void setIntegrationExportStatusHandler(IntegrationExportStatusHandler integrationExportStatusHandler) {
		this.integrationExportStatusHandler = integrationExportStatusHandler;
	}


	public SecurityRSATransportEncryptionServerHandler getSecurityRSATransportEncryptionServerHandler() {
		return this.securityRSATransportEncryptionServerHandler;
	}


	public void setSecurityRSATransportEncryptionServerHandler(SecurityRSATransportEncryptionServerHandler securityRSATransportEncryptionServerHandler) {
		this.securityRSATransportEncryptionServerHandler = securityRSATransportEncryptionServerHandler;
	}


	public Map<String, ExportMessagingMessageConverter> getExportMessagingMessageConverterMap() {
		return this.exportMessagingMessageConverterMap;
	}


	public void setExportMessagingMessageConverterMap(Map<String, ExportMessagingMessageConverter> exportMessagingMessageConverterMap) {
		this.exportMessagingMessageConverterMap = exportMessagingMessageConverterMap;
	}


	public IntegrationExportDestinationService getIntegrationExportDestinationService() {
		return this.integrationExportDestinationService;
	}


	public void setIntegrationExportDestinationService(IntegrationExportDestinationService integrationExportDestinationService) {
		this.integrationExportDestinationService = integrationExportDestinationService;
	}
}
