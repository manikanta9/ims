package com.clifton.integration.outgoing.export.server;

import com.clifton.export.messaging.ExportStatuses;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportHistory;
import com.clifton.integration.outgoing.export.IntegrationExportStatus;


/**
 * @author theodorez
 */
public class IntegrationExportRunnerResult {

	private final IntegrationExport export;
	private final IntegrationExportStatus exportStatus;
	private final IntegrationExportHistory exportRunHistory;


	public IntegrationExportRunnerResult(IntegrationExport export, IntegrationExportStatus exportStatus, IntegrationExportHistory exportRunHistory) {
		this.export = export;
		this.exportStatus = exportStatus;
		this.exportRunHistory = exportRunHistory;
	}


	public boolean success() {
		return this.exportStatus != null && ExportStatuses.FAILED != this.exportStatus.getIntegrationExportStatusName();
	}


	public IntegrationExport getExport() {
		return this.export;
	}


	public IntegrationExportStatus getExportStatus() {
		return this.exportStatus;
	}


	public IntegrationExportHistory getExportRunHistory() {
		return this.exportRunHistory;
	}
}
