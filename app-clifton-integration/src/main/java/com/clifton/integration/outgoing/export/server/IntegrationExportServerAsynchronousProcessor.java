package com.clifton.integration.outgoing.export.server;


import com.clifton.core.context.Context;
import com.clifton.core.context.ContextHandler;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.core.messaging.jms.asynchronous.AsynchronousProcessor;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportStatuses;
import com.clifton.export.messaging.message.AbstractMessagingMessage;
import com.clifton.export.messaging.message.ExportMessagingContent;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.export.messaging.message.ExportMessagingMessageList;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileSourceType;
import com.clifton.integration.file.archive.IntegrationFileArchiveParameters;
import com.clifton.integration.file.archive.IntegrationFileArchiveService;
import com.clifton.integration.file.archive.IntegrationFileArchiveStrategySearchForm;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportBuildCommand;
import com.clifton.integration.outgoing.export.IntegrationExportProcessingHandler;
import com.clifton.integration.outgoing.export.IntegrationExportService;
import com.clifton.integration.outgoing.export.converter.ExportMessagingMessageConverter;
import com.clifton.integration.source.IntegrationSource;
import com.clifton.integration.source.IntegrationSourceService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class IntegrationExportServerAsynchronousProcessor implements AsynchronousProcessor<AbstractMessagingMessage> {


	private IntegrationSourceService integrationSourceService;
	private IntegrationFileArchiveService integrationFileArchiveService;

	private IntegrationExportService integrationExportService;

	private String defaultRunAsUserName = SecurityUser.SYSTEM_USER;
	private SecurityUserService securityUserService;
	private ContextHandler contextHandler;
	private IntegrationExportStatusHandler integrationExportStatusHandler;

	/**
	 * Will have an entry for EMAIL, FTP and PHONE
	 */
	private Map<String, ExportMessagingMessageConverter> exportMessagingMessageConverterMap;

	/**
	 * Represents the source type which is export in this case.
	 */
	private IntegrationFileSourceType fileSourceType = IntegrationFileSourceType.EXPORT;

	/**
	 * The location of the share for this integration server
	 */
	private String exportShareDirectory;


	private IntegrationExportProcessingHandler integrationExportProcessingHandler;


	@Override
	public void process(final AbstractMessagingMessage message, @SuppressWarnings("unused") AsynchronousMessageHandler handler) {

		// set the current user for database actions
		getContextHandler().setBean(Context.USER_BEAN_NAME, getRunAsUser());

		switch (message.getAction()) {
			case RESEND:
				if (message.getProperties().containsKey("integrationExportId")) {
					Integer exportId = new Integer(message.getProperties().get("integrationExportId"));
					IntegrationExport export = getIntegrationExportService().getIntegrationExport(exportId);
					getIntegrationExportProcessingHandler().reprocessIntegrationExport(export);
				}
				break;
			case SEND_NEW:
				processExportMessagingExport(message);
				break;
			default:
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void processExportMessagingExport(AbstractMessagingMessage message) {
		IntegrationSource source = getIntegrationSourceForMessage(message);

		//Archive the message content
		List<IntegrationFile> commonMessageFileList = getIntegrationFilesForMessage(message, source, message.getContentList());

		if (message instanceof ExportMessagingMessageList) {
			ExportMessagingMessageList listMessage = (ExportMessagingMessageList) message;

			for (ExportMessagingMessage messagingMessage : CollectionUtils.getIterable(listMessage.getMessageList())) {
				//Archive the individual message content
				List<IntegrationFile> messagingMessageFiles = getIntegrationFilesForMessage(messagingMessage, source, messagingMessage.getContentList());
				messagingMessageFiles.addAll(commonMessageFileList);
				//Send the export with Individual Message Content + Top Level Content
				getIntegrationExportProcessingHandler().processIntegrationExport(new IntegrationExportBuildCommand(messagingMessage, source, messagingMessageFiles));
			}
		}
		else if (message instanceof ExportMessagingMessage) {
			getIntegrationExportProcessingHandler().processIntegrationExport(new IntegrationExportBuildCommand((ExportMessagingMessage) message, source, commonMessageFileList));
		}
	}


	private List<IntegrationFile> getIntegrationFilesForMessage(AbstractMessagingMessage message, IntegrationSource source, List<ExportMessagingContent> contentList) {
		try {
			return archiveExportFiles(message, source, contentList);
		}
		catch (Exception e) {
			handleMessageError(e, message);
			throw e;
		}
	}


	private IntegrationSource getIntegrationSourceForMessage(AbstractMessagingMessage message) {
		try {// get the source system name and look up the integration source
			IntegrationSource source = getIntegrationSourceService().getIntegrationSourceByName(message.getSourceSystemName());
			ValidationUtils.assertNotNull(source, "Cannot find integration source system for [" + message.getSourceSystemName() + "].");
			return source;
		}
		catch (Exception e) {
			handleMessageError(e, message);
			throw e;
		}
	}


	@Transactional
	protected List<IntegrationFile> archiveExportFiles(AbstractMessagingMessage exportMessage, IntegrationSource source, List<ExportMessagingContent> contentList) {
		List<IntegrationFile> result = new ArrayList<>();
		for (ExportMessagingContent content : CollectionUtils.getIterable(contentList)) {
			String originalFileName = getOriginalFileName(content.getFileNameWithPath());
			if (StringUtils.isEmpty(originalFileName)) {
				//DO NOT try to archive files with no file name
				continue;
			}
			String sourceFileNameWithPath = FileUtils.combinePath(getExportShareDirectory(), content.getFileNameWithPath());

			IntegrationFileArchiveParameters params = new IntegrationFileArchiveParameters(new File(sourceFileNameWithPath), originalFileName, getFileSourceType(), null);
			params.setImportSource(source);
			params.setDestinationFileNameOverride(originalFileName);
			params.setSourceSystemDefinitionName(exportMessage.getSourceSystemDefinitionName());
			params.setDeleteSourceFile(content.isDeleteSourceFile());
			params.setFilePassword(content.getPassword());
			params.setBaseArchivePath(exportMessage.getBaseArchivePath());
			//Set the strategy
			if (!StringUtils.isEmpty(exportMessage.getArchiveStrategyName())) {
				IntegrationFileArchiveStrategySearchForm strategySearchForm = new IntegrationFileArchiveStrategySearchForm();
				strategySearchForm.setName(exportMessage.getArchiveStrategyName());
				params.setArchiveStrategy(CollectionUtils.getFirstElementStrict(getIntegrationFileArchiveService().getIntegrationFileArchiveStrategyList(strategySearchForm)));
			}

			if (!exportMessage.getProperties().isEmpty()) {
				Map<String, Object> additionalPropertyMap = new HashMap<>(exportMessage.getProperties());
				params.setAdditionalProperties(additionalPropertyMap);
			}
			result.add(getIntegrationFileArchiveService().archiveIntegrationFile(params));
		}
		return result;
	}


	private String getOriginalFileName(String fileName) {
		fileName = FileUtils.getFileName(fileName);

		int uniqueIdentifierIndex = fileName.lastIndexOf("__");
		if (uniqueIdentifierIndex >= 0) {
			String original = fileName.substring(0, uniqueIdentifierIndex);
			String extension = FileUtils.getFileExtension(fileName);

			return original + "." + extension;
		}

		return fileName;
	}


	private void handleMessageError(Exception e, AbstractMessagingMessage message) {
		handleBasicError(e, message.getSourceSystemIdentifier(), message.getSourceSystemName());
	}


	private void handleBasicError(Exception e, String sourceSystemIdentifier, String sourceSystemName) {
		String errorMessage = e.getMessage() + "\nCAUSED BY: " + ExceptionUtils.getFullStackTrace(e);
		getIntegrationExportStatusHandler().sendIntegrationExportStatus(sourceSystemIdentifier, sourceSystemName
				, ExportStatuses.FAILED, errorMessage);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private SecurityUser getRunAsUser() {
		return getSecurityUserService().getSecurityUserByName(getDefaultRunAsUserName());
	}


	public String getDefaultRunAsUserName() {
		return this.defaultRunAsUserName;
	}


	public void setDefaultRunAsUserName(String defaultRunAsUserName) {
		this.defaultRunAsUserName = defaultRunAsUserName;
	}


	public IntegrationSourceService getIntegrationSourceService() {
		return this.integrationSourceService;
	}


	public void setIntegrationSourceService(IntegrationSourceService integrationSourceService) {
		this.integrationSourceService = integrationSourceService;
	}


	public IntegrationFileArchiveService getIntegrationFileArchiveService() {
		return this.integrationFileArchiveService;
	}


	public void setIntegrationFileArchiveService(IntegrationFileArchiveService integrationFileArchiveService) {
		this.integrationFileArchiveService = integrationFileArchiveService;
	}


	public IntegrationExportService getIntegrationExportService() {
		return this.integrationExportService;
	}


	public void setIntegrationExportService(IntegrationExportService integrationExportService) {
		this.integrationExportService = integrationExportService;
	}


	public SecurityUserService getSecurityUserService() {
		return this.securityUserService;
	}


	public void setSecurityUserService(SecurityUserService securityUserService) {
		this.securityUserService = securityUserService;
	}


	public ContextHandler getContextHandler() {
		return this.contextHandler;
	}


	public void setContextHandler(ContextHandler contextHandler) {
		this.contextHandler = contextHandler;
	}


	public IntegrationExportStatusHandler getIntegrationExportStatusHandler() {
		return this.integrationExportStatusHandler;
	}


	public void setIntegrationExportStatusHandler(IntegrationExportStatusHandler integrationExportStatusHandler) {
		this.integrationExportStatusHandler = integrationExportStatusHandler;
	}


	public Map<String, ExportMessagingMessageConverter> getExportMessagingMessageConverterMap() {
		return this.exportMessagingMessageConverterMap;
	}


	public void setExportMessagingMessageConverterMap(Map<String, ExportMessagingMessageConverter> exportMessagingMessageConverterMap) {
		this.exportMessagingMessageConverterMap = exportMessagingMessageConverterMap;
	}


	public IntegrationFileSourceType getFileSourceType() {
		return this.fileSourceType;
	}


	public void setFileSourceType(IntegrationFileSourceType fileSourceType) {
		this.fileSourceType = fileSourceType;
	}


	public String getExportShareDirectory() {
		return this.exportShareDirectory;
	}


	public void setExportShareDirectory(String exportShareDirectory) {
		this.exportShareDirectory = exportShareDirectory;
	}


	public IntegrationExportProcessingHandler getIntegrationExportProcessingHandler() {
		return this.integrationExportProcessingHandler;
	}


	public void setIntegrationExportProcessingHandler(IntegrationExportProcessingHandler integrationExportProcessingHandler) {
		this.integrationExportProcessingHandler = integrationExportProcessingHandler;
	}
}
