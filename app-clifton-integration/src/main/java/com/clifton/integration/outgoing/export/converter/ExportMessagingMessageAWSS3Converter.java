package com.clifton.integration.outgoing.export.converter;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;


/**
 * Move the file to a AWS S3 location.
 *
 * @author rtschumper
 */
public class ExportMessagingMessageAWSS3Converter extends AbstractExportMessagingMessageConverter {

	@Override
	protected IntegrationExportDestination populateExportMessagingDestination(ExportMessagingDestination exportMessagingDestination, IntegrationExportDestination integrationExportDestination, String subDestinationValue, ExportMapKeys key) {
		return integrationExportDestination;
	}
}

