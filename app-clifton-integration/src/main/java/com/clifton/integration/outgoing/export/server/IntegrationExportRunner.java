package com.clifton.integration.outgoing.export.server;


import com.clifton.integration.outgoing.export.IntegrationExport;


/**
 * The <code>IntegrationExportRunner</code> defines a runner that will execute the sending of an IntegrationExport.
 *
 * @author mwacker
 */
public interface IntegrationExportRunner {

	public IntegrationExportRunnerResult runIntegrationExport(IntegrationExport export);
}
