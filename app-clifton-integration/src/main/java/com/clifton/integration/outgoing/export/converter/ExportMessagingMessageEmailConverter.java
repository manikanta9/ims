package com.clifton.integration.outgoing.export.converter;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;

import static com.clifton.export.messaging.ExportMapKeys.EMAIL_BCC;
import static com.clifton.export.messaging.ExportMapKeys.EMAIL_CC;
import static com.clifton.export.messaging.ExportMapKeys.EMAIL_TO;


/**
 * @author mwacker
 */
public class ExportMessagingMessageEmailConverter extends AbstractExportMessagingMessageConverter {

	@Override
	protected IntegrationExportDestination populateExportMessagingDestination(ExportMessagingDestination exportMessagingDestination, IntegrationExportDestination integrationExportDestination, String subDestinationValue, ExportMapKeys key) {
		return integrationExportDestination;
	}


	@Override
	public String getDestinationValue(ExportMessagingDestination exportMessagingDestination, String subDestinationValue, ExportMapKeys key) {
		if (key == EMAIL_TO || key == EMAIL_CC || key == EMAIL_BCC) {
			return subDestinationValue;
		}
		return super.getDestinationValue(exportMessagingDestination, subDestinationValue, key);
	}
}
