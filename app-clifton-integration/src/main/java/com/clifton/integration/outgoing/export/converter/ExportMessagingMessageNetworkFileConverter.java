package com.clifton.integration.outgoing.export.converter;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;


/**
 * Move the file to a network location.
 *
 * @author mwacker
 */
public class ExportMessagingMessageNetworkFileConverter extends AbstractExportMessagingMessageConverter {

	@Override
	protected IntegrationExportDestination populateExportMessagingDestination(ExportMessagingDestination exportMessagingDestination, IntegrationExportDestination integrationExportDestination, String subDestinationValue, ExportMapKeys key) {
		Boolean overwriteFile = getDestinationProperty(exportMessagingDestination, ExportMapKeys.NETWORK_OVERWRITE, Boolean.class);
		integrationExportDestination.setOverwriteFile(overwriteFile != null && overwriteFile);
		Boolean writeExtension = getDestinationProperty(exportMessagingDestination, ExportMapKeys.NETWORK_WRITE_EXTENSION, Boolean.class);
		integrationExportDestination.setWriteExtension(writeExtension != null && writeExtension);

		return integrationExportDestination;
	}
}
