package com.clifton.integration.outgoing.export.converter;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.security.secret.SecuritySecret;


/**
 * @author mwacker
 */
public class ExportMessagingMessageFtpConverter extends AbstractExportMessagingMessageConverter {

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public String getDestinationValue(ExportMessagingDestination exportMessagingDestination, String subDestinationValue, ExportMapKeys key) {
		return getFtpUrl(exportMessagingDestination);
	}


	@Override
	public String getDestinationText(ExportMessagingDestination exportMessagingDestination) {
		return getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_REMOTE_FOLDER);
	}


	@Override
	protected IntegrationExportDestination populateExportMessagingDestination(ExportMessagingDestination exportMessagingDestination, IntegrationExportDestination integrationExportDestination, String subDestinationValue, ExportMapKeys key) {

		String ftpUrl = getFtpUrl(exportMessagingDestination);

		integrationExportDestination.setDestinationValue(ftpUrl);

		integrationExportDestination.setDestinationText(getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_REMOTE_FOLDER));
		integrationExportDestination.setDestinationUserName(getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_USER));

		integrationExportDestination.setDestinationPasswordSecuritySecret(getSecret(exportMessagingDestination, ExportMapKeys.FTP_PASSWORD));

		integrationExportDestination.setPgpPublicKey(getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_PGP_PUBLIC_KEY));
		integrationExportDestination.setActiveFtp(getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_ACTIVE_MODE, Boolean.class));

		integrationExportDestination.setDestinationSshPrivateKeySecuritySecret(getSecret(exportMessagingDestination, ExportMapKeys.FTP_SSH_PRIVATE_KEY));

		Boolean pgpArmor = getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_PGP_ARMOR, Boolean.class);
		integrationExportDestination.setPgpArmor(pgpArmor != null && pgpArmor);

		Boolean pgpIntegrityCheck = getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_PGP_INTEGRITY_CHECK, Boolean.class);
		integrationExportDestination.setPgpIntegrityCheck(pgpIntegrityCheck != null && pgpIntegrityCheck);

		return integrationExportDestination;
	}

	@Override
	public IntegrationExportDestination copyProperties(IntegrationExportDestination existingIntegrationExportDestination, IntegrationExportDestination integrationExportDestination){
		String[] auditProperties = new String[]{"createUserId", "createDate", "updateUserId", "updateDate", "rv", "id"};
		BeanUtils.copyProperties(integrationExportDestination , existingIntegrationExportDestination, auditProperties);
		return existingIntegrationExportDestination;
	}


	private SecuritySecret getSecret(ExportMessagingDestination exportMessagingDestination, ExportMapKeys exportMapKey) {
		String property = getDestinationProperty(exportMessagingDestination, exportMapKey);
		if (!StringUtils.isEmpty(property)) {
			SecuritySecret passwordSecret = new SecuritySecret();
			passwordSecret.setSecretString(property);
			return passwordSecret;
		}
		return null;
	}


	private String getFtpUrl(ExportMessagingDestination exportMessagingDestination) {
		String port = getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_PORT);
		return getDestinationProperty(exportMessagingDestination, ExportMapKeys.FTP_URL) + (!StringUtils.isEmpty(port) ? ":" + port : "");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
}
