package com.clifton.integration.outgoing.export.server;

import com.clifton.export.messaging.ExportStatuses;


/**
 * @author mwacker
 */
public interface IntegrationExportStatusHandler {

	public void sendIntegrationExportStatus(String sourceSystemIdentifier, String sourceSystemName, ExportStatuses status, String message);
}
