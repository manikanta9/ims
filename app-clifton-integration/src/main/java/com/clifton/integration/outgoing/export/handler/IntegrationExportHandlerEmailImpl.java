package com.clifton.integration.outgoing.export.handler;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.email.Email;
import com.clifton.core.util.email.EmailHandler;
import com.clifton.core.util.email.EmailHandlerImpl;
import com.clifton.core.util.email.MimeContentTypes;
import com.clifton.export.messaging.ExportDestinationSubTypes;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;

import java.io.File;
import java.util.Collections;
import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationExportHandlerEmailImpl</code> handles email sending for integration exports.
 *
 * @author mwacker
 */
public class IntegrationExportHandlerEmailImpl implements IntegrationExportHandler {

	private EmailHandler emailHandler;
	private IntegrationFileService integrationFileService;


	@Override
	public List<IntegrationExportFileResult> send(IntegrationExport integrationExport, Map<String, String> context) {
		if (!StringUtils.isEmpty(integrationExport.getServerHostname())) {
			EmailHandlerImpl abstractEmailHandler = (EmailHandlerImpl) getEmailHandler();
			EmailHandler testEmailHandler = new EmailHandlerImpl(integrationExport.getServerHostname(), integrationExport.getServerPort(), abstractEmailHandler.getStoreProtocol(), abstractEmailHandler.getTransportProtocol(), abstractEmailHandler.getDefaultFromAddress());
			testEmailHandler.send(getEmailFromExport(integrationExport));
			testEmailHandler.closeService();
		}
		else {
			getEmailHandler().send(getEmailFromExport(integrationExport));
		}

		/*
		 * It doesn't make much sense for IntegrationExportHandlerEmailImpl to return per-file results, since the attachments are all sent in a single e-mail and
		 * it should be all or nothing. If there is an exception, it will propagate up.
		 */
		return null;
	}


	private Email getEmailFromExport(IntegrationExport export) {
		String messageSubject = export.getMessageSubject();

		String messageText = export.getMessageText();

		String fromAddress = IntegrationExportHandlerUtils.getDestinationSubTypeValue(ExportDestinationSubTypes.EMAIL_FROM_ADDRESS, export);
		String fromName = IntegrationExportHandlerUtils.getDestinationSubTypeText(ExportDestinationSubTypes.EMAIL_FROM_ADDRESS, export);

		String senderAddress = IntegrationExportHandlerUtils.getDestinationSubTypeValue(ExportDestinationSubTypes.EMAIL_SENDER_ADDRESS, export);
		String senderName = IntegrationExportHandlerUtils.getDestinationSubTypeText(ExportDestinationSubTypes.EMAIL_SENDER_ADDRESS, export);

		String[] toAddresses = StringUtils.toStringArray(IntegrationExportHandlerUtils.getDestinationSubTypeValueList(ExportDestinationSubTypes.EMAIL_TO, export));

		String[] ccAddresses = StringUtils.toStringArray(IntegrationExportHandlerUtils.getDestinationSubTypeValueList(ExportDestinationSubTypes.EMAIL_CC, export));

		String[] bccAddresses = StringUtils.toStringArray(IntegrationExportHandlerUtils.getDestinationSubTypeValueList(ExportDestinationSubTypes.EMAIL_BCC, export));

		List<IntegrationFile> integrationFileList = export.getDestinationType().getIntegrationExportDestinationTypeName() == ExportDestinationTypes.BY_ROW_EMAIL ? Collections.emptyList() : export.getIntegrationFileList();
		FileWrapper[] attachments = new FileWrapper[integrationFileList.size()];
		int i = 0;
		for (IntegrationFile file : CollectionUtils.getIterable(integrationFileList)) {
			String fullFilePath = getIntegrationFileService().getIntegrationFileAbsolutePath(file);
			StringBuilder attachmentFileName = new StringBuilder();
			if (!StringUtils.isEmpty(export.getFilenamePrefix())) {
				attachmentFileName.append(export.getFilenamePrefix());
			}
			attachmentFileName.append(IntegrationFileUtils.removeTimeStamp(FileUtils.getFileName(file.getFileNameWithPath())));
			attachments[i] = new FileWrapper(new File(fullFilePath), attachmentFileName.toString(), false);
			i++;
		}

		return new Email(fromName, fromAddress, senderName, senderAddress, toAddresses, ccAddresses, bccAddresses, messageSubject, messageText, MimeContentTypes.TEXT_HTML, attachments);
	}

	/////////////////////////////////////////////////////////////////////////////////////


	public EmailHandler getEmailHandler() {
		return this.emailHandler;
	}


	public void setEmailHandler(EmailHandler emailHandler) {
		this.emailHandler = emailHandler;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}
}
