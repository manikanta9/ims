package com.clifton.integration.outgoing.export.converter;

import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;


/**
 * @author mwacker
 */
public class ExportMessagingMessageFaxConverter extends AbstractExportMessagingMessageConverter {


	@Override
	protected IntegrationExportDestination populateExportMessagingDestination(ExportMessagingDestination exportMessagingDestination, IntegrationExportDestination integrationExportDestination, String subDestinationValue, ExportMapKeys key) {
		return integrationExportDestination;
	}
}
