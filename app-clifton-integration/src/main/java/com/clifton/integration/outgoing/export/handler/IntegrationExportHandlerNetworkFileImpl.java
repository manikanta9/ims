package com.clifton.integration.outgoing.export.handler;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus.IntegrationExportFileStatusNames;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationExportHandlerFtpImpl</code> handles send files to via ftp for integration exports.
 *
 * @author msiddiqui
 */
public class IntegrationExportHandlerNetworkFileImpl implements IntegrationExportHandler {

	private IntegrationFileService integrationFileService;


	@Override
	public List<IntegrationExportFileResult> send(IntegrationExport export, Map<String, String> context) {
		List<IntegrationExportFileResult> results = new ArrayList<>();

		IntegrationExportIntegrationExportDestination exportToExportDestination = CollectionUtils.getOnlyElement(export.getLinkedDestinationList());

		if (exportToExportDestination != null && exportToExportDestination.getDestinationSubType().getDestinationType().getIntegrationExportDestinationTypeName() == ExportDestinationTypes.NETWORK_FILE) {
			IntegrationExportDestination integrationExportDestination = exportToExportDestination.getReferenceTwo();
			FileContainer networkPath = FileContainerFactory.getFileContainer(integrationExportDestination.getDestinationValue());
			ValidationUtils.assertTrue(networkPath.exists(), "Cannot send files [" + networkPath + "] because it does not exist.");

			for (IntegrationFile file : export.getIntegrationFileList()) {
				IntegrationExportFileResult result = new IntegrationExportFileResult();
				result.setFile(file);
				results.add(result);

				try {
					FileContainer fullFilePath = FileContainerFactory.getFileContainer(getIntegrationFileService().getIntegrationFileAbsolutePath(file));
					String targetFileName = IntegrationFileUtils.removeTimeStamp(fullFilePath.getName());

					String targetAbsolutePath = FileUtils.combinePaths(networkPath.getPath(), targetFileName);
					FileContainer targetFileContainer = FileContainerFactory.getFileContainer(targetAbsolutePath);

					boolean overwrite = integrationExportDestination.isOverwriteFile();
					boolean writeExtension = integrationExportDestination.isWriteExtension();

					if(!overwrite && targetFileContainer.exists()) {
						throw new ValidationException("Target path " + targetFileContainer.getAbsolutePath() + "already exists. Enable Overwrite File to process this export");
					}
					if(writeExtension) {
						String targetWriteAbsolutePath = FileUtils.combinePaths(networkPath.getPath(), targetFileName + "." + IntegrationFileUtils.FILE_NAME_WRITING_EXTENSION);
						FileContainer targetWriteFileContainer = FileContainerFactory.getFileContainer(targetWriteAbsolutePath);
						FileUtils.copyFileOverwrite(fullFilePath, targetWriteFileContainer);
						boolean successful;
						if(overwrite && targetFileContainer.exists()) {
							// Need to create a temp file (Path: Parent/NanoTime_FileName) if target already exists. Windows doesn't allow rename to overwrite.
							String tempAbsolutePath = FileUtils.combinePaths(targetFileContainer.getParent(), (System.nanoTime() + "_" + targetFileContainer.getName()));
							FileContainer tempFileContainer = FileContainerFactory.getFileContainer(tempAbsolutePath);
							// Rename the original file to the temp file
							successful = targetFileContainer.renameToFile(tempFileContainer);
							if(successful){
								// Rename the write extension file to the original file
								successful = targetWriteFileContainer.renameToFile(targetFileContainer);
								if(successful) {
									// Delete the original file
									tempFileContainer.deleteFile();
								} else {
									// Reset the original file
									tempFileContainer.renameToFile(targetFileContainer);
								}
							}
						} else {
							successful = targetWriteFileContainer.renameToFile(targetFileContainer);
						}
						if (!successful) {
							throw new ValidationException("Could not rename " + targetWriteFileContainer.getAbsolutePath() + " to " + targetFileContainer.getAbsolutePath());
						}
					}
					else if(overwrite) {
						FileUtils.copyFileOverwrite(fullFilePath, FileContainerFactory.getFileContainer(FileUtils.combinePath(networkPath.getPath(), targetFileName)));
					} else{
						FileUtils.copyFile(fullFilePath, FileContainerFactory.getFileContainer(FileUtils.combinePath(networkPath.getPath(), targetFileName)));
					}
					result.setStatus(IntegrationExportFileStatusNames.PROCESSED);
				}
				catch (Throwable e) {
					result.setStatus(IntegrationExportFileStatusNames.FAILED);
					result.setException(e);
				}
			}
		}
		return results;
	}


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}

}
