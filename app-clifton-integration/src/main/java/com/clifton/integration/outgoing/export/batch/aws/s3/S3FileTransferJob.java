package com.clifton.integration.outgoing.export.batch.aws.s3;

import com.clifton.calendar.date.CalendarDateGenerationHandler;
import com.clifton.calendar.date.DateGenerationOptions;
import com.clifton.core.converter.json.JsonStringToMapConverter;
import com.clifton.core.converter.template.TemplateConfig;
import com.clifton.core.converter.template.TemplateConverter;
import com.clifton.core.dataaccess.file.FilePath;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.aws.s3.AWSS3Handler;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.runner.Task;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.security.credential.SecurityCredential;
import com.clifton.security.credential.SecurityCredentialService;
import com.clifton.security.secret.SecuritySecret;
import com.clifton.security.secret.SecuritySecretService;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Pattern;
import java.util.stream.Collectors;


@Component
public class S3FileTransferJob implements Task, ValidationAware {

	private AWSS3Handler awsS3Handler;

	private SecurityCredentialService securityCredentialService;

	private SecuritySecretService securitySecretService;

	/**
	 * The path to the network location where we will be pulling files from.
	 */
	private String fromPath;
	/**
	 * The name of the AWS S3 bucket the files are going to.
	 */
	private String awsBucketName;
	/**
	 * The name of the AWS S3 region the bucket is located in.
	 */
	private String awsRegionName;
	/**
	 * The id of the aws user in the credential store.
	 */
	private Integer awsUser;
	/**
	 * A regex filter used to select the files to be moved.  This can be a Freemarker template
	 * with the DATE variable.  For example, (${DATE?string("MMddyy")}) would create a regex list
	 * (031616) on 03/16/2016 and match a file named 'FuturesWeekly_031616.xlsx'.
	 */
	private String filterRegex;
	/**
	 * A server-side glob or wildcard pattern, can significantly improve performance.
	 * This can take advantage of the Freemarker template pattern ${DATE}?string("MMddyy").
	 */
	private String wildcardPattern;
	/**
	 * Delete the files from the source directory after they have been moved.
	 */
	private boolean deleteSourceFiles;
	/**
	 * Number of days to go back to calculate the DATE variable for the filterRegex template.
	 */
	private Integer daysFromToday = 1;
	/**
	 * The date generation strategy used to calculate the DATE variable for the filterRegex template.
	 */
	private DateGenerationOptions dateGenerationOption;
	/**
	 * The JSON string representation of the map of source to destination file extension mappings.
	 */
	private String destinationExtensionMap;
	/**
	 * The map built from the JSON representation in destinationExtensionMap
	 */
	private Map<String, String> translatedDestinationExtensionMap;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	private TemplateConverter templateConverter;
	private CalendarDateGenerationHandler calendarDateGenerationHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public Status run(Map<String, Object> context) {
		String wildcard = StringUtils.isEmpty(getWildcardPattern()) ? "*" : doFreemarkerReplacements(getWildcardPattern());
		Pattern fileNamePattern = getFileNamePattern();
		String finalFromPath = doFreemarkerReplacements(getFromPath());
		FileContainer fromDirectoryContainer = FileContainerFactory.getFileContainer(finalFromPath);
		this.translatedDestinationExtensionMap = buildTranslatedDestinationExtensionMap();

		Status status = Status.ofEmptyMessage();
		final FileCopyResult fileCopyResult = new FileCopyResult(status);
		try {
			fromDirectoryContainer.list(wildcard, fn -> true).forEach(fn -> {
				Path filePath = Paths.get(fn);
				if (!filePath.toFile().isDirectory()) {
					if (fileNamePattern.matcher(fn).matches()) {
						String fileName = doFileExtensionReplacement(fn);

						long fileLength;
						try {
							fileLength = FileUtils.getLength(FilePath.forPath(FileUtils.combinePath(finalFromPath, fn)));
						}
						catch (IOException ioException) {
							throw new ValidationException(ioException.getMessage());
						}

						FileContainer fromFile = FileContainerFactory.getFileContainer(FileUtils.combinePath(finalFromPath, fn));

						// If the user selected a specific AWS S3 user policy to run the batch job with
						if (getAwsUser() != null) {
							SecurityCredential securityCredential = getSecurityCredentialService().getSecurityCredential(getAwsUser().shortValue());
							String accessKey = securityCredential.getUserName();
							String secretKey = decrypt(securityCredential);
							getAwsS3Handler().send(getAwsRegionName(), getAwsBucketName(), fromFile, fileName, fileLength, accessKey, secretKey);
						}
						else {
							getAwsS3Handler().send(getAwsRegionName(), getAwsBucketName(), fromFile, fileName, fileLength);
						}
						fileCopyResult.setFileCount(fileCopyResult.getFileCount() + 1);
					}
					else {
						fileCopyResult.setSkippedFileCount(fileCopyResult.getSkippedFileCount() + 1);
					}
				}
			});
		}
		catch (Throwable e) {
			status.addError(ExceptionUtils.getDetailedMessage(e));
		}

		status.setMessage("File Transfer Count: " + fileCopyResult.getFileCount() + "\nFile Skipped Count: " + fileCopyResult.getSkippedFileCount() + "\nError Count: " + fileCopyResult.getErrorCount());
		if (fileCopyResult.getFileCount() == 0) {
			status.setActionPerformed(false);
		}
		return status;
	}


	@Override
	public void validate() throws ValidationException {
		String finalFromPath = doFreemarkerReplacements(getFromPath());
		ValidationUtils.assertTrue(FileContainerFactory.getFileContainer(finalFromPath).isDirectory(), "The from path [" + finalFromPath + "] does not exist.");
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private String doFileExtensionReplacement(String fileName) {
		if (!CollectionUtils.isEmpty(getTranslatedDestinationExtensionMap())) {
			// allow adding an extension to a file without one
			String extension = Optional.ofNullable(FileUtils.getFileExtension(fileName)).orElse("");
			if (getTranslatedDestinationExtensionMap().containsKey(extension.toUpperCase())) {
				return FileUtils.getFileNameWithoutExtension(fileName) + "." + getTranslatedDestinationExtensionMap().get(extension.toUpperCase());
			}
			else if (getTranslatedDestinationExtensionMap().containsKey("+")) {
				return fileName + "." + getTranslatedDestinationExtensionMap().get("+");
			}
		}
		return fileName;
	}


	private Date getReportingDate() {
		if (getDateGenerationOption() != null) {
			return getCalendarDateGenerationHandler().generateDate(getDateGenerationOption(), getDaysFromToday());
		}
		return getCalendarDateGenerationHandler().generateDate(DateGenerationOptions.PREVIOUS_BUSINESS_DAY);
	}


	private Pattern getFileNamePattern() {
		String fileRegex = doFreemarkerReplacements(getFilterRegex());
		if (!StringUtils.isEmpty(fileRegex)) {
			return Pattern.compile(fileRegex);
		}
		return Pattern.compile(".*");
	}


	private String doFreemarkerReplacements(String freemarker) {
		if (!StringUtils.isEmpty(freemarker)) {
			TemplateConfig config = new TemplateConfig(freemarker);
			config.addBeanToContext("DATE", DateUtils.clearTime(getReportingDate()));
			return getTemplateConverter().convert(config);
		}
		return null;
	}


	@SuppressWarnings("unchecked")
	private Map<String, String> buildTranslatedDestinationExtensionMap() {
		if (!StringUtils.isEmpty(getDestinationExtensionMap())) {
			Map<String, Object> objectMap = (new JsonStringToMapConverter()).convert(getDestinationExtensionMap());
			if (objectMap.containsKey(JsonStringToMapConverter.ROOT_ARRAY_NAME)) {
				List<Object> arr = (List<Object>) objectMap.get(JsonStringToMapConverter.ROOT_ARRAY_NAME);
				if (!CollectionUtils.isEmpty(arr)) {
					Map<String, String> strMap = (Map<String, String>) arr.get(0);
					return strMap.entrySet().stream()
							.collect(Collectors.toMap(e -> e.getKey().toUpperCase(), Map.Entry::getValue));
				}
			}
		}
		return Collections.emptyMap();
	}


	private String decrypt(SecurityCredential securityCredential) {
		SecuritySecret securitySecret = securityCredential.getPasswordSecret();
		securitySecret = getSecuritySecretService().decryptSecuritySecret(securitySecret);
		return securitySecret.getSecretString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public AWSS3Handler getAwsS3Handler() {
		return this.awsS3Handler;
	}


	public void setAwsS3Handler(AWSS3Handler awsS3Handler) {
		this.awsS3Handler = awsS3Handler;
	}


	public SecurityCredentialService getSecurityCredentialService() {
		return this.securityCredentialService;
	}


	public void setSecurityCredentialService(SecurityCredentialService securityCredentialService) {
		this.securityCredentialService = securityCredentialService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public String getFromPath() {
		return this.fromPath;
	}


	public void setFromPath(String fromPath) {
		this.fromPath = fromPath;
	}


	public String getAwsBucketName() {
		return this.awsBucketName;
	}


	public void setAwsBucketName(String awsBucketName) {
		this.awsBucketName = awsBucketName;
	}


	public String getAwsRegionName() {
		return this.awsRegionName;
	}


	public void setAwsRegionName(String awsRegionName) {
		this.awsRegionName = awsRegionName;
	}


	public Integer getAwsUser() {
		return this.awsUser;
	}


	public void setAwsUser(Integer awsUser) {
		this.awsUser = awsUser;
	}


	public String getFilterRegex() {
		return this.filterRegex;
	}


	public void setFilterRegex(String filterRegex) {
		this.filterRegex = filterRegex;
	}


	public String getWildcardPattern() {
		return this.wildcardPattern;
	}


	public void setWildcardPattern(String wildcardPattern) {
		this.wildcardPattern = wildcardPattern;
	}


	public boolean isDeleteSourceFiles() {
		return this.deleteSourceFiles;
	}


	public void setDeleteSourceFiles(boolean deleteSourceFiles) {
		this.deleteSourceFiles = deleteSourceFiles;
	}


	public com.clifton.core.converter.template.TemplateConverter getTemplateConverter() {
		return this.templateConverter;
	}


	public void setTemplateConverter(com.clifton.core.converter.template.TemplateConverter templateConverter) {
		this.templateConverter = templateConverter;
	}


	public Integer getDaysFromToday() {
		return this.daysFromToday;
	}


	public void setDaysFromToday(Integer daysFromToday) {
		this.daysFromToday = daysFromToday;
	}


	public DateGenerationOptions getDateGenerationOption() {
		return this.dateGenerationOption;
	}


	public void setDateGenerationOption(DateGenerationOptions dateGenerationOption) {
		this.dateGenerationOption = dateGenerationOption;
	}


	public CalendarDateGenerationHandler getCalendarDateGenerationHandler() {
		return this.calendarDateGenerationHandler;
	}


	public void setCalendarDateGenerationHandler(CalendarDateGenerationHandler calendarDateGenerationHandler) {
		this.calendarDateGenerationHandler = calendarDateGenerationHandler;
	}


	public String getDestinationExtensionMap() {
		return this.destinationExtensionMap;
	}


	public void setDestinationExtensionMap(String destinationExtensionMap) {
		this.destinationExtensionMap = destinationExtensionMap;
	}


	public Map<String, String> getTranslatedDestinationExtensionMap() {
		return this.translatedDestinationExtensionMap;
	}


	/**
	 * Used to hold the results of the folder copy.  Needed because the
	 * lambda loop can only access final objects.
	 */
	private static class FileCopyResult {

		private final Status status;
		private int fileCount = 0;
		private int errorCount = 0;
		private int skippedFileCount = 0;


		public FileCopyResult(Status status) {
			this.status = status;
		}


		public int getFileCount() {
			return this.fileCount;
		}


		public void setFileCount(int fileCount) {
			this.fileCount = fileCount;
		}


		public int getErrorCount() {
			return this.errorCount;
		}


		public void setErrorCount(int errorCount) {
			this.errorCount = errorCount;
		}


		public int getSkippedFileCount() {
			return this.skippedFileCount;
		}


		public void setSkippedFileCount(int skippedFileCount) {
			this.skippedFileCount = skippedFileCount;
		}


		public Status getStatus() {
			return this.status;
		}
	}
}
