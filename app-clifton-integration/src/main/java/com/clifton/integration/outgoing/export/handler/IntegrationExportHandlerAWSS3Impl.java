package com.clifton.integration.outgoing.export.handler;

import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.container.FileContainer;
import com.clifton.core.dataaccess.file.container.FileContainerFactory;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.aws.s3.AWSS3Handler;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationExportHandlerAWSS3Impl</code> handles send files to via AWS S3 for integration exports.
 *
 * @author BrandonC, rtschumper
 */
@Component
public class IntegrationExportHandlerAWSS3Impl implements IntegrationExportHandler {

	private AWSS3Handler awsS3Handler;

	private IntegrationFileService integrationFileService;

	public List<IntegrationExportFileResult> send(IntegrationExport export, Map<String, String> context) {
		List<IntegrationExportFileResult> results = new ArrayList<>();

		for (IntegrationFile file : export.getIntegrationFileList()) {
			IntegrationExportFileResult result = new IntegrationExportFileResult();
			result.setFile(file);
			results.add(result);

			try{
				long fileLength = file.getFileSize();
				String fileName = IntegrationFileUtils.removeTimeStamp(FileUtils.getFileName(this.getIntegrationFileService().getIntegrationFileAbsolutePath(file)));

				FileContainer fromFile = FileContainerFactory.getFileContainer(this.getIntegrationFileService().getIntegrationFileAbsolutePath(file));

				// For testing
				if (!StringUtils.isEmpty(export.getAwsS3EndpointOverride())) {
					getAwsS3Handler().send(export.getMessageText(), export.getMessageSubject(), fromFile, fileName, fileLength, export.getAwsS3EndpointOverride());
				}
				else if((export.getAwsS3AccessKeyId() != null) && (export.getAwsS3SecretKey() != null)){
					getAwsS3Handler().send(export.getMessageText(), export.getMessageSubject(), fromFile, fileName, fileLength, export.getAwsS3AccessKeyId(), export.getAwsS3SecretKey());
				}
				else {
					getAwsS3Handler().send(export.getMessageText(), export.getMessageSubject(), fromFile, fileName, fileLength);
				}

				result.setStatus(IntegrationExportFileStatus.IntegrationExportFileStatusNames.PROCESSED);
			}
			catch (Exception e){
				result.setStatus(IntegrationExportFileStatus.IntegrationExportFileStatusNames.FAILED);
				result.setException(e);
			}
		}

		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////              Getter and Setter Methods                /////////////
	////////////////////////////////////////////////////////////////////////////

	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}

	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}

	public AWSS3Handler getAwsS3Handler(){
		return this.awsS3Handler;
	}

	public void setAwsS3Handler(AWSS3Handler awsS3Handler){
		this.awsS3Handler = awsS3Handler;
	}

}
