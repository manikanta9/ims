package com.clifton.integration.outgoing.export.handler;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.PGPFileUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.ftp.FtpConfig;
import com.clifton.core.util.ftp.FtpProtocols;
import com.clifton.core.util.ftp.FtpUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.file.IntegrationFileUtils;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus.IntegrationExportFileStatusNames;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;
import com.clifton.security.secret.SecuritySecretService;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationExportHandlerFtpImpl</code> handles send files to via ftp for integration exports.
 *
 * @author msiddiqui
 */
public class IntegrationExportHandlerFtpImpl implements IntegrationExportHandler {

	private IntegrationFileService integrationFileService;

	private SecuritySecretService securitySecretService;


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public List<IntegrationExportFileResult> send(IntegrationExport export, Map<String, String> context) {
		List<IntegrationExportFileResult> results = new ArrayList<>();

		/*
		 * Get the destination associated with this export. We get the first element from the list, and there should
		 * never be more than one destination associated with an IntegrationExport if the export type is of FTP.
		 */
		IntegrationExportIntegrationExportDestination destinationM2m = CollectionUtils.getOnlyElement(export.getLinkedDestinationList());

		if (destinationM2m != null && destinationM2m.getDestinationSubType().getDestinationType().getIntegrationExportDestinationTypeName() == ExportDestinationTypes.FTP) {
			IntegrationExportDestination integrationExportDestination = destinationM2m.getReferenceTwo();

			String url = integrationExportDestination.getDestinationValue();
			FtpProtocols ftpProtocol = FtpProtocols.valueOf(destinationM2m.getDestinationSubType().getIntegrationExportDestinationSubTypeName().name());
			Boolean activeFtp = integrationExportDestination.getActiveFtp();

			String password = null;
			if (integrationExportDestination.getDestinationPasswordSecuritySecret() != null) {
				password = getSecuritySecretService().decryptSecuritySecret(integrationExportDestination.getDestinationPasswordSecuritySecret()).getSecretString();
			}

			FtpConfig ftpConfig = new FtpConfig(url, ftpProtocol, integrationExportDestination.getDestinationUserName(), password);
			ftpConfig.setActiveFtp(activeFtp != null && activeFtp);
			if (integrationExportDestination.getDestinationSshPrivateKeySecuritySecret() != null) {
				ftpConfig.setSshPrivateKey(getSecuritySecretService().decryptSecuritySecret(integrationExportDestination.getDestinationSshPrivateKeySecuritySecret()).getSecretString());
			}

			for (IntegrationFile file : export.getIntegrationFileList()) {
				IntegrationExportFileResult result = new IntegrationExportFileResult();
				result.setFile(file);
				results.add(result);

				File encryptedFile = null;
				try {
					String fullFilePath = getIntegrationFileService().getIntegrationFileAbsolutePath(file);
					File unencryptedFile = new File(fullFilePath);

					StringBuilder filenameWithoutTimestamp = new StringBuilder();
					if (!StringUtils.isEmpty(export.getFilenamePrefix())) {
						filenameWithoutTimestamp.append(export.getFilenamePrefix());
					}
					filenameWithoutTimestamp.append(FileUtils.getFileName(IntegrationFileUtils.removeTimeStamp(file.getFileNameWithPath())));
					File fileToSend = unencryptedFile;

					if (integrationExportDestination.getPgpPublicKey() != null) {
						encryptedFile = encryptOutgoingFile(unencryptedFile, integrationExportDestination.getPgpPublicKey(), integrationExportDestination.isPgpArmor(), integrationExportDestination.isPgpIntegrityCheck());
						filenameWithoutTimestamp.append(".pgp");
						fileToSend = encryptedFile;
					}
					context.put(EXPORT_HANDLER_MESSAGE, "File sent with name: " + filenameWithoutTimestamp.toString());
					FileWrapper fileWrapper = new FileWrapper(fileToSend, filenameWithoutTimestamp.toString(), false);
					FtpUtils.put(ftpConfig, integrationExportDestination.getDestinationText(), fileWrapper);

					result.setStatus(IntegrationExportFileStatusNames.PROCESSED);
				}
				catch (Exception e) {
					LogUtils.error(this.getClass(), "Failed to send FTP tp:\n " + ftpConfig + "\n\n", e);
					result.setStatus(IntegrationExportFileStatusNames.FAILED);
					result.setException(e);
				}
				finally {
					if ((encryptedFile != null) && encryptedFile.exists()) {
						FileUtils.delete(encryptedFile);
					}
				}
			}
		}

		return results;
	}


	private File encryptOutgoingFile(File outgoingFile, String pgpPublicKey, boolean pgpArmor, boolean pgpIntegrityCheck) {
		try {
			File pgpEncryptedFile = File.createTempFile(outgoingFile.getName(), ".pgp");
			PGPFileUtils.encrypt(outgoingFile.getAbsolutePath(), pgpEncryptedFile.getAbsolutePath(), pgpPublicKey, pgpArmor, pgpIntegrityCheck);
			return pgpEncryptedFile;
		}
		catch (IOException e) {
			throw new RuntimeException("Failed to encrypt file [" + outgoingFile.getAbsolutePath() + "].", e);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}
}
