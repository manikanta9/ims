package com.clifton.integration.outgoing.export.server;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.export.messaging.ExportStatuses;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus.IntegrationExportFileStatusNames;
import com.clifton.integration.outgoing.export.IntegrationExportHistory;
import com.clifton.integration.outgoing.export.IntegrationExportIntegrationFile;
import com.clifton.integration.outgoing.export.IntegrationExportService;
import com.clifton.integration.outgoing.export.IntegrationExportStatus;
import com.clifton.integration.outgoing.export.handler.IntegrationExportHandler;
import com.clifton.integration.outgoing.export.search.IntegrationExportIntegrationFileSearchForm;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class IntegrationExportRunnerImpl implements IntegrationExportRunner {


	private IntegrationExportService integrationExportService;

	/**
	 * Will have an entry for EMAIL, FTP and PHONE
	 */
	private Map<String, IntegrationExportHandler> integrationExportHandlerMap;


	@Override
	public IntegrationExportRunnerResult runIntegrationExport(IntegrationExport export) {
		ExportStatuses existingStatus = null;
		// populate the export properties that are needed to run the export
		if (CollectionUtils.isEmpty(export.getLinkedDestinationList()) || CollectionUtils.isEmpty(export.getIntegrationFileList())) {
			IntegrationExport existingExport = getIntegrationExportService().getIntegrationExportPopulated(export.getId());
			if (existingExport != null) {
				existingStatus = existingExport.getStatus().getIntegrationExportStatusName();
				export = existingExport;
			}
		}
		if (export.getRetryCount() == null) {
			export.setRetryCount(0);
		}
		ValidationUtils.assertNotEmpty(export.getLinkedDestinationList(), "No destination are specified for the current export.");
		IntegrationExportHandler exportHandler = getIntegrationExportHandlerMap().get(export.getLinkedDestinationList().get(0).getReferenceTwo().getType().getName());

		return doRunAttempt(export, existingStatus, export.getRetryCount(), exportHandler);
	}


	private IntegrationExportRunnerResult doRunAttempt(IntegrationExport export, ExportStatuses existingStatus, int retryAttempt, IntegrationExportHandler exportHandler) {
		String smtpHostName = export.getServerHostname();
		Integer smtpHostPort = export.getServerPort();
		String awsS3Bucket = export.getAwsS3Bucket();
		String awsS3Region = export.getAwsS3Region();
		String awsS3AccessKeyId = export.getAwsS3AccessKeyId();
		String awsS3SecretKey = export.getAwsS3SecretKey();
		String awsS3EndpointOverride = export.getAwsS3EndpointOverride();

		//Fully populate the export
		export = getIntegrationExportService().getIntegrationExportPopulated(export.getId());
		export.setServerHostname(smtpHostName);
		export.setServerPort(smtpHostPort);
		export.setAwsS3Bucket(awsS3Bucket);
		export.setAwsS3Region(awsS3Region);
		export.setAwsS3AccessKeyId(awsS3AccessKeyId);
		export.setAwsS3SecretKey(awsS3SecretKey);
		export.setAwsS3EndpointOverride(awsS3EndpointOverride);

		StringBuilder completedMsgBuilder = new StringBuilder();
		if (retryAttempt > 0) {
			completedMsgBuilder.append("Retry Attempt: ").append(retryAttempt).append("\n");
		}

		IntegrationExportHistory exportRunHistory = createAndInitializeExportHistory(export, retryAttempt);
		List<IntegrationExportIntegrationFile> exportFiles = getIntegrationExportFileList(export);

		List<IntegrationExportFileResult> results;
		boolean errorOccurred = false;
		try {
			Map<String, String> exportHandlerContextMap = new HashMap<>();
			//Send the file(s)
			results = exportHandler.send(export, exportHandlerContextMap);
			if (!CollectionUtils.isEmpty(exportHandlerContextMap) && !StringUtils.isEmpty(exportHandlerContextMap.get(IntegrationExportHandler.EXPORT_HANDLER_MESSAGE))) {
				completedMsgBuilder.append(exportHandlerContextMap.get(IntegrationExportHandler.EXPORT_HANDLER_MESSAGE));
				completedMsgBuilder.append("\n");
			}
			List<IntegrationFile> failedFiles = getFilesWithStatus(results, IntegrationExportFileStatusNames.FAILED);
			if (CollectionUtils.isEmpty(failedFiles)) {
				completedMsgBuilder.append("Completed - no failures.");
			}
			else {
				errorOccurred = true;
				completedMsgBuilder.append("Export run had [").append(failedFiles.size()).append("] failures. See file results for more details");
			}

			updateIntegrationFileStatuses(exportFiles, results);
		}
		catch (Throwable e) {
			//TODO figure out how to deal with these empty text/subjects
			LogUtils.warn(getClass(), "Error running Export for destination " + ((!StringUtils.isEmpty(export.getMessageSubject())) ? export.getMessageSubject() : "FAX/FTP"), e);

			//If an exception is caught here, it likely means the whole export failed.
			errorOccurred = true;
			completedMsgBuilder.append(formatErrorMessage(e));
			markAllIntegrationFilesAsFailed(exportFiles);
		}

		IntegrationExportStatus exportStatus = getCompletionStatus(existingStatus, !errorOccurred);
		updateCompletedExportAndExportHistory(export, exportRunHistory, exportStatus, completedMsgBuilder.toString());

		return new IntegrationExportRunnerResult(export, exportStatus, exportRunHistory);
	}


	private Map<IntegrationFile, IntegrationExportIntegrationFile> getFileExportMap(List<IntegrationExportIntegrationFile> exportFileLinks) {
		return BeanUtils.getBeanMap(exportFileLinks, IntegrationExportIntegrationFile::getReferenceTwo);
	}


	private List<IntegrationExportIntegrationFile> getIntegrationExportFileList(IntegrationExport export) {
		IntegrationExportIntegrationFileSearchForm searchForm = new IntegrationExportIntegrationFileSearchForm();
		searchForm.setExportId(export.getId());
		return getIntegrationExportService().getIntegrationExportIntegrationFileList(searchForm);
	}


	private IntegrationExportHistory createAndInitializeExportHistory(IntegrationExport export, int retryAttempt) {
		// set the definition status and history to received
		IntegrationExportStatus exportRunStatus = getIntegrationExportService().getIntegrationExportStatusByName(ExportStatuses.RECEIVED.name());

		IntegrationExportHistory exportRunHistory = new IntegrationExportHistory();

		//Save the history
		exportRunHistory.setExport(export);
		exportRunHistory.setExportStatus(exportRunStatus);
		exportRunHistory.setStartDate(new Date());

		exportRunHistory.setDescription(exportRunStatus.getLabel());

		// set the retry number on the history
		exportRunHistory.setRetryAttemptNumber((short) retryAttempt);

		exportRunHistory = getIntegrationExportService().saveIntegrationExportHistory(exportRunHistory);

		return exportRunHistory;
	}


	private void updateCompletedExportAndExportHistory(IntegrationExport export, IntegrationExportHistory runHistory, IntegrationExportStatus status, String description) {
		Date completionDate = new Date();

		export.setStatus(status);
		export.setSentDate(completionDate);
		getIntegrationExportService().saveIntegrationExport(export);

		runHistory.setExportStatus(status);
		runHistory.setEndDate(completionDate);
		runHistory.setDescription(StringUtils.formatStringUpToNCharsWithDots(description, DataTypes.DESCRIPTION_LONG.getLength(), true));
		getIntegrationExportService().saveIntegrationExportHistory(runHistory);
	}


	private void updateIntegrationFileStatuses(List<IntegrationExportIntegrationFile> exportFileLinks, List<IntegrationExportFileResult> results) {

		//If the list of results is empty, then we just consider everything as PROCESSED. An exception should have been thrown if there was actually an error.
		if (CollectionUtils.isEmpty(results)) {
			updateAllIntegrationFiles(exportFileLinks, IntegrationExportFileStatusNames.PROCESSED, null);
		}
		else {
			Map<IntegrationFile, IntegrationExportIntegrationFile> exportFilesLeftToUpdate = getFileExportMap(exportFileLinks);

			//Match the status of each result to an IntegrationExportIntegrationFile. Set the description to the error if there was one.
			for (IntegrationExportFileResult result : CollectionUtils.getIterable(results)) {
				IntegrationExportIntegrationFile exportFile = exportFilesLeftToUpdate.remove(result.getFile());
				exportFile.setStatus(getExportFileStatusByName(result.getStatus()));

				if (result.getException() != null) {
					exportFile.setStatusDescription(formatErrorMessage(result.getException()));
				}

				getIntegrationExportService().saveIntegrationExportIntegrationFile(exportFile);
			}

			//Set the status for any remaining files to FAILED since no status was returned.
			markAllIntegrationFilesAsFailed(new ArrayList<>(CollectionUtils.getValues(exportFilesLeftToUpdate)));
		}
	}


	private void markAllIntegrationFilesAsFailed(List<IntegrationExportIntegrationFile> exportFileLinks) {
		updateAllIntegrationFiles(exportFileLinks, IntegrationExportFileStatusNames.FAILED, "The file failed to send. Check the associated Export Run History for any errors.");
	}


	private void updateAllIntegrationFiles(List<IntegrationExportIntegrationFile> exportFileLinks, IntegrationExportFileStatusNames status, String description) {
		for (IntegrationExportIntegrationFile exportFile : CollectionUtils.getIterable(exportFileLinks)) {
			exportFile.setStatus(getExportFileStatusByName(status));
			exportFile.setStatusDescription(description);

			getIntegrationExportService().saveIntegrationExportIntegrationFile(exportFile);
		}
	}


	private IntegrationExportFileStatus getExportFileStatusByName(IntegrationExportFileStatusNames name) {
		return getIntegrationExportService().getIntegrationExportFileStatusByName(name);
	}


	private List<IntegrationFile> getFilesWithStatus(List<IntegrationExportFileResult> resultList, IntegrationExportFileStatusNames status) {
		List<IntegrationFile> filesWithStatus = new ArrayList<>();
		for (IntegrationExportFileResult result : CollectionUtils.getIterable(resultList)) {
			if (result.getStatus() == status) {
				filesWithStatus.add(result.getFile());
			}
		}

		return filesWithStatus;
	}


	private IntegrationExportStatus getCompletionStatus(ExportStatuses existingExportStatus, boolean success) {
		ExportStatuses statusResult;

		if (!success) {
			statusResult = ExportStatuses.FAILED;
		}
		else if (existingExportStatus == ExportStatuses.PROCESSED || existingExportStatus == ExportStatuses.REPROCESSED) {
			statusResult = ExportStatuses.REPROCESSED;
		}
		else {
			statusResult = ExportStatuses.PROCESSED;
		}

		return getIntegrationExportService().getIntegrationExportStatusByName(statusResult.name());
	}


	private String formatErrorMessage(Throwable e) {
		StringBuilder msgBuilder = new StringBuilder();
		msgBuilder.append("Error: ").append(e.getMessage());
		msgBuilder.append(System.lineSeparator());
		if (e.getCause() != null) {
			msgBuilder.append("Cause: ");
			msgBuilder.append(e.getCause().getMessage());
			msgBuilder.append(System.lineSeparator());
		}
		msgBuilder.append("Stack Trace: ");
		msgBuilder.append(ExceptionUtils.getFullStackTrace(e));

		return StringUtils.formatStringUpToNCharsWithDots(msgBuilder.toString(), DataTypes.DESCRIPTION_LONG.getLength());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public IntegrationExportService getIntegrationExportService() {
		return this.integrationExportService;
	}


	public void setIntegrationExportService(IntegrationExportService integrationExportService) {
		this.integrationExportService = integrationExportService;
	}


	public Map<String, IntegrationExportHandler> getIntegrationExportHandlerMap() {
		return this.integrationExportHandlerMap;
	}


	public void setIntegrationExportHandlerMap(Map<String, IntegrationExportHandler> integrationExportHandlerMap) {
		this.integrationExportHandlerMap = integrationExportHandlerMap;
	}
}
