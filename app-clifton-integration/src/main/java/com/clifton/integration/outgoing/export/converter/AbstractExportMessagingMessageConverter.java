package com.clifton.integration.outgoing.export.converter;

import com.clifton.core.util.StringUtils;
import com.clifton.export.messaging.ExportDestinationTypes;
import com.clifton.export.messaging.ExportMapKeys;
import com.clifton.export.messaging.message.ExportMessagingDestination;
import com.clifton.export.messaging.message.ExportMessagingMessage;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestination;
import com.clifton.integration.outgoing.export.destination.IntegrationExportDestinationType;
import com.clifton.integration.source.IntegrationSource;

import java.util.Date;


/**
 * Abstract converter for export message object to integration objects.
 * <p>
 * NOTE: This class is stateful and should only be used once.
 *
 * @author mwacker
 */
public abstract class AbstractExportMessagingMessageConverter implements ExportMessagingMessageConverter {


	protected abstract IntegrationExportDestination populateExportMessagingDestination(ExportMessagingDestination exportMessagingDestination, IntegrationExportDestination integrationExportDestination, String subDestinationValue, ExportMapKeys key);


	@Override
	public IntegrationExport convertToExport(ExportMessagingMessage exportMessagingMessage, ExportMessagingDestination exportMessagingDestination, IntegrationExportDestinationType destinationType, IntegrationSource source) {
		IntegrationExport integrationExport = new IntegrationExport();

		integrationExport.setRetryCount(exportMessagingMessage.getRetryCount());
		integrationExport.setRetryDelayInSeconds(exportMessagingMessage.getRetryDelayInSeconds());

		integrationExport.setSourceSystemIdentifier(exportMessagingMessage.getSourceSystemIdentifier());
		integrationExport.setSourceSystemDefinitionName(exportMessagingMessage.getSourceSystemDefinitionName());

		integrationExport.setIntegrationSource(source);
		integrationExport.setDestinationType(destinationType);

		integrationExport.setMessageSubject(getDestinationProperty(exportMessagingDestination, ExportMapKeys.MESSAGE_SUBJECT));
		integrationExport.setMessageText(getDestinationProperty(exportMessagingDestination, ExportMapKeys.MESSAGE_TEXT));

		integrationExport.setArchiveStrategyName(exportMessagingMessage.getArchiveStrategyName());

		if (destinationType.getIntegrationExportDestinationTypeName() == ExportDestinationTypes.EMAIL || destinationType.getIntegrationExportDestinationTypeName() == ExportDestinationTypes.BY_ROW_EMAIL) {
			String smtpHostName = getDestinationProperty(exportMessagingDestination, ExportMapKeys.SMTP_HOSTNAME);
			String smtpHostPort = getDestinationProperty(exportMessagingDestination, ExportMapKeys.SMTP_PORT);
			if (!StringUtils.isEmpty(smtpHostName) && !StringUtils.isEmpty(smtpHostPort)) {
				integrationExport.setServerHostname(smtpHostName);
				integrationExport.setServerPort(Integer.valueOf(smtpHostPort));
			}
		}

		if (destinationType.getIntegrationExportDestinationTypeName() == ExportDestinationTypes.AWS_S3) {
			String awsS3Bucket = getDestinationProperty(exportMessagingDestination, ExportMapKeys.AWS_S3_BUCKET);
			String awsS3Region = getDestinationProperty(exportMessagingDestination, ExportMapKeys.AWS_S3_REGION);
			String awsS3AccessKeyId = getDestinationProperty(exportMessagingDestination, ExportMapKeys.AWS_S3_ACCESS_KEY_ID);
			String awsS3SecretKey = getDestinationProperty(exportMessagingDestination, ExportMapKeys.AWS_S3_SECRET_KEY);
			String awsS3EndpointOverride = getDestinationProperty(exportMessagingDestination, ExportMapKeys.AWS_S3_ENDPOINT_OVERRIDE);
			if (!StringUtils.isEmpty(awsS3Bucket) && !StringUtils.isEmpty(awsS3Region)) {
				integrationExport.setMessageSubject(awsS3Bucket);
				integrationExport.setMessageText(awsS3Region);

				integrationExport.setAwsS3Bucket(awsS3Bucket);
				integrationExport.setAwsS3Region(awsS3Region);
				integrationExport.setAwsS3SecretKey(awsS3SecretKey);
				integrationExport.setAwsS3AccessKeyId(awsS3AccessKeyId);
			}
			if(!StringUtils.isEmpty(awsS3EndpointOverride)){
				integrationExport.setAwsS3EndpointOverride(awsS3EndpointOverride);
			}
		}

		integrationExport.setReceivedDate(new Date());
		integrationExport.setFilenamePrefix(exportMessagingDestination.getPropertyList().get(ExportMapKeys.FILENAME_PREFIX));

		return integrationExport;
	}


	@Override
	public IntegrationExportDestination createOrUpdateIntegrationExportDestination(IntegrationExportDestination integrationExportDestination, ExportMessagingDestination exportMessagingDestination, IntegrationExportDestinationType destinationType, String subDestinationValue, ExportMapKeys key) {
		if (integrationExportDestination == null) {
			integrationExportDestination = new IntegrationExportDestination();
		}
		integrationExportDestination.setDestinationText(getDestinationText(exportMessagingDestination, key));
		integrationExportDestination.setDestinationValue(getDestinationValue(exportMessagingDestination, subDestinationValue, key));
		integrationExportDestination.setType(destinationType);

		return populateExportMessagingDestination(exportMessagingDestination, integrationExportDestination, subDestinationValue, key);
	}


	@Override
	public String getDestinationValue(ExportMessagingDestination exportMessagingDestination, String subDestinationValue, ExportMapKeys key) {
		return getDestinationProperty(exportMessagingDestination, key);
	}


	public String getDestinationText(ExportMessagingDestination exportMessagingDestination, ExportMapKeys key) {
		String textValueKey = key.getExportTextValueKey();
		if (!StringUtils.isEmpty(textValueKey)) {
			String destinationKeyText = getDestinationProperty(exportMessagingDestination, ExportMapKeys.valueOf(textValueKey));
			if (!StringUtils.isEmpty(destinationKeyText)) {
				return destinationKeyText;
			}
		}
		return null;
	}


	protected String getDestinationProperty(ExportMessagingDestination destination, ExportMapKeys propertyName) {
		return getDestinationProperty(destination, propertyName, String.class);
	}


	@SuppressWarnings("unchecked")
	protected <T> T getDestinationProperty(ExportMessagingDestination destination, ExportMapKeys propertyName, Class<T> resultType) {
		String value = destination.getPropertyList().get(propertyName);
		Object result = value;
		if (resultType == Boolean.class) {
			result = Boolean.valueOf(value);
		}
		return (T) result;
	}
}
