package com.clifton.integration.outgoing.export.server;

import com.clifton.core.messaging.jms.asynchronous.AsynchronousMessageHandler;
import com.clifton.export.messaging.ExportStatuses;
import com.clifton.export.messaging.message.ExportMessagingStatusMessage;
import org.springframework.stereotype.Component;


/**
 * @author mwacker
 */
@Component
public class IntegrationExportStatusHandlerImpl implements IntegrationExportStatusHandler {

	private AsynchronousMessageHandler integrationExportServerAsynchronousMessageHandler;


	@Override
	public void sendIntegrationExportStatus(String sourceSystemIdentifier, String sourceSystemName, ExportStatuses status, String message) {
		ExportMessagingStatusMessage statusMessage = new ExportMessagingStatusMessage();
		statusMessage.setSourceSystemIdentifier(sourceSystemIdentifier);
		statusMessage.setStatus(status);
		statusMessage.setMessage(message);
		statusMessage.setSourceSystemName(sourceSystemName);
		statusMessage.getProperties().put("sourceSystemName", sourceSystemName);
		getIntegrationExportServerAsynchronousMessageHandler().send(statusMessage);
	}


	public AsynchronousMessageHandler getIntegrationExportServerAsynchronousMessageHandler() {
		return this.integrationExportServerAsynchronousMessageHandler;
	}


	public void setIntegrationExportServerAsynchronousMessageHandler(AsynchronousMessageHandler integrationExportServerAsynchronousMessageHandler) {
		this.integrationExportServerAsynchronousMessageHandler = integrationExportServerAsynchronousMessageHandler;
	}
}
