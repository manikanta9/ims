package com.clifton.integration.outgoing.export.handler;


import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.export.messaging.ExportDestinationSubTypes;
import com.clifton.fax.FaxMessage;
import com.clifton.fax.FaxMessageService;
import com.clifton.integration.file.IntegrationFile;
import com.clifton.integration.file.IntegrationFileService;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;
import com.clifton.integration.outgoing.export.IntegrationExportFileStatus;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;
import org.springframework.stereotype.Component;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationExportHandlerFaxImpl</code> handles send files to via fax for integration exports.
 *
 * @author theodorez
 */

@Component
public class IntegrationExportHandlerFaxImpl implements IntegrationExportHandler {

	private IntegrationFileService integrationFileService;
	private FaxMessageService faxMessageService;

	private String defaultSenderName;
	private String defaultSenderEmail;
	private String defaultSenderNumber;

	/////////////////////////////////////////////////////////////////////////////////////


	@Override
	public List<IntegrationExportFileResult> send(IntegrationExport export, Map<String, String> context) {
		FaxMessage faxMessage = new FaxMessage();
		faxMessage.setAttachments(getAttachments(export.getIntegrationFileList()));
		faxMessage.setSenderName(getSenderName(export));
		faxMessage.setSenderEmail(getSenderEmailAddress(export));
		faxMessage.setSenderNumber(getSenderNumber(export));
		faxMessage.setRecipientName(export.getMessageSubject());
		List<FaxMessage> faxServerResults = sendFax(faxMessage, export.getLinkedDestinationList());

		return processResults(faxServerResults, export.getIntegrationFileList());
	}


	/////////////////////////////////////////////////////////////////////////////////////


	private List<FaxMessage> sendFax(FaxMessage faxMessage, List<IntegrationExportIntegrationExportDestination> destinations) {
		List<FaxMessage> faxResults = new ArrayList<>();
		for (IntegrationExportIntegrationExportDestination destination : destinations) {
			if (destination.getDestinationSubType().getIntegrationExportDestinationSubTypeName() == ExportDestinationSubTypes.FAX_RECIPIENT) {
				faxMessage.setRecipientName(destination.getReferenceTwo().getDestinationText());
				faxMessage.setRecipientNumber(destination.getReferenceTwo().getDestinationValue());
				faxResults.add(getFaxMessageService().sendFax(faxMessage));
			}
		}
		return faxResults;
	}


	private List<IntegrationExportFileResult> processResults(List<FaxMessage> faxServerResults, List<IntegrationFile> files) {
		IntegrationExportFileStatus.IntegrationExportFileStatusNames statusName = IntegrationExportFileStatus.IntegrationExportFileStatusNames.PROCESSED;
		Exception exception = null;
		for (FaxMessage faxResult : faxServerResults) {
			if (faxResult.getException() != null && StringUtils.isEmpty(faxResult.getFaxServerIdentifier())) {
				statusName = IntegrationExportFileStatus.IntegrationExportFileStatusNames.FAILED;
				exception = faxResult.getException();
				break;
			}
		}
		//ALL Files get the same exception and status
		List<IntegrationExportFileResult> results = new ArrayList<>();
		for (IntegrationFile file : files) {
			IntegrationExportFileResult result = new IntegrationExportFileResult();
			result.setFile(file);
			result.setStatus(statusName);
			result.setException(exception);
			results.add(result);
		}
		return results;
	}


	private List<FileWrapper> getAttachments(List<IntegrationFile> files) {
		List<FileWrapper> attachments = new ArrayList<>();
		for (IntegrationFile file : CollectionUtils.getIterable(files)) {
			String fullFilePath = this.getIntegrationFileService().getIntegrationFileAbsolutePath(file);
			File fileSystemFile = new File(fullFilePath);
			attachments.add(new FileWrapper(fileSystemFile, FileUtils.getFileName(file.getFileNameWithPath()), true));
		}
		return attachments;
	}


	private String getSenderEmailAddress(IntegrationExport export) {
		String senderEmail = IntegrationExportHandlerUtils.getDestinationSubTypeValue(ExportDestinationSubTypes.FAX_SENDER, export);
		return senderEmail != null ? senderEmail : getDefaultSenderEmail();
	}


	private String getSenderName(IntegrationExport export) {
		String senderName = IntegrationExportHandlerUtils.getDestinationSubTypeText(ExportDestinationSubTypes.FAX_SENDER, export);
		return senderName != null ? senderName : getDefaultSenderName();
	}


	private String getSenderNumber(IntegrationExport export) {
		String senderNumber = IntegrationExportHandlerUtils.getDestinationSubTypeText(ExportDestinationSubTypes.FAX_SENDER, export);
		return senderNumber != null ? senderNumber : getDefaultSenderNumber();
	}


	/////////////////////////////////////////////////////////////////////////////////////


	public IntegrationFileService getIntegrationFileService() {
		return this.integrationFileService;
	}


	public void setIntegrationFileService(IntegrationFileService integrationFileService) {
		this.integrationFileService = integrationFileService;
	}


	public FaxMessageService getFaxMessageService() {
		return this.faxMessageService;
	}


	public void setFaxMessageService(FaxMessageService faxMessageService) {
		this.faxMessageService = faxMessageService;
	}


	public String getDefaultSenderName() {
		return this.defaultSenderName;
	}


	public void setDefaultSenderName(String defaultSenderName) {
		this.defaultSenderName = defaultSenderName;
	}


	public String getDefaultSenderNumber() {
		return this.defaultSenderNumber;
	}


	public void setDefaultSenderNumber(String defaultSenderNumber) {
		this.defaultSenderNumber = defaultSenderNumber;
	}


	public String getDefaultSenderEmail() {
		return this.defaultSenderEmail;
	}


	public void setDefaultSenderEmail(String defaultSenderEmail) {
		this.defaultSenderEmail = defaultSenderEmail;
	}
}
