package com.clifton.integration.outgoing.export.handler;

import com.clifton.export.messaging.ExportDestinationSubTypes;
import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.destination.IntegrationExportIntegrationExportDestination;

import java.util.ArrayList;
import java.util.List;


public class IntegrationExportHandlerUtils {

	/**
	 * Searches the linked destinations of the export and returns the value of the first instance
	 * where the destination subtype matches the specified subtype
	 */
	public static String getDestinationSubTypeValue(ExportDestinationSubTypes subType, IntegrationExport export) {
		for (IntegrationExportIntegrationExportDestination destination : export.getLinkedDestinationList()) {
			if (destination.getDestinationSubType().getIntegrationExportDestinationSubTypeName() == subType) {
				return destination.getReferenceTwo().getDestinationValue();
			}
		}
		return null;
	}


	/**
	 * Searches the linked destinations of the export and returns a list of all values
	 * where the destination subtype matches the specified subtype
	 */
	public static List<String> getDestinationSubTypeValueList(ExportDestinationSubTypes subType, IntegrationExport export) {
		List<String> valueList = new ArrayList<>();
		for (IntegrationExportIntegrationExportDestination destination : export.getLinkedDestinationList()) {
			if (destination.getDestinationSubType().getIntegrationExportDestinationSubTypeName() == subType) {
				valueList.add(destination.getReferenceTwo().getDestinationValue());
			}
		}
		return valueList;
	}


	/**
	 * Searches the linked destinations of the export and returns the text of the first instance
	 * where the destination subtype matches the specified subtype
	 */
	public static String getDestinationSubTypeText(ExportDestinationSubTypes subType, IntegrationExport export) {
		for (IntegrationExportIntegrationExportDestination destination : export.getLinkedDestinationList()) {
			if (destination.getDestinationSubType().getIntegrationExportDestinationSubTypeName() == subType) {
				return destination.getReferenceTwo().getDestinationText();
			}
		}
		return null;
	}
}
