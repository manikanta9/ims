package com.clifton.integration.outgoing.export.handler;


import com.clifton.integration.outgoing.export.IntegrationExport;
import com.clifton.integration.outgoing.export.IntegrationExportFileResult;

import java.util.List;
import java.util.Map;


/**
 * The <code>IntegrationExportHandler</code> defines the method used for send to external parties.
 *
 * @author mwacker
 */
public interface IntegrationExportHandler {

	public static final String EXPORT_HANDLER_MESSAGE = "MESSAGE";


	public List<IntegrationExportFileResult> send(IntegrationExport export, Map<String, String> context);
}
