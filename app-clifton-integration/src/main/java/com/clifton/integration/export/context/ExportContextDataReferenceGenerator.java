package com.clifton.integration.export.context;

import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.core.validation.ValidationAware;
import com.clifton.export.context.ExportContentContextMapKeys;
import com.clifton.export.context.ExportContextGenerator;
import com.clifton.export.definition.ExportDefinitionContent;

import java.util.EnumMap;
import java.util.HashMap;
import java.util.Map;


/**
 * If the content that was generated for the given ExportDefinitionContent is a String,
 * it will be placed into the specified reference variable to be used in Freemarker
 *
 * @author theodorez
 */
public class ExportContextDataReferenceGenerator implements ExportContextGenerator, ValidationAware {

	private String referenceVariableName;

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Override
	public Map<ExportContentContextMapKeys, Object> getContext(ExportDefinitionContent content, Object generatedData) {
		Map<ExportContentContextMapKeys, Object> map = new EnumMap<>(ExportContentContextMapKeys.class);
		if (generatedData instanceof String) {
			Map<String, Object> freemarkerMap = new HashMap<>();
			freemarkerMap.put(getReferenceVariableName(), generatedData);
			map.put(ExportContentContextMapKeys.EXPORT_CONTENT_REFERENCE_MAP, freemarkerMap);
		}
		else {
			throw new ValidationException("Cannot reference export data that is a File. ExportDefinitionContent: " + content.getContentSystemBean().getLabel());
		}
		return map;
	}


	@Override
	public String getContextLabel() {
		return "Export Data Reference";
	}


	@Override
	public void validate() {
		ValidationUtils.assertNotEmpty(getReferenceVariableName(), "Reference Variable Name cannot be empty");
	}

	////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////////


	public String getReferenceVariableName() {
		return this.referenceVariableName;
	}


	public void setReferenceVariableName(String referenceVariableName) {
		this.referenceVariableName = referenceVariableName;
	}
}
