package com.clifton.integration.export.content.transformation;


import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.dataaccess.file.DataTableFileConfig;
import com.clifton.core.dataaccess.file.DataTableToCsvFileConverter;
import com.clifton.core.dataaccess.file.FileUtils;
import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.logging.LogUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.export.content.transformation.ExportTransformationToFileGenerator;
import com.clifton.export.definition.ExportDefinitionContent;
import com.clifton.integration.incoming.transformation.IntegrationTransformation;
import com.clifton.integration.incoming.transformation.IntegrationTransformationFile;
import com.clifton.integration.incoming.transformation.IntegrationTransformationService;
import com.clifton.integration.incoming.transformation.parameter.IntegrationTransformationParameterHandler;
import com.clifton.integration.incoming.transformation.parameter.IntegrationTransformationParameterService;
import com.clifton.integration.incoming.transformer.FileTransformer;
import com.clifton.integration.incoming.transformer.FileTransformerCommand;
import com.clifton.security.secret.SecuritySecretService;

import java.io.File;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


/**
 * @author TerryS
 */
public class ExportTransformationDataTableToFileUsingPentahoGenerator extends ExportTransformationToFileGenerator<DataTable> {

	private Integer transformationId;

	private FileTransformer fileTransformer;
	private IntegrationTransformationService integrationTransformationService;
	private IntegrationTransformationParameterService integrationTransformationParameterService;
	private SecuritySecretService securitySecretService;
	private IntegrationTransformationParameterHandler integrationTransformationParameterHandler;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public FileWrapper generateTransformation(DataTable dataTable, Integer runId, ExportDefinitionContent content, Map<String, Object> context) {
		FileWrapper fileWrapper = null;

		try {
			fileWrapper = convertDataTable(dataTable);

			List<IntegrationTransformationFile> integrationTransformationFileList = getIntegrationTransformationFileList();
			IntegrationTransformation integrationTransformation = getIntegrationTransformationService().getIntegrationTransformation(getTransformationId());
			Map<String, String> transformationParameters = getIntegrationTransformationParameterHandler().getIntegrationTransformationParameterMap(integrationTransformation);
			FileTransformerCommand command = new FileTransformerCommand(runId, fileWrapper.getFile().toFile(), integrationTransformationFileList, transformationParameters);
			File newFile = getFileTransformer().doTransform(command);
			if (newFile != null) {
				return new FileWrapper(newFile, getOutputFileName(content, runId), true);
			}
		}
		finally {
			if (fileWrapper != null && fileWrapper.isTempFile()) {
				try {
					if (FileUtils.fileExists(fileWrapper.getFile())) {
						FileUtils.delete(fileWrapper.getFile());
					}
				}
				catch (Exception e) {
					LogUtils.error(getClass(), "Failed to delete file [" + fileWrapper.getFile().getPath() + "].", e);
				}
			}
		}

		return fileWrapper;
	}


	@Override
	public Class<DataTable> getInputClass() {
		return DataTable.class;
	}

	////////////////////////////////////////////////////////////////////////////


	private FileWrapper convertDataTable(DataTable dataTable) {
		DataTableToCsvFileConverter csvFileConverter = new DataTableToCsvFileConverter();
		DataTableFileConfig config = new DataTableFileConfig(dataTable);
		File file = csvFileConverter.convert(config);
		return new FileWrapper(file, file.getName(), true);
	}


	private List<IntegrationTransformationFile> getIntegrationTransformationFileList() {
		return CollectionUtils.getStream(getIntegrationTransformationService().getIntegrationTransformationFileListForTransformation(getTransformationId()))
				.sorted(Comparator.comparing(IntegrationTransformationFile::getOrder))
				.collect(Collectors.toList());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public FileTransformer getFileTransformer() {
		return this.fileTransformer;
	}


	public void setFileTransformer(FileTransformer fileTransformer) {
		this.fileTransformer = fileTransformer;
	}


	public IntegrationTransformationService getIntegrationTransformationService() {
		return this.integrationTransformationService;
	}


	public void setIntegrationTransformationService(IntegrationTransformationService integrationTransformationService) {
		this.integrationTransformationService = integrationTransformationService;
	}


	public Integer getTransformationId() {
		return this.transformationId;
	}


	public void setTransformationId(Integer transformationId) {
		this.transformationId = transformationId;
	}


	public IntegrationTransformationParameterService getIntegrationTransformationParameterService() {
		return this.integrationTransformationParameterService;
	}


	public void setIntegrationTransformationParameterService(IntegrationTransformationParameterService integrationTransformationParameterService) {
		this.integrationTransformationParameterService = integrationTransformationParameterService;
	}


	public SecuritySecretService getSecuritySecretService() {
		return this.securitySecretService;
	}


	public void setSecuritySecretService(SecuritySecretService securitySecretService) {
		this.securitySecretService = securitySecretService;
	}


	public IntegrationTransformationParameterHandler getIntegrationTransformationParameterHandler() {
		return this.integrationTransformationParameterHandler;
	}


	public void setIntegrationTransformationParameterHandler(IntegrationTransformationParameterHandler integrationTransformationParameterHandler) {
		this.integrationTransformationParameterHandler = integrationTransformationParameterHandler;
	}
}
