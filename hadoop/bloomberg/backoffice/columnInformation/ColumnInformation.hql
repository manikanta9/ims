USE clifton;

CREATE EXTERNAL TABLE IF NOT EXISTS clifton.ColumnInformation(
	column_header string, data_type string, conversion_statement string, table_name string, create_column_order int, create_column_options string, insert_constraint string, is_insert boolean, is_partition boolean, is_cluster boolean, is_additional boolean
)
ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
STORED AS TEXTFILE
LOCATION '/warehouse/clifton/ColumnInformation'
tblproperties ("skip.header.line.count"="1");

--Create view with appropriate data types because the CSV SERDE only allows String typing
CREATE VIEW IF NOT EXISTS clifton.column_information(
	column_header, data_type, conversion_statement, table_name, create_column_order, create_column_options, insert_constraint, is_insert, is_partition, is_cluster, is_additional
)
AS
SELECT
	column_header,
    data_type,
    IF(conversion_statement = 'NULL', NULL, conversion_statement) AS conversion_statement,
    table_name,
    cast(create_column_order as int) AS create_column_order,
    IF(create_column_options = 'NULL', NULL, create_column_options) AS create_column_options,
    IF(insert_constraint = 'NULL', NULL, insert_constraint) AS insert_constraint,
    cast(is_insert AS boolean) AS is_insert,
    cast(is_partition AS boolean) AS is_partition,
    cast(is_cluster AS boolean) AS is_cluster,
    cast(is_additional AS boolean) AS is_additional
FROM clifton.ColumnInformation