	USE clifton;

	CREATE EXTERNAL TABLE clifton.FileDefinition(FilePrefix string, FileAggregationTable string, FinalTable string)
	ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
	STORED AS TEXTFILE
	LOCATION '/warehouse/clifton/FileDefinition'
	tblproperties ("skip.header.line.count"="1");