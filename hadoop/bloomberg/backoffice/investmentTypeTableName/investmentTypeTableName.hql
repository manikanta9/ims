	USE clifton;

    CREATE EXTERNAL TABLE clifton.InvestmentTypeTableName(InvestmentType string, TableName string)
    ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.OpenCSVSerde'
    STORED AS TEXTFILE
    LOCATION '/warehouse/clifton/InvestmentTypeTableName'
    tblproperties ("skip.header.line.count"="1");