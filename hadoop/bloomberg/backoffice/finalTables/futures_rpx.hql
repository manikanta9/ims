CREATE TABLE IF NOT EXISTS bloomberg.futures_rpx(
TICKER string,
EXCH_CODE string,
ID_BB_GLOBAL string,
UNIQUE_ID_FUT_OPT string,
PARSEKYABLE_DES_SOURCE string,
MARKET_SECTOR_DES string,
SECURITY_TYP string,
SECURITY_TYP2 string,
FUTURES_CATEGORY string,
NAME string,
FEED_SOURCE string,
ID_BB_SEC_NUM_DES string,
ID_BB_UNIQUE string,
FUT_PX_SESSION string,
CRNCY string,
ID_BB_COMPANY int,
PRICING_SOURCE string,
QUOTED_CRNCY string,
LAST_UPDATE string,
LAST_UPDATE_DT date,
OPEN_INT_CHANGE int,
OPEN_INT_DATE date,
OPT_UNDL_PX DECIMAL(28,15),
PRIOR_CLOSE_ASK DECIMAL(28,15),
PRIOR_CLOSE_BID DECIMAL(28,15),
PRIOR_CLOSE_MID DECIMAL(28,15),
PX_ASK DECIMAL(28,15),
PX_BID DECIMAL(28,15),
PX_CLOSE_DT date,
PX_HIGH DECIMAL(28,15),
PX_LAST DECIMAL(28,15),
PX_LAST_ACTUAL DECIMAL(28,15),
PX_LAST_EOD DECIMAL(28,15),
PX_LOW DECIMAL(28,15),
PX_OPEN DECIMAL(28,15),
PX_SCALING_FACTOR DECIMAL(28,15),
PX_SETTLE DECIMAL(28,15),
PX_SETTLE_LAST_DT date,
PX_VOLUME int,
PX_VOLUME_1D int,
PX_YEST_DT date,
PX_YEST_HIGH DECIMAL(28,15),
PX_YEST_LOW DECIMAL(28,15),
SETTLE_DT date,
VOLUME_AVG_100D DECIMAL(28,15),
VOLUME_AVG_10D DECIMAL(28,15),
VOLUME_AVG_30D DECIMAL(28,15),
VOLUME_AVG_3M DECIMAL(28,15),
VOLUME_AVG_6M DECIMAL(28,15),
CONTRACT_VALUE DECIMAL(28,15),
DAILY_LIMIT_DOWN DECIMAL(28,15),
DAILY_LIMIT_UP DECIMAL(28,15),
FUT_AGGTE_OPEN_INT int,
FUT_CNVS_FACTOR DECIMAL(28,15),
FUT_CTD_PX DECIMAL(28,15),
FUT_EXCH_NAME_LONG string,
FUT_NOTICE_FIRST date,
FUT_PX DECIMAL(28,15),
FUT_SEC_HEDGE_ML DECIMAL(28,15),
FUT_SEC_SPEC_ML DECIMAL(28,15),
OPEN_INT int,
OPEN_INT_TOTAL_CALL DECIMAL(28,15),
OPEN_INT_TOTAL_PUT DECIMAL(28,15),
PX_SETTLE_E_MINI_ACTUAL DECIMAL(28,15),
PX_YEST_CLOSE DECIMAL(28,15),
PX_YEST_OPEN DECIMAL(28,15),
PX_YEST_OPEN_INT DECIMAL(28,15),
QUOTE_UNITS string,
STRATEGY_TYP string,
QT_SPEC string,
INTERNAL_IDENTIFIER string,
FILE_NAME string,
TSTAMP TIMESTAMP
)
PARTITIONED BY (FILE_DATE date)
CLUSTERED BY(INTERNAL_IDENTIFIER) INTO 32 BUCKETS
STORED AS ORC
TBLPROPERTIES('orc.compress'='snappy','transactional'='true')