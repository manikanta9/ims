--Can be used to regenerate the creation statement for a given final table after changes are made to the column information

SELECT 'CREATE TABLE IF NOT EXISTS ${database_name}.${final_table_name}(' || n.col_str || ') ' || IF(pt.col_str IS NOT NULL AND LENGTH(pt.col_str) > 0, ' PARTITIONED BY (' || pt.col_str || ') ', '') || IF(i.col_str IS NOT NULL AND LENGTH(i.col_str) > 0, ' CLUSTERED BY(' || i.col_str || ') INTO 32 BUCKETS ', '') || 'STORED AS ORC TBLPROPERTIES(\'orc.compress\'=\'snappy\', \'transactional\'=\'true\')' as create_string
FROM
    (
        SELECT CONCAT_WS(', ', COLLECT_LIST(ci.column_header || ' ' || ci.data_type)) as col_str
        FROM
        (
            SELECT *
            FROM clifton.column_information
            WHERE final_table_name = '${final_table_name}'
            AND create_column_order IS NOT NULL
            AND is_partition IS FALSE
            ORDER BY create_column_order ASC
        ) as ci
    ) as n,
    (
        SELECT CONCAT_WS(', ', COLLECT_LIST(ptci.column_header || ' ' || ptci.data_type)) as col_str
        FROM
        (
            SELECT *
            FROM clifton.column_information
            WHERE final_table_name = '${final_table_name}'
            AND create_column_order IS NOT NULL
            AND is_partition IS TRUE
            ORDER BY create_column_order ASC
        ) as ptci
    ) as pt,
    (
        SELECT CONCAT_WS(', ', COLLECT_LIST(identifiers.column_header)) as col_str
        FROM
        (
            SELECT *
            FROM clifton.column_information
            WHERE final_table_name = '${final_table_name}'
            AND create_column_order IS NOT NULL
            AND is_cluster IS TRUE
            ORDER BY create_column_order ASC
        ) as identifiers
    ) as i