--DROP FUNCTION report.InvestmentGroupExpanded

CREATE FUNCTION report.InvestmentGroupExpanded
	(
	@InvestmentGroupID INT = 1
	, @LevelsDeep INT = 2 
    )
RETURNS TABLE
AS

RETURN


--defaults
--SELECT @InvestmentGroupID=InvestmentGroupID, @LevelsDeep=LevelsDeep 
--FROM dbo.InvestmentGroup WHERE InvestmentGroupName = 'Accounting Reporting';


WITH ReportGroup (
		InvestmentGroupItemID
		, GroupItemName
		, ParentInvestmentGroupItemID
		, ParentGroupItemName
		, ParentItemOrder
		, ItemOrder
		, [Level]
		
		)
AS
(
-- Anchor member definition
    SELECT InvestmentGroupItemID, GroupItemName=p.GroupItemName, InvestmentGroupItemID, ParentGroupItemName = p.GroupItemName, ParentItemOrder = ItemOrder, ItemOrder, 1
    FROM CliftonIMS.dbo.InvestmentGroupItem p
    WHERE InvestmentGroupID = @InvestmentGroupID AND ParentInvestmentGroupItemID IS NULL
    
    UNION ALL

-- Recursive member definition
    SELECT c.InvestmentGroupItemID, GroupItemName=c.GroupItemName, c.ParentInvestmentGroupItemID, ParentGroupItemName = pg.GroupItemName
		,pg.ParentItemOrder
		,c.ItemOrder 
        ,pg.Level + 1
    FROM CliftonIMS.dbo.InvestmentGroupItem c

    INNER JOIN ReportGroup AS pg
        ON c.ParentInvestmentGroupItemID = pg.InvestmentGroupItemID
)
-- Statement that executes the CTE
SELECT g.*
	,gii.InvestmentInstrumentID
FROM ReportGroup g
INNER JOIN CliftonIMS.dbo.InvestmentGroupItemInstrument gii ON g.InvestmentGroupItemID = gii.InvestmentGroupItemID
WHERE Level <= @LevelsDeep
