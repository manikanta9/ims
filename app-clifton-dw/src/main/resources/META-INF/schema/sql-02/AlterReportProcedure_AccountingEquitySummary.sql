
ALTER PROC Report.AccountingEquitySummary
 
		@ClientInvestmentAccountID INT = 397
		,@ReportingDate DATETIME = '12/31/09'   
		,@StartDate DATETIME = '10/1/09' 

AS 


DECLARE @LastQuarterEndDate DATE
	, @LastYearEndDate DATE
	, @ThisYearStartDate DATE
	, @LastQuarterEndPortfolioValue DECIMAL(28,2)
	, @LastYearEndPortfolioValue  DECIMAL(28,2)
	

SELECT 
	@LastQuarterEndDate = LastQuarterEndDate
		, @LastYearEndDate = LastYearEndDate
		, @ThisYearStartDate = YearStartDate
FROM Datawarehouse.GetQuarterEndDates(@ReportingDate) 




--Objective: pull position and cash data without a dependency on physical FactPosition data
-- to do this the Position Summary view is used which only looks at positions and not cash
-- all cash activity prior to the desired period needs to be added to arrive at the correct beginning balance
SELECT 
	@LastQuarterEndPortfolioValue = SUM(CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE p.MarketValueBase END )
FROM Datawarehouse.PositionSummary(@ClientInvestmentAccountID, @LastQuarterEndDate) p
	INNER JOIN dbo.InvestmentSecurity s ON p.InvestmentSecurityID = s.InvestmentSecurityID
	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
	INNER JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID

 
SELECT @LastQuarterEndPortfolioValue = ISNULL(@LastQuarterEndPortfolioValue, 0) + SUM(BaseDebitCredit)
FROM dbo.FactTransaction fact
	INNER JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
		AND fact.TransactionDate <= @LastQuarterEndDate  
		--add cash portion  - excluding currencies which are included in the position summary above
		AND aa.IsCash = 1 AND aa.IsPosition = 0
--GROUP BY ClientInvestmentAccountID, CurrencySecurityID, HoldingInvestmentAccountID, fact.AccountingAccountID


SELECT 
	@LastYearEndPortfolioValue = SUM(CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE p.MarketValueBase END )
FROM Datawarehouse.PositionSummary(@ClientInvestmentAccountID, @LastYearEndDate) p
	INNER JOIN dbo.InvestmentSecurity s ON p.InvestmentSecurityID = s.InvestmentSecurityID
	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
	INNER JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID

SELECT @LastYearEndPortfolioValue = ISNULL(@LastYearEndPortfolioValue,0) + SUM(BaseDebitCredit)
FROM dbo.FactTransaction fact
	INNER JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
		AND fact.TransactionDate <= @LastYearEndDate  
		--add cash portion  - excluding currencies which are included in the position summary above
		AND aa.IsCash = 1 AND aa.IsPosition = 0
		
--GROUP BY ClientInvestmentAccountID, CurrencySecurityID, HoldingInvestmentAccountID, fact.AccountingAccountID



DECLARE @EquitySummary TABLE (AccountingAccountID INT
	, BaseGainLossITD DECIMAL(28,2)
	, BaseGainLossPTD DECIMAL(28,2)
	, BaseGainLossYTD DECIMAL(28,2)
	)

--Prior Period Appreciation
INSERT INTO @EquitySummary
SELECT AccountingAccountID = 302, SUM(BaseGainLossITD), SUM(BaseGainLossPTD), SUM(BaseGainLossYTD)
FROM (
	SELECT AccountingAccountID = 302
		, BaseGainLossITD = 0
		, BaseGainLossPTD = -@LastQuarterEndPortfolioValue
		, BaseGainLossYTD = -@LastYearEndPortfolioValue
 
	 
		
	) BE --beginning equity


INSERT INTO @EquitySummary


		--Portfolio Appreciation
		SELECT 344, -ISNULL(SUM(t.GainLossSinceInceptionBase), 0), -ISNULL(SUM(t.GainLossSinceLastPeriodBase), 0), -ISNULL(SUM(t.GainLossSinceLastYearEndBase), 0)
		--FROM dbo.FactIncomeStatement t
		--WHERE ClientAccountID = @ClientInvestmentAccountID AND SnapshotDate = @ReportingDate 
		FROM dbo.FactIncomeStatement t
		WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
			AND SnapshotDate = @ReportingDate
		

UNION 

--Equity Activity
	SELECT aa.AccountingAccountID
		, SUM(t.BaseDebitCredit)  --ITD
		, SUM(Case when OriginalTransactionDate >= @StartDate THEN t.BaseDebitCredit ELSE 0 END)  --PTD
		, SUM(case when OriginalTransactionDate > @ThisYearStartDate then t.BaseDebitCredit ELSE 0 END )  --YTD
	FROM dbo.AccountingAccount aa
		INNER JOIN FactTransaction t ON aa.AccountingAccountID = t.AccountingAccountID
	WHERE aa.AccountingAccountTypeID = 3 AND t.ClientInvestmentAccountID = @ClientInvestmentAccountID
		AND OriginalTransactionDate <= @ReportingDate
	GROUP BY 
		aa.AccountingAccountID

--Output Report Details
SELECT aa.AccountingAccountID, AccountingAccountDescription
	, BaseAppreciationITD = -ISNULL(BaseGainLossITD, 0)
	, BaseAppreciationPTD = -ISNULL(BaseGainLossPTD, 0)
	, BaseAppreciationYTD = -ISNULL(BaseGainLossYTD, 0)
	, InceptionDate 
	, BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 

FROM dbo.AccountingAccount aa
	INNER JOIN @EquitySummary es ON aa.AccountingAccountID = es.AccountingAccountID
	
	INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = @ClientInvestmentAccountID
	INNER JOIN dbo.BusinessClient bc ON a.BusinessClientID = bc.BusinessClientID
		LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID

WHERE aa.AccountingAccountTypeID = 3
	--AND aa.AccountingAccountID NOT IN (300,310,320)

ORDER BY AccountingAccountID


 
 
 
