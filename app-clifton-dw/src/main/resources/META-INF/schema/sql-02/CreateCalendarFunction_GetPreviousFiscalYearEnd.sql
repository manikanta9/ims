/****** Returns the last day of the previous fiscal year using the annual start month (where 1=January) to determine when the year starts ******/
CREATE FUNCTION [calendar].[GetPreviousFiscalYearEnd](@FromDate DATE, @FiscalYearStartMonth INT)
	RETURNS DATE AS
BEGIN
	IF (MONTH(@FromDate) < @FiscalYearStartMonth)
	BEGIN
		RETURN DATEADD(DAY, -1, CONVERT(DATE, CAST(@FiscalYearStartMonth AS VARCHAR) + '/1/' + STR(Year(@FromDate)-1)))
	END
	
	RETURN DATEADD(DAY, -1, CONVERT(DATE, CAST(@FiscalYearStartMonth AS VARCHAR) + '/1/' + STR(Year(@FromDate))))
END
