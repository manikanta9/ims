--EXEC [Report].[GetGainLossSummaryForGroup] @ClientInvestmentAccountID = 2626,@ReportDate='1/15/2014',@PurposeName='Clifton Sub-Account',@ByClientAccount=1

ALTER PROC [Report].[GetGainLossSummaryForGroup] @ClientInvestmentAccountID AS INT, @ReportDate AS DATE, @PurposeName NVARCHAR(100) = NULL, @IncludeSubAccounts BIT = 1,
		@InvestmentGroupName NVARCHAR(200) = 'Accounting Reporting', @AccountGroupName NVARCHAR(200) = 'Position Gain Loss Accounts', @GainLossStartDate DATE = '1/1/1900',
		@ByClientAccount BIT = 0
	AS
BEGIN
	DECLARE @PreviousDay AS DATE, @PreviousMonthEnd AS DATE, @PreviousYearEnd AS DATE
	SET @PreviousDay = calendar.GetBusinessDayFrom(@ReportDate, -1)
	SET @PreviousMonthEnd = calendar.GetPreviousMonthEnd(@ReportDate)
	SET @PreviousYearEnd = calendar.GetPreviousYearEnd(@ReportDate)
	
	IF @ByClientAccount = 0
	BEGIN
		SELECT
			Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,NULL as AccountName, NULL as AccountNumber
			,SUM(CurrentBalance - PreviousBalance) "DailyGain", SUM(CurrentBalance - PreviousMonthBalance) "MTDGain", SUM(CurrentBalance - PreviousYearBalance) "YTDGain", SUM(CurrentBalance - BeforeInceptionBalance) "ITDGain"
		FROM
			(SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName
				, Balance "CurrentBalance", 0 "PreviousBalance", 0 "PreviousMonthBalance", 0 "PreviousYearBalance", 0 "BeforeInceptionBalance"
			FROM Report.GetBalanceForGroupsWithSubAccounts(@ClientInvestmentAccountID, @ReportDate, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName
				, 0, Balance, 0, 0, 0
			FROM Report.GetBalanceForGroupsWithSubAccounts(@ClientInvestmentAccountID, @PreviousDay, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName
				, 0, 0, Balance, 0, 0
			FROM Report.GetBalanceForGroupsWithSubAccounts(@ClientInvestmentAccountID, @PreviousMonthEnd, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName
				, 0, 0, 0, Balance, 0
			FROM Report.GetBalanceForGroupsWithSubAccounts(@ClientInvestmentAccountID, @PreviousYearEnd, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName
				, 0, 0, 0, 0, Balance
			FROM Report.GetBalanceForGroupsWithSubAccounts(@ClientInvestmentAccountID, @GainLossStartDate, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
		) b
		GROUP BY Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName
		HAVING SUM(CurrentBalance - PreviousBalance) <> 0 OR SUM(CurrentBalance - PreviousMonthBalance) <> 0 OR SUM(CurrentBalance - PreviousYearBalance) <> 0 OR SUM(CurrentBalance - BeforeInceptionBalance) <> 0
		ORDER BY 2, 1, 3
	END
	ELSE
	BEGIN
		SELECT
			Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ia.AccountName,ia.AccountNumber
			,SUM(CurrentBalance - PreviousBalance) "DailyGain", SUM(CurrentBalance - PreviousMonthBalance) "MTDGain", SUM(CurrentBalance - PreviousYearBalance) "YTDGain", SUM(CurrentBalance - BeforeInceptionBalance) "ITDGain"
		FROM
			(SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ClientAccountID
				, Balance "CurrentBalance", 0 "PreviousBalance", 0 "PreviousMonthBalance", 0 "PreviousYearBalance", 0 "BeforeInceptionBalance"
			FROM Report.GetBalanceForGroupsWithSubAccountsBySubAccount(@ClientInvestmentAccountID, @ReportDate, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ClientAccountID
				, 0, Balance, 0, 0, 0
			FROM Report.GetBalanceForGroupsWithSubAccountsBySubAccount(@ClientInvestmentAccountID, @PreviousDay, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ClientAccountID
				, 0, 0, Balance, 0, 0
			FROM Report.GetBalanceForGroupsWithSubAccountsBySubAccount(@ClientInvestmentAccountID, @PreviousMonthEnd, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ClientAccountID
				, 0, 0, 0, Balance, 0
			FROM Report.GetBalanceForGroupsWithSubAccountsBySubAccount(@ClientInvestmentAccountID, @PreviousYearEnd, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
			UNION ALL
			SELECT
				Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ClientAccountID
				, 0, 0, 0, 0, Balance
			FROM Report.GetBalanceForGroupsWithSubAccountsBySubAccount(@ClientInvestmentAccountID, @GainLossStartDate, @InvestmentGroupName, @AccountGroupName, @PurposeName, @IncludeSubAccounts)
		) b
			INNER JOIN InvestmentAccountDimension ia ON ia.InvestmentAccountID = b.ClientAccountID
		GROUP BY Level1GroupItemName, Level1GroupItemOrder, InvestmentInstrumentName,ia.AccountName,ia.AccountNumber
		HAVING SUM(CurrentBalance - PreviousBalance) <> 0 OR SUM(CurrentBalance - PreviousMonthBalance) <> 0 OR SUM(CurrentBalance - PreviousYearBalance) <> 0 OR SUM(CurrentBalance - BeforeInceptionBalance) <> 0
		ORDER BY 5, 2, 1, 3
	END
	
END
