EXEC sys.sp_rename @objname = 'dbo.FactIncomeStatement.LocalGainLossITD', @newname = 'GainLossSinceInceptionLocal', @objtype = 'Column'  
EXEC sys.sp_rename @objname = 'dbo.FactIncomeStatement.LocalGainLossPTD', @newname = 'GainLossSinceLastPeriodLocal', @objtype = 'Column'  
EXEC sys.sp_rename @objname = 'dbo.FactIncomeStatement.LocalGainLossYTD', @newname = 'GainLossSinceLastYearEndLocal', @objtype = 'Column'  


EXEC sys.sp_rename @objname = 'dbo.FactIncomeStatement.BaseGainLossITD', @newname = 'GainLossSinceInceptionBase', @objtype = 'Column'  
EXEC sys.sp_rename @objname = 'dbo.FactIncomeStatement.BaseGainLossPTD', @newname = 'GainLossSinceLastPeriodBase', @objtype = 'Column'  
EXEC sys.sp_rename @objname = 'dbo.FactIncomeStatement.BaseGainLossYTD', @newname = 'GainLossSinceLastYearEndBase', @objtype = 'Column'  


ALTER TABLE dbo.FactIncomeStatement	ADD LastYearEndDate DATE
ALTER TABLE dbo.FactIncomeStatement	ADD LastPeriodDate DATE


