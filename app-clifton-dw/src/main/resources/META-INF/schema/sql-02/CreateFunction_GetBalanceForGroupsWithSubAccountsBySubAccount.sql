	
CREATE FUNCTION [Report].[GetBalanceForGroupsWithSubAccountsBySubAccount](@ClientInvestmentAccountID INT, @BalanceDate DATE, @InvestmentGroupName NVARCHAR(200), @AccountGroupName NVARCHAR(200), @PurposeName NVARCHAR(100), @IncludeSubAccounts BIT = 1)
	RETURNS TABLE AS

	RETURN
		SELECT
			Level1GroupItemName, Level1GroupItemOrder,
			Level2GroupItemName, Level2GroupItemOrder,
			Level3GroupItemName, Level3GroupItemOrder,
			AccountGroupItemName, AccountGroupItemOrder,
			bs.InvestmentSecurityID, COALESCE(s.InstrumentGroupName, s.InvestmentInstrumentName) "InvestmentInstrumentName",
			ISNULL(SUM(CASE WHEN ag.IsCreditMinusDebit = 1 THEN -BaseDebitCreditBalance ELSE BaseDebitCreditBalance END), 0) AS "Balance",
			bs.ClientInvestmentAccountID "ClientAccountID"
		FROM AccountingBalanceSnapshot bs
			INNER JOIN InvestmentSecurityDimension s ON bs.InvestmentSecurityID = s.InvestmentSecurityID
			INNER JOIN InvestmentGroupInstrumentDimension g ON s.InvestmentInstrumentID = g.InvestmentInstrumentID
			INNER JOIN AccountingAccountGroupAccountDimension ag ON bs.AccountingAccountID = ag.AccountingAccountID
			INNER JOIN Report.GetInvestmentAccountsForPurpose(@ClientInvestmentAccountID, @PurposeName, 1, @IncludeSubAccounts) sa ON bs.ClientInvestmentAccountID = sa.InvestmentAccountID
		WHERE 
			SnapshotDate = @BalanceDate
			AND InvestmentGroupName = @InvestmentGroupName
			AND AccountGroupName = @AccountGroupName
		GROUP BY
			g.Level1GroupItemName, g.Level1GroupItemOrder, g.Level1GroupItemType,
			g.Level2GroupItemName, g.Level2GroupItemOrder, g.Level2GroupItemType,
			g.Level3GroupItemName, g.Level3GroupItemOrder, g.Level3GroupItemType,
			ag.AccountGroupItemName, ag.AccountGroupItemOrder,
			bs.InvestmentSecurityID, COALESCE(s.InstrumentGroupName, s.InvestmentInstrumentName),
			bs.ClientInvestmentAccountID
