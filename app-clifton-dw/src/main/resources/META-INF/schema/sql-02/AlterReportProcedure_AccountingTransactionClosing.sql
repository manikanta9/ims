--DROP PROC [Report].[AccountingTransactionClosing]

ALTER PROC [Report].[AccountingTransactionClosing]
		@ClientInvestmentAccountID INT = 397
		,@ReportingDate DATETIME = '12/31/09'   
		,@StartDate DATETIME = '10/1/09' 
		,@InvestmentGroupID INT  = 1 
		
AS 


SELECT 
		fact.FactTransactionID
       ,fact.AccountingJournalID
       ,fact.AccountingTransactionID
       ,fact.ParentTransactionID
       ,fact.PositionTransactionID
       ,fact.ClientInvestmentAccountID
       ,fact.HoldingInvestmentAccountID
       ,fact.InvestmentSecurityID
       ,fact.CurrencySecurityID
       ,fact.AccountingAccountID
       ,fact.SystemTableID
       ,fact.FKFieldID
       ,fact.Price
       ,fact.Quantity
       ,fact.LocalDebitCredit
       ,fact.BaseDebitCredit
       ,fact.PositionGross
       ,fact.PositionCostBasis
       ,fact.PositionCommission
       ,fact.PositionCommissionCombined
       ,fact.FXRateToBase
       ,fact.IsSecurityPosition
       ,fact.IsCurrencyPosition
       ,fact.IsBaseCash
       ,fact.IsBuy
       ,fact.IsOpening
       ,fact.OpeningDate
       ,fact.OpeningPrice
       ,fact.OpeningFXRate
       ,fact.ClosingProportion
       ,fact.GLDate
       ,fact.TransactionDate
       ,fact.OriginalTransactionDate
       ,fact.ReportingDate
       ,fact.SettlementDate
       ,fact.TransactionDescription
       ,fact.CreateUserID
       ,fact.CreateDate

	--CalculatedColumns
		,RealizedGainLoss = fact.PositionRealized
		,PositionRealizedNet  
		,RealizedGainLossBase = CASE WHEN IsOpening = 0 THEN fact.PositionRealized * fact.FXRateToBase ELSE 0 END 
		,RealizedGainLossNetBase = CASE WHEN IsOpening = 0 THEN fact.PositionRealizedNet * fact.FXRateToBase ELSE 0 END 
		

	--Organizational Columns
	,LongShortIndicator = CASE WHEN Quantity < 0 THEN 'Long' ELSE 'Short' END 
	,BuySellIndicator = CASE WHEN IsBuy = 1 THEN 'Buy' ELSE 'Sell' END 
	,AccountName
	,AccountNumber

	,aa.AccountingAccountName
	,aat.AccountingAccountTypeName
	,aat.AccountingAccountTypeOrder
	
		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns
		 
		
		,PageGrouping = 'Investments'
		,Level1Grouping = ParentGroupItemName
		,Level2Grouping = GroupItemName

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns
		,ParentInstrumentGroupItemName = ParentGroupItemName
		,InstrumentGroupItemName = GroupItemName
		,InstrumentInvestmentCountry = ParentGroupItemName
		,ReportOrderParent = ParentItemOrder
		,ReportOrderChild = ItemOrder
		,BaseLanguage = CASE base.Symbol
						WHEN 'JPY' THEN 'Japanese'
						ELSE 'default' END 
		,BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 
		,InstrumentCurrencySymbol = icsi.Symbol
		,CurrencySymbol = icsi.Symbol
		,IsCurrency = CASE WHEN aa.AccountingAccountID IN (100) THEN 1 ELSE 0 END 
		,InstrumentCode = ii.IdentifierPrefix
		,InstrumentName = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.InvestmentSecurityName ELSE ii.InvestmentInstrumentName END 
		,IsBaseCurrency = CASE WHEN icsi.Symbol = base.Symbol THEN 1 ELSE 0 END 
		,BaseCurrencyDescr = ''
		,MatureDate = s.EndDate
		,s.InvestmentSecurityName
		,s.Symbol
		-----------------------------------------------------------------------------------------------------------------------------------------
	       
FROM dbo.FactTransaction fact
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
			
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID

		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
			LEFT JOIN CliftonIMS.dbo.InvestmentSecurity icsi ON ii.TradingCurrencyID = icsi.InvestmentSecurityID
		
		INNER JOIN dbo.InvestmentInstrumentHierarchy iih ON ii.InvestmentInstrumentHierarchyID = iih.InvestmentInstrumentHierarchyID
			INNER JOIN CliftonIMS.dbo.InvestmentType it ON iih.InvestmentTypeID = it.InvestmentTypeID
			LEFT JOIN report.InvestmentGroupExpanded(@InvestmentGroupID, 2) ige ON ige.InvestmentInstrumentID = ii.InvestmentInstrumentID
				
WHERE  fact.ClientInvestmentAccountID = @ClientInvestmentAccountID 
	and fact.TransactionDate BETWEEN @StartDate AND @ReportingDate 
	AND IsPosition = 1
	AND IsOpening = 0
	--AND aa.AccountingAccountID >= 400
	
ORDER BY
		ParentGroupItemName
	    ,GroupItemName 
	    ,ii.InvestmentInstrumentName
	    ,s.EndDate
	    ,fact.TransactionDate
	    
