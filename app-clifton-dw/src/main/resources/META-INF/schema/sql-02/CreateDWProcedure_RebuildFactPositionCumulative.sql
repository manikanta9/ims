/*
	exec Datawarehouse.RebuildFactCurrentPositionCumulative 223, '9/1/10'
*/

--DROP PROC Datawarehouse.RebuildFactCurrentPositionCumulative

CREATE PROC Datawarehouse.RebuildFactCurrentPositionCumulative


      @ClientInvestmentAccountID INT = 223
    , @ReportingDate DATETIME = '9/15/10'

AS 


SET NOCOUNT ON 

/*

	The position build uses the position from yesterday and adds the changes for today's activity.

*/

DECLARE @StartTime DATETIME
	, @EndTime DATETIME
	, @Duration DECIMAL(12,6)
SET @StartTime = GETDATE()



DECLARE 
	@LastQtrDate DATETIME
	,@LastYearDate DATETIME
	,@LastSnapshotDate DATETIME 

SELECT 
	@LastQtrDate = m.LastQuarterEndDate
	,@LastYearDate = m.LastYearEndDate
FROM Datawarehouse.GetQuarterEndDates(@ReportingDate) m 

--pull from yesterday or most recent snapshot date
SELECT @LastSnapshotDate = MAX(SnapshotDate)
FROM dbo.FactPositionSnapshot
WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND SnapshotDate < @ReportingDate 

PRINT '@LastSnapshotDate = ' + CONVERT(VARCHAR(20),@LastSnapshotDate, 101)




	DECLARE @BaseCashPosition TABLE (ClientInvestmentAccountID INT, HoldingInvestmentAccountID INT, InvestmentSecurityID INT, MarketValueLocal DECIMAL(19,2), MarketValueBase DECIMAL(19,2))
	INSERT INTO @BaseCashPosition
	SELECT ClientInvestmentAccountID
		,HoldingInvestmentAccountID
		,t.InvestmentSecurityID
		,SUM(LocalDebitCredit)
		,SUM(BaseDebitCredit)
	FROM    dbo.AccountingTransaction t
			INNER JOIN dbo.InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID
			LEFT JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
			INNER JOIN dbo.AccountingAccount aa ON t.AccountingAccountID = aa.AccountingAccountID
	WHERE   ClientInvestmentAccountID = @ClientInvestmentAccountID
			AND ReportingDate <= @ReportingDate
			AND aa.IsCash = 1 AND IsPosition = 0 ---exclude currency
			--AND aa.AccountingAccountID = 100
	GROUP BY ClientInvestmentAccountID
		,HoldingInvestmentAccountID
		,t.InvestmentSecurityID


SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'Create temp table @BaseCashPosition = ' +  STR(@Duration)
SET @StartTime = GETDATE()

DELETE 
FROM dbo.FactPositionSnapshot WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND SnapshotDate = @ReportingDate


SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'delete FactPositionSnapshot: ' +  STR(@Duration)
SET @StartTime = GETDATE()




INSERT INTO dbo.FactPositionSnapshot
        ( SnapshotDate
        ,AccountingTransactionID
        ,ClientInvestmentAccountID
        ,HoldingInvestmentAccountID
        ,InvestmentSecurityID
        ,CurrencySecurityID
        ,AccountingAccountID
        ,SystemTableID
        ,FKFieldID
        ,GLDate
        ,TransactionDate
        ,OriginalTransactionDate
        ,SettlementDate
        ,MarketPrice
        ,OpenFXToBase
        ,MarketFXToBase
        ,MarketFXToUS
        ,Quantity
        ,QuantityRemaining
        ,CostBasis
        ,CostBasisRemaining
        ,CostBasisRemainingBase
        ,MarketValueLocal
        ,MarketValueBase
        ,MarketValueLastQuarterEnd
        ,MarketValueLastYearEnd
        
        ,BalanceSheetMarketValueBase
        
        ,NotionalValueLocal
        ,AdjustedNotionalValueLocal
        ,OpeningPrice
        ,OpenTradeEquity
        ,PriorOpenTradeEquity
        
        ,LocalGainLossITD
        ,LocalGainLossPTD
        ,LocalGainLossYTD
        ,BaseGainLossITD
        ,BaseGainLossPTD
        ,BaseGainLossYTD
        
        ,IsInvestmentLocal
        

        )
/*
EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID = 19, @ReportingDate = '12/31/09'
*/

SELECT @ReportingDate
		,t.AccountingTransactionID
       ,ClientInvestmentAccountID = @ClientInvestmentAccountID
       ,p.HoldingInvestmentAccountID
       ,p.InvestmentSecurityID
       ,CurrencySecurityID = COALESCE(isci.InvestmentSecurityID, ii.TradingCurrencyID, t.InvestmentSecurityID) 
       ,t.AccountingAccountID
       ,t.SystemTableID 
       ,t.FKFieldID
       ,t.GLDate
       ,t.TransactionDate
       ,t.OriginalTransactionDate
       ,t.SettlementDate
       
       ,Price = CASE 
						WHEN t.AccountingAccountID = 105 THEN ISNULL(p.MarketFxToBase, 1)
						WHEN aa.IsCash = 1 THEN 1 
				ELSE ISNULL(markprice.Price, 1) END 
       
       ,t.FXRateToBase
       
		,MarketFXToBase = ISNULL(p.MarketFxToBase, 1)
		,MarketFXToUS = ISNULL(Datawarehouse.GetFXRate(p.MarketFxToBase, 1), 1)

       ,Quantity = OpeningQuantity
       ,QuantityRemaining = NetQuantity

       ,OpeningCostBasis
       ,CostBasisRemaining = NetCostBasis
       ,CostBasisRemainingBase = CostBasisRemainingBase

		,MarketValue = MarketValueLocal
		,MarketValueBase = MarketValueBase
		
		,MarketValueLastQuarterEnd = CASE WHEN t.OriginalTransactionDate <= @LastQtrDate AND LastQtrPrice.InvestmentSecurityID IS NOT NULL 
							THEN  CONVERT(DECIMAL(28,2), ISNULL(LastQtrPrice.Price,1) * ii.PriceMultiplier * NetQuantity) ELSE 0 END 

		,MarketValueLastYearEnd = CASE WHEN t.OriginalTransactionDate <= @LastYearDate AND LastYearPrice.InvestmentSecurityID IS NOT NULL 
							THEN  CONVERT(DECIMAL(28,2), ISNULL(LastYearPrice.Price,1) * ii.PriceMultiplier * NetQuantity) ELSE 0 END 
		
		
		,BalanceSheetMarketValueBase = 0
		,NotionalValueLocal = ISNULL(markprice.Price,1) * ii.PriceMultiplier * NetQuantity
		,AdjNotionalValueLocal = ISNULL(markprice.Price,1) * ii.PriceMultiplier * NetQuantity
		,OpeningPrice = OpenPrice
        ,OpenTradeEquity = 0 
        ,PriorOpenTradeEquity = 0

        ,LocalGainLossITD = 0
        ,LocalGainLossPTD = 0
        ,LocalGainLossYTD = 0
        ,BaseGainLossITD = 0
        ,BaseGainLossPTD = 0
        ,BaseGainLossYTD = 0
        
        ,IsInvestmentLocal = CASE WHEN isci.Symbol = base.Symbol 
			THEN 1 ELSE 0 END

		
FROM Datawarehouse.PositionSummaryCumulative(@ClientInvestmentAccountID, @ReportingDate, @LastSnapshotDate) p
	INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = @ClientInvestmentAccountID
		
	INNER JOIN dbo.AccountingTransaction t ON p.PostionID = t.AccountingTransactionID
		INNER JOIN dbo.AccountingAccount aa ON t.AccountingAccountID = aa.AccountingAccountID
		INNER JOIN dbo.InvestmentSecurity si ON t.InvestmentSecurityID = si.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON si.InvestmentInstrumentID = ii.InvestmentInstrumentID
			LEFT JOIN dbo.InvestmentSecurity isci ON ii.TradingCurrencyID = isci.InvestmentSecurityID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID

	----get market price
	--LEFT JOIN Datawarehouse.RecentPrice(@ReportingDate)  markprice ON p.InvestmentSecurityID = markprice.InvestmentSecurityID AND markprice.ProviderCompanyID = @BloombergCompanyID
	----get quarter end market prices
	--LEFT JOIN Datawarehouse.RecentPrice(@LastQtrDate)  LastQtrPrice ON p.InvestmentSecurityID = LastQtrPrice.InvestmentSecurityID AND LastQtrPrice.ProviderCompanyID = @BloombergCompanyID
	--LEFT JOIN Datawarehouse.RecentPrice(@LastYearDate)  LastYearPrice ON p.InvestmentSecurityID = LastYearPrice.InvestmentSecurityID AND LastQtrPrice.ProviderCompanyID = @BloombergCompanyID


	LEFT JOIN dbo.FactSettlementPrice markprice ON markprice.PriceDate = @ReportingDate AND markprice.InvestmentSecurityID = p.InvestmentSecurityID
	LEFT JOIN dbo.FactSettlementPrice LastQtrPrice ON LastQtrPrice.PriceDate = @LastQtrDate AND LastQtrPrice.InvestmentSecurityID = p.InvestmentSecurityID
	LEFT JOIN dbo.FactSettlementPrice LastYearPrice ON LastYearPrice.PriceDate = @LastYearDate AND LastYearPrice.InvestmentSecurityID = p.InvestmentSecurityID

	------get quarter end market prices
	--	LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markbaseFX ON markbaseFX.FromCurrencySecurityID = a.BaseCurrencyID AND markbaseFX.ProviderCompanyID = brka.IssuingCompanyID
	--	LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markcurrFX ON markcurrFX.FromCurrencySecurityID = ii.TradingCurrencyID AND markcurrFX.ProviderCompanyID = brka.IssuingCompanyID

--WHERE p.IsCash = 0
WHERE aa.IsPosition = 1

UNION 

SELECT @ReportingDate
		,AccountingTransactionID = NULL 
		,@ClientInvestmentAccountID
		,@ClientInvestmentAccountID
		,p.InvestmentSecurityID
		,CurrencySecurityID = CASE WHEN p.InvestmentSecurityID = a.BaseCurrencyID THEN p.InvestmentSecurityID ELSE ISNULL(ii.TradingCurrencyID, (SELECT InvestmentSecurityID FROM dbo.InvestmentSecurity WHERE Symbol = 'USD')) END --- set the 
		,AccountingAccountID = 100
		,SystemTableID = 0 
		,FKFieldID = 0 
		,GLDate = @ReportingDate
		,TransactionDate = @ReportingDate
		,OriginalTransactionDate = @ReportingDate
		,SettlementDate = @ReportingDate
		,Price = 1
		,FXRateToBase = 1

		,MarketFXToBase = 1
		,MarketFXToUS = ISNULL(Datawarehouse.GetFXRate(markbaseFX.Price, 1), 1)

		,OpeningQuantity = 1
		,NetQuantity = 1

		,OpeningCostBasis = p.MarketValueLocal
		,CostBasisRemaining = p.MarketValueLocal
		,CostBasisRemaining = p.MarketValueLocal

		,MarketValue = p.MarketValueLocal
		,MarketValueBase = p.MarketValueLocal
		,MarketValue = 0
		,MarketValue = 0
		,BalanceSheetMarketValueBase = 0

		,NotionalValueLocal = p.MarketValueLocal
		,AdjNotionalValueLocal = p.MarketValueLocal
		,1
		,0
		,0
		,LocalGainLossITD = 0
		,LocalGainLossPTD = 0
		,LocalGainLossYTD = 0
		,BaseGainLossITD = 0
		,BaseGainLossPTD = 0
		,BaseGainLossYTD = 0
		,IsInvestmentLocal = 1  --base currency

FROM  @BaseCashPosition p
	INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = p.ClientInvestmentAccountID
		INNER JOIN dbo.InvestmentAccount brka ON brka.InvestmentAccountID = p.HoldingInvestmentAccountID
			
		INNER JOIN dbo.InvestmentSecurity si ON p.InvestmentSecurityID = si.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON si.InvestmentInstrumentID = ii.InvestmentInstrumentID
			LEFT JOIN dbo.InvestmentSecurity isci ON ii.TradingCurrencyID = isci.InvestmentSecurityID

	LEFT JOIN dbo.FactSettlementPrice markprice ON markprice.PriceDate = @ReportingDate AND markprice.InvestmentSecurityID = p.InvestmentSecurityID
	LEFT JOIN dbo.FactSettlementPrice markbaseFX ON markbaseFX.PriceDate = @ReportingDate AND markbaseFX.InvestmentSecurityID =  a.BaseCurrencyID
	LEFT JOIN dbo.FactSettlementPrice markcurrFX ON markcurrFX.PriceDate = @ReportingDate AND markcurrFX.InvestmentSecurityID = ii.TradingCurrencyID


ORDER BY InvestmentSecurityID


--set market value for positions to be todays open trade equity 
UPDATE fact
SET BalanceSheetMarketValueBase = CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END 
FROM dbo.FactPositionSnapshot fact 
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
		INNER JOIN CliftonIMS.dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 


SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'Insert FactPositionSnapshot: ' +  STR(@Duration)
SET @StartTime = GETDATE()


EXEC Datawarehouse.RebuildFactIncomeStatementCumulative @ClientInvestmentAccountID, @ReportingDate, @LastSnapshotDate

SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'RebuildFactIncomeStatement: ' +  STR(@Duration)
SET @StartTime = GETDATE()


/*
--drop PROC Datawarehouse.RebuildFactCurrentPosition


DECLARE @AccountNumber VARCHAR(100) = '107600'
	,@ClientInvestmentAccountID INT 
	,@PositionTransferDate DATETIME = '12/31/2008'
	,@ReportingDate DATETIME = '7/31/10'
	,@StartDate DATETIME = '7/1/10'
	,@Symbol VARCHAR(100) = 'CAD'
	
DECLARE @InvestmentSecurityID INT = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = @Symbol)
SELECT @ClientInvestmentAccountID = InvestmentAccountID FROM dbo.InvestmentAccount WHERE AccountNumber = @AccountNumber
--PRINT @ClientInvestmentAccountID

Set @ClientInvestmentAccountID = 26 

Exec Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '12/31/09'

  


*/
