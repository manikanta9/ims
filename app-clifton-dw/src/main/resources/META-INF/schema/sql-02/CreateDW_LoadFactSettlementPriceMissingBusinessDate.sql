DECLARE @RunDate DATE 

DECLARE ProcessingCursor CURSOR FORWARD_ONLY FOR 

SELECT CalendarDate
FROM dbo.Calendar
WHERE MonthEndDate = CalendarDate
	AND IsBusinessDay = 0
	AND DateYear >= 2010
	
OPEN ProcessingCursor
FETCH NEXT FROM ProcessingCursor into @RunDate
WHILE @@Fetch_Status = 0 
BEGIN

	PRINT @RunDate

	EXEC Datawarehouse.RebuildFactSettlementPriceNonBusinessReportDay @RunDate
 
FETCH NEXT FROM ProcessingCursor into @RunDate
END

CLOSE ProcessingCursor
DEALLOCATE ProcessingCursor
