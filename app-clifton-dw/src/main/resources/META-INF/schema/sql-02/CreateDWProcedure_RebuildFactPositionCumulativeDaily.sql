
--DROP PROC Datawarehouse.RebuildFactCurrentPositionCumulativeDaily

CREATE PROC Datawarehouse.RebuildFactCurrentPositionCumulativeDaily

      @ClientInvestmentAccountID INT = 223
    , @StartDate DATETIME = '1/1/10'

AS 

/*

select * from FactPositionSnapshot

exec Datawarehouse.RebuildFactCurrentPositionCumulativeDaily 223, '1/1/10'

*/

DECLARE @RunDate DATE

SET NOCOUNT ON 
--DELETE FROM dbo.FactIncomeStatement
--DELETE FROM dbo.FactPositionSnapshot

DECLARE ProcessingCursor CURSOR FORWARD_ONLY FOR 

	SELECT CalendarDate FROM dbo.Calendar
	WHERE (IsBusinessDay = 1 OR CalendarDate = MonthEndDate)
		AND CalendarDate >= @StartDate
		AND CalendarDate >= (SELECT MIN(TransactionDate) FROM dbo.AccountingTransaction WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID)
		AND CalendarDate < GETDATE()

OPEN ProcessingCursor
FETCH NEXT FROM ProcessingCursor into @RunDate
WHILE @@Fetch_Status = 0 
BEGIN
	PRINT '----REBUILDING SNAPSHOT-----------------------------------------------------------------------------------------------------------------------'
	PRINT '------RunDate = ' + CONVERT(VARCHAR(40), @RunDate, 101)
	
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, @RunDate
	--EXEC Datawarehouse.RebuildFactCurrentPositionCumulative @ClientInvestmentAccountID, @RunDate


FETCH NEXT FROM ProcessingCursor into @RunDate

END

CLOSE ProcessingCursor
DEALLOCATE ProcessingCursor
