/****** Returns the last day of the previous fiscal quarter using the annual start month (where 1=January) to determine when quarters occur ******/
CREATE FUNCTION [calendar].[GetPreviousFiscalQuarterEnd](@FromDate DATE, @FiscalYearStartMonth INT)
	RETURNS DATE AS
BEGIN
	-- Same as Calendar Quarters	
	IF (@FiscalYearStartMonth IN (1, 4, 7, 10))
	BEGIN
		RETURN calendar.GetPreviousQuarterEnd(@FromDate)
	END
	
	DECLARE @Month INT, @Year INT
	SET @Month = Month(@FromDate)
	SET @Year = Year(@FromDate)
	
	IF (@FiscalYearStartMonth IN (2, 5, 8, 11))
	BEGIN
		SET @Month = CASE @Month
		WHEN 1 THEN 11
		WHEN 2 THEN 11
		WHEN 3 THEN 2
		WHEN 4 THEN 2
		WHEN 5 THEN 2
		WHEN 6 THEN 5
		WHEN 7 THEN 5
		WHEN 8 THEN 5
		WHEN 9 THEN 8
		WHEN 10 THEN 8
		WHEN 11 THEN 8
		WHEN 12 THEN 11
		END
	END
	

	IF (@FiscalYearStartMonth IN (3, 6, 9, 12))
	BEGIN
		SET @Month = CASE @Month
		WHEN 1 THEN 12
		WHEN 2 THEN 12
		WHEN 3 THEN 12
		WHEN 4 THEN 3
		WHEN 5 THEN 3
		WHEN 6 THEN 3
		WHEN 7 THEN 6
		WHEN 8 THEN 6
		WHEN 9 THEN 6
		WHEN 10 THEN 9
		WHEN 11 THEN 9
		WHEN 12 THEN 9
		END
	END
	
	IF (@Month >= Month(@FromDate))
	BEGIN
		SET @Year = @Year - 1
	END
	
	RETURN DATEADD(DAY, -1, CONVERT(DATE, STR(@Month) + '/1/' + STR(@Year)))
END
