
--DROP PROC Datawarehouse.RebuildFactSettlementPriceNonBusinessReportDay 


CREATE PROC Datawarehouse.RebuildFactSettlementPriceNonBusinessReportDay 


	@RunDate DATE = '10/31/10'


AS 


SET NOCOUNT ON 

DECLARE 
	@InvestmentSecurityID INT 
	, @Symbol VARCHAR(50)
	, @LastBusinessDate DATE  

SELECT @LastBusinessDate = MAX(CalendarDate) FROM dbo.Calendar WHERE CalendarDate < @RunDate AND IsBusinessDay = 1


DECLARE ProcessingCursor CURSOR LOCAL FORWARD_ONLY FOR 

	SELECT s.Symbol, s.InvestmentSecurityID
	FROM dbo.FactSettlementPrice p
		INNER JOIN dbo.InvestmentSecurity s ON  p.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		INNER JOIN dbo.InvestmentInstrumentHierarchy iih ON ii.InvestmentInstrumentHierarchyID = iih.InvestmentInstrumentHierarchyID
		INNER JOIN dbo.InvestmentType it ON iih.InvestmentTypeID = it.InvestmentTypeID
	WHERE PriceDate = @LastBusinessDate	

	
OPEN ProcessingCursor
FETCH NEXT FROM ProcessingCursor into @Symbol, @InvestmentSecurityID
WHILE @@Fetch_Status = 0 
BEGIN

	IF NOT EXISTS ( SELECT *
		FROM dbo.FactSettlementPrice p
		WHERE PriceDate = @RunDate	AND InvestmentSecurityID = @InvestmentSecurityID )
	BEGIN 
	 --PRINT @Symbol
	INSERT INTO dbo.FactSettlementPrice
	        ( InvestmentSecurityID
	        ,PriceDate
	        ,Price
	        )
	 SELECT InvestmentSecurityID
	        ,@RunDate
	        ,Price
		FROM dbo.FactSettlementPrice p
		WHERE PriceDate = @LastBusinessDate	AND InvestmentSecurityID = @InvestmentSecurityID
	END

FETCH NEXT FROM ProcessingCursor into @Symbol, @InvestmentSecurityID

END

CLOSE ProcessingCursor
DEALLOCATE ProcessingCursor


