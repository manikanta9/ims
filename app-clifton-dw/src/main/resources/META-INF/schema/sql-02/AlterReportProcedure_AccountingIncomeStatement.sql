--DROP PROC [Report].[AccountingIncomeStatement]

ALTER PROC [Report].[AccountingIncomeStatement]
		@ClientInvestmentAccountID INT = 190
		,@ReportingDate DATETIME = '10/31/10'   
		,@StartDate DATETIME = '10/1/10' 
		,@InvestmentGroupID INT  = 1 

AS


SELECT 
	gl.ClientInvestmentAccountID
	,ReportingDate = @ReportingDate
	,aa.AccountingAccountName
	,gl.IncomeStatus
	,gl.InvestmentSecurityID
	,gl.CurrencySecurityID
	,gl.GainLossSinceInceptionLocal
	,gl.GainLossSinceLastPeriodLocal
	,gl.GainLossSinceLastYearEndLocal
	,gl.GainLossSinceInceptionBase
	,gl.GainLossSinceLastPeriodBase
	,gl.GainLossSinceLastYearEndBase
	,s.Symbol
	,s.InvestmentSecurityName

	--Organizational Columns
	,AccountName
	,AccountNumber
		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns
		,ParentInstrumentGroupItemName = ParentGroupItemName
		,InstrumentGroupItemName = GroupItemName
		,InstrumentInvestmentCountry = ParentGroupItemName
		,ReportOrderParent = ParentItemOrder
		,ReportOrderChild = ItemOrder

		,BaseLanguage = CASE base.Symbol
						WHEN 'JPY' THEN 'Japanese'
						ELSE 'default' END 

		,BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 

		,InstrumentCurrencySymbol = CASE 
				(CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.Symbol ELSE icsi.Symbol END)
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 

		 
		 
		,CurrencySymbol = icsi.Symbol
		,IsCurrency = CASE WHEN aa.AccountingAccountID IN (100) THEN 1 ELSE 0 END 
		,InstrumentCode = ii.IdentifierPrefix
		,InstrumentName = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.InvestmentSecurityName ELSE ii.InvestmentInstrumentName END 
		,IsBaseCurrency = CASE WHEN icsi.Symbol = base.Symbol THEN 1 ELSE 0 END 
		,BaseCurrencyDescr = ''
		,MatureDate = s.EndDate
		-----------------------------------------------------------------------------------------------------------------------------------------



	
FROM  dbo.FactIncomeStatement gl
	INNER JOIN dbo.AccountingAccount aa ON aa.AccountingAccountID = gl.AccountingAccountID
	INNER JOIN dbo.InvestmentSecurity cs ON gl.CurrencySecurityID = cs.InvestmentSecurityID  --currency
	INNER JOIN dbo.InvestmentSecurity s ON gl.InvestmentSecurityID = s.InvestmentSecurityID
	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	LEFT JOIN CliftonIMS.dbo.InvestmentSecurity icsi ON ii.TradingCurrencyID = icsi.InvestmentSecurityID
	
		LEFT JOIN report.InvestmentGroupExpanded(@InvestmentGroupID, 2) ige ON ige.InvestmentInstrumentID = ii.InvestmentInstrumentID

	INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = gl.ClientInvestmentAccountID
		LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
WHERE gl.ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND gl.SnapshotDate = @ReportingDate

ORDER BY  		
		ige.ParentItemOrder	
		,ige.ItemOrder	
	


/*
	EXEC report.AccountingIncomeStatement 1, '12/31/09', '11/1/09'
*/
