/*
drop PROC [Report].[AccountingPositionDetail]
*/

ALTER PROC [Report].[AccountingPositionDetail]
		@ClientInvestmentAccountID INT = 190
		,@ReportingDate DATETIME = '10/31/10' 
		,@InvestmentGroupID INT  = 1 

AS

SELECT
		fact.OriginalTransactionDate
		,fact.SettlementDate
		,Btick = s.Symbol
		,s.Symbol
		
		,a.AccountName
		,a.AccountNumber

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Measures
		,fact.OpeningPrice
		,MarketPrice = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN fact.MarketFXToBase ELSE fact.MarketPrice END 
		,fact.MarketValueLocal
		,MarketValueBase
		,BalanceSheetCostBase = CASE WHEN it.InvestmentTypeName = 'Futures' THEN 0 ELSE fact.CostBasisRemainingBase END 
		,BalanceSheetMarketValueBase =  CASE WHEN it.InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END 
		,fact.NotionalValueLocal
		,fact.AdjustedNotionalValueLocal
		,MarketValue = fact.MarketValueLocal
		,fact.OpenFXToBase
		,fact.MarketFXToBase
		,fact.MarketFXToUS
		,fact.Quantity
		,fact.QuantityRemaining

		,LongShortIndicator = CASE WHEN Quantity >= 0 THEN 'Long' ELSE 'Short' END 

		,fact.CostBasis
		,fact.CostBasisRemaining
		,ii.PriceMultiplier

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Calculated Columns
		,CostBasisRemainingBase = CONVERT(DECIMAL(19,2), (CostBasisRemaining) * fact.MarketFXToBase)
		,CostBasisRemainingUSDollar = CONVERT(DECIMAL(19,2), CostBasisRemaining * fact.MarketFXToBase) * fact.MarketFXToUS
		,MarketValueUSDollar = CONVERT(DECIMAL(19,2), MarketValueLocal * fact.MarketFXToBase * fact.MarketFXToUS)
		,NotionalValueBase = CONVERT(DECIMAL(19,2), NotionalValueLocal * fact.MarketFXToBase)
		,AdjustedNotionalValueBase = CONVERT(DECIMAL(19,2), AdjustedNotionalValueLocal * fact.MarketFXToBase)
		,UnrealizedGainLossLocal =  CONVERT(DECIMAL(19,2), MarketValueLocal - CostBasisRemaining)
		,UnrealizedGainLossBase =  CONVERT(DECIMAL(19,2), MarketValueBase - CostBasisRemainingBase)
		,UnrealizedGainLossUSDollar =  CONVERT(DECIMAL(19,2), (MarketValueLocal - CostBasisRemaining) * fact.MarketFXToBase * fact.MarketFXToUS)
		
		--ID Columns
		,fact.SnapshotDate
		,ReportingDate = @ReportingDate
		,fact.TransactionDate

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns
		,ParentInstrumentGroupItemName = ParentGroupItemName
		,InstrumentGroupItemName = GroupItemName
		,InstrumentInvestmentCountry = ParentGroupItemName
		,ReportOrderParent = ParentItemOrder
		,ReportOrderChild = ItemOrder
		,BaseLanguage = CASE base.Symbol
						WHEN 'JPY' THEN 'Japanese'
						ELSE 'default' END 
		,BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 
		,InstrumentCurrencySymbol = icsi.Symbol
		,CurrencySymbol = icsi.Symbol
		,IsCurrency = CASE WHEN aa.AccountingAccountID IN (100) THEN 1 ELSE 0 END 
		,InstrumentCode = ii.IdentifierPrefix
		,InstrumentName = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.InvestmentSecurityName ELSE ii.InvestmentInstrumentName END 
		,IsBaseCurrency = CASE WHEN icsi.Symbol = base.Symbol THEN 1 ELSE 0 END 
		,BaseCurrencyDescr = ''
		,MatureDate = s.EndDate
		,s.InvestmentSecurityName
		-----------------------------------------------------------------------------------------------------------------------------------------
		
FROM dbo.FactPositionSnapshot fact 
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
			
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID

		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
			LEFT JOIN CliftonIMS.dbo.InvestmentSecurity icsi ON ii.TradingCurrencyID = icsi.InvestmentSecurityID
		
		INNER JOIN dbo.InvestmentInstrumentHierarchy iih ON ii.InvestmentInstrumentHierarchyID = iih.InvestmentInstrumentHierarchyID
			INNER JOIN CliftonIMS.dbo.InvestmentType it ON iih.InvestmentTypeID = it.InvestmentTypeID
			LEFT JOIN report.InvestmentGroupExpanded(@InvestmentGroupID, 2) ige ON ige.InvestmentInstrumentID = ii.InvestmentInstrumentID

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 
	AND  aa.AccountingAccountID NOT IN (100, 105)
ORDER BY  		
		ige.ParentItemOrder	
		,ige.ItemOrder	



