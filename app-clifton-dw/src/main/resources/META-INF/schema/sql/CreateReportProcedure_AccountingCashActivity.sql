/*

DROP PROC [Report].[AccountingCashActivity]
Select * from dbo.FactTransaction
EXEC report.AccountingCashActivity 397

*/

CREATE PROC [Report].[AccountingCashActivity]
		@ClientInvestmentAccountID INT = 187
		,@ReportingDate DATETIME = '12/31/09'   
		,@StartDate DATETIME = '11/1/09' 
		,@AccountingAids BIT = 1

	 AS 

DECLARE @BalanceSummary TABLE (
		ClientInvestmentAccountID INT
		, HoldingInvestmentAccountID INT
		, AccountingAccountID INT 
		, CurrencySecurityID INT
		, OpeningBalanceAmount DECIMAL(28,2)
		, ActivityAmount DECIMAL(28,2)
		, EndingBalanceAmount DECIMAL(28,2)
		)

--get opening balance - assume all previous balances are the total
INSERT INTO @BalanceSummary
EXEC report.GetCashBalanceSummary @ClientInvestmentAccountID, @ReportingDate, @StartDate

SELECT 

		s.Symbol
		,InstrumentName = 
		CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.InvestmentSecurityName ELSE si.InvestmentInstrumentName END 
		,CurrencySymbol = CASE WHEN aa.AccountingAccountID = 100 then base.Symbol else icsi.Symbol END 
		,CurrencyName = 
				CASE WHEN aa.AccountingAccountID = 100 THEN base.InvestmentSecurityName ELSE icsi.InvestmentSecurityName END 

		,a.AccountName
		,a.AccountNumber

		,PageGrouping = ''
		,Level1Grouping = ra.AccountName + ' (' + ra.AccountNumber + ')'
		,Level2Grouping = 
						CASE WHEN aa.AccountingAccountID = 100 THEN base.InvestmentSecurityName ELSE icsi.InvestmentSecurityName END 

		,HoldingAccountName = ha.AccountName
		,HoldingAccountNumber = ha.AccountNumber
		
		,ReportingAccountName = ra.AccountName
		,ReportingAccountNumber = ra.AccountNumber

		,BuySell = CASE WHEN fact.CurrencySecurityID <> fact.InvestmentSecurityID THEN CASE WHEN IsBuy = 1 THEN 'BUY' ELSE 'SELL' END ELSE '' END 
		,OpenClose = CASE WHEN fact.CurrencySecurityID <> fact.InvestmentSecurityID THEN CASE WHEN IsOpening = 1 THEN 'OPEN' ELSE 'CLOSE' END  ELSE '' END 
		,MatureDate = s.EndDate
		,InvestmentSecurityName = (s.InvestmentSecurityName)
		,aa.AccountingAccountName
		,Price = case when fact.Price = 1 THEN NULL ELSE fact.Price END 
		,fact.Quantity
		,Bought = CASE WHEN fact.Quantity < 0 THEN NULL ELSE fact.Quantity END 
		,Sold = -CASE WHEN fact.Quantity > 0 THEN NULL ELSE fact.Quantity END 
		,fact.BaseDebitCredit
		,FXRateToBase = CASE WHEN aa.AccountingAccountID = 105 THEN fact.FXRateToBase ELSE NULL END 

		,OpeningBalanceAmount
		,EndingBalanceAmount
		,ActivityAmount

		,LocalDebitCredit 
		--,LocalDebitCredit = CASE WHEN aa.AccountingAccountID = 105 THEN fact.LocalDebitCredit ELSE NULL END 
		,LocalDebit = 
			CASE WHEN ISNULL(fact.LocalDebitCredit, 0) <= 0   
						THEN ABS(fact.LocalDebitCredit) ELSE 0 END  
		,LocalCredit =  
			CASE WHEN ISNULL(fact.LocalDebitCredit, 0) > 0  
						THEN ABS(fact.LocalDebitCredit) ELSE 0 END  
		,fact.TransactionDate
		,TransactionDescription = fact.TransactionDescription
		,BaseLanguage = CASE base.Symbol
						WHEN 'JPY' THEN 'Japanese'
						ELSE 'default' END 
		,BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 
		,InstrumentCurrencySymbol = CASE CASE WHEN aa.AccountingAccountID = 100 then base.Symbol else icsi.Symbol END 
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 
		,InstrumentInvestmentCountry = 
				CASE 
					--WHEN s.Symbol = icsi.Symbol THEN 'Cash & Currencies'
					WHEN aa.AccountingAccountID IN (100) THEN 'Cash'
				
					ELSE 
						CASE WHEN icsi.Symbol = base.Symbol 
							THEN 'Domestic Investments'
							ELSE 'International Investments' END
					END 

FROM dbo.FactTransaction fact
		LEFT JOIN @BalanceSummary openbal ON 
				fact.ClientInvestmentAccountID = openbal.ClientInvestmentAccountID 
					AND fact.HoldingInvestmentAccountID = openbal.HoldingInvestmentAccountID 
					AND fact.AccountingAccountID = openbal.AccountingAccountID
					AND fact.CurrencySecurityID = openbal.CurrencySecurityID
					
			LEFT JOIN CliftonIMS.dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
		INNER JOIN dbo.InvestmentAccount ha ON fact.HoldingInvestmentAccountID = ha.InvestmentAccountID
		INNER JOIN dbo.InvestmentAccount ra ON fact.ReportingInvestmentAccountID = ra.InvestmentAccountID
			LEFT JOIN dbo.InvestmentAccountType rat ON ra.InvestmentAccountTypeID = rat.InvestmentAccountTypeID
			
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
			INNER JOIN dbo.InvestmentInstrument si ON s.InvestmentInstrumentID = si.InvestmentInstrumentID
				INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ih.InvestmentInstrumentHierarchyID = si.InvestmentInstrumentHierarchyID
				INNER JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
		LEFT JOIN dbo.InvestmentSecurity icsi ON fact.CurrencySecurityID = icsi.InvestmentSecurityID

WHERE fact.TransactionDate BETWEEN @StartDate AND @ReportingDate 

		AND (IsCash = 1 OR IsPosition = 1)
		AND fact.ClientInvestmentAccountID = @ClientInvestmentAccountID
		AND (@AccountingAids = 1 OR fact.IsCustodial = 1)

ORDER BY Level1Grouping, TransactionDate, aa.AccountingAccountName, s.Symbol, fact.Price

