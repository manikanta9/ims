--select * from Report.InvestmentMatrixView (3)

CREATE FUNCTION Report.InvestmentMatrixView (@InvestmentGroupID INT)

RETURNS TABLE 

AS

RETURN 


SELECT 
		ig.InvestmentGroupName

		,igi.GroupItemName

		,ig.InvestmentGroupID


		,ii.InvestmentInstrumentID
		,ii.InvestmentInstrumentName
		,ii.TradingCurrencyID
		,ii.IdentifierPrefix
		,ii.InvestmentInstrumentDescription
		,ii.PriceMultiplier
		,ii.UnderlyingInstrumentID

		,s.InvestmentSecurityID
		,s.InvestmentSecurityName
		,s.InvestmentSecurityDescription
		,s.IsCurrency
		,s.Symbol
		,s.Cusip
		,s.StartDate
		,s.EndDate
		,s.FirstNoticeDate
		,s.FirstDeliveryDate
		,s.LastDeliveryDate
		,CurrencySymbol = icsi.Symbol
		,CurrencySecurityName = icsi.InvestmentSecurityName


	
FROM CliftonIMS.dbo.InvestmentGroup ig
	INNER JOIN CliftonIMS.dbo.InvestmentGroupItem igi ON ig.InvestmentGroupID = igi.InvestmentGroupID
	INNER JOIN CliftonIMS.dbo.InvestmentGroupItemInstrument igii ON igi.InvestmentGroupItemID = igii.InvestmentGroupItemID
	INNER JOIN CliftonIMS.dbo.InvestmentInstrument ii ON igii.InvestmentInstrumentID = ii.InvestmentInstrumentID
	INNER JOIN CliftonIMS.dbo.InvestmentSecurity s ON ii.InvestmentInstrumentID = s.InvestmentInstrumentID
	LEFT JOIN CliftonIMS.dbo.InvestmentSecurity icsi ON ii.TradingCurrencyID = icsi.InvestmentSecurityID
WHERE ig.InvestmentGroupID = @InvestmentGroupID
