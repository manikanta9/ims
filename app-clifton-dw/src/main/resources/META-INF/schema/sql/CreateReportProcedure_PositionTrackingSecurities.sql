/*
drop proc [Report].[PositionLotTrackingSecurities] 

exec [Report].[PositionLotTrackingSecurities] 397

*/


CREATE PROC [Report].[PositionLotTrackingSecurities] 

		@ClientInvestmentAccountID INT = 297
 
AS


	SELECT 
		DISTINCT 
		s.Symbol
		,s.InvestmentSecurityID
		,si.InvestmentInstrumentID
		,si.IdentifierPrefix
		,si.InvestmentInstrumentName

FROM dbo.FactTransaction fact
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
			INNER JOIN dbo.InvestmentInstrument si ON s.InvestmentInstrumentID = si.InvestmentInstrumentID
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND aa.IsPosition = 1
UNION ALL 	
SELECT ' All Securities', NULL , NULL, NULL, NULL 
	
	
	--AND aa.AccountingAccountID <> 105
 
ORDER BY s.Symbol
