CREATE FUNCTION Datawarehouse.RecentFxRate
    (
     
     @MarketDate DATETIME 
		 

    )
RETURNS TABLE
AS

RETURN


SELECT RecentFxRateDate
		,p.MarketDataExchangeRateID
		,p.FromCurrencySecurityID
		,p.ToCurrencySecurityID
		,p.MarketDataSourceID
		,p.RateDate
		,p.ExchangeRate
		,p.CreateUserID
		,p.CreateDate
		,p.UpdateUserID
		,p.UpdateDate

		,ProviderCompanyID = (SELECT BusinessCompanyID FROM dbo.BusinessCompany WHERE CompanyName = 'Goldman Sachs')

FROM CliftonIMS.dbo.MarketDataExchangeRate p
	INNER JOIN CliftonIMS.dbo.MarketDataSource ps ON p.MarketDataSourceID = ps.MarketDataSourceID AND DataSourceName = 'Goldman Sachs'
	INNER JOIN Datawarehouse.RecentFxRateDate(@MarketDate) r ON p.FromCurrencySecurityID = r.FromCurrencySecurityID AND  p.ToCurrencySecurityID = r.ToCurrencySecurityID AND RecentFxRateDate = RateDate

