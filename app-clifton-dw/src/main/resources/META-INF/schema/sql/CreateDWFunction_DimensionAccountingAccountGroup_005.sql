--DROP FUNCTION dbo.DimensionAccountingAccountGroup  

CREATE FUNCTION dbo.DimensionAccountingAccountGroup  (@AccountGroupName Varchar(100) )

RETURNS TABLE
AS

RETURN



SELECT 
	aagi.AccountGroupItemName
	,aagi.AccountGroupItemDescription
	,aagi.IsUnrealizedIncluded
	,aagi.AccountGroupItemOrder
   ,aaigiaa.AccountingAccountID
   ,aaigiaa.IsCreditMinusDebit

FROM CliftonIMS.dbo.AccountingAccountGroup aag
	INNER JOIN CliftonIMS.dbo.AccountingAccountGroupItem aagi ON aag.AccountingAccountGroupID = aagi.AccountingAccountGroupID
	INNER JOIN CliftonIMS.dbo.AccountingAccountGroupItemAccountingAccount aaigiaa ON aagi.AccountingAccountGroupItemID = aaigiaa.AccountingAccountGroupItemID

WHERE aag.AccountGroupName = @AccountGroupName
