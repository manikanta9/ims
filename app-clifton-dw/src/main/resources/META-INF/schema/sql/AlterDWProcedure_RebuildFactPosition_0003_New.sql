/*
	exec Datawarehouse.RebuildFactCurrentPosition 397, '12/31/09'
*/

ALTER PROC [Datawarehouse].[RebuildFactCurrentPosition]

      @ClientInvestmentAccountID INT
    , @ReportingDate DATETIME
AS 

DECLARE @BloombergCompanyID INT 
SET @BloombergCompanyID = (SELECT BusinessCompanyID FROM CliftonIMS.dbo.BusinessCompany WHERE CompanyName LIKE 'Bloomberg')


DECLARE @StartTime DATETIME
	, @EndTime DATETIME
	, @Duration DECIMAL(12,6)
SET @StartTime = GETDATE()


DECLARE 
	@LastQtrDate DATETIME
	,@LastYearDate DATETIME

SELECT 
	@LastQtrDate = m.LastQuarterEndDate
	,@LastYearDate = m.LastYearEndDate
FROM Datawarehouse.GetQuarterEndDates(@ReportingDate) m 



	--cash temp table added with the change of the base position query to use a different data source
	DECLARE @BaseCashPosition TABLE (ClientInvestmentAccountID INT, HoldingInvestmentAccountID INT, InvestmentSecurityID INT, MarketValueLocal DECIMAL(19,2), MarketValueBase DECIMAL(19,2))

		INSERT INTO @BaseCashPosition
		SELECT ClientInvestmentAccountID
			,HoldingInvestmentAccountID
			,t.InvestmentSecurityID
			,SUM(LocalDebitCredit)
			,SUM(BaseDebitCredit)
		FROM    dbo.AccountingTransaction t
				INNER JOIN dbo.InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID
				LEFT JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
				INNER JOIN dbo.AccountingAccount aa ON t.AccountingAccountID = aa.AccountingAccountID
		WHERE   ClientInvestmentAccountID = @ClientInvestmentAccountID
				AND ReportingDate <= @ReportingDate
				AND aa.IsCash = 1 AND IsPosition = 0 ---exclude currency
				--AND aa.AccountingAccountID = 100
		GROUP BY ClientInvestmentAccountID
			,HoldingInvestmentAccountID
			,t.InvestmentSecurityID


SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
--PRINT 'Create temp table @BaseCashPosition = ' +  STR(@Duration)
SET @StartTime = GETDATE()

DELETE FROM dbo.FactPositionSnapshot WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND SnapshotDate = @ReportingDate


SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
--PRINT 'delete FactPositionSnapshot: ' +  STR(@Duration)
SET @StartTime = GETDATE()



INSERT INTO dbo.FactPositionSnapshot
    ( SnapshotDate
        ,AccountingTransactionID
        ,ClientInvestmentAccountID
        ,HoldingInvestmentAccountID
        ,InvestmentSecurityID
        ,CurrencySecurityID
        ,AccountingAccountID
        ,SystemTableID
        ,FKFieldID
        ,GLDate
        ,TransactionDate
        ,OriginalTransactionDate
        ,SettlementDate
        ,MarketPrice
        ,OpenFXToBase
        ,MarketFXToBase
        ,MarketFXToUS
        ,Quantity
        ,QuantityRemaining
        ,CostBasisRemaining
        ,CostBasisRemainingBase
        ,MarketValueLocal
        ,MarketValueBase
        ,OpeningPrice
        ,OpenTradeEquity
        ,PriorOpenTradeEquity
		,CostBasis
		,BalanceSheetMarketValueBase
		
        ,LocalGainLossITD 
        ,LocalGainLossPTD 
        ,LocalGainLossYTD
        ,BaseGainLossITD 
        ,BaseGainLossPTD 
        ,BaseGainLossYTD 
		,MarketValueLastQuarterEnd
		,MarketValueLastYearEnd
		,NotionalValueLocal
		,AdjustedNotionalValueLocal

        )

/*
EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID = 19, @ReportingDate = '12/31/09'
*/

SELECT 

       p.PositionDate
       ,p.AccountingTransactionID
       ,p.ClientAccountID
       ,p.HoldingAccountID
       ,p.InvestmentSecurityID
       ,p.CurrencySecurityID
       ,p.AccountingAccountID
		,3 ---AccountingTransaction table
       ,p.AccountingTransactionID
       ,p.TradeDate
       ,p.TradeDate
       ,p.TradeDate
       ,p.SettleDate
       ,p.MarketPrice
       ,p.TradeFxRate
       ,p.MarketFxRate
       ,p.MarketFxRate
       ,p.TradeQuantity
       ,p.RemainingQuantity
       ,p.CostBasisLocal
       ,p.CostBasisBase
       ,p.MarketValueLocal
       ,p.MarketValueBase
       ,p.TradePrice
       ,p.OpenTradeEquityLocal
       ,p.PriorOpenTradeEquityLocal
       ,p.TradePrice * PriceMultiplier * p.TradeQuantity
       
		,BalanceSheetMarketValueBase = 0
		,LocalGainLossITD = 0
		,LocalGainLossPTD = 0
		,LocalGainLossYTD = 0
		,BaseGainLossITD = 0
		,BaseGainLossPTD = 0
		,BaseGainLossYTD = 0
		,MarketValueLastQuarterEnd = ISNULL(PLastQtr.MarketValueLocal, 0)
		,MarketValueLastYearEnd = ISNULL(PLastYr.MarketValueLocal, 0)
		,NotionalValueLocal = 0
		,AdjustedNotionalValueLocal = 0 

FROM CliftonIMS.dbo.AccountingPositionDaily p
	INNER JOIN CliftonIMS.dbo.InvestmentSecurity s ON p.InvestmentSecurityID = s.InvestmentSecurityID
	INNER JOIN CliftonIMS.dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	LEFT JOIN  CliftonIMS.dbo.AccountingPositionDaily PLastQtr ON PLastQtr.PositionDate = @LastQtrDate AND PLastQtr.AccountingTransactionID = p.AccountingTransactionID
	LEFT JOIN  CliftonIMS.dbo.AccountingPositionDaily PLastYr ON PLastYr.PositionDate = @LastYearDate AND PLastYr.AccountingTransactionID = p.AccountingTransactionID
WHERE p.ClientAccountID = @ClientInvestmentAccountID AND p.PositionDate = @ReportingDate


UPDATE fact
SET BalanceSheetMarketValueBase = CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END 
FROM dbo.FactPositionSnapshot fact 
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
		INNER JOIN CliftonIMS.dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID


WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 


SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
--PRINT 'Insert FactPositionSnapshot: ' +  STR(@Duration)
SET @StartTime = GETDATE()



EXEC Datawarehouse.RebuildFactIncomeStatement @ClientInvestmentAccountID, @ReportingDate 

SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
--PRINT 'RebuildFactIncomeStatement: ' +  STR(@Duration)
SET @StartTime = GETDATE()


/*


DECLARE @AccountNumber VARCHAR(100) = '107600'
	,@ClientInvestmentAccountID INT 
	,@PositionTransferDate DATETIME = '12/31/2008'
	,@ReportingDate DATETIME = '7/31/10'
	,@StartDate DATETIME = '7/1/10'
	,@Symbol VARCHAR(100) = 'CAD'
	
DECLARE @InvestmentSecurityID INT = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = @Symbol)
SELECT @ClientInvestmentAccountID = InvestmentAccountID FROM dbo.InvestmentAccount WHERE AccountNumber = @AccountNumber

Set @ClientInvestmentAccountID = 26 

Exec Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '12/31/09'

  


*/

