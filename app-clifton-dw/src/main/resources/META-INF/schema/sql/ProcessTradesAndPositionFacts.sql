
DECLARE 
	@StartTime DATETIME
	, @EndTime DATETIME
	, @Duration DECIMAL(12,6)

SET NOCOUNT ON 

DELETE FROM CliftonIMS.dbo.AccountingJournalDetail
DELETE FROM CliftonIMS.dbo.AccountingTransaction
DELETE FROM CliftonIMS.dbo.AccountingJournal
DELETE FROM dbo.FactPositionSnapshot
DELETE FROM dbo.FactTransaction

DECLARE @ClientInvestmentAccountID INT
	, @Rows DECIMAL(22,6) 
	, @AccountNumber VARCHAR(100)
	, @AccountName VARCHAR(100)

DECLARE ProcessTrades CURSOR FORWARD_ONLY FOR 
	
	SELECT DISTINCT ClientInvestmentAccountID,0, AccountNumber, AccountName
	FROM CliftonIMS.dbo.TradeAllocation ta
		INNER JOIN CliftonIMS.dbo.InvestmentAccount ia ON ta.ClientInvestmentAccountID = ia.InvestmentAccountID
	WHERE ClientInvestmentAccountID IN(27, 20)
	GROUP BY ClientInvestmentAccountID, AccountNumber, AccountName
	 
	UNION 
	
	SELECT DISTINCT ClientInvestmentAccountID,0, AccountNumber, AccountName
	FROM CliftonIMS.dbo.AccountingTransaction ta
		INNER JOIN CliftonIMS.dbo.InvestmentAccount ia ON ta.ClientInvestmentAccountID = ia.InvestmentAccountID
	WHERE ClientInvestmentAccountID IN(27, 20)
	GROUP BY ClientInvestmentAccountID, AccountNumber, AccountName
 

OPEN ProcessTrades
FETCH NEXT FROM ProcessTrades into @ClientInvestmentAccountID, @Rows, @AccountNumber, @AccountName
WHILE @@Fetch_Status = 0 
BEGIN
	SET @StartTime = GETDATE()
	PRINT '----------------------------------------------------------------------------------------------'
	PRINT 'Account ' + @AccountNumber + '-' + @AccountName + str(@ClientInvestmentAccountID) + '  - Rows to process: ' + STR(@Rows) +  ISNULL(' projected time to complete: ' +CONVERT(VARCHAR(20), @Rows * (@Rows / @Duration / 1000.)), '')
	
	EXEC CliftonIMS.Accounting.PostAllTrades @ClientInvestmentAccountID  
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '11/30/09'
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '12/31/09'
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '1/31/10'
	--EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '2/28/10'
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '3/31/10'
	--EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '4/30/10'
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, '5/31/10'

		SET @EndTime = GETDATE()
		SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
		PRINT  'Completed with duration: ' + CONVERT(VARCHAR(20), @Duration / 1000.) + ' seconds at a rows per second ratio of : ' + CONVERT(VARCHAR(20), @Rows / @Duration / 1000.)


FETCH NEXT FROM ProcessTrades into @ClientInvestmentAccountID, @Rows, @AccountNumber, @AccountName

END

CLOSE ProcessTrades
DEALLOCATE ProcessTrades
 

