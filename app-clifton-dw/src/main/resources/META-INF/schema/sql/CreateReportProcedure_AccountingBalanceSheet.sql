/*
exec Report.AccountingBalanceSheet  
drop proc Report.AccountingBalanceSheet
*/

CREATE PROC Report.AccountingBalanceSheet
		@ClientInvestmentAccountID INT = 187
		,@ReportingDate DATETIME = '12/31/09'  AS 


--EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, @ReportingDate

DECLARE @TotalMarketValue DECIMAL(28,2)

SET @TotalMarketValue = 
	(SELECT SUM(CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END)
		FROM dbo.FactPositionSnapshot fact 
			INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
			INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
			INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
			INNER JOIN CliftonIMS.dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
				LEFT JOIN CliftonIMS.dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
					LEFT JOIN CliftonIMS.dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
				INNER JOIN CliftonIMS.dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
					LEFT JOIN CliftonIMS.dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 

)


SELECT	
		a.AccountName
		,a.AccountNumber


		,ParentInstrumentGroupItemName =
			CASE WHEN aa.IsCash = 1 THEN s.Symbol + ' - ' + s.InvestmentSecurityName ELSE InvestmentTypeName END 

		,InstrumentGroupItemName = 
			CASE WHEN aa.IsCash = 1 THEN s.Symbol + ' - ' + s.InvestmentSecurityName ELSE InvestmentTypeName END 
		,InstrumentInvestmentCountry = CASE WHEN InvestmentTypeName = 'Currency' THEN 'Cash and Currency' ELSE 'Investments' END 

	    ,BaseCurrencySymbol = MAX(
				CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END )
		-----------------------------------------------------------------------------------------------------------------------------------------
		--Measures
		,MarketValueBase = SUM(MarketValueBase)
		,BalanceSheetCostBase = SUM(CASE WHEN InvestmentTypeName = 'Futures' THEN 0 ELSE fact.CostBasisRemainingBase END)
		,BalanceSheetMarketValueBase = SUM(CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END)
		,MarketValue = SUM(fact.MarketValueLocal)
		,UnrealizedGainLossBase =  SUM(CONVERT(DECIMAL(19,2), MarketValueBase - CostBasisRemainingBase))
		,TotalMarketValueBase = @TotalMarketValue
		,MarketValuePct = 
				SUM(CASE WHEN InvestmentTypeName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END)
				/@TotalMarketValue
		,MarketFXToBase = MAX(fact.MarketFXToBase)

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns

       
FROM dbo.FactPositionSnapshot fact 
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
		INNER JOIN CliftonIMS.dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
		LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
			LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 
GROUP BY 
		a.AccountName
		,a.AccountNumber
		,InvestmentTypeName
		,CASE WHEN aa.IsCash = 1 THEN s.Symbol + ' - ' + s.InvestmentSecurityName ELSE InvestmentTypeName END
