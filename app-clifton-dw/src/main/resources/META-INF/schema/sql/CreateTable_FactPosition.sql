--DROP TABLE [dbo].[FactPositionSnapshot]

CREATE TABLE [dbo].[FactPositionSnapshot](
	[FactPositionSnapshotID] [int] IDENTITY(1,1) NOT NULL,
	[SnapshotDate] [datetime] NULL,
	[AccountingTransactionID] [bigint] NULL,
	[ClientInvestmentAccountID] [int] NOT NULL,
	[HoldingInvestmentAccountID] [int] NOT NULL,
	[InvestmentSecurityID] [int] NOT NULL,
	[CurrencySecurityID] [int] NOT NULL,
	[AccountingAccountID] [int] NOT NULL,
	[SystemTableID] [int] NOT NULL,
	[FKFieldID] [int] NOT NULL,
	[IsInvestmentLocal] [bit] NULL,
	[GLDate] [datetime] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[OriginalTransactionDate] [datetime] NOT NULL,
	[SettlementDate] [datetime] NOT NULL,
	[OpeningPrice] [decimal](19, 10) NULL,
	[MarketPrice] [decimal](19, 10) NULL,
	[OpenFXToBase] [decimal](19, 10) NULL,
	[MarketFXToBase] [decimal](19, 10) NOT NULL,
	[MarketFXToUS] [decimal](19, 10) NOT NULL,

	[Quantity] [decimal](19, 10) NOT NULL,
	[QuantityRemaining] [decimal](19, 10) NULL,

	[CostBasis] [decimal](19, 2) NOT NULL,
	[CostBasisRemaining] [decimal](19, 2) NOT NULL,
	[CostBasisRemainingBase] [decimal](19, 2) NOT NULL,

	[MarketValueLocal] [decimal](19, 2) NOT NULL,
	[MarketValueBase] [decimal](19, 2) NOT NULL,

	[BalanceSheetMarketValueBase] [decimal](19, 2) NOT NULL,

	[LocalGainLossITD] [decimal](19, 2) NOT NULL,
	[LocalGainLossPTD] [decimal](19, 2) NOT NULL,
	[LocalGainLossYTD] [decimal](19, 2) NOT NULL,
	[BaseGainLossITD] [decimal](19, 2) NOT NULL,
	[BaseGainLossPTD] [decimal](19, 2) NOT NULL,
	[BaseGainLossYTD] [decimal](19, 2) NOT NULL,

	[MarketValueLastQuarterEnd] [decimal](19, 2) NOT NULL,
	[MarketValueLastYearEnd] [decimal](19, 2) NOT NULL,
	[NotionalValueLocal] [decimal](19, 2) NOT NULL,
	[AdjustedNotionalValueLocal] [decimal](19, 2) NOT NULL,
	[OpenTradeEquity] [decimal](19, 2) NOT NULL,
	[PriorOpenTradeEquity] [decimal](19, 2) NOT NULL,
 CONSTRAINT [PK_FactPositionSnapshot] PRIMARY KEY  NONCLUSTERED
(
	[FactPositionSnapshotID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]



--CREATE INDEX IX_FactPositionSnapshot_ClientInvestmentAccountIDSnapshotDate ON FactPositionSnapshot (ClientInvestmentAccountID, SnapshotDate)
CREATE CLUSTERED INDEX IX_FactPositionSnapshot_SnapshotDate ON FactPositionSnapshot (SnapshotDate, ClientInvestmentAccountID)

CREATE INDEX IX_FactPositionSnapshot_InvestmentSecurityID ON FactPositionSnapshot (InvestmentSecurityID)
CREATE INDEX IX_FactPositionSnapshot_CurrencySecurityID ON FactPositionSnapshot (CurrencySecurityID)
CREATE INDEX IX_FactPositionSnapshot_ClientInvestmentAccountID ON FactPositionSnapshot (ClientInvestmentAccountID)
CREATE INDEX IX_FactPositionSnapshot_HoldingInvestmentAccountID ON FactPositionSnapshot (HoldingInvestmentAccountID)
CREATE INDEX IX_FactPositionSnapshot_AccountingAccountID ON FactPositionSnapshot (AccountingAccountID)
