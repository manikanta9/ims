-- ON QA GIVE Wei Read ACCESS BACK TO Read DW, Andrew & Jared Full Access
-- AND EXECUTE PERMISSION TO FUNCTIONS HE NEEDS TO READ DATA
IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'db_executor')
	BEGIN
		CREATE ROLE db_executor
		GRANT EXECUTE TO db_executor
	END

IF NOT EXISTS(SELECT *
			  FROM sys.database_principals
			  WHERE name = 'PARAPORT\MPLS SQL QA Readers')
	BEGIN
		CREATE USER [PARAPORT\MPLS SQL QA Readers] FOR LOGIN [PARAPORT\MPLS SQL QA Readers];
	END
EXEC sp_addrolemember 'db_datareader', 'PARAPORT\MPLS SQL QA Readers';
EXEC sp_addrolemember 'db_executor', 'PARAPORT\MPLS SQL QA Readers';




