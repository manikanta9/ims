--DROP FUNCTION [Datawarehouse].[GetValidBusinessDate]

CREATE FUNCTION [Datawarehouse].[GetValidBusinessDate]	(

	 @ReportingDate DATETIME

) RETURNS DATETIME AS

--need to allow a lookup and a conversion from any currency to any base currency using a US exchange rate table

BEGIN


RETURN
	 
		(SELECT MAX(EndDate)
		FROM DimensionCalendar
		WHERE StartDate <= @ReportingDate AND IsBusinessDay = 1 )
END
