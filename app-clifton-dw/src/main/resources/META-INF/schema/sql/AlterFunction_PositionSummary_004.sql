ALTER FUNCTION [Datawarehouse].[PositionSummary]
    (
     
		@ClientInvestmentAccountID INT 
		--,@InvestmentSecurityID INT 
		--consider removing the reporting date
		,@ReportingDate DATETIME 
    )
RETURNS TABLE
AS

RETURN

SELECT 
    PostionID = opening.AccountingTransactionID
    ,SnapshotDate = @ReportingDate
    ,opening.AccountingTransactionID
    ,opening.AccountingAccountID
    ,ClientInvestmentAccountID = @ClientInvestmentAccountID
	,rollup.*
	,OpenDate = opening.OriginalTransactionDate
	,OpenPrice = ISNULL(opening.Price, 1)
	,CurrencySecurityID = CASE WHEN IsCash = 1 THEN s.InvestmentSecurityID ELSE ii.TradingCurrencyID END 
	,MarketPrice = CASE WHEN IsCash = 1 THEN MarketFxRate ELSE markprice.Price END 
	,CostBasisRemaining = NetCostBasis
	,CostBasisRemainingBase = CONVERT(DECIMAL(19,2), CASE WHEN IsCash = 1 THEN ISNULL(opening.Price, 1) * NetCostBasis ELSE CONVERT(DECIMAL(19,2), ISNULL(MarketFxToBase, 1) * NetCostBasis) END )
	
	,MarketValueLocal = CONVERT(DECIMAL(19,2), CASE WHEN IsCash = 1 THEN NetQuantity ELSE PriceMultiplier * isnull(markprice.Price, 1) * NetQuantity END )
	,MarketValueBase = 
				CONVERT(DECIMAL(19,2), CASE WHEN IsCash = 1 
				THEN CONVERT(DECIMAL(19,2), NetQuantity * MarketFxToBase) 
				ELSE (PriceMultiplier * ISNULL(markprice.Price, 1) * NetQuantity) * MarketFxToBase
				
				END )

	,OpenTradeEquityLocal = CONVERT(DECIMAL(19,2), (
					PriceMultiplier 
					* CASE WHEN IsCash = 1 THEN 1 ELSE markprice.Price END 
					* NetQuantity
					) - NetCostBasis)
					
	,OpenTradeEquityBase = CONVERT(DECIMAL(19,2), CASE WHEN IsCash = 1 
							--currency
							THEN CONVERT(DECIMAL(19,2), (NetQuantity * MarketFxToBase)) - CONVERT(DECIMAL(19,2), (ISNULL(opening.Price, 1) * NetCostBasis))
							--investments
							ELSE
								CONVERT(DECIMAL(28,2),(PriceMultiplier * markprice.Price * NetQuantity * MarketFxToBase))
									- CONVERT(DECIMAL(28,2),(NetCostBasis * MarketFxToBase))
							END )
	
	
FROM (
	-- moved this to a sub query so that rollup can happen prior to getting the opening transaction price and date - aggregations need to be separate from selection of specific opening record

			SELECT  
					ParentTransactionID = CASE WHEN t.IsOpening = 0 THEN t.ParentTransactionID ELSE t.AccountingTransactionID END 
					,t.HoldingInvestmentAccountID
					,s.InvestmentSecurityID

					,OpeningQuantity = sum(CASE WHEN IsOpening = 1 THEN CASE WHEN aa.IsCash = 1 THEN t.LocalDebitCredit ELSE t.Quantity END ELSE 0 END)
					,ClosingQuantity = sum(CASE WHEN IsOpening = 0 THEN CASE WHEN aa.IsCash = 1 THEN t.LocalDebitCredit ELSE t.Quantity END ELSE 0 END)
					,NetQuantity = SUM(CASE WHEN aa.IsCash = 1 THEN t.LocalDebitCredit ELSE t.Quantity END)

					,OpeningCostBasis = sum(CASE WHEN ParentTransactionID IS NULL THEN t.PositionCostBasis ELSE 0 END)
					,ClosingCostBasis = sum(CASE WHEN ParentTransactionID IS NOT NULL THEN t.PositionCostBasis ELSE 0 END)
					,NetCostBasis = SUM(t.PositionCostBasis)

					,TotalLots = COUNT(*)
					,ClosingLots = sum(CASE WHEN ParentTransactionID IS NOT NULL THEN 1 ELSE 0 END)
					,IsCash = aa.IsCash
					,IsPosition = aa.IsPosition
					,MarketFxRate = ISNULL(MAX(markcurrFX.ExchangeRate), 1)
					,MarketFxToBase = 
								MAX(ISNULL(CliftonDW.Datawarehouse.GetFXRate(markcurrFX.ExchangeRate, markbaseFX.ExchangeRate), 1))
							 
		           
			FROM    CliftonIMS.dbo.AccountingTransaction t
					INNER JOIN dbo.InvestmentAccount ca ON ca.InvestmentAccountID = t.ClientInvestmentAccountID
					INNER JOIN dbo.InvestmentAccount ha ON ha.InvestmentAccountID = t.HoldingInvestmentAccountID
					INNER JOIN dbo.InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID
					LEFT JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
					INNER JOIN dbo.AccountingAccount aa ON t.AccountingAccountID = aa.AccountingAccountID
					
					LEFT JOIN CliftonIMS.Datawarehouse.SettlementFxRate(@ReportingDate) markcurrFX ON markcurrFX.InvestmentSecurityID = CASE WHEN IsCash = 1 THEN s.InvestmentSecurityID ELSE ii.TradingCurrencyID END 
					LEFT JOIN CliftonIMS.Datawarehouse.SettlementFxRate(@ReportingDate) markbaseFX ON markbaseFX.InvestmentSecurityID = ca.BaseCurrencyID



					--LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markcurrFX ON 
					--		markcurrFX.FromCurrencySecurityID =  CASE WHEN IsCash = 1 THEN s.InvestmentSecurityID ELSE ii.TradingCurrencyID END 
					--		AND markcurrFX.ProviderCompanyID = ha.IssuingCompanyID
					--LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markbaseFX ON markbaseFX.FromCurrencySecurityID = ca.BaseCurrencyID AND markbaseFX.ProviderCompanyID = ha.IssuingCompanyID
		            
			WHERE   ClientInvestmentAccountID = @ClientInvestmentAccountID
					AND ReportingDate <= @ReportingDate
					AND aa.IsPosition = 1 
		            
			GROUP BY CASE WHEN t.IsOpening = 0 THEN t.ParentTransactionID ELSE t.AccountingTransactionID END , s.InvestmentSecurityID, aa.IsCash, aa.IsPosition, t.HoldingInvestmentAccountID
			HAVING SUM(t.PositionCostBasis) <> 0 OR SUM(t.Quantity) <> 0 


    ) rollup
		INNER JOIN dbo.InvestmentAccount ca ON ca.InvestmentAccountID = @ClientInvestmentAccountID
		INNER JOIN CliftonIMS.dbo.AccountingTransaction opening ON rollup.ParentTransactionID = opening.AccountingTransactionID
            INNER JOIN dbo.InvestmentSecurity s ON opening.InvestmentSecurityID = s.InvestmentSecurityID
            LEFT JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		--LEFT JOIN Datawarehouse.RecentPrice(@ReportingDate) markprice ON rollup.InvestmentSecurityID = markprice.InvestmentSecurityID 
		--LEFT JOIN dbo.FactSettlementPrice markprice ON rollup.InvestmentSecurityID = markprice.InvestmentSecurityID AND PriceDate = @ReportingDate
		LEFT JOIN CliftonIMS.Datawarehouse.SettlementPrice(@ReportingDate) markprice ON rollup.InvestmentSecurityID = markprice.InvestmentSecurityID 
		

  
	--LEFT JOIN CliftonIMS.Datawarehouse.SettlementPrice(@ReportingDate) markprice ON apd.InvestmentSecurityID = markprice.InvestmentSecurityID AND PriceDate = @ReportingDate
	--LEFT JOIN CliftonIMS.Datawarehouse.SettlementFxRate(@ReportingDate) markcurrFX ON markcurrFX.RateDate = @ReportingDate AND markcurrFX.InvestmentSecurityID = apd.CurrencySecurityID
	--LEFT JOIN CliftonIMS.Datawarehouse.SettlementFxRate(@ReportingDate) markbaseFX ON markbaseFX.RateDate = @ReportingDate AND markbaseFX.InvestmentSecurityID = ca.BaseCurrencyID
