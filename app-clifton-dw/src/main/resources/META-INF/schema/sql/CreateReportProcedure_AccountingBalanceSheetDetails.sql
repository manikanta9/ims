CREATE PROC report.BalanceSheetDetails

 
		@ClientInvestmentAccountID INT = 19
		,@ReportingDate DATETIME = '11/30/09'    
		
AS

--SELECT Symbol, * 
--FROM dbo.FactTransaction t
--	INNER JOIN CliftonIMS.dbo.InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID
--WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
--	AND TransactionDate <= @ReportingDate
--	AND Symbol = 'JPY'
--	AND AccountingAccountID IN (100)
--ORDER BY t.AccountingTransactionID



--RETURN		
		
DECLARE @TotalMarketValue DECIMAL(28,2)

SET @TotalMarketValue = 
	(SELECT SUM(CASE WHEN ic.InvestmentCategoryName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END)
		FROM dbo.FactPositionSnapshot fact 
					LEFT JOIN CliftonIMS.dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
						LEFT JOIN CliftonIMS.dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
					
				INNER JOIN CliftonIMS.dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
					LEFT JOIN CliftonIMS.dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
				INNER JOIN CliftonIMS.dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
					INNER JOIN CliftonIMS.dbo.InvestmentInstrument si ON s.InvestmentInstrumentID = si.InvestmentInstrumentID
						INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ih.InvestmentInstrumentHierarchy = si.InvestmentInstrumentHierarchy
						INNER JOIN CliftonIMS.dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
						LEFT JOIN CliftonIMS.dbo.InvestmentSecurity icsi ON fact.CurrencySecurityID = icsi.InvestmentSecurityID

		WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 
)


SELECT	
 
		s.Symbol
		,aa.AccountingAccountID 
		,AccountingTransactionID
		
		,fact.MarketFXToBase
		 
		,ParentInstrumentGroupItemName = ic.InvestmentCategoryName
	    ,InstrumentGroupItemName = it.InvestmentTypeName ---si.InvestmentInstrumentName
		,InstrumentInvestmentCountry = ic.InvestmentCategoryName

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Measures
		,MarketValueBase = (MarketValueBase)
		,BalanceSheetCostBase = (CASE WHEN ic.InvestmentCategoryName = 'Futures' THEN 0 ELSE fact.CostBasisRemainingBase END)
		,BalanceSheetMarketValueBase = (CASE WHEN ic.InvestmentCategoryName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END)
		,MarketValue = (fact.MarketValueLocal)
		,UnrealizedGainLossBase =  (CONVERT(DECIMAL(19,2), MarketValueBase - CostBasisRemainingBase))
		,TotalMarketValueBase = @TotalMarketValue
		,MarketValuePct = 
				(CASE WHEN ic.InvestmentCategoryName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END)
				/@TotalMarketValue


		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns

     
FROM dbo.FactPositionSnapshot fact 
			LEFT JOIN CliftonIMS.dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN CliftonIMS.dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
			
		INNER JOIN CliftonIMS.dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN CliftonIMS.dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		INNER JOIN CliftonIMS.dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
			INNER JOIN CliftonIMS.dbo.InvestmentInstrument si ON s.InvestmentInstrumentID = si.InvestmentInstrumentID
				INNER JOIN CliftonIMS.dbo.InvestmentInstrumentHierarchy ih ON ih.InvestmentInstrumentHierarchy = si.InvestmentInstrumentHierarchy
				INNER JOIN CliftonIMS.dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
				LEFT JOIN CliftonIMS.dbo.InvestmentSecurity icsi ON fact.CurrencySecurityID = icsi.InvestmentSecurityID

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 
	
ORDER BY InstrumentInvestmentCountry, ParentInstrumentGroupItemName, MarketValueBase 


