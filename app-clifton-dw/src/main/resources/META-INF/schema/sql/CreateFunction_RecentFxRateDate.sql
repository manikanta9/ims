CREATE FUNCTION Datawarehouse.RecentFxRateDate
    (
     
     @MarketDate DATETIME 
		 

    )
RETURNS TABLE
AS

RETURN


SELECT FromCurrencySecurityID, ToCurrencySecurityID, RecentFXRateDate = MAX(p.RateDate)
FROM CliftonIMS.dbo.MarketDataExchangeRate p
	INNER JOIN CliftonIMS.dbo.MarketDataSource ps ON p.MarketDataSourceID = ps.MarketDataSourceID AND DataSourceName = 'Goldman Sachs'
WHERE p.RateDate <= @MarketDate
GROUP BY FromCurrencySecurityID, ToCurrencySecurityID
