/*
DROP PROC [Report].[PositionLotTracking]  

SELECT * FROM dbo.InvestmentSecurity WHERE Symbol = 'CSJ'



*/

CREATE PROC [Report].[PositionLotTracking] 

		@ClientInvestmentAccountID INT = 397
		,@ReportingDate DATETIME = '12/31/10'   
		,@StartDate DATETIME = '1/1/09' 
		,@OpenInvestmentSecurityID INT = 13557 

--- use trade tables as a starting point

AS

	SELECT 

		s.Symbol
		,OpeningJournalID = Opening.AccountingJournalID
		,OpeningSymbol = openingsecurity.Symbol
		,s.InvestmentSecurityName
		,OpeningTransactionDescription = Opening.TransactionDescription
		,OpeningTransactionDate = Opening.TransactionDate
		,InvestmentCurrencyName = icsi.InvestmentSecurityName
		,BrokerageAccountName = brka.AccountName
		,BrokerageAccountNumber = brka.AccountNumber
		,BrokerageIssuerName = brkb.CompanyName

		,fact.ParentTransactionID
		,fact.FactTransactionID
	   ,fact.AccountingAccountID
	   ,fact.TransactionDescription
	   ,fact.AccountingJournalID
	   ,fact.AccountingTransactionID
	   ,fact.PositionTransactionID

	   ,si.PriceMultiplier
	   ,aa.AccountingAccountName
	   ,fact.Price
	   ,fact.Quantity
	   ,fact.LocalDebitCredit
	   ,fact.BaseDebitCredit
	   ,fact.PositionGross
	   ,fact.PositionCostBasis
	   ,fact.PositionCommission
	   ,RealizedGainLoss= fact.PositionRealized
	   ,RealizedGainLossBase = fact.PositionRealized * fact.FXRateToBase  
	   
	   ,fact.FXRateToBase
	   ,fact.IsSecurityPosition
	   ,fact.IsCurrencyPosition
	   ,fact.IsBaseCash
	   ,fact.IsBuy
	   ,IsOpening = CASE WHEN fact.IsOpening = 1 THEN 'O' ELSE 'C' END 
	   ,BSOC =	CASE WHEN aa.IsPosition = 0 THEN NULL ELSE 
				CASE WHEN fact.IsBuy = 1 THEN 'B' ELSE 'S' END +
				CASE WHEN fact.IsOpening = 1 THEN 'O' ELSE 'C' END END 
				
	   ,fact.OpeningDate
	   ,fact.OpeningPrice
	   ,fact.OpeningFXRate
	   ,fact.ClosingProportion
	   ,fact.GLDate
	   ,fact.TransactionDate
	   ,fact.OriginalTransactionDate
	   ,fact.ReportingDate
	   ,fact.SettlementDate

FROM dbo.FactTransaction fact 
			LEFT JOIN dbo.FactTransaction opening ON fact.PositionTransactionID = Opening.AccountingTransactionID
				LEFT JOIN dbo.InvestmentSecurity openingsecurity ON Opening.InvestmentSecurityID = openingsecurity.InvestmentSecurityID
				LEFT JOIN dbo.InvestmentSecurity os ON fact.InvestmentSecurityID = os.InvestmentSecurityID
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
			
		LEFT JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
		LEFT JOIN dbo.InvestmentAccount brka ON brka.InvestmentAccountID = fact.HoldingInvestmentAccountID
				LEFT JOIN dbo.BusinessCompany  brkb ON brka.IssuingCompanyID = brkb.BusinessCompanyID
		
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		LEFT JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
			LEFT JOIN dbo.InvestmentInstrument si ON s.InvestmentInstrumentID = si.InvestmentInstrumentID
		LEFT JOIN dbo.InvestmentSecurity icsi ON fact.CurrencySecurityID = icsi.InvestmentSecurityID

				
WHERE fact.TransactionDate <= @ReportingDate 
	AND fact.ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND aa.IsPosition = 1
	--AND aa.AccountingAccountID <> 105
	AND fact.InvestmentSecurityID = @OpenInvestmentSecurityID
ORDER BY Opening.TransactionDate, fact.PositionTransactionID, fact.TransactionDate


