
ALTER FUNCTION [Datawarehouse].[IncomeStatementRealizedCumulative]  (
     @ClientInvestmentAccountID INT = 223
    ,@ReportingDate DATETIME = '9/1/10'
	,@LastSnapshotDate DATETIME = '8/31/10'
    ) 

RETURNS TABLE AS  

RETURN

SELECT 
			SnapshotDate
	       ,ClientInvestmentAccountID
	       ,HoldingInvestmentAccountID
	       ,InvestmentSecurityID
	       ,CurrencySecurityID
	       ,AccountingAccountID
	       ,IncomeStatus
	       ,GainLossSinceInceptionLocal = SUM(GainLossSinceInceptionLocal)
	       ,GainLossSinceLastPeriodLocal = SUM(GainLossSinceLastPeriodLocal)
	       ,GainLossSinceLastYearEndLocal = SUM(GainLossSinceLastYearEndLocal)
	       ,GainLossSinceInceptionBase = SUM(GainLossSinceInceptionBase)
	       ,GainLossSinceLastPeriodBase = SUM(GainLossSinceLastPeriodBase)
	       ,GainLossSinceLastYearEndBase = SUM(GainLossSinceLastYearEndBase)
FROM (

	SELECT 
			SnapshotDate
			   ,ClientInvestmentAccountID
			   ,HoldingInvestmentAccountID
			   ,InvestmentSecurityID
			   ,CurrencySecurityID
			   ,fact.AccountingAccountID
			   ,IncomeStatus
			   ,GainLossSinceInceptionLocal
			   ,GainLossSinceLastPeriodLocal
			   ,GainLossSinceLastYearEndLocal
			   ,GainLossSinceInceptionBase
			   ,GainLossSinceLastPeriodBase
			   ,GainLossSinceLastYearEndBase
		       
		       
	FROM dbo.FactIncomeStatement fact
				LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
	WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID 
		AND fact.SnapshotDate = @LastSnapshotDate

UNION 

SELECT
		@ReportingDate
		,ClientInvestmentAccountID
		,HoldingInvestmentAccountID
		,fact.InvestmentSecurityID
		,fact.CurrencySecurityID
		--,aa.AccountingAccountName  
		,aa.AccountingAccountID
		,'Realized Gains or Losses'
		
		--sign is flipped because income statement credit entries that are stored negatively are positive income
		,-1 * fact.LocalDebitCredit  
		,-1 * CASE WHEN fact.TransactionDate >  m.LastQuarterEndDate THEN fact.LocalDebitCredit ELSE 0 END   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastYearEndDate THEN fact.LocalDebitCredit ELSE 0 END   
		
		,-1 * fact.BaseDebitCredit   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastQuarterEndDate THEN fact.BaseDebitCredit ELSE 0 END   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastYearEndDate THEN fact.BaseDebitCredit ELSE 0 END  


FROM dbo.FactTransaction fact
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
			LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
		
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN Datawarehouse.GetQuarterEndDates(@ReportingDate) m ON m.ReportingDate = @ReportingDate

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID 
	AND fact.TransactionDate <= @ReportingDate  
	AND fact.TransactionDate > @LastSnapshotDate
	AND aat.IsBalanceSheetAccount = 0



) alldata

GROUP BY 
			SnapshotDate
	       ,ClientInvestmentAccountID
	       ,HoldingInvestmentAccountID
	       ,InvestmentSecurityID
	       ,CurrencySecurityID
	       ,AccountingAccountID
	       ,IncomeStatus
