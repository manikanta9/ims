--DROP FUNCTION Datawarehouse.GetQuarterEndDates

CREATE FUNCTION Datawarehouse.GetQuarterEndDates
    (
     
     @ReportingDate DATETIME 

    )
RETURNS TABLE
AS

RETURN


SELECT 
		thisQ.QuarterID
		,thisQ.QuarterStartDate
		,thisQ.QuarterEndDate
		,LastQuarterStartDate = lastQ.QuarterStartDate
		,LastQuarterEndDate = lastQ.QuarterEndDate
		,thisQ.YearStartDate
		,thisQ.YearEndDate
		,LastYearStartDate = lastQ.YearStartDate
		,LastYearEndDate = DATEADD(day, -1, thisQ.YearStartDate)
		,ReportingDate = @ReportingDate
	

FROM dbo.CalendarQuarters thisQ
	LEFT JOIN dbo.CalendarQuarters lastQ ON DATEADD(day, -1, thisQ.QuarterStartDate) = lastQ.QuarterEndDate
WHERE @ReportingDate BETWEEN thisQ.QuarterStartDate AND thisQ.QuarterEndDate


/*


SELECT *
FROM Datawarehouse.GetQuarterEndDates('1/31/10')

*/
