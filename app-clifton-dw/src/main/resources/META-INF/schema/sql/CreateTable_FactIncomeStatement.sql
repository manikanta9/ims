--DROP TABLE  [dbo].[FactIncomeStatement]

CREATE TABLE [dbo].[FactIncomeStatement](
	[FactIncomeStatementID] [int] IDENTITY(1,1) NOT NULL,
	[SnapshotDate] [datetime] NOT NULL,
	[ClientInvestmentAccountID] [int] NOT NULL,
	[HoldingInvestmentAccountID] [int] NOT NULL,
	[InvestmentSecurityID] [int] NOT NULL,
	[CurrencySecurityID] [int] NOT NULL,
	[AccountingAccountID] [int] NOT NULL,
	[IncomeStatus] VARCHAR(100) NOT NULL,
	
	LocalGainLossITD [decimal](19, 2) NULL,
	LocalGainLossPTD [decimal](19, 2)  NULL,
	LocalGainLossYTD [decimal](19, 2)  NULL,
	
	BaseGainLossITD [decimal](19, 2)  NULL,
	BaseGainLossPTD [decimal](19, 2)  NULL,
	BaseGainLossYTD [decimal](19, 2)  NULL,

	
 CONSTRAINT [PK_FactIncomeStatement] PRIMARY KEY NONCLUSTERED 
(
	[FactIncomeStatementID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE INDEX IX_FactIncomeStatement_ClientInvestmentAccountIDSnapshotDate ON FactIncomeStatement (ClientInvestmentAccountID, SnapshotDate)
CREATE CLUSTERED INDEX IX_FactIncomeStatement_SnapshotDateClientInvestmentAccountID ON FactIncomeStatement (SnapshotDate, ClientInvestmentAccountID)
CREATE INDEX IX_FactIncomeStatement_InvestmentSecurityID ON FactIncomeStatement (InvestmentSecurityID)
CREATE INDEX IX_FactIncomeStatement_CurrencySecurityID ON FactIncomeStatement (CurrencySecurityID)
CREATE INDEX IX_FactIncomeStatement_ClientInvestmentAccountID ON FactIncomeStatement (ClientInvestmentAccountID)
CREATE INDEX IX_FactIncomeStatement_HoldingInvestmentAccountID ON FactIncomeStatement (HoldingInvestmentAccountID)
CREATE INDEX IX_FactIncomeStatement_AccountingAccountID ON FactIncomeStatement (AccountingAccountID)
CREATE INDEX IX_FactIncomeStatement_SnapshotDate ON FactIncomeStatement (SnapshotDate)
