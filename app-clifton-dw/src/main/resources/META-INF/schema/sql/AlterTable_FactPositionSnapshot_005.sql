
ALTER TABLE dbo.FactPositionSnapshot DROP COLUMN LocalGainLossITD
ALTER TABLE dbo.FactPositionSnapshot DROP COLUMN LocalGainLossPTD
ALTER TABLE dbo.FactPositionSnapshot DROP COLUMN LocalGainLossYTD

ALTER TABLE dbo.FactPositionSnapshot DROP COLUMN BaseGainLossITD
ALTER TABLE dbo.FactPositionSnapshot DROP COLUMN BaseGainLossPTD
ALTER TABLE dbo.FactPositionSnapshot DROP COLUMN BaseGainLossYTD

ALTER TABLE dbo.FactPositionSnapshot 
ADD AccruedIncomeLocal DECIMAL(19,2)
	, AccruedIncomeBase DECIMAL(19,2)



ALTER TABLE dbo.FactIncomeStatement ADD 
		[IsUnrealizedIncome] [bit] NOT NULL CONSTRAINT [DF_FactIncomeStatement_IsUnrealizedIncome] DEFAULT ((0))

--UPDATE dbo.FactIncomeStatement SET  IsUnrealizedIncome = 1 WHERE IncomeStatus LIKE '%Change in Unrealized Gains or Losses%'
