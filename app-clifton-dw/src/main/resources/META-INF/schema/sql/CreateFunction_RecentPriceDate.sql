CREATE FUNCTION Datawarehouse.RecentPriceDate
    (
     
     @MarketDate DATETIME 
		 

    )
RETURNS TABLE
AS

RETURN


SELECT InvestmentSecurityID, RecentPriceDate = MAX(p.MeasureDate)
FROM CliftonIMS.dbo.MarketDataValue p
	INNER JOIN CliftonIMS.dbo.MarketDataSource ps ON p.MarketDataSourceID = ps.MarketDataSourceID AND DataSourceName = 'Bloomberg'
	INNER JOIN CliftonIMS.dbo.MarketDataField pf ON p.MarketDataFieldID = pf.MarketDataFieldID AND DataFieldName = 'Settlement Price'
WHERE p.MeasureDate <= @MarketDate
GROUP BY InvestmentSecurityID
