--DROP PROC [Report].[AccountingCashBalanceSummary]

CREATE PROC [Report].[AccountingCashBalanceSummary]
--DECLARE 
		@ClientInvestmentAccountID INT = 190
		,@ReportingDate DATETIME = '1/31/10'
		,@StartDate DATETIME = '1/1/10' 

	 AS 

DECLARE @BalanceSummary TABLE (
		ClientInvestmentAccountID INT
		, HoldingInvestmentAccountID INT
		, AccountingAccountID INT 
		, CurrencySecurityID INT
		, OpeningBalanceAmount DECIMAL(28,2)
		, ActivityAmount DECIMAL(28,2)
		, EndingBalanceAmount DECIMAL(28,2)
		)

--get opening balance - assume all previous balances are the total
INSERT INTO @BalanceSummary
EXEC report.GetCashBalanceSummary @ClientInvestmentAccountID, @ReportingDate, @StartDate  -- not sure why this works  need research.

SELECT 
		CurrencySymbol = s.Symbol
		, CurrencyName = UPPER(s.InvestmentSecurityName)
		, a.AccountName
		, a.AccountNumber
		, aa.AccountingAccountName
		, ii.TradingCurrencyID
		, bs.OpeningBalanceAmount
		, bs.ActivityAmount
		, bs.EndingBalanceAmount
		, BaseEndingBalanceAmount = bs.EndingBalanceAmount * ISNULL(Datawarehouse.GetFXRate(markcurrFX.ExchangeRate, markbaseFX.ExchangeRate), 1)
		, MarketFXToBase = ISNULL(Datawarehouse.GetFXRate(markcurrFX.ExchangeRate, markbaseFX.ExchangeRate), 1)
		 
		, InstrumentCurrencySymbol = CASE s.Symbol
			WHEN 'JPY' THEN CHAR(165)
			WHEN 'EUR' THEN CHAR(128)
			ELSE CHAR(36) END 
		, BaseCurrencySymbol = CASE Base.Symbol
			WHEN 'JPY' THEN CHAR(165)
			WHEN 'EUR' THEN CHAR(128)
			ELSE CHAR(36) END 

		, HoldingAccountName = ha.AccountName
		, HoldingAccountNumber = ha.AccountNumber
		, hat.AccountTypeName
		

FROM @BalanceSummary bs
	INNER JOIN dbo.AccountingAccount aa ON bs.AccountingAccountID = aa.AccountingAccountID
	INNER JOIN dbo.InvestmentAccount ha ON bs.HoldingInvestmentAccountID = ha.InvestmentAccountID
	
		INNER JOIN dbo.InvestmentAccountType hat ON ha.InvestmentAccountTypeID = hat.InvestmentAccountTypeID
	INNER JOIN dbo.InvestmentSecurity s ON bs.CurrencySecurityID = s.InvestmentSecurityID
	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = bs.ClientInvestmentAccountID
			INNER JOIN dbo.InvestmentAccount brka ON bs.HoldingInvestmentAccountID = brka.InvestmentAccountID
		LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		

					LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markcurrFX ON 
							markcurrFX.FromCurrencySecurityID =  CASE WHEN aa.IsCash = 1 THEN s.InvestmentSecurityID ELSE ii.TradingCurrencyID END 
							AND markcurrFX.ProviderCompanyID = ha.IssuingCompanyID
					LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markbaseFX ON markbaseFX.FromCurrencySecurityID = a.BaseCurrencyID AND markbaseFX.ProviderCompanyID = ha.IssuingCompanyID


		--LEFT JOIN Datawarehouse.RecentPrice(@ReportingDate) markbaseFX ON markbaseFX.InvestmentSecurityID = a.BaseCurrencyID AND markbaseFX.ProviderCompanyID = brka.IssuingCompanyID
		--LEFT JOIN Datawarehouse.RecentPrice(@ReportingDate) markcurrFX ON markcurrFX.InvestmentSecurityID = si.TradingCurrencyID AND markcurrFX.ProviderCompanyID = brka.IssuingCompanyID

WHERE 

		bs.OpeningBalanceAmount + bs.ActivityAmount + bs.EndingBalanceAmount <> 0

ORDER BY s.Symbol



/*
exec Report.AccountingCashBalanceSummary

*/

