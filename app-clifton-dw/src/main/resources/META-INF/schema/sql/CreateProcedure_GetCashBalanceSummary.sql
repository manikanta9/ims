--DROP PROC report.GetCashBalanceSummary


CREATE PROC report.GetCashBalanceSummary
		@ClientInvestmentAccountID INT = 207
		,@ReportingDate DATETIME = '1/1/09'   
		,@StartDate DATETIME = '12/31/09' 

	 AS 

--SET @StartDate = '1/1/1910'

DECLARE @BalanceSummary TABLE (
		ClientInvestmentAccountID INT
		, HoldingInvestmentAccountID INT
		, AccountingAccountID INT 
		, CurrencySecurityID INT
		, OpeningBalanceAmount DECIMAL(28,2)
		, ActivityAmount DECIMAL(28,2)
		, EndingBalanceAmount DECIMAL(28,2)
		)

--get opening balance - assume all previous balances are the total
INSERT INTO @BalanceSummary
SELECT 
		ClientInvestmentAccountID
		, HoldingInvestmentAccountID
		, fact.AccountingAccountID
		, CurrencySecurityID
		, OpeningBalance = SUM(LocalDebitCredit)
		, ActivityAmount = 0
		, EndingAmount = 0

FROM dbo.FactTransaction fact
	INNER JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID
		AND fact.TransactionDate < @StartDate  
		AND aa.IsCash = 1
GROUP BY ClientInvestmentAccountID, CurrencySecurityID, HoldingInvestmentAccountID, fact.AccountingAccountID

INSERT INTO @BalanceSummary
SELECT 
		fact.ClientInvestmentAccountID
		, fact.HoldingInvestmentAccountID
		, fact.AccountingAccountID
		, fact.CurrencySecurityID
		, OpeningBalance = 0
		, ActivityAmount = 0
		, EndingAmount = 0
FROM dbo.FactTransaction fact
	INNER JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
	LEFT JOIN @BalanceSummary bs ON aa.AccountingAccountID = bs.AccountingAccountID
WHERE  fact.ClientInvestmentAccountID = @ClientInvestmentAccountID
		AND aa.IsCash = 1
		AND bs.AccountingAccountID IS NULL 
GROUP BY fact.ClientInvestmentAccountID, fact.CurrencySecurityID, fact.HoldingInvestmentAccountID, fact.AccountingAccountID

--get activity 
UPDATE bs SET
	 ActivityAmount = ISNULL(TotalActivityAmount, 0)
		, EndingBalanceAmount = OpeningBalanceAmount + ISNULL(TotalActivityAmount, 0)

FROM @BalanceSummary bs 
LEFT JOIN 
(
	SELECT ClientInvestmentAccountID, CurrencySecurityID, HoldingInvestmentAccountID, fact.AccountingAccountID
			, TotalActivityAmount = SUM(LocalDebitCredit)
	FROM dbo.FactTransaction fact
		INNER JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
	WHERE  ClientInvestmentAccountID = @ClientInvestmentAccountID
			AND fact.TransactionDate BETWEEN @StartDate AND @ReportingDate 
					AND aa.IsCash = 1
	GROUP BY ClientInvestmentAccountID, CurrencySecurityID, HoldingInvestmentAccountID, fact.AccountingAccountID) ActivitySummary
		ON bs.ClientInvestmentAccountID = ActivitySummary.ClientInvestmentAccountID
		AND bs.CurrencySecurityID = ActivitySummary.CurrencySecurityID
		AND bs.HoldingInvestmentAccountID = ActivitySummary.HoldingInvestmentAccountID
		AND bs.AccountingAccountID = ActivitySummary.AccountingAccountID


SELECT * FROM @BalanceSummary
