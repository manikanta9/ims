--DROP PROC [Report].[AccountingIncomeStatement]

ALTER PROC [Report].[AccountingIncomeStatement]
		@ClientInvestmentAccountID INT = 397
		,@ReportingDate DATETIME = '12/31/09'   
		,@StartDate DATETIME = '11/1/09' 

AS


SELECT 
	gl.ClientInvestmentAccountID
	,ReportingDate = @ReportingDate
	,aa.AccountingAccountName
	,IncomeStatus = CASE WHEN IsUnrealizedIncome = 1 THEN 'Change in Unrealized Gains or Losses' ELSE 
		ISNULL(ag.AccountGroupItemName, 'Realized Gains or Losses' )
		END 
	,gl.InvestmentSecurityID
	,gl.CurrencySecurityID

	,gl.GainLossSinceInceptionLocal
	,gl.GainLossSinceLastPeriodLocal
	,gl.GainLossSinceLastYearEndLocal
	,gl.GainLossSinceInceptionBase
	,gl.GainLossSinceLastPeriodBase
	,gl.GainLossSinceLastYearEndBase
	,s.Symbol
	,s.InvestmentSecurityName

	--Organizational Columns
	,AccountName
	,AccountNumber
		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns

		,ParentInstrumentGroupItemName =
				CASE WHEN aa.IsCash = 1 THEN s.Symbol + ' - ' + s.InvestmentSecurityName ELSE s.GroupItemName END 
		
	   ,InstrumentGroupItemName = 
				CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.Symbol + ' - ' + s.InvestmentSecurityName ELSE s.GroupItemName END 
		,InstrumentInvestmentCountry = CASE WHEN s.GroupItemName = 'Currency' THEN 'Cash and Currency' ELSE 'Investments' END 
		,ReportOrderParent = 'All'
		,ReportOrderChild = 'All'

		,BaseLanguage = CASE base.Symbol
						WHEN 'JPY' THEN 'Japanese'
						ELSE 'default' END 

		,BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 

		,InstrumentCurrencySymbol = CASE 
				(CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.Symbol ELSE s.CurrencySymbol END)
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 

		 
		,CurrencySymbol = s.CurrencySymbol
		,IsCurrency = CASE WHEN aa.AccountingAccountID IN (100) THEN 1 ELSE 0 END 
		,InstrumentCode = s.IdentifierPrefix
		,InstrumentName = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.InvestmentSecurityName ELSE s.InvestmentInstrumentName END 
		,IsBaseCurrency = CASE WHEN s.CurrencySymbol = base.Symbol THEN 1 ELSE 0 END 
		,BaseCurrencyDescr = ''
		,MatureDate = s.EndDate
		-----------------------------------------------------------------------------------------------------------------------------------------



	
FROM  dbo.FactIncomeStatement gl
	INNER JOIN dbo.AccountingAccount aa ON aa.AccountingAccountID = gl.AccountingAccountID
	LEFT JOIN dbo.DimensionAccountingAccountGroup ('Portfolio Performance Summary') ag ON ag.AccountingAccountID = aa.AccountingAccountID
	INNER JOIN dbo.InvestmentSecurity cs ON gl.CurrencySecurityID = cs.InvestmentSecurityID  --currency
	INNER JOIN Report.InvestmentMatrixView (Datawarehouse.GetClientReportingGroupID(@ClientInvestmentAccountID)) s ON gl.InvestmentSecurityID = s.InvestmentSecurityID

	INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = gl.ClientInvestmentAccountID
		LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
WHERE gl.ClientInvestmentAccountID = @ClientInvestmentAccountID
	AND gl.SnapshotDate = @ReportingDate

ORDER BY InstrumentGroupItemName, s.Symbol--, OriginalTransactionDate, AccountingTransactionID
	
