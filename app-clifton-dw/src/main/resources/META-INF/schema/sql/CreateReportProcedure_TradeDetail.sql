/*
DROP PROC [Report].[TradeDetails] 



SELECT * FROM dbo.InvestmentSecurity WHERE Symbol = 'ESH0'

exec [Report].[TradeDetails]

*/

CREATE PROC [Report].[TradeDetails] 

		@ClientInvestmentAccountID INT = 221
		,@ReportingDate DATETIME = '12/31/10'   
		,@StartDate DATETIME = '11/1/09' 
		,@OpenInvestmentSecurityID INT = 12808 

--- use trade tables as a starting point

AS


	SELECT 

		s.Symbol
		,s.InvestmentSecurityName
		,InvestmentCurrencyName = icsi.InvestmentSecurityName

		,HoldingAccountName = holding.AccountName
	   ,si.PriceMultiplier
	   
	   
	   ,IsOpening  = CASE WHEN taf.IsOpen = 1 THEN 'O' ELSE 'C' END 
	   ,BSOC =	 
				CASE WHEN t.IsBuy = 1 THEN 'B' ELSE 'S' END +
				CASE WHEN taf.IsOpen = 1 THEN 'O' ELSE 'C' END  
		,Quantity = CASE WHEN IsBuy =1 THEN Quantity ELSE -Quantity END 

		,t.TradeDescription
		,t.TradeDate
		,t.SettlementDate
		,t.ReportingDate
		,t.MigrationSourceKey
		,t.QuantityIntended
		,tt.TradeTypeName
		,ta.TradeAllocationID
		,ta.TradeAllocationDescription
		,ta.CommissionPerUnit
		,ta.FeeAmount

		,taf.Quantity
		,taf.PaymentUnitPrice
		,taf.PaymentTotalPrice
		,taf.NotionalUnitPrice
		,taf.NotionalTotalPrice
		,taf.Note
		,taf.FXRateToBase
		,taf.JournalBookingDate
		,taf.BasePaymentUnitPrice
		,taf.BaseNotionalUnitPrice
		,taf.IsOpen

FROM CliftonIMS.dbo.TradeAllocationFill taf
		INNER JOIN CliftonIMS.dbo.TradeAllocation ta ON taf.TradeAllocationID = ta.TradeAllocationID
		INNER JOIN CliftonIMS.dbo.Trade t ON ta.TradeID = t.TradeID
			INNER JOIN CliftonIMS.dbo.TradeType tt ON t.TradeTypeID = tt.TradeTypeID


				LEFT JOIN dbo.InvestmentSecurity os ON t.InvestmentSecurityID = os.InvestmentSecurityID
			
		LEFT JOIN dbo.InvestmentAccount client ON client.InvestmentAccountID = ta.ClientInvestmentAccountID
		LEFT JOIN dbo.InvestmentAccount holding ON holding.InvestmentAccountID = ta.HoldingInvestmentAccountID
				LEFT JOIN dbo.BusinessCompany  brkb ON holding.IssuingCompanyID = brkb.BusinessCompanyID
		
			LEFT JOIN dbo.InvestmentSecurity base ON client.BaseCurrencyID = base.InvestmentSecurityID
		LEFT JOIN dbo.InvestmentSecurity s ON t.InvestmentSecurityID = s.InvestmentSecurityID
			LEFT JOIN dbo.InvestmentInstrument si ON s.InvestmentInstrumentID = si.InvestmentInstrumentID
				LEFT JOIN dbo.InvestmentInstrumentHierarchy ih ON ih.InvestmentInstrumentHierarchyID = si.InvestmentInstrumentHierarchyID
				LEFT JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
		LEFT JOIN dbo.InvestmentSecurity icsi ON si.TradingCurrencyID = icsi.InvestmentSecurityID

WHERE t.TradeDate BETWEEN @StartDate AND @ReportingDate 
	AND ta.ClientInvestmentAccountID = @ClientInvestmentAccountID

