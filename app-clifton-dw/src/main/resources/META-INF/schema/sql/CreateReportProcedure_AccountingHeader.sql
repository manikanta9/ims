--DROP PROC [Report].[AccountingHeader] 


CREATE PROC [Report].[AccountingHeader] 

	@ClientInvestmentAccountID INT = 19
	,@ReportingDate DATETIME = '11/1/09'
	,@StartDate DATETIME = '12/31/09'

AS


/*
exec Report.AccountingHeader 1
*/
--IF NOT EXISTS (SELECT * FROM dbo.FactPositionSnapshot WHERE SnapshotDate = @ReportingDate)
--EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, @ReportingDate 


SELECT 

	a.InvestmentAccountID
	,a.AccountNumber
	,a.AccountName
	,a.InvestmentAccountTypeID
	, CASE WHEN ws.WorkflowStatusName = 'Active' THEN 1 ELSE 0 END AS 'IsActive'
	,client.InceptionDate
	,a.BaseCurrencyID
	, BaseCurrencyDescr = s.InvestmentSecurityDescription
	, BaseCurrencyExchangeRate = Datawarehouse.GetFXRate(markbaseFX.ExchangeRate, 1)
	, BaseLanguage = CASE s.Symbol
					WHEN 'JPY' THEN 'Japanese'
					ELSE 'default' END 
	,BaseCurrencySymbol = CASE s.Symbol
					WHEN 'JPY' THEN CHAR(165)
					WHEN 'EUR' THEN CHAR(128)
					ELSE CHAR(36) END 
 
	FROM dbo.InvestmentAccount a 
		INNER JOIN dbo.WorkflowStatus ws ON ws.WorkflowStatusID = a.WorkflowStatusID
		INNER JOIN dbo.BusinessClient client ON a.BusinessClientID = client.BusinessClientID

		LEFT JOIN dbo.InvestmentSecurity s ON a.BaseCurrencyID = s.InvestmentSecurityID
		LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markbaseFX ON markbaseFX.FromCurrencySecurityID = a.BaseCurrencyID 
				--for account level base exchange rate lookups we are using Goldman rates
				AND markbaseFX.ProviderCompanyID = (SELECT BusinessCompanyID FROM BusinessCompany WHERE CompanyName = 'Goldman Sachs')
					
		
	WHERE a.InvestmentAccountID = @ClientInvestmentAccountID 
	

