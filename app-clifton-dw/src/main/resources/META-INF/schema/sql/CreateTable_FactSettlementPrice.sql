--DROP TABLE  [dbo].[FactSettlementPrice]
/*
SELECT v.*
FROM dbo.MarketDataValue v
INNER JOIN dbo.MarketDataField f ON v.MarketDataFieldID = f.MarketDataFieldID
WHERE DataFieldName = 'Settlement Price'
*/

CREATE TABLE [dbo].[FactSettlementPrice](
	[FactSettlementPriceID] [int] IDENTITY(1,1) NOT NULL,
	InvestmentSecurityID INT ,
	[PriceDate] DATETIME NOT NULL,
	[Price] NUMERIC(28,12) NOT NULL

 CONSTRAINT [PK_FactSettlementPrice] PRIMARY KEY NONCLUSTERED 
(
	[FactSettlementPriceID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]


CREATE UNIQUE INDEX IX_FactSettlementPrice_DateSecurity ON FactSettlementPrice (InvestmentSecurityID, PriceDate)
CREATE CLUSTERED INDEX IX_FactSettlementPrice_InvestmentSecurityID ON FactSettlementPrice (InvestmentSecurityID)
CREATE INDEX IX_FactSettlementPrice_PriceDate ON FactSettlementPrice (PriceDate)
