/*
	drop  PROC  [Datawarehouse].[RebuildFactTransaction]
	exec [Datawarehouse].[RebuildFactTransaction] 1636

*/

CREATE  PROC  [Datawarehouse].[RebuildFactTransaction]

      @AccountingJournalID INT

AS 

--PRINT 'Sam I am: ' + STR(@AccountingJournalID)

	
DELETE FROM dbo.FactTransaction WHERE AccountingJournalID = @AccountingJournalID

INSERT INTO dbo.FactTransaction
        ( AccountingJournalID
        ,AccountingTransactionID
        ,ParentTransactionID
        ,PositionTransactionID
        ,ClientInvestmentAccountID
        ,HoldingInvestmentAccountID
        ,ReportingInvestmentAccountID
        ,InvestmentSecurityID
        ,CurrencySecurityID
        ,AccountingAccountID
        ,SystemTableID
        ,FKFieldID
        ,Price
        ,Quantity
        ,LocalDebitCredit
        ,BaseDebitCredit
        ,PositionGross
        ,PositionCostBasis
        ,PositionCommission
        ,PositionCommissionCombined
        ,PositionRealized
        ,PositionRealizedNet
        ,FXRateToBase
        ,IsSecurityPosition
        ,IsCurrencyPosition
        ,IsBaseCash
        ,IsBuy
        ,IsOpening
        ,IsCustodial

        ,OpeningDate
        ,OpeningPrice
        ,OpeningFXRate
        ,ClosingProportion
        ,GLDate
        ,TransactionDate
        ,OriginalTransactionDate
        ,ReportingDate
        ,SettlementDate
        ,TransactionDescription
        ,CreateUserID
        ,CreateDate
        )

SELECT t.AccountingJournalID

		,t.AccountingTransactionID
		,t.ParentTransactionID
		,PositionTransactionID = CASE WHEN t.IsOpening = 0 THEN t.ParentTransactionID ELSE t.AccountingTransactionID END
		,t.ClientInvestmentAccountID
		,t.HoldingInvestmentAccountID
		,ReportingInvestmentAccountID = ISNULL(case when isPosition = 1 then iar.RelatedAccountID ELSE t.HoldingInvestmentAccountID end, t.HoldingInvestmentAccountID)
		,t.InvestmentSecurityID
		,CurrencyID = COALESCE(isci.InvestmentSecurityID, ii.TradingCurrencyID, t.InvestmentSecurityID) 
		,t.AccountingAccountID
		,t.SystemTableID
		,t.FKFieldID
		,CASE WHEN aa.AccountingAccountID = 100 THEN NULL ELSE t.Price END 
		,Quantity  = CASE WHEN aa.AccountingAccountID = 105 THEN NULL ELSE t.Quantity END 
		,t.LocalDebitCredit
		,t.BaseDebitCredit
		,t.PositionGross
		,t.PositionCostBasis
		,t.PositionCommission
		,PositionCommissionCombined = 
				CASE WHEN aa.IsPosition = 1 AND t.IsOpening = 0 THEN t.PositionCommission + ISNULL((abs(t.Quantity / pt.Quantity) * pt.PositionCommission), 0) ELSE 0 END 
		,t.PositionRealized
		,PositionRealizedNet = t.PositionRealized + 
				CASE WHEN aa.IsPosition = 1 AND t.IsOpening = 0 THEN t.PositionCommission + ISNULL((abs(t.Quantity / pt.Quantity) * pt.PositionCommission), 0) ELSE 0 END 
		,t.FXRateToBase

		,IsSecurityPosition = CASE WHEN aa.IsPosition = 1 AND t.AccountingAccountID = 105 THEN 1 ELSE 0 END 
		,IsCurrencyPosition = CASE WHEN aa.IsPosition = 1 AND t.AccountingAccountID = 120 THEN 1 ELSE 0 END 
		,IsBaseCash = CASE WHEN t.AccountingAccountID = 100 THEN 1 ELSE 0 END 
		,IsBuy = CASE WHEN t.Quantity > 0 THEN 1 ELSE 0 END 
		,t.IsOpening
		,CASE WHEN isPosition = 1 OR hat.AccountTypeName NOT LIKE '%broker%' THEN 1 ELSE 0 END 

		,pt.OriginalTransactionDate
		,pt.Price
		,pt.FXRateToBase
		,ClosingProportion = t.Quantity / pt.Quantity

		,t.GLDate
		,t.TransactionDate
		,t.OriginalTransactionDate
		,t.ReportingDate
		,t.SettlementDate
		,t.TransactionDescription
		,CreateUserID = 0
		,CreateDate = GETDATE()

FROM dbo.AccountingTransaction t
	INNER JOIN dbo.AccountingAccount aa ON t.AccountingAccountID = aa.AccountingAccountID
	INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = t.ClientInvestmentAccountID

	INNER JOIN dbo.InvestmentAccount ha ON  ha.InvestmentAccountID = t.HoldingInvestmentAccountID
		INNER JOIN dbo.InvestmentAccountType hat ON hat.InvestmentAccountTypeID = ha.InvestmentAccountTypeID
	LEFT JOIN dbo.InvestmentAccountRelationship iar ON iar.MainAccountID = ha.InvestmentAccountID
		LEFT JOIN CliftonIMS.dbo.InvestmentAccountRelationshipPurpose iarp ON iar.InvestmentAccountRelationshipPurposeID = iarp.InvestmentAccountRelationshipPurposeID AND RelationshipPurposeName =  'Mark to Market'

		INNER JOIN dbo.InvestmentSecurity si ON t.InvestmentSecurityID = si.InvestmentSecurityID
		INNER JOIN dbo.InvestmentInstrument ii ON si.InvestmentInstrumentID = ii.InvestmentInstrumentID
			LEFT JOIN dbo.InvestmentSecurity isci ON ii.TradingCurrencyID = isci.InvestmentSecurityID
	LEFT JOIN dbo.AccountingTransaction pt ON t.ParentTransactionID = pt.AccountingTransactionID
	
WHERE t.AccountingJournalID = @AccountingJournalID


