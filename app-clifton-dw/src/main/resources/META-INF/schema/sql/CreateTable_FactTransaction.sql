--DROP TABLE [dbo].[FactTransaction]

CREATE TABLE [dbo].[FactTransaction](
	[FactTransactionID] [bigint] IDENTITY(1,1) NOT NULL,
	[AccountingJournalID] [bigint] NOT NULL,
	[AccountingTransactionID] [bigint] NOT NULL,
	[ParentTransactionID] [bigint] NULL,
	[PositionTransactionID] [bigint] NULL,
	[ClientInvestmentAccountID] [int] NOT NULL,
	[HoldingInvestmentAccountID] [int] NOT NULL,
	[ReportingInvestmentAccountID] [int] NOT NULL,
	[InvestmentSecurityID] [int] NOT NULL,
	[CurrencySecurityID] [int] NOT NULL,
	[AccountingAccountID] [int] NOT NULL,
	[SystemTableID] [int] NOT NULL,
	[FKFieldID] [int] NOT NULL,
	[Price] [decimal](19, 10) NULL,
	[Quantity] [decimal](19, 10) NULL,
	[LocalDebitCredit] [decimal](19, 2) NOT NULL,
	[BaseDebitCredit] [decimal](19, 2) NOT NULL,
	[PositionGross] [decimal](19, 2) NULL,
	[PositionCostBasis] [decimal](19, 2) NULL,
	[PositionCommission] [decimal](19, 2) NULL,
	[PositionCommissionCombined] [decimal](19, 2) NULL,
	
	[PositionRealized] [decimal](19, 2) NULL,
	[PositionRealizedNet] [decimal](19, 2) NULL,
	[FXRateToBase] [decimal](19, 10) NOT NULL,
	[IsSecurityPosition] [bit] NULL,
	[IsCurrencyPosition] [bit] NULL,
	[IsBaseCash] [bit] NULL,
	[IsBuy] [bit] NULL,
	[IsOpening] [bit] NULL,
	IsCustodial [bit] NULL,
	
	[OpeningDate] [datetime] NULL,
	[OpeningPrice] [decimal](19, 10) NULL,
	[OpeningFXRate] [decimal](19, 10) NULL,
	[ClosingProportion] [decimal](19, 10) NULL,
	[GLDate] [datetime] NOT NULL,
	[TransactionDate] [datetime] NOT NULL,
	[OriginalTransactionDate] [datetime] NOT NULL,
	[ReportingDate] [datetime] NOT NULL,
	[SettlementDate] [datetime] NOT NULL,
	[TransactionDescription] [nvarchar](500) NULL,
	[CreateUserID] [int] NOT NULL,
	[CreateDate] [datetime] NOT NULL,
 CONSTRAINT [PK_FactTransaction] PRIMARY KEY CLUSTERED 
(
	[FactTransactionID] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

