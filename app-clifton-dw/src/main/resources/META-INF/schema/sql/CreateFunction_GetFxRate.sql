CREATE FUNCTION [Datawarehouse].[GetFXRate]	(

	 @CurrencyRateToUS Decimal(28,12)
	,@BaseCurrencyRateToUS Decimal(28,12)

) RETURNS Decimal(28,12) AS

--need to allow a lookup and a conversion from any currency to any base currency using a US exchange rate table

BEGIN

DECLARE
	  @FXRate Decimal(28,12)

RETURN
	 
	 @CurrencyRateToUS / @BaseCurrencyRateToUS 
 
END
