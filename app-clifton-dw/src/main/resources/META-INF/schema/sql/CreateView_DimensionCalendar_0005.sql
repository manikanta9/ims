ALTER  VIEW DimensionCalendar AS 


SELECT c.CalendarID
	   ,d.CalendarDayID
       ,d.CalendarYearID
       ,d.CalendarMonthID
       ,d.CalendarWeekdayID
       ,d.DayNumberInMonth
       ,d.DayNumberInYear
       ,d.StartDate
       ,d.EndDate
       ,IsHolidayDay = CASE WHEN hd.CalendarHolidayDayID IS NULL THEN 0 ELSE 1 END 
       ,IsBusinessDay = CASE WHEN hd.CalendarHolidayDayID IS NULL AND CalendarWeekdayID NOT IN (1,7) THEN 1 ELSE 0 END 
       ,h.HolidayName
       ---,ValidBusinessDate =[Datawarehouse].[GetValidDate](d.EndDate)
       --- the above is elegant but very slow since it is self referencing
       
       ,LastQuarterEndDate = CONVERT(DATE,
			CASE CalendarMonthID
			WHEN 1 THEN '12/31/' + CONVERT(VARCHAR(4), CalendarYearID - 1)
			WHEN 2 THEN '12/31/' + CONVERT(VARCHAR(4), CalendarYearID - 1)
			WHEN 3 THEN '12/31/' + CONVERT(VARCHAR(4), CalendarYearID - 1)
			WHEN 4 THEN '3/31/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 5 THEN '3/31/' + CONVERT(VARCHAR(4), CalendarYearID )    
			WHEN 6 THEN '3/31/' + CONVERT(VARCHAR(4), CalendarYearID )    
			WHEN 7 THEN '6/30/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 8 THEN '6/30/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 9 THEN '6/30/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 10 THEN '9/30/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 11 THEN '9/30/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 12 THEN '9/30/' + CONVERT(VARCHAR(4), CalendarYearID )  
			END )
       ,ThisQuarterStartDate = CONVERT(DATE,
			CASE CalendarMonthID
			WHEN 1 THEN '1/1/' + CONVERT(VARCHAR(4), CalendarYearID)
			WHEN 2 THEN '1/1/' + CONVERT(VARCHAR(4), CalendarYearID)
			WHEN 3 THEN '1/1/' + CONVERT(VARCHAR(4), CalendarYearID)
			WHEN 4 THEN '4/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 5 THEN '4/1/' + CONVERT(VARCHAR(4), CalendarYearID )    
			WHEN 6 THEN '4/1/' + CONVERT(VARCHAR(4), CalendarYearID )    
			WHEN 7 THEN '7/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 8 THEN '7/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 9 THEN '7/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 10 THEN '10/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 11 THEN '10/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			WHEN 12 THEN '10/1/' + CONVERT(VARCHAR(4), CalendarYearID )  
			END )
       ,LastYearEndDate = CONVERT(DATE, '12/31/' + CONVERT(VARCHAR(4), CalendarYearID - 1) )
       
FROM CliftonIMS.dbo.Calendar c
	CROSS JOIN CliftonIMS.dbo.CalendarDay d 
	LEFT JOIN CliftonIMS.dbo.CalendarHolidayDay hd ON c.CalendarID = hd.CalendarID AND d.CalendarDayID = hd.CalendarDayID
	LEFT JOIN CliftonIMS.dbo.CalendarHoliday h ON hd.CalendarHolidayID = h.CalendarHolidayID
WHERE IsDefaultSystemCalendar = 1 

