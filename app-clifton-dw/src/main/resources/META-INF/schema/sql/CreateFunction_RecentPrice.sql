CREATE FUNCTION Datawarehouse.RecentPrice
    (
     
     @MarketDate DATETIME 
		 

    )
RETURNS TABLE
AS

RETURN


SELECT RecentPriceDate
		,p.MarketDataValueID
		,p.MarketDataFieldID
		,p.MarketDataSourceID
		,p.InvestmentSecurityID
		,PriceDate = p.MeasureDate
		,Price = p.MeasureValue
		,p.CreateUserID
		,p.CreateDate
		,p.UpdateUserID
		,p.UpdateDate

		,ProviderCompanyID = (SELECT BusinessCompanyID FROM dbo.BusinessCompany WHERE CompanyName = 'Bloomberg')

FROM CliftonIMS.dbo.MarketDataValue p
	INNER JOIN CliftonIMS.dbo.MarketDataSource ps ON p.MarketDataSourceID = ps.MarketDataSourceID AND DataSourceName = 'Bloomberg'
	INNER JOIN CliftonIMS.dbo.MarketDataField pf ON p.MarketDataFieldID = pf.MarketDataFieldID AND DataFieldName = 'Settlement Price'
	INNER JOIN Datawarehouse.RecentPriceDate(@MarketDate) r ON p.InvestmentSecurityID = r.InvestmentSecurityID AND RecentPriceDate = MeasureDate
