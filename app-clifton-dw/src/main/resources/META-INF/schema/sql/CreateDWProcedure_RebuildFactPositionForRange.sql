/*
DROP PROC  Datawarehouse.RebuildFactCurrentPositionForRange
*/



CREATE  PROC  Datawarehouse.RebuildFactCurrentPositionForRange

      @ClientInvestmentAccountID INT = 397 

AS 

-- processes month end snapshot data
SET NOCOUNT ON 

DECLARE 
	@ReportingDate DATE
	,@InceptionDate DATE
	
	,@RunDate DATE
	,@MonthStart DATE
	,@MonthEnd DATE

DECLARE @StartTime DATETIME
	, @EndTime DATETIME
	, @Duration DECIMAL(12,6)
SET @StartTime = GETDATE()



SELECT @RunDate = MIN(TransactionDate)
FROM dbo.FactTransaction 
WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID

SET @MonthStart = @RunDate


WHILE @RunDate <= GETDATE()
BEGIN
	SET @RunDate = DATEADD(Month, 1, @RunDate)
	SET @MonthEnd = DATEADD(DAY, -1, @MonthStart)
	PRINT '---------------------------------------------------------------------------------------------------------------------'
	PRINT @MonthEnd
	EXEC Datawarehouse.RebuildFactCurrentPosition @ClientInvestmentAccountID, @MonthEnd

SET @MonthStart = CONVERT(DATE, STR(MONTH(@RunDate)) + '/1/' + STR(Year(@RunDate)))

SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)

PRINT 'Runtime = ' +  STR(@Duration)
SET @StartTime = GETDATE()

END

