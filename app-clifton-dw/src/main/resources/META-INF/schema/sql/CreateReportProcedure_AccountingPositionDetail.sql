/*
drop PROC [Report].[AccountingPositionDetail]
*/

CREATE PROC [Report].[AccountingPositionDetail]
		@ClientInvestmentAccountID INT = 18
		,@ReportingDate DATETIME = '12/31/09'  AS 


SELECT
		fact.OriginalTransactionDate
		,fact.SettlementDate
		,Btick = s.Symbol
		,s.Symbol
		
		,a.AccountName
		,a.AccountNumber

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Measures
		,fact.OpeningPrice
		,MarketPrice = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN fact.MarketFXToBase ELSE fact.MarketPrice END 
		,fact.MarketValueLocal
		,MarketValueBase
		,BalanceSheetCostBase = CASE WHEN s.InvestmentGroupName = 'Futures' THEN 0 ELSE fact.CostBasisRemainingBase END 
		,BalanceSheetMarketValueBase =  CASE WHEN s.InvestmentGroupName = 'Futures' THEN  MarketValueBase - CostBasisRemainingBase ELSE fact.MarketValueBase END 
		,fact.NotionalValueLocal
		,fact.AdjustedNotionalValueLocal
		,MarketValue = fact.MarketValueLocal
		,fact.OpenFXToBase
		,fact.MarketFXToBase
		,fact.MarketFXToUS
		,fact.Quantity
		,fact.QuantityRemaining

		,LongShortIndicator = CASE WHEN Quantity >= 0 THEN 'Long' ELSE 'Short' END 

		,fact.CostBasis
		,fact.CostBasisRemaining
		,s.PriceMultiplier

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Calculated Columns
		,CostBasisRemainingBase = CONVERT(DECIMAL(19,2), (CostBasisRemaining) * fact.MarketFXToBase)
		,CostBasisRemainingUSDollar = CONVERT(DECIMAL(19,2), CostBasisRemaining * fact.MarketFXToBase) * fact.MarketFXToUS
		,MarketValueUSDollar = CONVERT(DECIMAL(19,2), MarketValueLocal * fact.MarketFXToBase * fact.MarketFXToUS)
		,NotionalValueBase = CONVERT(DECIMAL(19,2), NotionalValueLocal * fact.MarketFXToBase)
		,AdjustedNotionalValueBase = CONVERT(DECIMAL(19,2), AdjustedNotionalValueLocal * fact.MarketFXToBase)
		,UnrealizedGainLossLocal =  CONVERT(DECIMAL(19,2), MarketValueLocal - CostBasisRemaining)
		,UnrealizedGainLossBase =  CONVERT(DECIMAL(19,2), MarketValueBase - CostBasisRemainingBase)
		,UnrealizedGainLossUSDollar =  CONVERT(DECIMAL(19,2), (MarketValueLocal - CostBasisRemaining) * fact.MarketFXToBase * fact.MarketFXToUS)
		
		--ID Columns
		,fact.SnapshotDate
		,ReportingDate = @ReportingDate
		,fact.TransactionDate

		-----------------------------------------------------------------------------------------------------------------------------------------
		--Organizational Columns
	   ,InstrumentGroupItemName = 
				CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.Symbol + ' - ' + s.InvestmentSecurityName ELSE s.GroupItemName END 
		,InstrumentInvestmentCountry = CASE WHEN s.GroupItemName = 'Currency' THEN 'Cash and Currency' ELSE 'Investments' END 
		,ReportOrderParent = 'All'
		,ReportOrderChild = 'All'

		,BaseLanguage = CASE base.Symbol
						WHEN 'JPY' THEN 'Japanese'
						ELSE 'default' END 

		,BaseCurrencySymbol = CASE base.Symbol
						WHEN 'JPY' THEN CHAR(165)
						WHEN 'EUR' THEN CHAR(128)
						ELSE CHAR(36) END 

		,InstrumentCurrencySymbol = s.CurrencySymbol

		,CurrencySymbol = s.CurrencySymbol
		,IsCurrency = CASE WHEN aa.AccountingAccountID IN (100) THEN 1 ELSE 0 END 
		,s.IdentifierPrefix 
		,InstrumentName = CASE WHEN aa.AccountingAccountID IN (100, 105) THEN s.InvestmentSecurityName ELSE s.InvestmentInstrumentName END 
		,IsBaseCurrency = CASE WHEN s.CurrencySymbol = base.Symbol THEN 1 ELSE 0 END 
		,BaseCurrencyDescr = ''
		,MatureDate = s.EndDate
		,s.InvestmentSecurityName
		-----------------------------------------------------------------------------------------------------------------------------------------
		
FROM dbo.FactPositionSnapshot fact 
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
			
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		INNER JOIN Report.InvestmentMatrixView (Datawarehouse.GetClientReportingGroupID(@ClientInvestmentAccountID)) s ON fact.InvestmentSecurityID = s.InvestmentSecurityID

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 
	AND  aa.AccountingAccountID NOT IN (100, 105)
ORDER BY s.GroupItemName, s.InvestmentGroupName, s.Symbol



