CREATE PROC report.InvestmentAccountWithPosition  

AS 

Select DISTINCT 
	AccountNumberName = AccountNumber + ' - ' + AccountName
	, ia.* 
from CliftonIMS.dbo.InvestmentAccount ia
	INNER JOIN dbo.FactPositionSnapshot cp ON cp.ClientInvestmentAccountID = ia.InvestmentAccountID  
order by AccountNumber
