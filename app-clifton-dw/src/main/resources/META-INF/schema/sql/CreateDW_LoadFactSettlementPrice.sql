DECLARE @ReportingDate DATE = '12/31/09'
--need to build a FactPrice record for every day that a security is active
-- if the security has a maturity date then do not build the price after the maturity date

INSERT INTO dbo.FactSettlementPrice
        ( InvestmentSecurityID
        ,PriceDate
        ,Price
        )
SELECT 
	s.InvestmentSecurityID
	,dv.MeasureDate
	,DV.MeasureValue
	
FROM dbo.InvestmentSecurity s
	INNER JOIN dbo.MarketDataValue dv ON s.InvestmentSecurityID = dv.InvestmentSecurityID 
		INNER JOIN dbo.MarketDataField df ON dv.MarketDataFieldID = df.MarketDataFieldID AND df.DataFieldName = 'Settlement Price'

	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	INNER JOIN dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
	INNER JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
ORDER BY 2



DELETE FROM dbo.FactSettlementPrice WHERE InvestmentSecurityID IN (
		SELECT DISTINCT
				p.FromCurrencySecurityID

		FROM CliftonIMS.dbo.MarketDataExchangeRate p
			INNER JOIN CliftonIMS.dbo.MarketDataSource ps ON p.MarketDataSourceID = ps.MarketDataSourceID AND DataSourceName = 'Goldman Sachs'
		WHERE BusinessCompanyID  = (SELECT BusinessCompanyID FROM dbo.BusinessCompany WHERE CompanyName = 'Goldman Sachs'))



INSERT INTO dbo.FactSettlementPrice
        ( InvestmentSecurityID
        ,PriceDate
        ,Price
        )
SELECT DISTINCT
		p.FromCurrencySecurityID
		,p.RateDate
		,p.ExchangeRate

FROM CliftonIMS.dbo.MarketDataExchangeRate p
	INNER JOIN CliftonIMS.dbo.MarketDataSource ps ON p.MarketDataSourceID = ps.MarketDataSourceID AND DataSourceName = 'Goldman Sachs'
WHERE BusinessCompanyID  = (SELECT BusinessCompanyID FROM dbo.BusinessCompany WHERE CompanyName = 'Goldman Sachs')





/*
--GOTO addmissing
TRUNCATE TABLE dbo.FactSettlementPrice

INSERT INTO dbo.FactSettlementPrice
        ( InvestmentSecurityID
        ,PriceDate
        ,PriceValue
        )
SELECT 
	s.InvestmentSecurityID
	,dv.MeasureDate
	,DV.MeasureValue
	
FROM dbo.InvestmentSecurity s
	INNER JOIN dbo.MarketDataValue dv ON s.InvestmentSecurityID = dv.InvestmentSecurityID 
		INNER JOIN dbo.MarketDataField df ON dv.MarketDataFieldID = df.MarketDataFieldID AND df.DataFieldName = 'Settlement Price'

	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	INNER JOIN dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
	INNER JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
WHERE it.InvestmentTypeName IN ('Futures','Stocks')
	AND dv.MeasureDate >= '1/1/2009'
ORDER BY 2

 addmissing:
 -- Find prices which are missing for each day when the price is needed
	-- trying to avoid doing this by security - but it seems it may be the only way

DECLARE @SecurityToHandle Table (InvestmentSecurityID int )
INSERT INTO @SecurityToHandle
SELECT s.InvestmentSecurityID
FROM dbo.InvestmentSecurity s
	INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
	INNER JOIN dbo.InvestmentInstrumentHierarchy ih ON ii.InvestmentInstrumentHierarchyID = ih.InvestmentInstrumentHierarchyID
	INNER JOIN dbo.InvestmentType it ON ih.InvestmentTypeID = it.InvestmentTypeID
WHERE it.InvestmentTypeName IN ('Futures','Stocks')


DECLARE @DatesToHandle TABLE (PriceDate DATETIME)
INSERT INTO @DatesToHandle
SELECT StartDate
FROM CliftonIMS.dbo.CalendarDay cd 
	INNER JOIN (
			SELECT CalendarYearID, CalendarMonthID, MonthEndnumber = MAX(DayNumberInMonth)
			FROM CliftonIMS.dbo.CalendarDay f 
			WHERE EndDate < GETDATE()
			GROUP BY CalendarYearID, CalendarMonthID
			) monthends ON cd.CalendarMonthID = monthends.CalendarMonthID AND cd.CalendarYearID = monthends.CalendarYearID AND cd.DayNumberInMonth = MonthEndnumber
WHERE cd.CalendarWeekdayID NOT IN (1,7)

DECLARE @GetThesePrices TABLE (PriceDate DATETIME, InvestmentSecurityID INT)
INSERT INTO @GetThesePrices
SELECT AllPossible.* 
FROM (
	 SELECT PriceDate, InvestmentSecurityID
	 FROM @DatesToHandle
		CROSS JOIN @SecurityToHandle
			) AllPossible
			LEFT JOIN dbo.FactSettlementPrice p ON AllPossible.InvestmentSecurityID = p.InvestmentSecurityID AND AllPossible.PriceDate = p.PriceDate
WHERE  p.PriceDate IS NULL AND AllPossible.PriceDate <= GETDATE() AND AllPossible.PriceDate > '1/1/2009'


SET NOCOUNT ON 

DECLARE @ProcessDate DATETIME, @ProcessSecurityID INT, @Symbol VARCHAR(100)
DECLARE ProcessingCursor CURSOR FORWARD_ONLY FOR 

	SELECT DISTINCT p.PriceDate, p.InvestmentSecurityID, s.symbol
	FROM @GetThesePrices p
	INNER JOIN dbo.InvestmentSecurity s ON s.InvestmentSecurityID =  p.InvestmentSecurityID
	ORDER BY PriceDate

OPEN ProcessingCursor
FETCH NEXT FROM ProcessingCursor into @ProcessDate, @ProcessSecurityID, @Symbol
WHILE @@Fetch_Status = 0 
BEGIN
	
	Print 'Running  ' + CONVERT(VARCHAR(25), @ProcessDate, 101) + ' - '  + @Symbol

		IF NOT EXISTS (SELECT * FROM FactSettlementPrice WHERE InvestmentSecurityID = @ProcessSecurityID AND PriceDate = @ProcessDate)
		INSERT INTO dbo.FactSettlementPrice
				( InvestmentSecurityID
				,PriceDate
				,PriceValue
				)
		SELECT 
			p.InvestmentSecurityID
			,@ProcessDate
			,p.Price
			
		FROM Datawarehouse.RecentPrice(@ProcessDate) p 
		WHERE p.PriceDate = @ProcessDate AND InvestmentSecurityID = @ProcessSecurityID
		ORDER BY 1, 2

	



	
FETCH NEXT FROM ProcessingCursor into @ProcessDate, @ProcessSecurityID, @Symbol
END

CLOSE ProcessingCursor
DEALLOCATE ProcessingCursor




*/
