--SELECT * FROM Datawarehouse.FactIncomeStatement(19, '12/31/09', '11/1/09')

CREATE FUNCTION Datawarehouse.FactIncomeStatement
    (

		@ClientInvestmentAccountID INT = 1
		,@ReportingDate DATETIME = '12/31/09'   
		,@StartDate DATETIME = '11/1/09' 

    )

RETURNS TABLE

AS

RETURN 

--get currently open positions unrealized Gain or loss
SELECT
		ClientInvestmentAccountID = @ClientInvestmentAccountID
		,IncomeStatus = 'Change in Unrealized Gains or Losses'
		,aa.AccountingAccountID
		,aa.AccountingAccountName
		,fact.InvestmentSecurityID
		,fact.CurrencySecurityID
		,fact.OriginalTransactionDate
		,fact.AccountingTransactionID

		,LocalGainLossITD = (MarketValueLocal - CostBasisRemaining)
		,LocalGainLossPTD = (CASE WHEN MarketValueLastQuarterEnd = 0 
				THEN (MarketValueLocal - CostBasisRemaining) ELSE (MarketValueLocal - CostBasisRemaining) - (MarketValueLocal - MarketValueLastQuarterEnd) END ) 
		,LocalGainLossYTD = (CASE WHEN MarketValueLastYearEnd = 0 
				THEN (MarketValueLocal - CostBasisRemaining) ELSE (MarketValueLocal - CostBasisRemaining) - (MarketValueLocal - MarketValueLastYearEnd) END ) 

		,BaseGainLossITD = (MarketValueBase - CostBasisRemainingBase)
		,BaseGainLossPTD = (CASE WHEN MarketValueLastQuarterEnd = 0 
				THEN (MarketValueBase - CostBasisRemainingBase) ELSE (MarketValueBase - CostBasisRemainingBase) - ((MarketValueLocal - MarketValueLastQuarterEnd) * fact.MarketFXToBase ) END )
		,BaseGainLossYTD = (CASE WHEN MarketValueLastYearEnd = 0 
				THEN (MarketValueBase - CostBasisRemainingBase) ELSE (MarketValueBase - CostBasisRemainingBase) - ((MarketValueLocal - MarketValueLastYearEnd) * fact.MarketFXToBase ) END )

FROM dbo.FactPositionSnapshot fact 
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
			
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID


WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID AND fact.SnapshotDate = @ReportingDate 
	AND aa.AccountingAccountID NOT IN (100)  
	AND CostBasisRemaining <> 0



UNION ALL 

--get closing position activity - gains and losses
SELECT
		@ClientInvestmentAccountID
		,'Realized Gains or Losses'
		--,aa.AccountingAccountName  
		,aa.AccountingAccountID
		,aa.AccountingAccountName
		,fact.InvestmentSecurityID
		,fact.CurrencySecurityID
		,fact.OriginalTransactionDate
		,fact.AccountingTransactionID
		
		--sign is flipped because income statement credit entries that are stored negatively are positive income
		,-1 * fact.LocalDebitCredit  
		,-1 * CASE WHEN fact.TransactionDate >  m.LastQuarterEndDate THEN fact.LocalDebitCredit ELSE 0 END   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastYearEndDate THEN fact.LocalDebitCredit ELSE 0 END   
		
		,-1 * fact.BaseDebitCredit   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastQuarterEndDate THEN fact.BaseDebitCredit ELSE 0 END   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastYearEndDate THEN fact.BaseDebitCredit ELSE 0 END  


FROM dbo.FactTransaction fact
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID
				LEFT JOIN dbo.AccountingAccountType aat ON aa.AccountingAccountTypeID = aat.AccountingAccountTypeID
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN Datawarehouse.GetQuarterEndDates(@ReportingDate) m ON m.ReportingDate = @ReportingDate

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID 
	AND fact.TransactionDate <= @ReportingDate  
	AND aa.AccountingAccountID >= 400

