CREATE FUNCTION [Datawarehouse].[GetClientReportingGroupID]	(

	 @ClientAccountID INT 

) RETURNS INT  AS

--need to allow a lookup and a conversion from any currency to any base currency using a US exchange rate table

BEGIN

 --set which investment security matrix whould be used for the report orgnazation
--  currently this is based on the account's base currency





DECLARE @InvestmentReportingGroupID INT 
SELECT @InvestmentReportingGroupID = CASE WHEN bs.Symbol = 'USD'
			THEN (SELECT InvestmentGroupID FROM CliftonIMS.dbo.InvestmentGroup WHERE InvestmentGroupName = 'Accounting Reporting')
			ELSE (SELECT InvestmentGroupID FROM CliftonIMS.dbo.InvestmentGroup WHERE InvestmentGroupName = 'Foreign Accounting Reporting') 
			END 
	FROM dbo.InvestmentAccount ia
		INNER JOIN dbo.InvestmentSecurity bs ON ia.BaseCurrencyID = bs.InvestmentSecurityID -- base security 
	WHERE ia.InvestmentAccountID = @ClientAccountID

 
RETURN
	 
	@InvestmentReportingGroupID
 
END

