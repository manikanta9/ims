--EXEC Report.AccountingIncomeStatement 397, '12/31/09', '10/1/09'

--DROP PROC Datawarehouse.RebuildFactIncomeStatement
CREATE PROC Datawarehouse.RebuildFactIncomeStatement

      @ClientInvestmentAccountID INT = 397
    , @ReportingDate DATETIME = '12/31/09'
AS 


DECLARE @StartTime DATETIME
	, @EndTime DATETIME
	, @Duration DECIMAL(12,6)
SET @StartTime = GETDATE()

--this is needed during migration because the default price provider's ID is unknown at this time
DECLARE @BloombergCompanyID INT 
SET @BloombergCompanyID = (SELECT BusinessCompanyID FROM CliftonIMS.dbo.BusinessCompany WHERE CompanyName LIKE 'Bloomberg')


DECLARE 
	@LastQtrDate DATETIME
	,@LastYearDate DATETIME

SELECT 
	@LastQtrDate = m.LastQuarterEndDate
	,@LastYearDate = m.LastYearEndDate
FROM Datawarehouse.GetQuarterEndDates(@ReportingDate) m 


-----------------------------------------------------------------------------------------------------------------------------------------------
/*
	-- To determine unrealized gain loss income between two periods a price x shares x multiplier is not accurate enough
		-- because the shares and positions have likely changed the current position cannot be applied to historical prices
		
*/
DECLARE @GLPivot TABLE (
        HoldingInvestmentAccountID int 
        , InvestmentSecurityID int 
        , CurrencySecurityID int 
        , AccountingAccountID int 
		, OpenTradeEquityBase_Amount DECIMAL(19,2)
		, OpenTradeEquityBase_Source VARCHAR(20)
	 )
INSERT INTO @GLPivot	 
SELECT HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID, SUM(OpenTradeEquityBase), 'CurrentPeriod' 
FROM Datawarehouse.PositionSummary(@ClientInvestmentAccountID, @ReportingDate) p
GROUP BY  HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID
	UNION ALL
SELECT  HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID, SUM(OpenTradeEquityBase), 'LastQuarter' 
FROM Datawarehouse.PositionSummary(@ClientInvestmentAccountID, @LastQtrDate) p
GROUP BY  HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID
	UNION ALL
SELECT  HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID, SUM(OpenTradeEquityBase), 'LastYear' 
FROM Datawarehouse.PositionSummary(@ClientInvestmentAccountID, @LastYearDate) p
GROUP BY  HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID

DECLARE @GainLossDelta TABLE (
        HoldingInvestmentAccountID int 
        , InvestmentSecurityID int 
        , CurrencySecurityID int 
        , AccountingAccountID int 
		, OpenTradeEquityBase_Current DECIMAL(19,2)
		, OpenTradeEquityBase_LastQuarterEnd DECIMAL(19,2)
		, OpenTradeEquityBase_LastYearEnd DECIMAL(19,2))

INSERT INTO @GainLossDelta 
SELECT  HoldingInvestmentAccountID, InvestmentSecurityID, CurrencySecurityID, AccountingAccountID, isnull(CurrentPeriod, 0), isnull(LastQuarter, 0), isnull(LastYear, 0)
FROM (
	SELECT * FROM @GLPivot ) P
PIVOT (
		SUM(OpenTradeEquityBase_Amount)	
	FOR 	OpenTradeEquityBase_Source IN (CurrentPeriod, LastQuarter, LastYear) 
	) pvt

SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'Generate @GainLossDelta temp data: ' +  STR(@Duration)
SET @StartTime = GETDATE()

-----------------------------------------------------------------------------------------------------------------------------------------------


--Select * FROM dbo.FactIncomeStatement  
--WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID AND SnapshotDate = @ReportingDate
--and InvestmentSecurityID = 425


DELETE FROM dbo.FactIncomeStatement
WHERE ClientInvestmentAccountID = @ClientInvestmentAccountID AND SnapshotDate = @ReportingDate

SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'Delete FactIncomeStatement: ' +  STR(@Duration)
SET @StartTime = GETDATE()



INSERT INTO dbo.FactIncomeStatement
        ( SnapshotDate
        ,ClientInvestmentAccountID
        ,HoldingInvestmentAccountID
        ,InvestmentSecurityID
        ,CurrencySecurityID
        ,AccountingAccountID
        ,IncomeStatus
        ,LocalGainLossITD
        ,LocalGainLossPTD
        ,LocalGainLossYTD
        ,BaseGainLossITD
        ,BaseGainLossPTD
        ,BaseGainLossYTD
        )

SELECT 
		SnapshotDate
        ,ClientInvestmentAccountID
        ,HoldingInvestmentAccountID
        ,InvestmentSecurityID
        ,CurrencySecurityID
        ,AccountingAccountID
        ,IncomeStatus
        ,SUM(LocalGainLossITD)
        ,SUM(LocalGainLossPTD)
        ,SUM(LocalGainLossYTD)
        ,SUM(BaseGainLossITD)
        ,SUM(BaseGainLossPTD)
        ,SUM(BaseGainLossYTD)

FROM (
--get currently open positions unrealized Gain or loss
SELECT
		SnapshotDate = @ReportingDate
		,ClientInvestmentAccountID = @ClientInvestmentAccountID
		,p.HoldingInvestmentAccountID
		,p.InvestmentSecurityID
		,CurrencySecurityID = ISNULL(p.CurrencySecurityID, a.BaseCurrencyID)
		,aa.AccountingAccountID
		,IncomeStatus = 'Change in Unrealized Gains or Losses'

		,LocalGainLossITD = (OpenTradeEquityBase_Current)
		,LocalGainLossPTD = (OpenTradeEquityBase_Current - OpenTradeEquityBase_LastQuarterEnd) 
		,LocalGainLossYTD = (OpenTradeEquityBase_Current - OpenTradeEquityBase_LastYearEnd) 

		,BaseGainLossITD = (OpenTradeEquityBase_Current)  
		,BaseGainLossPTD = (OpenTradeEquityBase_Current - OpenTradeEquityBase_LastQuarterEnd) 
		,BaseGainLossYTD = (OpenTradeEquityBase_Current - OpenTradeEquityBase_LastYearEnd) 


from @GainLossDelta p
		
		INNER JOIN dbo.InvestmentSecurity s ON p.InvestmentSecurityID = s.InvestmentSecurityID
			INNER JOIN dbo.InvestmentInstrument ii ON s.InvestmentInstrumentID = ii.InvestmentInstrumentID
			LEFT JOIN dbo.AccountingAccount aa ON p.AccountingAccountID = aa.AccountingAccountID
			
		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = @ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID

		INNER JOIN dbo.InvestmentAccount ha ON ha.InvestmentAccountID = p.HoldingInvestmentAccountID
		LEFT JOIN Datawarehouse.RecentFxRate(@ReportingDate) markbaseFX ON markbaseFX.FromCurrencySecurityID = a.BaseCurrencyID AND markbaseFX.ProviderCompanyID = ha.IssuingCompanyID


WHERE aa.AccountingAccountID NOT IN (100)  
	
	
UNION ALL 


--get closing position activity - gains and losses
SELECT
		@ReportingDate
		,ClientInvestmentAccountID
		,HoldingInvestmentAccountID
		,fact.InvestmentSecurityID
		,fact.CurrencySecurityID
		--,aa.AccountingAccountName  
		,aa.AccountingAccountID
		,'Realized Gains or Losses'
		
		--sign is flipped because income statement credit entries that are stored negatively are positive income
		,-1 * fact.LocalDebitCredit  
		,-1 * CASE WHEN fact.TransactionDate >  m.LastQuarterEndDate THEN fact.LocalDebitCredit ELSE 0 END   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastYearEndDate THEN fact.LocalDebitCredit ELSE 0 END   
		
		,-1 * fact.BaseDebitCredit   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastQuarterEndDate THEN fact.BaseDebitCredit ELSE 0 END   
		,-1 * CASE WHEN fact.TransactionDate >  m.LastYearEndDate THEN fact.BaseDebitCredit ELSE 0 END  


FROM dbo.FactTransaction fact
			LEFT JOIN dbo.AccountingAccount aa ON fact.AccountingAccountID = aa.AccountingAccountID

		INNER JOIN dbo.InvestmentAccount a ON a.InvestmentAccountID = fact.ClientInvestmentAccountID
			LEFT JOIN dbo.InvestmentSecurity base ON a.BaseCurrencyID = base.InvestmentSecurityID
		INNER JOIN dbo.InvestmentSecurity s ON fact.InvestmentSecurityID = s.InvestmentSecurityID
		INNER JOIN Datawarehouse.GetQuarterEndDates(@ReportingDate) m ON m.ReportingDate = @ReportingDate

WHERE fact.ClientInvestmentAccountID = @ClientInvestmentAccountID 
	AND fact.TransactionDate <= @ReportingDate  
	AND aa.AccountingAccountID >= 400
	
	
) CombinedSet

GROUP BY 
		SnapshotDate
        ,ClientInvestmentAccountID
        ,HoldingInvestmentAccountID
        ,InvestmentSecurityID
        ,CurrencySecurityID
        ,AccountingAccountID
        ,IncomeStatus

 
 
 SET @EndTime = GETDATE()
SET @Duration = DATEDIFF(ms, @StartTime, @EndTime)
PRINT 'Insert new FactIncomeStatement: ' +  STR(@Duration)
SET @StartTime = GETDATE()
