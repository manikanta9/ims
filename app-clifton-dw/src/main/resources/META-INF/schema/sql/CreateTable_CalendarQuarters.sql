

SET NOCOUNT ON 
CREATE TABLE CalendarQuarters 
	(QuarterID INT
	, QuarterStartDate DATETIME, QuarterEndDate DATETIME
	, YearStartDate DATETIME, YearEndDate DATETIME
	)

DECLARE 
		@i INT 
        ,@QuarterStartDate DATETIME 
        ,@QuarterEndDate DATETIME 

SELECT 
		@i = 0
        ,@QuarterStartDate = '1/1/2000'
        ,@QuarterEndDate = '3/31/2000'


WHILE @i < 250
BEGIN 


SET @i = @i + 1 
INSERT INTO CalendarQuarters
        ( QuarterID
        ,QuarterStartDate
        ,QuarterEndDate
        ,YearStartDate
        ,YearEndDate
        )
VALUES  ( @i
        ,@QuarterStartDate
		,@QuarterEndDate
		,'1/1/' + CONVERT(VARCHAR(4), YEAR(@QuarterStartDate))
		,'12/31/' + CONVERT(VARCHAR(4), YEAR(@QuarterStartDate))
        
        )
SELECT @QuarterStartDate = DATEADD(q, 1, @QuarterStartDate)
	, @QuarterEndDate = DATEADD(q, 1, @QuarterEndDate) 
IF MONTH(@QuarterEndDate) = 12
SET @QuarterEndDate  = DATEADD(d,1,@QuarterEndDate)	


END 



SELECT * INTO Calendar FROM TCGSQL.TCP.dbo.CalendarDate
