<?xml version="1.0" encoding="UTF-8" ?>
<migrations xmlns="https://nexus.paraport.com/repository/schema/migrations"
            xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
            xsi:schemaLocation="https://nexus.paraport.com/repository/schema/migrations https://nexus.paraport.com/repository/schema/migrations/clifton-migrations.xsd"
            conditionalSql="SELECT 1 WHERE EXISTS (SELECT * FROM INFORMATION_SCHEMA.VIEWS WHERE TABLE_NAME = 'AccountingAccountDimension')">
		
	<!-- replace AccountingAccountDimension view with corresponding table and add corresponding rebuild code -->
	<sql>
		<statement>
DROP VIEW [dbo].[AccountingAccountDimension];
		</statement>
	</sql>


	<sql>
		<statement>

CREATE VIEW [dbo].[AccountingAccountDimensionView] AS
	SELECT aa.AccountingAccountID, aa.AccountingAccountName, aa.AccountingAccountDescription, aa.IsPosition, aa.IsCurrency, aa.IsCash, aa.IsCollateral, aa.IsCommission, aa.IsGainLoss, aa.IsReceivable, aa.IsCurrencyTranslation, aa.IsPostingNotAllowed,
		at.AccountingAccountTypeName, at.AccountingAccountTypeDescription, at.AccountingAccountTypeOrder, at.IsBalanceSheetAccount, at.IsGrowingOnDebintSide, aa.IsFee
	FROM CliftonIMS.dbo.AccountingAccount aa
		INNER JOIN CliftonIMS.dbo.AccountingAccountType at ON aa.AccountingAccountTypeID = at.AccountingAccountTypeID

		</statement>
	</sql>
	



	<sql>
		<statement>
CREATE TABLE AccountingAccountDimension (
	AccountingAccountID SMALLINT NOT NULL,
	AccountingAccountName NVARCHAR(50) NOT NULL,
	AccountingAccountDescription NVARCHAR(500) NOT NULL,
	IsPosition BIT NOT NULL,
	IsCurrency BIT NOT NULL,
	IsCash BIT NOT NULL,
	IsCollateral BIT NOT NULL,
	IsCommission BIT NOT NULL,
	IsGainLoss BIT NOT NULL,
	IsReceivable BIT NOT NULL,
	IsCurrencyTranslation BIT NOT NULL,
	IsPostingNotAllowed BIT NOT NULL,
	-- AccountingAccountType fields	
	AccountingAccountTypeName NVARCHAR(50) NOT NULL,
	AccountingAccountTypeDescription NVARCHAR(500) NOT NULL,
	AccountingAccountTypeOrder INT NOT NULL,
	IsBalanceSheetAccount BIT NOT NULL,
	IsGrowingOnDebintSide BIT NOT NULL,
	IsFee BIT NOT NULL
);

ALTER TABLE AccountingAccountDimension ADD CONSTRAINT [PK_AccountingAccountDimension]
	PRIMARY KEY CLUSTERED (AccountingAccountID);
CREATE INDEX [ix_AccountingAccountDimension_AccountingAccountName] ON AccountingAccountDimension (AccountingAccountName);
CREATE INDEX [ix_AccountingAccountDimension_AccountingAccountTypeName] ON AccountingAccountDimension (AccountingAccountTypeName);
		</statement>
	</sql>


	
	<sql>
		<statement>
CREATE PROC [etl].[AccountingAccountDimension_Rebuild]
/**
 Rebuilds AccountingAccountDimension (insert new accounts, delete removed accounts, update existing account fields).
	Rebuild is based on corresponding cross database view: AccountingAccountDimensionView   
*/
AS 


-- 1. delete

	DELETE FROM AccountingAccountDimension
		WHERE NOT EXISTS (
			SELECT * FROM AccountingAccountDimensionView v
			WHERE v.AccountingAccountID = AccountingAccountDimension.AccountingAccountID
		)

-- 2. update

	UPDATE AccountingAccountDimension SET
		AccountingAccountName = v.AccountingAccountName,
		AccountingAccountDescription = v.AccountingAccountDescription,
		IsPosition = v.IsPosition,
		IsCurrency = v.IsCurrency,
		IsCash = v.IsCash,
		IsCollateral = v.IsCollateral,
		IsCommission = v.IsCommission,
		IsGainLoss = v.IsGainLoss,
		IsUnrealizedGainLoss = v.IsUnrealizedGainLoss,
		IsReceivable = v.IsReceivable,
		IsCurrencyTranslation = v.IsCurrencyTranslation,
		IsPostingNotAllowed = v.IsPostingNotAllowed,
		IsNotOurAccount = v.IsNotOurAccount,
		AutoAccrualReversalOffsetAccountID = v.AutoAccrualReversalOffsetAccountID,
		UnrealizedCurrencyAccountID = v.UnrealizedCurrencyAccountID,
		UnrealizedCurrencyAccountName = v.UnrealizedCurrencyAccountName,
		-- AccountingAccountType fields
		AccountingAccountTypeID = v.AccountingAccountTypeID,
		AccountingAccountTypeName = v.AccountingAccountTypeName,
		AccountingAccountTypeDescription = v.AccountingAccountTypeDescription,
		AccountingAccountTypeOrder = v.AccountingAccountTypeOrder,
		IsBalanceSheetAccount = v.IsBalanceSheetAccount,
		IsGrowingOnDebintSide = v.IsGrowingOnDebintSide,
		IsFee = v.IsFee
	FROM AccountingAccountDimensionView v
	WHERE v.AccountingAccountID = AccountingAccountDimension.AccountingAccountID



-- 3. insert

	INSERT INTO AccountingAccountDimension
		SELECT
			AccountingAccountID,
			AccountingAccountName,
			AccountingAccountDescription,
			IsPosition,
			IsCurrency,
			IsCash,
			IsCollateral,
			IsCommission,
			IsGainLoss,
			IsUnrealizedGainLoss,
			IsReceivable,
			IsCurrencyTranslation,
			IsPostingNotAllowed,
			IsNotOurAccount,
			AutoAccrualReversalOffsetAccountID,
			UnrealizedCurrencyAccountID,
			UnrealizedCurrencyAccountName,
			-- AccountingAccountType fields
			AccountingAccountTypeID,
			AccountingAccountTypeName,
			AccountingAccountTypeDescription,
			AccountingAccountTypeOrder,
			IsBalanceSheetAccount,
			IsGrowingOnDebintSide,
			IsFee
		FROM AccountingAccountDimensionView v
		WHERE NOT EXISTS (
			SELECT * FROM AccountingAccountDimension d
			WHERE v.AccountingAccountID = d.AccountingAccountID
		)
		</statement>
		
		
		
		<statement>

ALTER PROC [etl].[DimensionTables_Rebuild]
	AS 

	EXEC [etl].[AccountingAccountDimension_Rebuild]
	EXEC [etl].[InvestmentAccountDimension_Rebuild]
	EXEC [etl].[InvestmentSecurityDimension_Rebuild]

		</statement>
	</sql>



	<sql>
		<statement>
			[etl].[DimensionTables_Rebuild]
		</statement>
	</sql>


	<sql>
		<statement>
			DELETE FROM AccountingPositionSnapshot WHERE AccountingAccountID IN (
				SELECT AccountingAccountID FROM
					(SELECT DISTINCT AccountingAccountID FROM AccountingPositionSnapshot) a
				WHERE NOT EXISTS (SELECT * FROM AccountingAccountDimension d WHERE d.AccountingAccountID = a.AccountingAccountID)
			)


			DELETE FROM AccountingBalanceSnapshot WHERE AccountingAccountID IN (
				SELECT AccountingAccountID FROM
					(SELECT DISTINCT AccountingAccountID FROM AccountingBalanceSnapshot) a
				WHERE NOT EXISTS (SELECT * FROM AccountingAccountDimension d WHERE d.AccountingAccountID = a.AccountingAccountID)
			)
		</statement>
	</sql>



	<sql>
		<statement>
			ALTER TABLE AccountingBalanceSnapshot ADD CONSTRAINT [FK_AccountingBalanceSnapshot_AccountingAccountDimension_AccountingAccountID] FOREIGN KEY (AccountingAccountID)
				REFERENCES AccountingAccountDimension(AccountingAccountID);

			ALTER TABLE AccountingPositionSnapshot ADD CONSTRAINT [FK_AccountingPositionSnapshot_AccountingAccountDimension_AccountingAccountID] FOREIGN KEY (AccountingAccountID)
				REFERENCES AccountingAccountDimension(AccountingAccountID);
		</statement>
	</sql>

	
</migrations>
