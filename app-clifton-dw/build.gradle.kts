import com.clifton.gradle.plugin.artifact.CliftonSchemaPlugin
import com.clifton.gradle.plugin.artifact.MigrationExec

dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////


	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	implementation(project(":clifton-core"))
	implementation(project(":clifton-security"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////


	testFixturesApi(testFixtures(project(":clifton-core")))
}

/*
 * Enforce database restore order when running parallel restores. Some databases reference other databases (such as the IMS database) during the restore process and will fail if
 * executed while any of the referenced databases are in a restoring state. This only affects builds where multiple database restores are simultaneously requested.
 */
tasks.named(CliftonSchemaPlugin.DB_MIGRATE_RESTORE_TASK_NAME, MigrationExec::class) { mustRunAfter(":app-clifton-ims:${CliftonSchemaPlugin.DB_MIGRATE_RESTORE_TASK_NAME}") }
