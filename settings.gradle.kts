import java.nio.file.Files
import java.nio.file.Path
import kotlin.streams.toList

// Plugin repository configuration must be manually configured in settings scripts; see https://docs.gradle.org/current/userguide/upgrading_version_5.html#the_pluginmanagement_block_in_settings_scripts_is_now_isolated
pluginManagement {
	repositories {
		clear()
		mavenCentral()
		mavenLocal()
		/*maven {
			name = "${extra["repo.name"]}"
			setUrl("${extra["repo.host"]}/${extra["repo.path"]}")
			credentials {
				username = "${extra["repo.username"]}"
				password = "${extra["repo.password"]}"
			}
		}*/
	}
}

plugins {
	//id("com.gradle.enterprise") version "3.5"
}

// Note: buildSrc sources cannot be accessed from within the settings.gradle.kts file. "Extra" properties must be accessed by using their keys directly.
val buildRootPath: String by extra

// Apply shared settings
apply(from = "$buildRootPath/shared-with-buildSrc/logging.settings.gradle.kts")
apply(from = "$buildRootPath/shared-with-buildSrc/build-cache.settings.gradle.kts")


// Define applicable projects
val excludedProjectRoots = listOf("buildSrc")
val projectNames = AutoLoadProjectUtils.getDirectoryProjects(rootDir.toPath(), rootDir.toPath())
		.filter { projectName -> !excludedProjectRoots.any { projectName.startsWith(it) } }
include(*projectNames.toTypedArray())
AutoLoadProjectUtils.normalizeProjectPaths(rootProject, rootProject)


////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


object AutoLoadProjectUtils {

	private const val SUB_PROJECT_DIRECTORY_NAME = "subprojects"

	/**
	 * Retrieves all sub-project names contained underneath the given directory, excluding the given directory itself.
	 *
	 * Project names are serialized relative to the given `rootPath`. For example, a project designated by `/my-project/build.gradle.kts` will produce `"my-project"`, but a project
	 * designated by `/my-project/subprojects/my-subproject/build.gradle.kts` will produce `"my-project:my-subproject"`.
	 */
	fun getDirectoryProjects(directory: Path, projectRootDirectory: Path): List<String> {
		return Files.list(directory).toList()
				.filter { isProjectDirectory(it) }
				.flatMap { childPath ->
					// Get project path in the form "(<parent project name>:)<child project name>"
					val relativePath = projectRootDirectory.relativize(childPath)
					val projectName = relativePath.toString()
							.replace(File.separatorChar, ':')
							.replace("$SUB_PROJECT_DIRECTORY_NAME:", "")
					val subprojectPath = childPath.resolve(SUB_PROJECT_DIRECTORY_NAME)
					val projects = mutableListOf(projectName)
					if (Files.exists(subprojectPath)) {
						projects += getDirectoryProjects(childPath.resolve(SUB_PROJECT_DIRECTORY_NAME), projectRootDirectory)
					}
					projects
				}
	}


	/**
	 * Normalizes the project paths for the given project and each of its descendants.
	 *
	 * This adds sub-project components to the path so that the project directories match our conventions. For example, the project path `my-project:my-subproject` by default uses
	 * the project directory `/my-project/my-subproject`. This function converts the project directory to add the sub-project directory component between each existing element. The
	 * resulting project directory will be `/my-project/subprojects/my-subproject`.
	 */
	fun normalizeProjectPaths(project: ProjectDescriptor, rootProject: ProjectDescriptor) {
		if (project != rootProject) {
			// Add sub-directory components to relative project path
			val rootProjectPath = rootProject.projectDir.toPath()
			val relativeProjectPath = rootProjectPath.relativize(project.projectDir.toPath())
			val replacedPath = relativeProjectPath.drop(1)
					.fold(relativeProjectPath.first()) { path, pathElement -> path.resolve(SUB_PROJECT_DIRECTORY_NAME).resolve(pathElement) }
			val normalizedProjectPath = rootProjectPath.resolve(replacedPath)
			project.projectDir = normalizedProjectPath.toFile()
		}
		project.children.forEach { normalizeProjectPaths(it, rootProject) }
	}


	/**
	 * Returns `true` if the given path corresponds to a project directory.
	 */
	private fun isProjectDirectory(directory: Path): Boolean {
		val isDirectory = Files.isDirectory(directory)
		val isGradleProject = Files.exists(directory.resolve("build.gradle")) || Files.exists(directory.resolve("build.gradle.kts"))
		return isDirectory && isGradleProject
	}
}
