package com.clifton.test.util;


import java.security.SecureRandom;
import java.util.Random;


public class RandomUtils {

	private static final NameGenerator NAME_GENERATOR = NameGenerator.defaultNameGenerator();
	private static final Random RANDOM = new SecureRandom();


	public static String randomNameWithPrefix(String prefix) {
		return prefix + " " + randomName();
	}


	public static String randomName() {
		int numSyllables = randomNumber() % 3 + 3;
		return NAME_GENERATOR.compose(numSyllables);
	}


	public static String randomNameAndNumber() {
		return randomName() + "-" + randomNumber(0, 999);
	}


	public static int randomNumber() {
		int ran = RANDOM.nextInt();
		if (ran == Integer.MIN_VALUE) {
			ran = Integer.MAX_VALUE;
		}

		return Math.abs(ran);
	}


	public static int randomNumber(int min, int max) {
		if (min < 0 || min >= max) {
			throw new IllegalArgumentException("'min' and 'max' must be non-negative and 'min' must be less than 'max'.");
		}

		return RANDOM.nextInt(max - min + 1) + min;
	}


	public static double randomDoubleZeroToOne() {
		return RANDOM.nextDouble();
	}
}
