package com.clifton.test.util;


import com.clifton.core.security.authorization.SecurityPermission;
import com.clifton.core.security.authorization.SecurityResource;
import com.clifton.core.util.CollectionUtils;
import com.clifton.security.authorization.SecurityPermissionAssignment;
import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.security.user.SecurityGroup;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.test.protocol.ImsProtocolClient;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;


@Component
public class UserUtils {

	@Resource
	private ImsProtocolClient client;

	@Resource
	private SecurityAuthorizationSetupService securityAuthorizationSetupService;

	@Resource
	private SecurityUserService securityUserService;


	public SecurityUser getCurrentUser() {
		return this.securityUserService.getSecurityUserByName(this.client.getCurrentUsername());
	}


	public SecurityUser getUserByName(String userName) {
		return this.securityUserService.getSecurityUserByName(userName);
	}


	public void switchUser(String userName, String password) {
		this.client.setUser(userName, password);
	}


	public String getCurrentUserName() {
		return this.client.getCurrentUsername();
	}


	public void addUserToSecurityGroup(String username, String groupName) {
		SecurityUser user = this.securityUserService.getSecurityUserByName(username);
		SecurityGroup group = this.securityUserService.getSecurityGroupByName(groupName);
		List<SecurityGroup> groupList = this.securityUserService.getSecurityGroupListByUser(user.getId());
		// If link doesn't exist, add it
		if (!CollectionUtils.contains(groupList, group)) {
			this.securityUserService.linkSecurityUserToGroup(user.getId(), group.getId());
		}
	}


	public void removeUserFromSecurityGroup(String username, String groupName) {
		SecurityUser user = this.securityUserService.getSecurityUserByName(username);
		SecurityGroup group = this.securityUserService.getSecurityGroupByName(groupName);
		this.securityUserService.unlinkSecurityUserFromGroup(user.getId(), group.getId());
	}


	public void addPermissionReadOnlyToUser(String username, String tableName) {
		SecurityPermission permissions = new SecurityPermission();
		permissions.setReadAllowed(true);
		addPermissionToUserImpl(username, tableName, permissions);
	}


	public void addPermissionToUser(String username, String tableName) {
		SecurityPermission permissions = new SecurityPermission();
		permissions.setFullControlAllowed(true);
		addPermissionToUserImpl(username, tableName, permissions);
	}


	private void addPermissionToUserImpl(String username, String tableName, SecurityPermission permission) {
		SecurityResource resource = this.securityAuthorizationSetupService.getSecurityResourceByName(tableName);

		SecurityUser user = this.securityUserService.getSecurityUserByName(username);

		SecurityPermissionAssignment permissionAssignment = new SecurityPermissionAssignment();
		permissionAssignment.setSecurityUser(user);
		permissionAssignment.setSecurityResource(resource);
		permissionAssignment.setSecurityPermission(permission);

		this.securityAuthorizationSetupService.saveSecurityPermissionAssignment(permissionAssignment);
	}


	/**
	 * Removes user from all groups and also any specific permission assignments
	 *
	 * @param userName
	 */
	public void removeAllPermissionsForUser(String userName) {
		SecurityUser user = this.securityUserService.getSecurityUserByName(userName);
		List<SecurityGroup> groupList = this.securityUserService.getSecurityGroupListByUser(user.getId());
		for (SecurityGroup group : CollectionUtils.getIterable(groupList)) {
			removeUserFromSecurityGroup(user.getUserName(), group.getName());
		}
		// Remove Explicit Permissions
		List<SecurityPermissionAssignment> permissionList = this.securityAuthorizationSetupService.getSecurityPermissionAssignmentListByUser(user.getId());
		for (SecurityPermissionAssignment assignment : CollectionUtils.getIterable(permissionList)) {
			this.securityAuthorizationSetupService.deleteSecurityPermissionAssignment(assignment.getId());
		}
	}
}
