package com.clifton.test.util;


import com.clifton.core.util.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;


/**
 * This class is released under GNU general public license
 * <p/>
 * Description: This class generates random names from syllables, and provides programmer a
 * simple way to set a group of rules for generator to avoid unpronounceable and bizarre names.
 * <p/>
 * SYLLABLE FILE REQUIREMENTS/FORMAT:
 * 1) all syllables are separated by line break.
 * 2) Syllable should not contain or start with whitespace, as this character is ignored and only first part of the syllable is read.
 * 3) + and - characters are used to set rules, and using them in other way, may result in unpredictable results.
 * 4) Empty lines are ignored.
 * <p/>
 * SYLLABLE CLASSIFICATION:
 * Name is usually composed from 3 different class of syllables, which include prefix, middle part and suffix.
 * To declare syllable as a prefix in the file, insert "-" as a first character of the line.
 * To declare syllable as a suffix in the file, insert "+" as a first character of the line.
 * everything else is read as a middle part.
 * <p/>
 * NUMBER OF SYLLABLES:
 * Names may have any positive number of syllables. In case of 2 syllables, name will be composed from prefix and suffix.
 * In case of 1 syllable, name will be chosen from amongst the prefixes.
 * In case of 3 and more syllables, name will begin with prefix, is filled with middle parts and ended with suffix.
 * <p/>
 * ASSIGNING RULES:
 * I included a way to set 4 kind of rules for every syllable. To add rules to the syllables, write them right after the
 * syllable and SEPARATE WITH WHITESPACE. (example: "aad +v -c"). The order of rules is not important.
 * <p/>
 * RULES:
 * 1) +v means that next syllable must definitely start with a Vowel.
 * 2) +c means that next syllable must definitely start with a consonant.
 * 3) -v means that this syllable can only be added to another syllable, that ends with a Vowel.
 * 4) -c means that this syllable can only be added to another syllable, that ends with a consonant.
 * So, our example: "aad +v -c" means that "aad" can only be after consonant and next syllable must start with Vowel.
 * Beware of creating logical mistakes, like providing only syllables ending with consonants, but expecting only Vowels, which will be detected
 * and RuntimeException will be thrown.
 * <p/>
 * TO START:
 * Create a new NameGenerator object, provide the syllable file, and create names using compose() method.
 */
public class NameGenerator {

	private static final String FILE = "/RandomNames.txt";
	final private static char[] Vowels = {'a', 'e', 'i', 'o', 'u', 'y'};
	final private static char[] consonants = {'b', 'c', 'd', 'f', 'g', 'h', 'j', 'k', 'l', 'm', 'n', 'p', 'q', 'r', 's', 't', 'v', 'w', 'x', 'y'};
	final ArrayList<String> pre = new ArrayList<>();
	final ArrayList<String> mid = new ArrayList<>();
	final ArrayList<String> sur = new ArrayList<>();


	/**
	 * Create new random name generator object. refresh() is automatically called.
	 */
	public NameGenerator(InputStream is) throws IOException {
		BufferedReader bufRead = new BufferedReader(new InputStreamReader(is, StandardCharsets.UTF_8));
		String line = "";

		while (line != null) {
			line = bufRead.readLine();
			if (!StringUtils.isEmpty(line)) {
				if (line.charAt(0) == '-') {
					this.pre.add(line.substring(1).toLowerCase());
				}
				else if (line.charAt(0) == '+') {
					this.sur.add(line.substring(1).toLowerCase());
				}
				else {
					this.mid.add(line.toLowerCase());
				}
			}
		}
		bufRead.close();
	}


	public static NameGenerator defaultNameGenerator() {
		try {
			return new NameGenerator(NameGenerator.class.getResourceAsStream(FILE));
		}
		catch (IOException e) {
			throw new RuntimeException("Problem loading " + FILE, e);
		}
	}


	private String upper(String s) {
		return s.substring(0, 1).toUpperCase().concat(s.substring(1));
	}


	private boolean containsConsFirst(ArrayList<String> array) {
		for (String s : array) {
			if (consonantFirst(s)) {
				return true;
			}
		}
		return false;
	}


	private boolean containsVocFirst(ArrayList<String> array) {
		for (String s : array) {
			if (VowelFirst(s)) {
				return true;
			}
		}
		return false;
	}


	private boolean allowCons(ArrayList<String> array) {
		for (String s : array) {
			if (hatesPreviousVowels(s) || !hatesPreviousConsonants(s)) {
				return true;
			}
		}
		return false;
	}


	private boolean allowVocs(ArrayList<String> array) {
		for (String s : array) {
			if (hatesPreviousConsonants(s) || !hatesPreviousVowels(s)) {
				return true;
			}
		}
		return false;
	}


	private boolean expectsVowel(String s) {
		if (s.substring(1).contains("+v")) {
			return true;
		}

		return false;
	}


	private boolean expectsConsonant(String s) {
		if (s.substring(1).contains("+c")) {
			return true;
		}

		return false;
	}


	private boolean hatesPreviousVowels(String s) {
		if (s.substring(1).contains("-c")) {
			return true;
		}

		return false;
	}


	private boolean hatesPreviousConsonants(String s) {
		if (s.substring(1).contains("-v")) {
			return true;
		}

		return false;
	}


	private String pureSyl(String s) {
		s = s.trim();
		if (s.charAt(0) == '+' || s.charAt(0) == '-') {
			s = s.substring(1);
		}
		return s.split(" ")[0];
	}


	private boolean VowelFirst(String s) {
		return (String.copyValueOf(Vowels).contains(String.valueOf(s.charAt(0)).toLowerCase()));
	}


	private boolean consonantFirst(String s) {
		return (String.copyValueOf(consonants).contains(String.valueOf(s.charAt(0)).toLowerCase()));
	}


	private boolean VowelLast(String s) {
		return (String.copyValueOf(Vowels).contains(String.valueOf(s.charAt(s.length() - 1)).toLowerCase()));
	}


	private boolean consonantLast(String s) {
		return (String.copyValueOf(consonants).contains(String.valueOf(s.charAt(s.length() - 1)).toLowerCase()));
	}


	/**
	 * Compose a new name.
	 *
	 * @param syllables The number of syllables used in name.
	 * @return Returns composed name as a String
	 * @throws RuntimeException when logical mistakes are detected inside chosen file, and program is unable to complete the name.
	 */
	public String compose(int syllables) {
		if (syllables > 2 && this.mid.isEmpty()) {
			throw new RuntimeException("You are trying to create a name with more than 3 parts, which requires middle parts, " + "which you have none in the file."
					+ " You should add some. Every word, which doesn't have + or - for a prefix is counted as a middle part.");
		}
		if (this.pre.isEmpty()) {
			throw new RuntimeException("You have no prefixes to start creating a name. add some and use \"-\" prefix, to identify it as a prefix for a name. (example: -asd)");
		}
		if (this.sur.isEmpty()) {
			throw new RuntimeException("You have no suffixes to end a name. add some and use \"+\" prefix, to identify it as a suffix for a name. (example: +asd)");
		}
		if (syllables < 1) {
			throw new RuntimeException("compose(int syllables) can't have less than 1 syllable");
		}
		int expecting = 0; // 1 for Vowel, 2 for consonant
		int last; // 1 for Vowel, 2 for consonant
		String name;
		int a = (int) (RandomUtils.randomDoubleZeroToOne() * this.pre.size());

		if (VowelLast(pureSyl(this.pre.get(a)))) {
			last = 1;
		}
		else {
			last = 2;
		}

		if (syllables > 2) {
			if (expectsVowel(this.pre.get(a))) {
				expecting = 1;
				if (!containsVocFirst(this.mid)) {
					throw new RuntimeException("Expecting \"middle\" part starting with Vowel, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
			}
			if (expectsConsonant(this.pre.get(a))) {
				expecting = 2;
				if (!containsConsFirst(this.mid)) {
					throw new RuntimeException("Expecting \"middle\" part starting with consonant, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
			}
		}
		else {
			if (expectsVowel(this.pre.get(a))) {
				expecting = 1;
				if (!containsVocFirst(this.sur)) {
					throw new RuntimeException("Expecting \"suffix\" part starting with Vowel, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
			}
			if (expectsConsonant(this.pre.get(a))) {
				expecting = 2;
				if (!containsConsFirst(this.sur)) {
					throw new RuntimeException("Expecting \"suffix\" part starting with consonant, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
			}
		}
		if (VowelLast(pureSyl(this.pre.get(a))) && !allowVocs(this.mid)) {
			throw new RuntimeException("Expecting \"middle\" part that allows last character of prefix to be a Vowel, "
					+ "but there is none. You should add one, or remove requirements that cannot be fulfilled.. the prefix used, was : \"" + this.pre.get(a) + "\", which"
					+ "means there should be a part available, that has \"-v\" requirement or no requirements for previous syllables at all.");
		}

		if (consonantLast(pureSyl(this.pre.get(a))) && !allowCons(this.mid)) {
			throw new RuntimeException("Expecting \"middle\" part that allows last character of prefix to be a consonant, "
					+ "but there is none. You should add one, or remove requirements that cannot be fulfilled.. the prefix used, was : \"" + this.pre.get(a) + "\", which"
					+ "means there should be a part available, that has \"-c\" requirement or no requirements for previous syllables at all.");
		}

		int[] b = new int[syllables];
		for (int i = 0; i < b.length - 2; i++) {

			do {
				b[i] = (int) (RandomUtils.randomDoubleZeroToOne() * this.mid.size());
			}
			while (expecting == 1 && !VowelFirst(pureSyl(this.mid.get(b[i]))) || expecting == 2 && !consonantFirst(pureSyl(this.mid.get(b[i]))) || last == 1
					&& hatesPreviousVowels(this.mid.get(b[i])) || last == 2 && hatesPreviousConsonants(this.mid.get(b[i])));

			expecting = 0;
			if (expectsVowel(this.mid.get(b[i]))) {
				expecting = 1;
				if (i < b.length - 3 && !containsVocFirst(this.mid)) {
					throw new RuntimeException("Expecting \"middle\" part starting with Vowel, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
				if (i == b.length - 3 && !containsVocFirst(this.sur)) {
					throw new RuntimeException("Expecting \"suffix\" part starting with Vowel, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
			}
			if (expectsConsonant(this.mid.get(b[i]))) {
				expecting = 2;
				if (i < b.length - 3 && !containsConsFirst(this.mid)) {
					throw new RuntimeException("Expecting \"middle\" part starting with consonant, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
				if (i == b.length - 3 && !containsConsFirst(this.sur)) {
					throw new RuntimeException("Expecting \"suffix\" part starting with consonant, " + "but there is none. You should add one, or remove requirement for one.. ");
				}
			}
			if (VowelLast(pureSyl(this.mid.get(b[i]))) && !allowVocs(this.mid) && syllables > 3) {
				throw new RuntimeException("Expecting \"middle\" part that allows last character of last syllable to be a Vowel, "
						+ "but there is none. You should add one, or remove requirements that cannot be fulfilled.. the part used, was : \"" + this.mid.get(b[i]) + "\", which "
						+ "means there should be a part available, that has \"-v\" requirement or no requirements for previous syllables at all.");
			}

			if (consonantLast(pureSyl(this.mid.get(b[i]))) && !allowCons(this.mid) && syllables > 3) {
				throw new RuntimeException("Expecting \"middle\" part that allows last character of last syllable to be a consonant, "
						+ "but there is none. You should add one, or remove requirements that cannot be fulfilled.. the part used, was : \"" + this.mid.get(b[i]) + "\", which "
						+ "means there should be a part available, that has \"-c\" requirement or no requirements for previous syllables at all.");
			}
			if (i == b.length - 3) {
				if (VowelLast(pureSyl(this.mid.get(b[i]))) && !allowVocs(this.sur)) {
					throw new RuntimeException("Expecting \"suffix\" part that allows last character of last syllable to be a Vowel, "
							+ "but there is none. You should add one, or remove requirements that cannot be fulfilled.. the part used, was : \"" + this.mid.get(b[i]) + "\", which "
							+ "means there should be a suffix available, that has \"-v\" requirement or no requirements for previous syllables at all.");
				}

				if (consonantLast(pureSyl(this.mid.get(b[i]))) && !allowCons(this.sur)) {
					throw new RuntimeException("Expecting \"suffix\" part that allows last character of last syllable to be a consonant, "
							+ "but there is none. You should add one, or remove requirements that cannot be fulfilled.. the part used, was : \"" + this.mid.get(b[i]) + "\", which "
							+ "means there should be a suffix available, that has \"-c\" requirement or no requirements for previous syllables at all.");
				}
			}
			if (VowelLast(pureSyl(this.mid.get(b[i])))) {
				last = 1;
			}
			else {
				last = 2;
			}
		}

		int c;
		do {
			c = (int) (RandomUtils.randomDoubleZeroToOne() * this.sur.size());
		}
		while (expecting == 1 && !VowelFirst(pureSyl(this.sur.get(c))) || expecting == 2 && !consonantFirst(pureSyl(this.sur.get(c))) || last == 1
				&& hatesPreviousVowels(this.sur.get(c)) || last == 2 && hatesPreviousConsonants(this.sur.get(c)));

		name = upper(pureSyl(this.pre.get(a).toLowerCase()));
		for (int i = 0; i < b.length - 2; i++) {
			name = name.concat(pureSyl(this.mid.get(b[i]).toLowerCase()));
		}
		if (syllables > 1) {
			name = name.concat(pureSyl(this.sur.get(c).toLowerCase()));
		}
		return name;
	}
}
