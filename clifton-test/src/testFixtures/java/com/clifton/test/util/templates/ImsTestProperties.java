package com.clifton.test.util.templates;


import com.clifton.test.util.PropertiesLoader;

import java.util.Properties;


/**
 * @author jgommels
 */
public class ImsTestProperties {

	public static final String IMS_URL;
	public static final String TEST_USER_ADMIN;
	public static final String TEST_USER_ADMIN_PASSWORD;
	public static final String TEST_USER_2;
	public static final String TEST_USER_2_PASSWORD;
	public static final String TEST_USER_3;
	public static final String TEST_USER_3_PASSWORD;

	public static final String TEST_ANALYST_USER;
	public static final String TEST_ANALYST_USER_PASSWORD;

	public static final String TEST_COMPLIANCE_USER;
	public static final String TEST_COMPLIANCE_USER_PASSWORD;

	public static final String TEST_NO_PERMISSION_USER;
	public static final String TEST_NO_PERMISSION_USER_PASSWORD;

	public static final String TEST_CLIENT_ADMIN_USER;
	public static final String TEST_CLIENT_ADMIN_USER_PASSWORD;

	public static final String TEST_TRADER_USER;
	public static final String TEST_TRADER_USER_PASSWORD;

	public static final String DATASOURCE_NAME;
	public static final String DATASOURCE_DRIVER_CLASS_NAME;
	public static final String DATASOURCE_URL;
	public static final String DATASOURCE_USER;
	public static final String DATASOURCE_PASSWORD;


	static {
		Properties properties = PropertiesLoader.getProperties();

		DATASOURCE_NAME = properties.getProperty("dataSource.databaseName");
		DATASOURCE_DRIVER_CLASS_NAME = properties.getProperty("dataSource.driverClassName");
		DATASOURCE_URL = properties.getProperty("dataSource.url");
		DATASOURCE_USER = properties.getProperty("dataSource.username");
		DATASOURCE_PASSWORD = properties.getProperty("dataSource.password");

		IMS_URL = properties.getProperty("ims.url");

		TEST_USER_ADMIN = properties.getProperty("ims.username");
		TEST_USER_ADMIN_PASSWORD = properties.getProperty("ims.password");

		TEST_USER_2 = properties.getProperty("ims.username2");
		TEST_USER_2_PASSWORD = properties.getProperty("ims.password2");

		TEST_USER_3 = properties.getProperty("ims.username3");
		TEST_USER_3_PASSWORD = properties.getProperty("ims.password3");

		TEST_ANALYST_USER = properties.getProperty("ims.analyst.username");
		TEST_ANALYST_USER_PASSWORD = properties.getProperty("ims.analyst.password");

		TEST_COMPLIANCE_USER = properties.getProperty("ims.compliance.username");
		TEST_COMPLIANCE_USER_PASSWORD = properties.getProperty("ims.compliance.password");

		TEST_NO_PERMISSION_USER = properties.getProperty("ims.no.permission.username");
		TEST_NO_PERMISSION_USER_PASSWORD = properties.getProperty("ims.no.permission.password");

		TEST_CLIENT_ADMIN_USER = properties.getProperty("ims.imsclientadminuser.username");
		TEST_CLIENT_ADMIN_USER_PASSWORD = properties.getProperty("ims.imsclientadminuser.password");

		TEST_TRADER_USER = properties.getProperty("ims.trader.username");
		TEST_TRADER_USER_PASSWORD = properties.getProperty("ims.trader.password");
	}
}
