package com.clifton.test.util;

import com.clifton.core.concurrent.ConcurrentUtils;
import com.clifton.core.concurrent.NamedThreadFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


/**
 * <code>ConcurrentExecutionUtils</code> allows for executing test tasks concurrently.
 *
 * @author NickK
 */
public class ConcurrentExecutionUtils {

	/**
	 * Executes the provided task within multiple threads. It will run the task n times, where n is the number
	 * of processors with a minimum of 4, using n/2 concurrent threads. Returns a list of each execution's result.
	 */
	public static <R> List<ConcurrentExecutionResponse<R>> executeConcurrently(Callable<R> task) {
		int processorCount = Runtime.getRuntime().availableProcessors();
		if (processorCount < 4) {
			processorCount = 4;
		}
		return executeConcurrently(processorCount / 2, processorCount, task);
	}


	/**
	 * Executes the provided task within multiple threads. It will run the task with concurrentTaskCount concurrent
	 * threads and a total of totalExecutionCount times. Returns a list of each execution's result.
	 */
	public static <R> List<ConcurrentExecutionResponse<R>> executeConcurrently(int concurrentTaskCount, int totalExecutionCount, Callable<R> task) {
		if (task == null) {
			return Collections.emptyList();
		}
		@SuppressWarnings("unchecked")
		Callable<R>[] tasks = new Callable[totalExecutionCount];
		for (int i = 0; i < totalExecutionCount; i++) {
			tasks[i] = task;
		}
		return executeConcurrently(concurrentTaskCount, tasks);
	}


	/**
	 * Executes the provided tasks concurrently within a pool of concurrentTaskCount threads.
	 * Returns a list of each execution's result.
	 */
	@SafeVarargs
	public static <R> List<ConcurrentExecutionResponse<R>> executeConcurrently(int concurrentTaskCount, Callable<R>... tasks) {
		if (tasks == null) {
			return Collections.emptyList();
		}
		ExecutorService executor = Executors.newFixedThreadPool(concurrentTaskCount, new NamedThreadFactory("TestPoolWorker"));
		CompletionService<R> completionService = new ExecutorCompletionService<>(executor);

		try {
			for (Callable<R> task : tasks) {
				completionService.submit(task);
			}

			return getFutureResultsFromCompletionServiceTasks(completionService, tasks.length);
		}
		finally {
			ConcurrentUtils.shutdownExecutorService(executor);
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * This method will gather the results of the tasks submitted to the provided {@link CompletionService}.
	 * Each result will have a {@link ConcurrentExecutionResponse} created for it. If a throwable is thrown
	 * while getting the result of a task, a response will be created reflecting the error.
	 * <p>
	 * This method should only be called the number of times a task was submitted to the completion service.
	 * Calling this method for (numberOfTasksSubmitted + 1) times will result in indefinite blocking.
	 */
	private static <R> List<ConcurrentExecutionResponse<R>> getFutureResultsFromCompletionServiceTasks(CompletionService<R> completionService, int numberOfTasksSubmitted) {
		List<ConcurrentExecutionResponse<R>> results = new ArrayList<>(numberOfTasksSubmitted);
		for (int i = 0; i < numberOfTasksSubmitted; i++) {
			try {
				R result = completionService.take().get();
				results.add(ConcurrentExecutionResponse.createResponse(result));
			}
			catch (InterruptedException e) {
				// reset interrupted flag
				Thread.currentThread().interrupt();
				results.add(ConcurrentExecutionResponse.createResponse(e));
			}
			catch (Throwable throwable) {
				results.add(ConcurrentExecutionResponse.createResponse(throwable));
			}
		}
		return results;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class ConcurrentExecutionResponse<R> {

		private R result;
		private Throwable error;


		private ConcurrentExecutionResponse() {
		}


		static <R> ConcurrentExecutionResponse<R> createResponse(R result) {
			ConcurrentExecutionResponse<R> response = new ConcurrentExecutionResponse<>();
			response.result = result;
			return response;
		}


		static <R> ConcurrentExecutionResponse<R> createResponse(Throwable error) {
			ConcurrentExecutionResponse<R> response = new ConcurrentExecutionResponse<>();
			response.error = error;
			return response;
		}


		public boolean isSuccessful() {
			return getError() == null;
		}


		public R getResult() {
			return this.result;
		}


		public Throwable getError() {
			return this.error;
		}
	}
}
