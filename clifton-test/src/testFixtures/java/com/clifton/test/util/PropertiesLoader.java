package com.clifton.test.util;


import com.clifton.core.dataaccess.file.TempFilePath;
import com.clifton.core.util.beans.Lazy;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;


public class PropertiesLoader {

	private static final String PROPERTIES_FILE = "/META-INF/integration-tests.properties";
	private static final String PROPERTIES_OVERRIDE_FILE = "/META-INF/integration-tests-overrides.properties";

	// Hold a reference to the loaded properties to avoid each test loading the same file.
	private static final Lazy<Properties> propertiesLazy = new Lazy<>(PropertiesLoader::generateProperties);
	private static final Lazy<File> propertiesFileLazy = new Lazy<>(PropertiesLoader::generatePropertiesFile);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static Properties getProperties() {
		return propertiesLazy.get();
	}


	public static File getPropertiesFileLazy() {
		return propertiesFileLazy.get();
	}


	public static String getPropertiesFileUri() {
		return getPropertiesFileLazy().toURI().toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static Properties generateProperties() {
		Properties props = new Properties();
		try {
			props.load(PropertiesLoader.class.getResourceAsStream(PROPERTIES_FILE));
		}
		catch (Exception e) {
			throw new RuntimeException("Couldn't load " + PROPERTIES_FILE, e);
		}
		try {
			props.load(PropertiesLoader.class.getResourceAsStream(PROPERTIES_OVERRIDE_FILE));
		}
		catch (Exception e) {
			throw new RuntimeException("Couldn't load " + PROPERTIES_OVERRIDE_FILE, e);
		}
		return props;
	}


	private static File generatePropertiesFile() {
		Properties properties = getProperties();
		try {
			TempFilePath tempFilePath = TempFilePath.createTempPath("test-properties", "properties");
			properties.store(new FileOutputStream(tempFilePath.getTempFile()), null);
			return tempFilePath.getTempFile();
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
