package com.clifton.test.junit;


import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.junit.jupiter.api.extension.TestWatcher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class TestWatcherLogger implements TestWatcher, BeforeAllCallback {

	private static final Logger log = LoggerFactory.getLogger(TestWatcherLogger.class);


	@Override
	public void beforeAll(ExtensionContext context) {
		log.info("Test STARTING: {}", context.getDisplayName());
	}


	@Override
	public void testSuccessful(ExtensionContext context) {
		log.info("Test SUCCEEDED: {}", context.getDisplayName());
	}


	@Override
	public void testFailed(ExtensionContext context, Throwable cause) {
		String message = "Test FAILED: " + (context == null ? "Unknown Context" : context.getDisplayName());
		log.error(message, cause);
	}
}
