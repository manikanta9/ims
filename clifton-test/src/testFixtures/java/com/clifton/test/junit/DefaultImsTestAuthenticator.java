package com.clifton.test.junit;

import com.clifton.test.util.UserUtils;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.extension.BeforeAllCallback;
import org.junit.jupiter.api.extension.ExtensionContext;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit.jupiter.SpringExtension;


/**
 * The <code>DefaultImsTestAuthenticator</code> runs before every unit test and logs in the
 * default user (typically imstestuser1).
 *
 * @author jgommels
 */
public class DefaultImsTestAuthenticator implements BeforeAllCallback {

	@Override
	public void beforeAll(ExtensionContext context) {
		ApplicationContext applicationContext = SpringExtension.getApplicationContext(context);
		UserUtils userUtils = applicationContext.getBean(UserUtils.class);
		userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
	}
}
