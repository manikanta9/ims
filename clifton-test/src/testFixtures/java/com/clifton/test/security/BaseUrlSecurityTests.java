package com.clifton.test.security;

import com.clifton.core.context.ApplicationContextService;
import com.clifton.core.context.DoNotAddRequestMapping;
import com.clifton.core.context.spring.UrlDefinitionDescriptor;
import com.clifton.core.security.authorization.SecureClass;
import com.clifton.core.security.authorization.SecureMethod;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.test.BaseIntegrationTest;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.protocol.ImsProtocolClient;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * @author manderson
 */
public abstract class BaseUrlSecurityTests extends BaseIntegrationTest {

	@Resource
	private ApplicationContextService applicationContextService;
	@Resource
	private ImsProtocolClient imsProtocolClient;


	@Test
	public void testUrlSecureClass() {
		// Start with admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);

		List<UrlDefinitionDescriptor> urlList = this.applicationContextService.getContextUrlMappingDefinitionList();

		// Switch to Non Admin User Without Any Security Permissions
		this.userUtils.switchUser(ImsTestProperties.TEST_NO_PERMISSION_USER, ImsTestProperties.TEST_NO_PERMISSION_USER_PASSWORD);

		List<String> errors = new ArrayList<>();

		int count = 0;
		for (UrlDefinitionDescriptor urlDefinition : CollectionUtils.getIterable(urlList)) {
			if (isSecureClass(urlDefinition)) {
				count++;
				String error = executeUrl(urlDefinition);
				if (!StringUtils.isEmpty(error)) {
					// If message contains: Admin specific message - then it's admin only URL which is good
					// then security was automatically configured and it is good
					if (!error.contains("Only Administrators can access") && !error.contains("non-admin user")) {
						errors.add(error);
					}
				}
			}
		}

		if (CollectionUtils.getSize(errors) > 0) {
			Collections.sort(errors);
			throw new RuntimeException("Invalid SecureClass (Admin Only) Mapping found for [" + errors.size() + "] out of [" + count + "] URLS: "
					+ StringUtils.collectionToDelimitedString(errors, StringUtils.NEW_LINE));
		}
	}


	/**
	 * Although the above test confirms the logic for admin only urls when using SecureClass annotation, in general we should not have any admin only
	 * and the security should be mapped to appropriate resource, i.e. System Management
	 */
	@Test
	public void testAdminOnlyUrlSecurity() {
		// Start with admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);

		List<UrlDefinitionDescriptor> urlList = this.applicationContextService.getContextUrlMappingDefinitionList();

		// Switch to Non Admin User Without Any Security Permissions
		this.userUtils.switchUser(ImsTestProperties.TEST_NO_PERMISSION_USER, ImsTestProperties.TEST_NO_PERMISSION_USER_PASSWORD);

		List<String> errors = new ArrayList<>();

		int count = 0;

		for (UrlDefinitionDescriptor urlDefinition : CollectionUtils.getIterable(urlList)) {
			// Hit Every URL
			count++;
			String error = executeUrl(urlDefinition);
			if (!StringUtils.isEmpty(error)) {
				if (error.contains("Only Administrators can access")) {
					errors.add(error);
				}
			}
		}

		if (CollectionUtils.getSize(errors) > 0) {
			Collections.sort(errors);
			throw new RuntimeException("Admin Security Mapping found for [" + errors.size() + "] out of [" + count + "] URLS. Security should be re-configured to avoid Admin only requirements: "
					+ StringUtils.NEW_LINE + StringUtils.collectionToDelimitedString(errors, StringUtils.NEW_LINE));
		}
	}


	@Test
	public void testUrlSecurity() {
		// Start with admin user
		this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);

		List<UrlDefinitionDescriptor> urlList = this.applicationContextService.getContextUrlMappingDefinitionList();

		// Switch to Non Admin User Without Any Security Permissions
		this.userUtils.switchUser(ImsTestProperties.TEST_NO_PERMISSION_USER, ImsTestProperties.TEST_NO_PERMISSION_USER_PASSWORD);

		List<String> errors = new ArrayList<>();

		int count = 0;
		int skipCount = 0;

		for (UrlDefinitionDescriptor urlDefinition : CollectionUtils.getIterable(urlList)) {
			if (!isSecureClass(urlDefinition)) {
				if (!isSecurityExplicitOrSkipped(urlDefinition)) {

					count++;
					String error = executeUrl(urlDefinition);
					if (!StringUtils.isEmpty(error)) {
						// If message contains: "Access Denied"
						// then security was automatically configured and it is good
						if (!error.contains("Access Denied")) {
							errors.add(error);
						}
						// UNLESS IT SAYS UNMAPPED RESOURCE...Additional check to help find tables missing security mappings before it gets to Production
						else if (error.contains("UNMAPPED RESOURCE")) {
							errors.add(error);
						}
					}
				}
				else {
					skipCount++;
				}
			}
		}

		if (CollectionUtils.getSize(errors) > 0) {
			Collections.sort(errors);
			throw new RuntimeException("Invalid Security Mapping found for [" + errors.size() + "] out of [" + count + "] URLS (Skipped " + skipCount + " explicit security): "
					+ StringUtils.NEW_LINE + StringUtils.collectionToDelimitedString(errors, StringUtils.NEW_LINE));
		}
	}


	protected String executeUrl(UrlDefinitionDescriptor urlDefinition) {
		String url = urlDefinition.getUrl();
		if (!url.startsWith("/")) {
			url = "/" + url;
		}
		if (!url.endsWith(".json")) {
			url = url + ".json";
		}
		String msgPrefix = url + " (" + urlDefinition.getBeanName() + "." + urlDefinition.getMethodName() + "): ";

		try {
			this.imsProtocolClient.request(url, new ArrayList<>(), Void.TYPE, "", false);
		}
		catch (Throwable e) {
			String msg = null;
			if (e instanceof ImsErrorResponseException) {
				msg = ((ImsErrorResponseException) e).getErrorMessageFromIMS();
			}
			return msgPrefix + (msg != null ? msg : e.getMessage());
		}
		return null;
	}


	private boolean isSecureClass(UrlDefinitionDescriptor urlDefinition) {
		Class<?> clz = CoreClassUtils.getClass(urlDefinition.getBeanClassName());
		// If Class is Defined as Secure (i.e. Admin Only) - Consider Explicit
		SecureClass sc = AnnotationUtils.getAnnotation(clz, SecureClass.class);
		if (sc != null) {
			return true;
		}
		return false;
	}


	/**
	 * Returns true if security annotation is present
	 * Also returns true is DoNotAddRequestMapping is present, but allowApi = true
	 */
	private boolean isSecurityExplicitOrSkipped(UrlDefinitionDescriptor urlDefinition) {
		Class<?> clz = CoreClassUtils.getClass(urlDefinition.getBeanClassName());
		if (isExcludeClass(clz)) {
			return true;
		}
		Method[] methods = clz.getMethods();
		for (Method method : methods) {
			if (method.getName().equals(urlDefinition.getMethodName())) {
				if (AnnotationUtils.isTrue(method, DoNotAddRequestMapping.class, DoNotAddRequestMapping::allowAPI)) {
					return true;
				}
				SecureMethod sm = AnnotationUtils.getAnnotation(method, SecureMethod.class);
				if (sm != null) {
					return true;
				}
			}
		}
		return false;
	}


	protected boolean isExcludeClass(Class<?> clz) {
		return false;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public ApplicationContextService getApplicationContextService() {
		return this.applicationContextService;
	}
}
