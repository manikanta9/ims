package com.clifton.test.spring;

import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.context.ExcludeFromComponentScan;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.beans.Lazy;
import com.clifton.test.protocol.ImsProtocolClient;
import com.clifton.test.protocol.ImsProtocolClientConfiguration;
import com.clifton.test.protocol.ImsProtocolClientImpl;
import com.clifton.test.protocol.ServiceInvocationHandler;
import com.clifton.test.util.templates.ImsTestProperties;
import org.reflections.Reflections;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanDefinitionHolder;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.BeanReference;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.config.RuntimeBeanReference;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Proxy;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


@Configuration
@ExcludeFromComponentScan
public class ImsProtocolClientSpringConfiguration {

	private static final Logger log = LoggerFactory.getLogger(ImsProtocolClientSpringConfiguration.class);
	public static final String PROTOCOL_CLIENT_BEAN_NAME = "imsProtocolClient";

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Bean(PROTOCOL_CLIENT_BEAN_NAME)
	public ImsProtocolClient imsProtocolClient(ImsProtocolClientConfiguration imsProtocolClientConfiguration) {
		//Load ims url through ims-url property override, otherwise through properties file if not defined
		String url = !StringUtils.isEmpty(System.getProperty("ims-url")) ? System.getProperty("ims-url") : ImsTestProperties.IMS_URL;
		String username = ImsTestProperties.TEST_USER_ADMIN;
		String password = ImsTestProperties.TEST_USER_ADMIN_PASSWORD;

		if (url == null) {
			throw new RuntimeException("ims.url was not specified in properties file.");
		}

		log.info("Configuring spring dependency injection...");
		return new ImsProtocolClientImpl(url, username, password, imsProtocolClientConfiguration);
	}


	@Bean
	public static BeanFactoryPostProcessor imsProtocolClientServiceProxyBeanFactoryPostProcessor() {
		return new ImsProtocolClientServiceProxyBeanFactoryPostProcessor();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The {@link ImsProtocolClientServiceProxyBeanFactoryPostProcessor} class is a {@link BeanFactoryPostProcessor} which defines {@link ServiceInvocationHandler} proxies for
	 * {@link Service} implementations. These proxies send requests directly to the targeted application, rather than being executed locally.
	 */
	private static class ImsProtocolClientServiceProxyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

		private static final Logger log = LoggerFactory.getLogger(ImsProtocolClientServiceProxyBeanFactoryPostProcessor.class);

		private static final Map<String, String> serviceInterfaceOverrideMap = new HashMap<>();
		// Scan and initialize the set of service implementation classes once.
		private static final Lazy<Set<Class<?>>> serviceImplClassListLazy = new Lazy<>(() -> {
			Set<Class<?>> serviceClasses = new Reflections(ContextConventionUtils.PACKAGE_BASIC_PREFIX_CONVENTION).getTypesAnnotatedWith(Service.class);
			return CollectionUtils.getStream(serviceClasses).filter(service -> service.getName().endsWith("ServiceImpl")).collect(Collectors.toSet());
		});


		static {
			ImsProtocolClientServiceProxyBeanFactoryPostProcessor.serviceInterfaceOverrideMap.put("com.clifton.core.context.spring.SpringApplicationContextServiceImpl", "com.clifton.core.context.ApplicationContextService");
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		@Override
		public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
			DefaultListableBeanFactory defaultListableBeanFactory = (DefaultListableBeanFactory) beanFactory;
			BeanReference protocolClientBeanRef = new RuntimeBeanReference(PROTOCOL_CLIENT_BEAN_NAME);
			for (Class<?> serviceImpl : serviceImplClassListLazy.get()) {
				BeanDefinitionHolder serviceProxyBeanDef = createServiceProxyBeanDefinition(serviceImpl, protocolClientBeanRef);
				if (serviceProxyBeanDef != null) {
					defaultListableBeanFactory.registerBeanDefinition(serviceProxyBeanDef.getBeanName(), serviceProxyBeanDef.getBeanDefinition());
				}
			}
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private <T> BeanDefinitionHolder createServiceProxyBeanDefinition(Class<T> serviceImplClass, BeanReference protocolClientBeanRef) {
			BeanDefinitionHolder beanDefinitionHolder = null;
			String serviceInterfaceName = getServiceInterfaceName(serviceImplClass.getName());
			try {
				@SuppressWarnings("unchecked")
				Class<? super T> serviceInterface = (Class<? super T>) Class.forName(serviceInterfaceName);
				// Create invocation handler definition
				AbstractBeanDefinition invocationHandlerBd = BeanDefinitionBuilder.genericBeanDefinition(ServiceInvocationHandler.class)
						.addConstructorArgValue(serviceImplClass)
						.addConstructorArgValue(protocolClientBeanRef)
						.getBeanDefinition();

				// Create proxy via invocation handler definition
				AbstractBeanDefinition serviceProxyBd = BeanDefinitionBuilder.genericBeanDefinition(JdkProxyFactoryBean.class)
						.addConstructorArgValue(serviceInterface.getClassLoader())
						.addConstructorArgValue(new Class[]{serviceInterface})
						.addConstructorArgValue(invocationHandlerBd)
						.getBeanDefinition();
				beanDefinitionHolder = new BeanDefinitionHolder(serviceProxyBd, serviceInterface.getName());
			}
			catch (ClassNotFoundException e) {
				log.warn("Could not find interface for service implementation {}. Expected interface name: {}. (This may not be a problem.)", serviceImplClass.getName(), serviceInterfaceName);
			}
			return beanDefinitionHolder;
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		/**
		 * The {@link JdkProxyFactoryBean} class is a {@link FactoryBean} for defining beans as {@link Proxy JDK proxies}. This type offers fewer features than the Spring AOP
		 * {@link ProxyFactoryBean}, but allows invocation handlers to be assigned by reference rather than by name, allowing value types such as inner beans (via {@link
		 * BeanDefinition} values) or explicit bean references.
		 *
		 * @see ProxyFactoryBean
		 */
		public static class JdkProxyFactoryBean implements FactoryBean<Object> {

			private final ClassLoader classLoader;
			private final Class<?>[] interfaces;
			private final InvocationHandler invocationHandler;
			private final Class<?> targetType;


			public JdkProxyFactoryBean(ClassLoader classLoader, Class<?>[] interfaces, InvocationHandler invocationHandler) {
				this.classLoader = classLoader;
				this.interfaces = interfaces;
				this.invocationHandler = invocationHandler;
				this.targetType = Proxy.getProxyClass(this.classLoader, interfaces);
			}


			@Override
			public Object getObject() {
				return Proxy.newProxyInstance(this.classLoader, this.interfaces, this.invocationHandler);
			}


			@Override
			public Class<?> getObjectType() {
				return this.targetType;
			}
		}

		////////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////////


		private String getServiceInterfaceName(String serviceImplName) {
			String overriddenServiceName = serviceInterfaceOverrideMap.get(serviceImplName);
			return overriddenServiceName != null ? overriddenServiceName : serviceImplName.substring(0, serviceImplName.length() - "Impl".length());
		}
	}
}
