package com.clifton.test.spring;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.Arrays;
import java.util.Collection;


/**
 * An {@link ApplicationContext} implementation which performs the context lifecycle startup events up to and including the {@link BeanFactoryPostProcessor} execution, and then
 * halts after storing relevant information.
 * <p>
 * This context implementation is useful for monitoring the state of the context during phases of its lifecycle.
 *
 * @author MikeH
 */
public class BeanFactoryWatchingApplicationContext extends ClassPathXmlApplicationContext {

	private Collection<String> generatedSingletonNameList;


	public BeanFactoryWatchingApplicationContext(String configLocation) throws BeansException {
		super(configLocation);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public void refresh() throws BeansException, IllegalStateException {
		/*
		 * This snippet is taken from org.springframework.context.support.AbstractApplicationContext.refresh(). This is the minimum required set of application context
		 * initialization steps necessary to verify the tested constraints.
		 */
		prepareRefresh();
		ConfigurableListableBeanFactory beanFactory = obtainFreshBeanFactory();
		prepareBeanFactory(beanFactory);
		postProcessBeanFactory(beanFactory);
		invokeBeanFactoryPostProcessors(beanFactory);
	}


	@Override
	protected void invokeBeanFactoryPostProcessors(ConfigurableListableBeanFactory beanFactory) {
		super.invokeBeanFactoryPostProcessors(beanFactory);
		// Store the list of generated singletons at this point in the context lifecycle
		setGeneratedSingletonNameList(Arrays.asList(beanFactory.getSingletonNames()));
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public Collection<String> getGeneratedSingletonNameList() {
		return this.generatedSingletonNameList;
	}


	public void setGeneratedSingletonNameList(Collection<String> generatedSingletonNameList) {
		this.generatedSingletonNameList = generatedSingletonNameList;
	}
}
