package com.clifton.test.spring;

import org.springframework.context.Lifecycle;
import org.springframework.context.LifecycleProcessor;
import org.springframework.context.support.AbstractApplicationContext;


/**
 * The {@link NoopLifecycleProcessor} class is a no-op {@link LifecycleProcessor}. This is used to disable {@link Lifecycle} beans start-up management in order to avoid
 * unnecessary component initialization. Lifecycle component initialization can be problematic during test execution. For example, JMS listener container lifecycle components
 * (<code>AbstractJmsListeningContainer</code>) may spin on start-up until a connection is successfully established, even though the endpoint may not be configured to work
 * within the current test environment.
 * <p>
 * To use this lifecycle processor, define it as a bean named {@value AbstractApplicationContext#LIFECYCLE_PROCESSOR_BEAN_NAME}.
 *
 * @author MikeH
 */
public class NoopLifecycleProcessor implements LifecycleProcessor {

	@Override
	public void onRefresh() {
		// Do nothing
	}


	@Override
	public void onClose() {
		// Do nothing
	}


	@Override
	public void start() {
		// Do nothing
	}


	@Override
	public void stop() {
		// Do nothing
	}


	@Override
	public boolean isRunning() {
		return false;
	}
}
