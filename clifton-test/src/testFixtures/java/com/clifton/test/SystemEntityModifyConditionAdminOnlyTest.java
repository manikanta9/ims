package com.clifton.test;


import com.clifton.core.beans.IdentityObject;
import com.clifton.core.dataaccess.dao.AdvancedReadOnlyDAO;
import com.clifton.core.dataaccess.dao.ReadOnlyDAO;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.search.hibernate.HibernateSearchConfigurer;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MapUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAware;
import com.clifton.system.security.authorization.entitymodify.SystemEntityModifyConditionAwareAdminRequired;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Spliterator;
import java.util.Spliterators;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;


/**
 * The {@link SystemEntityModifyConditionAdminOnlyTest} class defines integration tests to identify {@link SystemEntityModifyConditionAwareAdminRequired} database entities which
 * require administrative access due to missing {@link SystemEntityModifyConditionAware#getEntityModifyCondition() entity modify conditions}. Such entities should be avoided, since
 * administrator access is highly discouraged and is locked down to "break-glass" scenarios for certain applications. These tests allow these entities to be identified and either
 * resolved or ignored for future test executions.
 *
 * @author ignacioa
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS) // Allow non-static @BeforeAll implementation
@ExtendWith(SpringExtension.class)
public abstract class SystemEntityModifyConditionAdminOnlyTest {

	private static final int MAX_VIOLATIONS_PER_TABLE = 20;
	// Query batch size; loosely optimized based on local testing
	private static final int QUERY_BATCH_SIZE = 50000;

	@Resource
	private DaoLocator daoLocator;

	@SuppressWarnings("rawtypes")
	private final MultiValueMap<Class, Predicate<SystemEntityModifyConditionAwareAdminRequired>> approvedEntityConditionMap = new MultiValueHashMap<>(false);

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeAll
	public void setup() {
		configure();
	}


	protected void configure() {
		// Override to configure entity approval conditions
	}


	protected final <T extends SystemEntityModifyConditionAwareAdminRequired> void addApprovedEntity(Class<T> entityType, Predicate<T> condition) {
		@SuppressWarnings("unchecked")
		Predicate<SystemEntityModifyConditionAwareAdminRequired> castCondition = (Predicate<SystemEntityModifyConditionAwareAdminRequired>) condition;
		getApprovedEntityConditionMap().put(entityType, castCondition);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testEntityModifyConditionAdminRequired() {
		// Get invalid entities by class and ID
		Collection<ReadOnlyDAO<? extends SystemEntityModifyConditionAwareAdminRequired>> daoList = getDaoLocator().locateAll(SystemEntityModifyConditionAwareAdminRequired.class);
		Map<Class<? extends SystemEntityModifyConditionAwareAdminRequired>, List<Serializable>> invalidEntityIdListByClass = daoList.stream()
				.sorted(Comparator.comparing(dao -> dao.getConfiguration().getBeanClass().getSimpleName()))
				.map(dao -> MapUtils.entry(dao.getConfiguration().getBeanClass(), getInvalidEntityIdList(dao, QUERY_BATCH_SIZE)))
				.filter(classToInvalidEntityIdListEntry -> !classToInvalidEntityIdListEntry.getValue().isEmpty())
				.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, CollectionUtils.throwingMerger()));

		// Report violations
		if (!invalidEntityIdListByClass.isEmpty()) {
			String invalidEntityDelimiter = StringUtils.NEW_LINE + "- ";
			String invalidEntityPrefix = "One or more entities require administrative access due to missing entity modify conditions. Actions requiring administrative access are highly discouraged outside of break-glass scenarios. Please review the listed entities and assign an appropriate entity modify condition to each. Entities may be ignored by adding rules to ignore them, if necessary, via #configure and #addApprovedEntity. Types and IDs with violations:" + invalidEntityDelimiter;
			String errorMessage = invalidEntityIdListByClass.entrySet().stream()
					.sorted(Comparator.comparing(e -> e.getKey().getSimpleName()))
					.map(classToIdListEntry -> String.format("%s (%s): %s",
							classToIdListEntry.getKey().getSimpleName(),
							(classToIdListEntry.getValue().size() > MAX_VIOLATIONS_PER_TABLE) ? (MAX_VIOLATIONS_PER_TABLE + "+") : classToIdListEntry.getValue().size(),
							CollectionUtils.toString(classToIdListEntry.getValue(), MAX_VIOLATIONS_PER_TABLE, remaining -> "...")))
					.collect(Collectors.joining(invalidEntityDelimiter, invalidEntityPrefix, StringUtils.EMPTY_STRING));
			Assertions.fail(errorMessage);
		}
	}


	private List<Serializable> getInvalidEntityIdList(ReadOnlyDAO<? extends SystemEntityModifyConditionAwareAdminRequired> dao, int batchSize) {
		// Prepare spliterator
		@SuppressWarnings("unchecked")
		AdvancedReadOnlyDAO<? extends SystemEntityModifyConditionAwareAdminRequired, Criteria> advancedDao = (AdvancedReadOnlyDAO<? extends SystemEntityModifyConditionAwareAdminRequired, Criteria>) dao;
		List<Predicate<SystemEntityModifyConditionAwareAdminRequired>> approvedEntityConditionList = getApprovedEntityConditionMap().get(dao.getConfiguration().getBeanClass());
		AdvancedDaoIterator<? extends SystemEntityModifyConditionAwareAdminRequired> daoIterator = new AdvancedDaoIterator<>(advancedDao, batchSize);
		Spliterator<? extends SystemEntityModifyConditionAwareAdminRequired> daoSpliterator = Spliterators.spliteratorUnknownSize(daoIterator, Spliterator.IMMUTABLE);

		// Process values
		List<Serializable> invalidEntityIdList = StreamSupport.stream(daoSpliterator, false)
				.filter(entity -> entity.isEntityModifyConditionRequiredForNonAdmins() && entity.getEntityModifyCondition() == null)
				.filter(entity -> !CollectionUtils.anyMatch(approvedEntityConditionList, p -> p.test(entity)))
				.limit(MAX_VIOLATIONS_PER_TABLE + 1)
				.map(SystemEntityModifyConditionAwareAdminRequired::getIdentity)
				.sorted()
				.collect(Collectors.toList());
		return invalidEntityIdList;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * The {@link AdvancedDaoIterator} class is an {@link Iterator} to cycle through the entities backed by a given {@link AdvancedReadOnlyDAO}. This queries the DAO, pulling a
	 * limited number of entities at a time to limit memory consumption via a preconfigured {@link #pageSize}. When the end of any given page is reached, the iterator will query
	 * for a new page.
	 *
	 * <b>Note: This does not use database-level cursors and is not resilient to concurrent database modifications.</code>
	 */
	private static class AdvancedDaoIterator<T extends IdentityObject> implements Iterator<T> {

		private final AdvancedReadOnlyDAO<T, Criteria> advancedReadOnlyDAO;
		private final int pageSize;
		private int cursorPosition = 0;
		private int entityListPosition = 0;
		private List<? extends T> entityList;


		public AdvancedDaoIterator(AdvancedReadOnlyDAO<T, Criteria> advancedReadOnlyDAO, int pageSize) {
			this.advancedReadOnlyDAO = advancedReadOnlyDAO;
			this.pageSize = pageSize;
		}


		@Override
		public boolean hasNext() {
			advanceIfNecessary();
			return !this.entityList.isEmpty();
		}


		@Override
		public T next() {
			if (this.entityListPosition >= this.entityList.size()) {
				throw new NoSuchElementException("Cannot iterate past end of data.");
			}
			return this.entityList.get(this.entityListPosition++);
		}


		private void advanceIfNecessary() {
			if (this.entityList == null || this.entityListPosition >= this.entityList.size()) {
				if (this.entityList != null && this.entityList.size() < this.pageSize) {
					// Current entity list is smaller than page size; assume end of total result set reached
					this.entityList = Collections.emptyList();
				}
				else {
					this.entityList = this.advancedReadOnlyDAO.findBySearchCriteria((HibernateSearchConfigurer) criteria -> {
						criteria.setFirstResult(AdvancedDaoIterator.this.cursorPosition);
						criteria.setMaxResults(AdvancedDaoIterator.this.pageSize);
					});
					this.cursorPosition += this.pageSize;
				}
				this.entityListPosition = 0;
			}
		}
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public DaoLocator getDaoLocator() {
		return this.daoLocator;
	}


	public void setDaoLocator(DaoLocator daoLocator) {
		this.daoLocator = daoLocator;
	}


	@SuppressWarnings("rawtypes")
	public MultiValueMap<Class, Predicate<SystemEntityModifyConditionAwareAdminRequired>> getApprovedEntityConditionMap() {
		return this.approvedEntityConditionMap;
	}
}
