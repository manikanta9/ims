package com.clifton.test.protocol;


/**
 * The <code>ImsErrorResponseException</code> is an exception that is thrown when an error is returned from IMS.
 *
 * @author jgommels
 */
public class ImsErrorResponseException extends RuntimeException {

	private final String errorMessageFromIMS;


	public ImsErrorResponseException(String message, String errorMessageFromIMS) {
		super(message);
		this.errorMessageFromIMS = errorMessageFromIMS;
	}


	public ImsErrorResponseException(String message, String errorMessageFromIMS, Throwable cause) {
		super(message, cause);
		this.errorMessageFromIMS = errorMessageFromIMS;
	}


	public String getErrorMessageFromIMS() {
		return this.errorMessageFromIMS;
	}
}
