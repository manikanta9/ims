package com.clifton.test.protocol;


import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializer;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

import java.io.IOException;
import java.util.Map;


/**
 * The <code>ImsGsonFactory</code> is a factory class for building {@link ImsGson} objects that are configured for deserializing JSON messages from IMS into Java objects.
 *
 * @author jgommels
 */
public class ImsGsonFactory {

	/**
	 * Creates and returns a new {@link ImsGson}
	 */
	public static ImsGson newImsGson(ImsProtocolClientConfiguration configuration) {
		ImsResponseAdapter imsResponseAdapter = new ImsResponseAdapter(configuration);

		GsonBuilder gsonBuilder = new GsonBuilder()
				.setDateFormat(DateUtils.DATE_FORMAT_FULL)
				.registerTypeAdapter(ImsResponse.class, imsResponseAdapter)
				.registerTypeAdapter(Time.class, new TimeAdapter().nullSafe());

		if (configuration != null && configuration.getTypeAdapterMap() != null) {
			for (Map.Entry<Class<?>, JsonDeserializer<?>> entry : configuration.getTypeAdapterMap().entrySet()) {
				gsonBuilder.registerTypeAdapter(entry.getKey(), entry.getValue());
			}
		}
		return new ImsGson(gsonBuilder.create(), imsResponseAdapter);
	}


	/**
	 * The <code>TimeAdapter</code> is a TypeAdapter that instructs Gson how to deserialize to a {@link Time}.
	 *
	 * @author jgommels
	 */
	static class TimeAdapter extends TypeAdapter<Time> {

		@Override
		public Time read(JsonReader reader) throws IOException {
			String timeStr = reader.nextString();
			Time time = null;
			try {
				time = Time.parse(timeStr, Time.TIME_FORMAT_FULL);
			}
			catch (Exception e) {
				try {
					time = Time.parse(timeStr, Time.TIME_FORMAT_SHORT);
				}
				catch (Exception e2) {
					//Do nothing
				}
			}

			return time;
		}


		@Override
		public void write(JsonWriter writer, Time time) throws IOException {
			String timeStr = time.toString();
			writer.value(timeStr);
		}
	}
}
