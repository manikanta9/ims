package com.clifton.test.protocol;


import com.google.gson.Gson;


/**
 * The <code>ImsGson</code> is simply a wrapper for a {@link Gson} instance that is tailored to deserializing IMS responses.
 *
 * @author jgommels
 */
public class ImsGson {

	private final Gson gson;
	private final ImsResponseAdapter imsResponseAdapter;


	public ImsGson(Gson gson, ImsResponseAdapter imsResponseAdapter) {
		this.gson = gson;
		this.imsResponseAdapter = imsResponseAdapter;
	}


	public Gson getGson() {
		return this.gson;
	}


	public ImsResponseAdapter getImsResponseAdapter() {
		return this.imsResponseAdapter;
	}
}
