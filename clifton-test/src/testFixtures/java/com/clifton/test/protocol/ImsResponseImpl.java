package com.clifton.test.protocol;


/**
 * The <code>ImsResponse</code> represents a response from IMS.
 *
 * @author jgommels
 */
public class ImsResponseImpl<T> implements ImsResponse<T> {

	//Warning: Changing the names of these fields will break JSON deserialization
	private boolean success;
	private boolean validationError;
	private String message;
	private String orderBy;
	private boolean filterRequired;
	private int limit;
	private int start;
	private boolean uncommittedRequest;
	private boolean authenticationRequired;

	private T deserializedData; //Warning: Do not name this 'data', because then Gson will attempt to deserialize the json field 'data. Custom deserialization is needed here.


	@Override
	public boolean isSuccess() {
		return this.success;
	}


	@Override
	public boolean isValidationError() {
		return this.validationError;
	}


	@Override
	public String getMessage() {
		return this.message;
	}


	@Override
	public String getOrderBy() {
		return this.orderBy;
	}


	@Override
	public boolean isFilterRequired() {
		return this.filterRequired;
	}


	@Override
	public int getLimit() {
		return this.limit;
	}


	@Override
	public int getStart() {
		return this.start;
	}


	@Override
	public boolean isUncommittedRequest() {
		return this.uncommittedRequest;
	}


	@Override
	public void setData(T data) {
		this.deserializedData = data;
	}


	@Override
	public T getData() {
		return this.deserializedData;
	}


	@Override
	public boolean isAuthenticationRequired() {
		return this.authenticationRequired;
	}
}
