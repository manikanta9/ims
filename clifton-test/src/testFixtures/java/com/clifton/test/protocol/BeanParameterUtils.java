package com.clifton.test.protocol;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.IdentityObject;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * The <code>BeanParameterUtils</code> provides utility methods for generating HTTP parameters for IMS requests when retrieving or saving beans.
 *
 * @author jgommels
 */
public class BeanParameterUtils {

	private static final int MAX_RECURSIVE_DEPTH = 10;

	/**
	 * A set of properties that should never be serialized
	 */
	private static final Set<String> EXCLUDED_PROPERTIES = CollectionUtils.createHashSet("tradeRuleEvaluatorContext", "class", "genericSuperclass", "nameExpanded", "identity", "label", "labelExpanded", "leaf",
			"nameExpandedWithType", "rootParent", "newBean", "createUserId", "updateUserId", "rv", "entityModifyConditionRequiredForNonAdmins", "workflowStateEffectiveStartDate");


	/**
	 * Returns a map that maps parameter names to parameter values for the given name and object.
	 */
	public static List<NameValuePair> getParameters(String name, Object val, Set<Class<?>> overrideOnlySendIdWhenPresentClasses, Map<Class<?>, Set<String>> excludedPropertiesMap, Set<Class<?>> excludedClasses) {
		boolean requireSetter = false;
		List<NameValuePair> valuePairs = new ArrayList<>();
		boolean onlySendIdWhenPresent = false;
		if (overrideOnlySendIdWhenPresentClasses.contains(val.getClass())) {
			onlySendIdWhenPresent = true;
		}
		getAllParametersRecursively(val, name, valuePairs, onlySendIdWhenPresent, requireSetter, 0, excludedPropertiesMap, excludedClasses);
		return valuePairs;
	}


	/**
	 * Returns a map that maps a parameter names to parameter values for a given bean to be saved.
	 *
	 * @param obj the bean to be saved
	 */
	public static List<NameValuePair> getResourcePropertiesForSave(Object obj, Map<Class<?>, Set<String>> excludedPropertiesMap, Set<Class<?>> excludedClasses, Map<Class<?>, Set<String>> nestedSavePropertiesMap, Map<Class<?>, Set<String>> saveNullPropertiesMap) {
		Map<String, Object> properties = BeanUtils.describeWithRequiredSetter(obj);
		List<NameValuePair> params = new ArrayList<>();

		for (Map.Entry<String, Object> stringObjectEntry : properties.entrySet()) {
			Object val = stringObjectEntry.getValue();

			if (val != null && !isExcludedProperty(obj, stringObjectEntry.getKey(), excludedPropertiesMap)) {
				// We call the recursive method with a currentDepth of 1 to ensure proper path naming
				getAllParametersRecursively(val, stringObjectEntry.getKey(), params, true, true, 1, excludedPropertiesMap, excludedClasses);
			}
			else if (val == null && isNestedSaveProperty(obj, stringObjectEntry.getKey(), nestedSavePropertiesMap)) {
				// This is kind of a hack. In some cases, a nested property needs to get sent, even though it is not populated.
				params.add(new BasicNameValuePair(stringObjectEntry.getKey() + ".fakeProperty", "1"));
			}
			// Ability to force saving a null value as a way to clear it
			else if (val == null && isSaveNullProperty(obj, stringObjectEntry.getKey(), saveNullPropertiesMap)) {
				if (IdentityObject.class.isAssignableFrom(BeanUtils.getPropertyType(obj, stringObjectEntry.getKey()))) {
					params.add(new BasicNameValuePair(stringObjectEntry.getKey() + ".id", null));
				}
				else {
					params.add(new BasicNameValuePair(stringObjectEntry.getKey(), null));
				}
			}
		}

		return params;
	}


	private static void getAllParametersRecursively(Object val, String path, List<NameValuePair> params, boolean onlySendIdWhenPresent, boolean requireSetter, int currentDepth, Map<Class<?>, Set<String>> excludedPropertiesMap, Set<Class<?>> excludedClasses) {
		if (currentDepth > MAX_RECURSIVE_DEPTH) {
			throw new RuntimeException("Max recursive depth reached while serializing object. Current path is [" + path + "]");
		}
		if (val != null) {
			try {

				//First attempt to serialize as a simple property (primitive, Date, etc.)
				String valString = simplePropertyToString(val);
				if (valString != null) {
					params.add(new BasicNameValuePair(path, valString));
				}
				else if (onlySendIdWhenPresent && val instanceof IdentityObject && ((IdentityObject) val).getIdentity() != null) {
					params.add(new BasicNameValuePair(path + ".id", ((IdentityObject) val).getIdentity().toString()));
				}
				else if (val.getClass().isArray()) {
					// File Data
					int length = Array.getLength(val);
					for (int i = 0; i < length; i++) {
						//For now assume that arrays are arrays of simple primitives and wrappers, and not beans
						String value = simplePropertyToString(Array.get(val, i));
						params.add(new BasicNameValuePair(path, value));
					}
				}
				else if (val instanceof Collection) {
					Collection<?> collection = (Collection<?>) val;
					StringBuilder sb = new StringBuilder();
					sb.append('[');

					int elemCount = 0;
					boolean first = true;
					for (Object elem : collection) {
						Class<?> objType = elem.getClass();
						if (objType != null && IdentityObject.class.isAssignableFrom(objType)) {
							if (!first) {
								sb.append(',');
							}
							first = false;
							sb.append("{\"class\":\"").append(objType.getName()).append("\"");

							Map<String, Object> elemProps = requireSetter ? BeanUtils.describeWithRequiredSetter(elem) : BeanUtils.describe(elem);

							for (Map.Entry<String, Object> stringObjectEntry : elemProps.entrySet()) {
								if (!isExcludedProperty(elem, stringObjectEntry.getKey(), excludedPropertiesMap)) {
									String propPath = stringObjectEntry.getKey();
									Object elemPropVal = stringObjectEntry.getValue();
									if (elemPropVal != null) {
										String elemPropValStr;
										if (elemPropVal instanceof IdentityObject && ((IdentityObject) elemPropVal).getIdentity() != null) {
											propPath = stringObjectEntry.getKey() + ".id";
											elemPropValStr = ((IdentityObject) elemPropVal).getIdentity().toString();
										}
										else {
											elemPropValStr = simplePropertyToString(elemPropVal, true);
										}
										if (elemPropValStr != null) {
											appendKeyValuePairToStringList(propPath, elemPropValStr, sb);
										}
										else {
											List<NameValuePair> innerPairs = new ArrayList<>();
											getAllParametersRecursively(elemPropVal, stringObjectEntry.getKey(), innerPairs, true, requireSetter, 1, excludedPropertiesMap, excludedClasses);
											for (NameValuePair innerPair : CollectionUtils.getIterable(innerPairs)) {
												appendKeyValuePairToStringList(innerPair.getName(), innerPair.getValue(), sb);
											}
										}
									}
								}
							}
							sb.append('}');
						}
						else {
							getAllParametersRecursively(elem, path + "[" + elemCount + "]", params, onlySendIdWhenPresent, requireSetter, currentDepth + 1, excludedPropertiesMap, excludedClasses);
						}
						elemCount++;
					}
					sb.append(']');
					if (sb.length() > 2) {
						params.add(new BasicNameValuePair(path, sb.toString()));
					}
				}
				else if (val instanceof Map) {
					Map<?, ?> map = (Map<?, ?>) val;
					for (Map.Entry<?, ?> entry : map.entrySet()) {
						Object subVal = entry.getValue();
						if (subVal != null && !isExcludedProperty(val, String.valueOf(entry.getKey()), excludedPropertiesMap) && !isExcludedClass(subVal.getClass(), excludedClasses)) {
							String newPath;
							if (currentDepth == 0) {
								newPath = String.valueOf(entry.getKey());
							}
							else {
								newPath = path + "['" + entry.getKey() + "']";
							}
							getAllParametersRecursively(subVal, newPath, params, onlySendIdWhenPresent, requireSetter, currentDepth + 1, excludedPropertiesMap, excludedClasses);
						}
					}
				}
				else {
					Map<String, Object> properties = requireSetter ? BeanUtils.describeWithRequiredSetter(val) : BeanUtils.describe(val);
					for (Map.Entry<String, Object> propertyEntry : properties.entrySet()) {
						Object subVal = propertyEntry.getValue();
						if (subVal != null && !isExcludedProperty(val, propertyEntry.getKey(), excludedPropertiesMap) && !isExcludedClass(subVal.getClass(), excludedClasses)) {
							String newPath;
							if (currentDepth == 0) {
								newPath = propertyEntry.getKey();
							}
							else {
								newPath = path + "." + propertyEntry.getKey();
							}
							getAllParametersRecursively(subVal, newPath, params, onlySendIdWhenPresent, requireSetter, currentDepth + 1, excludedPropertiesMap, excludedClasses);
						}
					}
				}
			}
			catch (Throwable e) {
				throw new RuntimeException("Error getting parameters for path: " + path, e);
			}
		}
	}


	private static void appendKeyValuePairToStringList(String key, String value, StringBuilder sb) {
		sb.append(",\"").append(key).append("\":").append(value);
	}


	private static String simplePropertyToString(Object value) {
		return simplePropertyToString(value, false);
	}


	private static String simplePropertyToString(Object value, boolean wrapStringWithQuotes) {
		if (value != null) {
			if (value.getClass().isPrimitive() || value instanceof Number || value instanceof Boolean) {
				return value.toString();
			}
			else if (value instanceof String) {
				return wrapStringWithQuotes ? '"' + value.toString() + '"' : value.toString();
			}
			else if (value instanceof Date) {
				return convertDate((Date) value);
			}
			else if (value instanceof Time) {
				return DateUtils.fromDate(((Time) value).getDate(), Time.TIME_FORMAT_FULL);
			}
			else if (value instanceof Enum<?>) {
				return value.toString();
			}
		}

		return null;
	}


	private static boolean isExcludedProperty(Object type, String fieldName, Map<Class<?>, Set<String>> excludedPropertiesMap) {
		return (EXCLUDED_PROPERTIES.contains(fieldName) && !isExcludedPropertyOverridden(type, fieldName))
				|| (excludedPropertiesMap.containsKey(type.getClass()) && excludedPropertiesMap.get(type.getClass()).contains(fieldName) && !isExcludedPropertyOverridden(type, fieldName))
				|| isIncludedPropertyOverridden(type, fieldName);
	}


	private static boolean isSaveNullProperty(Object type, String fieldName, Map<Class<?>, Set<String>> saveNullPropertiesMap) {
		return saveNullPropertiesMap.containsKey(type.getClass()) && saveNullPropertiesMap.get(type.getClass()).contains(fieldName);
	}


	private static boolean isExcludedClass(Class<?> clazz, Set<Class<?>> excludedClasses) {
		for (Class<?> excludedClazz : excludedClasses) {
			if (excludedClazz.isAssignableFrom(clazz)) {
				return true;
			}
		}

		return false;
	}


	private static boolean isExcludedPropertyOverridden(Object type, String fieldName) {
		return RequestOverrideHandler.isRequestEntityPropertyOverrideIncluded(type.getClass(), fieldName);
	}


	private static boolean isIncludedPropertyOverridden(Object type, String fieldName) {
		return RequestOverrideHandler.isRequestEntityPropertyOverrideExcluded(type.getClass(), fieldName);
	}


	private static boolean isNestedSaveProperty(Object type, String fieldName, Map<Class<?>, Set<String>> nestedSavePropertiesMap) {
		return (nestedSavePropertiesMap.containsKey(type.getClass()) && nestedSavePropertiesMap.get(type.getClass()).contains(fieldName));
	}


	private static String convertDate(Date date) {
		return DateUtils.fromDate(date, DateUtils.DATE_FORMAT_SHORT);
	}
}
