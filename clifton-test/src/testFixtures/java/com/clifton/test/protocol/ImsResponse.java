package com.clifton.test.protocol;


/**
 * The <code>ImsResponse</code> represents a response from IMS.
 *
 * @param <T> the type of data returned
 * @author jgommels
 */
public interface ImsResponse<T> {

	/**
	 * @return whether the request was a success
	 */
	public boolean isSuccess();


	/**
	 * @return whether there was a validation error
	 */
	public boolean isValidationError();


	/**
	 * @return the message returned from the server, if any
	 */
	public String getMessage();


	/**
	 * @return the 'orderBy' setting
	 */
	public String getOrderBy();


	/**
	 * @return whether filtering is required
	 */
	public boolean isFilterRequired();


	/**
	 * @return the data size limit
	 */
	public int getLimit();


	/**
	 * @return the data page start
	 */
	public int getStart();


	/**
	 * @return whether is an uncommitted request
	 */
	public boolean isUncommittedRequest();


	/**
	 * Sets the data returned by the server.
	 */
	void setData(T data);


	/**
	 * @return the data returned by the server
	 */
	public T getData();


	/**
	 * @return whether the request requires authentication
	 */
	public boolean isAuthenticationRequired();
}
