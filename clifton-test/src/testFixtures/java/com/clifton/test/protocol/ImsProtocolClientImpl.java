package com.clifton.test.protocol;


import com.clifton.core.dataaccess.file.FileWrapper;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.http.CustomURIBuilder;
import com.clifton.core.util.http.CustomURIBuilderFactory;
import com.clifton.core.util.http.HttpEntityFactory;
import com.clifton.core.util.http.HttpException;
import com.clifton.core.util.http.SimpleHttpClientFactory;
import com.clifton.core.util.http.SimpleHttpResponse;
import com.clifton.core.util.http.authenticator.ImsAuthenticator;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


public class ImsProtocolClientImpl implements ImsProtocolClient {

	private static final Set<? extends Type> NON_WRAPPED_SERVICE_RETURN_TYPES = new HashSet<>(Arrays.asList(BigDecimal.class, Boolean.class, boolean.class, FileWrapper.class, Date.class));

	private static final Logger log = LoggerFactory.getLogger(ImsProtocolClientImpl.class);

	private static final String HTTP_AUTHENTICATOR_KEY = "HTTP_AUTHENTICATOR";
	private static final String USER_CREDENTIALS_KEY = "USER_CREDENTIALS";

	private final ImsGson imsGson;
	private final CustomURIBuilderFactory uriBuilderFac;
	private final ImsProtocolClientConfiguration configuration;
	// Default credentials to use by tests when the tests do not set a user directly.
	private final UserCredentials currentUserCredentials;


	public ImsProtocolClientImpl(String defaultURI, String defaultUserName, String defaultPassword, ImsProtocolClientConfiguration configuration) {
		this.imsGson = ImsGsonFactory.newImsGson(configuration);

		if (!defaultURI.endsWith("/")) {
			defaultURI += "/";
		}

		this.configuration = configuration;
		this.uriBuilderFac = new CustomURIBuilderFactory(CustomURIBuilder.uriFromString(defaultURI));
		this.currentUserCredentials = UserCredentials.newInstance(defaultUserName, defaultPassword);
	}


	/**
	 * Tests are allowed to update the user for requests using {@link com.clifton.test.util.UserUtils#switchUser(String, String)}.
	 * This will update the user credentials for the currently executing thread.
	 */
	@Override
	public void setUser(String username, String password) {
		this.configuration.getContextHandler().setBean(USER_CREDENTIALS_KEY, UserCredentials.newInstance(username, password));
	}


	/**
	 * Returns the user name assigned via {@link com.clifton.test.util.UserUtils#switchUser(String, String)} for the current
	 * thread. If no user was defined, the default user credentials from test setup will be used.
	 */
	@Override
	public String getCurrentUsername() {
		return getCurrentUserCredentials().userName;
	}


	private UserCredentials getCurrentUserCredentials() {
		UserCredentials threadCredentials = ((UserCredentials) this.configuration.getContextHandler().getBean(USER_CREDENTIALS_KEY));
		return (threadCredentials == null) ? this.currentUserCredentials : threadCredentials;
	}


	@Override
	public <T> T request(String path, Map<String, String> params, Type serviceReturnType, String resultPropertyName, boolean responseBody) {
		List<NameValuePair> paramValuePairs = new ArrayList<>();
		for (Map.Entry<String, String> stringStringEntry : params.entrySet()) {
			paramValuePairs.add(new BasicNameValuePair(stringStringEntry.getKey(), stringStringEntry.getValue()));
		}

		return request(path, paramValuePairs, serviceReturnType, resultPropertyName, responseBody);
	}


	@Override
	public <T> T request(String path, List<NameValuePair> params, Type serviceReturnType, String resultPropertyName, boolean responseBody) {
		return request(path, HttpEntityFactory.createHttpEntity(params), serviceReturnType, resultPropertyName, responseBody);
	}


	@Override
	@SuppressWarnings("unchecked")
	public <T> T request(String path, HttpEntity entity, Type serviceReturnType, String resultPropertyName, boolean responseBody) {
		URI uri = this.uriBuilderFac.newBuilder().appendToPath(path).build();
		Type fullReturnType = getAndPrepareFullReturnType(serviceReturnType, resultPropertyName);
		return (T) sendRequest(uri, entity, fullReturnType, responseBody, false);
	}


	private Object handleResponseBody(String response, Type responseType) {
		Object returnVal = response;
		if (responseType instanceof Class) {
			if (Date.class.isAssignableFrom((Class<?>) responseType)) {
				returnVal = DateUtils.toDate(response, DateUtils.DATE_FORMAT_FULL);
			}
			else if (BigDecimal.class.isAssignableFrom((Class<?>) responseType)) {
				if (StringUtils.isEmpty(response)) {
					returnVal = null;
				}
				else {
					returnVal = new BigDecimal(response); // need to preserve precision
				}
			}
			else if (Boolean.class.isAssignableFrom((Class<?>) responseType)) {
				returnVal = Boolean.valueOf(response);
			}
		}
		return returnVal;
	}


	private Object sendRequest(URI uri, HttpEntity httpEntity, Type fullReturnType, boolean responseBody, boolean isRetry) {
		ImsAuthenticator authenticator = getAuthenticator();
		// Ensure the current user is authenticated
		if (!this.getCurrentUsername().equals(authenticator.getCurrentUsername())) {
			UserCredentials userCredentials = this.getCurrentUserCredentials();
			authenticator.login(userCredentials.userName, userCredentials.password);
		}

		try (SimpleHttpResponse httpResponse = SimpleHttpClientFactory.getHttpClient().post(uri, httpEntity, authenticator.getCurrentHttpClientContext())) {
			return extractHttpResponseResult(httpResponse, fullReturnType, responseBody);
		}
		catch (HttpException e) {
			if (!isRetry) {
				log.warn("Request failed. Retrying with new authentication session...");
				authenticator.refreshLastLogin();
				return sendRequest(uri, httpEntity, fullReturnType, responseBody, true);
			}
			else {
				throw new HttpException("Request retry failed.", e);
			}
		}
		catch (IOException e) {
			throw new ImsResponseDeserializationException("Problem occurred closing IMS response", e);
		}
	}


	private Object extractHttpResponseResult(SimpleHttpResponse httpResponse, Type fullReturnType, boolean responseBody) {
		String responseString = null;
		try {
			responseString = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");
			if (log.isDebugEnabled()) {
				log.debug("Response content:\n{}", StringUtils.formatStringUpToNCharsWithDots(responseString, 10000));
			}

			Object response = (responseBody) ? handleResponseBody(responseString, fullReturnType)
					: this.imsGson.getGson().fromJson(responseString, fullReturnType);
			if (response instanceof ImsResponse) {
				ImsResponse<?> imsResponse = (ImsResponse<?>) response;
				if (imsResponse.isAuthenticationRequired()) {
					// Will signal a retry if one was not previously done.
					throw new HttpException("Authentication required.");
				}
				if (!imsResponse.isSuccess() || imsResponse.isValidationError()) {
					throw new ImsErrorResponseException("Error was returned from IMS: " + imsResponse.getMessage(), imsResponse.getMessage());
				}
				return imsResponse.getData();
			}
			return response;
		}
		// thrown from EntityUtils.toString(...) - catch specific to avoid catching other types (e.g. ImsErrorResponseException)
		catch (IOException | ParseException e) {
			throw new ImsResponseDeserializationException("Problem during deserialization of IMS response: '" + responseString + "' to " + fullReturnType, e);
		}
	}


	private ImsAuthenticator getAuthenticator() {
		ImsAuthenticator authenticator = (ImsAuthenticator) this.configuration.getContextHandler().getBean(HTTP_AUTHENTICATOR_KEY);
		if (authenticator == null) {
			authenticator = new ImsAuthenticator(this.uriBuilderFac);
			this.configuration.getContextHandler().setBean(HTTP_AUTHENTICATOR_KEY, authenticator);
		}
		return authenticator;
	}


	private Type getAndPrepareFullReturnType(Type serviceReturnType, String resultPropertyName) {
		ImsResponseAdapter imsResponseAdapter = this.imsGson.getImsResponseAdapter();
		imsResponseAdapter.setExpectedResultPropertyName(resultPropertyName);

		Type fullReturnType;

		if (NON_WRAPPED_SERVICE_RETURN_TYPES.contains(serviceReturnType)) {
			fullReturnType = serviceReturnType;
		}
		else {
			/*
			 * IMS requests and JSON deserialization are thread-save. Each thread has its own HTTP Client
			 * and associated ImsResponseAdapter for fully handling a request.
			 */
			imsResponseAdapter.setExpectedDataType(serviceReturnType);

			fullReturnType = ImsResponse.class;
		}

		return fullReturnType;
	}


	public CustomURIBuilder newURIBuilder() {
		return this.uriBuilderFac.newBuilder();
	}


	@Override
	public ImsProtocolClientConfiguration getConfiguration() {
		return this.configuration;
	}


	private static final class UserCredentials {

		private final String userName;
		private final String password;


		private UserCredentials(String userName, String password) {
			this.userName = userName;
			this.password = password;
		}


		static UserCredentials newInstance(String userName, String password) {
			return new UserCredentials(userName, password);
		}
	}
}
