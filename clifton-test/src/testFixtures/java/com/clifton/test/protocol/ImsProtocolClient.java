package com.clifton.test.protocol;


import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;

import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;


public interface ImsProtocolClient {

	public ImsProtocolClientConfiguration getConfiguration();


	/**
	 * Authenticates the user to be used in subsequent requests.
	 *
	 * @param username
	 * @param password
	 */
	public void setUser(String username, String password);


	/**
	 * @return the username of the user that is currently authenticated
	 */
	public String getCurrentUsername();


	/**
	 * Sends a request to an IMS service. If IMS returns an error, then an {@link ImsErrorResponseException} is thrown.
	 *
	 * @param path               the relative path of the service, which will be appended to the default URI.
	 * @param params             the parameters to send
	 * @param serviceReturnType  the expected return type from the service
	 * @param resultPropertyName the expected name of the result property (such as "data" or "result")
	 * @return the response from IMS
	 */
	public <T> T request(String path, Map<String, String> params, Type serviceReturnType, String resultPropertyName, boolean responseBody);


	public <T> T request(String path, List<NameValuePair> params, Type serviceReturnType, String resultPropertyName, boolean responseBody);


	public <T> T request(String path, HttpEntity entity, Type serviceReturnType, String resultPropertyName, boolean responseBody);
}
