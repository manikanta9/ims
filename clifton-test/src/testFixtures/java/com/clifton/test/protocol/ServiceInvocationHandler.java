package com.clifton.test.protocol;


import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextConventionUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.FunctionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.http.HttpEntityFactory;
import com.clifton.core.util.http.HttpUtils;
import com.clifton.core.util.http.MultipartEntityBuilderUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.message.BasicNameValuePair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * The <code>ServiceInvocationHandler</code> is an {@link InvocationHandler} that acts as a proxy for a service. It allows service methods to be called, and the invocations are then proxied
 * through the logic of this class, which translates the method call into a web call, then returns the response.
 *
 * @author jgommels
 */
public class ServiceInvocationHandler implements InvocationHandler {

	private static final String SYSTEM_UPLOAD_FILE_PROPERTY_NAME = "file";

	private static final Map<String, String> defaultParams = FunctionUtils.uncheckedSupplier(() -> {
		Map<String, String> params = new HashMap<>();
		//Set the requestedMaxDepth. Will want to make this configurable later.
		params.put(ImsProtocolClientConfiguration.REQUESTED_MAX_DEPTH, "6");
		return params;
	}).get();

	private static final Logger log = LoggerFactory.getLogger(ServiceInvocationHandler.class);


	private final ImsProtocolClientConfiguration integrationTestConfiguration;
	private final ImsProtocolClient protocolClient;
	private final Class<?> serviceImpl;


	ServiceInvocationHandler(Class<?> serviceImpl, ImsProtocolClient protocolClient) {
		this.protocolClient = protocolClient;
		this.serviceImpl = serviceImpl;
		this.integrationTestConfiguration = protocolClient.getConfiguration();
	}


	private void addDefaultParams(List<NameValuePair> params) {
		for (Map.Entry<String, String> stringStringEntry : defaultParams.entrySet()) {
			if (!HttpUtils.containsParam(stringStringEntry.getKey(), params)) {
				params.add(new BasicNameValuePair(stringStringEntry.getKey(), stringStringEntry.getValue()));
			}
		}
	}


	@Override
	public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
		// Handle methods from "Object"
		switch (method.getName()) {
			case "equals":
				return proxy == args[0];
			case "hashCode":
				return System.identityHashCode(proxy);
			case "toString":
				return proxy.getClass().getName() + "@" + Integer.toHexString(System.identityHashCode(proxy)) + ", with InvocationHandler " + this;
		}
		String methodName = method.getName();
		Class<?>[] paramTypes = method.getParameterTypes();

		Type returnType = method.getGenericReturnType();

		Method implMethod = this.serviceImpl.getMethod(method.getName(), paramTypes);
		String resultPropertyName = getResultPropertyName(implMethod);
		String url = getPath(implMethod);
		boolean responseBody = isResponseBody(implMethod);

		Object returnValue;
		if (methodName.contains("save") && args.length == 1) {
			Object objToSave = args[0];
			if (returnType == Void.TYPE) {
				returnType = objToSave.getClass();
			}
			returnValue = executeSave(url, objToSave, returnType, resultPropertyName, responseBody);
		}
		else {
			String[] paramNames = ArrayUtils.getStream(method.getParameters()).map(Parameter::getName).toArray(String[]::new);
			returnValue = execute(url, paramNames, args, returnType, resultPropertyName, responseBody);
		}
		return returnValue;
	}


	private Object execute(String url, String[] paramNames, Object[] paramValues, Type responseType, String resultPropertyName, boolean responseBody) {
		List<NameValuePair> httpParams = new ArrayList<>();

		MultipartEntityBuilder builder = MultipartEntityBuilder.create();
		boolean useMultipart = false;

		if (paramValues != null && paramValues.length > 0) {
			for (int i = 0; i < paramValues.length; i++) {
				String paramName = paramNames[i];
				Object paramValue = paramValues[i];
				if (paramValue != null) {
					httpParams.addAll(BeanParameterUtils.getParameters(paramName, paramValue, this.integrationTestConfiguration.getOverrideClassesOnlySendIdWhenPresent(), this.integrationTestConfiguration.getExcludedPropertiesMap(), this.integrationTestConfiguration.getExcludedClasses()));

					MultipartFile uploadFile = (MultipartFile) BeanUtils.getPropertyValue(paramValue, "file");
					if (uploadFile != null) {
						useMultipart = true;
						MultipartEntityBuilderUtils.addFile(SYSTEM_UPLOAD_FILE_PROPERTY_NAME, uploadFile, builder);
					}
				}
			}
		}

		this.integrationTestConfiguration.addParameterOverridesForPath(url, httpParams);
		addDefaultParams(httpParams);
		RequestOverrideHandler.addRequestParameterOverrides(httpParams);
		resultPropertyName = getResultPropertyName(httpParams, resultPropertyName);

		if (log.isDebugEnabled()) {
			log.debug("POST {} Params: {}", url, httpParams);
		}

		HttpEntity httpEntity;
		if (useMultipart) {
			MultipartEntityBuilderUtils.addNameValuePairs(httpParams, builder);
			httpEntity = builder.build();
		}
		else {
			httpEntity = HttpEntityFactory.createHttpEntity(httpParams);
		}

		return doExecute(url, httpEntity, responseType, resultPropertyName, responseBody, httpParams);
	}


	private String getResultPropertyName(List<NameValuePair> params, String resultPropertyName) {
		for (NameValuePair param : CollectionUtils.getIterable(params)) {
			if (ImsProtocolClientConfiguration.REQUESTED_PROPERTIES_ROOT.equals(param.getName())) {
				return param.getValue();
			}
		}
		return resultPropertyName;
	}


	private Object doExecute(String url, HttpEntity entity, Type returnType, String resultPropertyName, boolean responseBody, List<NameValuePair> params) {
		try {
			NameValuePair bindingDtoClass = HttpUtils.getParam(ImsProtocolClientConfiguration.DTO_CLASS_FOR_BINDING, params);
			if (bindingDtoClass != null) {
				returnType = CoreClassUtils.getClass(bindingDtoClass.getValue());
			}
			return this.protocolClient.request(url, entity, returnType, resultPropertyName, responseBody);
		}
		catch (ImsErrorResponseException e) {
			throw new ImsErrorResponseException("Error was returned from IMS: [" + e.getErrorMessageFromIMS() + "]\nURI: " + url + "\nParams: " + params, e.getErrorMessageFromIMS(), e);
		}
		catch (ImsResponseDeserializationException e) {
			StringBuilder sb = new StringBuilder();
			sb.append("There was a problem during JSON deserialization of the response from IMS. To debug this issue, check the server logs for exceptions, or change the logging level to DEBUG and run the test again.\n");
			sb.append("The request to the server was:\n");
			sb.append("URI: ").append(url).append("\n");
			if (params != null) {
				sb.append("HTTP Parameters: ").append(params).append("\n\n\n");
			}
			sb.append("Possible causes of this issue could be:\n");
			sb.append("1. The server returned an exception. Check the server logs to see if this is the case.\n");
			sb.append("2. For some reason the request was not constructed correctly (such as the URL, or the parameters). Change the logging level to DEBUG to investigate whether" + "this may be the cause. If this is the case, look into ").append(ServiceInvocationHandler.class.getName()).append("\n");
			sb.append("3. The return type being deserialized to is an interface that hasn't been registered with a concrete type (interfaces cannot be instantiated and therefore " + "must be registered with a concrete type. " + "If this is the case, look into ").append(ImsGsonFactory.class.getName()).append("\n");
			sb.append("4. Deserialization is not being handled properly for the return type for some other reason. If this is the case, look into ").append(this.getClass().getName());

			throw new ImsResponseDeserializationException(sb.toString(), e);
		}
		catch (Throwable e) {
			throw new RuntimeException("Error executing request for  URI: " + url + " and Params: " + params, e);
		}
	}


	@SuppressWarnings("rawtypes")
	private Object executeSave(String url, Object resource, Type returnType, String resultPropertyName, boolean responseBody) {
		List<NameValuePair> httpParams = BeanParameterUtils.getResourcePropertiesForSave(resource, this.integrationTestConfiguration.getExcludedPropertiesMap(), this.integrationTestConfiguration.getExcludedClasses(), this.integrationTestConfiguration.getNestedSavePropertiesMap(), this.integrationTestConfiguration.getSaveNullPropertiesMap());

		this.integrationTestConfiguration.addParameterOverridesForPath(url, httpParams);
		addDefaultParams(httpParams);
		RequestOverrideHandler.addRequestParameterOverrides(httpParams);

		Object response = doExecute(url, HttpEntityFactory.createHttpEntity(httpParams), returnType, resultPropertyName, responseBody, httpParams);

		//If instance of BaseSimpleEntity, set the ID of the resource parameter to be the same as the saved resource
		if (resource.getClass() == returnType && resource instanceof BaseSimpleEntity) {
			//noinspection unchecked
			((BaseSimpleEntity) resource).setId(((BaseSimpleEntity) response).getId());
		}

		return response;
	}


	private String getResultPropertyName(Method method) {
		ModelAttribute attribute = AnnotationUtils.getAnnotation(method, ModelAttribute.class);
		return (attribute != null) ? attribute.value() : "data";
	}


	private boolean isResponseBody(Method method) {
		return AnnotationUtils.isAnnotationPresent(method, ResponseBody.class);
	}


	private String getPath(Method method) {
		String path;

		RequestMapping mapping = AnnotationUtils.getAnnotation(method, RequestMapping.class);
		if (mapping != null) {
			path = mapping.value()[0];
			if (!path.endsWith(".json")) {
				path = path + ".json";
			}
		}
		else {
			//Adding this comment to force a pull request update.
			path = ContextConventionUtils.getUrlFromMethod(method);
		}

		return path;
	}
}
