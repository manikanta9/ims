package com.clifton.test.protocol;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.context.ContextHandler;
import com.google.gson.JsonDeserializer;
import org.apache.http.NameValuePair;

import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Configuration object that can be defined by the end implementing project.
 * Ex. app-clifton-ims-tests
 *
 * @author apopp
 */
public interface ImsProtocolClientConfiguration {

	public static final String ENABLE_OPEN_SESSION_IN_VIEW = "enableOpenSessionInView";
	public static final String ENABLE_VALIDATING_BINDING = "enableValidatingBinding";
	public static final String DISABLE_VALIDATING_BINDING_VALIDATING = "disableValidatingBindingValidation";
	public static final String REQUESTED_PROPERTIES_ROOT = "requestedPropertiesRoot";
	public static final String DTO_CLASS_FOR_BINDING = "dtoClassForBinding";
	public static final String REQUESTED_PROPERTIES_TO_EXCLUDE = "requestedPropertiesToExclude";
	public static final String REQUESTED_PROPERTIES_TO_EXCLUDE_GLOBALLY = "requestedPropertiesToExcludeGlobally";
	public static final String REQUESTED_PROPERTIES = "requestedProperties";
	public static final String REQUESTED_MAX_DEPTH = "requestedMaxDepth";


	/**
	 * A map that maps a resource class to a set of properties that are nested in the object but should also serialize it's properties.
	 * Example: Saving a BusinessClient also saves it's company property, Saving a BusinessClientRelationship also saves it's company property
	 */
	public Map<Class<?>, Set<String>> getNestedSavePropertiesMap();


	/**
	 * Classes that should always only pass ids when present for methods for them.
	 * Example: TradeUpload
	 */
	public Set<Class<?>> getOverrideClassesOnlySendIdWhenPresent();


	/**
	 * A map that maps a resource class to a set of properties that should never be serialized for that specific class.
	 * This is sometimes needed if the resource class has extra methods that don't match to a private field, which may result in
	 * an error response returned from the server when a save is requested. In some cases it is because a getter does not have a matching
	 * setter. In the future, this may be avoided entirely by using a different serialization strategy that doesn't involve using {@link BeanUtils#describe(Object)},
	 * or else perhaps doing some "reflection magic" to filter out these particular cases.
	 */
	public Map<Class<?>, Set<String>> getExcludedPropertiesMap();


	/**
	 * A map that maps a resource class to a set of properties that we should be serializing null values to allow clearing the value.
	 * By default, null values are skipped for creating the http params
	 */
	public Map<Class<?>, Set<String>> getSaveNullPropertiesMap();


	/**
	 * Classes that should be excluded from processing
	 */
	public Set<Class<?>> getExcludedClasses();


	/**
	 * Explicitly defined serialization adapters provided by end project
	 */
	public Map<Class<?>, JsonDeserializer<?>> getTypeAdapterMap();


	/**
	 * Allows the ability to dynamically add parameters to certain paths controlled by the end project.
	 * <p>
	 * Ex. tradeListFind.json may need to add enableOpenSessionInView=true
	 */
	public void addParameterOverridesForPath(String path, List<NameValuePair> params);


	/**
	 * Returns the {@link ContextHandler} for the configuration for use by other components of the test framework.
	 */
	public ContextHandler getContextHandler();
}
