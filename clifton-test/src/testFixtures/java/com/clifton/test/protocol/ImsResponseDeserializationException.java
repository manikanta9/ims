package com.clifton.test.protocol;

/**
 * @author jgommels
 */
public class ImsResponseDeserializationException extends RuntimeException {

	public ImsResponseDeserializationException(String message, Throwable cause) {
		super(message, cause);
	}
}
