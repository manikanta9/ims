package com.clifton.test.protocol;

import com.clifton.core.util.CollectionUtils;
import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Callable;


/**
 * Handler that can be used to override default IMS entity inclusions/exclusions during service calls.
 *
 * @author NickK
 */
public class RequestOverrideHandler {

	public static final String REQUESTED_PROPERTIES_ROOT_PARAMETER = "requestedPropertiesRoot";
	public static final String REQUESTED_PROPERTIES_PARAMETER = "requestedProperties";
	public static final String REQUESTED_MAX_DEPTH_PARAMETER = "requestedMaxDepth";
	public static final String REQUESTED_GLOBAL_EXCLUSION_PARAMETER = "requestedPropertiesToExcludeGlobally";

	/*
	 * Use a ThreadLocal to apply the field inclusion/exclusion overrides to only the service calls for the current thread.
	 */
	private static final ThreadLocal<Map<Boolean, Map<Class<?>, Set<String>>>> perThreadRequestEntityPropertyOverrides = ThreadLocal.withInitial(HashMap::new);
	private static final ThreadLocal<Map<String, String>> perThreadRequestParameterOverrides = ThreadLocal.withInitial(HashMap::new);


	/**
	 * Some entity fields are excluded from IMS requests by {@link BeanParameterUtils}. This utility can be used to
	 * temporarily override the globally defined inclusions/exclusions for an entity field or field(s) while the service is called.
	 *
	 * @param serviceCall             callable to handle the service request
	 * @param entityPropertyOverrides the entity overrides to use for the request
	 * @param <T>                     the expected return object type
	 * @return the result of serviceCall
	 */
	public static <T> T serviceCallWithOverrides(Callable<T> serviceCall, EntityPropertyOverrides... entityPropertyOverrides) {
		Arrays.stream(entityPropertyOverrides)
				.forEach(RequestOverrideHandler::overrideRequestEntityProperties);
		try {
			return serviceCall.call();
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to compute the result of the service call.", e);
		}
		finally {
			clearRequestOverrides();
		}
	}


	/**
	 * Some requests need to override parameters for a service request such as properties to return, depth, etc. This utility can be used to
	 * override the globally defined parameters for a service call.
	 *
	 * @param serviceCall               callable to handle the service request
	 * @param requestParameterOverrides the request parameter overrides to use for the request
	 * @param <T>                       the expected return object type
	 * @return the result of serviceCall
	 */
	public static <T> T serviceCallWithOverrides(Callable<T> serviceCall, RequestParameterOverrides... requestParameterOverrides) {
		Arrays.stream(requestParameterOverrides)
				.forEach(RequestOverrideHandler::overrideRequestParameters);
		try {
			return serviceCall.call();
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to compute the result of the service call.", e);
		}
		finally {
			clearRequestOverrides();
		}
	}


	/**
	 * Some requests need to override exclusions and parameters for a service request such as properties to return, depth, etc. This utility can be used to
	 * override the globally defined parameters for a service call.
	 *
	 * @param serviceCall               callable to handle the service request
	 * @param entityPropertyOverrides   the entity overrides to use for the request
	 * @param requestParameterOverrides the request parameter overrides to use for the request
	 * @param <T>                       the expected return object type
	 * @return the result of serviceCall
	 */
	public static <T> T serviceCallWithOverrides(Callable<T> serviceCall, EntityPropertyOverrides[] entityPropertyOverrides, RequestParameterOverrides... requestParameterOverrides) {
		Arrays.stream(entityPropertyOverrides)
				.forEach(RequestOverrideHandler::overrideRequestEntityProperties);
		Arrays.stream(requestParameterOverrides)
				.forEach(RequestOverrideHandler::overrideRequestParameters);
		try {
			return serviceCall.call();
		}
		catch (Exception e) {
			throw new RuntimeException("Unable to compute the result of the service call.", e);
		}
		finally {
			clearRequestOverrides();
		}
	}


	/**
	 * Returns the result of invoking the service call with a max depth of the provided value.
	 *
	 * @see #serviceCallWithOverrides(Callable, RequestParameterOverrides...)
	 */
	public static <T> T serviceCallWithOverriddenMaxDepth(Callable<T> serviceCall, int maxDepth) {
		return serviceCallWithOverrides(serviceCall, new RequestParameterOverrides(REQUESTED_MAX_DEPTH_PARAMETER, Integer.toString(maxDepth)));
	}


	/**
	 * Returns the results of invoking a listing service call with the provided properties populated for the returned list.
	 * Uses {@link #REQUESTED_PROPERTIES_ROOT_PARAMETER} of <code>data</code> and {@link #REQUESTED_PROPERTIES_PARAMETER} equal to the provided properties argument.
	 *
	 * @see #serviceCallWithOverrides(Callable, RequestParameterOverrides...)
	 */
	public static <T> T serviceCallWithDataRootPropertiesFiltering(Callable<T> serviceCall, String pipeDelimitedProperties) {
		return serviceCallWithOverrides(serviceCall, new RequestParameterOverrides(REQUESTED_PROPERTIES_ROOT_PARAMETER, "data"), new RequestParameterOverrides(REQUESTED_PROPERTIES_PARAMETER, pipeDelimitedProperties));
	}

	///////////////////////////////////////////////////////////////////////////


	/**
	 * Determines if an entity's globally excluded field should be overridden and included in the request to IMS.
	 *
	 * @param type      the entity class type to check field override for
	 * @param fieldName the entity's member field name to override
	 * @return true if fieldName is registered for type
	 */
	static boolean isRequestEntityPropertyOverrideIncluded(Class<?> type, String fieldName) {
		Map<Class<?>, Set<String>> enableEntityProperties = getIncludeExcludeMap(true);
		return enableEntityProperties.containsKey(type) && enableEntityProperties.get(type).contains(fieldName);
	}


	/**
	 * Determines if an entity's globally included field should be overridden and excluded in the request to IMS.
	 *
	 * @param type      the entity class type to check field override for
	 * @param fieldName the entity's member field name to override
	 * @return true if fieldName is registered for type
	 */
	static boolean isRequestEntityPropertyOverrideExcluded(Class<?> type, String fieldName) {
		Map<Class<?>, Set<String>> enableEntityProperties = getIncludeExcludeMap(false);
		return enableEntityProperties.containsKey(type) && enableEntityProperties.get(type).contains(fieldName);
	}


	/**
	 * Adds the request parameter overrides to the provided list of {@link NameValuePair}
	 */
	static void addRequestParameterOverrides(List<NameValuePair> httpParams) {
		Set<String> overrideParameterKeys = getParameterMap().keySet();
		httpParams.removeIf(existingParameter -> overrideParameterKeys.contains(existingParameter.getName()));
		getParameterMap().forEach((parameter, value) -> httpParams.add(new BasicNameValuePair(parameter, value)));
	}

	///////////////////////////////////////////////////////////////////////////


	/**
	 * Registers overrides to be used during service execution Be sure to clear the overrides when complete
	 * with {@link RequestOverrideHandler#clearRequestOverrides()}.
	 *
	 * @param entityPropertyOverrides the entity class type and it's field names to register overrides for
	 */
	private static void overrideRequestEntityProperties(EntityPropertyOverrides entityPropertyOverrides) {
		CollectionUtils.getValue(getIncludeExcludeMap(entityPropertyOverrides.include), entityPropertyOverrides.entity, () -> entityPropertyOverrides.entityProperties);
	}


	/**
	 * Registers overrides to be used during service execution Be sure to clear the overrides when complete
	 * with {@link RequestOverrideHandler#clearRequestOverrides()}.
	 *
	 * @param requestParameterOverrides the parameter to override
	 */
	private static void overrideRequestParameters(RequestParameterOverrides requestParameterOverrides) {
		CollectionUtils.getValue(getParameterMap(), requestParameterOverrides.parameter, () -> requestParameterOverrides.value);
	}


	/**
	 * Returns the current thread's inclusion or exclusion map for getting overrides.
	 */
	private static Map<Class<?>, Set<String>> getIncludeExcludeMap(boolean include) {
		Map<Boolean, Map<Class<?>, Set<String>>> overrideMap = perThreadRequestEntityPropertyOverrides.get();
		return CollectionUtils.getValue(overrideMap, include, HashMap::new);
	}


	/**
	 * Returns the current thread's parameter map override.
	 */
	private static Map<String, String> getParameterMap() {
		return perThreadRequestParameterOverrides.get();
	}


	/**
	 * Clear registered entity field overrides.
	 */
	private static void clearRequestOverrides() {
		perThreadRequestEntityPropertyOverrides.remove();
		perThreadRequestParameterOverrides.remove();
	}


	/**
	 * Class that can be used as a placeholder for an entity's field names that should be used for overridden service calls.
	 */
	public static class EntityPropertyOverrides {

		private final Class<?> entity;
		private final boolean include;
		private final Set<String> entityProperties;


		public EntityPropertyOverrides(Class<?> entity, boolean include, String... fieldNames) {
			this.entity = entity;
			this.include = include;
			this.entityProperties = CollectionUtils.createHashSet(fieldNames);
		}
	}

	/**
	 * Class that can be used to override request parameters for a service call.
	 */
	public static class RequestParameterOverrides {

		private final String parameter;
		private final String value;


		public RequestParameterOverrides(String parameter, String value) {
			this.parameter = parameter;
			this.value = value;
		}
	}
}
