package com.clifton.test.protocol;


import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;


/**
 * The <code>ImsResponseAdapter</code> is a {@link JsonDeserializer} that handles the deserialization of IMS responses. In particular, it examines
 * the 'data' property and determines how to deserialize it. Prior to deserialization, {@link ImsResponseAdapter#setExpectedDataType(Type)} must be called.
 *
 * @author jgommels
 */
@SuppressWarnings("rawtypes")
public class ImsResponseAdapter implements JsonDeserializer<ImsResponseImpl> {

	private static final String RESULT_PROPERTY_NAME_KEY = "RESULT_PROPERTY_NAME";
	private static final String DATA_TYPE_KEY = "DATA_TYPE";

	private final ImsProtocolClientConfiguration configuration;


	public ImsResponseAdapter(ImsProtocolClientConfiguration configuration) {
		this.configuration = configuration;
	}


	public void setExpectedResultPropertyName(String name) {
		this.configuration.getContextHandler().setBean(RESULT_PROPERTY_NAME_KEY, name);
	}


	/**
	 * Sets the expected data type to be returned from IMS.
	 */
	public void setExpectedDataType(Type dataType) {
		this.configuration.getContextHandler().setBean(DATA_TYPE_KEY, dataType);
	}


	@SuppressWarnings("unchecked")
	@Override
	public ImsResponseImpl deserialize(JsonElement json, @SuppressWarnings("unused") Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
		ImsResponseImpl response = context.deserialize(json, ImsResponseImpl.class);
		Object data;
		String resultPropertyName = (String) this.configuration.getContextHandler().getBean(RESULT_PROPERTY_NAME_KEY);
		Type dataType = (Type) this.configuration.getContextHandler().getBean(DATA_TYPE_KEY);

		//If the return type of the service method is 'void', then set the data to null.
		if (Void.TYPE == dataType || !response.isSuccess()) {
			data = null;
		}
		//If it's a list, deserialize it as a JsonArray
		else if (isListType(dataType)) {
			JsonElement elementToConvertToList = json.getAsJsonObject().get(resultPropertyName);
			if (elementToConvertToList == null) {
				throw new IllegalStateException("Cannot find '" + resultPropertyName + "' property in JSON result: " + json);
			}

			/*
			 * If the result element (most likely "data") has a "rows" element inside of it, then deserialize that
			 * element instead. Sometimes the serialized format will have a "rows" element inside of "data", and this is
			 * where the entities are contained.
			 */
			if (elementToConvertToList.isJsonObject() && elementToConvertToList.getAsJsonObject().has("rows")) {
				elementToConvertToList = elementToConvertToList.getAsJsonObject().get("rows");
			}
			data = context.deserialize(elementToConvertToList.getAsJsonArray(), dataType);
		}
		//If it's an interface, deserialize it to null for now
		else if (dataType instanceof Class && ((Class<?>) dataType).isInterface()) {
			data = null;
		}
		//If it's a primitive, deserialize it as a primitive
		// Strings appear to be special case and they aren't primitive, but need to use getAsJsonPrimitive
		else if (dataType instanceof Class && (((Class<?>) dataType).isPrimitive() || ((Class<?>) dataType).isAssignableFrom(String.class))) {
			data = context.deserialize(json.getAsJsonObject().getAsJsonPrimitive(resultPropertyName), dataType);
		}
		//Otherwise, deserialize it is an object
		else {
			data = context.deserialize(json.getAsJsonObject().getAsJsonObject(resultPropertyName), dataType);
		}
		response.setData(data);
		return response;
	}


	private boolean isListType(Type dataType) {
		if (dataType instanceof Class && ((Class<?>) dataType).isAssignableFrom(List.class)) {
			return true;
		}
		else if (dataType instanceof ParameterizedType) {
			return isListType(((ParameterizedType) dataType).getRawType());
		}

		return false;
	}
}
