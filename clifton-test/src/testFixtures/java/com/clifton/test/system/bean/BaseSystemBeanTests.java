package com.clifton.test.system.bean;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.system.bean.SystemBeanPropertyType;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.bean.SystemBeanType;
import com.clifton.system.bean.search.SystemBeanTypeSearchForm;
import com.clifton.test.BaseIntegrationTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;


/**
 * @author manderson
 */
public class BaseSystemBeanTests extends BaseIntegrationTest {

	@Resource
	private SystemBeanService systemBeanService;


	@Test
	public void testCanInstantiateAllSystemBeanTypes() {
		List<SystemBeanType> systemBeanList = this.systemBeanService.getSystemBeanTypeList(new SystemBeanTypeSearchForm());
		List<String> failedClassNames = new ArrayList<>();
		List<String> failedClassPropertyNames = new ArrayList<>();
		for (SystemBeanType beanType : systemBeanList) {
			try {
				Class<?> beanClass = CoreClassUtils.getClass(beanType.getClassName());
				BeanUtils.newInstance(beanClass);

				// After the Class is confirmed it is correct - check property type definitions
				List<SystemBeanPropertyType> propertyTypeList = this.systemBeanService.getSystemBeanPropertyTypeListByType(beanType.getId());
				for (SystemBeanPropertyType propertyType : CollectionUtils.getIterable(propertyTypeList)) {
					if (!BeanUtils.isPropertyPresent(beanClass, propertyType.getSystemPropertyName())) {
						failedClassPropertyNames.add(beanType.getClassName() + "." + propertyType.getSystemPropertyName());
					}
				}
			}
			catch (RuntimeException e) {
				if (e.getCause() instanceof ClassNotFoundException) {
					failedClassNames.add(beanType.getClassName());
				}
				else {
					throw e;
				}
			}
		}

		if (!failedClassNames.isEmpty() || !failedClassPropertyNames.isEmpty()) {
			StringBuilder errorMsg = new StringBuilder();
			if (!failedClassNames.isEmpty()) {
				errorMsg.append("Could not instantiate System Beans for the following class names:");
				errorMsg.append(System.lineSeparator());
				for (String className : failedClassNames) {
					errorMsg.append(className);
					errorMsg.append(System.lineSeparator());
				}
			}
			if (!failedClassPropertyNames.isEmpty()) {
				errorMsg.append("Could not find valid properties following bean type class system properties:");
				errorMsg.append(System.lineSeparator());
				for (String classPropertyName : failedClassPropertyNames) {
					errorMsg.append(classPropertyName);
					errorMsg.append(System.lineSeparator());
				}
			}
			Assertions.fail(errorMsg.toString());
		}
	}
}
