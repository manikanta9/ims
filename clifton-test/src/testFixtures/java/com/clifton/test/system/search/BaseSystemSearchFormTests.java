package com.clifton.test.system.search;

import com.clifton.core.beans.BaseEntity;
import com.clifton.core.beans.BaseSimpleEntity;
import com.clifton.core.beans.BaseUpdatableOnlyEntity;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.beans.ManyToManyEntity;
import com.clifton.core.beans.ObjectWrapper;
import com.clifton.core.dataaccess.search.ComparisonConditions;
import com.clifton.core.dataaccess.search.SearchField;
import com.clifton.core.dataaccess.search.SearchFieldCustomTypes;
import com.clifton.core.dataaccess.search.form.BaseSearchForm;
import com.clifton.core.dataaccess.search.form.SearchForm;
import com.clifton.core.dataaccess.search.form.SearchFormUtils;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.ArrayUtils;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.ClassUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.collections.MultiValueHashMap;
import com.clifton.core.util.collections.MultiValueMap;
import com.clifton.core.util.date.Time;
import com.clifton.test.BaseIntegrationTest;
import org.hibernate.SessionFactory;
import org.hibernate.metadata.ClassMetadata;
import org.hibernate.metadata.CollectionMetadata;
import org.hibernate.metamodel.internal.MetamodelImpl;
import org.hibernate.metamodel.spi.MetamodelImplementor;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.persister.entity.SingleTableEntityPersister;
import org.hibernate.persister.walking.spi.AttributeDefinition;
import org.hibernate.type.BagType;
import org.hibernate.type.Type;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Array;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.IllegalFormatException;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * Integration tests for search forms. These include expensive project-wide tests to reduce the time expenditure for test execution.
 *
 * @author MikeH
 */
public class BaseSystemSearchFormTests extends BaseIntegrationTest {

	private static final String[] FIELDS_TO_EXCLUDE_FROM_SEARCH_FIELD_TEST = {"dataHolder", "eventObjectData", "description"};

	private Set<Class<?>> omittedDeclaringClasses;
	private Map<Class<?>, String> primitiveToClassTranslate;

	@Resource
	private SessionFactory sessionFactory;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * return the names of all the DTO property names that are included in a SearchField
	 */
	private static String[] searchFieldNames(Field searchProperty) {
		SearchField searchFieldData = AnnotationUtils.getAnnotation(searchProperty, SearchField.class);
		if (searchFieldData == null || !SearchField.SAME_TABLE.equals(searchFieldData.searchFieldPath())) {
			return new String[0];
		}

		if (SearchField.SAME_AS_MARKED_FIELD.equals(searchFieldData.searchField())) {
			return new String[]{searchProperty.getName()};
		}

		return searchFieldData.searchField().split(",");
	}


	/**
	 * Validates the existence of search forms for each DTO AND search fields for each property in the corresponding hibernate mapping file.
	 * Both missing search forms and missing search fields are validated in this test, instead of in separate tests, because the same resources
	 * are used for the verification of missing search forms and missing search fields.
	 * <p>
	 * A summarizing list of search forms with missing search fields is built throughout the test and printed when the test fails.
	 */
	@Test
	@Disabled
	public void testSearchFormVerifyMissingFormsAndAllFields() {
		MultiValueMap<Class<? extends BaseSearchForm>, Field> missingSearchFieldMultiMap = new MultiValueHashMap<>(true);
		MultiValueMap<String, Field> dtoMissingSearchFormMultiMap = new MultiValueHashMap<>(true);

		// collect missing search fields for both existing search forms and search forms that need to be created
		collectMissingSearchFields(missingSearchFieldMultiMap, dtoMissingSearchFormMultiMap, CoreClassUtils.getSimpleEntityClassSet());

		// if any errors found, report them.
		AssertUtils.assertEmpty(missingSearchFieldMultiMap.keySet(), getErrorMessage(missingSearchFieldMultiMap, dtoMissingSearchFormMultiMap));
	}


	/**
	 * Populates the {@link MultiValueMap}s that are passed in with {@link SearchField}s that are missing. A missing search field is generated when
	 * * there is a hibernate mapping for a given property that doesn't have a corresponding search field.
	 * *
	 *
	 * @param missingSearchFieldMultiMap   empty map to populate with SearchForms that are missing search fields
	 * @param dtoMissingSearchFormMultiMap empty map to populate with dto classes that don't have a corresponding search form and the search fields that should be added to the missing search form
	 */
	private void collectMissingSearchFields(MultiValueMap<Class<? extends BaseSearchForm>, Field> missingSearchFieldMultiMap, MultiValueMap<String, Field> dtoMissingSearchFormMultiMap, Set<Class<?>> dtoClasses) {
		Map<String, Class<? extends BaseSearchForm>> searchMap = getSearchFormClassMap();

		// walk the list of classes, and check if all the first level properties have corresponding search fields.
		for (Class<?> dtoClass : CollectionUtils.getIterable(dtoClasses)) {
			// find the corresponding search form
			ClassMetadata metadata = (ClassMetadata) ((MetamodelImpl) getSessionFactory().getMetamodel()).entityPersister(dtoClass);
			if (metadata != null) {
				Map<String, Field> dtoFieldMap = getDtoFieldMap(dtoClass);
				Class<? extends BaseSearchForm> searchClass = searchMap.get(dtoClass.getSimpleName());
				Set<String> searchFieldSet = searchClass == null ? null : getSearchFieldSet(searchClass);

				// walk the properties of the hibernate metadata
				for (String propertyName : metadata.getPropertyNames()) {
					Type propertyType = metadata.getPropertyType(propertyName);

					// omit if the property is an association or a collection
					if (propertyType.isAssociationType() || propertyType.isCollectionType()) {
						continue;
					}

					Field field = dtoFieldMap.get(propertyName);
					if (field == null) { // property in the hibernate mapping NOT on the DTO
						continue;
					}

					// skip if the declaring class is in the omit set
					if (getOmittedDeclaringClasses().contains(field.getDeclaringClass())) {
						continue;
					}

					if (StringUtils.equalsAnyIgnoreCase(field.getName(), FIELDS_TO_EXCLUDE_FROM_SEARCH_FIELD_TEST)) {
						continue;
					}

					if (searchClass != null) {
						// if the property is on the search form, don't add it to the missing search field list
						if (searchFieldSet.contains(propertyName)) {
							continue;
						}
						missingSearchFieldMultiMap.put(searchClass, field);
					}
					else {
						dtoMissingSearchFormMultiMap.put(dtoClass.getSimpleName(), field);
					}
				}
			}
		}
	}


	/**
	 * Build an error message containing missing {@link SearchField}s grouped by the search form class that is missing them.
	 * This includes search forms that have not been created yet.
	 *
	 * @param missingSearchFieldMultiMap   search forms that already exist mapped to the fields that are missing
	 * @param dtoMissingSearchFormMultiMap DTOs that don't have a search form and the fields that should be in the search form
	 * @return an error message of missing search fields grouped by the search form they should be in
	 */
	private String getErrorMessage(MultiValueMap<Class<? extends BaseSearchForm>, Field> missingSearchFieldMultiMap, MultiValueMap<String, Field> dtoMissingSearchFormMultiMap) {
		// add message for existing search forms with missing fields
		StringBuilder errorMessageSb = new StringBuilder("The following search forms have missing search fields: ");
		missingSearchFieldMultiMap.entrySet().stream().sorted(Comparator.comparing(entry -> entry.getKey().toString()))
				.forEach(searchFormEntry -> formatMissingFieldMessage(searchFormEntry, errorMessageSb));
		// add message for dto's that don't have search forms
		errorMessageSb.append(StringUtils.NEW_LINE + StringUtils.NEW_LINE + "The following search forms do not exist and must be created with the listed search fields: ");
		dtoMissingSearchFormMultiMap.entrySet().stream().sorted(Map.Entry.comparingByKey())
				.forEach(dtoEntry -> formatMissingSearchFormMessage(dtoEntry, errorMessageSb));
		return errorMessageSb.toString();
	}


	/**
	 * Create an error message formatted like the missing field declaration for existing search forms that are missing {@link SearchField}s.
	 *
	 * @param searchFormEntry map entry containing search form class and collection of missing search fields
	 * @param errorMessageSb  string builder for building the error message
	 */
	private void formatMissingFieldMessage(Map.Entry<Class<? extends BaseSearchForm>, Collection<Field>> searchFormEntry, StringBuilder errorMessageSb) {
		String fieldReferencePrefix = StringUtils.TAB + "@SearchField" + StringUtils.NEW_LINE + StringUtils.TAB;
		StringJoiner problemStringJoiner = new StringJoiner(StringUtils.NEW_LINE + fieldReferencePrefix, fieldReferencePrefix, StringUtils.EMPTY_STRING);
		searchFormEntry.getValue().forEach(field -> problemStringJoiner.add(getDeclarationPrototype(field)));
		errorMessageSb.append(StringUtils.NEW_LINE).append(StringUtils.NEW_LINE)
				.append(searchFormEntry.getKey().getName()).append(":").append(StringUtils.NEW_LINE)
				.append(problemStringJoiner);
	}


	/**
	 * Create an error message formatted like the missing field declaration for DTOs that don't have a search form.
	 *
	 * @param dtoEntry       map entry containing dto class name and collection of required search fields
	 * @param errorMessageSb string builder for building the error message
	 */
	private void formatMissingSearchFormMessage(Map.Entry<String, Collection<Field>> dtoEntry, StringBuilder errorMessageSb) {
		String fieldReferencePrefix = StringUtils.TAB + "@SearchField" + StringUtils.NEW_LINE + StringUtils.TAB;
		StringJoiner problemStringJoiner = new StringJoiner(StringUtils.NEW_LINE + fieldReferencePrefix, fieldReferencePrefix, StringUtils.EMPTY_STRING);
		dtoEntry.getValue().forEach(field -> problemStringJoiner.add(getDeclarationPrototype(field)));
		errorMessageSb.append(StringUtils.NEW_LINE).append(StringUtils.NEW_LINE)
				.append(dtoEntry.getKey()).append("SearchForm").append(":").append(StringUtils.NEW_LINE)
				.append(problemStringJoiner);
	}


	/**
	 * turn filed types into the proper Class text
	 */
	private String translateClassType(Class<?> primitive) {
		String returnClassName = getPrimitiveToClassTranslate().get(primitive);
		return returnClassName == null ? primitive.getSimpleName() : returnClassName;
	}


	/**
	 * Load all the search form classes into a Map keyed by DTO class name.
	 */
	private Map<String, Class<? extends BaseSearchForm>> getSearchFormClassMap() {
		// Get search forms to validate
		List<Class<? extends BaseSearchForm>> searchFormClasses = CoreClassUtils.getProjectClassFiles(BaseSearchForm.class).stream()
				.filter(clazz -> AnnotationUtils.getAnnotation(clazz, SearchForm.class).hasOrmDtoClass())
				.filter(clazz -> !(Modifier.isAbstract(clazz.getModifiers())))
				.collect(Collectors.toList());

		Map<String, Class<? extends BaseSearchForm>> searchMap = new HashMap<>();
		Map<String, Class<?>> projectClassesByName = CoreClassUtils.getProjectClassFiles(Object.class).stream()
				.collect(Collectors.toMap(Class::getName, Function.identity()));
		for (Class<? extends BaseSearchForm> clazz : searchFormClasses) {
			searchMap.put(TestUtils.getDtoClassFromSearchForm(clazz, projectClassesByName).getSimpleName(), clazz);
		}
		return searchMap;
	}


	/**
	 * load all a SearchForm's fields into a Set
	 */
	private Set<String> getSearchFieldSet(Class<? extends BaseSearchForm> searchClass) {
		Set<String> searchFieldSet = new HashSet<>();
		for (Field field : ClassUtils.getClassFields(searchClass, true, true)) {
			for (String searchFieldName : searchFieldNames(field)) {
				if (searchFieldName.contains(".")) {
					// reference to a different entity, skip
				}
				searchFieldSet.add(searchFieldName);
			}
		}
		return searchFieldSet;
	}


	/**
	 * load all of a dtoEntities fields into a Map
	 */
	private Map<String, Field> getDtoFieldMap(Class<?> dtoClass) {
		return ArrayUtils.getStream(ClassUtils.getClassFields(dtoClass, true, false))
				.collect(Collectors.toMap(Field::getName, Function.identity(), (currentValue, newValue) -> currentValue));
	}


	private StringBuilder getDeclarationPrototype(Field field) {
		return new StringBuilder(Modifier.toString(field.getModifiers()))
				.append(' ').append(translateClassType(field.getType()))
				.append(' ').append(field.getName())
				.append(';');
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates search form fields within all search forms to verify the property types, property chaining, and Hibernate mappings for each field.
	 * <p>
	 * All search forms will be completely analyzed and an failed assertion will be thrown with a summarizing list if any search fields violate rules.
	 */
	@Test
	public void testSearchFormValidFields() {
		// Get search forms to validate
		List<Class<? extends BaseSearchForm>> searchFormClasses = CoreClassUtils.getProjectClassFiles(BaseSearchForm.class).stream()
				.filter(clazz -> AnnotationUtils.getAnnotation(clazz, SearchForm.class).hasOrmDtoClass())
				.filter(clazz -> !(Modifier.isAbstract(clazz.getModifiers())))
				.filter(clazz -> !isIgnoreSearchFormDtoValidation(clazz))
				.collect(Collectors.toList());

		// Validate each search form
		Map<String, Class<?>> projectClassesByName = CoreClassUtils.getProjectClassFiles(Object.class).stream()
				.collect(Collectors.toMap(Class::getName, Function.identity()));
		MultiValueMap<String, String> invalidSearchFormFields = new MultiValueHashMap<>(true);
		for (Class<? extends BaseSearchForm> searchFormClass : searchFormClasses) {
			invalidSearchFormFields.putAll(getInvalidSearchFormFieldsForClass(searchFormClass, projectClassesByName));
		}

		// Compile and report any errors
		if (!invalidSearchFormFields.isEmpty()) {
			StringBuilder errorMessageSb = new StringBuilder("Errors were found while validating search forms.");
			String fieldReferencePrefix = StringUtils.TAB + StringUtils.TAB;
			for (String problemCategory : invalidSearchFormFields.keySet().stream().sorted().collect(Collectors.toList())) {
				StringJoiner problemStringJoiner = new StringJoiner(StringUtils.NEW_LINE + fieldReferencePrefix, fieldReferencePrefix, StringUtils.EMPTY_STRING);
				invalidSearchFormFields.get(problemCategory).stream().sorted().forEach(problemStringJoiner::add);
				errorMessageSb.append(StringUtils.NEW_LINE)
						.append(StringUtils.TAB).append(problemCategory).append(':').append(StringUtils.NEW_LINE)
						.append(problemStringJoiner);
			}
			Assertions.fail(errorMessageSb.toString());
		}
	}


	/**
	 * Gets the map of invalid search form fields for the given search form class and the reason(s) for their invalidity.
	 *
	 * @param searchFormClass the search form class for which to check for invalid search form fields
	 * @return a multi-value map of problem categories to descriptions of search fields which fall under those categories
	 */
	private MultiValueMap<String, String> getInvalidSearchFormFieldsForClass(Class<? extends BaseSearchForm> searchFormClass, Map<String, Class<?>> projectClassesByName) {
		MultiValueMap<String, String> invalidSearchFormFields = new MultiValueHashMap<>(true);
		String[] skippedPaths = AnnotationUtils.getAnnotation(searchFormClass, SearchForm.class).skippedPathsForValidation();
		Class<?> dtoClass = TestUtils.getDtoClassFromSearchForm(searchFormClass, projectClassesByName);

		// Validate each field
		for (Field field : ClassUtils.getClassFields(searchFormClass, true, false)) {
			// Guard-clause: Skip non-search-field fields
			SearchField searchFieldData = AnnotationUtils.getAnnotation(field, SearchField.class);
			if (searchFieldData == null) {
				continue;
			}

			// Get all paths that may be built using this field, including a boolean value for whether or not the final field component type should be validated
			Map<String, Boolean> searchFieldsToCheck = new HashMap<>();

			String searchFieldPath = StringUtils.isEqual(searchFieldData.searchFieldPath(), SearchField.SAME_TABLE) ? null : searchFieldData.searchFieldPath();
			String searchField = StringUtils.isEqual(searchFieldData.searchField(), SearchField.SAME_AS_MARKED_FIELD) ? field.getName() : searchFieldData.searchField();

			// Queue validation for the sort field if valid
			final String sortField;
			if (StringUtils.isEqual(searchFieldData.sortField(), SearchField.SORT_NOT_ALLOWED)) {
				sortField = null;
			}
			else if (StringUtils.isEqual(searchFieldData.sortField(), SearchField.SORT_BY_SEARCH_FIELD)) {
				// Infer from search field
				if (ArrayUtils.anyMatch(searchFieldData.comparisonConditions(), ComparisonConditions::isSubQuery)) {
					sortField = null;
				}
				else if (searchFieldData.searchFieldCustomType().isOrderByMultipleSupported()) {
					sortField = searchField;
				}
				else {
					sortField = searchField.split(",")[0];
				}
			}
			else {
				sortField = searchFieldData.sortField();
			}
			if (!searchFieldData.searchFieldCustomType().isOrderByMultipleSupported() && StringUtils.contains(sortField, ",")) {
				// Multiple fields attempted when only a single field is supported
				invalidSearchFormFields.put("Sort field parameter contains multiple values when only a single value is allowed", getSearchFieldDescription(searchFormClass, field, StringUtils.joinExcludingNulls(".", searchFieldPath, sortField), null));
			}

			// Queue validation for the sort field(s), not incorporating type validation
			ArrayUtils.getStream(StringUtils.split(sortField, ","))
					.map(fieldName -> StringUtils.joinExcludingNulls(".", searchFieldPath, fieldName))
					.forEach(fullSearchFieldPath -> searchFieldsToCheck.put(fullSearchFieldPath, false));
			// Queue validation for the search field(s), enabling type validation; this supersedes queued sort fields of the same paths
			ArrayUtils.getStream(StringUtils.split(searchField, ","))
					.map(fieldName -> StringUtils.joinExcludingNulls(".", searchFieldPath, fieldName))
					.forEach(fullSearchFieldPath -> searchFieldsToCheck.put(fullSearchFieldPath, true));

			// Validate each path which will be built for the current field
			for (Map.Entry<String, Boolean> searchFieldToCheckInfo : searchFieldsToCheck.entrySet()) {
				String fullSearchFieldPath = searchFieldToCheckInfo.getKey();
				ObjectWrapper<String> propertyNameHolder = new ObjectWrapper<>("<MISSING PATH PROPERTY>");
				try {
					if (isHibernateMappingAvailableForPackage(dtoClass)) {
						// Validate each property within the path chain
						String[] pathProperties = fullSearchFieldPath.split("\\.");
						StringJoiner currentPathJoiner = new StringJoiner(".");
						Class<?> resolvedDtoClass = validateSearchFieldPath(pathProperties, dtoClass, skippedPaths, currentPathJoiner, propertyNameHolder);

						// For final item, if this item is a search field and does not use a formula, validate the DTO class against the corresponding search form property type
						if (resolvedDtoClass != null && searchFieldToCheckInfo.getValue() && StringUtils.isEqual(searchFieldData.formula(), SearchField.NO_FORMULA)) {
							validateSearchFormFieldType(field, searchFieldData, resolvedDtoClass, searchFieldsToCheck);
						}
					}
				}
				catch (Exception e) {
					// Report invalid search field with path information
					invalidSearchFormFields.put(e.getMessage(), getSearchFieldDescription(searchFormClass, field, fullSearchFieldPath, propertyNameHolder.getObject()));
				}
			}

			// Queue validation for required presence or absence of one-to-many and many-to-many joins
			Collection<String> requiredSourceToManyAssociationFieldsToCheck = new HashSet<>();
			Collection<String> disallowedSourceToManyAssociationFieldsToCheck = new HashSet<>();
			if (!ArrayUtils.isEmpty(searchFieldData.comparisonConditions())
					&& Arrays.stream(searchFieldData.comparisonConditions()).allMatch(ComparisonConditions::isSubQuery)) {
				// Sub-query conditions join on the search field path and sort fields only
				if (searchFieldPath != null) {
					disallowedSourceToManyAssociationFieldsToCheck.add(searchFieldPath);
				}
				List<String> sortFieldList = ArrayUtils.getStream(StringUtils.split(sortField, ","))
						.map(fieldName -> StringUtils.joinExcludingNulls(".", searchFieldPath, fieldName))
						.collect(Collectors.toList());
				disallowedSourceToManyAssociationFieldsToCheck.addAll(sortFieldList);
				// Sub-query conditions require an association for the first search field path parameter
				for (String fieldName : searchField.split(",")) {
					String firstPathElement = fieldName.split("\\.")[0];
					requiredSourceToManyAssociationFieldsToCheck.add(StringUtils.joinExcludingNulls(".", searchFieldPath, firstPathElement));
				}
			}
			else {
				disallowedSourceToManyAssociationFieldsToCheck.addAll(searchFieldsToCheck.keySet());
			}
			// Sanity check: An association for each included path must be either required or disallowed
			AssertUtils.assertEmpty(CollectionUtils.getIntersection(disallowedSourceToManyAssociationFieldsToCheck, requiredSourceToManyAssociationFieldsToCheck), "Unexpected state: Search field paths may not simultaneous require an association and disallow an association");

			// Validate one-to-many and many-to-many joins in paths
			sourceToManyValidation:
			for (String fullSearchFieldPath : CollectionUtils.combineCollections(requiredSourceToManyAssociationFieldsToCheck, disallowedSourceToManyAssociationFieldsToCheck)) {
				if (ArrayUtils.contains(skippedPaths, fullSearchFieldPath)) {
					continue;
				}
				boolean isAssociationRequired = requiredSourceToManyAssociationFieldsToCheck.contains(fullSearchFieldPath);
				String pathElement = null;
				try {
					String[] pathElements = fullSearchFieldPath.split("\\.");
					Class<?> currentElementType = dtoClass;
					for (int i = 0; i < pathElements.length; i++) {
						pathElement = pathElements[i];
						// Must get the persister from the map to prevent an exception being thrown
						ClassMetadata classMetadata = (ClassMetadata) ((MetamodelImpl) getSessionFactory().getMetamodel()).entityPersisters().get(currentElementType.getName());
						if (classMetadata == null) {
							// Path cannot be completed; ignore for n-to-many validation
							continue sourceToManyValidation;
						}
						Type propertyType;
						try {
							propertyType = classMetadata.getPropertyType(pathElement);
						}
						catch (Exception e) {
							// Path cannot be completed; ignore for n-to-many validation
							continue sourceToManyValidation;
						}
						currentElementType = propertyType.getReturnedClass();
						if (isAssociationRequired && i == pathElements.length - 1) {
							AssertUtils.assertTrue(propertyType.isCollectionType(), "The first search field path element must be a one-to-many or many-to-many association when using sub-query criteria");
						}
						else {
							AssertUtils.assertTrue(!propertyType.isCollectionType(), "One-to-many and many-to-many associations are not allowed outside of sub-query criteria");
						}
					}
				}
				catch (Exception e) {
					// Report invalid search field with path information
					invalidSearchFormFields.put(e.getMessage(), getSearchFieldDescription(searchFormClass, field, fullSearchFieldPath, pathElement));
				}
			}
			if (!StringUtils.isEqual(searchFieldData.formula(), SearchField.NO_FORMULA)) {
				String[] searchfields = searchField.split(",");
				if (searchfields.length > SearchFormUtils.getSqlFormulaMaximumNumberOfParameters(searchFieldData.formula())) {
					invalidSearchFormFields.put("The number of parameters referenced in the formula must be greater than or equal to the number of fields", getSearchFieldDescription(searchFormClass, field, null, null));
				}
				String formattedFormula = SearchFormUtils.convertSqlFormulaToStringFormatCompatible(searchFieldData.formula());
				try {
					String.format(formattedFormula, (Object[]) searchfields);
				}
				catch (IllegalFormatException e) {
					invalidSearchFormFields.put("searchField formula is not properly formatted", getSearchFieldDescription(searchFormClass, field, null, null));
				}
			}
		}
		return invalidSearchFormFields;
	}


	/**
	 * Validates that the given search field path is valid within the current search form.
	 */
	private Class<?> validateSearchFieldPath(String[] pathProperties, Class<?> dtoClass, String[] skippedPaths, StringJoiner
			currentPathJoiner, ObjectWrapper<String> propertyNameHolder) {
		AssertUtils.assertTrue(pathProperties.length > 0, "Missing search field path");
		Class<?> resolvedDtoClass = dtoClass;
		for (String pathProperty : pathProperties) {
			propertyNameHolder.setObject(pathProperty);
			currentPathJoiner.add(propertyNameHolder.getObject());
			if (ArrayUtils.contains(skippedPaths, currentPathJoiner.toString())) {
				// Remaining path is skipped
				resolvedDtoClass = null;
				break;
			}
			resolvedDtoClass = validateHibernateMapping(propertyNameHolder.getObject(), resolvedDtoClass);
		}
		return resolvedDtoClass;
	}


	/**
	 * Validates that the {@link Field} type within the search form matches the allowed fields for the given {@link SearchField} in the current search form.
	 */
	private void validateSearchFormFieldType(Field field, SearchField searchFieldData, Class<?> dtoClass, Map<String, Boolean> searchFieldsToCheck) {
		Class<?> computedDtoClass = getComputedDtoClass(dtoClass, searchFieldData);
		ComparisonConditions condition = searchFieldData.comparisonConditions().length > 0
				? searchFieldData.comparisonConditions()[0]
				: getDefaultConditionForType(field.getType());
		List<Class<?>> expectedTypeList = getExpectedFieldTypeListForCondition(field, computedDtoClass, condition, searchFieldData.searchFieldCustomType(), searchFieldsToCheck);

		// Verify search form property type matches type expected for queries
		AssertUtils.assertTrue(expectedTypeList.contains(field.getType()), "Invalid search form field type (expected one of: %s, found: [%s])", expectedTypeList, field.getType());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Validates an individual Hibernate mapping from a DTO to a property name.
	 * <p>
	 * Mappings are validated against the following rules:
	 * <ul>
	 * <li>The mapping exists
	 * <li>The DTO (or a valid mapped subclass) has the given property
	 * <li>The property on the DTO (or a valid mapped subclass) matches the mapped property type
	 * </ul>
	 *
	 * @param propertyName   the mapping property name, such as the name which would be passed to a HQL query
	 * @param parentDtoClass the DTO which should own the given property
	 * @return the mapped class for the given property name as provided by Hibernate
	 * @throws Exception on invalid mapping with the cause as its message
	 */
	private Class<?> validateHibernateMapping(String propertyName, Class<?> parentDtoClass) {
		// Verify presence of Hibernate mapping
		ClassMetadata classMetadata = (ClassMetadata) ((MetamodelImplementor) getSessionFactory().getMetamodel()).entityPersister(parentDtoClass);
		org.hibernate.type.Type propertyType;
		try {
			propertyType = classMetadata.getPropertyType(propertyName);
		}
		catch (Exception e) {
			throw new RuntimeException("Missing Hibernate mapping");
		}

		// Validate bean property
		boolean propertyIsBagType = propertyType instanceof BagType;
		if (!propertyIsBagType) {
			// Find the correct class to seek for the bean property: For subclass fields, a Hibernate-mapped subclass will be applied
			Class<?> mappedParentDtoClass = parentDtoClass;
			if (classMetadata instanceof SingleTableEntityPersister) {
				for (AttributeDefinition attributeDefinition : ((SingleTableEntityPersister) classMetadata).getAttributes()) {
					if (propertyName.equals(attributeDefinition.getName())) {
						if (attributeDefinition.getSource() instanceof EntityPersister) {
							mappedParentDtoClass = ((EntityPersister) attributeDefinition.getSource()).getMappedClass();
						}
						break;
					}
				}
			}

			PropertyDescriptor descriptor = BeanUtils.getPropertyDescriptor(mappedParentDtoClass, propertyName);
			if (descriptor == null || descriptor.getReadMethod() == null || descriptor.getWriteMethod() == null) {
				throw new RuntimeException("Bean property missing");
			}

			/*
			 * Verify bean property type matches mapping property type.
			 *
			 * The bean property type may be one of multiple possible types:
			 * - The Hibernate-mapped DTO class or any supertype
			 * - The Hibernate-mapped proxy DTO class or any supertype
			 * - Any of the Hibernate-mapped subclasses (no discriminator-value validation is performed here for this case)
			 */
			Class<?> beanPropertyType = ClassUtils.primitiveToWrapperIfNecessary(descriptor.getPropertyType());
			Class<?> mappedProxyDtoClass = propertyType.getReturnedClass();
			List<String> mappedDtoSubclasses = new ArrayList<>();
			// Must get the persister from the map to prevent an exception being thrown
			ClassMetadata propertyTypeClassMetadata = (ClassMetadata) ((MetamodelImpl) getSessionFactory().getMetamodel()).entityPersisters().get(propertyType.getReturnedClass().getName());
			if (propertyTypeClassMetadata instanceof SingleTableEntityPersister) {
				SingleTableEntityPersister propertyTypePersister = (SingleTableEntityPersister) propertyTypeClassMetadata;
				mappedProxyDtoClass = propertyTypePersister.getConcreteProxyClass();
				mappedDtoSubclasses.addAll(Arrays.asList(propertyTypePersister.getSubclassClosure()));
			}
			AssertUtils.assertTrue(beanPropertyType.isAssignableFrom(mappedProxyDtoClass) || mappedDtoSubclasses.contains(beanPropertyType.getName()),
					"Bean property type is not assignable from mapping type (expected: [%s], found: [%s])", propertyType.getReturnedClass().getName(), beanPropertyType.getName());
		}

		// Get child class via Hibernate mapping
		Class<?> currentDtoClass;
		if (propertyIsBagType) {
			CollectionMetadata collectionMetadata = (CollectionMetadata) ((MetamodelImplementor) getSessionFactory().getMetamodel()).collectionPersister(parentDtoClass.getName() + '.' + propertyName);
			currentDtoClass = collectionMetadata.getElementType().getReturnedClass();
		}
		else {
			currentDtoClass = propertyType.getReturnedClass();
		}
		return currentDtoClass;
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private ComparisonConditions getDefaultConditionForType(Class<?> type) {
		final ComparisonConditions defaultCondition;
		if (Integer.class.equals(type)
				|| Long.class.equals(type)
				|| Short.class.equals(type)
				|| Date.class.equals(type)
				|| Time.class.equals(type)
				|| BigDecimal.class.equals(type)
				|| Boolean.class.equals(type)
				|| Double.class.equals(type)
				|| type.isEnum()) {
			defaultCondition = ComparisonConditions.EQUALS;
		}
		else if (String.class.equals(type)) {
			defaultCondition = ComparisonConditions.LIKE;
		}
		else if (type.isArray()) {
			defaultCondition = ComparisonConditions.IN;
		}
		else {
			throw new IllegalArgumentException(String.format("Unsupported search field data type [%s]", type));
		}
		return defaultCondition;
	}


	private List<Class<?>> getExpectedFieldTypeListForCondition(Field field, Class<?> dtoClass, ComparisonConditions condition, SearchFieldCustomTypes
			searchFieldCustomType, Map<String, Boolean> searchFieldsToCheck) {
		List<Class<?>> expectedTypeList;

		// Special Case - If Allowing String and Numbers - search field should be a String
		if (searchFieldCustomType == SearchFieldCustomTypes.CONCATENATE_STRING_AND_NUMBER) {
			expectedTypeList = CollectionUtils.createList(String.class);
			return expectedTypeList;
		}
		else if (searchFieldCustomType == SearchFieldCustomTypes.COALESCE_SORT_ON_ALL_NULL) {
			expectedTypeList = CollectionUtils.createList(Boolean.class);
			return expectedTypeList;
		}

		switch (condition) {
			case IS_NULL:
			case IS_NOT_NULL:
				expectedTypeList = Collections.singletonList(Boolean.class);
				break;
			case IN:
			case IN_OR_IS_NULL:
			case NOT_IN:
				expectedTypeList = Collections.singletonList(Array.newInstance(dtoClass, 0).getClass());
				break;
			case EQUALS:
			case NOT_EQUALS:
			case EQUALS_OR_IS_NULL:
			case NOT_EQUALS_OR_IS_NULL:
				expectedTypeList = new ArrayList<>();
				expectedTypeList.add(field.getType().isArray() ? Array.newInstance(dtoClass, 0).getClass() : dtoClass);
				// Allow strings for enums when multiple search fields exist (e.g., search patterns)
				boolean enumMultiField = dtoClass.isEnum()
						&& searchFieldsToCheck.values().stream()
						.filter(value -> value)
						.count() > 1;
				if (enumMultiField) {
					expectedTypeList.add(field.getType().isArray() ? String[].class : String.class);
				}
				break;
			case LESS_THAN:
			case LESS_THAN_OR_EQUALS:
			case GREATER_THAN:
			case GREATER_THAN_OR_EQUALS:
			case LIKE:
			case NOT_LIKE:
			case BEGINS_WITH:
			case NOT_BEGINS_WITH:
			case ENDS_WITH:
			case NOT_ENDS_WITH:
			case LESS_THAN_OR_IS_NULL:
			case LESS_THAN_OR_EQUALS_OR_IS_NULL:
			case GREATER_THAN_OR_IS_NULL:
			case GREATER_THAN_OR_EQUALS_OR_IS_NULL:
			case EXISTS:
			case EXISTS_LIKE:
			case NOT_EXISTS:
			case NOT_EXISTS_LIKE:
				expectedTypeList = new ArrayList<>();
				expectedTypeList.add(field.getType().isArray() ? Array.newInstance(dtoClass, 0).getClass() : dtoClass);
				// Allow strings for non-equality comparisons against enum values
				if (dtoClass.isEnum()) {
					expectedTypeList.add(field.getType().isArray() ? String[].class : String.class);
				}
				break;
			default:
				throw new IllegalStateException(String.format("Unexpected condition type: [%s]", condition));
		}
		return expectedTypeList;
	}


	private Class<?> getComputedDtoClass(Class<?> dtoClass, SearchField searchFieldData) {
		// Override the DTO class as needed based on the computed search binding
		final Class<?> computedDtoClass;
		if (searchFieldData.searchFieldCustomType() == SearchFieldCustomTypes.DATE_DIFFERENCE_SECONDS || searchFieldData.searchFieldCustomType() == SearchFieldCustomTypes.DATE_DIFFERENCE_MILLISECONDS) {
			computedDtoClass = Long.class;
		}
		else {
			computedDtoClass = dtoClass;
		}
		return computedDtoClass;
	}


	private boolean isHibernateMappingAvailableForPackage(Class<?> resolvedDtoClass) {
		String dtoClassPath = resolvedDtoClass.getName();
		return ArrayUtils.getStream(getPackagesToExcludeFromHibernateMappingTest()).noneMatch(dtoClassPath::startsWith);
	}


	private String getSearchFieldDescription(Class<?> clazz, Field field, String criteriaPath, String pathElement) {
		StringJoiner sj = new StringJoiner(" ");
		if (clazz != null && field != null) {
			sj.add(String.format("Field: [%s#%s].", clazz.getName(), field.getName()));
		}
		if (!StringUtils.isEmpty(criteriaPath)) {
			sj.add(String.format("Criteria path: [%s].", criteriaPath));
		}
		if (!StringUtils.isEmpty(pathElement)) {
			sj.add(String.format("Path element: [%s].", pathElement));
		}
		return sj.toString();
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	protected String[] getPackagesToExcludeFromHibernateMappingTest() {
		return new String[0];
	}


	protected boolean isIgnoreSearchFormDtoValidation(Class<? extends BaseSearchForm> clazz) {
		return false;
	}


	/**
	 * lazy loaded list of classes that contain fields that are ignored.
	 */
	private Set<Class<?>> getOmittedDeclaringClasses() {
		if (this.omittedDeclaringClasses == null) {
			this.omittedDeclaringClasses = new HashSet<>();
			this.omittedDeclaringClasses.add(BaseEntity.class);
			this.omittedDeclaringClasses.add(BaseSimpleEntity.class);
			this.omittedDeclaringClasses.add(BaseUpdatableOnlyEntity.class);
			this.omittedDeclaringClasses.add(ManyToManyEntity.class);
		}
		return this.omittedDeclaringClasses;
	}


	/**
	 * lazy loaded list of primitives and the String name of the corresponding Class
	 */
	private Map<Class<?>, String> getPrimitiveToClassTranslate() {
		if (this.primitiveToClassTranslate == null) {
			this.primitiveToClassTranslate = new HashMap<>();
			this.primitiveToClassTranslate.put(byte.class, Byte.class.getSimpleName());
			this.primitiveToClassTranslate.put(short.class, Short.class.getSimpleName());
			this.primitiveToClassTranslate.put(int.class, Integer.class.getSimpleName());
			this.primitiveToClassTranslate.put(long.class, Long.class.getSimpleName());
			this.primitiveToClassTranslate.put(char.class, Character.class.getSimpleName());
			this.primitiveToClassTranslate.put(float.class, Float.class.getSimpleName());
			this.primitiveToClassTranslate.put(double.class, Double.class.getSimpleName());
			this.primitiveToClassTranslate.put(boolean.class, Boolean.class.getSimpleName());
		}
		return this.primitiveToClassTranslate;
	}


	public SessionFactory getSessionFactory() {
		return this.sessionFactory;
	}


	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
