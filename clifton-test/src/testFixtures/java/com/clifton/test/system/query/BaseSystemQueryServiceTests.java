package com.clifton.test.system.query;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.security.user.SecurityUserService;
import com.clifton.system.query.SystemQuery;
import com.clifton.system.query.SystemQueryParameter;
import com.clifton.system.query.SystemQueryParameterValue;
import com.clifton.system.query.SystemQueryService;
import com.clifton.system.query.execute.SystemQueryExecutionService;
import com.clifton.system.query.search.SystemQuerySearchForm;
import com.clifton.system.schema.SystemDataType;
import com.clifton.test.BaseIntegrationTest;
import com.clifton.test.protocol.ImsErrorResponseException;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


/**
 * @author manderson
 */
public abstract class BaseSystemQueryServiceTests extends BaseIntegrationTest {

	@Resource
	private SecurityUserService securityUserService;

	@Resource
	private SystemQueryService systemQueryService;

	@Resource
	private SystemQueryExecutionService systemQueryExecutionService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private static final Map<String, String> QUERY_PARAMETER_VALUE_OVERRIDE_MAP = new HashMap<>();

	private static final Set<String> QUERY_PARAMETER_REQUIRED_OVERRIDE_SET = new HashSet<>();

	private static final Map<String, String> QUERY_PARAMETER_VALUE_DEFAULT_MAP = new HashMap<>();

	private static final Set<String> QUERY_EXCLUDE_SET = new HashSet<>();


	static {
		QUERY_PARAMETER_VALUE_DEFAULT_MAP.put(SystemDataType.BOOLEAN, "false");
		QUERY_PARAMETER_VALUE_DEFAULT_MAP.put(SystemDataType.DATE, "01/31/1981");
		QUERY_PARAMETER_VALUE_DEFAULT_MAP.put(SystemDataType.INTEGER, "1");
		QUERY_PARAMETER_VALUE_DEFAULT_MAP.put(SystemDataType.DECIMAL, "0.0");
		QUERY_PARAMETER_VALUE_DEFAULT_MAP.put(SystemDataType.STRING, "Test");
		QUERY_PARAMETER_VALUE_DEFAULT_MAP.put(SystemDataType.TEXT, "Test");
	}


	/**
	 * Used to explicitly exclude some queries from running in tests
	 */
	protected static void addQueryToExcludeSet(String queryName) {
		QUERY_EXCLUDE_SET.add(queryName);
	}


	/**
	 * Ability to populate a specific override value for a query and parameter.  Useful to help speed up long running queries
	 */
	protected static void addQueryParameterValueOverride(String queryName, String parameterName, String overrideValue) {
		QUERY_PARAMETER_VALUE_OVERRIDE_MAP.put(queryName + ":" + parameterName, overrideValue);
	}


	/**
	 * For a specific query a parameter, require a value.  These are not technically required, but could be inherently required - ie. some we have mutually exclusive, but required within the SQL itself
	 * Marking as required and setting a value could also be used to speed up the query for test execution
	 */
	protected static void addQueryParameterRequiredOverride(String queryName, String parameterName) {
		QUERY_PARAMETER_REQUIRED_OVERRIDE_SET.add(queryName + ":" + parameterName);
	}


	/**
	 * Across all queries we can set a value to help speed up queries, i.e. any query that includes an optional client account - set a default value
	 */
	protected static void addQueryParameterRequiredOverride(String parameterName) {
		QUERY_PARAMETER_REQUIRED_OVERRIDE_SET.add(parameterName);
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testExecuteSystemQueries() {
		SystemQuerySearchForm searchForm = new SystemQuerySearchForm();
		searchForm.setUpdateStatement(false);
		// Excluding grouping queries - might need to have a separate test for them
		searchForm.setGroupingQuery(false);
		List<SystemQuery> queryList = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.systemQueryService.getSystemQueryList(searchForm), "id|name|createUserId");
		AtomicInteger errorCount = new AtomicInteger();
		AtomicInteger skipCount = new AtomicInteger();

		long start = System.currentTimeMillis();
		String errorString = "";
		if (!CollectionUtils.isEmpty(queryList)) {
			String errorFormat = "\nQuery: [%s]: %s";
			// Use a custom FJP to control the number of parallel threads.
			ForkJoinPool customThreadPool = new ForkJoinPool(2);
			try {
				errorString = customThreadPool.submit(
						() -> queryList.parallelStream()
								.map(query -> {
									if (QUERY_EXCLUDE_SET.contains(query.getName())) {
										skipCount.incrementAndGet();
									}
									else {
										try {
											executeSystemQuery(query);
										}
										catch (Throwable e) {
											// Ignore failed queries created by other tests
											if (query.getCreateUserId() != 0 && RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.securityUserService.getSecurityUser(query.getCreateUserId()).getDisplayName().startsWith("Test "), 2)) {
												skipCount.incrementAndGet();
												System.out.println("Skipped Test Query: " + query.getName());
											}
											else {
												String errorMessage = ((e instanceof ImsErrorResponseException) ? ((ImsErrorResponseException) e).getErrorMessageFromIMS() : ExceptionUtils.getOriginalMessage(e));
												// Track those that time out but don't consider them an error - we have SLAs on Prod to track slow queries and can work on speeding them up
												// But we don't want to error the test out as that gives unnecessary failures that we might not have any control over
												if (StringUtils.contains(errorMessage, "The query has timed out.")) {
													skipCount.incrementAndGet();
													// Count it toward the skip count, but print out the query
													System.out.println("WARNING: Potential Slow Query (Timed out): " + query.getName());
												}
												else {
													errorCount.incrementAndGet();
													return String.format(errorFormat, query.getName(), errorMessage);
												}
											}
										}
									}
									return "";
								}).collect(Collectors.joining())
				).get();
			}
			catch (InterruptedException e) {
				Thread.currentThread().interrupt();
			}
			catch (ExecutionException e) {
				throw new RuntimeException(e);
			}
			finally {
				customThreadPool.shutdown();
			}
		}
		System.out.println("TIME TO EXECUTE ALL QUERIES: " + DateUtils.getTimeDifference(System.currentTimeMillis() - start, true));
		if (errorCount.get() > 0) {
			Assertions.fail(errorCount + " failed out of " + (CollectionUtils.getSize(queryList) - skipCount.get()) + " queries: " + errorString);
		}
	}


	private void executeSystemQuery(SystemQuery query) {
		List<SystemQueryParameter> parameterList = RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.systemQueryService.getSystemQueryParameterListByQuery(query.getId()), "id|name|query.name|dataType.name|required");
		if (!CollectionUtils.isEmpty(parameterList)) {
			List<SystemQueryParameterValue> valueList = new ArrayList<>();
			for (SystemQueryParameter parameter : parameterList) {
				SystemQueryParameterValue value = new SystemQueryParameterValue();
				value.setParameter(parameter);
				setQueryParameterValue(value);
				valueList.add(value);
			}
			query.setParameterValueList(valueList);
		}
		RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.systemQueryExecutionService.getSystemQueryResultWithLimit(query, 1), 1); // RETURN MAX ROWS OF 1
	}


	private void setQueryParameterValue(SystemQueryParameterValue queryParameterValue) {
		SystemQueryParameter queryParameter = queryParameterValue.getParameter();
		String key = queryParameter.getQuery().getName() + ":" + queryParameter.getName();
		String value = getQueryParameterValueOverride(key);
		if (value == null) {
			// Booleans May Not necessarily Be Required, but when running always defaults to false
			if (SystemDataType.BOOLEAN.equals(queryParameter.getDataType().getName()) || (queryParameter.isRequired() || QUERY_PARAMETER_REQUIRED_OVERRIDE_SET.contains(key) || QUERY_PARAMETER_REQUIRED_OVERRIDE_SET.contains(queryParameter.getName()))) {
				value = QUERY_PARAMETER_VALUE_DEFAULT_MAP.get(queryParameter.getDataType().getName());
			}
		}
		queryParameterValue.setValue(value);
		queryParameterValue.setText(value);
	}


	private String getQueryParameterValueOverride(String key) {
		return QUERY_PARAMETER_VALUE_OVERRIDE_MAP.get(key);
	}
}
