package com.clifton.test.system.column;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.migrate.schema.MigrationSchemaService;
import com.clifton.core.dataaccess.migrate.schema.Subclass;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.CoreClassUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.system.schema.SystemSchemaService;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnStandard;
import com.clifton.test.BaseIntegrationTest;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;


public class BaseSystemColumnTests extends BaseIntegrationTest {

	private static final Set<String> TABLES_TO_EXCLUDE = CollectionUtils.createHashSet("MigrationModuleVersion");

	@Resource
	MigrationSchemaService migrationSchemaService;

	@Resource
	private SystemSchemaService systemSchemaService;

	@SuppressWarnings("rawtypes")
	@Resource
	private SystemColumnService systemColumnService;


	@SuppressWarnings("unchecked")
	@Test
	public void testAllSystemColumnBeanPropertyNamesValid() {
		List<String> violationList = new ArrayList<>();

		List<Table> tableList = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.migrationSchemaService.getTableList(), 3);

		for (Table table : CollectionUtils.getIterable(tableList)) {
			//Check to see if we can pull off the system table from the migration table name
			if (!this.TABLES_TO_EXCLUDE.contains(table.getName())) {
				SystemTable systemTable = RequestOverrideHandler.serviceCallWithOverriddenMaxDepth(() -> this.systemSchemaService.getSystemTableByName(table.getName()), 3);
				systemTable.setColumnList(RequestOverrideHandler.serviceCallWithDataRootPropertiesFiltering(() -> this.systemColumnService.getSystemColumnStandardListByTable(systemTable.getId()), "id|beanPropertyName"));

				try {
					Class<?> clazz = CoreClassUtils.getClass(table.getDtoClass());

					// Validate entity DTO has all columns defined on the system table
					for (SystemColumnStandard systemColumn : CollectionUtils.getIterable(systemTable.getColumnList())) {
						if (!BeanUtils.isPropertyPresent(clazz, systemColumn.getBeanPropertyName())) {
							// check sub-class properties if any
							boolean present = false;
							for (Subclass subclass : CollectionUtils.getIterable(table.getSubclassList())) {
								Class<?> subClazz = CoreClassUtils.getClass(subclass.getDtoClass());
								if (BeanUtils.isPropertyPresent(subClazz, systemColumn.getBeanPropertyName())) {
									present = true;
									break;
								}
							}
							if (!present) {
								violationList.add("Cannot find field [" + systemColumn.getBeanPropertyName() + "] on entity for table [" + systemTable.getName() + "]");
							}
						}
					}
				}
				catch (Throwable e) {
					violationList.add(e.getMessage());
				}
			}
		}

		ValidationUtils.assertEmpty(violationList, StringUtils.collectionToDelimitedString(violationList, "\n"));
	}
}
