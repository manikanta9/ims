package com.clifton.test;

import com.clifton.security.authorization.setup.SecurityAuthorizationSetupService;
import com.clifton.security.user.SecurityUser;
import com.clifton.security.user.SecurityUserService;
import com.clifton.test.junit.DefaultImsTestAuthenticator;
import com.clifton.test.junit.TestWatcherLogger;
import com.clifton.test.util.UserUtils;
import com.clifton.test.util.templates.ImsTestProperties;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


@ExtendWith({SpringExtension.class, TestWatcherLogger.class, DefaultImsTestAuthenticator.class})
public abstract class BaseIntegrationTest {

	//////////////////////////////////////////////////////////////////
	//						Test Util Classes
	//////////////////////////////////////////////////////////////////


	@Resource
	protected UserUtils userUtils;

	//////////////////////////////////////////////////////////////////
	//						FIX Service Classes
	//////////////////////////////////////////////////////////////////

	@Resource
	protected SecurityUserService securityUserService;

	@Resource
	protected SecurityAuthorizationSetupService securityAuthorizationSetupService;


	//////////////////////////////////////////////////////////////////


	//TODO May want to consider better way of handling this to allow testing of non-admin users
	protected SecurityUser admin;

	//////////////////////////////////////////////////////////////////


	/**
	 * Do not override. This method must be public void and needs to run before every test. If you need additional before logic
	 * define a public void method with a different name annotated with @Before
	 */
	@BeforeEach
	public final void baseBeforeTest() {
		baseBeforeTestImpl();
	}


	protected void baseBeforeTestImpl() {
		if (!ImsTestProperties.TEST_USER_ADMIN.equals(this.userUtils.getCurrentUserName())) {
			this.userUtils.switchUser(ImsTestProperties.TEST_USER_ADMIN, ImsTestProperties.TEST_USER_ADMIN_PASSWORD);
		}

		this.admin = (this.admin != null && SecurityUser.SYSTEM_USER.equals(this.admin.getUserName())) ? this.admin : this.securityUserService.getSecurityUserByName(SecurityUser.SYSTEM_USER);
	}
}
