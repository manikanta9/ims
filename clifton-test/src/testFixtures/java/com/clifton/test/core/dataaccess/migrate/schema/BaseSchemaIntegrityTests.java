package com.clifton.test.core.dataaccess.migrate.schema;

import com.clifton.core.dataaccess.dao.NonPersistentField;
import com.clifton.core.dataaccess.db.DataTypeEncodingTypeNames;
import com.clifton.core.dataaccess.migrate.schema.Column;
import com.clifton.core.dataaccess.migrate.schema.Link;
import com.clifton.core.dataaccess.migrate.schema.MigrationSchemaService;
import com.clifton.core.dataaccess.migrate.schema.Table;
import com.clifton.core.dataaccess.migrate.schema.search.TableSearchForm;
import com.clifton.core.shared.dataaccess.DataTypes;
import com.clifton.core.util.AnnotationUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.ExceptionUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.test.BaseIntegrationTest;
import com.clifton.test.protocol.RequestOverrideHandler;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import javax.annotation.Resource;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * @author NickK
 */
public abstract class BaseSchemaIntegrityTests extends BaseIntegrationTest {

	@Resource
	private SessionFactory sessionFactory;

	@Resource
	private MigrationSchemaService migrationSchemaService;


	@Test
	public void testSchemaIntegrity() {
		TableSearchForm tableSearchForm = new TableSearchForm();
		tableSearchForm.setDataSource("dataSource");
		List<Table> tableList = RequestOverrideHandler.serviceCallWithOverrides(() -> this.migrationSchemaService.getTableList(tableSearchForm),
				new RequestOverrideHandler.RequestParameterOverrides("propertiesToExcludeGlobally", "createUserId|createDate|updatedUserId|updateDate|constraintList")
		);
		List<String> conflictList = new ArrayList<>();
		try (Session session = this.sessionFactory.openSession()) {
			session.doWork(connection -> {
				List<String> sequenceNameList = getSequenceNameList(connection);
				DatabaseMetaData databaseMetaData = connection.getMetaData();
				CollectionUtils.getIterable(tableList).forEach(table -> {
					if (table.isBatchEnabled() && !sequenceNameList.contains(table.getName() + "Sequence")) {
						conflictList.add("Sequence is missing for the batch enabled table " + table.getName());
					}
					Map<String, Column> schemaColumnMap = CollectionUtils.getStream(table.getColumnList(true)).collect(Collectors.toMap(Column::getName, Function.identity()));
					try {
						Collection<DatabaseColumn> databaseColumnCollection = getDatabaseTableColumnCollection(databaseMetaData, table.getName());
						CollectionUtils.getStream(databaseColumnCollection).forEach(databaseColumn -> validateTableColumn(table, schemaColumnMap.get(databaseColumn.name), databaseColumn, conflictList));
						findOrphanedFields(table, conflictList);
					}
					catch (SQLException e) {
						throw new RuntimeException("Failed to get columns for table: " + table.getName() + ": " + ExceptionUtils.getOriginalMessage(e));
					}
				});
			});
		}
		if (!conflictList.isEmpty()) {
			Assertions.fail("Identified " + conflictList.size() + " issues:\n" + StringUtils.join(conflictList, Function.identity(), "\n"));
		}
	}


	private List<String> getSequenceNameList(Connection connection) {
		List<String> results = new ArrayList<>();
		try (Statement statement = connection.createStatement()) {
			boolean successful = statement.execute("SELECT SEQUENCE_NAME FROM INFORMATION_SCHEMA.SEQUENCES");
			if (!successful) {
				throw new RuntimeException("Failure querying for sequences");
			}
			try (ResultSet resultSet = statement.getResultSet()) {
				while (resultSet.next()) {
					results.add(resultSet.getString("SEQUENCE_NAME"));
				}
			}
		}
		catch (SQLException e) {
			throw new RuntimeException("Could not query for sequences", e);
		}
		return results;
	}


	private void findOrphanedFields(Table table, List<String> conflictList) {
		try {
			Set<String> beanPropertyNameSet = CollectionUtils.getStream(table.getColumnList(true)).map(Column::getBeanPropertyName).collect(Collectors.toSet());
			beanPropertyNameSet.addAll(CollectionUtils.getStream(table.getLinkList()).map(Link::getName).collect(Collectors.toSet()));
			Field[] fields = Class.forName(table.getDtoClass()).getDeclaredFields();
			for (Field field : fields) {
				if (processField(field)) {
					if (AnnotationUtils.isAnnotationPresent(field, NonPersistentField.class) && beanPropertyNameSet.contains(field.getName())) {
						conflictList.add("Class [" + table.getDtoClass() + "] contains field [" + field.getName() + "], but it exists in the database, and is annotated with 'NonPersistentField'.  Verify the annotation is correct.");
					}
					else if (!AnnotationUtils.isAnnotationPresent(field, NonPersistentField.class) && !beanPropertyNameSet.contains(field.getName())) {
						conflictList.add("Class [" + table.getDtoClass() + "] contains field [" + field.getName() + "], but it is not annotated with 'NonPersistentField', and does not exist in the database.");
					}
				}
			}
		}
		catch (Exception e) {
			conflictList.add(e.getMessage());
		}
	}


	private boolean processField(Field field) {
		int fieldModifierMask = field.getModifiers();
		if (Modifier.isStatic(fieldModifierMask) || Modifier.isFinal(fieldModifierMask) ||
				Modifier.isAbstract(fieldModifierMask) || Modifier.isInterface(fieldModifierMask) ||
				Collection.class.isAssignableFrom(field.getType())) {
			return false;
		}
		return true;
	}


	private Collection<DatabaseColumn> getDatabaseTableColumnCollection(DatabaseMetaData databaseMetaData, String tableName) throws SQLException {
		Set<DatabaseColumn> databaseColumnSet = new HashSet<>();
		ResultSet columns = databaseMetaData.getColumns(null, null, tableName, null);
		while (columns.next()) {
			databaseColumnSet.add(new DatabaseColumn(columns));
		}
		return databaseColumnSet;
	}


	private void validateTableColumn(Table table, Column column, DatabaseColumn databaseColumn, List<String> conflictList) {
		String tableName = table.getName();
		if (column == null) {
			conflictList.add("Column missing in schema [Table: " + tableName
					+ ", Column: " + databaseColumn.name + "]");
			return;
		}
		boolean databaseIdentity = databaseColumn.typeName != null && databaseColumn.typeName.contains("identity");
		String typeNameFragment = "NA";
		boolean validateLength = false;
		int length = 0;
		int precision = 0;
		switch (column.getDataType()) {
			case IDENTITY:
			case NATURAL_ID:
			case TIME:
			case INT: {
				typeNameFragment = "int";
				break;
			}
			case IDENTITY_LONG:
			case NATURAL_ID_LONG:
			case LONG: {
				typeNameFragment = "bigint";
				break;
			}
			case IDENTITY_SHORT:
			case NATURAL_ID_SHORT:
			case SHORT: {
				typeNameFragment = "smallint";
				break;
			}
			case IDENTITY_TINY:
			case NATURAL_ID_TINY:
			case TINY: {
				typeNameFragment = "tinyint";
				break;
			}
			case MONEY:
			case MONEY4:
			case DECIMAL_PRECISE:
			case PRICE:
			case QUANTITY:
			case EXCHANGE_RATE:
			case PERCENT:
			case PERCENT_PRECISE: {
				typeNameFragment = "decimal";
				validateLength = true;
				length = column.getDataType().getLength();
				precision = column.getDataType().getPrecision();
				break;
			}
			case NAME:
			case NAME_LONG:
			case DESCRIPTION:
			case DESCRIPTION_LONG:
			case STRING3:
			case STRING10:
			case TEXT:
			case CUSTOM_JSON_STRING:
			case ENUM: {
				typeNameFragment = column.getDataTypeEncodingType() == DataTypeEncodingTypeNames.UTF_8 ? "varchar" : "nvarchar";
				validateLength = true;
				length = column.getDataType().getLength();
				break;
			}
			case BIT: {
				typeNameFragment = "bit";
				break;
			}
			case DATE: {
				typeNameFragment = "date";
				break;
			}
			case DATETIME: {
				typeNameFragment = "datetime";
				break;
			}
			case ROWVERSION: {
				typeNameFragment = "timestamp";
				break;
			}
			case SECRET:
			case SECRET_SHORT:
			case SECRET_MAX:
			case COMPRESSED_TEXT: {
				typeNameFragment = "varbinary";
				validateLength = true;
				length = column.getDataType() == DataTypes.SECRET_MAX || column.getDataType() == DataTypes.COMPRESSED_TEXT ? Integer.MAX_VALUE : column.getDataType().getLength();
				break;
			}
		}
		typeNameFragment = typeNameFragment + (databaseIdentity ? " identity" : "");
		if (databaseIdentity != column.getDataType().isIdentity() && !table.isBatchEnabled()) {
			conflictList.add(createMismatchMessage(tableName, column.getName(), "identity", column.getDataType().isIdentity(), databaseIdentity));
		}
		if (databaseColumn.typeName == null || !databaseColumn.typeName.equals(typeNameFragment)) {
			conflictList.add(createMismatchMessage(tableName, column.getName(), "type", typeNameFragment, databaseColumn.typeName));
		}
		if (validateLength && !column.isCalculatedColumn()) {
			if (length != databaseColumn.length) {
				conflictList.add(createMismatchMessage(tableName, column.getName(), "length", column.getDataType().getLength(), databaseColumn.length));
			}
			if (precision != databaseColumn.precision) {
				conflictList.add(createMismatchMessage(tableName, column.getName(), "precision", column.getDataType().getPrecision(), databaseColumn.precision));
			}
		}
		// Note: Doesn't seem that encoding/collation is readily available, so assume that varchar = utf8 and nvarchar = unicode (CHARACTER_SET_NAME on INFORMATION_SCHEMA.COLUMNS) would have utf8 but that's isn't available on the current object
		boolean schemaRequired = column.getColumnRequired() == null ? true : column.getColumnRequired();
		if (databaseColumn.required != schemaRequired) {
			conflictList.add(createMismatchMessage(tableName, column.getName(), "required", schemaRequired, databaseColumn.required));
		}
	}


	private String createMismatchMessage(String tableName, String columnName, String property, Object schemaValue, Object databaseValue) {
		return "Column mismatch [Table: " + tableName + ", Column: " + columnName + ", Property: " + property + ", Schema: " + schemaValue + ", Database: " + databaseValue + "]";
	}


	private static final class DatabaseColumn {

		String name;
		String typeName;
		int precision;
		int length;
		boolean required;


		DatabaseColumn(ResultSet tableColumnData) throws SQLException {
			this.name = tableColumnData.getString("COLUMN_NAME");
			this.typeName = tableColumnData.getString("TYPE_NAME");
			this.length = tableColumnData.getInt("COLUMN_SIZE");
			this.precision = tableColumnData.getInt("DECIMAL_DIGITS");
			this.required = "NO".equals(tableColumnData.getString("IS_NULLABLE"));
		}
	}
}
