dependencies {
	///////////////////////////////////////////////////////////////////////////
	/////////////            External Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api("com.google.guava:guava:19.0")
	api("com.google.code.gson:gson")
	api("javax.inject:javax.inject:1")

	// JUnit 5
	api("org.junit.jupiter:junit-jupiter-api")
	api("org.junit.jupiter:junit-jupiter-engine")
	api("org.junit.jupiter:junit-jupiter-params")
	api("org.junit.platform:junit-platform-engine")
	api("org.junit.platform:junit-platform-launcher")
	api("org.mockito:mockito-junit-jupiter")
	api("org.hamcrest:hamcrest")

	api("org.springframework:spring-test")

	///////////////////////////////////////////////////////////////////////////
	/////////////            Internal Dependencies               //////////////
	///////////////////////////////////////////////////////////////////////////

	api(project(":clifton-core"))
	api(project(":clifton-security"))
	api(project(":clifton-system"))
	api(project(":clifton-system:clifton-system-query"))

	///////////////////////////////////////////////////////////////////////////
	/////////////              Test Dependencies                 //////////////
	///////////////////////////////////////////////////////////////////////////

	testFixturesApi(testFixtures(project(":clifton-core")))
}
