@file:Suppress("UnstableApiUsage")

import com.clifton.gradle.plugin.build.CliftonJavaExtension
import com.clifton.gradle.plugin.util.buildConfig
import org.springframework.boot.gradle.plugin.SpringBootPlugin

/*
 * The majority of our logic is implemented with buildSrc plugins. These are modular, allow for strict typing (with predetermined receivers), and allow for full language features
 * to be used, including resolving all imports, even build-source/plugin imports, at compile-time, preventing "buildScript" blocks which would be required in each script.
 *
 * See the "buildSrc" project (default name, not currently configurable) for more details.
 */
plugins {
	id("com.clifton.gradle.main")
}

// Configure build scan details
cliftonBuildScan {
	tag("Kotlin Build Script")
	finalize()
}

allprojects {
	// Apply repository settings to all projects
	apply(from = "${project.buildConfig.fullBuildRootPath}/shared-with-buildSrc/repo.gradle.kts")
}

subprojects {
	// Address https://github.com/gradle/gradle/issues/4823: Force parent project evaluation before sub-project evaluation for Kotlin build scripts
	if (gradle.startParameter.isConfigureOnDemand
			&& buildscript.sourceFile?.extension?.toLowerCase() == "kts"
			&& parent != rootProject) {
		generateSequence(parent) { project -> project.parent.takeIf { it != rootProject } }
				.forEach { evaluationDependsOn(it.path) }
	}

	// Use the Spring BOM for automatic consistent and compatible versioning
	configure<CliftonJavaExtension> {
		// Spring platform BOM
		bomList.add(SpringBootPlugin.BOM_COORDINATES)
		/*
		 * JUnit 5.6.3 is required to fix a bug in the OMS integration tests. The generated OpenAPI server library pulls Jackson version 2.12.2, which pulls JUnit 4.13.1. This
		 * causes a parsing error with JUnit 5.6.2 (https://github.com/junit-team/junit5/issues/2529).
		 */
		bomList.add("org.junit:junit-bom:5.6.3")
		// Upgrade Log4j above the Spring BOM version to remove vulnerabilities to CVE-2021-44228 (Log4Shell)
		bomList.add("org.apache.logging.log4j:log4j-bom:2.17.1")
	}

	// Override specific versions selected by the Spring BOM during dependency resolution
	configurations.all {
		resolutionStrategy.eachDependency {
			if (requested.group.startsWith("org.elasticsearch") && requested.name !in arrayOf("jna", "securesm")) {
				useVersion("5.6.2")
				because("Our ElasticSearch application is currently pinned at 5.6.16. We cannot upgrade the ElasticSearch library without also upgrading the ElasticSearch application. Upgrading the application is desired and is a backlog item.")
			}
		}
	}

	// Add dependency constraints
	dependencies {
		constraints {
			configurations.matching { !it.isCanBeConsumed && !it.isCanBeResolved }.configureEach {
				// Guard against Log4Shell and related issues (via https://twitter.com/CedricChampeau/status/1469608906196410368?s=20)
				add(name, "org.apache.logging.log4j:log4j-core") {
					version { reject("[2.0.+, 2.15)") }
					because("CVE-2021-44228: Log4j is vulnerable to remote code execution (Log4Shell)")
				}
				add(name, "org.apache.logging.log4j:log4j-core") {
					version { reject("[2.0.+, 2.16)") }
					because("CVE-2021-45046: Log4j is vulnerable to DOS and local code execution")
				}
				add(name, "org.apache.logging.log4j:log4j-core") {
					version { reject("[2.0.+, 2.17)") }
					because("CVE-2021-45105: Log4j is vulnerable to DOS via infinite recursion")
				}
				add(name, "org.apache.logging.log4j:log4j-core") {
					version { reject("[2.0.+, 2.17.1)") }
					because("CVE-2021-44832: Log4j is vulnerable to RCE via logging configuration file modification")
				}
			}
		}
	}
}
