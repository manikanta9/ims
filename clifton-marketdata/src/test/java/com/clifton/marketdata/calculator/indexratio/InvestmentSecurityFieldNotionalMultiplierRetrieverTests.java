package com.clifton.marketdata.calculator.indexratio;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Tests the InvestmentSecurityFieldNotionalMultiplierRetriever in both CUSTOM and MARKETDATA modes via instantiation and the MarketDataService methods.
 *
 * @author davidi
 */
@ContextConfiguration("NotionalMultiplierRetrieverTests-context.xml")
public class InvestmentSecurityFieldNotionalMultiplierRetrieverTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	MarketDataService marketDataService;

	@Resource
	SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////

	@Test
	public void testRetrieveNotionalMultiplier_CustomField_FV_Value_Exists() {
		Date testDate = DateUtils.toDate("08/31/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedValue = BigDecimal.valueOf(1.78208384586509);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("ZCSBRL-20200608-20290102-7", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - FV Notional Multiplier");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureRetriever(retriever, false);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_CustomField_FV_Value_Not_Exists_ExceptionIfMissing_False() {
		Date testDate = DateUtils.toDate("08/31/2021", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("ZCSBRL-20180222-20270104-9.845", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - FV Notional Multiplier");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureRetriever(retriever, false);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertNull(notionalMultiplier);
	}


	@Test
	public void testRetrieveNotionalMultiplier_CustomField_FV_Value_Not_Exists_ExceptionIfMissing_True() {
		Date testDate = DateUtils.toDate("08/31/2021", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("ZCSBRL-20180222-20270104-9.845", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - FV Notional Multiplier");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureRetriever(retriever, false);
		Assertions.assertThrows(ValidationException.class, () -> retriever.retrieveNotionalMultiplier(security, () -> testDate, true));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Exists() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedValue = BigDecimal.valueOf(1.16676);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Security Market Data Index Ratio");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureRetriever(retriever, true);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Exists_Using_MarketDataService1() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedValue = BigDecimal.valueOf(1.16676);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		BigDecimal notionalMultiplier = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Exists_Using_MarketDataService2() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedValue = BigDecimal.valueOf(1.16676);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		BigDecimal notionalMultiplier = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate);

		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Not_Exists_ExceptionIfMissing_False() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BDX8CX86", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Security Market Data Index Ratio");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureRetriever(retriever, true);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertNull(notionalMultiplier);
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Not_Exists_ExceptionIfMissing_True() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BDX8CX86", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Security Market Data Index Ratio");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureRetriever(retriever, true);
		Assertions.assertThrows(ValidationException.class, () -> retriever.retrieveNotionalMultiplier(security, () -> testDate, true));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Not_Exists_ExceptionIfMissing_False_Using_MarketDataService() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedValue = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BDX8CX86", false);
		BigDecimal notionalMultiplier = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate);

		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketData_IndexRatio_Value_Not_Exists_ExceptionIfMissing_True_Using_MarketDataService() {
		Date testDate = DateUtils.toDate("08/30/2021", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BDX8CX86", false);
		Assertions.assertThrows(ValidationException.class, () -> this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureRetriever(NotionalMultiplierRetriever retriever, boolean isUseMarketDataField) {
		if (isUseMarketDataField) {
			((InvestmentSecurityFieldNotionalMultiplierRetriever)retriever).setFieldType(InvestmentSecurityFieldNotionalMultiplierRetriever.FieldTypes.MARKETDATA);
			((InvestmentSecurityFieldNotionalMultiplierRetriever)retriever).setNotionalMultiplierFieldName("Index Ratio - CPI");
		}
		else {
			((InvestmentSecurityFieldNotionalMultiplierRetriever)retriever).setFieldType(InvestmentSecurityFieldNotionalMultiplierRetriever.FieldTypes.CUSTOM);
			((InvestmentSecurityFieldNotionalMultiplierRetriever)retriever).setNotionalMultiplierFieldName("FV Notional Multiplier");
		}
	}
}
