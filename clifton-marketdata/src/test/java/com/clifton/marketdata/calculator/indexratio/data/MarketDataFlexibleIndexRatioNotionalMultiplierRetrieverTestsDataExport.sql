/*
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId FROM InvestmentSecurity WHERE  Symbol IN ('91282CCA7',  '912796C56', 'GB0008932666', 'GB00BD9MZZ71', 'CDU1', 'CPIRAP21', 'UKRPI')
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue WHERE SystemColumnID IN (SELECT SystemColumnID FROM SystemColumn WHERE SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurity'))
                                                                                                        AND FKFieldID IN (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE  Symbol IN ('91282CCA7','GB0008932666', 'GB00BD9MZZ71', '912796C56'))
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'CPIRAP21') AND MeasureDate BETWEEN '06/01/2021' AND '08/01/2021'
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'UKRPI') AND MeasureDate BETWEEN '10/01/2015' AND '04/01/2016'
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'UKRPI') AND MeasureDate BETWEEN '01/01/2021' AND '08/01/2021'
UNION
SELECT 'InvestmentSecurityEvent' AS entityTableName, InvestmentSecurityEventID as entityId FROM InvestmentSecurityEvent WHERE InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'GB0008932666') AND EventDate BETWEEN '01/01/2021' AND '04/01/2021'
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID as entityId FROM SystemBeanProperty WHERE SystemBeanPropertyTypeID IN (SELECT SystemBeanPropertyTypeID FROM SystemBeanPropertyType WHERE SystemBeanPropertyTypeName IN ('Index Ratio Calculation Method','Linked Security Field','Linked Security Market Data Field','Secondary Index Ratio Calculation Method','Secondary Linked Security Field','Secondary Linked Security Market Data Field'))
*/
SELECT TOP 0 NULL
;

