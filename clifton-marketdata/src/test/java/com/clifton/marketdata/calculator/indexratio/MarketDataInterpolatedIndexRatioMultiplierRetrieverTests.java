package com.clifton.marketdata.calculator.indexratio;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Tests the InterpolatedMarketDataInterpolatedIndexRatioNotionalMultiplierRetriever via instantiation and the MarketDataService methods.
 *
 * @author davidi
 */
@ContextConfiguration("NotionalMultiplierRetrieverTests-context.xml")
public class MarketDataInterpolatedIndexRatioMultiplierRetrieverTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	MarketDataService marketDataService;

	@Resource
	SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testRetrieveNotionalMultiplier_NonInflationAdjustedSecurity_Bond() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912796C56", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true);

		Assertions.assertEquals(expectedIndexRatio, indexRatio);
	}


	@Test
	public void testRetrieveNotionalMultiplier_NonInflationAdjustedSecurity_Future() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("CDU1", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true);

		Assertions.assertEquals(expectedIndexRatio, indexRatio);
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_calculator_not_null() {

		// Note:  expected base RPI on 02/24/2016: 260.43448
		//        expected reference  RPI on 6/17/2021: 299.14
		//        expected IndexRatio on 6/17/2021: 1.14862

		Date testDate = DateUtils.toDate("06/17/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.14862);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Interpolated Index Ratio");

		MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever calculator = (MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);
		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_calculator_not_null_country_codes_list_null() {

		// Note:  expected base RPI on 02/24/2016: 260.43448
		//        expected reference  RPI on 6/17/2021: 299.14
		//        expected IndexRatio on 6/17/2021: 1.14862

		Date testDate = DateUtils.toDate("06/17/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.14862);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Interpolated Index Ratio");

		MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever calculator = (MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);
		calculator.setCountriesUsingQuarterlyReportingPeriods(null);
		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_calculator_not_null_with_default_configuration() {

		// Note:  expected base RPI on 02/24/2016: 260.43448
		//        expected reference  RPI on 6/17/2021: 299.14
		//        expected IndexRatio on 6/17/2021: 1.14862

		Date testDate = DateUtils.toDate("06/17/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.14862);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Interpolated Index Ratio");

		MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever calculator = (MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_index_ratio_not_found_exception_disabled() {

		Date testDate = DateUtils.toDate("07/08/2014", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Interpolated Index Ratio");

		MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever calculator = (MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);
		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_index_ratio_not_found_exception_enabled() {

		Date testDate = DateUtils.toDate("07/08/2014", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Interpolated Index Ratio");

		MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever calculator = (MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);
		Assertions.assertThrows(ValidationException.class, () -> calculator.retrieveNotionalMultiplier(security, () -> testDate, true));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_marketDataService() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.15684);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_Month_Start_After_Previous_Coupon_07_08_21() {

		// Note:  expected base RPI: 135.1
		//        expected reference  RPI on for Feb 2021: 296.0
		//        expected IndexRatio on 2/1/2021: 2.19096965

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(2.19096965);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB0008932666", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_marketDataService_australia_bond() {
		// For this security, we go back 3 quarters or 9 months for the RPI data
		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.45484);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00B24FFM16", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testRetrieveNotionalMultiplier_InflationAdjustedSecurity_marketDataService_new_zealand_bond() {
		// For this security, we go back 3 quarters or 9 months for the RPI data
		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.51007);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00B1L6W962", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, true);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureCalculatorDefault(NotionalMultiplierRetriever calculator) {
		((MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) calculator).setLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INFLATION_INDEX);
		((MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) calculator).setLinkedSecurityMarketDataField(MarketDataField.FIELD_INDEX_RATIO);
		((MarketDataInterpolatedIndexRatioNotionalMultiplierRetriever) calculator).setCountriesUsingQuarterlyReportingPeriods(CollectionUtils.createList("AU", "NZ"));
	}
}
