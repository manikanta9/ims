SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId
FROM InvestmentSecurity
WHERE Symbol IN
      ('MLDUSRVB', 'BNPJDM01')
UNION
SELECT 'SystemColumn' AS entityTableName, SystemColumnID AS entityId
FROM SystemColumn
WHERE SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurity')
UNION
SELECT 'MarketDataField' AS entityTableName, MarketDataFieldID as entityId
FROM MarketDataField
WHERE DataFieldName IN ('Index Divisor')
UNION
SELECT 'SystemBean' AS entityTableName, SystemBeanID as entityId
FROM SystemBean
WHERE SystemBeanTypeID = (SELECT SystemBeanTypeID
                          FROM SystemBeanType
                          WHERE SystemBeanTypeName = 'Investment Security Field Notional Multiplier Retriever')
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID as entityId
FROM SystemBeanProperty
WHERE SystemBeanPropertyTypeID IN (SELECT SystemBeanPropertyTypeID
                                   FROM SystemBeanPropertyType
                                   WHERE SystemBeanPropertyTypeName IN ('Field Type', 'Notional Multiplier Field'))
UNION
SELECT 'MarketDataSource' AS entityTableName, MarketDataSourceID AS entityId
FROM MarketDataSource
WHERE DataSourceName = 'Parametric Clifton'
;
