package com.clifton.marketdata.calculator.indexratio;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.MarketDataService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import com.clifton.system.schema.column.value.SystemColumnDatedValue;
import com.clifton.system.schema.column.value.SystemColumnValueService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * @author mitchellf
 */
@ContextConfiguration("NotionalMultiplierRetrieverTests-context.xml")
public class InvestmentSecurityFieldNotionalMultiplierRetrieverDatedValueTests extends BaseInMemoryDatabaseTests {

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	MarketDataService marketDataService;

	@Resource
	SystemBeanService systemBeanService;

	@Resource
	SystemColumnService systemColumnService;

	@Resource
	SystemColumnValueService systemColumnValueService;

	@Resource
	MarketDataFieldService marketDataFieldService;

	@Resource
	MarketDataSourceService marketDataSourceService;

	public static final String CUSTOM_DATED_SECURITY = "MLDUSRVB";
	public static final String MARKET_DATA_SECURITY = "BNPJDM01";
	/*
	Security MLDUSRVB will use dated custom column
	Security BNPJDM01 will use market data lookup
	 */


	@Test
	public void testRetrieveNotionalMultiplier_CustomFieldDated_IndexDivisor_Exists() {

		setupDatedValue("0.5", "09/30/2021", "10/01/2021");
		setupDatedValue("0.75", "10/02/2021", null);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(CUSTOM_DATED_SECURITY, false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Dated Index Divisor");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);

		Date testDate = DateUtils.toDate("09/30/2021", DateUtils.DATE_FORMAT_INPUT);
		BigDecimal expectedValue = BigDecimal.valueOf(0.5);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, true);
		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));

		Date testDate2 = DateUtils.toDate("10/02/2021", DateUtils.DATE_FORMAT_INPUT);
		expectedValue = BigDecimal.valueOf(0.75);
		notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate2, true);
		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_CustomFieldDated_IndexDivisor_DoesNotExist() {

		setupDatedValue("0.5", "09/30/2021", "10/01/2021");
		setupDatedValue("0.75", "10/03/2021", null);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(CUSTOM_DATED_SECURITY, false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Dated Index Divisor");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);

		Date testDate = DateUtils.toDate("09/29/2021", DateUtils.DATE_FORMAT_INPUT);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, true);
		Assertions.assertEquals(notionalMultiplier, BigDecimal.ONE);

		Date testDate2 = DateUtils.toDate("10/02/2021", DateUtils.DATE_FORMAT_INPUT);
		notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate2, true);
		Assertions.assertEquals(notionalMultiplier, BigDecimal.ONE);
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketDataFlexible_IndexDivisor_Exists() {
		setupMarketDataValue("0.5", "09/30/2021");
		setupMarketDataValue("0.75", "10/02/2021");

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(MARKET_DATA_SECURITY, false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Market Data Index Divisor");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		Date testDate = DateUtils.toDate("10/01/2021", DateUtils.DATE_FORMAT_INPUT);
		BigDecimal expectedValue = BigDecimal.valueOf(0.5);
		BigDecimal notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate, true);
		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));

		Date testDate2 = DateUtils.toDate("10/03/2021", DateUtils.DATE_FORMAT_INPUT);
		expectedValue = BigDecimal.valueOf(0.75);
		notionalMultiplier = retriever.retrieveNotionalMultiplier(security, () -> testDate2, true);
		Assertions.assertTrue(MathUtils.isEqual(expectedValue, notionalMultiplier));
	}


	@Test
	public void testRetrieveNotionalMultiplier_MarketDataFlexible_IndexDivisor_DoesNotExist() {
		setupMarketDataValue("0.5", "09/30/2021");
		setupMarketDataValue("0.75", "10/02/2021");

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(MARKET_DATA_SECURITY, false);
		SystemBean calcBean = this.systemBeanService.getSystemBeanByName("Notional Multiplier Retriever - Market Data Index Divisor");

		InvestmentSecurityFieldNotionalMultiplierRetriever retriever = (InvestmentSecurityFieldNotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		Date testDate = DateUtils.toDate("09/29/2021", DateUtils.DATE_FORMAT_INPUT);
		Assertions.assertThrows(ValidationException.class, () -> retriever.retrieveNotionalMultiplier(security, () -> testDate, true));
	}


	private void setupDatedValue(String value, String startDate, String endDate) {
		SystemColumnSearchForm sf = new SystemColumnSearchForm();
		sf.setColumnGroupName("Dated Security Fields");
		sf.setName("Index Divisor");
		SystemColumnCustom column = CollectionUtils.getOnlyElement(this.systemColumnService.getSystemColumnCustomList(sf));

		SystemColumnDatedValue columnValue = new SystemColumnDatedValue();
		columnValue.setColumn(column);
		columnValue.setValue(value);
		columnValue.setStartDate(DateUtils.toDate(startDate));
		if (endDate != null) {
			columnValue.setEndDate(DateUtils.toDate(endDate));
		}

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(CUSTOM_DATED_SECURITY, false);
		columnValue.setFkFieldId(security.getId());

		this.systemColumnValueService.saveSystemColumnDatedValue(columnValue);
	}


	private void setupMarketDataValue(String value, String date) {
		MarketDataSource dataSource = this.marketDataSourceService.getMarketDataSourceByName("Parametric Clifton");
		MarketDataField dataField = this.marketDataFieldService.getMarketDataFieldByName("Index Divisor");
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol(MARKET_DATA_SECURITY, false);

		MarketDataValue dataValue = new MarketDataValue();
		dataValue.setDataField(dataField);
		dataValue.setDataSource(dataSource);
		dataValue.setInvestmentSecurity(security);
		dataValue.setMeasureDate(DateUtils.toDate(date));
		dataValue.setMeasureValue(new BigDecimal(value));

		this.marketDataFieldService.saveMarketDataValue(dataValue);
	}
}
