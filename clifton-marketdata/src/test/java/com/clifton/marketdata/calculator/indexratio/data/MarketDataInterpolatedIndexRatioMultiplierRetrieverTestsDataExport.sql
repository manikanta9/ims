-- Tests use the additional data for testing, as it has some minor data changes / corrections required by the tests.  Overall test data can be regenerated  by uncommenting the SQL below.
SELECT TOP 0 NULL;
/*
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId FROM InvestmentSecurity WHERE  Symbol IN ('GB00BD9MZZ71',  '912796C56', 'GB0008932666', 'GB00B24FFM16', 'GB00B1L6W962', 'CDU1', 'CPIRAP21', 'UKRPI')
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue WHERE SystemColumnID IN (SELECT SystemColumnID FROM SystemColumn WHERE SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurity'))
                                                                                                        AND FKFieldID IN (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE  Symbol IN ('GB00BD9MZZ71','GB0008932666', '912796C56', 'GB00B24FFM16', 'GB00B1L6W962'))
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'UKRPI') AND MeasureDate BETWEEN '01/01/2020' AND '08/01/2021'
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'UKRPI') AND MeasureDate BETWEEN '01/01/2015' AND '02/01/2016'
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'UKRPI') AND MeasureDate BETWEEN '03/01/2016' AND '06/01/2016'
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Value') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'UKRPI') AND MeasureDate BETWEEN '01/01/2006' AND '12/31/2007'
UNION
SELECT 'InvestmentSecurityEvent' AS entityTableName, InvestmentSecurityEventID as entityId FROM InvestmentSecurityEvent WHERE InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'GB0008932666') AND EventDate BETWEEN '01/01/2021' AND '04/01/2021'
UNION
SELECT 'SystemBean' AS entityTableName, SystemBeanID as entityID FROM SystemBean WHERE SystemBeanID = (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName = 'Market Data Interpolated Index Ratio Notional Multiplier Retriever')
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID as entityId FROM SystemBeanProperty WHERE SystemBeanPropertyTypeID IN (SELECT SystemBeanPropertyTypeID FROM SystemBeanPropertyType WHERE SystemBeanPropertyTypeName IN ('Linked Security Field', 'Linked Security Market Data Field', 'Countries Using Quarterly Reporting Periods'))
;
*/

