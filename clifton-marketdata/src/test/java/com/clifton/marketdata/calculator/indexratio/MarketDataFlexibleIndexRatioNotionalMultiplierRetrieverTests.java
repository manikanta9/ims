package com.clifton.marketdata.calculator.indexratio;

import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.marketdata.MarketDataService;
import com.clifton.system.bean.SystemBean;
import com.clifton.system.bean.SystemBeanService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Tests the FlexibleNotionalMultiplierRetriever via instantiation and the MarketDataService methods
 *
 * @author davidi
 */
@ContextConfiguration("NotionalMultiplierRetrieverTests-context.xml")
public class MarketDataFlexibleIndexRatioNotionalMultiplierRetrieverTests extends BaseInMemoryDatabaseTests {

	private final static String MARKET_DATA_VALUE_FIELD = "Value";

	@Resource
	InvestmentInstrumentService investmentInstrumentService;

	@Resource
	MarketDataService marketDataService;

	@Resource
	SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testNonIndexRatioAdjustedSecurity_Bond() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("912796C56", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, false);

		Assertions.assertEquals(expectedIndexRatio, indexRatio);
	}


	@Test
	public void testNonIndexRatioAdjustedSecurity_Future() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("CDU1", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, false);

		Assertions.assertEquals(expectedIndexRatio, indexRatio);
	}


	@Test
	public void testIndexRatioAdjustedSecurity_calculator_not_null() {

		final Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);

		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.02016);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);

		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}

	@Test
	public void testIndexRatioAdjustedSecurity_calculator_not_null_calculation_method_order_reversed() {

		final Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);

		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.02016);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorReversed(calculator);

		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testIndexRatioAdjustedSecurity_calculator_not_null_calculation_field_value_retrieval_method_only() {

		final Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);

		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.02016);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorFieldValueRetrieval(calculator);

		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testIndexRatioAdjustedSecurity_calculator_not_null_calculation_inflation_index_based_method_only() {

		final Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);

		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorInflationIndexBased(calculator);

		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}



	@Test
	public void testIndexRatioAdjustedSecurity_index_ratio_not_found_exception_disabled() {

		Date testDate = DateUtils.toDate("07/08/2014", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.ONE;

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);

		BigDecimal indexRatio = calculator.retrieveNotionalMultiplier(security, () -> testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testIndexRatioAdjustedSecurity_index_ratio_not_found_exception_enabled() {

		Date testDate = DateUtils.toDate("07/08/2014", DateUtils.DATE_FORMAT_INPUT);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		configureCalculatorDefault(calculator);
		Assertions.assertThrows(ValidationException.class, () -> calculator.retrieveNotionalMultiplier(security, () -> testDate, true));
	}


	@Test
	public void testIndexRatioAdjustedSecurity() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.02016);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testIndexRatioAdjustedSecurity_marketData_method2() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.02016);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testIndexRatioAdjustedSecurity_Month_Start_After_Previous_Coupon_07_08_21() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(2.18060696);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB0008932666", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testIndexRatioAdjustedSecurity_base_inflation_value_lookup() {

		Date testDate = DateUtils.toDate("07/08/2021", DateUtils.DATE_FORMAT_INPUT);
		final BigDecimal expectedIndexRatio = BigDecimal.valueOf(1.16728);

		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GB00BD9MZZ71", false);
		BigDecimal indexRatio = this.marketDataService.getMarketDataIndexRatioForSecurity(security.getId(), testDate, false);

		Assertions.assertTrue(MathUtils.isEqual(expectedIndexRatio, indexRatio));
	}


	@Test
	public void testValidate_no_primary_calculation_method() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever) calculator).setPrimaryIndexRatioCalculationMethod(null);
		Assertions.assertThrows(ValidationException.class, ((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever) calculator)::validate);
	}


	@Test
	public void testValidate_primary_secondary_calculation_methods_are_same() {
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("91282CCA7", false);
		SystemBean calcBean = security.getInstrument().getHierarchy().getNotionalMultiplierRetriever();

		NotionalMultiplierRetriever calculator = (NotionalMultiplierRetriever) this.systemBeanService.getBeanInstance(calcBean);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever) calculator).setSecondaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.FIELD_VALUE_RETRIEVAL);
		Assertions.assertThrows(ValidationException.class, ((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever) calculator)::validate);
	}


	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	private void configureCalculatorDefault(NotionalMultiplierRetriever calculator) {
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.FIELD_VALUE_RETRIEVAL);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INDEX_RATIO);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityMarketDataField(MARKET_DATA_VALUE_FIELD);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.INFLATION_INDEX_BASED);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INFLATION_INDEX);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityMarketDataField(MARKET_DATA_VALUE_FIELD);
	}


	private void configureCalculatorReversed(NotionalMultiplierRetriever calculator) {
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.INFLATION_INDEX_BASED);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INFLATION_INDEX);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityMarketDataField(MARKET_DATA_VALUE_FIELD);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.FIELD_VALUE_RETRIEVAL);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INDEX_RATIO);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityMarketDataField(MARKET_DATA_VALUE_FIELD);
	}


	private void configureCalculatorFieldValueRetrieval(NotionalMultiplierRetriever calculator) {
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.FIELD_VALUE_RETRIEVAL);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INDEX_RATIO);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityMarketDataField(MARKET_DATA_VALUE_FIELD);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryIndexRatioCalculationMethod(null);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityField(null);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityMarketDataField(null);
	}


	private void configureCalculatorInflationIndexBased(NotionalMultiplierRetriever calculator) {
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryIndexRatioCalculationMethod(MarketDataFlexibleIndexRatioNotionalMultiplierRetriever.IndexRatioCalculationMethods.INFLATION_INDEX_BASED);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityField(InvestmentSecurity.CUSTOM_FIELD_INFLATION_INDEX);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setPrimaryLinkedSecurityMarketDataField(MARKET_DATA_VALUE_FIELD);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryIndexRatioCalculationMethod(null);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityField(null);
		((MarketDataFlexibleIndexRatioNotionalMultiplierRetriever)calculator).setSecondaryLinkedSecurityMarketDataField(null);
	}


}
