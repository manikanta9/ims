-- Tests use the additional data for testing, to preserve current data.  Overall test data can be regenerated  by uncommenting the SQL below.
SELECT TOP 0 NULL;
/*
SELECT 'InvestmentSecurity' AS entityTableName, InvestmentSecurityID AS entityId FROM InvestmentSecurity WHERE  Symbol IN ('GB00BD9MZZ71', 'GB00BDX8CX86', 'ZCSBRL-20200608-20290102-7', 'ZCSBRL-20180222-20270104-9.845')
UNION
SELECT 'SystemColumnValue' AS entityTableName, SystemColumnValueID AS entityId FROM SystemColumnValue WHERE SystemColumnID IN (SELECT SystemColumnID FROM SystemColumn WHERE SystemTableID = (SELECT SystemTableID FROM SystemTable WHERE TableName = 'InvestmentSecurity')) AND FKFieldID IN (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol IN ('GB00BD9MZZ71','ZCSBRL-20200608-20290102-7'))
UNION
SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID as entityId FROM MarketDataValue WHERE MarketDataFieldID = (SELECT MarketDataFieldID FROM MarketDataField WHERE DataFieldName = 'Index Ratio - CPI') AND InvestmentSecurityID = (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = 'GB00BD9MZZ71') AND MeasureDate BETWEEN '08/01/2021' AND '09/01/2021'
UNION
SELECT 'SystemBean' AS entityTableName, SystemBeanID as entityID FROM SystemBean WHERE SystemBeanID = (SELECT SystemBeanID FROM SystemBean WHERE SystemBeanName IN ('', ''))
UNION
SELECT 'SystemBeanProperty' AS entityTableName, SystemBeanPropertyID as entityId FROM SystemBeanProperty WHERE SystemBeanPropertyTypeID IN (SELECT SystemBeanPropertyTypeID FROM SystemBeanPropertyType WHERE SystemBeanPropertyTypeName IN ('Field Type', 'Notional Multiplier Field'))
;
*/


