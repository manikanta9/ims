package com.clifton.marketdata.live.theoretical.util;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;


/**
 * @author NickK
 */
public class MarketDataTheoreticalValueUtilTests {


	@Test
	public void testMarketDataBlackScholesPrice() {
		MarketDataTheoreticalValueUtil.MarketDataBlackScholesCommand command = new MarketDataTheoreticalValueUtil.MarketDataBlackScholesCommand()
				.setCall(true)
				.setUnderlyingSpotPrice(1.0)
				.setStrikePrice(1.05)
				.setYearsToMaturity(1)
				.setRiskFreeRate(0.01)
				.setDividendYield(0.02)
				.setVolatility(0.1);
		Assertions.assertEquals(0.017352279200636422, MarketDataTheoreticalValueUtil.getEuropeanOptionBlackScholesPrice(command), 0.000001);

		command.setUnderlyingSpotPrice(2776.42)
				.setStrikePrice(2275.0)
				.setYearsToMaturity(0.44383561643835616438)
				.setRiskFreeRate(0.0185460)
				.setDividendYield(0.0195746)
				.setVolatility(0.23959903717041016);
		Assertions.assertEquals(516.3440649549095, MarketDataTheoreticalValueUtil.getEuropeanOptionBlackScholesPrice(command), 0.000001);
	}
}
