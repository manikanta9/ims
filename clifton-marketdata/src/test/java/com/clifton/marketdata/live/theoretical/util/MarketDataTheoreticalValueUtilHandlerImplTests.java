package com.clifton.marketdata.live.theoretical.util;


import com.clifton.instruction.messages.beans.OptionStyleTypes;
import com.clifton.marketdata.live.theoretical.MarketDataOptionTheoreticalValueCommand;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;


/**
 * @author NickK
 */
@ExtendWith(SpringExtension.class)
@ContextConfiguration(locations = "classpath:com/clifton/marketdata/MarketDataProjectBasicTests-context.xml")
public class MarketDataTheoreticalValueUtilHandlerImplTests {

	@Resource
	private MarketDataTheoreticalValueUtilHandler marketDataTheoreticalValueUtilHandler;

	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testMarketDataOptionBlackScholesPrice() {
		MarketDataOptionTheoreticalValueCommand command = new MarketDataOptionTheoreticalValueCommand()
				.withCall(true)
				.withUnderlyingPrice(BigDecimal.ONE)
				.withStrikePrice(BigDecimal.valueOf(1.05))
				.withYearsToMaturity(BigDecimal.ONE)
				.withRiskFreeRate(BigDecimal.valueOf(0.01))
				.withDividendYield(BigDecimal.valueOf(0.02))
				.withVolatility(BigDecimal.valueOf(0.1))
				.withOptionStyle(OptionStyleTypes.EUROPEAN);
		Assertions.assertEquals(BigDecimal.valueOf(0.017352279200636422), this.marketDataTheoreticalValueUtilHandler.getMarketDataOptionPrice(command));

		command.withUnderlyingPrice(BigDecimal.valueOf(2776.42))
				.withStrikePrice(BigDecimal.valueOf(2275.0))
				.withYearsToMaturity(BigDecimal.valueOf(0.44383561643835616438))
				.withRiskFreeRate(BigDecimal.valueOf(0.0185460))
				.withDividendYield(BigDecimal.valueOf(0.0195746))
				.withVolatility(BigDecimal.valueOf(0.23959903717041016))
				.withOptionStyle(OptionStyleTypes.EUROPEAN);
		Assertions.assertEquals(BigDecimal.valueOf(516.3440649549095), this.marketDataTheoreticalValueUtilHandler.getMarketDataOptionPrice(command));
	}

// TODO Add this test after we get expected inputs and outputs
//	@Test
//	public void testMarketDataOptionBinomialPrice() {
//		Date startDate = new Date(2019, 2, 27);
//		Map<Date, BigDecimal> dividendSchedule = new HashMap<>();
//		dividendSchedule.put(startDate, BigDecimal.valueOf(1));
//		dividendSchedule.put(DateUtils.addDays(startDate, 1), BigDecimal.valueOf(1));
//		dividendSchedule.put(DateUtils.addDays(startDate, 2), BigDecimal.valueOf(1));
//		dividendSchedule.put(DateUtils.addDays(startDate, 3), BigDecimal.valueOf(1));
//		dividendSchedule.put(DateUtils.addDays(startDate, 4), BigDecimal.valueOf(1));
//
//		MarketDataOptionTheoreticalValueCommand command = new MarketDataOptionTheoreticalValueCommand()
//				.withCall(true)
//				.withUnderlyingPrice(BigDecimal.ONE)
//				.withStrikePrice(BigDecimal.valueOf(1.05))
//				.withYearsToMaturity(BigDecimal.ONE)
//				.withRiskFreeRate(BigDecimal.valueOf(0.01))
//				.withVolatility(BigDecimal.valueOf(0.1))
//				.withOptionStyle(InvestmentSecurity.OPTION_STYLE_AMERICAN)
//				.withDividendSchedule(null)
//				.withSteps(5);
//		Assertions.assertEquals(BigDecimal.valueOf(0.017352279200636422), this.marketDataTheoreticalValueUtilHandler.getMarketDataOptionPrice(command));
//
//		command.withUnderlyingPrice(BigDecimal.valueOf(2776.42))
//				.withStrikePrice(BigDecimal.valueOf(2275.0))
//				.withYearsToMaturity(BigDecimal.valueOf(0.44383561643835616438))
//				.withRiskFreeRate(BigDecimal.valueOf(0.0185460))
//				.withVolatility(BigDecimal.valueOf(0.23959903717041016))
//				.withOptionStyle(InvestmentSecurity.OPTION_STYLE_AMERICAN)
//				.withDividendSchedule(null)
//				.withSteps(5);
//		Assertions.assertEquals(BigDecimal.valueOf(516.3440649549095), this.marketDataTheoreticalValueUtilHandler.getMarketDataOptionPrice(command));
//	}
}
