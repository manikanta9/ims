package com.clifton.marketdata;


import com.clifton.core.test.BasicProjectTests;
import com.clifton.core.util.CollectionUtils;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.lang.reflect.Method;
import java.util.HashSet;
import java.util.Set;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class MarketDataProjectBasicTests extends BasicProjectTests {

	@Override
	public String getProjectPrefix() {
		return "marketData";
	}


	@Override
	protected boolean isMethodSkipped(Method method) {
		HashSet<String> skipMethods = new HashSet<>();
		skipMethods.add("getMarketDataSourceByName"); // first lookup caches null and insert in tests has observers disabled: cache is not cleared
		skipMethods.add("saveMarketDataExchangeRate");
		skipMethods.add("saveMarketDataInterestRate");
		skipMethods.add("saveMarketDataFieldMapping");
		skipMethods.add("saveMarketDataValue");
		return skipMethods.contains(method.getName());
	}


	@Override
	protected void configureApprovedPackageNames(Set<String> approvedList) {
		approvedList.add("theoretical");
		approvedList.add("securityallocation");
	}


	@Override
	protected Set<String> getIgnoreVoidSaveMethodSet() {
		Set<String> ignoredVoidSaveMethodSet = new HashSet<>();
		ignoredVoidSaveMethodSet.add("saveMarketDataInvestmentSecurityFieldUpdateResult");
		return ignoredVoidSaveMethodSet;
	}


	@Override
	public boolean isServiceSkipped(String beanName) {
		return CollectionUtils.createHashSet(
				"marketDataSecurityDataValueApiService" // ignored because SecureMethod annotation is on the impl
		).contains(beanName);
	}
}
