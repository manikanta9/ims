package com.clifton.marketdata.instrument.allocation;


import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.UpdatableDAO;
import com.clifton.core.dataaccess.datatable.DataRow;
import com.clifton.core.dataaccess.datatable.DataTable;
import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationCalculatedFieldTypes;
import com.clifton.investment.instrument.allocation.InvestmentSecurityAllocationHandler;
import com.clifton.investment.instrument.search.SecurityAllocationSearchForm;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataRatesService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 *
 */
@ContextConfiguration()
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class MarketDataInvestmentSecurityAllocationServiceImplTests {

	@Resource
	private CalendarBusinessDayService calendarBusinessDayService;

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSecurityAllocationHandler investmentSecurityAllocationHandler;

	@Resource
	private UpdatableDAO<InvestmentSecurity> investmentSecurityDAO;

	@Resource
	private MarketDataInvestmentSecurityAllocationService marketDataInvestmentSecurityAllocationService;

	@Resource
	private MarketDataFieldService marketDataFieldService;

	@Resource
	private MarketDataRatesService marketDataRatesService;

	@Resource
	private MarketDataSourceService marketDataSourceService;

	private static final Short DATA_SOURCE_PARAMETRIC_CLIFTON = 3;


	@Test
	public void testCalculateInvestmentSecurityAllocation_StartOnNonBusinessDay() {
		this.calendarSetupService.saveCalendarYearForYear((short) 2015);
		// Start on a Sunday
		Date startDate = DateUtils.toDate("02/22/2015");

		// Set up Market Data to Start on Friday Before ("02/20/2015");
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, DateUtils.toDate("02/20/2015"), startDate, null);

		String message = "";
		try {
			this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, DateUtils.addDays(startDate, 5), null);
		}
		catch (ValidationException e) {
			message = e.getMessage();
		}

		Assertions.assertEquals("[Error: Error calculating allocated security price for security [GSCI-UBS]: Unable to Calculate Security [GSCI-UBS]: Start Date of [02/22/2015] is not a business day.  Please move start date to a business day., " +
				"Error: Error calculating allocated security price for security [GSCI-UBS-FS (GSCI-UBS-FROM START)]: Unable to Calculate Security [GSCI-UBS-FS (GSCI-UBS-FROM START)]: Start Date of [02/22/2015] is not a business day.  Please move start date to a business day., " +
				"Error: Error calculating allocated security price for security [SPTR50]: Unable to Calculate Security [SPTR50]: Start Date of [02/22/2015] is not a business day.  Please move start date to a business day., " +
				"Error: Error calculating allocated security price for security [SPTR50-FS (SPTR50-FROM START)]: Unable to Calculate Security [SPTR50-FS (SPTR50-FROM START)]: Start Date of [02/22/2015] is not a business day.  Please move start date to a business day.]", message);
	}


	@Test
	public void testCalculateInvestmentSecurityAllocationPrices() {
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate);
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDate(startDate, (short) 1), 3), null);

		Assertions.assertEquals("Updated [5] securities, [20] market data value records.", status.getMessage());

		validateResults("SPTR50", new Double[]{0.474, 0.674, 0.869});
		validateResults("SPTR50-FS", new Double[]{0.474, 0.677, 0.879});

		validateResults("GSCI-UBS", new Double[]{0.298, -0.450, -0.121});
		validateResults("GSCI-UBS-FS", new Double[]{0.298, -0.451, -0.121});

		validateResults("BASKET", new Double[]{6.560, 0.003, -1.257});
	}


	@Test
	public void testCalculateInvestmentSecurityAllocationMonthlyReturns() {
		Date date = DateUtils.toDate("06/30/2015");
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.MONTHLY_RETURN, date);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.MONTHLY_RETURN, startDate);
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, DateUtils.getFirstDayOfMonth(date), null);

		Assertions.assertEquals("Updated [1] securities, [4] market data value records.", status.getMessage());

		validateMonthlyReturns("SYN-RUSSELL-2500", new Double[]{0.395, -0.270, -0.610, -1.012});
	}


	@Test
	public void testCalculateInvestmentSecurityAllocationPrices_Inactive_SecuritiesNotStartedYet() {
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate, DateUtils.addDays(startDate, 10), null);
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, new Date(), null);

		Assertions.assertEquals("Updated [0] securities, [0] market data value records.", status.getMessage());
	}


	@Test
	public void testCalculateInvestmentSecurityAllocationPrices_Inactive_SecuritiesEnded() {
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate, DateUtils.addDays(startDate, -30), DateUtils.addDays(startDate, -10));
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, new Date(), null);

		Assertions.assertEquals("Updated [0] securities, [0] market data value records.", status.getMessage());
	}


	@Test
	public void testCalculateInvestmentSecurityAllocationPrices_PartiallyInactive_SecuritiesNotStartedYet() {
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate, this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2), null);
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 3), null);

		Assertions.assertEquals("Updated [5] securities, [10] market data value records.", status.getMessage());
	}


	@Test
	public void testCalculateInvestmentSecurityAllocationPrices_PartiallyInactive_SecuritiesEnded() {
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate, null, this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2));
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, new Date(), null);

		Assertions.assertEquals("Updated [5] securities, [15] market data value records.", status.getMessage());
	}


	@Test
	public void testWeightedBasket_NoSystemMarketDataCalculations() {
		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.NONE);
		List<InvestmentSecurity> securityList = setup(InvestmentSecurityAllocationCalculatedFieldTypes.NONE, startDate, null, this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2));
		Status status = this.marketDataInvestmentSecurityAllocationService.calculateInvestmentSecurityAllocationMarketDataValues(securityList, DATA_SOURCE_PARAMETRIC_CLIFTON, startDate, new Date(), null);
		Assertions.assertEquals("Updated [0] securities, [0] market data value records.", status.getMessage());
	}

	////////////////////////////////////////////////////////////////////////////////s


	@Test
	public void testRebuildInvestmentSecurityAllocationRebalance() {
		// Method is defined in investment, but calculators/implementation is in market date

		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate);

		// "BASKET"
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("BASKET", false);
		Assertions.assertNotNull(security, "Expecting security with symbol BASKET to exist");

		// Rebuild the Weights on Security start:
		this.investmentSecurityAllocationHandler.rebuildInvestmentSecurityAllocationRebalance(security, DATA_SOURCE_PARAMETRIC_CLIFTON, security.getStartDate(), null, true);

		// Pull the MarketDataInvestmentSecurityAllocation On Start Date + 1 and Validate Results
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(security.getId());
		searchForm.setActiveOnDate(this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 1));
		List<MarketDataInvestmentSecurityAllocation> mdaList = this.marketDataInvestmentSecurityAllocationService.getMarketDataInvestmentSecurityAllocationList(searchForm);

		for (MarketDataInvestmentSecurityAllocation allocation : mdaList) {
			if ("AAPL".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: AAPL (APPLE): Shares [25],  Base Weight [7.39],  Current Weight [7.03],  Current Price [476.05015478]", allocation.toString());
			}
			else if ("ACN".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: ACN (ACCENT): Shares [680],  Base Weight [30.35],  Current Weight [29.68],  Current Price [73.942548621]", allocation.toString());
			}
			else if ("AH".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: AH (A-HOLD): Shares [642],  Base Weight [7.08],  Current Weight [6.58],  Current Price [12.83]", allocation.toString());
			}

			else if ("BRK".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: BRK (BERKSHIRE): Shares [501],  Base Weight [36.27],  Current Weight [33.56],  Current Price [113.48]", allocation.toString());
			}

			else if ("CL".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: CL (COLGATE): Shares [654],  Base Weight [18.91],  Current Weight [23.15],  Current Price [59.96]", allocation.toString());
			}
			else {
				Assertions.fail("Unexpected Security Allocation: " + allocation.toString());
			}
		}
	}


	@Test
	public void testProcessInvestmentSecurityAllocationRebalance_UsingBaseWeights() {
		// Method is defined in investment, but calculators/implementation is in market date

		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate);

		// "BASKET"
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("BASKET", false);
		Assertions.assertNotNull(security, "Expecting security with symbol BASKET to exist");

		// Rebuild the Weights on Security start:
		this.investmentSecurityAllocationHandler.rebuildInvestmentSecurityAllocationRebalance(security, DATA_SOURCE_PARAMETRIC_CLIFTON, security.getStartDate(), null, true);

		// Rebalance on Security Start + 2
		Date rebalDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2);
		this.investmentSecurityAllocationHandler.processInvestmentSecurityAllocationRebalance(security, DATA_SOURCE_PARAMETRIC_CLIFTON, security.getStartDate(), true,
				this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2), null);

		// Pull the MarketDataInvestmentSecurityAllocation On Start Date + 2 and Validate Results
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(security.getId());
		searchForm.setActiveOnDate(rebalDate);
		List<MarketDataInvestmentSecurityAllocation> mdaList = this.marketDataInvestmentSecurityAllocationService.getMarketDataInvestmentSecurityAllocationList(searchForm);

		for (MarketDataInvestmentSecurityAllocation allocation : mdaList) {

			if ("AAPL".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: AAPL (APPLE): Shares [26.3050005496],  Base Weight [7.39],  Current Weight [7.39],  Current Price [476.0535487]", allocation.toString());
			}
			else if ("ACN".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: ACN (ACCENT): Shares [695.2387523335],  Base Weight [30.35],  Current Weight [30.35],  Current Price [73.94545]", allocation.toString());
			}
			else if ("AH".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: AH (A-HOLD): Shares [690.8773913553],  Base Weight [7.08],  Current Weight [7.08],  Current Price [12.83]", allocation.toString());
			}

			else if ("BRK".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: BRK (BERKSHIRE): Shares [541.4116790608],  Base Weight [36.27],  Current Weight [36.27],  Current Price [113.48]", allocation.toString());
			}

			else if ("CL".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: CL (COLGATE): Shares [534.2021250167],  Base Weight [18.91],  Current Weight [18.91],  Current Price [59.96]", allocation.toString());
			}
			else {
				Assertions.fail("Unexpected Security Allocation: " + allocation.toString());
			}
		}
	}


	@Test
	public void testProcessInvestmentSecurityAllocationRebalance_UsingBaseShares() {
		// Method is defined in investment, but calculators/implementation is in market date

		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate);

		// "BASKET"
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("BASKET", false);
		Assertions.assertNotNull(security, "Expecting security with symbol BASKET to exist");

		// Rebuild the Weights on Security start:
		this.investmentSecurityAllocationHandler.rebuildInvestmentSecurityAllocationRebalance(security, DATA_SOURCE_PARAMETRIC_CLIFTON, security.getStartDate(), null, true);

		// Rebalance on Security Start + 2
		Date rebalDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2);
		this.investmentSecurityAllocationHandler.processInvestmentSecurityAllocationRebalance(security, DATA_SOURCE_PARAMETRIC_CLIFTON, security.getStartDate(), false,
				this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 2), null);

		// Pull the MarketDataInvestmentSecurityAllocation On Start Date + 2 and Validate Results
		SecurityAllocationSearchForm searchForm = new SecurityAllocationSearchForm();
		searchForm.setParentInvestmentSecurityId(security.getId());
		searchForm.setActiveOnDate(rebalDate);
		List<MarketDataInvestmentSecurityAllocation> mdaList = this.marketDataInvestmentSecurityAllocationService.getMarketDataInvestmentSecurityAllocationList(searchForm);

		for (MarketDataInvestmentSecurityAllocation allocation : mdaList) {
			if ("AAPL".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: AAPL (APPLE): Shares [25],  Base Weight [7.03],  Current Weight [7.03],  Current Price [476.0535487]", allocation.toString());
			}
			else if ("ACN".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: ACN (ACCENT): Shares [680],  Base Weight [29.68],  Current Weight [29.68],  Current Price [73.94545]", allocation.toString());
			}
			else if ("AH".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: AH (A-HOLD): Shares [642],  Base Weight [6.58],  Current Weight [6.58],  Current Price [12.83]", allocation.toString());
			}

			else if ("BRK".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: BRK (BERKSHIRE): Shares [501],  Base Weight [33.56],  Current Weight [33.56],  Current Price [113.48]", allocation.toString());
			}

			else if ("CL".equals(allocation.getInvestmentSecurity().getSymbol())) {
				Assertions.assertEquals("Security: CL (COLGATE): Shares [654],  Base Weight [23.15],  Current Weight [23.15],  Current Price [59.96]", allocation.toString());
			}
			else {
				Assertions.fail("Unexpected Security Allocation: " + allocation.toString());
			}
		}
	}


	@Test
	public void testGetMarketDataInvestmentSecurityAllocationWeightListForDateRange() {

		Date startDate = calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE);
		setup(InvestmentSecurityAllocationCalculatedFieldTypes.PRICE, startDate);

		// "BASKET"
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("BASKET", false);
		Assertions.assertNotNull(security, "Expecting security with symbol BASKET to exist");

		DataTable dt = this.marketDataInvestmentSecurityAllocationService.getMarketDataInvestmentSecurityAllocationWeightListForDateRange(security.getId(), startDate, null);

		Assertions.assertEquals(5, dt.getTotalRowCount(), "Expected 5 Rows of Data but was " + dt.getTotalRowCount());
		Assertions.assertEquals(7, dt.getColumnCount(), "Expected 7 Columns of Data but was " + dt.getColumnCount());

		for (int i = 0; i < dt.getTotalRowCount(); i++) {
			DataRow row = dt.getRow(i);

			String allocationLabel = (String) row.getValue(0);
			StringBuilder allocationResult = new StringBuilder(16);
			for (int j = 1; j < dt.getColumnCount(); j++) {
				BigDecimal value = (BigDecimal) row.getValue(j);
				allocationResult.append(CoreMathUtils.formatNumberMoney(value)).append("|");
			}

			if ("Security: AAPL (APPLE)".equals(allocationLabel)) {
				Assertions.assertEquals("7.39|7.03|7.03|7.13|||", allocationResult.toString());
			}
			else if ("Security: ACN (ACCENT)".equals(allocationLabel)) {
				Assertions.assertEquals("30.35|29.68|29.68|30.46|||", allocationResult.toString());
			}
			else if ("Security: AH (A-HOLD)".equals(allocationLabel)) {
				Assertions.assertEquals("7.08|6.58|6.58|6.86|||", allocationResult.toString());
			}

			else if ("Security: BRK (BERKSHIRE)".equals(allocationLabel)) {
				Assertions.assertEquals("36.27|33.56|33.56|33.60|||", allocationResult.toString());
			}

			else if ("Security: CL (COLGATE)".equals(allocationLabel)) {
				Assertions.assertEquals("18.91|23.15|23.15|21.94|||", allocationResult.toString());
			}
			else {
				Assertions.fail("Unexpected Security Allocation: " + allocationLabel);
			}
		}
	}


	////////////////////////////////////////////////////////////////
	//////////               Helper Methods              ///////////
	////////////////////////////////////////////////////////////////


	private Date calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes fieldType) {
		return calculateStartDate(fieldType, null);
	}


	private Date calculateStartDate(InvestmentSecurityAllocationCalculatedFieldTypes fieldType, Date date) {
		if (date == null) {
			date = DateUtils.clearTime(new Date());
		}
		short yr = new Integer(DateUtils.getYear(date)).shortValue();
		this.calendarSetupService.saveCalendarYearForYear(((Integer) (yr - 1)).shortValue());
		this.calendarSetupService.saveCalendarYearForYear(yr);
		this.calendarSetupService.saveCalendarYearForYear(((Integer) (yr + 1)).shortValue());
		// Go Back 4 Months to calculate last 4 returns
		if (fieldType.isMonthEndOnly()) {
			Date startDate = DateUtils.addMonths(DateUtils.getFirstDayOfMonth(date), -3);
			if (DateUtils.isWeekend(startDate)) {
				startDate = DateUtils.getPreviousWeekday(startDate);
			}
			return startDate;
		}
		// All Daily Data starts 5 business days ago (today's value isn't populated yet)
		int startDateBack = -5;
		if (!this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDefaultCalendar(date))) {
			startDateBack = -6;
		}
		return this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(date), startDateBack);
	}


	private List<InvestmentSecurity> setup(InvestmentSecurityAllocationCalculatedFieldTypes fieldType, Date startDate) {
		return setup(fieldType, startDate, null, null);
	}


	private List<InvestmentSecurity> setup(InvestmentSecurityAllocationCalculatedFieldTypes fieldType, Date startDate, Date securityStartDate, Date securityEndDate) {
		MarketDataField field = this.marketDataFieldService.getMarketDataFieldByName(MarketDataField.FIELD_LAST_TRADE_PRICE);
		MarketDataField monthlyReturnField = this.marketDataFieldService.getMarketDataFieldByName(MarketDataField.FIELD_MONTHLY_RETURN);
		MarketDataSource dataSource = this.marketDataSourceService.getMarketDataSourceByName("Bloomberg");

		SecuritySearchForm searchForm = new SecuritySearchForm();
		searchForm.setAllocationOfSecurities(true);
		List<InvestmentSecurity> securityList = this.investmentInstrumentService.getInvestmentSecurityList(searchForm);
		List<InvestmentSecurity> filteredList = new ArrayList<>();

		for (InvestmentSecurity security : securityList) {
			if (fieldType == security.getInstrument().getHierarchy().getSecurityAllocationType().getCalculatedFieldType()) {
				if (fieldType.isSystemCalculated()) {
					security.setStartDate(securityStartDate == null ? startDate : securityStartDate);
					security.setEndDate(securityEndDate);
					this.investmentSecurityDAO.save(security);
				}
				filteredList.add(security);
			}
		}

		if (InvestmentSecurityAllocationCalculatedFieldTypes.PRICE == fieldType) {
			// SPTR (USED BY SPTR50 & SPTR50-FS)
			loadPrices("SPTR", startDate, new Double[]{1981.3, 2000.08, 2027.037, 2062.2550}, field, dataSource);

			// ENH-GD-95T & SP-GCE-STR (USED BY GSCI-UBS & GSCI-UBS-FS)
			loadPrices("ENH-GD-95T", startDate, new Double[]{675.3573, 676.9251, 674.4845, 668.8979}, field, dataSource);
			loadPrices("SP-GCE-STR", startDate, new Double[]{728.2735, 730.9224, 726.9724, 731.24}, field, dataSource);

			// AAPL, ACN, AH, BRK, CL (USED BY BASKET)
			loadPrices("AAPL", startDate, new Double[]{470.05045715, 476.05015478, 476.0535487, 477.0516844415}, field, dataSource);
			loadPrices("ACN", startDate, new Double[]{70.94587126, 73.942548621, 73.94545, 74.9400145}, field, dataSource);
			loadPrices("AH", startDate, new Double[]{12.96, 12.83, 12.83, 13.2}, field, dataSource);
			loadPrices("BRK", startDate, new Double[]{115.08, 113.48, 113.48, 112.2}, field, dataSource);
			loadPrices("CL", startDate, new Double[]{45.96, 59.96, 59.96, 56.12}, field, dataSource);

			loadFxRates("EUR", "USD", startDate, new Double[]{1.3536, 1.3536, 1.35398, 1.35398}, this.marketDataSourceService.getMarketDataSourceByName("Goldman Sachs"));
		}
		else if (InvestmentSecurityAllocationCalculatedFieldTypes.MONTHLY_RETURN == fieldType) {
			// SYN-ALLOC-1, SYN-ALLOC-2 USED FOR MONTHLY RETURN WEIGHTED AVERAGE CALC
			// WILL LOAD PRICES FOR SYN-ALLOC-1 AND MONTHLY RETURNS FOR SYN-ALLOC-2 SO CAN TEST BOTH WAYS
			loadMonthEndValues("SYN-ALLOC-1", startDate, new Double[]{115.08, 113.48, 113.48, 112.2, 110.1}, field, dataSource);
			loadMonthEndValues("SYN-ALLOC-2", startDate, new Double[]{1.09, 2.5, -0.589, 0.0, .00068}, monthlyReturnField, dataSource);
		}
		// Field Type NONE won't calculate anything

		return filteredList;
	}


	private void loadPrices(String symbol, Date startDate, Double[] prices, MarketDataField field, MarketDataSource dataSource) {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, null);

		for (Double price : prices) {
			MarketDataValue mdv = new MarketDataValue();
			mdv.setDataField(field);
			mdv.setDataSource(dataSource);
			mdv.setInvestmentSecurity(sec);
			mdv.setMeasureDate(startDate);
			mdv.setMeasureValue(BigDecimal.valueOf(price));
			this.marketDataFieldService.saveMarketDataValue(mdv);

			startDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 1);
		}
	}


	private void loadMonthEndValues(String symbol, Date startDate, Double[] values, MarketDataField field, MarketDataSource dataSource) {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, null);
		startDate = DateUtils.getLastDayOfMonth(DateUtils.addMonths(startDate, -1));
		for (Double value : values) {
			MarketDataValue mdv = new MarketDataValue();
			mdv.setDataField(field);
			mdv.setDataSource(dataSource);
			mdv.setInvestmentSecurity(sec);
			mdv.setMeasureDate(startDate);
			mdv.setMeasureValue(BigDecimal.valueOf(value));
			this.marketDataFieldService.saveMarketDataValue(mdv);

			startDate = DateUtils.getLastDayOfMonth(DateUtils.addDays(startDate, 1));
		}
	}


	private void loadFxRates(String from, String to, Date startDate, Double[] rates, MarketDataSource dataSource) {
		InvestmentSecurity fromCcy = this.investmentInstrumentService.getInvestmentSecurityBySymbol(from, true);
		InvestmentSecurity toCcy = this.investmentInstrumentService.getInvestmentSecurityBySymbol(to, true);

		for (Double rate : rates) {
			MarketDataExchangeRate fxRate = new MarketDataExchangeRate();
			fxRate.setFromCurrency(fromCcy);
			fxRate.setToCurrency(toCcy);
			fxRate.setDataSource(dataSource);
			fxRate.setRateDate(startDate);
			fxRate.setExchangeRate(BigDecimal.valueOf(rate));
			this.marketDataRatesService.saveMarketDataExchangeRate(fxRate);

			startDate = this.calendarBusinessDayService.getBusinessDayFrom(CalendarBusinessDayCommand.forDefaultCalendar(startDate), 1);
		}
	}


	private void validateMonthlyReturns(String symbol, Double[] returnValues) {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, null);
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setInvestmentSecurityId(sec.getId());
		searchForm.setOrderBy("measureDate:asc");
		List<MarketDataValue> vList = this.marketDataFieldService.getMarketDataValueList(searchForm);

		Assertions.assertEquals(4, CollectionUtils.getSize(vList));

		Object[] values = BeanUtils.getPropertyValues(vList, "measureValue");

		for (int i = 0; i < returnValues.length; i++) {
			BigDecimal expected = BigDecimal.valueOf(returnValues[i]);
			BigDecimal actual = (BigDecimal) values[i];
			Assertions.assertTrue(MathUtils.isEqual(MathUtils.round(expected, 3), MathUtils.round(actual, 3)),
					"Expected Monthly Return of [" + MathUtils.round(expected, 3) + "] but was [" + MathUtils.round(actual, 3) + "]");
		}
	}


	private void validateResults(String symbol, Double[] percentChangesSinceStartDate) {
		InvestmentSecurity sec = this.investmentInstrumentService.getInvestmentSecurityBySymbol(symbol, null);
		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setInvestmentSecurityId(sec.getId());
		searchForm.setOrderBy("measureDate:asc");
		List<MarketDataValue> vList = this.marketDataFieldService.getMarketDataValueList(searchForm);

		Assertions.assertEquals(4, CollectionUtils.getSize(vList));

		Object[] values = BeanUtils.getPropertyValues(vList, "measureValue");

		for (int i = 0; i < percentChangesSinceStartDate.length; i++) {
			BigDecimal expected = BigDecimal.valueOf(percentChangesSinceStartDate[i]);
			BigDecimal actual = MathUtils.getPercentChange((BigDecimal) values[i], (BigDecimal) values[i + 1], true);
			Assertions.assertTrue(MathUtils.isEqual(MathUtils.round(expected, 3), MathUtils.round(actual, 3)),
					"Expected % Change of [" + MathUtils.round(expected, 3) + "] but had actual % change of [" + MathUtils.round(actual, 3) + "]");
		}
	}
}
