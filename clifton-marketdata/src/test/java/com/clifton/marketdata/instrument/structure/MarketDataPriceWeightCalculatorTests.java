package com.clifton.marketdata.instrument.structure;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.util.math.CoreMathUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.structure.InvestmentInstrumentStructureService;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureAllocation;
import com.clifton.investment.instrument.structure.InvestmentSecurityStructureWeight;
import com.clifton.marketdata.MarketDataRetriever;
import com.clifton.core.util.MathUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class MarketDataPriceWeightCalculatorTests implements ApplicationContextAware {

	private ApplicationContext applicationContext;

	@Resource
	private XmlReadOnlyDAO<InvestmentSecurityStructureWeight> investmentSecurityStructureWeightDAO;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentInstrumentStructureService investmentInstrumentStructureService;

	@Resource
	private MarketDataRetriever marketDataRetriever;

	///////////////////////////////////////////////////////////////////////////////

	private static final Map<String, Double> PRICE_MAP_DJUBS = new HashMap<>();
	private static final Map<String, Double> RESULT_MAP_DJUBS = new HashMap<>();

	private static final Map<String, Double> PRICE_MAP_GSCI = new HashMap<>();
	private static final Map<String, Double> RESULT_MAP_GSCI = new HashMap<>();


	static {
		PRICE_MAP_DJUBS.put("NG", 3.45300);
		PRICE_MAP_DJUBS.put("CL", 93.72000);
		PRICE_MAP_DJUBS.put("CO", 108.29000);
		PRICE_MAP_DJUBS.put("XB", 272.61000);
		PRICE_MAP_DJUBS.put("HO", 300.34000);
		PRICE_MAP_DJUBS.put("LC", 134.42500);
		PRICE_MAP_DJUBS.put("LH", 87.60000);
		PRICE_MAP_DJUBS.put("W", 782.75000);
		PRICE_MAP_DJUBS.put("KW", 838.25000);
		PRICE_MAP_DJUBS.put("C", 730.50000);
		PRICE_MAP_DJUBS.put("S", 1413.50000);
		PRICE_MAP_DJUBS.put("BO", 50.87000);
		PRICE_MAP_DJUBS.put("SM", 411.90000);
		PRICE_MAP_DJUBS.put("LA", 2043.25000);
		PRICE_MAP_DJUBS.put("HG", 363.75000);
		PRICE_MAP_DJUBS.put("LX", 1987.25000);
		PRICE_MAP_DJUBS.put("LN", 17481.00000);
		PRICE_MAP_DJUBS.put("GC", 1686.20000);
		PRICE_MAP_DJUBS.put("SI", 31.52900);
		PRICE_MAP_DJUBS.put("SB", 18.62000);
		PRICE_MAP_DJUBS.put("CT", 76.21000);
		PRICE_MAP_DJUBS.put("KC", 152.50000);

		RESULT_MAP_DJUBS.put("NG", 10.80);
		RESULT_MAP_DJUBS.put("CL", 9.07);
		RESULT_MAP_DJUBS.put("CO", 5.59);
		RESULT_MAP_DJUBS.put("XB", 3.34);
		RESULT_MAP_DJUBS.put("HO", 3.45);
		RESULT_MAP_DJUBS.put("LC", 3.27);
		RESULT_MAP_DJUBS.put("LH", 1.90);
		RESULT_MAP_DJUBS.put("W", 3.52);
		RESULT_MAP_DJUBS.put("KW", 1.35);
		RESULT_MAP_DJUBS.put("C", 7.40);
		RESULT_MAP_DJUBS.put("S", 5.51);
		RESULT_MAP_DJUBS.put("BO", 2.75);
		RESULT_MAP_DJUBS.put("SM", 2.58);
		RESULT_MAP_DJUBS.put("LA", 4.80);
		RESULT_MAP_DJUBS.put("HG", 7.09);
		RESULT_MAP_DJUBS.put("LX", 2.45);
		RESULT_MAP_DJUBS.put("LN", 2.25);
		RESULT_MAP_DJUBS.put("GC", 10.9);
		RESULT_MAP_DJUBS.put("SI", 4.02);
		RESULT_MAP_DJUBS.put("SB", 3.77);
		RESULT_MAP_DJUBS.put("CT", 1.75);
		RESULT_MAP_DJUBS.put("KC", 2.44);

		PRICE_MAP_GSCI.put("W", 782.75000);
		PRICE_MAP_GSCI.put("KW", 838.25000);
		PRICE_MAP_GSCI.put("C", 730.50000);
		PRICE_MAP_GSCI.put("S", 1413.50000);
		PRICE_MAP_GSCI.put("KC", 152.50000);
		PRICE_MAP_GSCI.put("SB", 18.62000);
		PRICE_MAP_GSCI.put("CC", 2270.00000);
		PRICE_MAP_GSCI.put("CT", 76.21000);
		PRICE_MAP_GSCI.put("LH", 87.60000);
		PRICE_MAP_GSCI.put("LC", 134.42500);
		PRICE_MAP_GSCI.put("FC", 150.37500);
		PRICE_MAP_GSCI.put("CL", 93.72000);
		PRICE_MAP_GSCI.put("HO", 300.34000);
		PRICE_MAP_GSCI.put("XB", 272.61000);
		PRICE_MAP_GSCI.put("CO", 108.89000);
		PRICE_MAP_GSCI.put("QS", 952.00000);
		PRICE_MAP_GSCI.put("NG", 3.45300);
		PRICE_MAP_GSCI.put("LA", 2043.25000);
		PRICE_MAP_GSCI.put("LP", 7986.75000);
		PRICE_MAP_GSCI.put("LL", 2291.50000);
		PRICE_MAP_GSCI.put("LN", 17481.00000);
		PRICE_MAP_GSCI.put("LX", 1987.25000);
		PRICE_MAP_GSCI.put("GC", 1686.20000);
		PRICE_MAP_GSCI.put("SI", 31.52900);

		RESULT_MAP_GSCI.put("W", 3.6094);
		RESULT_MAP_GSCI.put("KW", 0.7696);
		RESULT_MAP_GSCI.put("C", 5.1932);
		RESULT_MAP_GSCI.put("S", 2.7011);
		RESULT_MAP_GSCI.put("KC", 0.6267);
		RESULT_MAP_GSCI.put("SB", 1.4906);
		RESULT_MAP_GSCI.put("CC", 0.2179);
		RESULT_MAP_GSCI.put("CT", 0.9364);
		RESULT_MAP_GSCI.put("LH", 1.5765);
		RESULT_MAP_GSCI.put("LC", 2.8722);
		RESULT_MAP_GSCI.put("FC", 0.5216);
		RESULT_MAP_GSCI.put("CL", 24.204);
		RESULT_MAP_GSCI.put("HO", 6.1708);
		RESULT_MAP_GSCI.put("XB", 5.6373);
		RESULT_MAP_GSCI.put("CO", 22.0191);
		RESULT_MAP_GSCI.put("QS", 8.6231);
		RESULT_MAP_GSCI.put("NG", 2.3804);
		RESULT_MAP_GSCI.put("LA", 2.0872);
		RESULT_MAP_GSCI.put("LP", 3.3090);
		RESULT_MAP_GSCI.put("LL", 0.4441);
		RESULT_MAP_GSCI.put("LN", 0.5630);
		RESULT_MAP_GSCI.put("LX", 0.5173);
		RESULT_MAP_GSCI.put("GC", 3.0303);
		RESULT_MAP_GSCI.put("SI", 0.4992);
	}


	///////////////////////////////////////////////////////////////////////////


	@Test
	public void testDJUBSCalculation() {
		Mockito.when(this.marketDataRetriever.getPriceFlexible(ArgumentMatchers.any(InvestmentSecurity.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentSecurity security = (InvestmentSecurity) args[0];
			if (security != null) {
				return BigDecimal.valueOf(PRICE_MAP_DJUBS.get(security.getInstrument().getIdentifierPrefix()));
			}
			return null;
		});

		processAndValidateCalculation(1, RESULT_MAP_DJUBS, null);
	}


	@Test
	public void testGSCICalculation() {
		Mockito.when(this.marketDataRetriever.getPriceFlexible(ArgumentMatchers.any(InvestmentSecurity.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.eq(true))).thenAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InvestmentSecurity security = (InvestmentSecurity) args[0];
			if (security != null) {
				return BigDecimal.valueOf(PRICE_MAP_GSCI.get(security.getInstrument().getIdentifierPrefix()));
			}
			return null;
		});

		processAndValidateCalculation(2, RESULT_MAP_GSCI, 4);
	}


	//////////////////////////////////////////////////////////////////////////////////


	private void processAndValidateCalculation(int structureId, Map<String, Double> resultMap, Integer precision) {
		Date date = new Date();
		List<InvestmentSecurityStructureAllocation> securityAllocationList = getSecurityAllocationListForStructure(structureId, date);
		MarketDataPriceWeightCalculator weightCalc = new MarketDataPriceWeightCalculator();
		((ConfigurableApplicationContext) getApplicationContext()).getBeanFactory().autowireBeanProperties(weightCalc, AutowireCapableBeanFactory.AUTOWIRE_BY_NAME, false);
		if (precision != null) {
			weightCalc.setWeightPrecision(precision);
		}
		weightCalc.calculateCurrentWeights(this.investmentInstrumentStructureService.getInvestmentInstrumentStructure(structureId), securityAllocationList, date);

		// Validate Results
		for (InvestmentSecurityStructureAllocation alloc : securityAllocationList) {
			InvestmentInstrument i = alloc.getCurrentSecurity().getInstrument();
			validateResult(alloc, BigDecimal.valueOf(resultMap.get(i.getIdentifierPrefix())));
		}
	}


	private void validateResult(InvestmentSecurityStructureAllocation allocation, BigDecimal expected) {
		Assertions.assertTrue(MathUtils.isEqual(expected, allocation.getAllocationWeight()),
				"Expected " + CoreMathUtils.formatNumberDecimal(expected) + " for the weight for [" + allocation.getCurrentSecurity().getInstrument().getIdentifierPrefix() + "] but got [" + CoreMathUtils.formatNumberDecimal(allocation.getAllocationWeight()) + "].");
	}


	private List<InvestmentSecurityStructureAllocation> getSecurityAllocationListForStructure(int id, Date date) {
		// Structure Service Uses lots of hibernate queries - custom sql, so for tests, this may be the best way to get the list
		List<InvestmentSecurityStructureWeight> securityWeightList = this.investmentSecurityStructureWeightDAO.findAll();
		securityWeightList = BeanUtils.filter(securityWeightList, securityWeight -> securityWeight.getInstrumentWeight().getInstrumentStructure().getId(), id);
		List<InvestmentSecurityStructureAllocation> securityAllocationList = new ArrayList<>();
		for (InvestmentSecurityStructureWeight secWeight : securityWeightList) {
			InvestmentSecurityStructureAllocation alloc = new InvestmentSecurityStructureAllocation(secWeight, date);
			alloc.setCurrentSecurity(this.investmentInstrumentService.getInvestmentSecurityBySymbol(secWeight.getInstrumentWeight().getInstrument().getIdentifierPrefix() + "H3", null));
			securityAllocationList.add(alloc);
		}
		return securityAllocationList;
	}


	public ApplicationContext getApplicationContext() {
		return this.applicationContext;
	}


	@Override
	public void setApplicationContext(ApplicationContext applicationContext) {
		this.applicationContext = applicationContext;
	}
}
