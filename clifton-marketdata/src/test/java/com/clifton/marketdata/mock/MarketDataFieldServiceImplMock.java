package com.clifton.marketdata.mock;


import com.clifton.core.beans.BeanUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataFieldServiceImpl;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;

import java.util.Date;
import java.util.List;


public class MarketDataFieldServiceImplMock extends MarketDataFieldServiceImpl {

	/**
	 * Since we don't support <= < >= > searches in XML DAOs, need to override method
	 */
	@Override
	protected MarketDataValueHolder getMarketDataValueForDateImpl(int securityId, InvestmentSecurity security, Date date, String fieldName, Short dataSourceId, boolean flexible, boolean normalized) {
		if (normalized) {
			if (security == null) {
				security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
			}
			if (security.getInstrument().getBigInstrument() != null) {
				ValidationUtils.assertNotNull(security.getBigSecurity(), "Unable to get normalized value for security [" + security.getLabel() + "] and field [" + fieldName
						+ "] because the big security is not set.");
				securityId = security.getBigSecurity().getId();
			}
		}

		MarketDataValueSearchForm searchForm = new MarketDataValueSearchForm();
		searchForm.setInvestmentSecurityId(securityId);
		searchForm.setDataSourceId(dataSourceId);
		//searchForm.setDataFieldName(fieldName);
		if (!flexible) {
			searchForm.setMeasureDate(date);
		}
		List<MarketDataValue> valueList = getMarketDataValueList(searchForm);
		valueList = BeanUtils.sortWithFunction(valueList, MarketDataValue::getMeasureDate, false);
		for (MarketDataValue bean : CollectionUtils.getIterable(valueList)) {
			if (fieldName.equals(bean.getDataField().getName())) {
				Date beanValue = bean.getMeasureDate();
				if (beanValue != null && ((flexible && beanValue.compareTo(date) <= 0) || !flexible && beanValue.compareTo(date) == 0)) {
					return new MarketDataValueHolder(bean);
				}
			}
		}
		return null;
	}
}
