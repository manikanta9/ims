package com.clifton.marketdata.rates;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class MarketDataExchangeRateTests {

	@Test
	public void testLabel() {
		MarketDataSource dataSource = new MarketDataSource();
		dataSource.setName("Bloomberg");
		InvestmentSecurity fromCurrency = new InvestmentSecurity();
		fromCurrency.setSymbol("GBP");
		InvestmentSecurity toCurrency = new InvestmentSecurity();
		toCurrency.setSymbol("USD");

		MarketDataExchangeRate exchangeRate = new MarketDataExchangeRate();
		exchangeRate.setDataSource(dataSource);
		exchangeRate.setFromCurrency(fromCurrency);
		exchangeRate.setToCurrency(toCurrency);
		exchangeRate.setExchangeRate(new BigDecimal("1.6"));
		exchangeRate.setRateDate(DateUtils.toDate("05/21/2010"));

		Assertions.assertEquals("Bloomberg: 1 GBP = 1.6 USD on 05/21/2010", exchangeRate.getLabel());
	}
}
