package com.clifton.marketdata.rates.transformation;

import com.clifton.core.converter.template.FreemarkerTemplateConverter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


/**
 * @author mitchellf
 */
public class MarketDataValueTransformerTests {

	@Test
	public void testFreemarkerBigDecimalValueTransformation() {

		MarketDataValueFreemarkerTransformer valueTransformer = new MarketDataValueFreemarkerTransformer();

		valueTransformer.setTemplateConverter(new FreemarkerTemplateConverter());

		String freemarkerTemplate = "${INDEX_VALUE}*100";
		valueTransformer.setFreemarkerTemplate(freemarkerTemplate);

		BigDecimal result = valueTransformer.transform(BigDecimal.ONE);
		Assertions.assertEquals(result, new BigDecimal("100"));
	}
}
