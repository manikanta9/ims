package com.clifton.marketdata.rates;

import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.marketdata.datasource.MarketDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class MarketDataInterestRateTests {

	@Test
	public void testLabel() {
		MarketDataSource dataSource = new MarketDataSource();
		dataSource.setName("Bloomberg");
		InvestmentInterestRateIndex rateIndex = new InvestmentInterestRateIndex();
		rateIndex.setName("LIBOR");

		MarketDataInterestRate interestRate = new MarketDataInterestRate();
		interestRate.setDataSource(dataSource);
		interestRate.setInterestRateIndex(rateIndex);
		interestRate.setInterestRate(new BigDecimal("1.6"));
		interestRate.setInterestRateDate(DateUtils.toDate("05/21/2010"));

		Assertions.assertEquals("LIBOR from Bloomberg on 05/21/2010 was 1.6", interestRate.getLabel());
	}
}
