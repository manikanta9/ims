package com.clifton.marketdata.field;

import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.validation.FieldValidationException;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.security.authorization.SecurityAuthorizationService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class MarketDataFieldServiceImplTests {

	private static final short FIELD_DURATION = 3;
	private static final short FIELD_UPDATABLE = 4;
	private static final short FIELD_SYSTEM_DEFINED = 5;


	@Resource
	private MarketDataFieldService marketDataFieldService;
	@Resource
	private MarketDataSourceService marketDataSourceService;

	@Resource
	private SecurityAuthorizationService securityAuthorizationService;


	@Test
	public void testSaveMarketDataValue_SettlementPrice() {
		MarketDataSource bloomberg = this.marketDataSourceService.getMarketDataSource(MathUtils.SHORT_ONE);
		Assertions.assertNotNull(bloomberg);
		MarketDataSource citi = this.marketDataSourceService.getMarketDataSource(MathUtils.SHORT_TWO);
		Assertions.assertNotNull(citi);
		MarketDataField settlementPrice = this.marketDataFieldService.getMarketDataField(MathUtils.SHORT_ONE);
		Assertions.assertNotNull(settlementPrice);
		Date now = new Date();
		InvestmentSecurity ibm = new InvestmentSecurity();
		ibm.setId(1);

		// test successful insert
		MarketDataValue bean = new MarketDataValue();
		bean.setDataField(settlementPrice);
		bean.setDataSource(bloomberg);
		bean.setMeasureDate(now);
		bean.setMeasureValue(new BigDecimal(124));
		bean.setInvestmentSecurity(ibm);

		Assertions.assertTrue(bean.isNewBean());
		this.marketDataFieldService.saveMarketDataValue(bean);
		Assertions.assertFalse(bean.isNewBean());

		// test second one on same date failure
		now = new Date();
		bean = new MarketDataValue();
		bean.setDataField(settlementPrice);
		bean.setDataSource(citi);
		bean.setMeasureDate(now);
		bean.setMeasureValue(new BigDecimal("548.12345"));
		bean.setInvestmentSecurity(ibm);

		Assertions.assertTrue(bean.isNewBean());
		this.marketDataFieldService.saveMarketDataValue(bean);
		Assertions.assertFalse(bean.isNewBean());
	}


	@Test
	public void testSaveMarketDataValue_Bid() {
		MarketDataSource bloomberg = this.marketDataSourceService.getMarketDataSource(MathUtils.SHORT_ONE);
		Assertions.assertNotNull(bloomberg);
		MarketDataField bid = this.marketDataFieldService.getMarketDataField(MathUtils.SHORT_TWO);
		Assertions.assertNotNull(bid);
		Date now = new Date();
		InvestmentSecurity ibm = new InvestmentSecurity();
		ibm.setId(1);

		// test invalid precision
		MarketDataValue bean = new MarketDataValue();
		bean.setDataField(bid);
		bean.setDataSource(bloomberg);
		bean.setMeasureDate(now);
		bean.setMeasureTime(new Time(now));
		bean.setMeasureValue(new BigDecimal("124.1"));
		bean.setInvestmentSecurity(ibm);

		Assertions.assertTrue(bean.isNewBean());
		try {
			this.marketDataFieldService.saveMarketDataValue(bean);
			Assertions.fail("save should have failed");
		}
		catch (ValidationException e) {
			Assertions.assertEquals("'Measure Value' 124.1 cannot have more than 0 decimal places", e.getMessage());
		}

		// test successful insert
		bean.setMeasureValue(new BigDecimal("124"));
		Assertions.assertTrue(bean.isNewBean());
		this.marketDataFieldService.saveMarketDataValue(bean);
		Assertions.assertFalse(bean.isNewBean());
	}


	@Test
	public void testSaveMarketDataValue_PrecisionCheck() {
		MarketDataSource bloomberg = this.marketDataSourceService.getMarketDataSource(MathUtils.SHORT_ONE);
		Assertions.assertNotNull(bloomberg);
		MarketDataField duration = this.marketDataFieldService.getMarketDataField(FIELD_DURATION);
		Assertions.assertNotNull(duration);

		// test successful insert
		MarketDataValue bean = new MarketDataValue();
		bean.setDataField(duration);
		bean.setDataSource(bloomberg);
		bean.setMeasureDate(new Date());
		bean.setMeasureTime(new Time(bean.getMeasureDate()));
		bean.setMeasureValue(new BigDecimal("100.123400000000"));
		bean.setInvestmentSecurity(new InvestmentSecurity("IBM", 1));

		Assertions.assertTrue(bean.isNewBean());
		this.marketDataFieldService.saveMarketDataValue(bean);
		Assertions.assertFalse(bean.isNewBean());

		TestUtils.expectException(FieldValidationException.class, () -> {
			bean.setMeasureValue(new BigDecimal("100.12345"));
			this.marketDataFieldService.saveMarketDataValue(bean);
		}, "'Measure Value' 100.12345 cannot have more than 4 decimal places");
	}


	@Test
	public void testSaveMarketDataValueCaptureChangesOnlyInsertAndUpdate() {
		MarketDataFieldService mock = Mockito.spy(this.marketDataFieldService);
		Mockito.doReturn(null).when(mock).getMarketDataValueList(ArgumentMatchers.any(MarketDataValueSearchForm.class));

		MarketDataSource bloomberg = this.marketDataSourceService.getMarketDataSource(MathUtils.SHORT_ONE);
		Assertions.assertNotNull(bloomberg);
		MarketDataField duration = this.marketDataFieldService.getMarketDataField(MathUtils.SHORT_THREE);
		Assertions.assertNotNull(duration);
		InvestmentSecurity ibm = new InvestmentSecurity();
		ibm.setId(1);

		// insert a value
		MarketDataValue bean = new MarketDataValue();
		bean.setDataField(duration);
		bean.setDataSource(bloomberg);
		bean.setMeasureDate(DateUtils.toDate("8/08/2011"));
		bean.setMeasureTime(new Time(0));
		bean.setMeasureValue(new BigDecimal("5.21"));
		bean.setInvestmentSecurity(ibm);

		mock.saveMarketDataValue(bean);
		Assertions.assertFalse(bean.isNewBean());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(5.21), bean.getMeasureValue()));

		// Try to insert a new bean with the same value - shouldn't insert - just updates update date/time of previous bean
		MarketDataValue bean2 = new MarketDataValue();
		bean2.setDataField(duration);
		bean2.setDataSource(bloomberg);
		bean2.setMeasureDate(DateUtils.toDate("8/09/2011"));
		bean2.setMeasureTime(new Time(0));
		bean2.setMeasureValue(new BigDecimal("5.21"));
		bean2.setInvestmentSecurity(ibm);

		Mockito.doReturn(CollectionUtils.createList(bean)).when(mock).getMarketDataValueList(ArgumentMatchers.any(MarketDataValueSearchForm.class));
		mock.saveMarketDataValue(bean2);
		Assertions.assertEquals(bean2, bean);

		// Insert another bean with a different value - should insert
		MarketDataValue bean3 = new MarketDataValue();
		bean3.setDataField(duration);
		bean3.setDataSource(bloomberg);
		bean3.setMeasureDate(DateUtils.toDate("8/09/2011"));
		bean3.setMeasureTime(new Time(0));
		bean3.setMeasureValue(new BigDecimal("5.19"));
		bean3.setInvestmentSecurity(ibm);

		mock.saveMarketDataValue(bean3);
		Assertions.assertFalse(bean3.isNewBean());
		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(5.19), bean3.getMeasureValue()));

		// Update the measure value - Should update, even though setting it to previous value, we are still updating an existing bean
		bean3.setMeasureValue(BigDecimal.valueOf(5.21));
		Mockito.doReturn(CollectionUtils.createList(bean)).when(mock).getMarketDataValueList(ArgumentMatchers.any(MarketDataValueSearchForm.class));
		mock.saveMarketDataValue(bean3);

		Assertions.assertTrue(MathUtils.isEqual(BigDecimal.valueOf(5.21), bean3.getMeasureValue()));
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@Test
	public void testUpdateMarketDataFieldNameWhenNonAdminSucceeds() {
		setupTest(false);
		final MarketDataField updatable = this.marketDataFieldService.getMarketDataField(FIELD_UPDATABLE);
		Assertions.assertNotNull(updatable);
		updatable.setName("Test");
		this.marketDataFieldService.saveMarketDataField(updatable);
		final MarketDataField updated = this.marketDataFieldService.getMarketDataField(FIELD_UPDATABLE);
		Assertions.assertEquals("Test", updated.getName());
	}


	@Test
	public void testUpdateSystemDefinedMarketDataFieldNameWhenAdminFails() {
		setupTest(true);
		final MarketDataField systemDefined = this.marketDataFieldService.getMarketDataField(FIELD_SYSTEM_DEFINED);
		Assertions.assertNotNull(systemDefined);
		systemDefined.setName("Test");
		TestUtils.expectException(FieldValidationException.class, () -> this.marketDataFieldService.saveMarketDataField(systemDefined), "System Defined Market Data Field Names cannot be changed.");
	}


	@Test
	public void testUpdateSystemDefinedMarketDataFieldExternalNameWhenNotAdminFails() {
		setupTest(false);
		final MarketDataField systemDefined = this.marketDataFieldService.getMarketDataField(FIELD_SYSTEM_DEFINED);
		Assertions.assertNotNull(systemDefined);
		systemDefined.setExternalFieldName("Test");
		TestUtils.expectException(ValidationException.class, () -> this.marketDataFieldService.saveMarketDataField(systemDefined), "You must be an admin user to UPDATE this MarketDataField.  Modify Condition has not been selected to validate additional security, but is required for Non Admin Users.");
	}


	@Test
	public void testUpdateSystemDefinedMarketDataFieldExternalNameWhenAdminSucceeds() {
		setupTest(true);
		final MarketDataField systemDefined = this.marketDataFieldService.getMarketDataField(FIELD_SYSTEM_DEFINED);
		Assertions.assertNotNull(systemDefined);
		systemDefined.setExternalFieldName("Test");
		this.marketDataFieldService.saveMarketDataField(systemDefined);
	}


	@Test
	public void testUpdateMarketDataFieldSystemDefinedFlagFails() {
		setupTest(true);
		final MarketDataField updatable = this.marketDataFieldService.getMarketDataField(FIELD_UPDATABLE);
		Assertions.assertNotNull(updatable);
		updatable.setSystemDefined(true);
		saveExpectingValidationException(updatable);
	}


	@Test
	public void testUpdateSystemDefinedMarketDataFieldSystemDefinedFlagWhenAdminFails() {
		setupTest(true);
		final MarketDataField systemDefined = this.marketDataFieldService.getMarketDataField(FIELD_SYSTEM_DEFINED);
		Assertions.assertNotNull(systemDefined);
		systemDefined.setSystemDefined(false);
		saveExpectingValidationException(systemDefined);
	}


	@Test
	public void testSaveSystemDefinedMarketDataFieldWhenAdminFails() {
		setupTest(true);
		final MarketDataField newSystemDefinedField = new MarketDataField();
		newSystemDefinedField.setName("Test");
		newSystemDefinedField.setSystemDefined(true);
		TestUtils.expectException(ValidationException.class, () -> this.marketDataFieldService.saveMarketDataField(newSystemDefinedField), "Adding new System Defined Market Data Fields is not allowed.");
	}


	@Test
	public void testDeleteSystemDefinedMarketDataFieldWhenAdminFails() {
		setupTest(true);
		TestUtils.expectException(ValidationException.class, () -> this.marketDataFieldService.deleteMarketDataField(FIELD_SYSTEM_DEFINED), "Deleting System Defined Market Data Fields is not allowed.");
	}


	private void setupTest(boolean isAdmin) {
		Mockito.when(this.securityAuthorizationService.isSecurityUserAdmin()).thenReturn(isAdmin);
	}


	private void saveExpectingValidationException(final MarketDataField field) {
		TestUtils.expectException(ValidationException.class, () -> this.marketDataFieldService.saveMarketDataField(field), "Modifications to the given fields are not allowed: [systemDefined]");
	}
}
