package com.clifton.marketdata.field;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.datasource.MarketDataSource;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;


public class MarketDataValueTests {

	@Test
	public void testLabel() {
		MarketDataSource dataSource = new MarketDataSource();
		dataSource.setName("Bloomberg");
		InvestmentSecurity security = new InvestmentSecurity();
		security.setSymbol("IBM");
		MarketDataField dataField = new MarketDataField();
		dataField.setName("Open Price");
		dataField.setDecimalPrecision(1);

		MarketDataValue dataValue = new MarketDataValue();
		dataValue.setDataSource(dataSource);
		dataValue.setInvestmentSecurity(security);
		dataValue.setDataField(dataField);
		dataValue.setMeasureDate(DateUtils.toDate("05/21/2010"));
		dataValue.setMeasureValue(new BigDecimal("100.5"));

		Assertions.assertEquals("Open Price for IBM from Bloomberg on 05/21/2010 was 100.5", dataValue.getLabel());

		dataValue.setMeasureTime(new Time(125 * 60 * 1000));
		Assertions.assertEquals("Open Price for IBM from Bloomberg on 05/21/2010 2:05 AM was 100.5", dataValue.getLabel());
	}


	@Test
	public void testMeasureDateWithTime() {
		MarketDataValue dataValue = new MarketDataValue();
		dataValue.setMeasureDate(DateUtils.toDate("05/21/2010"));

		Assertions.assertEquals("2010-05-21 00:00:00", DateUtils.fromDate(dataValue.getMeasureDateWithTime()));

		dataValue.setMeasureTime(new Time(125 * 60 * 1000));
		Assertions.assertEquals("2010-05-21 02:05:00", DateUtils.fromDate(dataValue.getMeasureDateWithTime()));
	}
}
