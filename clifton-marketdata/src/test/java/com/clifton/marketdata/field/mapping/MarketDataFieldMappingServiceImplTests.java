package com.clifton.marketdata.field.mapping;

import com.clifton.core.test.XmlDaoTransactionalExtension;
import com.clifton.core.util.AssertUtils;
import com.clifton.core.util.StringUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;


/**
 * @author manderson
 */
@ContextConfiguration
@ExtendWith({SpringExtension.class, XmlDaoTransactionalExtension.class})
public class MarketDataFieldMappingServiceImplTests {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Resource
	private InvestmentSetupService investmentSetupService;

	@Resource
	private MarketDataFieldService marketDataFieldService;

	@Resource
	private MarketDataSourceService marketDataSourceService;

	@Resource
	private MarketDataFieldMappingService marketDataFieldMappingService;

	@Resource
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	////////////////////////////////////////////////////////////////////////////////

	private static final Short INVESTMENT_TYPE_BONDS = 1;

	private static final Short SUB_TYPE_GENERIC_BONDS = 10;
	private static final Short SUB_TYPE_CENTRAL_GOVERNMENT_BONDS = 11;

	private static final Short SUB_TYPE_2_BONDS_BILLS = 10;

	private static final Short HIERARCHY_BONDS = 1;

	private static final Integer INSTRUMENT_BOND = 1;

	private static final Short MARKET_DATA_FIELD_LAST_TRADE_PRICE = 2;
	private static final Short MARKET_DATA_FIELD_SETTLEMENT_PRICE = 1;

	private static final Short MARKET_DATA_SOURCE_BLOOMBERG = 1;
	private static final Short MARKET_DATA_SOURCE_PPA = 2;

	////////////////////////////////////////////////////////////////////////////////
	///////////          Market Data Price Field Mapping Tests       ///////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testSaveMarketDataPriceFieldMappingWithMissingData() {
		// No Specificity At All
		MarketDataPriceFieldMapping bean = new MarketDataPriceFieldMapping();
		saveMarketDataPriceFieldMapping(bean, "At least Investment Type, Instrument Hierarchy, or Instrument is Required.");

		// No Price Field Selections
		bean.setInvestmentType(this.investmentSetupService.getInvestmentType(INVESTMENT_TYPE_BONDS));
		saveMarketDataPriceFieldMapping(bean, "At least one price field is required.");
	}


	@Test
	public void testSaveMarketDataPriceFieldMappingWithInvalidSpecificity() {
		// Set Investment Instrument and Hierarchy
		MarketDataPriceFieldMapping bean = createMarketDataPriceFieldMapping(INSTRUMENT_BOND, HIERARCHY_BONDS, null, null, null);
		saveMarketDataPriceFieldMapping(bean, "Hierarchy can not be selected if a specific instrument is selected");

		// Set Instrument and Type
		bean = createMarketDataPriceFieldMapping(INSTRUMENT_BOND, null, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataPriceFieldMapping(bean, "Investment Type can not be selected if a specific instrument is selected");

		// Set Hierarchy and Type
		bean = createMarketDataPriceFieldMapping(null, HIERARCHY_BONDS, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataPriceFieldMapping(bean, "Investment Type can not be selected if a specific hierarchy is selected");

		// Set SubType without Type
		bean = createMarketDataPriceFieldMapping(null, null, null, SUB_TYPE_GENERIC_BONDS, null);
		saveMarketDataPriceFieldMapping(bean, "Sub Type can only be selected if Investment Type is selected");

		// Set SubType2 without Type
		bean = createMarketDataPriceFieldMapping(null, null, null, null, SUB_TYPE_2_BONDS_BILLS);
		saveMarketDataPriceFieldMapping(bean, "Sub Type 2 can only be selected if Investment Type is selected");
	}


	@Test
	public void testSaveMarketDataPriceFieldMappingWithOverlappingSpecificity() {
		// Type
		MarketDataPriceFieldMapping typeBean = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataPriceFieldMapping(typeBean, null);

		// Version 2 - Set start Date
		MarketDataPriceFieldMapping overlappingTypeBean = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataPriceFieldMapping(overlappingTypeBean, "There already exists a mapping for the same specificity [ID: " + typeBean.getId() + ", Label: Type: Bond]");

		// Type and SubType
		MarketDataPriceFieldMapping typeSubTypeBean = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_GENERIC_BONDS, null);
		saveMarketDataPriceFieldMapping(typeSubTypeBean, null);

		MarketDataPriceFieldMapping overlappingTypeSubTypeBean = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_GENERIC_BONDS, null);
		saveMarketDataPriceFieldMapping(overlappingTypeSubTypeBean, "There already exists a mapping for the same specificity [ID: " + typeSubTypeBean.getId() + ", Label: Type: Bond, SubType: Generic Bonds]");

		// Hierarchy
		MarketDataPriceFieldMapping hierarchyBean = createMarketDataPriceFieldMapping(null, HIERARCHY_BONDS, null, null, null);
		saveMarketDataPriceFieldMapping(hierarchyBean, null);

		MarketDataPriceFieldMapping overlappingHierarchyBean = createMarketDataPriceFieldMapping(null, HIERARCHY_BONDS, null, null, null);
		saveMarketDataPriceFieldMapping(overlappingHierarchyBean, "There already exists a mapping for the same specificity [ID: " + hierarchyBean.getId() + ", Label: Hierarchy: Bond]");

		// Instrument
		MarketDataPriceFieldMapping instrumentBean = createMarketDataPriceFieldMapping(INSTRUMENT_BOND, null, null, null, null);
		saveMarketDataPriceFieldMapping(instrumentBean, null);

		MarketDataPriceFieldMapping overlappingInstrumentBean = createMarketDataPriceFieldMapping(INSTRUMENT_BOND, null, null, null, null);
		saveMarketDataPriceFieldMapping(overlappingInstrumentBean, "There already exists a mapping for the same specificity [ID: " + instrumentBean.getId() + ", Label: Instrument: Test Bond (Bond)]");
	}


	@Test
	public void testSaveMarketDataFieldMappingWithOverlappingSpecificity() {
		// Type
		MarketDataFieldMapping typeBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, null, null, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataFieldMapping(typeBean, null);

		MarketDataFieldMapping overlappingTypeBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, null, null, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataFieldMapping(overlappingTypeBean, "A data field mapping with the specified parameters already exists for datasource [Bloomberg].");

		// Try again - different datasource
		overlappingTypeBean.setMarketDataSource(this.marketDataSourceService.getMarketDataSource(MARKET_DATA_SOURCE_PPA));
		saveMarketDataFieldMapping(overlappingTypeBean, null);

		// Type and SubType
		MarketDataFieldMapping typeSubTypeBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_GENERIC_BONDS, null);
		saveMarketDataFieldMapping(typeSubTypeBean, null);

		MarketDataFieldMapping overlappingTypeSubTypeBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_GENERIC_BONDS, null);
		saveMarketDataFieldMapping(overlappingTypeSubTypeBean, "A data field mapping with the specified parameters already exists for datasource [Bloomberg].");

		// Try again - different datasource
		overlappingTypeSubTypeBean.setMarketDataSource(this.marketDataSourceService.getMarketDataSource(MARKET_DATA_SOURCE_PPA));
		saveMarketDataFieldMapping(overlappingTypeSubTypeBean, null);

		// Hierarchy
		MarketDataFieldMapping hierarchyBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, null, HIERARCHY_BONDS, null, null, null);
		saveMarketDataFieldMapping(hierarchyBean, null);

		MarketDataFieldMapping overlappingHierarchyBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, null, HIERARCHY_BONDS, null, null, null);
		saveMarketDataFieldMapping(overlappingHierarchyBean, "A data field mapping with the specified parameters already exists for datasource [Bloomberg].");

		// Try again - different datasource
		overlappingHierarchyBean.setMarketDataSource(this.marketDataSourceService.getMarketDataSource(MARKET_DATA_SOURCE_PPA));
		saveMarketDataFieldMapping(overlappingHierarchyBean, null);

		// Instrument
		MarketDataFieldMapping instrumentBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, INSTRUMENT_BOND, null, null, null, null);
		saveMarketDataFieldMapping(instrumentBean, null);

		MarketDataFieldMapping overlappingInstrumentBean = createMarketDataFieldMapping(MARKET_DATA_SOURCE_BLOOMBERG, INSTRUMENT_BOND, null, null, null, null);
		saveMarketDataFieldMapping(overlappingInstrumentBean, "A data field mapping with the specified parameters already exists for datasource [Bloomberg].");

		// Try again - different datasource
		overlappingInstrumentBean.setMarketDataSource(this.marketDataSourceService.getMarketDataSource(MARKET_DATA_SOURCE_PPA));
		saveMarketDataFieldMapping(overlappingInstrumentBean, null);
	}


	@Test
	public void testGetMarketDataPriceFieldMappingForInstrument() {
		// Type
		MarketDataPriceFieldMapping typeMapping = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, null, null);
		saveMarketDataPriceFieldMapping(typeMapping, null);

		// Type and SubType
		MarketDataPriceFieldMapping typeSubTypeMapping = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_GENERIC_BONDS, null);
		saveMarketDataPriceFieldMapping(typeSubTypeMapping, null);

		// Add a Second Type/SubType Mapping that Won't be Used for this instrument
		MarketDataPriceFieldMapping unusedTypeSubTypeMapping = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_CENTRAL_GOVERNMENT_BONDS, null);
		saveMarketDataPriceFieldMapping(unusedTypeSubTypeMapping, null);

		// Type and SubType2
		MarketDataPriceFieldMapping typeSubType2Mapping = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, null, SUB_TYPE_2_BONDS_BILLS);
		saveMarketDataPriceFieldMapping(typeSubType2Mapping, null);

		// Type and Sub Type and Sub Type 2
		MarketDataPriceFieldMapping typeSubTypeSubType2Mapping = createMarketDataPriceFieldMapping(null, null, INVESTMENT_TYPE_BONDS, SUB_TYPE_GENERIC_BONDS, SUB_TYPE_2_BONDS_BILLS);
		saveMarketDataPriceFieldMapping(typeSubTypeSubType2Mapping, null);

		// Hierarchy
		MarketDataPriceFieldMapping hierarchyMapping = createMarketDataPriceFieldMapping(null, HIERARCHY_BONDS, null, null, null);
		saveMarketDataPriceFieldMapping(hierarchyMapping, null);

		// Instrument
		MarketDataPriceFieldMapping instrumentMapping = createMarketDataPriceFieldMapping(INSTRUMENT_BOND, null, null, null, null);
		saveMarketDataPriceFieldMapping(instrumentMapping, null);

		// INSTRUMENT MAPPING SHOULD BE RETURNED
		InvestmentInstrument instrument = this.investmentInstrumentService.getInvestmentInstrument(INSTRUMENT_BOND);
		MarketDataPriceFieldMapping specificityMapping = this.marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(instrument);
		Assertions.assertEquals(instrumentMapping, specificityMapping);

		// DELETE INSTRUMENT MAPPING - HIERARCHY SHOULD BE RETURNED
		this.marketDataFieldMappingService.deleteMarketDataPriceFieldMapping(instrumentMapping.getId());
		specificityMapping = this.marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(instrument);
		Assertions.assertEquals(hierarchyMapping, specificityMapping);

		// DELETE HIERARCHY - TYPE/SUBTYPE/SUBTYPE2 SHOULD BE RETURNED
		this.marketDataFieldMappingService.deleteMarketDataPriceFieldMapping(hierarchyMapping.getId());
		specificityMapping = this.marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(instrument);
		Assertions.assertEquals(typeSubTypeSubType2Mapping, specificityMapping);

		// DELETE TYPE/SUBTYPE/SUBTYPE2 - TYPE/SUBTYPE2 SHOULD BE RETURNED
		this.marketDataFieldMappingService.deleteMarketDataPriceFieldMapping(typeSubTypeSubType2Mapping.getId());
		specificityMapping = this.marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(instrument);
		Assertions.assertEquals(typeSubType2Mapping, specificityMapping);

		// DELETE TYPE/SUBTYPE2 - TYPE/SUBTYPE2 SHOULD BE RETURNED
		this.marketDataFieldMappingService.deleteMarketDataPriceFieldMapping(typeSubType2Mapping.getId());
		specificityMapping = this.marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(instrument);
		Assertions.assertEquals(typeSubTypeMapping, specificityMapping);

		// DELETE TYPE/SUBTYPE - TYPE SHOULD BE RETURNED
		this.marketDataFieldMappingService.deleteMarketDataPriceFieldMapping(typeSubTypeMapping.getId());
		specificityMapping = this.marketDataFieldMappingRetriever.getMarketDataPriceFieldMappingByInstrument(instrument);
		Assertions.assertEquals(typeMapping, specificityMapping);
	}


	////////////////////////////////////////////////////////////////////////////////
	////////////          Market Data Price Field Types Tests       ////////////////
	////////////////////////////////////////////////////////////////////////////////


	@Test
	public void testMarketDataPriceFieldTypes() {
		MarketDataField lastTradePrice = this.marketDataFieldService.getMarketDataField(MARKET_DATA_FIELD_LAST_TRADE_PRICE);
		MarketDataField settlementPrice = this.marketDataFieldService.getMarketDataField(MARKET_DATA_FIELD_SETTLEMENT_PRICE);

		MarketDataSource bloombergDataSource = this.marketDataSourceService.getMarketDataSource(MARKET_DATA_SOURCE_BLOOMBERG);
		MarketDataSource ppaDataSource = this.marketDataSourceService.getMarketDataSource(MARKET_DATA_SOURCE_PPA);

		// Override Closing Only
		MarketDataPriceFieldMapping mapping = new MarketDataPriceFieldMapping();
		mapping.setClosingPriceMarketDataField(settlementPrice);
		Assertions.assertNull(MarketDataPriceFieldTypes.OFFICIAL.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSourceId(mapping));

		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.CLOSING.getPriceDataSourceId(mapping));

		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.LATEST.getPriceDataSourceId(mapping));

		// Override Official and Closing Price Fields
		mapping = new MarketDataPriceFieldMapping();
		mapping.setClosingPriceMarketDataField(settlementPrice);
		mapping.setOfficialPriceMarketDataField(settlementPrice);
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSourceId(mapping));

		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.CLOSING.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.CLOSING.getPriceDataSourceId(mapping));

		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.LATEST.getPriceDataSourceId(mapping));

		// Add Data Source to Official
		mapping.setOfficialPriceMarketDataSource(bloombergDataSource);
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertEquals(bloombergDataSource, MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSource(mapping));

		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceField(mapping, this.marketDataFieldService));
		// Returns result because Closing Is Less Restrictive than Official
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.CLOSING.getPriceDataSourceId(mapping));

		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.LATEST.getPriceDataSourceId(mapping));

		// Add Different DataSource to Closing
		mapping.setClosingPriceMarketDataSource(ppaDataSource);
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertEquals(bloombergDataSource, MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSource(mapping));

		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceField(mapping, this.marketDataFieldService));
		// Returns result because Closing Is Different Data Source than Official
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertEquals(ppaDataSource, MarketDataPriceFieldTypes.CLOSING.getPriceDataSource(mapping));

		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.LATEST.getPriceDataSourceId(mapping));

		// Remove Data Source From Official
		mapping.setOfficialPriceMarketDataSource(null);
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.OFFICIAL.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.OFFICIAL.getPriceDataSource(mapping));

		Assertions.assertEquals(settlementPrice, MarketDataPriceFieldTypes.CLOSING.getPriceField(mapping, this.marketDataFieldService));
		// DOES NOT Return result because Closing Is More Restrictive Than Official
		Assertions.assertNull(MarketDataPriceFieldTypes.CLOSING.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertEquals(ppaDataSource, MarketDataPriceFieldTypes.CLOSING.getPriceDataSource(mapping));

		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceField(mapping, this.marketDataFieldService));
		Assertions.assertEquals(lastTradePrice, MarketDataPriceFieldTypes.LATEST.getPriceFieldIfDifferent(mapping, this.marketDataFieldService));
		Assertions.assertNull(MarketDataPriceFieldTypes.LATEST.getPriceDataSourceId(mapping));
	}

	////////////////////////////////////////////////////////////////////////////////
	///////////                  Helper Methods                     ////////////////
	////////////////////////////////////////////////////////////////////////////////


	private MarketDataPriceFieldMapping createMarketDataPriceFieldMapping(Integer instrumentId, Short hierarchyId, Short typeId, Short subTypeId, Short subType2Id) {
		MarketDataPriceFieldMapping bean = new MarketDataPriceFieldMapping();
		if (instrumentId != null) {
			bean.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(instrumentId));
		}
		if (hierarchyId != null) {
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(hierarchyId));
		}
		if (typeId != null) {
			bean.setInvestmentType(this.investmentSetupService.getInvestmentType(typeId));
		}
		if (subTypeId != null) {
			bean.setInvestmentTypeSubType(this.investmentSetupService.getInvestmentTypeSubType(subTypeId));
		}
		if (subType2Id != null) {
			bean.setInvestmentTypeSubType2(this.investmentSetupService.getInvestmentTypeSubType2(subType2Id));
		}

		bean.setLatestPriceMarketDataField(this.marketDataFieldService.getMarketDataField(MARKET_DATA_FIELD_LAST_TRADE_PRICE));
		bean.setClosingPriceMarketDataField(this.marketDataFieldService.getMarketDataField(MARKET_DATA_FIELD_SETTLEMENT_PRICE));
		return bean;
	}


	private void saveMarketDataPriceFieldMapping(MarketDataPriceFieldMapping bean, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			this.marketDataFieldMappingService.saveMarketDataPriceFieldMapping(bean);
		}
		catch (Exception e) {
			errorMessage = e.getMessage();
		}
		if (StringUtils.isEmpty(expectedErrorMessage)) {
			AssertUtils.assertNull(errorMessage, "Did not expect an error message during save of MarketDataPriceFieldMapping bean, but received one with message " + errorMessage);
		}
		else {
			Assertions.assertEquals(expectedErrorMessage, errorMessage);
		}
	}


	private MarketDataFieldMapping createMarketDataFieldMapping(Short dataSourceId, Integer instrumentId, Short hierarchyId, Short typeId, Short subTypeId, Short subType2Id) {
		MarketDataFieldMapping bean = new MarketDataFieldMapping();
		bean.setMarketDataSource(this.marketDataSourceService.getMarketDataSource(dataSourceId));
		if (instrumentId != null) {
			bean.setInstrument(this.investmentInstrumentService.getInvestmentInstrument(instrumentId));
		}
		if (hierarchyId != null) {
			bean.setInstrumentHierarchy(this.investmentSetupService.getInvestmentInstrumentHierarchy(hierarchyId));
		}
		if (typeId != null) {
			bean.setInvestmentType(this.investmentSetupService.getInvestmentType(typeId));
		}
		if (subTypeId != null) {
			bean.setInvestmentTypeSubType(this.investmentSetupService.getInvestmentTypeSubType(subTypeId));
		}
		if (subType2Id != null) {
			bean.setInvestmentTypeSubType2(this.investmentSetupService.getInvestmentTypeSubType2(subType2Id));
		}
		return bean;
	}


	private void saveMarketDataFieldMapping(MarketDataFieldMapping bean, String expectedErrorMessage) {
		String errorMessage = null;
		try {
			this.marketDataFieldMappingService.saveMarketDataFieldMapping(bean);
		}
		catch (Exception e) {
			errorMessage = e.getMessage();
		}
		if (StringUtils.isEmpty(expectedErrorMessage)) {
			AssertUtils.assertNull(errorMessage, "Did not expect an error message during save of MarketDataFieldMapping bean, but received one with message " + errorMessage);
		}
		else {
			Assertions.assertEquals(expectedErrorMessage, errorMessage);
		}
	}
}
