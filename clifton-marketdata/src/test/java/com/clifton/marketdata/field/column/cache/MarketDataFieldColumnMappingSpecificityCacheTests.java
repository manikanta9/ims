package com.clifton.marketdata.field.column.cache;

import com.clifton.core.cache.specificity.SpecificityTestObjectFactory;
import com.clifton.core.dataaccess.dao.locator.DaoLocator;
import com.clifton.core.dataaccess.dao.xml.XmlReadOnlyDAO;
import com.clifton.core.util.CollectionUtils;
import com.clifton.investment.instrument.InvestmentInstrument;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.InvestmentSetupService;
import com.clifton.investment.setup.InvestmentType;
import com.clifton.investment.setup.InvestmentTypeSubType;
import com.clifton.investment.setup.InvestmentTypeSubType2;
import com.clifton.investment.setup.search.InstrumentHierarchySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldService;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMapping;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingCommand;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingSearchForm;
import com.clifton.marketdata.field.column.MarketDataFieldColumnMappingService;
import com.clifton.marketdata.field.search.MarketDataFieldGroupSearchForm;
import com.clifton.system.schema.SystemTable;
import com.clifton.system.schema.column.SystemColumn;
import com.clifton.system.schema.column.SystemColumnCustom;
import com.clifton.system.schema.column.SystemColumnService;
import com.clifton.system.schema.column.SystemColumnTemplate;
import com.clifton.system.schema.column.search.SystemColumnSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class MarketDataFieldColumnMappingSpecificityCacheTests {

	private MarketDataFieldColumnMappingSpecificityCache cache;

	@Mock
	private MarketDataFieldColumnMappingService marketDataFieldColumnMappingService;

	@Mock
	private MarketDataFieldService marketDataFieldService;

	@Mock
	private InvestmentSetupService investmentSetupService;

	@Mock
	private SystemColumnService systemColumnService;

	@Mock
	private DaoLocator daoLocator;

	@SuppressWarnings("rawtypes")
	@Mock
	private XmlReadOnlyDAO mockDao;


	private static SystemTable INVESTMENT_SECURITY_TABLE;
	private static SystemTable INVESTMENT_INSTRUMENT_TABLE;
	private static MarketDataSource DATASOURCE;

	private static SystemColumn SECURITY_START_DATE_COLUMN;
	private static SystemColumn INSTRUMENT_END_DATE_COLUMN;
	private static SystemColumn SECURITY_END_DATE_COLUMN;
	private static SystemColumn GROUP_DATE_COLUMN;
	private static SystemColumn DAY_COUNT_COLUMN1;
	private static SystemColumn DAY_COUNT_COLUMN2;
	private static SystemColumn DAY_COUNT_COLUMN3;

	private static List<SystemColumnCustom> DAY_COUNT_COLUMN_LIST;

	private static SystemColumnTemplate DAY_COUNT_TEMPLATE;

	private static MarketDataField DATE_FIELD;
	private static MarketDataField ISSUE_DATE_FIELD;
	private static MarketDataField ISSUE_DATE_FIELD2;
	private static MarketDataField INSTRUMENT_END_DATE_FIELD;
	private static MarketDataField DAY_COUNT_FIELD;

	private static MarketDataFieldGroup TEST_FIELD_GROUP;

	private static InvestmentType INVESTMENT_TYPE_SWAPTIONS;
	private static InvestmentType INVESTMENT_TYPE_BENCHMARKS;

	private static InvestmentTypeSubType INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS;
	private static InvestmentTypeSubType2 INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS;

	private static InvestmentInstrumentHierarchy FIXED_INCOME_SWAPS_HIERARCHY;
	private static InvestmentInstrumentHierarchy HIERARCHY_WITH_CHILDREN;
	private static InvestmentInstrumentHierarchy HIERARCHY_CHILD1;
	private static InvestmentInstrumentHierarchy HIERARCHY_CHILD2;
	private static InvestmentInstrumentHierarchy BENCHMARK_CREDIT_INDEX_HIERARCHY;


	static {
		DATASOURCE = new MarketDataSource();
		DATASOURCE.setId((short) 1);
		DATASOURCE.setName("Default Source");

		INVESTMENT_SECURITY_TABLE = new SystemTable();
		INVESTMENT_SECURITY_TABLE.setName("InvestmentSecurity");
		INVESTMENT_SECURITY_TABLE.setId((short) 20);

		INVESTMENT_INSTRUMENT_TABLE = new SystemTable();
		INVESTMENT_INSTRUMENT_TABLE.setName("InvestmentInstrument");
		INVESTMENT_INSTRUMENT_TABLE.setId((short) 30);

		//////////////////////////////////////////////////////////////////////

		DAY_COUNT_TEMPLATE = new SystemColumnTemplate();
		DAY_COUNT_TEMPLATE.setId(45);
		DAY_COUNT_TEMPLATE.setName("Day Count");

		//////////////////////////////////////////////////////////////////////

		SECURITY_START_DATE_COLUMN = new SystemColumnCustom();
		SECURITY_START_DATE_COLUMN.setId(10);
		SECURITY_START_DATE_COLUMN.setTable(INVESTMENT_SECURITY_TABLE);
		SECURITY_START_DATE_COLUMN.setName("StartDate");

		INSTRUMENT_END_DATE_COLUMN = new SystemColumnCustom();
		INSTRUMENT_END_DATE_COLUMN.setId(15);
		INSTRUMENT_END_DATE_COLUMN.setTable(INVESTMENT_INSTRUMENT_TABLE);
		INSTRUMENT_END_DATE_COLUMN.setName("StartDate");

		SECURITY_END_DATE_COLUMN = new SystemColumnCustom();
		SECURITY_END_DATE_COLUMN.setId(20);
		SECURITY_END_DATE_COLUMN.setTable(INVESTMENT_SECURITY_TABLE);
		SECURITY_END_DATE_COLUMN.setName("EndDate");


		GROUP_DATE_COLUMN = new SystemColumnCustom();
		GROUP_DATE_COLUMN.setId(12);
		GROUP_DATE_COLUMN.setTable(INVESTMENT_SECURITY_TABLE);
		GROUP_DATE_COLUMN.setName("StartDate");


		DAY_COUNT_COLUMN_LIST = new ArrayList<>();
		DAY_COUNT_COLUMN1 = new SystemColumnCustom();
		DAY_COUNT_COLUMN1.setId(13);
		DAY_COUNT_COLUMN1.setTable(INVESTMENT_SECURITY_TABLE);
		DAY_COUNT_COLUMN1.setName("Day Count");
		DAY_COUNT_COLUMN1.setTemplate(DAY_COUNT_TEMPLATE);
		DAY_COUNT_COLUMN_LIST.add((SystemColumnCustom) DAY_COUNT_COLUMN1);

		DAY_COUNT_COLUMN2 = new SystemColumnCustom();
		DAY_COUNT_COLUMN2.setId(14);
		DAY_COUNT_COLUMN2.setTable(INVESTMENT_SECURITY_TABLE);
		DAY_COUNT_COLUMN2.setName("Day Count");
		DAY_COUNT_COLUMN2.setTemplate(DAY_COUNT_TEMPLATE);
		DAY_COUNT_COLUMN_LIST.add((SystemColumnCustom) DAY_COUNT_COLUMN2);

		DAY_COUNT_COLUMN3 = new SystemColumnCustom();
		DAY_COUNT_COLUMN3.setId(15);
		DAY_COUNT_COLUMN3.setTable(INVESTMENT_SECURITY_TABLE);
		DAY_COUNT_COLUMN3.setName("Day Count");
		DAY_COUNT_COLUMN3.setTemplate(DAY_COUNT_TEMPLATE);
		DAY_COUNT_COLUMN_LIST.add((SystemColumnCustom) DAY_COUNT_COLUMN3);


		//////////////////////////////////////////////////////////////////////

		DATE_FIELD = new MarketDataField();
		DATE_FIELD.setId((short) 15);
		DATE_FIELD.setName("Date");

		ISSUE_DATE_FIELD = new MarketDataField();
		ISSUE_DATE_FIELD.setId((short) 25);
		ISSUE_DATE_FIELD.setName("Issue Date");

		ISSUE_DATE_FIELD2 = new MarketDataField();
		ISSUE_DATE_FIELD2.setId((short) 45);
		ISSUE_DATE_FIELD2.setName("Issue Date2");

		INSTRUMENT_END_DATE_FIELD = new MarketDataField();
		INSTRUMENT_END_DATE_FIELD.setId((short) 35);
		INSTRUMENT_END_DATE_FIELD.setName("End Date");


		DAY_COUNT_FIELD = new MarketDataField();
		DAY_COUNT_FIELD.setId((short) 40);
		DAY_COUNT_FIELD.setName("Day Count");

		//////////////////////////////////////////////////////////////////////

		TEST_FIELD_GROUP = new MarketDataFieldGroup();
		TEST_FIELD_GROUP.setName("Test");
		TEST_FIELD_GROUP.setId((short) 35);

		//////////////////////////////////////////////////////////////////////

		INVESTMENT_TYPE_SWAPTIONS = new InvestmentType();
		INVESTMENT_TYPE_SWAPTIONS.setId((short) 12);
		INVESTMENT_TYPE_SWAPTIONS.setName("Swaptions");

		INVESTMENT_TYPE_BENCHMARKS = new InvestmentType();
		INVESTMENT_TYPE_BENCHMARKS.setId((short) 5);
		INVESTMENT_TYPE_BENCHMARKS.setName("Benchmarks");

		INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS = new InvestmentTypeSubType();
		INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.setId((short) 32);
		INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.setName("Interest Rate Swaptions");


		INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS = new InvestmentTypeSubType2();
		INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS.setId((short) 1);
		INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS.setName("Interest Rate Swaptions");

		//////////////////////////////////////////////////////////////////////

		FIXED_INCOME_SWAPS_HIERARCHY = new InvestmentInstrumentHierarchy();
		FIXED_INCOME_SWAPS_HIERARCHY.setName("Swaptions - IRS");
		FIXED_INCOME_SWAPS_HIERARCHY.setId((short) 15);

		HIERARCHY_WITH_CHILDREN = new InvestmentInstrumentHierarchy();
		HIERARCHY_WITH_CHILDREN.setName("Hierarchy With Children");
		HIERARCHY_WITH_CHILDREN.setId((short) 150);


		HIERARCHY_CHILD1 = new InvestmentInstrumentHierarchy();
		HIERARCHY_CHILD1.setName("Child1");
		HIERARCHY_CHILD1.setId((short) 175);

		HIERARCHY_CHILD2 = new InvestmentInstrumentHierarchy();
		HIERARCHY_CHILD2.setName("Child2");
		HIERARCHY_CHILD2.setId((short) 200);

		BENCHMARK_CREDIT_INDEX_HIERARCHY = new InvestmentInstrumentHierarchy();
		BENCHMARK_CREDIT_INDEX_HIERARCHY.setName("Index");
		BENCHMARK_CREDIT_INDEX_HIERARCHY.setId((short) 111);
	}


	@SuppressWarnings("unchecked")
	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
		final List<MarketDataFieldColumnMapping> mappingList = buildColumnMappingList();
		final List<MarketDataFieldColumnMapping> groupMappingList = buildColumnGroupMappingList();
		final List<InvestmentInstrumentHierarchy> hierarchyList = buildInstrumentHierarchyList();

		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			MarketDataFieldColumnMappingSearchForm sf = ((MarketDataFieldColumnMappingSearchForm) args[0]);
			if (sf.getInstrumentHierarchyId() != null) {
				return null;
			}
			if (sf.getDataFieldGroupId() == null) {
				return mappingList;
			}
			return groupMappingList;
		}).when(this.marketDataFieldColumnMappingService).getMarketDataFieldColumnMappingList(ArgumentMatchers.any(MarketDataFieldColumnMappingSearchForm.class));

		final Map<Integer, MarketDataFieldColumnMapping> columnMappingMap = getColumnMappingMap(mappingList);
		columnMappingMap.putAll(getColumnMappingMap(groupMappingList));
		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			Integer id = (Integer) args[0];
			return columnMappingMap.get(id);
		}).when(this.marketDataFieldColumnMappingService).getMarketDataFieldColumnMapping(ArgumentMatchers.any(Integer.class));


		Mockito.doAnswer(invocation -> {
			Object[] args = invocation.getArguments();
			InstrumentHierarchySearchForm sf = ((InstrumentHierarchySearchForm) args[0]);
			if (sf.getParentId().equals(HIERARCHY_WITH_CHILDREN.getId())) {
				return hierarchyList;
			}
			return new ArrayList<>();
		}).when(this.investmentSetupService).getInvestmentInstrumentHierarchyList(ArgumentMatchers.any(InstrumentHierarchySearchForm.class));

		List<MarketDataFieldGroup> fieldGroupList = new ArrayList<>();
		fieldGroupList.add(TEST_FIELD_GROUP);

		Mockito.doAnswer(invocation -> fieldGroupList).when(this.marketDataFieldService).getMarketDataFieldGroupList(ArgumentMatchers.any(MarketDataFieldGroupSearchForm.class));

		Mockito.when(this.marketDataFieldColumnMappingService.resolveSystemColumnListFromTemplate(ArgumentMatchers.any(MarketDataFieldColumnMapping.class))).thenReturn(DAY_COUNT_COLUMN_LIST);

		this.cache = new MarketDataFieldColumnMappingSpecificityCache();
		this.cache.setMarketDataFieldColumnMappingService(this.marketDataFieldColumnMappingService);
		this.cache.setMarketDataFieldService(this.marketDataFieldService);
		this.cache.setDaoLocator(this.daoLocator);
		this.cache.setCacheHandler(SpecificityTestObjectFactory.getCacheHandlerForSpecificityCache(MarketDataFieldColumnMappingTargetHolder.class));
		this.cache.setInvestmentSetupService(this.investmentSetupService);

		Mockito.when(this.daoLocator.locate(ArgumentMatchers.any(Class.class))).thenReturn(this.mockDao);


		Mockito.when(this.systemColumnService.getSystemColumnCustomList(ArgumentMatchers.any(SystemColumnSearchForm.class))).thenReturn(DAY_COUNT_COLUMN_LIST);
		this.cache.setSystemColumnService(this.systemColumnService);
	}


	@Test
	public void testColumnMappingSpecificityLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId((short) 100);

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(1, result.size());

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(15, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());

		Assertions.assertEquals(Integer.valueOf(15), mapping.getId());
	}


	@Test
	public void testColumnMappingSpecificityInvestmentInstrumentLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentInstrument> iCommand = new MarketDataFieldColumnMappingCommand<>();
		iCommand.setTableName(INVESTMENT_INSTRUMENT_TABLE.getName());
		iCommand.setDataSource(DATASOURCE);
		iCommand.setInstrumentHierarchyId((short) 100);
		iCommand.setInvestmentTypeId((short) 100);

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(iCommand);

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(35, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(INSTRUMENT_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_INSTRUMENT_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(INSTRUMENT_END_DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(INSTRUMENT_END_DATE_FIELD.getId(), mapping.getDataField().getId());

		Assertions.assertEquals(Integer.valueOf(35), mapping.getId());

		Assertions.assertEquals(1, result.size());
	}


	@Test
	public void testColumnMappingSpecificityInvestmentHierarchyWithChildrenLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId(HIERARCHY_CHILD1.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(1, result.size());


		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(70, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getId(), mapping.getDataField().getId());


		command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId(HIERARCHY_CHILD2.getId());

		result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(1, result.size());


		mapping = result.get(0);
		Assertions.assertEquals(80, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getId(), mapping.getDataField().getId());
	}


	@Test
	public void testColumnMappingSpecificityInvestmentHierarchyLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId(FIXED_INCOME_SWAPS_HIERARCHY.getId());
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(150, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());


		mapping = result.get(2);
		Assertions.assertEquals(55, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());


		//////////////////////////////////////////////////////////////////////

		command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId(FIXED_INCOME_SWAPS_HIERARCHY.getId());
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubTypeId(INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId());

		result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		mapping = result.get(2);
		Assertions.assertEquals(55, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());


		//////////////////////////////////////////////////////////////////////

		command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId(FIXED_INCOME_SWAPS_HIERARCHY.getId());
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubTypeId((short) (INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId() + 10));

		result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(150, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(2);
		Assertions.assertEquals(55, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		Assertions.assertEquals(Integer.valueOf(55), mapping.getId());
	}


	@Test
	public void testColumnMappingSpecificityInvestmentTypeLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(150, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(2);
		Assertions.assertEquals(25, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getId(), mapping.getDataField().getId());
	}


	@Test
	public void testColumnMappingSpecificityInvestmentSubTypeLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubTypeId(INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		MarketDataFieldColumnMapping mapping = result.get(2);
		Assertions.assertEquals(60, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(Integer.valueOf(10), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getId(), mapping.getDataField().getId());


		command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubTypeId((short) (INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId().intValue() + 10)); // should not match anything and pick the type specific value

		result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(150, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(2);
		Assertions.assertEquals(25, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(Integer.valueOf(10), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getId(), mapping.getDataField().getId());
	}


	@Test
	public void testColumnGroupMappingSpecificityLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId((short) 100);
		command.setDataFieldGroupId(TEST_FIELD_GROUP.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(1, result.size());
		Assertions.assertNotNull(result.get(0));

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(45, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(GROUP_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getName(), mapping.getColumn().getTable().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD.getId(), mapping.getDataField().getId());

		Assertions.assertEquals(Integer.valueOf(45), mapping.getId());
	}


	@Test
	public void testColumnDuplicateMappingSpecificityLookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_INSTRUMENT_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId(BENCHMARK_CREDIT_INDEX_HIERARCHY.getId());
		command.setInvestmentTypeId(INVESTMENT_TYPE_BENCHMARKS.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(1, result.size());
		Assertions.assertNotNull(result.get(0));

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(Integer.valueOf(90), mapping.getId());
	}


	@Test
	public void testColumnMappingSpecificityInvestmentSubType2Lookup() {
		MarketDataFieldColumnMappingCommand<InvestmentSecurity> command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubTypeId(INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId());
		command.setInvestmentTypeSubType2Id(INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS.getId());

		List<MarketDataFieldColumnMapping> result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		MarketDataFieldColumnMapping mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(130, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(2);
		Assertions.assertEquals(110, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());


		command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubType2Id(INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS.getId());
		//command.setInvestmentTypeSubTypeId((short) (INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId().intValue() + 10)); // should not match anything and pick the type specific value

		result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(130, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(2);
		Assertions.assertEquals(120, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getId(), mapping.getDataField().getId());


		command = new MarketDataFieldColumnMappingCommand<>();
		command.setTableName(INVESTMENT_SECURITY_TABLE.getName());
		command.setDataSource(DATASOURCE);
		command.setInstrumentHierarchyId((short) 100);
		command.setInvestmentTypeId(INVESTMENT_TYPE_SWAPTIONS.getId());
		command.setInvestmentTypeSubType2Id(INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS.getId());
		command.setInvestmentTypeSubTypeId((short) (INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS.getId().intValue() + 10));

		result = this.cache.getMostSpecificResultList(command);
		Assertions.assertEquals(3, result.size());

		mapping = result.get(0);
		Assertions.assertEquals(160, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertNull(mapping.getColumn());
		Assertions.assertEquals(DAY_COUNT_TEMPLATE.getId(), mapping.getTemplate().getId());
		Assertions.assertEquals(DAY_COUNT_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DAY_COUNT_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(1);
		Assertions.assertEquals(130, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_END_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(DATE_FIELD.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(DATE_FIELD.getId(), mapping.getDataField().getId());

		mapping = result.get(2);
		Assertions.assertEquals(120, mapping.getId().intValue());
		Assertions.assertEquals(DATASOURCE.getId(), mapping.getDataSource().getId());
		Assertions.assertEquals(SECURITY_START_DATE_COLUMN.getId(), mapping.getColumn().getId());
		Assertions.assertEquals(INVESTMENT_SECURITY_TABLE.getId(), mapping.getColumn().getTable().getId());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getName(), mapping.getDataField().getName());
		Assertions.assertEquals(ISSUE_DATE_FIELD2.getId(), mapping.getDataField().getId());
	}


	private List<MarketDataFieldColumnMapping> buildColumnMappingList() {
		List<MarketDataFieldColumnMapping> result = new ArrayList<>();

		MarketDataFieldColumnMapping mapping = new MarketDataFieldColumnMapping();
		mapping.setId(15);
		mapping.setDataField(DATE_FIELD);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);

		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(25);
		mapping.setDataField(ISSUE_DATE_FIELD);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);

		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(35);
		mapping.setDataField(INSTRUMENT_END_DATE_FIELD);
		mapping.setColumn(INSTRUMENT_END_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);

		result.add(mapping);


		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(55);
		mapping.setDataField(DATE_FIELD);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInstrumentHierarchy(FIXED_INCOME_SWAPS_HIERARCHY);

		result.add(mapping);


		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(60);
		mapping.setDataField(ISSUE_DATE_FIELD2);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		mapping.setInvestmentTypeSubType(INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(70);
		mapping.setDataField(ISSUE_DATE_FIELD2);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInstrumentHierarchy(HIERARCHY_WITH_CHILDREN);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(80);
		mapping.setDataField(ISSUE_DATE_FIELD);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInstrumentHierarchy(HIERARCHY_CHILD2);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(90);
		mapping.setDataField(ISSUE_DATE_FIELD);
		mapping.setColumn(INSTRUMENT_END_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInstrumentHierarchy(BENCHMARK_CREDIT_INDEX_HIERARCHY);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(100);
		mapping.setDataField(ISSUE_DATE_FIELD);
		mapping.setColumn(INSTRUMENT_END_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_BENCHMARKS);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(110);
		mapping.setDataField(DATE_FIELD);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		mapping.setInvestmentTypeSubType(INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS);
		mapping.setInvestmentTypeSubType2(INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS);
		result.add(mapping);


		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(120);
		mapping.setDataField(ISSUE_DATE_FIELD2);
		mapping.setColumn(SECURITY_START_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		mapping.setInvestmentTypeSubType2(INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS);
		result.add(mapping);


		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(130);
		mapping.setDataField(DATE_FIELD);
		mapping.setColumn(SECURITY_END_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		mapping.setInvestmentTypeSubType2(INVESTMENT_TYPE_SUBTYPE2_CENTRALLY_CLEARED_SWAPS);
		result.add(mapping);


		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(140);
		mapping.setDataField(ISSUE_DATE_FIELD2);
		mapping.setColumn(SECURITY_END_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		mapping.setInvestmentTypeSubType(INVESTMENT_TYPE_SUBTYPE_IRS_SWAPTIONS);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(150);
		mapping.setDataField(DATE_FIELD);
		mapping.setColumn(SECURITY_END_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		result.add(mapping);

		///////////////////////////////

		mapping = new MarketDataFieldColumnMapping();
		mapping.setId(160);
		mapping.setDataField(DAY_COUNT_FIELD);
		mapping.setTemplate(DAY_COUNT_TEMPLATE);
		mapping.setDataSource(DATASOURCE);
		mapping.setInvestmentType(INVESTMENT_TYPE_SWAPTIONS);
		result.add(mapping);

		return result;
	}


	private Map<Integer, MarketDataFieldColumnMapping> getColumnMappingMap(List<MarketDataFieldColumnMapping> columnMappingList) {
		final Map<Integer, MarketDataFieldColumnMapping> result = new HashMap<>();
		for (MarketDataFieldColumnMapping columnMapping : CollectionUtils.getIterable(columnMappingList)) {
			result.put(columnMapping.getId(), columnMapping);
		}
		return result;
	}


	private List<MarketDataFieldColumnMapping> buildColumnGroupMappingList() {
		List<MarketDataFieldColumnMapping> result = new ArrayList<>();

		MarketDataFieldColumnMapping mapping = new MarketDataFieldColumnMapping();
		mapping.setDataField(ISSUE_DATE_FIELD);
		mapping.setColumn(GROUP_DATE_COLUMN);
		mapping.setDataSource(DATASOURCE);
		mapping.setId(45);

		result.add(mapping);
		return result;
	}


	private List<InvestmentInstrumentHierarchy> buildInstrumentHierarchyList() {
		List<InvestmentInstrumentHierarchy> result = new ArrayList<>();

		result.add(HIERARCHY_CHILD1);
		result.add(HIERARCHY_CHILD2);

		return result;
	}
}
