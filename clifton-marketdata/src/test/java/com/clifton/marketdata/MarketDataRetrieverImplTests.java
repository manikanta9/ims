package com.clifton.marketdata;

import com.clifton.calendar.setup.CalendarSetupService;
import com.clifton.core.test.util.TestUtils;
import com.clifton.core.util.MathUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.core.validation.ValidationExceptionWithCause;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.Date;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class MarketDataRetrieverImplTests {

	@Resource
	private CalendarSetupService calendarSetupService;

	@Resource
	private MarketDataRetrieverImpl marketDataRetriever;

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;


	@BeforeEach
	public void setupCalendarData() {
		// Setup Once
		short year = 2011;
		if (this.calendarSetupService.getCalendarYear(year) == null) {
			this.calendarSetupService.saveCalendarYearForYear(year);
		}
	}


	@Test
	public void testGetMarketDataPriceFlexible() {
		Date date = DateUtils.toDate("02/28/2011");

		BigDecimal value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("HOH1", null), date, false);
		Assertions.assertEquals(292.58, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("LLLH1", null), date, false);
		Assertions.assertEquals(1107.1, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("LNH1", null), date, false);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("RIY", null), date, false);
		Assertions.assertEquals(732.44, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), date, false);
		Assertions.assertEquals(120.34375, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("BIG", null), date, false);
		Assertions.assertEquals(100.44, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("SMALL", null), date, false);
		Assertions.assertEquals(100.44, value.doubleValue(), 0.0d);
	}


	@Test
	public void testGetMarketDataPriceFlexibleException() {
		TestUtils.expectException(ValidationException.class, () -> this.marketDataRetriever.getPriceFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("LNH1", null), DateUtils.toDate("02/28/2011"), true), "Security [LNH1] price data [Settlement Price or Last Trade Price] is missing on or before date [02/28/2011].");
	}


	@Test
	public void testGetMarketDataPrice() {
		Date date = DateUtils.toDate("02/28/2011");

		BigDecimal value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("HOH1", null), date, false, null);
		Assertions.assertEquals(292.58, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("LLLH1", null), date, false, null);
		Assertions.assertEquals(1107.1, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("LNH1", null), date, false, null);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("RIY", null), date, false, null);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), date, false, null);
		Assertions.assertEquals(120.34375, value.doubleValue(), 0.0d);

		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("BIG", null), date, false, null);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("SMALL", null), date, false, null);
		Assertions.assertNull(value);


		date = DateUtils.toDate("08/15/2011");
		// Returns 7/30 price
		value = this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("MONTHLY-BENCHMARK", null), date, false, null);
		Assertions.assertEquals(150.0, value.doubleValue(), 0.0d);
	}


	@Test
	public void testGetMarketDataPriceException() {
		TestUtils.expectException(ValidationException.class, () -> this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("RIY", null), DateUtils.toDate("02/28/2011"), true, null), "Security [RIY] price data [Last Trade Price] is missing on date [02/28/2011]. 02/28/2011 is not a holiday for the calendar [US Bank Holidays].");
	}


	@Test
	public void testGetMarketDataPriceException_Monthly() {
		TestUtils.expectException(ValidationException.class, () -> this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("MONTHLY-BENCHMARK", null), DateUtils.toDate("02/20/2011"), true, null), "Security [MONTHLY-BENCHMARK] price data [Last Trade Price] is missing on or before date [02/20/2011] and on or after [01/31/2011].");
	}


	@Test
	public void testGetMarketDataPriceExceptionForBig() {
		TestUtils.expectException(ValidationException.class, () -> this.marketDataRetriever.getPrice(this.investmentInstrumentService.getInvestmentSecurityBySymbol("SMALL", null), DateUtils.toDate("02/28/2011"), true, null), "Security [BIG (Big Security for: SMALL)] price data [Last Trade Price] is missing on date [02/28/2011]. 02/28/2011 is not a holiday for the calendar [US Bank Holidays].");
	}


	@Test
	public void testGetMarketDataDurationFlexible() {
		BigDecimal value = this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/28/2011"), false);
		Assertions.assertEquals(14.5, value.doubleValue(), 0);

		value = this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/23/2011"), false);
		Assertions.assertEquals(12.69, value.doubleValue(), 0);

		value = this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), false);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/28/2011"), "Duration (Spot)", false);
		Assertions.assertEquals(14.512, value.doubleValue(), 0);

		value = this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/23/2011"), "Duration (Spot)", false);
		Assertions.assertEquals(12.6912, value.doubleValue(), 0);

		value = this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), "Duration (Spot)", false);
		Assertions.assertNull(value);
	}


	@Test
	public void testGetMarketDataDurationByFieldFlexible() {
		BigDecimal value = this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/28/2011"), "Duration", false);
		Assertions.assertEquals(14.5, value.doubleValue(), 0);

		value = this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/23/2011"), "Duration", false);
		Assertions.assertEquals(12.69, value.doubleValue(), 0);

		value = this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), "Duration", false);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/28/2011"), "Duration (Spot)", false);
		Assertions.assertEquals(14.512, value.doubleValue(), 0);

		value = this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/23/2011"), "Duration (Spot)", false);
		Assertions.assertEquals(12.6912, value.doubleValue(), 0);

		value = this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), "Duration (Spot)", false);
		Assertions.assertNull(value);
	}


	@Test
	public void testGetMarketDataDurationByField() {
		BigDecimal value = this.marketDataRetriever.getSecurityDataFieldValue(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/28/2011"), "Duration", false);
		Assertions.assertEquals(14.5, value.doubleValue(), 0);

		value = this.marketDataRetriever.getSecurityDataFieldValue(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/23/2011"), "Duration", false);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getSecurityDataFieldValue(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), "Duration", false);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getSecurityDataFieldValue(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/28/2011"), "Duration (Spot)", false);
		Assertions.assertEquals(14.512, value.doubleValue(), 0);

		value = this.marketDataRetriever.getSecurityDataFieldValue(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/23/2011"), "Duration (Spot)", false);
		Assertions.assertNull(value);

		value = this.marketDataRetriever.getSecurityDataFieldValue(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), "Duration (Spot)", false);
		Assertions.assertNull(value);
	}


	@Test
	public void testGetMarketDataDurationFlexibleException() {
		TestUtils.expectException(ValidationException.class, () -> this.marketDataRetriever.getDurationFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), true), "Missing Duration for security [USM1] on or before [02/21/2011].");
	}


	@Test
	public void testGetMarketDataDurationByFieldFlexibleException() {
		TestUtils.expectException(ValidationExceptionWithCause.class, () -> this.marketDataRetriever.getSecurityDataFieldValueFlexible(this.investmentInstrumentService.getInvestmentSecurityBySymbol("USM1", null), DateUtils.toDate("02/21/2011"), "Duration", true), "Missing Duration for security [USM1] on or before [02/21/2011].");
	}


	@Test
	public void testInvestmentSecurityReturn() {
		// No Adjustment Factor
		InvestmentSecurity security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("RIY", null);
		BigDecimal returnValue = this.marketDataRetriever.getInvestmentSecurityReturn(null, security, DateUtils.toDate("02/22/2011"), DateUtils.toDate("02/23/2011"), null, true);
		// 729.6 TO 724.53
		Assertions.assertEquals(new BigDecimal("-0.6949"), MathUtils.round(returnValue, 4));

		// With Adjustment Factor
		security = this.investmentInstrumentService.getInvestmentSecurityBySymbol("GMOEX", null);
		returnValue = this.marketDataRetriever.getInvestmentSecurityReturn(null, security, DateUtils.toDate("07/01/2016"), DateUtils.toDate("07/05/2016"), null, true);
		// 10 * 3 Adjustment Factor to 10.5 * 3 Adjustment Factor
		Assertions.assertEquals(new BigDecimal("5.0000"), MathUtils.round(returnValue, 4));

		returnValue = this.marketDataRetriever.getInvestmentSecurityReturn(null, security, DateUtils.toDate("07/01/2016"), DateUtils.toDate("07/07/2016"), null, true);
		// 10 * 3 Adjustment Factor to 33
		Assertions.assertEquals(new BigDecimal("10.0000"), MathUtils.round(returnValue, 4));
	}
}
