SELECT 'MarketDataValue' AS entityTableName, MarketDataValueID AS entityId FROM MarketDataValue WHERE InvestmentSecurityID
IN (SELECT InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol IN ('AAPL', 'IBM')) AND MeasureDate BETWEEN '2021-09-01' AND '2021-10-01'
UNION
SELECT 'InvestmentSecurity'AS entityTableName, InvestmentSecurityID AS entityID FROM InvestmentSecurity WHERE Symbol = 'ORCL'
UNION
SELECT 'Calendar' AS entityTableName, CalendarID AS entityId FROM Calendar
UNION
SELECT 'CalendarDay' AS entityTableName, CalendarDayID AS entityId FROM CalendarDay WHERE CalendarYearID = 2021 AND CalendarMonthID IN (9,10)
UNION
SELECT 'CalendarHolidayDay' AS entityTableName, CalendarHolidayDayID AS entityId FROM CalendarHolidayDay WHERE CalendarDayID IN (SELECT CalendarDayID FROM CalendarDay WHERE CalendarYearID = 2021 AND CalendarMonthID IN (9,10))
UNION
SELECT 'InvestmentExchange' AS entityTableName, InvestmentExchangeID AS entityid FROM InvestmentExchange
UNION
SELECT 'MarketDataPriceFieldMapping' AS entityTableName, MarketDataPriceFieldMappingID AS entityid FROM MarketDataPriceFieldMapping
UNION
SELECT 'MarketDataFieldMapping' AS entityTableName, MarketDataFieldMappingID AS entityid FROM MarketDataFieldMapping
UNION
SELECT 'MarketDataFieldFieldGroup' AS entityTableName, MarketDataFieldFieldGroupID AS entityid FROM MarketDataFieldFieldGroup;
