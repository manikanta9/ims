package com.clifton.marketdata.api.pricing;

import com.clifton.core.shared.dataaccess.DataTypeNames;
import com.clifton.core.test.dataaccess.BaseInMemoryDatabaseTests;
import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.validation.ValidationException;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.live.MarketDataLive;
import com.clifton.marketdata.live.MarketDataLiveService;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionOperations;
import com.clifton.marketdata.provider.subscription.MarketDataSubscriptionSecurityData;
import com.clifton.marketdata.rates.MarketDataExchangeRate;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * A set of tests to verify operation of the {@link MarketDataSecurityDataValueApiService} implementation.
 *
 * @author davidi
 */
@ContextConfiguration("MarketDataSecurityDataValueApiServiceTests-context.xml")
public class MarketDataSecurityDataValueApiServiceTests extends BaseInMemoryDatabaseTests {

	@Resource
	private InvestmentInstrumentService investmentInstrumentService;

	@Mock
	private MarketDataProviderLocator mockMarketDataProviderLocator;

	@Mock
	private MarketDataLiveService mockMarketDataLiveService;

	@Resource
	@InjectMocks
	private MarketDataSecurityDataValueApiService marketDataSecurityDataValueApiService;

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	@BeforeEach
	public void setup() {
		MockitoAnnotations.initMocks(this);
	}


	@Test
	public void getPriceFromLocalMarketData() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = DateUtils.toDate("09/07/2021");
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		Assertions.assertEquals(date, securityDataValue.getMeasureDate());
		BigDecimal expectedPrice = BigDecimal.valueOf(156.69).setScale(15, BigDecimal.ROUND_HALF_UP);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getPriceFromLocalMarketData_using_forPrice_command() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = DateUtils.toDate("09/07/2021");
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forPrice(investmentSecurity.getId(), date);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		Assertions.assertEquals(date, securityDataValue.getMeasureDate());
		BigDecimal expectedPrice = BigDecimal.valueOf(156.69).setScale(15, BigDecimal.ROUND_HALF_UP);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getPriceFromLocalMarketData_for_holiday() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = DateUtils.toDate("09/06/2021");
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(154.30).setScale(15, BigDecimal.ROUND_HALF_UP);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getHistoricalPrice_exchange_equals_security_exchange() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = DateUtils.toDate("09/08/2021");
		String exchangeName = "NASDAQ Global Select";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date).withExchangeName(exchangeName);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(155.11).setScale(15, BigDecimal.ROUND_HALF_UP);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getHistoricalPriceFromProvider() {
		Date date = DateUtils.toDate("09/07/2021");
		MockedMarketDataProvider mockedMarketDataProvider = new MockedMarketDataProvider();
		mockedMarketDataProvider.setLastPrice(BigDecimal.valueOf(156.69));
		mockedMarketDataProvider.setMeasureDate(date);
		Mockito.when(this.mockMarketDataProviderLocator.locate(Mockito.any())).thenReturn(mockedMarketDataProvider);

		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date).withExchangeName(exchangeName);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(156.69);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getLivePriceFromProvider_with_exchange() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = new Date();
		MarketDataLive marketDataLive = new MarketDataLive(investmentSecurity.getId(), "Last Trade Price", DataTypeNames.DECIMAL, BigDecimal.valueOf(142.65), date);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLiveFieldValuesWithCommand(Mockito.any())).thenReturn(CollectionUtils.createList(marketDataLive));

		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forLive(investmentSecurity.getId(), "Last Trade Price").withExchangeName(exchangeName);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(142.65);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getLivePriceFromProvider_with_exchange_using_forLivePrice_command() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = new Date();
		MarketDataLive marketDataLive = new MarketDataLive(investmentSecurity.getId(), "Last Trade Price", DataTypeNames.DECIMAL, BigDecimal.valueOf(142.65), date);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLivePrice(Mockito.anyInt(), Mockito.any())).thenReturn(marketDataLive);

		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forLivePrice(investmentSecurity.getId()).withExchangeName(exchangeName);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(142.65);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getLivePriceFromProvider_with_exchange_using_null_date_live() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = new Date();
		MarketDataLive marketDataLive = new MarketDataLive(investmentSecurity.getId(), "Last Trade Price", DataTypeNames.DECIMAL, BigDecimal.valueOf(142.65), date);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLivePrice(Mockito.anyInt(), Mockito.any())).thenReturn(marketDataLive);

		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withSecurityId(investmentSecurity.getId()).withExchangeName(exchangeName);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(142.65);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getLivePriceFromProvider_with_exchange_using_future_date_live() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = new Date();
		MarketDataLive marketDataLive = new MarketDataLive(investmentSecurity.getId(), "Last Trade Price", DataTypeNames.DECIMAL, BigDecimal.valueOf(142.65), date);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLivePrice(Mockito.anyInt(), Mockito.any())).thenReturn(marketDataLive);

		Date futureDate = DateUtils.addDays(date, 7);
		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withSecurityId(investmentSecurity.getId()).withExchangeName(exchangeName).withRequestedDate(futureDate);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(142.65);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}



	@Test
	public void getLivePriceFromProvider_with_exchange_using_forLivePrice_command_with_field() {
		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("AAPL", false);
		Date date = new Date();
		MarketDataLive marketDataLive = new MarketDataLive(investmentSecurity.getId(), "Last Trade Price", DataTypeNames.DECIMAL, BigDecimal.valueOf(152.65), date);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLiveFieldValuesWithCommand(Mockito.any())).thenReturn(CollectionUtils.createList(marketDataLive));

		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forLivePrice(investmentSecurity.getId()).withExchangeName(exchangeName).withFieldName("Last Trade Price");
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		BigDecimal expectedPrice = BigDecimal.valueOf(152.65);
		BigDecimal price = (BigDecimal) securityDataValue.getFieldValue();
		Assertions.assertEquals(expectedPrice, price);
	}


	@Test
	public void getHistoricalPriceFromProvider_data_not_found_with_exception() {
		Date date = DateUtils.toDate("09/07/2021");
		MockedMarketDataProvider mockedMarketDataProvider = new MockedMarketDataProvider();
		mockedMarketDataProvider.setLastPrice(null);
		mockedMarketDataProvider.setMeasureDate(null);
		Mockito.when(this.mockMarketDataProviderLocator.locate(Mockito.any())).thenReturn(mockedMarketDataProvider);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLiveFieldValuesWithCommand(Mockito.any())).thenReturn(new ArrayList<>());

		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("ORCL", false);
		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date).withExchangeName(exchangeName).withExceptionOnMissingData(true);
		Assertions.assertThrows(ValidationException.class, () -> this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command));
	}


	@Test
	public void getHistoricalPriceFromProvider_data_not_found_with_exception_using_error_prefix() {
		Date date = DateUtils.toDate("09/07/2021");
		MockedMarketDataProvider mockedMarketDataProvider = new MockedMarketDataProvider();
		mockedMarketDataProvider.setLastPrice(null);
		mockedMarketDataProvider.setMeasureDate(null);
		Mockito.when(this.mockMarketDataProviderLocator.locate(Mockito.any())).thenReturn(mockedMarketDataProvider);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLiveFieldValuesWithCommand(Mockito.any())).thenReturn(new ArrayList<>());

		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("ORCL", false);
		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date).withExchangeName(exchangeName).withExceptionOnMissingData(true).withExceptionPrefix("Data Not Found Error");
		Assertions.assertThrows(ValidationException.class, () -> this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command));
	}


	@Test
	public void getHistoricalPriceFromProvider_data_not_found_no_exception() {
		Date date = DateUtils.toDate("09/07/2021");
		MockedMarketDataProvider mockedMarketDataProvider = new MockedMarketDataProvider();
		mockedMarketDataProvider.setLastPrice(null);
		mockedMarketDataProvider.setMeasureDate(null);
		Mockito.when(this.mockMarketDataProviderLocator.locate(Mockito.any())).thenReturn(mockedMarketDataProvider);
		Mockito.when(this.mockMarketDataLiveService.getMarketDataLiveFieldValuesWithCommand(Mockito.any())).thenReturn(new ArrayList<>());

		InvestmentSecurity investmentSecurity = this.investmentInstrumentService.getInvestmentSecurityBySymbol("ORCL", false);
		String exchangeName = "NASDAQ Global Market";
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(investmentSecurity.getId(), "Last Trade Price", date).withExchangeName(exchangeName);
		MarketDataSecurityDataValue securityDataValue = this.marketDataSecurityDataValueApiService.getMarketDataSecurityDataValue(command);
		Assertions.assertNull(securityDataValue);
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_forLivePrice() {
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forLivePrice(5432);
		Assertions.assertEquals(5432, command.getSecurityId());
		Assertions.assertTrue(command.isLiveValue());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_forLive() {
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forLive(5432, "Last Trade Price");
		Assertions.assertEquals(5432, command.getSecurityId());
		Assertions.assertEquals("Last Trade Price", command.getMarketDataFieldName());
		Assertions.assertTrue(command.isLiveValue());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_forPrice() {
		Date date = DateUtils.toDate("10/06/2021");
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forPrice(5432, date);
		Assertions.assertEquals(5432, command.getSecurityId());
		Assertions.assertEquals(date, command.getRequestedDate());
		Assertions.assertFalse(command.isLiveValue());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_forField() {
		Date date = DateUtils.toDate("10/06/2021");
		MarketDataSecurityDataValueCommand command = MarketDataSecurityDataValueCommand.forField(5432, "Ask Price", date);
		Assertions.assertEquals(5432, command.getSecurityId());
		Assertions.assertEquals("Ask Price", command.getMarketDataFieldName());
		Assertions.assertEquals(date, command.getRequestedDate());
		Assertions.assertFalse(command.isLiveValue());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withSecurityId() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withSecurityId(5432);
		Assertions.assertEquals(5432, command.getSecurityId());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withExchangeName() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withExchangeName("NASDAQ Global Market");
		Assertions.assertEquals("NASDAQ Global Market", command.getExchangeName());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withLiveValue_True() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withLiveValue(true);
		Assertions.assertTrue(command.isLiveValue());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withLiveValue_False() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withLiveValue(false);
		Assertions.assertFalse(command.isLiveValue());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withFlexible_True() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withFlexible(true);
		Assertions.assertTrue(command.isFlexible());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withFlexible_False() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withFlexible(false);
		Assertions.assertFalse(command.isFlexible());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withFlexibleMaxDaysBackAllowed() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withFlexibleMaxDaysBackAllowed(5);
		Assertions.assertEquals(5, command.getFlexibleMaxDaysBackAllowed());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withMarketSector() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withMarketSector("Index");
		Assertions.assertEquals("Index", command.getMarketSectorName());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withRequestedDate() {
		Date date = DateUtils.toDate("09/27/2021");
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withRequestedDate(date);
		Assertions.assertEquals(date, command.getRequestedDate());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withFieldName() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withFieldName("Last Trade Price");
		Assertions.assertEquals("Last Trade Price", command.getMarketDataFieldName());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withMarketDataSourceName() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withMarketDataSourceName("Bloomberg");
		Assertions.assertEquals("Bloomberg", command.getMarketDataSourceName());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withPricingSourceName() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withPricingSourceName("Bloomberg Valuation Service");
		Assertions.assertEquals("Bloomberg Valuation Service", command.getPricingSourceName());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withProviderOverrideName() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withProviderOverrideName("Bloomberg Terminal");
		Assertions.assertEquals("Bloomberg Terminal", command.getProviderOverrideName());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withExceptionOnMissingData_true() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withExceptionOnMissingData(true);
		Assertions.assertTrue(command.isExceptionOnMissingData());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withExceptionOnMissingData_false() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withExceptionOnMissingData(false);
		Assertions.assertFalse(command.isExceptionOnMissingData());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withExceptionPrefix() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withExceptionPrefix("Exception detected");
		Assertions.assertEquals("Exception detected", command.getExceptionPrefix());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withTimeoutOverride() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withTimeoutOverride(2000);
		Assertions.assertEquals(2000, command.getTimeoutOverride());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withNormalizedPrice_True() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withNormalizedPrice(true);
		Assertions.assertTrue(command.isNormalizedPrice());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withNormalizedPrice_False() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withNormalizedPrice(false);
		Assertions.assertFalse(command.isNormalizedPrice());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withMaxAgeInSeconds() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withMaxAgeInSeconds(600);
		Assertions.assertEquals(600, command.getMaxAgeInSeconds());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withIgnoreErrors_True() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withIgnoreErrors(true);
		Assertions.assertTrue(command.isIgnoreErrors());
	}


	@Test
	public void testMarketDataSecurityDataValueCommand_withIgnoreErrors_False() {
		MarketDataSecurityDataValueCommand command = new MarketDataSecurityDataValueCommand().withIgnoreErrors(false);
		Assertions.assertFalse(command.isIgnoreErrors());
	}

	////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////


	public static class MockedMarketDataProvider implements MarketDataProvider<MarketDataValueGeneric<?>> {

		private BigDecimal lastPrice;
		private Date measureDate;

		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


		@Override
		public String getMarketDataSourceName() {
			return null;
		}


		@Override
		public MarketDataInterestRate getMarketDataInterestRateLatest(MarketDataInterestRateIndexMapping mapping) {
			return null;
		}


		@Override
		public MarketDataInterestRate getMarketDataInterestRateForDate(MarketDataInterestRateIndexMapping mapping, Date date) {
			return null;
		}


		@Override
		public List<MarketDataInterestRate> getMarketDataInterestRateListForPeriod(MarketDataInterestRateIndexMapping mapping, Date fromDate, Date toDate) {
			return null;
		}


		@Override
		public MarketDataExchangeRate getMarketDataExchangeRateLatest(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency) {
			return null;
		}


		@Override
		public MarketDataExchangeRate getMarketDataExchangeRateForDate(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date date) {
			return null;
		}


		@Override
		public List<MarketDataExchangeRate> getMarketDataExchangeRateListForPeriod(InvestmentSecurity fromCurrency, InvestmentSecurity toCurrency, Date fromDate, Date toDate) {
			return null;
		}


		@Override
		public DataFieldValueResponse<MarketDataValueGeneric<?>> getMarketDataValueLatest(DataFieldValueCommand command) {
			return null;
		}


		@Override
		public List<MarketDataValueGeneric<?>> getMarketDataValueListHistorical(DataFieldValueCommand command, List<String> errors) {
			MarketDataValue marketDataValue = new MarketDataValue();
			marketDataValue.setMeasureDate(getMeasureDate());
			marketDataValue.setMeasureValue(getLastPrice());
			return CollectionUtils.createList(marketDataValue);
		}


		@Override
		public List<MarketDataSubscriptionSecurityData> subscribeToMarketData(DataFieldValueCommand command) {
			return null;
		}


		@Override
		public List<MarketDataSubscriptionSecurityData> unsubscribeToMarketData(DataFieldValueCommand command) {
			return null;
		}


		@Override
		public List<MarketDataSubscriptionSecurityData> processMarketDataSubscriptionOperation(MarketDataSubscriptionOperations subscriptionOperation) {
			return null;
		}

		////////////////////////////////////////////////////////////////////////
		////////////////////////////////////////////////////////////////////////


		public BigDecimal getLastPrice() {
			return this.lastPrice;
		}


		public void setLastPrice(BigDecimal lastPrice) {
			this.lastPrice = lastPrice;
		}


		public Date getMeasureDate() {
			return this.measureDate;
		}


		public void setMeasureDate(Date measureDate) {
			this.measureDate = measureDate;
		}
	}
}
