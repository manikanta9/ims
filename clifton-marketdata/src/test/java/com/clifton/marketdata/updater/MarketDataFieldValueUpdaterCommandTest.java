package com.clifton.marketdata.updater;

import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Calendar;
import java.util.Date;
import java.util.stream.Stream;


/**
 * @author TerryS
 */
class MarketDataFieldValueUpdaterCommandTest {

	private static Stream<Arguments> doNotSkipArgumentList() {
		return Stream.of(
				// skip always flag must be on to skip regardless of skip day or timestamp
				Arguments.of(false, null, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// Thursday is not the skip day
				Arguments.of(true, Calendar.THURSDAY, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// skip always overrides
				Arguments.of(false, Calendar.THURSDAY, Time.parse("14:14:14"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// skip always overrides skip day
				Arguments.of(false, Calendar.FRIDAY, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// skip always overrides skip day
				Arguments.of(false, Calendar.FRIDAY, Time.parse("16:16:16"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// skip always overrides skip day and override timestamp
				Arguments.of(false, Calendar.FRIDAY, Time.parse("14:14:14"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// When the override timestamp is specified, it overrides the skip always flag and forces a timestamp comparison - data is older then override timestamp, don't skip it
				Arguments.of(true, null, Time.parse("16:16:16"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15"))
		);
	}


	private static Stream<Arguments> skipArgumentList() {
		return Stream.of(
				// Skip always if skip flag is true and its not overridden by the skip day of week nor override timestamp
				Arguments.of(true, null, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// When the day of week is selected, it overrides the skip always flag and forces the day to match.
				Arguments.of(true, Calendar.FRIDAY, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// When the override timestamp is specified, it overrides the skip always flag and forces a timestamp comparison - data is newer than override timestamp, skip it
				Arguments.of(true, null, Time.parse("14:14:14"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15"))
		);
	}


	private static Stream<Arguments> doNotUpdateArgumentList() {
		return Stream.of(
				// 2 - no overwrite existing, no timestamp
				Arguments.of(false, Calendar.FRIDAY, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 4 - no overwrite existing, timestamps match
				Arguments.of(false, Calendar.FRIDAY, Time.parse("15:15:15"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 5 - no overwrite existing, skip day match or timestamp
				Arguments.of(true, Calendar.THURSDAY, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 6 - provider time not equal to than override
				Arguments.of(false, Calendar.THURSDAY, Time.parse("14:14:14"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 8 - provider time not equal to than override
				Arguments.of(false, Calendar.FRIDAY, Time.parse("14:14:14"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 9 - provider time not equal to override
				Arguments.of(false, Calendar.THURSDAY, Time.parse("14:14:14"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15"))
		);
	}


	private static Stream<Arguments> updateArgumentList() {
		return Stream.of(
				// 1 - overwrite existing with no further restrictions.
				Arguments.of(true, null, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 3 - overwrite existing with no skip day
				Arguments.of(true, null, Time.parse("15:15:15"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 7 - overwrite existing with no skip day
				Arguments.of(true, null, Time.parse("16:16:16"), DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 10 - overwrite existing and day of week match
				Arguments.of(true, Calendar.FRIDAY, null, DateUtils.toDate("09/04/2020"), Time.parse("15:15:15")),
				// 11 - no overwrite existing or skip day provider time not equal to override
				Arguments.of(true, Calendar.FRIDAY, Time.parse("15:15:15"), DateUtils.toDate("09/04/2020"), Time.parse("14:14:14"))
		);
	}


	private static MarketDataFieldValueUpdaterCommand buildSkipMarketDataFieldValueUpdaterCommand(boolean skipDataRequestIfAlreadyExists, Integer skipOnWeekdayId, Time latestValuesOverrideTimestamp) {
		MarketDataFieldValueUpdaterCommand command = new MarketDataFieldValueUpdaterCommand();
		command.setSkipDataRequestIfAlreadyExists(skipDataRequestIfAlreadyExists);
		if (skipOnWeekdayId != null) {
			command.setSkipOnWeekdayId(skipOnWeekdayId.shortValue());
		}
		command.setLatestValuesOverrideTimestamp(latestValuesOverrideTimestamp);
		return command;
	}


	private static MarketDataFieldValueUpdaterCommand buildUpdateMarketDataFieldValueUpdaterCommand(boolean overwriteExistingData, Integer skipOnWeekdayId, Time latestValuesOverrideTimestamp) {
		MarketDataFieldValueUpdaterCommand command = new MarketDataFieldValueUpdaterCommand();
		command.setOverwriteExistingData(overwriteExistingData);
		if (skipOnWeekdayId != null) {
			command.setSkipOnWeekdayId(skipOnWeekdayId.shortValue());
		}
		command.setLatestValuesOverrideTimestamp(latestValuesOverrideTimestamp);
		return command;
	}


	@ParameterizedTest
	@MethodSource("doNotSkipArgumentList")
	public void testDoNotSkip(boolean skipDataRequestIfAlreadyExists, Integer skipOnWeekdayId, Time latestValuesOverrideTimestamp, Date valueDate, Time valueTimeStamp) {
		MarketDataFieldValueUpdaterCommand command = buildSkipMarketDataFieldValueUpdaterCommand(skipDataRequestIfAlreadyExists, skipOnWeekdayId, latestValuesOverrideTimestamp);
		Assertions.assertFalse(command.calculateSkipIfAlreadyExists(valueDate, valueTimeStamp));
	}


	@ParameterizedTest
	@MethodSource("skipArgumentList")
	public void testSkip(boolean skipDataRequestIfAlreadyExists, Integer skipOnWeekdayId, Time latestValuesOverrideTimestamp, Date valueDate, Time valueTimeStamp) {
		MarketDataFieldValueUpdaterCommand command = buildSkipMarketDataFieldValueUpdaterCommand(skipDataRequestIfAlreadyExists, skipOnWeekdayId, latestValuesOverrideTimestamp);
		Assertions.assertTrue(command.calculateSkipIfAlreadyExists(valueDate, valueTimeStamp));
	}


	@ParameterizedTest
	@MethodSource("doNotUpdateArgumentList")
	public void testDoNotUpdate(boolean overwriteExistingData, Integer skipOnWeekdayId, Time latestValuesOverrideTimestamp, Date valueDate, Time valueTimeStamp) {
		MarketDataFieldValueUpdaterCommand command = buildUpdateMarketDataFieldValueUpdaterCommand(overwriteExistingData, skipOnWeekdayId, latestValuesOverrideTimestamp);
		Assertions.assertFalse(command.calculateOverwriteExistingData(valueDate, valueTimeStamp));
	}


	@ParameterizedTest
	@MethodSource("updateArgumentList")
	public void testUpdate(boolean overwriteExistingData, Integer skipOnWeekdayId, Time latestValuesOverrideTimestamp, Date valueDate, Time valueTimeStamp) {
		MarketDataFieldValueUpdaterCommand command = buildUpdateMarketDataFieldValueUpdaterCommand(overwriteExistingData, skipOnWeekdayId, latestValuesOverrideTimestamp);
		Assertions.assertTrue(command.calculateOverwriteExistingData(valueDate, valueTimeStamp));
	}
}
