package com.clifton.marketdata.updater;

import com.clifton.calendar.holiday.CalendarBusinessDayCommand;
import com.clifton.calendar.holiday.CalendarBusinessDayService;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.status.Status;
import com.clifton.investment.rates.InvestmentInterestRateIndex;
import com.clifton.investment.rates.InvestmentInterestRateIndexBuilder;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceBuilder;
import com.clifton.marketdata.datasource.MarketDataSourceSectorBuilder;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataValueGeneric;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.rates.MarketDataInterestRate;
import com.clifton.marketdata.rates.MarketDataInterestRateIndexMapping;
import com.clifton.marketdata.rates.MarketDataRatesService;
import com.clifton.marketdata.rates.search.InterestRateIndexMappingSearchForm;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


@ContextConfiguration
@ExtendWith(SpringExtension.class)
public class MarketDataInterestRatesUpdaterServiceTest<T extends MarketDataValueGeneric<?>> {

	@Resource
	private MarketDataUpdaterServiceImpl marketDataUpdaterService;

	@Mock
	private MarketDataSourceService marketDataSourceService;

	@Mock
	private MarketDataRatesService marketDataRatesService;

	@Mock
	private MarketDataProviderLocator marketDataProviderLocator;

	@Mock
	private MarketDataProvider<T> dataProvider;

	@Mock
	private CalendarBusinessDayService calendarBusinessDayService;

	@Captor
	private ArgumentCaptor<MarketDataInterestRate> marketDataInterestRateCaptor;

	private MarketDataSource marketDataSource;
	private InvestmentInterestRateIndex investmentInterestRateIndex;

	private Date startDate;
	private Date endDate;


	@BeforeEach
	public void beforeTest() {
		MockitoAnnotations.initMocks(this);
		Mockito.reset(this.marketDataSourceService, this.marketDataRatesService, this.marketDataProviderLocator, this.dataProvider, this.calendarBusinessDayService);

		this.startDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 8));
		this.endDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 12));

		this.marketDataUpdaterService.setMarketDataSourceService(this.marketDataSourceService);
		this.marketDataUpdaterService.setMarketDataRatesService(this.marketDataRatesService);
		this.marketDataUpdaterService.setMarketDataProviderLocator(this.marketDataProviderLocator);
		this.marketDataUpdaterService.setCalendarBusinessDayService(this.calendarBusinessDayService);

		this.marketDataSource = MarketDataSourceBuilder.createBloomberg().toMarketDataSource();
		Mockito.when(this.marketDataSourceService.getMarketDataSourceByName(null)).thenReturn(this.marketDataSource);

		this.investmentInterestRateIndex = InvestmentInterestRateIndexBuilder.create_ICE_LIBOR_USD_Overnight().toInvestmentInterestRateIndex();
		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateIndexMappingList(ArgumentMatchers.any(InterestRateIndexMappingSearchForm.class))).thenAnswer(invocation -> {
			List<MarketDataInterestRateIndexMapping> mappings = new ArrayList<>();

			MarketDataInterestRateIndexMapping mapping = new MarketDataInterestRateIndexMapping();
			mapping.setId(1);
			mapping.setReferenceOne(this.investmentInterestRateIndex);
			mapping.setReferenceTwo(MarketDataSourceSectorBuilder.createCommodity().toMarketDataSourceSector());
			mappings.add(mapping);

			return mappings;
		});

		Mockito.when(this.marketDataProviderLocator.locate(ArgumentMatchers.any(MarketDataSource.class))).thenAnswer(invocation -> this.dataProvider);

		Mockito.when(this.calendarBusinessDayService.isBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class))).thenAnswer(invocation -> true);

		Mockito.when(this.dataProvider.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class)))
				.thenAnswer(invocation -> {
					MarketDataInterestRateIndexMapping mapping = invocation.getArgument(0);
					Date startDate = invocation.getArgument(1);
					Date endDate = invocation.getArgument(2);

					List<MarketDataInterestRate> marketDataInterestRateList = new ArrayList<>();

					LocalDate start = DateUtils.asLocalDate(startDate);
					LocalDate end = DateUtils.asLocalDate(endDate);
					int identifier = 2;
					while (!start.isAfter(end)) {
						if (DateUtils.isWeekday(DateUtils.asUtilDate(start)) && this.calendarBusinessDayService.isBusinessDay(CalendarBusinessDayCommand.forDate(DateUtils.asUtilDate(start), (short) 1))) {
							MarketDataInterestRate marketDataInterestRate = new MarketDataInterestRate();

							marketDataInterestRate.setId(identifier);
							marketDataInterestRate.setDataSource(this.marketDataSource);
							marketDataInterestRate.setInterestRate(new BigDecimal("0.222"));
							marketDataInterestRate.setInterestRateDate(DateUtils.asUtilDate(start));
							marketDataInterestRate.setInterestRateIndex(this.investmentInterestRateIndex);
							marketDataInterestRateList.add(marketDataInterestRate);
						}
						start = start.plus(1, ChronoUnit.DAYS);
						identifier++;
					}
					return marketDataInterestRateList;
				});
	}


	@Test
	public void skipExistingStartingDate() {
		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateListForIndicesPeriod(ArgumentMatchers.anyList(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(invocation -> {
			List<MarketDataInterestRate> marketDataInterestRateList = new ArrayList<>();

			MarketDataInterestRate marketDataInterestRate = new MarketDataInterestRate();
			marketDataInterestRate.setId(1);
			marketDataInterestRate.setDataSource(MarketDataSourceBuilder.createBloomberg().toMarketDataSource());
			marketDataInterestRate.setInterestRate(new BigDecimal("0.2256300000"));
			marketDataInterestRate.setInterestRateDate(this.startDate);
			marketDataInterestRate.setInterestRateIndex(this.investmentInterestRateIndex);

			marketDataInterestRateList.add(marketDataInterestRate);

			return marketDataInterestRateList;
		});

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, (short) 1, this.startDate, this.endDate, true);
		Status status = this.marketDataUpdaterService.updateMarketDataInterestRates(command);
		Assertions.assertNotNull(status);
		Mockito.verify(this.marketDataRatesService, Mockito.times(4)).saveMarketDataInterestRate(this.marketDataInterestRateCaptor.capture());
		List<MarketDataInterestRate> updatedRates = this.marketDataInterestRateCaptor.getAllValues();
		Assertions.assertEquals(4, updatedRates.size());
		List<Date> rateDates = updatedRates.stream().map(MarketDataInterestRate::getInterestRateDate).collect(Collectors.toList());
		rateDates.removeAll(Arrays.asList(
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 9)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 10)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 11)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 12))));
		Assertions.assertTrue(rateDates.isEmpty());
		Assertions.assertTrue(command.getStatus().getErrorList().isEmpty());
		Mockito.verify(this.dataProvider, Mockito.times(1))
				.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class));
	}


	@Test
	public void skipExistingEndingDate() {
		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateListForIndicesPeriod(ArgumentMatchers.anyList(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(invocation -> {
			List<MarketDataInterestRate> marketDataInterestRateList = new ArrayList<>();

			MarketDataInterestRate marketDataInterestRate = new MarketDataInterestRate();
			marketDataInterestRate.setId(1);
			marketDataInterestRate.setDataSource(MarketDataSourceBuilder.createBloomberg().toMarketDataSource());
			marketDataInterestRate.setInterestRate(new BigDecimal("0.2256300000"));
			marketDataInterestRate.setInterestRateDate(this.endDate);
			marketDataInterestRate.setInterestRateIndex(this.investmentInterestRateIndex);

			marketDataInterestRateList.add(marketDataInterestRate);

			return marketDataInterestRateList;
		});

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, (short) 1, this.startDate, this.endDate, true);
		Status status = this.marketDataUpdaterService.updateMarketDataInterestRates(command);
		Assertions.assertNotNull(status);
		Mockito.verify(this.marketDataRatesService, Mockito.times(4)).saveMarketDataInterestRate(this.marketDataInterestRateCaptor.capture());
		List<MarketDataInterestRate> updatedRates = this.marketDataInterestRateCaptor.getAllValues();
		Assertions.assertEquals(4, updatedRates.size());
		List<Date> rateDates = updatedRates.stream().map(MarketDataInterestRate::getInterestRateDate).collect(Collectors.toList());
		rateDates.removeAll(Arrays.asList(
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 8)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 9)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 10)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 11))));
		Assertions.assertTrue(rateDates.isEmpty());
		Assertions.assertTrue(command.getStatus().getErrorList().isEmpty());
		Mockito.verify(this.dataProvider, Mockito.times(1))
				.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class));
	}


	@Test
	public void middleHolidayDate() {
		Mockito.when(this.calendarBusinessDayService.isBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class))).thenAnswer(
				invocation -> !((CalendarBusinessDayCommand) invocation.getArgument(0)).getDate().equals(DateUtils.asUtilDate(LocalDate.of(2010, 11, 10))));

		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateListForIndicesPeriod(ArgumentMatchers.anyList(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(
				invocation -> Collections.emptyList());

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, (short) 1, this.startDate, this.endDate, true);
		Status status = this.marketDataUpdaterService.updateMarketDataInterestRates(command);
		Assertions.assertNotNull(status);
		Mockito.verify(this.marketDataRatesService, Mockito.times(4)).saveMarketDataInterestRate(this.marketDataInterestRateCaptor.capture());
		List<MarketDataInterestRate> updatedRates = this.marketDataInterestRateCaptor.getAllValues();
		Assertions.assertEquals(4, updatedRates.size());
		List<Date> rateDates = updatedRates.stream().map(MarketDataInterestRate::getInterestRateDate).collect(Collectors.toList());
		rateDates.removeAll(Arrays.asList(
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 8)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 9)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 11)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 12))));
		Assertions.assertTrue(rateDates.isEmpty());
		Assertions.assertTrue(command.getStatus().getErrorList().isEmpty());
		Mockito.verify(this.dataProvider, Mockito.times(1))
				.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class));
	}


	@Test
	public void weekendDate() {
		this.startDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 11));
		this.endDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 15));

		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateListForIndicesPeriod(ArgumentMatchers.anyList(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(
				invocation -> Collections.emptyList());

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, (short) 1, this.startDate, this.endDate, true);
		Status status = this.marketDataUpdaterService.updateMarketDataInterestRates(command);
		Assertions.assertNotNull(status);
		Mockito.verify(this.marketDataRatesService, Mockito.times(3)).saveMarketDataInterestRate(this.marketDataInterestRateCaptor.capture());
		List<MarketDataInterestRate> updatedRates = this.marketDataInterestRateCaptor.getAllValues();
		Assertions.assertEquals(3, updatedRates.size());
		List<Date> rateDates = updatedRates.stream().map(MarketDataInterestRate::getInterestRateDate).collect(Collectors.toList());
		rateDates.removeAll(Arrays.asList(
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 11)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 12)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 15))));
		Assertions.assertTrue(rateDates.isEmpty());
		Assertions.assertTrue(command.getStatus().getErrorList().isEmpty());
		Mockito.verify(this.dataProvider, Mockito.times(1))
				.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class));
	}


	@Test
	public void weekendAndHolidayDate() {
		this.startDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 11));
		this.endDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 15));

		Mockito.when(this.calendarBusinessDayService.isBusinessDay(ArgumentMatchers.any(CalendarBusinessDayCommand.class))).thenAnswer(
				invocation -> !((CalendarBusinessDayCommand) invocation.getArgument(0)).getDate().equals(DateUtils.asUtilDate(LocalDate.of(2010, 11, 12))));

		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateListForIndicesPeriod(ArgumentMatchers.anyList(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(
				invocation -> Collections.emptyList());

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, (short) 1, this.startDate, this.endDate, true);
		Status status = this.marketDataUpdaterService.updateMarketDataInterestRates(command);
		Assertions.assertNotNull(status);
		Mockito.verify(this.marketDataRatesService, Mockito.times(2)).saveMarketDataInterestRate(this.marketDataInterestRateCaptor.capture());
		List<MarketDataInterestRate> updatedRates = this.marketDataInterestRateCaptor.getAllValues();
		Assertions.assertEquals(2, updatedRates.size());
		List<Date> rateDates = updatedRates.stream().map(MarketDataInterestRate::getInterestRateDate).collect(Collectors.toList());
		rateDates.removeAll(Arrays.asList(
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 11)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 15))));
		Assertions.assertTrue(rateDates.isEmpty());
		Assertions.assertTrue(command.getStatus().getErrorList().isEmpty());
		Mockito.verify(this.dataProvider, Mockito.times(1))
				.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class));
	}


	@Test
	public void weekendAndExistingMiddleDate() {
		this.startDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 11));
		this.endDate = DateUtils.asUtilDate(LocalDate.of(2010, 11, 15));

		Mockito.when(this.marketDataRatesService.getMarketDataInterestRateListForIndicesPeriod(ArgumentMatchers.anyList(), ArgumentMatchers.any(), ArgumentMatchers.any())).thenAnswer(invocation -> {
			List<MarketDataInterestRate> marketDataInterestRateList = new ArrayList<>();

			MarketDataInterestRate marketDataInterestRate = new MarketDataInterestRate();
			marketDataInterestRate.setId(1);
			marketDataInterestRate.setDataSource(MarketDataSourceBuilder.createBloomberg().toMarketDataSource());
			marketDataInterestRate.setInterestRate(new BigDecimal("0.2256300000"));
			marketDataInterestRate.setInterestRateDate(DateUtils.asUtilDate(LocalDate.of(2010, 11, 12)));
			marketDataInterestRate.setInterestRateIndex(this.investmentInterestRateIndex);

			marketDataInterestRateList.add(marketDataInterestRate);

			return marketDataInterestRateList;
		});

		MarketDataInterestRateUpdaterCommand command = new MarketDataInterestRateUpdaterCommand(null, (short) 1, this.startDate, this.endDate, true);
		Status status = this.marketDataUpdaterService.updateMarketDataInterestRates(command);
		Assertions.assertNotNull(status);
		Mockito.verify(this.marketDataRatesService, Mockito.times(2)).saveMarketDataInterestRate(this.marketDataInterestRateCaptor.capture());
		List<MarketDataInterestRate> updatedRates = this.marketDataInterestRateCaptor.getAllValues();
		Assertions.assertEquals(2, updatedRates.size());
		List<Date> rateDates = updatedRates.stream().map(MarketDataInterestRate::getInterestRateDate).collect(Collectors.toList());
		rateDates.removeAll(Arrays.asList(
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 11)),
				DateUtils.asUtilDate(LocalDate.of(2010, 11, 15))));
		Assertions.assertTrue(rateDates.isEmpty());
		Assertions.assertTrue(command.getStatus().getErrorList().isEmpty());
		Mockito.verify(this.dataProvider, Mockito.times(2))
				.getMarketDataInterestRateListForPeriod(ArgumentMatchers.any(MarketDataInterestRateIndexMapping.class), ArgumentMatchers.any(Date.class), ArgumentMatchers.any(Date.class));
	}
}
