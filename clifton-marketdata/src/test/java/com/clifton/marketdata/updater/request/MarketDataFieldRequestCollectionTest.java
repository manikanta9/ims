package com.clifton.marketdata.updater.request;

import com.clifton.core.util.CollectionUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.updater.MarketDataFieldValueUpdaterCommand;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.IsEqual;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;


public class MarketDataFieldRequestCollectionTest {

	private static final List<DayOfWeek> WeekEnd = Arrays.asList(DayOfWeek.SATURDAY, DayOfWeek.SUNDAY);

	private static LocalDate startDate = LocalDate.of(2016, 3, 21);
	private static LocalDate endDate = LocalDate.of(2016, 4, 4);
	private static List<LocalDate> intervalDates;
	private static InvestmentSecurity security = new InvestmentSecurity("EVN", 1);
	private static List<MarketDataField> fieldList = Arrays.asList(
			new MarketDataField("ONE"),
			new MarketDataField("TWO"),
			new MarketDataField("THREE")
	);
	private static MarketDataFieldValueUpdaterCommand command = new MarketDataFieldValueUpdaterCommand();


	@BeforeAll
	public static void beforeClass() {

		// names don't matter for equivalency, assign unique IDs.
		short id = 0;
		for (MarketDataField field : fieldList) {

			field.setId(id++);
		}

		intervalDates = new ArrayList<>();
		LocalDate runningDate = startDate;
		while (runningDate.compareTo(endDate) <= 0) {

			intervalDates.add(runningDate);
			runningDate = runningDate.plus(1, ChronoUnit.DAYS);
		}
		command.setStartDate(DateUtils.asUtilDate(startDate));
		command.setEndDate(DateUtils.asUtilDate(endDate));
		command.setSkipDataRequestIfAlreadyExists(true);

		// equivalency for MarketDataField should depend on ID.
		MarketDataField tmpMarketDataField1 = new MarketDataField("ONE");
		tmpMarketDataField1.setId((short) 0);
		MarketDataField tmpMarketDataFieldCopy = new MarketDataField("ONE");
		tmpMarketDataFieldCopy.setId((short) 0);
		MatcherAssert.assertThat(tmpMarketDataField1, CoreMatchers.equalTo(tmpMarketDataFieldCopy));
		// change ID, the equivalency check should fail.
		tmpMarketDataFieldCopy.setId((short) 9);
		MatcherAssert.assertThat(tmpMarketDataField1, CoreMatchers.not(CoreMatchers.equalTo(tmpMarketDataFieldCopy)));

		// equivalency of lists depends on actual list contents and ultimately ID.
		List<MarketDataField> copyFieldList = new ArrayList<>();
		copyFieldList.add(new MarketDataField("ONE"));
		copyFieldList.add(new MarketDataField("TWO"));

		id = 0;
		for (MarketDataField field : copyFieldList) {

			field.setId(id++);
		}

		// second list is missing the third MarketDataField, should not equal
		MatcherAssert.assertThat(new MarketDataFieldRequest(security, new HashSet<>(copyFieldList), startDate, LocalDate.of(2016, 3, 22)),
				CoreMatchers.not(CoreMatchers.equalTo(new MarketDataFieldRequest(security, new HashSet<>(fieldList), startDate, LocalDate.of(2016, 3, 22)))));

		MarketDataField copyThird = new MarketDataField("THREE");
		copyThird.setId(id);
		copyFieldList.add(copyThird);

		// added the third MarketDataField, they are are equivalent.
		MatcherAssert.assertThat(new MarketDataFieldRequest(security, new HashSet<>(copyFieldList), startDate, LocalDate.of(2016, 3, 22)),
				CoreMatchers.equalTo(new MarketDataFieldRequest(security, new HashSet<>(fieldList), startDate, LocalDate.of(2016, 3, 22))));
	}


	@Test
	public void noSkippingAllowed() {

		MarketDataFieldValueUpdaterCommand noSkipping = new MarketDataFieldValueUpdaterCommand();
		noSkipping.setStartDate(command.getStartDate());
		noSkipping.setEndDate(command.getEndDate());
		noSkipping.setSkipDataRequestIfAlreadyExists(false);
		MarketDataFieldRequestCollection requests = new MarketDataFieldRequestCollection(security, fieldList, noSkipping);

		List<MarketDataFieldRequest> requestList = new ArrayList<>();
		for (MarketDataFieldRequest request : CollectionUtils.getIterable(requests.populateRequests())) {

			requestList.add(request);
		}
		MatcherAssert.assertThat(requestList.size(), CoreMatchers.equalTo(1));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), startDate, endDate)));
	}


	@Test
	public void addMissingFieldValueSameFieldsTest() {

		MarketDataFieldRequestCollection requests = new MarketDataFieldRequestCollection(security, fieldList, command);

		for (MarketDataField field : fieldList) {

			for (LocalDate date : intervalDates) {

				requests.addMissingFieldValue(field, date);
			}
		}

		List<MarketDataFieldRequest> requestList = new ArrayList<>();
		for (MarketDataFieldRequest request : CollectionUtils.getIterable(requests.populateRequests())) {

			requestList.add(request);
		}
		MatcherAssert.assertThat(requestList.size(), CoreMatchers.equalTo(1));

		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), startDate, endDate)));
	}


	@Test
	public void addMissingFieldValueDiffFieldsTest() {

		MarketDataFieldRequestCollection requests = new MarketDataFieldRequestCollection(security, fieldList, command);

		for (MarketDataField field : fieldList) {

			for (LocalDate date : intervalDates) {

				if (WeekEnd.contains(date.getDayOfWeek())) {
					requests.addNonBusinessDate(date);
				}
				else {
					if (date.getDayOfWeek() == DayOfWeek.MONDAY
							&& field.equals(fieldList.get(0))) {

						continue;
					}
					requests.addMissingFieldValue(field, date);
				}
			}
		}

		List<MarketDataFieldRequest> requestList = new ArrayList<>();
		for (MarketDataFieldRequest request : CollectionUtils.getIterable(requests.populateRequests())) {

			requestList.add(request);
		}
		MatcherAssert.assertThat(requestList.size(), CoreMatchers.equalTo(5));

		List<MarketDataField> monday = new ArrayList<>(fieldList);
		monday.remove(0);
		Assertions.assertEquals(2, monday.size());

		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(monday), LocalDate.of(2016, 3, 21), LocalDate.of(2016, 3, 21))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 3, 22), LocalDate.of(2016, 3, 27))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(monday), LocalDate.of(2016, 3, 28), LocalDate.of(2016, 3, 28))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 3, 29), LocalDate.of(2016, 4, 3))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(monday), LocalDate.of(2016, 4, 4), LocalDate.of(2016, 4, 4))));
	}


	@Test
	public void addMissingFieldValueDiffFieldsWedDataTest() {

		MarketDataFieldRequestCollection requests = new MarketDataFieldRequestCollection(security, fieldList, command);

		for (MarketDataField field : fieldList) {

			for (LocalDate date : intervalDates) {

				if (WeekEnd.contains(date.getDayOfWeek())) {
					requests.addNonBusinessDate(date);
				}
				else {
					// we already fetched one field value on mondays
					if (date.getDayOfWeek() == DayOfWeek.MONDAY
							&& field.equals(fieldList.get(0))) {

						continue;
					}
					// we already fetched ALL field values for wednesdays
					if (date.getDayOfWeek() != DayOfWeek.WEDNESDAY) {

						requests.addMissingFieldValue(field, date);
					}
				}
			}
		}

		List<MarketDataFieldRequest> requestList = new ArrayList<>();
		for (MarketDataFieldRequest request : CollectionUtils.getIterable(requests.populateRequests())) {

			requestList.add(request);
		}
		MatcherAssert.assertThat(requestList.size(), CoreMatchers.equalTo(7));

		// Mondays, only has data missing for 2 fields.
		List<MarketDataField> monday = new ArrayList<>(fieldList);
		monday.remove(0);
		Assertions.assertEquals(2, monday.size());

		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(monday), LocalDate.of(2016, 3, 21), LocalDate.of(2016, 3, 21))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 3, 22), LocalDate.of(2016, 3, 22))));
		// 3/23/2016 is a wednesday, no missing data.
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 3, 24), LocalDate.of(2016, 3, 27))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(monday), LocalDate.of(2016, 3, 28), LocalDate.of(2016, 3, 28))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 3, 29), LocalDate.of(2016, 3, 29))));
		// 3/30/2016 is a wednesday, no missing data.
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 3, 31), LocalDate.of(2016, 4, 3))));
		Assertions.assertTrue(requestList.contains(new MarketDataFieldRequest(security, new HashSet<>(monday), LocalDate.of(2016, 4, 4), LocalDate.of(2016, 4, 4))));
	}


	@Test
	public void rangeWithSkippedHolidayDataTest() {

		MarketDataFieldValueUpdaterCommand tmp = new MarketDataFieldValueUpdaterCommand();
		tmp.setStartDate(DateUtils.asUtilDate(LocalDate.of(2016, 7, 1)));
		tmp.setEndDate(DateUtils.asUtilDate(LocalDate.of(2016, 7, 18)));
		tmp.setSkipDataRequestIfAlreadyExists(true);
		MarketDataFieldRequestCollection requests = new MarketDataFieldRequestCollection(security, fieldList, command);
		// holidays and weekends
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 2));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 3));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 4));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 9));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 10));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 16));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 17));
		// missing data
		for (MarketDataField field : fieldList) {
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 1));
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 5));
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 14));
		}
		List<MarketDataFieldRequest> requestList = requests.populateRequests();

		List<MarketDataFieldRequest> expected = Arrays.asList(
				new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 7, 1), LocalDate.of(2016, 7, 5)),
				new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 7, 14), LocalDate.of(2016, 7, 14))
		);
		MatcherAssert.assertThat(expected, IsEqual.equalTo(requestList));
	}


	@Test
	public void rangeWithSkippedDataTest() {

		MarketDataFieldValueUpdaterCommand tmp = new MarketDataFieldValueUpdaterCommand();
		tmp.setStartDate(DateUtils.asUtilDate(LocalDate.of(2016, 7, 1)));
		tmp.setEndDate(DateUtils.asUtilDate(LocalDate.of(2016, 7, 18)));
		tmp.setSkipDataRequestIfAlreadyExists(true);
		MarketDataFieldRequestCollection requests = new MarketDataFieldRequestCollection(security, fieldList, command);
		// holidays and weekends
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 2));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 3));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 4));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 9));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 10));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 16));
		requests.addNonBusinessDate(LocalDate.of(2016, 7, 17));
		// missing data
		for (MarketDataField field : fieldList) {
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 1));
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 5));
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 7));
			requests.addMissingFieldValue(field, LocalDate.of(2016, 7, 14));
		}
		List<MarketDataFieldRequest> requestList = requests.populateRequests();

		List<MarketDataFieldRequest> expected = Arrays.asList(
				new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 7, 1), LocalDate.of(2016, 7, 5)),
				new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 7, 7), LocalDate.of(2016, 7, 7)),
				new MarketDataFieldRequest(security, new HashSet<>(fieldList), LocalDate.of(2016, 7, 14), LocalDate.of(2016, 7, 14))
		);
		MatcherAssert.assertThat(expected, IsEqual.equalTo(requestList));
	}
}
