package com.clifton.marketdata.updater;

import com.clifton.core.beans.BeanUtils;
import com.clifton.core.dataaccess.dao.AdvancedUpdatableDAO;
import com.clifton.core.util.StringUtils;
import com.clifton.core.util.compare.CompareUtils;
import com.clifton.core.util.date.DateUtils;
import com.clifton.core.util.date.Time;
import com.clifton.core.util.status.Status;
import com.clifton.core.util.status.StatusDetail;
import com.clifton.core.util.status.StatusHolder;
import com.clifton.investment.exchange.InvestmentExchangeTypes;
import com.clifton.investment.instrument.InvestmentInstrumentBuilder;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.instrument.InvestmentSecurityBuilder;
import com.clifton.investment.instrument.search.SecuritySearchForm;
import com.clifton.marketdata.datasource.MarketDataSource;
import com.clifton.marketdata.datasource.MarketDataSourceBuilder;
import com.clifton.marketdata.datasource.MarketDataSourcePricingSource;
import com.clifton.marketdata.datasource.MarketDataSourceSector;
import com.clifton.marketdata.datasource.MarketDataSourceService;
import com.clifton.marketdata.field.MarketDataField;
import com.clifton.marketdata.field.MarketDataFieldGroup;
import com.clifton.marketdata.field.MarketDataFieldServiceImpl;
import com.clifton.marketdata.field.MarketDataValue;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.field.mapping.MarketDataFieldMapping;
import com.clifton.marketdata.field.mapping.MarketDataFieldMappingRetriever;
import com.clifton.marketdata.field.search.MarketDataValueSearchForm;
import com.clifton.marketdata.provider.DataFieldValueCommand;
import com.clifton.marketdata.provider.DataFieldValueResponse;
import com.clifton.marketdata.provider.MarketDataProvider;
import com.clifton.marketdata.provider.MarketDataProviderLocator;
import com.clifton.marketdata.updater.jobs.MarketDataFieldValueUpdaterJob;
import com.clifton.core.util.MathUtils;
import org.hamcrest.MatcherAssert;
import org.hamcrest.core.StringContains;
import org.hibernate.Criteria;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.ArgumentMatchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;


@ContextConfiguration
@ExtendWith({SpringExtension.class, MockitoExtension.class})
class MarketDataUpdaterServiceTest {

	@Resource
	private MarketDataUpdaterServiceImpl marketDataUpdaterService;

	@Resource
	private Map<String, MarketDataFieldValueUpdaterJob> testConfigurations;

	@Mock
	private MarketDataSourceService marketDataSourceService;

	@Mock
	private MarketDataProviderLocator marketDataProviderLocator;

	@Mock
	private InvestmentInstrumentService investmentInstrumentService;

	@Mock
	private MarketDataProvider<MarketDataValue> dataProvider;

	@Mock
	private MarketDataFieldMappingRetriever marketDataFieldMappingRetriever;

	@Mock
	private AdvancedUpdatableDAO<MarketDataValue, Criteria> marketDataValueDAO;

	@Spy
	private MarketDataFieldServiceImpl marketDataFieldService;

	private final InvestmentSecurity apple = InvestmentSecurityBuilder.newStock("APPL")
			.withId(1)
			.withInstrument(InvestmentInstrumentBuilder.createDiax().toInvestmentInstrument())
			.toInvestmentSecurity();

	private final MarketDataSource bloombergMarketDataSource = MarketDataSourceBuilder.createBloomberg().toMarketDataSource();

	// isUpToOnePerDay = true, isLatestValueOnly = false, isCaptureChangesOnly = false, isTimeSensitive = true
	private final MarketDataField lastTradePriceMarketDataField = new MarketDataField();

	private final Map<String, Object> runContext = new HashMap<>();


	@BeforeEach
	public void beforeEach() {
		this.bloombergMarketDataSource.setMarketDataValueSupported(true);

		MarketDataFieldMapping marketDataFieldMapping = new MarketDataFieldMapping();
		MarketDataFieldGroup fieldGroup = new MarketDataFieldGroup();
		fieldGroup.setId((short) 1);
		marketDataFieldMapping.setFieldGroup(fieldGroup);

		this.lastTradePriceMarketDataField.setId((short) 4);
		this.lastTradePriceMarketDataField.setName("Last Trade Price");
		this.lastTradePriceMarketDataField.setExternalFieldName("PX_LAST");
		this.lastTradePriceMarketDataField.setTimeSensitive(true);
		this.lastTradePriceMarketDataField.setDecimalPrecision(15);
		this.lastTradePriceMarketDataField.setUpToOnePerDay(true);

		MarketDataSourceSector sourceSector = new MarketDataSourceSector();
		sourceSector.setId((short) 4);
		sourceSector.setName("Equity");
		marketDataFieldMapping.setMarketSector(sourceSector);
		MarketDataSourcePricingSource marketDataSourcePricingSource = new MarketDataSourcePricingSource();
		marketDataSourcePricingSource.setId((short) 1);
		marketDataSourcePricingSource.setName("Bloomberg Valuation Service");
		marketDataFieldMapping.setPricingSource(marketDataSourcePricingSource);
		marketDataFieldMapping.setExchangeCodeType(InvestmentExchangeTypes.PRIMARY_EXCHANGE);

		this.marketDataUpdaterService.setMarketDataSourceService(this.marketDataSourceService);
		this.marketDataUpdaterService.setMarketDataProviderLocator(this.marketDataProviderLocator);
		this.marketDataUpdaterService.setInvestmentInstrumentService(this.investmentInstrumentService);
		this.marketDataUpdaterService.setMarketDataFieldMappingRetriever(this.marketDataFieldMappingRetriever);
		this.marketDataUpdaterService.setMarketDataFieldService(this.marketDataFieldService);
		this.marketDataFieldService.setInvestmentInstrumentService(this.investmentInstrumentService);
		this.marketDataFieldService.setMarketDataValueDAO(this.marketDataValueDAO);

		Mockito.when(this.marketDataSourceService.getMarketDataSourceByName(this.bloombergMarketDataSource.getName()))
				.thenReturn(this.bloombergMarketDataSource);
		Mockito.when(this.marketDataProviderLocator.locate(this.bloombergMarketDataSource))
				.thenAnswer(invocation -> this.dataProvider);
		Mockito.when(this.investmentInstrumentService.getInvestmentSecurityList(Mockito.any(SecuritySearchForm.class)))
				.thenReturn(Collections.singletonList(this.apple));
		Mockito.lenient().when(this.investmentInstrumentService.getInvestmentSecurity(this.apple.getId())).thenReturn(this.apple);
		Mockito.when(this.marketDataFieldMappingRetriever.getMarketDataFieldMappingByInstrument(this.apple.getInstrument(), this.bloombergMarketDataSource))
				.thenReturn(marketDataFieldMapping);
		Mockito.doReturn((Collections.singletonList(this.lastTradePriceMarketDataField)))
				.when(this.marketDataFieldService)
				.getMarketDataFieldListByGroup(fieldGroup.getId());
		Mockito.lenient().doReturn(this.lastTradePriceMarketDataField)
				.when(this.marketDataFieldService)
				.getMarketDataFieldByName(this.lastTradePriceMarketDataField.getName());
		Mockito.lenient().doReturn(this.lastTradePriceMarketDataField)
				.when(this.marketDataFieldService)
				.getMarketDataField(this.lastTradePriceMarketDataField.getId());
	}


	@Test
	public void test1_A() {
		StatusHolder statusHolder = new StatusHolder("test1");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test1");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date now = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, new Time(now), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		now = new Date();
		now = DateUtils.addSeconds(now, 4);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);

		Status status = testJob.run(this.runContext);

		// still query data provider, start and end dates are today
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayFrom());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayTo());

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		// no update or inserts
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(Mockito.any());

		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 0; Failed: 1", status.getMessage());
		Assertions.assertEquals(1, status.getErrorCount());
		MatcherAssert.assertThat(status.getErrorList().get(0).getNote(), StringContains.containsString("Cannot add another measure for field 'Last Trade Price' on the same date. Update existing measure instead."));
	}


	@Test
	public void test1_B() {
		StatusHolder statusHolder = new StatusHolder("test1");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test1");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date now = new Date();
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// still query data provider, start and end dates are today
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayFrom());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayTo());

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// insert new value timestamp
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test2_A() {
		StatusHolder statusHolder = new StatusHolder("test2");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test2");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date now = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, new Time(now), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		now = new Date();
		now = DateUtils.addSeconds(now, 4);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// still query data provider, start and end dates are today
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayFrom());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayTo());

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// updates existing value timestamp
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertFalse(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test2_B() {
		StatusHolder statusHolder = new StatusHolder("test2");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test2");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date now = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, new Time(now), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);

		Status status = testJob.run(this.runContext);

		// still query data provider, start and end dates are today
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayFrom());
		Assertions.assertEquals(0, testJob.getDaysBackIncludingTodayTo());

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(Mockito.any());

		// no updates or inserts the values are the same
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test3_A() {
		StatusHolder statusHolder = new StatusHolder("test3");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test3");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, new Time(now), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time different from the provider
		now = new Date();
		now = DateUtils.addSeconds(now, 4);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		// no update or inserts
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(Mockito.any());

		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 0; Failed: 1", status.getMessage());
		Assertions.assertEquals(1, status.getErrorCount());
		MatcherAssert.assertThat(status.getErrorList().get(0).getNote(), StringContains.containsString("Cannot add another measure for field 'Last Trade Price' on the same date. Update existing measure instead."));
	}


	@Test
	public void test3_B() {
		StatusHolder statusHolder = new StatusHolder("test3");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test3");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// insert new value timestamp
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test4_A() {
		StatusHolder statusHolder = new StatusHolder("test4");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test4");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, new Time(now), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time different from the provider
		now = new Date();
		now = DateUtils.addSeconds(now, 4);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// updates existing value timestamp
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertFalse(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test4_B() {
		StatusHolder statusHolder = new StatusHolder("test4");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test4");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, new Time(now), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, new Time(now), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(Mockito.any());

		// no updates or inserts the values are the same
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test5_A() {
		StatusHolder statusHolder = new StatusHolder("test5");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test5");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date valueDate = new Date();
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, valueDate, new Time(valueDate), false);

		// get existing
		Mockito.doAnswer(a -> {
			InvestmentSecurity investmentSecurity = a.getArgument(0, InvestmentSecurity.class);
			Date date = a.getArgument(1, Date.class);
			String fieldName = a.getArgument(2, String.class);
			Short dataSourceId = a.getArgument(3, Short.class);
			boolean matches = MathUtils.isEqual(investmentSecurity.getId(), existingMarketValue.getInvestmentSecurity().getId())
					&& StringUtils.isEqual(fieldName, existingMarketValue.getDataField().getName())
					&& MathUtils.isEqual(dataSourceId, existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(date, existingMarketValue.getMeasureDate());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return new MarketDataValueHolder(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		Status status = testJob.run(this.runContext);

		// already exists in database, no need to fetch from provider.
		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(Mockito.any());
		// no update or inserts
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(Mockito.any());

		Assertions.assertEquals("Securities: 1; Processed: 0; Skipped: 1; Values Updated: 0; Values Skipped: 0; Failed: 0", status.getMessage());
		Assertions.assertEquals(0, status.getErrorCount());
	}


	@Test
	public void test5_B() {
		StatusHolder statusHolder = new StatusHolder("test5");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test5");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date valueDate = new Date();
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, valueDate, new Time(valueDate), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		// get existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// insert new value timestamp
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test6_A() {
		StatusHolder statusHolder = new StatusHolder("test6");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test6");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isLatestValueIsForPreviousWeekday());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time different from the provider
		previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		previousWorkday = DateUtils.addSeconds(previousWorkday, 4);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// updates existing value timestamp
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertFalse(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test6_B() {
		StatusHolder statusHolder = new StatusHolder("test6");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test6");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isLatestValueIsForPreviousWeekday());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(Mockito.any());

		// no updates or inserts the values are the same
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test7_A() {
		StatusHolder statusHolder = new StatusHolder("test7");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test7");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isLatestValueIsForPreviousWeekday());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), false);

		// get existing
		Mockito.doAnswer(a -> {
			InvestmentSecurity investmentSecurity = a.getArgument(0, InvestmentSecurity.class);
			Date date = a.getArgument(1, Date.class);
			String fieldName = a.getArgument(2, String.class);
			Short dataSourceId = a.getArgument(3, Short.class);
			boolean matches = MathUtils.isEqual(investmentSecurity.getId(), existingMarketValue.getInvestmentSecurity().getId())
					&& StringUtils.isEqual(fieldName, existingMarketValue.getDataField().getName())
					&& MathUtils.isEqual(dataSourceId, existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(date, existingMarketValue.getMeasureDate());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return new MarketDataValueHolder(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 0; Skipped: 1; Values Updated: 0; Values Skipped: 0; Failed: 0", status.getMessage());
	}


	@Test
	public void test7_B() {
		StatusHolder statusHolder = new StatusHolder("test7");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test7");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isLatestValueIsForPreviousWeekday());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		// get existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());
		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(Mockito.any());

		// inserts the value
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test8_A() {
		StatusHolder statusHolder = new StatusHolder("test8");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test8");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		previousWorkday = DateUtils.addMilliseconds(previousWorkday, 4);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);
		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		Status status = testJob.run(this.runContext);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// updates existing bean
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertFalse(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test8_B() {
		StatusHolder statusHolder = new StatusHolder("test8");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test8");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		// get existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);
		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		Status status = testJob.run(this.runContext);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// inserts new bean
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test9_A() {
		StatusHolder statusHolder = new StatusHolder("test9");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test9");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), false);

		// get existing
		Mockito.doAnswer(a -> {
			InvestmentSecurity investmentSecurity = a.getArgument(0, InvestmentSecurity.class);
			Date date = a.getArgument(1, Date.class);
			String fieldName = a.getArgument(2, String.class);
			Short dataSourceId = a.getArgument(3, Short.class);
			boolean matches = MathUtils.isEqual(investmentSecurity.getId(), existingMarketValue.getInvestmentSecurity().getId())
					&& StringUtils.isEqual(fieldName, existingMarketValue.getDataField().getName())
					&& MathUtils.isEqual(dataSourceId, existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(date, existingMarketValue.getMeasureDate());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return new MarketDataValueHolder(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		Status status = testJob.run(this.runContext);

		// already exists, do nothing.
		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 0; Skipped: 1; Values Updated: 0; Values Skipped: 0; Failed: 0", status.getMessage());
	}


	@Test
	public void test9_B() {
		StatusHolder statusHolder = new StatusHolder("test9");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test9");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		// get existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);
		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		Status status = testJob.run(this.runContext);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// inserts new bean
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test10_A() {
		StatusHolder statusHolder = new StatusHolder("test10");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test10");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertEquals(Calendar.WEDNESDAY, (int) testJob.getSkipOnWeekdayId());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		LocalDate previousWednesday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.WEDNESDAY));
		int adjustment = (int) ChronoUnit.DAYS.between(previousWednesday, LocalDate.now());
		testJob.setDaysBackIncludingTodayFrom(adjustment);
		testJob.setDaysBackIncludingTodayTo(adjustment);

		Date marketDate = DateUtils.asUtilDate(previousWednesday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, marketDate, new Time(marketDate), false);

		// get existing
		Mockito.doAnswer(a -> {
			InvestmentSecurity investmentSecurity = a.getArgument(0, InvestmentSecurity.class);
			Date date = a.getArgument(1, Date.class);
			String fieldName = a.getArgument(2, String.class);
			Short dataSourceId = a.getArgument(3, Short.class);
			boolean matches = MathUtils.isEqual(investmentSecurity.getId(), existingMarketValue.getInvestmentSecurity().getId())
					&& StringUtils.isEqual(fieldName, existingMarketValue.getDataField().getName())
					&& MathUtils.isEqual(dataSourceId, existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(date, existingMarketValue.getMeasureDate());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return new MarketDataValueHolder(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		Status status = testJob.run(this.runContext);

		// already exists, do nothing.
		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 0; Skipped: 1; Values Updated: 0; Values Skipped: 0; Failed: 0", status.getMessage());
		Assertions.assertEquals(2, status.getDetailList().size());
		List<String> detailNotes = status.getDetailList().stream().map(StatusDetail::getNote).collect(Collectors.toList());
		Assertions.assertTrue(detailNotes.contains("Equities / Funds / CEF: DIAX: APPL skipped because existing data is present"));
		Assertions.assertTrue(detailNotes.contains("No values returned"));
	}


	@Test
	public void test10_B() {
		StatusHolder statusHolder = new StatusHolder("test10");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test10");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		// get existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		LocalDate previousWednesday = LocalDate.now().with(TemporalAdjusters.previous(DayOfWeek.WEDNESDAY));
		int adjustment = (int) ChronoUnit.DAYS.between(previousWednesday, LocalDate.now());
		testJob.setDaysBackIncludingTodayFrom(adjustment);
		testJob.setDaysBackIncludingTodayTo(adjustment);

		Date marketDate = DateUtils.asUtilDate(previousWednesday);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, marketDate, new Time(marketDate), true);
		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		Status status = testJob.run(this.runContext);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// inserts new bean
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void test16_A() {
		StatusHolder statusHolder = new StatusHolder("test16");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test16");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);
		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		Status status = testJob.run(this.runContext);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).getMarketDataValueList(Mockito.any());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		// nothing, its already there
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 0; Failed: 1", status.getMessage());
		Assertions.assertEquals(1, status.getErrorCount());
		MatcherAssert.assertThat(status.getErrorList().get(0).getNote(), StringContains.containsString("Cannot add another measure for field 'Last Trade Price' on the same date. Update existing measure instead."));
	}


	@Test
	public void test16_B() {
		StatusHolder statusHolder = new StatusHolder("test16");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("test16");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertFalse(testJob.isLatestValuesOnly());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		// get existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		Date previousWorkday = new Date();
		previousWorkday = DateUtils.getPreviousWeekday(previousWorkday);
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, previousWorkday, new Time(previousWorkday), true);
		Mockito.when(this.dataProvider.getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any())).thenReturn(Collections.singletonList(providerMarketValue));

		Status status = testJob.run(this.runContext);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);
		ArgumentCaptor<MarketDataValue> marketDataValueDaoCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueListHistorical(ArgumentMatchers.any(), ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).getMarketDataValueList(Mockito.any());
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueDaoCaptor.capture());

		// inserts new value
		Assertions.assertEquals(updateIfExistsCaptor.getValue(), testJob.isOverwriteExistingData());
		MarketDataValue value = marketDataValueDaoCaptor.getValue();
		Assertions.assertTrue(value.isNewBean());
		Assertions.assertTrue(CompareUtils.isEqual(value.getMeasureTime(), providerMarketValue.getMeasureTime()));
		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void timestamp3_A() {
		StatusHolder statusHolder = new StatusHolder("timestamp3");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp3");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		Time time = Time.parse("17:15:00");
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, time, false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time the same
		now = new Date();
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);


		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		// no update or inserts
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 0; Failed: 1", status.getMessage());
		Assertions.assertEquals(1, status.getErrorCount());
		MatcherAssert.assertThat(status.getErrorList().get(0).getNote(), StringContains.containsString("Cannot add another measure for field 'Last Trade Price' on the same date. Update existing measure instead."));
	}


	@Test
	public void timestamp3_B() {
		StatusHolder statusHolder = new StatusHolder("timestamp3");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp3");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		Time time = Time.parse("17:14:00");
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, time, false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the provider measure time newer
		now = new Date();
		time = Time.parse("17:30:00");
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		// update timestamp, should be the same as the override
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 0; Failed: 1", status.getMessage());
		Assertions.assertEquals(1, status.getErrorCount());
		MatcherAssert.assertThat(status.getErrorList().get(0).getNote(), StringContains.containsString("Cannot add another measure for field 'Last Trade Price' on the same date. Update existing measure instead."));
	}


	@Test
	public void timestamp3_C() {
		StatusHolder statusHolder = new StatusHolder("timestamp3");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp3");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		// no existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time newer
		Date now = new Date();
		Time time = Time.parse("17:30:00");
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(Mockito.any(MarketDataValue.class), Mockito.anyBoolean());
		// insert
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueCaptor.capture());
		MarketDataValue updateMarketDataValue = marketDataValueCaptor.getValue();
		Assertions.assertEquals(updateMarketDataValue.getMeasureTime(), testJob.getLatestValuesOverrideTimestamp());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void timestamp4_A() {
		StatusHolder statusHolder = new StatusHolder("timestamp4");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp4");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		Time time = Time.parse("17:15:00");
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, time, false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time the same
		now = new Date();
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);
		ArgumentCaptor<Boolean> updateIfExistsCaptor = ArgumentCaptor.forClass(Boolean.class);


		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(marketDataValueCaptor.capture(), updateIfExistsCaptor.capture());
		// no updates
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void timestamp4_B() {
		StatusHolder statusHolder = new StatusHolder("timestamp4");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp4");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		Time time = Time.parse("17:14:00");
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, time, false);

		// get existing
		Mockito.doAnswer(a -> {
			MarketDataValueSearchForm searchForm = a.getArgument(0);
			boolean matches = MathUtils.isEqual(searchForm.getDataFieldId(), existingMarketValue.getDataField().getId())
					&& MathUtils.isEqual(searchForm.getDataSourceId(), existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(searchForm.getMeasureDate(), existingMarketValue.getMeasureDate())
					&& MathUtils.isEqual(searchForm.getInvestmentSecurityId(), existingMarketValue.getInvestmentSecurity().getId());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return Collections.singletonList(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the provider measure time newer
		now = new Date();
		time = Time.parse("17:30:00");
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		// update timestamp, should be the same as the override
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueCaptor.capture());
		MarketDataValue updateMarketDataValue = marketDataValueCaptor.getValue();
		Assertions.assertEquals(updateMarketDataValue.getMeasureTime(), testJob.getLatestValuesOverrideTimestamp());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void timestamp4_C() {
		StatusHolder statusHolder = new StatusHolder("timestamp4");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp4");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertTrue(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		// no existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueList(Mockito.any());

		// make the existing measure time newer
		Date now = new Date();
		Time time = Time.parse("17:30:00");
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(Mockito.any(MarketDataValue.class), Mockito.anyBoolean());
		// insert
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueCaptor.capture());
		MarketDataValue updateMarketDataValue = marketDataValueCaptor.getValue();
		Assertions.assertEquals(updateMarketDataValue.getMeasureTime(), testJob.getLatestValuesOverrideTimestamp());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void timestamp5_A() {
		StatusHolder statusHolder = new StatusHolder("timestamp5");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp5");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Date now = new Date();
		Time time = Time.parse("17:15:00");
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, time, false);

		// get existing
		Mockito.doAnswer(a -> {
			InvestmentSecurity investmentSecurity = a.getArgument(0, InvestmentSecurity.class);
			Date date = a.getArgument(1, Date.class);
			String fieldName = a.getArgument(2, String.class);
			Short dataSourceId = a.getArgument(3, Short.class);
			boolean matches = MathUtils.isEqual(investmentSecurity.getId(), existingMarketValue.getInvestmentSecurity().getId())
					&& StringUtils.isEqual(fieldName, existingMarketValue.getDataField().getName())
					&& MathUtils.isEqual(dataSourceId, existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(date, existingMarketValue.getMeasureDate());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return new MarketDataValueHolder(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());


		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(0)).getMarketDataValueLatest(ArgumentMatchers.any());
		Mockito.verify(this.marketDataFieldService, Mockito.times(0)).saveMarketDataValueWithOptions(ArgumentMatchers.any(MarketDataValue.class), ArgumentMatchers.anyBoolean());
		// no update or inserts
		Mockito.verify(this.marketDataValueDAO, Mockito.times(0)).save(ArgumentMatchers.any());

		Assertions.assertEquals("Securities: 1; Processed: 0; Skipped: 1; Values Updated: 0; Values Skipped: 0; Failed: 0", status.getMessage());
		Assertions.assertEquals(0, status.getErrorCount());
	}


	@Test
	public void timestamp5_B() {
		StatusHolder statusHolder = new StatusHolder("timestamp5");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp5");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		Date now = new Date();
		Time time = Time.parse("17:14:00");
		MarketDataValue existingMarketValue = createMarketDataValue(this.apple, now, time, false);

		// get existing
		Mockito.doAnswer(a -> {
			InvestmentSecurity investmentSecurity = a.getArgument(0, InvestmentSecurity.class);
			Date date = a.getArgument(1, Date.class);
			String fieldName = a.getArgument(2, String.class);
			Short dataSourceId = a.getArgument(3, Short.class);
			boolean matches = MathUtils.isEqual(investmentSecurity.getId(), existingMarketValue.getInvestmentSecurity().getId())
					&& StringUtils.isEqual(fieldName, existingMarketValue.getDataField().getName())
					&& MathUtils.isEqual(dataSourceId, existingMarketValue.getDataSource().getId())
					&& DateUtils.isEqual(date, existingMarketValue.getMeasureDate());
			if (matches) {
				MarketDataValue value = new MarketDataValue();
				BeanUtils.copyProperties(existingMarketValue, value);
				return new MarketDataValueHolder(value);
			}
			else {
				return null;
			}
		})
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		// make the provider measure time newer
		now = new Date();
		time = Time.parse("17:30:00");
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		Status status = testJob.run(this.runContext);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		// update timestamp, should be the same as the override
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueCaptor.capture());
		MarketDataValue updateMarketDataValue = marketDataValueCaptor.getValue();
		Assertions.assertEquals(updateMarketDataValue.getMeasureTime(), testJob.getLatestValuesOverrideTimestamp());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	@Test
	public void timestamp5_C() {
		StatusHolder statusHolder = new StatusHolder("timestamp5");
		MarketDataFieldValueUpdaterJob testJob = this.testConfigurations.get("timestamp5");
		testJob.setStatusHolderObject(statusHolder);
		testJob.validate();
		Assertions.assertFalse(testJob.isBatchDataRequests());
		Assertions.assertFalse(testJob.isOverwriteExistingData());
		Assertions.assertTrue(testJob.isLatestValuesOnly());
		Assertions.assertTrue(testJob.isSkipDataRequestIfAlreadyExists());
		Assertions.assertNotNull(testJob.getLatestValuesOverrideTimestamp());
		Assertions.assertTrue(this.lastTradePriceMarketDataField.isUpToOnePerDay());

		Map<InvestmentSecurity, Map<MarketDataField, MarketDataValue>> securityDataFieldValueMap = new HashMap<>();
		Map<MarketDataField, MarketDataValue> marketDataFieldMap = new HashMap<>();

		// no existing
		Mockito.doAnswer(a -> null)
				.when(this.marketDataFieldService)
				.getMarketDataValueForDate(Mockito.any(InvestmentSecurity.class), Mockito.any(Date.class), Mockito.anyString(), Mockito.anyShort(), Mockito.anyBoolean(), Mockito.anyBoolean());

		// make the existing measure time newer
		Date now = new Date();
		Time time = Time.parse("17:30:00");
		MarketDataValue providerMarketValue = createMarketDataValue(this.apple, now, time, true);

		marketDataFieldMap.put(this.lastTradePriceMarketDataField, providerMarketValue);
		securityDataFieldValueMap.put(this.apple, marketDataFieldMap);
		DataFieldValueResponse<MarketDataValue> fieldValueResponse = DataFieldValueResponse.createResponse(securityDataFieldValueMap);
		Mockito.when(this.dataProvider.getMarketDataValueLatest(ArgumentMatchers.any())).thenReturn(fieldValueResponse);
		ArgumentCaptor<DataFieldValueCommand> dataFieldValueCommandCapture = ArgumentCaptor.forClass(DataFieldValueCommand.class);

		ArgumentCaptor<MarketDataValue> marketDataValueCaptor = ArgumentCaptor.forClass(MarketDataValue.class);

		Status status = testJob.run(this.runContext);

		// get from data provider
		Mockito.verify(this.dataProvider, Mockito.times(1)).getMarketDataValueLatest(dataFieldValueCommandCapture.capture());
		Mockito.verify(this.marketDataFieldService, Mockito.times(1)).saveMarketDataValueWithOptions(Mockito.any(MarketDataValue.class), Mockito.anyBoolean());
		// insert
		Mockito.verify(this.marketDataValueDAO, Mockito.times(1)).save(marketDataValueCaptor.capture());
		MarketDataValue updateMarketDataValue = marketDataValueCaptor.getValue();
		Assertions.assertEquals(updateMarketDataValue.getMeasureTime(), testJob.getLatestValuesOverrideTimestamp());

		Assertions.assertEquals("Securities: 1; Processed: 1; Skipped: 0; Values Updated: 0; Values Skipped: 1; Failed: 0", status.getMessage());
	}


	private MarketDataValue createMarketDataValue(InvestmentSecurity security, Date measureDate, Time measureTime, boolean providerValue) {
		MarketDataValue marketDataValue = new MarketDataValue();
		if (!providerValue) {
			marketDataValue.setId(1);
		}
		marketDataValue.setMeasureTime(measureTime);
		marketDataValue.setMeasureDate(DateUtils.clearTime(measureDate));
		marketDataValue.setMeasureValue(new BigDecimal("120.88"));
		marketDataValue.setDataField(this.lastTradePriceMarketDataField);
		marketDataValue.setDataSource(this.bloombergMarketDataSource);
		marketDataValue.setInvestmentSecurity(security);
		return marketDataValue;
	}
}
