CREATE FUNCTION InvestmentSecurityLookup
(
	@Symbol nvarchar(50),
	@DataSourceID INT
)
RETURNS INT
AS
BEGIN
	
	DECLARE @InvestmentSecurityID INT
	DECLARE @SectorName nvarchar(50)
	
	SELECT
		@InvestmentSecurityID = InvestmentSecurityID
	FROM MarketDataSourceSecurity mdss
	WHERE DataSourceSymbol = @Symbol AND mdss.MarketDataSourceID = @DataSourceID
	
	IF(@InvestmentSecurityID IS NOT NULL) RETURN @InvestmentSecurityID
		
	SET @SectorName = SUBSTRING(@Symbol, LEN(@Symbol) - CHARINDEX(' ', REVERSE(@Symbol)) + 2, LEN(@Symbol))

	IF (SELECT COUNT(*) FROM MarketDataSourceSector WHERE SectorName = @SectorName) > 0
	BEGIN
		SET @SectorName = ''
		SET @Symbol = SUBSTRING(@Symbol,1, LEN(@Symbol) - CHARINDEX(' ', REVERSE(@Symbol)))
	END

	SELECT @InvestmentSecurityID = InvestmentSecurityID FROM InvestmentSecurity WHERE Symbol = @Symbol
	
	RETURN @InvestmentSecurityID

END
