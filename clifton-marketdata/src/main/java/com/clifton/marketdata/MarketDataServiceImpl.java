package com.clifton.marketdata;


import com.clifton.core.util.validation.ValidationUtils;
import com.clifton.investment.instrument.InvestmentInstrumentService;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.investment.setup.InvestmentInstrumentHierarchy;
import com.clifton.investment.setup.retriever.NotionalMultiplierRetriever;
import com.clifton.system.bean.SystemBeanService;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataServiceImpl</code> class provides basic implementation of the MarketDataService interface.
 *
 * @author Mary Anderson
 */
@Service
public class MarketDataServiceImpl implements MarketDataService {

	private InvestmentInstrumentService investmentInstrumentService;
	private MarketDataRetriever marketDataRetriever;
	private SystemBeanService systemBeanService;

	////////////////////////////////////////////////////////////////////////////
	///////          Investment Security Market Data Methods           /////////
	////////////////////////////////////////////////////////////////////////////


	@Override
	public BigDecimal getMarketDataPriceFlexibleForSecurity(int securityId, Date date, Boolean exceptionIfMissing) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		return getMarketDataRetriever().getPriceFlexible(security, date == null ? new Date() : date, exceptionIfMissing);
	}


	@Override
	public BigDecimal getMarketDataIndexRatioForSecurity(int securityId, Date date) {
		return getMarketDataIndexRatioForSecurity(securityId, date, false);
	}


	@Override
	public BigDecimal getMarketDataIndexRatioForSecurity(int securityId, Date date, boolean exceptionIfMissing) {
		InvestmentSecurity security = getInvestmentInstrumentService().getInvestmentSecurity(securityId);
		InvestmentInstrumentHierarchy hierarchy = security.getInstrument().getHierarchy();
		if (hierarchy.isIndexRatioAdjusted()) {
			ValidationUtils.assertNotNull(security.getInstrument().getHierarchy().getNotionalMultiplierRetriever(), "A Notional Multiplier Retriever is required to calculate the Index Ratio for this security: " + security.getLabel() + ".");
			NotionalMultiplierRetriever retriever = (NotionalMultiplierRetriever) getSystemBeanService().getBeanInstance(security.getInstrument().getHierarchy().getNotionalMultiplierRetriever());
			BigDecimal indexRatio = retriever.retrieveNotionalMultiplier(security, () -> date, exceptionIfMissing);
			return indexRatio == null ? BigDecimal.ONE : indexRatio;
		}
		return BigDecimal.ONE;
	}

	////////////////////////////////////////////////////////////////////////////
	/////////             Getter & Setter Methods                   ////////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataRetriever getMarketDataRetriever() {
		return this.marketDataRetriever;
	}


	public void setMarketDataRetriever(MarketDataRetriever marketDataRetriever) {
		this.marketDataRetriever = marketDataRetriever;
	}


	public InvestmentInstrumentService getInvestmentInstrumentService() {
		return this.investmentInstrumentService;
	}


	public void setInvestmentInstrumentService(InvestmentInstrumentService investmentInstrumentService) {
		this.investmentInstrumentService = investmentInstrumentService;
	}


	public SystemBeanService getSystemBeanService() {
		return this.systemBeanService;
	}


	public void setSystemBeanService(SystemBeanService systemBeanService) {
		this.systemBeanService = systemBeanService;
	}
}
