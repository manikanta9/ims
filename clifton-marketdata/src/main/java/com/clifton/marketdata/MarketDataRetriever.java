package com.clifton.marketdata;


import com.clifton.investment.account.InvestmentAccount;
import com.clifton.investment.instrument.InvestmentSecurity;
import com.clifton.marketdata.field.MarketDataValueHolder;
import com.clifton.marketdata.live.MarketDataLive;

import java.math.BigDecimal;
import java.util.Date;


/**
 * The <code>MarketDataRetrieverImpl</code> contain helpful methods for dealing with market data look ups
 * and calculations against market data.  This class is not a service, i.e. NOT exposed to UI but used
 * within code only
 * <p>
 * Examples: Get Security Return for Given Date Range, CCY Return for Date Range, etc.
 *
 * @author manderson
 */
public interface MarketDataRetriever {

	////////////////////////////////////////////////////////////////////////////
	///////                       Price Lookup Methods                   ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns BigDecimal price value representing the given {@link InvestmentSecurity} price for a given date.
	 * 1.  Currencies don't have prices, so returns null if the given security is a currency
	 * 2.  Looks at price for the give date
	 * 3.  If both are missing, checks if the security's exchange calendar (or default system calendar if not set for the exchange) is a holiday, if yes will get the price for the previous business day
	 * If still missing, then will either return null, or throw a ValidationException (if exceptionIfMissing is true)
	 * 4.  Uses Normalized Pricing, so looks up BIG price if necessary
	 * <p>
	 * Note price fields that are used for the security are defined by MarketDataFieldMapping.  Last Trade Price is used as the default
	 * price field if none is explicitly mapped.
	 * <p>
	 * For securities priced monthly, quarterly, etc. Will return the most recent price as long as it's considered a valid date for the given date. i.e. >= previous month end
	 *
	 * @param exceptionMessagePrefix - Can be used in the exception message to clarify what security caused the issue - i.e. Asset Class Benchmark, Manager Benchmark, etc.
	 */
	public BigDecimal getPrice(InvestmentSecurity security, Date date, boolean exceptionIfMissing, String exceptionMessagePrefix);


	/**
	 * Same as getPrice or getPriceFlexible except it returns the MarketDataValueHolder object in order to retrieve additional details about the price, i.e. date, adjustment factor
	 */
	public MarketDataValueHolder getPriceMarketDataValueHolder(InvestmentSecurity security, Date date, boolean flexible, boolean exceptionIfMissing, String exceptionMessagePrefix);


	/**
	 * Returns the latest official price for the security as a MarketDataValueHolder so we know the date the price exists.
	 * If maxDaysBackAllowed is set, will confirm the price date is within x days from the given date
	 * If price is missing or not within given date range, will ALWAYS throw an exception.
	 * Will only return NULL if the official price field is not mapped
	 * This is currently only used for Billing for the Fund Investors
	 * <p>
	 * * @param exceptionMessagePrefix - Can be used in the exception message to clarify what security caused the issue - i.e. Asset Class Benchmark, Manager Benchmark, etc.
	 */
	public MarketDataValueHolder getOfficialPriceFlexible(InvestmentSecurity security, Date date, Integer maxDaysBackAllowed);


	/**
	 * Returns the date of the price used for the given date.  This can be used to find securities that are not priced monthly, but
	 * we need to know the actual date of the price that is being used (for violations)
	 */
	public Date getPriceDate(InvestmentSecurity security, Date date, boolean exceptionIfMissing, String exceptionMessagePrefix);


	/**
	 * Returns BigDecimal price value representing the given {@link InvestmentSecurity} price for a given date.
	 * 1.  Currencies don't have prices, so returns null if the given security is a currency
	 * 2.  Looks for most recent price on or before the given date.
	 * 3.  If missing, then will either return null, or throw a ValidationException (if exceptionIfMissing is true)
	 * 4.  Uses Normalized Pricing, so looks up BIG price if necessary
	 * <p>
	 * Note price fields that are used for the security are defined by MarketDataFieldMapping.  Last Trade Price is used as the default
	 * price field if none is explicitly mapped.
	 */
	public BigDecimal getPriceFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	/**
	 * USES MarketDataValue.measureValueAdjusted values
	 * <p>
	 * Returns BigDecimal price value representing the given {@link InvestmentSecurity} price for a given date.
	 * 1.  Currencies don't have prices, so returns null if the given security is a currency
	 * 2.  Looks at price for the give date
	 * 3.  If both are missing, checks if the security's exchange calendar (or default system calendar if not set for the exchange) is a holiday, if yes will get the price for the previous business day
	 * If still missing, then will either return null, or throw a ValidationException (if exceptionIfMissing is true)
	 * 4.  Uses Normalized Pricing, so looks up BIG price if necessary
	 * <p>
	 * Note price fields that are used for the security are defined by MarketDataFieldMapping.  Last Trade Price is used as the default
	 * price field if none is explicitly mapped.
	 *
	 * @param exceptionMessagePrefix - Can be used in the exception message to clarify what security caused the issue - i.e. Asset Class Benchmark, Manager Benchmark, etc.
	 */
	public BigDecimal getPriceAdjusted(InvestmentSecurity security, Date date, boolean exceptionIfMissing, String exceptionMessagePrefix);


	/**
	 * USES MarketDataValue.measureValueAdjusted values
	 * <p>
	 * Returns BigDecimal price value representing the given {@link InvestmentSecurity} adjusted price for a given date.
	 * 1.  Currencies don't have prices, so returns null if the given security is a currency
	 * 2.  Looks for most recent price on or before the given date.
	 * 3.  If missing, then will either return null, or throw a ValidationException (if exceptionIfMissing is true)
	 * 4.  Uses Normalized Pricing, so looks up BIG price if necessary
	 * <p>
	 * Note price fields that are used for the security are defined by MarketDataFieldMapping.  Last Trade Price is used as the default
	 * price field if none is explicitly mapped.
	 */
	public BigDecimal getPriceAdjustedFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);

	////////////////////////////////////////////////////////////////////////////
	///////              Other Data Fields Lookup Methods                ///////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * Returns the duration value for a given security on or before a given date, using {@link com.clifton.marketdata.field.MarketDataField#FIELD_DURATION} as the field name.
	 * If exceptionIfMissing is true, will throw an exception  if it can't find a duration value, otherwise will return null;
	 */
	public BigDecimal getDurationFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	/**
	 * Returns the duration value for a given security on or before a given date, with an optional field name override.
	 * Uses <code>fieldNameOverride</code>, if not null, or {@link com.clifton.marketdata.field.MarketDataField#FIELD_DURATION} as the field name.
	 * If exceptionIfMissing is true, will throw an exception  if it can't find a duration value, otherwise will return null;
	 */
	public BigDecimal getDurationFlexible(InvestmentSecurity security, Date date, String fieldNameOverride, boolean exceptionIfMissing);


	public BigDecimal getCreditDuration(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	/**
	 * Returns the delta value for a given security on or before a given date.  If exceptionIfMissing is true, will throw an exception if it can't find
	 * a delta value, otherwise will return null;
	 */
	public BigDecimal getDeltaFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	/**
	 * Returns the Delta value for the given security on the given date or on or before given date based on the security pricing frequency.
	 * So, if the security is priced daily, will look for the delta on the given date, else will do the flexible retrieval.
	 * <p>
	 * If exceptionIfMissing is true, will throw an exception if it can't find a delta value, otherwise will return null;
	 */
	public BigDecimal getDeltaOnDateOrFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	/**
	 * Returns the DV01 value for a given security on or before a given date.  If exceptionIfMissing is true, will throw an exception if it can't find
	 * a DV01 value, otherwise will return null;
	 */
	public BigDecimal getDV01Flexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	/**
	 * Returns the DV01 (Spot) value for a given security on or before a given date.  If exceptionIfMissing is true, will throw an exception if it can't find
	 * a DV01 (Spot) value, otherwise will return null;
	 */
	public BigDecimal getDV01SpotFlexible(InvestmentSecurity security, Date date, boolean exceptionIfMissing);


	public BigDecimal getFutureFairValue(InvestmentSecurity security, Date date);


	/**
	 * Returns the value for a given security and {@link com.clifton.marketdata.field.MarketDataField} name on or before a given date.
	 * If exceptionIfMissing is true, will throw an exception if it can't find a value for the provided field.
	 *
	 * @throws com.clifton.core.validation.ValidationExceptionWithCause with the security as the cause if a value cannot be found
	 */
	public BigDecimal getSecurityDataFieldValueFlexible(InvestmentSecurity security, Date date, String fieldName, boolean exceptionIfMissing);


	/**
	 * Returns the value for a given security and {@link com.clifton.marketdata.field.MarketDataField} name on a given date (non-flexible).
	 * If exceptionIfMissing is true, will throw an exception if it can't find a value for the provided field.
	 *
	 * @throws com.clifton.core.validation.ValidationExceptionWithCause with the security as the cause if a value cannot be found
	 */
	public BigDecimal getSecurityDataFieldValue(InvestmentSecurity security, Date date, String fieldName, boolean exceptionIfMissing);

	////////////////////////////////////////////////////////////////////////////
	/////              Market Data Return Calculation Methods             //////
	////////////////////////////////////////////////////////////////////////////


	/**
	 * NOTE: ALWAYS USES ADJUSTED PRICES FOR RETURNS
	 *
	 * @param account                - Is not required and is only used if looking for CCY returns.  Will use the FX Rate to account base CCY, if not defined will use USD
	 * @param exceptionMessagePrefix - Can be used in the exception message to clarify what security caused the issue - i.e. Asset Class Benchmark, Manager Benchmark, etc.
	 */
	public BigDecimal getInvestmentSecurityReturn(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix, boolean returnAsHundredsValue);


	/**
	 * NOTE: ALWAYS USES ADJUSTED PRICES FOR RETURNS
	 * <p>
	 * Same as getInvestmentSecurityReturn, however uses flexible pricing (i.e. Price on or before dates)
	 *
	 * @param account                - Is not required and is only used if looking for CCY returns.  Will use the FX Rate to account base CCY, if not defined will use USD
	 * @param exceptionMessagePrefix - Can be used in the exception message to clarify what security caused the issue - i.e. Asset Class Benchmark, Manager Benchmark, etc.
	 */
	public BigDecimal getInvestmentSecurityReturnFlexible(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix,
	                                                      boolean returnAsHundredsValue);


	/**
	 * NOTE: ALWAYS USES ADJUSTED PRICES FOR RETURNS
	 * <p>
	 * Same as getInvestmentSecurityReturnFlexible, will perform an annualization calculation based on whether or not
	 * the specified date range should be interpreted as days of months accuracy
	 *
	 * @param account                - Is not required and is only used if looking for CCY returns.  Will use the FX Rate to account base CCY, if not defined will use USD
	 * @param exceptionMessagePrefix - Can be used in the exception message to clarify what security caused the issue - i.e. Asset Class Benchmark, Manager Benchmark, etc.
	 */
	public BigDecimal getInvestmentSecurityReturnAnnualized(InvestmentAccount account, InvestmentSecurity security, Date startDate, Date endDate, String exceptionMessagePrefix,
	                                                        boolean returnAsHundredsValue, boolean dailyNotMonthlyCalculation);


	////////////////////////////////////////////////////////////////////////////
	///////        Investment Security Market Data Live Methods         ////////
	////////////////////////////////////////////////////////////////////////////


	public MarketDataLive getLivePrice(InvestmentSecurity security);


	public MarketDataLive getLiveFutureFairValue(InvestmentSecurity security);


	/**
	 * Returns the live Delta value for the provided security. If the security does not support a Delta value or no value could be obtained, null will be returned.
	 */
	public MarketDataLive getLiveDelta(InvestmentSecurity security);
}
